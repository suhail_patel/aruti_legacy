﻿Option Strict On
'************************************************************************************************************************************
'Class Name :clsScoreCalibrationApproval.vb
'Purpose    :
'Date       :29-May-2019
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsScoreCalibrationApproval
    Private Shared ReadOnly mstrModuleName As String = "clsScoreCalibrationApproval"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Enums "

    Public Enum enCalibrationStatus
        NotSubmitted = 0
        Submitted = 1
        Approved = 2
        Rejected = 3
    End Enum

#End Region

#Region " Private variables "

    Private mintCalibrationId As Integer = 0
    Private mstrTranguid As String = String.Empty
    Private mdtTransactionDate As DateTime = Nothing
    Private mintMappingunkid As Integer = 0
    Private mstrApprovalremark As String = String.Empty
    Private mstrCalibration_Remark As String = String.Empty
    Private mblnIsFinal As Boolean = False
    Private mintStatusunkid As Integer = 0
    Private mintEmployeunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mDecCalibrated_Score As Decimal = 0
    Private mintLoginemployeeunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mstrVoidreason As String = String.Empty
    Private mdtAuditdatetime As DateTime = Nothing
    Private mintAudittype As Integer = 0
    Private mintAuditUserunkid As Integer = 0
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean = False
    Private mblnIsProcessed As Boolean = False

#End Region

#Region " Properties "

    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
        End Set
    End Property

    Public Property _CalibrationId() As Integer
        Get
            Return mintCalibrationId
        End Get
        Set(ByVal value As Integer)
            mintCalibrationId = value
        End Set
    End Property

    Public Property _TransactionDate() As DateTime
        Get
            Return mdtTransactionDate
        End Get
        Set(ByVal value As DateTime)
            mdtTransactionDate = value
        End Set
    End Property

    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    Public Property _Approvalremark() As String
        Get
            Return mstrApprovalremark
        End Get
        Set(ByVal value As String)
            mstrApprovalremark = value
        End Set
    End Property

    Public Property _IsFinal() As Boolean
        Get
            Return mblnIsFinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsFinal = value
        End Set
    End Property

    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    Public Property _Employeunkid() As Integer
        Get
            Return mintEmployeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeunkid = value
        End Set
    End Property

    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Public Property _Calibrated_Score() As Decimal
        Get
            Return mDecCalibrated_Score
        End Get
        Set(ByVal value As Decimal)
            mDecCalibrated_Score = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _Auditdatetime() As DateTime
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditdatetime = value
        End Set
    End Property

    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    Public Property _AuditUserunkid() As Integer
        Get
            Return mintAuditUserunkid
        End Get
        Set(ByVal value As Integer)
            mintAuditUserunkid = value
        End Set
    End Property

    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    Public Property _IsProcessed() As Boolean
        Get
            Return mblnIsProcessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsProcessed = value
        End Set
    End Property

    Public Property _Calibration_Remark() As String
        Get
            Return mstrCalibration_Remark
        End Get
        Set(ByVal value As String)
            mstrCalibration_Remark = value
        End Set
    End Property

#End Region

#Region " Private & Public Function(s) "

    Public Function InsertByDataTable(ByVal xDataTable As DataTable, _
                                      ByVal xDatabaseName As String, _
                                      ByVal xUserUnkid As Integer, _
                                      ByVal xYearUnkid As Integer, _
                                      ByVal xCompanyUnkid As Integer, _
                                      ByVal xTransactiondate As DateTime, _
                                      ByVal xMappingunkid As Integer, _
                                      ByVal xApprovalRemark As String, _
                                      ByVal xStatusunkid As Integer, _
                                      ByVal xAuditDatetime As DateTime, _
                                      ByVal xAuditTypeId As Integer, _
                                      ByVal xIpAddr As String, _
                                      ByVal xHost As String, _
                                      ByVal xFormName As String, _
                                      ByVal xIsweb As Boolean, _
                                      Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.BindTransaction()
        Try
            For Each xRow As DataRow In xDataTable.Rows
                _Approvalremark = xApprovalRemark
                _Auditdatetime = xAuditDatetime
                _Audittype = xAuditTypeId
                _AuditUserunkid = xUserUnkid
                _Calibrated_Score = CDec(xRow("calibratescore"))
                _CalibrationId = CInt(xRow("grpid"))
                _Employeunkid = CInt(xRow("employeeunkid"))
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                _Calibration_Remark = xRow("calibration_remark").ToString()
                'S.SANDEEP |27-JUL-2019| -- END
                _Form_Name = xFormName
                _Host = xHost
                _Ip = xIpAddr
                _IsFinal = CBool(xRow("isfinal"))
                _IsProcessed = False
                _Isvoid = False
                _Isweb = xIsweb
                _Loginemployeeunkid = 0
                _Mappingunkid = xMappingunkid
                _Periodunkid = CInt(xRow("periodunkid"))
                _Statusunkid = xStatusunkid
                _Tranguid = Guid.NewGuid.ToString
                _TransactionDate = xTransactiondate
                _Voidreason = ""

                If Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, objDataOperation, False) = False Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                If xStatusunkid = enCalibrationStatus.Rejected Then

                    'S.SANDEEP |12-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'StrQ = "INSERT INTO hrassess_computescore_rejection_tran(tranguid,employeeunkid,periodunkid,calibrated_score,calibratnounkid,overall_remark,submit_date,calibration_remark) VALUES " & _
                    '       " ('" & _Tranguid & "','" & _Employeunkid & "','" & _Periodunkid & "','" & _Calibrated_Score & "','" & _CalibrationId & "','" & xRow("o_remark").ToString() & "','" & xRow("s_date").ToString & "','" & xRow("calibration_remark").ToString() & "'); " & _
                    '       "UPDATE hrassess_computescore_approval_tran SET " & _
                    '       "    isprocessed = 1 " & _
                    '       "WHERE calibratnounkid = @calibratnounkid " & _
                    '       "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid ; " & _
                    '       "UPDATE hrassess_compute_score_master SET " & _
                    '       "     calibrated_score = 0 " & _
                    '       "    ,issubmitted = 0 " & _
                    '       "    ,calibration_remark = '' " & _
                    '       "    ,calibratnounkid = 0 " & _
                    '       "    ,submit_date = NULL " & _
                    '       "    ,overall_remark = '' " & _
                    '       "WHERE calibratnounkid = @calibratnounkid " & _
                    '       "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

                    'S.SANDEEP |30-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : {Incorrect Syntax near error}
                    'StrQ = "INSERT INTO hrassess_computescore_rejection_tran(tranguid,employeeunkid,periodunkid,calibrated_score,calibratnounkid,overall_remark,submit_date,calibration_remark,finaloverallscore) VALUES " & _
                    '       " ('" & _Tranguid & "','" & _Employeunkid & "','" & _Periodunkid & "','" & _Calibrated_Score & "','" & _CalibrationId & "','" & xRow("o_remark").ToString() & "','" & xRow("s_date").ToString & "','" & xRow("calibration_remark").ToString() & "','" & xRow("povisionscore").ToString & "'); " & _
                    '       "UPDATE hrassess_computescore_approval_tran SET " & _
                    '       "    isprocessed = 1 " & _
                    '       "WHERE calibratnounkid = @calibratnounkid " & _
                    '       "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid ; " & _
                    '       "UPDATE hrassess_compute_score_master SET " & _
                    '       "     calibrated_score = 0 " & _
                    '       "    ,issubmitted = 0 " & _
                    '       "    ,calibration_remark = '' " & _
                    '       "    ,calibratnounkid = 0 " & _
                    '       "    ,submit_date = NULL " & _
                    '       "    ,overall_remark = '' " & _
                    '       "WHERE calibratnounkid = @calibratnounkid " & _
                    '       "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

                    'S.SANDEEP |10-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : Calibration Issues
                    'StrQ = "INSERT INTO hrassess_computescore_rejection_tran(tranguid,employeeunkid,periodunkid,calibrated_score,calibratnounkid,overall_remark,submit_date,calibration_remark,finaloverallscore) VALUES " & _
                    '       "(@tranguid,@employeeunkid,@periodunkid,@calibrated_score,@calibratnounkid,@overall_remark,@submit_date,@calibration_remark,@finaloverallscore)" & _
                    '       "UPDATE hrassess_computescore_approval_tran SET " & _
                    '       "    isprocessed = 1 " & _
                    '       "WHERE calibratnounkid = @calibratnounkid " & _
                    '       "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid ; " & _
                    '       "UPDATE hrassess_compute_score_master SET " & _
                    '       "     calibrated_score = 0 " & _
                    '       "    ,issubmitted = 0 " & _
                    '       "    ,calibration_remark = '' " & _
                    '       "    ,calibratnounkid = 0 " & _
                    '       "    ,submit_date = NULL " & _
                    '       "    ,overall_remark = '' " & _
                    '       "WHERE calibratnounkid = @calibratnounkid " & _
                    '       "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

                    StrQ = "INSERT INTO hrassess_computescore_rejection_tran(tranguid,employeeunkid,periodunkid,calibrated_score,calibratnounkid,overall_remark,submit_date,calibration_remark,finaloverallscore) VALUES " & _
                           "(@tranguid,@employeeunkid,@periodunkid,@calibrated_score,@calibratnounkid,@overall_remark,@submit_date,@calibration_remark,@finaloverallscore)" & _
                           "UPDATE hrassess_computescore_approval_tran SET " & _
                           "    isprocessed = 1 " & _
                           "WHERE calibratnounkid = @calibratnounkid " & _
                           "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid ; " & _
                           "UPDATE hrassess_computescore_approval_tran SET statusunkid = 0 WHERE calibratnounkid = @calibratnounkid AND mappingunkid <= 0; " & _
                           "UPDATE hrassess_computescore_approval_tran SET isvoid = 1,voidreason = 'Batch Rejected' WHERE calibratnounkid = @calibratnounkid AND statusunkid > 0; " & _
                           "UPDATE hrassess_computescore_approval_tran SET isprocessed = 0 WHERE calibratnounkid = @calibratnounkid AND mappingunkid <= 0 AND isvoid = 0; " & _
                           "UPDATE hrassess_compute_score_master SET issubmitted = 0,submit_date = NULL WHERE calibratnounkid = @calibratnounkid "


                    '"UPDATE hrassess_compute_score_master SET " & _
                    '"     calibrated_score = 0 " & _
                    '"    ,issubmitted = 0 " & _
                    '"    ,calibration_remark = '' " & _
                    '"    ,calibratnounkid = 0 " & _
                    '"    ,submit_date = NULL " & _
                    '"    ,overall_remark = '' " & _
                    '"WHERE calibratnounkid = @calibratnounkid " & _
                    '"AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "
                    'S.SANDEEP |10-OCT-2019| -- END


                    'S.SANDEEP |30-SEP-2019| -- END
                    
                    'S.SANDEEP |12-JUL-2019| -- END

                    objDataOperation.ClearParameters()


                    'S.SANDEEP |30-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : {Incorrect Syntax near error}
                    objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, _Tranguid)
                    objDataOperation.AddParameter("@overall_remark", SqlDbType.NVarChar, xRow("o_remark").ToString().Length, xRow("o_remark").ToString())
                    If IsDBNull(xRow("s_date")) Then
                        objDataOperation.AddParameter("@submit_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, Now)
                    Else
                        objDataOperation.AddParameter("@submit_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, xRow("s_date"))
                    End If
                    objDataOperation.AddParameter("@calibration_remark", SqlDbType.NVarChar, xRow("calibration_remark").ToString().Length, xRow("calibration_remark").ToString())
                    objDataOperation.AddParameter("@finaloverallscore", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, xRow("povisionscore"))
                    'S.SANDEEP |30-SEP-2019| -- END
                    objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow("grpid"))
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow("employeeunkid"))
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow("periodunkid"))
                    objDataOperation.AddParameter("@calibrated_score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, xRow("calibratescore"))

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                ElseIf xStatusunkid = enCalibrationStatus.Approved Then
                    If CBool(xRow("isfinal")) Then
                        StrQ = "UPDATE hrassess_computescore_approval_tran SET " & _
                               "    isprocessed = 1 " & _
                               "WHERE calibratnounkid = @calibratnounkid " & _
                               "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid ; " & _
                               "UPDATE hrassess_compute_score_master SET " & _
                               "     calibrated_score = @calibrated_score " & _
                               "    ,isprocessed = 1 " & _
                               "WHERE calibratnounkid = @calibratnounkid " & _
                               "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow("grpid"))
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow("employeeunkid"))
                        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow("periodunkid"))
                        objDataOperation.AddParameter("@calibrated_score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, xRow("calibratescore"))

                        objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If
                End If

            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertByDataTable; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Insert(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal blnCheckRowExistance As Boolean = True) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim IStrQ, UStrQ As String : IStrQ = "" : UStrQ = ""

            IStrQ = "INSERT INTO hrassess_computescore_approval_tran ( " & _
                        "  tranguid " & _
                        ", calibratnounkid " & _
                        ", transactiondate " & _
                        ", mappingunkid " & _
                        ", approvalremark " & _
                        ", calibration_remark " & _
                        ", isfinal " & _
                        ", statusunkid " & _
                        ", employeeunkid " & _
                        ", periodunkid " & _
                        ", calibrated_score " & _
                        ", loginemployeeunkid " & _
                        ", isvoid " & _
                        ", voidreason " & _
                        ", auditdatetime " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", ip " & _
                        ", host " & _
                        ", form_name " & _
                        ", isweb " & _
                        ", isprocessed " & _
                    ") VALUES (" & _
                        "  @tranguid " & _
                        ", @calibratnounkid " & _
                        ", @transactiondate " & _
                        ", @mappingunkid " & _
                        ", @approvalremark " & _
                        ", @calibration_remark " & _
                        ", @isfinal " & _
                        ", @statusunkid " & _
                        ", @employeeunkid " & _
                        ", @periodunkid " & _
                        ", @calibrated_score " & _
                        ", @loginemployeeunkid " & _
                        ", @isvoid " & _
                        ", @voidreason " & _
                        ", @auditdatetime " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @ip " & _
                        ", @host " & _
                        ", @form_name " & _
                        ", @isweb " & _
                        ", @isprocessed " & _
                    ") "
            UStrQ = "UPDATE hrassess_computescore_approval_tran SET " & _
                    "     calibrated_score = @calibrated_score " & _
                    "    ,auditdatetime = @auditdatetime " & _
                    "    ,audittype = @audittype " & _
                    "    ,audituserunkid = @audituserunkid " & _
                    "    ,ip = @ip " & _
                    "    ,host = @host " & _
                    "    ,form_name = @form_name " & _
                    "    ,statusunkid = @statusunkid " & _
                    "    ,calibration_remark  = @calibration_remark " & _
                    "WHERE calibratnounkid = @calibratnounkid " & _
                    "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

            If blnCheckRowExistance Then
                If IsExists(mintEmployeunkid, mintPeriodunkid, mintCalibrationId, objDataOperation) = False Then
                    StrQ = IStrQ
                Else
                    StrQ = UStrQ
                End If
            Else
                StrQ = IStrQ
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.NAME_SIZE, mintCalibrationId.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactionDate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, mstrApprovalremark.Length, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@calibrated_score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecCalibrated_Score.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditdatetime.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcessed.ToString)
            objDataOperation.AddParameter("@calibration_remark", SqlDbType.NVarChar, mstrCalibration_Remark.Length, mstrCalibration_Remark)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Public Function GetList(ByVal xPeriodId As Integer, _
    '                        ByVal xDatabaseName As String, _
    '                        ByVal xUserUnkid As Integer, _
    '                        ByVal xYearUnkid As Integer, _
    '                        ByVal xCompanyUnkid As Integer, _
    '                        ByVal xPeriodStart As DateTime, _
    '                        ByVal xPeriodEnd As DateTime, _
    '                        ByVal xUserModeSetting As String, _
    '                        ByVal xOnlyApproved As Boolean, _
    '                        ByVal xIncludeIn_ActiveEmployee As Boolean, _
    '                        ByVal strTableName As String, _
    '                        Optional ByVal strFilterString As String = "", _
    '                        Optional ByVal xDataOpr As clsDataOperation = Nothing, _
    '                        Optional ByVal blnGroupByCalibrator As Boolean = False) As DataTable 'S.SANDEEP |25-OCT-2019| -- START {blnGroupByCalibrator}-- END
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception = Nothing
    '    Dim objDataOperation As clsDataOperation
    '    Dim dsList As New DataSet
    '    If xDataOpr IsNot Nothing Then
    '        objDataOperation = xDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    Try
    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

    '        'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

    '        StrQ = "SELECT " & _
    '               "     A.Employee " & _
    '               "    ,A.DATE " & _
    '               "    ,A.povisionscore " & _
    '               "    ,A.calibratescore " & _
    '               "    ,A.isgrp " & _
    '               "    ,A.employeeunkid " & _
    '               "    ,A.periodunkid " & _
    '               "    ,A.statusunkid " & _
    '               "    ,A.grpid " & _
    '               "    ,A.submitdate " & _
    '               "    ,A.s_date " & _
    '               "    ,A.c_remark " & _
    '               "    ,A.o_remark " & _
    '               "    ,'' AS pRating " & _
    '               "    ,'' AS cRating " & _
    '               "    ,'' AS allocation " & _
    '               "    ,audituserunkid " & _
    '               "    ,'' AS calibuser " & _
    '               "    ,calibratnounkid " & _
    '               "FROM " & _
    '               "( " & _
    '               "    SELECT DISTINCT " & _
    '               "         @CalibrationNo +  calibration_no AS Employee " & _
    '               "        ,'' AS DATE " & _
    '               "        ,ISNULL(CONVERT(NVARCHAR(8),pScore.submit_date,112),'') AS submitdate " & _
    '               "        ,'' AS c_remark " & _
    '               "        ,'' AS o_remark " & _
    '               "        ,CAST(0 AS DECIMAL(36, 2)) AS povisionscore " & _
    '               "        ,CAST(0 AS DECIMAL(36, 2)) AS calibratescore " & _
    '               "        ,CAST(1 AS BIT) AS isgrp " & _
    '               "        ,hrassess_calibration_number.calibratnounkid AS grpid " & _
    '               "        ,0 AS employeeunkid " & _
    '               "        ,hrassess_computescore_approval_tran.periodunkid " & _
    '               "        ,hrassess_computescore_approval_tran.statusunkid " & _
    '               "        ,'' AS calibration_remark " & _
    '               "        ,NULL AS s_date " & _
    '               "        ,hrassess_computescore_approval_tran.audituserunkid " & _
    '               "        ,hrassess_computescore_approval_tran.calibratnounkid " & _
    '               "    FROM hrassess_computescore_approval_tran " & _
    '               "        JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
    '               "        JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '               "    LEFT JOIN " & _
    '               "    ( " & _
    '               "        SELECT DISTINCT " & _
    '               "             calibratnounkid " & _
    '               "            ,employeeunkid " & _
    '               "            ,periodunkid " & _
    '               "            ,finaloverallscore " & _
    '               "            ,submit_date " & _
    '               "            ,calibration_remark AS c_remark " & _
    '               "            ,overall_remark AS o_remark " & _
    '               "        FROM hrassess_compute_score_master " & _
    '               "        WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
    '               "    ) AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
    '               "     AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid "

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= xDateJoinQry
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= xAdvanceJoinQry
    '        End If

    '        StrQ &= "    WHERE isvoid = 0 /*AND isprocessed = 0*/ AND statusunkid IN (0,1) AND hrassess_computescore_approval_tran.periodunkid = @periodunkid "

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        If xIncludeIn_ActiveEmployee = False Then
    '            If xDateFilterQry.Trim.Length > 0 Then
    '                StrQ &= xDateFilterQry
    '            End If
    '        End If

    '        If strFilterString.Trim.Length > 0 Then
    '            StrQ &= " AND " & strFilterString
    '        End If

    '        StrQ &= "UNION " & _
    '               "    SELECT " & _
    '               "         '/r/n' + employeecode + ' - ' + firstname + ' ' + surname AS Employee " & _
    '               "        ,CONVERT(NVARCHAR(8), transactiondate, 112) AS DATE " & _
    '               "        ,ISNULL(CONVERT(NVARCHAR(8),pScore.submit_date,112),'') AS submitdate " & _
    '               "        ,ISNULL(pScore.c_remark,'') AS c_remark " & _
    '               "        ,ISNULL(pScore.o_remark,'') AS o_remark " & _
    '               "        ,CAST(pScore.finaloverallscore AS DECIMAL(36, 2)) AS povisionscore " & _
    '               "        ,CAST(calibrated_score AS DECIMAL(36, 2)) AS calibratescore " & _
    '               "        ,CAST(0 AS BIT) AS isgrp " & _
    '               "        ,hrassess_calibration_number.calibratnounkid AS grpid " & _
    '               "        ,hrassess_computescore_approval_tran.employeeunkid " & _
    '               "        ,hrassess_computescore_approval_tran.periodunkid " & _
    '               "        ,hrassess_computescore_approval_tran.statusunkid " & _
    '               "        ,hrassess_computescore_approval_tran.calibration_remark " & _
    '               "        ,pScore.submit_date AS s_date " & _
    '               "        ,hrassess_computescore_approval_tran.audituserunkid " & _
    '               "        ,hrassess_computescore_approval_tran.calibratnounkid " & _
    '               "    FROM hrassess_computescore_approval_tran " & _
    '               "    LEFT JOIN " & _
    '               "    ( " & _
    '               "        SELECT DISTINCT " & _
    '               "             calibratnounkid " & _
    '               "            ,employeeunkid " & _
    '               "            ,periodunkid " & _
    '               "            ,finaloverallscore " & _
    '               "            ,submit_date " & _
    '               "            ,calibration_remark AS c_remark " & _
    '               "            ,overall_remark AS o_remark " & _
    '               "        FROM hrassess_compute_score_master " & _
    '               "        WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
    '               "    ) AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
    '               "     AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
    '               "    JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
    '               "    JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid "
    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= xDateJoinQry
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= xAdvanceJoinQry
    '        End If

    '        StrQ &= "    WHERE isvoid = 0 /*AND isprocessed = 0*/ AND statusunkid IN (0,1) AND hrassess_computescore_approval_tran.periodunkid = @periodunkid "

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        If xIncludeIn_ActiveEmployee = False Then
    '            If xDateFilterQry.Trim.Length > 0 Then
    '                StrQ &= xDateFilterQry
    '            End If
    '        End If

    '        If strFilterString.Trim.Length > 0 Then
    '            StrQ &= " AND " & strFilterString
    '        End If

    '        StrQ &= ") AS A WHERE 1 = 1 " & _
    '                "ORDER BY A.grpid, A.isgrp DESC "
    '        'S.SANDEEP |13-DEC-2019| -- START {REPLACED "AND isprocessed = 0" WITH /*AND isprocessed = 0*/ } -- END


    '        'S.SANDEEP |11-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : PA CHANGES
    '        '---------- REMOVED -> statusunkid = 1
    '        '---------- ADDED   -> statusunkid IN (0,1)
    '        'S.SANDEEP |11-JUL-2019| -- END

    '        objDataOperation.AddParameter("@CalibrationNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Calibration No : "))
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)

    '        dsList = objDataOperation.ExecQuery(StrQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'S.SANDEEP |14-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        If dsList IsNot Nothing Then
    '            Dim iCol As DataColumn
    '            iCol = New DataColumn
    '            With iCol
    '                .DataType = GetType(System.Boolean)
    '                .ColumnName = "ispgrp"
    '                .DefaultValue = False
    '            End With
    '            dsList.Tables(0).Columns.Add(iCol)

    '            iCol = New DataColumn
    '            With iCol
    '                .DataType = GetType(System.Int32)
    '                .ColumnName = "pgrpid"
    '                .DefaultValue = 0
    '            End With
    '            dsList.Tables(0).Columns.Add(iCol)
    '        End If
    '        'S.SANDEEP |14-NOV-2019| -- END

    '        'S.SANDEEP |26-AUG-2019| -- START
    '        'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    '        '---------- ADDED   -> '' AS allocation,audituserunkid,'' AS calibuser
    '        Dim dtUsrAllocation As DataTable = GetUserAllocation(xPeriodId, xCompanyUnkid, objDataOperation)
    '        If dtUsrAllocation IsNot Nothing Then
    '            If dtUsrAllocation.Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateRating(x, dtUsrAllocation, True))
    '        End If
    '        'S.SANDEEP |26-AUG-2019| -- END

    '        'S.SANDEEP |25-OCT-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        If blnGroupByCalibrator = True Then
    '            Dim iCalibrator As Dictionary(Of Integer, String)
    '            iCalibrator = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("audituserunkid").ToString().Trim.Length > 0).Select(Function(row) New With {Key .attribute1_name = row.Field(Of Integer)("audituserunkid"), Key .attribute2_name = row.Field(Of String)("calibuser")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
    '            If iCalibrator IsNot Nothing AndAlso iCalibrator.Keys.Count > 0 Then
    '                'S.SANDEEP |14-NOV-2019| -- START
    '                'ISSUE/ENHANCEMENT : Calibration Issues
    '                'Dim iCol As DataColumn
    '                'iCol = New DataColumn
    '                'With iCol
    '                '    .DataType = GetType(System.Boolean)
    '                '    .ColumnName = "ispgrp"
    '                '    .DefaultValue = False
    '                'End With
    '                'dsList.Tables(0).Columns.Add(iCol)

    '                'iCol = New DataColumn
    '                'With iCol
    '                '    .DataType = GetType(System.Int32)
    '                '    .ColumnName = "pgrpid"
    '                '    .DefaultValue = 0
    '                'End With
    '                'dsList.Tables(0).Columns.Add(iCol)
    '                'S.SANDEEP |14-NOV-2019| -- END


    '                If iCalibrator IsNot Nothing AndAlso iCalibrator.Keys.Count > 0 Then
    '                    For Each iKey As Integer In iCalibrator.Keys
    '                        Dim dt() As DataRow
    '                        dt = dsList.Tables(0).Select("audituserunkid = " & iKey)
    '                        If dt.Length > 0 Then
    '                            dt.AsEnumerable().ToList().ForEach(Function(x) UpdateParentGrp(x, iKey))
    '                        End If
    '                        Dim dr As DataRow = dsList.Tables(0).NewRow()
    '                        dr("Employee") = Language.getMessage(mstrModuleName, 211, "Calibrator :") & " " & iCalibrator(iKey) & _
    '                        IIf(dt(0)("allocation").ToString.Trim.Length > 0, " - (" & dt(0)("allocation").ToString & ")", "").ToString()
    '                        dr("DATE") = ""
    '                        dr("povisionscore") = "0"
    '                        dr("calibratescore") = "0"
    '                        dr("isgrp") = False
    '                        dr("employeeunkid") = dt(0)("employeeunkid")
    '                        dr("periodunkid") = dt(0)("periodunkid")
    '                        dr("statusunkid") = dt(0)("statusunkid")
    '                        dr("grpid") = dt(0)("grpid")
    '                        dr("submitdate") = dt(0)("submitdate")
    '                        dr("s_date") = dt(0)("s_date")
    '                        dr("c_remark") = ""
    '                        dr("o_remark") = ""
    '                        dr("pRating") = ""
    '                        dr("cRating") = ""
    '                        dr("allocation") = dt(0)("allocation")
    '                        dr("audituserunkid") = iKey
    '                        dr("calibuser") = iCalibrator(iKey)
    '                        dr("calibratnounkid") = dt(0)("calibratnounkid")
    '                        dr("ispgrp") = True
    '                        dr("pgrpid") = iKey
    '                        dsList.Tables(0).Rows.Add(dr)
    '                    Next
    '                End If
    '            End If
    '            Dim oData As DataTable
    '            oData = New DataView(dsList.Tables(0), "", "pgrpid,ispgrp DESC, grpid,isgrp DESC,employeeunkid", DataViewRowState.CurrentRows).ToTable().Copy()
    '            dsList.Tables.RemoveAt(0)
    '            dsList.Tables.Add(oData.Copy)
    '        End If
    '        'S.SANDEEP |25-OCT-2019| -- END

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dsList.Tables(0)
    'End Function

    Public Function GetList(ByVal xPeriodId As Integer, _
                            ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal strFilterString As String = "", _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal blnGroupByCalibrator As Boolean = False, _
                            Optional ByVal strEmployeeFilter As String = "") As DataTable 'S.SANDEEP |25-OCT-2019| -- START {blnGroupByCalibrator}-- END
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            RepairInvalidData(xPeriodId, xUserUnkid, objDataOperation, xDatabaseName)

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation.ExecNonQuery("SET ARITHABORT ON")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            StrQ = "SELECT A.Employee " & _
                     ",A.DATE " & _
                     ",CASE WHEN A.povisionscore <= 0 THEN '' ELSE CAST(A.povisionscore AS NVARCHAR(MAX)) END AS povisionscore " & _
                     ",CASE WHEN A.calibratescore <= 0 THEN '' ELSE CAST(A.calibratescore AS NVARCHAR(MAX)) END AS calibratescore " & _
                     ",A.isgrp " & _
                     ",A.employeeunkid " & _
                     ",A.periodunkid " & _
                     ",A.statusunkid " & _
                     ",A.grpid " & _
                     ",A.submitdate " & _
                     ",A.s_date " & _
                     ",A.c_remark " & _
                     ",A.o_remark " & _
                     ",CASE WHEN povisionscore > 0 THEN CASE WHEN povisionscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                     " ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND povisionscore >= score_from AND povisionscore <= score_to), '') END " & _
                     " ELSE '' END  AS pRating " & _
                     ",CASE WHEN calibratescore > 0 THEN CASE WHEN calibratescore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                     " ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibratescore >= score_from AND calibratescore <= score_to), '') END " & _
                     " ELSE '' END AS cRating " & _
                     ",'' AS allocation " & _
                     ",audituserunkid " & _
                     ",'' AS calibuser " & _
                     ",calibratnounkid " & _
                   "FROM " & _
                   "( " & _
                "SELECT DISTINCT " & _
                      "@CalibrationNo + CBN.calibration_no AS Employee " & _
                     ",'' AS DATE " & _
                     ",ISNULL(CONVERT(NVARCHAR(8), pScore.submit_date, 112), '') AS submitdate " & _
                     ",'' AS c_remark " & _
                     ",'' AS o_remark " & _
                     ",CAST(0 AS DECIMAL(36, 2)) AS povisionscore " & _
                     ",CAST(0 AS DECIMAL(36, 2)) AS calibratescore " & _
                     ",CAST(1 AS BIT) AS isgrp " & _
                     ",CBN.calibratnounkid AS grpid " & _
                     ",0 AS employeeunkid " & _
                     ",hrassess_computescore_approval_tran.periodunkid " & _
                     ",hrassess_computescore_approval_tran.statusunkid " & _
                     ",'' AS calibration_remark " & _
                     ",NULL AS s_date " & _
                     ",hrassess_computescore_approval_tran.audituserunkid " & _
                     ",hrassess_computescore_approval_tran.calibratnounkid " & _
                "FROM hrassess_computescore_approval_tran " & _
                "JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
                "JOIN hrscore_calibration_approver_tran AS CT ON CT.employeeunkid = hremployee_master.employeeunkid AND CT.visibletypeid = 1 AND CT.isvoid = 0 " & _
                "JOIN hrscore_calibration_approver_master AS CAM ON CAM.mappingunkid = CT.mappingunkid " & _
                "JOIN hrassess_calibration_number AS CBN ON hrassess_computescore_approval_tran.calibratnounkid = CBN.calibratnounkid " & _
                "LEFT JOIN " & _
                "( " & _
                     "SELECT DISTINCT " & _
                           "calibratnounkid " & _
                          ",EPT.employeeunkid " & _
                          ",periodunkid " & _
                          ",finaloverallscore " & _
                          ",submit_date " & _
                          ",calibration_remark AS c_remark " & _
                          ",overall_remark AS o_remark " & _
                     "FROM hrassess_compute_score_master " & _
                          "JOIN hrscore_calibration_approver_tran AS EPT ON EPT.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
                          "JOIN hrscore_calibration_approver_master AS BAM ON BAM.mappingunkid = EPT.mappingunkid AND BAM.iscalibrator = 1 " & _
                     "WHERE periodunkid  = @periodunkid AND hrassess_compute_score_master.isvoid = 0 AND EPT.isvoid = 0 AND EPT.visibletypeid = 1 " & _
                     "AND BAM.isvoid = 0 AND BAM.visibletypeid = 1 AND BAM.mapuserunkid  = @mapuserunkid " & _
                ") AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                "AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
                "AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            StrQ &= " WHERE hrassess_computescore_approval_tran.isvoid = 0 AND hrassess_computescore_approval_tran.statusunkid IN (0,1) " & _
                    " AND hrassess_computescore_approval_tran.periodunkid  = @periodunkid AND CAM.mapuserunkid  = @mapuserunkid AND CAM.isactive = 1 AND CAM.visibletypeid = 1 AND CAM.isvoid = 0 " & _
                    " AND CAM.iscalibrator = 1 "
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If strFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & strFilterString
            End If
            StrQ &= "UNION " & _
                "SELECT '/r/n' + employeecode + ' - ' + firstname + ' ' + surname AS Employee " & _
                          ",CONVERT(NVARCHAR(8), transactiondate, 112) AS DATE " & _
                          ",ISNULL(CONVERT(NVARCHAR(8), pScore.submit_date, 112), '') AS submitdate " & _
                          ",ISNULL(pScore.c_remark, '') AS c_remark " & _
                          ",ISNULL(pScore.o_remark, '') AS o_remark " & _
                          ",CAST(pScore.finaloverallscore AS DECIMAL(36, 2)) AS povisionscore " & _
                          ",CAST(calibrated_score AS DECIMAL(36, 2)) AS calibratescore " & _
                          ",CAST(0 AS BIT) AS isgrp " & _
                          ",hrassess_calibration_number.calibratnounkid AS grpid " & _
                          ",hrassess_computescore_approval_tran.employeeunkid " & _
                          ",hrassess_computescore_approval_tran.periodunkid " & _
                          ",hrassess_computescore_approval_tran.statusunkid " & _
                          ",hrassess_computescore_approval_tran.calibration_remark " & _
                          ",pScore.submit_date AS s_date " & _
                          ",hrassess_computescore_approval_tran.audituserunkid " & _
                          ",hrassess_computescore_approval_tran.calibratnounkid " & _
                     "FROM hrassess_computescore_approval_tran " & _
                     "JOIN hrscore_calibration_approver_tran AS CT ON CT.employeeunkid = hrassess_computescore_approval_tran.employeeunkid AND CT.visibletypeid = 1 AND CT.isvoid = 0 " & _
                     "JOIN hrscore_calibration_approver_master AS CAM ON CAM.mappingunkid = CT.mappingunkid " & _
                     "LEFT JOIN ( " & _
                          "SELECT DISTINCT " & _
                           "calibratnounkid " & _
                          ",EPT.employeeunkid " & _
                          ",periodunkid " & _
                          ",finaloverallscore " & _
                          ",submit_date " & _
                          ",calibration_remark AS c_remark " & _
                          ",overall_remark AS o_remark " & _
                     "FROM hrassess_compute_score_master " & _
                          "JOIN hrscore_calibration_approver_tran AS EPT ON EPT.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
                          "JOIN hrscore_calibration_approver_master AS BAM ON BAM.mappingunkid = EPT.mappingunkid AND BAM.iscalibrator = 1 " & _
                     "WHERE periodunkid  = @periodunkid AND hrassess_compute_score_master.isvoid = 0 AND EPT.isvoid = 0 AND EPT.visibletypeid = 1 " & _
                     "AND BAM.isvoid = 0 AND BAM.visibletypeid = 1 AND BAM.mapuserunkid  = @mapuserunkid " & _
                          ") AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                          "AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
                          "AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                     "JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
                     "JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            StrQ &= " WHERE hrassess_computescore_approval_tran.isvoid = 0 AND hrassess_computescore_approval_tran.statusunkid IN (0,1) " & _
                    " AND hrassess_computescore_approval_tran.periodunkid  = @periodunkid AND CAM.mapuserunkid  = @mapuserunkid AND CAM.isactive = 1 AND CAM.visibletypeid = 1 AND CAM.isvoid = 0  " & _
                    " AND CAM.iscalibrator = 1 "
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If strFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & strFilterString
            End If
            If strEmployeeFilter.Trim.Length > 0 Then
                StrQ &= strEmployeeFilter
            End If
            StrQ &= ") AS A " & _
                    " WHERE 1 = 1 " & _
                    "ORDER BY A.grpid, A.isgrp DESC "


            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
            objDataOperation.AddParameter("@CalibrationNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Calibration No : "))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then
                    Dim iCol As DataColumn
                    iCol = New DataColumn
                    With iCol
                        .DataType = GetType(System.Boolean)
                        .ColumnName = "ispgrp"
                        .DefaultValue = False
                    End With
                    dsList.Tables(0).Columns.Add(iCol)

                    iCol = New DataColumn
                    With iCol
                        .DataType = GetType(System.Int32)
                        .ColumnName = "pgrpid"
                        .DefaultValue = 0
                    End With
                    dsList.Tables(0).Columns.Add(iCol)
            End If

            Dim dtUsrAllocation As DataTable = GetUserAllocation(xPeriodId, xCompanyUnkid, objDataOperation)
            If dtUsrAllocation IsNot Nothing Then
                If dtUsrAllocation.Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateRating(x, dtUsrAllocation, True))
            End If

            If blnGroupByCalibrator = True Then
                Dim iCalibrator As Dictionary(Of Integer, String)
                iCalibrator = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("audituserunkid").ToString().Trim.Length > 0).Select(Function(row) New With {Key .attribute1_name = row.Field(Of Integer)("audituserunkid"), Key .attribute2_name = row.Field(Of String)("calibuser")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
                If iCalibrator IsNot Nothing AndAlso iCalibrator.Keys.Count > 0 Then

                    If iCalibrator IsNot Nothing AndAlso iCalibrator.Keys.Count > 0 Then
                        For Each iKey As Integer In iCalibrator.Keys
                            Dim dt() As DataRow
                            dt = dsList.Tables(0).Select("audituserunkid = " & iKey)
                            If dt.Length > 0 Then
                                dt.AsEnumerable().ToList().ForEach(Function(x) UpdateParentGrp(x, iKey))
                            End If
                            Dim dr As DataRow = dsList.Tables(0).NewRow()
                            dr("Employee") = Language.getMessage(mstrModuleName, 211, "Calibrator :") & " " & iCalibrator(iKey) & _
                            IIf(dt(0)("allocation").ToString.Trim.Length > 0, " - (" & dt(0)("allocation").ToString & ")", "").ToString()
                            dr("DATE") = ""
                            dr("povisionscore") = ""
                            dr("calibratescore") = ""
                            dr("isgrp") = False
                            dr("employeeunkid") = dt(0)("employeeunkid")
                            dr("periodunkid") = dt(0)("periodunkid")
                            dr("statusunkid") = dt(0)("statusunkid")
                            dr("grpid") = dt(0)("grpid")
                            dr("submitdate") = dt(0)("submitdate")
                            dr("s_date") = dt(0)("s_date")
                            dr("c_remark") = ""
                            dr("o_remark") = ""
                            dr("pRating") = ""
                            dr("cRating") = ""
                            dr("allocation") = dt(0)("allocation")
                            dr("audituserunkid") = iKey
                            dr("calibuser") = iCalibrator(iKey)
                            dr("calibratnounkid") = dt(0)("calibratnounkid")
                            dr("ispgrp") = True
                            dr("pgrpid") = iKey
                            dsList.Tables(0).Rows.Add(dr)
                        Next
                    End If
                End If
                Dim oData As DataTable
                oData = New DataView(dsList.Tables(0), "", "pgrpid,ispgrp DESC, grpid,isgrp DESC,employeeunkid", DataViewRowState.CurrentRows).ToTable().Copy()
                dsList.Tables.RemoveAt(0)
                dsList.Tables.Add(oData.Copy)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function

    Public Function GetListForSubmit(ByVal xPeriodId As Integer, _
                                     ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal strTableName As String, _
                                     Optional ByVal strFilterString As String = "", _
                                     Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                     Optional ByVal blnIsReviewerNeeded As Boolean = True) As DataTable 'S.SANDEEP |25-OCT-2019| -- START {blnIsReviewerNeeded} -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        'S.SANDEEP |21-SEP-2019| -- START
        'ISSUE/ENHANCEMENT : {HPO Chart Issue}
        Dim iData As DataTable = Nothing
        'S.SANDEEP |21-SEP-2019| -- END
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim objUser As New clsUserAddEdit
            objUser._DataOpr = objDataOperation
            objUser._Userunkid = xUserUnkid
            Dim intEmpId As Integer = 0
            intEmpId = objUser._EmployeeUnkid
            objUser = Nothing

            objDataOperation.ExecNonQuery("SET ARITHABORT ON")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            StrQ = "SELECT DISTINCT " & _
                   "     CAST(0 AS BIT) AS iCheck " & _
                   "    ,hremployee_master.employeecode AS Code " & _
                   "    ,hremployee_master.firstname + ' ' + hremployee_master.surname AS EmployeeName " & _
                   "    ,EJOB.job_name AS JobTitle " & _
                   "    ,CAST(CSM.finaloverallscore AS DECIMAL(36,2)) AS Provision_Score " & _
                   "    ,CAST(ISNULL(CSM.calibrated_score,CSM.finaloverallscore) AS DECIMAL(36,2)) AS calibrated_score " & _
                   "    ,CSM.employeeunkid " & _
                   "    ,CSM.periodunkid " & _
                   "    ,CAST(0 AS BIT) AS isChanged " & _
                   "    ,ISNULL(hrassess_calibration_number.calibration_no,'') AS calibration_no " & _
                   "    ,CSM.calibration_remark " & _
                   "    ,ISNULL(CSM.calibratnounkid,0) AS calibratnounkid " & _
                   "    ,'' AS provRating " & _
                   "    ,'' AS calibRating " & _
                   "FROM hrassess_compute_score_master AS CSM " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         CSM.computescoremasterunkid " & _
                   "        ,DENSE_RANK()OVER(PARTITION BY CSM.employeeunkid,CSM.periodunkid ORDER BY CSM.finaloverallscore DESC) AS rno " & _
                   "    FROM hrassess_compute_score_master AS CSM " & _
                   "    WHERE CSM.issubmitted = 0 AND CSM.periodunkid = @periodunkid " & _
                   "    AND CSM.isvoid = 0 /*AND CSM.calibrated_score <= 0*/ " & _
                   ") AS CL ON CL.computescoremasterunkid = CSM.computescoremasterunkid AND CL.rno = 1 " & _
                   "   LEFT JOIN hrassess_calibration_number ON hrassess_calibration_number.calibratnounkid = CSM.calibratnounkid " & _
                   "   JOIN " & _
                   "   ( " & _
                   "       SELECT " & _
                   "            periodunkid " & _
                   "           ,CASE WHEN selfemployeeunkid <=0 THEN assessedemployeeunkid ELSE selfemployeeunkid END AS EmpId " & _
                   "       FROM hrevaluation_analysis_master " & _
                   "       WHERE isvoid = 0 AND periodunkid = @periodunkid AND iscommitted = 1 " & _
                   "   ) AS EV ON EV.periodunkid = CSM.periodunkid AND CSM.employeeunkid = EV.EmpId " & _
                   "   JOIN hremployee_master ON CSM.employeeunkid = hremployee_master.employeeunkid " & _
                   "   LEFT JOIN " & _
                   "   ( " & _
                   "       SELECT " & _
                   "            CT.employeeunkid " & _
                   "           ,CT.jobunkid " & _
                   "           ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS xno " & _
                   "       FROM hremployee_categorization_tran AS CT " & _
                   "       WHERE CT.isvoid = 0 AND CONVERT(NVARCHAR(8),CT.effectivedate,112) <= @Date " & _
                   "   ) AS JB ON JB.employeeunkid = hremployee_master.employeeunkid AND JB.xno = 1 " & _
                   "   JOIN hrjob_master AS EJOB ON JB.jobunkid = EJOB.jobunkid "

            'S.SANDEEP |21-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {HPO Chart Issue}
            '----------- ADDED
            '"JOIN " & _
            '"( " & _
            '"    SELECT " & _
            '"         CSM.computescoremasterunkid " & _
            '"        ,DENSE_RANK()OVER(PARTITION BY CSM.employeeunkid,CSM.periodunkid ORDER BY CSM.finaloverallscore DESC) AS rno " & _
            '"    FROM hrassess_compute_score_master AS CSM " & _
            '"    WHERE CSM.issubmitted = 0 AND CSM.periodunkid = @periodunkid " & _
            '"    AND CSM.isvoid = 0 AND CSM.calibrated_score <= 0 " & _
            '") AS CL ON CL.computescoremasterunkid = CSM.computescoremasterunkid AND CL.rno = 1 " & _
            'S.SANDEEP |21-SEP-2019| -- END

            'S.SANDEEP |26-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {Calibration Issue}
            ' -------- REMOVED 
            '/*AND CSM.calibrated_score <= 0*/
            'S.SANDEEP |26-SEP-2019| -- END

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            Dim intMatchValue As Integer = 0
            If blnIsReviewerNeeded Then
                intMatchValue = 3
            Else
                intMatchValue = 2
            End If
            StrQ &= " JOIN " & _
                    " ( " & _
                    "     SELECT " & _
                    "         employeeunkid " & _
                    "        ,COUNT(1) AS iAll " & _
                    "     FROM hrassess_compute_score_master " & _
                    "     WHERE isvoid = 0 AND periodunkid = @periodunkid AND issubmitted = 0 " & _
                    "     GROUP BY employeeunkid HAVING COUNT(1) <= " & intMatchValue & " " & _
                    " ) AS D ON D.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |25-OCT-2019| -- END

            StrQ &= "WHERE CSM.issubmitted = 0 AND CSM.periodunkid = @periodunkid " & _
                    "AND CSM.isvoid = 0 /*AND CSM.finaloverallscore > 0*/ "

            If intEmpId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid NOT IN (" & intEmpId.ToString() & ") "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If strFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & strFilterString
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListForSumit; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function

    'S.SANDEEP |26-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : {Calibration Issue}
    Public Sub GetUACFilters(ByVal strDatabaseName As String, ByVal strUserAccessMode As String, ByRef strFilter As String, ByRef strJoin As String, ByRef strSelect As String, ByRef strAccessJoin As String, ByRef strOuterJoin As String, ByRef strFinalString As String, ByVal intUserId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = ""
        Try
            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as branchid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0 " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
                        strJoin &= "AND Fn.branchid = [@emp].branchid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

                    Case enAllocation.DEPARTMENT_GROUP
                        strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0 " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.deptgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
                        strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

                    Case enAllocation.DEPARTMENT
                        strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0 " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.deptid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
                        strJoin &= "AND Fn.deptid = [@emp].deptid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

                    Case enAllocation.SECTION_GROUP
                        strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0 " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.secgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
                        strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

                    Case enAllocation.SECTION
                        strSelect &= ",ISNULL(SC.secid,0) AS secid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0 " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.secid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SC.secid,0) > 0 "
                        strJoin &= "AND Fn.secid = [@emp].secid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

                    Case enAllocation.UNIT_GROUP
                        strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0 " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.unitgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
                        strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

                    Case enAllocation.UNIT
                        strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0 " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.unitid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
                        strJoin &= "AND Fn.unitid = [@emp].unitid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

                    Case enAllocation.TEAM
                        strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as teamid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0 " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.teamid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
                        strJoin &= "AND Fn.teamid = [@emp].teamid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

                    Case enAllocation.JOB_GROUP
                        strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0  " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.jgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
                        strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

                    Case enAllocation.JOBS
                        strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jobid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0  " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.jobid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
                        strJoin &= "AND Fn.jobid = [@emp].jobid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

                    Case enAllocation.CLASS_GROUP
                        strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0  " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.clsgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
                        strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

                    Case enAllocation.CLASSES
                        strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.iscalibrator = 0  " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.clsid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                                         "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
                        strJoin &= "AND Fn.clsid = [@emp].clsid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = hrclasses_master.classesunkid "
                End Select

                objDataOperation.ClearParameters()
                StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
                Dim strvalue = ""
                objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                strvalue = CStr(objDataOperation.GetParameterValue("@Value"))

                If strvalue.Trim.Length > 0 Then
                    Select Case CInt(strvalues(index))
                        Case enAllocation.BRANCH
                            strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT_GROUP
                            strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT
                            strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

                        Case enAllocation.SECTION_GROUP
                            strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

                        Case enAllocation.SECTION
                            strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

                        Case enAllocation.UNIT_GROUP
                            strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

                        Case enAllocation.UNIT
                            strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

                        Case enAllocation.TEAM
                            strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

                        Case enAllocation.JOB_GROUP
                            strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

                        Case enAllocation.JOBS
                            strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

                        Case enAllocation.CLASS_GROUP
                            strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

                        Case enAllocation.CLASSES
                            strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

                    End Select
                End If
            Next
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Public Sub GetUACFilters(ByVal strDatabaseName As String, ByVal strUserAccessMode As String, ByRef strFilter As String, ByRef strJoin As String, ByRef strSelect As String, ByRef strAccessJoin As String, ByRef strOuterJoin As String, ByRef strFinalString As String, ByVal intUserId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing)
    '    Dim objDataOperation As New clsDataOperation
    '    If xDataOpr IsNot Nothing Then
    '        objDataOperation = xDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Dim StrQ As String = ""
    '    Dim StrFinalAccess As String = String.Empty
    '    Try
    '        If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()

    '        StrFinalAccess = "JOIN " & _
    '                         "( "
    '        Dim StrFirstJoin As String = String.Empty
    '        Dim StrPAlias As String = String.Empty

    '        Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
    '        For index As Integer = 0 To strvalues.Length - 1
    '            Select Case CInt(strvalues(index))
    '                Case enAllocation.BRANCH
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    BR.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT DISTINCT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as branchid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
    '                                       ") AS BR "
    '                        StrPAlias = "BR"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        " ( " & _
    '                                        "     SELECT DISTINCT " & _
    '                                        "        UPM.userunkid " & _
    '                                        "       ,allocationunkid as branchid " & _
    '                                        "     FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "       JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                        "     WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
    '                                        " ) AS BR ON BR.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.DEPARTMENT_GROUP
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    DG.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as deptgrpid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
    '                                       ") AS DG "
    '                        StrPAlias = "DG"
    '                    Else
    '                        StrFirstJoin &= "    JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as deptgrpid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.deptgrpid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
    '                                        "    ) AS DG ON DG.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.DEPARTMENT
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    DP.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as deptid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
    '                                       ") AS DP "
    '                        StrPAlias = "DP"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as deptid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.deptid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
    '                                        "    ) AS DP ON DP.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.SECTION_GROUP
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    SG.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as secgrpid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
    '                                       ") AS SG "
    '                        StrPAlias = "SG"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as secgrpid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.secgrpid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
    '                                        "    ) AS SG ON SG.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.SECTION
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    SC.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as secid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
    '                                       ") AS SC "
    '                        StrPAlias = "SC"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as secid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.secid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
    '                                        "    ) AS SC ON SC.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.UNIT_GROUP
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    UG.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as unitgrpid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
    '                                       ") AS UG "
    '                        StrPAlias = "UG"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as unitgrpid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.unitgrpid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
    '                                        "    ) AS UG ON UG.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.UNIT
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    UT.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as unitid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
    '                                       ") AS UT "
    '                        StrPAlias = "UT"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as unitid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.unitid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
    '                                        "    ) AS UT ON UT.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.TEAM
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    TM.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as teamid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
    '                                       ") AS TM "
    '                        StrPAlias = "TM"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as teamid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.teamid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
    '                                        "    ) AS TM ON TM.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.JOB_GROUP
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    JG.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as jgrpid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
    '                                       ") AS JG "
    '                        StrPAlias = "JG"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as jgrpid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.jgrpid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
    '                                        "    ) AS JG ON JG.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.JOBS
    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    JB.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as jobid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
    '                                       ") AS JB "
    '                        StrPAlias = "JB"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as jobid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.jobid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
    '                                        "    ) AS JB ON JB.userunkid = " & StrPAlias & ".userunkid "
    '                    End If


    '                Case enAllocation.CLASS_GROUP

    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    CG.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as clsgrpid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
    '                                       ") AS CG "
    '                        StrPAlias = "CG"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as clsgrpid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.clsgrpid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
    '                                        "    ) AS CG ON CG.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '                Case enAllocation.CLASSES

    '                    If StrFirstJoin.Trim.Length <= 0 Then
    '                        StrFirstJoin = "SELECT " & _
    '                                       "    CL.userunkid " & _
    '                                       "FROM " & _
    '                                       "( " & _
    '                                       "  SELECT " & _
    '                                       "     UPM.userunkid " & _
    '                                       "    ,allocationunkid as clsid " & _
    '                                       "  FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                       "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                       "    JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                       "  WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
    '                                       ") AS CL "
    '                        StrPAlias = "CL"
    '                    Else
    '                        StrFirstJoin &= " JOIN " & _
    '                                        "    ( " & _
    '                                        "        SELECT " & _
    '                                        "             UPM.userunkid " & _
    '                                        "            ,allocationunkid as clsid " & _
    '                                        "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                        "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                        "            JOIN #EMP ON #EMP.clsid = UPT.allocationunkid " & _
    '                                        "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
    '                                        "    ) AS CL ON CL.userunkid = " & StrPAlias & ".userunkid "
    '                    End If

    '            End Select

    '            objDataOperation.ClearParameters()
    '            StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
    '            Dim strvalue = ""
    '            objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

    '            objDataOperation.ExecNonQuery(StrQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            End If

    '            strvalue = CStr(objDataOperation.GetParameterValue("@Value"))

    '            If strvalue.Trim.Length > 0 Then
    '                Select Case CInt(strvalues(index))
    '                    Case enAllocation.BRANCH
    '                        strFinalString &= " AND BR.branchid IN (" & strvalue & ") "

    '                    Case enAllocation.DEPARTMENT_GROUP
    '                        strFinalString &= " AND DG.deptgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.DEPARTMENT
    '                        strFinalString &= " AND DP.deptid IN (" & strvalue & ") "

    '                    Case enAllocation.SECTION_GROUP
    '                        strFinalString &= " AND SG.secgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.SECTION
    '                        strFinalString &= " AND SC.secid IN (" & strvalue & ") "

    '                    Case enAllocation.UNIT_GROUP
    '                        strFinalString &= " AND UG.unitgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.UNIT
    '                        strFinalString &= " AND UT.unitid IN (" & strvalue & ") "

    '                    Case enAllocation.TEAM
    '                        strFinalString &= " AND TM.teamid IN (" & strvalue & ") "

    '                    Case enAllocation.JOB_GROUP
    '                        strFinalString &= " AND JG.jgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.JOBS
    '                        strFinalString &= " AND JB.jobid IN (" & strvalue & ") "

    '                    Case enAllocation.CLASS_GROUP
    '                        strFinalString &= " AND CG.clsgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.CLASSES
    '                        strFinalString &= " AND CL.clsid IN (" & strvalue & ") "

    '                End Select
    '            End If
    '        Next
    '        StrFinalAccess &= StrFirstJoin
    '        StrFinalAccess &= " WHERE 1 = 1 " & _
    '        strFinalString & " " & _
    '                          ") AS OP	ON cfuser_master.userunkid = OP.userunkid "

    '        If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
    '        If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)

    '        strAccessJoin = StrFinalAccess

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'S.SANDEEP |26-SEP-2019| -- END

    Public Function IsValidOperation(ByVal intPeriodUnkid As Integer _
                                   , ByVal strDatabaseName As String _
                                   , ByVal intCompanyId As Integer _
                                   , ByVal intYearId As Integer _
                                   , ByVal strUserAccessMode As String _
                                   , ByVal intPrivilegeId As Integer _
                                   , ByVal xEmployeeAsOnDate As String _
                                   , ByVal intUserId As Integer _
                                   , ByVal intCurrentPriorityId As Integer _
                                   , ByVal intCalibrationId As Integer _
                                   , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                   , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
                                   , Optional ByVal mstrFilterString As String = "" _
                                   , Optional ByVal strEmployeeIds As String = "") As Boolean
        Try
            Dim StrQ As String = String.Empty
            Dim dList As DataTable = Nothing
            Dim oData As DataTable = Nothing
            Dim blnFlag As Boolean = True
            'S.SANDEEP |12-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : DYNAMIC ACCESS CHECKING
            'dList = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, xDataOpr, intCurrentPriorityId, mblnIsSubmitForApproaval, mstrFilterString, strEmployeeIds)

            'S.SANDEEP |01-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
            'dList = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, xDataOpr, intCurrentPriorityId, mblnIsSubmitForApproaval, mstrFilterString, strEmployeeIds, intCalibrationId)
            dList = GetNextEmployeeApproversNew(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, False, xDataOpr, intCurrentPriorityId, mblnIsSubmitForApproaval, mstrFilterString, strEmployeeIds, intCalibrationId)
            Dim strMyEmpIds As String = ""
            strMyEmpIds = String.Join(",", dList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = intUserId).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToArray())
            If strMyEmpIds.Trim.Length <= 0 Then strMyEmpIds = "0"
            dList = New DataView(dList, "employeeunkid IN (" & strMyEmpIds & ")", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP |01-MAY-2020| -- END

            'S.SANDEEP |12-DEC-2019| -- END
            If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                Dim drs() As DataRow = Nothing
                drs = dList.Select("priority = '" & intCurrentPriorityId & "'", "employeeunkid, priority")
                Dim iApprovedRows As Integer = 0
                iApprovedRows = drs.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(enCalibrationStatus.Approved)).Count()
                If drs.Length = iApprovedRows Then
                    blnFlag = False
                End If
                iApprovedRows = drs.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(enCalibrationStatus.Rejected)).Count()
                If drs.Length = iApprovedRows Then
                    blnFlag = False
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidOperation; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsValidApprover(ByVal intPeriodUnkid As Integer _
                                   , ByVal strDatabaseName As String _
                                   , ByVal intCompanyId As Integer _
                                   , ByVal intYearId As Integer _
                                   , ByVal strUserAccessMode As String _
                                   , ByVal intPrivilegeId As Integer _
                                   , ByVal xEmployeeAsOnDate As String _
                                   , ByVal intUserId As Integer _
                                   , ByVal intCurrentPriorityId As Integer _
                                   , ByVal intCalibrationId As Integer _
                                   , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                   , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
                                   , Optional ByVal mstrFilterString As String = "" _
                                   , Optional ByVal strEmployeeIds As String = "") As Boolean
        Dim StrQ As String = String.Empty
        Dim dList As DataTable = Nothing
        Dim oData As DataTable = Nothing
        Dim blnFlag As Boolean = False
        Dim blnMinPriorityFlag As Boolean = False
        Try
            'S.SANDEEP |12-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : DYNAMIC ACCESS CHECKING
            'dList = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, xDataOpr, intCurrentPriorityId, mblnIsSubmitForApproaval, mstrFilterString, strEmployeeIds)

            'S.SANDEEP |01-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
            'dList = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, xDataOpr, intCurrentPriorityId, mblnIsSubmitForApproaval, mstrFilterString, strEmployeeIds, intCalibrationId)
            dList = GetNextEmployeeApproversNew(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, False, xDataOpr, intCurrentPriorityId, mblnIsSubmitForApproaval, mstrFilterString, strEmployeeIds, intCalibrationId)
            Dim strMyEmpIds As String = ""
            strMyEmpIds = String.Join(",", dList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = intUserId).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToArray())
            If strMyEmpIds.Trim.Length <= 0 Then strMyEmpIds = "0"
            dList = New DataView(dList, "employeeunkid IN (" & strMyEmpIds & ")", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP |01-MAY-2020| -- END

            'S.SANDEEP |12-DEC-2019| -- END
            Dim iPriority As List(Of Integer) = dList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
            iPriority.Sort()
            Dim intSecondLastPriority As Integer = -1
            If iPriority.Min() = intCurrentPriorityId Then
                intSecondLastPriority = intCurrentPriorityId
                blnMinPriorityFlag = True
            Else
                Try
                    intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intCurrentPriorityId) - 1)
                Catch ex As Exception
                    intSecondLastPriority = intCurrentPriorityId
                End Try
            End If
            If blnMinPriorityFlag = False Then
                Dim drs() As DataRow = Nothing
                drs = dList.Select("priority = '" & intSecondLastPriority & "' AND grpid = '" & intCalibrationId & "'", "employeeunkid, priority")
                If drs.Length > 0 Then
                    Dim row = drs.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(enCalibrationStatus.Approved))
                    If row IsNot Nothing AndAlso row.Count > 0 Then
                        blnFlag = True
                    End If
                    'S.SANDEEP |12-DEC-2019| -- START
                    'ISSUE/ENHANCEMENT : DYNAMIC ACCESS CHECKING
                Else
                    If iPriority.Min() = intSecondLastPriority Then
                        intSecondLastPriority = intSecondLastPriority
                    Else
                        Try
                            intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intSecondLastPriority) - 1)
                        Catch ex As Exception
                            intSecondLastPriority = intCurrentPriorityId
                        End Try
                    End If
                    drs = dList.Select("priority = '" & intSecondLastPriority & "' AND grpid = '" & intCalibrationId & "'", "employeeunkid, priority")
                    Dim row = drs.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(enCalibrationStatus.Approved))
                    If row IsNot Nothing AndAlso row.Count > 0 Then
                        blnFlag = True
                    End If
                    'S.SANDEEP |12-DEC-2019| -- END
                End If
            Else
                blnFlag = True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovalData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function

    Public Function GetEmployeeApprovalData(ByVal intPeriodUnkid As Integer _
                                            , ByVal strDatabaseName As String _
                                            , ByVal intCompanyId As Integer _
                                            , ByVal intYearId As Integer _
                                            , ByVal strUserAccessMode As String _
                                            , ByVal intPrivilegeId As Integer _
                                            , ByVal xEmployeeAsOnDate As String _
                                            , ByVal intUserId As Integer _
                                            , ByVal blnOnlyMyApproval As Boolean _
                                            , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                            , Optional ByVal intCurrentPriorityId As Integer = 0 _
                                            , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
                                            , Optional ByVal mstrFilterString As String = "" _
                                            , Optional ByVal strEmployeeIds As String = "" _
                                            , Optional ByVal blnMyReport As Boolean = False _
                                            , Optional ByVal blnGroupByCalibrator As Boolean = False _
                                            , Optional ByVal strAdvanceFilter As String = "") As DataTable
        'S.SANDEEP |25-OCT-2019| -- START {blnGroupByCalibrator,strAdvanceFilter}-- END

        Dim StrQ As String = String.Empty
        Dim dList As DataTable = Nothing
        Dim oData As DataTable = Nothing
        Try
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'dList = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, blnOnlyMyApproval, xDataOpr, intCurrentPriorityId, mblnIsSubmitForApproaval, mstrFilterString, strEmployeeIds)
            dList = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, xDataOpr, intCurrentPriorityId, mblnIsSubmitForApproaval, mstrFilterString, strEmployeeIds, , , strAdvanceFilter)

            If dList IsNot Nothing Then
                Dim iCol As DataColumn
                iCol = New DataColumn
                With iCol
                    .DataType = GetType(System.Boolean)
                    .ColumnName = "ispgrp"
                    .DefaultValue = False
                End With
                dList.Columns.Add(iCol)

                iCol = New DataColumn
                With iCol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "pgrpid"
                    .DefaultValue = 0
                End With
                dList.Columns.Add(iCol)
            End If
            'S.SANDEEP |25-OCT-2019| -- END


            'oData = New DataView(dList, "userunkid = " & intUserId & " AND mappingunkid <=0 AND (rno = 1 or isgrp = 1) AND employeeunkid <> uempid ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()

            If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                If blnOnlyMyApproval Then
                    Dim strcalibUnkids As String = String.Empty
                    Dim iCalibId As List(Of Int64) = dList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isprocessed") = False).Select(Function(x) x.Field(Of Int64)("grpid")).Distinct().ToList()

                    If iCalibId IsNot Nothing AndAlso iCalibId.Count > 0 Then
                        iCalibId.Sort()
                        For Each ivalue In iCalibId
                            Dim intLowerPriority As Integer = -1
                            If IsDBNull(dList.Compute("MAX(priority)", "grpid = '" & ivalue & "' AND priority < " & intCurrentPriorityId)) = False Then
                                intLowerPriority = CInt(dList.Compute("MAX(priority)", "grpid = '" & ivalue & "' AND priority < " & intCurrentPriorityId))
                                If intLowerPriority <> -1 Then
                                    Dim itmp() As DataRow = dList.Select("iStatusId = " & CInt(enCalibrationStatus.Approved) & " AND priority = '" & intLowerPriority & "' AND grpid = '" & ivalue & "'")
                                    If itmp IsNot Nothing AndAlso itmp.Length > 0 Then
                                        strcalibUnkids &= "," & ivalue.ToString()
                    End If
                            End If
                                'S.SANDEEP |23-DEC-2019| -- START
                            Else
                                intLowerPriority = intCurrentPriorityId
                                'S.SANDEEP |10-JAN-2020| -- START
                                'ISSUE/ENHANCEMENT : WRONG FILTER APPLIED
                                'Dim itmp() As DataRow = dList.Select("iStatusId = " & CInt(enCalibrationStatus.Approved) & " AND priority = '" & intLowerPriority & "' AND grpid = '" & ivalue & "'")
                                Dim itmp() As DataRow = dList.Select("priority = '" & intLowerPriority & "' AND grpid = '" & ivalue & "'")
                                'S.SANDEEP |10-JAN-2020| -- END
                                If itmp IsNot Nothing AndAlso itmp.Length > 0 Then
                                    strcalibUnkids &= "," & ivalue.ToString()
                                End If
                                'S.SANDEEP |23-DEC-2019| -- END
                            End If
                        Next
                        If strcalibUnkids.Trim.Length > 0 Then strcalibUnkids = Mid(strcalibUnkids, 2)
                        'S.SANDEEP |23-DEC-2019| -- START
                        If strcalibUnkids.Trim.Length <= 0 Then strcalibUnkids = "-1"
                        'S.SANDEEP |23-DEC-2019| -- END
                        oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1 AND grpid IN (" & strcalibUnkids & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                    End If


                    'Dim blnFlag As Boolean = False
                    'Dim iPriority As List(Of Integer) = dList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                    'iPriority.Sort()
                    'Dim intSecondLastPriority As Integer = -1
                    'If iPriority.Min() = intCurrentPriorityId Then
                    '    intSecondLastPriority = intCurrentPriorityId
                    '    blnFlag = True
                    'Else
                    '    Try
                    '        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intCurrentPriorityId) - 1)
                    '    Catch ex As Exception
                    '        intSecondLastPriority = intCurrentPriorityId
                    '        blnFlag = True
                    '    End Try
                    'End If
                    'dList = New DataView(dList, "priority >= '" & intSecondLastPriority & "' AND priority <= '" & intCurrentPriorityId & "' AND isprocessed = 0 ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                    'Dim drs() As DataRow = Nothing
                    'drs = dList.Select("priority = '" & intSecondLastPriority & "'", "employeeunkid, priority")
                    'If drs.Length > 0 Then
                    '    Dim row = drs.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(enCalibrationStatus.Approved))
                    '    Dim strIds As String
                    '    If row.Count() > 0 Then
                    '        strIds = String.Join(",", drs.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList().Except(row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList()).Distinct().ToArray())
                    '        If blnFlag Then
                    '            strIds = ""
                    '        End If
                    '        If strIds.Trim.Length > 0 Then
                    '            oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1 AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                    '        Else
                    '            oData = New DataView(dList, "userunkid = " & intUserId & " ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                    '        End If
                    '    Else
                    '        strIds = String.Join(",", drs.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
                    '        If blnFlag = False Then
                    '            strIds = " AND employeeunkid NOT IN(" & strIds & ") "
                    '        Else
                    '            strIds = ""
                    '        End If
                    '        oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1 " & strIds, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                    '    End If
                    'Else
                    '    oData = New DataView(dList, "userunkid = " & intUserId, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable() '& " AND iStatusId = 1 "
                    'End If

                Else
                    If blnMyReport Then
                        Dim drs() As DataRow = Nothing
                        If intCurrentPriorityId <= 0 Then
                            intCurrentPriorityId = dList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = intUserId).Select(Function(x) x.Field(Of Integer)("priority")).FirstOrDefault()
                        End If
                        drs = dList.Select("priority <= '" & intCurrentPriorityId & "'", "employeeunkid, priority")
                        If drs.Length > 0 Then
                            oData = drs.CopyToDataTable()
                        Else
                            oData = dList.Clone()
                        End If
                    Else
                    oData = dList.Copy()
                End If
                End If
            Else
                oData = dList.Copy()
            End If

            'Dim strValue As String = String.Join(",", oData.AsEnumerable().Where(Function(x) CBool(x.Field(Of Boolean)("isgrp")) = False And x.Field(Of Integer)("iStatusId") = 2).GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")) _
            '                          .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("employeeunkid")) _
            '                          .Count()}).Where(Function(y) y.barCount = 1).Select(Function(a) a.barid.ToString).ToArray())

            'If strValue.Trim.Length > 0 Then
            '    oData = New DataView(oData, "employeeunkid NOT IN (" & strValue & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
            'End If

            'If strValue.Trim.Length <= 0 Then
            '    strValue = String.Join(",", oData.AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")) _
            '                       .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("employeeunkid")) _
            '                       .Count()}).Where(Function(y) y.barCount = 1).Select(Function(a) a.barid.ToString).ToArray())

            '    If strValue.Trim.Length > 0 Then
            '        oData = New DataView(oData, "employeeunkid NOT IN (" & strValue & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
            '    End If
            'End If

            '''''''''''''' ADDING GROUP & SORTING
            If oData.Rows.Count > 0 Then
                Dim iGroup As Dictionary(Of Int64, String)
                iGroup = oData.AsEnumerable().Select(Function(row) New With {Key .attribute1_name = row.Field(Of Int64)("grpid"), Key .attribute2_name = row.Field(Of String)("calibration_no")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)

                Dim iPriority As List(Of Integer) = dList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                iPriority.Sort()
                Dim intSecondLastPriority As Integer = -1
                If iPriority.Min() = intCurrentPriorityId Then
                    intSecondLastPriority = intCurrentPriorityId
                Else
                    Try
                        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intCurrentPriorityId) - 1)
                    Catch ex As Exception
                        intSecondLastPriority = intCurrentPriorityId
                    End Try
                End If

                If iGroup IsNot Nothing AndAlso iGroup.Keys.Count > 0 Then
                    For Each iKey As Integer In iGroup.Keys
                        Dim strDate As String = String.Empty
                        Dim intFinalStatusId As Integer = 0
                        Dim dt() As DataRow = oData.Select("grpid = '" & iKey & "' AND priority <= " & intSecondLastPriority)
                        If dt.Length > 0 Then
                            If dt(0)("submitdate").ToString.Trim.Length > 0 Then strDate = CStr(dt(0)("submitdate"))
                        End If
                        'dt = oData.Select("grpid = '" & iKey & "'AND priority <= " & intSecondLastPriority)
                        If dt.Length > 0 Then
                            If dt(0)("iStatusId").ToString.Trim.Length > 0 Then intFinalStatusId = CInt(dt(0)("iStatusId"))
                        End If
                        If intFinalStatusId <= 0 Then intFinalStatusId = 1
                        Dim dr As DataRow = oData.NewRow()
                        dr("iCheck") = False
                        dr("calibration_no") = iGroup(iKey)
                        dr("Employee") = Language.getMessage(mstrModuleName, 1, "Calibration No :") & " " & iGroup(iKey)
                        dr("DATE") = ""
                        dr("povisionscore") = "0.00"
                        dr("calibratescore") = "0.00"
                        dr("transactiondate") = DBNull.Value
                        dr("mappingunkid") = 0
                        dr("iStatus") = ""
                        dr("iStatusId") = intFinalStatusId
                        dr("userunkid") = "0"
                        dr("username") = ""
                        dr("approver_email") = ""
                        dr("priority") = intCurrentPriorityId
                        dr("employeeunkid") = 0
                        dr("levelname") = ""
                        dr("oemployee") = ""
                        dr("employee_name") = ""
                        dr("periodunkid") = intPeriodUnkid
                        dr("statusunkid") = 0
                        dr("tranguid") = ""
                        dr("calibratnounkid") = iKey
                        dr("mappingunkid") = 0
                        dr("approvalremark") = ""
                        dr("isfinal") = False
                        dr("isprocessed") = 0
                        dr("isgrp") = 1
                        dr("grpid") = iKey
                        dr("rno") = 0
                        dr("calibration_remark") = ""
                        dr("submitdate") = strDate
                        dr("c_remark") = ""
                        dr("o_remark") = ""
                        If dt.Length > 0 Then
                            dr("allocation") = dt(0)("allocation").ToString
                            dr("calibuser") = dt(0)("calibuser").ToString
                            'S.SANDEEP |25-OCT-2019| -- START
                            'ISSUE/ENHANCEMENT : Calibration Issues
                            dr("audituserunkid") = CInt(dt(0)("audituserunkid"))
                            'S.SANDEEP |25-OCT-2019| -- END
                        Else
                            dt = oData.Select("grpid = '" & iKey & "'")
                            If dt.Length > 0 Then
                                dr("audituserunkid") = CInt(dt(0)("audituserunkid"))
                            End If
                        End If
                        oData.Rows.Add(dr)
                    Next
                End If
            End If

            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'oData = New DataView(oData, "", "grpid,isgrp DESC,employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            If blnGroupByCalibrator = False Then oData = New DataView(oData, "", "grpid,isgrp DESC,employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            If blnGroupByCalibrator = True Then
                Dim iCalibrator As Dictionary(Of Integer, String)
                iCalibrator = oData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isgrp") = False And x.Field(Of Integer)("audituserunkid").ToString().Trim.Length > 0).Select(Function(row) New With {Key .attribute1_name = row.Field(Of Integer)("audituserunkid"), Key .attribute2_name = row.Field(Of String)("calibuser")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
                If iCalibrator IsNot Nothing AndAlso iCalibrator.Keys.Count > 0 Then
                    If iCalibrator IsNot Nothing AndAlso iCalibrator.Keys.Count > 0 Then
                        For Each iKey As Integer In iCalibrator.Keys
                            Dim strDate As String = String.Empty
                            Dim intFinalStatusId As Integer = 0
                            Dim dt() As DataRow
                            dt = oData.Select("audituserunkid = " & iKey)
                            If dt.Length = 1 Then Continue For
                            If dt.Length > 0 Then
                                If dt(0)("iStatusId").ToString.Trim.Length > 0 Then intFinalStatusId = CInt(dt(0)("iStatusId"))
                                dt.AsEnumerable().ToList().ForEach(Function(x) UpdateParentGrp(x, iKey))
                            End If
                            If intFinalStatusId <= 0 Then intFinalStatusId = 1
                            Dim dr As DataRow = oData.NewRow()
                            dr("iCheck") = False
                            dr("calibration_no") = dt(0)("calibration_no")
                            dr("Employee") = Language.getMessage(mstrModuleName, 211, "Calibrator :") & " " & iCalibrator(iKey) & _
                            IIf(dt(0)("allocation").ToString.Trim.Length > 0, " - (" & dt(0)("allocation").ToString & ")", "").ToString()
                            dr("DATE") = ""
                            dr("povisionscore") = "0.00"
                            dr("calibratescore") = "0.00"
                            dr("transactiondate") = DBNull.Value
                            dr("mappingunkid") = 0
                            dr("iStatus") = ""
                            dr("iStatusId") = intFinalStatusId
                            dr("userunkid") = "0"
                            dr("username") = ""
                            dr("approver_email") = ""
                            dr("priority") = intCurrentPriorityId
                            dr("employeeunkid") = 0
                            dr("levelname") = ""
                            dr("oemployee") = ""
                            dr("employee_name") = ""
                            dr("periodunkid") = intPeriodUnkid
                            dr("statusunkid") = 0
                            dr("tranguid") = ""
                            dr("calibratnounkid") = CInt(dt(0)("calibratnounkid"))
                            dr("mappingunkid") = 0
                            dr("approvalremark") = ""
                            dr("isfinal") = False
                            dr("isprocessed") = 0
                            dr("isgrp") = 0
                            dr("grpid") = 0
                            dr("rno") = 0
                            dr("calibration_remark") = ""
                            dr("submitdate") = strDate
                            dr("c_remark") = ""
                            dr("o_remark") = ""
                            dr("ispgrp") = True
                            dr("pgrpid") = iKey
                            If dt.Length > 0 Then
                                dr("allocation") = dt(0)("allocation").ToString
                                dr("calibuser") = dt(0)("calibuser").ToString
                                dr("audituserunkid") = CInt(dt(0)("audituserunkid"))
                            Else
                                dt = oData.Select("grpid = '" & iKey & "'")
                                If dt.Length > 0 Then
                                    dr("audituserunkid") = CInt(dt(0)("audituserunkid"))
                                End If
                            End If
                            oData.Rows.Add(dr)
                        Next
                    End If
                End If
                oData = New DataView(oData, "", "pgrpid,ispgrp DESC, grpid,isgrp DESC,employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            End If
            'S.SANDEEP |25-OCT-2019| -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovalData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return oData
    End Function

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    'Public Function GetNextEmployeeApprovers(ByVal intPeriodUnkid As Integer _
    '                                        , ByVal strDatabaseName As String _
    '                                        , ByVal intCompanyId As Integer _
    '                                        , ByVal intYearId As Integer _
    '                                        , ByVal strUserAccessMode As String _
    '                                        , ByVal intPrivilegeId As Integer _
    '                                        , ByVal xEmployeeAsOnDate As String _
    '                                        , ByVal intUserId As Integer _
    '                                        , ByVal blnOnlyMyApproval As Boolean _
    '                                        , Optional ByVal xDataOpr As clsDataOperation = Nothing _
    '                                        , Optional ByVal intCurrentPriorityId As Integer = 0 _
    '                                        , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
    '                                        , Optional ByVal mstrFilterString As String = "" _
    '                                        , Optional ByVal strEmployeeIds As String = "" _
    '                                        , Optional ByVal intCalibrationUnkid As Integer = 0 _
    '                                        , Optional ByVal blnAddUACFilter As Boolean = False _
    '                                        ) As DataTable 'S.SANDEEP |05-SEP-2019| -- START {intCalibrationUnkid} -- END
    '    'S.SANDEEP |25-OCT-2019| -- START {blnAddUACFilter} -- END
    '    Dim StrQ As String = String.Empty
    '    Dim dtList As DataTable = Nothing
    '    Dim objDataOperation As New clsDataOperation
    '    If xDataOpr IsNot Nothing Then
    '        objDataOperation = xDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Try
    '        Dim strFilter As String = String.Empty
    '        Dim strJoin As String = String.Empty
    '        Dim strSelect As String = String.Empty
    '        Dim strAccessJoin As String = String.Empty
    '        Dim strOuterJoin As String = String.Empty
    '        Dim strFinalString As String = ""


    '        'S.SANDEEP |25-OCT-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        'Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, intUserId, objDataOperation)
    '        If blnAddUACFilter Then
    '            'Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, intUserId, objDataOperation)
    '        End If
    '        'S.SANDEEP |25-OCT-2019| -- END


    '        Dim objCalibApprover As New clscalibrate_approver_master
    '        Dim dsApprover As New DataSet
    '        'dsApprover = objCalibApprover.GetList("List", True, "", CInt(IIf(blnOnlyMyApproval, intUserId, 0)), objDataOperation, False)
    '        dsApprover = objCalibApprover.GetList("List", True, "", intUserId, objDataOperation, False)
    '        objCalibApprover = Nothing

    '        Dim dsFinal As New DataSet
    '        Dim strUACJoin, strUACFilter, xAdvanceJoinQry As String
    '        strUACJoin = "" : strUACFilter = "" : xAdvanceJoinQry = ""
    '        modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM")

    '        If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then
    '            For Each iRow As DataRow In dsApprover.Tables(0).Rows
    '                'Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(xEmployeeAsOnDate), strDatabaseName, "AEM")
    '                Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, CInt(iRow("mapuserunkid")), objDataOperation)
    '                objDataOperation.ClearParameters()

    '                StrQ = "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL " & _
    '                       "DROP TABLE #EMP "
    '                StrQ &= "DECLARE @emp AS TABLE (empid INT,deptid INT,jobid INT,clsgrpid INT,clsid INT,branchid INT,deptgrpid INT,secgrpid INT,secid INT,unitgrpid INT,unitid INT,teamid INT,jgrpid INT,periodid INT,ecodename nvarchar(max),ename nvarchar(max),calibratnounkid INT,Code nvarchar(255), JobTitle nvarchar(max)) " & _
    '                        "INSERT INTO @emp (empid, deptid, jobid, clsgrpid, clsid, branchid, deptgrpid, secgrpid, secid, unitgrpid, unitid, teamid, jgrpid, periodid, ecodename, ename,calibratnounkid,Code,JobTitle) " & _
    '                        "SELECT " & _
    '                        "     AEM.employeeunkid " & _
    '                        "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
    '                        "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
    '                        "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
    '                        "    ,ISNULL(T.classunkid,0) AS classunkid " & _
    '                        "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
    '                        "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
    '                        "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                        "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
    '                        "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
    '                        "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
    '                        "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
    '                        "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
    '                        "    ,TAT.periodunkid " & _
    '                        "    ,AEM.employeecode + ' - ' + AEM.firstname + ' ' + AEM.surname " & _
    '                        "    ,AEM.firstname + ' ' + AEM.surname " & _
    '                        "    ,TAT.calibratnounkid " & _
    '                        "    ,AEM.employeecode " & _
    '                        "    ,EJOB.job_name AS JobTitle " & _
    '                        "FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
    '                        "    JOIN hrassess_computescore_approval_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid "
    '                If strUACJoin.Trim.Length > 0 AndAlso intUserId > 0 Then
    '                    StrQ &= strUACJoin
    '                End If
    '                If xAdvanceJoinQry.Trim.Length > 0 Then
    '                    StrQ &= xAdvanceJoinQry
    '                End If
    '                StrQ &= "LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        stationunkid " & _
    '                        "       ,deptgroupunkid " & _
    '                        "       ,departmentunkid " & _
    '                        "       ,sectiongroupunkid " & _
    '                        "       ,sectionunkid " & _
    '                        "       ,unitgroupunkid " & _
    '                        "       ,unitunkid " & _
    '                        "       ,teamunkid " & _
    '                        "       ,classgroupunkid " & _
    '                        "       ,classunkid " & _
    '                        "       ,employeeunkid " & _
    '                        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                        "   FROM hremployee_transfer_tran " & _
    '                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "
    '                If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND employeeunkid IN (" & strEmployeeIds & ") "
    '                StrQ &= ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
    '                        "LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        jobgroupunkid " & _
    '                        "       ,jobunkid " & _
    '                        "       ,employeeunkid " & _
    '                        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                        "   FROM hremployee_categorization_tran " & _
    '                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '                        ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
    '                        "LEFT JOIN hrjob_master AS EJOB ON EJOB.jobunkid = J.jobunkid "
    '                StrQ &= "WHERE 1 = 1 AND TAT.isvoid = 0 AND TAT.statusunkid IN (0,1) AND  [TAT].periodunkid = @periodunkid "
    '                If blnOnlyMyApproval Then
    '                    StrQ &= " AND ISNULL([TAT].[isprocessed],0) = 0 "
    '                End If
    '                If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND AEM.employeeunkid IN (" & strEmployeeIds & ") "

    '                'S.SANDEEP |05-SEP-2019| -- START
    '                'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
    '                If intCalibrationUnkid > 0 Then
    '                    StrQ &= " AND [TAT].calibratnounkid = '" & intCalibrationUnkid & "' "
    '                End If
    '                'S.SANDEEP |05-SEP-2019| -- END

    '                StrQ &= " SELECT * INTO #EMP FROM @emp "
    '                'S.SANDEEP |12-JUL-2019| -- START
    '                'ISSUE/ENHANCEMENT : PA CHANGES
    '                'StrQ &= "SELECT DISTINCT " & _
    '                '        "    CAST(0 AS BIT) AS iCheck " & _
    '                '        "   ,iData.calibration_no " & _
    '                '        "   ,iData.Employee " & _
    '                '        "   ,iData.DATE " & _
    '                '        "   ,iData.povisionscore " & _
    '                '        "   ,ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) AS calibratescore " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[transactiondate], iData.Date) ELSE NULL END AS transactiondate " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[mappingunkid], iData.mappingunkid) ELSE 0 END AS mappingunkid " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatus], @Pending) ELSE '' END AS iStatus " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END AS iStatusId " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.userunkid ELSE 0 END AS userunkid " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.username ELSE '' END AS username " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.email ELSE '' END AS approver_email " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.priority ELSE 0 END AS priority " & _
    '                '        "   ,[@emp].empid AS employeeunkid " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.LvName, '') ELSE '' END AS levelname " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.uempid END AS uempid " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.ecompid END AS ecompid " & _
    '                '        "   ,[@emp].ecodename AS oemployee " & _
    '                '        "   ,[@emp].ename AS employee_name " & _
    '                '        "   ,iData.employeeunkid " & _
    '                '        "   ,iData.periodunkid " & _
    '                '        "   ,iData.statusunkid " & _
    '                '        "   ,iData.tranguid " & _
    '                '        "   ,iData.calibratnounkid " & _
    '                '        "   ,iData.approvalremark " & _
    '                '        "   ,iData.isfinal " & _
    '                '        "   ,iData.isprocessed " & _
    '                '        "   ,iData.isgrp " & _
    '                '        "   ,iData.grpid " & _
    '                '        "   ,CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.periodunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno " & _
    '                '        "   ,[@emp].Code " & _
    '                '        "   ,[@emp].JobTitle " & _
    '                '        "   ,CAST(1 AS BIT) AS isChanged " & _
    '                '        "   ,CASE WHEN ISNULL(iData.calibration_remark,'') = '' THEN ISNULL(iData.c_remark,'') ELSE ISNULL(iData.calibration_remark,'') END AS calibration_remark " & _
    '                '        "   ,iData.submitdate " & _
    '                '        "   ,iData.c_remark " & _
    '                '        "   ,iData.o_remark " & _
    '                '        "   ,iData.s_date " & _
    '                '        "   ,'' AS lstpRating " & _
    '                '        "   ,'' AS lstcRating " & _
    '                '        "FROM @emp " & _
    '                '        "   JOIN " & _
    '                '        "       ( " & _
    '                '        "           SELECT " & _
    '                '        "                 a.calibration_no " & _
    '                '        "                ,a.Employee " & _
    '                '        "                ,a.DATE " & _
    '                '        "                ,a.povisionscore " & _
    '                '        "                ,a.calibratescore " & _
    '                '        "                ,a.isgrp " & _
    '                '        "                ,a.grpid " & _
    '                '        "                ,a.employeeunkid " & _
    '                '        "                ,a.periodunkid " & _
    '                '        "                ,a.statusunkid " & _
    '                '        "                ,a.tranguid " & _
    '                '        "                ,a.calibratnounkid " & _
    '                '        "                ,a.mappingunkid " & _
    '                '        "                ,a.approvalremark " & _
    '                '        "                ,a.isfinal " & _
    '                '        "                ,a.isprocessed " & _
    '                '        "                ,a.calibration_remark " & _
    '                '        "                ,a.submitdate " & _
    '                '        "                ,a.c_remark " & _
    '                '        "                ,a.o_remark " & _
    '                '        "                ,a.s_date " & _
    '                '        "           FROM " & _
    '                '        "           ( " & _
    '                '        "               SELECT " & _
    '                '        "                    calibration_no " & _
    '                '        "                   ,'/r/n' + employeecode + ' - ' + firstname + ' ' + surname AS Employee " & _
    '                '        "                   ,CONVERT(NVARCHAR(8), transactiondate, 112) AS DATE " & _
    '                '        "                   ,ISNULL(CONVERT(NVARCHAR(8),pScore.submit_date,112),'') AS submitdate " & _
    '                '        "                   ,ISNULL(pScore.c_remark, ISNULL(rScore.c_remark,'')) AS c_remark " & _
    '                '        "                   ,ISNULL(pScore.o_remark, ISNULL(rScore.o_remark,'')) AS o_remark " & _
    '                '        "                   ,CAST(ISNULL(pScore.finaloverallscore,rScore.finaloverallscore) AS DECIMAL(36, 2)) AS povisionscore " & _
    '                '        "                   ,CAST(calibrated_score AS DECIMAL(36, 2)) AS calibratescore " & _
    '                '        "                   ,CAST(0 AS BIT) AS isgrp " & _
    '                '        "                   ,hrassess_calibration_number.calibratnounkid AS grpid " & _
    '                '        "                   ,hrassess_computescore_approval_tran.employeeunkid " & _
    '                '        "                   ,hrassess_computescore_approval_tran.periodunkid " & _
    '                '        "                   ,hrassess_computescore_approval_tran.statusunkid " & _
    '                '        "                   ,hrassess_computescore_approval_tran.tranguid " & _
    '                '        "                   ,hrassess_computescore_approval_tran.calibratnounkid " & _
    '                '        "                   ,hrassess_computescore_approval_tran.mappingunkid " & _
    '                '        "                   ,hrassess_computescore_approval_tran.approvalremark " & _
    '                '        "                   ,hrassess_computescore_approval_tran.isfinal " & _
    '                '        "                   ,hrassess_computescore_approval_tran.isprocessed " & _
    '                '        "                   ,hrassess_computescore_approval_tran.calibration_remark " & _
    '                '        "                   ,pScore.submit_date AS s_date " & _
    '                '        "               FROM hrassess_computescore_approval_tran " & _
    '                '        "               LEFT JOIN " & _
    '                '        "               ( " & _
    '                '        "                   SELECT DISTINCT " & _
    '                '        "                        hrassess_computescore_rejection_tran.employeeunkid " & _
    '                '        "                       ,hrassess_computescore_rejection_tran.periodunkid " & _
    '                '        "                       ,hrassess_computescore_rejection_tran.calibratnounkid " & _
    '                '        "                       ,finaloverallscore " & _
    '                '        "                       ,hrassess_computescore_rejection_tran.calibration_remark AS c_remark " & _
    '                '        "                       ,hrassess_computescore_rejection_tran.overall_remark AS o_remark " & _
    '                '        "                   FROM hrassess_computescore_rejection_tran " & _
    '                '        "                       JOIN hrassess_compute_score_master ON hrassess_compute_score_master.employeeunkid = hrassess_computescore_rejection_tran.employeeunkid " & _
    '                '        "                       AND hrassess_compute_score_master.periodunkid = hrassess_computescore_rejection_tran.periodunkid " & _
    '                '        "                   WHERE hrassess_computescore_rejection_tran.periodunkid = @periodunkid AND isvoid = 0 " & _
    '                '        "               ) AS rScore ON rScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
    '                '        "                   AND rScore.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
    '                '        "                   AND rScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
    '                '        "               LEFT JOIN " & _
    '                '        "               ( " & _
    '                '        "                   SELECT DISTINCT " & _
    '                '        "                        calibratnounkid " & _
    '                '        "                       ,employeeunkid " & _
    '                '        "                       ,periodunkid " & _
    '                '        "                       ,finaloverallscore " & _
    '                '        "                       ,submit_date " & _
    '                '        "                       ,calibration_remark AS c_remark " & _
    '                '        "                       ,overall_remark AS o_remark " & _
    '                '        "                   FROM hrassess_compute_score_master " & _
    '                '        "                   WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
    '                '        "               ) AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
    '                '        "               AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
    '                '        "               JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
    '                '        "               JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                '        "           WHERE statusunkid IN (0,1) AND isvoid = 0 aND hrassess_computescore_approval_tran.periodunkid = @periodunkid " & _
    '                '        "       ) AS A WHERE 1 = 1 AND A.statusunkid IN (0,1) " & _
    '                '        "   ) AS iData ON iData.employeeunkid = [@emp].empid " & _
    '                '        "    JOIN " & _
    '                '        "    ( " & _
    '                '        "        SELECT DISTINCT " & _
    '                '        "             cfuser_master.userunkid " & _
    '                '        "            ,cfuser_master.username " & _
    '                '        "            ,cfuser_master.email " & _
    '                '                      strSelect & " " & _
    '                '        "            ,LM.priority " & _
    '                '        "            ,LM.levelname AS LvName " & _
    '                '        "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
    '                '        "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
    '                '        "            ,EM.mapuserunkid " & _
    '                '        "            ,EM.mappingunkid AS oMappingId " & _
    '                '        "        FROM hrmsConfiguration..cfuser_master " & _
    '                '        "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '                '        "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '                '                     strAccessJoin & " " & _
    '                '        "        JOIN " & strDatabaseName & "..hrscore_calibration_approver_master EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
    '                '        "        JOIN " & strDatabaseName & "..hrscore_calibration_approverlevel_master AS LM ON EM.levelunkid = LM.levelunkid " & _
    '                '        "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "




    '                '"   ,CASE WHEN ISNULL(NV.approvalremark,ISNULL(bcalibration_remark,'')) = '' THEN ISNULL(iData.calibration_remark,'') ELSE ISNULL(NV.approvalremark,ISNULL(bcalibration_remark,'')) END AS calibration_remark " & _
    '                '        "   ,iData.submitdate " & _
    '                '        "   ,iData.c_remark " & _
    '                '        "   ,iData.o_remark " & _
    '                '        "   ,iData.s_date " & _
    '                '        "   ,'' AS lstpRating " & _
    '                '        "   ,ISNULL(B.lstcRating,'') AS lstcRating " & _
    '                '        "   ,ISNULL(acal.acrating,'') AS acrating " & _
    '                '        "   ,ISNULL(acal.acremark,'') AS acremark " & _
    '                '        "   ,ISNULL(B.lstcRating,'') AS lstapRating " & _
    '                '        "   ,ISNULL(bcalibration_remark,'') AS apcalibremark " & _

    '                StrQ &= "SELECT DISTINCT " & _
            '        "    CAST(0 AS BIT) AS iCheck " & _
            '        "   ,iData.calibration_no " & _
            '        "   ,iData.Employee " & _
            '        "   ,iData.DATE " & _
    '                                "   ,ISNULL(iData.povisionscore,0) AS povisionscore " & _
            '        "   ,ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) AS calibratescore " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[transactiondate], iData.Date) ELSE NULL END AS transactiondate " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[mappingunkid], iData.mappingunkid) ELSE 0 END AS mappingunkid " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatus], @Pending) ELSE '' END AS iStatus " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END AS iStatusId " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.userunkid ELSE 0 END AS userunkid " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.username ELSE '' END AS username " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.email ELSE '' END AS approver_email " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.priority ELSE 0 END AS priority " & _
            '        "   ,[@emp].empid AS employeeunkid " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.LvName, '') ELSE '' END AS levelname " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.uempid END AS uempid " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN Fn.ecompid END AS ecompid " & _
            '        "   ,[@emp].ecodename AS oemployee " & _
            '        "   ,[@emp].ename AS employee_name " & _
            '        "   ,iData.employeeunkid " & _
            '        "   ,iData.periodunkid " & _
            '        "   ,iData.statusunkid " & _
            '        "   ,iData.tranguid " & _
            '        "   ,iData.calibratnounkid " & _
    '                        "   ,CASE WHEN ISNULL(bapprovalremark,'') = '' THEN iData.approvalremark ELSE ISNULL(bapprovalremark,'') END AS approvalremark " & _
            '        "   ,iData.isfinal " & _
            '        "   ,iData.isprocessed " & _
            '        "   ,iData.isgrp " & _
            '        "   ,iData.grpid " & _
            '        "   ,CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.periodunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno " & _
            '        "   ,[@emp].Code " & _
            '        "   ,[@emp].JobTitle " & _
            '        "   ,CAST(1 AS BIT) AS isChanged " & _
    '                        "   ,ISNULL(NV.calibration_remark,ISNULL(iData.calibration_remark,ISNULL(iData.c_remark,''))) AS calibration_remark " & _
            '        "   ,iData.submitdate " & _
            '        "   ,iData.c_remark " & _
            '        "   ,iData.o_remark " & _
            '        "   ,iData.s_date " & _
            '        "   ,'' AS lstpRating " & _
            '        "   ,'' AS lstcRating " & _
    '                        "   ,ISNULL(acal.acrating,'') AS acrating " & _
    '                        "   ,ISNULL(acal.acremark,'') AS acremark " & _
    '                        "   ,ISNULL(B.lstcRating,'') AS lstapRating " & _
    '                        "   ,ISNULL(bcalibration_remark,'') AS apcalibremark " & _
    '                        "   ,ISNULL(LL.username,'') AS lusername " & _
    '                        "   ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND ISNULL(LL.cscore,0) >= score_from AND ISNULL(LL.cscore,0) <= score_to) AS lrating " & _
    '                        "   ,ISNULL(LL.calibration_remark,'') AS lcalibremark " & _
    '                        "   ,'' AS allocation " & _
    '                        "   ,audituserunkid " & _
    '                        "   ,'' AS calibuser " & _
    '                        "   ,Fn.email " & _
            '        "FROM @emp " & _
            '        "   JOIN " & _
            '        "       ( " & _
            '        "           SELECT " & _
            '        "                 a.calibration_no " & _
            '        "                ,a.Employee " & _
            '        "                ,a.DATE " & _
            '        "                ,a.povisionscore " & _
            '        "                ,a.calibratescore " & _
            '        "                ,a.isgrp " & _
            '        "                ,a.grpid " & _
            '        "                ,a.employeeunkid " & _
            '        "                ,a.periodunkid " & _
            '        "                ,a.statusunkid " & _
            '        "                ,a.tranguid " & _
            '        "                ,a.calibratnounkid " & _
            '        "                ,a.mappingunkid " & _
            '        "                ,a.approvalremark " & _
            '        "                ,a.isfinal " & _
            '        "                ,a.isprocessed " & _
            '        "                ,a.calibration_remark " & _
            '        "                ,a.submitdate " & _
            '        "                ,a.c_remark " & _
            '        "                ,a.o_remark " & _
            '        "                ,a.s_date " & _
    '                        "                ,a.audituserunkid " & _
            '        "           FROM " & _
            '        "           ( " & _
            '        "               SELECT " & _
            '        "                    calibration_no " & _
            '        "                   ,'/r/n' + employeecode + ' - ' + firstname + ' ' + surname AS Employee " & _
            '        "                   ,CONVERT(NVARCHAR(8), transactiondate, 112) AS DATE " & _
            '        "                   ,ISNULL(CONVERT(NVARCHAR(8),pScore.submit_date,112),'') AS submitdate " & _
            '        "                   ,ISNULL(pScore.c_remark, ISNULL(rScore.c_remark,'')) AS c_remark " & _
            '        "                   ,ISNULL(pScore.o_remark, ISNULL(rScore.o_remark,'')) AS o_remark " & _
            '        "                   ,CAST(ISNULL(pScore.finaloverallscore,rScore.finaloverallscore) AS DECIMAL(36, 2)) AS povisionscore " & _
            '        "                   ,CAST(calibrated_score AS DECIMAL(36, 2)) AS calibratescore " & _
            '        "                   ,CAST(0 AS BIT) AS isgrp " & _
            '        "                   ,hrassess_calibration_number.calibratnounkid AS grpid " & _
            '        "                   ,hrassess_computescore_approval_tran.employeeunkid " & _
            '        "                   ,hrassess_computescore_approval_tran.periodunkid " & _
            '        "                   ,hrassess_computescore_approval_tran.statusunkid " & _
            '        "                   ,hrassess_computescore_approval_tran.tranguid " & _
            '        "                   ,hrassess_computescore_approval_tran.calibratnounkid " & _
            '        "                   ,hrassess_computescore_approval_tran.mappingunkid " & _
            '        "                   ,hrassess_computescore_approval_tran.approvalremark " & _
            '        "                   ,hrassess_computescore_approval_tran.isfinal " & _
            '        "                   ,hrassess_computescore_approval_tran.isprocessed " & _
            '        "                   ,hrassess_computescore_approval_tran.calibration_remark " & _
            '        "                   ,pScore.submit_date AS s_date " & _
    '                        "                   ,hrassess_computescore_approval_tran.audituserunkid " & _
            '        "               FROM hrassess_computescore_approval_tran " & _
            '        "               LEFT JOIN " & _
            '        "               ( " & _
            '        "                   SELECT DISTINCT " & _
            '        "                        hrassess_computescore_rejection_tran.employeeunkid " & _
            '        "                       ,hrassess_computescore_rejection_tran.periodunkid " & _
            '        "                       ,hrassess_computescore_rejection_tran.calibratnounkid " & _
    '                       "                       ,hrassess_computescore_rejection_tran.finaloverallscore " & _
            '        "                       ,hrassess_computescore_rejection_tran.calibration_remark AS c_remark " & _
            '        "                       ,hrassess_computescore_rejection_tran.overall_remark AS o_remark " & _
            '        "                   FROM hrassess_computescore_rejection_tran " & _
    '                       "                   WHERE hrassess_computescore_rejection_tran.periodunkid = @periodunkid " & _
            '        "               ) AS rScore ON rScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
            '        "                   AND rScore.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
            '        "                   AND rScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
    '                                "               JOIN " & _
            '        "               ( " & _
            '        "                   SELECT DISTINCT " & _
            '        "                        calibratnounkid " & _
            '        "                       ,employeeunkid " & _
            '        "                       ,periodunkid " & _
            '        "                       ,finaloverallscore " & _
            '        "                       ,submit_date " & _
            '        "                       ,calibration_remark AS c_remark " & _
            '        "                       ,overall_remark AS o_remark " & _
            '        "                   FROM hrassess_compute_score_master " & _
            '        "                   WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
            '        "               ) AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
            '        "               AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
            '        "               JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
            '        "               JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "           WHERE statusunkid IN (0,1) AND isvoid = 0 aND hrassess_computescore_approval_tran.periodunkid = @periodunkid " & _
            '        "       ) AS A WHERE 1 = 1 AND A.statusunkid IN (0,1) " & _
            '        "   ) AS iData ON iData.employeeunkid = [@emp].empid " & _
            '        "    JOIN " & _
            '        "    ( " & _
            '        "        SELECT DISTINCT " & _
            '        "             cfuser_master.userunkid " & _
            '        "            ,cfuser_master.username " & _
            '        "            ,cfuser_master.email " & _
            '                      strSelect & " " & _
            '        "            ,LM.priority " & _
            '        "            ,LM.levelname AS LvName " & _
            '        "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
            '        "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
            '        "            ,EM.mapuserunkid " & _
            '        "            ,EM.mappingunkid AS oMappingId " & _
            '        "        FROM hrmsConfiguration..cfuser_master " & _
            '        "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
            '        "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
            '                     strAccessJoin & " " & _
            '        "        JOIN " & strDatabaseName & "..hrscore_calibration_approver_master EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
            '        "        JOIN " & strDatabaseName & "..hrscore_calibration_approverlevel_master AS LM ON EM.levelunkid = LM.levelunkid " & _
    '                                "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 AND EM.mapuserunkid = '" & CInt(iRow("mapuserunkid")) & "' "
    '                'S.SANDEEP |12-JUL-2019| -- END

    '                'S.SANDEEP |27-JUL-2019| -- START
    '                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    '                '-------------> REMOVED
    '                '"   ,CASE WHEN ISNULL(iData.calibration_remark,'') = '' THEN ISNULL(iData.c_remark,'') ELSE ISNULL(iData.calibration_remark,'') END AS calibration_remark "

    '                '-------------> ADDED
    '                '"   ,ISNULL(bcalibration_remark,'') AS calibration_remark "
    '                '"   ,[HTAT].calibration_remark AS bcalibration_remark " & _
    '                '"   ,ISNULL(acal.acrating,'') AS acrating " & _
    '                '"   ,ISNULL(acal.acremark,'') AS acremark " & _
    '                'S.SANDEEP |27-JUL-2019| -- END

    '                If strFilter.Trim.Length > 0 Then
    '                    StrQ &= " AND (" & strFilter & " ) "
    '                End If
    '                StrQ &= "     AND yearunkid = @Y " & _
    '                        "     AND privilegeunkid = @P " & _
    '                        ") AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
    '                        "LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        [HA].[mapuserunkid] " & _
    '                        "       ,[HM].[priority] " & _
    '                        "       ,[HTAT].[statusunkid] AS iStatusId " & _
    '                        "       ,[HTAT].[transactiondate] " & _
    '                        "       ,[HTAT].[mappingunkid] " & _
    '                        "       ,[HTAT].[periodunkid] AS iPeriodId " & _
    '                        "       ,[HTAT].calibratnounkid " & _
    '                        "       ,[HTAT].calibrated_score AS bscore " & _
    '                        "       ,[HTAT].calibration_remark AS bcalibration_remark " & _
    '                        "       ,[HTAT].approvalremark AS bapprovalremark " & _
    '                        "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND [HTAT].calibrated_score >= score_from AND [HTAT].calibrated_score <= score_to) AS lstcRating " & _
    '                        "       ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
    '                        "             WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ ' + CM.[username] + ' ]' " & _
    '                        "             WHEN [HTAT].[statusunkid] = 3 THEN @Reject + ' : [ ' + CM.[username] + ' ]' " & _
    '                        "        ELSE @PendingApproval END AS iStatus " & _
    '                        "       ,DENSE_RANK() OVER (PARTITION BY [HTAT].[employeeunkid],[HTAT].periodunkid ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
    '                        "       ,HTAT.[employeeunkid] " & _
    '                        "   FROM hrassess_computescore_approval_tran AS HTAT " & _
    '                        "       JOIN [dbo].[hrscore_calibration_approver_master] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] " & _
    '                        "       JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
    '                        "       LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
    '                        "   WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND [HTAT].[isvoid] = 0 " & _
    '                        "   AND HTAT.periodunkid = @periodunkid "
    '                If blnOnlyMyApproval Then
    '                    StrQ &= "   AND ISNULL(HTAT.isprocessed, 0) = 0 "
    '                End If
    '                StrQ &= ") AS B ON [B].[priority] = [Fn].[priority] AND [B].[iPeriodId] = [iData].[periodunkid] AND [iData].calibratnounkid = [B].calibratnounkid " & _
    '                        "AND [iData].employeeunkid = [B].employeeunkid " & _
    '                        "LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        X.xPeriodId " & _
    '                        "       ,X.xPeriodName " & _
    '                        "       ,X.employeeunkid " & _
    '                        "       ,X.calibratnounkid " & _
    '                        "       ,X.cscore " & _
    '                        "       ,x.approvalremark " & _
    '                        "       ,x.calibration_remark " & _
    '                        "   FROM " & _
    '                        "   ( " & _
    '                        "       SELECT " & _
    '                        "            DENSE_RANK() OVER (PARTITION BY MAT.employeeunkid,MAT.periodunkid ORDER BY MAT.auditdatetime DESC) as xno " & _
    '                        "           ,CP.periodunkid AS xPeriodId " & _
    '                        "           ,CP.period_name AS xPeriodName " & _
    '                        "           ,MAT.employeeunkid " & _
    '                        "           ,MAT.calibratnounkid " & _
    '                        "           ,CAST(MAT.calibrated_score AS DECIMAL(36,2)) AS cscore " & _
    '                        "           ,MAT.approvalremark " & _
    '                        "           ,MAT.calibration_remark " & _
    '                        "       FROM hrassess_computescore_approval_tran AS MAT " & _
    '                        "           JOIN cfcommon_period_tran CP ON MAT.periodunkid = CP.periodunkid " & _
    '                        "       WHERE MAT.isvoid = 0 AND MAT.periodunkid = @periodunkid "
    '                If blnOnlyMyApproval Then
    '                    StrQ &= " AND MAT.isprocessed = 0 "
    '                End If
    '                StrQ &= "   ) AS X WHERE X.xno = 1 " & _
    '                        ") AS NV ON NV.employeeunkid = iData.employeeunkid AND NV.xPeriodId = iData.periodunkid AND  NV.calibratnounkid = iData.calibratnounkid " & _
    '                        "LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        X.xPeriodId " & _
    '                        "       ,X.xPeriodName " & _
    '                        "       ,X.employeeunkid " & _
    '                        "       ,X.calibratnounkid " & _
    '                        "       ,X.cscore " & _
    '                        "       ,x.approvalremark " & _
    '                        "       ,x.calibration_remark " & _
    '                        "       ,X.username " & _
    '                        "   FROM " & _
    '                        "   ( " & _
    '                        "       SELECT " & _
    '                        "            DENSE_RANK() OVER (PARTITION BY MAT.employeeunkid ,MAT.periodunkid ORDER BY MAT.auditdatetime DESC) AS xno " & _
    '                        "           ,CP.periodunkid AS xPeriodId " & _
    '                        "           ,CP.period_name AS xPeriodName " & _
    '                        "           ,MAT.employeeunkid " & _
    '                        "           ,MAT.calibratnounkid " & _
    '                        "           ,MAT.calibration_remark " & _
    '                        "           ,CAST(MAT.calibrated_score AS DECIMAL(36, 2)) AS cscore " & _
    '                        "           ,MAT.approvalremark " & _
    '                        "           ,CM.username " & _
    '                        "       FROM hrassess_computescore_approval_tran AS MAT " & _
    '                        "           JOIN cfcommon_period_tran CP ON MAT.periodunkid = CP.periodunkid " & _
    '                        "           JOIN [dbo].[hrscore_calibration_approver_master] AS HA ON MAT.[mappingunkid] = [HA].[mappingunkid] " & _
    '                        "           JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
    '                        "           LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
    '                        "       WHERE MAT.isvoid = 0 " & _
    '                        "       AND MAT.periodunkid = @periodunkid " & _
    '                        "       AND MAT.isprocessed = 0 " & _
    '                        "       AND HM.priority < " & intCurrentPriorityId & " " & _
    '                        "   ) AS X " & _
    '                        "   WHERE X.xno = 1 " & _
    '                        ") AS LL ON LL.employeeunkid = iData.employeeunkid " & _
    '                        "   AND LL.xPeriodId = iData.periodunkid " & _
    '                        "   AND LL.calibratnounkid = iData.calibratnounkid " & _
    '                        strOuterJoin & " " & _
    '                        "LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        employeeunkid AS aempid " & _
    '                        "       ,periodunkid AS aprid " & _
    '                        "       ,calibrated_score As acscore " & _
    '                        "       ,calibration_remark AS acremark " & _
    '                        "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to) AS acrating " & _
    '                        "   FROM hrassess_computescore_approval_tran " & _
    '                        "   WHERE mappingunkid = 0 AND isvoid = 0 AND periodunkid = @periodunkid " & _
    '                        ") AS acal ON acal.aempid = iData.employeeunkid AND acal.aprid = iData.periodunkid " & _
    '                        "WHERE 1 = 1 AND iData.statusunkid IN (0,1) AND [iData].periodunkid = @periodunkid "
    '                'S.SANDEEP |27-JUL-2019| -- START
    '                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    '                '-------------------------------------------------- ADDED
    '                '"LEFT JOIN " & _
    '                '"( " & _
    '                '"   SELECT " & _
    '                '"        employeeunkid AS aempid " & _
    '                '"       ,periodunkid AS aprid " & _
    '                '"       ,calibrated_score As acscore " & _
    '                '"       ,calibration_remark AS acremark " & _
    '                '"       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to) AS acrating " & _
    '                '"   FROM hrassess_computescore_approval_tran " & _
    '                '"   WHERE mappingunkid = 0 AND isvoid = 0 AND periodunkid = @periodunkid " & _
    '                '") AS acal ON acal.aempid = iData.employeeunkid AND acal.aprid = iData.periodunkid " & _
    '                'S.SANDEEP |27-JUL-2019| -- END

    '                If blnOnlyMyApproval Then
    '                    StrQ &= " AND ISNULL([iData].[isprocessed],0) = 0 "
    '                End If

    '                If mblnIsSubmitForApproaval = False Then
    '                    StrQ &= " AND Fn.priority > " & intCurrentPriorityId
    '                End If

    '                If mstrFilterString.Trim.Length > 0 Then
    '                    StrQ &= " AND " & mstrFilterString
    '                End If

    '                'S.SANDEEP |05-SEP-2019| -- START
    '                'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
    '                If intCalibrationUnkid > 0 Then
    '                    StrQ &= " AND [iData].calibratnounkid = '" & intCalibrationUnkid & "' "
    '                End If
    '                'S.SANDEEP |05-SEP-2019| -- END

    '                If blnOnlyMyApproval Then
    '                    StrQ &= " AND Fn.userunkid = @U "
    '                    objDataOperation.AddParameter("@U", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
    '                End If

    '                StrQ &= "ORDER BY iData.calibratnounkid "

    '                StrQ &= "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL " & _
    '                        "DROP TABLE #EMP "

    '                'S.SANDEEP |11-JUL-2019| -- START
    '                'ISSUE/ENHANCEMENT : PA CHANGES
    '                '---------- REMOVED -> statusunkid = 1
    '                '---------- ADDED   -> statusunkid IN (0,1)
    '                'S.SANDEEP |11-JUL-2019| -- END

    '                objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
    '                objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '                objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)
    '                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkid)

    '                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
    '                objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
    '                objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
    '                objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

    '                Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                End If

    '                If dsList.Tables(0).Rows.Count > 0 Then

    '                    'S.SANDEEP |26-AUG-2019| -- START
    '                    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    '                    'Dim objRating As New clsAppraisal_Rating
    '                    'Dim dsRating As New DataSet
    '                    'dsRating = objRating.getComboList("List", False)
    '                    'objRating = Nothing
    '                    'If dsRating.Tables(0).Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateRating(x, dsRating))

    '                    Dim dtUsrAllocation As DataTable = GetUserAllocation(intPeriodUnkid, intCompanyId, objDataOperation)
    '                    If dtUsrAllocation IsNot Nothing Then
    '                        If dtUsrAllocation.Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateRating(x, dtUsrAllocation, True))
    '                    End If

    '                    Dim objRating As New clsAppraisal_Rating
    '                    Dim dsRating As New DataSet
    '                    dsRating = objRating.getComboList("List", False)
    '                    objRating = Nothing
    '                    If dsRating.Tables(0).Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateRating(x, dsRating.Tables(0), False))

    '                    'S.SANDEEP |26-AUG-2019| -- END
    '                End If

    '                If dsFinal.Tables.Count <= 0 Then
    '                    dsFinal = dsList
    '                Else
    '                    dsFinal.Tables(0).Merge(dsList.Tables(0).Copy, True)
    '                End If

    '            Next
    '        End If

    '        If dsFinal IsNot Nothing AndAlso dsFinal.Tables.Count > 0 Then
    '            dtList = dsFinal.Tables(0).Copy()
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApprovers; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dtList
    'End Function

    Public Function GetNextEmployeeApprovers(ByVal intPeriodUnkid As Integer _
                                            , ByVal strDatabaseName As String _
                                            , ByVal intCompanyId As Integer _
                                            , ByVal intYearId As Integer _
                                            , ByVal strUserAccessMode As String _
                                            , ByVal intPrivilegeId As Integer _
                                            , ByVal xEmployeeAsOnDate As String _
                                            , ByVal intUserId As Integer _
                                            , ByVal blnOnlyMyApproval As Boolean _
                                            , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                            , Optional ByVal intCurrentPriorityId As Integer = 0 _
                                            , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
                                            , Optional ByVal mstrFilterString As String = "" _
                                            , Optional ByVal strEmployeeIds As String = "" _
                                            , Optional ByVal intCalibrationUnkid As Integer = 0 _
                                            , Optional ByVal blnAddUACFilter As Boolean = False _
                                            , Optional ByVal mstrAdvanceFilter As String = "" _
                                            ) As DataTable
        Dim StrQ As String = String.Empty
        Dim dtList As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""

            Dim dsFinal As New DataSet
            Dim strUACJoin, strUACFilter, xAdvanceJoinQry As String
            strUACJoin = "" : strUACFilter = "" : xAdvanceJoinQry = ""

            modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM")
            modGlobal.GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(xEmployeeAsOnDate), strDatabaseName, "AEM")


            objDataOperation.ExecNonQuery("SET ARITHABORT ON")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                   "DROP TABLE #USR "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                StrQ &= "SELECT " & _
                        "    B" & index.ToString() & ".userunkid " & _
                        "   ,A.employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                        "   JOIN " & strDatabaseName & "..hrassess_computescore_approval_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            stationunkid " & _
                        "           ,deptgroupunkid " & _
                        "           ,departmentunkid " & _
                        "           ,sectiongroupunkid " & _
                        "           ,sectionunkid " & _
                        "           ,unitgroupunkid " & _
                        "           ,unitunkid " & _
                        "           ,teamunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,classunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
                        "   AND T.Rno = 1 " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            jobgroupunkid " & _
                        "           ,jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
                        "   AND J.Rno = 1 " & _
                        "   WHERE TAT.isvoid = 0 "
                If blnOnlyMyApproval Then
                    StrQ &= " AND TAT.isprocessed = 0 "
                End If
                StrQ &= ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "       JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid  AND CAM.iscalibrator = 0 " & _
                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        "   AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
                If index < strvalues.Length - 1 Then
                    StrQ &= " INTERSECT "
                End If
            Next

            StrQ &= ") AS Fl "

            StrQ &= "DECLARE @emp AS TABLE (empid INT,deptid INT,jobid INT,clsgrpid INT,clsid INT,branchid INT,deptgrpid INT,secgrpid INT,secid INT,unitgrpid INT,unitid INT,teamid INT,jgrpid INT,periodid INT,ecodename nvarchar(max),ename nvarchar(max),calibratnounkid INT,Code nvarchar(255), JobTitle nvarchar(max), eemail nvarchar(max)) " & _
                    "INSERT INTO @emp (empid, deptid, jobid, clsgrpid, clsid, branchid, deptgrpid, secgrpid, secid, unitgrpid, unitid, teamid, jgrpid, periodid, ecodename, ename,calibratnounkid,Code,JobTitle,eemail) " & _
                    "SELECT " & _
                    "     AEM.employeeunkid " & _
                    "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                    "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                    "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                    "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                    "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                    "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                    "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                    "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                    "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                    "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                    "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                    "    ,TAT.periodunkid " & _
                    "    ,AEM.employeecode + ' - ' + AEM.firstname + ' ' + AEM.surname " & _
                    "    ,AEM.firstname + ' ' + AEM.surname " & _
                    "    ,TAT.calibratnounkid " & _
                    "    ,AEM.employeecode " & _
                    "    ,EJOB.job_name AS JobTitle " & _
                    "    ,AEM.email AS eemail " & _
                    "FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                    "    JOIN hrassess_computescore_approval_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid "
            'S.SANDEEP |21-NOV-2019| -- START {eemail} -- END
            If strUACJoin.Trim.Length > 0 AndAlso intUserId > 0 Then
                StrQ &= strUACJoin
            End If
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        stationunkid " & _
                    "       ,deptgroupunkid " & _
                    "       ,departmentunkid " & _
                    "       ,sectiongroupunkid " & _
                    "       ,sectionunkid " & _
                    "       ,unitgroupunkid " & _
                    "       ,unitunkid " & _
                    "       ,teamunkid " & _
                    "       ,classgroupunkid " & _
                    "       ,classunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "
            If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND employeeunkid IN (" & strEmployeeIds & ") "
            StrQ &= ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        jobgroupunkid " & _
                    "       ,jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                    ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN hrjob_master AS EJOB ON EJOB.jobunkid = J.jobunkid "
            StrQ &= "WHERE 1 = 1 AND TAT.isvoid = 0 AND TAT.statusunkid IN (0,1) AND  [TAT].periodunkid = @periodunkid "
            If blnOnlyMyApproval Then
                StrQ &= " AND ISNULL([TAT].[isprocessed],0) = 0 "
            End If
            If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND AEM.employeeunkid IN (" & strEmployeeIds & ") "

            If intCalibrationUnkid > 0 Then
                StrQ &= " AND [TAT].calibratnounkid = '" & intCalibrationUnkid & "' "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            StrQ &= "SELECT DISTINCT " & _
                    "    CAST(0 AS BIT) AS iCheck " & _
                    "   ,iData.calibration_no " & _
                    "   ,iData.Employee " & _
                    "   ,iData.DATE " & _
                            "   ,ISNULL(iData.povisionscore,0) AS povisionscore " & _
                    "   ,ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) AS calibratescore " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[transactiondate], iData.Date) ELSE NULL END AS transactiondate " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[mappingunkid], iData.mappingunkid) ELSE 0 END AS mappingunkid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatus], @Pending) ELSE '' END AS iStatus " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END AS iStatusId " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.userunkid ELSE 0 END AS userunkid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.username ELSE '' END AS username " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.email ELSE '' END AS approver_email " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.priority ELSE 0 END AS priority " & _
                    "   ,[@emp].empid AS employeeunkid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.LvName, '') ELSE '' END AS levelname " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.uempid END AS uempid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.ecompid END AS ecompid " & _
                    "   ,[@emp].ecodename AS oemployee " & _
                    "   ,[@emp].ename AS employee_name " & _
                    "   ,iData.employeeunkid " & _
                    "   ,iData.periodunkid " & _
                    "   ,iData.statusunkid " & _
                    "   ,iData.tranguid " & _
                    "   ,iData.calibratnounkid " & _
                    "   ,CASE WHEN ISNULL(bapprovalremark,'') = '' THEN iData.approvalremark ELSE ISNULL(bapprovalremark,'') END AS approvalremark " & _
                    "   ,iData.isfinal " & _
                    "   ,iData.isprocessed " & _
                    "   ,iData.isgrp " & _
                    "   ,iData.grpid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.periodunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno " & _
                    "   ,[@emp].Code " & _
                    "   ,[@emp].JobTitle " & _
                    "   ,CAST(1 AS BIT) AS isChanged " & _
                    "   ,ISNULL(NV.calibration_remark,ISNULL(iData.calibration_remark,ISNULL(iData.c_remark,''))) AS calibration_remark " & _
                    "   ,iData.submitdate " & _
                    "   ,iData.c_remark " & _
                    "   ,iData.o_remark " & _
                    "   ,iData.s_date " & _
                    "   ,'' AS lstpRating " & _
                    "   ,'' AS lstcRating " & _
                    "   ,ISNULL(acal.acrating,'') AS acrating " & _
                    "   ,ISNULL(acal.acremark,'') AS acremark " & _
                    "   ,ISNULL(B.lstcRating,'') AS lstapRating " & _
                    "   ,ISNULL(bcalibration_remark,'') AS apcalibremark " & _
                    "   ,ISNULL(LL.username,'') AS lusername " & _
                    "   ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND ISNULL(LL.cscore,0) >= score_from AND ISNULL(LL.cscore,0) <= score_to) AS lrating " & _
                    "   ,ISNULL(LL.calibration_remark,'') AS lcalibremark " & _
                    "   ,'' AS allocation " & _
                    "   ,audituserunkid " & _
                    "   ,'' AS calibuser " & _
                    "   ,Fn.email " & _
                    "   ,[@emp].JobTitle " & _
                    "   ,[@emp].eemail AS eemail " & _
                    "FROM @emp " & _
                    "   JOIN #USR ON [@emp].empid = #USR.employeeunkid " & _
                    "   JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "                 a.calibration_no " & _
                    "                ,a.Employee " & _
                    "                ,a.DATE " & _
                    "                ,a.povisionscore " & _
                    "                ,a.calibratescore " & _
                    "                ,a.isgrp " & _
                    "                ,a.grpid " & _
                    "                ,a.employeeunkid " & _
                    "                ,a.periodunkid " & _
                    "                ,a.statusunkid " & _
                    "                ,a.tranguid " & _
                    "                ,a.calibratnounkid " & _
                    "                ,a.mappingunkid " & _
                    "                ,a.approvalremark " & _
                    "                ,a.isfinal " & _
                    "                ,a.isprocessed " & _
                    "                ,a.calibration_remark " & _
                    "                ,a.submitdate " & _
                    "                ,a.c_remark " & _
                    "                ,a.o_remark " & _
                    "                ,a.s_date " & _
                    "                ,a.audituserunkid " & _
                    "           FROM " & _
                    "           ( " & _
                    "               SELECT " & _
                    "                    calibration_no " & _
                    "                   ,'/r/n' + employeecode + ' - ' + firstname + ' ' + surname AS Employee " & _
                    "                   ,CONVERT(NVARCHAR(8), transactiondate, 112) AS DATE " & _
                    "                   ,ISNULL(CONVERT(NVARCHAR(8),pScore.submit_date,112),'') AS submitdate " & _
                    "                   ,ISNULL(pScore.c_remark, ISNULL(rScore.c_remark,'')) AS c_remark " & _
                    "                   ,ISNULL(pScore.o_remark, ISNULL(rScore.o_remark,'')) AS o_remark " & _
                    "                   ,CAST(ISNULL(pScore.finaloverallscore,rScore.finaloverallscore) AS DECIMAL(36, 2)) AS povisionscore " & _
                    "                   ,CAST(calibrated_score AS DECIMAL(36, 2)) AS calibratescore " & _
                    "                   ,CAST(0 AS BIT) AS isgrp " & _
                    "                   ,hrassess_calibration_number.calibratnounkid AS grpid " & _
                    "                   ,hrassess_computescore_approval_tran.employeeunkid " & _
                    "                   ,hrassess_computescore_approval_tran.periodunkid " & _
                    "                   ,hrassess_computescore_approval_tran.statusunkid " & _
                    "                   ,hrassess_computescore_approval_tran.tranguid " & _
                    "                   ,hrassess_computescore_approval_tran.calibratnounkid " & _
                    "                   ,hrassess_computescore_approval_tran.mappingunkid " & _
                    "                   ,hrassess_computescore_approval_tran.approvalremark " & _
                    "                   ,hrassess_computescore_approval_tran.isfinal " & _
                    "                   ,hrassess_computescore_approval_tran.isprocessed " & _
                    "                   ,hrassess_computescore_approval_tran.calibration_remark " & _
                    "                   ,pScore.submit_date AS s_date " & _
                    "                   ,hrassess_computescore_approval_tran.audituserunkid " & _
                    "               FROM hrassess_computescore_approval_tran " & _
                    "               LEFT JOIN " & _
                    "               ( " & _
                    "                   SELECT DISTINCT " & _
                    "                        hrassess_computescore_rejection_tran.employeeunkid " & _
                    "                       ,hrassess_computescore_rejection_tran.periodunkid " & _
                    "                       ,hrassess_computescore_rejection_tran.calibratnounkid " & _
                   "                       ,hrassess_computescore_rejection_tran.finaloverallscore " & _
                    "                       ,hrassess_computescore_rejection_tran.calibration_remark AS c_remark " & _
                    "                       ,hrassess_computescore_rejection_tran.overall_remark AS o_remark " & _
                    "                   FROM hrassess_computescore_rejection_tran " & _
                   "                   WHERE hrassess_computescore_rejection_tran.periodunkid = @periodunkid " & _
                    "               ) AS rScore ON rScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                    "                   AND rScore.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
                    "                   AND rScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                            "               JOIN " & _
                    "               ( " & _
                    "                   SELECT DISTINCT " & _
                    "                        calibratnounkid " & _
                    "                       ,employeeunkid " & _
                    "                       ,periodunkid " & _
                    "                       ,finaloverallscore " & _
                    "                       ,submit_date " & _
                    "                       ,calibration_remark AS c_remark " & _
                    "                       ,overall_remark AS o_remark " & _
                    "                   FROM hrassess_compute_score_master " & _
                    "                   WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
                    "               ) AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                    "               AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                    "               JOIN hrassess_calibration_number ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
                    "               JOIN hremployee_master ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "           WHERE statusunkid IN (0,1) AND isvoid = 0 aND hrassess_computescore_approval_tran.periodunkid = @periodunkid " & _
                    "       ) AS A WHERE 1 = 1 AND A.statusunkid IN (0,1) " & _
                    "   ) AS iData ON iData.employeeunkid = [@emp].empid " & _
                    "    JOIN " & _
                    "    ( " & _
                    "        SELECT DISTINCT " & _
                    "             cfuser_master.userunkid " & _
                    "            ,cfuser_master.username " & _
                    "            ,cfuser_master.email " & _
                                  strSelect & " " & _
                    "            ,LM.priority " & _
                    "            ,LM.levelname AS LvName " & _
                    "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                    "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                    "            ,EM.mapuserunkid " & _
                    "            ,EM.mappingunkid AS oMappingId " & _
                    "        FROM hrmsConfiguration..cfuser_master " & _
                    "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                    "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                                 strAccessJoin & " " & _
                    "        JOIN " & strDatabaseName & "..hrscore_calibration_approver_master EM ON EM.mapuserunkid = cfuser_master.userunkid  AND EM.iscalibrator = 0 " & _
                    "        JOIN " & strDatabaseName & "..hrscore_calibration_approverlevel_master AS LM ON EM.levelunkid = LM.levelunkid " & _
                    "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
            'S.SANDEEP |21-NOV-2019| -- START {eemail} -- END
            If strFilter.Trim.Length > 0 Then
                StrQ &= " AND (" & strFilter & " ) "
            End If
            StrQ &= "     AND yearunkid = @Y " & _
                    "     AND privilegeunkid = @P " & _
                    ") AS Fn ON #USR.userunkid = Fn.userunkid /*" & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & "*/ " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        [HA].[mapuserunkid] " & _
                    "       ,[HM].[priority] " & _
                    "       ,[HTAT].[statusunkid] AS iStatusId " & _
                    "       ,[HTAT].[transactiondate] " & _
                    "       ,[HTAT].[mappingunkid] " & _
                    "       ,[HTAT].[periodunkid] AS iPeriodId " & _
                    "       ,[HTAT].calibratnounkid " & _
                    "       ,[HTAT].calibrated_score AS bscore " & _
                    "       ,[HTAT].calibration_remark AS bcalibration_remark " & _
                    "       ,[HTAT].approvalremark AS bapprovalremark " & _
                    "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND [HTAT].calibrated_score >= score_from AND [HTAT].calibrated_score <= score_to) AS lstcRating " & _
                    "       ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
                    "             WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ ' + CM.[username] + ' ]' " & _
                    "             WHEN [HTAT].[statusunkid] = 3 THEN @Reject + ' : [ ' + CM.[username] + ' ]' " & _
                    "        ELSE @PendingApproval END AS iStatus " & _
                    "       ,DENSE_RANK() OVER (PARTITION BY [HTAT].[employeeunkid],[HTAT].periodunkid ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
                    "       ,HTAT.[employeeunkid] " & _
                    "   FROM hrassess_computescore_approval_tran AS HTAT " & _
                    "       JOIN [dbo].[hrscore_calibration_approver_master] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] AND [HA].iscalibrator = 0 " & _
                    "       JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
                    "       LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                    "   WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND [HTAT].[isvoid] = 0 " & _
                    "   AND HTAT.periodunkid = @periodunkid "
            If blnOnlyMyApproval Then
                StrQ &= "   AND ISNULL(HTAT.isprocessed, 0) = 0 "
            End If
            StrQ &= ") AS B ON [B].[priority] = [Fn].[priority] AND [B].[iPeriodId] = [iData].[periodunkid] AND [iData].calibratnounkid = [B].calibratnounkid " & _
                    "AND [iData].employeeunkid = [B].employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        X.xPeriodId " & _
                    "       ,X.xPeriodName " & _
                    "       ,X.employeeunkid " & _
                    "       ,X.calibratnounkid " & _
                    "       ,X.cscore " & _
                    "       ,x.approvalremark " & _
                    "       ,x.calibration_remark " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            DENSE_RANK() OVER (PARTITION BY MAT.employeeunkid,MAT.periodunkid ORDER BY MAT.auditdatetime DESC) as xno " & _
                    "           ,CP.periodunkid AS xPeriodId " & _
                    "           ,CP.period_name AS xPeriodName " & _
                    "           ,MAT.employeeunkid " & _
                    "           ,MAT.calibratnounkid " & _
                    "           ,CAST(MAT.calibrated_score AS DECIMAL(36,2)) AS cscore " & _
                    "           ,MAT.approvalremark " & _
                    "           ,MAT.calibration_remark " & _
                    "       FROM hrassess_computescore_approval_tran AS MAT " & _
                    "           JOIN cfcommon_period_tran CP ON MAT.periodunkid = CP.periodunkid " & _
                    "       WHERE MAT.isvoid = 0 AND MAT.periodunkid = @periodunkid "
            If blnOnlyMyApproval Then
                StrQ &= " AND MAT.isprocessed = 0 "
            End If
            StrQ &= "   ) AS X WHERE X.xno = 1 " & _
                    ") AS NV ON NV.employeeunkid = iData.employeeunkid AND NV.xPeriodId = iData.periodunkid AND  NV.calibratnounkid = iData.calibratnounkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        X.xPeriodId " & _
                    "       ,X.xPeriodName " & _
                    "       ,X.employeeunkid " & _
                    "       ,X.calibratnounkid " & _
                    "       ,X.cscore " & _
                    "       ,x.approvalremark " & _
                    "       ,x.calibration_remark " & _
                    "       ,X.username " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            DENSE_RANK() OVER (PARTITION BY MAT.employeeunkid ,MAT.periodunkid ORDER BY MAT.auditdatetime DESC) AS xno " & _
                    "           ,CP.periodunkid AS xPeriodId " & _
                    "           ,CP.period_name AS xPeriodName " & _
                    "           ,MAT.employeeunkid " & _
                    "           ,MAT.calibratnounkid " & _
                    "           ,MAT.calibration_remark " & _
                    "           ,CAST(MAT.calibrated_score AS DECIMAL(36, 2)) AS cscore " & _
                    "           ,MAT.approvalremark " & _
                    "           ,CM.username " & _
                    "       FROM hrassess_computescore_approval_tran AS MAT " & _
                    "           JOIN cfcommon_period_tran CP ON MAT.periodunkid = CP.periodunkid " & _
                    "           JOIN [dbo].[hrscore_calibration_approver_master] AS HA ON MAT.[mappingunkid] = [HA].[mappingunkid] AND [HA].iscalibrator = 0 " & _
                    "           JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM ON [HA].[levelunkid] = [HM].[levelunkid] " & _
                    "           LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                    "       WHERE MAT.isvoid = 0 " & _
                    "       AND MAT.periodunkid = @periodunkid " & _
                    "       AND MAT.isprocessed = 0 " & _
                    "       AND HM.priority < " & intCurrentPriorityId & " " & _
                    "   ) AS X " & _
                    "   WHERE X.xno = 1 " & _
                    ") AS LL ON LL.employeeunkid = iData.employeeunkid " & _
                    "   AND LL.xPeriodId = iData.periodunkid " & _
                    "   AND LL.calibratnounkid = iData.calibratnounkid " & _
                    strOuterJoin & " " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        employeeunkid AS aempid " & _
                    "       ,periodunkid AS aprid " & _
                    "       ,calibrated_score As acscore " & _
                    "       ,calibration_remark AS acremark " & _
                    "       ,calibratnounkid as acalibratnounkid " & _
                    "       ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to) AS acrating " & _
                    "   FROM hrassess_computescore_approval_tran " & _
                    "   WHERE mappingunkid = 0 AND isvoid = 0 AND periodunkid = @periodunkid " & _
                    ") AS acal ON acal.aempid = iData.employeeunkid AND acal.aprid = iData.periodunkid AND acal.acalibratnounkid = [iData].calibratnounkid " & _
                    "WHERE 1 = 1 AND iData.statusunkid IN (0,1) AND [iData].periodunkid = @periodunkid "

            'S.SANDEEP |08-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            '-- ADDED {1.,calibratnounkid as acalibratnounkid | 2. AND acal.acalibratnounkid = [iData].calibratnounkid}
            'S.SANDEEP |08-NOV-2019| -- END

            If blnOnlyMyApproval Then
                StrQ &= " AND ISNULL([iData].[isprocessed],0) = 0 "
            End If

            If mblnIsSubmitForApproaval = False Then
                StrQ &= " AND Fn.priority > " & intCurrentPriorityId
            End If

            If mstrFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & mstrFilterString
            End If

            If intCalibrationUnkid > 0 Then
                StrQ &= " AND [iData].calibratnounkid = '" & intCalibrationUnkid & "' "
            End If

            If blnOnlyMyApproval Then
                StrQ &= " AND Fn.userunkid = @U "
                objDataOperation.AddParameter("@U", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            End If

            StrQ &= "ORDER BY iData.calibratnounkid "

            StrQ &= "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                    "DROP TABLE #USR "

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkid)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtUsrAllocation As DataTable = GetUserAllocation(intPeriodUnkid, intCompanyId, objDataOperation)
                If dtUsrAllocation IsNot Nothing Then
                    If dtUsrAllocation.Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateRating(x, dtUsrAllocation, True))
                End If
                Dim objRating As New clsAppraisal_Rating
                Dim dsRating As New DataSet
                dsRating = objRating.getComboList("List", False)
                objRating = Nothing
                If dsRating.Tables(0).Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateRating(x, dsRating.Tables(0), False))
            End If

                    If dsFinal.Tables.Count <= 0 Then
                        dsFinal = dsList
                    Else
                        dsFinal.Tables(0).Merge(dsList.Tables(0).Copy, True)
                    End If

            If dsFinal IsNot Nothing AndAlso dsFinal.Tables.Count > 0 Then
                dtList = dsFinal.Tables(0).Copy()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApprovers; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtList
    End Function
    'S.SANDEEP |25-OCT-2019| -- END


    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    'Private Function UpdateRating(ByVal x As DataRow, ByVal dsRatings As DataSet) As Boolean
    '    Try
    '        Dim dr() As DataRow = Nothing
    '        dr = dsRatings.Tables(0).Select("scrf <= " & CDec(x.Item("povisionscore")) & " AND scrt >= " & CDec(x.Item("povisionscore")))
    '        If dr.Length > 0 Then
    '            x.Item("lstpRating") = dr(0)("name")
    '        End If
    '        dr = Nothing
    '        'S.SANDEEP |27-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    '        'dr = dsRatings.Tables(0).Select("scrf <= " & CDec(x.Item("calibratescore")) & " AND scrt >= " & CDec(x.Item("calibratescore")))
    '        'If dr.Length > 0 Then
    '        '    x.Item("lstcRating") = dr(0)("name")
    '        'End If
    '        If x.Item("lstcRating").ToString.Trim.Length <= 0 Then
    '            dr = dsRatings.Tables(0).Select("scrf <= " & CDec(x.Item("calibratescore")) & " AND scrt >= " & CDec(x.Item("calibratescore")))
    '            If dr.Length > 0 Then
    '                x.Item("lstcRating") = dr(0)("name")
    '            End If
    '        End If
    '        'S.SANDEEP |27-JUL-2019| -- END
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: UpdateRating; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return True
    'End Function
    Public Function UpdateRating(ByVal x As DataRow, ByVal dt As DataTable, ByVal blnIsAllocation As Boolean) As Boolean
        Try
            Dim dr() As DataRow = Nothing
            If blnIsAllocation = False Then
                If IsDBNull(x.Item("povisionscore")) = True Then x.Item("povisionscore") = 0
                dr = dt.Select("scrf <= " & CDec(x.Item("povisionscore")) & " AND scrt >= " & CDec(x.Item("povisionscore")))
            If dr.Length > 0 Then
                x.Item("lstpRating") = dr(0)("name")
            End If
            dr = Nothing
            If x.Item("lstcRating").ToString.Trim.Length <= 0 Then
                    If IsDBNull(x.Item("calibratescore")) = True Then x.Item("calibratescore") = 0
                    dr = dt.Select("scrf <= " & CDec(x.Item("calibratescore")) & " AND scrt >= " & CDec(x.Item("calibratescore")))
            If dr.Length > 0 Then
                x.Item("lstcRating") = dr(0)("name")
            End If
            End If
            Else
                dr = dt.Select("calibratnounkid = " & CInt(x.Item("calibratnounkid")) & " AND audituserunkid = " & CInt(x.Item("audituserunkid")))
                If dr.Length > 0 Then
                    x.Item("allocation") = dr(0)("allocation").ToString()
                    x.Item("calibuser") = dr(0)("username").ToString()
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRating; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP |26-AUG-2019| -- END

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Private Function UpdateParentGrp(ByVal x As DataRow, ByVal intValue As Integer) As Boolean
        Try
            x("pgrpid") = intValue
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateParentGrp; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP |25-OCT-2019| -- END


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="intNotificationType">1 = Submit for Approval , 2 = Notify Next User for Approve, 3 = Send Rejection to Selected User</param>    
    Public Sub SendNotification(ByVal intNotificationType As Integer, _
                                ByVal strDatabaseName As String, _
                                ByVal strUserAccessMode As String, _
                                ByVal intCompanyId As Integer, _
                                ByVal intYearId As Integer, _
                                ByVal intPrivilegeId As Integer, _
                                ByVal intUserId As Integer, _
                                ByVal intPeriodUnkid As Integer, _
                                ByVal strPeriodName As String, _
                                ByVal strCalibrationNo As String, _
                                ByVal xEmployeeAsOnDate As String, _
                                ByVal strEmployeeIds As String, _
                                ByVal strFormName As String, _
                                ByVal eMode As enLogin_Mode, _
                                ByVal strSenderAddress As String, _
                                ByVal strArutiSelfServiceURL As String, _
                                ByVal strUserName As String, _
                                Optional ByVal intPriority As Integer = -1, _
                                Optional ByVal strRemark As String = "", _
                                Optional ByVal intEmployeeIds As String = "", _
                                Optional ByVal objDataOpr As clsDataOperation = Nothing, _
                                Optional ByVal intGeneratedCalibUnkid As Integer = 0) 'S.SANDEEP |05-SEP-2019| -- START {intGeneratedCalibUnkid} -- END
        Try
            Dim dtAppr As DataTable = Nothing
            Dim StrMessage As New System.Text.StringBuilder
            'S.SANDEEP |05-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
            'Select Case intNotificationType
            '    Case 1  'Submit for Approval
            '        dtAppr = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, Nothing)
            '        If intPriority <> -1 Then
            '            dtAppr = New DataView(dtAppr, "priority > '" & intPriority & "'", "priority", DataViewRowState.CurrentRows).ToTable()
            '            intPriority = -1
            '        End If
            '    Case 2  'Notify Next User for Approve
            '        dtAppr = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, Nothing, intPriority, False)

            '    Case 3  'Send Rejection to Selected User
            '        dtAppr = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, Nothing)

            'End Select

            Select Case intNotificationType
                'S.SANDEEP |25-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                'Case 1  'Submit for Approval
                '    dtAppr = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, Nothing, , , , intEmployeeIds, intGeneratedCalibUnkid)
                '    If intPriority <> -1 Then
                '        dtAppr = New DataView(dtAppr, "priority > '" & intPriority & "'", "priority", DataViewRowState.CurrentRows).ToTable()
                '        intPriority = -1
                '    End If
                'Case 2  'Notify Next User for Approve
                '    dtAppr = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, Nothing, intPriority, False, , intEmployeeIds, intGeneratedCalibUnkid)
                'Case 3  'Send Rejection to Selected User
                '    dtAppr = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, Nothing, , , , intEmployeeIds, intGeneratedCalibUnkid)

                Case 1  'Submit for Approval
                    dtAppr = GetNextEmployeeApproversNew(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, False, Nothing, , , , intEmployeeIds, intGeneratedCalibUnkid, True)
                    If intPriority <> -1 Then
                        dtAppr = New DataView(dtAppr, "priority > '" & intPriority & "'", "priority", DataViewRowState.CurrentRows).ToTable()
                        intPriority = -1
                    End If
                Case 2  'Notify Next User for Approve
                    dtAppr = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, Nothing, intPriority, False, , intEmployeeIds, intGeneratedCalibUnkid, True)
                Case 3  'Send Rejection to Selected User
                    dtAppr = GetNextEmployeeApprovers(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, Nothing, , , , intEmployeeIds, intGeneratedCalibUnkid, True)
                    'S.SANDEEP |25-OCT-2019| -- END
            End Select
            'S.SANDEEP |05-SEP-2019| -- END

            'S.SANDEEP |24-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : SEQUENCE CONTAINS NO ELEMENT
            If dtAppr.Rows.Count <= 0 Then Exit Sub
            'S.SANDEEP |24-SEP-2019| -- END

            Dim dr() As DataRow = Nothing
            Dim iPriority As List(Of Integer) = dtAppr.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
            Dim intNextPriority As Integer = -1

            'S.SANDEEP |24-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : SEQUENCE CONTAINS NO ELEMENT
            'iPriority.Sort()
            'If intPriority = -1 Then intNextPriority = iPriority.Min()
            'If intNextPriority = -1 Then
            '    Try
            '        intNextPriority = iPriority.Item(iPriority.IndexOf(intPriority) + 1)
            '    Catch ex As Exception
            '        intNextPriority = intPriority
            '    End Try
            'End If
            If iPriority IsNot Nothing AndAlso iPriority.Count > 0 Then
            iPriority.Sort()
            If intPriority = -1 Then intNextPriority = iPriority.Min()
            If intNextPriority = -1 Then
                Try
                    intNextPriority = iPriority.Item(iPriority.IndexOf(intPriority) + 1)
        Catch ex As Exception
                    intNextPriority = intPriority
        End Try
            End If
            Else
                Exit Sub
            End If
            'S.SANDEEP |24-SEP-2019| -- END

            Dim dtFilterTab As DataTable = dtAppr.DefaultView.ToTable(True, "username", "email", "userunkid", "priority")
            Dim strSubject As String = ""
            If intNotificationType = 1 Then 'Send Submit for Approval
                strSubject = Language.getMessage(mstrModuleName, 209, "Notification for Submit for Calibration :") & " " & strCalibrationNo
            ElseIf intNotificationType = 2 Then 'Send Approve/Reject to Selected User
                strSubject = Language.getMessage(mstrModuleName, 201, "Notification for Approve/Reject of Calibration :") & " " & strCalibrationNo
            ElseIf intNotificationType = 3 Then 'Send Rejection to Selected User
                strSubject = Language.getMessage(mstrModuleName, 200, "Notification for Rejection of Calibration :") & " " & strCalibrationNo
            End If

            For Each iRow As DataRow In dtFilterTab.Rows
                If iRow("email").ToString().Trim().Length <= 0 Then Continue For
                StrMessage = New System.Text.StringBuilder
                Select Case intNotificationType
                    Case 1 'NOTIFICATION FOR SUBMIT FOR APPROVAL
                        dr = dtAppr.Select("priority = '" & intNextPriority & "' AND userunkid = " & CInt(iRow("userunkid")) & " " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())

                        If dr.Length > 0 Then
                            StrMessage.Append("<HTML><BODY>")
                            StrMessage.Append(vbCrLf)
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 203, "Dear") & " " & "<b>" & getTitleCase(iRow("username").ToString()) & "</b></span></p>")
                            StrMessage.Append(vbCrLf)
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 210, "This is to inform you that calibration has been submitted for the period of") & " (" & "<b>" & strPeriodName & "</b>" & "). " & Language.getMessage(mstrModuleName, 205, "Please login to aruti to approve/reject them.") & " </span></p>")
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append(vbCrLf)
                            StrMessage.Append("</span></b></p>")
                            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                            StrMessage.Append("</span></p>")
                            StrMessage.Append("</BODY></HTML>")

                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("email").ToString().Trim()
                            objSendMail._Subject = strSubject
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                            objSendMail.SendMail(intCompanyId)
                            objSendMail = Nothing
                        End If
                    Case 2 'NOTIFICATION FOR NEXT LEVEL OF APPROVAL
                        dr = dtAppr.Select("priority = '" & intNextPriority & "' AND userunkid = " & CInt(iRow("userunkid")) & "  " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())

                        If dr.Length > 0 Then
                            StrMessage.Append("<HTML><BODY>")
                            StrMessage.Append(vbCrLf)
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 203, "Dear") & " " & "<b>" & getTitleCase(iRow("username").ToString()) & "</b></span></p>")
                            StrMessage.Append(vbCrLf)
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 204, "This is to inform you that calibration process done for the period of") & " (" & "<b>" & strPeriodName & "</b>" & "). " & Language.getMessage(mstrModuleName, 205, "Please login to aruti to approve/reject them.") & " </span></p>")
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append(vbCrLf)
                            StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 206, "Approval Remark :") & " " & strRemark & " " & "</span></b>")
                            StrMessage.Append("</span></b></p>")
                            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                            StrMessage.Append("</span></p>")
                            StrMessage.Append("</BODY></HTML>")

                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("email").ToString().Trim()
                            objSendMail._Subject = strSubject
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                            objSendMail.SendMail(intCompanyId)
                            objSendMail = Nothing
                        End If

                    Case 3 'NOTIFICATION FOR REJECTION OF EMPLOYEE MOVEMENT
                        dr = dtAppr.Select("priority <= " & intPriority & " AND userunkid = " & CInt(iRow("userunkid")) & " " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())
                        If dr.Length > 0 Then
                            StrMessage.Append("<HTML><BODY>")
                            StrMessage.Append(vbCrLf)
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 203, "Dear") & " " & "<b>" & getTitleCase(iRow("username").ToString()) & "</b></span></p>")
                            StrMessage.Append(vbCrLf)
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 207, "This is to inform you that calibration process has been rejected for the period of") & " (" & "<b>" & strPeriodName & "</b>" & ").  </span></p>")
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                            StrMessage.Append(vbCrLf)
                            StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 208, "Rejection Remark :") & " " & strRemark & " " & "</span></b>")
                            StrMessage.Append("</span></b></p>")
                            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                            StrMessage.Append("</span></p>")
                            StrMessage.Append("</BODY></HTML>")

                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("email").ToString().Trim()
                            objSendMail._Subject = strSubject
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                            objSendMail.SendMail(intCompanyId)
                            objSendMail = Nothing
                        End If
                End Select
            Next


            'For Each iRow As DataRow In dtFilterTab.Rows
            '    StrMessage = New System.Text.StringBuilder
            '    Select Case intNotificationType
            '        Case 1 'Notify Next User for Approve
            '            dr = dtAppr.Select("rno = 1 AND priority > " & intPriority & " AND userunkid = " & CInt(iRow("userunkid")) & "  " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())
            '            If dr.Length > 0 Then
            '                StrMessage.Append("<HTML><BODY>")
            '                StrMessage.Append(vbCrLf)
            '                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            '                StrMessage.Append(Language.getMessage(mstrModuleName, 203, "Dear") & " " & "<b>" & getTitleCase(iRow("username").ToString()) & "</b></span></p>")
            '                StrMessage.Append(vbCrLf)
            '                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            '                StrMessage.Append(Language.getMessage(mstrModuleName, 204, "This is to inform you that calibration process done for the period of") & " (" & "<b>" & strPeriodName & "</b>" & ") " & Language.getMessage(mstrModuleName, 205, "Please login to aruti to approve/reject them.") & " </span></p>")
            '                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            '                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            '                StrMessage.Append(vbCrLf)
            '                StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 206, "Approval Remark :") & " " & strRemark & " " & "</span></b>")
            '                StrMessage.Append("</span></b></p>")
            '                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            '                StrMessage.Append("</span></p>")
            '                StrMessage.Append("</BODY></HTML>")

            '                Dim objSendMail As New clsSendMail
            '                objSendMail._ToEmail = iRow("email").ToString().Trim()
            '                objSendMail._Subject = strSubject
            '                objSendMail._Message = StrMessage.ToString()
            '                objSendMail._Form_Name = strFormName
            '                objSendMail._LogEmployeeUnkid = -1
            '                objSendMail._OperationModeId = eMode
            '                objSendMail._UserUnkid = intUserId
            '                objSendMail._SenderAddress = strSenderAddress
            '                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
            '                objSendMail.SendMail(intCompanyId)
            '                objSendMail = Nothing

            '            End If
            '        Case 2 'Send Rejection to Selected User
            '            dr = dtAppr.Select("priority <= " & intPriority & " AND userunkid = " & CInt(iRow("userunkid")) & " " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())
            '            If dr.Length > 0 Then
            '                StrMessage.Append("<HTML><BODY>")
            '                StrMessage.Append(vbCrLf)
            '                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            '                StrMessage.Append(Language.getMessage(mstrModuleName, 203, "Dear") & " " & "<b>" & getTitleCase(iRow("username").ToString()) & "</b></span></p>")
            '                StrMessage.Append(vbCrLf)
            '                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            '                StrMessage.Append(Language.getMessage(mstrModuleName, 207, "This is to inform you that calibration process has been rejected for the period of") & " (" & "<b>" & strPeriodName & "</b>" & ").  </span></p>")
            '                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            '                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            '                StrMessage.Append(vbCrLf)
            '                StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 208, "Rejection Remark :") & " " & strRemark & " " & "</span></b>")
            '                StrMessage.Append("</span></b></p>")
            '                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            '                StrMessage.Append("</span></p>")
            '                StrMessage.Append("</BODY></HTML>")

            '                Dim objSendMail As New clsSendMail
            '                objSendMail._ToEmail = iRow("email").ToString().Trim()
            '                objSendMail._Subject = strSubject
            '                objSendMail._Message = StrMessage.ToString()
            '                objSendMail._Form_Name = strFormName
            '                objSendMail._LogEmployeeUnkid = -1
            '                objSendMail._OperationModeId = eMode
            '                objSendMail._UserUnkid = intUserId
            '                objSendMail._SenderAddress = strSenderAddress
            '                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
            '                objSendMail.SendMail(intCompanyId)
            '                objSendMail = Nothing
            '            End If
            '    End Select
            'Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function GetStatusList(Optional ByVal strList As String = "List", _
                                  Optional ByVal blnAddSelect As Boolean = False, _
                                  Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                  Optional ByVal blnFromApprovalScreen As Boolean = False) As DataSet

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION ALL "
            End If
            If blnFromApprovalScreen = False Then
                StrQ &= "SELECT '" & enCalibrationStatus.NotSubmitted & "' AS Id, @NotSubmitted AS Name " & _
                        "UNION ALL SELECT '" & enCalibrationStatus.Submitted & "' AS Id, @Submitted AS Name " & _
                        "UNION ALL SELECT '" & enCalibrationStatus.Approved & "' AS Id, @Approved AS Name " & _
                        "UNION ALL SELECT '" & enCalibrationStatus.Rejected & "' AS Id, @Rejected AS Name "
                objDataOperation.AddParameter("@NotSubmitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Not Submitted"))
                objDataOperation.AddParameter("@Submitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Submitted"))

            Else
                StrQ &= "SELECT '" & enCalibrationStatus.Submitted & "' AS Id, @Submitted AS Name " & _
                        "UNION ALL SELECT '" & enCalibrationStatus.Approved & "' AS Id, @Approved AS Name " & _
                        "UNION ALL SELECT '" & enCalibrationStatus.Rejected & "' AS Id, @Rejected AS Name "
                objDataOperation.AddParameter("@Submitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            End If


            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Select"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Rejected"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetStatusList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function Void_Calibration(ByVal blnIsGrp As Boolean, _
                                     ByVal intCalibrationUnkid As Integer, _
                                     ByVal intEmployeeId As Integer, _
                                     ByVal intPeriodId As Integer, _
                                     ByVal strDataBaseName As String, _
                                     ByVal intCompanyId As Integer, _
                                     ByVal intYearId As Integer, _
                                     ByVal dtAuditdatetime As DateTime, _
                                     ByVal eAuditType As enAuditType, _
                                     ByVal intUserId As Integer, _
                                     ByVal strIPAddress As String, _
                                     ByVal strHostName As String, _
                                     ByVal strFormName As String, _
                                     ByVal strReason As String, _
                                     ByVal blnIsWeb As Boolean) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objData As New clsDataOperation
        objData.BindTransaction()
        Try
            If blnIsGrp Then

                StrQ = "INSERT INTO atcommon_log(refunkid, table_name, audittype, audituserunkid, auditdatetime, ip, host, value, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, loginemployeeunkid) " & _
                       "VALUES(@refunkid, @table_name, @audittype, @audituserunkid, @auditdatetime, @ip, @host, NULL, @form_name, '', '', '', '', '', @isweb, 0) "

                objData.AddParameter("@refunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationUnkid)
                objData.AddParameter("@table_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "hrassess_computescore_approval_tran")
                objData.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
                objData.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                objData.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtAuditdatetime)
                objData.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIPAddress)
                objData.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHostName)
                objData.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strFormName)
                objData.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsWeb)

                objData.ExecNonQuery(StrQ)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                End If

                objData.ClearParameters()

                StrQ = "UPDATE hrassess_computescore_approval_tran SET " & _
                       "     isvoid = 1 " & _
                       "    ,voidreason = @voidreason " & _
                       "    ,auditdatetime = @auditdatetime " & _
                       "    ,audittype = @audittype " & _
                       "    ,audituserunkid = @audituserunkid " & _
                       "    ,ip = @ip " & _
                       "    ,host = @host " & _
                       "    ,form_name = @form_name " & _
                       "WHERE calibratnounkid = @calibratnounkid "

                objData.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationUnkid)
                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strReason)
                objData.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
                objData.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                objData.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtAuditdatetime)
                objData.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIPAddress)
                objData.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHostName)
                objData.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strFormName)

                objData.ExecNonQuery(StrQ)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                End If

                StrQ = "UPDATE hrassess_compute_score_master SET " & _
                       " calibrated_score = 0 " & _
                       ",calibration_remark = '' " & _
                       ",calibratnounkid = 0 " & _
                       "WHERE calibratnounkid = @calibratnounkid "

                objData.ClearParameters()
                objData.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationUnkid)

                objData.ExecNonQuery(StrQ)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                End If

                dsList = clsCommonATLog.GetChildList(objData, "hrassess_compute_score_master", "calibratnounkid", intCalibrationUnkid)

                If dsList.Tables(0).Rows.Count > 0 Then
                    For Each iRow As DataRow In dsList.Tables(0).Rows
                        If clsCommonATLog.Insert_AtLog(objData, eAuditType, "hrassess_compute_score_master", "computescoremasterunkid", CInt(iRow("computescoremasterunkid")), False, intUserId, intCompanyId) = False Then
                            Throw New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                        End If
                    Next
                End If

            Else

                StrQ = "UPDATE hrassess_computescore_approval_tran SET " & _
                       "     isvoid = 1 " & _
                       "    ,voidreason = @voidreason " & _
                       "    ,auditdatetime = @auditdatetime " & _
                       "    ,audittype = @audittype " & _
                       "    ,audituserunkid = @audituserunkid " & _
                       "    ,ip = @ip " & _
                       "    ,host = @host " & _
                       "    ,form_name = @form_name " & _
                       "WHERE calibratnounkid = @calibratnounkid " & _
                       "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

                objData.ClearParameters()
                objData.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationUnkid)
                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strReason)
                objData.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
                objData.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                objData.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtAuditdatetime)
                objData.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIPAddress)
                objData.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHostName)
                objData.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strFormName)
                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objData.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                objData.ExecNonQuery(StrQ)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                End If

                StrQ = "UPDATE hrassess_compute_score_master SET " & _
                       "     calibrated_score = 0 " & _
                       "    ,calibration_remark = '' " & _
                       "    ,calibratnounkid = 0 " & _
                       "WHERE calibratnounkid = @calibratnounkid " & _
                       "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

                objData.ClearParameters()
                objData.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationUnkid)
                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objData.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                objData.ExecNonQuery(StrQ)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                End If

                dsList = clsCommonATLog.GetChildList(objData, "hrassess_compute_score_master", "calibratnounkid", intCalibrationUnkid)

                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim oRow As IEnumerable(Of DataRow) = Nothing
                    oRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("calibratnounkid") = intCalibrationUnkid And _
                                                                             x.Field(Of Integer)("employeeunkid") = intEmployeeId And _
                                                                             x.Field(Of Integer)("periodunkid") = intPeriodId)
                    If oRow IsNot Nothing Then
                        For Each iRow In oRow
                            If clsCommonATLog.Insert_AtLog(objData, eAuditType, "hrassess_compute_score_master", "computescoremasterunkid", CInt(iRow("computescoremasterunkid")), False, intUserId, intCompanyId) = False Then
                                Throw New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                            End If
                        Next
                    End If
                End If
            End If
            objData.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objData.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void_Calibration; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsExists(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal intCalibrationId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT 1 " & _
                   "FROM hrassess_computescore_approval_tran " & _
                   "WHERE isvoid = 0 " & _
                   "AND employeeunkid = @employeeunkid " & _
                   "AND periodunkid = @periodunkid /*AND calibratnounkid = @calibratnounkid*/ " & _
                   "AND isprocessed = 0 "

            objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.NAME_SIZE, mintCalibrationId.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsExists; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetApproverInfo(ByVal intUserId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsInfo As New DataSet
        Try
            Using objData As New clsDataOperation
                StrQ = "SELECT " & _
                       "     LM.levelname " & _
                       "    ,LM.priority " & _
                       "    ,UM.username " & _
                       "    ,EM.mapuserunkid " & _
                       "    ,EM.mappingunkid " & _
                       "FROM hrscore_calibration_approver_master AS EM " & _
                       "    JOIN hrscore_calibration_approverlevel_master AS LM ON EM.levelunkid = LM.levelunkid " & _
                       "    JOIN hrmsConfiguration..cfuser_master AS UM ON EM.mapuserunkid = UM.userunkid " & _
                       "WHERE EM.isactive = 1 AND EM.isvoid = 0 AND EM.iscalibrator = 0 AND EM.mapuserunkid = @mapuserunkid "

                objData.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                End If

                dsInfo = objData.ExecQuery(StrQ, "List")
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsInfo
    End Function

    Public Function GetFinalCalibarionRating(ByVal intCompanyId As Integer, _
                                             ByVal blnIncludePreviousRating As Boolean, _
                                             ByVal intEmployeeId As Integer, _
                                             ByVal blnIsCalibrationSettingActive As Boolean, _
                                             Optional ByVal strFilterString As String = "", _
                                             Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            Dim strExtraFilter As String = ""
            If blnIncludePreviousRating = False Then
                strExtraFilter = "  AND isclosed = 0 "
            End If

            StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & CInt(IIf(blnIsCalibrationSettingActive = True, 1, 0)) & " " & _
                   "SELECT DISTINCT " & _
                   "     financialyear_name AS fy " & _
                   "    ,ISNULL(CPT.period_name, '') AS pname " & _
                   "    ,CSM.employeeunkid as empid " & _
                   "    ,CSM.periodunkid AS pid " & _
                   "    ,ISNULL(CPT.start_date, '') AS sdate " & _
                   "    ,CPT.yearunkid AS yid " & _
                   "    ,ISNULL(CSM.isprocessed, 0) AS isprocessed " & _
                   "    ,database_name AS dbname " & _
                   "    ,CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
                   "          WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
                   "          WHEN @Setting = 0 THEN CSM.finaloverallscore END AS fscr " & _
                   "    ,ISNULL((SELECT TOP 1 " & _
                   "                grade_award " & _
                   "             FROM hrapps_ratings " & _
                   "             WHERE isvoid = 0 " & _
                   "             AND ISNULL(CASE WHEN @Setting =  1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
                   "                             WHEN @Setting =  1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
                   "                             WHEN @Setting =  0 THEN CSM.finaloverallscore END, 0) >= score_from " & _
                   "             AND ISNULL(CASE WHEN @Setting =  1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
                   "                             WHEN @Setting =  1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
                   "                             WHEN @Setting =  0 THEN CSM.finaloverallscore END, 0) <= score_to) " & _
                   "            ,'') AS rating " & _
                   "    ,isclosed " & _
                   "FROM hrassess_compute_score_master AS CSM " & _
                   "    JOIN cfcommon_period_tran AS CPT ON CPT.periodunkid = CSM.periodunkid " & _
                   "    JOIN hrmsConfiguration..cffinancial_year_tran ON CPT.yearunkid = cffinancial_year_tran.yearunkid " & _
                   "WHERE CSM.isvoid = 0 AND CSM.employeeunkid = '" & intEmployeeId & "' "

            If strExtraFilter.Trim.Length > 0 Then
                StrQ &= strExtraFilter
            End If

            'StrQ = "SELECT " & _
            '        "   financialyear_name AS fy " & _
            '        "  ,database_name AS dbname " & _
            '        "  ,yearunkid " & _
            '        "FROM hrmsConfiguration..cffinancial_year_tran " & _
            '        "WHERE companyunkid = '" & intCompanyId & "' " & strExtraFilter & " "
            'dsList = objDataOperation.ExecQuery(StrQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Dim dr As IEnumerable(Of DataRow) = dsList.Tables(0).AsEnumerable()
            'Dim StrFQ = "DECLARE @Setting AS BIT ; SET @Setting = " & CInt(IIf(blnIsCalibrationSettingActive = True, 1, 0)) & " "
            'Dim blnIsPeriodFound As Boolean = False
            'For Each ro In dr
            '    StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(periodunkid AS NVARCHAR(100)) FROM cfcommon_period_tran WHERE yearunkid = '" & ro("yearunkid").ToString() & "' AND isactive = 1 AND modulerefid = 5 FOR XML PATH('')), 1, 1, ''),'') AS eval "
            '    Dim strPeriodId As String = String.Empty
            '    dsList = objDataOperation.ExecQuery(StrQ, "List")
            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            '    If dsList.Tables(0).Rows.Count > 0 Then
            '        strPeriodId = dsList.Tables(0).Rows(0)("eval").ToString()
            '        If strPeriodId.Trim.Length <= 0 Then strPeriodId = "0"
            '    End If

            '    If strPeriodId.Trim.Length > 0 Then

            '        StrQ = "SELECT " & _
            '                "   '" & ro("fy").ToString() & "' AS fy " & _
            '                "   ,A" & ro("yearunkid").ToString() & ".pname " & _
            '                "   ,CASE WHEN @Setting = 1 AND A" & ro("yearunkid").ToString() & ".isprocessed = 1 THEN A" & ro("yearunkid").ToString() & ".PF " & _
            '                "         WHEN @Setting = 1 AND A" & ro("yearunkid").ToString() & ".isprocessed = 0 THEN '' " & _
            '                "         WHEN @Setting = 0 THEN A" & ro("yearunkid").ToString() & ".PF END AS rating " & _
            '                "   ,'" & ro("dbname").ToString() & "' AS dbname " & _
            '                "   ,A" & ro("yearunkid").ToString() & ".yid " & _
            '                "   ,A" & ro("yearunkid").ToString() & ".pid " & _
            '                "   ,A" & ro("yearunkid").ToString() & ".employeeunkid AS empid " & _
            '                "   ,A" & ro("yearunkid").ToString() & ".finaloverallscore AS fscr " & _
            '                "   ,A" & ro("yearunkid").ToString() & ".start_date AS sdate " & _
            '                "FROM " & _
            '                "( " & _
            '                "   SELECT DISTINCT " & _
            '                "        CSM.employeeunkid " & _
            '                "       ,CSM.periodunkid AS pid " & _
            '                "       ,ISNULL(CPT.period_name,'') AS pname " & _
            '                "       ,ISNULL(CPT.start_date,'') AS start_date " & _
            '                "       ,CPT.yearunkid AS yid " & _
            '                "       ,ISNULL(CSM.isprocessed,0) AS isprocessed " & _
            '                "       ,CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
            '                "             WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
            '                "             WHEN @Setting = 0 THEN CSM.finaloverallscore END AS finaloverallscore " & _
            '                "       ,ISNULL((SELECT TOP 1 grade_award " & _
            '                "                FROM " & ro("dbname").ToString() & "..hrapps_ratings " & _
            '                "                WHERE isvoid = 0 " & _
            '                "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
            '                "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
            '                "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) >= score_from " & _
            '                "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
            '                "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
            '                "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) <= score_to " & _
            '                "        ), '') AS PF " & _
            '                "   FROM " & ro("dbname").ToString() & "..hrassess_compute_score_master AS CSM " & _
            '                "       JOIN " & ro("dbname").ToString() & "..cfcommon_period_tran AS CPT ON CPT.periodunkid = CSM.periodunkid " & _
            '                "   WHERE CSM.isvoid = 0 AND CSM.periodunkid IN (" & strPeriodId & ") " & _
            '                "       AND CSM.employeeunkid = '" & intEmployeeId & "' " & _
            '                ") AS A" & ro("yearunkid").ToString() & ""

            '        StrFQ &= StrQ

            '        StrFQ &= " UNION "
            '        blnIsPeriodFound = True
            '    End If
            'Next

            'If StrFQ.Trim.Length > 0 Then
            '    If blnIsPeriodFound Then
            '        StrFQ = StrFQ.Substring(0, StrFQ.Length - 6)
            '    End If
            'End If



            'dsList = objDataOperation.ExecQuery(StrFQ, "List")
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
            dtTable = dsList.Tables(0).Copy
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFinalCalibarionRating; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtTable
    End Function

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Public Function GetUserAllocation(ByVal intPeriodUnkid As Integer, _
                                      ByVal intCompanyid As Integer, _
                                      Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataTable
        Dim StrQ As String = String.Empty
        Dim dtTable As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim dsList As New DataSet
        Dim intAllocationId As Integer = 0
        Try
            StrQ = "SELECT " & _
                   "    cua.key_value As AllocId " & _
                   "FROM hrmsConfiguration..cfconfiguration AS cua " & _
                   "WHERE UPPER(cua.[key_name]) = 'DISPLAYCALIBRATIONUSERALLOCATIONID' " & _
                   "AND cua.companyunkid = '" & intCompanyid & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intAllocationId = CInt(dsList.Tables(0).Rows(0)("AllocId"))
            End If


                StrQ = "SELECT DISTINCT " & _
                       "     CAT.calibratnounkid " & _
                       "    ,CAT.audituserunkid " & _
                       "    ,usr.username " & _
                       "    ,usr.companyunkid " & _
                       "    ,usr.employeeunkid " & _
                       "    ,isnull(fyt.database_name,'') as dname " & _
                       "    ,isnull(cff.key_value,'') as eod " & _
                       "FROM hrassess_computescore_approval_tran AS CAT " & _
                       "    LEFT JOIN hrmsConfiguration..cfuser_master AS usr ON usr.userunkid = CAT.audituserunkid " & _
                       "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran fyt ON fyt.companyunkid = usr.companyunkid AND fyt.isclosed = 0 " & _
                       "    LEFT JOIN hrmsConfiguration..cfconfiguration as cff ON cff.companyunkid = usr.companyunkid AND UPPER(cff.[key_name]) = 'EMPLOYEEASONDATE' " & _
                       "WHERE statusunkid IN (0,1) AND CAT.periodunkid = '" & intPeriodUnkid & "' "


                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                dtTable = dsList.Tables(0).Copy()
                Dim dCol As New DataColumn
                With dCol
                    .DataType = GetType(System.String)
                    .ColumnName = "allocation"
                    .DefaultValue = ""
                End With
                dtTable.Columns.Add(dCol)
            If intAllocationId > 0 Then
                StrQ = "SELECT " & _
                       "     AEM.employeeunkid " & _
                       "    ,ISNULL(CASE " & intAllocationId & " " & _
                       "                WHEN " & enAllocation.BRANCH & " THEN hrstation_master.name " & _
                       "                WHEN " & enAllocation.DEPARTMENT_GROUP & " THEN hrdepartment_group_master.name " & _
                       "                WHEN " & enAllocation.DEPARTMENT & " THEN hrdepartment_master.name " & _
                       "                WHEN " & enAllocation.SECTION_GROUP & " THEN hrsectiongroup_master.name " & _
                       "                WHEN " & enAllocation.SECTION & " THEN hrsection_master.name " & _
                       "                WHEN " & enAllocation.UNIT_GROUP & " THEN hrunitgroup_master.name " & _
                       "                WHEN " & enAllocation.UNIT & " THEN hrunit_master.name " & _
                       "                WHEN " & enAllocation.TEAM & " THEN hrteam_master.name " & _
                       "                WHEN " & enAllocation.JOB_GROUP & " THEN hrjobgroup_master.name " & _
                       "                WHEN " & enAllocation.JOBS & " THEN hrjob_master.job_name " & _
                       "                WHEN " & enAllocation.CLASS_GROUP & " THEN hrclassgroup_master.name " & _
                       "                WHEN " & enAllocation.CLASSES & " THEN hrclasses_master.name " & _
                       "     END,'') AS Allocation " & _
                       "FROM #DBName#hremployee_master AS AEM " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             stationunkid " & _
                       "            ,deptgroupunkid " & _
                       "            ,departmentunkid " & _
                       "            ,sectiongroupunkid " & _
                       "            ,sectionunkid " & _
                       "            ,unitgroupunkid " & _
                       "            ,unitunkid " & _
                       "            ,teamunkid " & _
                       "            ,classgroupunkid " & _
                       "            ,classunkid " & _
                       "            ,employeeunkid " & _
                       "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                       "        FROM #DBName#hremployee_transfer_tran " & _
                       "        WHERE isvoid = 0 " & _
                       "        AND CONVERT(CHAR(8), effectivedate, 112) <= #EffDate# " & _
                       "        AND #DBName#hremployee_transfer_tran.employeeunkid = #EmpId# " & _
                       "    ) AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             jobgroupunkid " & _
                       "            ,jobunkid " & _
                       "            ,employeeunkid " & _
                       "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                       "        FROM #DBName#hremployee_categorization_tran " & _
                       "        WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= #EffDate# " & _
                       "        AND #DBName#hremployee_categorization_tran.employeeunkid = #EmpId# " & _
                       "    ) AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                       "    LEFT JOIN #DBName#hrstation_master ON T.stationunkid = hrstation_master.stationunkid AND hrstation_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrdepartment_group_master ON T.deptgroupunkid = hrdepartment_group_master.deptgroupunkid AND hrdepartment_group_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrdepartment_master ON T.departmentunkid = hrdepartment_master.departmentunkid AND hrdepartment_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrsectiongroup_master ON T.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid AND hrsectiongroup_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrsection_master ON T.sectionunkid = hrsection_master.sectionunkid AND hrsection_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrunitgroup_master ON T.unitgroupunkid = hrunitgroup_master.unitgroupunkid AND hrunitgroup_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrunit_master ON T.unitunkid = hrunit_master.unitunkid AND hrunit_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrteam_master ON T.teamunkid = hrteam_master.teamunkid AND hrteam_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrclassgroup_master ON T.classgroupunkid = hrclassgroup_master.classgroupunkid AND hrclassgroup_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrclasses_master ON T.classunkid = hrclasses_master.classesunkid AND hrclasses_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrjob_master ON J.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                       "    LEFT JOIN #DBName#hrjobgroup_master ON J.jobgroupunkid = hrjobgroup_master.jobgroupunkid AND hrjobgroup_master.isactive = 1 " & _
                       "WHERE 1 = 1 AND AEM.employeeunkid = #EmpId# "

                'S.SANDEEP |24-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : Orignal Query Replaced
                Dim strOriginal As String = String.Empty
                strOriginal = StrQ
                'S.SANDEEP |24-SEP-2019| -- START
                For Each iRow As DataRow In dsList.Tables(0).Rows
                    If iRow.Item("dname").ToString.Trim.Length > 0 Then
                        'S.SANDEEP |24-SEP-2019| -- START
                        'ISSUE/ENHANCEMENT : Orignal Query Replaced
                        'StrQ = StrQ.Replace("#DBName#", iRow.Item("dname").ToString() & "..")
                        'StrQ = StrQ.Replace("#EffDate#", "'" & iRow.Item("eod").ToString() & "'")
                        'StrQ = StrQ.Replace("#EmpId#", iRow.Item("employeeunkid").ToString())
                        StrQ = strOriginal
                        StrQ = StrQ.Replace("#DBName#", iRow.Item("dname").ToString() & "..")
                        StrQ = StrQ.Replace("#EffDate#", "'" & iRow.Item("eod").ToString() & "'")
                        StrQ = StrQ.Replace("#EmpId#", iRow.Item("employeeunkid").ToString())
                        'S.SANDEEP |24-SEP-2019| -- START

                        Dim dsTrans As New DataSet

                        dsTrans = objDataOperation.ExecQuery(StrQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        End If
                        Dim iRowIndex As Integer = -1
                        If dsTrans.Tables(0).Rows.Count > 0 Then
                            iRowIndex = dsList.Tables(0).Rows.IndexOf(iRow)
                            dtTable.Rows(iRowIndex)("allocation") = dsTrans.Tables(0).Rows(0)("Allocation").ToString()
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUserAllocation; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtTable
    End Function

    Public Function GetComboListCalibrationNo(ByVal strList As String, ByVal intPeriodId As Integer, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION ALL "
            End If
            StrQ &= "SELECT CBN.calibratnounkid AS Id, CBN.calibration_no AS Name " & _
                    "FROM hrassess_calibration_number AS CBN " & _
                    "WHERE CBN.calibratnounkid IN (SELECT DISTINCT CAT.calibratnounkid FROM hrassess_computescore_approval_tran AS CAT WHERE CAT.isvoid = 0 AND CAT.periodunkid = @periodunkid) "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Select"))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboListCalibrationNo; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'S.SANDEEP |26-AUG-2019| -- END

'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
    Public Function IsApproverPresent(ByVal strDatabaseName As String, _
                                      ByVal strUserAccessMode As String, _
                                      ByVal intCompanyId As Integer, _
                                      ByVal intYearId As Integer, _
                                      ByVal intPrivilegeId As Integer, _
                                      ByVal intUserId As Integer, _
                                      ByVal xEmployeeAsOnDate As String, _
                                      ByVal intEmployeeId As Integer, _
                                      ByVal xDicDetails As Dictionary(Of String, List(Of String)), _
                                      ByVal xFaildEmp As List(Of Integer), _
                                      Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                      ) As String
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim strMessage As String = ""
        Try
            Dim strUACJoin, strUACFilter As String
            strUACJoin = "" : strUACFilter = ""
            StrQ = "SELECT " & _
                   "     UM.userunkid " & _
                   "    ,UM.username " & _
                   "FROM hrscore_calibration_approver_master AS EM " & _
                   "    JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = EM.mapuserunkid " & _
                   "    JOIN hrscore_calibration_approverlevel_master AS LM ON EM.levelunkid = LM.levelunkid " & _
                   "    JOIN hrmsConfiguration..cfcompanyaccess_privilege AS CP ON CP.userunkid = UM.userunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_privilege AS UP ON UM.userunkid = UP.userunkid " & _
                   "WHERE isvoid = 0 AND EM.isactive = 1 AND EM.iscalibrator = 0 AND CP.yearunkid = @Y AND UP.privilegeunkid = @P "

            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count <= 0 Then
                strMessage = Language.getMessage(mstrModuleName, 7, "Sorry, No approver(s) defined for calibration. Please define approver in order to continue.")
                Exit Try
            End If

            Dim blnFlag As Boolean = False

            For Each dr As DataRow In dsList.Tables(0).Rows
                modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, CInt(dr("userunkid")), intCompanyId, intYearId, strUserAccessMode, "AEM", True)
                StrQ = "SELECT " & _
                       "     AEM.employeeunkid " & _
                       "    ,AEM.employeecode AS ecode " & _
                       "    ,AEM.firstname+' '+ISNULL(AEM.othername,'')+' '+AEM.surname AS ename " & _
                       "    ,AEM.employeecode+' - '+AEM.firstname+' '+ISNULL(AEM.othername,'')+' '+AEM.surname AS dename " & _
                       "FROM " & strDatabaseName & "..hremployee_master AS AEM "
                If strUACJoin.Trim.Length > 0 Then
                    StrQ &= strUACJoin
                End If
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        stationunkid " & _
                        "       ,deptgroupunkid " & _
                        "       ,departmentunkid " & _
                        "       ,sectiongroupunkid " & _
                        "       ,sectionunkid " & _
                        "       ,unitgroupunkid " & _
                        "       ,unitunkid " & _
                        "       ,teamunkid " & _
                        "       ,classgroupunkid " & _
                        "       ,classunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "   FROM hremployee_transfer_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   AND hremployee_transfer_tran.employeeunkid = '" & intEmployeeId & "' " & _
                        ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        jobgroupunkid " & _
                        "       ,jobunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "   FROM hremployee_categorization_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   AND hremployee_categorization_tran.employeeunkid = '" & intEmployeeId & "' " & _
                        ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                        "WHERE AEM.employeeunkid = '" & intEmployeeId & "' "

                Dim dsEmp As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                If dsEmp.Tables(0).Rows.Count > 0 Then
                    If xDicDetails.ContainsKey(dr.Item("username").ToString()) Then
                        xDicDetails(dr.Item("username").ToString()).Add(dsEmp.Tables(0).Rows(0)("dename").ToString())
                    Else
                        Dim iList As New List(Of String) : iList.Add(dsEmp.Tables(0).Rows(0)("dename").ToString())
                        xDicDetails.Add(dr.Item("username").ToString(), iList)
                    End If
                    'Exit For
                Else
                    xFaildEmp.Add(intEmployeeId)
                    'Exit For
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsApproverPresent; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMessage
    End Function

    'Public Sub SendSubmitNotification(ByVal strDatabaseName As String, _
    '                                  ByVal strUserAccessMode As String, _
    '                                  ByVal intCompanyId As Integer, _
    '                                  ByVal intYearId As Integer, _
    '                                  ByVal intPrivilegeId As Integer, _
    '                                  ByVal intUserId As Integer, _
    '                                  ByVal intPeriodUnkid As Integer, _
    '                                  ByVal strPeriodName As String, _
    '                                  ByVal xEmployeeAsOnDate As String, _
    '                                  ByVal strEmployeeIds As String, _
    '                                  ByVal strFormName As String, _
    '                                  ByVal eMode As enLogin_Mode, _
    '                                  ByVal strSenderAddress As String, _
    '                                  ByVal strArutiSelfServiceURL As String, _
    '                                  ByVal strUserName As String, _
    '                                  Optional ByVal objDataOpr As clsDataOperation = Nothing)
    '    Dim StrQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation
    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Try
    '        Dim objUsr As New clsUserAddEdit
    '        Dim dsUser As New DataSet
    '        dsUser = objUsr.Get_UserBy_PrivilegeId(CInt(enUserPriviledge.AllowtoCalibrateProvisionalScore), intYearId)
    '        Dim dtUser As New DataTable
    '        dtUser = New DataView(dsUser.Tables(0), "UEmail <> ''", "", DataViewRowState.CurrentRows).ToTable()
    '        objUsr = Nothing

    '        If dtUser.Rows.Count <= 0 Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, you cannot send notification to start calibration. Reason, Privilege [Allow to Calibrate Provisional Score] is not assigned to user(s) or the user(s) email is not set into the system.")
    '            Exit Sub
    '        End If

    '        'S.SANDEEP |20-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        'StrQ = "DECLARE @words VARCHAR (MAX) " & _
    '        '       "SET @words = '" & strEmployeeIds & "' " & _
    '        '       "DECLARE @split TABLE(word int) " & _
    '        '       "DECLARE @word INT, @start INT, @end INT, @stop INT " & _
    '        '       "SELECT @words = @words + ',', @start = 1, @stop = LEN(@words) + 1 " & _
    '        '       "WHILE @start < @stop " & _
    '        '       "BEGIN " & _
    '        '       "    SELECT " & _
    '        '       "         @end   = CHARINDEX(',',@words,@start) " & _
    '        '       "        ,@word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
    '        '       "        ,@start = @end+1 " & _
    '        '       "    INSERT @split VALUES (@word) " & _
    '        '       "END " & _
    '        '       "SELECT DISTINCT " & _
    '        '       "     employeecode AS ecode " & _
    '        '       "    ,firstname +' '+ surname AS ename " & _
    '        '       "FROM @split " & _
    '        '       "    JOIN hremployee_master ON hremployee_master.employeeunkid = [@split].word " & _
    '        '       "WHERE 1 = 1 "

    '        StrQ = "IF OBJECT_ID('tempdb..##UR') IS NOT NULL " & _
    '               "DROP TABLE ##UR "
    '        StrQ &= "SELECT " & _
    '                "* " & _
    '                "INTO ##UR " & _
    '                "FROM " & _
    '                "( "

    '        If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '        Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
    '        For index As Integer = 0 To strvalues.Length - 1
    '            Dim xStrJoinColName As String = ""
    '            Dim xIntAllocId As Integer = 0
    '            Select Case CInt(strvalues(index))
    '                Case enAllocation.BRANCH
    '                    xStrJoinColName = "stationunkid"
    '                    xIntAllocId = CInt(enAllocation.BRANCH)
    '                Case enAllocation.DEPARTMENT_GROUP
    '                    xStrJoinColName = "deptgroupunkid"
    '                    xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
    '                Case enAllocation.DEPARTMENT
    '                    xStrJoinColName = "departmentunkid"
    '                    xIntAllocId = CInt(enAllocation.DEPARTMENT)
    '                Case enAllocation.SECTION_GROUP
    '                    xStrJoinColName = "sectiongroupunkid"
    '                    xIntAllocId = CInt(enAllocation.SECTION_GROUP)
    '                Case enAllocation.SECTION
    '                    xStrJoinColName = "sectionunkid"
    '                    xIntAllocId = CInt(enAllocation.SECTION)
    '                Case enAllocation.UNIT_GROUP
    '                    xStrJoinColName = "unitgroupunkid"
    '                    xIntAllocId = CInt(enAllocation.UNIT_GROUP)
    '                Case enAllocation.UNIT
    '                    xStrJoinColName = "unitunkid"
    '                    xIntAllocId = CInt(enAllocation.UNIT)
    '                Case enAllocation.TEAM
    '                    xStrJoinColName = "teamunkid"
    '                    xIntAllocId = CInt(enAllocation.TEAM)
    '                Case enAllocation.JOB_GROUP
    '                    xStrJoinColName = "jobgroupunkid"
    '                    xIntAllocId = CInt(enAllocation.JOB_GROUP)
    '                Case enAllocation.JOBS
    '                    xStrJoinColName = "jobunkid"
    '                    xIntAllocId = CInt(enAllocation.JOBS)
    '                Case enAllocation.CLASS_GROUP
    '                    xStrJoinColName = "classgroupunkid"
    '                    xIntAllocId = CInt(enAllocation.CLASS_GROUP)
    '                Case enAllocation.CLASSES
    '                    xStrJoinColName = "classunkid"
    '                    xIntAllocId = CInt(enAllocation.CLASSES)
    '            End Select
    '            StrQ &= "SELECT " & _
    '                    "    B" & index.ToString() & ".userunkid " & _
    '                    "   ,A.employeeunkid " & _
    '                    "FROM " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                    "        AEM.employeeunkid " & _
    '                    "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
    '                    "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
    '                    "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
    '                    "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
    '                    "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
    '                    "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
    '                    "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
    '                    "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
    '                    "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
    '                    "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
    '                    "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
    '                    "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
    '                    "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
    '                    "   LEFT JOIN " & _
    '                    "   ( " & _
    '                    "       SELECT " & _
    '                    "            stationunkid " & _
    '                    "           ,deptgroupunkid " & _
    '                    "           ,departmentunkid " & _
    '                    "           ,sectiongroupunkid " & _
    '                    "           ,sectionunkid " & _
    '                    "           ,unitgroupunkid " & _
    '                    "           ,unitunkid " & _
    '                    "           ,teamunkid " & _
    '                    "           ,classgroupunkid " & _
    '                    "           ,classunkid " & _
    '                    "           ,employeeunkid " & _
    '                    "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                    "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
    '                    "       WHERE isvoid = 0 " & _
    '                    "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '                    "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
    '                    "   AND T.Rno = 1 " & _
    '                    "   LEFT JOIN " & _
    '                    "   ( " & _
    '                    "       SELECT " & _
    '                    "            jobgroupunkid " & _
    '                    "           ,jobunkid " & _
    '                    "           ,employeeunkid " & _
    '                    "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                    "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
    '                    "       WHERE isvoid = 0 " & _
    '                    "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '                    "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
    '                    "   AND J.Rno = 1 " & _
    '                    "   WHERE AEM.isapproved = 1 "
    '            StrQ &= ") AS A " & _
    '                    "JOIN " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                    "        UPM.userunkid " & _
    '                    "       ,UPT.allocationunkid " & _
    '                    "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                    "       JOIN hrscore_calibration_approver_master AS CAM ON CAM.mapuserunkid = UPM.userunkid " & _
    '                    "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                    "   WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
    '                    "   AND CAM.isvoid = 0 AND CAM.isactive = 1 AND CAM.iscalibrator = 0 " & _
    '                    ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
    '            If index < strvalues.Length - 1 Then
    '                StrQ &= " INTERSECT "
    '            End If
    '        Next

    '        StrQ &= ") AS Fl " & _
    '                "SELECT " & _
    '                "    ##UR.userunkid " & _
    '                "   ,##UR.employeeunkid " & _
    '                "FROM ##UR " & _
    '                "JOIN hrmsConfiguration..cfuser_privilege ON cfuser_privilege.userunkid = ##UR.userunkid " & _
    '                "WHERE privilegeunkid = " & CInt(enUserPriviledge.AllowtoCalibrateProvisionalScore) & " " & _
    '                "DROP TABLE ##UR "

    '        objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
    '        objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)

    '        Dim oUser As New DataSet
    '        oUser = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        StrQ = "DECLARE @words VARCHAR (MAX) " & _
    '               "SET @words = '" & strEmployeeIds & "' " & _
    '               "DECLARE @split TABLE(word int) " & _
    '               "DECLARE @word INT, @start INT, @end INT, @stop INT " & _
    '               "SELECT @words = @words + ',', @start = 1, @stop = LEN(@words) + 1 " & _
    '               "WHILE @start < @stop " & _
    '               "BEGIN " & _
    '               "    SELECT " & _
    '               "         @end   = CHARINDEX(',',@words,@start) " & _
    '               "        ,@word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
    '               "        ,@start = @end+1 " & _
    '               "    INSERT @split VALUES (@word) " & _
    '               "END " & _
    '               "SELECT DISTINCT " & _
    '               "     [@split].word " & _
    '               "FROM @split " & _
    '               "WHERE 1 = 1 "

    '        Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        Dim ivals = (From A In oUser.Tables(0).AsEnumerable() _
    '                     Join B In dsList.Tables(0).AsEnumerable() On A.Field(Of Integer)("employeeunkid") Equals B.Field(Of Integer)("word") Select A)

    '        If ivals Is Nothing Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, you cannot send notification to start calibration. Reason, Privilege [Allow to Calibrate Provisional Score] is not assigned to user(s) or the user(s) email is not set into the system.")
    '            Exit Sub
    '        End If
    '        If ivals.Count <= 0 Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, you cannot send notification to start calibration. Reason, Privilege [Allow to Calibrate Provisional Score] is not assigned to user(s) or the user(s) email is not set into the system.")
    '            Exit Sub
    '        End If

    '        Dim iFinal As IEnumerable(Of DataRow) = Nothing
    '        'S.SANDEEP |22-NOV-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        Dim oData As DataTable = Nothing
    '        'S.SANDEEP |22-NOV-2019| -- END
    '        If ivals IsNot Nothing AndAlso ivals.Count > 0 Then
    '            oData = ivals.CopyToDataTable().DefaultView().ToTable(True, "userunkid", "employeeunkid")
    '            iFinal = (From A In dtUser.AsEnumerable() _
    '                          Join B In oData.AsEnumerable() On A.Field(Of Integer)("UId") Equals B.Field(Of Integer)("userunkid") Select A)
    '        End If
    '        If iFinal Is Nothing Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, you cannot send notification to start calibration. Reason, Privilege [Allow to Calibrate Provisional Score] is not assigned to user(s) or the user(s) email is not set into the system.")
    '            Exit Sub
    '        End If
    '        If iFinal.Count <= 0 Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, you cannot send notification to start calibration. Reason, Privilege [Allow to Calibrate Provisional Score] is not assigned to user(s) or the user(s) email is not set into the system.")
    '            Exit Sub
    '        End If
    '        If iFinal IsNot Nothing AndAlso iFinal.Count > 0 Then
    '            dtUser = iFinal.CopyToDataTable().DefaultView().ToTable(True, "UName", "UEmail", "Uid", "Id")
    '        End If
    '        'S.SANDEEP |20-NOV-2019| -- END

    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            Dim info1 As Globalization.TextInfo = Globalization.CultureInfo.InvariantCulture.TextInfo
    '            Dim objMail As New clsSendMail
    '            'S.SANDEEP |22-NOV-2019| -- START
    '            'ISSUE/ENHANCEMENT : Calibration Issues
    '            Dim strMsg As String = String.Empty
    '            'S.SANDEEP |22-NOV-2019| -- END
    '            For Each dtRow As DataRow In dtUser.Rows
    '                If dtRow.Item("UEmail").ToString().Trim.Length <= 0 Then Continue For
    '                Dim strLink As String = ""
    '                Dim strMessage As String = ""
    '                objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification to Calibrate Provisional Score.")
    '                strMessage = "<HTML> <BODY>"
    '                strMessage &= Language.getMessage(mstrModuleName, 10, "Dear") & " " & info1.ToTitleCase(dtRow.Item("UName").ToString().ToLower()) & ", <BR><BR>"
    '                strMessage &= Language.getMessage(mstrModuleName, 11, "Please note that, Compute score process is done for the") & " "
    '                strMessage &= Language.getMessage(mstrModuleName, 12, "period") & " <b>(" & strPeriodName & ")</b>.&nbsp;"
    '                strMessage &= Language.getMessage(mstrModuleName, 13, "Kindly click the link below in order to Calibrate Provisional Score.")
    '                strMessage &= "<BR>"
    '                strLink = strArutiSelfServiceURL & "/Assessment New/Peformance_Calibration/wPg_SubmitList.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & dtRow.Item("UId").ToString & "|" & intPeriodUnkid.ToString))
    '                strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
    '                strMessage &= "<BR><BR>"
    '                strMessage &= Language.getMessage(mstrModuleName, 15, "Regards,")
    '                strMessage &= "<BR>"
    '                strMessage &= info1.ToTitleCase((strUserName).ToLower()).ToString()
    '                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '                strMessage &= "</BODY></HTML>"
    '                objMail._Message = strMessage
    '                objMail._ToEmail = dtRow.Item("UEmail").ToString()
    '                If eMode <= 0 Then eMode = enLogin_Mode.DESKTOP
    '                objMail._Form_Name = strFormName
    '                objMail._LogEmployeeUnkid = 0
    '                objMail._OperationModeId = eMode
    '                objMail._UserUnkid = intUserId
    '                objMail._SenderAddress = strSenderAddress
    '                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                'S.SANDEEP |22-NOV-2019| -- START
    '                'ISSUE/ENHANCEMENT : Calibration Issues
    '                'objMail.SendMail(intCompanyId)
    '                strMsg = objMail.SendMail(intCompanyId)
    '                If oData IsNot Nothing AndAlso strMsg.Trim.Length <= 0 Then
    '                    Dim itemp As DataRow() = oData.Select("userunkid = '" & CInt(dtRow("Uid")) & "'")
    '                    If itemp IsNot Nothing AndAlso itemp.Length > 0 Then
    '                        For idx As Integer = 0 To itemp.Length - 1
    '                            StrQ = "UPDATE hrassess_compute_score_master SET isnotified = 1 WHERE employeeunkid IN (" & itemp(idx)("employeeunkid").ToString() & ") AND isnotified = 0 "

    '                            objDataOperation.ExecNonQuery(StrQ)

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            End If
    '                        Next
    '                    End If
    '                End If
    '                'S.SANDEEP |22-NOV-2019| -- END

    '            Next
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: SendSubmitNotification; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Public Sub SendSubmitNotification(ByVal intCompanyId As Integer, _
                                      ByVal intYearId As Integer, _
                                      ByVal intPrivilegeId As Integer, _
                                      ByVal intUserId As Integer, _
                                      ByVal intPeriodUnkid As Integer, _
                                      ByVal strPeriodName As String, _
                                      ByVal strFormName As String, _
                                      ByVal eMode As enLogin_Mode, _
                                      ByVal strSenderAddress As String, _
                                      ByVal strEmpIds As String, _
                                      ByVal strArutiSelfServiceURL As String, _
                                      ByVal strUserName As String, _
                                      ByVal dtUser As DataTable, _
                                      Optional ByVal objDataOpr As clsDataOperation = Nothing)
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
                Dim info1 As Globalization.TextInfo = Globalization.CultureInfo.InvariantCulture.TextInfo
                Dim objMail As New clsSendMail
                Dim strMsg As String = String.Empty
                For Each dtRow As DataRow In dtUser.Rows
                    If dtRow.Item("UEmail").ToString().Trim.Length <= 0 Then Continue For
                    Dim strLink As String = ""
                    Dim strMessage As String = ""
                    objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification to Calibrate Provisional Score.")
                    strMessage = "<HTML> <BODY>"
                    strMessage &= Language.getMessage(mstrModuleName, 10, "Dear") & " " & info1.ToTitleCase(dtRow.Item("UName").ToString().ToLower()) & ", <BR><BR>"
                    strMessage &= Language.getMessage(mstrModuleName, 11, "Please note that, Compute score process is done for the") & " "
                    strMessage &= Language.getMessage(mstrModuleName, 12, "period") & " <b>(" & strPeriodName & ")</b>.&nbsp;"
                    strMessage &= Language.getMessage(mstrModuleName, 13, "Kindly click the link below in order to Calibrate Provisional Score.")
                    strMessage &= "<BR>"
                    strLink = strArutiSelfServiceURL & "/Assessment New/Peformance_Calibration/wPg_SubmitList.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & dtRow.Item("UId").ToString & "|" & intPeriodUnkid.ToString))
                    strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                    strMessage &= "<BR><BR>"
                    strMessage &= Language.getMessage(mstrModuleName, 15, "Regards,")
                    strMessage &= "<BR>"
                    strMessage &= info1.ToTitleCase((strUserName).ToLower()).ToString()
                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                    strMessage &= "</BODY></HTML>"
                    objMail._Message = strMessage
                    objMail._ToEmail = dtRow.Item("UEmail").ToString()
                    If eMode <= 0 Then eMode = enLogin_Mode.DESKTOP
                    objMail._Form_Name = strFormName
                    objMail._LogEmployeeUnkid = 0
                    objMail._OperationModeId = eMode
                    objMail._UserUnkid = intUserId
                    objMail._SenderAddress = strSenderAddress
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                    strMsg = objMail.SendMail(intCompanyId)
                'If oData IsNot Nothing AndAlso strMsg.Trim.Length <= 0 Then
                '    Dim itemp As DataRow() = oData.Select("userunkid = '" & CInt(dtRow("Uid")) & "'")
                '    If itemp IsNot Nothing AndAlso itemp.Length > 0 Then
                '        For idx As Integer = 0 To itemp.Length - 1
                '            StrQ = "UPDATE hrassess_compute_score_master SET isnotified = 1 WHERE employeeunkid IN (" & itemp(idx)("employeeunkid").ToString() & ") AND isnotified = 0 "

                '            objDataOperation.ExecNonQuery(StrQ)

                '            If objDataOperation.ErrorMessage <> "" Then
                '                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '            End If
                '        Next
                '    End If
                'End If
            Next

            StrQ = "DECLARE @DATA NVARCHAR(MAX) = '" & strEmpIds & "' " & _
                   "DECLARE @CNT INT " & _
                   "DECLARE @TEMP TABLE (empid int) " & _
                   "DECLARE @SEP AS CHAR(1) " & _
                   "SET @SEP = ',' " & _
                   "SET @CNT = 1 " & _
                   "WHILE (CHARINDEX(@SEP,@DATA)>0) " & _
                   "    BEGIN " & _
                   "        INSERT INTO @TEMP (empid) " & _
                   "        SELECT DATA = LTRIM(RTRIM(SUBSTRING(@DATA,1,CHARINDEX(@SEP,@DATA)-1))) " & _
                   "        SET @DATA = SUBSTRING(@DATA,CHARINDEX(@SEP,@DATA)+1,LEN(@DATA)) " & _
                   "        SET @CNT = @CNT + 1 " & _
                   "    END " & _
                   "INSERT INTO @TEMP (empid) " & _
                   "SELECT DATA = LTRIM(RTRIM(@DATA)) " & _
                   "UPDATE hrassess_compute_score_master " & _
                   "SET isnotified = 1 " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT empid FROM @TEMP " & _
                   ") AS A WHERE A.empid = hrassess_compute_score_master.employeeunkid " & _
                   "AND isnotified = 0 AND periodunkid = '" & intPeriodUnkid & "' " & _
                   "AND isvoid = 0 "

                                objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendSubmitNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |16-AUG-2019| -- END

    'S.SANDEEP |21-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Public Sub SendNotificationToEmployee(ByVal xTable As DataTable, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal intUserId As Integer, _
                                          ByVal strPeriodName As String, _
                                          ByVal strEmployeeIds As String, _
                                          ByVal strFormName As String, _
                                          ByVal eMode As enLogin_Mode, _
                                          ByVal strSenderAddress As String, _
                                          ByVal strArutiSelfServiceURL As String)
        Try
            Dim oFinal As DataTable = New DataView(xTable, "isfinal = true AND eemail <> ''", "", DataViewRowState.CurrentRows).ToTable()
            If oFinal IsNot Nothing AndAlso oFinal.Rows.Count > 0 Then
                For Each row As DataRow In oFinal.Rows
                    Dim StrMessage = New System.Text.StringBuilder
                    StrMessage.Append("<HTML><BODY>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 203, "Dear") & " " & "<b>" & getTitleCase(row("employee_name").ToString()) & "</b></span></p>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 212, "This is to inform you that calibration score process has been completed for the period of") & " (" & "<b>" & strPeriodName & "</b>" & "). " & _
                                      Language.getMessage(mstrModuleName, 214, "Please login to Aruti Self Service to view your final score.") & " </span></p>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("</span></b></p>")
                    StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                    StrMessage.Append("</span></p>")
                    StrMessage.Append("</BODY></HTML>")

                    Dim objSendMail As New clsSendMail
                    objSendMail._ToEmail = row("eemail").ToString().Trim()
                    objSendMail._Subject = Language.getMessage(mstrModuleName, 213, "Notification for Final Calibrated Score")
                    objSendMail._Message = StrMessage.ToString()
                    objSendMail._Form_Name = strFormName
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = eMode
                    objSendMail._UserUnkid = intUserId
                    objSendMail._SenderAddress = strSenderAddress
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                    objSendMail.SendMail(intCompanyId)
                    objSendMail = Nothing

                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotificationToEmployee; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |21-NOV-2019| -- END


    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
    Public Function IsValidToStartCalibration(ByVal xPeriodId As Integer, _
                                              ByVal xDatabaseName As String, _
                                              ByVal xUserUnkid As Integer, _
                                              ByVal xYearUnkid As Integer, _
                                              ByVal xCompanyUnkid As Integer, _
                                              ByVal xPeriodStart As DateTime, _
                                              ByVal xPeriodEnd As DateTime, _
                                              ByVal xUserModeSetting As String, _
                                              ByVal xOnlyApproved As Boolean, _
                                              ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                              ByVal strTableName As String, _
                                              ByVal blnIsReviewerNeeded As Boolean, _
                                              ByRef strMsg As String) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim objDataOperation As New clsDataOperation
        Try
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName, "EM")

            Dim intMatchValue As Integer = 0
            If blnIsReviewerNeeded Then
                intMatchValue = 3
            Else
                intMatchValue = 2
            End If

            objDataOperation.ExecNonQuery("SET ARITHABORT ON")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            StrQ = "SELECT " & _
                   "     CAM.mappingunkid " & _
                   "    ,CAM.mapuserunkid " & _
                   "    ,CAT.employeeunkid " & _
                   "    ,ISNULL(D.iAll,0) AS ascnt " & _
                   "    ,EM.employeecode + ' - ' + EM.firstname+' '+ EM.surname AS empname " & _
                   "FROM hrscore_calibration_approver_master AS CAM " & _
                   "    JOIN hrscore_calibration_approver_tran AS CAT ON CAM.mappingunkid = CAT.mappingunkid " & _
                   "    JOIN hremployee_master AS EM ON CAT.employeeunkid = EM.employeeunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             CSM.employeeunkid " & _
                   "            ,COUNT(1) AS iAll " & _
                   "        FROM hrassess_compute_score_master AS CSM " & _
                   "            JOIN hrscore_calibration_approver_tran AS CT ON CSM.employeeunkid = CT.employeeunkid AND CT.isvoid = 0 AND CT.visibletypeid = 1 " & _
                   "            JOIN hrscore_calibration_approver_master AS CA ON CA.mappingunkid = CT.mappingunkid AND CA.isvoid = 0 AND CA.visibletypeid = 1 " & _
                   "            AND CA.iscalibrator = 1 AND CA.mapuserunkid = @mapuserunkid " & _
                   "        WHERE CSM.isvoid = 0 AND CSM.periodunkid = @periodunkid AND CSM.issubmitted = 0 AND CSM.calibratnounkid <= 0 " & _
                   "        GROUP BY CSM.employeeunkid HAVING COUNT(1) = @Match " & _
                   "    ) AS D ON D.employeeunkid = CAT.employeeunkid "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            StrQ &= "    WHERE CAM.isvoid = 0 AND CAM.visibletypeid = 1 AND CAM.iscalibrator = 1 " & _
                    "    AND CAM.mapuserunkid = @mapuserunkid AND CAT.isvoid = 0 AND CAT.visibletypeid = 1 "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
            objDataOperation.AddParameter("@Match", SqlDbType.Int, eZeeDataType.INT_SIZE, intMatchValue)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 215, "Sorry, No Employee Found with this user. Make sure that you have made this user as calibrator and assigned employee to this user.")
                Exit Try
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                If dsList.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("ascnt") <= 0).Count > 0 Then
                    If intMatchValue = 2 Then
                        strMsg = Language.getMessage(mstrModuleName, 216, "Sorry, Some Employee has not done the assessment or not assessed by assessor. Please review those employee in order to start calibration.")
                    ElseIf intMatchValue = 3 Then
                        strMsg = Language.getMessage(mstrModuleName, 217, "Sorry, Some Employee has not done the assessment or not assessed by assessor/reviewer. Please review those employee in order to start calibration.")
                    End If
                    Exit Try
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidToStartCalibration; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function ApproveRejectBatch(ByVal eOprTypeId As Integer, _
                                       ByVal xPeriodId As Integer, _
                                       ByVal strPeriodName As String, _
                                       ByVal xDatabaseName As String, _
                                       ByVal xUserUnkid As Integer, _
                                       ByVal xYearUnkid As Integer, _
                                       ByVal xCompanyUnkid As Integer, _
                                       ByVal xPeriodStart As String, _
                                       ByVal xPeriodEnd As String, _
                                       ByVal xdicGuidRating As Dictionary(Of String, String), _
                                       ByVal xdicGuidRemark As Dictionary(Of String, String), _
                                       ByVal strApprlRemark As String, _
                                       ByVal intCurrentPriority As Integer, _
                                       ByVal strIPAddress As String, _
                                       ByVal strHostName As String, _
                                       ByVal strScreenName As String, _
                                       ByVal blnIsWeb As Boolean, _
                                       ByVal strSenderAddress As String) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim blnFlag As Boolean = False
        Dim intCalibrationId As Integer = 0
        Dim ds As New DataSet
        Dim iEmpLst As New List(Of Integer)
        Try
            If xdicGuidRating.Keys.Count > 0 Then
                Using objDataOperation As New clsDataOperation
                    objDataOperation.BindTransaction()
                    For Each key As String In xdicGuidRating.Keys
                        Dim mDecScore As Decimal = 0
                        Dim decScrF, decScrT As Decimal
                        decScrF = 0 : decScrT = 0


                        StrQ = "SELECT score_from, score_to FROM hrapps_ratings WHERE isvoid = 0 AND grade_award = @grade_award "

                        objDataOperation.AddParameter("@grade_award", SqlDbType.NVarChar, xdicGuidRating(key).Length, xdicGuidRating(key))

                        ds = objDataOperation.ExecQuery(StrQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If


                        If ds.Tables(0).Rows.Count > 0 Then
                            decScrF = CDec(ds.Tables(0).Rows(0)("score_from"))
                            decScrT = CDec(ds.Tables(0).Rows(0)("score_to"))
                        End If

                        Dim rnd As New Random
                        mDecScore = CDec(rnd.NextDouble() * (decScrT - decScrF) + decScrF)
                        If mDecScore <= 0 Then mDecScore = decScrT

                        StrQ = "SELECT * FROM hrassess_computescore_approval_tran WHERE tranguid = @guid AND periodunkid = @periodunkid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@guid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, key)
                        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)

                        ds = objDataOperation.ExecQuery(StrQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If ds.Tables(0).Rows.Count > 0 Then
                            If intCalibrationId <= 0 Then intCalibrationId = CInt(ds.Tables(0).Rows(0)("calibratnounkid"))
                            iEmpLst.Add(CInt(ds.Tables(0).Rows(0)("employeeunkid")))
                            StrQ = "SELECT @mapid = mappingunkid FROM hrscore_calibration_approver_master WHERE isvoid = 0 AND isactive = 1 AND visibletypeid = 1 AND mapuserunkid = @mapuserunkid AND iscalibrator = 0 "

                            Dim intMappingId As Integer = 0
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@mapid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMappingId, ParameterDirection.InputOutput)
                            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)

                            objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                objDataOperation.ReleaseTransaction(False)
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            intMappingId = CInt(objDataOperation.GetParameterValue("@mapid"))

                            Dim strNewGuid As String = Guid.NewGuid.ToString()

                            StrQ = "INSERT INTO hrassess_computescore_approval_tran " & _
                                   "(tranguid,calibratnounkid,transactiondate,mappingunkid,approvalremark,calibration_remark,isfinal,statusunkid,employeeunkid,periodunkid,calibrated_score,loginemployeeunkid,isvoid,voidreason,auditdatetime,audittype,audituserunkid,ip,host,form_name,isweb,isprocessed) " & _
                                   "VALUES " & _
                                   "(@tranguid,@calibratnounkid,GETDATE(),@mappingunkid,@approvalremark,@calibration_remark,CAST(0 AS BIT),@statusunkid,@employeeunkid,@periodunkid,@calibrated_score,0,CAST(0 AS BIT),'',GETDATE(),1,@audituserunkid,@ip,@host,@form_name,CAST(1 AS BIT),CAST(0 AS BIT)) "


                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strNewGuid.ToString)
                            objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ds.Tables(0).Rows(0)("calibratnounkid").ToString)
                            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMappingId.ToString)
                            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, mstrApprovalremark.Length, strApprlRemark.ToString)
                            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, eOprTypeId.ToString)
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ds.Tables(0).Rows(0)("employeeunkid").ToString)
                            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                            objDataOperation.AddParameter("@calibrated_score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecScore.ToString)
                            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid.ToString)
                            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIPAddress.ToString)
                            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHostName.ToString)
                            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strScreenName.ToString)
                            objDataOperation.AddParameter("@calibration_remark", SqlDbType.NVarChar, mstrCalibration_Remark.Length, xdicGuidRemark(key))

                            objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                objDataOperation.ReleaseTransaction(False)
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If eOprTypeId = CInt(enCalibrationStatus.Rejected) Then
                                StrQ = "INSERT INTO hrassess_computescore_rejection_tran(tranguid,employeeunkid,periodunkid,calibrated_score,calibratnounkid,overall_remark,submit_date,calibration_remark,finaloverallscore) " & _
                                       "SELECT DISTINCT " & _
                                       "     tranguid " & _
                                       "    ,hrassess_compute_score_master.employeeunkid " & _
                                       "    ,hrassess_compute_score_master.periodunkid " & _
                                       "    ,hrassess_computescore_approval_tran.calibrated_score " & _
                                       "    ,hrassess_computescore_approval_tran.calibratnounkid " & _
                                       "    ,overall_remark " & _
                                       "    ,submit_date " & _
                                       "    ,hrassess_compute_score_master.calibration_remark " & _
                                       "    ,finaloverallscore " & _
                                       "FROM hrassess_computescore_approval_tran " & _
                                       "    JOIN hrassess_compute_score_master ON hrassess_compute_score_master.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                                       "    AND hrassess_compute_score_master.periodunkid = hrassess_computescore_approval_tran.periodunkid AND hrassess_compute_score_master.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                                       "WHERE tranguid = '" & strNewGuid & "' AND hrassess_computescore_approval_tran.calibratnounkid = @calibratnounkid " & _
                                       "    AND NOT EXISTS (SELECT * FROM hrassess_computescore_rejection_tran AS CRT WHERE CRT.tranguid = hrassess_computescore_approval_tran.tranguid AND CRT.periodunkid = hrassess_computescore_approval_tran.periodunkid AND CRT.employeeunkid = hrassess_computescore_approval_tran.employeeunkid AND CRT.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid) "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId.ToString)

                                objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    objDataOperation.ReleaseTransaction(False)
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If
                        End If
                    Next

                    ds = New DataSet
                    StrQ = "SELECT " & _
                           "     CAT.employeeunkid " & _
                           "    ,LVL.priority " & _
                           "    ,LVL.levelname " & _
                           "    ,USR.username " & _
                           "    ,CAM.mappingunkid " & _
                           "    ,CSA.calibratnounkid " & _
                           "    ,USR.email " & _
                           "    ,CBN.calibration_no " & _
                           "    ,CAM.mapuserunkid " & _
                           "    ,ISNULL(CSAT.mappingunkid,0) AS cmappingunkid " & _
                           "    ,ISNULL(CSAT.tranguid,'') AS ctranguid " & _
                           "FROM hrscore_calibration_approver_tran AS CAT " & _
                           "    JOIN hrassess_computescore_approval_tran AS CSA ON CAT.employeeunkid = CSA.employeeunkid " & _
                           "    JOIN hrassess_calibration_number AS CBN ON CBN.calibratnounkid = CSA.calibratnounkid " & _
                           "    JOIN hrscore_calibration_approver_master AS CAM ON CAM.mappingunkid = CAT.mappingunkid " & _
                           "    JOIN hrmsConfiguration..cfuser_master AS USR ON CAM.mapuserunkid = USR.userunkid " & _
                           "    JOIN hrscore_calibration_approverlevel_master AS LVL ON CAM.levelunkid = LVL.levelunkid " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             SAT.mappingunkid " & _
                           "            ,SAT.employeeunkid " & _
                           "            ,SAT.periodunkid " & _
                           "            ,SAT.tranguid " & _
                           "        FROM hrassess_computescore_approval_tran AS SAT " & _
                           "        WHERE SAT.isvoid = 0 AND SAT.periodunkid = @periodunkid AND SAT.calibratnounkid = @calibratnounkid AND SAT.statusunkid > 1 " & _
                           "    ) AS CSAT ON CAM.mappingunkid = CSAT.mappingunkid AND CSA.periodunkid = CSAT.periodunkid " & _
                           "    AND CSA.employeeunkid = CSAT.employeeunkid " & _
                           "WHERE CAT.isvoid = 0 AND CAT.visibletypeid = 1 AND CAM.visibletypeid = 1 AND CAM.isactive = 1 AND CAM.iscalibrator = 0 " & _
                           "AND CSA.periodunkid = @periodunkid AND CSA.isvoid = 0  AND CSA.calibratnounkid = @calibratnounkid AND CSA.statusunkid IN (0,1) " & _
                           "/*AND CSA.employeeunkid = 2027 AND LVL.priority <= 2*/ " & _
                           "ORDER BY CAT.employeeunkid,LVL.priority "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId)
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)

                    ds = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If eOprTypeId = CInt(enCalibrationStatus.Rejected) Then
                        StrQ = "UPDATE hrassess_computescore_approval_tran SET statusunkid = 0 WHERE calibratnounkid = @calibratnounkid AND mappingunkid <= 0; " & _
                               "UPDATE hrassess_computescore_approval_tran SET isvoid = 1,voidreason = 'Batch Rejected' WHERE calibratnounkid = @calibratnounkid AND statusunkid > 0; " & _
                               "UPDATE hrassess_computescore_approval_tran SET isprocessed = 0 WHERE calibratnounkid = @calibratnounkid AND mappingunkid <= 0 AND isvoid = 0; " & _
                               "UPDATE hrassess_compute_score_master SET issubmitted = 0,submit_date = NULL WHERE calibratnounkid = @calibratnounkid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId.ToString)

                        objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    '******************* NOTIFICATION SECTION ********************************'
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim mdicSent As New Dictionary(Of Integer, Integer)
                        Dim strSubject As String = ""
                        Dim StrMessage As New System.Text.StringBuilder
                        If eOprTypeId = CInt(enCalibrationStatus.Rejected) Then
                            Dim dr As IEnumerable(Of DataRow) = From row In ds.Tables(0).AsEnumerable() Join id In iEmpLst On row.Field(Of Integer)("employeeunkid") Equals id Where row.Field(Of Integer)("priority") <= intCurrentPriority Select row
                            strSubject = Language.getMessage(mstrModuleName, 200, "Notification for Rejection of Calibration :") & " " & dr(0)("calibration_no").ToString()
                            For Each r As DataRow In dr
                                If mdicSent.ContainsKey(CInt(r("mapuserunkid"))) = True Then Continue For
                                mdicSent.Add(CInt(r("mapuserunkid")), CInt(r("calibratnounkid")))

                                StrMessage = New System.Text.StringBuilder
                                StrMessage.Append("<HTML><BODY>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 203, "Dear") & " " & "<b>" & getTitleCase(r("username").ToString()) & "</b></span></p>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 207, "This is to inform you that calibration process has been rejected for the period of") & " (" & "<b>" & strPeriodName & "</b>" & ").  </span></p>")
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 208, "Rejection Remark :") & " " & strApprlRemark & " " & "</span></b>")
                                StrMessage.Append("</span></b></p>")
                                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                                StrMessage.Append("</span></p>")
                                StrMessage.Append("</BODY></HTML>")

                                Dim objSendMail As New clsSendMail
                                objSendMail._ToEmail = r("email").ToString().Trim()
                                objSendMail._Subject = strSubject
                                objSendMail._Message = StrMessage.ToString()
                                objSendMail._Form_Name = strScreenName
                                objSendMail._LogEmployeeUnkid = -1
                                objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
                                objSendMail._UserUnkid = xUserUnkid
                                objSendMail._SenderAddress = strSenderAddress
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                                objSendMail.SendMail(xCompanyUnkid)
                                objSendMail = Nothing
                            Next
                        ElseIf eOprTypeId = CInt(enCalibrationStatus.Approved) Then
                            Dim dr() As DataRow = Nothing
                            For Each empid In iEmpLst
                                dr = ds.Tables(0).Select("employeeunkid = '" & empid & "' AND priority > '" & intCurrentPriority & "'", "priority")
                                If dr.Length > 0 Then
                                    If mdicSent.ContainsKey(CInt(dr(0)("mapuserunkid"))) = True Then Continue For
                                    mdicSent.Add(CInt(dr(0)("mapuserunkid")), CInt(dr(0)("calibratnounkid")))
                                    strSubject = Language.getMessage(mstrModuleName, 201, "Notification for Approve/Reject of Calibration :") & " " & dr(0)("calibration_no").ToString()
                                    StrMessage = New System.Text.StringBuilder
                                    StrMessage.Append("<HTML><BODY>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                    StrMessage.Append(Language.getMessage(mstrModuleName, 203, "Dear") & " " & "<b>" & getTitleCase(dr(0)("username").ToString()) & "</b></span></p>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                    StrMessage.Append(Language.getMessage(mstrModuleName, 210, "This is to inform you that calibration has been submitted for the period of") & " (" & "<b>" & strPeriodName & "</b>" & "). " & Language.getMessage(mstrModuleName, 205, "Please login to aruti to approve/reject them.") & " </span></p>")
                                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("</span></b></p>")
                                    StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                                    StrMessage.Append("</span></p>")
                                    StrMessage.Append("</BODY></HTML>")

                                    Dim objSendMail As New clsSendMail
                                    objSendMail._ToEmail = dr(0)("email").ToString().Trim()
                                    objSendMail._Subject = strSubject
                                    objSendMail._Message = StrMessage.ToString()
                                    objSendMail._Form_Name = strScreenName
                                    objSendMail._LogEmployeeUnkid = -1
                                    objSendMail._OperationModeId = enLogin_Mode.EMP_SELF_SERVICE
                                    objSendMail._UserUnkid = xUserUnkid
                                    objSendMail._SenderAddress = strSenderAddress
                                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                                    objSendMail.SendMail(xCompanyUnkid)
                                    objSendMail = Nothing
                                Else
                                    dr = ds.Tables(0).Select("employeeunkid = '" & empid & "' AND priority = '" & intCurrentPriority & "'", "priority")
                                    If dr.Length > 0 Then
                                        StrQ = "UPDATE hrassess_computescore_approval_tran SET isfinal = 1 WHERE calibratnounkid = @calibratnounkid AND tranguid = @tranguid AND isvoid = 0;"

                                        objDataOperation.ClearParameters()
                                        objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId.ToString)
                                        objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr(0)("ctranguid").ToString)

                                        objDataOperation.ExecNonQuery(StrQ)

                                        If objDataOperation.ErrorMessage <> "" Then
                                            objDataOperation.ReleaseTransaction(False)
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    End If
                                End If
                            Next
                            If ds.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("cmappingunkid") <= 0 And x.Field(Of Integer)("calibratnounkid") = intCalibrationId).Count = 0 Then
                                StrQ = "UPDATE hrassess_computescore_approval_tran SET " & _
                                       "    isprocessed = 1 " & _
                                       "WHERE calibratnounkid = @calibratnounkid " & _
                                       "AND isvoid = 0  AND periodunkid = @periodunkid " & _
                                       "UPDATE hrassess_compute_score_master SET " & _
                                       "    isprocessed = 1 " & _
                                       "WHERE calibratnounkid = @calibratnounkid " & _
                                       "AND isvoid = 0  AND periodunkid = @periodunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)

                                objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If
                        End If
                    End If
                    objDataOperation.ReleaseTransaction(True)
                End Using
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ApproveRejectBatch; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function CalibrateData(ByVal xPeriodId As Integer, _
                                  ByVal xDatabaseName As String, _
                                  ByVal xUserUnkid As Integer, _
                                  ByVal xYearUnkid As Integer, _
                                  ByVal xCompanyUnkid As Integer, _
                                  ByVal xPeriodStart As DateTime, _
                                  ByVal xPeriodEnd As DateTime, _
                                  ByVal strIPAddress As String, _
                                  ByVal strHostName As String, _
                                  ByVal strScreenName As String, _
                                  ByVal blnIsWeb As Boolean, _
                                  ByVal blnIsReviewerNeeded As Boolean, _
                                  ByVal strFilter As String) As DataTable
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim objDataoperation As New clsDataOperation
        Dim intCalibrationId As Integer = 0
        Try
            objDataoperation.BindTransaction()
            Dim intMatchValue As Integer = 0
            If blnIsReviewerNeeded Then
                intMatchValue = 3
            Else
                intMatchValue = 2
            End If

            'StrQ = "IF EXISTS(SELECT 1 " & _
            '       "    FROM hrscore_calibration_approver_master AS CAM " & _
            '       "        JOIN hrscore_calibration_approver_tran AS CAT ON CAM.mappingunkid = CAT.mappingunkid " & _
            '       "        JOIN hrassess_compute_score_master AS CSM ON CAT.employeeunkid = CSM.employeeunkid " & _
            '       "        JOIN " & _
            '       "        ( " & _
            '       "            SELECT " & _
            '       "                 employeeunkid " & _
            '       "                ,COUNT(1) AS iAll " & _
            '       "            FROM hrassess_compute_score_master " & _
            '       "            WHERE isvoid = 0 AND periodunkid = @periodunkid AND issubmitted = 0 " & _
            '       "            GROUP BY employeeunkid HAVING COUNT(1) = '" & intMatchValue & "' " & _
            '       "        ) AS D ON D.employeeunkid = CAT.employeeunkid " & _
            '       "    WHERE CAM.isvoid = 0 AND CAM.visibletypeid = 1 AND CAT.isvoid = 0 AND CAT.visibletypeid = 1 AND CAM.isactive = 1 " & _
            '       "    AND CAM.mapuserunkid = @mapuserunkid AND CSM.calibratnounkid <= 0 AND CSM.isvoid = 0 AND CSM.periodunkid  = @periodunkid AND CAM.iscalibrator = 1) " & _
            '       "BEGIN "

            StrQ = "IF EXISTS(SELECT 1 " & _
                   "    FROM hrscore_calibration_approver_master AS CAM WITH (NOLOCK) " & _
                   "        JOIN hrscore_calibration_approver_tran AS CAT WITH (NOLOCK) ON CAM.mappingunkid = CAT.mappingunkid " & _
                   "        JOIN hrassess_compute_score_master AS CSM WITH (NOLOCK) ON CAT.employeeunkid = CSM.employeeunkid "
            StrQ &= " JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            employeeunkid " & _
                    "       FROM hrassess_compute_score_master WITH (NOLOCK) " & _
                    "       WHERE isvoid = 0 AND periodunkid = @periodunkid AND calibratnounkid <= 0 AND issubmitted = 0 " & _
                    "       AND finaloverallscore > 0 AND assessmodeid = 1 AND isnotified = 1 " & _
                    "       INTERSECT " & _
                    "       SELECT " & _
                    "            employeeunkid " & _
                    "       FROM hrassess_compute_score_master WITH (NOLOCK) " & _
                    "       WHERE isvoid = 0 AND periodunkid = @periodunkid AND calibratnounkid <= 0 AND issubmitted = 0 " & _
                    "       AND finaloverallscore > 0 AND assessmodeid = 2 AND isnotified = 1 "
            If blnIsReviewerNeeded Then
                StrQ &= "       INTERSECT " & _
                        "       SELECT " & _
                        "            employeeunkid " & _
                        "       FROM hrassess_compute_score_master WITH (NOLOCK) " & _
                        "       WHERE isvoid = 0 AND periodunkid = @periodunkid AND calibratnounkid <= 0 AND issubmitted = 0 " & _
                        "       AND finaloverallscore > 0 AND assessmodeid = 3 AND isnotified = 1 "
            End If
            StrQ &= "        ) AS D ON D.employeeunkid = CAT.employeeunkid " & _
                   "    WHERE CAM.isvoid = 0 AND CAM.visibletypeid = 1 AND CAT.isvoid = 0 AND CAT.visibletypeid = 1 AND CAM.isactive = 1 " & _
                   "    AND CAM.mapuserunkid = @mapuserunkid AND CSM.calibratnounkid <= 0 AND CSM.isvoid = 0 AND CSM.periodunkid  = @periodunkid AND CAM.iscalibrator = 1) " & _
                   "BEGIN "

            'StrQ &= "DECLARE @NoType AS INT " & _
            '        "DECLARE @Count AS INT, @CPrifix AS NVARCHAR(MAX), @NxtNo AS INT, @NewNo AS NVARCHAR(MAX) " & _
            '        "SET @NoType = ISNULL((SELECT CAST(key_value AS INT) FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @companyunkid AND UPPER([key_name]) ='SCORECALIBRATIONFORMNOTYPE'),0) " & _
            '        "IF (@NoType = 1) " & _
            '             "BEGIN " & _
            '                  "SET @Count = 1 " & _
            '                  "WHILE (@Count > 0) " & _
            '                  "BEGIN " & _
            '                       "SET @CPrifix = '';SET @NxtNo = 0 " & _
            '                       "SET @CPrifix = ISNULL((SELECT key_value FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @companyunkid AND UPPER([key_name]) ='SCORECALIBRATIONFORMNOPRIFIX'),'') " & _
            '                       "SET @NxtNo = ISNULL((SELECT CAST(key_value AS INT) FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @companyunkid AND UPPER([key_name]) ='NEXTSCORECALIBRATIONFORMNO'),0) " & _
            '                       "SET @NewNo = @CPrifix+CAST(@NxtNo AS NVARCHAR(MAX)) " & _
            '                       "IF EXISTS(SELECT 1 FROM hrassess_calibration_number WHERE calibration_no = @NewNo) " & _
            '                       "BEGIN " & _
            '                            "UPDATE hrmsConfiguration..cfconfiguration SET key_value = @NxtNo+1 WHERE companyunkid = @companyunkid AND UPPER([key_name]) = 'NEXTSCORECALIBRATIONFORMNO' " & _
            '                            "CONTINUE " & _
            '                       "END " & _
            '                       "ELSE " & _
            '                       "BEGIN " & _
            '                            "INSERT INTO hrassess_calibration_number (calibration_no,calibuserunkid) VALUES(@NewNo,'" & xUserUnkid & "') " & _
            '                            "UPDATE hrmsConfiguration..cfconfiguration SET key_value = @NxtNo+1 WHERE companyunkid = @companyunkid AND UPPER([key_name]) = 'NEXTSCORECALIBRATIONFORMNO' " & _
            '                            "SET @Count = 0 " & _
            '                       "END " & _
            '                       "IF (@Count = 0) " & _
            '                            "BREAK " & _
            '                       "ELSE " & _
            '                            "CONTINUE " & _
            '                  "END " & _
            '             "END " & _
            '        "ELSE " & _
            '             "BEGIN " & _
            '                  "SET @Count = 1 " & _
            '                  "WHILE (@Count > 0) " & _
            '                  "BEGIN " & _
            '                       "SET @NewNo = '' " & _
            '                       "SET @NewNo = (SELECT CONVERT(NVARCHAR(MAX),GETDATE(),112)+REPLACE(CONVERT(NVARCHAR(MAX),GETDATE(),14),':','')) " & _
            '                       "IF EXISTS(SELECT 1 FROM hrassess_calibration_number WHERE calibration_no = @NewNo) " & _
            '                            "BEGIN " & _
            '                                 "CONTINUE " & _
            '                            "END " & _
            '                       "ELSE " & _
            '                            "BEGIN " & _
            '                                 "INSERT INTO hrassess_calibration_number (calibration_no,calibuserunkid) VALUES(@NewNo,'" & xUserUnkid & "') " & _
            '                                 "SET @Count = 0 " & _
            '                            "END " & _
            '                       "IF (@Count = 0) " & _
            '                            "BREAK " & _
            '                       "ELSE " & _
            '                            "CONTINUE " & _
            '                  "END " & _
            '             "END " & _
            '        "SELECT TOP 1 @CbId = calibratnounkid FROM hrassess_calibration_number WHERE calibration_no = @NewNo " & _
            '        "INSERT INTO hrassess_computescore_approval_tran " & _
            '        "( " & _
            '         "tranguid " & _
            '        ",calibratnounkid " & _
            '        ",transactiondate " & _
            '        ",mappingunkid " & _
            '        ",approvalremark " & _
            '        ",calibration_remark " & _
            '        ",isfinal " & _
            '        ",statusunkid " & _
            '        ",employeeunkid " & _
            '        ",periodunkid " & _
            '        ",calibrated_score " & _
            '        ",loginemployeeunkid " & _
            '        ",isvoid " & _
            '        ",voidreason " & _
            '        ",auditdatetime " & _
            '        ",audittype " & _
            '        ",audituserunkid " & _
            '        ",ip " & _
            '        ",host " & _
            '        ",form_name " & _
            '        ",isweb " & _
            '        ",isprocessed " & _
            '        ") " & _
            '        " " & _
            '        "SELECT " & _
            '             " LOWER(NEWID()) AS tranguid " & _
            '             ",(SELECT TOP 1 calibratnounkid FROM hrassess_calibration_number WHERE calibration_no = @NewNo) as calibratnounkid " & _
            '             ",(select getdate())transactiondate " & _
            '             ",0 AS mappingunkid " & _
            '             ",'' as approvalremark " & _
            '             ",'' as calibration_remark " & _
            '             ",0 as isfinal " & _
            '             ",0 as statusunkid " & _
            '             ",A.employeeunkid " & _
            '             ",A.periodunkid " & _
            '             ",A.calibrated_score as calibrated_score " & _
            '             ",0 as loginemployeeunkid " & _
            '             ",0 as isvoid " & _
            '             ",'' as voidreason " & _
            '             ",(select getdate()) as auditdatetime " & _
            '             ",1 as audittype " & _
            '             ",@mapuserunkid " & _
            '             ",@ip as ip " & _
            '             ",@host as host " & _
            '             ",@form_name as form_name " & _
            '             ",@isweb as isweb " & _
            '             ",0 as isprocessed " & _
            '        "FROM " & _
            '        "( " & _
            '             "SELECT DISTINCT " & _
            '                   "CAST(0 AS BIT) AS iCheck " & _
            '                  ",CAST(CSM.finaloverallscore AS DECIMAL(36,2)) AS Provision_Score " & _
            '                  ",CAST(ISNULL(CSM.calibrated_score,CSM.finaloverallscore) AS DECIMAL(36,2)) AS calibrated_score " & _
            '                  ",CSM.employeeunkid " & _
            '                  ",CSM.periodunkid " & _
            '                  ",CAST(0 AS BIT) AS isChanged " & _
            '                  ",ISNULL(hrassess_calibration_number.calibration_no,'') AS calibration_no " & _
            '                  ",CSM.calibration_remark " & _
            '                  ",ISNULL(CSM.calibratnounkid,0) AS calibratnounkid " & _
            '                  ",CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
            '                   "ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), '') END  AS provRating " & _
            '                  ",'' AS calibRating " & _
            '                  ",CAM.mappingunkid " & _
            '             "FROM hrassess_compute_score_master AS CSM " & _
            '             "JOIN hrscore_calibration_approver_tran AS CAT ON CAT.employeeunkid = CSM.employeeunkid AND CAT.isvoid = 0 AND CAT.visibletypeid = 1 " & _
            '             "JOIN hrscore_calibration_approver_master AS CAM ON CAM.mappingunkid = CAT.mappingunkid AND CAM.isvoid = 0 AND CAM.visibletypeid = 1 AND CAM.isactive = 1 AND CAM.iscalibrator = 1 " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "CSM.computescoremasterunkid " & _
            '                       ",DENSE_RANK()OVER(PARTITION BY CSM.employeeunkid,CSM.periodunkid ORDER BY CSM.finaloverallscore DESC) AS rno " & _
            '                  "FROM hrassess_compute_score_master AS CSM " & _
            '                  "WHERE CSM.issubmitted = 0 AND CSM.periodunkid = @periodunkid " & _
            '                  "AND CSM.isvoid = 0 " & _
            '             ") AS CL ON CL.computescoremasterunkid = CSM.computescoremasterunkid AND CL.rno = 1 " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "hrassess_compute_score_master.employeeunkid " & _
            '                       ",COUNT(1) AS iAll " & _
            '                  "FROM hrassess_compute_score_master " & _
            '                  "JOIN hrscore_calibration_approver_tran AS CT ON CT.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
            '                  "JOIN hrscore_calibration_approver_master AS CM ON  CM.mappingunkid = CT.mappingunkid " & _
            '                  "WHERE hrassess_compute_score_master.isvoid = 0 " & _
            '                  "AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            '                  "AND hrassess_compute_score_master.issubmitted = 0 AND CM.mapuserunkid = @mapuserunkid " & _
            '                  "AND CM.visibletypeid = 1 AND CT.visibletypeid = 1 AND CM.isvoid = 0 AND CT.isvoid = 0 " & _
            '                  "AND CM.isactive = 1 AND CM.iscalibrator = 1 " & _
            '                  "GROUP BY hrassess_compute_score_master.employeeunkid " & _
            '                  "HAVING COUNT(1) = '" & intMatchValue & "' " & _
            '             ") AS D ON D.employeeunkid = CAT.employeeunkid " & _
            '             "LEFT JOIN hrassess_calibration_number ON hrassess_calibration_number.calibratnounkid = CSM.calibratnounkid " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "periodunkid " & _
            '                       ",CASE WHEN selfemployeeunkid <=0 THEN assessedemployeeunkid ELSE selfemployeeunkid END AS EmpId " & _
            '                  "FROM hrevaluation_analysis_master " & _
            '                  "WHERE isvoid = 0 AND periodunkid = @periodunkid AND iscommitted = 1 " & _
            '             ") AS EV ON EV.periodunkid = CSM.periodunkid AND CSM.employeeunkid = EV.EmpId " & _
            '             "JOIN hremployee_master ON CSM.employeeunkid = hremployee_master.employeeunkid AND CSM.isvoid = 0 " & _
            '             "AND CAM.iscalibrator = 1 AND CAM.mapuserunkid = @mapuserunkid " & _
            '        ") AS A " & _
            '        "WHERE NOT EXISTS(SELECT * FROM hrassess_computescore_approval_tran WHERE A.employeeunkid = hrassess_computescore_approval_tran.employeeunkid AND A.periodunkid = hrassess_computescore_approval_tran.periodunkid AND hrassess_computescore_approval_tran.isvoid = 0) "

            StrQ &= "DECLARE @NoType AS INT " & _
                    "DECLARE @Count AS INT, @CPrifix AS NVARCHAR(MAX), @NxtNo AS INT, @NewNo AS NVARCHAR(MAX) " & _
                    "SET @NoType = ISNULL((SELECT CAST(key_value AS INT) FROM hrmsConfiguration..cfconfiguration WITH (NOLOCK) WHERE companyunkid = @companyunkid AND UPPER([key_name]) ='SCORECALIBRATIONFORMNOTYPE'),0) " & _
                    "IF (@NoType = 1) " & _
                         "BEGIN " & _
                              "SET @Count = 1 " & _
                              "WHILE (@Count > 0) " & _
                              "BEGIN " & _
                                   "SET @CPrifix = '';SET @NxtNo = 0 " & _
                                   "SET @CPrifix = ISNULL((SELECT key_value FROM hrmsConfiguration..cfconfiguration WITH (NOLOCK) WHERE companyunkid = @companyunkid AND UPPER([key_name]) ='SCORECALIBRATIONFORMNOPRIFIX'),'') " & _
                                   "SET @NxtNo = ISNULL((SELECT CAST(key_value AS INT) FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @companyunkid AND UPPER([key_name]) ='NEXTSCORECALIBRATIONFORMNO'),0) " & _
                                   "SET @NewNo = @CPrifix+CAST(@NxtNo AS NVARCHAR(MAX)) " & _
                                   "IF EXISTS(SELECT 1 FROM hrassess_calibration_number WITH (NOLOCK) WHERE calibration_no = @NewNo) " & _
                                   "BEGIN " & _
                                        "UPDATE hrmsConfiguration..cfconfiguration SET key_value = @NxtNo+1 WHERE companyunkid = @companyunkid AND UPPER([key_name]) = 'NEXTSCORECALIBRATIONFORMNO' " & _
                                        "CONTINUE " & _
                                   "END " & _
                                   "ELSE " & _
                                   "BEGIN " & _
                                        "INSERT INTO hrassess_calibration_number (calibration_no,calibuserunkid) VALUES(@NewNo,'" & xUserUnkid & "') " & _
                                        "UPDATE hrmsConfiguration..cfconfiguration SET key_value = @NxtNo+1 WHERE companyunkid = @companyunkid AND UPPER([key_name]) = 'NEXTSCORECALIBRATIONFORMNO' " & _
                                        "SET @Count = 0 " & _
                                   "END " & _
                                   "IF (@Count = 0) " & _
                                        "BREAK " & _
                                   "ELSE " & _
                                        "CONTINUE " & _
                              "END " & _
                         "END " & _
                    "ELSE " & _
                         "BEGIN " & _
                              "SET @Count = 1 " & _
                              "WHILE (@Count > 0) " & _
                              "BEGIN " & _
                                   "SET @NewNo = '' " & _
                                   "SET @NewNo = (SELECT CONVERT(NVARCHAR(MAX),GETDATE(),112)+REPLACE(CONVERT(NVARCHAR(MAX),GETDATE(),14),':','')) " & _
                                   "IF EXISTS(SELECT 1 FROM hrassess_calibration_number WITH (NOLOCK) WHERE calibration_no = @NewNo) " & _
                                        "BEGIN " & _
                                             "CONTINUE " & _
                                        "END " & _
                                   "ELSE " & _
                                        "BEGIN " & _
                                             "INSERT INTO hrassess_calibration_number (calibration_no,calibuserunkid) VALUES(@NewNo,'" & xUserUnkid & "') " & _
                                             "SET @Count = 0 " & _
                                        "END " & _
                                   "IF (@Count = 0) " & _
                                        "BREAK " & _
                                   "ELSE " & _
                                        "CONTINUE " & _
                              "END " & _
                         "END " & _
                    "SELECT TOP 1 @CbId = calibratnounkid FROM hrassess_calibration_number WITH (NOLOCK) WHERE calibration_no = @NewNo " & _
                    "INSERT INTO hrassess_computescore_approval_tran " & _
                    "( " & _
                     "tranguid " & _
                    ",calibratnounkid " & _
                    ",transactiondate " & _
                    ",mappingunkid " & _
                    ",approvalremark " & _
                    ",calibration_remark " & _
                    ",isfinal " & _
                    ",statusunkid " & _
                    ",employeeunkid " & _
                    ",periodunkid " & _
                    ",calibrated_score " & _
                    ",loginemployeeunkid " & _
                    ",isvoid " & _
                    ",voidreason " & _
                    ",auditdatetime " & _
                    ",audittype " & _
                    ",audituserunkid " & _
                    ",ip " & _
                    ",host " & _
                    ",form_name " & _
                    ",isweb " & _
                    ",isprocessed " & _
                    ") " & _
                    " " & _
                    "SELECT " & _
                         " LOWER(NEWID()) AS tranguid " & _
                         ",(SELECT TOP 1 calibratnounkid FROM hrassess_calibration_number WHERE calibration_no = @NewNo) as calibratnounkid " & _
                         ",(select getdate())transactiondate " & _
                         ",0 AS mappingunkid " & _
                         ",'' as approvalremark " & _
                         ",'' as calibration_remark " & _
                         ",0 as isfinal " & _
                         ",0 as statusunkid " & _
                         ",A.employeeunkid " & _
                         ",A.periodunkid " & _
                         ",A.calibrated_score as calibrated_score " & _
                         ",0 as loginemployeeunkid " & _
                         ",0 as isvoid " & _
                         ",'' as voidreason " & _
                         ",(select getdate()) as auditdatetime " & _
                         ",1 as audittype " & _
                         ",@mapuserunkid " & _
                         ",@ip as ip " & _
                         ",@host as host " & _
                         ",@form_name as form_name " & _
                         ",@isweb as isweb " & _
                         ",0 as isprocessed " & _
                    "FROM " & _
                    "( " & _
                         "SELECT DISTINCT " & _
                               "CAST(0 AS BIT) AS iCheck " & _
                              ",CAST(CSM.finaloverallscore AS DECIMAL(36,2)) AS Provision_Score " & _
                              ",CAST(ISNULL(CSM.calibrated_score,CSM.finaloverallscore) AS DECIMAL(36,2)) AS calibrated_score " & _
                              ",CSM.employeeunkid " & _
                              ",CSM.periodunkid " & _
                              ",CAST(0 AS BIT) AS isChanged " & _
                              ",ISNULL(hrassess_calibration_number.calibration_no,'') AS calibration_no " & _
                              ",CSM.calibration_remark " & _
                              ",ISNULL(CSM.calibratnounkid,0) AS calibratnounkid " & _
                              ",CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                               "ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), '') END  AS provRating " & _
                              ",'' AS calibRating " & _
                              ",CAM.mappingunkid " & _
                         "FROM hrassess_compute_score_master AS CSM WITH (NOLOCK) " & _
                         "JOIN hrscore_calibration_approver_tran AS CAT WITH (NOLOCK) ON CAT.employeeunkid = CSM.employeeunkid AND CAT.isvoid = 0 AND CAT.visibletypeid = 1 " & _
                         "JOIN hrscore_calibration_approver_master AS CAM WITH (NOLOCK) ON CAM.mappingunkid = CAT.mappingunkid AND CAM.isvoid = 0 AND CAM.visibletypeid = 1 AND CAM.isactive = 1 AND CAM.iscalibrator = 1 " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "CSM.computescoremasterunkid " & _
                                   ",DENSE_RANK()OVER(PARTITION BY CSM.employeeunkid,CSM.periodunkid ORDER BY CSM.finaloverallscore DESC) AS rno " & _
                              "FROM hrassess_compute_score_master AS CSM WITH (NOLOCK) " & _
                              "WHERE CSM.issubmitted = 0 AND CSM.periodunkid = @periodunkid " & _
                              "AND CSM.isvoid = 0 " & _
                         ") AS CL ON CL.computescoremasterunkid = CSM.computescoremasterunkid AND CL.rno = 1 " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT " & _
                              " hrassess_compute_score_master.employeeunkid " & _
                              "FROM hrassess_compute_score_master WITH (NOLOCK) " & _
                              "JOIN hrscore_calibration_approver_tran AS CT WITH (NOLOCK) ON CT.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
                              "JOIN hrscore_calibration_approver_master AS CM WITH (NOLOCK) ON  CM.mappingunkid = CT.mappingunkid " & _
                              "WHERE hrassess_compute_score_master.isvoid = 0 " & _
                              "AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
                              "AND hrassess_compute_score_master.issubmitted = 0 AND CM.mapuserunkid = @mapuserunkid " & _
                              "AND CM.visibletypeid = 1 AND CT.visibletypeid = 1 AND CM.isvoid = 0 AND CT.isvoid = 0 " & _
                              "AND CM.isactive = 1 AND CM.iscalibrator = 1  AND finaloverallscore > 0 AND assessmodeid = 1 AND isnotified = 1 " & _
                              "INTERSECT " & _
                              "SELECT " & _
                              " hrassess_compute_score_master.employeeunkid " & _
                              "FROM hrassess_compute_score_master " & _
                              "JOIN hrscore_calibration_approver_tran AS CT WITH (NOLOCK) ON CT.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
                              "JOIN hrscore_calibration_approver_master AS CM WITH (NOLOCK) ON  CM.mappingunkid = CT.mappingunkid " & _
                              "WHERE hrassess_compute_score_master.isvoid = 0 " & _
                              "AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
                              "AND hrassess_compute_score_master.issubmitted = 0 AND CM.mapuserunkid = @mapuserunkid " & _
                              "AND CM.visibletypeid = 1 AND CT.visibletypeid = 1 AND CM.isvoid = 0 AND CT.isvoid = 0 " & _
                              "AND CM.isactive = 1 AND CM.iscalibrator = 1  AND finaloverallscore > 0 AND assessmodeid = 2 AND isnotified = 1 "
            If blnIsReviewerNeeded Then
                StrQ &= "INTERSECT " & _
                        "SELECT " & _
                        "   hrassess_compute_score_master.employeeunkid " & _
                        "FROM hrassess_compute_score_master WITH (NOLOCK) " & _
                        "JOIN hrscore_calibration_approver_tran AS CT WITH (NOLOCK) ON CT.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
                        "JOIN hrscore_calibration_approver_master AS CM WITH (NOLOCK) ON  CM.mappingunkid = CT.mappingunkid " & _
                        "WHERE hrassess_compute_score_master.isvoid = 0 " & _
                        "AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
                        "AND hrassess_compute_score_master.issubmitted = 0 AND CM.mapuserunkid = @mapuserunkid " & _
                        "AND CM.visibletypeid = 1 AND CT.visibletypeid = 1 AND CM.isvoid = 0 AND CT.isvoid = 0 " & _
                        "AND CM.isactive = 1 AND CM.iscalibrator = 1  AND finaloverallscore > 0 AND assessmodeid = 3 AND isnotified = 1 "
            End If
            StrQ &= ") AS D ON D.employeeunkid = CAT.employeeunkid " & _
                         "LEFT JOIN hrassess_calibration_number WITH (NOLOCK) ON hrassess_calibration_number.calibratnounkid = CSM.calibratnounkid " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "periodunkid " & _
                                   ",CASE WHEN selfemployeeunkid <=0 THEN assessedemployeeunkid ELSE selfemployeeunkid END AS EmpId " & _
                              "FROM hrevaluation_analysis_master WITH (NOLOCK) " & _
                              "WHERE isvoid = 0 AND periodunkid = @periodunkid AND iscommitted = 1 " & _
                         ") AS EV ON EV.periodunkid = CSM.periodunkid AND CSM.employeeunkid = EV.EmpId " & _
                         "JOIN hremployee_master ON CSM.employeeunkid = hremployee_master.employeeunkid AND CSM.isvoid = 0 " & _
                         "AND CAM.iscalibrator = 1 AND CAM.mapuserunkid = @mapuserunkid " & _
                    ") AS A " & _
                    "WHERE NOT EXISTS(SELECT * FROM hrassess_computescore_approval_tran WHERE A.employeeunkid = hrassess_computescore_approval_tran.employeeunkid AND A.periodunkid = hrassess_computescore_approval_tran.periodunkid AND hrassess_computescore_approval_tran.isvoid = 0) "

            StrQ &= "UPDATE hrassess_compute_score_master SET " & _
                    "calibratnounkid = C.calibratnounkid " & _
                    "FROM " & _
                    "( " & _
                    "   SELECT " & _
                    "        CST.calibratnounkid " & _
                    "       ,CST.employeeunkid " & _
                    "       ,CST.periodunkid " & _
                    "       ,CSM.computescoremasterunkid " & _
                    "   FROM hrassess_computescore_approval_tran AS CST WITH (NOLOCK)" & _
                    "       JOIN hrassess_compute_score_master AS CSM WITH (NOLOCK) ON CSM.employeeunkid = CST.employeeunkid AND CSM.periodunkid = CST.periodunkid " & _
                    "   WHERE CST.calibratnounkid = @CbId AND CST.periodunkid = @periodunkid AND CSM.isvoid = 0 AND CST.isvoid = 0 AND CST.statusunkid IN (0,1) " & _
                    ") AS C WHERE C.computescoremasterunkid = hrassess_compute_score_master.computescoremasterunkid AND hrassess_compute_score_master.isvoid = 0 " & _
                    "AND hrassess_compute_score_master.calibratnounkid <= 0 "

            StrQ &= " END "

            objDataoperation.AddParameter("@CbId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId, ParameterDirection.InputOutput)
            objDataoperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIPAddress)
            objDataoperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHostName)
            objDataoperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strScreenName)
            objDataoperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsWeb)
            objDataoperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
            objDataoperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyUnkid)
            objDataoperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)

            objDataoperation.ExecNonQuery(StrQ)

            If objDataoperation.ErrorMessage <> "" Then
                objDataoperation.ReleaseTransaction(False)
                exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                Throw exForce
            End If
            objDataoperation.ReleaseTransaction(True)
            intCalibrationId = CInt(objDataoperation.GetParameterValue("@CbId"))

            StrQ = "SELECT DISTINCT " & _
                    "    CAST(0 AS BIT) AS iCheck " & _
                    "   ,EM.employeecode AS Code " & _
                    "   ,EM.firstname + ' ' + EM.surname AS EmployeeName " & _
                    "   ,EJOB.job_name AS JobTitle " & _
                    "   ,CAST(CSM.finaloverallscore AS DECIMAL(36,2)) AS Provision_Score " & _
                    "   ,CAST(ISNULL(CST.calibrated_score,0) AS DECIMAL(36,2)) AS calibrated_score " & _
                    "   ,CSM.employeeunkid " & _
                    "   ,CSM.periodunkid " & _
                    "   ,CAST(0 AS BIT) AS isChanged " & _
                    "   ,ISNULL(CBN.calibration_no,'') AS calibration_no " & _
                    "   ,CSM.calibration_remark " & _
                    "   ,ISNULL(CSM.calibratnounkid,0) AS calibratnounkid " & _
                    "   ,CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                    "    ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), '') END  AS provRating " & _
                    "   ,CASE WHEN CST.calibrated_score > 0 THEN CASE WHEN CST.calibrated_score > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                    "    ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND CST.calibrated_score >= score_from AND CST.calibrated_score <= score_to), '') END " & _
                    "    ELSE '' END AS calibRating " & _
                    "   ,CST.tranguid " & _
                    "   ,ISNULL(EAP.username,'') AS username " & _
                    "   ,ISNULL(EAP.email,'') AS email " & _
                    "   ,CASE WHEN ISNULL(EAP.mapuserunkid,0) <= 0 THEN @ANA WHEN ISNULL(EAP.email,'') = '' THEN @ENA ELSE '' END AS LMsg " & _
                    "   ,CASE WHEN ISNULL(EAP.mapuserunkid,0) <= 0 THEN 1 WHEN ISNULL(EAP.email,'') = '' THEN 2 ELSE 0 END AS MsgId " & _
                    "FROM hrassess_computescore_approval_tran AS CST WITH (NOLOCK) " & _
                    "   JOIN hrassess_calibration_number AS CBN WITH (NOLOCK) ON CBN.calibratnounkid = CST.calibratnounkid " & _
                    "   JOIN hremployee_master AS EM WITH (NOLOCK) ON CST.employeeunkid = EM.employeeunkid " & _
                    "   JOIN hrscore_calibration_approver_tran AS CT WITH (NOLOCK) ON CT.employeeunkid = EM.employeeunkid AND CT.visibletypeid = 1 AND CT.isvoid = 0 " & _
                    "   JOIN hrscore_calibration_approver_master AS CAM WITH (NOLOCK) ON CAM.mappingunkid = CT.mappingunkid " & _
                    "   JOIN hrassess_compute_score_master AS CSM WITH (NOLOCK) ON CSM.employeeunkid = CST.employeeunkid AND CSM.periodunkid = CST.periodunkid AND CBN.calibratnounkid = CSM.calibratnounkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            CT.employeeunkid " & _
                    "           ,CT.jobunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS xno " & _
                    "       FROM hremployee_categorization_tran AS CT WITH (NOLOCK) " & _
                    "       WHERE CT.isvoid = 0 AND CONVERT(NVARCHAR(8),CT.effectivedate,112) <= @Date " & _
                    "   ) AS JB ON JB.employeeunkid = EM.employeeunkid AND JB.xno = 1 " & _
                    "   JOIN hrjob_master AS EJOB WITH (NOLOCK) ON JB.jobunkid = EJOB.jobunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           * " & _
                    "       FROM " & _
                    "       ( " & _
                    "           SELECT " & _
                    "                CT.employeeunkid " & _
                    "               ,AC.mapuserunkid " & _
                    "               ,AL.levelname " & _
                    "               ,CASE WHEN UM.firstname+' '+UM.lastname <> ' ' THEN UM.firstname + ' ' + UM.lastname ELSE UM.username END AS username " & _
                    "               ,UM.email " & _
                    "               ,AL.priority " & _
                    "               ,DENSE_RANK()OVER(PARTITION BY CT.employeeunkid ORDER BY AL.priority ASC) AS rno " & _
                    "           FROM hrscore_calibration_approver_master AS AC WITH (NOLOCK) " & _
                    "               JOIN hrmsConfiguration..cfuser_master AS UM WITH (NOLOCK) ON AC.mapuserunkid = UM.userunkid " & _
                    "               JOIN hrscore_calibration_approverlevel_master AS AL WITH (NOLOCK) ON AC.levelunkid = AL.levelunkid " & _
                    "               JOIN hrscore_calibration_approver_tran AS CT WITH (NOLOCK) ON AC.mappingunkid = CT.mappingunkid " & _
                    "           WHERE AC.iscalibrator = 0 AND AC.isvoid = 0 AND CT.isvoid = 0 " & _
                    "               AND AC.visibletypeid = 1 AND CT.visibletypeid = 1 " & _
                    "               AND CT.employeeunkid IN (SELECT IT.employeeunkid " & _
                    "                                    FROM hrscore_calibration_approver_master AS IC WITH (NOLOCK) " & _
                    "                                       JOIN hrscore_calibration_approver_tran AS IT WITH (NOLOCK) ON IC.mappingunkid = IT.mappingunkid " & _
                    "                                    WHERE IC.isvoid = 0 AND IT.isvoid = 0 AND IC.mapuserunkid = @mapuserunkid AND IC.iscalibrator = 1 " & _
                    "                                       AND IC.visibletypeid = 1 AND IT.visibletypeid = 1) " & _
                    "       ) AS AP " & _
                    "       WHERE AP.rno = 1 " & _
                    "   ) AS EAP ON EAP.employeeunkid = EM.employeeunkid " & _
                    "WHERE CSM.isvoid = 0 AND CST.statusunkid IN (0,1) AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND CAM.mapuserunkid = @mapuserunkid " & _
                    "AND CAM.isvoid = 0 AND CAM.visibletypeid = 1 " & _
                    "AND CSM.issubmitted = 0 "

            If intCalibrationId > 0 Then
                StrQ &= " AND CST.calibratnounkid = @calibratnounkid "
            End If

            If strFilter.Trim.Length > 0 Then
                StrQ &= " AND " & strFilter
            End If

            StrQ &= " ORDER BY CAST(ISNULL(CST.calibrated_score,0) AS DECIMAL(36,2)) "

            objDataoperation.ClearParameters()
            objDataoperation.AddParameter("@ANA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 218, "Approver Not Assigned"))
            objDataoperation.AddParameter("@ENA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 219, "Email Address Not Assigned"))
            objDataoperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId)
            objDataoperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
            objDataoperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            objDataoperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)

            dsList = objDataoperation.ExecQuery(StrQ, "List")

            If objDataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function UpdateCalibrateRating(ByVal intPeriodUnkid As Integer, ByVal strGuids() As String, ByVal strRemark As String, ByVal intRatingId As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim blnFlag As Boolean = False
        Try
            If strGuids.Length > 0 Then

                Using objDataOperation As New clsDataOperation
                    objDataOperation.BindTransaction()

                    Dim mDecScore As Decimal = 0
                    If intRatingId > 0 Then
                        Dim decScrF, decScrT As Decimal
                        decScrF = 0 : decScrT = 0

                        Dim ds As New DataSet
                        StrQ = "SELECT score_from, score_to FROM hrapps_ratings WITH (NOLOCK) WHERE isvoid = 0 AND appratingunkid = @ratingid "

                        objDataOperation.AddParameter("@ratingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRatingId)

                        ds = objDataOperation.ExecQuery(StrQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If


                        If ds.Tables(0).Rows.Count > 0 Then
                            decScrF = CDec(ds.Tables(0).Rows(0)("score_from"))
                            decScrT = CDec(ds.Tables(0).Rows(0)("score_to"))
                        End If

                        Dim rnd As New Random
                        mDecScore = CDec(rnd.NextDouble() * (decScrT - decScrF) + decScrF)
                        If mDecScore <= 0 Then mDecScore = decScrT
                    End If

                    For Each tguid As String In strGuids
                        objDataOperation.ClearParameters()
                        If intRatingId > 0 Then
                            StrQ = "UPDATE hrassess_computescore_approval_tran SET calibrated_score = @score, calibration_remark = @remark WHERE tranguid = @guid AND periodunkid = @periodunkid "

                            objDataOperation.AddParameter("@score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecScore)
                            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, strRemark.Length, strRemark)
                            objDataOperation.AddParameter("@guid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, tguid)
                            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkid)

                            objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                objDataOperation.ReleaseTransaction(False)
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            StrQ = "UPDATE hrassess_compute_score_master " & _
                                   "    SET calibrated_score = @score,calibration_remark = @remark " & _
                                   "FROM " & _
                                   "( " & _
                                   "    SELECT " & _
                                   "        employeeunkid,periodunkid,calibratnounkid " & _
                                   "    FROM hrassess_computescore_approval_tran WITH (NOLOCK) " & _
                                   "    WHERE tranguid = @guid AND isvoid = 0 AND statusunkid IN (0,1) " & _
                                   ") AS A WHERE A.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
                                   "AND A.periodunkid = hrassess_compute_score_master.periodunkid " & _
                                   "AND A.calibratnounkid = hrassess_compute_score_master.calibratnounkid "

                            objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                objDataOperation.ReleaseTransaction(False)
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Else
                            StrQ = "UPDATE hrassess_computescore_approval_tran " & _
                                   "SET calibrated_score = A.finaloverallscore,calibration_remark = @remark " & _
                                   "FROM " & _
                                   "( " & _
                                   "    SELECT DISTINCT " & _
                                   "         CAT.tranguid " & _
                                   "        ,CSM.finaloverallscore " & _
                                   "    FROM hrassess_computescore_approval_tran AS CAT WITH (NOLOCK) " & _
                                   "        JOIN hrassess_compute_score_master AS CSM WITH (NOLOCK) ON CAT.calibratnounkid = CSM.calibratnounkid AND CAT.employeeunkid = CSM.employeeunkid AND CAT.periodunkid = CSM.periodunkid " & _
                                   "    WHERE CAT.isvoid = 0 AND CAT.isvoid = 0 AND CAT.tranguid = @guid AND CAT.periodunkid = @periodunkid " & _
                                   ") AS A WHERE A.tranguid = hrassess_computescore_approval_tran.tranguid AND hrassess_computescore_approval_tran.periodunkid = @periodunkid "

                            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, strRemark.Length, strRemark)
                            objDataOperation.AddParameter("@guid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, tguid)
                            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkid)

                            objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                objDataOperation.ReleaseTransaction(False)
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            StrQ = "UPDATE hrassess_compute_score_master " & _
                                   "    SET calibrated_score = A.calibrated_score,calibration_remark = @remark " & _
                                   "FROM " & _
                                   "( " & _
                                   "    SELECT " & _
                                   "        employeeunkid,periodunkid,calibratnounkid,calibrated_score " & _
                                   "    FROM hrassess_computescore_approval_tran WITH (NOLOCK) " & _
                                   "    WHERE tranguid = @guid AND isvoid = 0 AND statusunkid IN (0,1) " & _
                                   ") AS A WHERE A.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
                                   "AND A.periodunkid = hrassess_compute_score_master.periodunkid " & _
                                   "AND A.calibratnounkid = hrassess_compute_score_master.calibratnounkid "

                            objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                objDataOperation.ReleaseTransaction(False)
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If
                    Next
                    objDataOperation.ReleaseTransaction(True)
                End Using
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function SubmitCalibration(ByVal intCompanyId As Integer, _
                                      ByVal intPeriodId As Integer, _
                                      ByVal intCalibratorId As Integer, _
                                      ByVal strGuids() As String, _
                                      ByVal strCalibrationNo As String, _
                                      ByVal strPeriodName As String, _
                                      ByVal strOverallRemark As String, _
                                      ByVal strFormName As String, _
                                      ByVal strSenderAddress As String, _
                                      ByVal strIPAddress As String, _
                                      ByVal strHostName As String) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim intCalibrationId As Integer = 0
        Dim xCalibrationNo As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objCSM As New clsComputeScore_master
        Try
            If strGuids.Length > 0 Then
                Using objDataOperation As New clsDataOperation
                    objDataOperation.BindTransaction()

                    StrQ = "SELECT @cabid = calibratnounkid FROM hrassess_computescore_approval_tran WITH (NOLOCK) WHERE tranguid = @guid AND isvoid = 0 AND statusunkid IN (0,1) AND periodunkid = @periodunkid "

                    objDataOperation.AddParameter("@cabid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId, ParameterDirection.InputOutput)
                    objDataOperation.AddParameter("@guid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strGuids(0).ToString)
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    intCalibrationId = CInt(objDataOperation.GetParameterValue("@cabid"))

                    xCalibrationNo = objCSM.GetCalibrationNo(intCalibrationId, objDataOperation)

                    objDataOperation.ClearParameters()
                    If xCalibrationNo <> strCalibrationNo Then
                        StrQ = "UPDATE hrassess_calibration_number SET calibration_no = @calibration_no WHERE calibratnounkid = @calibratnounkid "

                        objDataOperation.AddParameter("@calibration_no", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strCalibrationNo)
                        objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId)

                        objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    objDataOperation.ClearParameters()

                    StrQ = "SELECT DISTINCT " & _
                           "     ISNULL(EAP.username,'') AS username " & _
                           "    ,ISNULL(EAP.email,'') AS email " & _
                           "FROM hrassess_computescore_approval_tran AS CST WITH (NOLOCK) " & _
                           "JOIN hrscore_calibration_approver_tran AS CAT WITH (NOLOCK) ON CAT.employeeunkid = CST.employeeunkid " & _
                           "JOIN hrscore_calibration_approver_master AS CAM WITH (NOLOCK) ON CAM.mappingunkid = CAT.mappingunkid " & _
                           "JOIN " & _
                           "( " & _
                           "   SELECT " & _
                           "       * " & _
                           "   FROM " & _
                           "   ( " & _
                           "       SELECT " & _
                           "            CT.employeeunkid " & _
                           "           ,AC.mapuserunkid " & _
                           "           ,AL.levelname " & _
                           "           ,CASE WHEN UM.firstname+' '+UM.lastname <> ' ' THEN UM.firstname + ' ' + UM.lastname ELSE UM.username END AS username " & _
                           "           ,UM.email " & _
                           "           ,AL.priority " & _
                           "           ,DENSE_RANK()OVER(PARTITION BY CT.employeeunkid ORDER BY AL.priority ASC) AS rno " & _
                           "       FROM hrscore_calibration_approver_master AS AC WITH (NOLOCK) " & _
                           "           JOIN hrmsConfiguration..cfuser_master AS UM WITH (NOLOCK) ON AC.mapuserunkid = UM.userunkid " & _
                           "           JOIN hrscore_calibration_approverlevel_master AS AL WITH (NOLOCK) ON AC.levelunkid = AL.levelunkid " & _
                           "           JOIN hrscore_calibration_approver_tran AS CT WITH (NOLOCK) ON AC.mappingunkid = CT.mappingunkid " & _
                           "       WHERE AC.iscalibrator = 0 AND AC.isvoid = 0 AND CT.isvoid = 0 " & _
                           "           AND AC.visibletypeid = 1 AND CT.visibletypeid = 1 " & _
                           "           AND CT.employeeunkid IN (SELECT IT.employeeunkid " & _
                           "                                    FROM hrscore_calibration_approver_master AS IC WITH (NOLOCK) " & _
                           "                                       JOIN hrscore_calibration_approver_tran AS IT WITH (NOLOCK) ON IC.mappingunkid = IT.mappingunkid " & _
                           "                                    WHERE IC.isvoid = 0 AND IT.isvoid = 0 AND IC.mapuserunkid = @mapuserunkid AND IC.iscalibrator = 1 " & _
                           "                                       AND IC.visibletypeid = 1 AND IT.visibletypeid = 1) " & _
                           "   ) AS AP " & _
                           "   WHERE AP.rno = 1 " & _
                           ") AS EAP ON EAP.employeeunkid = CST.employeeunkid " & _
                           "WHERE CST.statusunkid IN (0,1) AND CST.isvoid = 0 AND CST.periodunkid = @periodunkid /*AND CAM.mapuserunkid = @mapuserunkid*/ " & _
                           "AND CAM.isvoid = 0 AND CAM.visibletypeid = 1 AND CAM.iscalibrator = 0 " & _
                           "AND CST.calibratnounkid = @calibratnounkid "

                    objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId)
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                    objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibratorId)

                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    For Each iRow As DataRow In dsList.Tables(0).Rows

                        Dim StrMessage As New System.Text.StringBuilder

                        StrMessage.Append("<HTML><BODY>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 203, "Dear") & " " & "<b>" & getTitleCase(iRow("username").ToString()) & "</b></span></p>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 210, "This is to inform you that calibration has been submitted for the period of") & " (" & "<b>" & strPeriodName & "</b>" & "). " & Language.getMessage(mstrModuleName, 205, "Please login to aruti to approve/reject them.") & " </span></p>")
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("</span></b></p>")
                        StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                        StrMessage.Append("</span></p>")
                        StrMessage.Append("</BODY></HTML>")

                        Dim objSendMail As New clsSendMail
                        objSendMail._ToEmail = iRow("email").ToString().Trim()
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 209, "Notification for Submit for Calibration :") & " " & strCalibrationNo
                        objSendMail._Message = StrMessage.ToString()
                        objSendMail._Form_Name = strFormName
                        objSendMail._LogEmployeeUnkid = -1
                        objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
                        objSendMail._UserUnkid = intCalibratorId
                        objSendMail._SenderAddress = strSenderAddress
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        objSendMail.SendMail(intCompanyId)
                        objSendMail = Nothing
                    Next

                    objDataOperation.ClearParameters()
                    StrQ = "UPDATE hrassess_compute_score_master " & _
                           "SET issubmitted = 1 " & _
                           " ,submit_date = (SELECT GETDATE()) " & _
                           " ,overall_remark = @overall_remark " & _
                           "WHERE calibratnounkid = @calibratnounkid; " & _
                           "UPDATE hrassess_computescore_approval_tran SET " & _
                           "statusunkid = 1 " & _
                           "WHERE mappingunkid = 0 AND calibratnounkid = @calibratnounkid "

                    objDataOperation.AddParameter("@overall_remark", SqlDbType.NVarChar, strOverallRemark.Length, strOverallRemark)
                    objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId)

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objDataOperation.ReleaseTransaction(True)
                End Using
                Return True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            objCSM = Nothing
        End Try
    End Function

    'Public Function UpdateCalibrateRating(ByVal strTranGuid As String, ByVal mdecScore As Decimal, ByVal strRemark As String, Optional ByVal intRatingId As Integer = 0) As Boolean
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception = Nothing
    '    Dim blnFlag As Boolean = False
    '    Try
    '        Using objDataOperation As New clsDataOperation
    '            objDataOperation.BindTransaction()

    '            If intRatingId > 0 Then
    '                Dim decScrF, decScrT As Decimal
    '                decScrF = 0 : decScrT = 0
    '                Dim ds As New DataSet
    '                StrQ = "SELECT score_from, score_to FROM hrapps_ratings WHERE isvoid = 0 AND appratingunkid = @ratingid "

    '                objDataOperation.AddParameter("@ratingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRatingId)

    '                ds = objDataOperation.ExecQuery(StrQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If


    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    decScrF = CDec(ds.Tables(0).Rows(0)("score_from"))
    '                    decScrT = CDec(ds.Tables(0).Rows(0)("score_to"))
    '                End If

    '                Dim rnd As New Random
    '                mdecScore = CDec(rnd.NextDouble() * (decScrT - decScrF) + decScrF)
    '                If mdecScore <= 0 Then mdecScore = decScrT
    '            End If

    '            objDataOperation.ClearParameters()
    '            StrQ = "UPDATE hrassess_computescore_approval_tran SET calibrated_score = @score, calibration_remark = @remark WHERE tranguid = @guid "

    '            objDataOperation.AddParameter("@score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecScore)
    '            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, strRemark.Length, strRemark)
    '            objDataOperation.AddParameter("@guid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strTranGuid)

    '            objDataOperation.ExecNonQuery(StrQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                objDataOperation.ReleaseTransaction(False)
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            StrQ = "UPDATE hrassess_compute_score_master " & _
    '                   "    SET calibrated_score = @score,calibration_remark = @remark " & _
    '                   "FROM " & _
    '                   "( " & _
    '                   "    SELECT " & _
    '                   "        employeeunkid,periodunkid,calibratnounkid " & _
    '                   "    FROM hrassess_computescore_approval_tran " & _
    '                   "    WHERE tranguid = @guid AND isvoid = 0 AND statusunkid IN (0,1) " & _
    '                   ") AS A WHERE A.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
    '                   "AND A.periodunkid = hrassess_compute_score_master.periodunkid " & _
    '                   "AND A.calibratnounkid = hrassess_compute_score_master.calibratnounkid "

    '            objDataOperation.ExecNonQuery(StrQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                objDataOperation.ReleaseTransaction(False)
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            objDataOperation.ReleaseTransaction(True)
    '        End Using
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: UpdateCalibrateRating; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return blnFlag
    'End Function

    Public Function GetComboListCalibrationNo(ByVal strList As String, ByVal intPeriodId As Integer, ByVal intCalibUsrId As Integer, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION ALL "
            End If

            StrQ &= "SELECT CBN.calibratnounkid AS Id, CBN.calibration_no AS Name " & _
                    "FROM hrassess_calibration_number AS CBN " & _
                    "WHERE CBN.calibratnounkid IN (SELECT DISTINCT CAT.calibratnounkid FROM hrassess_computescore_approval_tran AS CAT WHERE CAT.isvoid = 0 AND CAT.periodunkid = @periodunkid) " & _
                    "AND CBN.calibuserunkid = @calibuserunkid "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Select"))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@calibuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibUsrId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList = GetComboListCalibrationNo(strList, intPeriodId, True)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboListCalibrationNo; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetEmployeeApprovalDataNew(ByVal intPeriodUnkid As Integer _
                                             , ByVal strDatabaseName As String _
                                             , ByVal intCompanyId As Integer _
                                             , ByVal intYearId As Integer _
                                             , ByVal strUserAccessMode As String _
                                             , ByVal intPrivilegeId As Integer _
                                             , ByVal xEmployeeAsOnDate As String _
                                             , ByVal intUserId As Integer _
                                             , ByVal blnOnlyMyApproval As Boolean _
                                             , ByVal blnIsCalibrator As Boolean _
                                             , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                             , Optional ByVal intCurrentPriorityId As Integer = 0 _
                                             , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
                                             , Optional ByVal mstrFilterString As String = "" _
                                             , Optional ByVal strEmployeeIds As String = "" _
                                             , Optional ByVal blnMyReport As Boolean = False _
                                             , Optional ByVal blnGroupByCalibrator As Boolean = False _
                                             , Optional ByVal strAdvanceFilter As String = "" _
                                             , Optional ByVal intStatusId As Integer = 0 _
                                             , Optional ByVal blnApplyAccessFilter As Boolean = False) As DataTable

        Dim StrQ As String = String.Empty
        Dim dList As DataTable = Nothing
        Dim oData As DataTable = Nothing
        If intStatusId <= 0 Then intStatusId = 1
        Dim mstrStatusIds As String = ""
        Try
            dList = GetNextEmployeeApproversNew(intPeriodUnkid, strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, intUserId, False, blnIsCalibrator, xDataOpr, intCurrentPriorityId, mblnIsSubmitForApproaval, mstrFilterString, strEmployeeIds, , , strAdvanceFilter, intStatusId, blnApplyAccessFilter)

            If dList IsNot Nothing Then
                Dim iCol As DataColumn
                iCol = New DataColumn
                With iCol
                    .DataType = GetType(System.Boolean)
                    .ColumnName = "ispgrp"
                    .DefaultValue = False
                End With
                dList.Columns.Add(iCol)

                iCol = New DataColumn
                With iCol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "pgrpid"
                    .DefaultValue = 0
                End With
                dList.Columns.Add(iCol)

                iCol = New DataColumn
                With iCol
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                    .ColumnName = "total"
                End With
                dList.Columns.Add(iCol)

            End If

            If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                Dim strMyEmpIds As String = ""
                strMyEmpIds = String.Join(",", dList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = intUserId).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToArray())
                If strMyEmpIds.Trim.Length <= 0 Then strMyEmpIds = "0"
                If blnOnlyMyApproval Then

                    dList = New DataView(dList, "employeeunkid IN (" & strMyEmpIds & ")", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable

                    Dim strcalibUnkids As String = String.Empty
                    Dim iCalibId As List(Of Int64) = dList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isprocessed") = False).Select(Function(x) x.Field(Of Int64)("grpid")).Distinct().ToList()

                    If iCalibId IsNot Nothing AndAlso iCalibId.Count > 0 Then
                        iCalibId.Sort()
                        For Each ivalue In iCalibId
                            Dim intLowerPriority As Integer = -1
                            If IsDBNull(dList.Compute("MAX(priority)", "grpid = '" & ivalue & "' AND priority < " & intCurrentPriorityId)) = False Then
                                intLowerPriority = CInt(dList.Compute("MAX(priority)", "grpid = '" & ivalue & "' AND priority < " & intCurrentPriorityId))
                                If intLowerPriority <> -1 Then
                                    Dim itmp() As DataRow = dList.Select("iStatusId = " & CInt(enCalibrationStatus.Approved) & " AND priority = '" & intLowerPriority & "' AND grpid = '" & ivalue & "'")
                                    If itmp IsNot Nothing AndAlso itmp.Length > 0 Then
                                        strcalibUnkids &= "," & ivalue.ToString()
                                    End If
                                End If
                            Else
                                intLowerPriority = intCurrentPriorityId
                                Dim itmp() As DataRow = dList.Select("priority = '" & intLowerPriority & "' AND grpid = '" & ivalue & "'")
                                If itmp IsNot Nothing AndAlso itmp.Length > 0 Then
                                    strcalibUnkids &= "," & ivalue.ToString()
                                    If IsDBNull(itmp(0)("iStatusId")) = False Then
                                        mstrStatusIds &= "," & itmp(0)("iStatusId").ToString()
                                    End If                                    
                                End If
                            End If
                        Next
                        If strcalibUnkids.Trim.Length > 0 Then strcalibUnkids = Mid(strcalibUnkids, 2)
                        If strcalibUnkids.Trim.Length <= 0 Then strcalibUnkids = "-1"

                        If mstrStatusIds.Trim.Length > 0 Then mstrStatusIds = Mid(mstrStatusIds, 2)
                        If mstrStatusIds.Trim.Length <= 0 Then mstrStatusIds = "0"
                        mstrStatusIds = mstrStatusIds & "," & intStatusId.ToString()

                        'oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = " & intStatusId & " AND grpid IN (" & strcalibUnkids & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                        oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId IN (" & mstrStatusIds & ") AND grpid IN (" & strcalibUnkids & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                        'S.SANDEEP |27-JUN-2020| -- START
                        'ISSUE/ENHANCEMENT : NMB ENHANCEMENT
                    Else
                        oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = " & intStatusId & " AND 1 = 2 ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                        'S.SANDEEP |27-JUN-2020| -- END
                    End If
                Else
                    If blnMyReport Then
                        Dim drs() As DataRow = Nothing
                        If intCurrentPriorityId <= 0 Then
                            intCurrentPriorityId = dList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = intUserId).Select(Function(x) x.Field(Of Integer)("priority")).FirstOrDefault()
                        End If
                        drs = dList.Select("priority <= '" & intCurrentPriorityId & "' AND employeeunkid IN (" & strMyEmpIds & ")", "employeeunkid, priority")
                        If drs.Length > 0 Then
                            oData = drs.CopyToDataTable()
                        Else
                            oData = dList.Clone()
                        End If
                    Else
                        oData = dList.Copy()
                    End If
                End If
            Else
                oData = dList.Copy()
            End If

            '''''''''''''' ADDING GROUP & SORTING
            If oData.Rows.Count > 0 Then
                Dim iGroup As Dictionary(Of Int64, String)
                iGroup = oData.AsEnumerable().Select(Function(row) New With {Key .attribute1_name = row.Field(Of Int64)("grpid"), Key .attribute2_name = row.Field(Of String)("calibration_no")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)

                Dim iPriority As List(Of Integer) = dList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                iPriority.Sort()
                Dim intSecondLastPriority As Integer = -1
                If iPriority.Min() = intCurrentPriorityId Then
                    intSecondLastPriority = intCurrentPriorityId
                Else
                    Try
                        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intCurrentPriorityId) - 1)
                    Catch ex As Exception
                        intSecondLastPriority = intCurrentPriorityId
                    End Try
                End If

                If iGroup IsNot Nothing AndAlso iGroup.Keys.Count > 0 Then
                    For Each iKey As Integer In iGroup.Keys
                        Dim strDate As String = String.Empty
                        Dim intFinalStatusId As Integer = 0
                        Dim dt() As DataRow = Nothing
                        If oData.Select("grpid = '" & iKey & "'").Length > 0 Then
                            Dim iMinPrioruty As Integer = CInt(oData.Select("grpid = '" & iKey & "'").CopyToDataTable.Compute("MIN(priority)", ""))
                            dt = oData.Select("grpid = '" & iKey & "' AND priority <= " & iMinPrioruty)
                        Else
                            dt = oData.Select("grpid = '" & iKey & "' AND priority <= " & intSecondLastPriority)
                        End If

                        If dt.Length > 0 Then
                            If dt(0)("submitdate").ToString.Trim.Length > 0 Then strDate = CStr(dt(0)("submitdate"))
                        End If

                        If dt.Length > 0 Then
                            If dt(0)("iStatusId").ToString.Trim.Length > 0 Then intFinalStatusId = CInt(dt(0)("iStatusId"))
                        End If
                        If intFinalStatusId <= 0 Then intFinalStatusId = 1
                        Dim dr As DataRow = oData.NewRow()
                        dr("total") = oData.Select("grpid = '" & iKey & "' AND isgrp = 0").Length
                        dr("iCheck") = False
                        dr("calibration_no") = iGroup(iKey)
                        dr("Employee") = Language.getMessage(mstrModuleName, 1, "Calibration No :") & " " & iGroup(iKey)
                        dr("DATE") = ""
                        dr("povisionscore") = "0.00"
                        dr("calibratescore") = "0.00"
                        dr("transactiondate") = DBNull.Value
                        dr("mappingunkid") = 0
                        dr("iStatus") = ""
                        dr("iStatusId") = intFinalStatusId
                        dr("userunkid") = "0"
                        dr("username") = ""
                        dr("approver_email") = ""
                        dr("priority") = intCurrentPriorityId
                        dr("employeeunkid") = 0
                        dr("levelname") = ""
                        dr("oemployee") = ""
                        dr("employee_name") = ""
                        dr("periodunkid") = intPeriodUnkid
                        dr("statusunkid") = 0
                        dr("tranguid") = ""
                        dr("calibratnounkid") = iKey
                        dr("mappingunkid") = 0
                        dr("approvalremark") = ""
                        dr("isfinal") = False
                        dr("isprocessed") = 0
                        dr("isgrp") = 1
                        dr("grpid") = iKey
                        dr("rno") = 0
                        dr("calibration_remark") = ""
                        dr("submitdate") = strDate
                        dr("c_remark") = ""
                        dr("o_remark") = ""
                        If dt.Length > 0 Then
                            dr("allocation") = dt(0)("allocation").ToString
                            dr("calibuser") = dt(0)("calibuser").ToString
                            dr("audituserunkid") = CInt(dt(0)("audituserunkid"))
                        Else
                            dt = oData.Select("grpid = '" & iKey & "'")
                            If dt.Length > 0 Then
                                dr("audituserunkid") = CInt(dt(0)("audituserunkid"))
                            End If
                        End If
                        oData.Rows.Add(dr)
                    Next
                End If
            End If

            If blnGroupByCalibrator = False Then oData = New DataView(oData, "", "grpid,isgrp DESC,employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            If blnGroupByCalibrator = True Then
                Dim iCalibrator As Dictionary(Of Integer, String)
                iCalibrator = oData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isgrp") = False And x.Field(Of Integer)("audituserunkid").ToString().Trim.Length > 0).Select(Function(row) New With {Key .attribute1_name = row.Field(Of Integer)("audituserunkid"), Key .attribute2_name = row.Field(Of String)("calibuser")}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
                If iCalibrator IsNot Nothing AndAlso iCalibrator.Keys.Count > 0 Then
                    If iCalibrator IsNot Nothing AndAlso iCalibrator.Keys.Count > 0 Then
                        For Each iKey As Integer In iCalibrator.Keys
                            Dim strDate As String = String.Empty
                            Dim intFinalStatusId As Integer = 0
                            Dim dt() As DataRow
                            dt = oData.Select("audituserunkid = " & iKey)
                            If dt.Length = 1 Then Continue For
                            If dt.Length > 0 Then
                                If dt(0)("iStatusId").ToString.Trim.Length > 0 Then intFinalStatusId = CInt(dt(0)("iStatusId"))
                                dt.AsEnumerable().ToList().ForEach(Function(x) UpdateParentGrp(x, iKey))
                            End If
                            If intFinalStatusId <= 0 Then intFinalStatusId = 1
                            Dim dr As DataRow = oData.NewRow()
                            dr("total") = oData.Select("grpid = '" & iKey & "' AND isgrp = 0").Length
                            dr("iCheck") = False
                            dr("calibration_no") = dt(0)("calibration_no")
                            dr("Employee") = Language.getMessage(mstrModuleName, 211, "Calibrator :") & " " & iCalibrator(iKey) & _
                            IIf(dt(0)("allocation").ToString.Trim.Length > 0, " - (" & dt(0)("allocation").ToString & ")", "").ToString()
                            dr("DATE") = ""
                            dr("povisionscore") = "0.00"
                            dr("calibratescore") = "0.00"
                            dr("transactiondate") = DBNull.Value
                            dr("mappingunkid") = 0
                            dr("iStatus") = ""
                            dr("iStatusId") = intFinalStatusId
                            dr("userunkid") = "0"
                            dr("username") = ""
                            dr("approver_email") = ""
                            dr("priority") = intCurrentPriorityId
                            dr("employeeunkid") = 0
                            dr("levelname") = ""
                            dr("oemployee") = ""
                            dr("employee_name") = ""
                            dr("periodunkid") = intPeriodUnkid
                            dr("statusunkid") = 0
                            dr("tranguid") = ""
                            dr("calibratnounkid") = CInt(dt(0)("calibratnounkid"))
                            dr("mappingunkid") = 0
                            dr("approvalremark") = ""
                            dr("isfinal") = False
                            dr("isprocessed") = 0
                            dr("isgrp") = 0
                            dr("grpid") = 0
                            dr("rno") = 0
                            dr("calibration_remark") = ""
                            dr("submitdate") = strDate
                            dr("c_remark") = ""
                            dr("o_remark") = ""
                            dr("ispgrp") = True
                            dr("pgrpid") = iKey
                            If dt.Length > 0 Then
                                dr("allocation") = dt(0)("allocation").ToString
                                dr("calibuser") = dt(0)("calibuser").ToString
                                dr("audituserunkid") = CInt(dt(0)("audituserunkid"))
                            Else
                                dt = oData.Select("grpid = '" & iKey & "'")
                                If dt.Length > 0 Then
                                    dr("audituserunkid") = CInt(dt(0)("audituserunkid"))
                                End If
                            End If
                            oData.Rows.Add(dr)
                        Next
                    End If
                End If
                oData = New DataView(oData, "", "pgrpid,ispgrp DESC, grpid,isgrp DESC,employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovalDataNew; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return oData
    End Function

    Public Function GetNextEmployeeApproversNew(ByVal intPeriodUnkid As Integer _
                                            , ByVal strDatabaseName As String _
                                            , ByVal intCompanyId As Integer _
                                            , ByVal intYearId As Integer _
                                            , ByVal strUserAccessMode As String _
                                            , ByVal intPrivilegeId As Integer _
                                            , ByVal xEmployeeAsOnDate As String _
                                            , ByVal intUserId As Integer _
                                            , ByVal blnOnlyMyApproval As Boolean _
                                            , ByVal blnIsCalibrator As Boolean _
                                            , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                            , Optional ByVal intCurrentPriorityId As Integer = 0 _
                                            , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
                                            , Optional ByVal mstrFilterString As String = "" _
                                            , Optional ByVal strEmployeeIds As String = "" _
                                            , Optional ByVal intCalibrationUnkid As Integer = 0 _
                                            , Optional ByVal blnAddUACFilter As Boolean = False _
                                            , Optional ByVal mstrAdvanceFilter As String = "" _
                                            , Optional ByVal intStatusId As Integer = 0 _
                                            , Optional ByVal blnApplyAccessFilter As Boolean = False) As DataTable
        Dim StrQ As String = String.Empty
        Dim dtList As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        If intStatusId <= 0 Then intStatusId = 1
        Try
            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""

            Dim dsFinal As New DataSet
            Dim strUACJoin, strUACFilter, xAdvanceJoinQry As String
            strUACJoin = "" : strUACFilter = "" : xAdvanceJoinQry = ""

            If blnApplyAccessFilter = True Then modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM")
            modGlobal.GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(xEmployeeAsOnDate), strDatabaseName, "AEM")

            objDataOperation.ExecNonQuery("SET ARITHABORT ON")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                   "DROP TABLE #USR "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( " & _
                    "   SELECT " & _
                    "        CT.employeeunkid " & _
                    "       ,AC.mapuserunkid as userunkid " & _
                    "   FROM hrscore_calibration_approver_master AS AC WITH(NOLOCK) " & _
                    "       JOIN hrmsConfiguration..cfuser_master AS UM WITH(NOLOCK) ON AC.mapuserunkid = UM.userunkid " & _
                    "       JOIN hrscore_calibration_approverlevel_master AS AL WITH(NOLOCK) ON AC.levelunkid = AL.levelunkid " & _
                    "       JOIN hrscore_calibration_approver_tran AS CT WITH(NOLOCK) ON AC.mappingunkid = CT.mappingunkid " & _
                    "   WHERE AC.iscalibrator = 0 AND AC.isvoid = 0 AND CT.isvoid = 0 " & _
                    "       AND AC.visibletypeid = 1 AND CT.visibletypeid = 1 " & _
                    "       AND CT.employeeunkid IN (SELECT DISTINCT IT.employeeunkid " & _
                    "       FROM hrscore_calibration_approver_master AS IC " & _
                    "           JOIN hrscore_calibration_approver_tran AS IT WITH(NOLOCK) ON IC.mappingunkid = IT.mappingunkid " & _
                    "       WHERE IC.isvoid = 0 AND IT.isvoid = 0 " & IIf(blnIsCalibrator = True, "AND IC.mapuserunkid =" & intUserId.ToString, "").ToString & " AND IC.iscalibrator = 1 " & _
                    "       AND IC.visibletypeid = 1 AND IT.visibletypeid = 1) " & _
                    ") AS Fl "

            StrQ &= "DECLARE @emp AS TABLE (empid INT,deptid INT,jobid INT,clsgrpid INT,clsid INT,branchid INT,deptgrpid INT,secgrpid INT,secid INT,unitgrpid INT,unitid INT,teamid INT,jgrpid INT,periodid INT,ecodename nvarchar(max),ename nvarchar(max),calibratnounkid INT,Code nvarchar(255), JobTitle nvarchar(max), eemail nvarchar(max)) " & _
                    "INSERT INTO @emp (empid, deptid, jobid, clsgrpid, clsid, branchid, deptgrpid, secgrpid, secid, unitgrpid, unitid, teamid, jgrpid, periodid, ecodename, ename,calibratnounkid,Code,JobTitle,eemail) " & _
                    "SELECT " & _
                    "     AEM.employeeunkid " & _
                    "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                    "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                    "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                    "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                    "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                    "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                    "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                    "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                    "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                    "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                    "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                    "    ,TAT.periodunkid " & _
                    "    ,AEM.employeecode + ' - ' + AEM.firstname + ' ' + AEM.surname " & _
                    "    ,AEM.firstname + ' ' + AEM.surname " & _
                    "    ,TAT.calibratnounkid " & _
                    "    ,AEM.employeecode " & _
                    "    ,EJOB.job_name AS JobTitle " & _
                    "    ,AEM.email AS eemail " & _
                    "FROM " & strDatabaseName & "..hremployee_master AS AEM WITH(NOLOCK) " & _
                    "    JOIN hrassess_computescore_approval_tran AS TAT WITH(NOLOCK) ON TAT.employeeunkid = AEM.employeeunkid "
            'S.SANDEEP |21-NOV-2019| -- START {eemail} -- END
            If strUACJoin.Trim.Length > 0 AndAlso intUserId > 0 Then
                StrQ &= strUACJoin
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            End If

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        stationunkid " & _
                    "       ,deptgroupunkid " & _
                    "       ,departmentunkid " & _
                    "       ,sectiongroupunkid " & _
                    "       ,sectionunkid " & _
                    "       ,unitgroupunkid " & _
                    "       ,unitunkid " & _
                    "       ,teamunkid " & _
                    "       ,classgroupunkid " & _
                    "       ,classunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_transfer_tran WITH(NOLOCK) " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "
            If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND employeeunkid IN (" & strEmployeeIds & ") "
            StrQ &= ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        jobgroupunkid " & _
                    "       ,jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_categorization_tran WITH(NOLOCK) " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                    ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN hrjob_master AS EJOB ON EJOB.jobunkid = J.jobunkid "
            StrQ &= "WHERE 1 = 1 AND TAT.isvoid = 0 AND TAT.statusunkid IN (0," & intStatusId & ") AND  [TAT].periodunkid = @periodunkid "
            If blnOnlyMyApproval Then
                StrQ &= " AND ISNULL([TAT].[isprocessed],0) = 0 "
            End If
            If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND AEM.employeeunkid IN (" & strEmployeeIds & ") "

            If intCalibrationUnkid > 0 Then
                StrQ &= " AND [TAT].calibratnounkid = '" & intCalibrationUnkid & "' "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            StrQ &= "SELECT DISTINCT " & _
                    "    CAST(0 AS BIT) AS iCheck " & _
                    "   ,iData.calibration_no " & _
                    "   ,iData.Employee " & _
                    "   ,iData.DATE " & _
                    "   ,ISNULL(iData.povisionscore,0) AS povisionscore " & _
                    "   ,ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) AS calibratescore " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[transactiondate], iData.Date) ELSE NULL END AS transactiondate " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[mappingunkid], iData.mappingunkid) ELSE 0 END AS mappingunkid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatus], @Pending) ELSE '' END AS iStatus " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END AS iStatusId " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.userunkid ELSE 0 END AS userunkid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.username ELSE '' END AS username " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.email ELSE '' END AS approver_email " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.priority ELSE 0 END AS priority " & _
                    "   ,[@emp].empid AS employeeunkid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.LvName, '') ELSE '' END AS levelname " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.uempid END AS uempid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN Fn.ecompid END AS ecompid " & _
                    "   ,[@emp].ecodename AS oemployee " & _
                    "   ,[@emp].ename AS employee_name " & _
                    "   ,iData.employeeunkid " & _
                    "   ,iData.periodunkid " & _
                    "   ,iData.statusunkid " & _
                    "   ,iData.tranguid " & _
                    "   ,iData.calibratnounkid " & _
                    "   ,CASE WHEN ISNULL(bapprovalremark,'') = '' THEN iData.approvalremark ELSE ISNULL(bapprovalremark,'') END AS approvalremark " & _
                    "   ,iData.isfinal " & _
                    "   ,iData.isprocessed " & _
                    "   ,iData.isgrp " & _
                    "   ,iData.grpid " & _
                    "   ,CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.periodunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno " & _
                    "   ,[@emp].Code " & _
                    "   ,[@emp].JobTitle " & _
                    "   ,CAST(1 AS BIT) AS isChanged " & _
                    "   ,ISNULL(NV.calibration_remark,ISNULL(iData.calibration_remark,ISNULL(iData.c_remark,''))) AS calibration_remark " & _
                    "   ,iData.submitdate " & _
                    "   ,iData.c_remark " & _
                    "   ,iData.o_remark " & _
                    "   ,iData.s_date " & _
                    "   ,'' AS lstpRating " & _
                    "   ,'' AS lstcRating " & _
                    "   ,ISNULL(acal.acrating,'') AS acrating " & _
                    "   ,ISNULL(acal.acremark,'') AS acremark " & _
                    "   ,ISNULL(B.lstcRating,'') AS lstapRating " & _
                    "   ,ISNULL(bcalibration_remark,'') AS apcalibremark " & _
                    "   ,ISNULL(LL.username,'') AS lusername " & _
                    "   ,(SELECT grade_award FROM hrapps_ratings WITH(NOLOCK) WHERE isvoid = 0 AND ISNULL(LL.cscore,0) >= score_from AND ISNULL(LL.cscore,0) <= score_to) AS lrating " & _
                    "   ,ISNULL(LL.calibration_remark,'') AS lcalibremark " & _
                    "   ,'' AS allocation " & _
                    "   ,audituserunkid " & _
                    "   ,'' AS calibuser " & _
                    "   ,Fn.email " & _
                    "   ,[@emp].JobTitle " & _
                    "   ,[@emp].eemail AS eemail " & _
                    "FROM @emp " & _
                    "   JOIN #USR ON [@emp].empid = #USR.employeeunkid " & _
                    "   JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "                 a.calibration_no " & _
                    "                ,a.Employee " & _
                    "                ,a.DATE " & _
                    "                ,a.povisionscore " & _
                    "                ,a.calibratescore " & _
                    "                ,a.isgrp " & _
                    "                ,a.grpid " & _
                    "                ,a.employeeunkid " & _
                    "                ,a.periodunkid " & _
                    "                ,a.statusunkid " & _
                    "                ,a.tranguid " & _
                    "                ,a.calibratnounkid " & _
                    "                ,a.mappingunkid " & _
                    "                ,a.approvalremark " & _
                    "                ,a.isfinal " & _
                    "                ,a.isprocessed " & _
                    "                ,a.calibration_remark " & _
                    "                ,a.submitdate " & _
                    "                ,a.c_remark " & _
                    "                ,a.o_remark " & _
                    "                ,a.s_date " & _
                    "                ,a.audituserunkid " & _
                    "           FROM " & _
                    "           ( " & _
                    "               SELECT " & _
                    "                    calibration_no " & _
                    "                   ,'/r/n' + employeecode + ' - ' + firstname + ' ' + surname AS Employee " & _
                    "                   ,CONVERT(NVARCHAR(8), transactiondate, 112) AS DATE " & _
                    "                   ,ISNULL(CONVERT(NVARCHAR(8),pScore.submit_date,112),'') AS submitdate " & _
                    "                   ,ISNULL(pScore.c_remark, ISNULL(rScore.c_remark,'')) AS c_remark " & _
                    "                   ,ISNULL(pScore.o_remark, ISNULL(rScore.o_remark,'')) AS o_remark " & _
                    "                   ,CAST(ISNULL(pScore.finaloverallscore,rScore.finaloverallscore) AS DECIMAL(36, 2)) AS povisionscore " & _
                    "                   ,CAST(calibrated_score AS DECIMAL(36, 2)) AS calibratescore " & _
                    "                   ,CAST(0 AS BIT) AS isgrp " & _
                    "                   ,hrassess_calibration_number.calibratnounkid AS grpid " & _
                    "                   ,hrassess_computescore_approval_tran.employeeunkid " & _
                    "                   ,hrassess_computescore_approval_tran.periodunkid " & _
                    "                   ,hrassess_computescore_approval_tran.statusunkid " & _
                    "                   ,hrassess_computescore_approval_tran.tranguid " & _
                    "                   ,hrassess_computescore_approval_tran.calibratnounkid " & _
                    "                   ,hrassess_computescore_approval_tran.mappingunkid " & _
                    "                   ,hrassess_computescore_approval_tran.approvalremark " & _
                    "                   ,hrassess_computescore_approval_tran.isfinal " & _
                    "                   ,hrassess_computescore_approval_tran.isprocessed " & _
                    "                   ,hrassess_computescore_approval_tran.calibration_remark " & _
                    "                   ,pScore.submit_date AS s_date " & _
                    "                   ,hrassess_computescore_approval_tran.audituserunkid " & _
                    "               FROM hrassess_computescore_approval_tran WITH(NOLOCK) " & _
                    "               LEFT JOIN " & _
                    "               ( " & _
                    "                   SELECT DISTINCT " & _
                    "                        hrassess_computescore_rejection_tran.employeeunkid " & _
                    "                       ,hrassess_computescore_rejection_tran.periodunkid " & _
                    "                       ,hrassess_computescore_rejection_tran.calibratnounkid " & _
                   "                       ,hrassess_computescore_rejection_tran.finaloverallscore " & _
                    "                       ,hrassess_computescore_rejection_tran.calibration_remark AS c_remark " & _
                    "                       ,hrassess_computescore_rejection_tran.overall_remark AS o_remark " & _
                    "                   FROM hrassess_computescore_rejection_tran WITH(NOLOCK) " & _
                   "                   WHERE hrassess_computescore_rejection_tran.periodunkid = @periodunkid " & _
                    "               ) AS rScore ON rScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                    "                   AND rScore.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
                    "                   AND rScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                            "               JOIN " & _
                    "               ( " & _
                    "                   SELECT DISTINCT " & _
                    "                        calibratnounkid " & _
                    "                       ,employeeunkid " & _
                    "                       ,periodunkid " & _
                    "                       ,finaloverallscore " & _
                    "                       ,submit_date " & _
                    "                       ,calibration_remark AS c_remark " & _
                    "                       ,overall_remark AS o_remark " & _
                    "                   FROM hrassess_compute_score_master WITH(NOLOCK) " & _
                    "                   WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
                    "               ) AS pScore ON pScore.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                    "               AND pScore.periodunkid = hrassess_computescore_approval_tran.periodunkid AND pScore.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                    "               JOIN hrassess_calibration_number WITH(NOLOCK) ON hrassess_computescore_approval_tran.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
                    "               JOIN hremployee_master WITH(NOLOCK) ON hrassess_computescore_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "           WHERE statusunkid IN (0," & intStatusId & ") AND isvoid = 0 aND hrassess_computescore_approval_tran.periodunkid = @periodunkid " & _
                    "       ) AS A WHERE 1 = 1 AND A.statusunkid IN (0," & intStatusId & ") " & _
                    "   ) AS iData ON iData.employeeunkid = [@emp].empid " & _
                    "    JOIN " & _
                    "    ( " & _
                    "        SELECT DISTINCT " & _
                    "             cfuser_master.userunkid " & _
                    "            ,cfuser_master.username " & _
                    "            ,cfuser_master.email " & _
                                  strSelect & " " & _
                    "            ,LM.priority " & _
                    "            ,LM.levelname AS LvName " & _
                    "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                    "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                    "            ,EM.mapuserunkid " & _
                    "            ,EM.mappingunkid AS oMappingId " & _
                    "        FROM hrmsConfiguration..cfuser_master WITH(NOLOCK) " & _
                    "            JOIN hrmsConfiguration..cfcompanyaccess_privilege WITH(NOLOCK) ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                    "            JOIN hrmsConfiguration..cfuser_privilege WITH(NOLOCK) ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                                 strAccessJoin & " " & _
                    "        JOIN " & strDatabaseName & "..hrscore_calibration_approver_master EM WITH(NOLOCK) ON EM.mapuserunkid = cfuser_master.userunkid AND EM.iscalibrator = 0 " & _
                    "        JOIN " & strDatabaseName & "..hrscore_calibration_approverlevel_master AS LM WITH(NOLOCK) ON EM.levelunkid = LM.levelunkid " & _
                    "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
            'S.SANDEEP |21-NOV-2019| -- START {eemail} -- END
            If strFilter.Trim.Length > 0 Then
                StrQ &= " AND (" & strFilter & " ) "
            End If
            StrQ &= "     AND yearunkid = @Y " & _
                    "     AND privilegeunkid = @P " & _
                    ") AS Fn ON #USR.userunkid = Fn.userunkid /*" & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & "*/ " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        [HA].[mapuserunkid] " & _
                    "       ,[HM].[priority] " & _
                    "       ,[HTAT].[statusunkid] AS iStatusId " & _
                    "       ,[HTAT].[transactiondate] " & _
                    "       ,[HTAT].[mappingunkid] " & _
                    "       ,[HTAT].[periodunkid] AS iPeriodId " & _
                    "       ,[HTAT].calibratnounkid " & _
                    "       ,[HTAT].calibrated_score AS bscore " & _
                    "       ,[HTAT].calibration_remark AS bcalibration_remark " & _
                    "       ,[HTAT].approvalremark AS bapprovalremark " & _
                    "       ,(SELECT grade_award FROM hrapps_ratings WITH(NOLOCK) WHERE isvoid = 0 AND [HTAT].calibrated_score >= score_from AND [HTAT].calibrated_score <= score_to) AS lstcRating " & _
                    "       ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
                    "             WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ ' + CM.[username] + ' ]' " & _
                    "             WHEN [HTAT].[statusunkid] = 3 THEN @Reject + ' : [ ' + CM.[username] + ' ]' " & _
                    "        ELSE @PendingApproval END AS iStatus " & _
                    "       ,DENSE_RANK() OVER (PARTITION BY [HTAT].[employeeunkid],[HTAT].periodunkid ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
                    "       ,HTAT.[employeeunkid] " & _
                    "   FROM hrassess_computescore_approval_tran AS HTAT WITH(NOLOCK) " & _
                    "       JOIN [dbo].[hrscore_calibration_approver_master] AS HA WITH(NOLOCK) ON [HTAT].[mappingunkid] = [HA].[mappingunkid] AND HA.iscalibrator = 0 " & _
                    "       JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM WITH(NOLOCK) ON [HA].[levelunkid] = [HM].[levelunkid] " & _
                    "       LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM WITH(NOLOCK) ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                    "   WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND [HTAT].[isvoid] = 0 " & _
                    "   AND HTAT.periodunkid = @periodunkid "
            If blnOnlyMyApproval Then
                StrQ &= "   AND ISNULL(HTAT.isprocessed, 0) = 0 "
            End If
            StrQ &= ") AS B ON [B].[priority] = [Fn].[priority] AND [B].[iPeriodId] = [iData].[periodunkid] AND [iData].calibratnounkid = [B].calibratnounkid " & _
                    "AND [iData].employeeunkid = [B].employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        X.xPeriodId " & _
                    "       ,X.xPeriodName " & _
                    "       ,X.employeeunkid " & _
                    "       ,X.calibratnounkid " & _
                    "       ,X.cscore " & _
                    "       ,x.approvalremark " & _
                    "       ,x.calibration_remark " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            DENSE_RANK() OVER (PARTITION BY MAT.employeeunkid,MAT.periodunkid ORDER BY MAT.auditdatetime DESC) as xno " & _
                    "           ,CP.periodunkid AS xPeriodId " & _
                    "           ,CP.period_name AS xPeriodName " & _
                    "           ,MAT.employeeunkid " & _
                    "           ,MAT.calibratnounkid " & _
                    "           ,CAST(MAT.calibrated_score AS DECIMAL(36,2)) AS cscore " & _
                    "           ,MAT.approvalremark " & _
                    "           ,MAT.calibration_remark " & _
                    "       FROM hrassess_computescore_approval_tran AS MAT WITH(NOLOCK) " & _
                    "           JOIN cfcommon_period_tran CP WITH(NOLOCK) ON MAT.periodunkid = CP.periodunkid " & _
                    "       WHERE MAT.isvoid = 0 AND MAT.periodunkid = @periodunkid "
            If blnOnlyMyApproval Then
                StrQ &= " AND MAT.isprocessed = 0 "
            End If
            StrQ &= "   ) AS X WHERE X.xno = 1 " & _
                    ") AS NV ON NV.employeeunkid = iData.employeeunkid AND NV.xPeriodId = iData.periodunkid AND  NV.calibratnounkid = iData.calibratnounkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        X.xPeriodId " & _
                    "       ,X.xPeriodName " & _
                    "       ,X.employeeunkid " & _
                    "       ,X.calibratnounkid " & _
                    "       ,X.cscore " & _
                    "       ,x.approvalremark " & _
                    "       ,x.calibration_remark " & _
                    "       ,X.username " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            DENSE_RANK() OVER (PARTITION BY MAT.employeeunkid ,MAT.periodunkid ORDER BY MAT.auditdatetime DESC) AS xno " & _
                    "           ,CP.periodunkid AS xPeriodId " & _
                    "           ,CP.period_name AS xPeriodName " & _
                    "           ,MAT.employeeunkid " & _
                    "           ,MAT.calibratnounkid " & _
                    "           ,MAT.calibration_remark " & _
                    "           ,CAST(MAT.calibrated_score AS DECIMAL(36, 2)) AS cscore " & _
                    "           ,MAT.approvalremark " & _
                    "           ,CM.username " & _
                    "       FROM hrassess_computescore_approval_tran AS MAT WITH(NOLOCK) " & _
                    "           JOIN cfcommon_period_tran CP WITH(NOLOCK) ON MAT.periodunkid = CP.periodunkid " & _
                    "           JOIN [dbo].[hrscore_calibration_approver_master] AS HA WITH(NOLOCK) ON MAT.[mappingunkid] = [HA].[mappingunkid] AND HA.iscalibrator = 0 " & _
                    "           JOIN [dbo].[hrscore_calibration_approverlevel_master] AS HM WITH(NOLOCK) ON [HA].[levelunkid] = [HM].[levelunkid] " & _
                    "           LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM WITH(NOLOCK) ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                    "       WHERE MAT.isvoid = 0 " & _
                    "       AND MAT.periodunkid = @periodunkid " & _
                    "       AND MAT.isprocessed = 0 " & _
                    "       AND HM.priority < " & intCurrentPriorityId & " " & _
                    "   ) AS X " & _
                    "   WHERE X.xno = 1 " & _
                    ") AS LL ON LL.employeeunkid = iData.employeeunkid " & _
                    "   AND LL.xPeriodId = iData.periodunkid " & _
                    "   AND LL.calibratnounkid = iData.calibratnounkid " & _
                    strOuterJoin & " " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        employeeunkid AS aempid " & _
                    "       ,periodunkid AS aprid " & _
                    "       ,calibrated_score As acscore " & _
                    "       ,calibration_remark AS acremark " & _
                    "       ,calibratnounkid as acalibratnounkid " & _
                    "       ,(SELECT grade_award FROM hrapps_ratings WITH(NOLOCK) WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to) AS acrating " & _
                    "   FROM hrassess_computescore_approval_tran WITH(NOLOCK) " & _
                    "   WHERE mappingunkid = 0 AND isvoid = 0 AND periodunkid = @periodunkid " & _
                    ") AS acal ON acal.aempid = iData.employeeunkid AND acal.aprid = iData.periodunkid AND acal.acalibratnounkid = [iData].calibratnounkid " & _
                    "WHERE 1 = 1 AND iData.statusunkid IN (0," & intStatusId & ") AND [iData].periodunkid = @periodunkid "

            'S.SANDEEP |08-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            '-- ADDED {1.,calibratnounkid as acalibratnounkid | 2. AND acal.acalibratnounkid = [iData].calibratnounkid}
            'S.SANDEEP |08-NOV-2019| -- END

            If blnOnlyMyApproval Then
                StrQ &= " AND ISNULL([iData].[isprocessed],0) = 0 "
            End If

            If mblnIsSubmitForApproaval = False Then
                StrQ &= " AND Fn.priority > " & intCurrentPriorityId
            End If

            If mstrFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & mstrFilterString
            End If

            If intCalibrationUnkid > 0 Then
                StrQ &= " AND [iData].calibratnounkid = '" & intCalibrationUnkid & "' "
            End If

            If blnOnlyMyApproval Then
                StrQ &= " AND Fn.userunkid = @U "
                objDataOperation.AddParameter("@U", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            End If

            'StrQ &= "ORDER BY iData.calibratnounkid "

            StrQ &= " OPTION(RECOMPILE); " & _
                    "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                    "DROP TABLE #USR " & _
                    " "

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkid)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")
            Dim tmp As DataTable = New DataView(dsList.Tables(0), "", "calibratnounkid ASC", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(tmp.Copy)
            tmp.Dispose()
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtUsrAllocation As DataTable = GetUserAllocation(intPeriodUnkid, intCompanyId, objDataOperation)
                If dtUsrAllocation IsNot Nothing Then
                    If dtUsrAllocation.Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateRating(x, dtUsrAllocation, True))
                End If
                Dim objRating As New clsAppraisal_Rating
                Dim dsRating As New DataSet
                dsRating = objRating.getComboList("List", False)
                objRating = Nothing
                If dsRating.Tables(0).Rows.Count > 0 Then dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateRating(x, dsRating.Tables(0), False))
            End If

            If dsFinal.Tables.Count <= 0 Then
                dsFinal = dsList
            Else
                dsFinal.Tables(0).Merge(dsList.Tables(0).Copy, True)
            End If

            If dsFinal IsNot Nothing AndAlso dsFinal.Tables.Count > 0 Then
                dtList = dsFinal.Tables(0).Copy()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApprovers; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtList
    End Function
    'S.SANDEEP |01-MAY-2020| -- END

    Private Sub RepairInvalidData(ByVal intPeriod As Integer, ByVal intUser As Integer, ByVal objDataOperation As clsDataOperation, ByVal strDatabase As String)
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            StrQ = "UPDATE " & strDatabase & "..hrassess_computescore_approval_tran " & _
                   "SET audituserunkid = A.mapuserunkid " & _
                   "FROM " & _
                   "( " & _
                   "   SELECT " & _
                   "        CM.mapuserunkid " & _
                   "       ,AP.tranguid " & _
                   "   FROM " & strDatabase & "..hrscore_calibration_approver_master AS CM " & _
                   "       JOIN " & strDatabase & "..hrscore_calibration_approver_tran AS CT ON CM.mappingunkid = CT.mappingunkid " & _
                   "       JOIN " & strDatabase & "..hrassess_computescore_approval_tran AS AP ON AP.employeeunkid = CT.employeeunkid AND AP.periodunkid = 52 " & _
                   "   WHERE CM.mapuserunkid = '" & intUser & "' AND AP.statusunkid <= 1 " & _
                   "       AND CM.isvoid = 0 AND CM.iscalibrator = 1 AND CM.visibletypeid = 1 " & _
                   "       AND CT.isvoid = 0 AND CT.visibletypeid = 1 AND AP.periodunkid = '" & intPeriod & "' " & _
                   "       AND AP.isvoid = 0 " & _
                   "       AND AP.audituserunkid <> CM.mapuserunkid " & _
                   ") AS A WHERE A.tranguid = hrassess_computescore_approval_tran.tranguid " & _
                   "AND isprocessed = 0 AND isvoid = 0 "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            StrQ = "UPDATE " & strDatabase & "..hrassess_compute_score_master SET calibratnounkid = A.calibratnounkid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         hrassess_computescore_approval_tran.employeeunkid " & _
                   "        ,hrassess_computescore_approval_tran.calibrated_score " & _
                   "        ,hrassess_computescore_approval_tran.calibratnounkid " & _
                   "        ,finaloverallscore " & _
                   "        ,statusunkid " & _
                   "        ,hrassess_computescore_approval_tran.periodunkid " & _
                   "    FROM " & strDatabase & "..hrassess_computescore_approval_tran " & _
                   "        LEFT JOIN " & strDatabase & "..hrassess_compute_score_master ON hrassess_compute_score_master.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                   "        AND hrassess_compute_score_master.periodunkid = hrassess_computescore_approval_tran.periodunkid " & _
                   "    WHERE hrassess_compute_score_master.isvoid = 0 AND statusunkid IN (0,1) AND hrassess_computescore_approval_tran.isvoid = 0 " & _
                   "        AND hrassess_computescore_approval_tran.periodunkid = '" & intPeriod & "' " & _
                   "        AND hrassess_compute_score_master.calibratnounkid < = 0 " & _
                   ") AS A " & _
                   "WHERE A.employeeunkid = hrassess_compute_score_master.employeeunkid " & _
                   "    AND A.periodunkid = hrassess_compute_score_master.periodunkid " & _
                   "    AND hrassess_compute_score_master.calibratnounkid <= 0 " & _
                   "    AND isvoid = 0 AND hrassess_compute_score_master.periodunkid = '" & intPeriod & "' "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: RepairInvalidData; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Calibration No :")
            Language.setMessage(mstrModuleName, 2, "Not Submitted")
            Language.setMessage(mstrModuleName, 3, "Submitted")
            Language.setMessage(mstrModuleName, 4, "Approved")
            Language.setMessage(mstrModuleName, 5, "Rejected")
            Language.setMessage(mstrModuleName, 6, "Select")
            Language.setMessage(mstrModuleName, 7, "Sorry, No approver(s) defined for calibration. Please define approver in order to continue.")
            Language.setMessage(mstrModuleName, 8, "Sorry, you cannot send notification to start calibration. Reason, Privilege [Allow to Calibrate Provisional Score] is not assigned to user(s) or the user(s) email is not set into the system.")
            Language.setMessage(mstrModuleName, 9, "Notification to Calibrate Provisional Score.")
            Language.setMessage(mstrModuleName, 10, "Dear")
            Language.setMessage(mstrModuleName, 11, "Please note that, Compute score process is done for the")
            Language.setMessage(mstrModuleName, 12, "period")
            Language.setMessage(mstrModuleName, 13, "Kindly click the link below in order to Calibrate Provisional Score.")
            Language.setMessage(mstrModuleName, 15, "Regards,")
            Language.setMessage(mstrModuleName, 16, "Select")
            Language.setMessage(mstrModuleName, 100, "Pending")
            Language.setMessage(mstrModuleName, 101, "Approved")
            Language.setMessage(mstrModuleName, 102, "Rejected")
            Language.setMessage(mstrModuleName, 103, "Pending for Approval")
            Language.setMessage(mstrModuleName, 200, "Notification for Rejection of Calibration :")
            Language.setMessage(mstrModuleName, 201, "Notification for Approve/Reject of Calibration :")
            Language.setMessage(mstrModuleName, 203, "Dear")
            Language.setMessage(mstrModuleName, 204, "This is to inform you that calibration process done for the period of")
            Language.setMessage(mstrModuleName, 205, "Please login to aruti to approve/reject them.")
            Language.setMessage(mstrModuleName, 206, "Approval Remark :")
            Language.setMessage(mstrModuleName, 207, "This is to inform you that calibration process has been rejected for the period of")
            Language.setMessage(mstrModuleName, 208, "Rejection Remark :")
            Language.setMessage(mstrModuleName, 209, "Notification for Submit for Calibration :")
            Language.setMessage(mstrModuleName, 210, "This is to inform you that calibration has been submitted for the period of")
            Language.setMessage(mstrModuleName, 211, "Calibrator :")
            Language.setMessage(mstrModuleName, 212, "This is to inform you that calibration score process has been completed for the period of")
            Language.setMessage(mstrModuleName, 213, "Notification for Final Calibrated Score")
            Language.setMessage(mstrModuleName, 214, "Please login to Aruti Self Service to view your final score.")
            Language.setMessage(mstrModuleName, 215, "Sorry, No Employee Found with this user. Make sure that you have made this user as calibrator and assigned employee to this user.")
            Language.setMessage(mstrModuleName, 216, "Sorry, Some Employee has not done the assessment or not assessed by assessor. Please review those employee in order to start calibration.")
            Language.setMessage(mstrModuleName, 217, "Sorry, Some Employee has not done the assessment or not assessed by assessor/reviewer. Please review those employee in order to start calibration.")
            Language.setMessage(mstrModuleName, 218, "Approver Not Assigned")
            Language.setMessage(mstrModuleName, 219, "Email Address Not Assigned")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class