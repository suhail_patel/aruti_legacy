﻿Option Strict On
'************************************************************************************************************************************
'Class Name :clscalibrate_approver_master.vb
'Purpose    :
'Date       :28-May-2019
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clscalibrate_approver_master
    Private Shared ReadOnly mstrModuleName As String = "clscalibrate_approver_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change
    Dim objcalibrate_approver_tran As New clscalibrate_approver_tran
    'Gajanan [15-April-2020] -- End


#Region " Private variables "

    Private mintMappingunkid As Integer
    Private mintLevelunkid As Integer
    Private mintMapuserunkid As Integer
    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change
    Private mintuserunkid As Integer = 0
    Private mblnisCalibrator As Boolean = False
    Private mintvisibilityType As Integer = clscalibrate_approver_tran.enCalibrationApproverVisibilityType.VISIBLE
    'Gajanan [15-April-2020] -- End

    Private mblnIsactive As Boolean = True
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change
    Private mintloginemployeeunkid As Integer = 0
	'Gajanan [15-April-2020] -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid(Optional ByVal objOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
            Call GetData(objOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property
    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change


    Public Property _Userunkid() As Integer
        Get
            Return mintuserunkid
        End Get
        Set(ByVal value As Integer)
            mintuserunkid = value
        End Set
    End Property

    Public Property _loginemployeeunkid() As Integer
        Get
            Return mintloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    Public Property _IsCalibrator() As Boolean
        Get
            Return mblnisCalibrator
        End Get
        Set(ByVal value As Boolean)
            mblnisCalibrator = value
        End Set
    End Property

    Public Property _VisibilityType() As Integer
        Get
            Return mintvisibilityType
        End Get
        Set(ByVal value As Integer)
            mintvisibilityType = value
        End Set
    End Property
    'Gajanan [15-April-2020] -- End
#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
    'Gajanan [15-April-2020] -- Add [visibletypeid,iscalibrator]
        Try
            strQ = "SELECT " & _
              "  mappingunkid " & _
              ", levelunkid " & _
              ", mapuserunkid " & _
              ", isactive " & _
              ", visibletypeid " & _
              ", iscalibrator " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrscore_calibration_approver_master " & _
             "WHERE mappingunkid = @mappingunkid "

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                          , Optional ByVal blnOnlyActive As Boolean = True _
                          , Optional ByVal mstrFilter As String = "" _
                          , Optional ByVal intApproverUserUnkId As Integer = 0 _
                            , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                          , Optional ByVal blnAddSelect As Boolean = False _
                          , Optional ByVal blnIsCalibrator As Boolean = False) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If blnAddSelect Then
                strQ = "SELECT " & _
                        "  0 AS mappingunkid " & _
                        " ,0 AS levelunkid " & _
                        " ,'' AS LevelCode " & _
                        " ,'' AS  Level " & _
                        " ,0 AS mapuserunkid " & _
                        " ,@Select AS approver " & _
                        " ,0 AS isactive " & _
                        " ,'' As Status " & _
                        " ,0 AS isvoid " & _
                        " ,0 AS voiduserunkid " & _
                        " ,NULL AS voiddatetime " & _
                        " ,'' AS voidreason " & _
                        " ,-1 AS priority " & _
                        " ,@Select AS dapprover " & _
                        "UNION ALL "
            End If
            strQ &= "SELECT " & _
                    "     TAM.mappingunkid " & _
                    "    ,TAM.levelunkid " & _
                    "    ,ISNULL(TLM.levelcode,'') as LevelCode " & _
                    "    ,ISNULL(TLM.levelname,'') as Level " & _
                    "    ,TAM.mapuserunkid " & _
                    "    ,ISNULL(UM.username,'') AS approver " & _
                    "    ,TAM.isactive " & _
                    "    ,CASE WHEN TAM.isactive = 0 THEN @InActive ELSE @Active END As Status " & _
                    "    ,TAM.isvoid " & _
                    "    ,TAM.voiduserunkid " & _
                    "    ,TAM.voiddatetime " & _
                    "    ,TAM.voidreason " & _
                    "    ,ISNULL(TLM.priority,0) AS priority " & _
                    "    ,TLM.levelname + ' - ' + UM.username AS dapprover " & _
                    "FROM hrscore_calibration_approver_master AS TAM " & _
                    "    LEFT JOIN hrscore_calibration_approverlevel_master AS TLM ON TAM.levelunkid = TLM.levelunkid " & _
                    "    LEFT JOIN hrmsconfiguration..cfuser_master AS UM ON UM.userunkid = TAM.mapuserunkid " & _
                    "WHERE TAM.isvoid = 0 AND visibletypeid = 1 "

            'S.SANDEEP |26-AUG-2019| -- START {'' AS dapprover, TLM.levelname + ' - ' + UM.username AS dapprover} -- END

            If blnOnlyActive Then
                strQ &= " AND TAM.isactive = 1 "
            End If

            If blnIsCalibrator Then
                strQ &= " AND TAM.iscalibrator = 1 "
            Else
                strQ &= " AND TAM.iscalibrator = 0 "
            End If

            If intApproverUserUnkId > 0 Then
                strQ &= " AND TAM.mapuserunkid = @mapuserunkid "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            objDataOperation.AddParameter("@InActive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "In Active"))
            objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Active"))
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    '''' <summary>
    '''' Modify By: Sandeep Sharma
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (hrscore_calibration_approver_master) </purpose>
    '''' 
    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change
	'Public Function Insert() As Boolean
    Public Function Insert(ByVal mdtran As DataTable, ByVal isCalibrator As Boolean) As Boolean
    'Gajanan [15-April-2020] -- End	
        If isExist(mintMapuserunkid, isCalibrator) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This user is already map with another level or same level.please select new user to map with this level.")
            Return False
        End If

        If isCalibrator = False Then
            If mdtran IsNot Nothing Then
                Dim StrEmpIds As String = ""
                StrEmpIds = GetAssignedEmpCSVLevelWise(mintLevelunkid)
                If StrEmpIds.Trim.Length > 0 AndAlso mdtran.Rows.Count > 0 Then
                    Dim dTmp() As DataRow = Nothing
                    dTmp = mdtran.Select("employeeunkid NOT IN(" & StrEmpIds & ")")
                    If dTmp.Length > 0 Then
                        mdtran = dTmp.CopyToDataTable()
                    ElseIf dTmp.Length <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 5, "Selected employee(s) are already mapped on same level with different approver .Please assign new employee(s) to map with this level.")
                        Return False
                    End If
                End If
            End If
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
		    'Gajanan [15-April-2020] -- Start
		    'Enhancement:Worked On Calibration Approver Change
            objDataOperation.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(clscalibrate_approver_tran.enCalibrationApproverVisibilityType.VISIBLE))
            objDataOperation.AddParameter("@iscalibrator", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisCalibrator)
		    'Gajanan [15-April-2020] -- End

            strQ = "INSERT INTO hrscore_calibration_approver_master ( " & _
                   "  levelunkid " & _
                   ", mapuserunkid " & _
                   ", isactive " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason" & _
                   ", visibletypeid" & _
                   ", iscalibrator" & _
                   ") VALUES (" & _
                   "  @levelunkid " & _
                   ", @mapuserunkid " & _
                   ", @isactive " & _
                   ", @isvoid " & _
                   ", @voiduserunkid " & _
                   ", @voiddatetime " & _
                   ", @voidreason" & _
                   ", @visibletypeid" & _
                   ", @iscalibrator" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMappingunkid = CInt(dsList.Tables(0).Rows(0).Item(0))
		    'Gajanan [15-April-2020] -- Start
		    'Enhancement:Worked On Calibration Approver Change

            objcalibrate_approver_tran._Mappingunkid = mintMappingunkid
            objcalibrate_approver_tran._DataList = mdtran
            objcalibrate_approver_tran._Userunkid = mintuserunkid
            objcalibrate_approver_tran._ClientIp = mstrClientIP
            objcalibrate_approver_tran._loginemployeeunkid = mintloginemployeeunkid
            objcalibrate_approver_tran._HostName = mstrHostName
            objcalibrate_approver_tran._FormName = mstrFormName
            objcalibrate_approver_tran._IsFromWeb = mblnIsFromWeb

            If objcalibrate_approver_tran._DataList IsNot Nothing AndAlso objcalibrate_approver_tran._DataList.Rows.Count > 0 Then
                If objcalibrate_approver_tran.Insert_CalibarteApproverData(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
		    'Gajanan [15-April-2020] -- End

            If InsertAuditTrailApproverMapping(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_appusermapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrscore_calibration_approver_master SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   "WHERE mappingunkid = @mappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = intUnkid

            If InsertAuditTrailApproverMapping(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'strQ = "<Query>"
            strQ = "SELECT 1 FROM hrassess_computescore_approval_tran WHERE isvoid = 0 AND mappingunkid = @mappingunkid "
            'S.SANDEEP |27-JUL-2019| -- END

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xMapUserId As Integer, ByVal xIscalibrator As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "  mappingunkid " & _
                   " ,levelunkid " & _
                   " ,mapuserunkid " & _
                   " ,isactive " & _
                   " ,isvoid " & _
                   " ,voiduserunkid " & _
                   " ,voiddatetime " & _
                   " ,voidreason " & _
                   "FROM hrscore_calibration_approver_master " & _
                   "WHERE mapuserunkid = @mapuserunkid " & _
                       " AND iscalibrator = @iscalibrator " & _
                   " AND isvoid = 0 AND visibletypeid = 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMapUserId)

            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            objDataOperation.AddParameter("@iscalibrator", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xIscalibrator)
            'Gajanan [15-April-2020] -- End
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InActiveApprover(ByVal xMappingId As Integer, ByVal intCompanyID As Integer, ByVal blnIsActive As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If blnIsActive Then
                strQ = " UPDATE hrscore_calibration_approver_master set isactive = 1 where mappingunkid = @mappingunkid AND isvoid = 0  "
            Else
                strQ = " UPDATE hrscore_calibration_approver_master set isactive = 0 where mappingunkid = @mappingunkid AND isvoid = 0 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMappingId)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = xMappingId

            If InsertAuditTrailApproverMapping(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InActiveApprover; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function GetEmployeeListByApprover(ByVal xDatabaseName As String, _
                                              ByVal xUserUnkid As Integer, _
                                              ByVal xYearUnkid As Integer, _
                                              ByVal xCompanyUnkid As Integer, _
                                              ByVal xEmpAsOnDate As DateTime, _
                                              ByVal xUserModeSetting As String, _
                                              ByVal xOnlyApproved As Boolean, _
                                              ByVal strListName As String, _
                                              ByVal mblnAddSelect As Boolean, _
                                              Optional ByVal strFilterQuery As String = "", _
                                              Optional ByVal blnReinstatementDate As Boolean = False, _
                                              Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                              Optional ByVal blnAddApprovalCondition As Boolean = True) As DataSet
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            dsList = objEmp.GetEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xEmpAsOnDate, xEmpAsOnDate, xUserModeSetting, xOnlyApproved, False, strListName, mblnAddSelect, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, strFilterQuery, blnReinstatementDate, blnIncludeAccessFilterQry, blnAddApprovalCondition)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeListByApprover; Module Name: " & mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
        Return dsList
    End Function

    Public Function InsertAuditTrailApproverMapping(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO athrscore_calibration_approver_master ( " & _
                   "  tranguid " & _
                   ", mappingunkid " & _
                   ", levelunkid " & _
                   ", mapuserunkid " & _
                   ", isactive " & _
                   ", visibletypeid " & _
                   ", iscalibrator " & _
                   ", audittype " & _
                   ", audituserunkid " & _
                   ", auditdatetime " & _
                   ", ip" & _
                   ", hostname" & _
                   ", form_name " & _
                   ", isweb " & _
                   ") VALUES (" & _
                   "  @tranguid " & _
                   ", @mappingunkid " & _
                   ", @levelunkid " & _
                   ", @mapuserunkid " & _
                   ", @isactive " & _
                   ", @visibletypeid " & _
                   ", @iscalibrator " & _
                   ", @audittype " & _
                   ", @audituserunkid " & _
                   ", @auditdatetime " & _
                   ", @ip" & _
                   ", @hostname" & _
                   ", @form_name " & _
                   ", @isweb " & _
                   ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
		    'Gajanan [15-April-2020] -- Start
		    'Enhancement:Worked On Calibration Approver Change
            objDataOperation.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvisibilityType)
            objDataOperation.AddParameter("@iscalibrator", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisCalibrator)
		    'Gajanan [15-April-2020] -- End
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailApproverMapping; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Public Function GetAssignedEmpCSVLevelWise(ByVal intLevelId As Integer) As String
        Dim StrQ As String = ""
        Dim StrIds As String = ""
        Dim exForce As Exception = Nothing
        Try
            Using objData As New clsDataOperation
                Dim dsList As New DataSet
                StrQ = "SELECT ISNULL(STUFF(( " & _
                       "    SELECT ','+ CAST(CT.employeeunkid AS NVARCHAR(50)) " & _
                       "    FROM hrscore_calibration_approver_master AS CM " & _
                       "        JOIN hrscore_calibration_approver_tran AS CT ON CM.mappingunkid = CT.mappingunkid " & _
                       "    WHERE CM.isvoid = 0 AND CM.visibletypeid = 1 " & _
                       "        AND CM.iscalibrator = 0 AND CM.levelunkid = '" & intLevelId & "' AND CT.isvoid = 0 " & _
                       "        AND CT.visibletypeid = 1 AND CM.isactive = 1 FOR XML PATH('')), 1, 1, ''),'') AS empids "

                dsList = objData.ExecQuery(StrQ, "List")

                If objData.ErrorMessage <> "" Then
                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                    Throw exForce
                End If


                If dsList.Tables(0).Rows.Count > 0 Then
                    StrIds = dsList.Tables(0).Rows(0)("empids").ToString.Trim()
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssignedEmpCSVLevelWise; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrIds
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This user is already map with another level or same level.please select new user to map with this level.")
            Language.setMessage(mstrModuleName, 2, "In Active")
            Language.setMessage(mstrModuleName, 3, "Active")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Selected employee(s) are already mapped on same level with different approver .Please assign new employee(s) to map with this level.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class