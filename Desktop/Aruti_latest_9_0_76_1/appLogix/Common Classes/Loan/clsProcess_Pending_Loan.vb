﻿'************************************************************************************************************************************
'Class Name : clsProcess_pending_loan.vb
'Purpose    :
'Date       :8/5/2010
'Written By :Vimal M. Gohil
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
Imports System

'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Vimal M. Gohil
''' </summary>
Public Class clsProcess_pending_loan
    Private Shared ReadOnly mstrModuleName As String = "clsProcess_pending_loan"
    Dim objDataOperation As clsDataOperation
    Dim objLoanAdvance As clsLoan_Advance
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintProcesspendingloanunkid As Integer
    Private mstrApplication_No As String = String.Empty
    Private mdtApplication_Date As Date
    Private mintEmployeeunkid As Integer
    Private mintLoanschemeunkid As Integer
    Private mdecLoan_Amount As Decimal 'Sohail (11 May 2011)
    Private mintApproverunkid As Integer
    Private mintLoan_Statusunkid As Integer
    Private mdecApproved_Amount As Decimal 'Sohail (11 May 2011)
    Private mblnIsloan As Boolean
    Private mblnIsvoid As Boolean
    Private mintUserunkid As Integer
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrRemark As String = String.Empty
    Private mstrVoidreason As String = String.Empty

    Private mintLoanAdvanceId As Integer
    Private mdecInterest_Amount As Decimal 'Sohail (11 May 2011)
    Private mdecNet_Amount As Decimal 'Sohail (11 May 2011)
    Private mblnIsAdvance As Boolean


    'Sandeep [ 21 Aug 2010 ] -- Start
    Private mblnIsexternal_Entity As Boolean
    Private mstrExternal_Entity_Name As String = String.Empty
    'Sandeep [ 21 Aug 2010 ] -- End 

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    Private mintLoginemployeeunkid As Integer = -1
    Private mintVoidloginemployeeunkid As Integer = -1
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    'S.SANDEEP [ 18 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrEmp_Remark As String = String.Empty
    'S.SANDEEP [ 18 APRIL 2012 ] -- END

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Processpendingloanunkid() As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set application_no
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Application_No() As String
        Get
            Return mstrApplication_No
        End Get
        Set(ByVal value As String)
            mstrApplication_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set application_date
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Application_Date() As Date
        Get
            Return mdtApplication_Date
        End Get
        Set(ByVal value As Date)
            mdtApplication_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_amount
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Loan_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecLoan_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecLoan_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_statusunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Loan_Statusunkid() As Integer
        Get
            Return mintLoan_Statusunkid
        End Get
        Set(ByVal value As Integer)
            mintLoan_Statusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approved_amount
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Approved_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecApproved_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecApproved_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isloan
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Isloan() As Boolean
        Get
            Return mblnIsloan
        End Get
        Set(ByVal value As Boolean)
            mblnIsloan = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property




    Public Property _LoanAdvanceId() As Integer
        Get
            Return mintLoanAdvanceId
        End Get
        Set(ByVal value As Integer)
            mintLoanAdvanceId = value
        End Set
    End Property

    Public Property _Interest_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecInterest_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecInterest_Amount = value
        End Set
    End Property

    Public Property _Net_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecNet_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecNet_Amount = value
        End Set
    End Property

    Public Property _IsAdvance() As Boolean
        Get
            Return mblnIsAdvance
        End Get
        Set(ByVal value As Boolean)
            mblnIsAdvance = value
        End Set
    End Property

    'Sandeep [ 21 Aug 2010 ] -- Start
    ''' <summary>
    ''' Purpose: Get or Set isexternal_entity
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isexternal_Entity() As Boolean
        Get
            Return mblnIsexternal_Entity
        End Get
        Set(ByVal value As Boolean)
            mblnIsexternal_Entity = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set external_entity_name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _External_Entity_Name() As String
        Get
            Return mstrExternal_Entity_Name
        End Get
        Set(ByVal value As String)
            mstrExternal_Entity_Name = Value
        End Set
    End Property
    'Sandeep [ 21 Aug 2010 ] -- End 


    'S.SANDEEP [ 12 OCT 2011 ] -- START
    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    'S.SANDEEP [ 18 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emp_Remark() As String
        Get
            Return mstrEmp_Remark
        End Get
        Set(ByVal value As String)
            mstrEmp_Remark = Value
        End Set
    End Property
    'S.SANDEEP [ 18 APRIL 2012 ] -- END


    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 21 Aug 2010 ] -- Start
            'strQ = "SELECT " & _
            '              "  processpendingloanunkid " & _
            '              ", application_no " & _
            '              ", application_date " & _
            '              ", employeeunkid " & _
            '              ", loanschemeunkid " & _
            '              ", loan_amount " & _
            '              ", approverunkid " & _
            '              ", loan_statusunkid " & _
            '              ", approved_amount " & _
            '              ", remark " & _
            '              ", isloan " & _
            '              ", isvoid " & _
            '              ", userunkid " & _
            '              ", voiduserunkid " & _
            '              ", voiddatetime " & _
            '              ", voidreason " & _
            '             "FROM lnloan_process_pending_loan " & _
            '             "WHERE processpendingloanunkid = @processpendingloanunkid "


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'strQ = "SELECT " & _
            '          "  processpendingloanunkid " & _
            '          ", application_no " & _
            '          ", application_date " & _
            '          ", employeeunkid " & _
            '          ", loanschemeunkid " & _
            '          ", loan_amount " & _
            '          ", approverunkid " & _
            '          ", loan_statusunkid " & _
            '          ", approved_amount " & _
            '          ", remark " & _
            '          ", isloan " & _
            '          ", isvoid " & _
            '          ", userunkid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason " & _
            '          ", ISNULL(isexternal_entity,0) As isexternal_entity " & _
            '          ", ISNULL(external_entity_name,'') As external_entity_name " & _
            ' "FROM lnloan_process_pending_loan " & _
            ' "WHERE processpendingloanunkid = @processpendingloanunkid "

            strQ = "SELECT " & _
              "  processpendingloanunkid " & _
              ", application_no " & _
              ", application_date " & _
              ", employeeunkid " & _
              ", loanschemeunkid " & _
              ", loan_amount " & _
              ", approverunkid " & _
              ", loan_statusunkid " & _
              ", approved_amount " & _
              ", remark " & _
              ", isloan " & _
              ", isvoid " & _
              ", userunkid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                          ", ISNULL(isexternal_entity,0) As isexternal_entity " & _
                          ", ISNULL(external_entity_name,'') As external_entity_name " & _
                      ", ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
                      ", ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
              ", ISNULL(emp_remark,'') AS emp_remark " & _
             "FROM lnloan_process_pending_loan " & _
             "WHERE processpendingloanunkid = @processpendingloanunkid "
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            'S.SANDEEP [ 18 APRIL 2012 emp_remark ] -- START -- END



            'Sandeep [ 21 Aug 2010 ] -- End 

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrApplication_No = dtRow.Item("application_no").ToString
                ' mdtApplication_Date = dtRow.Item("application_date")
                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mdecLoan_Amount = CDec(dtRow.Item("loan_amount")) 'Sohail (11 May 2011)
                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
                mintLoan_Statusunkid = CInt(dtRow.Item("loan_statusunkid"))
                mdecApproved_Amount = CDec(dtRow.Item("approved_amount")) 'Sohail (11 May 2011)
                mstrRemark = dtRow.Item("remark").ToString
                mblnIsloan = CBool(dtRow.Item("isloan"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                If IsDBNull(dtRow.Item("application_date")) Then
                    mdtApplication_Date = Nothing
                Else
                    mdtApplication_Date = dtRow.Item("application_date")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Sandeep [ 21 Aug 2010 ] -- Start
                mblnIsexternal_Entity = CBool(dtRow.Item("isexternal_entity"))
                mstrExternal_Entity_Name = dtRow.Item("external_entity_name").ToString
                'Sandeep [ 21 Aug 2010 ] -- End 

                'S.SANDEEP [ 12 OCT 2011 ] -- START
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                'S.SANDEEP [ 18 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mstrEmp_Remark = dtRow.Item("emp_remark").ToString
                'S.SANDEEP [ 18 APRIL 2012 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intStatusID As Integer = 0 _
                            , Optional ByVal strIncludeInactiveEmployee As String = "" _
                            , Optional ByVal strEmployeeAsOnDate As String = "" _
                            , Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet ', Optional ByVal blnOnlyActive As Boolean = True). [, Optional ByVal intStatusID As Integer = 0 : Sohail (21 Aug 2010)], 'Sohail (23 Apr 2012) - [strIncludeInactiveEmployee,strEmployeeAsOnDate,strUserAccessLevelFilterString]
        'Public Function GetList(ByVal strTableName As String) As DataSet    ', Optional ByVal blnOnlyActive As Boolean = True)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (21 Aug 2010) -- Start
            '***Changes:Set LoanScheme = '@Advance' when isloan = 0, Filter for loan_statusunkid added.
            'strQ = "SELECT " & _
            '  " lnloan_process_pending_loan.processpendingloanunkid " & _
            '  ",lnloan_process_pending_loan.application_no As Application_No " & _
            '  ",convert(char(8),application_date,112) As application_date " & _
            '  ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '  ", lnloan_scheme_master.name AS LoanScheme " & _
            '  ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
            '  ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
            '  ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending " & _
            '  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved " & _
            '  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected " & _
            '  " END As LoanStatus" & _
            '  ",lnloan_process_pending_loan.loan_statusunkid " & _
            '  ",lnloan_process_pending_loan.employeeunkid " & _
            '  ",lnloan_process_pending_loan.loanschemeunkid " & _
            '  ",lnloan_process_pending_loan.isloan " & _
            '  ",lnloan_process_pending_loan.loan_amount As Amount " & _
            '  ",lnloan_process_pending_loan.loanschemeunkid " & _
            '  ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
            '  ",lnloan_process_pending_loan.approverunkid " & _
            '  ",lnloan_process_pending_loan.remark As Remark " & _
            ' "FROM lnloan_process_pending_loan " & _
            ' "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
            ' "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
            ' "WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "


            
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'strQ = "SELECT " & _
            '              " lnloan_process_pending_loan.processpendingloanunkid " & _
            '              ",lnloan_process_pending_loan.application_no As Application_No " & _
            '             ",convert(char(8),application_date,112) As application_date " & _
            '              ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '              ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
            '              ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
            '              ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
            '              ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending " & _
            '              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved " & _
            '              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected " & _
            '              " END As LoanStatus" & _
            '              ",lnloan_process_pending_loan.loan_statusunkid " & _
            '              ",lnloan_process_pending_loan.employeeunkid " & _
            '              ",lnloan_process_pending_loan.loanschemeunkid " & _
            '              ",lnloan_process_pending_loan.isloan " & _
            '              ",lnloan_process_pending_loan.loan_amount As Amount " & _
            '              ",lnloan_process_pending_loan.loanschemeunkid " & _
            '              ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
            '              ",lnloan_process_pending_loan.approverunkid " & _
            '              ",lnloan_process_pending_loan.remark As Remark " & _
            '              ",lnloan_process_pending_loan.isexternal_entity " & _
            '              ",lnloan_process_pending_loan.external_entity_name " & _
            '             "FROM lnloan_process_pending_loan " & _
            '             "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
            '             "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
            '             "WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'strQ = "SELECT " & _
            '  " lnloan_process_pending_loan.processpendingloanunkid " & _
            '  ",lnloan_process_pending_loan.application_no As Application_No " & _
            ' ",convert(char(8),application_date,112) As application_date " & _
            '  ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '  ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
            '  ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
            '  ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
            '  ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending " & _
            '  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved " & _
            '  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected " & _
            '  " END As LoanStatus" & _
            '  ",lnloan_process_pending_loan.loan_statusunkid " & _
            '  ",lnloan_process_pending_loan.employeeunkid " & _
            '  ",lnloan_process_pending_loan.loanschemeunkid " & _
            '  ",lnloan_process_pending_loan.isloan " & _
            '  ",lnloan_process_pending_loan.loan_amount As Amount " & _
            '  ",lnloan_process_pending_loan.loanschemeunkid " & _
            '  ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
            '  ",lnloan_process_pending_loan.approverunkid " & _
            '  ",lnloan_process_pending_loan.remark As Remark " & _
            '  ",lnloan_process_pending_loan.isexternal_entity " & _
            '  ",lnloan_process_pending_loan.external_entity_name " & _
            '            "	,ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
            '            "	,ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
            ' "FROM lnloan_process_pending_loan " & _
            ' "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
            ' "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
            ' "WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "
            strQ = "SELECT " & _
              " lnloan_process_pending_loan.processpendingloanunkid " & _
              ",lnloan_process_pending_loan.application_no As Application_No " & _
             ",convert(char(8),application_date,112) As application_date " & _
              ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
              ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
              ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
              ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
              ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending " & _
              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved " & _
              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected " & _
              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 4 THEN @Assigned " & _
              " END As LoanStatus " & _
              ",lnloan_process_pending_loan.loan_statusunkid " & _
              ",lnloan_process_pending_loan.employeeunkid " & _
              ",lnloan_process_pending_loan.loanschemeunkid " & _
              ",lnloan_process_pending_loan.isloan " & _
              ",lnloan_process_pending_loan.loan_amount As Amount " & _
              ",lnloan_process_pending_loan.loanschemeunkid " & _
              ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
              ",lnloan_process_pending_loan.approverunkid " & _
              ",lnloan_process_pending_loan.remark As Remark " & _
              ",lnloan_process_pending_loan.isexternal_entity " & _
              ",lnloan_process_pending_loan.external_entity_name " & _
                        "	,ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
                        "	,ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
             ",hremployee_master.employeecode AS EmpCode " & _
             ",ISNULL(lnloan_advance_tran.balance_amount,0) AS balance " & _
             ",CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
             "       WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
             "       WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
             "       WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed " & _
             "       ELSE ''  END As finalStatus " & _
             ",lnloan_process_pending_loan.emp_remark " & _
             "FROM lnloan_process_pending_loan " & _
             "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
             "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
             "LEFT JOIN lnloan_advance_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid AND ISNULL(lnloan_advance_tran.isvoid,0) = 0 " & _
             "WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "

            'Anjan (20 Mar 2012)-End 
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            If intStatusID > 0 Then
                strQ &= "AND lnloan_process_pending_loan.loan_statusunkid = @loan_statusunkid"
                objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID.ToString)
            End If
            'Sohail (21 Aug 2010) -- End

            'Anjan (09 Aug 2011)-Start
            'Issue : For including setting of acitve and inactive employee.
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If strIncludeInactiveEmployee = "" Then strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            If CBool(strIncludeInactiveEmployee) = False Then
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                'Sohail (23 Apr 2012) -- End
                'Sohail (06 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                'Sohail (23 Apr 2012) -- End
                'Sohail (06 Jan 2012) -- End
            End If
            'Anjan (09 Aug 2011)-End 

            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            'End If
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '        End If
            'End Select
            If strUserAccessLevelFilterString = "" Then
                strQ &= UserAccessLevel._AccessLevelFilterString
            Else
                strQ &= strUserAccessLevelFilterString
                    End If
            'Sohail (23 Apr 2012) -- End
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Anjan (24 Jun 2011)-End 


            'If blnOnlyActive Then
            '    strQ &= " WHERE isactive = 1 "
            'End If

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Assigned"))
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))
            'Anjan (20 Mar 2012)-End 



            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_process_pending_loan) </purpose>
    Public Function Insert(Optional ByVal intCompanyUnkId As Integer = 0 _
                           , Optional ByVal intLoanApplicationNoType As Integer = 0 _
                           , Optional ByVal strLoanApplicationPrifix As String = "" _
                           , Optional ByVal intNextLoanApplicationNo As Integer = 0) As Boolean

       
       

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        'Sandeep [ 16 Oct 2010 ] -- Start
        objDataOperation.BindTransaction()
        'Sandeep [ 16 Oct 2010 ] -- End 

        'Anjan (21 Jan 2012)-Start
        'ENHANCEMENT : TRA COMMENTS 
        'Issue : To fix the issue on network of autocode concurrency when more than 1 user press save at same time.

        'Sandeep [ 16 Oct 2010 ] -- Start
        'Issue : Auto No. Generation



        'S.SANDEEP [ 28 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
        'Dim intLoanAppNoType As Integer = 0
        'Dim strLoanAppNoPrifix As String = ""
        'Dim intNextLoanAppNo As Integer = 0

        'intLoanAppNoType = ConfigParameter._Object._LoanApplicationNoType

        'If intLoanAppNoType = 1 Then
        '    strLoanAppNoPrifix = ConfigParameter._Object._LoanApplicationPrifix
        '    intNextLoanAppNo = ConfigParameter._Object._NextLoanApplicationNo
        '    mstrApplication_No = strLoanAppNoPrifix & intNextLoanAppNo
        'End If

        'If isExist(mintEmployeeunkid, mblnIsloan, mstrApplication_No, mdtApplication_Date, mintLoanschemeunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
        '    Return False
        'End If
        'Sandeep [ 16 Oct 2010 ] -- End 

        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim intLoanAppNoType As Integer = -1
        'intLoanAppNoType = ConfigParameter._Object._LoanApplicationNoType
        If intLoanApplicationNoType = 0 Then intLoanApplicationNoType = ConfigParameter._Object._LoanApplicationNoType
        If strLoanApplicationPrifix = "" Then strLoanApplicationPrifix = ConfigParameter._Object._LoanApplicationPrifix
        'Sohail (23 Apr 2012) -- End

        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        'If intLoanAppNoType = 0 Then
        If intLoanApplicationNoType = 0 Then
            'Sohail (23 Apr 2012) -- End
        If isExist(mintEmployeeunkid, mblnIsloan, mstrApplication_No, mdtApplication_Date, mintLoanschemeunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
                objDataOperation.ReleaseTransaction(False)
            Return False
        End If
        End If
        'S.SANDEEP [ 28 MARCH 2012 ] -- END


        'If isExist(mstrApplication_No) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application No is already defined. Please define new Application No.")
        'Return False
        '  End If

        'Anjan (21 Jan 2012)-End

        Try
            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
            ' objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
            objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            If mdtApplication_Date = Nothing Then
                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.AddParameter("@isexternal_entity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal_Entity.ToString)
            objDataOperation.AddParameter("@external_entity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExternal_Entity_Name.ToString)
            'strQ = "INSERT INTO lnloan_process_pending_loan ( " & _
            '  "  application_no " & _
            '  ", application_date " & _
            '  ", employeeunkid " & _
            '  ", loanschemeunkid " & _
            '  ", loan_amount " & _
            '  ", approverunkid " & _
            '  ", loan_statusunkid " & _
            '  ", approved_amount " & _
            '  ", remark " & _
            '  ", isloan" & _
            '  ", isvoid " & _
            '  ", userunkid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime" & _
            '  ", voidreason" & _
            '") VALUES (" & _
            '  "  @application_no " & _
            '  ", @application_date " & _
            '  ", @employeeunkid " & _
            '  ", @loanschemeunkid " & _
            '  ", @loan_amount " & _
            '  ", @approverunkid " & _
            '  ", @loan_statusunkid " & _
            '  ", @approved_amount " & _
            '  ", @remark " & _
            '  ", @isloan" & _
            '  ", @isvoid " & _
            '  ", @userunkid " & _
            '  ", @voiduserunkid " & _
            '  ", @voiddatetime" & _
            '  ", @voidreason" & _
            '"); SELECT @@identity"


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            'strQ = "INSERT INTO lnloan_process_pending_loan ( " & _
            '              "  application_no " & _
            '              ", application_date " & _
            '              ", employeeunkid " & _
            '              ", loanschemeunkid " & _
            '              ", loan_amount " & _
            '              ", approverunkid " & _
            '              ", loan_statusunkid " & _
            '              ", approved_amount " & _
            '              ", remark " & _
            '              ", isloan" & _
            '              ", isvoid " & _
            '              ", userunkid " & _
            '              ", voiduserunkid " & _
            '              ", voiddatetime" & _
            '              ", voidreason" & _
            '              ", isexternal_entity " & _
            '              ", external_entity_name" & _
            '          ") VALUES (" & _
            '              "  @application_no " & _
            '              ", @application_date " & _
            '              ", @employeeunkid " & _
            '              ", @loanschemeunkid " & _
            '              ", @loan_amount " & _
            '              ", @approverunkid " & _
            '              ", @loan_statusunkid " & _
            '              ", @approved_amount " & _
            '              ", @remark " & _
            '              ", @isloan" & _
            '              ", @isvoid " & _
            '              ", @userunkid " & _
            '              ", @voiduserunkid " & _
            '              ", @voiddatetime" & _
            '              ", @voidreason" & _
            '              ", @isexternal_entity " & _
            '              ", @external_entity_name" & _
            '            "); SELECT @@identity"

            'S.SANDEEP [ 18 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)
            'S.SANDEEP [ 18 APRIL 2012 ] -- END

            strQ = "INSERT INTO lnloan_process_pending_loan ( " & _
              "  application_no " & _
              ", application_date " & _
              ", employeeunkid " & _
              ", loanschemeunkid " & _
              ", loan_amount " & _
              ", approverunkid " & _
              ", loan_statusunkid " & _
              ", approved_amount " & _
              ", remark " & _
              ", isloan" & _
              ", isvoid " & _
              ", userunkid " & _
              ", voiduserunkid " & _
              ", voiddatetime" & _
              ", voidreason" & _
                                ", isexternal_entity " & _
                                ", external_entity_name" & _
                            ", loginemployeeunkid " & _
                            ", voidloginemployeeunkid" & _
                      ", emp_remark " & _
            ") VALUES (" & _
              "  @application_no " & _
              ", @application_date " & _
              ", @employeeunkid " & _
              ", @loanschemeunkid " & _
              ", @loan_amount " & _
              ", @approverunkid " & _
              ", @loan_statusunkid " & _
              ", @approved_amount " & _
              ", @remark " & _
              ", @isloan" & _
              ", @isvoid " & _
              ", @userunkid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime" & _
              ", @voidreason" & _
                                ", @isexternal_entity " & _
                                ", @external_entity_name" & _
                            ", @loginemployeeunkid " & _
                            ", @voidloginemployeeunkid" & _
                      ", @emp_remark " & _
            "); SELECT @@identity"

            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            'S.SANDEEP [ 18 APRIL 2012 emp_remark ] -- START -- END


            'Sandeep [ 21 Aug 2010 ] -- End 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintProcesspendingloanunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 28 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If intLoanApplicationNoType = 1 Then
                'If intLoanAppNoType = 1 Then
                If Set_AutoNumber(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", "NextLoanApplicationNo", strLoanApplicationPrifix, intCompanyUnkId) = False Then
                    'If Set_AutoNumber(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", "NextLoanApplicationNo", ConfigParameter._Object._LoanApplicationPrifix) = False Then
                    'Sohail (23 Apr 2012) -- End

                    'S.SANDEEP [ 17 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If objDataOperation.ErrorMessage <> "" Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'S.SANDEEP [ 17 NOV 2012 ] -- END
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If Get_Saved_Number(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", mstrApplication_No) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END
            End If
            'S.SANDEEP [ 28 MARCH 2012 ] -- END




            'Sandeep [ 16 Oct 2010 ] -- Start
            Call InsertAuditTrailForPendingLoan(objDataOperation, 1)



            'S.SANDEEP [ 28 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
            'If intLoanAppNoType = 1 Then
            '    ConfigParameter._Object._NextLoanApplicationNo = intNextLoanAppNo + 1
            '    ConfigParameter._Object.updateParam()
            '    ConfigParameter._Object.Refresh()
            'End If
            'S.SANDEEP [ 28 MARCH 2012 ] -- END




            objDataOperation.ReleaseTransaction(True)


            'Sandeep [ 16 Oct 2010 ] -- End 

            Return True
        Catch ex As Exception
            'Sandeep [ 16 Oct 2010 ] -- Start
            objDataOperation.ReleaseTransaction(False)
            'Sandeep [ 16 Oct 2010 ] -- End 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_process_pending_loan) </purpose>
    Public Function Update(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
        'Public Function Update() As Boolean
        If isExist(mintEmployeeunkid, mblnIsloan, mstrApplication_No, mdtApplication_Date, mintLoanschemeunkid, mintProcesspendingloanunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
            Return False
        End If

        'If isExist(mstrApplication_No, mintProcesspendingloanunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Application No is already defined. Please define new Application No.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 04 DEC 2013 ] -- START
        'Dim objDataOperation As New clsDataOperation

        ''Sandeep [ 16 Oct 2010 ] -- Start
        ''objDataOperation.BindTransaction()
        'objDataOperation.BindTransaction()
        ''Sandeep [ 16 Oct 2010 ] -- End 
        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END
        Try
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
            ' objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
            objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            If mdtApplication_Date = Nothing Then
                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.AddParameter("@isexternal_entity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal_Entity.ToString)
            objDataOperation.AddParameter("@external_entity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExternal_Entity_Name.ToString)

            'strQ = "UPDATE lnloan_process_pending_loan SET " & _
            '  "  application_no = @application_no" & _
            '  ", application_date = @application_date" & _
            '  ", employeeunkid = @employeeunkid" & _
            '  ", loanschemeunkid = @loanschemeunkid" & _
            '  ", loan_amount = @loan_amount" & _
            '  ", approverunkid = @approverunkid" & _
            '  ", loan_statusunkid = @loan_statusunkid" & _
            '  ", approved_amount = @approved_amount" & _
            '  ", remark = @remark" & _
            '  ", isloan = @isloan " & _
            '  ", isvoid = @isvoid" & _
            '  ", userunkid = @userunkid" & _
            '  ", voiduserunkid = @voiduserunkid" & _
            '  ", voiddatetime = @voiddatetime " & _
            '  ", voidreason = @voidreason " & _
            '"WHERE processpendingloanunkid = @processpendingloanunkid "


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            'strQ = "UPDATE lnloan_process_pending_loan SET " & _
            '                "  application_no = @application_no" & _
            '                ", application_date = @application_date" & _
            '                ", employeeunkid = @employeeunkid" & _
            '                ", loanschemeunkid = @loanschemeunkid" & _
            '                ", loan_amount = @loan_amount" & _
            '                ", approverunkid = @approverunkid" & _
            '                ", loan_statusunkid = @loan_statusunkid" & _
            '                ", approved_amount = @approved_amount" & _
            '                ", remark = @remark" & _
            '                ", isloan = @isloan " & _
            '                ", isvoid = @isvoid" & _
            '                ", userunkid = @userunkid" & _
            '                ", voiduserunkid = @voiduserunkid" & _
            '                ", voiddatetime = @voiddatetime " & _
            '                ", voidreason = @voidreason " & _
            '                ", isexternal_entity = @isexternal_entity" & _
            '                ", external_entity_name = @external_entity_name " & _
            '         "WHERE processpendingloanunkid = @processpendingloanunkid "

            'S.SANDEEP [ 18 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)
            'S.SANDEEP [ 18 APRIL 2012 ] -- END

            strQ = "UPDATE lnloan_process_pending_loan SET " & _
              "  application_no = @application_no" & _
              ", application_date = @application_date" & _
              ", employeeunkid = @employeeunkid" & _
              ", loanschemeunkid = @loanschemeunkid" & _
              ", loan_amount = @loan_amount" & _
              ", approverunkid = @approverunkid" & _
              ", loan_statusunkid = @loan_statusunkid" & _
              ", approved_amount = @approved_amount" & _
              ", remark = @remark" & _
              ", isloan = @isloan " & _
              ", isvoid = @isvoid" & _
              ", userunkid = @userunkid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
                            ", isexternal_entity = @isexternal_entity" & _
                            ", external_entity_name = @external_entity_name " & _
                            ", loginemployeeunkid = @loginemployeeunkid" & _
                            ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
              ", emp_remark = @emp_remark " & _
            "WHERE processpendingloanunkid = @processpendingloanunkid "
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            'S.SANDEEP [ 18 APRIL 2012 emp_remark ] -- START -- END

            'Sandeep [ 21 Aug 2010 ] -- End 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Dim blnFlag As Boolean = False

            'blnFlag = UpdateLoanData(mintLoanAdvanceId, mdblApproved_Amount, mdblInterest_Amount, mdblNet_Amount, mblnIsAdvance)
            'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            'objDataOperation.ReleaseTransaction(True)

            'Sandeep [ 16 Oct 2010 ] -- Start
            Call InsertAuditTrailForPendingLoan(objDataOperation, 2)

            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation.ReleaseTransaction(True)
            If objDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [ 04 DEC 2013 ] -- END

            'Sandeep [ 16 Oct 2010 ] -- End 

            Return True
        Catch ex As Exception
            'Sandeep [ 16 Oct 2010 ] -- Start
            objDataOperation.ReleaseTransaction(False)
            'Sandeep [ 16 Oct 2010 ] -- End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation = Nothing
            'S.SANDEEP [ 04 DEC 2013 ] -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_process_pending_loan) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        'Sandeep [ 16 Oct 2010 ] -- Start
        objDataOperation.BindTransaction()
        'Sandeep [ 16 Oct 2010 ] -- End 

        Try

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'strQ = "UPDATE lnloan_process_pending_loan SET " & _
            '                    "  isvoid = @isvoid" & _
            '                    ", voiduserunkid = @voiduserunkid" & _
            '                    ", voiddatetime = @voiddatetime " & _
            '                    ", voidreason = @voidreason " & _
            '            "WHERE processpendingloanunkid = @processpendingloanunkid "
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            If mintVoidloginemployeeunkid <= -1 Then
            strQ = "UPDATE lnloan_process_pending_loan SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime " & _
                    ", voidreason = @voidreason " & _
            "WHERE processpendingloanunkid = @processpendingloanunkid "
            Else
                strQ = "UPDATE lnloan_process_pending_loan SET " & _
                                "  isvoid = @isvoid" & _
                                ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                                ", voiddatetime = @voiddatetime " & _
                                ", voidreason = @voidreason " & _
                        "WHERE processpendingloanunkid = @processpendingloanunkid "
            End If

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            If mintVoidloginemployeeunkid > 0 Then
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sandeep [ 16 Oct 2010 ] -- Start
            Call InsertAuditTrailForPendingLoan(objDataOperation, 3)
            objDataOperation.ReleaseTransaction(True)
            'Sandeep [ 16 Oct 2010 ] -- End 

            Return True
        Catch ex As Exception
            'Sandeep [ 16 Oct 2010 ] -- Start
            objDataOperation.ReleaseTransaction(False)
            'Sandeep [ 16 Oct 2010 ] -- End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpId As Integer, ByVal blnLoan As Boolean, ByVal strAppNo As String, ByVal dtAppDate As Date, ByVal intLoanSchemeId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'objDataOperation = New clsDataOperation
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  processpendingloanunkid " & _
              ", application_no " & _
              ", application_date " & _
              ", employeeunkid " & _
              ", loanschemeunkid " & _
              ", loan_amount " & _
              ", approverunkid " & _
              ", loan_statusunkid " & _
              ", approved_amount " & _
              ", remark " & _
              ", isloan " & _
              ", isvoid " & _
              ", userunkid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
            "FROM lnloan_process_pending_loan " & _
            "WHERE CONVERT(CHAR(8),application_date,112) = @application_date " & _
            "AND application_no = @application_no " & _
            "AND isloan = @isloan "

            If intEmpId > 0 Then
                strQ &= "AND employeeunkid = @employeeunkid "
            End If

            If intLoanSchemeId > 0 Then
                strQ &= "AND loanschemeunkid = @loanschemeunkid "
            End If




            If intUnkid > 0 Then
                strQ &= " AND processpendingloanunkid <> @processpendingloanunkid"
            End If


            '  objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnLoan)
            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strAppNo)
            objDataOperation.AddParameter("@application_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtAppDate).ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanSchemeId)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    'Public Function isExist(ByVal strAppNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  processpendingloanunkid " & _
    '          ", application_no " & _
    '          ", application_date " & _
    '          ", employeeunkid " & _
    '          ", loanschemeunkid " & _
    '          ", loan_amount " & _
    '          ", approverunkid " & _
    '          ", loan_statusunkid " & _
    '          ", approved_amount " & _
    '          ", remark " & _
    '          ", isloan " & _
    '          ", isvoid " & _
    '          ", userunkid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '        "FROM lnloan_process_pending_loan " & _
    '         "WHERE application_no = @application_no "


    '        If intUnkid > 0 Then
    '            strQ &= " AND processpendingloanunkid <> @processpendingloanunkid"
    '        End If


    '        '  objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strAppNo)
    '        objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.Tables(0).Rows.Count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function


    'Sandeep [ 21 Aug 2010 ] -- Start
    'Public Function GetLoan_Status(Optional ByVal strListName As String = "List") As DataSet
    '    Dim strQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Try

    '        strQ = "SELECT 0 AS Id,@Select AS NAME " & _
    '                "UNION SELECT 1 AS Id,@Pending AS NAME " & _
    '                "UNION SELECT 2 AS Id,@Approved AS NAME " & _
    '                "UNION SELECT 3 AS Id,@Rejected AS NAME "

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 70, "Select"))
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 71, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 72, "Approved"))
    '        objDataOperation.AddParameter("Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Rejected"))

    '        'If blnisSaving = False Then
    '        '    objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "Written Off"))
    '        'Else
    '        '    objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 82, "Redemption"))
    '        'End If

    '        dsList = objDataOperation.ExecQuery(strQ, strListName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetLoan_Status", mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Public Function GetLoan_Status(Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal UseAssignedStatus As Boolean = False) As DataSet
        Dim strQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            If blnFlag = True Then
                strQ = "SELECT 0 AS Id,@Select AS NAME UNION  "
            End If

            strQ &= " SELECT 1 AS Id,@Pending AS NAME " & _
                    "UNION SELECT 2 AS Id,@Approved AS NAME " & _
                    "UNION SELECT 3 AS Id,@Rejected AS NAME "

            'Anjan (04 Apr 2011)-Start
            'Issue : Included new Status of Assigned as per Rutta's suggestion
            If UseAssignedStatus = True Then
                strQ &= "UNION SELECT 4 AS Id, @Assigned AS NAME "
                objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Assigned"))
            End If

            'Anjan (04 Apr 2011)-End

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))

            'Sandeep [ 23 Oct 2010 ] -- Start
            'objDataOperation.AddParameter("Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Rejected"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))
            'Sandeep [ 23 Oct 2010 ] -- End 

            'If blnisSaving = False Then
            '    objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "Written Off"))
            'Else
            '    objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 82, "Redemption"))
            'End If

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoan_Status", mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sandeep [ 21 Aug 2010 ] -- End 

    Public Function Get_Id_Used_In_Paymet(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   " prpayment_tran.referencetranunkid " & _
                   " FROM prpayment_tran " & _
                   " LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                   " LEFT JOIN lnloan_process_pending_loan ON lnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
                   " WHERE lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid"

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Get_Id_Used_In_LoanAdvance(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 16 Oct 2010 ] -- Start
            'strQ = "Select " & _
            '       " loanadvancetranunkid " & _
            '       "From lnloan_advance_tran " & _
            '       "WHERE processpendingloanunkid=@processpendingloanunkid "

            strQ = "Select " & _
                      " loanadvancetranunkid " & _
                      "From lnloan_advance_tran " & _
                      "WHERE processpendingloanunkid=@processpendingloanunkid " & _
                      "AND ISNULL(lnloan_advance_tran.isvoid,0) = 0 "
            'Sandeep [ 16 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Get_LoanAdvanceunkid(ByVal intUnkid As Integer, ByRef intLoanAdvanceId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "loanadvancetranunkid " & _
                   " FROM lnloan_advance_tran " & _
                   " WHERE processpendingloanunkid = @processpendingloanunkid"

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intLoanAdvanceId = CInt(dsList.Tables(0).Rows(0)("loanadvancetranunkid"))
            Else
                intLoanAdvanceId = 0
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sandeep [ 16 Oct 2010 ] -- Start
    Private Sub InsertAuditTrailForPendingLoan(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer)
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atlnloan_process_pending_loan ( " & _
            '            "  processpendingloanunkid " & _
            '            ", application_no " & _
            '            ", application_date " & _
            '            ", employeeunkid " & _
            '            ", loanschemeunkid " & _
            '            ", loan_amount " & _
            '            ", approverunkid " & _
            '            ", loan_statusunkid " & _
            '            ", approved_amount " & _
            '            ", remark " & _
            '            ", isloan " & _
            '            ", audittype " & _
            '            ", audituserunkid " & _
            '            ", auditdatetime " & _
            '            ", ip " & _
            '            ", machine_name" & _
            '            ", emp_remark " & _
            '       ") VALUES (" & _
            '            "  @processpendingloanunkid " & _
            '            ", @application_no " & _
            '            ", @application_date " & _
            '            ", @employeeunkid " & _
            '            ", @loanschemeunkid " & _
            '            ", @loan_amount " & _
            '            ", @approverunkid " & _
            '            ", @loan_statusunkid " & _
            '            ", @approved_amount " & _
            '            ", @remark " & _
            '            ", @isloan " & _
            '            ", @audittype " & _
            '            ", @audituserunkid " & _
            '            ", @auditdatetime " & _
            '            ", @ip " & _
            '            ", @machine_name" & _
            '            ", @emp_remark " & _
            '       "); SELECT @@identity"


            strQ = "INSERT INTO atlnloan_process_pending_loan ( " & _
                        "  processpendingloanunkid " & _
                        ", application_no " & _
                        ", application_date " & _
                        ", employeeunkid " & _
                        ", loanschemeunkid " & _
                        ", loan_amount " & _
                        ", approverunkid " & _
                        ", loan_statusunkid " & _
                        ", approved_amount " & _
                        ", remark " & _
                        ", isloan " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", emp_remark " & _
                     ", form_name " & _
                     ", module_name1 " & _
                     ", module_name2 " & _
                     ", module_name3 " & _
                     ", module_name4 " & _
                     ", module_name5 " & _
                     ", isweb " & _
                        ", loginemployeeunkid " & _
                   ") VALUES (" & _
                        "  @processpendingloanunkid " & _
                        ", @application_no " & _
                        ", @application_date " & _
                        ", @employeeunkid " & _
                        ", @loanschemeunkid " & _
                        ", @loan_amount " & _
                        ", @approverunkid " & _
                        ", @loan_statusunkid " & _
                        ", @approved_amount " & _
                        ", @remark " & _
                        ", @isloan " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name" & _
                        ", @emp_remark " & _
                     ", @form_name " & _
                     ", @module_name1 " & _
                     ", @module_name2 " & _
                     ", @module_name3 " & _
                     ", @module_name4 " & _
                     ", @module_name5 " & _
                     ", @isweb " & _
                        ", @loginemployeeunkid " & _
                   "); SELECT @@identity"


            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
            objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (23 Apr 2012) -- End
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            'objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            'S.SANDEEP [ 13 AUG 2012 ] -- END

            
            'S.SANDEEP [ 18 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)
            'S.SANDEEP [ 18 APRIL 2012 ] -- END


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            'S.SANDEEP [ 13 AUG 2012 ] -- END


            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForPendingLoan", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 16 Oct 2010 ] -- End 

    Public Function UpdateLoanData(ByVal intLoanId As Integer, _
                                   ByVal decNewAmount As Decimal, _
                                   ByVal decIntrest_Amt As Decimal, _
                                   ByVal decNetAmt As Decimal, _
                                   ByVal blnIsAdvance As Boolean) As Boolean 'Sohail (11 May 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanId.ToString)

            If blnIsAdvance = False Then
                strQ = "UPDATE lnloan_advance_tran SET " & _
                        "  loan_amount = @loan_amount" & _
                        ", interest_amount = @interest_amount" & _
                        ", net_amount = @net_amount" & _
                        " WHERE loanadvancetranunkid = @loanadvancetranunkid "

                objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decNewAmount.ToString) 'Sohail (11 May 2011)
                objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decIntrest_Amt.ToString) 'Sohail (11 May 2011)
                objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decNetAmt.ToString) 'Sohail (11 May 2011)
            Else
                strQ = "UPDATE lnloan_advance_tran SET " & _
                        "  advance_amount = @advance_amount" & _
                      " WHERE loanadvancetranunkid = @loanadvancetranunkid "

                objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decNewAmount.ToString) 'Sohail (11 May 2011)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    'Sohail (03 Nov 2010) -- Start
    Public Function Get_UnAssigned_ProcessPending_List(ByVal strTableName As String, Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
        'Sohail (12 Jan 2015) - [strUserAccessLevelFilterString]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            
            strQ = "SELECT  lnloan_process_pending_loan.processpendingloanunkid " & _
                          ", lnloan_process_pending_loan.application_no " & _
                          ", convert(char(8),lnloan_process_pending_loan.application_date,112) As application_date " & _
                          ", lnloan_process_pending_loan.employeeunkid " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                          ", lnloan_process_pending_loan.loanschemeunkid " & _
                          ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
                          ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
                          ", lnloan_process_pending_loan.loan_amount As Amount " & _
                          ", lnloan_process_pending_loan.approverunkid " & _
                          ", lnloan_process_pending_loan.loan_statusunkid " & _
                          ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected END As LoanStatus " & _
                          ", lnloan_process_pending_loan.approved_amount " & _
                          ", lnloan_process_pending_loan.remark " & _
                          ", lnloan_process_pending_loan.isloan " & _
                          ", lnloan_process_pending_loan.isvoid " & _
                          ", lnloan_process_pending_loan.userunkid " & _
                          ", lnloan_process_pending_loan.voiduserunkid " & _
                          ", lnloan_process_pending_loan.voiddatetime " & _
                          ", lnloan_process_pending_loan.voidreason " & _
                          ", lnloan_process_pending_loan.isexternal_entity " & _
                          ", lnloan_process_pending_loan.external_entity_name " & _
                    "FROM    lnloan_process_pending_loan " & _
                            "LEFT JOIN lnloan_advance_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
                            "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                            "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                    "WHERE   lnloan_advance_tran.loanadvancetranunkid IS NULL " & _
                            "AND ISNULL(lnloan_process_pending_loan.isvoid, 0) = 0 " & _
                            "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                            "AND lnloan_process_pending_loan.loan_statusunkid = 2 " '2=Approved


            'Anjan (09 Aug 2011)-Start
            'Issue : For including setting of acitve and inactive employee.
            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                'Sohail (06 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                'Sohail (06 Jan 2012) -- End
            End If
            'Anjan (09 Aug 2011)-End 

            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '        End If
            'End Select

            'Sohail (12 Jan 2015) -- Start
            'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            'strQ &= UserAccessLevel._AccessLevelFilterString
            If strUserAccessLevelFilterString = "" Then
            strQ &= UserAccessLevel._AccessLevelFilterString
            Else
                strQ &= strUserAccessLevelFilterString
            End If
            'Sohail (12 Jan 2015) -- End
            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            
            'S.SANDEEP [ 04 FEB 2012 ] -- END            

            'Anjan (24 Jun 2011)-End 


            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_UnAssigned_ProcessPending_List; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (03 Nov 2010) -- End



    'S.SANDEEP [ 06 SEP 2011 ] -- START
    'ENHANCEMENT : MAKING COMMON FUNCTION
    Public Function GetToAssignList(ByVal intStatusId As Integer, _
                                    ByVal intDepartmentId As Integer, _
                                    ByVal intBranchId As Integer, _
                                    ByVal intSecId As Integer, _
                                    ByVal intJobId As Integer, _
                                    ByVal intEmpId As Integer, _
                                    ByVal intApproverId As Integer, _
                                    ByVal intMode As Integer, _
                                    ByVal intSchemeId As Integer, _
                                    Optional ByVal StrListName As String = "List", _
                                    Optional ByVal strUserAccessFilter As String = "") As DataTable   'Pinkal (14-Jun-2013) [strUserAccessFilter]
        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))

            '/* SCHEME MASTER LIST */
            StrQ = "SELECT " & _
                             " loanschemeunkid AS loanschemeunkid " & _
                             ",@Loan+' --> '+ISNULL(name,'') AS Schemes " & _
                       "FROM lnloan_scheme_master " & _
                       "UNION ALL " & _
                       "SELECT " & _
                             " 0 AS loanschemeunkid " & _
                             ",@Advance AS Schemes "

            Dim dsScheme As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsScheme.Tables("List").Rows.Count > 0 Then
                '/* TRANSACTION LIST */
                StrQ = "SELECT " & _
                                 " ECode AS ECode " & _
                                 ",EName AS EName " & _
                                 ",Mode AS Mode " & _
                                 ",AppNo AS AppNo " & _
                                 ",Amount AS Amount " & _
                                 ",LScheme AS LScheme " & _
                                 ",LApp AS Approver " & _
                                 ",PId AS PId " & _
                                 ",ModeId AS ModeId " & _
                                 ",IsEx AS IsEx " & _
                                 ",BranchId AS BranchId " & _
                                 ",DeptId AS DeptId " & _
                                 ",SectionId AS SectionId " & _
                                 ",JobId AS JobId " & _
                                 ",EmpId AS EmpId " & _
                                 ",ApprId AS ApprId " & _
                                 ",SchemeId AS SchemeId " & _
                                 ",StatusId AS StatusId " & _
                            "FROM " & _
                            "( " & _
                            "SELECT " & _
                                  "ISNULL(hremployee_master.employeecode,'') AS ECode " & _
                                 ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
                                 ",ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
                                 ",ISNULL(lnloan_process_pending_loan.approved_amount,0) AS Amount " & _
                                 ",lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
                                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS ModeId " & _
                                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan+' --> '+ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS LScheme " & _
                                 ",ISNULL(LApp.firstname,'')+' '+ISNULL(LApp.othername,'')+' '+ISNULL(LApp.surname,'') AS LApp " & _
                                 ",lnloan_process_pending_loan.isexternal_entity AS IsEx " & _
                                 ",hremployee_master.stationunkid AS BranchId " & _
                                 ",hremployee_master.departmentunkid  As DeptId " & _
                                 ",hremployee_master.sectionunkid AS SectionId " & _
                                 ",hremployee_master.jobunkid AS JobId " & _
                                 ",hremployee_master.employeeunkid AS EmpId " & _
                                 ",LApp.employeeunkid AS ApprId " & _
                                 ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS SchemeId " & _
                                 ",lnloan_process_pending_loan.loan_statusunkid AS StatusId " & _
                            "FROM lnloan_process_pending_loan " & _
                                 "JOIN lnloan_approver_tran ON lnloan_approver_tran.approverunkid = lnloan_process_pending_loan.approverunkid " & _
                                 "JOIN hremployee_master as LApp ON LApp.employeeunkid = lnloan_approver_tran.approverunkid " & _
                                 "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                                 "JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                            "WHERE isvoid = 0 "
                If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                    'Sohail (06 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'StrQ &= " AND hremployee_master.isactive = 1 "
                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    'Sohail (06 Jan 2012) -- End
                End If

                'S.SANDEEP [ 01 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA DISCIPLINE CHANGES
                'Select Case ConfigParameter._Object._UserAccessModeSetting
                '    Case enAllocation.BRANCH
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.TEAM
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOB_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOBS
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                'End Select


                'Pinkal (12-Jun-2013) -- Start
                'Enhancement : TRA Changes

                If strUserAccessFilter.Trim.Length > 0 Then
                    StrQ &= strUserAccessFilter
                    StrQ &= strUserAccessFilter.Replace("hremployee_master", "LApp")
                Else
                StrQ &= UserAccessLevel._AccessLevelFilterString
                StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "LApp")
                End If

                'StrQ &= UserAccessLevel._AccessLevelFilterString
                'StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "LApp")

                'Pinkal (12-Jun-2013) -- End

                
                'S.SANDEEP [ 01 JUNE 2012 ] -- END

                

                StrQ &= ") AS ToAssign WHERE 1 = 1 AND StatusId = " & intStatusId

                If intDepartmentId > 0 Then
                    StrQ &= " AND DeptId = '" & intDepartmentId & "'"
                End If

                If intApproverId > 0 Then
                    StrQ &= " AND ApprId = '" & intApproverId & "'"
                End If

                If intBranchId > 0 Then
                    StrQ &= " AND BranchId = '" & intBranchId & "'"
                End If

                If intEmpId > 0 Then
                    StrQ &= " AND EmpId = '" & intEmpId & "'"
                End If

                If intJobId > 0 Then
                    StrQ &= " AND JobId = '" & intJobId & "'"
                End If

                'S.SANDEEP [ 04 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= "  AND JobId IN (" & UserAccessLevel._AccessLevel & ") "
                'End If
                'S.SANDEEP [ 04 FEB 2012 ] -- END

                Select Case intMode
                    Case 0
                        StrQ &= " AND ModeId = 1 "
                    Case 1
                        StrQ &= " AND ModeId = 2 "
                End Select
                
                If intSchemeId > 0 Then
                    StrQ &= " AND SchemeId = '" & intSchemeId & "'"
                End If

                If intSecId > 0 Then
                    StrQ &= " AND SectionId = '" & intSecId & "'"
                End If

                Dim dsTrans As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If



                'Pinkal (12-Jun-2013) -- Start
                'Enhancement : TRA Changes

                    If StrListName.Length > 0 Then
                        dtTable = New DataTable(StrListName)
                    Else
                        dtTable = New DataTable("List")
                    End If


                    dtTable.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
                    dtTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
                    dtTable.Columns.Add("Employee", System.Type.GetType("System.String")).DefaultValue = ""
                    dtTable.Columns.Add("Approver", System.Type.GetType("System.String")).DefaultValue = ""
                    dtTable.Columns.Add("Amount", System.Type.GetType("System.String")).DefaultValue = ""
                    dtTable.Columns.Add("Pendingunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
                    dtTable.Columns.Add("IsEx", System.Type.GetType("System.Boolean")).DefaultValue = False
                    dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
                    dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
                    dtTable.Columns.Add("ApprId", System.Type.GetType("System.Int32")).DefaultValue = -1
                    dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1

                'Pinkal (12-Jun-2013) -- End


                If dsTrans.Tables("List").Rows.Count > 0 Then

                    Dim dtRow As DataRow = Nothing

                    Dim dtFilter As DataTable

                    For Each dsRow As DataRow In dsScheme.Tables("List").Rows
                        dtFilter = New DataView(dsTrans.Tables("List"), "SchemeId = '" & dsRow.Item("loanschemeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

                        If dtFilter.Rows.Count > 0 Then
                            dtRow = dtTable.NewRow

                            dtRow.Item("ECode") = dsRow.Item("Schemes")
                            dtRow.Item("IsGrp") = True
                            dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")

                            dtTable.Rows.Add(dtRow)

                            For Each dtlRow As DataRow In dtFilter.Rows
                                dtRow = dtTable.NewRow

                                dtRow.Item("ECode") = Space(5) & dtlRow.Item("ECode")
                                dtRow.Item("Employee") = dtlRow.Item("EName")
                                dtRow.Item("Approver") = dtlRow.Item("Approver")
                                dtRow.Item("Amount") = Format(CDec(dtlRow.Item("Amount")), GUI.fmtCurrency)
                                dtRow.Item("Pendingunkid") = dtlRow.Item("PId")
                                dtRow.Item("IsEx") = dtlRow.Item("IsEx")
                                dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
                                dtRow.Item("ApprId") = dtlRow.Item("ApprId")
                                dtRow.Item("EmpId") = dtlRow.Item("EmpId")

                                dtTable.Rows.Add(dtRow)
                            Next

                        End If

                    Next

                End If

            End If


            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetToAssignList", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 06 SEP 2011 ] -- END 


    'S.SANDEEP [ 09 AUG 2011 ] -- START
    'ENHANCEMENT : GLOBAL LOAN/ADVANCE APPROVEMENT
    Public Function GetDataList(ByVal intStatusId As Integer, _
                                ByVal intDepartmentId As Integer, _
                                ByVal intBranchId As Integer, _
                                ByVal intSecId As Integer, _
                                ByVal intJobId As Integer, _
                                ByVal intModeId As Integer) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))

            StrQ = "SELECT " & _
                    " loanschemeunkid AS loanschemeunkid " & _
                    ",@Loan+' --> '+ISNULL(name,'') AS Schemes " & _
                    "FROM lnloan_scheme_master " & _
                   "UNION ALL " & _
                    "SELECT " & _
                    " 0 AS loanschemeunkid " & _
                    ",@Advance AS Schemes "

            Dim dsScheme As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsScheme.Tables(0).Rows.Count > 0 Then

                StrQ = "SELECT " & _
                    "	 CAST(0 AS BIT) AS IsChecked " & _
                    "	,ISNULL(hremployee_master.employeecode,'') AS ECode " & _
                    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                    "	,CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
                    "	,ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
                    "	,ISNULL(lnloan_process_pending_loan.loan_amount,0) AS Amount " & _
                    "	,ISNULL(lnloan_process_pending_loan.approved_amount,0) AS AppAmount " & _
                    "	,lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
                    "   ,CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END AS ModeId " & _
                    "   ,ISNULL(lnloan_process_pending_loan.loanschemeunkid,0) AS SchId " & _
                    "FROM lnloan_process_pending_loan " & _
                    "	JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE isvoid = 0 "

                If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                    'Sohail (06 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'StrQ &= " AND isactive = 1 "
                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    'Sohail (06 Jan 2012) -- End
                End If

                'S.SANDEEP [ 04 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
                'End If

                'S.SANDEEP [ 01 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA DISCIPLINE CHANGES
                'Select Case ConfigParameter._Object._UserAccessModeSetting
                '    Case enAllocation.BRANCH
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.TEAM
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOB_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOBS
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
                '        End If
                'End Select
                StrQ &= UserAccessLevel._AccessLevelFilterString
                'S.SANDEEP [ 01 JUNE 2012 ] -- END


                'S.SANDEEP [ 04 FEB 2012 ] -- END

                If intStatusId > 0 Then
                    StrQ &= " AND lnloan_process_pending_loan.loan_statusunkid = " & intStatusId
                End If

                If intBranchId > 0 Then
                    StrQ &= " AND hremployee_master.stationunkid = " & intBranchId
                End If

                If intDepartmentId > 0 Then
                    StrQ &= " AND hremployee_master.departmentunkid = " & intDepartmentId
                End If

                If intJobId > 0 Then
                    StrQ &= " AND hremployee_master.jobunkid = " & intJobId
                End If

                If intSecId > 0 Then
                    StrQ &= " AND hremployee_master.sectionunkid = " & intSecId
                End If

                If intModeId > -1 Then
                    Select Case intModeId
                        Case 0  'ADVANCE
                            StrQ &= " AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END = 0 "
                        Case 1  'LOAN
                            StrQ &= " AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END = 1 "
                    End Select
                End If

            End If


            Dim dsTrans As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = New DataTable("List")


            'Pinkal (12-Jun-2013) -- Start
            'Enhancement : TRA Changes

                dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
                dtTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("EName", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("AppNo", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Amount", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("AppAmount", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("PId", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
                dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1

            'Pinkal (12-Jun-2013) -- End

            If dsTrans.Tables(0).Rows.Count > 0 Then
                Dim dtRow As DataRow = Nothing

                Dim dtFilter As DataTable

                For Each dsRow As DataRow In dsScheme.Tables("List").Rows
                    dtFilter = New DataView(dsTrans.Tables("List"), "SchId = '" & dsRow.Item("loanschemeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

                    If dtFilter.Rows.Count > 0 Then
                        dtRow = dtTable.NewRow

                        dtRow.Item("ECode") = dsRow.Item("Schemes")
                        dtRow.Item("IsGrp") = True
                        dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")

                        dtTable.Rows.Add(dtRow)

                        For Each dtlRow As DataRow In dtFilter.Rows
                            dtRow = dtTable.NewRow

                            dtRow.Item("ECode") = Space(5) & dtlRow.Item("ECode")
                            dtRow.Item("EName") = dtlRow.Item("EName")
                            dtRow.Item("AppNo") = dtlRow.Item("AppNo")
                            dtRow.Item("Amount") = Format(CDec(dtlRow.Item("Amount")), GUI.fmtCurrency)
                            dtRow.Item("AppAmount") = Format(CDec(dtlRow.Item("AppAmount")), GUI.fmtCurrency)
                            dtRow.Item("PId") = dtlRow.Item("PId")
                            dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")

                            dtTable.Rows.Add(dtRow)
                        Next

                    End If
                Next

            End If

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDataList", mstrModuleName)
            Return Nothing
        Finally
            dtTable.Dispose()
        End Try
    End Function
    'S.SANDEEP [ 09 AUG 2011 ] -- END 



    'Public Function Get_History_List(ByVal strTableName As String) As DataSet    ', Optional ByVal blnOnlyActive As Boolean = True)
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  processpendingloanunkid " & _
    '          ", employeeunkid " & _
    '          ", loanschemeunkid " & _
    '          ", loan_amount " & _
    '          ", approverunkid " & _
    '          ", loan_statusunkid " & _
    '          ", approved_amount " & _
    '          ", isloan " & _
    '         "FROM lnloan_process_pending_loan "

    '        'If blnOnlyActive Then
    '        '    strQ &= " WHERE isactive = 1 "
    '        'End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Public Sub GetLoanData()
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '               " loan_amount " & _
    '               ",interest_rate " & _
    '               ",interest_amount " & _
    '               ",net_amount " & _
    '               ",loan_duration" & _
    '               ",isloan " & _
    '               ",calctype_id" & _
    '               "FROM lnloan_advance_tran " & _
    '               "WHERE loanadvancetranunkid = @loanadvancetranunkid"

    '        objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            mintLoanAdvaceunkid = CInt(dtRow.Item("loanadvancetranunkid"))
    '            mdblLoan_Amount = cdec(dtRow.Item("loan_amount"))
    '            mdblInterestRate = cdec(dtRow.Item("interest_rate"))
    '            mdblInterestAmount = cdec(dtRow.Item("interest_amount"))
    '            mdblNetAmount = cdec(dtRow.Item("net_amount"))
    '            mintLoanDuration = CInt(dtRow.Item("loan_duration"))
    '            mblnIsloan = CBool(dtRow.Item("isloan"))
    '            mintCalcTypeid = CInt(dtRow.Item("calctype_id"))

    '            Exit For
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Sub


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
			Language.setMessage(mstrModuleName, 2, "WEB")
			Language.setMessage(mstrModuleName, 3, "Loan")
			Language.setMessage(mstrModuleName, 4, "Advance")
			Language.setMessage(mstrModuleName, 5, "Pending")
			Language.setMessage(mstrModuleName, 6, "Approved")
			Language.setMessage(mstrModuleName, 7, "Rejected")
			Language.setMessage(mstrModuleName, 9, "Select")
			Language.setMessage(mstrModuleName, 10, "Assigned")
			Language.setMessage("clsMasterData", 96, "In Progress")
			Language.setMessage("clsMasterData", 97, "On Hold")
			Language.setMessage("clsMasterData", 98, "Written Off")
			Language.setMessage("clsMasterData", 100, "Completed")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class