﻿'************************************************************************************************************************************
'Class Name : clsloanapproval_process_Tran.vb
'Purpose    :
'Date       :4/24/2015
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsloanapproval_process_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsloanapproval_process_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintPendingloantranunkid As Integer
    Private mintProcesspendingloanunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintApprovertranunkid As Integer
    Private mintApproverempunkid As Integer
    Private mintPriority As Integer
    Private mdtApprovaldate As Date
    Private mintDeductionperiodunkid As Integer
    Private mdecLoan_Amount As Decimal
    Private mintDuration As Integer
    Private mdecInstallmentamt As Decimal
    Private mintNoofinstallment As Integer
    Private mintCountryunkid As Integer
    Private mintStatusunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mintVisibleId As Integer = -1
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    '' <summary>
    '' Purpose: Get or Set pendingloantranunkid
    '' Modify By: Pinkal
    '' </summary>

    'Nilay (07-Feb-2016) -- Start
    'Public Property _Pendingloantranunkid() As Integer
    Public Property _Pendingloantranunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        'Nilay (07-Feb-2016) -- End

        Get
            Return mintPendingloantranunkid
        End Get
        Set(ByVal value As Integer)
            mintPendingloantranunkid = value
            'Nilay (07-Feb-2016) -- Start
            'Call GetData()
            Call GetData(objDataOp)
            'Nilay (07-Feb-2016) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processpendingloanunkid() As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovertranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Priority
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverempunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approverempunkid() As Integer
        Get
            Return mintApproverempunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverempunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deductionperiodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Deductionperiodunkid() As Integer
        Get
            Return mintDeductionperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintDeductionperiodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loan_Amount() As Decimal
        Get
            Return mdecLoan_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecLoan_Amount = value
        End Set
    End Property

    '' <summary>
    '' Purpose: Get or Set duration
    '' Modify By: Pinkal
    '' </summary>

    'Nilay (21-Oct-2015) -- Start
    'ENHANCEMENT : NEW LOAN Given By Rutta
    'Public Property _Duration() As Integer
    '    Get
    '        Return mintDuration
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintDuration = Value
    '    End Set
    'End Property
    'Nilay (21-Oct-2015) -- End

    ''' <summary>
    ''' Purpose: Get or Set installmentamt
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Installmentamt() As Decimal
        Get
            Return mdecInstallmentamt
        End Get
        Set(ByVal value As Decimal)
            mdecInstallmentamt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set noofinstallment
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Noofinstallment() As Integer
        Get
            Return mintNoofinstallment
        End Get
        Set(ByVal value As Integer)
            mintNoofinstallment = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visibleid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _VisibleId() As Integer
        Get
            Return mintVisibleId
        End Get
        Set(ByVal value As Integer)
            mintVisibleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

#End Region

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Nilay (07-Feb-2016) -- Start
    'Public Sub GetData()
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        'Nilay (07-Feb-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Nilay (07-Feb-2016) -- Start
        'Dim objDataOperation As New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        'Nilay (07-Feb-2016) -- End

        Try
            strQ = "SELECT " & _
                      "  pendingloantranunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", approvertranunkid " & _
                      ", approverempunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", deductionperiodunkid " & _
                      ", loan_amount " & _
                      ", duration " & _
                      ", installmentamt " & _
                      ", noofinstallment " & _
                      ", countryunkid " & _
                      ", statusunkid " & _
                      ", visibleid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                     "FROM lnloanapproval_process_tran " & _
                     "WHERE pendingloantranunkid = @pendingloantranunkid "

            'Nilay (05-May-2016) -- Start
            objDataOperation.ClearParameters()
            'Nilay (05-May-2016) -- End

            objDataOperation.AddParameter("@pendingloantranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintPendingloanTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintpendingloantranunkid = CInt(dtRow.Item("pendingloantranunkid"))
                mintprocesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintapprovertranunkid = CInt(dtRow.Item("approvertranunkid"))
                mintApproverempunkid = CInt(dtRow.Item("approverempunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mdtapprovaldate = dtRow.Item("approvaldate")
                mintdeductionperiodunkid = CInt(dtRow.Item("deductionperiodunkid"))
                mdecLoan_Amount = dtRow.Item("loan_amount")
                mintduration = CInt(dtRow.Item("duration"))
                mdecInstallmentamt = dtRow.Item("installmentamt")
                mintnoofinstallment = CInt(dtRow.Item("noofinstallment"))
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintVisibleId = CInt(dtRow.Item("visibleid"))
                mstrremark = dtRow.Item("remark").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Nilay (07-Feb-2016) -- Start
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Nilay (07-Feb-2016) -- End
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal mstrFilter As String = "", _
                            Optional ByVal strAdvanceFilter As String = "") As DataSet
        'Nilay (01-Mar-2016) -- [strAdvanceFilter]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName, "Emp")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName, "Emp")

            strQ = "SELECT " & _
                        " -1 AS pendingloantranunkid  " & _
                        ",lnloan_process_pending_loan.processpendingloanunkid  " & _
                        ",lnloan_process_pending_loan.application_no " & _
                        ",NULL AS application_date " & _
                        " ,lnloanapproval_process_tran.employeeunkid " & _
                        ",'' AS Employee " & _
                        ",-1 AS Approverunkid  " & _
                        ",-1 AS approverempunkid  " & _
                        ",-1 AS lnlevelunkid " & _
                        ",-1 AS priority " & _
                        ",'' AS Approver " & _
                        ",'' AS ApproverName  " & _
                        ",-1 AS loanschemeunkid  " & _
                        ",'' AS Scheme  " & _
                        ",'' AS loan_advance  " & _
                        ",-1 AS deductionperiodunkid  " & _
                        ",'' AS deduct_period  " & _
                        ",NULL AS loan_amount " & _
                        ",NULL AS approved_amount " & _
                        ",0 AS duration " & _
                        ",0 AS installmentamt " & _
                        ",0 AS noofinstallment " & _
                        ",-1 AS loan_statusunkid  " & _
                        ",-1 AS statusunkid  " & _
                        ",'' AS status " & _
                        ",-1 AS currencyunkid  " & _
                        ",'' AS remark  " & _
                        ",lnloan_process_pending_loan.isloan  " & _
                        ",lnloan_process_pending_loan.isexternal_entity  " & _
                        ",lnloan_process_pending_loan.external_entity_name  " & _
                        ",-1 AS sectiongroupunkid " & _
                        ",-1 AS unitgroupunkid " & _
                        ",-1 AS teamunkid " & _
                        ",-1 AS stationunkid " & _
                        ",-1 AS deptgroupunkid " & _
                        ",-1 AS departmentunkid " & _
                        ",-1 AS sectionunkid " & _
                        ",-1 AS unitunkid " & _
                        ",-1 AS jobunkid " & _
                        ",-1 AS classgroupunkid " & _
                        ",-1 AS classunkid " & _
                        ",-1 AS jobgroupunkid " & _
                        ",-1 AS gradegroupunkid " & _
                        ",-1 AS gradeunkid " & _
                        ",-1 AS gradelevelunkid " & _
                        ",0 AS isvoid " & _
                        ",NULL AS voiddatetime  " & _
                        ",'' AS voidreason  " & _
                        ",-1 AS voiduserunkid  " & _
                        ", 0 AS MappedUserId " & _
                        ", 0 AS visibleid " & _
                        ",1 AS isgrp " & _
                " FROM lnloan_process_pending_loan " & _
                        " LEFT JOIN lnloanapproval_process_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
                        " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = lnloanapproval_process_tran.employeeunkid " & _
                        " LEFT JOIN hremployee_master App ON App.employeeunkid = lnloanapproval_process_tran.approverempunkid " & _
                        " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid " & _
                        " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                        " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                        " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanapproval_process_tran.deductionperiodunkid " & _
                        " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid  "

            'Pinkal (20-Sep-2017) --  Issue-On Loan Approval list ,Grouping on application no. is not display [  ", 0 AS MappedUserId " & _]

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            strQ &= " WHERE lnloan_process_pending_loan.isvoid = 0 "
            'strQ &= " WHERE lnloan_process_pending_loan.isvoid = 0 AND lnloan_approver_mapping.userunkid =  " & xUserUnkid
            'Nilay (01-Mar-2016) -- End

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            '",-1 AS employeeunkid " & _ -- REMOVED
            '",lnloanapproval_process_tran.employeeunkid " & _ -- ADDED
            'Nilay (01-Apr-2016) -- End

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " UNION " & _
                       " SELECT " & _
                            " lnloanapproval_process_tran.pendingloantranunkid  " & _
                            ",lnloan_process_pending_loan.processpendingloanunkid  " & _
                            ",'' AS application_no " & _
                            ",lnloan_process_pending_loan.application_date " & _
                            ",lnloanapproval_process_tran.employeeunkid " & _
                            ",ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.othername, '') + ' ' + ISNULL(Emp.surname, '') AS Employee  " & _
                            ",lnloanapproval_process_tran.approvertranunkid AS Approverunkid  " & _
                            ",lnloanapproval_process_tran.approverempunkid " & _
                            ",lnapproverlevel_master.lnlevelunkid  " & _
                            ",lnloanapproval_process_tran.priority  " & _
                            " ,#APPROVER_NAME# AS Approver " & _
                            " ,#APPROVER_NAME# + ' - ' + ISNULL(lnapproverlevel_master.lnlevelname, '') AS ApproverName " & _
                            ",lnloan_process_pending_loan.loanschemeunkid   " & _
                            ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS [Scheme]  " & _
                            ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END as loan_advance " & _
                            ",lnloanapproval_process_tran.deductionperiodunkid  " & _
                            ",ISNULL(cfcommon_period_tran.period_name, '') AS deduct_period  " & _
                            ",ISNULL(lnloan_process_pending_loan.loan_amount, 0.00) AS loan_amount " & _
                            ",ISNULL(lnloanapproval_process_tran.loan_amount, 0.00) AS approved_amount  " & _
                            ",ISNULL(lnloanapproval_process_tran.duration, 0.00) AS duration  " & _
                            ",ISNULL(lnloanapproval_process_tran.installmentamt, 0.00) AS installmentamt  " & _
                            ",ISNULL(lnloanapproval_process_tran.noofinstallment, 0.00) AS noofinstallment  " & _
                            ",lnloan_process_pending_loan.loan_statusunkid  " & _
                            ",lnloanapproval_process_tran.statusunkid " & _
                            ",CASE WHEN lnloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.PENDING & " THEN @Pending " & _
                                 " WHEN lnloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.APPROVED & " THEN @Approved " & _
                                 " WHEN lnloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.REJECTED & " THEN @Reject " & _
                                 " WHEN lnloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.ASSIGNED & " THEN @Assigned " & _
                            " END AS status  " & _
                            ",lnloanapproval_process_tran.countryunkid AS currencyunkid  " & _
                            ",lnloanapproval_process_tran.remark  " & _
                            ",lnloan_process_pending_loan.isloan  " & _
                            ",lnloan_process_pending_loan.isexternal_entity  " & _
                            ",lnloan_process_pending_loan.external_entity_name  " & _
                            ",ISNULL(ETT.stationunkid, 0) AS stationunkid  " & _
                            ",ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid  " & _
                            ",ISNULL(ETT.departmentunkid, 0) AS departmentunkid  " & _
                            ",ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid  " & _
                            ",ISNULL(ETT.sectionunkid, 0) AS sectionunkid  " & _
                            ",ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid  " & _
                            ",ISNULL(ETT.unitunkid, 0) AS unitunkid  " & _
                            ",ISNULL(ETT.teamunkid, 0) AS teamunkid  " & _
                            ",ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid  " & _
                            ",ISNULL(ETT.classunkid, 0) AS classunkid  " & _
                            ",ISNULL(ECT.jobgroupunkid, 0) AS jobgroupunkid  " & _
                            ",ISNULL(ECT.jobunkid, 0) AS jobunkid  " & _
                            ",ISNULL(GRD.gradegroupunkid, 0) AS gradegroupunkid  " & _
                            ",ISNULL(GRD.gradeunkid, 0) AS gradeunkid  " & _
                            ",ISNULL(GRD.gradelevelunkid, 0) AS gradelevelunkid  " & _
                            ",lnloanapproval_process_tran.isvoid " & _
                            ",lnloanapproval_process_tran.voiddatetime  " & _
                            ",lnloanapproval_process_tran.voidreason  " & _
                            ",lnloanapproval_process_tran.voiduserunkid  " & _
                            ",lnloan_approver_mapping.userunkid AS MappedUserId " & _
                            ",lnloanapproval_process_tran.visibleid " & _
                            ",0 AS isgrp " & _
                        " FROM lnloanapproval_process_tran " & _
                            " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
                            " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = lnloanapproval_process_tran.employeeunkid " & _
                            " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid " & _
                            " #APPROVER_JOIN# " & _
                            " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                            " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                            " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanapproval_process_tran.deductionperiodunkid " & _
                            " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid  " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        stationunkid " & _
                            "       ,deptgroupunkid " & _
                            "       ,departmentunkid " & _
                            "       ,sectiongroupunkid " & _
                            "       ,sectionunkid " & _
                            "       ,unitgroupunkid " & _
                            "       ,unitunkid " & _
                            "       ,teamunkid " & _
                            "       ,classgroupunkid " & _
                            "       ,classunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ETT ON ETT.employeeunkid = Emp.employeeunkid AND ETT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobgroupunkid " & _
                            "       ,jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ECT ON ECT.employeeunkid = Emp.employeeunkid AND ECT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        gradegroupunkid " & _
                            "       ,gradeunkid " & _
                            "       ,gradelevelunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS GRD ON GRD.employeeunkid = Emp.employeeunkid AND GRD.rno = 1 "
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'ADD Enum in [status]
            'Nilay (20-Sept-2016) -- End

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry '.Replace("hremployee_master", "Emp")
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry '.Replace("hremployee_master", "Emp")
            End If

            StrQCondition &= " WHERE   lnloanapproval_process_tran.isvoid = 0 "

            StrQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry '.Replace("hremployee_master", "Emp") & " "
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrFilter
            End If

            If strAdvanceFilter.Trim.Length > 0 Then

                'Varsha Rana (19-Sept-2017) -- Start
                'Issue - The multi-part identifier "hremployee_master.maritalstatusunkid" could not be bound.
                'StrQCondition &= " AND " & strAdvanceFilter.Replace("ADF", "Emp")
                StrQCondition &= " AND " & strAdvanceFilter.Replace("ADF", "Emp").Replace("hremployee_master", "Emp")
                'Varsha Rana (19-Sept-2017) -- End


            End If

            'strQ &= " ORDER BY lnloan_process_pending_loan.processpendingloanunkid DESC,  application_no   DESC,priority ,isgrp DESC "

            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') ")
            strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master AS App ON App.employeeunkid = lnloanapproval_process_tran.approverempunkid AND App.isapproved = 1 ")
            strQ &= StrQCondition
            strQ = strQ.Replace("#EXT_APPROVER#", "0")
            strQ &= StrQDtFilters

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Rejected"))
            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))
            objDataOperation.AddParameter("@LoginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
            'Nilay (28-Aug-2015) -- Start
            'Sohail (15 Jul 2020) -- Start
            'ZRA Issue # : : Language issue on process loan screen.
            'objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 11, "Loan"))
            'objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 12, "Advance"))
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 3, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 4, "Advance"))
            'Sohail (15 Jul 2020) -- End
            'Nilay (28-Aug-2015) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = StrFinalQurey
                StrQDtFilters = ""

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanapprover_master.approverempunkid ")

                    strQ &= StrQCondition
                Else
                    'xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dRow("effectivedate")), eZeeDate.convertDate(dRow("effectivedate")), , , dRow("dbname").ToString, "Emp")
                    'Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dRow("effectivedate")), dRow("dbname").ToString, "Emp")

                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') END ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master AS App ON App.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry '.Replace("hremployee_master", "Emp")
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry '.Replace("hremployee_master", "Emp")
                    End If

                    strQ &= StrQCondition

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry '.Replace("hremployee_master", "Emp") & " "
                        End If
                    End If

                    If strAdvanceFilter.Trim.Length > 0 Then
                        strQ &= " AND " & strAdvanceFilter.Replace("ADF", "Emp")
                    End If

                End If

                strQ = strQ.Replace("#EXT_APPROVER#", "1")

                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
                objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
                objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Rejected"))
                objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))
                objDataOperation.AddParameter("@LoginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
                'Sohail (15 Jul 2020) -- Start
                'ZRA Issue # : : Language issue on process loan screen.
                'objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 11, "Loan"))
                'objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 12, "Advance"))
                objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 3, "Loan"))
                objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 4, "Advance"))
                'Sohail (15 Jul 2020) -- End

                dsExtList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(0).Copy)
                Else
                    Dim dtExtList As DataTable = New DataView(dsExtList.Tables("List"), "isgrp = 0", "", DataViewRowState.CurrentRows).ToTable
                    dsExtList.Tables.RemoveAt(0)
                    dsExtList.Tables.Add(dtExtList.Copy)
                    dsList.Tables(0).Merge(dsExtList.Tables(0), True)

                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "processpendingloanunkid DESC, application_no DESC,priority ,isgrp DESC", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal xDatabaseName As String, _
    '                        ByVal xUserUnkid As Integer, _
    '                        ByVal xYearUnkid As Integer, _
    '                        ByVal xCompanyUnkid As Integer, _
    '                        ByVal xPeriodStart As DateTime, _
    '                        ByVal xPeriodEnd As DateTime, _
    '                        ByVal xUserModeSetting As String, _
    '                        ByVal xOnlyApproved As Boolean, _
    '                        ByVal xIncludeIn_ActiveEmployee As Boolean, _
    '                        ByVal strTableName As String, _
    '                        Optional ByVal mstrFilter As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        Dim xAdvanceJoinQry As String = ""
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

    '        strQ = "SELECT " & _
    '                    " -1 AS pendingloantranunkid  " & _
    '                    ",lnloan_process_pending_loan.processpendingloanunkid  " & _
    '                    ",lnloan_process_pending_loan.application_no " & _
    '                    ",NULL AS application_date " & _
    '                    ",-1 AS employeeunkid  " & _
    '                    ",'' AS Employee " & _
    '                    ",-1 AS Approverunkid  " & _
    '                    ",-1 AS approverempunkid  " & _
    '                    ",-1 AS lnlevelunkid " & _
    '                    ",-1 AS priority " & _
    '                    ",'' AS Approver " & _
    '                    ",'' AS ApproverName  " & _
    '                    ",-1 AS loanschemeunkid  " & _
    '                    ",'' AS Scheme  " & _
    '                    ",'' AS loan_advance  " & _
    '                    ",-1 AS deductionperiodunkid  " & _
    '                    ",'' AS deduct_period  " & _
    '                    ",NULL AS loan_amount " & _
    '                    ",NULL AS approved_amount " & _
    '                    ",0 AS duration " & _
    '                    ",0 AS installmentamt " & _
    '                    ",0 AS noofinstallment " & _
    '                    ",-1 AS loan_statusunkid  " & _
    '                    ",-1 AS statusunkid  " & _
    '                    ",'' AS status " & _
    '                    ",-1 AS currencyunkid  " & _
    '                    ",'' AS remark  " & _
    '                    ",lnloan_process_pending_loan.isloan  " & _
    '                    ",lnloan_process_pending_loan.isexternal_entity  " & _
    '                    ",lnloan_process_pending_loan.external_entity_name  " & _
    '                    ",-1 AS sectiongroupunkid " & _
    '                    ",-1 AS unitgroupunkid " & _
    '                    ",-1 AS teamunkid " & _
    '                    ",-1 AS stationunkid " & _
    '                    ",-1 AS deptgroupunkid " & _
    '                    ",-1 AS departmentunkid " & _
    '                    ",-1 AS sectionunkid " & _
    '                    ",-1 AS unitunkid " & _
    '                    ",-1 AS jobunkid " & _
    '                    ",-1 AS classgroupunkid " & _
    '                    ",-1 AS classunkid " & _
    '                    ",-1 AS jobgroupunkid " & _
    '                    ",-1 AS gradegroupunkid " & _
    '                    ",-1 AS gradeunkid " & _
    '                    ",-1 AS gradelevelunkid " & _
    '                    ",0 AS isvoid " & _
    '                    ",NULL AS voiddatetime  " & _
    '                    ",'' AS voidreason  " & _
    '                    ",-1 AS voiduserunkid  " & _
    '                    ",-1 AS MappedUserId " & _
    '                    ", 0 AS visibleid " & _
    '                    ",1 AS isgrp " & _
    '            " FROM lnloan_process_pending_loan " & _
    '                    " LEFT JOIN lnloanapproval_process_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
    '                    " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = lnloanapproval_process_tran.employeeunkid " & _
    '                    " LEFT JOIN hremployee_master App ON App.employeeunkid = lnloanapproval_process_tran.approverempunkid " & _
    '                    " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid " & _
    '                    " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                    " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                    " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanapproval_process_tran.deductionperiodunkid " & _
    '                    " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid  "

    '        'Nilay (28-Aug-2015) -- Start
    '        '",'' AS loan_advance  " & _ --- Added
    '        'Nilay (28-Aug-2015) -- End

    '        'Nilay (21-Oct-2015) -- Start
    '        'ENHANCEMENT : NEW LOAN Given By Rutta
    '        '",NULL AS approved_amount " & _ --- ADDED
    '        'Nilay (21-Oct-2015) -- End

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            strQ &= xAdvanceJoinQry.Replace("hremployee_master", "Emp")
    '        End If

    '        strQ &= " WHERE   lnloan_process_pending_loan.isvoid = 0 "

    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrFilter
    '        End If

    '        strQ &= " UNION " & _
    '                   " SELECT " & _
    '                        " lnloanapproval_process_tran.pendingloantranunkid  " & _
    '                        ",lnloan_process_pending_loan.processpendingloanunkid  " & _
    '                        ",'' AS application_no " & _
    '                        ",lnloan_process_pending_loan.application_date " & _
    '                        ",lnloanapproval_process_tran.employeeunkid " & _
    '                        ",ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.othername, '') + ' ' + ISNULL(Emp.surname, '') AS Employee  " & _
    '                        ",lnloanapproval_process_tran.approvertranunkid AS Approverunkid  " & _
    '                        ",lnloanapproval_process_tran.approverempunkid " & _
    '                        ",lnapproverlevel_master.lnlevelunkid  " & _
    '                        ",lnloanapproval_process_tran.priority  " & _
    '                        ",ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') AS Approver " & _
    '                        ",ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') + ' - ' + ISNULL(lnapproverlevel_master.lnlevelname, '') AS ApproverName " & _
    '                        ",lnloan_process_pending_loan.loanschemeunkid   " & _
    '                        ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS [Scheme]  " & _
    '                        ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END as loan_advance " & _
    '                        ",lnloanapproval_process_tran.deductionperiodunkid  " & _
    '                        ",ISNULL(cfcommon_period_tran.period_name, '') AS deduct_period  " & _
    '                        ",ISNULL(lnloan_process_pending_loan.loan_amount, 0.00) AS loan_amount " & _
    '                        ",ISNULL(lnloanapproval_process_tran.loan_amount, 0.00) AS approved_amount  " & _
    '                        ",ISNULL(lnloanapproval_process_tran.duration, 0.00) AS duration  " & _
    '                        ",ISNULL(lnloanapproval_process_tran.installmentamt, 0.00) AS installmentamt  " & _
    '                        ",ISNULL(lnloanapproval_process_tran.noofinstallment, 0.00) AS noofinstallment  " & _
    '                        ",lnloan_process_pending_loan.loan_statusunkid  " & _
    '                        ",lnloanapproval_process_tran.statusunkid " & _
    '                        ",CASE WHEN lnloanapproval_process_tran.statusunkid = 1 THEN @Pending " & _
    '                             " WHEN lnloanapproval_process_tran.statusunkid = 2 THEN @Approved " & _
    '                             " WHEN lnloanapproval_process_tran.statusunkid = 3 THEN @Reject " & _
    '                             " WHEN lnloanapproval_process_tran.statusunkid = 4 THEN @Assigned " & _
    '                        " END AS status  " & _
    '                        ",lnloanapproval_process_tran.countryunkid AS currencyunkid  " & _
    '                        ",lnloanapproval_process_tran.remark  " & _
    '                        ",lnloan_process_pending_loan.isloan  " & _
    '                        ",lnloan_process_pending_loan.isexternal_entity  " & _
    '                        ",lnloan_process_pending_loan.external_entity_name  " & _
    '                        ",ISNULL(ETT.stationunkid, 0) AS stationunkid  " & _
    '                        ",ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid  " & _
    '                        ",ISNULL(ETT.departmentunkid, 0) AS departmentunkid  " & _
    '                        ",ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid  " & _
    '                        ",ISNULL(ETT.sectionunkid, 0) AS sectionunkid  " & _
    '                        ",ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid  " & _
    '                        ",ISNULL(ETT.unitunkid, 0) AS unitunkid  " & _
    '                        ",ISNULL(ETT.teamunkid, 0) AS teamunkid  " & _
    '                        ",ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid  " & _
    '                        ",ISNULL(ETT.classunkid, 0) AS classunkid  " & _
    '                        ",ISNULL(ECT.jobgroupunkid, 0) AS jobgroupunkid  " & _
    '                        ",ISNULL(ECT.jobunkid, 0) AS jobunkid  " & _
    '                        ",ISNULL(GRD.gradegroupunkid, 0) AS gradegroupunkid  " & _
    '                        ",ISNULL(GRD.gradeunkid, 0) AS gradeunkid  " & _
    '                        ",ISNULL(GRD.gradelevelunkid, 0) AS gradelevelunkid  " & _
    '                        ",lnloanapproval_process_tran.isvoid " & _
    '                        ",lnloanapproval_process_tran.voiddatetime  " & _
    '                        ",lnloanapproval_process_tran.voidreason  " & _
    '                        ",lnloanapproval_process_tran.voiduserunkid  " & _
    '                        ",lnloan_approver_mapping.userunkid AS MappedUserId " & _
    '                        ",lnloanapproval_process_tran.visibleid " & _
    '                        ",0 AS isgrp " & _
    '                    " FROM lnloanapproval_process_tran " & _
    '                        " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
    '                        " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = lnloanapproval_process_tran.employeeunkid " & _
    '                        " LEFT JOIN hremployee_master App ON App.employeeunkid = lnloanapproval_process_tran.approverempunkid " & _
    '                        " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid " & _
    '                        " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                        " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                        " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanapproval_process_tran.deductionperiodunkid " & _
    '                        " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid  " & _
    '                        " LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        stationunkid " & _
    '                        "       ,deptgroupunkid " & _
    '                        "       ,departmentunkid " & _
    '                        "       ,sectiongroupunkid " & _
    '                        "       ,sectionunkid " & _
    '                        "       ,unitgroupunkid " & _
    '                        "       ,unitunkid " & _
    '                        "       ,teamunkid " & _
    '                        "       ,classgroupunkid " & _
    '                        "       ,classunkid " & _
    '                        "       ,employeeunkid " & _
    '                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                        "   FROM hremployee_transfer_tran " & _
    '                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                        ") AS ETT ON ETT.employeeunkid = Emp.employeeunkid AND ETT.rno = 1 " & _
    '                        " LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        jobgroupunkid " & _
    '                        "       ,jobunkid " & _
    '                        "       ,employeeunkid " & _
    '                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                        "   FROM hremployee_categorization_tran " & _
    '                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                        ") AS ECT ON ECT.employeeunkid = Emp.employeeunkid AND ECT.rno = 1 " & _
    '                        " LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        gradegroupunkid " & _
    '                        "       ,gradeunkid " & _
    '                        "       ,gradelevelunkid " & _
    '                        "       ,employeeunkid " & _
    '                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
    '                        "   FROM prsalaryincrement_tran " & _
    '                        "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                        ") AS GRD ON GRD.employeeunkid = Emp.employeeunkid AND GRD.rno = 1 "

    '        'Nilay (21-Oct-2015) -- Start
    '        'ENHANCEMENT : NEW LOAN Given By Rutta
    '        '",ISNULL(lnloanapproval_process_tran.loan_amount, 0.00) AS loan_amount  " & _ --- Replaced loan_amount with approved_amount
    '        '",ISNULL(lnloan_process_pending_loan.loan_amount, 0.00) AS loan_amount " & _ --- ADDED
    '        'Nilay (21-Oct-2015) -- End

    '        'Nilay (28-Aug-2015) -- Start
    '        ''",ISNULL(lnloan_scheme_master.name, '') AS [Scheme]  " & _ --- Removed
    '        '",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS [Scheme]  " & _ --- Added
    '        '",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END as loan_advance " & _ --- Added
    '        'Nilay (28-Aug-2015) -- End

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            strQ &= xAdvanceJoinQry.Replace("hremployee_master", "Emp")
    '        End If

    '        strQ &= " WHERE   lnloanapproval_process_tran.isvoid = 0 "

    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrFilter
    '        End If

    '        strQ &= " ORDER BY lnloan_process_pending_loan.processpendingloanunkid DESC,  application_no   DESC,priority ,isgrp DESC "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Rejected"))
    '        objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))
    '        objDataOperation.AddParameter("@LoginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
    '        'Nilay (28-Aug-2015) -- Start
    '        objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 11, "Loan"))
    '        objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 12, "Advance"))
    '        'Nilay (28-Aug-2015) -- End

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    'Nilay (01-Mar-2016) -- End

    

    'Public Function GetList(ByVal intLoginUserId As Integer, Optional ByVal mstrFilter As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '                    " -1 AS pendingloantranunkid  " & _
    '                    ",lnloan_process_pending_loan.processpendingloanunkid  " & _
    '                 ",lnloan_process_pending_loan.application_no " & _
    '                 ",NULL AS application_date " & _
    '                    ",-1 AS employeeunkid  " & _
    '                 ",'' AS Employee " & _
    '                    ",-1 AS Approverunkid  " & _
    '                    ",-1 AS approverempunkid  " & _
    '                 ",-1 AS lnlevelunkid " & _
    '                 ",-1 AS priority " & _
    '                     ",'' AS Approver " & _
    '                    ",'' AS ApproverName  " & _
    '                    ",-1 AS loanschemeunkid  " & _
    '                    ",'' AS Scheme  " & _
    '                    ",'' AS loan_advance  " & _
    '                    ",-1 AS deductionperiodunkid  " & _
    '                    ",'' AS deduct_period  " & _
    '                 ",NULL AS loan_amount " & _
    '                 ",0 AS duration " & _
    '                 ",0 AS installmentamt " & _
    '                 ",0 AS noofinstallment " & _
    '                    ",-1 AS loan_statusunkid  " & _
    '                    ",-1 AS statusunkid  " & _
    '                 ",'' AS status " & _
    '                    ",-1 AS currencyunkid  " & _
    '                    ",'' AS remark  " & _
    '                    ",lnloan_process_pending_loan.isloan  " & _
    '                    ",lnloan_process_pending_loan.isexternal_entity  " & _
    '                    ",lnloan_process_pending_loan.external_entity_name  " & _
    '                 ",-1 AS sectiongroupunkid " & _
    '                 ",-1 AS unitgroupunkid " & _
    '                 ",-1 AS teamunkid " & _
    '                 ",-1 AS stationunkid " & _
    '                 ",-1 AS deptgroupunkid " & _
    '                 ",-1 AS departmentunkid " & _
    '                 ",-1 AS sectionunkid " & _
    '                 ",-1 AS unitunkid " & _
    '                 ",-1 AS jobunkid " & _
    '                 ",-1 AS classgroupunkid " & _
    '                 ",-1 AS classunkid " & _
    '                 ",-1 AS jobgroupunkid " & _
    '                 ",-1 AS gradegroupunkid " & _
    '                 ",-1 AS gradeunkid " & _
    '                 ",-1 AS gradelevelunkid " & _
    '                 ",0 AS isvoid " & _
    '                    ",NULL AS voiddatetime  " & _
    '                    ",'' AS voidreason  " & _
    '                    ",-1 AS voiduserunkid  " & _
    '                    ",-1 AS MappedUserId " & _
    '                    ", 0 AS visibleid " & _
    '                 ",1 AS isgrp " & _
    '            " FROM lnloan_process_pending_loan " & _
    '                    " LEFT JOIN lnloanapproval_process_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
    '                    " LEFT JOIN hremployee_master Emp ON Emp.employeeunkid = lnloanapproval_process_tran.employeeunkid " & _
    '                    " LEFT JOIN hremployee_master App ON App.employeeunkid = lnloanapproval_process_tran.approverempunkid " & _
    '                    " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid " & _
    '                                        " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                    " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                    " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanapproval_process_tran.deductionperiodunkid " & _
    '                    " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid  " & _
    '                    " WHERE   lnloan_process_pending_loan.isvoid = 0 "

    '        'Nilay (28-Aug-2015) -- Start
    '        '",'' AS loan_advance  " & _ --- Added
    '        'Nilay (28-Aug-2015) -- End

    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrFilter
    '        End If

    '        strQ &= " UNION " & _
    '                   " SELECT " & _
    '                    " lnloanapproval_process_tran.pendingloantranunkid  " & _
    '                    ",lnloan_process_pending_loan.processpendingloanunkid  " & _
    '                    ",'' AS application_no " & _
    '                    ",lnloan_process_pending_loan.application_date " & _
    '                    ",lnloanapproval_process_tran.employeeunkid " & _
    '                    ",ISNULL(emp.employeecode, '') + ' - ' + ISNULL(emp.firstname, '') + ' ' + ISNULL(emp.othername, '') + ' ' + ISNULL(emp.surname, '') AS Employee  " & _
    '                    ",lnloanapproval_process_tran.approvertranunkid AS Approverunkid  " & _
    '                    ",lnloanapproval_process_tran.approverempunkid " & _
    '                    ",lnapproverlevel_master.lnlevelunkid  " & _
    '                    ",lnloanapproval_process_tran.priority  " & _
    '                    ",ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') AS Approver " & _
    '                    ",ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') + ' - ' + ISNULL(lnapproverlevel_master.lnlevelname, '') AS ApproverName " & _
    '                    ",lnloan_process_pending_loan.loanschemeunkid   " & _
    '                    ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS [Scheme]  " & _
    '                    ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END as loan_advance " & _
    '                    ",lnloanapproval_process_tran.deductionperiodunkid  " & _
    '                    ",ISNULL(cfcommon_period_tran.period_name, '') AS deduct_period  " & _
    '                    ",ISNULL(lnloanapproval_process_tran.loan_amount, 0.00) AS loan_amount  " & _
    '                    ",ISNULL(lnloanapproval_process_tran.duration, 0.00) AS duration  " & _
    '                    ",ISNULL(lnloanapproval_process_tran.installmentamt, 0.00) AS installmentamt  " & _
    '                    ",ISNULL(lnloanapproval_process_tran.noofinstallment, 0.00) AS noofinstallment  " & _
    '                    ",lnloan_process_pending_loan.loan_statusunkid  " & _
    '                    ",lnloanapproval_process_tran.statusunkid " & _
    '                    ",CASE WHEN lnloanapproval_process_tran.statusunkid = 1 THEN @Pending " & _
    '                         "WHEN lnloanapproval_process_tran.statusunkid = 2 THEN @Approved " & _
    '                         "WHEN lnloanapproval_process_tran.statusunkid = 3 THEN @Reject " & _
    '                         "WHEN lnloanapproval_process_tran.statusunkid = 4 THEN @Assigned " & _
    '                    " END AS status  " & _
    '                    ",lnloanapproval_process_tran.countryunkid AS currencyunkid  " & _
    '                    ",lnloanapproval_process_tran.remark  " & _
    '                    ",lnloan_process_pending_loan.isloan  " & _
    '                    ",lnloan_process_pending_loan.isexternal_entity  " & _
    '                    ",lnloan_process_pending_loan.external_entity_name  " & _
    '                    ",ISNULL(Emp.sectiongroupunkid, 0) AS sectiongroupunkid  " & _
    '                    ",ISNULL(Emp.unitgroupunkid, 0) AS unitgroupunkid  " & _
    '                    ",ISNULL(Emp.teamunkid, 0) AS teamunkid  " & _
    '                    ",ISNULL(Emp.stationunkid, 0) AS stationunkid  " & _
    '                    ",ISNULL(Emp.deptgroupunkid, 0) AS deptgroupunkid  " & _
    '                    ",ISNULL(Emp.departmentunkid, 0) AS departmentunkid  " & _
    '                    ",ISNULL(Emp.sectionunkid, 0) AS sectionunkid  " & _
    '                    ",ISNULL(Emp.unitunkid, 0) AS unitunkid  " & _
    '                    ",ISNULL(Emp.jobunkid, 0) AS jobunkid  " & _
    '                    ",ISNULL(Emp.classgroupunkid, 0) AS classgroupunkid  " & _
    '                    ",ISNULL(Emp.classunkid, 0) AS classunkid  " & _
    '                    ",ISNULL(Emp.jobgroupunkid, 0) AS jobgroupunkid  " & _
    '                    ",ISNULL(Emp.gradegroupunkid, 0) AS gradegroupunkid  " & _
    '                    ",ISNULL(Emp.gradeunkid, 0) AS gradeunkid  " & _
    '                    ",ISNULL(Emp.gradelevelunkid, 0) AS gradelevelunkid  " & _
    '                    ", lnloanapproval_process_tran.isvoid " & _
    '                    ",lnloanapproval_process_tran.voiddatetime  " & _
    '                    ",lnloanapproval_process_tran.voidreason  " & _
    '                    ",lnloanapproval_process_tran.voiduserunkid  " & _
    '                    ",lnloan_approver_mapping.userunkid AS MappedUserId " & _
    '                    ",lnloanapproval_process_tran.visibleid " & _
    '                    ",0 AS isgrp " & _
    '                    " FROM lnloanapproval_process_tran " & _
    '                    " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
    '                    " LEFT JOIN hremployee_master Emp ON Emp.employeeunkid = lnloanapproval_process_tran.employeeunkid " & _
    '                    " LEFT JOIN hremployee_master App ON App.employeeunkid = lnloanapproval_process_tran.approverempunkid " & _
    '                    " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid " & _
    '                    " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                    " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                    " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanapproval_process_tran.deductionperiodunkid " & _
    '                    " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid  " & _
    '                    " WHERE   lnloanapproval_process_tran.isvoid = 0 "

    '        'Nilay (28-Aug-2015) -- Start
    '        ''",ISNULL(lnloan_scheme_master.name, '') AS [Scheme]  " & _ --- Removed
    '        '",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS [Scheme]  " & _ --- Added
    '        '",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END as loan_advance " & _ --- Added
    '        'Nilay (28-Aug-2015) -- End

    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrFilter
    '        End If

    '        strQ &= " ORDER BY lnloan_process_pending_loan.processpendingloanunkid DESC,  application_no   DESC,priority ,isgrp DESC "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Rejected"))
    '        objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))
    '        objDataOperation.AddParameter("@LoginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoginUserId)
    '        'Nilay (28-Aug-2015) -- Start
    '        objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 11, "Loan"))
    '        objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 12, "Advance"))
    '        'Nilay (28-Aug-2015) -- End

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (10-Oct-2015) -- End

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Public Function GetApprovalTranList(ByVal xDatabaseName As String, _
                                        ByVal xUserUnkid As Integer, _
                                        ByVal xYearUnkid As Integer, _
                                        ByVal xCompanyUnkid As Integer, _
                                        ByVal xPeriodStart As DateTime, _
                                        ByVal xPeriodEnd As DateTime, _
                                        ByVal xUserModeSetting As String, _
                                        ByVal xOnlyApproved As Boolean, _
                                        ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                        ByVal strTableName As String, _
                                        Optional ByVal intEmployeeID As Integer = 0, _
                                        Optional ByVal intLoanApplicationId As Integer = 0, _
                                        Optional ByVal mstrFilter As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = " SELECT " & _
                        " lnloan_process_pending_loan.processpendingloanunkid " & _
                        ",lnloanapproval_process_tran.pendingloantranunkid " & _
                        ",lnloan_process_pending_loan.application_no " & _
                        ",lnloan_process_pending_loan.application_date " & _
                        ",ISNULL(lnloan_scheme_master.loanschemeunkid,-1) AS loanschemeunkid " & _
                        ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS 'Scheme' " & _
                        ",lnloan_process_pending_loan.external_entity_name " & _
                        ",lnloan_process_pending_loan.isexternal_entity " & _
                        ", ISNULL(emp.firstname,'') + ' ' + ISNULL(emp.othername,'') + ' ' + ISNULL(emp.surname,'') AS Employee " & _
                        " ,#APPROVER_NAME# + ' - ' + lnapproverlevel_master.lnlevelname AS ApproverName " & _
                        ",lnapproverlevel_master.lnlevelunkid " & _
                        ",lnapproverlevel_master.lnlevelname " & _
                        ",lnapproverlevel_master.priority " & _
                        ",cfcommon_period_tran.period_name " & _
                        ",lnloanapproval_process_tran.loan_amount " & _
                        ",lnloanapproval_process_tran.duration " & _
                        ",lnloanapproval_process_tran.installmentamt " & _
                        ",lnloanapproval_process_tran.noofinstallment " & _
                        ",CASE WHEN lnloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.PENDING & " then @Pending " & _
                        "      WHEN lnloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.APPROVED & " then @Approved " & _
                        "      WHEN lnloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.REJECTED & " then @Reject " & _
                        "      WHEN lnloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.ASSIGNED & " then @Assigned end as status " & _
                        ",lnloanapproval_process_tran.countryunkid " & _
                        ",lnloanapproval_process_tran.employeeunkid " & _
                        ",lnloanapproval_process_tran.approvertranunkid " & _
                        ",lnloanapproval_process_tran.approverempunkid " & _
                        ",lnloanapproval_process_tran.userunkid " & _
                        ",lnloanapproval_process_tran.statusunkid " & _
                        ",ISNULL(lnloan_process_pending_loan.loan_statusunkid,1) as 'loan_statusunkid' " & _
                        ",ISNULL(ETT.stationunkid,0) AS stationunkid " & _
                        ",ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
                        ",ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                        ",ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                        ",ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
                        ",ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                        ",ISNULL(ETT.unitunkid,0) AS unitunkid " & _
                        ",ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                        ",ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
                        ",ISNULL(ETT.classunkid,0) AS classunkid " & _
                        ",ISNULL(ECT.jobgroupunkid,0) AS jobgroupunkid " & _
                        ",ISNULL(ECT.jobunkid,0) AS jobunkid " & _
                        ",ISNULL(GRD.gradegroupunkid,0) AS gradegroupunkid " & _
                        ",ISNULL(GRD.gradeunkid,0) AS gradeunkid " & _
                        ",ISNULL(GRD.gradelevelunkid,0) AS gradelevelunkid " & _
                        ",lnloanapproval_process_tran.isvoid " & _
                        ",lnloan_process_pending_loan.isloan " & _
                        ",lnloanapproval_process_tran.remark " & _
                    " FROM lnloanapproval_process_tran " & _
                        " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
                        " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                        " LEFT JOIN hremployee_master emp ON lnloanapproval_process_tran.employeeunkid = emp.employeeunkid " & _
                        " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid AND lnloanapprover_master.approverempunkid = lnloanapproval_process_tran.approverempunkid " & _
                        " #APPROVER_JOIN# " & _
                        " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                        " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_process_pending_loan.deductionperiodunkid " & _
                        " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        stationunkid " & _
                            "       ,deptgroupunkid " & _
                            "       ,departmentunkid " & _
                            "       ,sectiongroupunkid " & _
                            "       ,sectionunkid " & _
                            "       ,unitgroupunkid " & _
                            "       ,unitunkid " & _
                            "       ,teamunkid " & _
                            "       ,classgroupunkid " & _
                            "       ,classunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ETT ON ETT.employeeunkid = emp.employeeunkid AND ETT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobgroupunkid " & _
                            "       ,jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ECT ON ECT.employeeunkid = emp.employeeunkid AND ECT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        gradegroupunkid " & _
                            "       ,gradeunkid " & _
                            "       ,gradelevelunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS GRD ON GRD.employeeunkid = emp.employeeunkid AND GRD.rno = 1 "
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'ADD Enum in [status]
            'Nilay (20-Sept-2016) -- End



            'Pinkal (23-Feb-2018) --   'Enhancement - Working on B5 Customize Medical Reports.[",lnloanapproval_process_tran.remark " & _]


            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "emp")
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry.Replace("hremployee_master", "emp")
            End If

            StrQCondition &= " WHERE lnloanapproval_process_tran.isvoid = 0 "

            StrQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry.Replace("hremployee_master", "emp") & " "
                End If
            End If

            If intEmployeeID > 0 Then
                StrQCondition &= " AND lnloanapproval_process_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            End If

            If intLoanApplicationId > 0 Then
                StrQCondition &= " AND lnloanapproval_process_tran.processpendingloanunkid = @processpendingloanunkid "
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanApplicationId)
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrFilter
            End If

            'strQ &= " ORDER BY application_no, priority desc "

            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(app.firstname, '') + ' ' + ISNULL(app.othername, '') + ' ' + ISNULL(app.surname, '') ")
            strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master AS app ON app.employeeunkid = lnloanapproval_process_tran.approverempunkid AND app.isapproved = 1 ")
            strQ &= StrQCondition
            strQ = strQ.Replace("#EXT_APPROVER#", "0")
            strQ &= StrQDtFilters

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Reject"))
            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))
            'Sohail (15 Jul 2020) -- Start
            'ZRA Issue # : : Language issue on process loan screen.
            'objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 12, "Advance"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 4, "Advance"))
            'Sohail (15 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = StrFinalQurey
                StrQDtFilters = ""

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanapprover_master.approverempunkid ")
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dRow("effectivedate")), eZeeDate.convertDate(dRow("effectivedate")), , , dRow("dbname").ToString)
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dRow("effectivedate")), dRow("dbname").ToString)

                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') END ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master AS App ON App.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry.Replace("hremployee_master", "emp")
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry.Replace("hremployee_master", "emp")
                    End If
                End If

                strQ &= StrQCondition

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQDtFilters &= xDateFilterQry.Replace("hremployee_master", "Emp") & " "
                    End If
                End If

                strQ = strQ.Replace("#EXT_APPROVER#", "1")

                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                If intEmployeeID > 0 Then
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
                End If
                If intLoanApplicationId > 0 Then
                    objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanApplicationId)
                End If
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
                objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
                objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Reject"))
                objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))
                'Sohail (15 Jul 2020) -- Start
                'ZRA Issue # : : Language issue on process loan screen.
                'objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 12, "Advance"))
                objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 4, "Advance"))
                'Sohail (15 Jul 2020) -- End

                dsExtList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dsExtList.Tables(0), True)
                End If

            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "application_no, priority desc", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApprovalTranList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetApprovalTranList(ByVal xDatabaseName As String, _
    '                                    ByVal xUserUnkid As Integer, _
    '                                    ByVal xYearUnkid As Integer, _
    '                                    ByVal xCompanyUnkid As Integer, _
    '                                    ByVal xPeriodStart As DateTime, _
    '                                    ByVal xPeriodEnd As DateTime, _
    '                                    ByVal xUserModeSetting As String, _
    '                                    ByVal xOnlyApproved As Boolean, _
    '                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
    '                                    ByVal strTableName As String, _
    '                                    Optional ByVal intEmployeeID As Integer = 0, _
    '                                    Optional ByVal intLoanApplicationId As Integer = 0, _
    '                                    Optional ByVal mstrFilter As String = "") As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation
    '    objDataOperation.ClearParameters()

    '    Try
    '        strQ = " SELECT " & _
    '                    " lnloan_process_pending_loan.processpendingloanunkid " & _
    '                    ",lnloanapproval_process_tran.pendingloantranunkid " & _
    '                    ",lnloan_process_pending_loan.application_no " & _
    '                    ",lnloan_process_pending_loan.application_date " & _
    '                    ",ISNULL(lnloan_scheme_master.loanschemeunkid,-1) AS loanschemeunkid " & _
    '                    ",CASE WHEN lnloan_process_pending_loan.isexternal_entity = 0 THEN lnloan_scheme_master.name " & _
    '                    " ELSE lnloan_process_pending_loan.external_entity_name " & _
    '                    "END AS 'Scheme' " & _
    '                    ",lnloan_process_pending_loan.external_entity_name " & _
    '                    ",lnloan_process_pending_loan.isexternal_entity " & _
    '                    ", ISNULL(emp.firstname,'') + ' ' + ISNULL(emp.othername,'') + ' ' + ISNULL(emp.surname,'') AS Employee " & _
    '                    ",ISNULL(app.firstname,'') + ' ' + ISNULL(app.othername,'') + ' ' + ISNULL(app.surname,'') + ' - ' + lnapproverlevel_master.lnlevelname AS ApproverName " & _
    '                    ",lnapproverlevel_master.lnlevelunkid " & _
    '                    ",lnapproverlevel_master.lnlevelname " & _
    '                    ",lnapproverlevel_master.priority " & _
    '                    ",cfcommon_period_tran.period_name " & _
    '                    ",lnloanapproval_process_tran.loan_amount " & _
    '                    ",lnloanapproval_process_tran.duration " & _
    '                    ",lnloanapproval_process_tran.installmentamt " & _
    '                    ",lnloanapproval_process_tran.noofinstallment " & _
    '                    ",CASE WHEN lnloanapproval_process_tran.statusunkid = 1 then @Pending " & _
    '                    "      WHEN lnloanapproval_process_tran.statusunkid = 2 then @Approved " & _
    '                    "      WHEN lnloanapproval_process_tran.statusunkid = 3 then @Reject " & _
    '                    "      WHEN lnloanapproval_process_tran.statusunkid = 4 then @Assigned end as status " & _
    '                    ",lnloanapproval_process_tran.countryunkid " & _
    '                    ",lnloanapproval_process_tran.employeeunkid " & _
    '                    ",lnloanapproval_process_tran.approvertranunkid " & _
    '                    ",lnloanapproval_process_tran.approverempunkid " & _
    '                    ",lnloanapproval_process_tran.userunkid " & _
    '                    ",lnloanapproval_process_tran.statusunkid " & _
    '                    ",ISNULL(lnloan_process_pending_loan.loan_statusunkid,1) as 'loan_statusunkid' " & _
    '                    ",ISNULL(ETT.stationunkid,0) AS stationunkid " & _
    '                    ",ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
    '                    ",ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
    '                    ",ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                    ",ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
    '                    ",ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
    '                    ",ISNULL(ETT.unitunkid,0) AS unitunkid " & _
    '                    ",ISNULL(ETT.teamunkid,0) AS teamunkid " & _
    '                    ",ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
    '                    ",ISNULL(ETT.classunkid,0) AS classunkid " & _
    '                    ",ISNULL(ECT.jobgroupunkid,0) AS jobgroupunkid " & _
    '                    ",ISNULL(ECT.jobunkid,0) AS jobunkid " & _
    '                    ",ISNULL(GRD.gradegroupunkid,0) AS gradegroupunkid " & _
    '                    ",ISNULL(GRD.gradeunkid,0) AS gradeunkid " & _
    '                    ",ISNULL(GRD.gradelevelunkid,0) AS gradelevelunkid " & _
    '                    ",lnloanapproval_process_tran.isvoid " & _
    '                    ",lnloan_process_pending_loan.isloan " & _
    '                " FROM lnloanapproval_process_tran " & _
    '                    " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
    '                    " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                    " LEFT JOIN hremployee_master emp ON lnloanapproval_process_tran.employeeunkid = emp.employeeunkid " & _
    '                    " LEFT JOIN hremployee_master app ON lnloanapproval_process_tran.approverempunkid = app.employeeunkid " & _
    '                    " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid AND lnloanapprover_master.approverempunkid = lnloanapproval_process_tran.approverempunkid " & _
    '                    " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                    " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_process_pending_loan.deductionperiodunkid " & _
    '                    " LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        stationunkid " & _
    '                        "       ,deptgroupunkid " & _
    '                        "       ,departmentunkid " & _
    '                        "       ,sectiongroupunkid " & _
    '                        "       ,sectionunkid " & _
    '                        "       ,unitgroupunkid " & _
    '                        "       ,unitunkid " & _
    '                        "       ,teamunkid " & _
    '                        "       ,classgroupunkid " & _
    '                        "       ,classunkid " & _
    '                        "       ,employeeunkid " & _
    '                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                        "   FROM hremployee_transfer_tran " & _
    '                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                        ") AS ETT ON ETT.employeeunkid = emp.employeeunkid AND ETT.rno = 1 " & _
    '                        " LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        jobgroupunkid " & _
    '                        "       ,jobunkid " & _
    '                        "       ,employeeunkid " & _
    '                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                        "   FROM hremployee_categorization_tran " & _
    '                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                        ") AS ECT ON ECT.employeeunkid = emp.employeeunkid AND ECT.rno = 1 " & _
    '                        " LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        gradegroupunkid " & _
    '                        "       ,gradeunkid " & _
    '                        "       ,gradelevelunkid " & _
    '                        "       ,employeeunkid " & _
    '                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
    '                        "   FROM prsalaryincrement_tran " & _
    '                        "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                        ") AS GRD ON GRD.employeeunkid = emp.employeeunkid AND GRD.rno = 1 "

    '        strQ &= " WHERE lnloanapproval_process_tran.isvoid = 0 "

    '        If intEmployeeID > 0 Then
    '            strQ &= " AND lnloanapproval_process_tran.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
    '        End If

    '        If intLoanApplicationId > 0 Then
    '            strQ &= " AND lnloanapproval_process_tran.processpendingloanunkid = @processpendingloanunkid "
    '            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanApplicationId)
    '        End If

    '        If mstrFilter.Trim.Length > 0 Then strQ &= " AND " & mstrFilter

    '        strQ &= " ORDER BY application_no, priority desc "

    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Reject"))
    '        objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))
    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetApprovalTranList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (01-Mar-2016) -- End

   

    'Public Function GetApprovalTranList(Optional ByVal intEmployeeID As Integer = 0, Optional ByVal intLoanApplicationId As Integer = 0, Optional ByVal mstrFilter As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation
    '    objDataOperation.ClearParameters()

    '    Try
    '        strQ = " SELECT " & _
    '                   " lnloan_process_pending_loan.processpendingloanunkid " & _
    '                   ", lnloanapproval_process_tran.pendingloantranunkid " & _
    '                   ", lnloan_process_pending_loan.application_no " & _
    '                   ", lnloan_process_pending_loan.application_date " & _
    '                   ", ISNULL(lnloan_scheme_master.loanschemeunkid,-1) AS loanschemeunkid " & _
    '                   ",CASE WHEN lnloan_process_pending_loan.isexternal_entity = 0 THEN lnloan_scheme_master.name " & _
    '                             "ELSE lnloan_process_pending_loan.external_entity_name " & _
    '                    "END AS 'Scheme' " & _
    '                    ", lnloan_process_pending_loan.external_entity_name " & _
    '                    ",lnloan_process_pending_loan.isexternal_entity " & _
    '                    ", ISNULL(emp.firstname,'') + ' ' + ISNULL(emp.othername,'') + ' ' + ISNULL(emp.surname,'') AS Employee " & _
    '                    ",ISNULL(app.firstname,'') + ' ' + ISNULL(app.othername,'') + ' ' + ISNULL(app.surname,'') + ' - ' + lnapproverlevel_master.lnlevelname AS ApproverName " & _
    '                    ", lnapproverlevel_master.lnlevelunkid " & _
    '                    ", lnapproverlevel_master.lnlevelname " & _
    '                    ", lnapproverlevel_master.priority " & _
    '                    ",cfcommon_period_tran.period_name " & _
    '                    ",lnloanapproval_process_tran.loan_amount " & _
    '                    ",lnloanapproval_process_tran.duration " & _
    '                    ",lnloanapproval_process_tran.installmentamt " & _
    '                    ",lnloanapproval_process_tran.noofinstallment " & _
    '                    ",CASE WHEN lnloanapproval_process_tran.statusunkid = 1 then @Pending " & _
    '                    "           WHEN lnloanapproval_process_tran.statusunkid = 2 then @Approved " & _
    '                    "           WHEN lnloanapproval_process_tran.statusunkid = 3 then @Reject " & _
    '                    "           WHEN lnloanapproval_process_tran.statusunkid = 4 then @Assigned end as status " & _
    '                    ",lnloanapproval_process_tran.countryunkid " & _
    '                    ",lnloanapproval_process_tran.employeeunkid " & _
    '                    ",lnloanapproval_process_tran.approvertranunkid " & _
    '                    ",lnloanapproval_process_tran.approverempunkid " & _
    '                    ",lnloanapproval_process_tran.userunkid " & _
    '                    ",lnloanapproval_process_tran.statusunkid " & _
    '                    ",ISNULL(lnloan_process_pending_loan.loan_statusunkid,1) as 'loan_statusunkid' " & _
    '                    ",ISNULL(emp.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                    ",ISNULL(emp.unitgroupunkid,0) AS unitgroupunkid " & _
    '                    ",ISNULL(emp.teamunkid,0) AS teamunkid " & _
    '                    ",ISNULL(emp.stationunkid,0) AS stationunkid " & _
    '                    ",ISNULL(emp.deptgroupunkid,0) AS deptgroupunkid " & _
    '                    ",ISNULL(emp.departmentunkid,0) AS departmentunkid " & _
    '                    ",ISNULL(emp.sectionunkid,0) AS sectionunkid " & _
    '                    ",ISNULL(emp.unitunkid,0) AS unitunkid " & _
    '                    ",ISNULL(emp.jobunkid,0) AS jobunkid " & _
    '                    ",ISNULL(emp.classgroupunkid,0) AS classgroupunkid " & _
    '                    ",ISNULL(emp.classunkid,0) AS classunkid " & _
    '                    ",ISNULL(emp.jobgroupunkid,0) AS jobgroupunkid " & _
    '                    ",ISNULL(emp.gradegroupunkid,0) AS gradegroupunkid " & _
    '                    ",ISNULL(emp.gradeunkid,0) AS gradeunkid " & _
    '                    ",ISNULL(emp.gradelevelunkid,0) AS gradelevelunkid " & _
    '                    ", lnloanapproval_process_tran.isvoid " & _
    '                    ",lnloan_process_pending_loan.isloan " & _
    '                    " FROM lnloanapproval_process_tran " & _
    '                    " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
    '                    " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                    " LEFT JOIN hremployee_master emp ON lnloanapproval_process_tran.employeeunkid = emp.employeeunkid " & _
    '                    " LEFT JOIN hremployee_master app ON lnloanapproval_process_tran.approverempunkid = app.employeeunkid " & _
    '                    " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid AND lnloanapprover_master.approverempunkid = lnloanapproval_process_tran.approverempunkid " & _
    '                    " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                    " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_process_pending_loan.deductionperiodunkid " & _
    '                    " WHERE lnloanapproval_process_tran.isvoid = 0 "


    '        If intEmployeeID > 0 Then
    '            strQ &= " AND lnloanapproval_process_tran.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
    '        End If

    '        If intLoanApplicationId > 0 Then
    '            strQ &= " AND lnloanapproval_process_tran.processpendingloanunkid = @processpendingloanunkid "
    '            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanApplicationId)
    '        End If

    '        If mstrFilter.Trim.Length > 0 Then strQ &= " AND " & mstrFilter

    '        strQ &= " ORDER BY application_no, priority desc "

    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Reject"))
    '        objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))
    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetApprovalTranList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    '    Return dsList
    'End Function
    'Nilay (10-Oct-2015) -- End

    'Nilay (07-Feb-2016) -- Start
    'Loan Swapping - To get pending list Ids
    Public Function GetProcessPendingLoanIds(ByVal intApproverTranunkid As Integer, ByVal intEmployeeId As Integer, ByVal objDataOp As clsDataOperation) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = String.Empty
        Dim strProcessPendingIds As String = String.Empty
        Try

            Dim exForce As Exception
            Dim objDataOperation As clsDataOperation

            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
            Else
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()



            'Pinkal (11-Dec-2017) -- Start
            'Bug - Solved Loan Approver Migration Issue .

            'strQ = "SELECT STUFF " & _
            '              "( " & _
            '                    "( " & _
            '                    "   SELECT ',' + CAST(lnloanapproval_process_tran.processpendingloanunkid AS NVARCHAR(max)) " & _
            '                    "   FROM lnloanapproval_process_tran " & _
            '                    "   JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid" & _
            '                    "       AND approvertranunkid = " & intApproverTranunkid & _
            '                    "   WHERE  lnloan_process_pending_loan.loan_statusunkid = 1 AND lnloanapproval_process_tran.statusunkid = 1 " & _
            '                    "       AND lnloan_process_pending_loan.employeeunkid = " & intEmployeeId & _
            '                    "       AND lnloan_process_pending_loan.isvoid = 0 " & _
            '                    "       AND lnloanapproval_process_tran.isvoid = 0 " & _
            '                    "   ORDER BY lnloanapproval_process_tran.processpendingloanunkid  FOR XML PATH('') " & _
            '                    "),1,1,'' " & _
            '              ") AS CSV "

            strQ = "SELECT STUFF " & _
                          "( " & _
                                "( " & _
                                "   SELECT ',' + CAST(lnloanapproval_process_tran.processpendingloanunkid AS NVARCHAR(max)) " & _
                                "   FROM lnloanapproval_process_tran " & _
                                "   JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid" & _
                                "   WHERE  lnloan_process_pending_loan.loan_statusunkid = 1 AND lnloanapproval_process_tran.statusunkid = 1 " & _
                             "       AND lnloan_process_pending_loan.employeeunkid = @employeeunkid " & _
                                "       AND lnloan_process_pending_loan.isvoid = 0 " & _
                                "       AND lnloanapproval_process_tran.isvoid = 0 " & _
                             "       AND lnloanapproval_process_tran.approvertranunkid = @ApproverTranId " & _
                                "   ORDER BY lnloanapproval_process_tran.processpendingloanunkid  FOR XML PATH('') " & _
                                "),1,1,'' " & _
                          ") AS CSV "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@ApproverTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverTranunkid)
            'Pinkal (11-Dec-2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strProcessPendingIds = dsList.Tables("List").Rows(0)("CSV").ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetProcessPendingLoanIds; Module Name: " & mstrModuleName)
        End Try
        Return strProcessPendingIds
    End Function
    'Nilay (07-Feb-2016) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloanapproval_process_tran) </purpose>
    Public Function Insert(ByVal mblnLoanApproverForLoanScheme As Boolean, ByVal intSchemeId As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverempunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDuration.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecInstallmentamt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO lnloanapproval_process_tran ( " & _
                      "  processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", approvertranunkid " & _
                      ", approverempunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", deductionperiodunkid " & _
                      ", loan_amount " & _
                      ", duration " & _
                      ", installmentamt " & _
                      ", noofinstallment " & _
                      ", countryunkid " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", visibleid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @processpendingloanunkid " & _
                      ", @employeeunkid " & _
                      ", @approvertranunkid " & _
                      ", @approverempunkid " & _
                      ", @priority " & _
                      ", @approvaldate " & _
                      ", @deductionperiodunkid " & _
                      ", @loan_amount " & _
                      ", @duration " & _
                      ", @installmentamt " & _
                      ", @noofinstallment " & _
                      ", @countryunkid " & _
                      ", @statusunkid " & _
                      ", @remark " & _
                      ", @visibleid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPendingloantranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForLoanApproval(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloanapproval_process_tran) </purpose>
    Public Function Update(ByVal mblnLoanApproverForLoanScheme As Boolean, _
                           ByVal intSchemeId As Integer, _
                           Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Nilay (20-Sept-2016) -- Start
        'Enhancement : Cancel feature for approved but not assigned loan application
        'If mintStatusunkid = 1 Then Return True
        If mintStatusunkid = enLoanApplicationStatus.PENDING Then Return True
        'Nilay (20-Sept-2016) -- End

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingloantranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingloantranunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverempunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDuration.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecInstallmentamt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark.ToString)
            'Shani (21-Jul-2016) -- End
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE lnloanapproval_process_tran SET " & _
                      "  processpendingloanunkid = @processpendingloanunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", approvertranunkid = @approvertranunkid" & _
                      ", approverempunkid = @approverempunkid" & _
                      ", priority = @priority " & _
                      ", approvaldate = @approvaldate" & _
                      ", deductionperiodunkid = @deductionperiodunkid" & _
                      ", loan_amount = @loan_amount" & _
                      ", duration = @duration" & _
                      ", installmentamt = @installmentamt" & _
                      ", noofinstallment = @noofinstallment" & _
                      ", countryunkid = @countryunkid" & _
                      ", statusunkid = @statusunkid" & _
                      ", remark = @remark" & _
                      ", visibleid = @visibleid " & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                    "WHERE pendingloantranunkid = @pendingloantranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForLoanApproval(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT pendingloantranunkid,processpendingloanunkid,approvertranunkid, approverempunkid,priority,visibleid FROM lnloanapproval_process_tran WHERE isvoid = 0 AND processpendingloanunkid = @processpendingloanunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                strQ = " UPDATE lnloanapproval_process_tran set " & _
                          " visibleid = " & mintStatusunkid & _
                          " WHERE  processpendingloanunkid = @processpendingloanunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid AND approverempunkid = @approverempunkid AND isvoid = 0   "


                Dim dtVisibility As DataTable = New DataView(dsApprover.Tables(0), "priority = " & mintPriority, "", DataViewRowState.CurrentRows).ToTable

                For i As Integer = 0 To dtVisibility.Rows.Count - 1
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("processpendingloanunkid").ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approvertranunkid").ToString)
                    objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approverempunkid").ToString)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

                Dim intMinPriority As Integer = IIf(IsDBNull(dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority)), -1, dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority))

                dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

                If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'If mintStatusunkid = 2 Then   'ONLY APPROVED STATUS
                    If mintStatusunkid = enLoanApplicationStatus.APPROVED Then
                        'Nilay (20-Sept-2016) -- End
                        strQ = " UPDATE lnloanapproval_process_tran set " & _
                                  " visibleid = 1 " & _
                                  " WHERE  processpendingloanunkid = @processpendingloanunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid AND approverempunkid = @approverempunkid AND isvoid = 0   "

                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'ElseIf mintStatusunkid = 3 Then   ' REJECTED  STATUS
                    ElseIf mintStatusunkid = enLoanApplicationStatus.REJECTED Then
                        'Nilay (20-Sept-2016) -- End
                        strQ = " UPDATE lnloanapproval_process_tran set " & _
                                  " visibleid = -1 " & _
                                  " WHERE  processpendingloanunkid = @processpendingloanunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid AND approverempunkid = @approverempunkid AND isvoid = 0   "
                    End If

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("processpendingloanunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approvertranunkid").ToString)
                        objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approverempunkid").ToString)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If

            End If

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'If mintStatusunkid = 2 Then  'Approved
            If mintStatusunkid = enLoanApplicationStatus.APPROVED Then
                'Nilay (20-Sept-2016) -- End
                strQ = " Update lnloanapproval_process_tran SET " & _
                          " deductionperiodunkid=@deductionperiodunkid" & _
                          ",loan_amount = @loan_amount " & _
                          ",duration =@duration " & _
                          ",installmentamt = @installmentamt " & _
                          ",noofinstallment = @noofinstallment " & _
                          ",countryunkid = @countryunkid " & _
                          " WHERE isvoid = 0 AND processpendingloanunkid = @processpendingloanunkid and employeeunkid = @employeeunkid AND priority >= @priority "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid)
                objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount)
                objDataOperation.AddParameter("@duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDuration)
                objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInstallmentamt)
                objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment)
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            If UpdateLoanApplicationStatus(mblnLoanApproverForLoanScheme, intSchemeId, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Nilay (05-May-2016) -- Start
    Public Function UpdateLoanApplication(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = " UPDATE lnloanapproval_process_tran SET " & _
                          "     employeeunkid = @employeeunkid" & _
                          "    ,approvaldate = @approvaldate" & _
                          "    ,deductionperiodunkid = @deductionperiodunkid" & _
                          "    ,loan_amount = @loan_amount" & _
                          "    ,installmentamt = @installmentamt" & _
                          "    ,noofinstallment = @noofinstallment" & _
                          "    ,remark = @remark" & _
                          " WHERE processpendingloanunkid = @processpendingloanunkid " & _
                          "    AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecInstallmentamt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT " & _
                       "  pendingloantranunkid " & _
                       ", processpendingloanunkid " & _
                   " FROM lnloanapproval_process_tran " & _
                   " WHERE isvoid=0 AND processpendingloanunkid = @processpendingloanunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then

                For Each dRow As DataRow In dsList.Tables("List").Rows
                    mintPendingloantranunkid = dRow.Item("pendingloantranunkid")
                    Call GetData(objDataOperation)

                    If InsertAuditTrailForLoanApproval(objDataOperation, 2) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoanApplication; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function IsPendingLoanApplication(ByVal intProcessPendingLoanunkid As Integer, Optional ByVal blnGetApplicationStatus As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = " SELECT " & _
                   "     processpendingloanunkid " & _
                   "    ,statusunkid " & _
                   " FROM lnloanapproval_process_tran " & _
                   " WHERE isvoid = 0 " & _
                   "    AND processpendingloanunkid = @processpendingloanunkid "

            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            '       "    AND statusunkid <> 1 "
            If blnGetApplicationStatus = True Then
                strQ &= "    AND statusunkid = 1 AND visibleid < 2 "
            Else
                strQ &= "    AND statusunkid <> 1 "
            End If
            'Shani (21-Jul-2016) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessPendingLoanunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPendingLoanApplication; Module Name: " & mstrModuleName)
        End Try
    End Function
    'Nilay (05-May-2016) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloanapproval_process_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM lnloanapproval_process_tran " & _
            "WHERE pendingloantranunkid = @pendingloantranunkid "

            objDataOperation.AddParameter("@pendingloantranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@pendingloantranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  pendingloantranunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", approvertranunkid " & _
                      ", approverempunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", deductionperiodunkid " & _
                      ", loan_amount " & _
                      ", duration " & _
                      ", installmentamt " & _
                      ", noofinstallment " & _
                      ", countryunkid " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                     "FROM lnloanapproval_process_tran " & _
                     "WHERE name = @name " & _
                     "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND pendingloantranunkid <> @pendingloantranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@pendingloantranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetLastApproverApprovalID(ByVal intLoanApplicationId As Integer, ByVal intPriority As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintApprovalID As Integer = -1
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = " SELECT TOP 1 pendingloantranunkid " & _
                      " FROM lnloanapproval_process_tran " & _
                      " WHERE priority < @priority AND processpendingloanunkid = @processpendingloanunkid AND isvoid = 0 " & _
                      " ORDER BY priority DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanApplicationId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintApprovalID = CInt(dsList.Tables(0).Rows(0)("pendingloantranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastApproverApprovalID; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return mintApprovalID
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function UpdateLoanApplicationStatus(ByVal mblnLoanApproverForLoanScheme As Boolean, ByVal intSchemeId As Integer, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim exForce As Exception
        Try
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'If mintStatusunkid <> 3 Then '[PENDING AND APPROVED ALLOWED]
            If mintStatusunkid <> enLoanApplicationStatus.REJECTED AndAlso mintStatusunkid <> enLoanApplicationStatus.CANCELLED Then
                'Nilay (20-Sept-2016) -- End
                Dim objLoanApprover As New clsLoanApprover_master
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dtApprover As DataTable = objLoanApprover.GetEmployeeApprover(mintEmployeeunkid, mblnLoanApproverForLoanScheme.ToString(), intSchemeId, "priority > " & mintPriority)
                'Hemant (04 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT#4201(PACRA) - Loan Approver not able to see pending loan.
                'Dim dtApprover As DataTable = objLoanApprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, _
                '                                                                                  User._Object._Userunkid, _
                '                                                                                  FinancialYear._Object._YearUnkid, _
                '                                                                                  Company._Object._Companyunkid, _
                '                                                                                  mintEmployeeunkid, _
                '                                                                                  mblnLoanApproverForLoanScheme, _
                '                                                                                  intSchemeId, _
                '                                                                                  "priority > " & mintPriority)
                Dim dtApprover As DataTable = GetLoanApprovalData(mintEmployeeunkid, _
                                                                                  mblnLoanApproverForLoanScheme, _
                                                                                  intSchemeId, _
                                                                  "  lnloan_process_pending_loan.processpendingloanunkid = " & mintProcesspendingloanunkid & " AND lnapproverlevel_master.priority > " & mintPriority, _
                                                                  objDataOperation)
                'Hemant (04 Oct 2019) -- End
                'Nilay (10-Oct-2015) -- End

                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count <= 0 Then
                    Dim objLoanApplication As New clsProcess_pending_loan
                    objLoanApplication._Processpendingloanunkid = mintProcesspendingloanunkid
                    objLoanApplication._Approverunkid = mintApprovertranunkid
                    objLoanApplication._Approved_Amount = mdecLoan_Amount
                    objLoanApplication._Loan_Statusunkid = mintStatusunkid
                    objLoanApplication._WebClientIP = mstrWebClientIP
                    objLoanApplication._WebFormName = mstrWebFormName
                    objLoanApplication._WebHostName = mstrWebHostName
                    'Nilay (13-Sept-2016) -- Start
                    'Enhancement : Enable Default Parameter Edit & other fixes
                    'If objLoanApplication.Update(objDataOperation) = False Then
                    If objLoanApplication.Update(False, objDataOperation) = False Then
                        'Nilay (13-Sept-2016) -- End
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objLoanApplication = Nothing
                End If
                dtApprover.Rows.Clear()
                dtApprover = Nothing

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'ElseIf mintStatusunkid = 3 Then  '[REJECTED]
            ElseIf mintStatusunkid = enLoanApplicationStatus.REJECTED Then
                'Nilay (20-Sept-2016) -- End
                Dim objLoanApplication As New clsProcess_pending_loan
                objLoanApplication._Processpendingloanunkid = mintProcesspendingloanunkid
                objLoanApplication._Approverunkid = mintApprovertranunkid
                objLoanApplication._Loan_Statusunkid = mintStatusunkid
                objLoanApplication._WebClientIP = mstrWebClientIP
                objLoanApplication._WebFormName = mstrWebFormName
                objLoanApplication._WebHostName = mstrWebHostName
                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                'If objLoanApplication.Update(objDataOperation) = False Then
                If objLoanApplication.Update(False, objDataOperation) = False Then
                    'Nilay (13-Sept-2016) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objLoanApplication = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoanApplicationStatus; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloanapproval_process_tran) </purpose>
    Public Function InsertAuditTrailForLoanApproval(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingloantranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingloantranunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverempunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDuration.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecInstallmentamt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            strQ = "INSERT INTO atlnloanapproval_process_tran ( " & _
                      "  pendingloantranunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", approvertranunkid " & _
                      ", approverempunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", deductionperiodunkid " & _
                      ", loan_amount " & _
                      ", duration " & _
                      ", installmentamt " & _
                      ", noofinstallment " & _
                      ", countryunkid " & _
                      ", statusunkid " & _
                      ", visibleid " & _
                      ", remark " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @pendingloantranunkid " & _
                      ", @processpendingloanunkid " & _
                      ", @employeeunkid " & _
                      ", @approvertranunkid " & _
                      ", @approverempunkid " & _
                      ", @priority " & _
                      ", @approvaldate " & _
                      ", @deductionperiodunkid " & _
                      ", @loan_amount " & _
                      ", @duration " & _
                      ", @installmentamt " & _
                      ", @noofinstallment " & _
                      ", @countryunkid " & _
                      ", @statusunkid " & _
                      ", @visibleid " & _
                      ", @remark " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @module_name1 " & _
                      ", @module_name2 " & _
                      ", @module_name3 " & _
                      ", @module_name4 " & _
                      ", @module_name5 " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForLoanApproval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Shani
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloanapproval_process_tran) </purpose>
    ''' Shani(26-Nov-2015) -- Start
    ''' ENHANCEMENT : Add Loan Import Screen
    Public Function UpdateVisibleStatusByProcessUnkId(ByVal intProcesspendingloanunkid As Integer, ByVal intEmpUnkid As Integer, ByVal intApproverTranUnkid As Integer, ByVal intApproverEmpUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            Using objDo As New clsDataOperation
                objDo.BindTransaction()
                objDo.ClearParameters()

                objDo.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcesspendingloanunkid.ToString)
                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid.ToString)
                objDo.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverTranUnkid.ToString)
                objDo.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverEmpUnkid.ToString)

                strQ = "UPDATE lnloanapproval_process_tran SET visibleid = 2 WHERE processpendingloanunkid =  @processpendingloanunkid ; "

                strQ &= "UPDATE lnloanapproval_process_tran SET statusunkid = 2 WHERE processpendingloanunkid = @processpendingloanunkid " & _
                        "AND employeeunkid = @employeeunkid " & _
                        "AND approvertranunkid  = @approvertranunkid " & _
                        "AND approverempunkid = @approverempunkid"

                dsList = objDo.ExecQuery(strQ, "List")

                If objDo.ErrorMessage <> "" Then
                    objDo.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objDo.ReleaseTransaction(True)
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateVisibleStatusByProcessUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
    ' Shani(26-Nov-2015) -- End
    
    'Hemant (04 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT#4201(PACRA) - Loan Approver not able to see pending loan.
    Public Function GetLoanApprovalData(ByVal intEmployeeID As Integer, _
                                            ByVal blnLoanApproverForLoanScheme As Boolean, _
                                            Optional ByVal intSchemeId As Integer = -1, _
                                            Optional ByVal mstrFilter As String = "", _
                                            Optional ByVal objDataOp As clsDataOperation = Nothing) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
            Else
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()

            strQ = " SELECT " & _
                      "  lnloan_process_pending_loan.processpendingloanunkid " & _
                      ", lnloan_process_pending_loan.application_no " & _
                      ", lnloanapproval_process_tran.pendingloantranunkid " & _
                      ", lnloanapproval_process_tran.employeeunkid " & _
                      ", #EMP_CODE# AS employeecode " & _
                      ", #EMP_NAME# AS employeename " & _
                      ", lnloanapproval_process_tran.approvertranunkid " & _
                      ", lnloanapproval_process_tran.approverempunkid " & _
                      ", #APPROVER_CODE# AS approveremployeecode " & _
                      ", #APPROVER_NAME# AS approveremployeename " & _
                      ", lnapproverlevel_master.lnlevelunkid " & _
                      ", lnapproverlevel_master.lnlevelname " & _
                      ", lnapproverlevel_master.priority " & _
                      ", lnloanapprover_master.isexternalapprover " & _
                      ", lnloan_approver_mapping.userunkid AS MappedUserID " & _
                      " FROM lnloan_process_pending_loan " & _
                      " JOIN lnloanapproval_process_tran ON lnloanapproval_process_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
                      " JOIN lnloanapprover_master ON lnloanapproval_process_tran.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
                      "     AND lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isactive = 1 AND lnloanapprover_master.isswap = 0 " & _
                      " #EMPLOYEE_JOIN# " & _
                      " #APROVER_EMPLOYEE_JOIN# " & _
                      " JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid "

            If blnLoanApproverForLoanScheme = True And intSchemeId > 0 Then

                strQ &= "  JOIN lnapprover_scheme_mapping ON lnapprover_scheme_mapping.approverunkid = lnloanapprover_master.lnapproverunkid AND lnapprover_scheme_mapping.isvoid = 0 " & _
                            "  JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnapprover_scheme_mapping.loanschemeunkid AND lnapprover_scheme_mapping.loanschemeunkid = @loanschemeunkid "

                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeId.ToString)

            End If

            strQ &= " JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid "

            strQFinal = strQ

            strQCondition = " WHERE lnloanapproval_process_tran.isvoid = 0  AND lnloanapproval_process_tran.employeeunkid = " & intEmployeeID

            strQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If mstrFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & mstrFilter.Trim
            End If

            strQ &= strQCondition

            strQ = strQ.Replace("#EMP_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#EMP_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapproval_process_tran.employeeunkid AND hremployee_master.isapproved = 1 ")
            strQ = strQ.Replace("#APPROVER_CODE#", "ISNULL(approveremp.employeecode,'') ")
            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(approveremp.firstname,'') + ' ' + ISNULL(approveremp.othername, '') + ' ' + ISNULL(approveremp.surname,'') ")
            strQ = strQ.Replace("#APROVER_EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master approveremp ON approveremp.employeeunkid = lnloanapproval_process_tran.approverempunkid AND approveremp.isapproved = 1 ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow In dsCompany.Tables("Company").Rows
                strQ = strQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#EMP_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
                    strQ = strQ.Replace("#EMP_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapproval_process_tran.employeeunkid AND hremployee_master.isapproved = 1 ")
                    strQ = strQ.Replace("#APPROVER_CODE#", "'' ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APROVER_EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanapprover_master.approverempunkid ")

                Else
                    strQ = strQ.Replace("#EMP_CODE#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN '' " & _
                                   "ELSE ISNULL(hremployee_master.employeecode,'') END ")
                    strQ = strQ.Replace("#EMP_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN '' " & _
                                                                "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN #DB_NAME#hremployee_master ON hremployee_master.employeeunkid = lnloanapproval_process_tran.employeeunkid AND hremployee_master.isapproved = 1 ")

                    strQ = strQ.Replace("#APPROVER_CODE#", "CASE WHEN ISNULL(approveremp.firstname, '') + ' ' + ISNULL(approveremp.surname, '') = ' ' THEN '' " & _
                                                       "ELSE ISNULL(approveremp.employeecode,'') END ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(approveremp.firstname, '') + ' ' + ISNULL(approveremp.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(approveremp.firstname, '') + ' ' + ISNULL(approveremp.surname, '') END ")
                    strQ = strQ.Replace("#APROVER_EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master approveremp ON  approveremp.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                End If

                strQ &= strQCondition
                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeId.ToString)

                dsExtList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables("List").Copy)
                Else
                    dsList.Tables("List").Merge(dsExtList.Tables("List"), True)
                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority,employeename", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)
            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
                Return dsList.Tables("List")
            Else
                Return Nothing
            End If
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "GetLoanApprovalData", mstrMessage)
            Return Nothing
        Finally
            exForce = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Hemant (04 Oct 2019) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "WEB")
            Language.setMessage("clsProcess_pending_loan", 5, "Pending")
            Language.setMessage("clsProcess_pending_loan", 6, "Approved")
            Language.setMessage("clsProcess_pending_loan", 7, "Rejected")
            Language.setMessage("clsProcess_pending_loan", 10, "Assigned")
            Language.setMessage("clsProcess_pending_loan", 7, "Reject")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class