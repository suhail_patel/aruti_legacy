﻿'************************************************************************************************************************************
'Class Name : clsLoan_Advance.vb
'Purpose    :
'Date       :07/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Math
Imports eZee.Common.eZeeForm

Public Class clsLoan_Advance
    Private Shared ReadOnly mstrModuleName As String = "clsLoan_Advance"
    Dim mstrMessage As String = ""
    Private objStatusTran As New clsLoan_Status_tran
    Private objEMITenure As New clslnloan_emitenure_tran
    Private objInterestTran As New clslnloan_interest_tran
    Private objTopupTran As New clslnloan_topup_tran

#Region " Private variables "

    Private mintLoanadvancetranunkid As Integer
    Private mstrLoanvoucher_No As String = String.Empty
    Private mdtEffective_Date As Date
    Private mintYearunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintApproverunkid As Integer
    Private mstrLoan_Purpose As String = String.Empty
    Private mdecBalance_Amount As Decimal
    Private mblnIsbrought_Forward As Boolean
    Private mblnIsloan As Boolean
    Private mintLoanschemeunkid As Integer
    Private mdecLoan_Amount As Decimal
    Private mdecInterest_Rate As Decimal
    Private mintLoan_Duration As Integer
    Private mdecInterest_Amount As Decimal
    Private mdecNet_Amount As Decimal
    Private mintCalctype_Id As Integer
    Private mintLoanscheduleunkid As Integer
    Private mintEmi_Tenure As Integer
    Private mdecEmi_Amount As Decimal
    Private mdtEmi_Start_Date As Date
    Private mdtEmi_End_Date As Date
    Private mdecAdvance_Amount As Decimal
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mintLoanStatus As Integer
    Private mintProcesspendingloanunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mintDeductionperiodunkid As Integer
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private objDataOpr As clsDataOperation
    Private mdecLoanBF_Amount As Decimal = 0
    'S.SANDEEP [06 MAY 2015] -- START
    Private mintCountryUnkid As Integer = 0
    Private mdecLoanBf_Balance As Decimal = 0
    Private mdblRateOfInterest As Double = 0
    Private mintNoOfInstallment As Integer = 0
    Private mdecInstallmentAmount As Decimal = 0
    'S.SANDEEP [06 MAY 2015] -- END

    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Private mdecBalance_AmountPaidCurrency As Decimal = 0
    Private mdecExchange_rate As Decimal = 0
    Private mdecBasecurrency_amount As Decimal = 0
    Private mdecOpening_balance As Decimal = 0
    Private mdecOpening_balancePaidCurrency As Decimal = 0
    'Sohail (07 May 2015) -- End
    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private mintInterest_Calctype_Id As Integer
    Private mdecEMI_Principal_amount As Decimal = 0
    Private mdecEMI_Interest_amount As Decimal = 0
    'Sohail (15 Dec 2015) -- End
    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private mintMapped_TranheadUnkid As Integer = 0
    'Sohail (29 Apr 2019) -- End

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanadvancetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanadvancetranunkid() As Integer
        Get
            Return mintLoanadvancetranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanadvancetranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanvoucher_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanvoucher_No() As String
        Get
            Return mstrLoanvoucher_No
        End Get
        Set(ByVal value As String)
            mstrLoanvoucher_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effective_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>

    Public Property _Effective_Date() As Date
        Get
            Return mdtEffective_Date
        End Get
        Set(ByVal value As Date)
            mdtEffective_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_purpose
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loan_Purpose() As String
        Get
            Return mstrLoan_Purpose
        End Get
        Set(ByVal value As String)
            mstrLoan_Purpose = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set balance_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Balance_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecBalance_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecBalance_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isbrought_forward
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isbrought_Forward() As Boolean
        Get
            Return mblnIsbrought_Forward
        End Get
        Set(ByVal value As Boolean)
            mblnIsbrought_Forward = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isloan
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isloan() As Boolean
        Get
            Return mblnIsloan
        End Get
        Set(ByVal value As Boolean)
            mblnIsloan = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loan_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecLoan_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecLoan_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interest_rate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Interest_Rate() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecInterest_Rate
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecInterest_Rate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_duration
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loan_Duration() As Integer
        Get
            Return mintLoan_Duration
        End Get
        Set(ByVal value As Integer)
            mintLoan_Duration = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interest_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Interest_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecInterest_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecInterest_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set net_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Net_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecNet_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecNet_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calctype_id
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Calctype_Id() As Integer
        Get
            Return mintCalctype_Id
        End Get
        Set(ByVal value As Integer)
            mintCalctype_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanscheduleunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanscheduleunkid() As Integer
        Get
            Return mintLoanscheduleunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanscheduleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_tenure
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emi_Tenure() As Integer
        Get
            Return mintEmi_Tenure
        End Get
        Set(ByVal value As Integer)
            mintEmi_Tenure = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emi_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecEmi_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecEmi_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_start_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emi_Start_Date() As Date
        Get
            Return mdtEmi_Start_Date
        End Get
        Set(ByVal value As Date)
            mdtEmi_Start_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_end_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Emi_End_Date() As Date
        Get
            Return mdtEmi_End_Date
        End Get
        Set(ByVal value As Date)
            mdtEmi_End_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set advance_amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Advance_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecAdvance_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecAdvance_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Loan Status
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LoanStatus() As Integer
        Get
            Return mintLoanStatus
        End Get
        Set(ByVal value As Integer)
            mintLoanStatus = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Processpendingloanunkid() As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deductionperiodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Deductionperiodunkid() As Integer
        Get
            Return mintDeductionperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintDeductionperiodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LoginEmployeeUnkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DataOpr
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public WriteOnly Property _DataOpr() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOpr = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LoanBF_Amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LoanBF_Amount() As Decimal
        Get
            Return mdecLoanBF_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecLoanBF_Amount = value
        End Set
    End Property

    'S.SANDEEP [06 MAY 2015] -- START
    Public Property _CountryUnkid() As Integer
        Get
            Return mintCountryUnkid
        End Get
        Set(ByVal value As Integer)
            mintCountryUnkid = value
        End Set
    End Property

    Public Property _LoanBf_Balance() As Decimal
        Get
            Return mdecLoanBf_Balance
        End Get
        Set(ByVal value As Decimal)
            mdecLoanBf_Balance = value
        End Set
    End Property

    Public WriteOnly Property _RateOfInterest() As Double
        Set(ByVal value As Double)
            mdblRateOfInterest = value
        End Set
    End Property

    Public WriteOnly Property _NoOfInstallments() As Integer
        Set(ByVal value As Integer)
            mintNoOfInstallment = value
        End Set
    End Property

    Public WriteOnly Property _InstallmentAmount() As Decimal
        Set(ByVal value As Decimal)
            mdecInstallmentAmount = value
        End Set
    End Property
    'S.SANDEEP [06 MAY 2015] -- END

    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Public Property _Balance_AmountPaidCurrency() As Decimal
        Get
            Return mdecBalance_AmountPaidCurrency
        End Get
        Set(ByVal value As Decimal)
            mdecBalance_AmountPaidCurrency = value
        End Set
    End Property

    Public Property _Exchange_rate() As Decimal
        Get
            Return mdecExchange_rate
        End Get
        Set(ByVal value As Decimal)
            mdecExchange_rate = value
        End Set
    End Property

    Public Property _Basecurrency_amount() As Decimal
        Get
            Return mdecBasecurrency_amount
        End Get
        Set(ByVal value As Decimal)
            mdecBasecurrency_amount = value
        End Set
    End Property

    Public Property _Opening_balance() As Decimal
        Get
            Return mdecOpening_balance
        End Get
        Set(ByVal value As Decimal)
            mdecOpening_balance = value
        End Set
    End Property

    Public Property _Opening_balancePaidCurrency() As Decimal
        Get
            Return mdecOpening_balancePaidCurrency
        End Get
        Set(ByVal value As Decimal)
            mdecOpening_balancePaidCurrency = value
        End Set
    End Property
    'Sohail (07 May 2015) -- End

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
    Public Property _Interest_Calctype_Id() As Integer
        Get
            Return mintInterest_Calctype_Id
        End Get
        Set(ByVal value As Integer)
            mintInterest_Calctype_Id = value
        End Set
    End Property

    Public WriteOnly Property _EMI_Principal_amount() As Decimal
        Set(ByVal value As Decimal)
            mdecEMI_Principal_amount = value
        End Set
    End Property

    Public WriteOnly Property _EMI_Interest_amount() As Decimal
        Set(ByVal value As Decimal)
            mdecEMI_Interest_amount = value
        End Set
    End Property
    'Sohail (15 Dec 2015) -- End

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Public Property _Mapped_TranheadUnkid() As Integer
        Get
            Return mintMapped_TranheadUnkid
        End Get
        Set(ByVal value As Integer)
            mintMapped_TranheadUnkid = value
        End Set
    End Property
    'Sohail (29 Apr 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If
        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End
        Try

            strQ = "SELECT " & _
                      "  loanadvancetranunkid " & _
                      ", loanvoucher_no " & _
                      ", effective_date " & _
                      ", yearunkid " & _
                      ", periodunkid " & _
                      ", employeeunkid " & _
                      ", approverunkid " & _
                      ", loan_purpose " & _
                      ", balance_amount " & _
                      ", isbrought_forward " & _
                      ", isloan " & _
                      ", loanschemeunkid " & _
                      ", loan_amount " & _
                      ", interest_rate " & _
                      ", loan_duration " & _
                      ", interest_amount " & _
                      ", net_amount " & _
                      ", calctype_id " & _
                      ", loanscheduleunkid " & _
                      ", emi_tenure " & _
                      ", emi_amount " & _
                      ", advance_amount " & _
                      ", loan_statusunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", processpendingloanunkid " & _
                      ", ISNULL(deductionperiodunkid,0) As deductionperiodunkid " & _
                      ", ISNULL(countryunkid,0) AS countryunkid " & _
                      ", ISNULL(bf_balance,0) AS bf_balance " & _
                      ", ISNULL(exchange_rate,0) AS exchange_rate " & _
                      ", ISNULL(basecurrency_amount,0) AS basecurrency_amount " & _
                      ", ISNULL(balance_amountPaidCurrency,0) AS balance_amountPaidCurrency " & _
                      ", ISNULL(opening_balance,0) AS opening_balance " & _
                      ", ISNULL(opening_balancePaidCurrency,0) AS opening_balancePaidCurrency " & _
                      ", ISNULL(interest_calctype_id, 1) AS interest_calctype_id " & _
                      ", ISNULL(mapped_tranheadunkid, 0) AS mapped_tranheadunkid " & _
                     "FROM lnloan_advance_tran " & _
                     "WHERE loanadvancetranunkid = @loanadvancetranunkid "
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (15 Dec 2015) - [interest_calctype_id]
            'Sohail (07 May 2015) - [balance_amountPaidCurrency, exchange_rate, basecurrency_amount, opening_balance, opening_balancePaidCurrency]
            'S.SANDEEP [06 MAY 2015] -- {countryunkid,bf_balance}

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoanadvancetranunkid = CInt(dtRow.Item("loanadvancetranunkid"))
                mstrLoanvoucher_No = dtRow.Item("loanvoucher_no").ToString
                mdtEffective_Date = dtRow.Item("effective_date")
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
                mstrLoan_Purpose = dtRow.Item("loan_purpose").ToString
                mdecBalance_Amount = CDec(dtRow.Item("balance_amount"))
                mblnIsbrought_Forward = CBool(dtRow.Item("isbrought_forward"))
                mblnIsloan = CBool(dtRow.Item("isloan"))
                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mdecLoan_Amount = CDec(dtRow.Item("loan_amount"))
                mdecInterest_Rate = CDec(dtRow.Item("interest_rate"))
                mintLoan_Duration = CInt(dtRow.Item("loan_duration"))
                mdecInterest_Amount = CDec(dtRow.Item("interest_amount"))
                mdecNet_Amount = CDec(dtRow.Item("net_amount"))
                mintCalctype_Id = CInt(dtRow.Item("calctype_id"))
                mintLoanscheduleunkid = CInt(dtRow.Item("loanscheduleunkid"))
                mintEmi_Tenure = CInt(dtRow.Item("emi_tenure"))
                mdecEmi_Amount = CDec(dtRow.Item("emi_amount"))
                mdecAdvance_Amount = CDec(dtRow.Item("advance_amount"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintLoanStatus = dtRow.Item("loan_statusunkid")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintDeductionperiodunkid = CInt(dtRow.Item("deductionperiodunkid"))
                'S.SANDEEP [06 MAY 2015] -- START
                mintCountryUnkid = CInt(dtRow.Item("countryunkid"))
                mdecLoanBf_Balance = CDec(dtRow.Item("bf_balance"))
                'S.SANDEEP [06 MAY 2015] -- END
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                mdecBalance_AmountPaidCurrency = CDec(dtRow.Item("balance_amountPaidCurrency"))
                mdecExchange_rate = CDec(dtRow.Item("exchange_rate"))
                mdecBasecurrency_amount = CDec(dtRow.Item("basecurrency_amount"))
                mdecOpening_balance = CDec(dtRow.Item("opening_balance"))
                mdecOpening_balancePaidCurrency = CDec(dtRow.Item("opening_balancePaidCurrency"))
                'Sohail (07 May 2015) -- End
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                mintInterest_Calctype_Id = CInt(dtRow.Item("interest_calctype_id"))
                'Sohail (15 Dec 2015) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                mintMapped_TranheadUnkid = Convert.ToInt32(dtRow.Item("mapped_tranheadunkid"))
                'Sohail (29 Apr 2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '' <summary>
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal intLoanAdvanceTranUnkID As Integer = 0, _
    '                        Optional ByVal intEmpUnkID As Integer = 0, _
    '                        Optional ByVal intStatusID As Integer = 0, _
    '                        Optional ByVal dtPeriodStart As DateTime = Nothing, _
    '                        Optional ByVal dtPeriodEnd As DateTime = Nothing, _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "", _
    '                        Optional ByVal strFilter As String = "") As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try
    '        strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
    '                      ", ISNULL(hremployee_master.employeecode,'') + ' - ' +  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
    '                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                      ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
    '                             "THEN lnloan_scheme_master.code + ' - ' +   lnloan_scheme_master.name " & _
    '                             "ELSE @Advance " & _
    '                        "END AS LoanScheme " & _
    '                      ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
    '                             "ELSE @Advance " & _
    '                        "END AS Loan_Advance " & _
    '                      ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
    '                             "ELSE 2 " & _
    '                        "END AS Loan_AdvanceUnkid " & _
    '                      ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
    '                             "THEN lnloan_advance_tran.loan_amount " & _
    '                             "ELSE lnloan_advance_tran.advance_amount " & _
    '                        "END AS Amount " & _
    '                      ", cfcommon_period_tran.period_name AS PeriodName " & _
    '                      ", lnloan_advance_tran.loan_statusunkid " & _
    '                      ", lnloan_advance_tran.periodunkid " & _
    '                      ", lnloan_advance_tran.loanadvancetranunkid " & _
    '                      ", hremployee_master.employeeunkid " & _
    '                      ", lnloan_scheme_master.loanschemeunkid " & _
    '                      ", lnloan_advance_tran.isloan " & _
    '                      ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
    '                      ", lnloan_advance_tran.balance_amount " & _
    '                      ", lnloan_advance_tran.isloan " & _
    '                      ", lnloan_advance_tran.loanschemeunkid " & _
    '                      ", lnloan_advance_tran.emi_amount " & _
    '                      ", lnloan_advance_tran.advance_amount " & _
    '                      ", lnloan_advance_tran.loan_statusunkid " & _
    '                      ", lnloan_advance_tran.processpendingloanunkid " & _
    '                      ", lnloan_advance_tran.deductionperiodunkid " & _
    '                      ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
    '                      ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
    '                      ", isbrought_forward AS isbrought_forward " & _
    '                      ", LoanStatus.loanstatustranunkid " & _
    '                      ", CONVERT(CHAR(8), LoanStatus.status_date, 112) AS status_date " & _
    '                      ", LoanStatus.statusunkid " & _
    '                      ", LoanStatus.remark " & _
    '                      ", hremployee_master.employeecode AS empcode " & _
    '                      ", lnloan_scheme_master.code AS loancode " & _
    '                      ", lnloan_advance_tran.emi_tenure AS Intallments " & _
    '                      ", CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
    '                      "         WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
    '                      "         WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
    '                      "         WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed END AS loan_status "
    '        'S.SANDEEP [06 MAY 2015] -- START
    '        strQ &= ", ISNULL(Cur.cSign,'') AS cSign " & _
    '                ", lnloan_advance_tran.bf_balance " & _
    '                ", ISNULL(exchange_rate,0) AS exchange_rate " & _
    '                ", ISNULL(basecurrency_amount,0) AS basecurrency_amount " & _
    '                ", ISNULL(balance_amountPaidCurrency,0) AS balance_amountPaidCurrency " & _
    '                ", ISNULL(opening_balance,0) AS opening_balance " & _
    '                ", ISNULL(opening_balancePaidCurrency,0) AS opening_balancePaidCurrency "
    '        'Sohail (07 May 2015) - [balance_amountPaidCurrency, exchange_rate, basecurrency_amount, opening_balance, opening_balancePaidCurrency]
    '        'S.SANDEEP [06 MAY 2015] -- END
    '        strQ &= "FROM    lnloan_advance_tran " & _
    '                        "LEFT JOIN ( SELECT  lnloan_status_tran.loanstatustranunkid " & _
    '                                          ", lnloan_status_tran.loanadvancetranunkid " & _
    '                                          ", lnloan_status_tran.status_date " & _
    '                                          ", lnloan_status_tran.statusunkid " & _
    '                                          ", lnloan_status_tran.remark " & _
    '                                    "FROM    lnloan_status_tran " & _
    '                                            "JOIN ( SELECT   MAX(lnloan_status_tran.loanstatustranunkid) AS loanstatustranunkid " & _
    '                                                          ", lnloan_status_tran.loanadvancetranunkid " & _
    '                                    "FROM    lnloan_status_tran " & _
    '                                            "INNER JOIN ( SELECT MAX(lnloan_status_tran.status_date) AS status_date " & _
    '                                                              ", lnloan_status_tran.loanadvancetranunkid " & _
    '                                                         "FROM   lnloan_status_tran " & _
    '                                                         "WHERE  ISNULL(isvoid, 0) = 0 " & _
    '                                                         "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
    '                                                                       ") AS Loan_Status ON lnloan_status_tran.loanadvancetranunkid = Loan_Status.loanadvancetranunkid " & _
    '                                                                              "AND lnloan_status_tran.status_date = Loan_Status.status_date " & _
    '                                    "WHERE   ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
    '                                                   "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
    '                                                 ") AS A ON A.loanstatustranunkid = lnloan_status_tran.loanstatustranunkid " & _
    '                                  ") AS LoanStatus ON lnloan_advance_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
    '                        "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
    '                                                          "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
    '                        "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
    '                        "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
    '                                                                              "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " "

    '        'S.SANDEEP [06 MAY 2015] -- START
    '        '" WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
    '        strQ &= " LEFT JOIN " & _
    '                " ( " & _
    '                "   SELECT " & _
    '                "        countryunkid AS cTid " & _
    '                "       ,currency_name AS cSign " & _
    '                "       ,ROW_NUMBER()OVER(PARTITION BY countryunkid ORDER BY exchange_date DESC) AS rno " & _
    '                "   FROM cfexchange_rate WHERE isactive = 1 " & _
    '                " )AS Cur ON Cur.cTid = lnloan_advance_tran.countryunkid AND Cur.rno = 1 " & _
    '                "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
    '        'S.SANDEEP [06 MAY 2015] -- END


    '        If dtPeriodStart <> Nothing AndAlso dtPeriodEnd <> Nothing Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
    '        End If

    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If

    '        objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
    '        objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))
    '        objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
    '        objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
    '        objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
    '        objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

    '        If intLoanAdvanceTranUnkID > 0 Then
    '            strQ &= "AND lnloan_advance_tran.loanadvancetranunkid = @loanadvancetranunkid "
    '            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceTranUnkID)
    '        End If

    '        If intEmpUnkID > 0 Then
    '            strQ &= "AND lnloan_advance_tran.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
    '        End If

    '        If intStatusID > 0 Then
    '            strQ &= "AND LoanStatus.statusunkid = @statusunkid "
    '            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
    '        End If

    '        If strFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & strFilter.Trim
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (10-Oct-2015) -- End

    'Hemant (26 Mar 2019) -- Start
    'ISSUE BBL#3577: Writing off loan/advance takes too much time.
    'Public Function GetList(ByVal xDataBaseName As String, _
    '                        ByVal xUserId As Integer, _
    '                        ByVal xYearId As Integer, _
    '                        ByVal xCompanyId As Integer, _
    '                        ByVal xIncludeInactiveEmp As Boolean, _
    '                        ByVal xPeriodStartDate As DateTime, _
    '                        ByVal xPeriodEndDate As DateTime, _
    '                        ByVal xAsOnDate As DateTime, _
    '                        ByVal xUserAccessFilterString As String, _
    '                        ByVal strTableName As String, _
    '                        Optional ByVal xFilterString As String = "", _
    '                        Optional ByVal intEmpUnkID As Integer = 0, _
    '                        Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
    '                        Optional ByVal mstrAdvanceFilter As String = "" _
    '                        ) As DataSet
    '    'Sohail (06 Jan 2016) - [blnApplyUserAccessFilter]
    '    'Nilay (10-Oct-2015) -- [strTableName], [intEmpUnkID]

    '    'Varsha (06 Jan 2018) -- Start
    '    'Enhancement : Advance filter option in Aruti Quickbook
    '    ' Added : [mstrAdvanceFilter]
    '    'Varsha (06 Jan 2018) -- End

    '    Dim dsList As DataSet = Nothing
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Dim objDataOperation As New clsDataOperation
    '    Try

    '        'Nilay (10-Oct-2015) -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStartDate, xPeriodEndDate, , , xDataBaseName)
    '        'Sohail (06 Jan 2016) -- Start
    '        'Enhancement - Show Close Year Process Logs on Close Year Wizard.
    '        'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEndDate, , xDataBaseName, xUserId, xCompanyId, xYearId, xUserAccessFilterString)
    '        If blnApplyUserAccessFilter = True Then
    '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEndDate, , xDataBaseName, xUserId, xCompanyId, xYearId, xUserAccessFilterString)
    '        End If
    '        'Sohail (06 Jan 2016) -- End
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEndDate, xDataBaseName)
    '        'Nilay (10-Oct-2015) -- End

    '        'Nilay (15-Dec-2015) -- Start
    '        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '        Dim dsLoanCalc As DataSet = GetLoanCalculationTypeList("List", True, True)
    '        Dim dicLoanCalc As Dictionary(Of Integer, String) = (From p In dsLoanCalc.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("Name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

    '        'Nilay (23-Feb-2016) -- Start
    '        'Dim dsInterestCalc As DataSet = GetLoan_Interest_Calculation_Type("List", True)
    '        Dim dsInterestCalc As DataSet = GetLoan_Interest_Calculation_Type("List", True, True)
    '        'Nilay (23-Feb-2016) -- End
    '        Dim dicInterestCalc As Dictionary(Of Integer, String) = (From p In dsInterestCalc.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("NAME").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
    '        'Nilay (15-Dec-2015) -- End

    '        StrQ = "SELECT " & _
    '                "	 LN.VocNo " & _
    '                "	,LN.effective_date " & _
    '                "	,LN.PeriodName " & _
    '                "	,LN.DeductionPeriodName " & _
    '                "	,LN.Employee " & _
    '                "	,LN.EmpName " & _
    '                "	,LN.LoanScheme " & _
    '                "	,LN.Loan_Advance " & _
    '                "	,LN.Loan_AdvanceUnkid " & _
    '                "	,LN.Amount " & _
    '                "	,LN.cSign " & _
    '                "	,LN.Deduction_Start_Date " & _
    '                "	,LN.Deduction_End_Date " & _
    '                "	,LN.loanadvancetranunkid " & _
    '                "	,LN.loanvoucher_no " & _
    '                "	,LN.periodunkid " & _
    '                "	,LN.employeeunkid " & _
    '                "	,LN.approverunkid " & _
    '                "	,LN.loan_purpose " & _
    '                "	,LN.balance_amount " & _
    '                "	,LN.isbrought_forward " & _
    '                "	,LN.isloan " & _
    '                "	,LN.loanschemeunkid " & _
    '                "	,LN.loan_amount " & _
    '                "	,LN.calctype_id " & _
    '                "   ,LN.LoanCalcType " & _
    '                "	,ISNULL(LN.interest_calctype_id, 1) AS interest_calctype_id " & _
    '                "   ,LN.InterestCalcType " & _
    '                "	,LN.advance_amount " & _
    '                "   ,CASE WHEN CONVERT(CHAR(8),LN.effective_date,112) <= @enddate THEN LN.loan_statusunkid ELSE -999 END loan_statusunkid " & _
    '                "	,LN.userunkid " & _
    '                "	,LN.isvoid " & _
    '                "	,LN.voiduserunkid " & _
    '                "	,LN.voiddatetime " & _
    '                "	,LN.voidreason " & _
    '                "	,LN.processpendingloanunkid " & _
    '                "	,LN.deductionperiodunkid " & _
    '                "	,LN.bf_amount " & _
    '                "	,LN.bf_balance " & _
    '                "	,LN.countryunkid " & _
    '                "   ,CASE WHEN ISNULL(EM.loan_duration, 0) <=0 THEN ISNULL(FULNEMI.loan_duration,0) ELSE ISNULL(EM.loan_duration, 0) END AS loan_duration " & _
    '                "   ,CASE WHEN ISNULL(EM.emi_tenure, 0) <=0 THEN ISNULL(FULNEMI.emi_tenure,0) ELSE ISNULL(EM.emi_tenure, 0) END AS installments " & _
    '                "   ,CASE WHEN ISNULL(EM.emi_amount, 0) <=0 THEN ISNULL(FULNEMI.emi_amount,0) ELSE ISNULL(EM.emi_amount, 0) END AS emi_amount " & _
    '                "   ,CASE WHEN ISNULL(LR.interest_rate, 0) <=0 THEN ISNULL(FULNRATE.interest_rate,0) ELSE ISNULL(LR.interest_rate, 0) END AS interest_rate " & _
    '                "   ,CASE WHEN ISNULL(LT.topup_amount, 0) <=0 THEN ISNULL(FULNTOPUP.topup_amount,0) ELSE ISNULL(LT.topup_amount, 0) END AS topup_amount " & _
    '                "   ,CASE WHEN LN.isloan=1 THEN " & _
    '                "           CASE WHEN CONVERT(CHAR(8),LN.effective_date,112) <= @enddate THEN ISNULL(LN.loan_status, '') ELSE @FutureLoan END " & _
    '                "         WHEN LN.isloan=0 THEN " & _
    '                "           CASE WHEN CONVERT(CHAR(8),LN.effective_date,112) <= @enddate THEN ISNULL(LN.loan_status, '') ELSE @FutureAdvance END " & _
    '                "    END AS loan_status " & _
    '                "   ,LN.empcode " & _
    '                "   ,LN.loancode " & _
    '                "   ,ISNULL(exchange_rate,0) AS exchange_rate " & _
    '                "   ,ISNULL(basecurrency_amount,0) AS basecurrency_amount " & _
    '                "   ,ISNULL(LN.balance_amountPaidCurrency,0) AS balance_amountPaidCurrency " & _
    '                "   ,ISNULL(LN.opening_balance,0) AS opening_balance " & _
    '                "   ,ISNULL(LN.opening_balancePaidCurrency,0) AS opening_balancePaidCurrency " & _
    '                "   ,LN.Assign_Start_Date " & _
    '                "   ,LN.Assign_End_Date " & _
    '                "   ,CASE WHEN ISNULL(LN.Deduction_Start_Date,0) > ISNULL(@startdate,0) THEN 0 ELSE 1 END AS visiblestatus " & _
    '                "   ,LN.otherop_approval " & _
    '                "   ,LN.opstatus_id " & _
    '                "   ,LN.interest_amount " & _
    '                "   ,LN.net_amount " & _
    '                "   ,ISNULL(TSUM.total_topup,0) AS total_topup " & _
    '                "   ,isexternal_entity  " & _
    '                "FROM " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "		 lnloan_advance_tran.loanvoucher_no AS VocNo " & _
    '                "		,CONVERT(CHAR(8),lnloan_advance_tran.effective_date,112) AS effective_date " & _
    '                "		,ISNULL(ASP.period_name,'') AS PeriodName " & _
    '                "		,ISNULL(DDP.period_name,'') AS DeductionPeriodName " & _
    '                "		,ISNULL(hremployee_master.employeecode,'') + ' - ' +  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
    '                "		,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                "		,CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_scheme_master.code + ' - ' +   lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
    '                "		,CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
    '                "		,CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
    '                "		,CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_advance_tran.loan_amount ELSE lnloan_advance_tran.advance_amount END AS Amount " & _
    '                "		,ISNULL(Cur.cSign,'') AS cSign " & _
    '                "		,CONVERT(CHAR(8), DDP.start_date, 112) AS Deduction_Start_Date " & _
    '                "		,CONVERT(CHAR(8), DDP.end_date, 112) AS Deduction_End_Date " & _
    '                "		,DENSE_RANK() OVER (PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY lnloan_status_tran.status_date DESC, lnloan_status_tran.loanstatustranunkid DESC ) AS LN_RNO " & _
    '                "		,lnloan_advance_tran.loanadvancetranunkid " & _
    '                "		,lnloan_advance_tran.loanvoucher_no " & _
    '                "		,lnloan_advance_tran.periodunkid " & _
    '                "		,lnloan_advance_tran.employeeunkid " & _
    '                "		,lnloan_advance_tran.approverunkid " & _
    '                "		,lnloan_advance_tran.loan_purpose " & _
    '                "		,lnloan_advance_tran.balance_amount " & _
    '                "		,lnloan_advance_tran.isbrought_forward " & _
    '                "		,lnloan_advance_tran.isloan " & _
    '                "		,lnloan_advance_tran.loanschemeunkid " & _
    '                "		,lnloan_advance_tran.loan_amount " & _
    '                "		,lnloan_advance_tran.calctype_id "
    '        StrQ &= "       ,CASE lnloan_advance_tran.calctype_id "
    '        For Each pair In dicLoanCalc
    '            StrQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
    '        Next
    '        StrQ &= "         END AS LoanCalcType "
    '        StrQ &= "       ,lnloan_advance_tran.interest_calctype_id "
    '        StrQ &= "       ,CASE lnloan_advance_tran.interest_calctype_id "
    '        For Each pair In dicInterestCalc
    '            StrQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
    '        Next
    '        StrQ &= "         END AS InterestCalcType "
    '        StrQ &= "		,lnloan_advance_tran.advance_amount " & _
    '                "		,lnloan_status_tran.statusunkid AS loan_statusunkid " & _
    '                "		,lnloan_advance_tran.userunkid " & _
    '                "		,lnloan_advance_tran.isvoid " & _
    '                "		,lnloan_advance_tran.voiduserunkid " & _
    '                "		,lnloan_advance_tran.voiddatetime " & _
    '                "		,lnloan_advance_tran.voidreason " & _
    '                "		,lnloan_advance_tran.processpendingloanunkid " & _
    '                "		,lnloan_advance_tran.deductionperiodunkid " & _
    '                "		,lnloan_advance_tran.bf_amount " & _
    '                "		,ISNULL(lnloan_advance_tran.bf_balance,0) AS bf_balance " & _
    '                "		,lnloan_advance_tran.countryunkid " & _
    '                "       ,hremployee_master.employeecode AS empcode " & _
    '                "       ,lnloan_scheme_master.code AS loancode " & _
    '                "       ,LSP.periodunkid AS LPeriodId " & _
    '                "       ,CASE WHEN lnloan_status_tran.statusunkid = " & enLoanStatus.IN_PROGRESS & " THEN @InProgress " & _
    '                "             WHEN lnloan_status_tran.statusunkid = " & enLoanStatus.ON_HOLD & " THEN @OnHold " & _
    '                "             WHEN lnloan_status_tran.statusunkid = " & enLoanStatus.WRITTEN_OFF & " THEN @WrittenOff " & _
    '                "             WHEN lnloan_status_tran.statusunkid = " & enLoanStatus.COMPLETED & " THEN @Completed END AS loan_status " & _
    '                "       , ISNULL(lnloan_advance_tran.exchange_rate,0) AS exchange_rate " & _
    '                "       , ISNULL(lnloan_advance_tran.basecurrency_amount,0) AS basecurrency_amount " & _
    '                "       , ISNULL(lnloan_advance_tran.balance_amountpaidcurrency,0) AS balance_amountpaidcurrency " & _
    '                "       , ISNULL(lnloan_advance_tran.opening_balance,0) AS opening_balance " & _
    '                "       , ISNULL(lnloan_advance_tran.opening_balancepaidcurrency,0) AS opening_balancepaidcurrency " & _
    '                "       , CONVERT(CHAR(8), ASP.start_date, 112) AS Assign_Start_Date " & _
    '                "       , CONVERT(CHAR(8), ASP.end_date, 112) AS Assign_End_Date " & _
    '                "       , CASE WHEN OP_STATUS.final_status = 1 AND OP_STATUS.final_approved = 0 THEN @Pending " & _
    '                "              WHEN OP_STATUS.final_status = 100 THEN @NoOperation " & _
    '                "              ELSE @Approved " & _
    '                "         END AS 'otherop_approval' " & _
    '                "       , CASE WHEN OP_STATUS.final_status = 1 AND OP_STATUS.final_approved = 0 THEN 1" & _
    '                "	           WHEN OP_STATUS.final_status = 100 THEN 100 " & _
    '                "	           ELSE 2 " & _
    '                "         END AS 'opstatus_id' " & _
    '                "       , ISNULL(lnloan_advance_tran.interest_amount,0) AS interest_amount " & _
    '                "       , ISNULL(lnloan_advance_tran.net_amount,0) AS net_amount " & _
    '                "       , lnloan_process_pending_loan.isexternal_entity " & _
    '                "	FROM lnloan_advance_tran " & _
    '                "       LEFT JOIN ( " & _
    '                "                   SELECT DISTINCT " & _
    '                "                            lnloan_advance_tran.loanadvancetranunkid " & _
    '                "                           ,final_status " & _
    '                "                           ,final_approved " & _
    '                "                   FROM lnloan_advance_tran " & _
    '                "                       JOIN lnloanotherop_approval_tran ON lnloanotherop_approval_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                "                   WHERE lnloanotherop_approval_tran.isvoid=0 AND lnloan_advance_tran.isvoid=0 " & _
    '                "                       AND final_status = 1 " & _
    '                "                   UNION " & _
    '                "                       SELECT " & _
    '                "                            lnloan_advance_tran.loanadvancetranunkid " & _
    '                "                           ,100 AS final_status " & _
    '                "                           ,CAST(0 AS BIT) AS final_approved " & _
    '                "                       FROM lnloan_advance_tran " & _
    '                "                       WHERE loanadvancetranunkid NOT IN(SELECT DISTINCT loanadvancetranunkid FROM lnloanotherop_approval_tran WHERE isvoid = 0) " & _
    '                "                           AND lnloan_advance_tran.isvoid=0 " & _
    '                "                 ) AS OP_STATUS ON OP_STATUS.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                "		LEFT JOIN lnloan_status_tran ON lnloan_status_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                "		LEFT JOIN cfcommon_period_tran AS LSP ON lnloan_status_tran.periodunkid = LSP.periodunkid AND LSP.modulerefid = " & enModuleReference.Payroll & " AND CONVERT(CHAR(8),LSP.start_date,112) <= @asondate " & _
    '                "		LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '                "		LEFT JOIN cfcommon_period_tran AS ASP ON ASP.periodunkid = lnloan_advance_tran.periodunkid AND ASP.modulerefid = " & enModuleReference.Payroll & " " & _
    '                "		LEFT JOIN cfcommon_period_tran AS DDP ON DDP.periodunkid = lnloan_advance_tran.deductionperiodunkid AND DDP.modulerefid = " & enModuleReference.Payroll & " " & _
    '                "		JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_advance_tran.employeeunkid " & _
    '                " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
    '                "		LEFT JOIN " & _
    '                "		( " & _
    '                "			SELECT " & _
    '                "				 countryunkid AS cTid " & _
    '                "				,currency_name AS cSign " & _
    '                "				,ROW_NUMBER()OVER(PARTITION BY countryunkid ORDER BY exchange_date DESC) AS rno " & _
    '                "			FROM cfexchange_rate WHERE isactive = 1 " & _
    '                "		)AS Cur ON Cur.cTid = lnloan_advance_tran.countryunkid AND Cur.rno = 1 "

    '        'Varsha Rana (12-Sept-2017) -- [isexternal_entity]
    '        'Nilay (21-Dec-2016) -- Start
    '        'NOTE: To display Future Loan on Loan Advance List
    '        'REMOVED: CASE WHEN ISNULL(LPeriodId,0) = 0 THEN  -999 ELSE LN.loan_statusunkid END loan_statusunkid
    '        'ADDED: CASE WHEN CONVERT(CHAR(8),LN.effective_date,112) <= @enddate THEN LN.loan_statusunkid ELSE -999 END loan_statusunkid
    '        'REMOVED: [CASE WHEN ISNULL(LPeriodId,0) = 0 THEN @FutureLoan ELSE ISNULL(LN.loan_status, '') END AS loan_status]
    '        'ADDED: "   ,CASE WHEN LN.isloan=1 THEN " & _
    '        '       "           CASE WHEN CONVERT(CHAR(8),LN.effective_date,112) <= @enddate THEN ISNULL(LN.loan_status, '') ELSE @FutureLoan END " & _
    '        '       "         WHEN LN.isloan=0 THEN " & _
    '        '       "           CASE WHEN CONVERT(CHAR(8),LN.effective_date,112) <= @enddate THEN ISNULL(LN.loan_status, '') ELSE @FutureAdvance END " & _
    '        '       "    END AS loan_status " & _
    '        'Nilay (21-Dec-2016) -- End

    '        'Nilay (08-Nov-2016) -- Start
    '        'REMOVED : ISNULL(SUM(LT.topup_amount) OVER (PARTITION BY LT.loanadvancetranunkid ORDER BY LT.loanadvancetranunkid, LT.end_date DESC),0) AS total_topup
    '        'ADDED : ISNULL(TSUM.total_topup,0) AS total_topup
    '        'Nilay (08-Nov-2016) -- End

    '        'Nilay (27-Oct-2016) -- [total_topup]
    '        'Nilay (20-Sept-2016) -- Start
    '        'Enhancement : Cancel feature for approved but not assigned loan application
    '        'ADDED Enum in [loan_status]
    '        'Nilay (20-Sept-2016) -- End

    '        'Nilay (21-Jul-2016) -- [interest_amount,net_amount]

    '        'Nilay (02-Jul-2016) -- Start
    '        'ADDED - [otherop_approval, opstatus_id]

    '        '------------------------------------------ ADDED -----------------------------------------
    '        '"LEFT JOIN ( " & _
    '        '"             SELECT DISTINCT " & _
    '        '"                      lnloan_advance_tran.loanadvancetranunkid " & _
    '        '"                     ,final_status " & _
    '        '"                     ,final_approved " & _
    '        '"             FROM lnloan_advance_tran " & _
    '        '"                 JOIN lnloanotherop_approval_tran ON lnloanotherop_approval_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '        '"             WHERE lnloanotherop_approval_tran.isvoid=0 AND lnloan_advance_tran.isvoid=0 " & _
    '        '"                 AND final_status = 1 " & _
    '        '"             UNION " & _
    '        '"                 SELECT " & _
    '        '"                      lnloan_advance_tran.loanadvancetranunkid " & _
    '        '"                     ,-100 AS final_status " & _
    '        '"                     ,CAST(0 AS BIT) AS final_approved " & _
    '        '"                 FROM lnloan_advance_tran " & _
    '        '"                 WHERE loanadvancetranunkid NOT IN(SELECT DISTINCT loanadvancetranunkid FROM lnloanotherop_approval_tran WHERE isvoid = 0) " & _
    '        '"                     AND lnloan_advance_tran.isvoid=0 " & _
    '        '"           ) AS OP_STATUS ON OP_STATUS.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '        'Nilay (02-Jul-2016) -- End

    '        'Nilay (10-Feb-2016) -- Start
    '        'OLD : DENSE_RANK() OVER (PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY LSP.end_date DESC, lnloan_status_tran.loanstatustranunkid DESC ) AS LN_RNO
    '        'NEW : DENSE_RANK() OVER (PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY lnloan_status_tran.status_date DESC, lnloan_status_tran.loanstatustranunkid DESC ) AS LN_RNO
    '        'Nilay (10-Feb-2016) -- End

    '        'Sohail (15 Dec 2015) -- Start
    '        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '        '[interest_calctype_id]
    '        'OLD -- DENSE_RANK() OVER (PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY LSP.end_date DESC, lnloan_status_tran.status_date DESC, lnloan_status_tran.loanstatustranunkid DESC ) AS LN_RNO
    '        'Sohail (15 Dec 2015) -- End

    '        'Shani(26-Nov-2015) -- [Assign_Start_Date,Assign_End_Date,visiblestatus]

    '        'Nilay (15-Dec-2015) -- Start
    '        '[LoanCalcType, InterestCalcType] -- ADDED
    '        '[Intallments] Replaced by [installments]
    '        '[LN.loan_statusunkid]-- Removed
    '        '----------------------------------------------- OLD -----------------------------------------------------------
    '        'CASE WHEN ISNULL(EM.loan_duration, 0) <=0 THEN FULNEMI.loan_duration ELSE ISNULL(EM.loan_duration, 0) END AS loan_duration 
    '        'CASE WHEN ISNULL(EM.emi_tenure, 0) <=0 THEN FULNEMI.emi_tenure ELSE ISNULL(EM.emi_tenure, 0) END AS installments " 
    '        'CASE WHEN ISNULL(EM.emi_amount, 0) <=0 THEN FULNEMI.emi_amount ELSE ISNULL(EM.emi_amount, 0) END AS emi_amount "
    '        'CASE WHEN ISNULL(LR.interest_rate, 0) <=0 THEN FULNRATE.interest_rate ELSE ISNULL(LR.interest_rate, 0) END AS interest_rate
    '        'CASE WHEN ISNULL(LT.topup_amount, 0) <=0 THEN FULNTOPUP.topup_amount ELSE ISNULL(LT.topup_amount, 0) END AS topup_amount
    '        '----------------------------------------------- NEW ----------------------------------------------------------------
    '        'CASE WHEN ISNULL(EM.loan_duration, 0) <=0 THEN ISNULL(FULNEMI.loan_duration,0) ELSE ISNULL(EM.loan_duration, 0) END AS loan_duration 
    '        'CASE WHEN ISNULL(EM.emi_tenure, 0) <=0 THEN ISNULL(FULNEMI.emi_tenure,0) ELSE ISNULL(EM.emi_tenure, 0) END AS installments " 
    '        'CASE WHEN ISNULL(EM.emi_amount, 0) <=0 THEN ISNULL(FULNEMI.emi_amount,0) ELSE ISNULL(EM.emi_amount, 0) END AS emi_amount "
    '        'CASE WHEN ISNULL(LR.interest_rate, 0) <=0 THEN ISNULL(FULNRATE.interest_rate,0) ELSE ISNULL(LR.interest_rate, 0) END AS interest_rate
    '        'CASE WHEN ISNULL(LT.topup_amount, 0) <=0 THEN ISNULL(FULNTOPUP.topup_amount,0) ELSE ISNULL(LT.topup_amount, 0) END AS topup_amount

    '        'Nilay (15-Dec-2015) -- End

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= xDateJoinQry
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If


    '        'Varsha (06 Jan 2018) -- Start
    '        'Enhancement : Advance filter option in Aruti Quickbook
    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= xAdvanceJoinQry
    '        End If
    '        'Varsha (06 Jan 2018) -- End     


    '        StrQ &= "	WHERE lnloan_advance_tran.isvoid = 0 AND lnloan_status_tran.isvoid = 0 "

    '        'Pinkal (17-Dec-2015) -- Start
    '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
    '        If intEmpUnkID > 0 Then
    '            StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
    '        End If
    '        'Pinkal (17-Dec-2015) -- End

    '        'Nilay (28-Aug-2015) -- Start
    '        '"   ,ISNULL(LN.loan_status,'') AS loan_status " -- Replaced With "   ,CASE WHEN ISNULL(LPeriodId,0) = 0 THEN @FutureLoan ELSE ISNULL(LN.loan_status, '') END AS loan_status "
    '        '"   ,CASE WHEN ISNULL(LPeriodId,0) = 0 THEN  -999 ELSE LN.loan_statusunkid END loan_statusunkid " -- Added
    '        '"       ,LSP.periodunkid AS LPeriodId " -- Added
    '        '"	AND CONVERT(CHAR(8),LSP.start_date,112) <= @asondate " -- Removed
    '        '"		LEFT JOIN cfcommon_period_tran AS LSP ON lnloan_status_tran.periodunkid = LSP.periodunkid AND LSP.modulerefid = " & enModuleReference.Payroll & " " -- Replaced With below
    '        '"		LEFT JOIN cfcommon_period_tran AS LSP ON lnloan_status_tran.periodunkid = LSP.periodunkid AND LSP.modulerefid = " & enModuleReference.Payroll & " AND CONVERT(CHAR(8),LSP.start_date,112) <= @asondate " -- Added

    '        'Reason - To display following field when loan status is future deduction loan
    '        ' ----------------------- Removed ----------------------
    '        '"	,ISNULL(EM.loan_duration,0) AS loan_duration " & _ 
    '        '"	,ISNULL(EM.emi_tenure,0) AS Intallments " & _
    '        '"	,ISNULL(EM.emi_amount,0) AS emi_amount " & _
    '        '"	,ISNULL(LR.interest_rate,0) AS interest_rate " & _
    '        '"	,ISNULL(LT.topup_amount,0) AS topup_amount " & _
    '        ' ----------------------- Removed ----------------------
    '        ' ----------------------------------------------------- Added -----------------------------------------------------------------
    '        '"   ,CASE WHEN ISNULL(EM.loan_duration, 0) <=0 THEN FULNEMI.loan_duration ELSE ISNULL(EM.loan_duration, 0) END AS loan_duration " & _
    '        '"   ,CASE WHEN ISNULL(EM.emi_tenure, 0) <=0 THEN FULNEMI.emi_tenure ELSE ISNULL(EM.emi_tenure, 0) END AS Intallments " & _
    '        '"   ,CASE WHEN ISNULL(EM.emi_amount, 0) <=0 THEN FULNEMI.emi_amount ELSE ISNULL(EM.emi_amount, 0) END AS emi_amount " & _
    '        '"   ,CASE WHEN ISNULL(LR.interest_rate, 0) <=0 THEN FULNRATE.interest_rate ELSE ISNULL(LR.interest_rate, 0) END AS interest_rate " & _
    '        '"   ,CASE WHEN ISNULL(LT.topup_amount, 0) <=0 THEN FULNTOPUP.topup_amount ELSE ISNULL(LT.topup_amount, 0) END AS topup_amount " & _
    '        ' ----------------------------------------------------- Added -----------------------------------------------------------------

    '        'Nilay (28-Aug-2015) -- End

    '        'Sohail (07 May 2015) - [balance_amountPaidCurrency, exchange_rate, basecurrency_amount, opening_balance, opening_balancePaidCurrency]

    '        'Nilay (10-Oct-2015) -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'If xIncludeInactiveEmp = False Then
    '        '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        'End If

    '        If xIncludeInactiveEmp = False Then
    '            If xDateFilterQry.Trim.Length > 0 Then
    '                StrQ &= xDateFilterQry
    '            End If
    '        End If
    '        'Nilay (10-Oct-2015) -- End

    '        'Nilay (10-Oct-2015) -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'If xUserAccessFilterString.Trim.Length > 0 Then
    '        '    StrQ &= xUserAccessFilterString
    '        'End If

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If
    '        'Nilay (10-Oct-2015) -- End


    '        ''Varsha (06 Jan 2018) -- Start
    '        ''Enhancement : Advance filter option in Aruti Quickbook
    '        If mstrAdvanceFilter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvanceFilter
    '        End If
    '        ''Varsha (06 Jan 2018) -- End


    '        'Nilay (15-Dec-2015) -- Start
    '        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '        'StrQ &= ") AS LN " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 DENSE_RANK() OVER (PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC) AS EM_RNO " & _
    '        '        "		,lnloan_advance_tran.loanadvancetranunkid " & _
    '        '        "		,lnloan_emitenure_tran.loan_duration " & _
    '        '        "		,lnloan_emitenure_tran.emi_tenure " & _
    '        '        "		,lnloan_emitenure_tran.emi_amount " & _
    '        '        "	FROM lnloan_advance_tran " & _
    '        '        "		LEFT JOIN lnloan_emitenure_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
    '        '        "		LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
    '        '        "	WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 AND ISNULL(lnloan_emitenure_tran.isvoid,0) = 0 " & _
    '        '        "	AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @asondate " & _
    '        '        ") AS EM ON LN.loanadvancetranunkid = EM.loanadvancetranunkid AND EM.EM_RNO = 1 " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 DENSE_RANK() OVER (PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY end_date DESC,lnloan_interest_tran.effectivedate DESC) AS LR_RNO " & _
    '        '        "		,ISNULL(lnloan_interest_tran.interest_rate,0) AS interest_rate " & _
    '        '        "		,lnloan_advance_tran.loanadvancetranunkid " & _
    '        '        "	FROM lnloan_advance_tran " & _
    '        '        "		LEFT JOIN lnloan_interest_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
    '        '        "		LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
    '        '        "	WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 AND ISNULL(lnloan_interest_tran.isvoid,0) = 0 " & _
    '        '        "	AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @asondate " & _
    '        '        ") AS LR ON LN.loanadvancetranunkid = LR.loanadvancetranunkid AND LR.LR_RNO = 1 " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 DENSE_RANK() OVER (PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY end_date DESC) AS LT_RNO " & _
    '        '        "		,lnloan_advance_tran.loanadvancetranunkid " & _
    '        '        "		,lnloan_topup_tran.topup_amount " & _
    '        '        "	FROM lnloan_advance_tran " & _
    '        '        "		LEFT JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
    '        '        "		LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_topup_tran.periodunkid " & _
    '        '        "	WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 AND ISNULL(lnloan_topup_tran.isvoid,0) = 0 " & _
    '        '        "	AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @asondate " & _
    '        '        ") AS LT ON LN.loanadvancetranunkid = LT.loanadvancetranunkid AND LT.LT_RNO = 1 "

    '        StrQ &= ") AS LN " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "		 DENSE_RANK() OVER (PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC) AS EM_RNO " & _
    '                "		,lnloan_advance_tran.loanadvancetranunkid " & _
    '                "		,ISNULL(lnloan_emitenure_tran.loan_duration,0) AS loan_duration " & _
    '                "		,ISNULL(lnloan_emitenure_tran.emi_tenure,0) AS emi_tenure " & _
    '                "		,ISNULL(lnloan_emitenure_tran.emi_amount,0) AS emi_amount " & _
    '                "	FROM lnloan_advance_tran " & _
    '                "		LEFT JOIN lnloan_emitenure_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
    '                "		LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
    '                "	WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 AND ISNULL(lnloan_emitenure_tran.isvoid,0) = 0 " & _
    '                "	AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @asondate "

    '        'Pinkal (17-Dec-2015) -- Start
    '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
    '        If intEmpUnkID > 0 Then
    '            StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
    '        End If
    '        'Pinkal (17-Dec-2015) -- End


    '        StrQ &= ") AS EM ON LN.loanadvancetranunkid = EM.loanadvancetranunkid AND EM.EM_RNO = 1 " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "		 DENSE_RANK() OVER (PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY end_date DESC,lnloan_interest_tran.effectivedate DESC) AS LR_RNO " & _
    '                "		,ISNULL(lnloan_interest_tran.interest_rate,0) AS interest_rate " & _
    '                "		,lnloan_advance_tran.loanadvancetranunkid " & _
    '                "	FROM lnloan_advance_tran " & _
    '                "		LEFT JOIN lnloan_interest_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
    '                "		LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
    '                "	WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 AND ISNULL(lnloan_interest_tran.isvoid,0) = 0 " & _
    '                "	AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @asondate "

    '        'Pinkal (17-Dec-2015) -- Start
    '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
    '        If intEmpUnkID > 0 Then
    '            StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
    '        End If
    '        'Pinkal (17-Dec-2015) -- End


    '        StrQ &= ") AS LR ON LN.loanadvancetranunkid = LR.loanadvancetranunkid AND LR.LR_RNO = 1 " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "		 DENSE_RANK() OVER (PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY end_date DESC) AS LT_RNO " & _
    '                "		,lnloan_advance_tran.loanadvancetranunkid " & _
    '                "		,ISNULL(lnloan_topup_tran.topup_amount,0) AS topup_amount " & _
    '                "       ,cfcommon_period_tran.end_date AS end_date " & _
    '                "	FROM lnloan_advance_tran " & _
    '                "		LEFT JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
    '                "		LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_topup_tran.periodunkid " & _
    '                "	WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 AND ISNULL(lnloan_topup_tran.isvoid,0) = 0 " & _
    '                "	AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @asondate "
    '        'Nilay (27-Oct-2016) -- [end_date]

    '        'Pinkal (17-Dec-2015) -- Start
    '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
    '        If intEmpUnkID > 0 Then
    '            StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
    '        End If
    '        'Pinkal (17-Dec-2015) -- End

    '        StrQ &= ") AS LT ON LN.loanadvancetranunkid = LT.loanadvancetranunkid AND LT.LT_RNO = 1 "

    '        'Nilay (15-Dec-2015) -- End

    '        'Nilay (28-Aug-2015) -- Start
    '        '" WHERE 1 = 1 AND LN.LN_RNO = 1 "
    '        'Reason - When Loan is applied for month other than first open period (Future Loan) then to display data of loan_duration, installments, emi_amount, 
    '        '         interest_rate, topup_amount 
    '        '         List of JOINS added - FULNEMI, FULNRATE, FULNTOPUP

    '        'Nilay (15-Dec-2015) -- Start
    '        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '        'StrQ &= "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        lnloan_advance_tran.loanadvancetranunkid " & _
    '        '        "       ,lnloan_emitenure_tran.loan_duration " & _
    '        '        "       ,lnloan_emitenure_tran.emi_tenure " & _
    '        '        "       ,lnloan_emitenure_tran.emi_amount " & _
    '        '        "   FROM lnloan_advance_tran " & _
    '        '        "       LEFT JOIN lnloan_emitenure_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
    '        '        "   WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '        '        "   AND ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 " & _
    '        '        ") AS FULNEMI ON LN.loanadvancetranunkid = FULNEMI.loanadvancetranunkid " & _
    '        '        " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        ISNULL(lnloan_interest_tran.interest_rate,0) AS interest_rate " & _
    '        '        "       ,lnloan_advance_tran.loanadvancetranunkid " & _
    '        '        "   FROM lnloan_advance_tran " & _
    '        '                "LEFT JOIN lnloan_interest_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
    '        '        "   WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '        '        "   AND ISNULL(lnloan_interest_tran.isvoid, 0) = 0 " & _
    '        '        ") AS FULNRATE ON LN.loanadvancetranunkid = FULNRATE.loanadvancetranunkid " & _
    '        '        " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        lnloan_advance_tran.loanadvancetranunkid " & _
    '        '        "       ,lnloan_topup_tran.topup_amount " & _
    '        '        "   FROM lnloan_advance_tran " & _
    '        '        "       LEFT JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
    '        '        "   WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '        '        "   AND ISNULL(lnloan_topup_tran.isvoid, 0) = 0 " & _
    '        '        ") AS FULNTOPUP ON LN.loanadvancetranunkid = FULNTOPUP.loanadvancetranunkid " & _
    '        '        " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 " & _
    '        '        " WHERE 1 = 1 AND LN.LN_RNO = 1 "

    '        StrQ &= "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        lnloan_advance_tran.loanadvancetranunkid " & _
    '                "       ,ISNULL(lnloan_emitenure_tran.loan_duration,0) AS loan_duration " & _
    '                "       ,ISNULL(lnloan_emitenure_tran.emi_tenure,0) AS emi_tenure " & _
    '                "       ,ISNULL(lnloan_emitenure_tran.emi_amount,0) AS emi_amount " & _
    '                "   FROM lnloan_advance_tran " & _
    '                "       LEFT JOIN lnloan_emitenure_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
    '                "   WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                "   AND ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 "

    '        'Pinkal (17-Dec-2015) -- Start
    '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
    '        If intEmpUnkID > 0 Then
    '            StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
    '        End If
    '        'Pinkal (17-Dec-2015) -- End

    '        StrQ &= ") AS FULNEMI ON LN.loanadvancetranunkid = FULNEMI.loanadvancetranunkid " & _
    '                " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        ISNULL(lnloan_interest_tran.interest_rate,0) AS interest_rate " & _
    '                "       ,lnloan_advance_tran.loanadvancetranunkid " & _
    '                "   FROM lnloan_advance_tran " & _
    '                        "LEFT JOIN lnloan_interest_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
    '                "   WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                "   AND ISNULL(lnloan_interest_tran.isvoid, 0) = 0 " 
    '        'Pinkal (17-Dec-2015) -- Start
    '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
    '        If intEmpUnkID > 0 Then
    '            StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
    '        End If
    '        'Pinkal (17-Dec-2015) -- End


    '        StrQ &= ") AS FULNRATE ON LN.loanadvancetranunkid = FULNRATE.loanadvancetranunkid " & _
    '                " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        lnloan_advance_tran.loanadvancetranunkid " & _
    '                "       ,ISNULL(lnloan_topup_tran.topup_amount,0) AS topup_amount " & _
    '                "   FROM lnloan_advance_tran " & _
    '                "       LEFT JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
    '                "   WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                "   AND ISNULL(lnloan_topup_tran.isvoid, 0) = 0 "
    '        'Pinkal (17-Dec-2015) -- Start
    '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
    '        If intEmpUnkID > 0 Then
    '            StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
    '        End If
    '        'Pinkal (17-Dec-2015) -- End

    '        'Nilay (08-Nov-2016) -- Start
    '        'StrQ &= ") AS FULNTOPUP ON LN.loanadvancetranunkid = FULNTOPUP.loanadvancetranunkid " & _
    '        '       " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 " & _
    '        '       " WHERE 1 = 1 AND LN.LN_RNO = 1 "
    '        StrQ &= ") AS FULNTOPUP ON LN.loanadvancetranunkid = FULNTOPUP.loanadvancetranunkid " & _
    '               " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 "

    '        StrQ &= "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        lnloan_advance_tran.loanadvancetranunkid " & _
    '                "       ,ISNULL(SUM(topup_amount),0) AS total_topup " & _
    '                "   FROM lnloan_advance_tran " & _
    '                "        LEFT JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
    '                "   WHERE    lnloan_advance_tran.isvoid = 0 " & _
    '                "        AND lnloan_topup_tran.isvoid = 0 " & _
    '                "   GROUP BY lnloan_advance_tran.loanadvancetranunkid " & _
    '                ") AS TSUM ON TSUM.loanadvancetranunkid = LN.loanadvancetranunkid "

    '        StrQ &= " WHERE 1 = 1 AND LN.LN_RNO = 1 "
    '        'Nilay (08-Nov-2016) -- End

    '        'Nilay (15-Dec-2015) -- End
    '        'Nilay (28-Aug-2015) -- End

    '        If xFilterString.Trim.Length > 0 Then
    '            StrQ &= " AND " & xFilterString
    '        End If

    '        'Nilay (27-Oct-2016) -- Start
    '        StrQ &= " ORDER by LN.loanadvancetranunkid, LN.loanvoucher_no "
    '        'Nilay (27-Oct-2016) -- End

    '        'Nilay (02-Jul-2016) -- Start
    '        objDataOperation.ClearParameters()
    '        'Nilay (02-Jul-2016) -- End

    '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStartDate))
    '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEndDate))
    '        objDataOperation.AddParameter("@asondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEndDate))

    '        objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
    '        objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))
    '        objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
    '        objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
    '        objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
    '        objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))
    '        'Nilay (28-Aug-2015) -- Start
    '        objDataOperation.AddParameter("@FutureLoan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Future Loan"))
    '        'Nilay (28-Aug-2015) -- End

    '        'Nilay (21-Dec-2016) -- Start
    '        '@FutureAdvance
    '        objDataOperation.AddParameter("@FutureAdvance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Future Advance"))
    '        'Nilay (21-Dec-2016) -- End

    '        'Nilay (02-Jul-2016) -- Start
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 1, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 2, "Approved"))
    '        objDataOperation.AddParameter("@NoOperation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 7, "No Other Operation"))
    '        'Nilay (02-Jul-2016) -- End


    '        'Nilay (06-Nov-2015) -- Start
    '        'dsList = objDataOperation.ExecQuery(StrQ, "List")
    '        dsList = objDataOperation.ExecQuery(StrQ, strTableName)
    '        'Nilay (06-Nov-2015) -- End

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetList(ByVal xDataBaseName As String, _
                            ByVal xUserId As Integer, _
                            ByVal xYearId As Integer, _
                            ByVal xCompanyId As Integer, _
                            ByVal xIncludeInactiveEmp As Boolean, _
                            ByVal xPeriodStartDate As DateTime, _
                            ByVal xPeriodEndDate As DateTime, _
                            ByVal xAsOnDate As DateTime, _
                            ByVal xUserAccessFilterString As String, _
                            ByVal strTableName As String, _
                            Optional ByVal xFilterString As String = "", _
                            Optional ByVal intEmpUnkID As Integer = 0, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal mstrAdvanceFilter As String = "" _
                            ) As DataSet


        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim objMaster As New clsMasterData
        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStartDate, xPeriodEndDate, , , xDataBaseName)
            
            If blnApplyUserAccessFilter = True Then
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEndDate, , xDataBaseName, xUserId, xCompanyId, xYearId, xUserAccessFilterString)
            End If

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEndDate, xDataBaseName)

            Dim dsLoanCalc As DataSet = GetLoanCalculationTypeList("List", True, True)
            Dim dicLoanCalc As Dictionary(Of Integer, String) = (From p In dsLoanCalc.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("Name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            
            Dim dsInterestCalc As DataSet = GetLoan_Interest_Calculation_Type("List", True, True)
            Dim dicInterestCalc As Dictionary(Of Integer, String) = (From p In dsInterestCalc.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("NAME").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            Dim dsLoanSavingsStatus As DataSet = objMaster.GetLoan_Saving_Status("List")
            Dim dicLoanSavingsStatus As Dictionary(Of Integer, String) = (From p In dsLoanSavingsStatus.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("NAME").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)


            StrQ = "SELECT   hremployee_master.employeeunkid " & _
                               ", employeecode  " & _
                               ", firstname " & _
                               ", othername " & _
                               ", surname " & _
                               ", email "
            'Sohail (17 Feb 2021) - [email]

            StrQ &= "INTO #TableEmp " & _
                     "FROM hremployee_master "
                      
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If


            StrQ &= "WHERE 1 = 1 "

            If intEmpUnkID > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = " & intEmpUnkID
            End If


            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter & " "
            End If

            StrQ &= "                   SELECT DISTINCT " & _
                   "                            lnloan_advance_tran.loanadvancetranunkid " & _
                   "                           ,final_status " & _
                   "                           ,final_approved " & _
                   "                   INTO #TableOP_STATUS " & _
                   "                   FROM lnloan_advance_tran " & _
                   "                       JOIN lnloanotherop_approval_tran ON lnloanotherop_approval_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                   "                   WHERE lnloanotherop_approval_tran.isvoid=0 AND lnloan_advance_tran.isvoid=0 " & _
                   "                       AND final_status = 1 " & _
                   "                   UNION " & _
                   "                       SELECT " & _
                   "                            lnloan_advance_tran.loanadvancetranunkid " & _
                   "                           ,100 AS final_status " & _
                   "                           ,CAST(0 AS BIT) AS final_approved " & _
                   "                       FROM lnloan_advance_tran " & _
                   "                       WHERE loanadvancetranunkid NOT IN(SELECT DISTINCT loanadvancetranunkid FROM lnloanotherop_approval_tran WHERE isvoid = 0) " & _
                   "                           AND lnloan_advance_tran.isvoid=0 "

            StrQ &= "			SELECT " & _
                    "				 countryunkid AS cTid " & _
                    "				,currency_name AS cSign " & _
                    "				,ROW_NUMBER()OVER(PARTITION BY countryunkid ORDER BY exchange_date DESC) AS rno " & _
                    "        INTO #TableCur " & _
                    "			FROM cfexchange_rate WHERE isactive = 1 "

            StrQ &= "	SELECT " & _
                  "		 DENSE_RANK() OVER (PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC) AS EM_RNO " & _
                  "		,lnloan_advance_tran.loanadvancetranunkid " & _
                  "		,ISNULL(lnloan_emitenure_tran.loan_duration,0) AS loan_duration " & _
                  "		,ISNULL(lnloan_emitenure_tran.emi_tenure,0) AS emi_tenure " & _
                  "		,ISNULL(lnloan_emitenure_tran.emi_amount,0) AS emi_amount " & _
                  "   INTO #TableEM " & _
                  "	FROM lnloan_advance_tran " & _
                  "		LEFT JOIN lnloan_emitenure_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
                  "		LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
                  "	WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 AND ISNULL(lnloan_emitenure_tran.isvoid,0) = 0 " & _
                  "	AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @asondate "


            If intEmpUnkID > 0 Then
                StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
            End If

            StrQ &= "	SELECT " & _
                   "		 DENSE_RANK() OVER (PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY end_date DESC,lnloan_interest_tran.effectivedate DESC) AS LR_RNO " & _
                   "		,ISNULL(lnloan_interest_tran.interest_rate,0) AS interest_rate " & _
                   "		,lnloan_advance_tran.loanadvancetranunkid " & _
                   "   INTO #TableLR " & _
                   "	FROM lnloan_advance_tran " & _
                   "		LEFT JOIN lnloan_interest_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
                   "		LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
                   "	WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 AND ISNULL(lnloan_interest_tran.isvoid,0) = 0 " & _
                   "	AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @asondate "

            If intEmpUnkID > 0 Then
                StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
            End If

            StrQ &= "	SELECT " & _
                    "		 DENSE_RANK() OVER (PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY end_date DESC) AS LT_RNO " & _
                    "		,lnloan_advance_tran.loanadvancetranunkid " & _
                    "		,ISNULL(lnloan_topup_tran.topup_amount,0) AS topup_amount " & _
                    "       ,cfcommon_period_tran.end_date AS end_date " & _
                    "   INTO #TableLT " & _
                    "	FROM lnloan_advance_tran " & _
                    "		LEFT JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
                    "		LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_topup_tran.periodunkid " & _
                    "	WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 AND ISNULL(lnloan_topup_tran.isvoid,0) = 0 " & _
                    "	AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @asondate "

            If intEmpUnkID > 0 Then
                StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
            End If

            StrQ &= "   SELECT " & _
                    "        lnloan_advance_tran.loanadvancetranunkid " & _
                    "       ,ISNULL(lnloan_emitenure_tran.loan_duration,0) AS loan_duration " & _
                    "       ,ISNULL(lnloan_emitenure_tran.emi_tenure,0) AS emi_tenure " & _
                    "       ,ISNULL(lnloan_emitenure_tran.emi_amount,0) AS emi_amount " & _
                    "   INTO #TableFULNEMI " & _
                    "   FROM lnloan_advance_tran " & _
                    "       LEFT JOIN lnloan_emitenure_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
                    "   WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                    "   AND ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 "

            If intEmpUnkID > 0 Then
                StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
            End If

            StrQ &= "   SELECT " & _
                    "        ISNULL(lnloan_interest_tran.interest_rate,0) AS interest_rate " & _
                    "       ,lnloan_advance_tran.loanadvancetranunkid " & _
                    "   INTO #TableFULNRATE " & _
                    "   FROM lnloan_advance_tran " & _
                            "LEFT JOIN lnloan_interest_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
                    "   WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                    "   AND ISNULL(lnloan_interest_tran.isvoid, 0) = 0 "

            If intEmpUnkID > 0 Then
                StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
            End If

            StrQ &= "   SELECT " & _
                    "        lnloan_advance_tran.loanadvancetranunkid " & _
                    "       ,ISNULL(lnloan_topup_tran.topup_amount,0) AS topup_amount " & _
                    "   INTO #TableFULNTOPUP " & _
                    "   FROM lnloan_advance_tran " & _
                    "       LEFT JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
                    "   WHERE ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                    "   AND ISNULL(lnloan_topup_tran.isvoid, 0) = 0 "

            If intEmpUnkID > 0 Then
                StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
            End If

            StrQ &= "   SELECT " & _
                    "        lnloan_advance_tran.loanadvancetranunkid " & _
                    "       ,ISNULL(SUM(topup_amount),0) AS total_topup " & _
                    "   INTO #TableTSUM  " & _
                    "   FROM lnloan_advance_tran " & _
                    "        LEFT JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
                    "   WHERE    lnloan_advance_tran.isvoid = 0 " & _
                    "        AND lnloan_topup_tran.isvoid = 0 " & _
                    "   GROUP BY lnloan_advance_tran.loanadvancetranunkid "

            StrQ &= "SELECT " & _
                    "	 LN.VocNo " & _
                    "	,LN.effective_date " & _
                    "	,LN.PeriodName " & _
                    "	,LN.DeductionPeriodName " & _
                    "	,LN.Employee " & _
                    "	,LN.EmpName " & _
                    "	,LN.Firstname " & _
                    "	,LN.Othername " & _
                    "	,LN.Surname " & _
                    "	,LN.email " & _
                    "	,LN.LoanScheme " & _
                    "	,LN.Loan_Advance " & _
                    "	,LN.Loan_AdvanceUnkid " & _
                    "	,LN.Amount " & _
                    "	,LN.cSign " & _
                    "	,LN.Deduction_Start_Date " & _
                    "	,LN.Deduction_End_Date " & _
                    "	,LN.loanadvancetranunkid " & _
                    "	,LN.loanvoucher_no " & _
                    "	,LN.periodunkid " & _
                    "	,LN.employeeunkid " & _
                    "	,LN.approverunkid " & _
                    "	,LN.loan_purpose " & _
                    "	,LN.balance_amount " & _
                    "	,LN.isbrought_forward " & _
                    "	,LN.isloan " & _
                    "	,LN.loanschemeunkid " & _
                    "	,LN.loan_amount " & _
                    "	,LN.calctype_id " & _
                    "   ,LN.LoanCalcType " & _
                    "	,ISNULL(LN.interest_calctype_id, 1) AS interest_calctype_id " & _
                    "   ,LN.InterestCalcType " & _
                    "	,LN.advance_amount " & _
                    "   ,CASE WHEN CONVERT(CHAR(8),LN.effective_date,112) <= @enddate THEN LN.loan_statusunkid ELSE -999 END loan_statusunkid " & _
                    "	,LN.userunkid " & _
                    "	,LN.isvoid " & _
                    "	,LN.voiduserunkid " & _
                    "	,LN.voiddatetime " & _
                    "	,LN.voidreason " & _
                    "	,LN.processpendingloanunkid " & _
                    "	,LN.deductionperiodunkid " & _
                    "	,LN.bf_amount " & _
                    "	,LN.bf_balance " & _
                    "	,LN.countryunkid " & _
                    "   ,CASE WHEN ISNULL(#TableEM.loan_duration, 0) <=0 THEN ISNULL(#TableFULNEMI.loan_duration,0) ELSE ISNULL(#TableEM.loan_duration, 0) END AS loan_duration " & _
                    "   ,CASE WHEN ISNULL(#TableEM.emi_tenure, 0) <=0 THEN ISNULL(#TableFULNEMI.emi_tenure,0) ELSE ISNULL(#TableEM.emi_tenure, 0) END AS installments " & _
                    "   ,CASE WHEN ISNULL(#TableEM.emi_amount, 0) <=0 THEN ISNULL(#TableFULNEMI.emi_amount,0) ELSE ISNULL(#TableEM.emi_amount, 0) END AS emi_amount " & _
                    "   ,CASE WHEN ISNULL(#TableLR.interest_rate, 0) <=0 THEN ISNULL(#TableFULNRATE.interest_rate,0) ELSE ISNULL(#TableLR.interest_rate, 0) END AS interest_rate " & _
                    "   ,CASE WHEN ISNULL(#TableLT.topup_amount, 0) <=0 THEN ISNULL(#TableFULNTOPUP.topup_amount,0) ELSE ISNULL(#TableLT.topup_amount, 0) END AS topup_amount " & _
                    "   ,CASE WHEN LN.isloan=1 THEN " & _
                    "           CASE WHEN CONVERT(CHAR(8),LN.effective_date,112) <= @enddate THEN ISNULL(LN.loan_status, '') ELSE @FutureLoan END " & _
                    "         WHEN LN.isloan=0 THEN " & _
                    "           CASE WHEN CONVERT(CHAR(8),LN.effective_date,112) <= @enddate THEN ISNULL(LN.loan_status, '') ELSE @FutureAdvance END " & _
                    "    END AS loan_status " & _
                    "   ,LN.empcode " & _
                    "   ,LN.loancode " & _
                    "   ,ISNULL(exchange_rate,0) AS exchange_rate " & _
                    "   ,ISNULL(basecurrency_amount,0) AS basecurrency_amount " & _
                    "   ,ISNULL(LN.balance_amountPaidCurrency,0) AS balance_amountPaidCurrency " & _
                    "   ,ISNULL(LN.opening_balance,0) AS opening_balance " & _
                    "   ,ISNULL(LN.opening_balancePaidCurrency,0) AS opening_balancePaidCurrency " & _
                    "   ,LN.Assign_Start_Date " & _
                    "   ,LN.Assign_End_Date " & _
                    "   ,CASE WHEN ISNULL(LN.Deduction_Start_Date,0) > ISNULL(@startdate,0) THEN 0 ELSE 1 END AS visiblestatus " & _
                    "   ,LN.otherop_approval " & _
                    "   ,LN.opstatus_id " & _
                    "   ,LN.interest_amount " & _
                    "   ,LN.net_amount " & _
                    "   ,ISNULL(#TableTSUM.total_topup,0) AS total_topup " & _
                    "   ,isexternal_entity  " & _
                    ", ISNULL(mapped_tranheadunkid, 0) AS mapped_tranheadunkid " & _
                    ", CAST(0 AS BIT) AS ischecked " & _
                    "FROM " & _
                    "( " & _
                    "	SELECT " & _
                    "		 lnloan_advance_tran.loanvoucher_no AS VocNo " & _
                    "		,CONVERT(CHAR(8),lnloan_advance_tran.effective_date,112) AS effective_date " & _
                    "		,ISNULL(ASP.period_name,'') AS PeriodName " & _
                    "		,ISNULL(DDP.period_name,'') AS DeductionPeriodName " & _
                    "		,ISNULL(#TableEmp.employeecode,'') + ' - ' +  ISNULL(#TableEmp.firstname, '') + ' ' + ISNULL(#TableEmp.othername, '') + ' ' + ISNULL(#TableEmp.surname, '') AS Employee " & _
                    "		,ISNULL(#TableEmp.firstname, '') + ' ' + ISNULL(#TableEmp.othername, '') + ' ' + ISNULL(#TableEmp.surname, '') AS EmpName " & _
                    "		,ISNULL(#TableEmp.firstname, '') AS firstname " & _
                    "		,ISNULL(#TableEmp.Othername, '') AS Othername " & _
                    "		,ISNULL(#TableEmp.Surname, '') AS Surname " & _
                    "		,ISNULL(#TableEmp.email,'') AS email " & _
                    "		,CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_scheme_master.code + ' - ' +   lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
                    "		,CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
                    "		,CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
                    "		,CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_advance_tran.loan_amount ELSE lnloan_advance_tran.advance_amount END AS Amount " & _
                    "		,ISNULL(#TableCur.cSign,'') AS cSign " & _
                    "		,CONVERT(CHAR(8), DDP.start_date, 112) AS Deduction_Start_Date " & _
                    "		,CONVERT(CHAR(8), DDP.end_date, 112) AS Deduction_End_Date " & _
                    "		,DENSE_RANK() OVER (PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY lnloan_status_tran.status_date DESC, lnloan_status_tran.loanstatustranunkid DESC ) AS LN_RNO " & _
                    "		,lnloan_advance_tran.loanadvancetranunkid " & _
                    "		,lnloan_advance_tran.loanvoucher_no " & _
                    "		,lnloan_advance_tran.periodunkid " & _
                    "		,lnloan_advance_tran.employeeunkid " & _
                    "		,lnloan_advance_tran.approverunkid " & _
                    "		,lnloan_advance_tran.loan_purpose " & _
                    "		,lnloan_advance_tran.balance_amount " & _
                    "		,lnloan_advance_tran.isbrought_forward " & _
                    "		,lnloan_advance_tran.isloan " & _
                    "		,lnloan_advance_tran.loanschemeunkid " & _
                    "		,lnloan_advance_tran.loan_amount " & _
                    "		,lnloan_advance_tran.calctype_id "
            'Sohail (17 Feb 2021) - [firstname, othername, surname, email, ischecked]
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            StrQ &= "       ,CASE lnloan_advance_tran.calctype_id "
            For Each pair In dicLoanCalc
                StrQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            StrQ &= "         END AS LoanCalcType "
            StrQ &= "       ,lnloan_advance_tran.interest_calctype_id "
            StrQ &= "       ,CASE lnloan_advance_tran.interest_calctype_id "
            For Each pair In dicInterestCalc
                StrQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            StrQ &= "         END AS InterestCalcType "
            StrQ &= "		,lnloan_advance_tran.advance_amount " & _
                    "		,lnloan_status_tran.statusunkid AS loan_statusunkid " & _
                    "		,lnloan_advance_tran.userunkid " & _
                    "		,lnloan_advance_tran.isvoid " & _
                    "		,lnloan_advance_tran.voiduserunkid " & _
                    "		,lnloan_advance_tran.voiddatetime " & _
                    "		,lnloan_advance_tran.voidreason " & _
                    "		,lnloan_advance_tran.processpendingloanunkid " & _
                    "		,lnloan_advance_tran.deductionperiodunkid " & _
                    "		,lnloan_advance_tran.bf_amount " & _
                    "		,ISNULL(lnloan_advance_tran.bf_balance,0) AS bf_balance " & _
                    "		,lnloan_advance_tran.countryunkid " & _
                    "       ,#TableEmp.employeecode AS empcode " & _
                    "       ,lnloan_scheme_master.code AS loancode " & _
                    "       ,LSP.periodunkid AS LPeriodId " 
            StrQ &= "       ,CASE lnloan_status_tran.statusunkid "
            For Each pair In dicLoanSavingsStatus
                StrQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            StrQ &= "         END AS loan_status "
            StrQ &= "       , ISNULL(lnloan_advance_tran.exchange_rate,0) AS exchange_rate " & _
                    "       , ISNULL(lnloan_advance_tran.basecurrency_amount,0) AS basecurrency_amount " & _
                    "       , ISNULL(lnloan_advance_tran.balance_amountpaidcurrency,0) AS balance_amountpaidcurrency " & _
                    "       , ISNULL(lnloan_advance_tran.opening_balance,0) AS opening_balance " & _
                    "       , ISNULL(lnloan_advance_tran.opening_balancepaidcurrency,0) AS opening_balancepaidcurrency " & _
                    "       , CONVERT(CHAR(8), ASP.start_date, 112) AS Assign_Start_Date " & _
                    "       , CONVERT(CHAR(8), ASP.end_date, 112) AS Assign_End_Date " & _
                    "       , CASE WHEN #TableOP_STATUS.final_status = 1 AND #TableOP_STATUS.final_approved = 0 THEN @Pending " & _
                    "              WHEN #TableOP_STATUS.final_status = 100 THEN @NoOperation " & _
                    "              ELSE @Approved " & _
                    "         END AS 'otherop_approval' " & _
                    "       , CASE WHEN #TableOP_STATUS.final_status = 1 AND #TableOP_STATUS.final_approved = 0 THEN 1" & _
                    "	           WHEN #TableOP_STATUS.final_status = 100 THEN 100 " & _
                    "	           ELSE 2 " & _
                    "         END AS 'opstatus_id' " & _
                    "       , ISNULL(lnloan_advance_tran.interest_amount,0) AS interest_amount " & _
                    "       , ISNULL(lnloan_advance_tran.net_amount,0) AS net_amount " & _
                    "       , lnloan_process_pending_loan.isexternal_entity " & _
                    ", ISNULL(lnloan_advance_tran.mapped_tranheadunkid, 0) AS mapped_tranheadunkid " & _
                    "	FROM lnloan_advance_tran " & _
                    "       LEFT JOIN #TableOP_STATUS ON #TableOP_STATUS.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                    "		LEFT JOIN lnloan_status_tran ON lnloan_status_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                    "		LEFT JOIN cfcommon_period_tran AS LSP ON lnloan_status_tran.periodunkid = LSP.periodunkid AND LSP.modulerefid = " & enModuleReference.Payroll & " AND CONVERT(CHAR(8),LSP.start_date,112) <= @asondate " & _
                    "		LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                    "		LEFT JOIN cfcommon_period_tran AS ASP ON ASP.periodunkid = lnloan_advance_tran.periodunkid AND ASP.modulerefid = " & enModuleReference.Payroll & " " & _
                    "		LEFT JOIN cfcommon_period_tran AS DDP ON DDP.periodunkid = lnloan_advance_tran.deductionperiodunkid AND DDP.modulerefid = " & enModuleReference.Payroll & " " & _
                    "		JOIN #TableEmp ON #TableEmp.employeeunkid = lnloan_advance_tran.employeeunkid " & _
                    " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
                    "		LEFT JOIN  #TableCur ON #TableCur.cTid = lnloan_advance_tran.countryunkid AND #TableCur.rno = 1 "
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]


            StrQ &= "	WHERE lnloan_advance_tran.isvoid = 0 AND lnloan_status_tran.isvoid = 0 "

            If intEmpUnkID > 0 Then
                StrQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmpUnkID
            End If


            StrQ &= ") AS LN " & _
                    "LEFT JOIN  #TableEM ON LN.loanadvancetranunkid = #TableEM.loanadvancetranunkid AND #TableEM.EM_RNO = 1 " & _
                    "LEFT JOIN  #TableLR ON LN.loanadvancetranunkid = #TableLR.loanadvancetranunkid AND #TableLR.LR_RNO = 1 " & _
                    "LEFT JOIN #TableLT ON LN.loanadvancetranunkid = #TableLT.loanadvancetranunkid AND #TableLT.LT_RNO = 1 " & _
                    " LEFT JOIN  #TableFULNEMI ON LN.loanadvancetranunkid = #TableFULNEMI.loanadvancetranunkid " & _
                    " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 " & _
                    "LEFT JOIN  #TableFULNRATE ON LN.loanadvancetranunkid = #TableFULNRATE.loanadvancetranunkid " & _
                    " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 " & _
                    " LEFT JOIN #TableFULNTOPUP ON LN.loanadvancetranunkid = #TableFULNTOPUP.loanadvancetranunkid " & _
                   " AND CASE WHEN ISNULL(LPeriodId, 0) = 0 THEN -999 ELSE LN.loan_statusunkid END = -999 "

            StrQ &= "LEFT JOIN #TableTSUM ON  #TableTSUM.loanadvancetranunkid = LN.loanadvancetranunkid "


            StrQ &= " WHERE 1 = 1 AND LN.LN_RNO = 1 "

            If xFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & xFilterString
            End If


            StrQ &= " ORDER by LN.loanadvancetranunkid, LN.loanvoucher_no "

            StrQ &= "DROP TABLE #TableEmp " & _
                    "DROP TABLE #TableOP_STATUS " & _
                    "DROP TABLE #TableCur " & _
                    "DROP TABLE #TableEM " & _
                    "DROP TABLE #TableLR " & _
                    "DROP TABLE #TableLT " & _
                    "DROP TABLE #TableFULNEMI " & _
                    "DROP TABLE #TableFULNRATE " & _
                    "DROP TABLE #TableFULNTOPUP " & _
                    "DROP TABLE #TableTSUM "

            objDataOperation.ClearParameters()


            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEndDate))
            objDataOperation.AddParameter("@asondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEndDate))

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))
            objDataOperation.AddParameter("@FutureLoan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Future Loan"))

            objDataOperation.AddParameter("@FutureAdvance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Future Advance"))

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 2, "Approved"))
            objDataOperation.AddParameter("@NoOperation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsloanotherop_approval_tran", 7, "No Other Operation"))

            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'Hemant (26 Mar 2019) -- End
    ' <summary>
    ' Modify By: Sandeep J. Sharma
    ' </summary>
    ' <returns>Boolean</returns>
    ' <purpose> INSERT INTO Database Table (lnloan_advance_tran) </purpose>

    'Nilay (25-Mar-2016) -- Start
    'Public Function Insert(ByVal iUserId As Integer, _
    '                      ByVal intCompanyId As Integer, ByVal intYearId As Integer, ByVal xDatabaseName As String) As Boolean

    Public Function Insert(ByVal xDatabaseName As String, _
                           ByVal iUserId As Integer, _
                           ByVal intYearId As Integer, _
                           ByVal intCompanyId As Integer, _
                           ByVal xPeriodStart As Date, _
                           ByVal xPeriodEnd As Date, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean) As Boolean
        'Nilay (25-Mar-2016) -- End

        'Sohail (15 Dec 2015) -- [xDatabaseName]
        'Public Function Insert(Optional ByVal iUserId As Integer = 0, _
        '                       Optional ByVal intCompanyId As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

             'Hemant (30 Aug 2019) -- Start
             'ISSUE#0004110(ZURI) :  Error on global assigning loans..
             'Dim objDataOperation As New clsDataOperation
             'objDataOperation.BindTransaction()
             'Hemant (30 Aug 2019) -- End

        Dim intVocNoType As Integer = 0
        Dim objConfig As New clsConfigOptions
        objConfig._Companyunkid = IIf(intCompanyId = 0, Company._Object._Companyunkid, intCompanyId)
        'Nilay (05-May-2016) -- Start
        If intYearId > 0 Then mintYearunkid = intYearId
        'Nilay (05-May-2016) -- End

        intVocNoType = objConfig._LoanVocNoType

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        If intVocNoType = 0 Then
            If isExist(mstrLoanvoucher_No, , objDataOperation) Then
                'Hemant (30 Aug 2019) -- [objDataOperation]
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
        ElseIf intVocNoType = 1 Then
            mstrLoanvoucher_No = ""
        End If

        Try
            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanvoucher_No.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
            'Nilay (15-Dec-2015) -- Start
            'objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId.ToString)
            'Nilay (05-May-2016) -- Start

            'Nilay (05-May-2016) -- End

            'Nilay (15-Dec-2015) -- End
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Purpose.ToString)
            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isbrought_forward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbrought_Forward.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@loanscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanscheduleunkid.ToString)
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdvance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanStatus.ToString)
            objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoanBF_Amount)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            'S.SANDEEP [06 MAY 2015] -- START
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryUnkid.ToString)
            objDataOperation.AddParameter("@bf_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoanBf_Balance)
            'S.SANDEEP [06 MAY 2015] -- END
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            objDataOperation.AddParameter("@balance_amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_AmountPaidCurrency.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_rate)
            objDataOperation.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBasecurrency_amount)
            objDataOperation.AddParameter("@opening_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOpening_balance)
            objDataOperation.AddParameter("@opening_balancePaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOpening_balancePaidCurrency)
            'Sohail (07 May 2015) -- End
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterest_Calctype_Id.ToString)
            'Sohail (15 Dec 2015) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
            'Sohail (29 Apr 2019) -- End

            strQ = "INSERT INTO lnloan_advance_tran ( " & _
              "  loanvoucher_no " & _
              ", effective_date " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", approverunkid " & _
              ", loan_purpose " & _
              ", balance_amount " & _
              ", isbrought_forward " & _
              ", isloan " & _
              ", loanschemeunkid " & _
              ", loan_amount " & _
              ", interest_rate " & _
              ", loan_duration " & _
              ", interest_amount " & _
              ", net_amount " & _
              ", calctype_id " & _
              ", loanscheduleunkid " & _
              ", emi_tenure " & _
              ", emi_amount " & _
              ", advance_amount " & _
              ", loan_statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime" & _
              ", voidreason " & _
              ", processpendingloanunkid " & _
              ", deductionperiodunkid" & _
              ",bf_amount  " & _
              ", countryunkid " & _
              ", bf_balance " & _
              ", exchange_rate " & _
              ", basecurrency_amount " & _
              ", balance_amountPaidCurrency " & _
              ", opening_balance " & _
              ", opening_balancePaidCurrency " & _
              ", interest_calctype_id " & _
              ", mapped_tranheadunkid " & _
            ") VALUES (" & _
              "  @loanvoucher_no " & _
              ", @effective_date " & _
              ", @yearunkid " & _
              ", @periodunkid " & _
              ", @employeeunkid " & _
              ", @approverunkid " & _
              ", @loan_purpose " & _
              ", @balance_amount " & _
              ", @isbrought_forward " & _
              ", @isloan " & _
              ", @loanschemeunkid " & _
              ", @loan_amount " & _
              ", @interest_rate " & _
              ", @loan_duration " & _
              ", @interest_amount " & _
              ", @net_amount " & _
              ", @calctype_id " & _
              ", @loanscheduleunkid " & _
              ", @emi_tenure " & _
              ", @emi_amount " & _
              ", @advance_amount " & _
              ", @loan_statusunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime" & _
              ", @voidreason " & _
              ", @processpendingloanunkid " & _
              ", @deductionperiodunkid" & _
              ", @bf_amount  " & _
              ", @countryunkid " & _
              ", @bf_balance " & _
              ", @exchange_rate " & _
              ", @basecurrency_amount " & _
              ", @balance_amountPaidCurrency " & _
              ", @opening_balance " & _
              ", @opening_balancePaidCurrency " & _
              ", @interest_calctype_id " & _
              ", @mapped_tranheadunkid " & _
            "); SELECT @@identity"
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (15 Dec 2015) - [interest_calctype_id]
            'Sohail (07 May 2015) - [balance_amountPaidCurrency, exchange_rate, basecurrency_amount, opening_balance, opening_balancePaidCurrency]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoanadvancetranunkid = dsList.Tables(0).Rows(0).Item(0)

            If intVocNoType = 1 Then
                If Set_AutoNumber(objDataOperation, mintLoanadvancetranunkid, "lnloan_advance_tran", "loanvoucher_no", "loanadvancetranunkid", "NextLoanVocNo", objConfig._LoanVocPrefix, objConfig._Companyunkid) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If Get_Saved_Number(objDataOperation, mintLoanadvancetranunkid, "lnloan_advance_tran", "loanvoucher_no", "loanadvancetranunkid", mstrLoanvoucher_No) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            objStatusTran._Loanadvancetranunkid = mintLoanadvancetranunkid
            objStatusTran._Statusunkid = mintLoanStatus
            objStatusTran._Staus_Date = mdtEffective_Date
            'S.SANDEEP [06 MAY 2015] -- START
            objStatusTran._Periodunkid = mintPeriodunkid
            'S.SANDEEP [06 MAY 2015] -- END

            'SHANI (07 JUL 2015) -- Start
            objStatusTran._Userunkid = mintUserunkid
            'SHANI (07 JUL 2015) -- End 
            'Nilay (05-May-2016) -- Start
            objStatusTran._WebClientIP = mstrWebClientIP
            objStatusTran._WebFormName = mstrWebFormName
            objStatusTran._WebHostName = mstrWebHostName
            'Nilay (05-May-2016) -- End

            If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 1, iUserId) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'S.SANDEEP [06 MAY 2015] -- START
            If mdblRateOfInterest > 0 Then
                objInterestTran._DataOperation = objDataOperation
                objInterestTran._Effectivedate = mdtEffective_Date
                objInterestTran._Interest_Rate = mdblRateOfInterest
                objInterestTran._Isvoid = False
                objInterestTran._Loanadvancetranunkid = mintLoanadvancetranunkid
                'Nilay (01-Apr-2016) -- Start
                'objInterestTran._Periodunkid = mintDeductionperiodunkid
                'Hemant (12 Nov 2021) -- Start
                'ISSUE(REA) : When calculating EMI, system is set period of when applied for application  not the deduction period set.
                'objInterestTran._Periodunkid = mintPeriodunkid
                objInterestTran._Periodunkid = mintDeductionperiodunkid
                'Hemant (12 Nov 2021) -- End
                'Nilay (01-Apr-2016) -- End
                objInterestTran._Userunkid = mintUserunkid
                objInterestTran._Voiddatetime = Nothing
                objInterestTran._Voidreason = ""
                objInterestTran._Voiduserunkid = -1
                'Nilay (20-Nov-2015) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                objInterestTran._IsDefault = 1
                objInterestTran._Approverunkid = mintApproverunkid
                'Nilay (20-Nov-2015) -- End

                'Nilay (05-May-2016) -- Start
                objInterestTran._WebIP = mstrWebClientIP
                objInterestTran._WebFormName = mstrWebFormName
                objInterestTran._WebHost = mstrWebHostName
                'Nilay (05-May-2016) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'Nilay (25-Mar-2016) -- Start
                'If objInterestTran.Insert(False, intYearId) = False Then
                If objInterestTran.Insert(xDatabaseName, iUserId, intYearId, intCompanyId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, False) = False Then
                    'Nilay (25-Mar-2016) -- End

                    'If objInterestTran.Insert(False) = False Then
                    'S.SANDEEP [04 JUN 2015] -- END

                    If objInterestTran._Message <> "" Then
                        mstrMessage = objInterestTran._Message
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            objEMITenure._DataOperation = objDataOperation
            objEMITenure._Effectivedate = mdtEffective_Date
            objEMITenure._Emi_Amount = mdecInstallmentAmount
            objEMITenure._Emi_Tenure = mintNoOfInstallment
            objEMITenure._End_Date = DateAdd(DateInterval.Month, mintNoOfInstallment, mdtEffective_Date).AddDays(-1)
            objEMITenure._Isvoid = False
            objEMITenure._Loan_Duration = mintNoOfInstallment
            objEMITenure._Loanadvancetranunkid = mintLoanadvancetranunkid

            'Shani(26-Nov-2015) -- Start
            'ENHANCEMENT : Add Loan Import Form
            'objEMITenure._Periodunkid = mintPeriodunkid
            'Nilay (01-Apr-2016) -- Start
            'objEMITenure._Periodunkid = mintDeductionperiodunkid
            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : When calculating EMI, system is set period of when applied for application  not the deduction period set.
            'objEMITenure._Periodunkid = mintPeriodunkid
            objEMITenure._Periodunkid = mintDeductionperiodunkid
            'Hemant (12 Nov 2021) -- End
            'Nilay (01-Apr-2016) -- End
            'Shani(26-Nov-2015) -- End

            objEMITenure._Userunkid = mintUserunkid
            objEMITenure._Voiddatetime = Nothing
            objEMITenure._Voidreason = ""
            objEMITenure._Voiduserunkid = -1
            objEMITenure._Exchange_rate = mdecExchange_rate
            objEMITenure._Basecurrency_amount = (mdecInstallmentAmount / mdecExchange_rate)
            'Nilay (20-Nov-2015) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            objEMITenure._IsDefault = 1
            objEMITenure._Approverunkid = mintApproverunkid
            'Nilay (20-Nov-2015) -- End
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            objEMITenure._Principal_amount = mdecEMI_Principal_amount
            objEMITenure._Interest_amount = mdecEMI_Interest_amount
            'Sohail (15 Dec 2015) -- End

            'Nilay (05-May-2016) -- Start
            objEMITenure._WebIP = mstrWebClientIP
            objEMITenure._WebFormName = mstrWebFormName
            objEMITenure._WebHost = mstrWebHostName
            'Nilay (05-May-2016) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Nilay (25-Mar-2016) -- Start
            'If objEMITenure.Insert(False, intYearId) = False Then
            If objEMITenure.Insert(xDatabaseName, iUserId, intYearId, intCompanyId, _
                                   xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, False) = False Then
                'Nilay (25-Mar-2016) -- End

                'If objEMITenure.Insert(False) = False Then
                'S.SANDEEP [04 JUN 2015] -- END

                If objEMITenure._Message <> "" Then
                    mstrMessage = objEMITenure._Message
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                Else
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'If xRateTable IsNot Nothing Then
            '    objInterestTran._Loanadvancetranunkid = mintLoanadvancetranunkid
            '    objInterestTran._DataTable = xRateTable.Copy
            '    If objInterestTran.InsertUpdateDelete_InerestRate_Tran(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If

            'If xEMITable IsNot Nothing Then
            '    objEMITenure._Loanadvancetranunkid = mintLoanadvancetranunkid
            '    objEMITenure._DataTable = xEMITable.Copy
            '    If objEMITenure.InsertUpdateDelete_EMI_Tran(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If

            'If xTopUpTable IsNot Nothing Then
            '    objTopupTran._Loanadvancetranunkid = mintLoanadvancetranunkid
            '    objTopupTran._DataTable = xTopUpTable.Copy
            '    If objTopupTran.InsertUpdateDelete_Topup_Tran(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If
            'S.SANDEEP [06 MAY 2015] -- END

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'If objStatusTran.Insert(objDataOperation) Then

            'Nilay (25-Mar-2016) -- Start
            'If objStatusTran.Insert(xDatabaseName, intYearId, objDataOperation) Then
            If objStatusTran.Insert(xDatabaseName, iUserId, intYearId, intCompanyId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                    objDataOperation) Then
                'Nilay (25-Mar-2016) -- End

                'Sohail (15 Dec 2015) -- End

                Dim blnFlag As Boolean = False
                Dim objProcessPendingLoan As New clsProcess_pending_loan
                'Hemant (30 Aug 2019) -- Start
                'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                objProcessPendingLoan._xDataOp = objDataOperation
                'Hemant (30 Aug 2019) -- End	
                objProcessPendingLoan._Processpendingloanunkid = mintProcesspendingloanunkid
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'objProcessPendingLoan._Loan_Statusunkid = 4
                objProcessPendingLoan._Loan_Statusunkid = enLoanApplicationStatus.ASSIGNED
                'Nilay (20-Sept-2016) -- End

                'Nilay (05-May-2016) -- Start
                objProcessPendingLoan._WebClientIP = mstrWebClientIP
                objProcessPendingLoan._WebFormName = mstrWebFormName
                objProcessPendingLoan._WebHostName = mstrWebHostName
                objProcessPendingLoan._Loginemployeeunkid = mintLogEmployeeUnkid
                'Nilay (05-May-2016) -- End

                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                'blnFlag = objProcessPendingLoan.Update(objDataOperation)
                blnFlag = objProcessPendingLoan.Update(False, objDataOperation)
                'Nilay (13-Sept-2016) -- End
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                objDataOperation.ReleaseTransaction(True)
            Else
                objDataOperation.ReleaseTransaction(False)
            End If

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '' <summary>
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> Update Database Table (lnloan_advance_tran) </purpose>

    'Nilay (05-May-2016) -- Start
    'Public Function Update(Optional ByVal iUserId As Integer = 0, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
    Public Function Update(Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        'Nilay (05-May-2016) -- End

        If isExist(mstrLoanvoucher_No, mintLoanadvancetranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        'S.SANDEEP [ 29 May 2013 ] -- START
        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
        'Dim objDataOperation As New clsDataOperation
        'objDataOperation.BindTransaction()

        'S.SANDEEP [ 04 DEC 2013 ] -- START
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END



        'S.SANDEEP [ 29 May 2013 ] -- END

        Try
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanvoucher_No.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Purpose.ToString)
            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isbrought_forward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbrought_Forward.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@loanscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanscheduleunkid.ToString)
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdvance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanStatus.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)

            'S.SANDEEP [06 MAY 2015] -- START
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryUnkid.ToString)
            objDataOperation.AddParameter("@bf_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoanBf_Balance)
            'S.SANDEEP [06 MAY 2015] -- END
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            objDataOperation.AddParameter("@balance_amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_AmountPaidCurrency.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_rate)
            objDataOperation.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBasecurrency_amount)
            objDataOperation.AddParameter("@opening_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOpening_balance)
            objDataOperation.AddParameter("@opening_balancePaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOpening_balancePaidCurrency)
            'Sohail (07 May 2015) -- End
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterest_Calctype_Id.ToString)
            'Sohail (15 Dec 2015) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
            'Sohail (29 Apr 2019) -- End

            strQ = "UPDATE lnloan_advance_tran SET " & _
              "  loanvoucher_no = @loanvoucher_no" & _
              ", effective_date = @effective_date" & _
              ", yearunkid = @yearunkid" & _
              ", periodunkid = @periodunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", approverunkid = @approverunkid" & _
              ", loan_purpose = @loan_purpose" & _
              ", balance_amount = @balance_amount" & _
              ", isbrought_forward = @isbrought_forward" & _
              ", isloan = @isloan" & _
              ", loanschemeunkid = @loanschemeunkid" & _
              ", loan_amount = @loan_amount" & _
              ", interest_rate = @interest_rate" & _
              ", loan_duration = @loan_duration" & _
              ", interest_amount = @interest_amount" & _
              ", net_amount = @net_amount" & _
              ", calctype_id = @calctype_id" & _
              ", loanscheduleunkid = @loanscheduleunkid" & _
              ", emi_tenure = @emi_tenure" & _
              ", emi_amount = @emi_amount" & _
              ", advance_amount = @advance_amount" & _
              ", loan_statusunkid = @loan_statusunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
              ", processpendingloanunkid = @processpendingloanunkid " & _
              ", deductionperiodunkid = @deductionperiodunkid " & _
              ", countryunkid = @countryunkid " & _
              ", bf_balance = @bf_balance " & _
              ", exchange_rate = @exchange_rate " & _
              ", basecurrency_amount = @basecurrency_amount " & _
              ", balance_amountPaidCurrency = @balance_amountPaidCurrency " & _
              ", opening_balance = @opening_balance " & _
              ", opening_balancePaidCurrency = @opening_balancePaidCurrency " & _
              ", interest_calctype_id = @interest_calctype_id " & _
              ", mapped_tranheadunkid = @mapped_tranheadunkid " & _
            "WHERE loanadvancetranunkid = @loanadvancetranunkid "
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (15 Dec 2015) - [interest_calctype_id]
            'Sohail (07 May 2015) - [balance_amountPaidCurrency, exchange_rate, basecurrency_amount, opening_balance, opening_balancePaidCurrency]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (05-May-2016) -- Start
            'If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 2) = False Then
            If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 2, mintUserunkid) = False Then
                'Nilay (05-May-2016) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'S.SANDEEP [06 MAY 2015] -- START
            'If xRateTable IsNot Nothing Then
            '    objInterestTran._Loanadvancetranunkid = mintLoanadvancetranunkid
            '    objInterestTran._DataTable = xRateTable.Copy
            '    If objInterestTran.InsertUpdateDelete_InerestRate_Tran(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If

            'If xEMITable IsNot Nothing Then
            '    objEMITenure._Loanadvancetranunkid = mintLoanadvancetranunkid
            '    objEMITenure._DataTable = xEMITable.Copy
            '    If objEMITenure.InsertUpdateDelete_EMI_Tran(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If

            'If xTopUpTable IsNot Nothing Then
            '    objTopupTran._Loanadvancetranunkid = mintLoanadvancetranunkid
            '    objTopupTran._DataTable = xTopUpTable.Copy
            '    If objTopupTran.InsertUpdateDelete_Topup_Tran(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If
            'S.SANDEEP [06 MAY 2015] -- END

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            'S.SANDEEP [06 MAY 2015] -- START
            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP [06 MAY 2015] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ' <summary>
    ' Modify By: Sandeep J. Sharma
    ' </summary>
    ' <returns>Boolean</returns>
    ' <purpose> Delete Database Table (lnloan_advance_tran) </purpose>

    'Nilay (25-Mar-2016) -- Start
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="xDatabaseName"></param>
    ''' <param name="xUserUnkid"></param>
    ''' <param name="xYearUnkid"></param>
    ''' <param name="xCompanyUnkid"></param>
    ''' <param name="xPeriodStart"></param>
    ''' <param name="xPeriodEnd"></param>
    ''' <param name="xUserModeSetting"></param>
    ''' <param name="xOnlyApproved"></param>
    ''' <param name="intUnkid"></param>
    ''' <param name="blnIsFromLoanAdvanceList">If you delete from LoanAdvance List pass this para True else false</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Delete(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal xPeriodStart As Date, _
                           ByVal xPeriodEnd As Date, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal intUnkid As Integer, _
                           ByVal blnIsFromLoanAdvanceList As Boolean) As Boolean
        'Nilay (20-Sept-2016) -- [blnIsFromLoanAdvanceList]
        'Nilay (25-Mar-2016) -- End

        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Loan/Advance. Reason : This Loan/Advance is already linked to some transactions.")
            Return False
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objEMI As New clslnloan_emitenure_tran
        Dim objIRate As New clslnloan_interest_tran
        Dim objTopUp As New clslnloan_topup_tran

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE lnloan_advance_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime " & _
                     ", voidreason = @voidreason " & _
                   "WHERE loanadvancetranunkid = @loanadvancetranunkid "

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim xTable As DataTable = Nothing
            '################## LOAN EMI TENURE ######################| START'
            dsList = clsCommonATLog.GetChildList(objDataOperation, "lnloan_emitenure_tran", "loanadvancetranunkid", intUnkid)
            For Each xRow As DataRow In dsList.Tables(0).Rows
                objEMI._DataOperation = objDataOperation
                objEMI._Isvoid = True
                objEMI._Voiddatetime = mdtVoiddatetime
                objEMI._Voidreason = mstrVoidreason
                objEMI._Voiduserunkid = mintVoiduserunkid
                'Nilay (25-Mar-2016) -- Start
                'If objEMI.Delete(xRow.Item("lnemitranunkid"), False) = False Then

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If objEMI.Delete(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                '                 xRow.Item("lnemitranunkid"), False) = False Then
                If objEMI.Delete(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                             xRow.Item("lnemitranunkid"), False, blnIsFromLoanAdvanceList) = False Then
                    'Nilay (20-Sept-2016) -- End

                    'Nilay (25-Mar-2016) -- End

                    If objEMI._Message <> "" Then
                        mstrMessage = objEMI._Message
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Next
            'objEMI._Loanadvancetranunkid = intUnkid
            'xTable = objEMI._DataTable
            'For Each xRow As DataRow In xTable.Rows
            '    xRow.Item("AUD") = "D"
            '    xRow.Item("isvoid") = mblnIsvoid.ToString
            '    xRow.Item("voiduserunkid") = mintVoiduserunkid.ToString
            '    xRow.Item("voiddatetime") = mdtVoiddatetime
            '    xRow.Item("voidreason") = mstrVoidreason.ToString
            'Next

            'If objEMI.InsertUpdateDelete_EMI_Tran(objDataOperation) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            '################## LOAN EMI TENURE ######################| END '

            '################## LOAN INTEREST RATE ######################| START'

            dsList = clsCommonATLog.GetChildList(objDataOperation, "lnloan_interest_tran", "loanadvancetranunkid", intUnkid)
            For Each xRow As DataRow In dsList.Tables(0).Rows
                objIRate._DataOperation = objDataOperation
                objIRate._Isvoid = True
                objIRate._Voiddatetime = mdtVoiddatetime
                objIRate._Voidreason = mstrVoidreason
                objIRate._Voiduserunkid = mintVoiduserunkid
                'Nilay (05-May-2016) -- Start
                objIRate._WebIP = mstrWebClientIP
                objIRate._WebFormName = mstrWebFormName
                objIRate._WebHost = mstrWebHostName
                'Nilay (05-May-2016) -- End

                'Nilay (25-Mar-2016) -- Start
                'If objIRate.Delete(xRow.Item("lninteresttranunkid"), False) = False Then

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If objIRate.Delete(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                '                   xRow.Item("lninteresttranunkid"), False) = False Then
                If objIRate.Delete(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                               xRow.Item("lninteresttranunkid"), False, blnIsFromLoanAdvanceList) = False Then
                    'Nilay (20-Sept-2016) -- End


                    'Nilay (25-Mar-2016) -- End

                    If objIRate._Message <> "" Then
                        mstrMessage = objIRate._Message
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Next

            'xTable = Nothing
            'objIRate._Loanadvancetranunkid = intUnkid
            'xTable = objIRate._DataTable
            'For Each xRow As DataRow In xTable.Rows
            '    xRow.Item("AUD") = "D"
            '    xRow.Item("isvoid") = mblnIsvoid.ToString
            '    xRow.Item("voiduserunkid") = mintVoiduserunkid.ToString
            '    xRow.Item("voiddatetime") = mdtVoiddatetime
            '    xRow.Item("voidreason") = mstrVoidreason.ToString
            'Next

            'If objIRate.InsertUpdateDelete_InerestRate_Tran(objDataOperation) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            '################## LOAN INTEREST RATE ######################| END '

            '################## LOAN TOP UP ######################| START'

            dsList = clsCommonATLog.GetChildList(objDataOperation, "lnloan_topup_tran", "loanadvancetranunkid", intUnkid)
            For Each xRow As DataRow In dsList.Tables(0).Rows
                objTopUp._DataOperation = objDataOperation
                objTopUp._Isvoid = True
                objTopUp._Voiddatetime = mdtVoiddatetime
                objTopUp._Voidreason = mstrVoidreason
                objTopUp._Voiduserunkid = mintVoiduserunkid
                'Nilay (05-May-2016) -- Start
                objTopUp._WebIP = mstrWebClientIP
                objTopUp._WebFormName = mstrWebFormName
                objTopUp._WebHost = mstrWebHostName
                'Nilay (05-May-2016) -- End

                'Nilay (25-Mar-2016) -- Start
                'If objTopUp.Delete(xRow.Item("lntopuptranunkid"), False) = False Then

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If objTopUp.Delete(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                '                   xRow.Item("lntopuptranunkid"), False) = False Then
                If objTopUp.Delete(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                               xRow.Item("lntopuptranunkid"), False, blnIsFromLoanAdvanceList) = False Then
                    'Nilay (20-Sept-2016) -- End


                    'Nilay (25-Mar-2016) -- End

                    If objTopUp._Message <> "" Then
                        mstrMessage = objTopUp._Message
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Next

            'xTable = Nothing
            'objTopUp._Loanadvancetranunkid = intUnkid
            'xTable = objTopUp._DataTable
            'For Each xRow As DataRow In xTable.Rows
            '    xRow.Item("AUD") = "D"
            '    xRow.Item("isvoid") = mblnIsvoid.ToString
            '    xRow.Item("voiduserunkid") = mintVoiduserunkid.ToString
            '    xRow.Item("voiddatetime") = mdtVoiddatetime
            '    xRow.Item("voidreason") = mstrVoidreason.ToString
            'Next

            'If objTopUp.InsertUpdateDelete_Topup_Tran(objDataOperation) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            '################## LOAN TOP UP ######################| END '

            Dim blnFlag As Boolean = False
            Dim objStatusTran As New clsLoan_Status_tran
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            objStatusTran._Loanadvancetranunkid = intUnkid
            'Nilay (01-Apr-2016) -- End
            objStatusTran._Isvoid = mblnIsvoid.ToString
            objStatusTran._Voiddatetime = mdtVoiddatetime
            objStatusTran._Voiduserunkid = mintVoiduserunkid
            objStatusTran._Voidreason = mstrVoidreason
            'Nilay (05-May-2016) -- Start
            objStatusTran._WebClientIP = mstrWebClientIP
            objStatusTran._WebFormName = mstrWebFormName
            objStatusTran._WebHostName = mstrWebHostName
            'Nilay (05-May-2016) -- End

            blnFlag = objStatusTran.Delete(intUnkid, objDataOperation)
            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            mintLoanadvancetranunkid = intUnkid

            'Nilay (05-May-2016) -- Start
            'If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 3) = False Then
            If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 3, xUserUnkid) = False Then
                'Nilay (05-May-2016) -- End

                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            Dim blnLoanAdvanceFlag As Boolean = False
            Dim objProcessPendingLoan As New clsProcess_pending_loan
            objProcessPendingLoan._Processpendingloanunkid = mintProcesspendingloanunkid
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'objProcessPendingLoan._Loan_Statusunkid = 2
            objProcessPendingLoan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED
            'Nilay (20-Sept-2016) -- End

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'blnLoanAdvanceFlag = objProcessPendingLoan.Update(objDataOperation)
            blnLoanAdvanceFlag = objProcessPendingLoan.Update(False, objDataOperation)
            'Nilay (13-Sept-2016) -- End

            If blnLoanAdvanceFlag = False Then objDataOperation.ReleaseTransaction(False)

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            'Sandeep [ 09 Oct 2010 ] -- Start
            'strQ = "<Query>"

            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'strQ = "SELECT loanadvancetranunkid FROM prpayrollprocess_tran WHERE loanadvancetranunkid = @loanadvancetranunkid "
            strQ = "SELECT loanadvancetranunkid FROM prpayrollprocess_tran WHERE isvoid=0 AND loanadvancetranunkid = @loanadvancetranunkid "
            'Nilay (21-Jul-2016) -- End

            'Sandeep [ 09 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrMessage = ""

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                'Nilay (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                'strQ = "SELECT referencetranunkid FROM prpayment_tran WHERE referenceid IN (1,2) AND referencetranunkid = @loanadvancetranunkid "
                strQ = "SELECT referencetranunkid FROM prpayment_tran WHERE isvoid=0 AND referenceid IN (1,2) AND referencetranunkid = @loanadvancetranunkid "
                'Nilay (21-Jul-2016) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'objDataOperation = New clsDataOperation
        Dim objDataOperation As New clsDataOperation
        'Hemant (30 Aug 2019) -- Start
        'ISSUE#0004110(ZURI) :  Error on global assigning loans..
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (30 Aug 2019) -- End

        Try
            strQ = "SELECT " & _
                   "  loanadvancetranunkid " & _
                   ", loanvoucher_no " & _
                   ", effective_date " & _
                   ", yearunkid " & _
                   ", periodunkid " & _
                   ", employeeunkid " & _
                   ", approverunkid " & _
                   ", loan_purpose " & _
                   ", balance_amount " & _
                   ", isbrought_forward " & _
                   ", isloan " & _
                   ", loanschemeunkid " & _
                   ", loan_amount " & _
                   ", interest_rate " & _
                   ", interest_amount " & _
                   ", net_amount " & _
                   ", calctype_id " & _
                   ", loanscheduleunkid " & _
                   ", emi_tenure " & _
                   ", emi_amount " & _
                   ", advance_amount " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", processpendingloanunkid " & _
             "FROM lnloan_advance_tran " & _
             "WHERE loanvoucher_no = @loanvoucher_no "

            If intUnkid > 0 Then
                strQ &= " AND loanadvancetranunkid <> @loanadvancetranunkid"
            End If

            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            'Hemant (30 Aug 2019) -- Start
            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'Hemant (30 Aug 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
            'Hemant (30 Aug 2019) -- Start
            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
            'Hemant (30 Aug 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Gets The History </purpose>
    Public Function GetLoanAdvance_History_List(ByVal intUnkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "    ISNULL(lnScheme,'') AS lnScheme " & _
                    "   ,ISNULL(lnVocNo,'') AS lnVocNo " & _
                    "   ,lnDate AS lnDate " & _
                    "   ,ISNULL(lnAmount,0) AS lnAmount " & _
                    "   ,ISNULL(lnNetAmount,0) AS lnNetAmount " & _
                    "   ,ISNULL(lnBalance,0) AS lnBalance " & _
                    "   ,(ISNULL(lnNetAmount,0)-ISNULL(lnBalance,0)) AS lnPaid " & _
                    "   ,ISNULL(lnStatus,'') AS lnStatus " & _
                    "   ,lnUnkid AS lnUnkid " & _
                    "   ,'' AS lnvDate " & _
                    "FROM " & _
                    "    ( SELECT " & _
                    "        CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                    "             THEN ISNULL(lnloan_scheme_master.name, '') " & _
                    "             ELSE @Advance " & _
                    "        END AS lnScheme " & _
                    "      ,ISNULL(lnloan_advance_tran.loanvoucher_no, '') AS lnVocNo " & _
                    "       ,CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS lnDate " & _
                    "       ,CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                    "             THEN ISNULL(lnloan_advance_tran.loan_amount, 0) " & _
                    "             ELSE lnloan_advance_tran.advance_amount " & _
                    "        END AS lnAmount " & _
                    "       ,CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                    "             THEN ISNULL(lnloan_advance_tran.net_amount, 0) " & _
                    "             ELSE lnloan_advance_tran.advance_amount " & _
                    "        END AS lnNetAmount " & _
                    "       ,CASE WHEN lnloan_advance_tran.loan_statusunkid = " & enLoanStatus.IN_PROGRESS & " THEN @InProgress " & _
                    "             WHEN lnloan_advance_tran.loan_statusunkid = " & enLoanStatus.ON_HOLD & " THEN @OnHold " & _
                    "             WHEN lnloan_advance_tran.loan_statusunkid = " & enLoanStatus.WRITTEN_OFF & " THEN @WrittenOff " & _
                    "             WHEN lnloan_advance_tran.loan_statusunkid = " & enLoanStatus.COMPLETED & " THEN @Completed " & _
                    "        END AS lnStatus " & _
                    "       ,lnloan_advance_tran.loanadvancetranunkid AS lnUnkid " & _
                    "       ,lnloan_advance_tran.balance_amount AS lnBalance " & _
                    "      FROM " & _
                    "        lnloan_advance_tran " & _
                    "        LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                    "      WHERE " & _
                    "        ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                    "    ) AS lnHistory " & _
                    "WHERE " & _
                    "    lnUnkid = @Unkid "
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'ADDED Enum in [lnStatus]
            'Nilay (20-Sept-2016) -- End

            'S.SANDEEP [06 MAY 2015] {lnvDate} -- ADDED

            objDataOperation.AddParameter("@Unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [06 MAY 2015] -- START
            For Each xRows As DataRow In dsList.Tables(0).Rows
                If xRows.Item("lnDate").ToString.Trim.Length > 0 Then
                    xRows.Item("lnvDate") = eZeeDate.convertDate(xRows.Item("lnDate").ToString).ToShortDateString
                End If
            Next
            'S.SANDEEP [06 MAY 2015] -- END

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoanAdvance_History_List", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function GetLoanAdvance_History_List(ByVal intEmployeeId As Integer, ByVal dtDateAsOn As Date) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ, StrSubQ, StrInnerQuery, StrStatusQ, StrLastDB As String

        StrQ = "" : StrSubQ = "" : StrInnerQuery = "" : StrStatusQ = "" : StrLastDB = ""

        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try
            Dim dsDBNames As DataSet = New DataSet

            StrQ = "SELECT " & _
                   "    DISTINCT database_name AS DBase " & _
                   "    ,cffinancial_year_tran.yearunkid " & _
                   "FROM lnloan_advance_tran " & _
                   "    JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
                   "    JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
                   "WHERE lnloan_advance_tran.employeeunkid = '" & intEmployeeId & "' AND isvoid = 0 ORDER BY cffinancial_year_tran.yearunkid ASC "

            dsDBNames = objDataOperation.ExecQuery(StrQ, "ListBox")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsDBNames.Tables(0).Rows.Count > 0 Then

                StrLastDB = dsDBNames.Tables(0).Rows(dsDBNames.Tables(0).Rows.Count - 1).Item("DBase")

                StrQ = "SELECT " & _
                        "	 lnScheme " & _
                        "	,lnVocNo " & _
                        "	,lnAmount " & _
                        "	,lnNetAmount " & _
                        "	,lnDate " & _
                        "	,lnNetAmount - lnPaid AS lnBalance " & _
                        "	,lnPaid " & _
                        "	,lnStatus " & _
                        "	,'' AS lnvDate " & _
                        "FROM " & _
                        "( "

                StrInnerQuery = "SELECT DISTINCT " & _
                                "	 lnScheme " & _
                                "	,lnVocNo " & _
                                "	,lnAmount " & _
                                "	,lnNetAmount " & _
                                "	,lnDate " & _
                                "	,SUM(lnPaid)OVER(PARTITION BY C.lnVocNo) AS lnPaid " & _
                                "   ,lnadvunkid " & _
                                "FROM " & _
                                "( "

                For Each drow As DataRow In dsDBNames.Tables(0).Rows
                    StrSubQ &= "UNION " & _
                               "	SELECT DISTINCT " & _
                               "		 CASE WHEN isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS lnScheme " & _
                               "		,CASE WHEN isloan = 1 THEN loan_amount ELSE advance_amount END AS lnAmount " & _
                               "		,lnloan_advance_tran.interest_amount AS LoanInterestAmt " & _
                               "		,ISNULL(loan_amount,0)+ISNULL(lnloan_advance_tran.interest_amount,0) AS lnNetAmount " & _
                               "		,SUM(amount)OVER(PARTITION BY lnloan_advance_tran.loanadvancetranunkid) AS lnPaid " & _
                               "		,lnloan_advance_tran.employeeunkid AS LoanEmpId " & _
                               "		,loanvoucher_no AS lnVocNo " & _
                               "		,CONVERT(CHAR(8),effective_date,112) AS lnDate " & _
                               "		,lnloan_advance_tran.loanadvancetranunkid AS lnadvunkid " & _
                               "	FROM " & drow.Item("DBase") & "..lnloan_balance_tran " & _
                               "		JOIN " & drow.Item("DBase") & "..lnloan_advance_tran ON " & drow.Item("DBase") & "..lnloan_advance_tran.loanadvancetranunkid = " & drow.Item("DBase") & "..lnloan_balance_tran.loanadvancetranunkid " & _
                               "		LEFT JOIN " & drow.Item("DBase") & "..lnloan_scheme_master ON " & drow.Item("DBase") & "..lnloan_advance_tran.loanschemeunkid = " & drow.Item("DBase") & "..lnloan_scheme_master.loanschemeunkid " & _
                               "	WHERE " & drow.Item("DBase") & "..lnloan_advance_tran.isvoid = 0 AND lnloan_advance_tran.employeeunkid = '" & intEmployeeId & "' "
                Next
                StrSubQ = Mid(StrSubQ, 6)
                StrInnerQuery &= StrSubQ
                StrInnerQuery &= ") AS C "
                StrQ &= StrInnerQuery


                StrQ &= ") A " & _
                    "LEFT JOIN " & _
                    "( "
                For Each drow As DataRow In dsDBNames.Tables(0).Rows
                    StrStatusQ &= "UNION " & _
                                  "	SELECT " & _
                                  "		 DENSE_RANK() OVER (PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY LSP.end_date DESC, lnloan_status_tran.status_date DESC, lnloan_status_tran.loanstatustranunkid DESC ) AS LN_RNO " & _
                                  "		,lnloan_status_tran.statusunkid " & _
                                  "		,lnloan_advance_tran.loanadvancetranunkid " & _
                                  "		,CASE WHEN lnloan_status_tran.statusunkid = " & enLoanStatus.IN_PROGRESS & " THEN @InProgress " & _
                                  "			  WHEN lnloan_status_tran.statusunkid = " & enLoanStatus.ON_HOLD & " THEN @OnHold " & _
                                  "			  WHEN lnloan_status_tran.statusunkid = " & enLoanStatus.WRITTEN_OFF & " THEN @WrittenOff " & _
                                  "			  WHEN lnloan_status_tran.statusunkid = " & enLoanStatus.COMPLETED & " THEN @Completed END AS lnStatus " & _
                                  "	FROM " & drow.Item("DBase") & "..lnloan_advance_tran " & _
                                  "		LEFT JOIN " & drow.Item("DBase") & "..lnloan_status_tran ON lnloan_status_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                                  "		LEFT JOIN " & drow.Item("DBase") & "..cfcommon_period_tran AS LSP ON lnloan_status_tran.periodunkid = LSP.periodunkid AND LSP.modulerefid = 1 " & _
                                  "	WHERE lnloan_advance_tran.isvoid = 0 AND lnloan_advance_tran.employeeunkid = '" & intEmployeeId & "' AND lnloan_status_tran.isvoid  = 0 " & _
                                  "		AND CONVERT(CHAR(8),LSP.start_date,112) <= '" & eZeeDate.convertDate(dtDateAsOn) & "' "
                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'ADDED Enum in [lnStatus]
                    'Nilay (20-Sept-2016) -- End

                    If StrLastDB <> drow.Item("DBase") Then
                        StrStatusQ &= " AND " & drow.Item("DBase") & "..lnloan_status_tran.loanadvancetranunkid NOT IN(SELECT lnloan_status_tran.loanadvancetranunkid FROM " & StrLastDB & "..lnloan_status_tran WHERE lnloan_status_tran.isvoid = 0) "
                    End If
                Next
                StrStatusQ = Mid(StrStatusQ, 6)
                StrQ &= StrStatusQ

                StrQ &= ") AS B ON A.lnadvunkid = B.loanadvancetranunkid AND B.LN_RNO = 1 " & _
                        "WHERE 1 = 1 "
            Else
                StrQ = "SELECT " & _
                        "	 '' AS lnScheme " & _
                        "	,'' AS lnVocNo " & _
                        "	,0 AS lnAmount " & _
                        "	,0 AS lnNetAmount " & _
                        "	,CONVERT(CHAR(8),GETDATE(),112) AS lnDate " & _
                        "	,0 AS lnBalance " & _
                        "	,0 AS lnPaid " & _
                        "	,'' AS lnStatus " & _
                        "	,'' AS lnvDate "

            End If


            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsDBNames.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Clear()
            End If

            For Each xRows As DataRow In dsList.Tables(0).Rows
                If xRows.Item("lnDate").ToString.Trim.Length > 0 Then
                    xRows.Item("lnvDate") = eZeeDate.convertDate(xRows.Item("lnDate").ToString).ToShortDateString
                End If
            Next

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoanAdvanceHistroy_List", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
    'Public Function Get_Unpaid_LoanAdvance_List(ByVal strTableName As String, Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
    Public Function Get_Unpaid_LoanAdvance_List(ByVal xDatabaseName As String _
                                               , ByVal xUserUnkid As Integer _
                                               , ByVal xYearUnkid As Integer _
                                               , ByVal xCompanyUnkid As Integer _
                                               , ByVal xPeriodStart As Date _
                                               , ByVal xPeriodEnd As Date _
                                               , ByVal xUserModeSetting As String _
                                               , ByVal xOnlyApproved As Boolean _
                                               , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                               , ByVal blnApplyUserAccessFilter As Boolean _
                                               , ByVal strTableName As String _
                                               , Optional ByVal intEmployeeID As Integer = -1) As DataSet
        'Sohail (15 Dec 2015) -- End
        'Pinkal (17-Dec-2015) -- Start     'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.[Optional ByVal intEmployeeID As Integer = -1]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (15 Dec 2015) -- Start
        'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        'Sohail (06 Jan 2016) -- Start
        'Enhancement - Show Close Year Process Logs on Close Year Wizard.
        'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        If blnApplyUserAccessFilter = True Then
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        End If
        'Sohail (06 Jan 2016) -- End
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        'Sohail (15 Dec 2015) -- End

        Dim objDataOperation As New clsDataOperation

        Try
            'Sohail (20 Aug 2010) -- Start
            'Changes:Set loanscheme ='@Advance' for isloan=0
            'strQ = "SELECT  UnpaidLoan = ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                    "THEN ( loan_amount " & _
            '                           "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
            '                    "ELSE ( advance_amount " & _
            '                           "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
            '               "END ) , " & _
            '            "lnloan_advance_tran.loanvoucher_no AS VocNo , " & _
            '            "ISNULL(hremployee_master.firstname, '') + ' ' " & _
            '            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
            '            "+ ISNULL(hremployee_master.surname, '') AS EmpName , " & _
            '            "lnloan_scheme_master.name AS LoanScheme , " & _
            '            "CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
            '                 "ELSE @Advance " & _
            '            "END AS Loan_Advance , " & _
            '            "CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
            '                 "ELSE 2 " & _
            '            "END AS Loan_AdvanceUnkid , " & _
            '            "CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                 "THEN lnloan_advance_tran.net_amount " & _
            '                 "ELSE lnloan_advance_tran.advance_amount " & _
            '            "END AS Amount , " & _
            '            "cfcommon_period_tran.period_name AS PeriodName , " & _
            '            "lnloan_advance_tran.loan_statusunkid , " & _
            '            "lnloan_advance_tran.periodunkid , " & _
            '            "lnloan_advance_tran.loanadvancetranunkid , " & _
            '            "hremployee_master.employeeunkid , " & _
            '            "hremployee_master.employeecode , " & _
            '            "lnloan_scheme_master.loanschemeunkid , " & _
            '            "lnloan_advance_tran.isloan , " & _
            '            "CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date , " & _
            '            "lnloan_advance_tran.balance_amount , " & _
            '            "lnloan_advance_tran.isloan , " & _
            '            "lnloan_advance_tran.loanschemeunkid , " & _
            '            "lnloan_advance_tran.emi_amount , " & _
            '            "lnloan_advance_tran.advance_amount , " & _
            '            "lnloan_advance_tran.loan_statusunkid " & _
            '    "FROM    lnloan_advance_tran " & _
            '            "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
            '            "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
            '            "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
            '                                            "AND ISNULL(prpayment_tran.isvoid, " & _
            '                                                       "0) = 0 " & _
            '    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
            '    "GROUP BY lnloan_advance_tran.loanvoucher_no , " & _
            '            "ISNULL(hremployee_master.firstname, '') + ' ' " & _
            '            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
            '            "+ ISNULL(hremployee_master.surname, '') , " & _
            '            "lnloan_advance_tran.isloan , " & _
            '            "lnloan_advance_tran.loan_amount , " & _
            '            "lnloan_advance_tran.advance_amount , " & _
            '            "lnloan_scheme_master.name , " & _
            '            "lnloan_advance_tran.net_amount , " & _
            '            "cfcommon_period_tran.period_name , " & _
            '            "lnloan_advance_tran.loan_statusunkid , " & _
            '            "lnloan_advance_tran.periodunkid , " & _
            '            "lnloan_advance_tran.loanadvancetranunkid , " & _
            '            "hremployee_master.employeeunkid , " & _
            '            "hremployee_master.employeecode , " & _
            '            "lnloan_scheme_master.loanschemeunkid , " & _
            '            "lnloan_advance_tran.effective_date , " & _
            '            "lnloan_advance_tran.balance_amount , " & _
            '            "lnloan_advance_tran.loanschemeunkid , " & _
            '            "lnloan_advance_tran.emi_amount " & _
            '    "HAVING  ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
            '                   "THEN ( loan_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
            '                   "ELSE ( advance_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
            '              "END ) > 0 "



            strQ = "SELECT  UnpaidLoan = ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                                            "THEN ( lnloan_advance_tran.loan_amount " & _
                                                   "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
                                            "ELSE ( lnloan_advance_tran.advance_amount " & _
                                                   "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
                                       "END ) , " & _
                        "lnloan_advance_tran.loanvoucher_no AS VoucherNo , " & _
                        "ISNULL(hremployee_master.firstname, '') + ' ' " & _
                        "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                        "+ ISNULL(hremployee_master.surname, '') AS EmpName , " & _
                        "CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                            "THEN lnloan_scheme_master.name " & _
                            "ELSE @Advance " & _
                        "END AS LoanScheme , " & _
                        "CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
                             "ELSE @Advance " & _
                        "END AS Loan_Advance , " & _
                        "CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
                             "ELSE 2 " & _
                        "END AS Loan_AdvanceUnkid , " & _
                        "CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                             "THEN lnloan_advance_tran.net_amount " & _
                             "ELSE lnloan_advance_tran.advance_amount " & _
                        "END AS Amount , " & _
                        "cfcommon_period_tran.period_name AS PeriodName , " & _
                        "lnloan_advance_tran.loan_statusunkid , " & _
                        "lnloan_advance_tran.periodunkid , " & _
                        "lnloan_advance_tran.loanadvancetranunkid , " & _
                        "hremployee_master.employeeunkid , " & _
                        "hremployee_master.employeecode , " & _
                        "lnloan_advance_tran.isloan , " & _
                        "CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date , " & _
                        "lnloan_advance_tran.balance_amount , " & _
                        "ISNULL(lnloan_advance_tran.loanschemeunkid, 0) AS loanschemeunkid , " & _
                        "lnloan_advance_tran.emi_amount , " & _
                        "lnloan_advance_tran.advance_amount , " & _
                        "lnloan_advance_tran.processpendingloanunkid " & _
                "FROM    lnloan_advance_tran " & _
                        "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
                        "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
                        "LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
                        "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                                                        "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                        "AND prpayment_tran.referenceid IN ( " & clsPayment_tran.enPaymentRefId.LOAN & ", " & clsPayment_tran.enPaymentRefId.ADVANCE & " ) "
            'Sohail (18 Jan 2019) - [LEFT JOIN lnloan_process_pending_loan], [AND prpayment_tran.referenceid IN ( " & clsPayment_tran.enPaymentRefId.LOAN & ", " & clsPayment_tran.enPaymentRefId.ADVANCE & " )]

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            'Sohail (15 Dec 2015) -- End

            strQ &= "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                    "AND lnloan_process_pending_loan.isexternal_entity = 0 "
            'Sohail (18 Jan 2019) - [AND lnloan_process_pending_loan.isexternal_entity = 0]

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            If intEmployeeID > 0 Then
                strQ &= " AND lnloan_advance_tran.employeeunkid = " & intEmployeeID
            End If
            'Pinkal (17-Dec-2015) -- End

            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select
            'Sohail (12 Jan 2015) -- Start
            'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            'strQ &= UserAccessLevel._AccessLevelFilterString
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            'If strUserAccessLevelFilterString = "" Then
            '    strQ &= UserAccessLevel._AccessLevelFilterString
            'Else
            '    strQ &= strUserAccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Sohail (15 Dec 2015) -- End
            'Sohail (12 Jan 2015) -- End
            'S.SANDEEP [ 01 JUNE 2012 ] -- END


            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Anjan (24 Jun 2011)-End 

            strQ &= "GROUP BY lnloan_advance_tran.loanvoucher_no , " & _
                        "ISNULL(hremployee_master.firstname, '') + ' ' " & _
                        "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                        "+ ISNULL(hremployee_master.surname, '') , " & _
                        "lnloan_advance_tran.isloan , " & _
                        "lnloan_advance_tran.loan_amount , " & _
                        "lnloan_advance_tran.advance_amount , " & _
                        "lnloan_scheme_master.name , " & _
                        "lnloan_advance_tran.net_amount , " & _
                        "cfcommon_period_tran.period_name , " & _
                        "lnloan_advance_tran.loan_statusunkid , " & _
                        "lnloan_advance_tran.periodunkid , " & _
                        "lnloan_advance_tran.loanadvancetranunkid , " & _
                        "hremployee_master.employeeunkid , " & _
                        "hremployee_master.employeecode , " & _
                        "lnloan_scheme_master.loanschemeunkid , " & _
                        "lnloan_advance_tran.effective_date , " & _
                        "lnloan_advance_tran.balance_amount , " & _
                        "lnloan_advance_tran.loanschemeunkid , " & _
                        "lnloan_advance_tran.emi_amount , " & _
                        "lnloan_advance_tran.processpendingloanunkid " & _
                "HAVING  ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
                               "THEN ( lnloan_advance_tran.loan_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
                               "ELSE ( lnloan_advance_tran.advance_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
                          "END ) > 0 "

            'Sohail (20 Aug 2010) -- End

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Advance"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Unpaid_LoanAdvance_List; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function InsertAuditTrailForLoanAdvanceTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, Optional ByVal iUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 29 May 2013 ] -- START -- END
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO atlnloan_advance_tran ( " & _
                        "  loanadvancetranunkid " & _
                        ", loanvoucher_no " & _
                        ", effective_date " & _
                        ", yearunkid " & _
                        ", periodunkid " & _
                        ", employeeunkid " & _
                        ", approverunkid " & _
                        ", loan_purpose " & _
                        ", balance_amount " & _
                        ", isbrought_forward " & _
                        ", isloan " & _
                        ", loanschemeunkid " & _
                        ", loan_amount " & _
                        ", interest_rate " & _
                        ", loan_duration " & _
                        ", interest_amount " & _
                        ", net_amount " & _
                        ", calctype_id " & _
                        ", loanscheduleunkid " & _
                        ", emi_tenure " & _
                        ", emi_amount " & _
                        ", advance_amount " & _
                        ", loan_statusunkid " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name " & _
                        ", processpendingloanunkid " & _
                        ", form_name " & _
                        ", module_name1 " & _
                        ", module_name2 " & _
                        ", module_name3 " & _
                        ", module_name4 " & _
                        ", module_name5 " & _
                        ", isweb " & _
                        ", bf_amount  " & _
                        ", loginemployeeunkid " & _
                        ", deductionperiodunkid" & _
                        ", bf_balance " & _
                        ", countryunkid " & _
                        ", exchange_rate " & _
                        ", basecurrency_amount " & _
                        ", balance_amountPaidCurrency " & _
                        ", opening_balance " & _
                        ", opening_balancePaidCurrency " & _
                        ", interest_calctype_id " & _
                        ", mapped_tranheadunkid " & _
                  ") VALUES (" & _
                        "  @loanadvancetranunkid " & _
                        ", @loanvoucher_no " & _
                        ", @effective_date " & _
                        ", @yearunkid " & _
                        ", @periodunkid " & _
                        ", @employeeunkid " & _
                        ", @approverunkid " & _
                        ", @loan_purpose " & _
                        ", @balance_amount " & _
                        ", @isbrought_forward " & _
                        ", @isloan " & _
                        ", @loanschemeunkid " & _
                        ", @loan_amount " & _
                        ", @interest_rate " & _
                        ", @loan_duration " & _
                        ", @interest_amount " & _
                        ", @net_amount " & _
                        ", @calctype_id " & _
                        ", @loanscheduleunkid " & _
                        ", @emi_tenure " & _
                        ", @emi_amount " & _
                        ", @advance_amount " & _
                        ", @loan_statusunkid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @processpendingloanunkid " & _
                        ", @form_name " & _
                        ", @module_name1 " & _
                        ", @module_name2 " & _
                        ", @module_name3 " & _
                        ", @module_name4 " & _
                        ", @module_name5 " & _
                        ", @isweb " & _
                        ", @bf_amount  " & _
                        ", @loginemployeeunkid " & _
                        ", @deductionperiodunkid" & _
                        ", @bf_balance " & _
                        ", @countryunkid " & _
                        ", @exchange_rate " & _
                        ", @basecurrency_amount " & _
                        ", @balance_amountPaidCurrency " & _
                        ", @opening_balance " & _
                        ", @opening_balancePaidCurrency " & _
                        ", @interest_calctype_id " & _
                        ", @mapped_tranheadunkid " & _
                    "); "
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (15 Dec 2015) - [interest_calctype_id]
            'Sohail (07 May 2015) - [balance_amountPaidCurrency, exchange_rate, basecurrency_amount, opening_balance, opening_balancePaidCurrency]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanvoucher_No.ToString)
            objDataOperation.AddParameter("@effective_date ", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Purpose.ToString)
            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isbrought_forward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbrought_Forward.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@loanscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanscheduleunkid.ToString)
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdvance_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanStatus.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(iUserUnkid <= 0, User._Object._Userunkid, iUserUnkid))
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            'objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 
            objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoanBF_Amount.ToString)
            'Sohail (07 May 2015) -- End
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 6, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString) 'Sohail (08 Apr 2013)
            'S.SANDEEP [06 MAY 2015] -- START
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryUnkid)
            objDataOperation.AddParameter("@bf_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoanBf_Balance)
            'S.SANDEEP [06 MAY 2015] -- END
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            objDataOperation.AddParameter("@balance_amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_AmountPaidCurrency.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_rate)
            objDataOperation.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBasecurrency_amount)
            objDataOperation.AddParameter("@opening_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOpening_balance)
            objDataOperation.AddParameter("@opening_balancePaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOpening_balancePaidCurrency)
            'Sohail (07 May 2015) -- End
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterest_Calctype_Id.ToString)
            'Sohail (15 Dec 2015) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
            'Sohail (29 Apr 2019) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForLoanAdvanceTran", mstrModuleName)
            Return False
        End Try
    End Function

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
    'Public Function Is_Payment_Done(Optional ByVal intDeductionPeriodID As Integer = 0) As Boolean
    Public Function Is_Payment_Done(ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As Date _
                                    , ByVal xPeriodEnd As Date _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , ByVal blnApplyUserAccessFilter As Boolean _
                                    , Optional ByVal intDeductionPeriodID As Integer = 0 _
                                    , Optional ByVal strAdvanceJoinFilter As String = "" _
                                    ) As Boolean
        'Sohail (12 Oct 2021) - [strAdvanceJoinFilter]
        'Sohail (15 Dec 2015) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (15 Dec 2015) -- Start
        'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        If blnApplyUserAccessFilter = True Then 'Sohail (12 Oct 2021)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        End If 'Sohail (12 Oct 2021)
        If strAdvanceJoinFilter.Trim.Length > 0 Then 'Sohail (12 Oct 2021)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        End If 'Sohail (12 Oct 2021)
        'Sohail (15 Dec 2015) -- End

        Dim objDataOperation As New clsDataOperation

        Try

            strQ = "SELECT  lnloan_advance_tran.loanadvancetranunkid " & _
                          ", lnloan_advance_tran.loanvoucher_no " & _
                          ", lnloan_advance_tran.effective_date " & _
                          ", lnloan_advance_tran.yearunkid " & _
                          ", lnloan_advance_tran.periodunkid " & _
                          ", lnloan_advance_tran.employeeunkid " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                          ", lnloan_scheme_master.name AS LoanSchemeName " & _
                          ", lnloan_advance_tran.approverunkid " & _
                          ", lnloan_advance_tran.loan_purpose " & _
                          ", lnloan_advance_tran.balance_amount " & _
                          ", lnloan_advance_tran.isbrought_forward " & _
                          ", lnloan_advance_tran.isloan " & _
                          ", lnloan_advance_tran.loanschemeunkid " & _
                          ", lnloan_advance_tran.loan_amount " & _
                          ", lnloan_advance_tran.interest_rate " & _
                          ", lnloan_advance_tran.loan_duration " & _
                          ", lnloan_advance_tran.interest_amount " & _
                          ", lnloan_advance_tran.net_amount " & _
                          ", lnloan_advance_tran.calctype_id " & _
                          ", lnloan_advance_tran.loanscheduleunkid " & _
                          ", lnloan_advance_tran.emi_tenure " & _
                          ", lnloan_advance_tran.emi_amount " & _
                          ", lnloan_advance_tran.advance_amount " & _
                          ", lnloan_advance_tran.loan_statusunkid " & _
                          ", lnloan_advance_tran.userunkid " & _
                          ", lnloan_advance_tran.isvoid " & _
                          ", lnloan_advance_tran.voiduserunkid " & _
                          ", lnloan_advance_tran.voiddatetime " & _
                          ", lnloan_advance_tran.voidreason " & _
                          ", lnloan_advance_tran.processpendingloanunkid " & _
                          ", lnloan_advance_tran.deductionperiodunkid " & _
                    "FROM    lnloan_advance_tran " & _
                            "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                                    "AND prpayment_tran.referenceid IN ( 1, 2 ) " & _
                                    "AND prpayment_tran.paytypeid = 1 " & _
                            "LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid=lnloan_scheme_master.loanschemeunkid " & _
                            "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid=hremployee_master.employeeunkid " & _
                            "LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid "

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Sohail (15 Dec 2015) -- End

            strQ &= "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND lnloan_process_pending_loan.isexternal_entity = 0 " & _
                            "AND prpayment_tran.paymenttranunkid IS NULL "


            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            'strQ &= UserAccessLevel._AccessLevelFilterString
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Sohail (15 Dec 2015) -- End
            'S.SANDEEP [ 01 JUNE 2012 ] -- END


            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Anjan (24 Jun 2011)-End 



            If intDeductionPeriodID > 0 Then
                strQ &= "AND lnloan_advance_tran.deductionperiodunkid = @deductionperiodunkid "
                objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeductionPeriodID)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows.Count <= 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Unpaid_LoanAdvance_List; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Calculate_LoanInfo(ByVal enCalc As enLoanCalcId, _
                                                       ByVal decPrincipleAmt As Decimal, _
                                                       ByVal intDuration As Integer, _
                                                       ByVal dblRate As Double, _
                                                       ByRef mdecNetAmount As Decimal, _
                                                       ByRef mdecInterest_Amount As Decimal, _
                                                       Optional ByRef decRedEMI As Decimal = 0) As Decimal
        Try
            Select Case enCalc
                Case enLoanCalcId.Simple_Interest
                    If intDuration > 12 Then
                        mdecInterest_Amount = (decPrincipleAmt * dblRate * (intDuration / 12)) / 100
                    Else
                        mdecInterest_Amount = ((decPrincipleAmt * dblRate * intDuration) / 100) / 12
                    End If
                    mdecNetAmount = mdecInterest_Amount + decPrincipleAmt
                Case enLoanCalcId.Compound_Interest
                    If intDuration > 12 Then
                        mdecInterest_Amount = decPrincipleAmt * CDec(Pow((1 + (dblRate / 100)), (intDuration / 12)))
                    Else
                        mdecInterest_Amount = decPrincipleAmt * CDec(Pow((1 + (dblRate / 100)), intDuration))
                    End If
                    mdecNetAmount = mdecInterest_Amount + decPrincipleAmt
                Case enLoanCalcId.Reducing_Amount
                    Dim mdecMonthlyRate, mdecTotalRate, mdecNewReduceAmt As Decimal

                    mdecMonthlyRate = 0 : mdecTotalRate = 0 : mdecNewReduceAmt = 0

                    mdecMonthlyRate = ((dblRate / 12) / 100) + 1
                    mdecTotalRate = CDec(Pow(mdecMonthlyRate, intDuration)) - 1

                    If mdecTotalRate > 0 Then
                        mdecTotalRate = (mdecMonthlyRate - 1) / mdecTotalRate
                    Else
                        mdecTotalRate = 0
                    End If

                    mdecTotalRate = mdecTotalRate + (mdecMonthlyRate - 1)
                    decRedEMI = decPrincipleAmt * mdecTotalRate

                    For i As Integer = 1 To CInt(intDuration)
                        If i = 1 Then
                            mdecInterest_Amount += decPrincipleAmt * (mdecMonthlyRate - 1)
                            mdecNewReduceAmt = (decPrincipleAmt - (decRedEMI - (decPrincipleAmt * (mdecMonthlyRate - 1))))
                        Else
                            mdecInterest_Amount += mdecNewReduceAmt * (mdecMonthlyRate - 1)
                            mdecNewReduceAmt = (mdecNewReduceAmt - (decRedEMI - (mdecNewReduceAmt * (mdecMonthlyRate - 1))))
                        End If
                    Next

                    mdecNetAmount = mdecInterest_Amount + decPrincipleAmt

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_LoanInfo", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Calculate_LoanEMI(ByVal intEMI_ModeId As Integer, ByVal decNetAmount As Decimal, ByRef decInstallmentAmt As Decimal, ByRef intNoOfEMI As Integer) As Decimal
        Try
            Select Case intEMI_ModeId
                Case 1  'AMOUNT
                    If decInstallmentAmt > 0 Then
                        intNoOfEMI = CInt(IIf(decInstallmentAmt = 0, 0, decNetAmount / decInstallmentAmt))
                    Else
                        intNoOfEMI = 0
                        Exit Try
                    End If

                    If CInt(CDec(IIf(decInstallmentAmt = 0, 0, decNetAmount / decInstallmentAmt))) > intNoOfEMI Then
                        intNoOfEMI += 1
                    End If
                Case 2  'MONTHS
                    If intNoOfEMI > 0 Then
                        decInstallmentAmt = CDec(IIf(intNoOfEMI = 0, 0, decNetAmount / intNoOfEMI))
                    Else
                        decInstallmentAmt = 0
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_LoanEMI", mstrModuleName)
        Finally
        End Try
    End Function

    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    'Public Function GetDataForProcessPayroll(ByVal strTableName As String, ByVal intEmpUnkID As Integer, ByVal dtPeriodStart As DateTime) As DataTable
    '    Dim dtTable As DataTable = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try
    '        strQ = "SELECT   loanadvancetranunkid " & _
    '                      ", lnloan_advance_tran.balance_amount " & _
    '                      ", lnloan_advance_tran.isloan " & _
    '                      ", lnloan_advance_tran.emi_amount " & _
    '                      ", lnloan_advance_tran.advance_amount " & _
    '                      ", lnloan_advance_tran.balance_amount " & _
    '                "FROM    lnloan_advance_tran " & _
    '                "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
    '                "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                "AND     balance_amount > 0 " & _
    '                "AND     loan_statusunkid = 1 " & _
    '                "AND     TableDeductionPeriod.start_date <= @startdate " & _
    '                "AND     employeeunkid = @employeeunkid "

    '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)

    '        dtTable = objDataOperation.ExecQuery(strQ, strTableName).Tables(0)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetDataForProcessPayroll; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dtTable IsNot Nothing Then dtTable.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dtTable
    'End Function
    Public Function GetDataForProcessPayroll(ByVal strTableName As String, ByVal intEmpUnkID As Integer, ByVal dtPeriodStart As DateTime, Optional ByVal intLoanAdvanceUnkId As Integer = 0) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim strFilter As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            If intLoanAdvanceUnkId > 0 Then
                strFilter &= " AND lnloan_advance_tran.loanadvancetranunkid = @loanadvancetranunkid "
                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceUnkId)
            End If

            strQ = "SELECT  Loan.loanadvancetranunkid  " & _
                          ", Loan.loaneffectivedate " & _
                          ", Loan.emi_amount / ExRate.exchange_rate AS emi_amount " & _
                          ", Loan.advance_amount / ExRate.exchange_rate AS advance_amount " & _
                          ", Loan.isloan " & _
                          ", Loan.balance_amount " & _
                          ", Loan.calctype_id " & _
                          ", Loan.loan_amount / ExRate.exchange_rate AS loan_amount " & _
                          ", ExRate.exchangerateunkid AS currencyunkid " & _
                          ", ISNULL(IntRate.IntRateEffectiveDate, Loan.loaneffectivedate) AS IntRateEffectiveDate " & _
                          ", ISNULL(IntRate.interest_rate, 0) AS interest_rate " & _
                          ", Tenure.TenureEffectiveDate " & _
                          ", Tenure.emi_tenure " & _
                          ", ISNULL(Topup.TopupEffectiveDate, '19000101') AS TopupEffectiveDate " & _
                          ", ISNULL(Topup.topup_amount, 0) AS topup_amount " & _
                          ", ISNULL(Receipt.ReceiptEffectiveDate, '19000101') AS ReceiptEffectiveDate " & _
                          ", ISNULL(Receipt.expaidamt, 0) AS ReceiptAmount " & _
                          ", ISNULL(Balance.cf_amount, Loan.loan_amount / ExRate.exchange_rate) AS PrincipleBalance " & _
                          ", ISNULL(Balance.cf_balance, Loan.loan_amount / ExRate.exchange_rate) AS BalanceAmount " & _
                          ", ISNULL(Balance.BalanceEffectiveDate, Loan.loaneffectivedate) AS BalanceEffectiveDate " & _
                          ", DATEADD(DAY, -1, DATEADD(MONTH, Tenure.emi_tenure, Tenure.TenureEffectiveDate)) AS LoanEndDate /* <TODO : Pick end date from EMI Tenure table>*/ " & _
                          ", DATEDIFF(DAY, Tenure.TenureEffectiveDate, DATEADD(DAY, -1, DATEADD(MONTH, Tenure.emi_tenure, Tenure.TenureEffectiveDate))) + 1 AS TenureDays " & _
                          ", Loan.interest_calctype_id " & _
                    "FROM    ( SELECT    lnloan_advance_tran.loanadvancetranunkid  " & _
                                      ", ISNULL(lnloan_emitenure_tran.emi_amount, 0) AS emi_amount " & _
                                      ", lnloan_advance_tran.advance_amount " & _
                          ", lnloan_advance_tran.isloan " & _
                          ", lnloan_advance_tran.balance_amount " & _
                                      ", lnloan_advance_tran.loan_amount " & _
                                      ", lnloan_advance_tran.calctype_id " & _
                                      ", ISNULL(lnloan_advance_tran.interest_calctype_id, 1) AS interest_calctype_id " & _
                                      ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS loaneffectivedate " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY lnloan_emitenure_tran.loanadvancetranunkid ORDER BY lnloan_emitenure_tran.effectivedate DESC ) AS ROWNO " & _
                    "FROM    lnloan_advance_tran " & _
                                        "LEFT JOIN lnloan_emitenure_tran ON lnloan_emitenure_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
                                        "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
                                                                                  "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
                              "WHERE     ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 " & _
                                        "AND TableDeductionPeriod.start_date <= @startdate " & _
                                        "AND lnloan_advance_tran.employeeunkid = @employeeunkid " & _
                                        strFilter & _
                            ") AS Loan " & _
                            "JOIN ( SELECT   lnloan_advance_tran.loanadvancetranunkid  " & _
                                          ", lnloan_status_tran.statusunkid " & _
                                          ", DENSE_RANK() OVER ( PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY lnloan_status_tran.status_date DESC ) AS ROWNO " & _
                                   "FROM     lnloan_advance_tran " & _
                                            "LEFT JOIN lnloan_status_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_status_tran.loanadvancetranunkid " & _
                                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_status_tran.periodunkid " & _
                                            "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
                                                                                  "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
                                   "WHERE    ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                                            "AND ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
                                            "AND TableDeductionPeriod.start_date <= @startdate " & _
                                            "AND lnloan_advance_tran.employeeunkid = @employeeunkid " & _
                                            strFilter & _
                                 ") AS Status ON Loan.loanadvancetranunkid = Status.loanadvancetranunkid " & _
                            "JOIN ( SELECT   lnloan_advance_tran.loanadvancetranunkid  " & _
                                          ", cfexchange_rate.exchange_rate " & _
                                          ", ISNULL(cfexchange_rate.exchangerateunkid, 1) AS exchangerateunkid " & _
                                          ", DENSE_RANK() OVER ( PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY exchange_date DESC, exchangerateunkid DESC ) AS ROWNO " & _
                                   "FROM     lnloan_advance_tran " & _
                                            "LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
                                            "LEFT JOIN cfexchange_rate ON cfexchange_rate.countryunkid = lnloan_process_pending_loan.countryunkid " & _
                                            "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
                                                                                  "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
                                            "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                                                                        "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                                                                        "AND prpayment_tran.referenceid IN ( " & clsPayment_tran.enPaymentRefId.LOAN & ", " & clsPayment_tran.enPaymentRefId.ADVANCE & " ) " & _
                    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                                            "AND ISNULL(lnloan_process_pending_loan.isvoid, 0) = 0 " & _
                                            "AND cfexchange_rate.isactive = 1 " & _
                                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                            "AND ( prpayment_tran.paymenttranunkid IS NOT NULL " & _
                                                  "OR lnloan_process_pending_loan.isexternal_entity = 1 " & _
                                                ") " & _
                    "AND     TableDeductionPeriod.start_date <= @startdate " & _
                                            "AND lnloan_advance_tran.employeeunkid = @employeeunkid " & _
                                            strFilter & _
                                 ") AS ExRate ON Loan.loanadvancetranunkid = ExRate.loanadvancetranunkid " & _
                            "LEFT JOIN ( SELECT  lnloan_interest_tran.loanadvancetranunkid  " & _
                                              ", CONVERT(CHAR(8), lnloan_interest_tran.effectivedate, 112) AS IntRateEffectiveDate " & _
                                              ", lnloan_interest_tran.interest_rate " & _
                                              ", DENSE_RANK() OVER ( PARTITION BY lnloan_interest_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_interest_tran.effectivedate DESC, lnloan_interest_tran.lninteresttranunkid DESC ) AS ROWNO " & _
                                        "FROM    lnloan_interest_tran " & _
                                                "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
                                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
                                                                                  "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                        "WHERE   ISNULL(lnloan_interest_tran.isvoid, 0) = 0 " & _
                                                "AND lnloan_advance_tran.isvoid = 0 " & _
                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) <= @startdate " & _
                                                "AND lnloan_advance_tran.employeeunkid = @employeeunkid " & _
                                                strFilter & _
                                      ") AS IntRate ON Loan.loanadvancetranunkid = IntRate.loanadvancetranunkid " & _
                            "LEFT JOIN ( SELECT  lnloan_emitenure_tran.loanadvancetranunkid  " & _
                                              ", CONVERT(CHAR(8), lnloan_emitenure_tran.effectivedate, 112) AS TenureEffectiveDate " & _
                                              ", lnloan_emitenure_tran.emi_tenure " & _
                                              ", DENSE_RANK() OVER ( PARTITION BY lnloan_emitenure_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_emitenure_tran.effectivedate DESC, lnloan_emitenure_tran.lnemitranunkid DESC ) AS ROWNO " & _
                                        "FROM    lnloan_emitenure_tran " & _
                                                "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
                                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
                                                                                  "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                        "WHERE   ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 " & _
                                                "AND lnloan_advance_tran.isvoid = 0 " & _
                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) <= @startdate " & _
                                                "AND lnloan_advance_tran.employeeunkid = @employeeunkid " & _
                                                strFilter & _
                                      ") AS Tenure ON Loan.loanadvancetranunkid = Tenure.loanadvancetranunkid " & _
                            "LEFT JOIN ( SELECT  lnloan_topup_tran.loanadvancetranunkid  " & _
                                              ", CONVERT(CHAR(8), lnloan_topup_tran.effectivedate, 112) AS TopupEffectiveDate " & _
                                              ", lnloan_topup_tran.topup_amount " & _
                                              ", 1 AS ROWNO " & _
                                        "FROM    lnloan_topup_tran " & _
                                                "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
                                        "WHERE   ISNULL(lnloan_topup_tran.isvoid, 0) = 0 " & _
                                                "AND lnloan_advance_tran.isvoid = 0 " & _
                                                "AND CONVERT(CHAR(8), lnloan_topup_tran.effectivedate, 112) BETWEEN @startdate AND @enddate " & _
                                                "AND lnloan_advance_tran.employeeunkid = @employeeunkid " & _
                                                strFilter & _
                                      ") AS Topup ON Loan.loanadvancetranunkid = Topup.loanadvancetranunkid " & _
                            "LEFT JOIN ( SELECT  prpayment_tran.referencetranunkid  " & _
                                              ", CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) AS ReceiptEffectiveDate " & _
                                              ", prpayment_tran.expaidamt " & _
                                              ", DENSE_RANK() OVER ( PARTITION BY prpayment_tran.referencetranunkid ORDER BY cfcommon_period_tran.end_date DESC, prpayment_tran.paymentdate DESC, prpayment_tran.paymenttranunkid DESC ) AS ROWNO " & _
                                        "FROM    prpayment_tran " & _
                                                "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                                                                                 "AND prpayment_tran.referenceid IN ( " & clsPayment_tran.enPaymentRefId.LOAN & ", " & clsPayment_tran.enPaymentRefId.ADVANCE & " ) " & _
                                                                                 "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.RECEIVED & " " & _
                                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prpayment_tran.periodunkid " & _
                                                                                  "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                        "WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                "AND lnloan_advance_tran.isvoid = 0 " & _
                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) <= @startdate " & _
                                                "AND lnloan_advance_tran.employeeunkid = @employeeunkid " & _
                                                strFilter & _
                                      ") AS Receipt ON Loan.loanadvancetranunkid = Receipt.referencetranunkid " & _
                            "LEFT JOIN ( SELECT  lnloan_balance_tran.loanadvancetranunkid  " & _
                                              ", lnloan_balance_tran.cf_balance " & _
                                              ", lnloan_balance_tran.cf_amount " & _
                                              ", CONVERT(CHAR(8), lnloan_balance_tran.end_date, 112) AS BalanceEffectiveDate " & _
                                              ", DENSE_RANK() OVER ( PARTITION BY lnloan_balance_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_balance_tran.end_date DESC, lnloan_balance_tran.loanbalancetranunkid DESC ) AS ROWNO " & _
                                        "FROM    lnloan_balance_tran " & _
                                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_balance_tran.periodunkid " & _
                                                                                  "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                        "WHERE   ISNULL(lnloan_balance_tran.isvoid, 0) = 0 " & _
                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) < @startdate " & _
                                                "AND lnloan_balance_tran.employeeunkid = @employeeunkid " & _
                                                strFilter & _
                                      ") AS Balance ON Loan.loanadvancetranunkid = Balance.loanadvancetranunkid " & _
                    "WHERE   Loan.ROWNO = 1 " & _
                            "AND Status.ROWNO = 1 " & _
                            "AND ExRate.ROWNO = 1 " & _
                            "AND ISNULL(IntRate.ROWNO, 1) = 1 " & _
                            "AND ISNULL(Tenure.ROWNO, 1) = 1 " & _
                            "AND ISNULL(Topup.ROWNO, 1) = 1 " & _
                            "AND ISNULL(Receipt.ROWNO, 1) = 1 " & _
                            "AND ISNULL(Balance.ROWNO, 1) = 1 " & _
                            "AND Status.statusunkid = " & enLoanStatus.IN_PROGRESS & " " & _
                            "AND (ISNULL(Balance.cf_amount, Loan.loan_amount) > 0 OR Loan.isloan = 0) "
            'Sohail (15 Dec 2015) - [interest_calctype_id]

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)

            dtTable = objDataOperation.ExecQuery(strQ, strTableName).Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataForProcessPayroll; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dtTable IsNot Nothing Then dtTable.Dispose()
            objDataOperation = Nothing
        End Try
        Return dtTable
    End Function

    'Nilay (25-Mar-2016) -- Start
    'Public Function Calculate_LoanBalanceInfo(ByVal strTableName As String _
    '                                          , ByVal dtAsOnDate As Date _
    '                                          , ByVal strEmployeeIDs As String _
    '                                          , ByVal intLoanAdvanceUnkId As Integer _
    '                                          , Optional ByVal dtPeriodStartDate As Date = Nothing _
    '                                          , Optional ByVal blnFromProcessPayroll As Boolean = False _
    '                                          , Optional ByVal blnIncludeUnPaidLoan As Boolean = False _
    '                                          , Optional ByVal strLoanSchemeIDs As String = "" _
    '                                          , Optional ByVal blnIncludeOnHoldWrittenOff As Boolean = False _
    '                                          , Optional ByVal blnForReport As Boolean = False _
    '                                          , Optional ByVal blnFirstNameThenSurname As Boolean = True _
    '                                          , Optional ByVal blnIgnoreCurrentMonthTopupRepaymentInBFBalance As Boolean = False _
    '                                          , Optional ByVal blnIncludeCompletedLoanAdvance As Boolean = False _
    '                                          , Optional ByVal strExtraReportFilterString As String = "" _
    '                                          , Optional ByVal strAdvanceQueryJoin As String = "" _
    '                                          , Optional ByVal strEmployeeAlloctionJoin As String = "" _
    '                                          , Optional ByVal intLoanStatus As Integer = 0 _
    '                                          ) As DataSet

    '''<summary>
    '''Calculate Loan Balance
    '''</summary>
    '''<param name="strTableName">Table Name</param>
    '''<param name="dtAsOnDate">Loan Balance As on Date</param>
    '''<param name="strEmployeeIDs">Pass Blank to Ignore</param>
    '''<param name="intLoanAdvanceUnkId">Pass Zero to Ignore</param>
    '''<param name="dtPeriodStartDate">Pass Period Start Date to get Total Monthly Deduction between Period Start Date and As On Date</param>
    '''<param name="blnFromProcessPayroll">Pass True if function is calling from Process Payroll to calculate Sum of Total PMT amount</param>
    '''<param name="blnIncludeUnPaidLoan">Pass True to if function is calling from other than Process Payroll</param>
    '''<param name="strLoanSchemeIDs">Pass Blank to Ignore</param>
    '''<param name="blnIncludeOnHoldWrittenOff">Pass True to Ignore loan status filter</param>
    '''<param name="blnForReport">Pass True to include Period Name, Emp Name, Loan Scheme Name column</param>
    '''<param name="blnFirstNameThenSurname">Pass False to get Surname then First Name for Emp Name column</param>
    '''<param name="blnIgnoreCurrentMonthTopupRepaymentInBFBalance">Pass True to ignore current month (open period) Topup amount and Repayment amount in BF Balance calculation (for Report)</param>
    '''<param name="blnIncludeCompletedLoanAdvance">Pass True to include COMPLETED status and WRITTEN_OFF status loan and advance</param>
    '''<param name="strLoanAdvanceTranIDs">To process for multiple loans simultaneously(i.e. Global Change Status(written off))</param> 
    '''<returns></returns>
    '''<remarks></remarks>
    Public Function Calculate_LoanBalanceInfo(ByVal xDatabaseName As String _
                                            , ByVal xUserUnkid As Integer _
                                            , ByVal xYearUnkid As Integer _
                                            , ByVal xCompanyUnkid As Integer _
                                            , ByVal xPeriodStart As DateTime _
                                            , ByVal xPeriodEnd As DateTime _
                                            , ByVal xUserModeSetting As String _
                                            , ByVal xOnlyApproved As Boolean _
                                            , ByVal strTableName As String _
                                              , ByVal dtAsOnDate As Date _
                                              , ByVal strEmployeeIDs As String _
                                            , Optional ByVal intLoanAdvanceUnkId As Integer = 0 _
                                              , Optional ByVal dtPeriodStartDate As Date = Nothing _
                                              , Optional ByVal blnFromProcessPayroll As Boolean = False _
                                              , Optional ByVal blnIncludeUnPaidLoan As Boolean = False _
                                              , Optional ByVal strLoanSchemeIDs As String = "" _
                                              , Optional ByVal blnIncludeOnHoldWrittenOff As Boolean = False _
                                              , Optional ByVal blnForReport As Boolean = False _
                                              , Optional ByVal blnFirstNameThenSurname As Boolean = True _
                                              , Optional ByVal blnIgnoreCurrentMonthTopupRepaymentInBFBalance As Boolean = False _
                                              , Optional ByVal blnIncludeCompletedLoanAdvance As Boolean = False _
                                              , Optional ByVal strExtraReportFilterString As String = "" _
                                              , Optional ByVal strAdvance_Filter As String = "" _
                                              , Optional ByVal strEmployeeAlloctionJoin As String = "" _
                                              , Optional ByVal intLoanStatus As Integer = 0 _
                                              , Optional ByVal blnIsMSS As Boolean = True _
                                            , Optional ByVal strLoanAdvanceTranIDs As String = "" _
                                            , Optional ByVal blnAddProjectedPeriodDeductions As Boolean = False _
                                              , Optional ByVal intBranchUnkId As Integer = 0 _
                                              , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                              , Optional ByVal blnIncludeNoInterestMappedHeadCalcType As Boolean = True _
                                              , Optional ByVal strFmtCurrency As String = "" _
                                              , Optional ByVal blnApplyDateFilter As Boolean = True _
                                              ) As DataSet
        'Sohail (12 Oct 2021) - [blnApplyDateFilter]
        'Sohail (26 Oct 2020) - [strFmtCurrency]
        'Sohail (29 Apr 2019) - [blnIncludeNoInterestMappedHeadCalcType]
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        'Dim strQ As String = ""
        Dim strB As New System.Text.StringBuilder
        Dim strFilter As String = ""
        Dim strCompletedFilter As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End
        'Hemant (25 Feb 2019) -- Start
        'Issue PACRA #3524: Not able to Void The payroll Due to error Attempted to Divide by Zero .
        Dim strLoanVoucherNo As String = String.Empty
        'Hemant (25 Feb 2019) -- End

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String 'Sohail (04 Aug 2017) -- [xAdvanceJoinQry]
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = "" 'Sohail (04 Aug 2017) -- [xAdvanceJoinQry]
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , True, xDatabaseName)
            If blnApplyDateFilter = True Then
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , True, xDatabaseName)
            End If
            'Sohail (12 Oct 2021) -- End
            'Sohail (04 Aug 2017) -- Start
            'Enhancement - 69.1 - Advance filter on open period for loan report.
            If strAdvance_Filter.Trim.Length > 0 Then 'Sohail (26 Oct 2020)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            End If 'Sohail (26 Oct 2020)
            'Sohail (04 Aug 2017) -- End

            If blnIsMSS Then
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If

            Dim mdecTotPMTAmt As Decimal = 0
            Dim mdecTotPMTAmtPaidCurrency As Decimal = 0
            Dim mdecTotIntAmt As Decimal = 0
            Dim mdecTotIntAmtPaidCurrency As Decimal = 0
            Dim mdecTotPrincipalAmt As Decimal = 0
            Dim mdecTotPrincipalAmtPaidCurrency As Decimal = 0
            Dim mdecLastProjectedBalance As Decimal = 0
            Dim mdecLastProjectedBalancePaidCurrency As Decimal = 0

            'Hemant (03 Oct 2018)  -- Start
            'Enhancement :CCK # 0002584 Credit and Debit Amounts Not Balancing on Journal
            'Sohail (20 Oct 2020) -- Start
            'NMB Performance Issue : # : Performance issue on global payment screen.
            'Dim strFmtCurrency As String = String.Empty
            If strFmtCurrency.Trim = "" Then
                'Sohail (26 Oct 2020) -- End
            Dim objConfigOptions As New clsConfigOptions
            objConfigOptions._Companyunkid = xCompanyUnkid
            strFmtCurrency = objConfigOptions._CurrencyFormat
                'Sohail (26 Oct 2020) -- Start
                'NMB Performance Issue : # : Performance issue on global payment screen.
                objConfigOptions = Nothing
            End If
            'Sohail (26 Oct 2020) -- End
            'Hemant (03 Oct 2018)  -- End

            strB.Length = 0
            strB.Append("CREATE TABLE #TableEmp " & _
                        "( " & _
                        "  employeeunkid INT " & _
                        ", EmpCode VARCHAR(MAX) " & _
                        ", stationunkid INT " & _
                        ", EmpName VARCHAR(MAX) " & _
                        ") " & _
                        "INSERT INTO #TableEmp " & _
                        "SELECT   hremployee_master.employeeunkid " & _
                               ", employeecode AS EmpCode " & _
                               ", Alloc.stationunkid " _
                         )

            If blnFirstNameThenSurname = False Then
                strB.Append("          , ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName ")
            Else
                strB.Append("          , ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName ")
            End If

            strB.Append("FROM hremployee_master " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "      SELECT " & _
                        "           stationunkid " & _
                        "          ,employeeunkid " & _
                        "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "      FROM hremployee_transfer_tran " & _
                        "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " _
                        )

            If xDateJoinQry.Trim.Length > 0 Then
                strB.Append(xDateJoinQry)
            End If

            If xUACQry.Trim.Length > 0 Then
                strB.Append(xUACQry)
            End If

            'Sohail (04 Aug 2017) -- Start
            'Enhancement - 69.1 - Advance filter on open period for loan report.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            If xAdvanceJoinQry.Trim.Length > 0 Then
                strB.Append(xAdvanceJoinQry)
            End If
            'Sohail (04 Aug 2017) -- End

            strB.Append("WHERE 1 = 1 ")

            'Sohail (30 Apr 2019) -- Start
            'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
            If strEmployeeIDs.Trim <> "" AndAlso strEmployeeIDs.Trim <> "0" AndAlso strEmployeeIDs.Trim <> "-1" Then
                strB.Append(" AND hremployee_master.employeeunkid IN (" & strEmployeeIDs & ") ")
            End If
            'Sohail (30 Apr 2019) -- End

            'If mblnIsActive = False Then
            If xDateFilterQry.Trim.Length > 0 Then
                strB.Append(xDateFilterQry)
            End If
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strB.Append(" AND " & xUACFiltrQry)
            End If

            If intBranchUnkId > 0 Then
                strB.Append(" AND Alloc.stationunkid = " & intBranchUnkId & " ")
            End If

            'Sohail (30 Apr 2019) -- Start
            'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
            'If strEmployeeIDs.Trim <> "" AndAlso strEmployeeIDs.Trim <> "0" AndAlso strEmployeeIDs.Trim <> "-1" Then
            '    strB.Append(" AND hremployee_master.employeeunkid IN (" & strEmployeeIDs & ") ")
            'End If
            'Sohail (30 Apr 2019) -- End

            'Sohail (04 Aug 2017) -- Start
            'Enhancement - 69.1 - Advance filter on open period for loan report.
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    strQ &= " AND " & mstrAdvance_Filter
            'End If
            If strAdvance_Filter.Trim.Length > 0 Then
                strB.Append(" AND " & strAdvance_Filter & " ")
            End If
            'Sohail (04 Aug 2017) -- End

            strB.Append("SELECT * INTO    #TableLoanAdvance FROM ( " & _
                            "SELECT  lnloan_advance_tran.loanadvancetranunkid  " & _
                                  ", ISNULL(lnloan_emitenure_tran.basecurrency_amount, lnloan_advance_tran.emi_amount) AS emi_amount " & _
                                  ", ISNULL(lnloan_emitenure_tran.emi_amount, lnloan_advance_tran.emi_amount * lnloan_advance_tran.exchange_rate) AS emi_amountPaidCurrency " & _
                                  ", lnloan_advance_tran.advance_amount " & _
                                  ", lnloan_advance_tran.isloan " & _
                                  ", lnloan_advance_tran.balance_amount " & _
                                  ", lnloan_advance_tran.balance_amountPaidCurrency " & _
                                  ", lnloan_advance_tran.loan_amount " & _
                                  ", lnloan_advance_tran.opening_balance " & _
                                  ", lnloan_advance_tran.opening_balancePaidCurrency " & _
                                  ", CASE lnloan_advance_tran.isloan WHEN 1 THEN lnloan_advance_tran.loan_amount ELSE lnloan_advance_tran.advance_amount END AS loan_advance_amount " & _
                                  ", lnloan_advance_tran.calctype_id " & _
                                  ", ISNULL(lnloan_advance_tran.basecurrency_amount, lnloan_advance_tran.loan_amount) AS basecurrency_amount " & _
                                  ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS loaneffectivedate " & _
                                  ", lnloan_advance_tran.loanschemeunkid " & _
                                  ", lnloan_advance_tran.employeeunkid " & _
                                  ", lnloan_advance_tran.interest_rate " & _
                                  ", lnloan_advance_tran.emi_tenure " & _
                                  ", lnloan_advance_tran.countryunkid " & _
                                  ", lnloan_advance_tran.exchange_rate " & _
                                  ", lnloan_advance_tran.deductionperiodunkid " & _
                                  ", lnloan_advance_tran.isbrought_forward " & _
                                  ", cfcommon_period_tran.prescribed_interest_rate " & _
                                  ", ISNULL(lnloan_advance_tran.interest_calctype_id, 1) AS interest_calctype_id " & _
                                  ", lnloan_advance_tran.loan_statusunkid " & _
                                  ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS end_date  " & _
                                  ", lnloan_advance_tran.processpendingloanunkid " & _
                                  ", ISNULL(RefLoan.loanschemeunkid, 0) AS loanschemeinterestunkid " & _
                                  ", ISNULL(lnloan_advance_tran.mapped_tranheadunkid, 0) AS mapped_tranheadunkid " & _
                                  ", DENSE_RANK() OVER ( PARTITION BY lnloan_emitenure_tran.loanadvancetranunkid ORDER BY lnloan_emitenure_tran.effectivedate DESC ) AS ROWNO " _
                                  )
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (02 Apr 2018) - [loanschemeinterestunkid]

            If blnForReport = True Then

                strB.Append("     , TableDeductionPeriod.period_name AS PeriodName " & _
                                 ", lnloan_advance_tran.loanvoucher_no " & _
                                 ", lnloan_advance_tran.exchange_rate AS loan_exchange_rate " & _
                                 ", lnloan_advance_tran.net_amount " & _
                                 ", #TableEmp.EmpCode  " & _
                                 ", #TableEmp.EmpName " _
                                 )
            End If

            strB.Append("    FROM     lnloan_advance_tran " & _
                                      "JOIN #TableEmp ON #TableEmp.employeeunkid = lnloan_advance_tran.employeeunkid " & _
                                      "LEFT JOIN lnloan_emitenure_tran ON lnloan_emitenure_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                                      "LEFT JOIN lnloan_scheme_master AS RefLoan ON lnloan_advance_tran.loanschemeunkid = RefLoan.refloanschemeunkid " & _
                                            "AND RefLoan.isactive = 1 AND isloan = 1 " & _
                                      "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
                                      "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
                                           "AND TableDeductionPeriod.modulerefid = " & CInt(enModuleReference.Payroll) & " " _
                                      )
            'Sohail (02 Apr 2018) - [Added : "AND RefLoan.isactive = 1 AND isloan = 1]
            'Sohail (02 Apr 2018) - [LEFT JOIN lnloan_scheme_master AS RefLoan]

            If blnForReport = True Then

                'strQ &= "                JOIN #TableEmp ON #TableEmp.employeeunkid = lnloan_advance_tran.employeeunkid "


            Else
                'strQ &= "                    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_advance_tran.employeeunkid "
                'strQ &= "                JOIN #TableEmp ON #TableEmp.employeeunkid = lnloan_advance_tran.employeeunkid "

            End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If

            strB.Append("    WHERE     lnloan_advance_tran.isvoid = 0 " & _
                                      "AND ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 " & _
                                      "AND TableDeductionPeriod.start_date <= @AsOnDate " & _
                                      "AND lnloan_advance_tran.effective_date <= @PeriodStartDate " _
                                      )

            If blnIncludeCompletedLoanAdvance = False Then
                strB.Append(" AND (lnloan_advance_tran.isloan = 1 OR TableDeductionPeriod.start_date = @PeriodStartDate) ")
            End If

            If intLoanAdvanceUnkId > 0 Then
                strB.Append(" AND lnloan_advance_tran.loanadvancetranunkid = @loanadvancetranunkid ")
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceUnkId)
                'Sohail (03 May 2018) -- End
            End If

            If strLoanAdvanceTranIDs.Trim <> "" AndAlso strLoanAdvanceTranIDs.Trim <> "0" AndAlso strLoanAdvanceTranIDs.Trim <> "-1" Then
                strB.Append(" AND lnloan_advance_tran.loanadvancetranunkid IN(" & strLoanAdvanceTranIDs & ") ")
                'strFilter &= " AND #TBLnAdv.loanadvancetranunkid IS NOT NULL "
            End If

            If strLoanSchemeIDs.Trim <> "" AndAlso strLoanSchemeIDs.Trim <> "0" AndAlso strLoanSchemeIDs.Trim <> "-1" Then
                strB.Append(" AND lnloan_advance_tran.loanschemeunkid IN (" & strLoanSchemeIDs & ") ")
            End If


            If blnForReport = True Then
                If strExtraReportFilterString.Trim.Length > 0 Then
                    strB.Append(strExtraReportFilterString)
            End If
            End If

            strB.Append("    ) AS A " & _
                             "WHERE A.ROWNO = 1 " & _
                             " " & _
                        "SELECT * INTO    #TableStatus FROM ( " & _
                            "SELECT   #TableLoanAdvance.loanadvancetranunkid  " & _
                                      ", lnloan_status_tran.statusunkid " & _
                                      ", lnloan_status_tran.iscalculateinterest " & _
                                      ", lnloan_status_tran.settle_amount " & _
                                      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS StatusPeriodEndDate " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY CONVERT(CHAR(8),lnloan_status_tran.status_date,112) DESC, lnloan_status_tran.loanstatustranunkid DESC ) AS ROWNO " & _
                               "FROM     #TableLoanAdvance " & _
                                        "LEFT JOIN lnloan_status_tran ON #TableLoanAdvance.loanadvancetranunkid = lnloan_status_tran.loanadvancetranunkid " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_status_tran.periodunkid " & _
                                        "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = #TableLoanAdvance.deductionperiodunkid " & _
                                                                              "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
                               "WHERE    ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
                                        "AND TableDeductionPeriod.start_date <= @AsOnDate " & _
                                        "AND cfcommon_period_tran.start_date <= @AsOnDate " & _
                        ") AS B " & _
                        "WHERE B.ROWNO = 1 " & _
                        " " & _
                        "SELECT * INTO    #TableInterest FROM ( " & _
                            "SELECT  lnloan_interest_tran.loanadvancetranunkid  " & _
                                  ", CONVERT(CHAR(8), lnloan_interest_tran.effectivedate, 112) AS IntRateEffectiveDate " & _
                                  ", lnloan_interest_tran.interest_rate " & _
                                  ", DENSE_RANK() OVER ( PARTITION BY lnloan_interest_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_interest_tran.effectivedate DESC, lnloan_interest_tran.lninteresttranunkid DESC ) AS ROWNO " & _
                            "FROM    #TableLoanAdvance " & _
                                    "LEFT JOIN lnloan_interest_tran ON #TableLoanAdvance.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
                                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
                                                                      "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                            "WHERE   ISNULL(lnloan_interest_tran.isvoid, 0) = 0 " & _
                                                "AND CONVERT(CHAR(8), lnloan_interest_tran.effectivedate, 112) <= @AsOnDate " & _
                        ") AS C " & _
                        "WHERE C.ROWNO = 1 " & _
                        " " & _
                        "SELECT * INTO    #TableTenure FROM ( " & _
                            "SELECT  lnloan_emitenure_tran.loanadvancetranunkid  " & _
                                  ", CONVERT(CHAR(8), lnloan_emitenure_tran.effectivedate, 112) AS TenureEffectiveDate " & _
                                  ", lnloan_emitenure_tran.emi_tenure " & _
                                  ", lnloan_emitenure_tran.basecurrency_amount AS emi_amount " & _
                                  ", lnloan_emitenure_tran.emi_amount AS emi_amountPaidCurrency " & _
                                  ", CONVERT(CHAR(8), lnloan_emitenure_tran.end_date, 112) AS TenureEndDate " & _
                                  ", lnloan_emitenure_tran.principal_amount AS emi_principal_amount " & _
                                  ", DENSE_RANK() OVER ( PARTITION BY lnloan_emitenure_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_emitenure_tran.effectivedate DESC, lnloan_emitenure_tran.lnemitranunkid DESC ) AS ROWNO " & _
                            "FROM    #TableLoanAdvance " & _
                                    "LEFT JOIN lnloan_emitenure_tran ON #TableLoanAdvance.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
                                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
                                                                      "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                            "WHERE   ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 " & _
                                     "AND CONVERT(CHAR(8), lnloan_emitenure_tran.effectivedate, 112) <= @AsOnDate " & _
                        " ) AS D " & _
                        "WHERE D.ROWNO = 1 " & _
                        " " & _
                        "SELECT * INTO    #TableBalance FROM ( " & _
                            "SELECT  lnloan_balance_tran.loanadvancetranunkid  " & _
                                  ", lnloan_balance_tran.loanbalancetranunkid " & _
                                  ", lnloan_balance_tran.cf_balance " & _
                                  ", lnloan_balance_tran.cf_balancePaidCurrency " & _
                                  ", lnloan_balance_tran.bf_amount " & _
                                  ", lnloan_balance_tran.bf_amountPaidCurrency " & _
                                  ", lnloan_balance_tran.cf_amount " & _
                                  ", lnloan_balance_tran.cf_amountPaidCurrency " & _
                                  ", lnloan_balance_tran.nexteffective_days " & _
                                  ", lnloan_balance_tran.nexteffective_months " & _
                                  ", lnloan_balance_tran.transaction_periodunkid " & _
                                  ", lnloan_balance_tran.periodunkid " & _
                                  ", CONVERT(CHAR(8), lnloan_balance_tran.end_date + 1, 112) AS BalanceEffectiveDate " & _
                                  ", DENSE_RANK() OVER ( PARTITION BY lnloan_balance_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_balance_tran.end_date DESC, lnloan_balance_tran.loanbalancetranunkid DESC ) AS ROWNO " _
                                  )
            'Sohail (18 Jan 2019 -- [AND cfcommon_period_tran.start_date <= @AsOnDate   in SELECT * INTO    #TableStatus block]:Issue: 74.1 and 76.1 Reason:Loan was coming with last status, it should pick as on date status
            'Hemant (04 Aug 2018) -- [lnloan_status_tran.settle_amount] = Reason : To Prevent Written-off loan in Current Period

            If blnForReport = True Then
                strB.Append("    , TranPeriod.period_name AS TranPeriodName " & _
                                              ", CONVERT(CHAR(8), TranPeriod.end_date, 112) AS TranPeriod_end_date " & _
                                              ", cfcommon_period_tran.period_name AS PeriodName " & _
                                              ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " _
                                              )
            End If

            strB.Append("    FROM    #TableLoanAdvance " & _
                                    "LEFT JOIN lnloan_balance_tran ON lnloan_balance_tran.loanadvancetranunkid =  #TableLoanAdvance.loanadvancetranunkid " & _
                                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_balance_tran.periodunkid " & _
                                                                      "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                    "LEFT JOIN cfcommon_period_tran AS TranPeriod ON TranPeriod.periodunkid = lnloan_balance_tran.transaction_periodunkid " & _
                                                                      "AND TranPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
                            "WHERE   ISNULL(lnloan_balance_tran.isvoid, 0) = 0 " & _
                            "AND CONVERT(CHAR(8), lnloan_balance_tran.end_date, 112) < @AsOnDate " & _
                        ") AS E " & _
                        "WHERE E.ROWNO = 1 " & _
                        " " & _
                        "SELECT * INTO    #TableMonthlyDeduction FROM ( " & _
                            "SELECT  lnloan_balance_tran.loanadvancetranunkid  " & _
                                  ", SUM(lnloan_balance_tran.amount) AS TotalMonthlyDeduction " & _
                                  ", SUM(lnloan_balance_tran.amountPaidCurrency) AS TotalMonthlyDeductionPaidCurrency " & _
                                  ", SUM(lnloan_balance_tran.interest_amount) AS TotalMonthlyInterest " & _
                                  ", SUM(lnloan_balance_tran.interest_amountPaidCurrency) AS TotalMonthlyInterestPaidCurrency " & _
                                  ", SUM(lnloan_balance_tran.repayment_amount) AS TotalMonthlyRepayment " & _
                                  ", SUM(lnloan_balance_tran.repayment_amountPaidCurrency) AS TotalMonthlyRepaymentPaidCurrency " & _
                                  ", SUM(lnloan_balance_tran.topup_amount) AS TotalMonthlyTopup " & _
                                  ", SUM(lnloan_balance_tran.topup_amountPaidCurrency) AS TotalMonthlyTopupPaidCurrency " & _
                                  ", SUM(lnloan_balance_tran.principal_amount) AS TotalMonthlyPrincipalAmt " & _
                                  ", SUM(lnloan_balance_tran.principal_amountPaidCurrency) AS TotalMonthlyPrincipalAmtPaidCurrency " & _
                                  ", 1 AS ROWNO " & _
                            "FROM    #TableLoanAdvance " & _
                                    "LEFT JOIN lnloan_balance_tran ON lnloan_balance_tran.loanadvancetranunkid =  #TableLoanAdvance.loanadvancetranunkid " & _
                                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_balance_tran.periodunkid " & _
                                                                      "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                            "WHERE   ISNULL(lnloan_balance_tran.isvoid, 0) = 0 " & _
                                    "AND CONVERT(CHAR(8), lnloan_balance_tran.transactiondate, 112) BETWEEN @PeriodStartDate AND @PrevAsOnDate " & _
                            "GROUP BY lnloan_balance_tran.loanadvancetranunkid " & _
                        ") AS F " & _
                    "WHERE F.ROWNO = 1 " & _
                    " " & _
                    "SELECT * INTO    #TableTotalTopup FROM ( " & _
                        "SELECT  #TableLoanAdvance.loanadvancetranunkid  " & _
                              ", ISNULL(SUM(lnloan_topup_tran.topup_amount), 0) AS total_topup " & _
                              ", ISNULL(SUM(lnloan_topup_tran.basecurrency_amount), 0) AS total_topupPaidCurrency " & _
                              "/*, DENSE_RANK() OVER ( PARTITION BY lnloan_topup_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_topup_tran.effectivedate DESC, lnloan_emitenure_tran.lnemitranunkid DESC ) AS ROWNO*/ " & _
                        "FROM    #TableLoanAdvance " & _
                                "LEFT JOIN lnloan_topup_tran ON #TableLoanAdvance.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_topup_tran.periodunkid " & _
                                                                  "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                        "WHERE   ISNULL(lnloan_topup_tran.isvoid, 0) = 0 " & _
                                 "/*AND CONVERT(CHAR(8), lnloan_topup_tran.effectivedate, 112) <= @AsOnDate*/ " & _
                        "GROUP BY #TableLoanAdvance.loanadvancetranunkid  " & _
                    " ) AS G " & _
                    "/*WHERE G.ROWNO = 1*/ " _
                    )
            'Sohail (03 Feb 2017) - [table G]

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If blnIncludeNoInterestMappedHeadCalcType = True Then

                strB.Append("SELECT prpayrollprocess_tran.loanadvancetranunkid " & _
                                ", prpayrollprocess_tran.employeeunkid " & _
                                ", prpayrollprocess_tran.amount " & _
                                ", prpayrollprocess_tran.amountpaidcurrency " & _
                           "INTO #MappedHead " & _
                           "FROM prpayrollprocess_tran " & _
                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                               "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                           "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                 "AND prtnaleave_tran.isvoid = 0 " & _
                                 "AND cfcommon_period_tran.modulerefid = 1 " & _
                                 "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                 "AND lnloan_advance_tran.calctype_id = " & enLoanCalcId.No_Interest_With_Mapped_Head & " " & _
                                 "AND cfcommon_period_tran.start_date = @PeriodStartDate " _
                            )

            End If
            'Sohail (29 Apr 2019) -- End



            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim ds As DataSet = GetLoanCalculationTypeList("LoanCalcType", True, True)
            Dim ds As DataSet = GetLoanCalculationTypeList("LoanCalcType", True, True, objDataOperation)
            'Sohail (03 May 2018) -- End
            Dim dicLoanCalcType As Dictionary(Of Integer, String) = (From p In ds.Tables("LoanCalcType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'ds = GetLoan_Interest_Calculation_Type("LoanIntCalcType", True, True)
            ds = GetLoan_Interest_Calculation_Type("LoanIntCalcType", True, True, , , objDataOperation)
            'Sohail (03 May 2018) -- End
            Dim dicLoanIntCalcType As Dictionary(Of Integer, String) = (From p In ds.Tables("LoanIntCalcType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            If dtPeriodStartDate <> Nothing Then
                objDataOperation.AddParameter("@PeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStartDate))
                objDataOperation.AddParameter("@PrevPeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStartDate.AddDays(-1)))
            Else
                objDataOperation.AddParameter("@PeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
            End If

            '*******************************************************************************************************************
            '*** If you change any field name then please change those field name in [Loan Advance Saving -> Generate_LoanDetailReport_New] Report without fail ***
            '*******************************************************************************************************************


            strB.Append("SELECT   Loan.loanadvancetranunkid  " & _
                          ", Loan.loanschemeunkid " & _
                          ", Loan.loaneffectivedate " & _
                          ", ISNULL(Balance.loanbalancetranunkid, Loan.loanadvancetranunkid) AS loanbalancetranunkid " & _
                          ", ISNULL(Tenure.emi_amount, Loan.emi_amount) AS emi_amount " & _
                          ", ISNULL(Tenure.emi_amountPaidCurrency, Loan.emi_amountPaidCurrency) AS emi_amountPaidCurrency " & _
                          ", ISNULL(Tenure.emi_principal_amount, Loan.emi_amount) AS emi_principal_amount " & _
                          ", Loan.basecurrency_amount AS advance_amount " & _
                          ", Loan.isloan " & _
                          ", Loan.balance_amount " & _
                          ", Loan.balance_amountPaidCurrency " & _
                          ", Loan.opening_balance " & _
                          ", Loan.opening_balancePaidCurrency " & _
                          ", Loan.calctype_id " & _
                          ", Loan.basecurrency_amount AS loan_amount " & _
                          ", Loan.loan_advance_amount AS loan_advance_amountPaidCurrency " & _
                          ", Loan.countryunkid " & _
                          ", ISNULL(ExRate.exchange_rate, Loan.exchange_rate) AS exchange_rate " & _
                          ", ISNULL(Currency.currency_sign, '') AS currency_sign " & _
                          ", Status.statusunkid " & _
                          ", Status.StatusPeriodEndDate " & _
                          ", CAST(CASE Status.statusunkid WHEN 2 THEN 1 ELSE 0 END AS BIT) AS isonhold " & _
                          ", Status.iscalculateinterest " & _
                          ", ExRate.exchangerateunkid AS currencyunkid " & _
                          ", ISNULL(IntRate.IntRateEffectiveDate, Loan.loaneffectivedate) AS IntRateEffectiveDate " & _
                          ", ISNULL(IntRate.interest_rate, Loan.interest_rate) AS interest_rate " & _
                          ", ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate) AS TenureEffectiveDate " & _
                          ", ISNULL(Tenure.emi_tenure, Loan.emi_tenure) AS emi_tenure " & _
                          ", ISNULL(DATEDIFF(MONTH, CurrMonth.end_date, Tenure.TenureEndDate), 0) AS Remaining_EMI " & _
                          ", ISNULL(Balance.bf_amount, Loan.basecurrency_amount) AS BF_Amount " & _
                          ", ISNULL(Balance.bf_amountPaidCurrency, Loan.loan_advance_amount) AS BF_AmountPaidCurrency " & _
                          ", ISNULL(Balance.cf_amount, Loan.basecurrency_amount) AS PrincipalBalance " & _
                          ", ISNULL(Balance.cf_amountPaidCurrency, Loan.loan_advance_amount) AS PrincipalBalancePaidCurrency " & _
                          ", ISNULL(Balance.cf_balance, CASE Loan.isbrought_forward WHEN 0 THEN Loan.basecurrency_amount ELSE Loan.opening_balance END) AS BalanceAmount " & _
                          ", ISNULL(Balance.cf_balancePaidCurrency, CASE Loan.isbrought_forward WHEN 0 THEN Loan.loan_advance_amount  ELSE Loan.opening_balancePaidCurrency END) AS BalanceAmountPaidCurrency " & _
                          ", ISNULL(Balance.BalanceEffectiveDate, Loan.loaneffectivedate) AS BalanceEffectiveDate " & _
                          ", ISNULL(Tenure.TenureEndDate, DATEADD(DAY, -1, DATEADD(MONTH, Tenure.emi_tenure, Tenure.TenureEffectiveDate))) AS LoanEndDate " & _
                          ", DATEDIFF(DAY, ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate), ISNULL(Tenure.TenureEndDate, CONVERT(CHAR(8), DATEADD(DAY, -1, DATEADD(MONTH, ISNULL(Tenure.emi_tenure, Loan.emi_tenure), ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate))), 112))) + 1 AS TenureDays " & _
                          ", ISNULL(Balance.nexteffective_days, DATEDIFF(DAY, ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate), ISNULL(Tenure.TenureEndDate, CONVERT(CHAR(8), DATEADD(DAY, -1, DATEADD(MONTH, ISNULL(Tenure.emi_tenure, Loan.emi_tenure), ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate))), 112))) + 1) AS nexteffective_days " & _
                          ", ISNULL(Balance.nexteffective_months, DATEDIFF(MONTH, ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate), ISNULL(Tenure.TenureEndDate, CONVERT(CHAR(8), DATEADD(DAY, -1, DATEADD(MONTH, ISNULL(Tenure.emi_tenure, Loan.emi_tenure), ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate))), 112))) + 1) AS nexteffective_months " & _
                          ", 0 AS DaysDiff " & _
                          ", 0.00 AS TotPMTAmount " & _
                          ", 0.00 AS TotPMTAmountPaidCurrency " & _
                          ", 0.00 AS TotInterestAmount " & _
                          ", 0.00 AS TotInterestAmountPaidCurrency " & _
                          ", 0.00 AS TotPrincipalAmount " & _
                          ", 0.00 AS TotPrincipalAmountPaidCurrency " & _
                          ", 0.00 AS LastProjectedBalance " & _
                          ", 0.00 AS LastProjectedBalancePaidCurrency " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyDeduction, 0) AS TotalMonthlyDeduction " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyDeductionPaidCurrency, 0) AS TotalMonthlyDeductionPaidCurrency " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyInterest, 0) AS TotalMonthlyInterest " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyInterestPaidCurrency, 0) AS TotalMonthlyInterestPaidCurrency " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyRepayment, 0) AS TotalMonthlyRepayment " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyRepaymentPaidCurrency, 0) AS TotalMonthlyRepaymentPaidCurrency " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyTopup, 0) AS TotalMonthlyTopup " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyTopupPaidCurrency, 0) AS TotalMonthlyTopupPaidCurrency " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyPrincipalAmt, 0) AS TotalMonthlyPrincipalAmt " & _
                          ", ISNULL(MonthlyDeduction.TotalMonthlyPrincipalAmtPaidCurrency, 0) AS TotalMonthlyPrincipalAmtPaidCurrency " & _
                          ", Loan.employeeunkid " & _
                          ", ISNULL(Balance.transaction_periodunkid,Loan.deductionperiodunkid) AS transactionperiodunkid " & _
                          ", ISNULL(Balance.periodunkid,Loan.deductionperiodunkid) AS periodunkid " & _
                          ", Loan.interest_calctype_id " & _
                          ", Loan.loanschemeinterestunkid " & _
                                ", Loan.loan_statusunkid " & _
                                ", ISNULL(TotalTopup.total_topup, 0) AS total_topup " & _
                          ", ISNULL(TotalTopup.total_topupPaidCurrency, 0) AS total_topupPaidCurrency " & _
                          ", Loan.mapped_tranheadunkid " _
                                )
            'Sohail (31 Jan 2020) - Telematics Africal LTD Issue # 0004475: Loan Top ups entered in January have double effect on Loan amount. (Removed from BalanceAmount and BalanceAmountPaidCurrency due to topup was getting counted twice=[+ ISNULL(MonthlyDeduction.TotalMonthlyTopup, 0)], [+ ISNULL(MonthlyDeduction.TotalMonthlyTopupPaidCurrency, 0)])
            'Sohail (07 Jan 2020) - [Le Grand Cassino issue : loan balance mismatch issue on close period when topup is taken on last emi period.] ["+ ISNULL(MonthlyDeduction.TotalMonthlyTopup, 0)" AS BalanceAmount]
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (02 Apr 2018) - [loanschemeinterestunkid]
            'Sohail (07 Sep 2017) - [TotalTopup.total_topup = ISNULL(TotalTopup.total_topup, 0) AS total_topup]
            'Nilay (08 Apr 2017) -- [Remaining_EMI]

            'Sohail (03 Feb 2017) - [total_topup, total_topupPaidCurrency]

            If blnForReport = True Then
                strB.Append("   , ISNULL(Balance.TranPeriodName, Loan.PeriodName) AS TranPeriodName " & _
                          ", ISNULL(Balance.PeriodName, Loan.PeriodName) AS PeriodName " & _
                          ", ISNULL(CurrMonth.periodunkid, 0) AS CurrMonthPeriodUnkId " & _
                          ", ISNULL(CurrMonth.period_name, '') AS CurrMonthPeriodName " & _
                          ", ISNULL(Loan.PeriodName, '') AS DeductionPeriodName " & _
                          ", Loan.loanvoucher_no " & _
                          ", Loan.LoanSchemeCode " & _
                          ", Loan.LoanSchemeName " & _
                          ", Loan.EmpName " & _
                          ", Loan.EmpCode " & _
                          ", ISNULL(Balance.TranPeriod_end_date, Loan.end_date) AS TranPeriodend_date  " & _
                          ", ISNULL(CurrMonth.start_date, Loan.loaneffectivedate) AS CurrMonthPeriod_start_date  " & _
                          ", ISNULL(CurrMonth.end_date, Loan.end_date) AS CurrMonthPeriod_end_date  " & _
                          ", ISNULL(Balance.end_date, Loan.end_date) AS end_date  " & _
                          ", ISNULL(M.membershipno, '') AS MembershipNo " & _
                          ", ISNULL(prescribed_interest_rate, 0) AS prescribed_interest_rate " & _
                          ", ISNULL(Loan.isshowloanbalonpayslip, 0) AS isshowloanbalonpayslip " & _
                          ", Loan.loan_exchange_rate AS LoanExchangeRate " & _
                               ", Loan.net_amount " & _
                               ", CASE Loan.calctype_id " _
                               )

                For Each pair In dicLoanCalcType
                    strB.Append(" WHEN " & pair.Key & "  THEN '" & pair.Value & "' ")
                Next

                strB.Append("   END AS calctype " & _
                               ", CASE Loan.interest_calctype_id " _
                            )

                For Each pair In dicLoanIntCalcType
                    strB.Append(" WHEN " & pair.Key & "  THEN '" & pair.Value & "' ")
                Next

                strB.Append(" END AS interest_calctype ")
                'Hemant (02 Jan 2019) -- Start
                'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
                strB.Append(", loan_account_no ")
                'Hemant (02 Jan 2019) -- End

            End If

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If blnIncludeNoInterestMappedHeadCalcType = True Then
                strB.Append(", ISNULL(MappedHead.amount, 0) AS MappedHeadAmount " & _
                            ", ISNULL(MappedHead.amountPaidCurrency, 0) AS MappedHeadAmountPaidCurrency " _
                          )
            End If
            'Sohail (29 Apr 2019) -- End

            strB.Append("FROM    ( SELECT    #TableLoanAdvance.loanadvancetranunkid  " & _
                                          ", #TableLoanAdvance.emi_amount " & _
                                          ", #TableLoanAdvance.emi_amountPaidCurrency " & _
                                          ", #TableLoanAdvance.advance_amount " & _
                                          ", #TableLoanAdvance.isloan " & _
                                          ", #TableLoanAdvance.balance_amount " & _
                                          ", #TableLoanAdvance.balance_amountPaidCurrency " & _
                                          ", #TableLoanAdvance.loan_amount " & _
                                          ", #TableLoanAdvance.opening_balance " & _
                                          ", #TableLoanAdvance.opening_balancePaidCurrency " & _
                                          ", #TableLoanAdvance.loan_advance_amount " & _
                                          ", #TableLoanAdvance.calctype_id " & _
                                          ", #TableLoanAdvance.basecurrency_amount " & _
                                          ", #TableLoanAdvance.loaneffectivedate " & _
                                          ", #TableLoanAdvance.loanschemeunkid " & _
                                          ", #TableLoanAdvance.employeeunkid " & _
                                          ", #TableLoanAdvance.interest_rate " & _
                                          ", #TableLoanAdvance.emi_tenure " & _
                                          ", #TableLoanAdvance.countryunkid " & _
                                          ", #TableLoanAdvance.exchange_rate " & _
                                          ", #TableLoanAdvance.deductionperiodunkid " & _
                                          ", #TableLoanAdvance.isbrought_forward " & _
                                          ", #TableLoanAdvance.prescribed_interest_rate " & _
                                          ", #TableLoanAdvance.interest_calctype_id " & _
                                          ", #TableLoanAdvance.loan_statusunkid " & _
                                          ", #TableLoanAdvance.end_date  " & _
                                          ", #TableLoanAdvance.loanschemeinterestunkid  " & _
                                          ", #TableLoanAdvance.mapped_tranheadunkid " & _
                                          ", #TableLoanAdvance.ROWNO " _
                                          )
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (02 Apr 2018) - [loanschemeinterestunkid]

            If blnForReport = True Then

                strB.Append("             , #TableLoanAdvance.PeriodName " & _
                                          ", #TableLoanAdvance.loanvoucher_no " & _
                                      ", lnloan_scheme_master.code AS LoanSchemeCode " & _
                                      ", lnloan_scheme_master.name AS LoanSchemeName  " & _
                                          ", #TableLoanAdvance.EmpCode  " & _
                                      ", ISNULL(lnloan_scheme_master.isshowloanbalonpayslip, 0) AS isshowloanbalonpayslip " & _
                                          ", #TableLoanAdvance.loan_exchange_rate " & _
                                          ", #TableLoanAdvance.net_amount " & _
                                          ", #TableLoanAdvance.EmpName " _
                                          )


            End If

            strB.Append("          FROM      #TableLoanAdvance ")


            If blnForReport = True Then

                strB.Append("LEFT JOIN lnloan_scheme_master ON #TableLoanAdvance.loanschemeunkid = lnloan_scheme_master.loanschemeunkid ")

            Else
                'strQ &= "                    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_advance_tran.employeeunkid "

            End If

            strB.Append("          WHERE     1 = 1 " & _
                            ") AS Loan " & _
                            "JOIN ( SELECT   #TableStatus.loanadvancetranunkid  " & _
                                          ", #TableStatus.statusunkid " & _
                                          ", #TableStatus.iscalculateinterest " & _
                                          ", #TableStatus.settle_amount  " & _
                                          ", #TableStatus.StatusPeriodEndDate " & _
                                   "FROM     #TableStatus " & _
                                   "WHERE    1 = 1 " & _
                                 ") AS Status ON Loan.loanadvancetranunkid = Status.loanadvancetranunkid " & _
                            "JOIN ( SELECT   #TableLoanAdvance.loanadvancetranunkid  " & _
                                          ", ISNULL(cfexchange_rate.exchange_rate, #TableLoanAdvance.exchange_rate) AS exchange_rate " & _
                                          ", ISNULL(cfexchange_rate.exchangerateunkid, 1) AS exchangerateunkid " & _
                                          ", ISNULL(lnloan_process_pending_loan.loan_account_no,'') As loan_account_no " & _
                                          ", DENSE_RANK() OVER ( PARTITION BY #TableLoanAdvance.loanadvancetranunkid ORDER BY exchange_date DESC, exchangerateunkid DESC ) AS ROWNO " & _
                                   "FROM     #TableLoanAdvance " & _
                                            "LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = #TableLoanAdvance.processpendingloanunkid " & _
                                            "LEFT JOIN cfexchange_rate ON cfexchange_rate.countryunkid = #TableLoanAdvance.countryunkid " & _
                                            "LEFT JOIN prpayment_tran ON #TableLoanAdvance.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                                                                        "AND prpayment_tran.isvoid = 0 " & _
                                                                        "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                                                                        "AND prpayment_tran.referenceid IN ( " & clsPayment_tran.enPaymentRefId.LOAN & ", " & clsPayment_tran.enPaymentRefId.ADVANCE & " ) " & _
                                            "WHERE    ISNULL(lnloan_process_pending_loan.isvoid, 0) = 0 " & _
                                            "AND cfexchange_rate.isactive = 1 " _
                                            )
            'Hemant (02 Jan 2019) - [loan_account_no]
            'Hemant (04 Aug 2018) -- [#TableStatus.settle_amount] = Reason :CCK # 0002584 To Prevent Written-off loan in Current Period
            If blnIncludeUnPaidLoan = False Then
                strB.Append("                AND ( prpayment_tran.paymenttranunkid IS NOT NULL " & _
                                                  "OR lnloan_process_pending_loan.isexternal_entity = 1 " & _
                                                 ") " _
                                                 )
            End If

            strB.Append("              ) AS ExRate ON Loan.loanadvancetranunkid = ExRate.loanadvancetranunkid " & _
                                        "LEFT JOIN ( SELECT  #TableInterest.loanadvancetranunkid  " & _
                                                          ", #TableInterest.IntRateEffectiveDate " & _
                                                          ", #TableInterest.interest_rate " & _
                                                    "FROM    #TableInterest " & _
                                                    "WHERE   1 = 1 " & _
                                      ") AS IntRate ON Loan.loanadvancetranunkid = IntRate.loanadvancetranunkid " & _
                                        "LEFT JOIN ( SELECT  #TableTenure.loanadvancetranunkid  " & _
                                                          ", #TableTenure.TenureEffectiveDate " & _
                                                          ", #TableTenure.emi_tenure " & _
                                                          ", #TableTenure.emi_amount " & _
                                                          ", #TableTenure.emi_amountPaidCurrency " & _
                                                          ", #TableTenure.TenureEndDate " & _
                                                          ", #TableTenure.emi_principal_amount " & _
                                                    "FROM    #TableTenure " & _
                                                    "WHERE   1 = 1 " & _
                                      ") AS Tenure ON Loan.loanadvancetranunkid = Tenure.loanadvancetranunkid " & _
                                        "LEFT JOIN ( SELECT  #TableBalance.loanadvancetranunkid  " & _
                                                          ", #TableBalance.loanbalancetranunkid " & _
                                                          ", #TableBalance.cf_balance " & _
                                                          ", #TableBalance.cf_balancePaidCurrency " & _
                                                          ", #TableBalance.bf_amount " & _
                                                          ", #TableBalance.bf_amountPaidCurrency " & _
                                                          ", #TableBalance.cf_amount " & _
                                                          ", #TableBalance.cf_amountPaidCurrency " & _
                                                          ", #TableBalance.nexteffective_days " & _
                                                          ", #TableBalance.nexteffective_months " & _
                                                          ", #TableBalance.transaction_periodunkid " & _
                                                          ", #TableBalance.periodunkid " & _
                                                          ", #TableBalance.BalanceEffectiveDate " _
                                                          )

            If blnForReport = True Then
                strB.Append("                              , #TableBalance.TranPeriodName " & _
                                                           ", #TableBalance.TranPeriod_end_date " & _
                                                           ", #TableBalance.PeriodName " & _
                                                           ", #TableBalance.end_date " _
                                                           )
            End If

            strB.Append("                           FROM    #TableBalance " & _
                                                    "WHERE   1 = 1 " _
                                                    )

            strB.Append(") AS Balance ON Loan.loanadvancetranunkid = Balance.loanadvancetranunkid " & _
                             "LEFT JOIN ( SELECT  #TableMonthlyDeduction.loanadvancetranunkid  " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyDeduction " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyDeductionPaidCurrency " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyInterest " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyInterestPaidCurrency " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyRepayment " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyRepaymentPaidCurrency " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyTopup " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyTopupPaidCurrency " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyPrincipalAmt " & _
                                               ", #TableMonthlyDeduction.TotalMonthlyPrincipalAmtPaidCurrency " & _
                                               ", #TableMonthlyDeduction.ROWNO " & _
                                         "FROM    #TableMonthlyDeduction " & _
                                         "WHERE   1 = 1 " & _
                                      ") AS MonthlyDeduction ON Loan.loanadvancetranunkid = MonthlyDeduction.loanadvancetranunkid " & _
                            "LEFT JOIN ( SELECT DISTINCT " & _
                                                "cfexchange_rate.countryunkid  " & _
                                              ", cfexchange_rate.currency_name " & _
                                              ", cfexchange_rate.currency_sign " & _
                                        "FROM    cfexchange_rate " & _
                                        "WHERE   cfexchange_rate.isactive = 1 " & _
                                      ") AS Currency ON Loan.countryunkid = Currency.countryunkid " & _
                            "LEFT JOIN ( SELECT TOP 1 " & _
                                                "cfcommon_period_tran.periodunkid  " & _
                                              ", cfcommon_period_tran.period_code " & _
                                              ", cfcommon_period_tran.period_name " & _
                                              ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                                              ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                                        "FROM    cfcommon_period_tran " & _
                                        "WHERE   cfcommon_period_tran.isactive = 1 " & _
                                                "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) = @PeriodStartDate " & _
                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) = @PrevAsOnDate " & _
                                       ") AS CurrMonth ON 1 = 1 " & _
                             "LEFT JOIN ( SELECT  employeeunkid " & _
                            "                     , membershipno = ISNULL(STUFF(( SELECT '; ' + membershipno " & _
                                         "FROM   #TableEmp " & _
                                                 "LEFT JOIN hremployee_meminfo_tran ON #TableEmp.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                            "                                                     WHERE membershipno IS NOT NULL " & _
                            "                                                           AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                 "AND #TableEmp.employeeunkid = a.employeeunkid " & _
                                         "FOR XML PATH('')), 1, 2, ''), '') " & _
                                             "FROM #TableEmp AS a " & _
                            "               GROUP BY employeeunkid " & _
                                     ") AS M ON M.employeeunkid = Loan.employeeunkid " & _
                            "LEFT JOIN ( SELECT  #TableTotalTopup.loanadvancetranunkid  " & _
                                              ", #TableTotalTopup.total_topup " & _
                                              ", #TableTotalTopup.total_topupPaidCurrency " & _
                                        "FROM    #TableTotalTopup " & _
                                        "WHERE   1 = 1 " & _
                                      ") AS TotalTopup ON Loan.loanadvancetranunkid = TotalTopup.loanadvancetranunkid " _
                                     )
            'Sohail (03 Feb 2017) - [table TotalTopup]

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If blnIncludeNoInterestMappedHeadCalcType = True Then
                strB.Append("LEFT JOIN ( SELECT   #MappedHead.loanadvancetranunkid  " & _
                                                ", #MappedHead.amount " & _
                                                ", #MappedHead.amountpaidcurrency " & _
                                         "FROM     #MappedHead " & _
                                         "WHERE    1 = 1 " & _
                                       ") AS MappedHead ON Loan.loanadvancetranunkid = MappedHead.loanadvancetranunkid " _
                            )
            End If
            'Sohail (29 Apr 2019) -- End

            'Sohail (26 Jul 2017) - Start
            'TRA Issue - Loans were not coming if payment is done and last EMI of loan and loan status is completed by doing payment but loan was deducted.
            'strB.Append("WHERE   1 = 1 AND ExRate.ROWNO = 1 AND NOT (Status.statusunkid IN (" & enLoanStatus.WRITTEN_OFF & ", " & enLoanStatus.COMPLETED & ") AND ISNULL(CurrMonth.end_date, Loan.end_date) > Status.StatusPeriodEndDate ) ")
            strB.Append("WHERE   1 = 1 AND ExRate.ROWNO = 1 AND NOT (Status.statusunkid IN (" & enLoanStatus.WRITTEN_OFF & ") AND ISNULL(CurrMonth.end_date, Loan.end_date) > Status.StatusPeriodEndDate ) ")
            'Sohail (26 Jul 2017) - End

            If blnIncludeOnHoldWrittenOff = False Then
                strB.Append(" AND ((Status.statusunkid = " & enLoanStatus.IN_PROGRESS & ") OR (Status.statusunkid = " & enLoanStatus.ON_HOLD & " AND Status.iscalculateinterest = 1 )) ")
            End If

            If blnIncludeCompletedLoanAdvance = False Then
                strB.Append(" AND (ISNULL(Balance.cf_amount, Loan.loan_amount) > 0 OR Loan.isloan = 0) AND NOT (Status.statusunkid = " & enLoanStatus.COMPLETED & " OR Status.statusunkid = " & enLoanStatus.WRITTEN_OFF & " ) ")
            End If

            If intLoanStatus > 0 Then
                If intLoanStatus <> enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED Then
                    strB.Append(" AND Status.statusunkid = " & intLoanStatus & " ")
                    'Sohail (26 Jul 2017) - Start
                    'TRA Issue - Loans were not coming if payment is done and last EMI of loan and loan status is completed by doing payment but loan was deducted.
                    strB.Append(" AND NOT (Status.statusunkid IN (" & enLoanStatus.COMPLETED & ")) ")
                    'Sohail (26 Jul 2017) - End
                Else
                    'strQ &= " AND (Status.statusunkid = " & enLoanStatus.IN_PROGRESS & ") "
                    'Sohail (26 Jul 2017) - Start
                    'TRA Issue - Loans were not coming if payment is done and last EMI of loan and loan status is completed by doing payment but loan was deducted.
                    'Hemant (04 Aug 2018) -- Start
                    'Issue : CCK - Written-off Loan in Current Period were coming on Loan Report and ONHOLD Loan set in Previous Period were not coming in Current Period
                    'strB.Append(" AND (Status.statusunkid IN (" & enLoanStatus.IN_PROGRESS & ") OR Status.StatusPeriodEndDate = @PrevAsOnDate) ")
                    strB.Append(" AND (Status.statusunkid IN (" & enLoanStatus.IN_PROGRESS & ") " & _
                                " OR (Status.statusunkid IN (" & CInt(enLoanStatus.COMPLETED) & ") AND Status.settle_amount = 0 AND Status.StatusPeriodEndDate = @PrevAsOnDate) " & _
                                " OR (IntRate.loanadvancetranunkid IS NOT NULL AND Status.statusunkid = " & CInt(enLoanStatus.ON_HOLD) & " AND Status.iscalculateinterest = 1) " & _
                                " ) ")
                    'Hemant (04 Aug 2018) -- End
                    'Sohail (26 Jul 2017) - End
                End If
            End If


            objDataOperation.AddParameter("@AsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            objDataOperation.AddParameter("@PrevAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate.AddDays(-1)))
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If intLoanAdvanceUnkId > 0 Then
                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceUnkId)
            End If
            'Sohail (03 May 2018) -- End

            strB.Append("DROP TABLE #TableEmp " & _
                       " DROP TABLE #TableLoanAdvance " & _
                       " DROP TABLE #TableStatus " & _
                       " DROP TABLE #TableInterest " & _
                       " DROP TABLE #TableTenure " & _
                       " DROP TABLE #TableBalance " & _
                       " DROP TABLE #TableMonthlyDeduction " & _
                       " DROP TABLE #TableTotalTopup " _
                       )
            'Sohail (03 Feb 2017) - [TableTotalTopup]

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If blnIncludeNoInterestMappedHeadCalcType Then
                strB.Append("DROP TABLE #MappedHead " _
                            )
            End If
            'Sohail (29 Apr 2019) -- End

            dsList = objDataOperation.ExecQuery(strB.ToString, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTemp As DataTable = dsList.Tables(0).Clone

            For Each dsRow As DataRow In dsList.Tables(strTableName).Rows

                With dsRow
                    Dim decPrincipalBalance As Decimal = CDec(.Item("PrincipalBalance").ToString) 'Amount on which PMT will be calculated for all type loan
                    Dim decPrincipalBalancePaidCurrency As Decimal = CDec(.Item("PrincipalBalancePaidCurrency").ToString) 'Amount on which PMT will be calculated for all type loan
                    Dim decCFBalance As Decimal = CDec(.Item("BalanceAmount").ToString) 'Latest CF Balance on which last projected amount will be calculated
                    Dim decCFBalancePaidCurrency As Decimal = CDec(.Item("BalanceAmountPaidCurrency").ToString) 'Latest CF Balance on which last projected amount will be calculated
                    Dim decBFAmount As Decimal = CDec(.Item("BF_Amount").ToString) 'Amount on which interest will be calculated for simple interest
                    Dim decBFAmountPaidCurrency As Decimal = CDec(.Item("BF_AmountPaidCurrency").ToString) 'Amount on which interest will be calculated for simple interest
                    Dim intNextEffectiveDays As Integer = CInt(.Item("nexteffective_days").ToString)
                    Dim intNextEffectiveMonths As Integer = CInt(.Item("nexteffective_months").ToString) 'Sohail (15 Dec 2015)
                    Dim decIntRate As Decimal = CDec(.Item("interest_rate").ToString)
                    Dim decExRate As Decimal = CDec(.Item("exchange_rate").ToString)
                    Dim intDaysDiff As Integer = DateDiff(DateInterval.Day, eZeeDate.convertDate(.Item("BalanceEffectiveDate").ToString), dtAsOnDate.AddDays(-1)) + 1
                    Dim intMonthsDiff As Integer = DateDiff(DateInterval.Month, eZeeDate.convertDate(.Item("BalanceEffectiveDate").ToString), dtAsOnDate.AddDays(-1)) + 1
                    'Hemant (25 Feb 2019) -- Start
                    'Issue PACRA #3524: Not able to Void The payroll Due to error Attempted to Divide by Zero .
                    'Hemant (26 Feb 2018) -- Start
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    If blnForReport = True Then
                        'Hemant (26 Feb 2018) -- End
                    strLoanVoucherNo = (.Item("loanvoucher_no").ToString)
                    End If
                    'Hemant (25 Feb 2019) -- End
                    'Sohail (29 Apr 2017) -- Start
                    'Issue - 66.1 - status should be piccked up from status tran table.
                    'Dim intLoanAdvanceStatus As Integer = CInt(.Item("loan_statusunkid").ToString) 'Sohail (13 Jan 2016)
                    Dim intLoanAdvanceStatus As Integer = CInt(.Item("statusunkid").ToString)
                    'Sohail (29 Apr 2017) -- End
                    Dim dtLoanEndDate As Date
                    If IsDBNull(.Item("loanenddate")) = True Then
                        dtLoanEndDate = dtAsOnDate.AddDays(-1)
                    Else
                        dtLoanEndDate = eZeeDate.convertDate(.Item("loanenddate").ToString)
                    End If
                    'Sohail (14 Mar 2017) -- Start
                    'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                    Dim intTenure As Integer = CInt(.Item("emi_tenure").ToString)
                    'Sohail (14 Mar 2017) -- End

                    mdecTotPMTAmt = 0
                    mdecTotPMTAmtPaidCurrency = 0
                    mdecTotIntAmt = 0
                    mdecTotIntAmtPaidCurrency = 0
                    mdecTotPrincipalAmt = 0
                    mdecTotPrincipalAmtPaidCurrency = 0



                    If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD Then
                        dtLoanEndDate = dtLoanEndDate.AddDays(1).AddMonths(1).AddDays(-1)
                    End If

                    Dim dtStart As Date = dtAsOnDate
                    Dim intMonth As Integer = 0
                    If blnAddProjectedPeriodDeductions = False Then
                        dtLoanEndDate = dtStart
                        'Sohail (04 Jul 2020) -- Start
                        'Le grande casino issue # 0004779 : Loans in progress are not getting deducted in payroll.
                    Else
                        If blnFromProcessPayroll = True AndAlso dtLoanEndDate < dtStart Then
                            dtLoanEndDate = dtStart
                        End If
                        'Sohail (04 Jul 2020) -- End
                    End If

                    While dtStart.AddDays(-1) <= dtLoanEndDate

                        intMonth += 1
                        If intMonth > 1 Then
                            mdecTotPMTAmt = 0
                            mdecTotPMTAmtPaidCurrency = 0
                            mdecTotIntAmt = 0
                            mdecTotIntAmtPaidCurrency = 0
                            mdecTotPrincipalAmt = 0
                            mdecTotPrincipalAmtPaidCurrency = 0
                            'mdecLastProjectedBalance = 0
                            'mdecLastProjectedBalancePaidCurrency = 0

                            intDaysDiff = DateDiff(DateInterval.Day, dtStart.AddMonths(-1), dtStart.AddDays(-1)) + 1
                            intMonthsDiff = DateDiff(DateInterval.Month, dtStart.AddMonths(-1), dtStart.AddDays(-1)) + 1

                            decCFBalance = mdecLastProjectedBalance
                            decCFBalancePaidCurrency = mdecLastProjectedBalancePaidCurrency
                        End If


                    Select Case CInt(.Item("calctype_id"))
                        Case enLoanCalcId.Simple_Interest
                                If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False AndAlso intMonth <= 1 Then
                                mdecTotPrincipalAmt = 0
                                mdecTotPrincipalAmtPaidCurrency = 0
                                mdecTotIntAmt = 0
                                mdecTotIntAmtPaidCurrency = 0
                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
                                ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True AndAlso intMonth <= 1 Then
                                mdecTotPrincipalAmt = 0
                                mdecTotPrincipalAmtPaidCurrency = 0
                                Select Case CInt(.Item("interest_calctype_id"))

                                    Case enLoanInterestCalcType.DAILY
                                            'Sohail (01 Nov 2017) -- Start
                                            'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                                            'mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
                                            'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)
                                            mdecTotIntAmt = ((decPrincipalBalancePaidCurrency * decIntRate * intDaysDiff) / (36500)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
                                            mdecTotIntAmtPaidCurrency = (decPrincipalBalancePaidCurrency * decIntRate * intDaysDiff) / (36500)
                                            'Sohail (01 Nov 2017) -- End

                                    Case enLoanInterestCalcType.MONTHLY
                                            'Sohail (01 Nov 2017) -- Start
                                            'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                                            'mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (1200)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
                                            'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (1200)
                                            mdecTotIntAmt = ((decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (1200)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
                                            mdecTotIntAmtPaidCurrency = (decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (1200)
                                            'Sohail (01 Nov 2017) -- End

                                            'Sohail (14 Mar 2017) -- Start
                                            'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                                        Case enLoanInterestCalcType.BY_TENURE
                                            'Sohail (01 Nov 2017) -- Start
                                            'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                                            'mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (intTenure * 100)) / decExRate
                                            'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (intTenure * 100)
                                            mdecTotIntAmt = ((decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (intTenure * 100)) / decExRate
                                            mdecTotIntAmtPaidCurrency = (decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (intTenure * 100)
                                            'Sohail (01 Nov 2017) -- End
                                            'Sohail (14 Mar 2017) -- End

                                            'Sohail (23 May 2017) -- Start
                                            'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                                        Case enLoanInterestCalcType.SIMPLE_INTEREST
                                            'Sohail (01 Nov 2017) -- Start
                                            'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                                            'mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (100)) / decExRate
                                            'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (100)
                                            mdecTotIntAmt = ((decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (100)) / decExRate
                                            mdecTotIntAmtPaidCurrency = (decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (100)
                                            'Sohail (01 Nov 2017) -- End
                                            'Sohail (23 May 2017) -- End

                                End Select

                                    'Hemant (03 Oct 2018)  -- Start
                                    'Enhancement : CCK # 0002584 Credit and Debit Amounts Not Balancing on Journal                                    
                                    mdecTotIntAmt = Format(mdecTotIntAmt, strFmtCurrency)
                                    mdecTotIntAmtPaidCurrency = Format(mdecTotIntAmtPaidCurrency, strFmtCurrency)
                                    'Hemant (03 Oct 2018)  -- End
                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
                            Else
                                Select Case CInt(.Item("interest_calctype_id"))

                                    Case enLoanInterestCalcType.DAILY
                                mdecTotPrincipalAmt = ((decPrincipalBalancePaidCurrency / intNextEffectiveDays) * intDaysDiff) / decExRate '(decPrincipalBalance / intNextEffectiveDays) * intDaysDiff
                                mdecTotPrincipalAmtPaidCurrency = (decPrincipalBalancePaidCurrency / intNextEffectiveDays) * intDaysDiff
                                            'Sohail (01 Nov 2017) -- Start
                                            'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                                            'mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
                                            'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)
                                            mdecTotIntAmt = ((decPrincipalBalancePaidCurrency * decIntRate * intDaysDiff) / (36500)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
                                            mdecTotIntAmtPaidCurrency = (decPrincipalBalancePaidCurrency * decIntRate * intDaysDiff) / (36500)
                                            'Sohail (01 Nov 2017) -- End
                                            'Hemant (29 Jan 2019) -- Start
                                            'PACRA Issue - 74.1 - Full EMI deducted for simple interest loans even if loan balance is less than EMI.
                                            'Hemant (10 Jul 2019) -- Start
                                            'If decCFBalance < mdecTotPrincipalAmt Then
                                            If (mdecTotPrincipalAmt - decCFBalance) > 1 Then
                                                'Hemant (10 Jul 2019) -- End
                                                mdecTotPrincipalAmt = decCFBalance
                                                mdecTotPrincipalAmtPaidCurrency = decCFBalancePaidCurrency
                                                mdecTotIntAmt = ((mdecTotPrincipalAmt * decIntRate * intDaysDiff) / (36500)) / decExRate
                                                mdecTotIntAmtPaidCurrency = (mdecTotPrincipalAmtPaidCurrency * decIntRate * intDaysDiff) / (36500)
                                            End If
                                            'Hemant (29 Jan 2019) -- End

                                    Case enLoanInterestCalcType.MONTHLY
                                        mdecTotPrincipalAmt = ((decPrincipalBalancePaidCurrency / intNextEffectiveMonths) * intMonthsDiff) / decExRate '(decPrincipalBalance / intNextEffectiveDays) * intDaysDiff
                                        mdecTotPrincipalAmtPaidCurrency = (decPrincipalBalancePaidCurrency / intNextEffectiveMonths) * intMonthsDiff
                                            'Sohail (01 Nov 2017) -- Start
                                            'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                                            'mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (1200)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
                                            'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (1200)
                                            mdecTotIntAmt = ((decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (1200)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
                                            mdecTotIntAmtPaidCurrency = (decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (1200)
                                            'Sohail (01 Nov 2017) -- End
                                            'Hemant (29 Jan 2019) -- Start
                                            'PACRA Issue - 74.1 - Full EMI deducted for simple interest loans even if loan balance is less than EMI.
                                            'Hemant (10 Jul 2019) -- Start
                                            'If decCFBalance < mdecTotPrincipalAmt Then
                                            If (mdecTotPrincipalAmt - decCFBalance) > 1 Then
                                                'Hemant (10 Jul 2019) -- End
                                                mdecTotPrincipalAmt = decCFBalance
                                                mdecTotPrincipalAmtPaidCurrency = decCFBalancePaidCurrency
                                                mdecTotIntAmt = ((mdecTotPrincipalAmt * decIntRate * intMonthsDiff) / (1200)) / decExRate
                                                mdecTotIntAmtPaidCurrency = (mdecTotPrincipalAmtPaidCurrency * decIntRate * intMonthsDiff) / (1200)
                                            End If
                                            'Hemant (29 Jan 2019) -- End
                                            'Sohail (14 Mar 2017) -- Start
                                            'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                                        Case enLoanInterestCalcType.BY_TENURE
                                            mdecTotPrincipalAmt = ((decPrincipalBalancePaidCurrency / intNextEffectiveMonths) * intMonthsDiff) / decExRate
                                            mdecTotPrincipalAmtPaidCurrency = (decPrincipalBalancePaidCurrency / intNextEffectiveMonths) * intMonthsDiff
                                            'Sohail (01 Nov 2017) -- Start
                                            'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                                            'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (intTenure * 100)
                                            mdecTotIntAmt = ((decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (intNextEffectiveMonths * 100)) / decExRate
                                            mdecTotIntAmtPaidCurrency = (decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (intNextEffectiveMonths * 100)
                                            'Sohail (01 Nov 2017) -- End
                                            'Sohail (14 Mar 2017) -- End
                                            'Hemant (29 Jan 2019) -- Start
                                            'PACRA Issue - 74.1 - Full EMI deducted for simple interest loans even if loan balance is less than EMI.
                                            'Hemant (10 Jul 2019) -- Start
                                            'If decCFBalance < mdecTotPrincipalAmt Then
                                            If (mdecTotPrincipalAmt - decCFBalance) > 1 Then
                                                'Hemant (10 Jul 2019) -- End
                                                mdecTotPrincipalAmt = decCFBalance
                                                mdecTotPrincipalAmtPaidCurrency = decCFBalancePaidCurrency
                                                mdecTotIntAmt = ((mdecTotPrincipalAmt * decIntRate * intMonthsDiff) / (intNextEffectiveMonths * 100)) / decExRate
                                                mdecTotIntAmtPaidCurrency = (mdecTotPrincipalAmtPaidCurrency * decIntRate * intMonthsDiff) / (intNextEffectiveMonths * 100)
                                            End If
                                            'Hemant (29 Jan 2019) -- End

                                            'Sohail (23 May 2017) -- Start
                                            'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                                        Case enLoanInterestCalcType.SIMPLE_INTEREST
                                            mdecTotPrincipalAmt = ((decPrincipalBalancePaidCurrency / intNextEffectiveMonths) * intMonthsDiff) / decExRate
                                            mdecTotPrincipalAmtPaidCurrency = (decPrincipalBalancePaidCurrency / intNextEffectiveMonths) * intMonthsDiff
                                            'Sohail (01 Nov 2017) -- Start
                                            'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                                            'mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (100)) / decExRate
                                            'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (100)
                                            mdecTotIntAmt = ((decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (100)) / decExRate
                                            mdecTotIntAmtPaidCurrency = (decPrincipalBalancePaidCurrency * decIntRate * intMonthsDiff) / (100)
                                            'Sohail (01 Nov 2017) -- End
                                            'Sohail (23 May 2017) -- End
                                            'Hemant (29 Jan 2019) -- Start
                                            'PACRA Issue - 74.1 - Full EMI deducted for simple interest loans even if loan balance is less than EMI.
                                            'Hemant (10 Jul 2019) -- Start
                                            'If decCFBalance < mdecTotPrincipalAmt Then
                                            If (mdecTotPrincipalAmt - decCFBalance) > 1 Then
                                                'Hemant (10 Jul 2019) -- End
                                                mdecTotPrincipalAmt = decCFBalance
                                                mdecTotPrincipalAmtPaidCurrency = decCFBalancePaidCurrency
                                                mdecTotIntAmt = ((mdecTotPrincipalAmt * decIntRate * intMonthsDiff) / (100)) / decExRate
                                                mdecTotIntAmtPaidCurrency = (mdecTotPrincipalAmtPaidCurrency * decIntRate * intMonthsDiff) / (100)
                                            End If
                                            'Hemant (29 Jan 2019) -- End
                                End Select

                                If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF Then
                                    mdecTotPrincipalAmt = 0
                                    mdecTotPrincipalAmtPaidCurrency = 0
                                    mdecTotIntAmt = 0
                                    mdecTotIntAmtPaidCurrency = 0
                                End If

                                    'Hemant (03 Oct 2018)  -- Start
                                    'Enhancement : CCK # 0002584 Credit and Debit Amounts Not Balancing on Journal
                                    'Sohail (08 Feb 2022) -- Start
                                    'Issue : : Zuri - Wrong Loan balance on payslip for non-base currency loans.
                                    'mdecTotPrincipalAmt = Format(mdecTotPrincipalAmt, strFmtCurrency)
                                    mdecTotPrincipalAmt = Format(mdecTotPrincipalAmtPaidCurrency / decExRate, strFmtCurrency)
                                    'Sohail ((08 Feb 2022) -- End
                                    mdecTotPrincipalAmtPaidCurrency = Format(mdecTotPrincipalAmtPaidCurrency, strFmtCurrency)
                                    'Sohail (08 Feb 2022) -- Start
                                    'Issue : : Zuri - Wrong Loan balance on payslip for non-base currency loans.
                                    'mdecTotIntAmt = Format(mdecTotIntAmt, strFmtCurrency)
                                    mdecTotIntAmt = Format(mdecTotIntAmtPaidCurrency / decExRate, strFmtCurrency)
                                    'Sohail ((08 Feb 2022) -- End
                                    mdecTotIntAmtPaidCurrency = Format(mdecTotIntAmtPaidCurrency, strFmtCurrency)
                                    'Hemant (03 Oct 2018)  -- End
                                    'Sohail (08 Feb 2022) -- Start
                                    'Issue : : Zuri - Wrong Loan balance on payslip for non-base currency loans.
                                    'mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                    mdecTotPMTAmt = (mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency) / decExRate
                                    'Sohail ((08 Feb 2022) -- End
                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                    'Sohail (08 Feb 2022) -- Start
                                    'Issue : : Zuri - Wrong Loan balance on payslip for non-base currency loans.
                                    'mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                    mdecLastProjectedBalance = (decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency) / decExRate
                                    'Sohail ((08 Feb 2022) -- End
                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
                            End If

                        Case enLoanCalcId.Compound_Interest
                            'If intDuration > 12 Then
                            '    mdecInterest_Amount = decPrincipalAmt * CDec(Pow((1 + (dblRate / 100)), (intDuration / 12)))
                            'Else
                            '    mdecInterest_Amount = decPrincipalAmt * CDec(Pow((1 + (dblRate / 100)), intDuration))
                            'End If
                            'mdecNetAmount = mdecInterest_Amount + decPrincipalAmt
                        Case enLoanCalcId.Reducing_Amount
                            Dim decPMT As Decimal = 0
                            Dim decPMTPaidCurrency As Decimal = 0
                            Dim mdecNewReduceAmt As Decimal = decCFBalance
                            Dim mdecNewReduceAmtPaidCurrency As Decimal = decCFBalancePaidCurrency
                            Dim decIntAmt As Decimal = 0
                            Dim decIntAmtPaidCurrency As Decimal = 0
                            Dim decPrincAmt As Decimal = 0
                            Dim decPrincAmtPaidCurrency As Decimal = 0

                            For i As Integer = 1 To intDaysDiff
                                    If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False AndAlso intMonth <= 1 Then
                                        'Sohail (13 Dec 2016) -- End
                                    decPMT = 0
                                    decPMTPaidCurrency = 0
                                    decIntAmt = 0
                                    decIntAmtPaidCurrency = 0
                                    decPrincAmt = 0
                                    decPrincAmtPaidCurrency = 0
                                    mdecNewReduceAmt -= decPrincAmt
                                    mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
                                    ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True AndAlso intMonth <= 1 Then
                                    Select Case CInt(.Item("interest_calctype_id"))

                                        Case enLoanInterestCalcType.DAILY
                                    decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                    decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 36500

                                        Case enLoanInterestCalcType.MONTHLY
                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 1200) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 1200

                                                'Sohail (14 Mar 2017) -- Start
                                                'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                                            Case enLoanInterestCalcType.BY_TENURE
                                                decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / (intTenure * 100)) / decExRate
                                                decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / (intTenure * 100)
                                                'Sohail (14 Mar 2017) -- End

                                                'Sohail (23 May 2017) -- Start
                                                'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                                            Case enLoanInterestCalcType.SIMPLE_INTEREST
                                                decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / (100)) / decExRate
                                                decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / (100)
                                                'Sohail (23 May 2017) -- End

                                    End Select

                                        'Hemant (03 Oct 2018)  -- Start
                                        'Enhancement : CCK # 0002584 Credit and Debit Amounts Not Balancing on Journal
                                        decIntAmt = Format(decIntAmt, strFmtCurrency)
                                        decIntAmtPaidCurrency = Format(decIntAmtPaidCurrency, strFmtCurrency)
                                        'Hemant (03 Oct 2018)  -- End
                                    decPMT = decIntAmt
                                    decPMTPaidCurrency = decIntAmtPaidCurrency
                                    decPrincAmt = 0
                                    decPrincAmtPaidCurrency = 0
                                    mdecNewReduceAmt -= decPrincAmt
                                    mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
                                Else

                                    Select Case CInt(.Item("interest_calctype_id"))

                                        Case enLoanInterestCalcType.DAILY
                                    decPMT = (Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalancePaidCurrency * -1))) / decExRate 'Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalance * -1))
                                    decPMTPaidCurrency = Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalancePaidCurrency * -1))
                                    decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                    decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 36500

                                        Case enLoanInterestCalcType.MONTHLY
                                            decPMT = (Pmt(decIntRate / 1200, intNextEffectiveMonths, (decPrincipalBalancePaidCurrency * -1))) / decExRate 'Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalance * -1))
                                            decPMTPaidCurrency = Pmt(decIntRate / 1200, intNextEffectiveMonths, (decPrincipalBalancePaidCurrency * -1))
                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 1200) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 1200

                                                'Sohail (14 Mar 2017) -- Start
                                                'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                                            Case enLoanInterestCalcType.BY_TENURE
                                                decPMT = (Pmt(decIntRate / 1200, intNextEffectiveMonths, (decPrincipalBalancePaidCurrency * -1))) / decExRate
                                                decPMTPaidCurrency = Pmt(decIntRate / 1200, intNextEffectiveMonths, (decPrincipalBalancePaidCurrency * -1))
                                                decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / (intTenure * 100)) / decExRate
                                                decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / (intTenure * 100)
                                                'Sohail (14 Mar 2017) -- End

                                                'Sohail (23 May 2017) -- Start
                                                'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                                            Case enLoanInterestCalcType.SIMPLE_INTEREST
                                                decPMT = (Pmt(decIntRate / 1200, intNextEffectiveMonths, (decPrincipalBalancePaidCurrency * -1))) / decExRate
                                                decPMTPaidCurrency = Pmt(decIntRate / 1200, intNextEffectiveMonths, (decPrincipalBalancePaidCurrency * -1))
                                                decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / (100)) / decExRate
                                                decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / (100)
                                                'Sohail (23 May 2017) -- End

                                    End Select

                                    If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF Then
                                        decPMT = 0
                                        decPMTPaidCurrency = 0
                                        decIntAmt = 0
                                        decPrincAmtPaidCurrency = 0
                                    End If

                                        'Hemant (03 Oct 2018)  -- Start
                                        'Enhancement : CCK # 0002584 Credit and Debit Amounts Not Balancing on Journal
                                        decPMT = Format(decPMT, strFmtCurrency)
                                        decPMTPaidCurrency = Format(decPMTPaidCurrency, strFmtCurrency)
                                        decIntAmt = Format(decIntAmt, strFmtCurrency)
                                        decIntAmtPaidCurrency = Format(decIntAmtPaidCurrency, strFmtCurrency)
                                        'Hemant (03 Oct 2018)  -- End
                                    decPrincAmt = decPMT - decIntAmt
                                    decPrincAmtPaidCurrency = decPMTPaidCurrency - decIntAmtPaidCurrency
                                        'Hemant (04 June 2019) -- Start
                                        'ISSUE/ENHANCEMENT : The employee loan still gets deducted even after loan tenure is over
                                        If decCFBalance < decPrincAmt Then
                                            decPrincAmt = decCFBalance
                                            decPrincAmtPaidCurrency = decCFBalancePaidCurrency
                                            decPMT = decPrincAmt + decIntAmt
                                            decPMTPaidCurrency = decPrincAmtPaidCurrency + decIntAmtPaidCurrency
                                        End If
                                        'Hemant (04 June 2019) -- End
                                    mdecNewReduceAmt -= decPrincAmt
                                    mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
                                        'Hemant (04 June 2019) -- Start
                                        'ISSUE/ENHANCEMENT : The employee loan still gets deducted even after loan tenure is over
                                        decPrincAmt = Format(decPrincAmt, strFmtCurrency)
                                        decPrincAmtPaidCurrency = Format(decPrincAmtPaidCurrency, strFmtCurrency)
                                        decPMT = Format(decPMT, strFmtCurrency)
                                        decPMTPaidCurrency = Format(decPMTPaidCurrency, strFmtCurrency)
                                        'Hemant (04 June 2019) -- End
                                End If

                                mdecTotPMTAmt += decPMT
                                mdecTotPMTAmtPaidCurrency += decPMTPaidCurrency
                                mdecTotIntAmt += decIntAmt
                                mdecTotIntAmtPaidCurrency += decIntAmtPaidCurrency
                                mdecTotPrincipalAmt += decPrincAmt
                                mdecTotPrincipalAmtPaidCurrency += decPrincAmtPaidCurrency

                                    'Sohail (14 Mar 2017) -- Start
                                    'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                                    'If CInt(.Item("interest_calctype_id")) = enLoanInterestCalcType.MONTHLY Then
                                    'Sohail (23 May 2017) -- Start
                                    'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                                    'If CInt(.Item("interest_calctype_id")) = enLoanInterestCalcType.MONTHLY OrElse CInt(.Item("interest_calctype_id")) = enLoanInterestCalcType.BY_TENURE Then
                                    If CInt(.Item("interest_calctype_id")) = enLoanInterestCalcType.MONTHLY OrElse CInt(.Item("interest_calctype_id")) = enLoanInterestCalcType.BY_TENURE OrElse CInt(.Item("interest_calctype_id")) = enLoanInterestCalcType.SIMPLE_INTEREST Then
                                        'Sohail (23 May 2017) -- End
                                        'Sohail (14 Mar 2017) -- End
                                    Exit For
                                End If
                            Next

                                'Sohail (08 Feb 2022) -- Start
                                'Issue : : Zuri - Wrong Loan balance on payslip for non-base currency loans.
                                'mdecLastProjectedBalance = mdecNewReduceAmt
                                mdecTotPMTAmt = mdecTotPMTAmtPaidCurrency / decExRate
                                mdecTotIntAmt = mdecTotIntAmtPaidCurrency / decExRate
                                mdecTotPrincipalAmt = mdecTotPrincipalAmtPaidCurrency / decExRate
                                mdecLastProjectedBalance = mdecNewReduceAmtPaidCurrency / decExRate
                                'Sohail ((08 Feb 2022) -- End
                            mdecLastProjectedBalancePaidCurrency = mdecNewReduceAmtPaidCurrency

                                'Sohail (10 Jun 2020) -- Start
                                'NMB Issue # : The added or subtracted value results in an un-representable datetime due Zero EMI amount.
                                'If intMonth > 1 AndAlso mdecLastProjectedBalancePaidCurrency > 0.001 AndAlso dtStart > dtLoanEndDate Then
                                If intMonth > 1 AndAlso mdecLastProjectedBalancePaidCurrency > 0.001 AndAlso dtStart > dtLoanEndDate AndAlso mdecTotPMTAmt > 0 Then
                                    'Sohail (10 Jun 2020) -- End
                                    dtLoanEndDate = dtLoanEndDate.AddDays(1).AddMonths(1).AddDays(-1)
                                End If

                        Case enLoanCalcId.No_Interest
                                If intMonth > 1 Then
                                    decCFBalance = mdecLastProjectedBalance
                                    decCFBalancePaidCurrency = mdecLastProjectedBalancePaidCurrency
                                End If

                                If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False AndAlso intMonth <= 1 Then
                                mdecTotPrincipalAmt = 0
                                mdecTotPrincipalAmtPaidCurrency = 0
                                mdecTotIntAmt = 0
                                mdecTotIntAmtPaidCurrency = 0
                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
                                ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True AndAlso intMonth <= 1 Then
                                mdecTotPrincipalAmt = 0
                                mdecTotPrincipalAmtPaidCurrency = 0
                                mdecTotIntAmt = 0
                                mdecTotIntAmtPaidCurrency = 0
                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
                            Else
                                If blnFromProcessPayroll = True Then
                                    mdecTotPrincipalAmt = CDec(.Item("emi_amountPaidCurrency").ToString) / decExRate 'CDec(.Item("emi_amount").ToString) 'Now pick emi_amout from emi tenure change screen.
                                    mdecTotPrincipalAmtPaidCurrency = CDec(.Item("emi_amountPaidCurrency").ToString) 'Now pick emi_amout from emi tenure change screen.
                                    If mdecTotPrincipalAmtPaidCurrency > decCFBalancePaidCurrency Then
                                        mdecTotPrincipalAmtPaidCurrency = decCFBalancePaidCurrency
                                        mdecTotPrincipalAmt = decCFBalance
                                    End If

                                    If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF Then
                                        mdecTotPrincipalAmt = 0
                                        mdecTotPrincipalAmtPaidCurrency = 0
                                    End If

                                Else
                                    mdecTotPrincipalAmt = 0
                                    mdecTotPrincipalAmtPaidCurrency = 0
                                End If

                                If intDaysDiff <= 0 Then
                                    mdecTotPrincipalAmt = 0
                                    mdecTotPrincipalAmtPaidCurrency = 0
                                End If

                                    'Hemant (03 Oct 2018)  -- Start
                                    'Enhancement : CCK # 0002584 Credit and Debit Amounts Not Balancing on Journal
                                    mdecTotPrincipalAmt = Format(mdecTotPrincipalAmt, strFmtCurrency)
                                    mdecTotPrincipalAmtPaidCurrency = Format(mdecTotPrincipalAmtPaidCurrency, strFmtCurrency)
                                    'Hemant (03 Oct 2018)  -- End
                                mdecTotIntAmt = 0
                                mdecTotIntAmtPaidCurrency = 0
                                    'Sohail (08 Feb 2022) -- Start
                                    'Issue : : Zuri - Wrong Loan balance on payslip for non-base currency loans.
                                    'mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                    mdecTotPMTAmt = (mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency) / decExRate
                                    'Sohail ((08 Feb 2022) -- End
                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                    'Sohail (08 Feb 2022) -- Start
                                    'Issue : : Zuri - Wrong Loan balance on payslip for non-base currency loans.
                                    'mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                    mdecLastProjectedBalance = (decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency) / decExRate
                                    'Sohail ((08 Feb 2022) -- End
                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency

                                    'Sohail (16 Jan 2018) -- Start
                                    'Issue : SUPPORT Shambani #1844 | A deducted loan isn't showing on report in 70.1.
                                    'If intMonth > 1 AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate Then
                                    'Sohail (10 Jun 2020) -- Start
                                    'NMB Issue # : The added or subtracted value results in an un-representable datetime due Zero EMI amount.
                                    'If blnAddProjectedPeriodDeductions = True AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate > 0 Then
                                    If blnAddProjectedPeriodDeductions = True AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate AndAlso mdecTotPMTAmt > 0 Then
                                        'Sohail (10 Jun 2020) -- End
                                        'Sohail (16 Jan 2018) -- End
                                        dtLoanEndDate = dtLoanEndDate.AddDays(1).AddMonths(1).AddDays(-1)
                                    End If
                            End If

                        Case enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI
                            Dim decPMT As Decimal = 0
                            Dim decPMTPaidCurrency As Decimal = 0
                            Dim mdecNewReduceAmt As Decimal = decCFBalance
                            Dim mdecNewReduceAmtPaidCurrency As Decimal = decCFBalancePaidCurrency
                            Dim decIntAmt As Decimal = 0
                            Dim decIntAmtPaidCurrency As Decimal = 0
                            Dim decPrincAmt As Decimal = 0
                            Dim decPrincAmtPaidCurrency As Decimal = 0

                                If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False AndAlso intMonth <= 1 Then
                                decPMT = 0
                                decPMTPaidCurrency = 0
                                decIntAmt = 0
                                decIntAmtPaidCurrency = 0
                                decPrincAmt = 0
                                decPrincAmtPaidCurrency = 0
                                mdecNewReduceAmt -= decPrincAmt
                                mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
                                ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True AndAlso intMonth <= 1 Then
                                Select Case CInt(.Item("interest_calctype_id"))

                                    Case enLoanInterestCalcType.DAILY
                                        decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                        decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 36500

                                    Case enLoanInterestCalcType.MONTHLY
                                        decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 1200) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                        decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 1200

                                        .Item("TotalMonthlyInterest") = 0
                                        .Item("TotalMonthlyInterestPaidCurrency") = 0

                                            'Sohail (14 Mar 2017) -- Start
                                            'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                                        Case enLoanInterestCalcType.BY_TENURE
                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / (intTenure * 100)) / decExRate
                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / (intTenure * 100)

                                            .Item("TotalMonthlyInterest") = 0
                                            .Item("TotalMonthlyInterestPaidCurrency") = 0
                                            'Sohail (14 Mar 2017) -- End

                                            'Sohail (23 May 2017) -- Start
                                            'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                                        Case enLoanInterestCalcType.SIMPLE_INTEREST
                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / (100)) / decExRate
                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / (100)

                                            .Item("TotalMonthlyInterest") = 0
                                            .Item("TotalMonthlyInterestPaidCurrency") = 0
                                            'Sohail (23 May 2017) -- End

                                End Select
                                    'Hemant (03 Oct 2018)  -- Start
                                    'Enhancement : CCK # 0002584 Credit and Debit Amounts Not Balancing on Journal
                                    decIntAmt = Format(decIntAmt, strFmtCurrency)
                                    decIntAmtPaidCurrency = Format(decIntAmtPaidCurrency, strFmtCurrency)
                                    'Hemant (03 Oct 2018)  -- End
                                decPMT = decIntAmt
                                decPMTPaidCurrency = decIntAmtPaidCurrency
                                decPrincAmt = 0
                                decPrincAmtPaidCurrency = 0
                                mdecNewReduceAmt -= decPrincAmt
                                mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
                            Else
                                If blnFromProcessPayroll = True Then
                                    decPrincAmt = CDec(.Item("emi_principal_amount").ToString) / decExRate
                                    decPrincAmtPaidCurrency = CDec(.Item("emi_principal_amount").ToString)

                                    If decPrincAmtPaidCurrency > decCFBalancePaidCurrency Then
                                        decPrincAmtPaidCurrency = decCFBalancePaidCurrency
                                        decPrincAmt = decCFBalance
                                    End If

                                Else
                                    decPrincAmt = 0
                                    decPrincAmtPaidCurrency = 0
                                End If

                                Select Case CInt(.Item("interest_calctype_id"))

                                    Case enLoanInterestCalcType.DAILY
                                        decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate * intDaysDiff) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                        decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate * intDaysDiff) / 36500

                                        If intDaysDiff <= 0 Then
                                            decPrincAmt = 0
                                            decPrincAmtPaidCurrency = 0
                                        End If
                                    Case enLoanInterestCalcType.MONTHLY
                                        decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate * intMonthsDiff) / 1200) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                        decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate * intMonthsDiff) / 1200

                                        If intMonthsDiff <= 0 Then
                                            decPrincAmt = 0
                                            decPrincAmtPaidCurrency = 0
                                        End If

                                            'Sohail (14 Mar 2017) -- Start
                                            'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                                        Case enLoanInterestCalcType.BY_TENURE
                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate * intMonthsDiff) / (intTenure * 100)) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate * intMonthsDiff) / (intTenure * 100)

                                            If intMonthsDiff <= 0 Then
                                                decPrincAmt = 0
                                                decPrincAmtPaidCurrency = 0
                                            End If
                                            'Sohail (14 Mar 2017) -- End

                                            'Sohail (23 May 2017) -- Start
                                            'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                                        Case enLoanInterestCalcType.SIMPLE_INTEREST
                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate * intMonthsDiff) / (100)) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate * intMonthsDiff) / (100)

                                            If intMonthsDiff <= 0 Then
                                                decPrincAmt = 0
                                                decPrincAmtPaidCurrency = 0
                                            End If
                                            'Sohail (23 May 2017) -- End

                                End Select

                                    'Sohail (08 Feb 2018) -- Start
                                    'SUPPORT KBC#1869 | LOAN BALANCE ON PAYSLIP DOES NOT SHOW in 70.1.
                                    'Sohail (03 Apr 2018) -- Start
                                    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
                                    'If blnFromProcessPayroll = False Then
                                    '    decIntAmt = 0
                                    '    decIntAmtPaidCurrency = 0
                                    'End If                                   
                                    'Sohail (03 Apr 2018) -- End
                                    'Sohail (08 Feb 2018) -- End

                                    'Hemant (09 Dec 2019) -- Start
                                    If blnFromProcessPayroll = False Then
                                        decIntAmt = 0
                                        decIntAmtPaidCurrency = 0
                                    End If
                                    'Hemant (09 Dec 2019) -- End

                                If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF Then
                                    decPrincAmt = 0
                                    decPrincAmtPaidCurrency = 0
                                    decIntAmt = 0
                                    decIntAmtPaidCurrency = 0
                                End If

                                    'Hemant (03 Oct 2018)  -- Start
                                    'Enhancement : CCK # 0002584 Credit and Debit Amounts Not Balancing on Journal
                                    decPrincAmt = Format(decPrincAmt, strFmtCurrency)
                                    decPrincAmtPaidCurrency = Format(decPrincAmtPaidCurrency, strFmtCurrency)
                                    decIntAmt = Format(decIntAmt, strFmtCurrency)
                                    decIntAmtPaidCurrency = Format(decIntAmtPaidCurrency, strFmtCurrency)
                                    'Hemant (03 Oct 2018)  -- End
                                decPMT = decPrincAmt + decIntAmt
                                decPMTPaidCurrency = decPrincAmtPaidCurrency + decIntAmtPaidCurrency
                                mdecNewReduceAmt -= decPrincAmt
                                mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency

                            End If

                            mdecTotPMTAmt += decPMT
                            mdecTotPMTAmtPaidCurrency += decPMTPaidCurrency
                            mdecTotIntAmt += decIntAmt
                            mdecTotIntAmtPaidCurrency += decIntAmtPaidCurrency
                            mdecTotPrincipalAmt += decPrincAmt
                            mdecTotPrincipalAmtPaidCurrency += decPrincAmtPaidCurrency

                            mdecLastProjectedBalance = mdecNewReduceAmt
                            mdecLastProjectedBalancePaidCurrency = mdecNewReduceAmtPaidCurrency

                                'Sohail (16 Jan 2018) -- Start
                                'Issue : SUPPORT Shambani #1844 | A deducted loan isn't showing on report in 70.1.
                                'If intMonth > 1 AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate Then
                                'Sohail (10 Jun 2020) -- Start
                                'NMB Issue # : The added or subtracted value results in an un-representable datetime due Zero EMI amount.
                                'If blnAddProjectedPeriodDeductions = True AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate Then
                                If blnAddProjectedPeriodDeductions = True AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate AndAlso mdecTotPMTAmt > 0 Then
                                    'Sohail (10 Jun 2020) -- End
                                    'Sohail (16 Jan 2018) -- End
                                    dtLoanEndDate = dtLoanEndDate.AddDays(1).AddMonths(1).AddDays(-1)
                                End If

                                'Sohail (29 Apr 2019) -- Start
                                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                            Case enLoanCalcId.No_Interest_With_Mapped_Head
                                If intMonth > 1 Then
                                    decCFBalance = mdecLastProjectedBalance
                                    decCFBalancePaidCurrency = mdecLastProjectedBalancePaidCurrency
                                End If

                                If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False AndAlso intMonth <= 1 Then
                                    mdecTotPrincipalAmt = 0
                                    mdecTotPrincipalAmtPaidCurrency = 0
                                    mdecTotIntAmt = 0
                                    mdecTotIntAmtPaidCurrency = 0
                                    mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                    mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                    mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                    mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
                                ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True AndAlso intMonth <= 1 Then
                                    mdecTotPrincipalAmt = 0
                                    mdecTotPrincipalAmtPaidCurrency = 0
                                    mdecTotIntAmt = 0
                                    mdecTotIntAmtPaidCurrency = 0
                                    mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                    mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                    mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                    mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
                                Else
                                    If blnFromProcessPayroll = True Then
                                        mdecTotPrincipalAmt = CDec(.Item("MappedHeadamount").ToString) / decExRate
                                        mdecTotPrincipalAmtPaidCurrency = CDec(.Item("MappedHeadamountpaidcurrency").ToString)
                                        If mdecTotPrincipalAmtPaidCurrency > decCFBalancePaidCurrency Then
                                            mdecTotPrincipalAmtPaidCurrency = decCFBalancePaidCurrency
                                            mdecTotPrincipalAmt = decCFBalance
                                        End If

                                        If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF Then
                                            mdecTotPrincipalAmt = 0
                                            mdecTotPrincipalAmtPaidCurrency = 0
                                        End If

                                    Else
                                        mdecTotPrincipalAmt = 0
                                        mdecTotPrincipalAmtPaidCurrency = 0
                                    End If

                                    If intDaysDiff <= 0 Then
                                        mdecTotPrincipalAmt = 0
                                        mdecTotPrincipalAmtPaidCurrency = 0
                                    End If

                                    mdecTotPrincipalAmt = Format(mdecTotPrincipalAmt, strFmtCurrency)
                                    mdecTotPrincipalAmtPaidCurrency = Format(mdecTotPrincipalAmtPaidCurrency, strFmtCurrency)
                                    mdecTotIntAmt = 0
                                    mdecTotIntAmtPaidCurrency = 0
                                    mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                    mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                    mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                    mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency

                                    'Sohail (10 Jun 2020) -- Start
                                    'NMB Issue # : The added or subtracted value results in an un-representable datetime due Zero EMI amount.
                                    'If blnAddProjectedPeriodDeductions = True AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate Then
                                    If blnAddProjectedPeriodDeductions = True AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate AndAlso mdecTotPrincipalAmt > 0 Then
                                        'Sohail (10 Jun 2020) -- End
                                        dtLoanEndDate = dtLoanEndDate.AddDays(1).AddMonths(1).AddDays(-1)
                                    End If
                                End If
                                'Sohail (29 Apr 2019) -- End

                        Case Else
                            If CInt(.Item("isloan")) = False Then
                                mdecTotPrincipalAmt = decBFAmount
                                mdecTotPrincipalAmtPaidCurrency = decBFAmountPaidCurrency
                                mdecTotIntAmt = 0
                                mdecTotIntAmtPaidCurrency = 0
                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
                                    'Sohail (24 Apr 2019) -- Start
                                    'NPK Issue - Support Issue Id # 0003771 - 76.1 - Not Able to delete the advance after voiding payroll payment of March (due to transaction was not getting voided from loan_balance_tran on voiding payment).
                                    'mdecLastProjectedBalance = CDec(.Item("balance_amount")) - mdecTotPrincipalAmt
                                    'mdecLastProjectedBalancePaidCurrency = CDec(.Item("balance_amountPaidCurrency")) - mdecTotPrincipalAmtPaidCurrency
                                    'Hemant (13 Nov 2019) -- Start
                                    'ISSUE/ENHANCEMENT#(MSL) - BBL - ISSUES WITH THE LOAN ADVANCE REPORT
                                    If intLoanAdvanceStatus = enLoanStatus.COMPLETED Then
                                        mdecLastProjectedBalance = 0
                                        mdecLastProjectedBalancePaidCurrency = 0
                                    Else
                                        'Hemant (13 Nov 2019) -- End
                                    mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
                                    mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
                                    End If 'Hemant (13 Nov 2019)
                                    'Sohail (24 Apr 2019) -- End                                    
                            End If
                    End Select


                        If intMonth <= 1 Then
                    .Item("DaysDiff") = intDaysDiff
                    .Item("TotPMTAmount") = mdecTotPMTAmt
                    .Item("TotPMTAmountPaidCurrency") = mdecTotPMTAmtPaidCurrency
                    .Item("TotInterestAmount") = mdecTotIntAmt
                    .Item("TotInterestAmountPaidCurrency") = mdecTotIntAmtPaidCurrency
                    .Item("TotPrincipalAmount") = mdecTotPrincipalAmt
                    .Item("TotPrincipalAmountPaidCurrency") = mdecTotPrincipalAmtPaidCurrency
                    .Item("LastProjectedBalance") = mdecLastProjectedBalance
                    .Item("LastProjectedBalancePaidCurrency") = mdecLastProjectedBalancePaidCurrency

                    If blnIgnoreCurrentMonthTopupRepaymentInBFBalance = True Then
                        .Item("BalanceAmount") = decCFBalance - CDec(.Item("TotalMonthlyTopup")) + CDec(.Item("TotalMonthlyRepayment")) + CDec(.Item("TotalMonthlyPrincipalAmt"))
                        .Item("BalanceAmountPaidCurrency") = decCFBalancePaidCurrency - CDec(.Item("TotalMonthlyTopupPaidCurrency")) + CDec(.Item("TotalMonthlyRepaymentPaidCurrency")) + CDec(.Item("TotalMonthlyPrincipalAmtPaidCurrency"))
                    End If

                    .AcceptChanges()

                        Else
                            Dim LoanRow As DataRow = dtTemp.NewRow
                            LoanRow.ItemArray = dsRow.ItemArray

                            With LoanRow
                                .Item("CurrMonthPeriodUnkid") = 0
                                .Item("CurrMonthPeriodName") = Format(dtStart.AddDays(-1), "MMM - yyyy") & "  (Projected)"
                                .Item("CurrMonthPeriod_start_date") = eZeeDate.convertDate(dtStart.AddMonths(-1))
                                .Item("CurrMonthPeriod_end_date") = eZeeDate.convertDate(dtStart.AddDays(-1))
                                .Item("DaysDiff") = intDaysDiff
                                .Item("TotPMTAmount") = mdecTotPMTAmt
                                .Item("TotPMTAmountPaidCurrency") = mdecTotPMTAmtPaidCurrency
                                .Item("TotInterestAmount") = mdecTotIntAmt
                                .Item("TotInterestAmountPaidCurrency") = mdecTotIntAmtPaidCurrency
                                .Item("TotPrincipalAmount") = mdecTotPrincipalAmt
                                .Item("TotPrincipalAmountPaidCurrency") = mdecTotPrincipalAmtPaidCurrency
                                .Item("LastProjectedBalance") = mdecLastProjectedBalance
                                .Item("LastProjectedBalancePaidCurrency") = mdecLastProjectedBalancePaidCurrency

                                If blnIgnoreCurrentMonthTopupRepaymentInBFBalance = True Then
                                    .Item("BalanceAmount") = decCFBalance
                                    .Item("BalanceAmountPaidCurrency") = decCFBalancePaidCurrency
                                End If

                            End With
                            dtTemp.Rows.Add(LoanRow)
                        End If

                        If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF OrElse intLoanAdvanceStatus = enLoanStatus.COMPLETED Then
                            Exit While
                        End If

                        dtStart = dtStart.AddMonths(1)
                    End While

                End With
            Next

            dsList.Tables(0).Merge(dtTemp)

            If intLoanStatus > 0 AndAlso intLoanStatus = enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED Then
                Dim dr_Row() As DataRow = dsList.Tables(strTableName).Select(" TotalMonthlyDeductionPaidCurrency + TotPMTAmountPaidCurrency <= 0 ")
                If dr_Row.Length > 0 Then
                    For Each dRow As DataRow In dr_Row
                        dsList.Tables(strTableName).Rows.Remove(dRow)
                    Next
                    dsList.Tables(strTableName).AcceptChanges()
                End If
            End If

        Catch ex As Exception
            'Hemant (25 Feb 2019) -- Start
            'Issue PACRA #3524: Not able to Void The payroll Due to error Attempted to Divide by Zero .
            'Throw New Exception(ex.Message & "; Procedure Name: Calculate_LoanInfo; Module Name: " & mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: Calculate_LoanInfo; Module Name: " & mstrModuleName & "; For Loan Voucher No : " & strLoanVoucherNo)
            'Hemant (25 Feb 2019) -- End
        Finally
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function

    'Sohail (02 Jan 2017) -- Start
    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
    'Public Function Calculate_LoanBalanceInfo(ByVal xDatabaseName As String _
    '                                        , ByVal xUserUnkid As Integer _
    '                                        , ByVal xYearUnkid As Integer _
    '                                        , ByVal xCompanyUnkid As Integer _
    '                                        , ByVal xPeriodStart As DateTime _
    '                                        , ByVal xPeriodEnd As DateTime _
    '                                        , ByVal xUserModeSetting As String _
    '                                        , ByVal xOnlyApproved As Boolean _
    '                                        , ByVal strTableName As String _
    '                                          , ByVal dtAsOnDate As Date _
    '                                          , ByVal strEmployeeIDs As String _
    '                                        , Optional ByVal intLoanAdvanceUnkId As Integer = 0 _
    '                                          , Optional ByVal dtPeriodStartDate As Date = Nothing _
    '                                          , Optional ByVal blnFromProcessPayroll As Boolean = False _
    '                                          , Optional ByVal blnIncludeUnPaidLoan As Boolean = False _
    '                                          , Optional ByVal strLoanSchemeIDs As String = "" _
    '                                          , Optional ByVal blnIncludeOnHoldWrittenOff As Boolean = False _
    '                                          , Optional ByVal blnForReport As Boolean = False _
    '                                          , Optional ByVal blnFirstNameThenSurname As Boolean = True _
    '                                          , Optional ByVal blnIgnoreCurrentMonthTopupRepaymentInBFBalance As Boolean = False _
    '                                          , Optional ByVal blnIncludeCompletedLoanAdvance As Boolean = False _
    '                                          , Optional ByVal strExtraReportFilterString As String = "" _
    '                                          , Optional ByVal strAdvanceQueryJoin As String = "" _
    '                                          , Optional ByVal strEmployeeAlloctionJoin As String = "" _
    '                                          , Optional ByVal intLoanStatus As Integer = 0 _
    '                                          , Optional ByVal blnIsMSS As Boolean = True _
    '                                        , Optional ByVal strLoanAdvanceTranIDs As String = "" _
    '                                          , Optional ByVal blnAddProjectedPeriodDeductions As Boolean = False _
    '                                          ) As DataSet
    '    'Sohail (13 Dec 2016) - [blnAddProjectedPeriodDeductions]
    '    'Nilay (04-Nov-2016) -- [strLoanAdvanceUnkIDs]

    '    'Nilay (02-Jul-2016) -- [blnIsMSS]
    '    'Nilay (25-Mar-2016) -- End

    '    'Sohail (18 Jan 2016) - [intLoanStatus]
    '    'Nilay (10-Oct-2015) -- ADDED -- strExtraReportFilterString, strAdvanceQueryJoin,strEmployeeAlloctionJoin

    '    Dim dsList As New DataSet
    '    Dim strQ As String = ""
    '    Dim strFilter As String = ""
    '    Dim strCompletedFilter As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try
    '        'Nilay (25-Mar-2016) -- Start
    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
    '        'If xDatabaseName.Trim.Length > 0 AndAlso xUserUnkid > 0 AndAlso xCompanyUnkid > 0 Then
    '        'Sohail (18 Apr 2016) -- Start
    '        'Issue - 59.1 - Terminated employees were coming on report even if Exclude from process payroll option ticked on employee master.
    '        'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , True, xDatabaseName)
    '        'Sohail (18 Apr 2016) -- End

    '        'Nilay (02-Jul-2016) -- Start
    '        'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
    '        If blnIsMSS Then
    '            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
    '        End If
    '        'Nilay (02-Jul-2016) -- End

    '        'End If
    '        'Nilay (25-Mar-2016) -- End

    '        Dim mdecTotPMTAmt As Decimal = 0
    '        Dim mdecTotPMTAmtPaidCurrency As Decimal = 0
    '        Dim mdecTotIntAmt As Decimal = 0
    '        Dim mdecTotIntAmtPaidCurrency As Decimal = 0
    '        Dim mdecTotPrincipalAmt As Decimal = 0
    '        Dim mdecTotPrincipalAmtPaidCurrency As Decimal = 0
    '        Dim mdecLastProjectedBalance As Decimal = 0
    '        Dim mdecLastProjectedBalancePaidCurrency As Decimal = 0

    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If strLoanAdvanceTranIDs.Trim = "0" Then strLoanAdvanceTranIDs = ""
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            strQ = " CREATE TABLE #TBLnAdv (loanadvancetranunkid INT) " & _
    '                   " INSERT INTO #TBLnAdv (loanadvancetranunkid) " & _
    '                   "    SELECT loanadvancetranunkid " & _
    '                   "    FROM lnloan_advance_tran " & _
    '                   "    WHERE loanadvancetranunkid IN(" & IIf(strLoanAdvanceTranIDs.Trim.Length > 0, strLoanAdvanceTranIDs, "0") & ") "
    '        End If
    '        'Nilay (04-Nov-2016) -- End

    '        If blnIncludeCompletedLoanAdvance = False Then
    '            strCompletedFilter &= " AND (lnloan_advance_tran.isloan = 1 OR TableDeductionPeriod.start_date = @PeriodStartDate) "
    '        End If

    '        If intLoanAdvanceUnkId > 0 Then
    '            strFilter &= " AND lnloan_advance_tran.loanadvancetranunkid = @loanadvancetranunkid "
    '            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceUnkId)
    '        End If

    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            'strFilter &= " AND lnloan_advance_tran.loanadvancetranunkid IN(" & strLoanAdvanceTranIDs & ") "
    '            strFilter &= " AND #TBLnAdv.loanadvancetranunkid IS NOT NULL "
    '        End If
    '        'Nilay (04-Nov-2016) -- End

    '        If strEmployeeIDs.Trim <> "" Then
    '            strFilter &= " AND lnloan_advance_tran.employeeunkid IN (" & strEmployeeIDs & ") "
    '        End If

    '        If strLoanSchemeIDs.Trim <> "" Then
    '            strFilter &= " AND lnloan_advance_tran.loanschemeunkid IN (" & strLoanSchemeIDs & ") "
    '        End If


    '        'Nilay (10-Oct-2015) -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        ', Optional ByVal strExtraReportFilterString As String = "" _       - ADDED
    '        ', Optional ByVal strAdvanceQueryJoin As String = "" _
    '        ', Optional ByVal strUserAccessFilterString As String = "" _
    '        ', Optional ByVal strEmployeeAlloctionJoin As String = "" _

    '        'If blnForReport Then
    '        '    If strFilter.Length > 0 Then
    '        '        strFilter &= strExtraReportFilterString
    '        '    Else
    '        '        strFilter = strExtraReportFilterString
    '        '    End If
    '        'End If
    '        'Nilay (10-Oct-2015) -- End

    '        'Sohail (13 Dec 2016) -- Start
    '        'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '        Dim ds As DataSet = GetLoanCalculationTypeList("LoanCalcType", True, True)
    '        Dim dicLoanCalcType As Dictionary(Of Integer, String) = (From p In ds.Tables("LoanCalcType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
    '        ds = GetLoan_Interest_Calculation_Type("LoanIntCalcType", True, True)
    '        Dim dicLoanIntCalcType As Dictionary(Of Integer, String) = (From p In ds.Tables("LoanIntCalcType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
    '        'Sohail (13 Dec 2016) -- End

    '        If dtPeriodStartDate <> Nothing Then
    '            objDataOperation.AddParameter("@PeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStartDate))
    '            objDataOperation.AddParameter("@PrevPeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStartDate.AddDays(-1)))
    '        Else
    '            'Nilay (21-Dec-2016) -- Start
    '            'ISSUE: On loan advance report when future deduction period is assigned then future advance displays in current open period as well because here deduction start date <= asondate. To overcome deduction start date should be < asondate
    '            'objDataOperation.AddParameter("@PeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
    '            objDataOperation.AddParameter("@PeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
    '            'Nilay (21-Dec-2016) -- End

    '        End If

    '        '*******************************************************************************************************************
    '        '*** If you change any field name then please change those field name in [Loan Advance Saving -> Generate_LoanDetailReport_New] Report without fail ***
    '        '*******************************************************************************************************************

    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If strLoanAdvanceTranIDs.Trim = "" Then
    '            strQ = ""
    '        End If
    '        'Nilay (04-Nov-2016) -- End

    '        strQ &= "SELECT  Loan.loanadvancetranunkid  " & _
    '                      ", Loan.loanschemeunkid " & _
    '                      ", Loan.loaneffectivedate " & _
    '                      ", ISNULL(Balance.loanbalancetranunkid, Loan.loanadvancetranunkid) AS loanbalancetranunkid " & _
    '                      ", ISNULL(Tenure.emi_amount, Loan.emi_amount) AS emi_amount " & _
    '                      ", ISNULL(Tenure.emi_amountPaidCurrency, Loan.emi_amountPaidCurrency) AS emi_amountPaidCurrency " & _
    '                      ", ISNULL(Tenure.emi_principal_amount, Loan.emi_amount) AS emi_principal_amount " & _
    '                      ", Loan.basecurrency_amount AS advance_amount " & _
    '                      ", Loan.isloan " & _
    '                      ", Loan.balance_amount " & _
    '                      ", Loan.balance_amountPaidCurrency " & _
    '                      ", Loan.opening_balance " & _
    '                      ", Loan.opening_balancePaidCurrency " & _
    '                      ", Loan.calctype_id " & _
    '                      ", Loan.basecurrency_amount AS loan_amount " & _
    '                      ", Loan.loan_advance_amount AS loan_advance_amountPaidCurrency " & _
    '                      ", Loan.countryunkid " & _
    '                      ", ISNULL(ExRate.exchange_rate, Loan.exchange_rate) AS exchange_rate " & _
    '                      ", ISNULL(Currency.currency_sign, '') AS currency_sign " & _
    '                      ", Status.statusunkid " & _
    '                      ", Status.StatusPeriodEndDate " & _
    '                      ", CAST(CASE Status.statusunkid WHEN 2 THEN 1 ELSE 0 END AS BIT) AS isonhold " & _
    '                      ", Status.iscalculateinterest " & _
    '                      ", ExRate.exchangerateunkid AS currencyunkid " & _
    '                      ", ISNULL(IntRate.IntRateEffectiveDate, Loan.loaneffectivedate) AS IntRateEffectiveDate " & _
    '                      ", ISNULL(IntRate.interest_rate, Loan.interest_rate) AS interest_rate " & _
    '                      ", ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate) AS TenureEffectiveDate " & _
    '                      ", ISNULL(Tenure.emi_tenure, Loan.emi_tenure) AS emi_tenure " & _
    '                      ", ISNULL(Balance.bf_amount, Loan.basecurrency_amount) AS BF_Amount " & _
    '                      ", ISNULL(Balance.bf_amountPaidCurrency, Loan.loan_advance_amount) AS BF_AmountPaidCurrency " & _
    '                      ", ISNULL(Balance.cf_amount, Loan.basecurrency_amount) AS PrincipalBalance " & _
    '                      ", ISNULL(Balance.cf_amountPaidCurrency, Loan.loan_advance_amount) AS PrincipalBalancePaidCurrency " & _
    '                      ", ISNULL(Balance.cf_balance, CASE Loan.isbrought_forward WHEN 0 THEN Loan.basecurrency_amount ELSE Loan.opening_balance END) AS BalanceAmount " & _
    '                      ", ISNULL(Balance.cf_balancePaidCurrency, CASE Loan.isbrought_forward WHEN 0 THEN Loan.loan_advance_amount  ELSE Loan.opening_balancePaidCurrency END) AS BalanceAmountPaidCurrency " & _
    '                      ", ISNULL(Balance.BalanceEffectiveDate, Loan.loaneffectivedate) AS BalanceEffectiveDate " & _
    '                      ", ISNULL(Tenure.TenureEndDate, DATEADD(DAY, -1, DATEADD(MONTH, Tenure.emi_tenure, Tenure.TenureEffectiveDate))) AS LoanEndDate " & _
    '                      ", DATEDIFF(DAY, ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate), ISNULL(Tenure.TenureEndDate, CONVERT(CHAR(8), DATEADD(DAY, -1, DATEADD(MONTH, ISNULL(Tenure.emi_tenure, Loan.emi_tenure), ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate))), 112))) + 1 AS TenureDays " & _
    '                      ", ISNULL(Balance.nexteffective_days, DATEDIFF(DAY, ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate), ISNULL(Tenure.TenureEndDate, CONVERT(CHAR(8), DATEADD(DAY, -1, DATEADD(MONTH, ISNULL(Tenure.emi_tenure, Loan.emi_tenure), ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate))), 112))) + 1) AS nexteffective_days " & _
    '                      ", ISNULL(Balance.nexteffective_months, DATEDIFF(MONTH, ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate), ISNULL(Tenure.TenureEndDate, CONVERT(CHAR(8), DATEADD(DAY, -1, DATEADD(MONTH, ISNULL(Tenure.emi_tenure, Loan.emi_tenure), ISNULL(Tenure.TenureEffectiveDate, Loan.loaneffectivedate))), 112))) + 1) AS nexteffective_months " & _
    '                      ", 0 AS DaysDiff " & _
    '                      ", 0.00 AS TotPMTAmount " & _
    '                      ", 0.00 AS TotPMTAmountPaidCurrency " & _
    '                      ", 0.00 AS TotInterestAmount " & _
    '                      ", 0.00 AS TotInterestAmountPaidCurrency " & _
    '                      ", 0.00 AS TotPrincipalAmount " & _
    '                      ", 0.00 AS TotPrincipalAmountPaidCurrency " & _
    '                      ", 0.00 AS LastProjectedBalance " & _
    '                      ", 0.00 AS LastProjectedBalancePaidCurrency " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyDeduction, 0) AS TotalMonthlyDeduction " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyDeductionPaidCurrency, 0) AS TotalMonthlyDeductionPaidCurrency " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyInterest, 0) AS TotalMonthlyInterest " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyInterestPaidCurrency, 0) AS TotalMonthlyInterestPaidCurrency " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyRepayment, 0) AS TotalMonthlyRepayment " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyRepaymentPaidCurrency, 0) AS TotalMonthlyRepaymentPaidCurrency " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyTopup, 0) AS TotalMonthlyTopup " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyTopupPaidCurrency, 0) AS TotalMonthlyTopupPaidCurrency " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyPrincipalAmt, 0) AS TotalMonthlyPrincipalAmt " & _
    '                      ", ISNULL(MonthlyDeduction.TotalMonthlyPrincipalAmtPaidCurrency, 0) AS TotalMonthlyPrincipalAmtPaidCurrency " & _
    '                      ", Loan.employeeunkid " & _
    '                      ", ISNULL(Balance.transaction_periodunkid,Loan.deductionperiodunkid) AS transactionperiodunkid " & _
    '                      ", ISNULL(Balance.periodunkid,Loan.deductionperiodunkid) AS periodunkid " & _
    '                      ", Loan.interest_calctype_id " & _
    '                      ", Loan.loan_statusunkid "
    '        'Sohail (25 Jan 2016) - [StatusPeriodEndDate]
    '        'Sohail (13 Jan 2016) - [loan_statusunkid]
    '        'Sohail (15 Dec 2015) - [interest_calctype_id, nexteffective_months, emi_principal_amount]

    '        If blnForReport = True Then
    '            strQ &= "  , ISNULL(Balance.TranPeriodName, Loan.PeriodName) AS TranPeriodName " & _
    '                      ", ISNULL(Balance.PeriodName, Loan.PeriodName) AS PeriodName " & _
    '                      ", ISNULL(CurrMonth.periodunkid, 0) AS CurrMonthPeriodUnkId " & _
    '                      ", ISNULL(CurrMonth.period_name, '') AS CurrMonthPeriodName " & _
    '                      ", ISNULL(Loan.PeriodName, '') AS DeductionPeriodName " & _
    '                      ", Loan.loanvoucher_no " & _
    '                      ", Loan.LoanSchemeCode " & _
    '                      ", Loan.LoanSchemeName " & _
    '                      ", Loan.EmpName " & _
    '                      ", Loan.EmpCode " & _
    '                      ", ISNULL(Balance.TranPeriod_end_date, Loan.end_date) AS TranPeriodend_date  " & _
    '                      ", ISNULL(CurrMonth.start_date, Loan.loaneffectivedate) AS CurrMonthPeriod_start_date  " & _
    '                      ", ISNULL(CurrMonth.end_date, Loan.end_date) AS CurrMonthPeriod_end_date  " & _
    '                      ", ISNULL(Balance.end_date, Loan.end_date) AS end_date  " & _
    '                      ", ISNULL(M.membershipno, '') AS MembershipNo " & _
    '                      ", ISNULL(prescribed_interest_rate, 0) AS prescribed_interest_rate " & _
    '                      ", ISNULL(Loan.isshowloanbalonpayslip, 0) AS isshowloanbalonpayslip " & _
    '                      ", Loan.loan_exchange_rate AS LoanExchangeRate " & _
    '                      ", Loan.net_amount "
    '            'Sohail (13 Dec 2016) - [loan_exchange_rate, net_amount]

    '            'Sohail (13 Dec 2016) -- Start
    '            'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '            strQ &= ", CASE Loan.calctype_id "
    '            For Each pair In dicLoanCalcType
    '                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
    '            Next
    '            strQ &= " END AS calctype "

    '            strQ &= ", CASE Loan.interest_calctype_id "
    '            For Each pair In dicLoanIntCalcType
    '                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
    '            Next
    '            strQ &= " END AS interest_calctype "
    '            'Sohail (13 Dec 2016) -- End
    '        End If
    '        'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
    '        'Nilay (10-Oct-2015) -- [MembershipNo], [prescribed_interest_rate]

    '        strQ &= "FROM    ( SELECT    lnloan_advance_tran.loanadvancetranunkid  " & _
    '                                  ", ISNULL(lnloan_emitenure_tran.basecurrency_amount, lnloan_advance_tran.emi_amount) AS emi_amount " & _
    '                                  ", ISNULL(lnloan_emitenure_tran.emi_amount, lnloan_advance_tran.emi_amount * lnloan_advance_tran.exchange_rate) AS emi_amountPaidCurrency " & _
    '                                  ", lnloan_advance_tran.advance_amount " & _
    '                                  ", lnloan_advance_tran.isloan " & _
    '                                  ", lnloan_advance_tran.balance_amount " & _
    '                                  ", lnloan_advance_tran.balance_amountPaidCurrency " & _
    '                                  ", lnloan_advance_tran.loan_amount " & _
    '                                  ", lnloan_advance_tran.opening_balance " & _
    '                                  ", lnloan_advance_tran.opening_balancePaidCurrency " & _
    '                                  ", CASE lnloan_advance_tran.isloan WHEN 1 THEN lnloan_advance_tran.loan_amount ELSE lnloan_advance_tran.advance_amount END AS loan_advance_amount " & _
    '                                  ", lnloan_advance_tran.calctype_id " & _
    '                                  ", ISNULL(lnloan_advance_tran.basecurrency_amount, lnloan_advance_tran.loan_amount) AS basecurrency_amount " & _
    '                                  ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS loaneffectivedate " & _
    '                                  ", lnloan_advance_tran.loanschemeunkid " & _
    '                                  ", lnloan_advance_tran.employeeunkid " & _
    '                                  ", lnloan_advance_tran.interest_rate " & _
    '                                  ", lnloan_advance_tran.emi_tenure " & _
    '                                  ", lnloan_advance_tran.countryunkid " & _
    '                                  ", lnloan_advance_tran.exchange_rate " & _
    '                                  ", lnloan_advance_tran.deductionperiodunkid " & _
    '                                  ", lnloan_advance_tran.isbrought_forward " & _
    '                                  ", cfcommon_period_tran.prescribed_interest_rate " & _
    '                                  ", ISNULL(lnloan_advance_tran.interest_calctype_id, 1) AS interest_calctype_id " & _
    '                                  ", lnloan_advance_tran.loan_statusunkid " & _
    '                                  ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS end_date  " & _
    '                                  ", DENSE_RANK() OVER ( PARTITION BY lnloan_emitenure_tran.loanadvancetranunkid ORDER BY lnloan_emitenure_tran.effectivedate DESC ) AS ROWNO "
    '        'Sohail (25 Jan 2016) -- [", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS end_date  " & _]
    '        'Sohail (13 Jan 2016) - [loan_statusunkid]
    '        'Sohail (15 Dec 2015) - [interest_calctype_id]
    '        'Nilay (10-Oct-2015) -- [prescribed_interest_rate]

    '        If blnForReport = True Then

    '            strQ &= "              , TableDeductionPeriod.period_name AS PeriodName " & _
    '                                  ", lnloan_advance_tran.loanvoucher_no " & _
    '                                  ", lnloan_scheme_master.code AS LoanSchemeCode " & _
    '                                  ", lnloan_scheme_master.name AS LoanSchemeName  " & _
    '                                  ", hremployee_master.employeecode AS EmpCode  " & _
    '                                  ", ISNULL(lnloan_scheme_master.isshowloanbalonpayslip, 0) AS isshowloanbalonpayslip " & _
    '                                  ", lnloan_advance_tran.exchange_rate AS loan_exchange_rate " & _
    '                                  ", lnloan_advance_tran.net_amount "
    '            'Sohail (13 Dec 2016) - [loan_exchange_rate, net_amount]
    '            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
    '            'Sohail (25 Jan 2016) -- [", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS end_date  " & _]

    '            If blnFirstNameThenSurname = False Then
    '                strQ &= "          , ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
    '            Else
    '                strQ &= "          , ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
    '            End If
    '        End If

    '        strQ &= "          FROM      lnloan_advance_tran " & _
    '                                    "LEFT JOIN lnloan_emitenure_tran ON lnloan_emitenure_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
    '                                    "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
    '                                                                              "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " "

    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            strQ &= " LEFT JOIN #TBLnAdv ON #TBLnAdv.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid "
    '        End If
    '        'Nilay (04-Nov-2016) -- End

    '        If blnForReport = True Then

    '            'Nilay (10-Oct-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'strQ &= "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_advance_tran.employeeunkid " & _
    '            '                        "LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid "

    '            'Nilay (13-Oct-2016) -- Start
    '            'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
    '            'strQ &= "                LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_advance_tran.employeeunkid " & _
    '            'strAdvanceQueryJoin & " " & _
    '            'strEmployeeAlloctionJoin & " " & _
    '            '                        "LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '            '                        "   LEFT JOIN " & _
    '            '                            "   ( " & _
    '            '                            "      SELECT " & _
    '            '                            "           stationunkid " & _
    '            '                            "          ,employeeunkid " & _
    '            '                            "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '            '                            "      FROM hremployee_transfer_tran " & _
    '            '                            "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '            '                            "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
    '            strQ &= "                LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_advance_tran.employeeunkid " & _
    '            strAdvanceQueryJoin & " " & _
    '                                    "LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '                                    "   LEFT JOIN " & _
    '                                        "   ( " & _
    '                                        "      SELECT " & _
    '                                        "           stationunkid " & _
    '                                        "          ,employeeunkid " & _
    '                                        "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                                        "      FROM hremployee_transfer_tran " & _
    '                                        "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                                        "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
    '            'Nilay (13-Oct-2016) -- End


    '            'Nilay (10-Oct-2015) -- End

    '            'Nilay (25-Mar-2016) -- Start
    '        Else
    '            strQ &= "                    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_advance_tran.employeeunkid "
    '            'Nilay (25-Mar-2016) -- End

    '        End If

    '        'Nilay (09-Dec-2015) -- Start

    '        'Nilay (25-Mar-2016) -- Start
    '        If xDateJoinQry.Trim.Length > 0 Then
    '            strQ &= xDateJoinQry
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            strQ &= xUACQry
    '        End If
    '        'Nilay (25-Mar-2016) -- End

    '        'Nilay (21-Dec-2016) -- Start
    '        'ISSUE: On loan report & loan advance report when future deduction period is assigned then future advance displays in current open period as well. To overcome AND lnloan_advance_tran.effective_date <= @PeriodStartDate 
    '        'strQ &= "          WHERE     ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '        '                            "AND ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 " & _
    '        '                            "AND TableDeductionPeriod.start_date <= @AsOnDate " & _
    '        '                            strCompletedFilter & _
    '        '                            strFilter
    '        strQ &= "          WHERE     ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                                    "AND ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 " & _
    '                                    "AND TableDeductionPeriod.start_date <= @AsOnDate " & _
    '                                    "AND lnloan_advance_tran.effective_date <= @PeriodStartDate " & _
    '                                    strCompletedFilter & _
    '                                    strFilter
    '        'Nilay (21-Dec-2016) -- End

    '        'Nilay (25-Mar-2016) -- Start
    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            strQ &= " AND " & xUACFiltrQry
    '        End If

    '        If xDateFilterQry.Trim.Length > 0 Then
    '            strQ &= xDateFilterQry
    '        End If
    '        'Nilay (25-Mar-2016) -- End

    '        'strQ &= "          WHERE     ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '        '                            "AND ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 " & _
    '        '                            "AND TableDeductionPeriod.start_date < @AsOnDate " & _
    '        '                            strCompletedFilter & _
    '        '                            strFilter
    '        'Nilay (09-Dec-2015) -- End


    '        If blnForReport = True Then
    '            If strExtraReportFilterString.Trim.Length > 0 Then
    '                strQ &= strExtraReportFilterString
    '            End If
    '        End If
    '        strQ &= ") AS Loan " & _
    '                        "JOIN ( SELECT   lnloan_advance_tran.loanadvancetranunkid  " & _
    '                                      ", lnloan_status_tran.statusunkid " & _
    '                                      ", lnloan_status_tran.iscalculateinterest " & _
    '                                      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS StatusPeriodEndDate " & _
    '                                      ", DENSE_RANK() OVER ( PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY CONVERT(CHAR(8),lnloan_status_tran.status_date,112) DESC, lnloan_status_tran.loanstatustranunkid DESC ) AS ROWNO " & _
    '                               "FROM     lnloan_advance_tran " & _
    '                                        "LEFT JOIN lnloan_status_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_status_tran.loanadvancetranunkid " & _
    '                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_status_tran.periodunkid " & _
    '                                        "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
    '                                                                              "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " "
    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            strQ &= " LEFT JOIN #TBLnAdv ON #TBLnAdv.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid "
    '        End If
    '        'Nilay (04-Nov-2016) -- End

    '        strQ &= "WHERE    ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                                        "AND ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
    '                                        "AND TableDeductionPeriod.start_date <= @AsOnDate " & _
    '                                        strCompletedFilter & _
    '                                        strFilter & _
    '                             ") AS Status ON Loan.loanadvancetranunkid = Status.loanadvancetranunkid " & _
    '                        "JOIN ( SELECT   lnloan_advance_tran.loanadvancetranunkid  " & _
    '                                      ", ISNULL(cfexchange_rate.exchange_rate, lnloan_advance_tran.exchange_rate) AS exchange_rate " & _
    '                                      ", ISNULL(cfexchange_rate.exchangerateunkid, 1) AS exchangerateunkid " & _
    '                                      ", DENSE_RANK() OVER ( PARTITION BY lnloan_advance_tran.loanadvancetranunkid ORDER BY exchange_date DESC, exchangerateunkid DESC ) AS ROWNO " & _
    '                               "FROM     lnloan_advance_tran " & _
    '                                        "LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
    '                                        "LEFT JOIN cfexchange_rate ON cfexchange_rate.countryunkid = lnloan_advance_tran.countryunkid " & _
    '                                        "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
    '                                                                              "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
    '                                        "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
    '                                                                    "AND prpayment_tran.isvoid = 0 " & _
    '                                                                    "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
    '                                                                    "AND prpayment_tran.referenceid IN ( " & clsPayment_tran.enPaymentRefId.LOAN & ", " & clsPayment_tran.enPaymentRefId.ADVANCE & " ) "
    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            strQ &= " LEFT JOIN #TBLnAdv ON #TBLnAdv.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid "
    '        End If
    '        'Nilay (04-Nov-2016) -- End
    '        strQ &= " WHERE    ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                                        "AND ISNULL(lnloan_process_pending_loan.isvoid, 0) = 0 " & _
    '                                        "AND cfexchange_rate.isactive = 1 " & _
    '                                        "AND TableDeductionPeriod.start_date <= @AsOnDate " & _
    '                                        strCompletedFilter & _
    '                                        strFilter
    '        'Nilay (20-Sept-2016) -- Start
    '        'Enhancement : Cancel feature for approved but not assigned loan application
    '        'nloan_status_tran.status_date REPLACED BY CONVERT(CHAR(8),lnloan_status_tran.status_date,112)
    '        'Nilay (20-Sept-2016) -- End

    '        'Sohail (25 Jan 2016) - [StatusPeriodEndDate]

    '        If blnIncludeUnPaidLoan = False Then
    '            strQ &= "                   AND ( prpayment_tran.paymenttranunkid IS NOT NULL " & _
    '                                              "OR lnloan_process_pending_loan.isexternal_entity = 1 " & _
    '                                            ") "
    '        End If

    '        strQ &= "             ) AS ExRate ON Loan.loanadvancetranunkid = ExRate.loanadvancetranunkid " & _
    '                        "LEFT JOIN ( SELECT  lnloan_interest_tran.loanadvancetranunkid  " & _
    '                                          ", CONVERT(CHAR(8), lnloan_interest_tran.effectivedate, 112) AS IntRateEffectiveDate " & _
    '                                          ", lnloan_interest_tran.interest_rate " & _
    '                                          ", DENSE_RANK() OVER ( PARTITION BY lnloan_interest_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_interest_tran.effectivedate DESC, lnloan_interest_tran.lninteresttranunkid DESC ) AS ROWNO " & _
    '                                    "FROM    lnloan_interest_tran " & _
    '                                            "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
    '                                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
    '                                                                              "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "
    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            strQ &= " LEFT JOIN #TBLnAdv ON #TBLnAdv.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid "
    '        End If
    '        'Nilay (04-Nov-2016) -- End
    '        strQ &= "WHERE   ISNULL(lnloan_interest_tran.isvoid, 0) = 0 " & _
    '                                            "AND lnloan_advance_tran.isvoid = 0 " & _
    '                                            "AND CONVERT(CHAR(8), lnloan_interest_tran.effectivedate, 112) <= @AsOnDate " & _
    '                                            strFilter & _
    '                                  ") AS IntRate ON Loan.loanadvancetranunkid = IntRate.loanadvancetranunkid " & _
    '                        "LEFT JOIN ( SELECT  lnloan_emitenure_tran.loanadvancetranunkid  " & _
    '                                          ", CONVERT(CHAR(8), lnloan_emitenure_tran.effectivedate, 112) AS TenureEffectiveDate " & _
    '                                          ", lnloan_emitenure_tran.emi_tenure " & _
    '                                          ", lnloan_emitenure_tran.basecurrency_amount AS emi_amount " & _
    '                                          ", lnloan_emitenure_tran.emi_amount AS emi_amountPaidCurrency " & _
    '                                          ", CONVERT(CHAR(8), lnloan_emitenure_tran.end_date, 112) AS TenureEndDate " & _
    '                                          ", lnloan_emitenure_tran.principal_amount AS emi_principal_amount " & _
    '                                          ", DENSE_RANK() OVER ( PARTITION BY lnloan_emitenure_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_emitenure_tran.effectivedate DESC, lnloan_emitenure_tran.lnemitranunkid DESC ) AS ROWNO " & _
    '                                    "FROM    lnloan_emitenure_tran " & _
    '                                            "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
    '                                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
    '                                                                              "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "
    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            strQ &= " LEFT JOIN #TBLnAdv ON #TBLnAdv.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid "
    '        End If
    '        'Nilay (04-Nov-2016) -- End
    '        strQ &= "WHERE   ISNULL(lnloan_emitenure_tran.isvoid, 0) = 0 " & _
    '                                            "AND lnloan_advance_tran.isvoid = 0 " & _
    '                                            "AND CONVERT(CHAR(8), lnloan_emitenure_tran.effectivedate, 112) <= @AsOnDate " & _
    '                                            strCompletedFilter.Replace("TableDeductionPeriod", "cfcommon_period_tran") & _
    '                                            strFilter & _
    '                                  ") AS Tenure ON Loan.loanadvancetranunkid = Tenure.loanadvancetranunkid " & _
    '                        "LEFT JOIN ( SELECT  lnloan_balance_tran.loanadvancetranunkid  " & _
    '                                          ", lnloan_balance_tran.loanbalancetranunkid " & _
    '                                          ", lnloan_balance_tran.cf_balance " & _
    '                                          ", lnloan_balance_tran.cf_balancePaidCurrency " & _
    '                                          ", lnloan_balance_tran.bf_amount " & _
    '                                          ", lnloan_balance_tran.bf_amountPaidCurrency " & _
    '                                          ", lnloan_balance_tran.cf_amount " & _
    '                                          ", lnloan_balance_tran.cf_amountPaidCurrency " & _
    '                                          ", lnloan_balance_tran.nexteffective_days " & _
    '                                          ", lnloan_balance_tran.nexteffective_months " & _
    '                                          ", lnloan_balance_tran.transaction_periodunkid " & _
    '                                          ", lnloan_balance_tran.periodunkid " & _
    '                                          ", CONVERT(CHAR(8), lnloan_balance_tran.end_date + 1, 112) AS BalanceEffectiveDate " & _
    '                                          ", DENSE_RANK() OVER ( PARTITION BY lnloan_balance_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_balance_tran.end_date DESC, lnloan_balance_tran.loanbalancetranunkid DESC ) AS ROWNO "
    '        'Sohail (15 Dec 2015) - [nexteffective_months, emi_principal_amount]

    '        If blnForReport = True Then
    '            strQ &= "                      , TranPeriod.period_name AS TranPeriodName " & _
    '                                          ", CONVERT(CHAR(8), TranPeriod.end_date, 112) AS TranPeriod_end_date " & _
    '                                          ", cfcommon_period_tran.period_name AS PeriodName " & _
    '                                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date "
    '        End If

    '        strQ &= "                     FROM    lnloan_balance_tran " & _
    '                                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_balance_tran.periodunkid " & _
    '                                                                              "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
    '                                            "LEFT JOIN cfcommon_period_tran AS TranPeriod ON TranPeriod.periodunkid = lnloan_balance_tran.transaction_periodunkid " & _
    '                                                                              "AND TranPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
    '                                    "WHERE   ISNULL(lnloan_balance_tran.isvoid, 0) = 0 " & _
    '                                            "AND CONVERT(CHAR(8), lnloan_balance_tran.end_date, 112) < @AsOnDate "

    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        'strFilter.Replace("#lnloan_advance_tran", "lnloan_balance_tran")
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            strQ &= strFilter.Replace("#TBLnAdv", "lnloan_balance_tran")
    '        Else
    '            strQ &= strFilter.Replace("lnloan_advance_tran", "lnloan_balance_tran")
    '        End If
    '        'Nilay (04-Nov-2016) -- End

    '        strQ &= ") AS Balance ON Loan.loanadvancetranunkid = Balance.loanadvancetranunkid " & _
    '                        "LEFT JOIN ( SELECT  lnloan_balance_tran.loanadvancetranunkid  " & _
    '                                          ", SUM(lnloan_balance_tran.amount) AS TotalMonthlyDeduction " & _
    '                                          ", SUM(lnloan_balance_tran.amountPaidCurrency) AS TotalMonthlyDeductionPaidCurrency " & _
    '                                          ", SUM(lnloan_balance_tran.interest_amount) AS TotalMonthlyInterest " & _
    '                                          ", SUM(lnloan_balance_tran.interest_amountPaidCurrency) AS TotalMonthlyInterestPaidCurrency " & _
    '                                          ", SUM(lnloan_balance_tran.repayment_amount) AS TotalMonthlyRepayment " & _
    '                                          ", SUM(lnloan_balance_tran.repayment_amountPaidCurrency) AS TotalMonthlyRepaymentPaidCurrency " & _
    '                                          ", SUM(lnloan_balance_tran.topup_amount) AS TotalMonthlyTopup " & _
    '                                          ", SUM(lnloan_balance_tran.topup_amountPaidCurrency) AS TotalMonthlyTopupPaidCurrency " & _
    '                                          ", SUM(lnloan_balance_tran.principal_amount) AS TotalMonthlyPrincipalAmt " & _
    '                                          ", SUM(lnloan_balance_tran.principal_amountPaidCurrency) AS TotalMonthlyPrincipalAmtPaidCurrency " & _
    '                                          ", 1 AS ROWNO " & _
    '                                    "FROM    lnloan_balance_tran " & _
    '                                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_balance_tran.periodunkid " & _
    '                                                                              "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
    '                                    "WHERE   ISNULL(lnloan_balance_tran.isvoid, 0) = 0 " & _
    '                                            "AND CONVERT(CHAR(8), lnloan_balance_tran.transactiondate, 112) BETWEEN @PeriodStartDate AND @PrevAsOnDate "
    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        'strFilter.Replace("#lnloan_advance_tran", "lnloan_balance_tran")
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            strQ &= strFilter.Replace("#TBLnAdv", "lnloan_balance_tran")
    '        Else
    '            strQ &= strFilter.Replace("lnloan_advance_tran", "lnloan_balance_tran")
    '        End If
    '        'Nilay (04-Nov-2016) -- End

    '        strQ &= " GROUP BY lnloan_balance_tran.loanadvancetranunkid " & _
    '                                  ") AS MonthlyDeduction ON Loan.loanadvancetranunkid = MonthlyDeduction.loanadvancetranunkid " & _
    '                        "LEFT JOIN ( SELECT DISTINCT " & _
    '                                            "cfexchange_rate.countryunkid  " & _
    '                                          ", cfexchange_rate.currency_name " & _
    '                                          ", cfexchange_rate.currency_sign " & _
    '                                    "FROM    cfexchange_rate " & _
    '                                    "WHERE   cfexchange_rate.isactive = 1 " & _
    '                                  ") AS Currency ON Loan.countryunkid = Currency.countryunkid " & _
    '                        "LEFT JOIN ( SELECT TOP 1 " & _
    '                                            "cfcommon_period_tran.periodunkid  " & _
    '                                          ", cfcommon_period_tran.period_code " & _
    '                                          ", cfcommon_period_tran.period_name " & _
    '                                          ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
    '                                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
    '                                    "FROM    cfcommon_period_tran " & _
    '                                    "WHERE   cfcommon_period_tran.isactive = 1 " & _
    '                                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
    '                                            "AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) = @PeriodStartDate " & _
    '                                            "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) = @PrevAsOnDate " & _
    '                                  ") AS CurrMonth ON 1 = 1 "

    '        'Nilay (10-Oct-2015) -- Start

    '        strQ &= "   LEFT JOIN ( " & _
    '                        "               SELECT  employeeunkid " & _
    '                        "                     , membershipno = ISNULL(STUFF(( SELECT '; ' + membershipno " & _
    '                        "                                                     FROM hremployee_master " & _
    '                        "                                                       LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
    '                        "                                                     WHERE membershipno IS NOT NULL " & _
    '                        "                                                           AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
    '                        "                                                           AND hremployee_master.employeeunkid = a.employeeunkid "

    '        If strEmployeeIDs.Trim <> "" Then
    '            strQ &= " AND hremployee_master.employeeunkid IN (" & strEmployeeIDs & ") "
    '        End If

    '        strQ &= "                                                     FOR " & _
    '                        "                                                       XML PATH('')), 1, 2, ''), '') " & _
    '                        "               FROM hremployee_master a " & _
    '                        "               GROUP BY employeeunkid " & _
    '                        "             ) AS M ON M.employeeunkid = Loan.employeeunkid "

    '        'Nilay (10-Oct-2015) -- End

    '        strQ &= "WHERE   Loan.ROWNO = 1 " & _
    '                        "AND Status.ROWNO = 1 " & _
    '                        "AND ExRate.ROWNO = 1 " & _
    '                        "AND ISNULL(IntRate.ROWNO, 1) = 1 " & _
    '                        "AND ISNULL(Tenure.ROWNO, 1) = 1 " & _
    '                        "AND ISNULL(Balance.ROWNO, 1) = 1 " & _
    '                        "AND ISNULL(MonthlyDeduction.ROWNO, 1) = 1 " & _
    '                        "AND NOT (Status.statusunkid IN (" & enLoanStatus.WRITTEN_OFF & ", " & enLoanStatus.COMPLETED & ") AND ISNULL(CurrMonth.end_date, Loan.end_date) > Status.StatusPeriodEndDate ) "
    '        'Sohail (25 Jan 2016) -- [AND NOT (Status.statusunkid IN (" & enLoanStatus.WRITTEN_OFF & ", " & enLoanStatus.COMPLETED & ") AND ISNULL(CurrMonth.end_date, Loan.end_date) > Status.StatusPeriodEndDate ) ]

    '        If blnIncludeOnHoldWrittenOff = False Then
    '            strQ &= " AND ((Status.statusunkid = " & enLoanStatus.IN_PROGRESS & ") OR (Status.statusunkid = " & enLoanStatus.ON_HOLD & " AND Status.iscalculateinterest = 1 )) "
    '        End If

    '        If blnIncludeCompletedLoanAdvance = False Then
    '            strQ &= " AND (ISNULL(Balance.cf_amount, Loan.loan_amount) > 0 OR Loan.isloan = 0) AND NOT (Status.statusunkid = " & enLoanStatus.COMPLETED & " OR Status.statusunkid = " & enLoanStatus.WRITTEN_OFF & " ) "
    '        End If

    '        'Sohail (18 Jan 2016) -- Start
    '        'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
    '        If intLoanStatus > 0 Then
    '            'Sohail (29 Jan 2016) -- Start
    '            'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
    '            'strQ &= " AND Status.statusunkid = " & intLoanStatus & " "
    '            If intLoanStatus <> enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED Then
    '                strQ &= " AND Status.statusunkid = " & intLoanStatus & " "
    '            Else
    '                'strQ &= " AND (Status.statusunkid = " & enLoanStatus.IN_PROGRESS & ") "
    '            End If
    '            'Sohail (29 Jan 2016) -- End
    '        End If
    '        'Sohail (18 Jan 2016) -- End



    '        objDataOperation.AddParameter("@AsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
    '        objDataOperation.AddParameter("@PrevAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate.AddDays(-1)))

    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If strLoanAdvanceTranIDs.Trim <> "" Then
    '            strQ &= " DROP TABLE #TBLnAdv "
    '        End If
    '        'Nilay (04-Nov-2016) -- End

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Sohail (13 Dec 2016) -- Start
    '        'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '        Dim dtTemp As DataTable = dsList.Tables(0).Clone
    '        'Sohail (13 Dec 2016) -- End

    '        For Each dsRow As DataRow In dsList.Tables(strTableName).Rows

    '            With dsRow
    '                Dim decPrincipalBalance As Decimal = CDec(.Item("PrincipalBalance").ToString) 'Amount on which PMT will be calculated for all type loan
    '                Dim decPrincipalBalancePaidCurrency As Decimal = CDec(.Item("PrincipalBalancePaidCurrency").ToString) 'Amount on which PMT will be calculated for all type loan
    '                Dim decCFBalance As Decimal = CDec(.Item("BalanceAmount").ToString) 'Latest CF Balance on which last projected amount will be calculated
    '                Dim decCFBalancePaidCurrency As Decimal = CDec(.Item("BalanceAmountPaidCurrency").ToString) 'Latest CF Balance on which last projected amount will be calculated
    '                Dim decBFAmount As Decimal = CDec(.Item("BF_Amount").ToString) 'Amount on which interest will be calculated for simple interest
    '                Dim decBFAmountPaidCurrency As Decimal = CDec(.Item("BF_AmountPaidCurrency").ToString) 'Amount on which interest will be calculated for simple interest
    '                Dim intNextEffectiveDays As Integer = CInt(.Item("nexteffective_days").ToString)
    '                Dim intNextEffectiveMonths As Integer = CInt(.Item("nexteffective_months").ToString) 'Sohail (15 Dec 2015)
    '                Dim decIntRate As Decimal = CDec(.Item("interest_rate").ToString)
    '                Dim decExRate As Decimal = CDec(.Item("exchange_rate").ToString)
    '                Dim intDaysDiff As Integer = DateDiff(DateInterval.Day, eZeeDate.convertDate(.Item("BalanceEffectiveDate").ToString), dtAsOnDate.AddDays(-1)) + 1
    '                Dim intMonthsDiff As Integer = DateDiff(DateInterval.Month, eZeeDate.convertDate(.Item("BalanceEffectiveDate").ToString), dtAsOnDate.AddDays(-1)) + 1
    '                Dim intLoanAdvanceStatus As Integer = CInt(.Item("loan_statusunkid").ToString) 'Sohail (13 Jan 2016)
    '                'Sohail (13 Dec 2016) -- Start
    '                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                'Dim dtLoanEndDate As Date = eZeeDate.convertDate(.Item("loanenddate").ToString) 'Sohail (13 Dec 2016)
    '                Dim dtLoanEndDate As Date
    '                If IsDBNull(.Item("loanenddate")) = True Then
    '                    dtLoanEndDate = dtAsOnDate.AddDays(-1)
    '                Else
    '                    dtLoanEndDate = eZeeDate.convertDate(.Item("loanenddate").ToString)
    '                End If
    '                'Sohail (13 Dec 2016) -- End

    '                mdecTotPMTAmt = 0
    '                mdecTotPMTAmtPaidCurrency = 0
    '                mdecTotIntAmt = 0
    '                mdecTotIntAmtPaidCurrency = 0
    '                mdecTotPrincipalAmt = 0
    '                mdecTotPrincipalAmtPaidCurrency = 0



    '                'Sohail (13 Dec 2016) -- Start
    '                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD Then
    '                    dtLoanEndDate = dtLoanEndDate.AddDays(1).AddMonths(1).AddDays(-1)
    '                End If

    '                Dim dtStart As Date = dtAsOnDate
    '                Dim intMonth As Integer = 0
    '                If blnAddProjectedPeriodDeductions = False Then
    '                    dtLoanEndDate = dtStart
    '                End If

    '                While dtStart.AddDays(-1) <= dtLoanEndDate

    '                    intMonth += 1
    '                    If intMonth > 1 Then
    '                        mdecTotPMTAmt = 0
    '                        mdecTotPMTAmtPaidCurrency = 0
    '                        mdecTotIntAmt = 0
    '                        mdecTotIntAmtPaidCurrency = 0
    '                        mdecTotPrincipalAmt = 0
    '                        mdecTotPrincipalAmtPaidCurrency = 0
    '                        'mdecLastProjectedBalance = 0
    '                        'mdecLastProjectedBalancePaidCurrency = 0

    '                        intDaysDiff = DateDiff(DateInterval.Day, dtStart.AddMonths(-1), dtStart.AddDays(-1)) + 1
    '                        intMonthsDiff = DateDiff(DateInterval.Month, dtStart.AddMonths(-1), dtStart.AddDays(-1)) + 1

    '                        decCFBalance = mdecLastProjectedBalance
    '                        decCFBalancePaidCurrency = mdecLastProjectedBalancePaidCurrency
    '                    End If
    '                    'Sohail (13 Dec 2016) -- End                        


    '                    Select Case CInt(.Item("calctype_id"))
    '                        Case enLoanCalcId.Simple_Interest
    '                            'Sohail (13 Dec 2016) -- Start
    '                            'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                            'If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False Then
    '                            If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False AndAlso intMonth <= 1 Then
    '                                'Sohail (13 Dec 2016) -- End
    '                                mdecTotPrincipalAmt = 0
    '                                mdecTotPrincipalAmtPaidCurrency = 0
    '                                mdecTotIntAmt = 0
    '                                mdecTotIntAmtPaidCurrency = 0
    '                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
    '                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
    '                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
    '                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
    '                                'Sohail (13 Dec 2016) -- Start
    '                                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                                'ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True Then
    '                            ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True AndAlso intMonth <= 1 Then
    '                                'Sohail (13 Dec 2016) -- End
    '                                mdecTotPrincipalAmt = 0
    '                                mdecTotPrincipalAmtPaidCurrency = 0
    '                                'Sohail (15 Dec 2015) -- Start
    '                                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '                                'mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
    '                                'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)
    '                                Select Case CInt(.Item("interest_calctype_id"))

    '                                    Case enLoanInterestCalcType.DAILY
    '                                        mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
    '                                        mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)

    '                                    Case enLoanInterestCalcType.MONTHLY
    '                                        mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (1200)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
    '                                        mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (1200)

    '                                End Select
    '                                'Sohail (15 Dec 2015) -- End

    '                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
    '                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
    '                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
    '                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
    '                            Else
    '                                'Sohail (15 Dec 2015) -- Start
    '                                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '                                'mdecTotPrincipalAmt = ((decPrincipalBalancePaidCurrency / intNextEffectiveDays) * intDaysDiff) / decExRate '(decPrincipalBalance / intNextEffectiveDays) * intDaysDiff
    '                                'mdecTotPrincipalAmtPaidCurrency = (decPrincipalBalancePaidCurrency / intNextEffectiveDays) * intDaysDiff
    '                                'mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
    '                                'mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)
    '                                Select Case CInt(.Item("interest_calctype_id"))

    '                                    Case enLoanInterestCalcType.DAILY
    '                                        mdecTotPrincipalAmt = ((decPrincipalBalancePaidCurrency / intNextEffectiveDays) * intDaysDiff) / decExRate '(decPrincipalBalance / intNextEffectiveDays) * intDaysDiff
    '                                        mdecTotPrincipalAmtPaidCurrency = (decPrincipalBalancePaidCurrency / intNextEffectiveDays) * intDaysDiff
    '                                        mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
    '                                        mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intDaysDiff) / (36500)

    '                                    Case enLoanInterestCalcType.MONTHLY
    '                                        mdecTotPrincipalAmt = ((decPrincipalBalancePaidCurrency / intNextEffectiveMonths) * intMonthsDiff) / decExRate '(decPrincipalBalance / intNextEffectiveDays) * intDaysDiff
    '                                        mdecTotPrincipalAmtPaidCurrency = (decPrincipalBalancePaidCurrency / intNextEffectiveMonths) * intMonthsDiff
    '                                        mdecTotIntAmt = ((decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (1200)) / decExRate '(decBFAmount * decIntRate * intDaysDiff) / (36500)
    '                                        mdecTotIntAmtPaidCurrency = (decBFAmountPaidCurrency * decIntRate * intMonthsDiff) / (1200)

    '                                End Select
    '                                'Sohail (15 Dec 2015) -- End

    '                                'Sohail (13 Jan 2016) -- Start
    '                                'Enhancement - Issue on Loan Report for Written off loans for KBC.
    '                                If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF Then
    '                                    mdecTotPrincipalAmt = 0
    '                                    mdecTotPrincipalAmtPaidCurrency = 0
    '                                    mdecTotIntAmt = 0
    '                                    mdecTotIntAmtPaidCurrency = 0
    '                                End If
    '                                'Sohail (13 Jan 2016) -- End

    '                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
    '                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
    '                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
    '                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
    '                            End If
    '                        Case enLoanCalcId.Compound_Interest
    '                            'If intDuration > 12 Then
    '                            '    mdecInterest_Amount = decPrincipalAmt * CDec(Pow((1 + (dblRate / 100)), (intDuration / 12)))
    '                            'Else
    '                            '    mdecInterest_Amount = decPrincipalAmt * CDec(Pow((1 + (dblRate / 100)), intDuration))
    '                            'End If
    '                            'mdecNetAmount = mdecInterest_Amount + decPrincipalAmt
    '                        Case enLoanCalcId.Reducing_Amount
    '                            Dim decPMT As Decimal = 0
    '                            Dim decPMTPaidCurrency As Decimal = 0
    '                            Dim mdecNewReduceAmt As Decimal = decCFBalance
    '                            Dim mdecNewReduceAmtPaidCurrency As Decimal = decCFBalancePaidCurrency
    '                            Dim decIntAmt As Decimal = 0
    '                            Dim decIntAmtPaidCurrency As Decimal = 0
    '                            Dim decPrincAmt As Decimal = 0
    '                            Dim decPrincAmtPaidCurrency As Decimal = 0

    '                            For i As Integer = 1 To intDaysDiff
    '                                'Sohail (13 Dec 2016) -- Start
    '                                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                                'If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False Then
    '                                If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False AndAlso intMonth <= 1 Then
    '                                    'Sohail (13 Dec 2016) -- End
    '                                    decPMT = 0
    '                                    decPMTPaidCurrency = 0
    '                                    decIntAmt = 0
    '                                    decIntAmtPaidCurrency = 0
    '                                    decPrincAmt = 0
    '                                    decPrincAmtPaidCurrency = 0
    '                                    mdecNewReduceAmt -= decPrincAmt
    '                                    mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
    '                                    'Sohail (13 Dec 2016) -- Start
    '                                    'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                                    'ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True Then
    '                                ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True AndAlso intMonth <= 1 Then
    '                                    'Sohail (13 Dec 2016) -- End
    '                                    'Sohail (15 Dec 2015) -- Start
    '                                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '                                    'decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                    'decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 36500
    '                                    Select Case CInt(.Item("interest_calctype_id"))

    '                                        Case enLoanInterestCalcType.DAILY
    '                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 36500

    '                                        Case enLoanInterestCalcType.MONTHLY
    '                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 1200) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 1200

    '                                    End Select
    '                                    'Sohail (15 Dec 2015) -- End
    '                                    decPMT = decIntAmt
    '                                    decPMTPaidCurrency = decIntAmtPaidCurrency
    '                                    decPrincAmt = 0
    '                                    decPrincAmtPaidCurrency = 0
    '                                    mdecNewReduceAmt -= decPrincAmt
    '                                    mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
    '                                Else
    '                                    'Sohail (15 Dec 2015) -- Start
    '                                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '                                    'decPMT = (Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalancePaidCurrency * -1))) / decExRate 'Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalance * -1))
    '                                    'decPMTPaidCurrency = Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalancePaidCurrency * -1))
    '                                    'decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                    'decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 36500
    '                                    Select Case CInt(.Item("interest_calctype_id"))

    '                                        Case enLoanInterestCalcType.DAILY
    '                                            decPMT = (Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalancePaidCurrency * -1))) / decExRate 'Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalance * -1))
    '                                            decPMTPaidCurrency = Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalancePaidCurrency * -1))
    '                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 36500

    '                                        Case enLoanInterestCalcType.MONTHLY
    '                                            decPMT = (Pmt(decIntRate / 1200, intNextEffectiveMonths, (decPrincipalBalancePaidCurrency * -1))) / decExRate 'Pmt(decIntRate / 36500, intNextEffectiveDays, (decPrincipalBalance * -1))
    '                                            decPMTPaidCurrency = Pmt(decIntRate / 1200, intNextEffectiveMonths, (decPrincipalBalancePaidCurrency * -1))
    '                                            decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 1200) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                            decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 1200

    '                                    End Select
    '                                    'Sohail (15 Dec 2015) -- End

    '                                    'Sohail (13 Jan 2016) -- Start
    '                                    'Enhancement - Issue on Loan Report for Written off loans for KBC.
    '                                    If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF Then
    '                                        decPMT = 0
    '                                        decPMTPaidCurrency = 0
    '                                        decIntAmt = 0
    '                                        decPrincAmtPaidCurrency = 0
    '                                    End If
    '                                    'Sohail (13 Jan 2016) -- End

    '                                    decPrincAmt = decPMT - decIntAmt
    '                                    decPrincAmtPaidCurrency = decPMTPaidCurrency - decIntAmtPaidCurrency
    '                                    mdecNewReduceAmt -= decPrincAmt
    '                                    mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
    '                                End If

    '                                mdecTotPMTAmt += decPMT
    '                                mdecTotPMTAmtPaidCurrency += decPMTPaidCurrency
    '                                mdecTotIntAmt += decIntAmt
    '                                mdecTotIntAmtPaidCurrency += decIntAmtPaidCurrency
    '                                mdecTotPrincipalAmt += decPrincAmt
    '                                mdecTotPrincipalAmtPaidCurrency += decPrincAmtPaidCurrency

    '                                'Sohail (15 Dec 2015) -- Start
    '                                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '                                If CInt(.Item("interest_calctype_id")) = enLoanInterestCalcType.MONTHLY Then
    '                                    Exit For
    '                                End If
    '                                'Sohail (15 Dec 2015) -- End
    '                            Next

    '                            mdecLastProjectedBalance = mdecNewReduceAmt
    '                            mdecLastProjectedBalancePaidCurrency = mdecNewReduceAmtPaidCurrency

    '                            'Sohail (13 Dec 2016) -- Start
    '                            'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                            If intMonth > 1 AndAlso mdecLastProjectedBalancePaidCurrency > 0.001 AndAlso dtStart > dtLoanEndDate Then
    '                                dtLoanEndDate = dtLoanEndDate.AddDays(1).AddMonths(1).AddDays(-1)
    '                            End If
    '                            'Sohail (13 Dec 2016) -- End

    '                        Case enLoanCalcId.No_Interest
    '                            'Sohail (13 Dec 2016) -- Start
    '                            'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                            If intMonth > 1 Then
    '                                decCFBalance = mdecLastProjectedBalance
    '                                decCFBalancePaidCurrency = mdecLastProjectedBalancePaidCurrency
    '                            End If
    '                            'Sohail (13 Dec 2016) -- End

    '                            'Sohail (13 Dec 2016) -- Start
    '                            'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                            'If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False Then
    '                            If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False AndAlso intMonth <= 1 Then
    '                                'Sohail (13 Dec 2016) -- End
    '                                mdecTotPrincipalAmt = 0
    '                                mdecTotPrincipalAmtPaidCurrency = 0
    '                                mdecTotIntAmt = 0
    '                                mdecTotIntAmtPaidCurrency = 0
    '                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
    '                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
    '                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
    '                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
    '                                'Sohail (13 Dec 2016) -- Start
    '                                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                                'ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True Then
    '                            ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True AndAlso intMonth <= 1 Then
    '                                'Sohail (13 Dec 2016) -- End
    '                                mdecTotPrincipalAmt = 0
    '                                mdecTotPrincipalAmtPaidCurrency = 0
    '                                mdecTotIntAmt = 0
    '                                mdecTotIntAmtPaidCurrency = 0
    '                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
    '                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
    '                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
    '                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency
    '                            Else
    '                                'mdecTotPrincipalAmt = (decPrincipalBalance / intNextEffectiveDays) * intDaysDiff
    '                                If blnFromProcessPayroll = True Then
    '                                    mdecTotPrincipalAmt = CDec(.Item("emi_amountPaidCurrency").ToString) / decExRate 'CDec(.Item("emi_amount").ToString) 'Now pick emi_amout from emi tenure change screen.
    '                                    mdecTotPrincipalAmtPaidCurrency = CDec(.Item("emi_amountPaidCurrency").ToString) 'Now pick emi_amout from emi tenure change screen.
    '                                    'Nilay (28-Aug-2015) -- Start
    '                                    'For last remaining emi transaction for no interest
    '                                    If mdecTotPrincipalAmtPaidCurrency > decCFBalancePaidCurrency Then
    '                                        mdecTotPrincipalAmtPaidCurrency = decCFBalancePaidCurrency
    '                                        mdecTotPrincipalAmt = decCFBalance
    '                                    End If
    '                                    'Nilay (28-Aug-2015) -- End

    '                                    'Sohail (13 Jan 2016) -- Start
    '                                    'Enhancement - Issue on Loan Report for Written off loans for KBC.
    '                                    If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF Then
    '                                        mdecTotPrincipalAmt = 0
    '                                        mdecTotPrincipalAmtPaidCurrency = 0
    '                                    End If
    '                                    'Sohail (13 Jan 2016) -- End

    '                                Else
    '                                    mdecTotPrincipalAmt = 0
    '                                    mdecTotPrincipalAmtPaidCurrency = 0
    '                                End If

    '                                'Sohail (13 Jan 2016) -- Start
    '                                'Enhancement - Issue on Loan Report for Written off loans for KBC.
    '                                If intDaysDiff <= 0 Then
    '                                    mdecTotPrincipalAmt = 0
    '                                    mdecTotPrincipalAmtPaidCurrency = 0
    '                                End If
    '                                'Sohail (13 Jan 2016) -- End

    '                                mdecTotIntAmt = 0
    '                                mdecTotIntAmtPaidCurrency = 0
    '                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
    '                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
    '                                mdecLastProjectedBalance = decCFBalance - mdecTotPrincipalAmt
    '                                mdecLastProjectedBalancePaidCurrency = decCFBalancePaidCurrency - mdecTotPrincipalAmtPaidCurrency

    '                                'Sohail (13 Dec 2016) -- Start
    '                                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                                If intMonth > 1 AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate Then
    '                                    dtLoanEndDate = dtLoanEndDate.AddDays(1).AddMonths(1).AddDays(-1)
    '                                End If
    '                                'Sohail (13 Dec 2016) -- End
    '                            End If

    '                            'Sohail (15 Dec 2015) -- Start
    '                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
    '                        Case enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI
    '                            Dim decPMT As Decimal = 0
    '                            Dim decPMTPaidCurrency As Decimal = 0
    '                            Dim mdecNewReduceAmt As Decimal = decCFBalance
    '                            Dim mdecNewReduceAmtPaidCurrency As Decimal = decCFBalancePaidCurrency
    '                            Dim decIntAmt As Decimal = 0
    '                            Dim decIntAmtPaidCurrency As Decimal = 0
    '                            Dim decPrincAmt As Decimal = 0
    '                            Dim decPrincAmtPaidCurrency As Decimal = 0

    '                            'For i As Integer = 1 To intDaysDiff
    '                            'Sohail (13 Dec 2016) -- Start
    '                            'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                            'If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False Then
    '                            If CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = False AndAlso intMonth <= 1 Then
    '                                'Sohail (13 Dec 2016) -- End
    '                                decPMT = 0
    '                                decPMTPaidCurrency = 0
    '                                decIntAmt = 0
    '                                decIntAmtPaidCurrency = 0
    '                                decPrincAmt = 0
    '                                decPrincAmtPaidCurrency = 0
    '                                mdecNewReduceAmt -= decPrincAmt
    '                                mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
    '                                'Sohail (13 Dec 2016) -- Start
    '                                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                                'ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True Then
    '                            ElseIf CInt(.Item("statusunkid").ToString) = enLoanStatus.ON_HOLD AndAlso CBool(.Item("iscalculateinterest").ToString) = True AndAlso intMonth <= 1 Then
    '                                'Sohail (13 Dec 2016) -- End
    '                                Select Case CInt(.Item("interest_calctype_id"))

    '                                    Case enLoanInterestCalcType.DAILY
    '                                        decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                        decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 36500

    '                                    Case enLoanInterestCalcType.MONTHLY
    '                                        decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate) / 1200) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                        decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate) / 1200

    '                                        .Item("TotalMonthlyInterest") = 0
    '                                        .Item("TotalMonthlyInterestPaidCurrency") = 0

    '                                End Select
    '                                decPMT = decIntAmt
    '                                decPMTPaidCurrency = decIntAmtPaidCurrency
    '                                decPrincAmt = 0
    '                                decPrincAmtPaidCurrency = 0
    '                                mdecNewReduceAmt -= decPrincAmt
    '                                mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency
    '                            Else
    '                                If blnFromProcessPayroll = True Then
    '                                    decPrincAmt = CDec(.Item("emi_principal_amount").ToString) / decExRate
    '                                    decPrincAmtPaidCurrency = CDec(.Item("emi_principal_amount").ToString)

    '                                    If decPrincAmtPaidCurrency > decCFBalancePaidCurrency Then
    '                                        decPrincAmtPaidCurrency = decCFBalancePaidCurrency
    '                                        decPrincAmt = decCFBalance
    '                                    End If

    '                                Else
    '                                    decPrincAmt = 0
    '                                    decPrincAmtPaidCurrency = 0
    '                                End If

    '                                Select Case CInt(.Item("interest_calctype_id"))

    '                                    Case enLoanInterestCalcType.DAILY
    '                                        decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate * intDaysDiff) / 36500) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                        decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate * intDaysDiff) / 36500

    '                                        If intDaysDiff <= 0 Then
    '                                            decPrincAmt = 0
    '                                            decPrincAmtPaidCurrency = 0
    '                                        End If
    '                                    Case enLoanInterestCalcType.MONTHLY
    '                                        decIntAmt = ((mdecNewReduceAmtPaidCurrency * decIntRate * intMonthsDiff) / 1200) / decExRate '(mdecNewReduceAmt * decIntRate) / 36500
    '                                        decIntAmtPaidCurrency = (mdecNewReduceAmtPaidCurrency * decIntRate * intMonthsDiff) / 1200

    '                                        If intMonthsDiff <= 0 Then
    '                                            decPrincAmt = 0
    '                                            decPrincAmtPaidCurrency = 0
    '                                        End If
    '                                End Select

    '                                'Sohail (13 Jan 2016) -- Start
    '                                'Enhancement - Issue on Loan Report for Written off loans for KBC.
    '                                If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF Then
    '                                    decPrincAmt = 0
    '                                    decPrincAmtPaidCurrency = 0
    '                                    decIntAmt = 0
    '                                    decIntAmtPaidCurrency = 0
    '                                End If
    '                                'Sohail (13 Jan 2016) -- End

    '                                decPMT = decPrincAmt + decIntAmt
    '                                decPMTPaidCurrency = decPrincAmtPaidCurrency + decIntAmtPaidCurrency
    '                                mdecNewReduceAmt -= decPrincAmt
    '                                mdecNewReduceAmtPaidCurrency -= decPrincAmtPaidCurrency

    '                            End If
    '                            'Next

    '                            mdecTotPMTAmt += decPMT
    '                            mdecTotPMTAmtPaidCurrency += decPMTPaidCurrency
    '                            mdecTotIntAmt += decIntAmt
    '                            mdecTotIntAmtPaidCurrency += decIntAmtPaidCurrency
    '                            mdecTotPrincipalAmt += decPrincAmt
    '                            mdecTotPrincipalAmtPaidCurrency += decPrincAmtPaidCurrency

    '                            mdecLastProjectedBalance = mdecNewReduceAmt
    '                            mdecLastProjectedBalancePaidCurrency = mdecNewReduceAmtPaidCurrency

    '                            'Sohail (15 Dec 2015) -- End

    '                            'Sohail (13 Dec 2016) -- Start
    '                            'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                            If intMonth > 1 AndAlso mdecLastProjectedBalancePaidCurrency > 0 AndAlso dtStart > dtLoanEndDate Then
    '                                dtLoanEndDate = dtLoanEndDate.AddDays(1).AddMonths(1).AddDays(-1)
    '                            End If
    '                            'Sohail (13 Dec 2016) -- End

    '                        Case Else
    '                            If CInt(.Item("isloan")) = False Then
    '                                'Sohail (03 Jun 2016) -- Start
    '                                'Enhancement -  Balance amount is zero when payslip payment done. so while voiding payment, advance amount was mot getting roll back
    '                                'mdecTotPrincipalAmt = CDec(.Item("balance_amount"))
    '                                'mdecTotPrincipalAmtPaidCurrency = CDec(.Item("balance_amountPaidCurrency"))
    '                                mdecTotPrincipalAmt = decBFAmount
    '                                mdecTotPrincipalAmtPaidCurrency = decBFAmountPaidCurrency
    '                                'Sohail (03 Jun 2016) -- End
    '                                mdecTotIntAmt = 0
    '                                mdecTotIntAmtPaidCurrency = 0
    '                                mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
    '                                mdecTotPMTAmtPaidCurrency = mdecTotPrincipalAmtPaidCurrency + mdecTotIntAmtPaidCurrency
    '                                mdecLastProjectedBalance = CDec(.Item("balance_amount")) - mdecTotPrincipalAmt
    '                                mdecLastProjectedBalancePaidCurrency = CDec(.Item("balance_amountPaidCurrency")) - mdecTotPrincipalAmtPaidCurrency
    '                            End If
    '                    End Select


    '                    If intMonth <= 1 Then 'Sohail (13 Dec 2016)
    '                        .Item("DaysDiff") = intDaysDiff
    '                        .Item("TotPMTAmount") = mdecTotPMTAmt
    '                        .Item("TotPMTAmountPaidCurrency") = mdecTotPMTAmtPaidCurrency
    '                        .Item("TotInterestAmount") = mdecTotIntAmt
    '                        .Item("TotInterestAmountPaidCurrency") = mdecTotIntAmtPaidCurrency
    '                        .Item("TotPrincipalAmount") = mdecTotPrincipalAmt
    '                        .Item("TotPrincipalAmountPaidCurrency") = mdecTotPrincipalAmtPaidCurrency
    '                        .Item("LastProjectedBalance") = mdecLastProjectedBalance
    '                        .Item("LastProjectedBalancePaidCurrency") = mdecLastProjectedBalancePaidCurrency

    '                        If blnIgnoreCurrentMonthTopupRepaymentInBFBalance = True Then
    '                            .Item("BalanceAmount") = decCFBalance - CDec(.Item("TotalMonthlyTopup")) + CDec(.Item("TotalMonthlyRepayment")) + CDec(.Item("TotalMonthlyPrincipalAmt"))
    '                            .Item("BalanceAmountPaidCurrency") = decCFBalancePaidCurrency - CDec(.Item("TotalMonthlyTopupPaidCurrency")) + CDec(.Item("TotalMonthlyRepaymentPaidCurrency")) + CDec(.Item("TotalMonthlyPrincipalAmtPaidCurrency"))
    '                        End If

    '                        .AcceptChanges()

    '                        'Sohail (13 Dec 2016) -- Start
    '                        'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                    Else
    '                        Dim LoanRow As DataRow = dtTemp.NewRow
    '                        LoanRow.ItemArray = dsRow.ItemArray

    '                        With LoanRow
    '                            .Item("CurrMonthPeriodUnkid") = 0
    '                            .Item("CurrMonthPeriodName") = Format(dtStart.AddDays(-1), "MMM - yyyy") & "  (Projected)"
    '                            .Item("CurrMonthPeriod_start_date") = eZeeDate.convertDate(dtStart.AddMonths(-1))
    '                            .Item("CurrMonthPeriod_end_date") = eZeeDate.convertDate(dtStart.AddDays(-1))
    '                            .Item("DaysDiff") = intDaysDiff
    '                            .Item("TotPMTAmount") = mdecTotPMTAmt
    '                            .Item("TotPMTAmountPaidCurrency") = mdecTotPMTAmtPaidCurrency
    '                            .Item("TotInterestAmount") = mdecTotIntAmt
    '                            .Item("TotInterestAmountPaidCurrency") = mdecTotIntAmtPaidCurrency
    '                            .Item("TotPrincipalAmount") = mdecTotPrincipalAmt
    '                            .Item("TotPrincipalAmountPaidCurrency") = mdecTotPrincipalAmtPaidCurrency
    '                            .Item("LastProjectedBalance") = mdecLastProjectedBalance
    '                            .Item("LastProjectedBalancePaidCurrency") = mdecLastProjectedBalancePaidCurrency

    '                            If blnIgnoreCurrentMonthTopupRepaymentInBFBalance = True Then
    '                                .Item("BalanceAmount") = decCFBalance
    '                                .Item("BalanceAmountPaidCurrency") = decCFBalancePaidCurrency
    '                            End If

    '                        End With
    '                        dtTemp.Rows.Add(LoanRow)
    '                    End If
    '                    'Sohail (13 Dec 2016) -- End

    '                    'Sohail (13 Dec 2016) -- Start
    '                    'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '                    If intLoanAdvanceStatus = enLoanStatus.WRITTEN_OFF OrElse intLoanAdvanceStatus = enLoanStatus.COMPLETED Then
    '                        Exit While
    '                    End If

    '                    dtStart = dtStart.AddMonths(1)
    '                End While
    '                'Sohail (13 Dec 2016) -- End

    '            End With
    '        Next

    '        'Sohail (13 Dec 2016) -- Start
    '        'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
    '        dsList.Tables(0).Merge(dtTemp)
    '        'Sohail (13 Dec 2016) -- End

    '        'Sohail (29 Jan 2016) -- Start
    '        'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
    '        If intLoanStatus > 0 AndAlso intLoanStatus = enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED Then
    '            Dim dr_Row() As DataRow = dsList.Tables(strTableName).Select(" TotalMonthlyDeductionPaidCurrency + TotPMTAmountPaidCurrency <= 0 ")
    '            If dr_Row.Length > 0 Then
    '                For Each dRow As DataRow In dr_Row
    '                    dsList.Tables(strTableName).Rows.Remove(dRow)
    '                Next
    '                dsList.Tables(strTableName).AcceptChanges()
    '            End If
    '        End If
    '        'Sohail (29 Jan 2016) -- End

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Calculate_LoanInfo", mstrModuleName)
    '    Finally
    '        'Nilay (28-Aug-2015) -- Start
    '        objDataOperation = Nothing
    '        'Nilay (28-Aug-2015) -- End
    '    End Try
    '    Return dsList
    'End Function
    'Sohail (02 Jan 2017) -- End

    Public Function GetLastLoanBalance(ByVal strTableName As String, ByVal intLoanAdvanceTranUnkID As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT  A.loanbalancetranunkid  " & _
                          ", A.loanschemeunkid " & _
                          ", A.periodunkid " & _
                          ", A.end_date " & _
                          ", A.loanadvancetranunkid " & _
                          ", A.employeeunkid " & _
                          ", A.payrollprocesstranunkid " & _
                          ", A.paymenttranunkid " & _
                          ", A.bf_amount " & _
                          ", A.amount " & _
                          ", A.cf_amount " & _
                          ", A.userunkid " & _
                          ", A.lninteresttranunkid " & _
                          ", A.lnemitranunkid " & _
                          ", A.lntopuptranunkid " & _
                          ", A.days " & _
                          ", A.bf_balance " & _
                          ", A.topup_amount " & _
                          ", A.repayment_amount " & _
                          ", A.interest_rate " & _
                          ", A.interest_amount " & _
                          ", A.cf_balance " & _
                          ", A.isonhold " & _
                          ", A.isreceipt " & _
                          ", A.principal_amount " & _
                          ", A.nexteffective_days " & _
                          ", A.nexteffective_months " & _
                    "FROM    ( SELECT    lnloan_balance_tran.loanbalancetranunkid  " & _
                                      ", lnloan_balance_tran.loanschemeunkid " & _
                                      ", lnloan_balance_tran.periodunkid " & _
                                      ", CONVERT(CHAR(8), lnloan_balance_tran.end_date, 112) end_date " & _
                                      ", lnloan_balance_tran.loanadvancetranunkid " & _
                                      ", lnloan_balance_tran.employeeunkid " & _
                                      ", lnloan_balance_tran.payrollprocesstranunkid " & _
                                      ", lnloan_balance_tran.paymenttranunkid " & _
                                      ", lnloan_balance_tran.bf_amount " & _
                                      ", lnloan_balance_tran.amount " & _
                                      ", lnloan_balance_tran.cf_amount " & _
                                      ", lnloan_balance_tran.userunkid " & _
                                      ", lnloan_balance_tran.lninteresttranunkid " & _
                                      ", lnloan_balance_tran.lnemitranunkid " & _
                                      ", lnloan_balance_tran.lntopuptranunkid " & _
                                      ", lnloan_balance_tran.days " & _
                                      ", lnloan_balance_tran.bf_balance " & _
                                      ", lnloan_balance_tran.topup_amount " & _
                                      ", lnloan_balance_tran.repayment_amount " & _
                                      ", lnloan_balance_tran.interest_rate " & _
                                      ", lnloan_balance_tran.interest_amount " & _
                                      ", lnloan_balance_tran.cf_balance " & _
                                      ", lnloan_balance_tran.isonhold " & _
                                      ", lnloan_balance_tran.isreceipt " & _
                                      ", lnloan_balance_tran.principal_amount " & _
                                      ", lnloan_balance_tran.nexteffective_days " & _
                                      ", lnloan_balance_tran.nexteffective_months " & _
                                     ", DENSE_RANK() OVER ( PARTITION BY lnloan_balance_tran.loanadvancetranunkid ORDER BY lnloan_balance_tran.transactiondate DESC, lnloan_balance_tran.loanbalancetranunkid DESC ) AS ROW_NO " & _
                              "FROM      lnloan_balance_tran " & _
                              "WHERE     lnloan_balance_tran.isvoid = 0 " & _
                                        "AND lnloan_balance_tran.loanadvancetranunkid = @loanadvancetranunkid " & _
                            ") AS A " & _
                    "WHERE   A.ROW_NO = 1 "
            'Hemant (29 Jan 2019) -- [PARTITION BY lnloan_balance_tran.loanadvancetranunkid ORDER BY lnloan_balance_tran.end_date DESC] = [PARTITION BY lnloan_balance_tran.loanadvancetranunkid ORDER BY lnloan_balance_tran.transactiondate DESC]
            'Sohail (15 Dec 2015) - [nexteffective_months]

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceTranUnkID)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastLoanBalance; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (07 May 2015) -- End

    Public Function Check_Transaction_Linking(ByVal xColumnName As String, _
                                              ByVal xTranUnkid As Integer, _
                                              ByVal xLoanAdvUnkid As Integer) As String
        Dim xMessage As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT " & xColumnName & " FROM lnloan_balance_tran WHERE " & xColumnName & _
                       " = '" & xTranUnkid & "' AND loanadvancetranunkid = '" & xLoanAdvUnkid & "' "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    Select Case xColumnName.ToUpper
                        Case "LNINTERESTTRANUNKID"  'INTEREST RATE
                            xMessage = Language.getMessage(mstrModuleName, 7, "Sorry, you cannot void selected interest rate. Reason : selected interest rate is already linked with loan deduction.")
                        Case "LNEMITRANUNKID"   'EMI
                            xMessage = Language.getMessage(mstrModuleName, 8, "Sorry, you cannot void selected installment amount or number. Reason : selected installment amount or number is already linked with loan deduction.")
                        Case "LNTOPUPTRANUNKID" 'TOPUP
                            xMessage = Language.getMessage(mstrModuleName, 9, "Sorry, you cannot void selected topup amount. Reason : selected topup amount is already linked with loan deduction.")
                    End Select
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Check_Transaction_Linking; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return xMessage
    End Function

    Public Sub Calulate_Projected_Loan_Balance(ByVal mdecLoanAmount As Decimal, _
                                                    ByVal intEffectiveDays As Integer, _
                                                    ByVal decLoanRate As Decimal, _
                                               ByVal eLnCalcType As enLoanCalcId, _
                                               ByVal eIntRateCalcTypeID As enLoanInterestCalcType, _
                                               ByVal intDuration As Integer, _
                                               ByVal intFirstInstallmentDays As Integer, _
                                               ByVal decOrigInstlAmount As Decimal, _
                                               ByRef decIntrAmount As Decimal, _
                                               ByRef decInstlAmount As Decimal, _
                                               ByRef decTotInterestAmount As Decimal, _
                                               ByRef decTotInstallmentAmount As Decimal _
                                               )
        'Sohail (15 Dec 2015) - [eIntRateCalcTypeID, intFirstInstallmentDays, decOrigInstlAmount, decTotInterestAmount, decTotInstallmentAmount]
        Dim mdecTotPMTAmt As Decimal = 0 : Dim mdecTotIntAmt As Decimal = 0 : Dim mdecTotPrincipalAmt As Decimal = 0
        Dim decGTotIntAmt As Decimal = 0 : Dim decGTotPrincAmt As Decimal = 0 : Dim decGTotNetAmt As Decimal = 0 'Sohail (15 Dec 2015)
        Try
            Select Case eLnCalcType
                Case enLoanCalcId.Simple_Interest
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'mdecTotPrincipalAmt = (mdecLoanAmount / intEffectiveDays) * intEffectiveDays
                    'mdecTotIntAmt = (mdecLoanAmount * decLoanRate * intEffectiveDays) / (36500)
                    'mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                    If eIntRateCalcTypeID = enLoanInterestCalcType.DAILY Then
                        mdecTotPrincipalAmt = (mdecLoanAmount / intEffectiveDays) * intFirstInstallmentDays
                        mdecTotIntAmt = (mdecLoanAmount * decLoanRate * intFirstInstallmentDays) / (36500)

                        decGTotPrincAmt = (mdecLoanAmount / intEffectiveDays) * intEffectiveDays
                        decGTotIntAmt = (mdecLoanAmount * decLoanRate * intEffectiveDays) / (36500)
                    ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY Then
                        mdecTotPrincipalAmt = (mdecLoanAmount / intDuration)
                        mdecTotIntAmt = (mdecLoanAmount * decLoanRate) / (1200)

                        decGTotPrincAmt = (mdecLoanAmount / intDuration) * intDuration
                        decGTotIntAmt = (mdecLoanAmount * decLoanRate * intDuration) / (1200)

                        'Sohail (14 Mar 2017) -- Start
                        'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                    ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.BY_TENURE Then
                        mdecTotPrincipalAmt = (mdecLoanAmount / intDuration)
                        mdecTotIntAmt = (mdecLoanAmount * decLoanRate) / (intDuration * 100)

                        decGTotPrincAmt = (mdecLoanAmount / intDuration) * intDuration
                        decGTotIntAmt = (mdecLoanAmount * decLoanRate * intDuration) / (intDuration * 100)
                        'Sohail (14 Mar 2017) -- End

                        'Sohail (23 May 2017) -- Start
                        'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                    ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.SIMPLE_INTEREST Then
                        mdecTotPrincipalAmt = (mdecLoanAmount / intDuration)
                        mdecTotIntAmt = (mdecLoanAmount * decLoanRate) / (100)

                        decGTotPrincAmt = (mdecLoanAmount / intDuration) * intDuration
                        decGTotIntAmt = (mdecLoanAmount * decLoanRate * intDuration) / (100)
                        'Sohail (23 May 2017) -- End

                    End If
                    mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                    decGTotNetAmt = decGTotPrincAmt + decGTotIntAmt
                    'Sohail (15 Dec 2015) -- End

                Case enLoanCalcId.Reducing_Amount
                    Dim decPMT As Decimal = 0
                    Dim mdecNewReduceAmt As Decimal = mdecLoanAmount
                    Dim decIntAmt As Decimal = 0
                    Dim decPrincAmt As Decimal = 0

                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'For i As Integer = 1 To intEffectiveDays
                    For i As Integer = 1 To intFirstInstallmentDays
                        'decPMT = Pmt(decLoanRate / 36500, intEffectiveDays, (mdecLoanAmount * -1))
                        If eIntRateCalcTypeID = enLoanInterestCalcType.DAILY Then
                        decPMT = Pmt(decLoanRate / 36500, intEffectiveDays, (mdecLoanAmount * -1))
                        decIntAmt = (mdecNewReduceAmt * decLoanRate) / 36500
                        ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY Then
                            decPMT = Pmt(decLoanRate / 1200, intDuration, (mdecLoanAmount * -1))
                            decIntAmt = (mdecNewReduceAmt * decLoanRate) / 1200
                            'Sohail (14 Mar 2017) -- Start
                            'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                        ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.BY_TENURE Then
                            decPMT = Pmt(decLoanRate / 1200, intDuration, (mdecLoanAmount * -1))
                            decIntAmt = (mdecNewReduceAmt * decLoanRate) / (intDuration * 100)
                            'Sohail (14 Mar 2017) -- End

                            'Sohail (23 May 2017) -- Start
                            'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                        ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.SIMPLE_INTEREST Then
                            decPMT = Pmt(decLoanRate / 1200, intDuration, (mdecLoanAmount * -1))
                            decIntAmt = (mdecNewReduceAmt * decLoanRate) / (100)
                            'Sohail (23 May 2017) -- End

                        End If
                        'Sohail (15 Dec 2015) -- End
                        decPrincAmt = decPMT - decIntAmt

                        mdecNewReduceAmt -= decPrincAmt

                        mdecTotPMTAmt += decPMT
                        mdecTotIntAmt += decIntAmt
                        mdecTotPrincipalAmt += decPrincAmt
                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        'Sohail (14 Mar 2017) -- Start
                        'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                        'If eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY Then
                        'Sohail (23 May 2017) -- Start
                        'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                        'If eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY OrElse eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY Then
                        If eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY OrElse eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY OrElse eIntRateCalcTypeID = enLoanInterestCalcType.SIMPLE_INTEREST Then
                            'Sohail (23 May 2017) -- End
                            'Sohail (14 Mar 2017) -- End
                            Exit For
                        End If
                        'Sohail (15 Dec 2015) -- End
                    Next

                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    If eIntRateCalcTypeID = enLoanInterestCalcType.DAILY Then
                        decGTotNetAmt = Pmt(decLoanRate / 36500, intEffectiveDays, (mdecLoanAmount * -1)) * intEffectiveDays
                        decGTotIntAmt = decGTotNetAmt - mdecLoanAmount
                    ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY Then
                        decGTotNetAmt = Pmt(decLoanRate / 1200, intDuration, (mdecLoanAmount * -1)) * intDuration
                        decGTotIntAmt = decGTotNetAmt - mdecLoanAmount
                        'Sohail (14 Mar 2017) -- Start
                        'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                    ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.BY_TENURE Then
                        decGTotNetAmt = Pmt(decLoanRate / (intDuration * 100), intDuration, (mdecLoanAmount * -1)) * intDuration
                        decGTotIntAmt = decGTotNetAmt - mdecLoanAmount
                        'Sohail (14 Mar 2017) -- End
                        'Sohail (23 May 2017) -- Start
                        'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                    ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.SIMPLE_INTEREST Then
                        decGTotNetAmt = Pmt(decLoanRate / (100), intDuration, (mdecLoanAmount * -1)) * intDuration
                        decGTotIntAmt = decGTotNetAmt - mdecLoanAmount
                        'Sohail (23 May 2017) -- End

                    End If
                    decGTotPrincAmt = decGTotNetAmt - decGTotIntAmt
                    'Sohail (15 Dec 2015) -- End

                Case enLoanCalcId.No_Interest
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'mdecTotPrincipalAmt = (mdecLoanAmount / intEffectiveDays) * intEffectiveDays
                    'mdecTotIntAmt = 0
                    'mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                    mdecTotPrincipalAmt = decOrigInstlAmount
                    mdecTotIntAmt = 0
                    mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt

                    decGTotPrincAmt = mdecLoanAmount
                    decGTotIntAmt = 0
                    decGTotNetAmt = decGTotPrincAmt + decGTotIntAmt
                Case enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI
                    Dim decPMT As Decimal = 0
                    Dim mdecNewReduceAmt As Decimal = mdecLoanAmount
                    Dim decIntAmt As Decimal = 0
                    Dim decPrincAmt As Decimal = 0
                    mdecTotPrincipalAmt = decOrigInstlAmount
                    If eIntRateCalcTypeID = enLoanInterestCalcType.DAILY Then
                        mdecTotIntAmt = (mdecLoanAmount * decLoanRate * intFirstInstallmentDays) / 36500

                        decGTotIntAmt = (mdecLoanAmount * decLoanRate * intEffectiveDays) / 36500
                        decGTotPrincAmt = mdecLoanAmount

                    ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY Then
                        mdecTotIntAmt = (mdecLoanAmount * decLoanRate) / 1200

                        For i As Integer = 1 To intDuration
                            decIntAmt = (mdecNewReduceAmt * decLoanRate) / 1200

                            mdecNewReduceAmt -= mdecTotPrincipalAmt


                            decGTotIntAmt += decIntAmt
                        Next
                        decGTotPrincAmt = mdecLoanAmount

                        'Sohail (14 Mar 2017) -- Start
                        'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                    ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.BY_TENURE Then
                        mdecTotIntAmt = (mdecLoanAmount * decLoanRate) / (intDuration * 100)

                        For i As Integer = 1 To intDuration
                            decIntAmt = (mdecNewReduceAmt * decLoanRate) / (intDuration * 100)

                            mdecNewReduceAmt -= mdecTotPrincipalAmt


                            decGTotIntAmt += decIntAmt
                        Next
                        decGTotPrincAmt = mdecLoanAmount
                        'Sohail (14 Mar 2017) -- End

                        'Sohail (23 May 2017) -- Start
                        'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                    ElseIf eIntRateCalcTypeID = enLoanInterestCalcType.SIMPLE_INTEREST Then
                        mdecTotIntAmt = (mdecLoanAmount * decLoanRate) / (100)

                        For i As Integer = 1 To intDuration
                            decIntAmt = (mdecNewReduceAmt * decLoanRate) / (100)

                            mdecNewReduceAmt -= mdecTotPrincipalAmt


                            decGTotIntAmt += decIntAmt
                        Next
                        decGTotPrincAmt = mdecLoanAmount
                        'Sohail (23 May 2017) -- End

                    End If
                    mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt
                    decGTotNetAmt = decGTotPrincAmt + decGTotIntAmt


                    'Sohail (15 Dec 2015) -- End

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                Case enLoanCalcId.No_Interest_With_Mapped_Head
                    mdecTotPrincipalAmt = decOrigInstlAmount
                    mdecTotIntAmt = 0
                    mdecTotPMTAmt = mdecTotPrincipalAmt + mdecTotIntAmt

                    decGTotPrincAmt = mdecLoanAmount
                    decGTotIntAmt = 0
                    decGTotNetAmt = decGTotPrincAmt + decGTotIntAmt

            End Select

            decIntrAmount = mdecTotIntAmt
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'decInstlAmount = (mdecTotPMTAmt / intDuration)
            decInstlAmount = mdecTotPMTAmt
            decTotInterestAmount = decGTotIntAmt
            decTotInstallmentAmount = decGTotPrincAmt
            'Sohail (15 Dec 2015) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Calulate_Projected_Loan_Balance; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Public Function GetLoanOperationList(ByVal intLoanAdvanceUnkid As Integer, Optional ByVal strList As String = "List", Optional ByVal fmtCurrency As String = "") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            Using objDo As New clsDataOperation

                StrQ = "SELECT DISTINCT " & _
                        "	 A.EffDate " & _
                        "	,A.Rate " & _
                        "	,A.NoOfInstallment " & _
                        "	,A.InstallmentAmount " & _
                        "	,A.TopupAmount " & _
                        "	,A.PeriodName " & _
                        "	,A.statusid " & _
                        "	,A.BaseAmt " & _
                        "	,A.exrate " & _
                        "	,A.IdType " & _
                        "	,A.TabUnkid " & _
                        "   ,A.oDate " & _
                        "   ,A.OprType " & _
                        "   ,A.periodunkid " & _
                        "   ,A.isdefault " & _
                        "   ,A.approverunkid " & _
                        "   ,A.approver " & _
                        "   ,A.isexternalapprover " & _
                        "   ,A.identify_guid " & _
                        "FROM " & _
                        "( " & _
                        "	SELECT " & _
                        "		 '' AS EffDate " & _
                        "		,CAST(lnloan_interest_tran.interest_rate AS NVARCHAR(MAX)) AS Rate " & _
                        "		,'' AS NoOfInstallment " & _
                        "		,'' AS InstallmentAmount " & _
                        "		,'' AS TopupAmount " & _
                        "		,ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                        "		,ISNULL(cfcommon_period_tran.statusid,0) AS statusid " & _
                        "		,CONVERT(CHAR(8),lnloan_interest_tran.effectivedate,112) AS oDate " & _
                        "		,lnloan_interest_tran.effectivedate AS mDate " & _
                        "		,0 AS BaseAmt " & _
                        "		,0 AS exrate " & _
                        "		,'" & enParameterMode.LN_RATE & "' AS IdType " & _
                        "		,lnloan_interest_tran.lninteresttranunkid AS TabUnkid " & _
                        "       ,@RChange AS OprType " & _
                        "       ,lnloan_interest_tran.periodunkid " & _
                        "       ,lnloan_interest_tran.isdefault " & _
                        "       ,lnloan_interest_tran.approverunkid " & _
                        "       ,@ApprovedBy + #APPROVER_NAME# + ' - ' + ISNULL(lnapproverlevel_master.lnlevelname, '') AS approver " & _
                        "       ,lnloanapprover_master.isexternalapprover AS isexternalapprover " & _
                        "       ,lnloan_interest_tran.identify_guid " & _
                        "	FROM lnloan_advance_tran " & _
                        "		JOIN lnloan_interest_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
                        "		JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
                        "       LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_interest_tran.approverunkid " & _
                        "       LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                        "       #APPROVER_JOIN# " & _
                        "	WHERE lnloan_advance_tran.isvoid = 0 AND lnloan_interest_tran.isvoid = 0 AND lnloan_advance_tran.loanadvancetranunkid = '" & intLoanAdvanceUnkid & "' " & _
                        "           #COMPANY_ID# " & _
                        "UNION " & _
                        "	SELECT " & _
                        "		 '' AS EffDate " & _
                        "		,'' AS Rate " & _
                        "		,CAST(lnloan_emitenure_tran.emi_tenure AS NVARCHAR(MAX)) AS NoOfInstallment " & _
                        "		,CAST(lnloan_emitenure_tran.emi_amount AS NVARCHAR(MAX)) AS InstallmentAmount " & _
                        "		,'' AS TopupAmount " & _
                        "		,ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                        "		,ISNULL(cfcommon_period_tran.statusid,0) AS statusid " & _
                        "		,CONVERT(CHAR(8),lnloan_emitenure_tran.effectivedate,112) AS oDate " & _
                        "		,lnloan_emitenure_tran.effectivedate AS mDate " & _
                        "		,lnloan_emitenure_tran.basecurrency_amount AS BaseAmt " & _
                        "		,lnloan_emitenure_tran.exchange_rate AS exrate " & _
                        "		,'" & enParameterMode.LN_EMI & "' AS IdType " & _
                        "		,lnloan_emitenure_tran.lnemitranunkid AS TabUnkid " & _
                        "       ,@IChange AS OprType " & _
                        "       ,lnloan_emitenure_tran.periodunkid " & _
                        "       ,lnloan_emitenure_tran.isdefault " & _
                        "       ,lnloan_emitenure_tran.approverunkid " & _
                        "       ,@ApprovedBy + #APPROVER_NAME# + ' - ' + ISNULL(lnapproverlevel_master.lnlevelname, '') AS approver " & _
                        "       ,lnloanapprover_master.isexternalapprover AS isexternalapprover " & _
                        "       ,lnloan_emitenure_tran.identify_guid " & _
                        "	FROM lnloan_advance_tran " & _
                        "		JOIN lnloan_emitenure_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
                        "		JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
                        "       LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_emitenure_tran.approverunkid " & _
                        "       LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                        "       #APPROVER_JOIN# " & _
                        "	WHERE lnloan_advance_tran.isvoid = 0 AND lnloan_emitenure_tran.isvoid = 0 AND lnloan_advance_tran.loanadvancetranunkid = '" & intLoanAdvanceUnkid & "' " & _
                        "          #COMPANY_ID# " & _
                        "UNION " & _
                        "	SELECT " & _
                        "		 '' AS EffDate " & _
                        "		,'' AS Rate " & _
                        "		,'' AS NoOfInstallment " & _
                        "		,'' AS InstallmentAmount " & _
                        "		,CAST(lnloan_topup_tran.topup_amount AS NVARCHAR(MAX)) AS TopupAmount " & _
                        "		,ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                        "		,ISNULL(cfcommon_period_tran.statusid,0) AS statusid " & _
                        "		,CONVERT(CHAR(8),effectivedate,112) AS oDate " & _
                        "		,lnloan_topup_tran.effectivedate AS mDate " & _
                        "		,lnloan_topup_tran.basecurrency_amount AS BaseAmt " & _
                        "		,lnloan_topup_tran.exchange_rate AS exrate " & _
                        "		,'" & enParameterMode.LN_TOPUP & "' AS IdType " & _
                        "		,lnloan_topup_tran.lntopuptranunkid AS TabUnkid " & _
                        "       ,@TChange AS OprType " & _
                        "       ,lnloan_topup_tran.periodunkid " & _
                        "       ,lnloan_topup_tran.isdefault " & _
                        "       ,lnloan_topup_tran.approverunkid " & _
                        "       ,@ApprovedBy + #APPROVER_NAME# + ' - ' + ISNULL(lnapproverlevel_master.lnlevelname, '') AS approver " & _
                        "       ,lnloanapprover_master.isexternalapprover AS isexternalapprover " & _
                        "       ,lnloan_topup_tran.identify_guid " & _
                        "	FROM lnloan_advance_tran " & _
                        "		JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
                        "		JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_topup_tran.periodunkid " & _
                        "       LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_topup_tran.approverunkid " & _
                        "       LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                        "       #APPROVER_JOIN# " & _
                        "	WHERE lnloan_advance_tran.isvoid = 0 AND lnloan_topup_tran.isvoid = 0 AND lnloan_advance_tran.loanadvancetranunkid = '" & intLoanAdvanceUnkid & "' " & _
                        "           #COMPANY_ID# " & _
                        ") AS A "
                'Nilay (13-Sept-2016) -- [identify_guid]

                strQFinal = StrQ

                strQCondition &= " WHERE A.isexternalapprover = #EXT_APPROVER# "
                StrQ &= strQCondition

                'ORDER BY A.oDate,A.IdType
                'Nilay (01-Mar-2016) -- [A.mDate REPLACED BY A.oDate]

                StrQ = StrQ.Replace("#APPROVER_NAME#", "ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.othername, '') + ' ' + ISNULL(Emp.surname, '') ")
                StrQ = StrQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = lnloanapprover_master.approverempunkid ")
                StrQ = StrQ.Replace("#COMPANY_ID#", "")
                StrQ = StrQ.Replace("#EXT_APPROVER#", "0")

                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                objDo.ClearParameters()
                'Shani (21-Jul-2016) -- End
                objDo.AddParameter("@RChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Rate Change"))
                objDo.AddParameter("@IChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Installment Change"))
                objDo.AddParameter("@TChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Topup Added"))
                objDo.AddParameter("@ApprovedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Approved By :- "))

                dsList = objDo.ExecQuery(StrQ, strList)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                Dim dsCompany As DataSet
                Dim objlnApprover As New clsLoanApprover_master

                dsCompany = objlnApprover.GetExternalApproverList(objDo, "Company")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                Dim dsExtList As New DataSet

                For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                    StrQ = strQFinal
                    If dRow("dbname").ToString.Trim.Length <= 0 Then
                        StrQ = StrQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                        StrQ = StrQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanapprover_master.approverempunkid ")
                    Else
                        StrQ = StrQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.othername, '') + ' ' + ISNULL(Emp.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.othername, '') + ' ' + ISNULL(Emp.surname, '') END ")
                        StrQ = StrQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                                                               "LEFT JOIN #DB_NAME#hremployee_master AS Emp ON Emp.employeeunkid = cfuser_master.employeeunkid ")
                        StrQ = StrQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                    End If

                    StrQ &= strQCondition
                    StrQ = StrQ.Replace("#COMPANY_ID#", " AND cfuser_master.companyunkid = " & dRow("companyunkid"))
                    StrQ = StrQ.Replace("#EXT_APPROVER#", "1")
                    'StrQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                    'Shani (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                    objDo.ClearParameters()
                    'Shani (21-Jul-2016) -- End
                    objDo.AddParameter("@RChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Rate Change"))
                    objDo.AddParameter("@IChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Installment Change"))
                    objDo.AddParameter("@TChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Topup Added"))
                    objDo.AddParameter("@ApprovedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Approved By :- "))

                    dsExtList = objDo.ExecQuery(StrQ, strList)

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables.Count <= 0 Then
                        dsList.Tables.Add(dsExtList.Tables(strList).Copy)
                    Else
                        dsList.Tables(0).Merge(dsExtList.Tables(0), True)
                    End If
                Next

                Dim dtTable As DataTable
                dtTable = New DataView(dsList.Tables("List"), "", "oDate,IdType", DataViewRowState.CurrentRows).ToTable.Copy
                dsList.Tables.RemoveAt(0)
                dsList.Tables.Add(dtTable.Copy)

            End Using

            If fmtCurrency.Trim.Length <= 0 Then fmtCurrency = ConfigParameter._Object._CurrencyFormat

            If dsList.Tables(0).Rows.Count > 0 Then

                For Each drow As DataRow In dsList.Tables(0).Rows
                    drow.Item("EffDate") = eZeeDate.convertDate(drow.Item("oDate").ToString).ToShortDateString
                    If drow.Item("InstallmentAmount").ToString <> "" Then
                        drow.Item("InstallmentAmount") = Format(CDec(drow.Item("InstallmentAmount")), fmtCurrency)
                    End If
                    If drow.Item("TopupAmount").ToString <> "" Then
                        drow.Item("TopupAmount") = Format(CDec(drow.Item("TopupAmount")), fmtCurrency)
                    End If
                Next

                dsList.Tables(0).AcceptChanges()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanOperationList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    'Public Function GetLoanOperationList(ByVal intLoanAdvanceUnkid As Integer, Optional ByVal strList As String = "List", Optional ByVal fmtCurrency As String = "") As DataSet
    '    Dim StrQ As String = String.Empty
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        Using objDo As New clsDataOperation

    '            StrQ = "SELECT " & _
    '                    "	 A.EffDate " & _
    '                    "	,A.Rate " & _
    '                    "	,A.NoOfInstallment " & _
    '                    "	,A.InstallmentAmount " & _
    '                    "	,A.TopupAmount " & _
    '                    "	,A.PeriodName " & _
    '                    "	,A.statusid " & _
    '                    "	,A.BaseAmt " & _
    '                    "	,A.exrate " & _
    '                    "	,A.IdType " & _
    '                    "	,A.TabUnkid " & _
    '                    "   ,A.oDate " & _
    '                    "   ,A.OprType " & _
    '                    "   ,A.periodunkid " & _
    '                    "FROM " & _
    '                    "( " & _
    '                    "	SELECT " & _
    '                    "		 '' AS EffDate " & _
    '                    "		,CAST(lnloan_interest_tran.interest_rate AS NVARCHAR(MAX)) AS Rate " & _
    '                    "		,'' AS NoOfInstallment " & _
    '                    "		,'' AS InstallmentAmount " & _
    '                    "		,'' AS TopupAmount " & _
    '                    "		,period_name AS PeriodName " & _
    '                    "		,statusid " & _
    '                    "		,CONVERT(CHAR(8),effectivedate,112) AS oDate " & _
    '                    "		,effectivedate AS mDate " & _
    '                    "		,0 AS BaseAmt " & _
    '                    "		,0 AS exrate " & _
    '                    "		,'" & enParameterMode.LN_RATE & "' AS IdType " & _
    '                    "		,lninteresttranunkid AS TabUnkid " & _
    '                    "       ,@RChange AS OprType " & _
    '                    "       ,lnloan_interest_tran.periodunkid " & _
    '                    "	FROM lnloan_advance_tran " & _
    '                    "		JOIN lnloan_interest_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_interest_tran.loanadvancetranunkid " & _
    '                    "		JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
    '                    "	WHERE lnloan_advance_tran.isvoid = 0 AND lnloan_interest_tran.isvoid = 0 AND lnloan_advance_tran.loanadvancetranunkid = '" & intLoanAdvanceUnkid & "' " & _
    '                    "UNION " & _
    '                    "	SELECT " & _
    '                    "		 '' AS EffDate " & _
    '                    "		,'' AS Rate " & _
    '                    "		,CAST(lnloan_emitenure_tran.emi_tenure AS NVARCHAR(MAX)) AS NoOfInstallment " & _
    '                    "		,CAST(lnloan_emitenure_tran.emi_amount AS NVARCHAR(MAX)) AS InstallmentAmount " & _
    '                    "		,'' AS TopupAmount " & _
    '                    "		,period_name AS PeriodName " & _
    '                    "		,statusid " & _
    '                    "		,CONVERT(CHAR(8),effectivedate,112) AS oDate " & _
    '                    "		,effectivedate AS mDate " & _
    '                    "		,lnloan_emitenure_tran.basecurrency_amount AS BaseAmt " & _
    '                    "		,lnloan_emitenure_tran.exchange_rate AS exrate " & _
    '                    "		,'" & enParameterMode.LN_EMI & "' AS IdType " & _
    '                    "		,lnemitranunkid AS TabUnkid " & _
    '                    "       ,@IChange AS OprType " & _
    '                    "       ,lnloan_emitenure_tran.periodunkid " & _
    '                    "	FROM lnloan_advance_tran " & _
    '                    "		JOIN lnloan_emitenure_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_emitenure_tran.loanadvancetranunkid " & _
    '                    "		JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
    '                    "	WHERE lnloan_advance_tran.isvoid = 0 AND lnloan_emitenure_tran.isvoid = 0 AND lnloan_advance_tran.loanadvancetranunkid = '" & intLoanAdvanceUnkid & "' " & _
    '                    "UNION " & _
    '                    "	SELECT " & _
    '                    "		 '' AS EffDate " & _
    '                    "		,'' AS Rate " & _
    '                    "		,'' AS NoOfInstallment " & _
    '                    "		,'' AS InstallmentAmount " & _
    '                    "		,CAST(lnloan_topup_tran.topup_amount AS NVARCHAR(MAX)) AS TopupAmount " & _
    '                    "		,period_name AS PeriodName " & _
    '                    "		,statusid " & _
    '                    "		,CONVERT(CHAR(8),effectivedate,112) AS oDate " & _
    '                    "		,effectivedate AS mDate " & _
    '                    "		,lnloan_topup_tran.basecurrency_amount AS BaseAmt " & _
    '                    "		,lnloan_topup_tran.exchange_rate AS exrate " & _
    '                    "		,'" & enParameterMode.LN_TOPUP & "' AS IdType " & _
    '                    "		,lntopuptranunkid AS TabUnkid " & _
    '                    "       ,@TChange AS OprType " & _
    '                    "       ,lnloan_topup_tran.periodunkid " & _
    '                    "	FROM lnloan_advance_tran " & _
    '                    "		JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
    '                    "		JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_topup_tran.periodunkid " & _
    '                    "	WHERE lnloan_advance_tran.isvoid = 0 AND lnloan_topup_tran.isvoid = 0 AND lnloan_advance_tran.loanadvancetranunkid = '" & intLoanAdvanceUnkid & "' " & _
    '                    ") AS A ORDER BY A.oDate,A.IdType "
    '            'Nilay (01-Mar-2016) -- [A.mDate REPLACED BY A.oDate]

    '            objDo.AddParameter("@RChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Rate Change"))
    '            objDo.AddParameter("@IChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Installment Change"))
    '            objDo.AddParameter("@TChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Topup Added"))

    '            dsList = objDo.ExecQuery(StrQ, strList)

    '            If objDo.ErrorMessage <> "" Then
    '                exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '                Throw exForce
    '            End If

    '        End Using

    '        If fmtCurrency.Trim.Length <= 0 Then fmtCurrency = ConfigParameter._Object._CurrencyFormat

    '        If dsList.Tables(0).Rows.Count > 0 Then

    '            For Each drow As DataRow In dsList.Tables(0).Rows
    '                drow.Item("EffDate") = eZeeDate.convertDate(drow.Item("oDate").ToString).ToShortDateString
    '                If drow.Item("InstallmentAmount").ToString <> "" Then
    '                    drow.Item("InstallmentAmount") = Format(CDec(drow.Item("InstallmentAmount")), fmtCurrency)
    '                End If
    '                If drow.Item("TopupAmount").ToString <> "" Then
    '                    drow.Item("TopupAmount") = Format(CDec(drow.Item("TopupAmount")), fmtCurrency)
    '                End If
    '            Next

    '            dsList.Tables(0).AcceptChanges()
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetLoanOperationList; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dsList
    'End Function
    'Nilay (01-Apr-2016) -- End


    'Nilay (23-Aug-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Public Function GetListByProcessPendingId(ByVal intProcessPendingunkid As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOpr = New clsDataOperation

            StrQ = "SELECT TOP 1 * FROM lnloan_advance_tran WHERE processpendingloanunkid = @processpendingloanunkid ORDER BY loanadvancetranunkid DESC "

            objDataOpr.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessPendingunkid.ToString)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetListByProcessPendingId ; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'Nilay (23-Aug-2016) -- End

    'Nilay (04-Nov-2016) -- Start
    'Enhancements: Global Change Status feature requested by {Rutta}
    Public Function GetListForGlobalChangeStatus(ByVal xDataBaseName As String, _
                                                 ByVal xUserId As Integer, _
                                                 ByVal xYearId As Integer, _
                                                 ByVal xCompanyId As Integer, _
                                                 ByVal xIncludeInactiveEmp As Boolean, _
                                                 ByVal xPeriodStartDate As DateTime, _
                                                 ByVal xPeriodEndDate As DateTime, _
                                                 ByVal xUserAccessFilterString As String, _
                                                 ByVal strTableName As String, _
                                                 Optional ByVal xFilterString As String = "", _
                                                 Optional ByVal strEmployeeIds As String = "", _
                                                 Optional ByVal blnApplyUserAccessFilter As Boolean = True) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStartDate, xPeriodEndDate, , , xDataBaseName)
            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEndDate, , xDataBaseName, xUserId, xCompanyId, xYearId, xUserAccessFilterString)
            End If
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEndDate, xDataBaseName)

            Dim dsLoanCalc As DataSet = GetLoanCalculationTypeList("List", True, True)
            Dim dicLoanCalc As Dictionary(Of Integer, String) = (From p In dsLoanCalc.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("Name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            Dim dsLoanInterestCalc As DataSet = GetLoan_Interest_Calculation_Type("List", True, True)
            Dim dicLoanInterestCalc As Dictionary(Of Integer, String) = (From p In dsLoanInterestCalc.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("Name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            'Sohail (05 Jun 2020) -- Start
            'NMB Issue # : Wrong write off amount was coming on global loan change status screen.
            StrQ = "SELECT * INTO    #TableBalance FROM ( " & _
                            "SELECT  lnloan_balance_tran.loanadvancetranunkid  " & _
                                  ", lnloan_balance_tran.loanbalancetranunkid " & _
                                  ", lnloan_balance_tran.cf_balance " & _
                                  ", lnloan_balance_tran.cf_balancePaidCurrency " & _
                                  ", CONVERT(CHAR(8), lnloan_balance_tran.end_date + 1, 112) AS BalanceEffectiveDate " & _
                                  ", DENSE_RANK() OVER ( PARTITION BY lnloan_balance_tran.loanadvancetranunkid ORDER BY cfcommon_period_tran.end_date DESC, lnloan_balance_tran.end_date DESC, lnloan_balance_tran.loanbalancetranunkid DESC ) AS ROWNO "

            StrQ &= "    FROM    lnloan_balance_tran " & _
                                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_balance_tran.periodunkid " & _
                                                                      "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                    "LEFT JOIN cfcommon_period_tran AS TranPeriod ON TranPeriod.periodunkid = lnloan_balance_tran.transaction_periodunkid " & _
                                                                      "AND TranPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
                            "WHERE   ISNULL(lnloan_balance_tran.isvoid, 0) = 0 " & _
                            "AND CONVERT(CHAR(8), lnloan_balance_tran.end_date, 112) <= @PeriodEndDate " & _
                        ") AS E " & _
                        "WHERE E.ROWNO = 1 "
            'Sohail (05 Jun 2020) -- End

            StrQ &= "SELECT " & _
                        "  CAST(0 AS BIT) AS IsChecked " & _
                        " ,CAST(0 AS BIT) AS IsError " & _
                        " ,lnloan_advance_tran.loanadvancetranunkid AS loanadvancetranunkid " & _
                        " ,prpayment_tran.paymenttranunkid AS paymenttranunkid " & _
                        " ,lnloan_advance_tran.loanvoucher_no AS VoucherNo " & _
                        " ,lnloan_advance_tran.periodunkid AS periodunkid " & _
                        " ,asp.period_name AS AssignedPeriod " & _
                        " ,lnloan_advance_tran.deductionperiodunkid AS deductionperiodunkid " & _
                        " ,DDP.period_name AS DeductionPeriod " & _
                        " ,hremployee_master.employeeunkid AS employeeunkid " & _
                        " ,ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                        " ,ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'') + '  ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                        " ,lnloan_scheme_master.loanschemeunkid AS loanschemeunkid " & _
                        " ,CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_scheme_master.code + ' - ' +   lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
                        " ,lnloan_advance_tran.isloan AS isloan " & _
                        " ,CASE WHEN lnloan_advance_tran.isloan=1 THEN " & enLoanAdvance.LOAN & " " & _
                        "       WHEN lnloan_advance_tran.isloan=0 THEN " & enLoanAdvance.ADVANCE & " " & _
                        "  END AS modeid " & _
                        " ,CASE WHEN lnloan_advance_tran.isloan=1 THEN @Loan WHEN lnloan_advance_tran.isloan=0 THEN @Advance END AS Mode " & _
                        " ,lnloan_advance_tran.calctype_id AS calctype_id " & _
                        " ,ISNULL(lnloan_advance_tran.mapped_tranheadunkid, 0) AS mapped_tranheadunkid "
            'Sohail (29 Jan 2020) - [employeecode]
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            StrQ &= "     ,CASE lnloan_advance_tran.calctype_id "
            For Each pair In dicLoanCalc
                StrQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            StrQ &= "      END AS LoanCalcType "
            StrQ &= "     ,lnloan_advance_tran.interest_calctype_id " & _
                        " ,CASE lnloan_advance_tran.interest_calctype_id "
            For Each pair In dicLoanInterestCalc
                StrQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            StrQ &= "      END AS LoanInterestCalcType "
            StrQ &= "     ,CASE WHEN lnloan_advance_tran.isloan=1 THEN ISNULL(lnloan_advance_tran.loan_amount,0) + ISNULL(TSUM.total_topup,0) " & _
                        "       WHEN lnloan_advance_tran.isloan=0 THEN ISNULL(lnloan_advance_tran.advance_amount,0) + ISNULL(TSUM.total_topup,0) " & _
                        "  END AS LoanAdvanceAmount " & _
                        " ,ISNULL(CRR.currency_sign,'') AS currency_sign " & _
                        " , ISNULL(#TableBalance.cf_balancepaidcurrency, lnloan_advance_tran.balance_amountpaidcurrency) AS balance_amount " & _
                        " , ISNULL(#TableBalance.cf_balancepaidcurrency, lnloan_advance_tran.balance_amountpaidcurrency) AS settelment_amount " & _
                        " ,ST.statusunkid AS statusunkid " & _
                        " ,CASE WHEN ST.statusunkid = " & enLoanStatus.IN_PROGRESS & " THEN @InProgress " & _
                        "       WHEN ST.statusunkid = " & enLoanStatus.ON_HOLD & " THEN @OnHold " & _
                        "       WHEN ST.statusunkid = " & enLoanStatus.WRITTEN_OFF & " THEN @WrittenOff " & _
                        "  END AS loan_status " & _
                        " ,ST.changestatusunkid " & _
                        " ,CASE WHEN ST.changestatusunkid = 0 THEN '' " & _
                        "       WHEN ST.changestatusunkid = " & enLoanStatus.IN_PROGRESS & " THEN @InProgress " & _
                        "       WHEN ST.changestatusunkid = " & enLoanStatus.ON_HOLD & " THEN @OnHold " & _
                        "       WHEN ST.changestatusunkid = " & enLoanStatus.WRITTEN_OFF & " THEN @WrittenOff " & _
                        "  END AS ChangedStatus " & _
                   "FROM lnloan_advance_tran " & _
                        " LEFT JOIN #TableBalance ON #TableBalance.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                        " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
                        " LEFT JOIN prpayment_tran ON prpayment_tran.referencetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                        "       AND prpayment_tran.isvoid=0 " & _
                        "       AND prpayment_tran.paytypeid = 1 " & _
                        "       AND prpayment_tran.referenceid IN(1,2) " & _
                        " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_advance_tran.employeeunkid " & _
                        " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
                        " LEFT JOIN cfcommon_period_tran AS ASP ON ASP.periodunkid = lnloan_advance_tran.periodunkid " & _
                                    "AND ASP.modulerefid = 1 " & _
                        " LEFT JOIN cfcommon_period_tran AS DDP ON DDP.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
                                    "AND DDP.modulerefid = 1 " & _
                        " LEFT JOIN " & _
                        " ( " & _
                        "   SELECT " & _
                        "        countryunkid AS countryunkid " & _
                        "       ,currency_sign AS currency_sign " & _
                        "       ,ROW_NUMBER() OVER ( PARTITION BY countryunkid ORDER BY exchange_date DESC ) AS CRNo " & _
                        "   FROM cfexchange_rate " & _
                        "   WHERE isactive = 1 " & _
                        " ) AS CRR ON CRR.countryunkid = lnloan_advance_tran.countryunkid AND CRR.CRNo = 1 " & _
                        " LEFT JOIN " & _
                        " ( " & _
                        "   SELECT " & _
                        "        lnloan_advance_tran.loanadvancetranunkid " & _
                        "       ,lnloan_status_tran.statusunkid " & _
                        "       ,0 AS changestatusunkid " & _
                        "       ,DENSE_RANK() OVER (PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY lnloan_status_tran.status_date DESC, lnloan_status_tran.loanstatustranunkid DESC) AS RNo " & _
                        "   FROM lnloan_advance_tran " & _
                        "       LEFT JOIN lnloan_status_tran ON lnloan_status_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                        " WHERE lnloan_advance_tran.isvoid=0 and lnloan_status_tran.isvoid=0 " & _
                        " ) AS ST ON ST.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                        " LEFT JOIN " & _
                        " ( " & _
                        "   SELECT " & _
                        "        lnloan_advance_tran.loanadvancetranunkid " & _
                        "       ,ISNULL(SUM(topup_amount),0) AS total_topup " & _
                        "   FROM lnloan_advance_tran " & _
                        "       LEFT JOIN lnloan_topup_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_topup_tran.loanadvancetranunkid " & _
                        "   WHERE lnloan_advance_tran.isvoid = 0 " & _
                        "       AND lnloan_topup_tran.isvoid = 0 " & _
                        "   GROUP BY lnloan_advance_tran.loanadvancetranunkid " & _
                        " ) AS TSUM ON TSUM.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid "
            'Sohail (05 Jun 2020) -- Start
            'NMB Issue # : Wrong write off amount was coming on global loan change status screen.
            'LEFT JOIN #TableBalance
            'lnloan_advance_tran.balance_amountpaidcurrency AS balance_amount = ISNULL(#TableBalance.cf_balancepaidcurrency, lnloan_advance_tran.balance_amountpaidcurrency) AS balance_amount
            'lnloan_advance_tran.balance_amountpaidcurrency AS settelment_amount = ISNULL(#TableBalance.cf_balancepaidcurrency, lnloan_advance_tran.balance_amountpaidcurrency) AS settelment_amount
            'Sohail (05 Jun 2020) -- End
            
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE ST.RNo=1 " & _
                    "   AND (prpayment_tran.paymenttranunkid IS NOT NULL OR lnloan_process_pending_loan.isexternal_entity = 1) " & _
                    "   AND CONVERT(CHAR(8),lnloan_advance_tran.effective_date,112) <= @PeriodEndDate "
            'Nilay (21-Dec-2016) -- [AND CONVERT(CHAR(8),lnloan_advance_tran.effective_date,112) <= @PeriodEndDate]

            If xIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & xFilterString
            End If

            If strEmployeeIds.Trim.Length > 0 Then
                StrQ &= " AND lnloan_advance_tran.employeeunkid IN(" & strEmployeeIds & ") "
            End If

            'Sohail (05 Jun 2020) -- Start
            'NMB Issue # : Wrong write off amount was coming on global loan change status screen.
            StrQ &= " DROP TABLE #TableBalance "
            'Sohail (05 Jun 2020) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
            'Nilay (21-Dec-2016) -- Start
            objDataOperation.AddParameter("@PeriodEndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEndDate))
            'Nilay (21-Dec-2016) -- End

            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetListForGlobalChangeStatus ; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'Nilay (04-Nov-2016) -- End


    Public Function GetLoanCalculationTypeList(Optional ByVal strListName As String = "List" _
                                               , Optional ByVal blnAddSelect As Boolean = False _
                                               , Optional ByVal blnShowBlankInSelect As Boolean = False _
                                               , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                               ) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Dim objDo As clsDataOperation
        If xDataOp Is Nothing Then
            objDo = New clsDataOperation
        Else
            objDo = xDataOp
        End If
        objDo.ClearParameters()

        'Sohail (03 May 2018) -- End
        Try
            'Using objDo As New clsDataOperation 'Sohail (03 May 2018)
                If blnAddSelect = True Then
                    If blnShowBlankInSelect = False Then
                        StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
                    Else
                        StrQ = "SELECT 0 AS Id, '' AS Name UNION "
                    End If
                End If
                StrQ &= "SELECT '" & enLoanCalcId.Simple_Interest & "', @Simple_Interest AS Name UNION " & _
                        "SELECT '" & enLoanCalcId.Reducing_Amount & "', @Reducing_Amount AS Name UNION " & _
                        "SELECT '" & enLoanCalcId.No_Interest & "', @No_Interest AS Name " & _
                    "UNION SELECT " & enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI & ", @Reducing_Balance_With_Fixed_Pricipal_EMI AS Name " & _
                    "UNION SELECT " & enLoanCalcId.No_Interest_With_Mapped_Head & ", @No_Interest_With_Mapped_Head AS Name "
            'Sohail (29 Apr 2019) - [No_Interest_With_Mapped_Head]

                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Select"))
                objDo.AddParameter("@Simple_Interest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Simple Interest"))
                objDo.AddParameter("@Reducing_Amount", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Reducing Balance"))
                objDo.AddParameter("@No_Interest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "No Interest"))
                objDo.AddParameter("@Reducing_Balance_With_Fixed_Pricipal_EMI", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Reducing Balance With Fixed Pricipal EMI"))
            objDo.AddParameter("@No_Interest_With_Mapped_Head", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "No Interest With Mapped Head")) 'Sohail (29 Apr 2019)

                dsList = objDo.ExecQuery(StrQ, strListName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            'End Using 'Sohail (03 May 2018)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetLoanCalculationTypeList ; Module Name: " & mstrModuleName)
        Finally
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDo = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Public Function GetLoan_Interest_Calculation_Type(Optional ByVal strListName As String = "List" _
                                                      , Optional ByVal blnAddSelect As Boolean = True _
                                                      , Optional ByVal blnShowBlankInSelect As Boolean = False _
                                                      , Optional ByVal blnAddByTenure As Boolean = True _
                                                      , Optional ByVal blnAddSimpleInterest As Boolean = True _
                                                      , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                      ) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        'Sohail (23 May 2017) - [blnAddSimpleInterest]
        'Sohail (14 Mar 2017) - [blnAddByTenure]
        'Nilay (23-Feb-2016) -- [blnShowBlankInSelect]

        Dim strQ As String = String.Empty
        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            If blnAddSelect = True Then
                'Nilay (23-Feb-2016) -- Start
                'strQ = "SELECT 0 AS Id,@Select AS NAME UNION "
                If blnShowBlankInSelect = False Then
                    strQ = "SELECT 0 AS Id, @Select AS Name UNION "
                Else
                    strQ = "SELECT 0 AS Id, '' AS Name UNION "
                End If
                'Nilay (23-Feb-2016) -- End
            End If

            strQ &= " SELECT " & enLoanInterestCalcType.DAILY & " AS Id,@DAILY AS NAME " & _
                    "UNION SELECT " & enLoanInterestCalcType.MONTHLY & " AS Id,@MONTHLY AS NAME "

            'Sohail (14 Mar 2017) -- Start
            'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
            If blnAddByTenure = True Then
                strQ &= " UNION SELECT " & enLoanInterestCalcType.BY_TENURE & " AS Id,@BY_TENURE AS NAME "
                objDataOperation.AddParameter("@BY_TENURE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "By Tenure"))
            End If
            'Sohail (14 Mar 2017) -- End

            'Sohail (23 May 2017) -- Start
            'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
            If blnAddSimpleInterest = True Then
                strQ &= " UNION SELECT " & enLoanInterestCalcType.SIMPLE_INTEREST & " AS Id,@SIMPLE_INTEREST AS NAME "
                'Hemant (19 Mar 2019) -- Start
                'ENHANCEMENT : Add Missing Column fields of AT Tables & Show its on "Audit & Trails" Module.
                'objDataOperation.AddParameter("@SIMPLE_INTEREST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Simple Interest"))
                objDataOperation.AddParameter("@SIMPLE_INTEREST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Simple Interest"))
                'Hemant (19 Mar 2019) -- End
            End If
            'Sohail (23 May 2017) -- End

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Select"))
            objDataOperation.AddParameter("@DAILY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Daily"))
            objDataOperation.AddParameter("@MONTHLY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Monthly"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoan_Interest_Calculation_Type", mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function
    'Sohail (15 Dec 2015) -- End

    ' Shani(26-Nov-2015) -- Start
    ' ENHANCEMENT : Add Loan Import Screen
    Public Function GetExistLoan(ByVal intSchemeId As Integer, ByVal intAssignPeriod As Integer, ByVal intDeducationPeriod As Integer, _
                                 ByVal intEmpUnkId As Integer, ByVal blnIsLaon As Boolean, ByVal decLoanAmt As Decimal, _
                                 Optional ByVal strListName As String = "List") As DataSet
        'Hemant(15 Dec 2018) - [decLoanAmt]
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Using objDo As New clsDataOperation
                'Nilay (27-Oct-2016) -- Start
                'CHANGES : Loan Importation validations & Written off checkpoints
                'StrQ = "SELECT loanadvancetranunkid FROM lnloan_advance_tran " & _
                '      " WHERE deductionperiodunkid = @deductionperiodunkid " & _
                '      " AND periodunkid = @periodunkid AND employeeunkid = @employeeunkid "
                'If blnIsLaon Then
                '    StrQ &= " AND loanschemeunkid = @loanschemeunkid and isloan = 1 "
                '    objDo.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeId)
                'Else
                '    StrQ &= " AND isloan = 0 "
                'End If
                ''S.SANDEEP [05 DEC 2015] -- START
                'StrQ &= " AND isvoid = 0 "
                ''S.SANDEEP [05 DEC 2015] -- END
                StrQ = " SELECT " & _
                            " loanadvancetranunkid " & _
                       " FROM lnloan_advance_tran " & _
                       " WHERE deductionperiodunkid = @deductionperiodunkid " & _
                            " AND periodunkid = @periodunkid " & _
                            " AND employeeunkid = @employeeunkid "
                If blnIsLaon Then
                    StrQ &= " AND loanschemeunkid = @loanschemeunkid and isloan = 1 "
                    objDo.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeId)
                Else
                    'Hemant (15 Dec 2018) -- Start
                    'Enhancement : Cant import advance for an employee with an existing advance in the same for BBLMS
                    'StrQ &= " AND isloan = 0 "
                    'Sohail (18 Dec 2018) -- Start
                    'BBLMS Issue - Incorrect sysntax near 'loan_amount' in 76.1.
                    'StrQ &= "loan_amount = @loan_amount  AND isloan = 0 "
                    'objDo.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decLoanAmt)
                    StrQ &= " AND advance_amount = @advance_amount  AND isloan = 0 "
                    objDo.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decLoanAmt)
                    'Sohail (18 Dec 2018) -- End
                    'Hemant (15 Dec 2018) -- End
                End If

                StrQ &= " AND isvoid = 0 AND loan_statusunkid IN(" & enLoanStatus.IN_PROGRESS & ", " & enLoanStatus.ON_HOLD & ") "

                objDo.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeducationPeriod)
                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssignPeriod)
                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkId)

                dsList = objDo.ExecQuery(StrQ, strListName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetExistLoan ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    ' Shani(26-Nov-2015) -- End

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 57
    Public Function GetExistLoan(ByVal intSchemeId As Integer, ByVal intEmpUnkId As Integer, ByVal blnIsLaon As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal decAdvanceAmount As Decimal = 0) As Boolean
        'Sohail (21 May 2020) - - [decAdvanceAmount]
        Dim StrQ As String = String.Empty
        Dim objDataOperation As clsDataOperation = Nothing
        Dim blnFlag As Boolean = False
        Dim iCount As Integer = -1
        Dim exForce As Exception = Nothing
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT 1 " & _
                   "FROM lnloan_advance_tran " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         lst.loanadvancetranunkid " & _
                   "        ,lst.statusunkid " & _
                   "        ,row_number()over(partition by lst.loanadvancetranunkid order by lst.status_date desc) as rno " & _
                   "    FROM lnloan_status_tran AS lst " & _
                   "        JOIN lnloan_advance_tran AS lat on lat.loanadvancetranunkid = lst.loanadvancetranunkid " & _
                   "    WHERE lat.isvoid = 0  AND lst.isvoid = 0 AND lat.employeeunkid = @empid " & _
                   "    AND lat.loanschemeunkid = @schemeid AND lat.isloan = @isloan " & _
                   ") AS st ON st.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid and st.rno = 1 " & _
                   "   AND st.statusunkid in (1,2) " & _
                   "WHERE isvoid = 0 AND lnloan_advance_tran.employeeunkid = @empid AND lnloan_advance_tran.loanschemeunkid = @schemeid " & _
                   " AND lnloan_advance_tran.isloan = @isloan "

            'Sohail (21 May 2020) -- Start
            'NMB Issue # : System was allowing to assign same loan even if same loan is in progress or on hold on loan import.
            '             15 Dec 2018 Enhancement : Cant import advance for an employee with an existing advance in the same for BBLMS
            If blnIsLaon = False Then
                StrQ &= " AND lnloan_advance_tran.advance_amount = @advance_amount "
                objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decAdvanceAmount)
            End If
            'Sohail (21 May 2020) -- End

            objDataOperation.AddParameter("@schemeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeId)
            objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkId)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsLaon)

            iCount = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCount > 0 Then
                blnFlag = True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExistLoan; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return blnFlag
    End Function
    'S.SANDEEP [20-SEP-2017] -- END

'''' <summary>
'''' Purpose: 
'''' Developer: Sandeep J. Sharma
'''' </summary>
'''' 

'Public Class clsLoan_Advance1
'    Private Shared ReadOnly mstrModuleName As String = "clsLoan_Advance1"
'    Dim mstrMessage As String = ""
'    Private objStatusTran As New clsLoan_Status_tran

'#Region " Private variables "
'    Private mintLoanadvancetranunkid As Integer
'    Private mstrLoanvoucher_No As String = String.Empty
'    Private mdtEffective_Date As Date
'    Private mintYearunkid As Integer
'    Private mintPeriodunkid As Integer
'    Private mintEmployeeunkid As Integer
'    Private mintApproverunkid As Integer
'    Private mstrLoan_Purpose As String = String.Empty
'    Private mdecBalance_Amount As Decimal 'Sohail (11 May 2011)
'    Private mblnIsbrought_Forward As Boolean
'    Private mblnIsloan As Boolean
'    Private mintLoanschemeunkid As Integer
'    Private mdecLoan_Amount As Decimal 'Sohail (11 May 2011)
'    Private mdecInterest_Rate As Decimal 'Sohail (11 May 2011)
'    Private mintLoan_Duration As Integer
'    Private mdecInterest_Amount As Decimal 'Sohail (11 May 2011)
'    Private mdecNet_Amount As Decimal 'Sohail (11 May 2011)
'    Private mintCalctype_Id As Integer
'    Private mintLoanscheduleunkid As Integer
'    Private mintEmi_Tenure As Integer
'    Private mdecEmi_Amount As Decimal 'Sohail (11 May 2011)
'    Private mdtEmi_Start_Date As Date
'    Private mdtEmi_End_Date As Date
'    Private mdecAdvance_Amount As Decimal 'Sohail (11 May 2011)
'    Private mintUserunkid As Integer
'    Private mblnIsvoid As Boolean
'    Private mintVoiduserunkid As Integer
'    Private mdtVoiddatetime As Date
'    Private mintLoanStatus As Integer
'    Private mintProcesspendingloanunkid As Integer
'    Private mstrVoidreason As String = String.Empty

'    'Sandeep [ 21 Aug 2010 ] -- Start
'    Private mintDeductionperiodunkid As Integer
'    'Sandeep [ 21 Aug 2010 ] -- End 

'    'S.SANDEEP [ 19 JULY 2012 ] -- START
'    'Enhancement : TRA Changes
'    Private mstrWebFormName As String = String.Empty
'    'S.SANDEEP [ 19 JULY 2012 ] -- END

'    'S.SANDEEP [ 13 AUG 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mintLogEmployeeUnkid As Integer = -1
'    Private mstrWebClientIP As String = String.Empty
'    Private mstrWebHostName As String = String.Empty
'    'S.SANDEEP [ 13 AUG 2012 ] -- END

'    'S.SANDEEP [ 29 May 2013 ] -- START
'    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
'    Dim objDataOpr As clsDataOperation
'    'S.SANDEEP [ 29 May 2013 ] -- END

'    'S.SANDEEP [ 18 JAN 2014 ] -- START
'    Private mdecLoanBF_Amount As Decimal = 0
'    'S.SANDEEP [ 18 JAN 2014 ] -- END

'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loanadvancetranunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loanadvancetranunkid() As Integer
'        Get
'            Return mintLoanadvancetranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanadvancetranunkid = value
'            Call GetData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loanvoucher_no
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loanvoucher_No() As String
'        Get
'            Return mstrLoanvoucher_No
'        End Get
'        Set(ByVal value As String)
'            mstrLoanvoucher_No = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set effective_date
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Effective_Date() As Date
'        Get
'            Return mdtEffective_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtEffective_Date = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set yearunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Yearunkid() As Integer
'        Get
'            Return mintYearunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintYearunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set periodunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Periodunkid() As Integer
'        Get
'            Return mintPeriodunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintPeriodunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set employeeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Employeeunkid() As Integer
'        Get
'            Return mintEmployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set approverunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Approverunkid() As Integer
'        Get
'            Return mintApproverunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintApproverunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loan_purpose
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loan_Purpose() As String
'        Get
'            Return mstrLoan_Purpose
'        End Get
'        Set(ByVal value As String)
'            mstrLoan_Purpose = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set balance_amount
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Balance_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecBalance_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecBalance_Amount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isbrought_forward
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isbrought_Forward() As Boolean
'        Get
'            Return mblnIsbrought_Forward
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsbrought_Forward = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isloan
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isloan() As Boolean
'        Get
'            Return mblnIsloan
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsloan = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loanschemeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loanschemeunkid() As Integer
'        Get
'            Return mintLoanschemeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanschemeunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loan_amount
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loan_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecLoan_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecLoan_Amount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set interest_rate
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Interest_Rate() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecInterest_Rate
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecInterest_Rate = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loan_duration
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loan_Duration() As Integer
'        Get
'            Return mintLoan_Duration
'        End Get
'        Set(ByVal value As Integer)
'            mintLoan_Duration = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set interest_amount
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Interest_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecInterest_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecInterest_Amount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set net_amount
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Net_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecNet_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecNet_Amount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set calctype_id
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Calctype_Id() As Integer
'        Get
'            Return mintCalctype_Id
'        End Get
'        Set(ByVal value As Integer)
'            mintCalctype_Id = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loanscheduleunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loanscheduleunkid() As Integer
'        Get
'            Return mintLoanscheduleunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanscheduleunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set emi_tenure
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Emi_Tenure() As Integer
'        Get
'            Return mintEmi_Tenure
'        End Get
'        Set(ByVal value As Integer)
'            mintEmi_Tenure = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set emi_amount
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Emi_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecEmi_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecEmi_Amount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set emi_start_date
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Emi_Start_Date() As Date
'        Get
'            Return mdtEmi_Start_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtEmi_Start_Date = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set emi_end_date
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Emi_End_Date() As Date
'        Get
'            Return mdtEmi_End_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtEmi_End_Date = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set advance_amount
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Advance_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecAdvance_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecAdvance_Amount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set Loan Status
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _LoanStatus() As Integer
'        Get
'            Return mintLoanStatus
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanStatus = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set processpendingloanunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Processpendingloanunkid() As Integer
'        Get
'            Return mintProcesspendingloanunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintProcesspendingloanunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = value
'        End Set
'    End Property

'    'Sandeep [ 21 Aug 2010 ] -- Start
'    ''' <summary>
'    ''' Purpose: Get or Set deductionperiodunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Deductionperiodunkid() As Integer
'        Get
'            Return mintDeductionperiodunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintDeductionperiodunkid = value
'        End Set
'    End Property
'    'Sandeep [ 21 Aug 2010 ] -- End 

'    'S.SANDEEP [ 19 JULY 2012 ] -- START
'    'Enhancement : TRA Changes

'    Public Property _WebFormName() As String
'        Get
'            Return mstrWebFormName
'        End Get
'        Set(ByVal value As String)
'            mstrWebFormName = value
'        End Set
'    End Property

'    'S.SANDEEP [ 19 JULY 2012 ] -- END

'    'S.SANDEEP [ 13 AUG 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
'        Set(ByVal value As Integer)
'            mintLogEmployeeUnkid = value
'        End Set
'    End Property
'    Public WriteOnly Property _WebClientIP() As String
'        Set(ByVal value As String)
'            mstrWebClientIP = value
'        End Set
'    End Property

'    Public WriteOnly Property _WebHostName() As String
'        Set(ByVal value As String)
'            mstrWebHostName = value
'        End Set
'    End Property
'    'S.SANDEEP [ 13 AUG 2012 ] -- END

'    'S.SANDEEP [ 29 May 2013 ] -- START
'    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
'    Public WriteOnly Property _DataOpr() As clsDataOperation
'        Set(ByVal value As clsDataOperation)
'            objDataOpr = value
'        End Set
'    End Property
'    'S.SANDEEP [ 29 May 2013 ] -- END


'    'S.SANDEEP [ 18 JAN 2014 ] -- START
'    Public Property _LoanBF_Amount() As Decimal
'        Get
'            Return mdecLoanBF_Amount
'        End Get
'        Set(ByVal value As Decimal)
'            mdecLoanBF_Amount = value
'        End Set
'    End Property
'    'S.SANDEEP [ 18 JAN 2014 ] -- END

'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objDataOperation As clsDataOperation
'        'S.SANDEEP [ 29 May 2013 ] -- START
'        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
'        'Dim objDataOperation As New clsDataOperation
'        If objDataOpr Is Nothing Then
'            objDataOperation = New clsDataOperation
'        Else
'            objDataOperation = objDataOpr
'        End If
'        'S.SANDEEP [ 29 May 2013 ] -- END

'        Try

'            'Sandeep [ 21 Aug 2010 ] -- Start
'            'strQ = "SELECT " & _
'            '          "  loanadvancetranunkid " & _
'            '          ", loanvoucher_no " & _
'            '          ", effective_date " & _
'            '          ", yearunkid " & _
'            '          ", periodunkid " & _
'            '          ", employeeunkid " & _
'            '          ", approverunkid " & _
'            '          ", loan_purpose " & _
'            '          ", balance_amount " & _
'            '          ", isbrought_forward " & _
'            '          ", isloan " & _
'            '          ", loanschemeunkid " & _
'            '          ", loan_amount " & _
'            '          ", interest_rate " & _
'            '          ", loan_duration " & _
'            '          ", interest_amount " & _
'            '          ", net_amount " & _
'            '          ", calctype_id " & _
'            '          ", loanscheduleunkid " & _
'            '          ", emi_tenure " & _
'            '          ", emi_amount " & _
'            '          ", advance_amount " & _
'            '          ", userunkid " & _
'            '          ", isvoid " & _
'            '          ", voiduserunkid " & _
'            '          ", voiddatetime " & _
'            '          ", loan_statusunkid " & _
'            '          ", ISNULL(processpendingloanunkid,-1) As processpendingloanunkid " & _
'            '          ", voidreason " & _
'            '         "FROM lnloan_advance_tran " & _
'            '         "WHERE loanadvancetranunkid = @loanadvancetranunkid "
'            strQ = "SELECT " & _
'                      "  loanadvancetranunkid " & _
'                      ", loanvoucher_no " & _
'                      ", effective_date " & _
'                      ", yearunkid " & _
'                      ", periodunkid " & _
'                      ", employeeunkid " & _
'                      ", approverunkid " & _
'                      ", loan_purpose " & _
'                      ", balance_amount " & _
'                      ", isbrought_forward " & _
'                      ", isloan " & _
'                      ", loanschemeunkid " & _
'                      ", loan_amount " & _
'                      ", interest_rate " & _
'                      ", loan_duration " & _
'                      ", interest_amount " & _
'                      ", net_amount " & _
'                      ", calctype_id " & _
'                      ", loanscheduleunkid " & _
'                      ", emi_tenure " & _
'                      ", emi_amount " & _
'                      ", advance_amount " & _
'                            ", loan_statusunkid " & _
'                      ", userunkid " & _
'                      ", isvoid " & _
'                      ", voiduserunkid " & _
'                      ", voiddatetime " & _
'                      ", voidreason " & _
'                            ", processpendingloanunkid " & _
'                            ", ISNULL(deductionperiodunkid,0) As deductionperiodunkid " & _
'                     "FROM lnloan_advance_tran " & _
'                     "WHERE loanadvancetranunkid = @loanadvancetranunkid "
'            'Sandeep [ 21 Aug 2010 ] -- End 


'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintLoanadvancetranunkid = CInt(dtRow.Item("loanadvancetranunkid"))
'                mstrLoanvoucher_No = dtRow.Item("loanvoucher_no").ToString
'                mdtEffective_Date = dtRow.Item("effective_date")
'                mintYearunkid = CInt(dtRow.Item("yearunkid"))
'                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
'                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
'                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
'                mstrLoan_Purpose = dtRow.Item("loan_purpose").ToString
'                mdecBalance_Amount = CDec(dtRow.Item("balance_amount")) 'Sohail (11 May 2011)
'                mblnIsbrought_Forward = CBool(dtRow.Item("isbrought_forward"))
'                mblnIsloan = CBool(dtRow.Item("isloan"))
'                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
'                mdecLoan_Amount = CDec(dtRow.Item("loan_amount")) 'Sohail (11 May 2011)
'                mdecInterest_Rate = CDec(dtRow.Item("interest_rate")) 'Sohail (11 May 2011)
'                mintLoan_Duration = CInt(dtRow.Item("loan_duration"))
'                mdecInterest_Amount = CDec(dtRow.Item("interest_amount")) 'Sohail (11 May 2011)
'                mdecNet_Amount = CDec(dtRow.Item("net_amount")) 'Sohail (11 May 2011)
'                mintCalctype_Id = CInt(dtRow.Item("calctype_id"))
'                mintLoanscheduleunkid = CInt(dtRow.Item("loanscheduleunkid"))
'                mintEmi_Tenure = CInt(dtRow.Item("emi_tenure"))
'                mdecEmi_Amount = CDec(dtRow.Item("emi_amount")) 'Sohail (11 May 2011)
'                mdecAdvance_Amount = CDec(dtRow.Item("advance_amount")) 'Sohail (11 May 2011)
'                mintUserunkid = CInt(dtRow.Item("userunkid"))
'                mblnIsvoid = CBool(dtRow.Item("isvoid"))
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))

'                If IsDBNull(dtRow.Item("voiddatetime")) Then
'                    mdtVoiddatetime = Nothing
'                Else
'                    mdtVoiddatetime = dtRow.Item("voiddatetime")
'                End If

'                mintLoanStatus = dtRow.Item("loan_statusunkid")
'                mstrVoidreason = dtRow.Item("voidreason").ToString

'                'Sandeep [ 21 Aug 2010 ] -- Start
'                mintDeductionperiodunkid = CInt(dtRow.Item("deductionperiodunkid"))
'                'Sandeep [ 21 Aug 2010 ] -- End 

'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()

'            'S.SANDEEP [ 29 May 2013 ] -- START
'            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
'            'objDataOperation = Nothing
'            'S.SANDEEP [ 29 May 2013 ] -- END


'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal intLoanAdvanceTranUnkID As Integer = 0, Optional ByVal intEmpUnkID As Integer = 0, Optional ByVal intStatusID As Integer = 0 _
'                            , Optional ByVal dtPeriodStart As DateTime = Nothing _
'                            , Optional ByVal dtPeriodEnd As DateTime = Nothing, Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet 'Sohail (06 Jan 2012)
'        'S.SANDEEP [ 29 May 2013 {strUserAccessLevelFilterString}] -- START -- END

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try
'            'Sohail (21 Aug 2010) -- Start
'            '***Changes:Set LoanScheme = '@Advance' when isloan = 0
'            'strQ = "SELECT " & _
'            '          " lnloan_advance_tran.loanvoucher_no AS VocNo " & _
'            '          ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
'            '          ",lnloan_scheme_master.name AS LoanScheme " & _
'            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
'            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
'            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_advance_tran.net_amount ELSE lnloan_advance_tran.advance_amount END AS Amount " & _
'            '          ",cfcommon_period_tran.period_name AS PeriodName " & _
'            '          ",lnloan_advance_tran.loan_statusunkid " & _
'            '          ",lnloan_advance_tran.periodunkid " & _
'            '          ",lnloan_advance_tran.loanadvancetranunkid " & _
'            '          ",hremployee_master.employeeunkid " & _
'            '          ",lnloan_scheme_master.loanschemeunkid " & _
'            '          ",lnloan_advance_tran.isloan " & _
'            '          ",convert(char(8),lnloan_advance_tran.effective_date,112) AS effective_date " & _
'            '          ",lnloan_advance_tran.balance_amount " & _
'            '          ",lnloan_advance_tran.isloan " & _
'            '          ",lnloan_advance_tran.loanschemeunkid " & _
'            '          ",lnloan_advance_tran.emi_amount " & _
'            '          ",lnloan_advance_tran.advance_amount " & _
'            '          ",lnloan_advance_tran.loan_statusunkid " & _
'            '          ",lnloan_advance_tran.processpendingloanunkid " & _
'            '        "FROM lnloan_advance_tran " & _
'            '          "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            '          "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
'            '          "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
'            '        "WHERE ISNULL(lnloan_advance_tran.isvoid,0) = 0 "
'            'Sohail (11 Sep 2010) -- Start
'            'Cahnges : Deduction period start and end date added
'            'strQ = "SELECT " & _
'            '          " lnloan_advance_tran.loanvoucher_no AS VocNo " & _
'            '          ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
'            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
'            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
'            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
'            '          ",CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_advance_tran.net_amount ELSE lnloan_advance_tran.advance_amount END AS Amount " & _
'            '          ",cfcommon_period_tran.period_name AS PeriodName " & _
'            '          ",lnloan_advance_tran.loan_statusunkid " & _
'            '          ",lnloan_advance_tran.periodunkid " & _
'            '          ",lnloan_advance_tran.loanadvancetranunkid " & _
'            '          ",hremployee_master.employeeunkid " & _
'            '          ",lnloan_scheme_master.loanschemeunkid " & _
'            '          ",lnloan_advance_tran.isloan " & _
'            '          ",convert(char(8),lnloan_advance_tran.effective_date,112) AS effective_date " & _
'            '          ",lnloan_advance_tran.balance_amount " & _
'            '          ",lnloan_advance_tran.isloan " & _
'            '          ",lnloan_advance_tran.loanschemeunkid " & _
'            '          ",lnloan_advance_tran.emi_amount " & _
'            '          ",lnloan_advance_tran.advance_amount " & _
'            '          ",lnloan_advance_tran.loan_statusunkid " & _
'            '          ",lnloan_advance_tran.processpendingloanunkid " & _
'            '          ",lnloan_advance_tran.deductionperiodunkid " & _
'            '        "FROM lnloan_advance_tran " & _
'            '          "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            '          "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
'            '          "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
'            '        "WHERE ISNULL(lnloan_advance_tran.isvoid,0) = 0 "

'            'Sohail (04 Mar 2011)-Start
'            'Purpose : Get fields of lnloan_advance_tran as well as latest loan status detail from lnloan_status_tran
'            'Sandeep ( 18 JAN 2011 ) -- START
'            'strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
'            '              ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
'            '                "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
'            '                "+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                     "THEN lnloan_scheme_master.name " & _
'            '                     "ELSE @Advance " & _
'            '                "END AS LoanScheme " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
'            '                     "ELSE @Advance " & _
'            '                "END AS Loan_Advance " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
'            '                     "ELSE 2 " & _
'            '                "END AS Loan_AdvanceUnkid " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                     "THEN lnloan_advance_tran.net_amount " & _
'            '                     "ELSE lnloan_advance_tran.advance_amount " & _
'            '                "END AS Amount " & _
'            '          ",cfcommon_period_tran.period_name AS PeriodName " & _
'            '          ",lnloan_advance_tran.loan_statusunkid " & _
'            '          ",lnloan_advance_tran.periodunkid " & _
'            '          ",lnloan_advance_tran.loanadvancetranunkid " & _
'            '          ",hremployee_master.employeeunkid " & _
'            '          ",lnloan_scheme_master.loanschemeunkid " & _
'            '          ",lnloan_advance_tran.isloan " & _
'            '              ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
'            '          ",lnloan_advance_tran.balance_amount " & _
'            '          ",lnloan_advance_tran.isloan " & _
'            '          ",lnloan_advance_tran.loanschemeunkid " & _
'            '          ",lnloan_advance_tran.emi_amount " & _
'            '          ",lnloan_advance_tran.advance_amount " & _
'            '          ",lnloan_advance_tran.loan_statusunkid " & _
'            '          ",lnloan_advance_tran.processpendingloanunkid " & _
'            '          ",lnloan_advance_tran.deductionperiodunkid " & _
'            '              ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
'            '              ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
'            '        "FROM lnloan_advance_tran " & _
'            '          "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            '          "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
'            '          "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
'            '                "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
'            '        "WHERE ISNULL(lnloan_advance_tran.isvoid,0) = 0 "

'            'strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
'            '              ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
'            '                "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
'            '                "+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                     "THEN lnloan_scheme_master.name " & _
'            '                     "ELSE @Advance " & _
'            '                "END AS LoanScheme " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
'            '                     "ELSE @Advance " & _
'            '                "END AS Loan_Advance " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
'            '                     "ELSE 2 " & _
'            '                "END AS Loan_AdvanceUnkid " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                     "THEN lnloan_advance_tran.net_amount " & _
'            '                     "ELSE lnloan_advance_tran.advance_amount " & _
'            '                "END AS Amount " & _
'            '          ",cfcommon_period_tran.period_name AS PeriodName " & _
'            '          ",lnloan_advance_tran.loan_statusunkid " & _
'            '          ",lnloan_advance_tran.periodunkid " & _
'            '          ",lnloan_advance_tran.loanadvancetranunkid " & _
'            '          ",hremployee_master.employeeunkid " & _
'            '          ",lnloan_scheme_master.loanschemeunkid " & _
'            '          ",lnloan_advance_tran.isloan " & _
'            '              ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
'            '          ",lnloan_advance_tran.balance_amount " & _
'            '          ",lnloan_advance_tran.isloan " & _
'            '          ",lnloan_advance_tran.loanschemeunkid " & _
'            '          ",lnloan_advance_tran.emi_amount " & _
'            '          ",lnloan_advance_tran.advance_amount " & _
'            '          ",lnloan_advance_tran.loan_statusunkid " & _
'            '          ",lnloan_advance_tran.processpendingloanunkid " & _
'            '          ",lnloan_advance_tran.deductionperiodunkid " & _
'            '              ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
'            '              ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
'            '          ",isbrought_forward AS isbrought_forward " & _
'            '        "FROM lnloan_advance_tran " & _
'            '          "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            '          "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
'            '          "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
'            '                "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
'            '        "WHERE ISNULL(lnloan_advance_tran.isvoid,0) = 0 "



'            'Anjan (02 Mar 2012)-Start
'            'ENHANCEMENT : TRA COMMENTS on Rutta's Request
'            'strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
'            '              ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                     "THEN lnloan_scheme_master.name " & _
'            '                     "ELSE @Advance " & _
'            '                "END AS LoanScheme " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
'            '                     "ELSE @Advance " & _
'            '                "END AS Loan_Advance " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
'            '                     "ELSE 2 " & _
'            '                "END AS Loan_AdvanceUnkid " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                     "THEN lnloan_advance_tran.net_amount " & _
'            '                     "ELSE lnloan_advance_tran.advance_amount " & _
'            '                "END AS Amount " & _
'            '              ", cfcommon_period_tran.period_name AS PeriodName " & _
'            '              ", lnloan_advance_tran.loan_statusunkid " & _
'            '              ", lnloan_advance_tran.periodunkid " & _
'            '              ", lnloan_advance_tran.loanadvancetranunkid " & _
'            '              ", hremployee_master.employeeunkid " & _
'            '              ", lnloan_scheme_master.loanschemeunkid " & _
'            '              ", lnloan_advance_tran.isloan " & _
'            '              ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
'            '              ", lnloan_advance_tran.balance_amount " & _
'            '              ", lnloan_advance_tran.isloan " & _
'            '              ", lnloan_advance_tran.loanschemeunkid " & _
'            '              ", lnloan_advance_tran.emi_amount " & _
'            '              ", lnloan_advance_tran.advance_amount " & _
'            '              ", lnloan_advance_tran.loan_statusunkid " & _
'            '              ", lnloan_advance_tran.processpendingloanunkid " & _
'            '              ", lnloan_advance_tran.deductionperiodunkid " & _
'            '              ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
'            '              ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
'            '              ", isbrought_forward AS isbrought_forward " & _
'            '              ", LoanStatus.loanstatustranunkid " & _
'            '              ", CONVERT(CHAR(8), LoanStatus.status_date, 112) AS status_date " & _
'            '              ", LoanStatus.statusunkid " & _
'            '              ", LoanStatus.remark " & _
'            '        "FROM    lnloan_advance_tran " & _
'            '                "LEFT JOIN ( SELECT  lnloan_status_tran.loanstatustranunkid " & _
'            '                                  ", lnloan_status_tran.loanadvancetranunkid " & _
'            '                                  ", lnloan_status_tran.status_date " & _
'            '                                  ", lnloan_status_tran.statusunkid " & _
'            '                                  ", lnloan_status_tran.remark " & _
'            '                                  ", lnloan_status_tran.userunkid " & _
'            '                                  ", lnloan_status_tran.isvoid " & _
'            '                                  ", lnloan_status_tran.voiddatetime " & _
'            '                                  ", lnloan_status_tran.voiduserunkid " & _
'            '                            "FROM    lnloan_status_tran " & _
'            '                                    "INNER JOIN ( SELECT MAX(lnloan_status_tran.status_date) AS status_date " & _
'            '                                                      ", lnloan_status_tran.loanadvancetranunkid " & _
'            '                                                 "FROM   lnloan_status_tran " & _
'            '                                                 "WHERE  ISNULL(isvoid, 0) = 0 " & _
'            '                                                 "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
'            '                                               ") AS LoanStatus ON lnloan_status_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
'            '                                                                  "AND lnloan_status_tran.status_date = LoanStatus.status_date " & _
'            '                            "WHERE   ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
'            '                          ") AS LoanStatus ON lnloan_advance_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
'            '                "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            '                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
'            '                "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
'            '                "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
'            '        "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
'            'Sohail (29 Jun 2012) -- Start
'            'TRA - ENHANCEMENT
'            'strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
'            '              ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                     "THEN lnloan_scheme_master.name " & _
'            '                     "ELSE @Advance " & _
'            '                "END AS LoanScheme " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
'            '                     "ELSE @Advance " & _
'            '                "END AS Loan_Advance " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
'            '                     "ELSE 2 " & _
'            '                "END AS Loan_AdvanceUnkid " & _
'            '              ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                     "THEN lnloan_advance_tran.net_amount " & _
'            '                     "ELSE lnloan_advance_tran.advance_amount " & _
'            '                "END AS Amount " & _
'            '              ", cfcommon_period_tran.period_name AS PeriodName " & _
'            '              ", lnloan_advance_tran.loan_statusunkid " & _
'            '              ", lnloan_advance_tran.periodunkid " & _
'            '              ", lnloan_advance_tran.loanadvancetranunkid " & _
'            '              ", hremployee_master.employeeunkid " & _
'            '              ", lnloan_scheme_master.loanschemeunkid " & _
'            '              ", lnloan_advance_tran.isloan " & _
'            '              ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
'            '              ", lnloan_advance_tran.balance_amount " & _
'            '              ", lnloan_advance_tran.isloan " & _
'            '              ", lnloan_advance_tran.loanschemeunkid " & _
'            '              ", lnloan_advance_tran.emi_amount " & _
'            '              ", lnloan_advance_tran.advance_amount " & _
'            '              ", lnloan_advance_tran.loan_statusunkid " & _
'            '              ", lnloan_advance_tran.processpendingloanunkid " & _
'            '              ", lnloan_advance_tran.deductionperiodunkid " & _
'            '              ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
'            '              ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
'            '              ", isbrought_forward AS isbrought_forward " & _
'            '              ", LoanStatus.loanstatustranunkid " & _
'            '              ", CONVERT(CHAR(8), LoanStatus.status_date, 112) AS status_date " & _
'            '              ", LoanStatus.statusunkid " & _
'            '              ", LoanStatus.remark " & _
'            '              ", hremployee_master.employeecode as empcode " & _
'            '              ", lnloan_scheme_master.code as loancode " & _
'            '              ", lnloan_advance_tran.emi_tenure AS Intallments " & _
'            '        "FROM    lnloan_advance_tran " & _
'            '                "LEFT JOIN ( SELECT  lnloan_status_tran.loanstatustranunkid " & _
'            '                                  ", lnloan_status_tran.loanadvancetranunkid " & _
'            '                                  ", lnloan_status_tran.status_date " & _
'            '                                  ", lnloan_status_tran.statusunkid " & _
'            '                                  ", lnloan_status_tran.remark " & _
'            '                                  ", lnloan_status_tran.userunkid " & _
'            '                                  ", lnloan_status_tran.isvoid " & _
'            '                                  ", lnloan_status_tran.voiddatetime " & _
'            '                                  ", lnloan_status_tran.voiduserunkid " & _
'            '                            "FROM    lnloan_status_tran " & _
'            '                                    "INNER JOIN ( SELECT MAX(lnloan_status_tran.status_date) AS status_date " & _
'            '                                                      ", lnloan_status_tran.loanadvancetranunkid " & _
'            '                                                 "FROM   lnloan_status_tran " & _
'            '                                                 "WHERE  ISNULL(isvoid, 0) = 0 " & _
'            '                                                 "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
'            '                                               ") AS LoanStatus ON lnloan_status_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
'            '                                                                  "AND lnloan_status_tran.status_date = LoanStatus.status_date " & _
'            '                            "WHERE   ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
'            '                          ") AS LoanStatus ON lnloan_advance_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
'            '                "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            '                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
'            '                "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
'            '                "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
'            '        "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
'            strQ = "SELECT  lnloan_advance_tran.loanvoucher_no AS VocNo " & _
'                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
'                          ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'                                 "THEN lnloan_scheme_master.name " & _
'                                 "ELSE @Advance " & _
'                            "END AS LoanScheme " & _
'                          ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
'                                 "ELSE @Advance " & _
'                            "END AS Loan_Advance " & _
'                          ", CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
'                                 "ELSE 2 " & _
'                            "END AS Loan_AdvanceUnkid " & _
'                          ", CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'                                 "THEN lnloan_advance_tran.net_amount " & _
'                                 "ELSE lnloan_advance_tran.advance_amount " & _
'                            "END AS Amount " & _
'                          ", cfcommon_period_tran.period_name AS PeriodName " & _
'                          ", lnloan_advance_tran.loan_statusunkid " & _
'                          ", lnloan_advance_tran.periodunkid " & _
'                          ", lnloan_advance_tran.loanadvancetranunkid " & _
'                          ", hremployee_master.employeeunkid " & _
'                          ", lnloan_scheme_master.loanschemeunkid " & _
'                          ", lnloan_advance_tran.isloan " & _
'                          ", CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date " & _
'                          ", lnloan_advance_tran.balance_amount " & _
'                          ", lnloan_advance_tran.isloan " & _
'                          ", lnloan_advance_tran.loanschemeunkid " & _
'                          ", lnloan_advance_tran.emi_amount " & _
'                          ", lnloan_advance_tran.advance_amount " & _
'                          ", lnloan_advance_tran.loan_statusunkid " & _
'                          ", lnloan_advance_tran.processpendingloanunkid " & _
'                          ", lnloan_advance_tran.deductionperiodunkid " & _
'                          ", CONVERT(CHAR(8), TableDeductionPeriod.start_date, 112) AS Deduction_Start_Date " & _
'                          ", CONVERT(CHAR(8), TableDeductionPeriod.end_date, 112) AS Deduction_End_Date " & _
'                          ", isbrought_forward AS isbrought_forward " & _
'                          ", LoanStatus.loanstatustranunkid " & _
'                          ", CONVERT(CHAR(8), LoanStatus.status_date, 112) AS status_date " & _
'                          ", LoanStatus.statusunkid " & _
'                          ", LoanStatus.remark " & _
'                          ", hremployee_master.employeecode AS empcode " & _
'                          ", lnloan_scheme_master.code AS loancode " & _
'                          ", lnloan_advance_tran.emi_tenure AS Intallments " & _
'                          ", CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
'                          "         WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
'                          "         WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
'                          "         WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed END AS loan_status " & _
'                    "FROM    lnloan_advance_tran " & _
'                            "LEFT JOIN ( SELECT  lnloan_status_tran.loanstatustranunkid " & _
'                                              ", lnloan_status_tran.loanadvancetranunkid " & _
'                                              ", lnloan_status_tran.status_date " & _
'                                              ", lnloan_status_tran.statusunkid " & _
'                                              ", lnloan_status_tran.remark " & _
'                                        "FROM    lnloan_status_tran " & _
'                                                "JOIN ( SELECT   MAX(lnloan_status_tran.loanstatustranunkid) AS loanstatustranunkid " & _
'                                                              ", lnloan_status_tran.loanadvancetranunkid " & _
'                                        "FROM    lnloan_status_tran " & _
'                                                "INNER JOIN ( SELECT MAX(lnloan_status_tran.status_date) AS status_date " & _
'                                                                  ", lnloan_status_tran.loanadvancetranunkid " & _
'                                                             "FROM   lnloan_status_tran " & _
'                                                             "WHERE  ISNULL(isvoid, 0) = 0 " & _
'                                                             "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
'                                                                           ") AS Loan_Status ON lnloan_status_tran.loanadvancetranunkid = Loan_Status.loanadvancetranunkid " & _
'                                                                                  "AND lnloan_status_tran.status_date = Loan_Status.status_date " & _
'                                        "WHERE   ISNULL(lnloan_status_tran.isvoid, 0) = 0 " & _
'                                                       "GROUP BY lnloan_status_tran.loanadvancetranunkid " & _
'                                                     ") AS A ON A.loanstatustranunkid = lnloan_status_tran.loanstatustranunkid " & _
'                                      ") AS LoanStatus ON lnloan_advance_tran.loanadvancetranunkid = LoanStatus.loanadvancetranunkid " & _
'                            "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
'                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
'                                                              "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
'                            "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
'                            "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid " & _
'                                                                                  "AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
'                    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
'            'S.SANDEEP [ 29 May 2013 {loan_status}] -- START -- END

'            'Sohail (29 Jun 2012) -- End
'            'Anjan (02 Mar 2012)-End 




'            'Anjan (09 Aug 2011)-Start
'            'Issue : For including setting of acitve and inactive employee.
'            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'            'Sohail (06 Jan 2012) -- Start
'            'TRA - ENHANCEMENT
'            If dtPeriodStart <> Nothing AndAlso dtPeriodEnd <> Nothing Then
'                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
'                'Sohail (06 Jan 2012) -- End
'            End If
'            'Anjan (09 Aug 2011)-End 

'            'Anjan (24 Jun 2011)-Start
'            'Issue : According to privilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            'End If

'            'S.SANDEEP [ 01 JUNE 2012 ] -- START
'            'ENHANCEMENT : TRA DISCIPLINE CHANGES
'            'Select Case ConfigParameter._Object._UserAccessModeSetting
'            '    Case enAllocation.BRANCH
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.TEAM
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOB_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOBS
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            '        End If
'            'End Select

'            'S.SANDEEP [ 29 May 2013 ] -- START
'            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
'            'strQ &= UserAccessLevel._AccessLevelFilterString
'            If strUserAccessLevelFilterString = "" Then
'                strQ &= UserAccessLevel._AccessLevelFilterString
'            Else
'                strQ &= strUserAccessLevelFilterString
'            End If
'            'S.SANDEEP [ 29 May 2013 ] -- END

'            'S.SANDEEP [ 01 JUNE 2012 ] -- END


'            'S.SANDEEP [ 04 FEB 2012 ] -- END

'            'Anjan (24 Jun 2011)-End 



'            'Sandeep ( 18 JAN 2011 ) -- END 
'            'Sohail (04 Mar 2011)-End

'            'Sohail (11 Sep 2010) -- End
'            'Sohail (21 Aug 2010) -- End

'            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
'            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))

'            'S.SANDEEP [ 29 May 2013 ] -- START
'            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
'            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
'            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
'            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
'            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))
'            'S.SANDEEP [ 29 May 2013 ] -- END

'            'Sohail (04 Mar 2011)-Start

'            If intLoanAdvanceTranUnkID > 0 Then
'                strQ &= "AND lnloan_advance_tran.loanadvancetranunkid = @loanadvancetranunkid "
'                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceTranUnkID)
'            End If

'            If intEmpUnkID > 0 Then
'                strQ &= "AND lnloan_advance_tran.employeeunkid = @employeeunkid "
'                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
'            End If

'            If intStatusID > 0 Then
'                strQ &= "AND LoanStatus.statusunkid = @statusunkid "
'                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
'            End If

'            'Sohail (04 Mar 2011)-End

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (lnloan_advance_tran) </purpose>
'    Public Function Insert(Optional ByVal iUserId As Integer = 0, Optional ByVal intCompanyId As Integer = 0) As Boolean 'S.SANDEEP [ 29 May 2013 ] -- START -- END
'        'Public Function Insert() As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation
'        objDataOperation.BindTransaction()

'        'Anjan (21 Jan 2012)-Start
'        'ENHANCEMENT : TRA COMMENTS 
'        'Issue : To fix the issue on network of autocode concurrency when more than 1 user press save at same time.


'        'S.SANDEEP [ 28 MARCH 2012 ] -- START
'        'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
'        ''Sandeep [ 16 Oct 2010 ] -- Start
'        ''Issue : Auto No. Generation
'        'Dim intVocNoType As Integer = 0
'        'Dim strVocPrifix As String = ""
'        'Dim intNextVocNo As Integer = 0

'        'intVocNoType = ConfigParameter._Object._LoanVocNoType

'        'If intVocNoType = 1 Then
'        '    strVocPrifix = ConfigParameter._Object._LoanVocPrefix
'        '    intNextVocNo = ConfigParameter._Object._NextLoanVocNo
'        '    mstrLoanvoucher_No = strVocPrifix & intNextVocNo
'        'End If
'        ''Sandeep [ 16 Oct 2010 ] -- End 

'        'If isExist(mstrLoanvoucher_No) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
'        '    Return False
'        'End If
'        'Sandeep [ 16 Oct 2010 ] -- Start
'        'Issue : Auto No. Generation


'        'Pinkal (12-Jun-2013) -- Start
'        'Enhancement : TRA Changes
'        Dim intVocNoType As Integer = 0
'        Dim objConfig As New clsConfigOptions
'        objConfig._Companyunkid = IIf(intCompanyId = 0, Company._Object._Companyunkid, intCompanyId)
'        intVocNoType = objConfig._LoanVocNoType

'        'Dim intVocNoType As Integer = 0
'        'intVocNoType = ConfigParameter._Object._LoanVocNoType

'        'Pinkal (12-Jun-2013) -- End



'        If intVocNoType = 0 Then
'            If isExist(mstrLoanvoucher_No) Then
'                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
'                objDataOperation.ReleaseTransaction(False)
'                Return False
'            End If
'        ElseIf intVocNoType = 1 Then
'            mstrLoanvoucher_No = ""
'        End If
'        'S.SANDEEP [ 28 MARCH 2012 ] -- END



'        'Anjan (21 Jan 2012)-End 

'        Try
'            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanvoucher_No.ToString)
'            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
'            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
'            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Purpose.ToString)
'            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@isbrought_forward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbrought_Forward.ToString)
'            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
'            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
'            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
'            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
'            objDataOperation.AddParameter("@loanscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanscheduleunkid.ToString)
'            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
'            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdvance_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If

'            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanStatus.ToString)

'            'Anjan (08 Aug 2012)-Start
'            'Issue : Loan report was picking principal amount instead of loan balance in new year.

'            'S.SANDEEP [ 18 JAN 2014 ] -- START
'            'objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString)
'            objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoanBF_Amount)
'            'S.SANDEEP [ 18 JAN 2014 ] -- END

'            'Anjan (08 Aug 2012)-End 


'            'Sandeep [ 21 Aug 2010 ] -- Start
'            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
'            'strQ = "INSERT INTO lnloan_advance_tran ( " & _
'            '  "  loanvoucher_no " & _
'            '  ", effective_date " & _
'            '  ", yearunkid " & _
'            '  ", periodunkid " & _
'            '  ", employeeunkid " & _
'            '  ", approverunkid " & _
'            '  ", loan_purpose " & _
'            '  ", balance_amount " & _
'            '  ", isbrought_forward " & _
'            '  ", isloan " & _
'            '  ", loanschemeunkid " & _
'            '  ", loan_amount " & _
'            '  ", interest_rate " & _
'            '  ", loan_duration " & _
'            '  ", interest_amount " & _
'            '  ", net_amount " & _
'            '  ", calctype_id " & _
'            '  ", loanscheduleunkid " & _
'            '  ", emi_tenure " & _
'            '  ", emi_amount " & _
'            '  ", advance_amount " & _
'            '  ", userunkid " & _
'            '  ", isvoid " & _
'            '  ", voiduserunkid " & _
'            '  ", voiddatetime" & _
'            '  ", loan_statusunkid " & _
'            '  ", voidreason " & _
'            '  ", processpendingloanunkid " & _
'            '") VALUES (" & _
'            '  "  @loanvoucher_no " & _
'            '  ", @effective_date " & _
'            '  ", @yearunkid " & _
'            '  ", @periodunkid " & _
'            '  ", @employeeunkid " & _
'            '  ", @approverunkid " & _
'            '  ", @loan_purpose " & _
'            '  ", @balance_amount " & _
'            '  ", @isbrought_forward " & _
'            '  ", @isloan " & _
'            '  ", @loanschemeunkid " & _
'            '  ", @loan_amount " & _
'            '  ", @interest_rate " & _
'            '  ", @loan_duration " & _
'            '  ", @interest_amount " & _
'            '  ", @net_amount " & _
'            '  ", @calctype_id " & _
'            '  ", @loanscheduleunkid " & _
'            '  ", @emi_tenure " & _
'            '  ", @emi_amount " & _
'            '  ", @advance_amount " & _
'            '  ", @userunkid " & _
'            '  ", @isvoid " & _
'            '  ", @voiduserunkid " & _
'            '  ", @voiddatetime" & _
'            '  ", @loan_statusunkid " & _
'            '  ", @voidreason " & _
'            '  ", @processpendingloanunkid " & _
'            '"); SELECT @@identity"

'            strQ = "INSERT INTO lnloan_advance_tran ( " & _
'              "  loanvoucher_no " & _
'              ", effective_date " & _
'              ", yearunkid " & _
'              ", periodunkid " & _
'              ", employeeunkid " & _
'              ", approverunkid " & _
'              ", loan_purpose " & _
'              ", balance_amount " & _
'              ", isbrought_forward " & _
'              ", isloan " & _
'              ", loanschemeunkid " & _
'              ", loan_amount " & _
'              ", interest_rate " & _
'              ", loan_duration " & _
'              ", interest_amount " & _
'              ", net_amount " & _
'              ", calctype_id " & _
'              ", loanscheduleunkid " & _
'              ", emi_tenure " & _
'              ", emi_amount " & _
'              ", advance_amount " & _
'                                ", loan_statusunkid " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime" & _
'              ", voidreason " & _
'              ", processpendingloanunkid " & _
'                                ", deductionperiodunkid" & _
'                                ",bf_amount  " & _
'            ") VALUES (" & _
'              "  @loanvoucher_no " & _
'              ", @effective_date " & _
'              ", @yearunkid " & _
'              ", @periodunkid " & _
'              ", @employeeunkid " & _
'              ", @approverunkid " & _
'              ", @loan_purpose " & _
'              ", @balance_amount " & _
'              ", @isbrought_forward " & _
'              ", @isloan " & _
'              ", @loanschemeunkid " & _
'              ", @loan_amount " & _
'              ", @interest_rate " & _
'              ", @loan_duration " & _
'              ", @interest_amount " & _
'              ", @net_amount " & _
'              ", @calctype_id " & _
'              ", @loanscheduleunkid " & _
'              ", @emi_tenure " & _
'              ", @emi_amount " & _
'              ", @advance_amount " & _
'                                ", @loan_statusunkid " & _
'              ", @userunkid " & _
'              ", @isvoid " & _
'              ", @voiduserunkid " & _
'              ", @voiddatetime" & _
'              ", @voidreason " & _
'              ", @processpendingloanunkid " & _
'                                ", @deductionperiodunkid" & _
'                                 ",@bf_amount  " & _
'            "); SELECT @@identity"
'            'Sandeep [ 21 Aug 2010 ] -- End 'Anjan (08 Aug 2012) bf_amount included




'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintLoanadvancetranunkid = dsList.Tables(0).Rows(0).Item(0)

'            'S.SANDEEP [ 28 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
'            If intVocNoType = 1 Then


'                'Pinkal (12-Jun-2013) -- Start
'                'Enhancement : TRA Changes
'                'If Set_AutoNumber(objDataOperation, mintLoanadvancetranunkid, "lnloan_advance_tran", "loanvoucher_no", "loanadvancetranunkid", "NextLoanVocNo", ConfigParameter._Object._LoanVocPrefix) = False Then
'                If Set_AutoNumber(objDataOperation, mintLoanadvancetranunkid, "lnloan_advance_tran", "loanvoucher_no", "loanadvancetranunkid", "NextLoanVocNo", objConfig._LoanVocPrefix, objConfig._Companyunkid) = False Then
'                    'Pinkal (12-Jun-2013) -- End


'                    'S.SANDEEP [ 17 NOV 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'If objDataOperation.ErrorMessage <> "" Then
'                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    '    Throw exForce
'                    'End If

'                    If objDataOperation.ErrorMessage <> "" Then
'                        objDataOperation.ReleaseTransaction(False)
'                        Return False
'                    End If
'                    'S.SANDEEP [ 17 NOV 2012 ] -- END
'                End If

'                'S.SANDEEP [ 20 APRIL 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If Get_Saved_Number(objDataOperation, mintLoanadvancetranunkid, "lnloan_advance_tran", "loanvoucher_no", "loanadvancetranunkid", mstrLoanvoucher_No) = False Then
'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If
'                End If
'                'S.SANDEEP [ 20 APRIL 2012 ] -- END
'            End If
'            'S.SANDEEP [ 28 MARCH 2012 ] -- END



'            objStatusTran._Loanadvancetranunkid = mintLoanadvancetranunkid
'            objStatusTran._Statusunkid = mintLoanStatus
'            objStatusTran._Staus_Date = mdtEffective_Date

'            'Anjan (11 Jun 2011)-Start
'            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'            'InsertAuditTrailForLoanAdvanceTran(objDataOperation, 1)
'            If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 1, iUserId) = False Then
'                objDataOperation.ReleaseTransaction(False)
'                Return False
'            End If
'            'Anjan (11 Jun 2011)-End


'            'S.SANDEEP [ 04 DEC 2013 ] -- START
'            'If objStatusTran.Insert() Then
'            If objStatusTran.Insert(objDataOperation) Then
'                'S.SANDEEP [ 04 DEC 2013 ] -- END

'                'Anjan (04 Apr 2011)-Start
'                'Issue : Included new Assigned status for loan/advance
'                Dim blnFlag As Boolean = False

'                Dim objProcessPendingLoan As New clsProcess_pending_loan

'                objProcessPendingLoan._Processpendingloanunkid = mintProcesspendingloanunkid
'                objProcessPendingLoan._Loan_Statusunkid = 4

'                'S.SANDEEP [ 04 DEC 2013 ] -- START
'                'blnFlag = objProcessPendingLoan.Update
'                blnFlag = objProcessPendingLoan.Update(objDataOperation)
'                'S.SANDEEP [ 04 DEC 2013 ] -- END
'                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

'                'Anjan (04 Apr 2011)-End

'                'Sandeep [ 16 Oct 2010 ] -- Start
'                'Issue : Auto No. Generation


'                'S.SANDEEP [ 28 MARCH 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
'                'If intVocNoType = 1 Then
'                '    ConfigParameter._Object._NextLoanVocNo = intNextVocNo + 1
'                '    ConfigParameter._Object.updateParam()
'                '    ConfigParameter._Object.Refresh()
'                'End If
'                'S.SANDEEP [ 28 MARCH 2012 ] -- END


'                'Sandeep [ 16 Oct 2010 ] -- End 
'                objDataOperation.ReleaseTransaction(True)
'            Else
'                objDataOperation.ReleaseTransaction(False)
'            End If



'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (lnloan_advance_tran) </purpose>
'    Public Function Update(Optional ByVal iUserId As Integer = 0, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 29 May 2013 ] -- START -- END
'        'Public Function Update() As Boolean 
'        If isExist(mstrLoanvoucher_No, mintLoanadvancetranunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objDataOperation As clsDataOperation

'        'S.SANDEEP [ 29 May 2013 ] -- START
'        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
'        'Dim objDataOperation As New clsDataOperation
'        'objDataOperation.BindTransaction()

'        'S.SANDEEP [ 04 DEC 2013 ] -- START
'        'objDataOperation = New clsDataOperation
'        'objDataOperation.BindTransaction()
'        If objDataOp Is Nothing Then
'            objDataOperation = New clsDataOperation
'            objDataOperation.BindTransaction()
'        Else
'            objDataOperation = objDataOp
'            objDataOperation.ClearParameters()
'        End If
'        'S.SANDEEP [ 04 DEC 2013 ] -- END



'        'S.SANDEEP [ 29 May 2013 ] -- END

'        Try
'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
'            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanvoucher_No.ToString)
'            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
'            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
'            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Purpose.ToString)
'            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@isbrought_forward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbrought_Forward.ToString)
'            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
'            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
'            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
'            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
'            objDataOperation.AddParameter("@loanscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanscheduleunkid.ToString)
'            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
'            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdvance_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If

'            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanStatus.ToString)

'            'Sandeep [ 21 Aug 2010 ] -- Start
'            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)

'            'strQ = "UPDATE lnloan_advance_tran SET " & _
'            '  "  loanvoucher_no = @loanvoucher_no" & _
'            '  ", effective_date = @effective_date" & _
'            '  ", yearunkid = @yearunkid" & _
'            '  ", periodunkid = @periodunkid" & _
'            '  ", employeeunkid = @employeeunkid" & _
'            '  ", approverunkid = @approverunkid" & _
'            '  ", loan_purpose = @loan_purpose" & _
'            '  ", balance_amount = @balance_amount" & _
'            '  ", isbrought_forward = @isbrought_forward" & _
'            '  ", isloan = @isloan" & _
'            '  ", loanschemeunkid = @loanschemeunkid" & _
'            '  ", loan_amount = @loan_amount" & _
'            '  ", interest_rate = @interest_rate" & _
'            '  ", loan_duration = @loan_duration" & _
'            '  ", interest_amount = @interest_amount" & _
'            '  ", net_amount = @net_amount" & _
'            '  ", calctype_id = @calctype_id" & _
'            '  ", loanscheduleunkid = @loanscheduleunkid" & _
'            '  ", emi_tenure = @emi_tenure" & _
'            '  ", emi_amount = @emi_amount" & _
'            '  ", advance_amount = @advance_amount" & _
'            '  ", userunkid = @userunkid" & _
'            '  ", isvoid = @isvoid" & _
'            '  ", voiduserunkid = @voiduserunkid" & _
'            '  ", voiddatetime = @voiddatetime " & _
'            '  ", loan_statusunkid = @loan_statusunkid " & _
'            '  ", voidreason = @voidreason " & _
'            '  ", processpendingloanunkid = @processpendingloanunkid " & _
'            '"WHERE loanadvancetranunkid = @loanadvancetranunkid "

'            strQ = "UPDATE lnloan_advance_tran SET " & _
'              "  loanvoucher_no = @loanvoucher_no" & _
'              ", effective_date = @effective_date" & _
'              ", yearunkid = @yearunkid" & _
'              ", periodunkid = @periodunkid" & _
'              ", employeeunkid = @employeeunkid" & _
'              ", approverunkid = @approverunkid" & _
'              ", loan_purpose = @loan_purpose" & _
'              ", balance_amount = @balance_amount" & _
'              ", isbrought_forward = @isbrought_forward" & _
'              ", isloan = @isloan" & _
'              ", loanschemeunkid = @loanschemeunkid" & _
'              ", loan_amount = @loan_amount" & _
'              ", interest_rate = @interest_rate" & _
'              ", loan_duration = @loan_duration" & _
'              ", interest_amount = @interest_amount" & _
'              ", net_amount = @net_amount" & _
'              ", calctype_id = @calctype_id" & _
'              ", loanscheduleunkid = @loanscheduleunkid" & _
'              ", emi_tenure = @emi_tenure" & _
'              ", emi_amount = @emi_amount" & _
'              ", advance_amount = @advance_amount" & _
'                            ", loan_statusunkid = @loan_statusunkid" & _
'              ", userunkid = @userunkid" & _
'              ", isvoid = @isvoid" & _
'              ", voiduserunkid = @voiduserunkid" & _
'              ", voiddatetime = @voiddatetime " & _
'              ", voidreason = @voidreason " & _
'              ", processpendingloanunkid = @processpendingloanunkid " & _
'                            ", deductionperiodunkid = @deductionperiodunkid " & _
'            "WHERE loanadvancetranunkid = @loanadvancetranunkid "
'            'Sandeep [ 21 Aug 2010 ] -- End 




'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If


'            'Anjan (11 Jun 2011)-Start
'            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'            'InsertAuditTrailForLoanAdvanceTran(objDataOperation, 2)
'            If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 2) = False Then
'                objDataOperation.ReleaseTransaction(False)
'                Return False
'            End If
'            'Anjan (11 Jun 2011)-End

'            If objDataOp Is Nothing Then
'                objDataOperation.ReleaseTransaction(True)
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'S.SANDEEP [ 29 May 2013 ] -- START
'            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
'            'objDataOperation = Nothing
'            'S.SANDEEP [ 29 May 2013 ] -- END
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (lnloan_advance_tran) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        'Sandeep [ 09 Oct 2010 ] -- Start
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        If isUsed(intUnkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Loan/Advance. Reason : This Loan/Advance is already linked to some transactions.")
'            Return False
'        End If
'        'Sandeep [ 09 Oct 2010 ] -- End 
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation
'        objDataOperation.BindTransaction()
'        Try
'            strQ = "UPDATE lnloan_advance_tran SET " & _
'                     "  isvoid = @isvoid" & _
'                     ", voiduserunkid = @voiduserunkid" & _
'                     ", voiddatetime = @voiddatetime " & _
'                     ", voidreason = @voidreason " & _
'            "WHERE loanadvancetranunkid = @loanadvancetranunkid "

'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Dim blnFlag As Boolean = False
'            Dim objStatusTran As New clsLoan_Status_tran
'            objStatusTran._Isvoid = mblnIsvoid.ToString
'            objStatusTran._Voiddatetime = mdtVoiddatetime
'            objStatusTran._Voiduserunkid = mintVoiduserunkid
'            objStatusTran._Voidreason = mstrVoidreason
'            'S.SANDEEP [ 04 DEC 2013 ] -- START
'            'blnFlag = objStatusTran.Delete(intUnkid)
'            blnFlag = objStatusTran.Delete(intUnkid, objDataOperation)
'            'S.SANDEEP [ 04 DEC 2013 ] -- END
'            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

'            mintLoanadvancetranunkid = intUnkid

'            'Anjan (11 Jun 2011)-Start
'            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'            'InsertAuditTrailForLoanAdvanceTran(objDataOperation, 3)
'            If InsertAuditTrailForLoanAdvanceTran(objDataOperation, 3) = False Then
'                objDataOperation.ReleaseTransaction(False)
'                Return False
'            End If
'            'Anjan (11 Jun 2011)-End

'            'Anjan (04 Apr 2011)-Start
'            'Issue : Rollbacked to status as approved if assigned status transaction is deleted
'            Dim blnLoanAdvanceFlag As Boolean = False
'            Dim objProcessPendingLoan As New clsProcess_pending_loan
'            objProcessPendingLoan._Processpendingloanunkid = mintProcesspendingloanunkid
'            objProcessPendingLoan._Loan_Statusunkid = 2

'            'S.SANDEEP [ 04 DEC 2013 ] -- START
'            'blnLoanAdvanceFlag = objProcessPendingLoan.Update()
'            blnLoanAdvanceFlag = objProcessPendingLoan.Update(objDataOperation)
'            'S.SANDEEP [ 04 DEC 2013 ] -- END

'            If blnLoanAdvanceFlag = False Then objDataOperation.ReleaseTransaction(False)

'            'Anjan (04 Apr 2011)-End

'            objDataOperation.ReleaseTransaction(True)
'            Return True

'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try

'            'Sandeep [ 09 Oct 2010 ] -- Start
'            'strQ = "<Query>"
'            strQ = "SELECT loanadvancetranunkid FROM prpayrollprocess_tran WHERE loanadvancetranunkid = @loanadvancetranunkid "
'            'Sandeep [ 09 Oct 2010 ] -- End 

'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mstrMessage = ""

'            If dsList.Tables(0).Rows.Count > 0 Then
'                Return True
'            Else
'                strQ = "SELECT referencetranunkid FROM prpayment_tran WHERE referenceid IN (1,2) AND referencetranunkid = @loanadvancetranunkid "

'                dsList = objDataOperation.ExecQuery(strQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                If dsList.Tables(0).Rows.Count > 0 Then
'                    Return True
'                Else
'                    Return False
'                End If
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'objDataOperation = New clsDataOperation
'        Dim objDataOperation As New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                   "  loanadvancetranunkid " & _
'                   ", loanvoucher_no " & _
'                   ", effective_date " & _
'                   ", yearunkid " & _
'                   ", periodunkid " & _
'                   ", employeeunkid " & _
'                   ", approverunkid " & _
'                   ", loan_purpose " & _
'                   ", balance_amount " & _
'                   ", isbrought_forward " & _
'                   ", isloan " & _
'                   ", loanschemeunkid " & _
'                   ", loan_amount " & _
'                   ", interest_rate " & _
'                   ", interest_amount " & _
'                   ", net_amount " & _
'                   ", calctype_id " & _
'                   ", loanscheduleunkid " & _
'                   ", emi_tenure " & _
'                   ", emi_amount " & _
'                   ", advance_amount " & _
'                   ", userunkid " & _
'                   ", isvoid " & _
'                   ", voiduserunkid " & _
'                   ", voiddatetime " & _
'                   ", processpendingloanunkid " & _
'             "FROM lnloan_advance_tran " & _
'             "WHERE loanvoucher_no = @loanvoucher_no "

'            If intUnkid > 0 Then
'                strQ &= " AND loanadvancetranunkid <> @loanadvancetranunkid"
'            End If

'            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Gets The History </purpose>
'    Public Function GetLoanAdvance_History_List(ByVal intUnkid As Integer) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                    "    ISNULL(lnScheme, '') AS lnScheme " & _
'                    "   ,ISNULL(lnVocNo, '') AS lnVocNo " & _
'                    "   ,lnDate AS lnDate " & _
'                    "   ,ISNULL(lnAmount, 0) AS lnAmount " & _
'                    "   ,ISNULL(lnNetAmount, 0) AS lnNetAmount " & _
'                    "   ,ISNULL(lnBalance, 0) AS lnBalance " & _
'                    "   ,(ISNULL(lnNetAmount, 0)-ISNULL(lnBalance, 0)) AS lnPaid " & _
'                    "   ,ISNULL(lnStatus, '') AS lnStatus " & _
'                    "   ,lnUnkid AS lnUnkid " & _
'                    "FROM " & _
'                    "    ( SELECT " & _
'                    "        CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'                    "             THEN ISNULL(lnloan_scheme_master.name, '') " & _
'                    "             ELSE @Advance " & _
'                    "        END AS lnScheme " & _
'                    "      ,ISNULL(lnloan_advance_tran.loanvoucher_no, '') AS lnVocNo " & _
'                    "       ,CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS lnDate " & _
'                    "       ,CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'                    "             THEN ISNULL(lnloan_advance_tran.loan_amount, 0) " & _
'                    "             ELSE lnloan_advance_tran.advance_amount " & _
'                    "        END AS lnAmount " & _
'                    "       ,CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'                    "             THEN ISNULL(lnloan_advance_tran.net_amount, 0) " & _
'                    "             ELSE lnloan_advance_tran.advance_amount " & _
'                    "        END AS lnNetAmount " & _
'                    "       ,CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
'                    "             WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
'                    "             WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
'                    "             WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed " & _
'                    "        END AS lnStatus " & _
'                    "       ,lnloan_advance_tran.loanadvancetranunkid AS lnUnkid " & _
'                    "       ,lnloan_advance_tran.balance_amount AS lnBalance " & _
'                    "      FROM " & _
'                    "        lnloan_advance_tran " & _
'                    "        LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
'                    "      WHERE " & _
'                    "        ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
'                    "    ) AS lnHistory " & _
'                    "WHERE " & _
'                    "    lnUnkid = @Unkid "

'            objDataOperation.AddParameter("@Unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Advance"))
'            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
'            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
'            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
'            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetLoanAdvance_History_List", mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    Public Function Get_Unpaid_LoanAdvance_List(ByVal strTableName As String) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try
'            'Sohail (20 Aug 2010) -- Start
'            'Changes:Set loanscheme ='@Advance' for isloan=0
'            'strQ = "SELECT  UnpaidLoan = ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                    "THEN ( loan_amount " & _
'            '                           "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
'            '                    "ELSE ( advance_amount " & _
'            '                           "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
'            '               "END ) , " & _
'            '            "lnloan_advance_tran.loanvoucher_no AS VocNo , " & _
'            '            "ISNULL(hremployee_master.firstname, '') + ' ' " & _
'            '            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
'            '            "+ ISNULL(hremployee_master.surname, '') AS EmpName , " & _
'            '            "lnloan_scheme_master.name AS LoanScheme , " & _
'            '            "CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
'            '                 "ELSE @Advance " & _
'            '            "END AS Loan_Advance , " & _
'            '            "CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
'            '                 "ELSE 2 " & _
'            '            "END AS Loan_AdvanceUnkid , " & _
'            '            "CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                 "THEN lnloan_advance_tran.net_amount " & _
'            '                 "ELSE lnloan_advance_tran.advance_amount " & _
'            '            "END AS Amount , " & _
'            '            "cfcommon_period_tran.period_name AS PeriodName , " & _
'            '            "lnloan_advance_tran.loan_statusunkid , " & _
'            '            "lnloan_advance_tran.periodunkid , " & _
'            '            "lnloan_advance_tran.loanadvancetranunkid , " & _
'            '            "hremployee_master.employeeunkid , " & _
'            '            "hremployee_master.employeecode , " & _
'            '            "lnloan_scheme_master.loanschemeunkid , " & _
'            '            "lnloan_advance_tran.isloan , " & _
'            '            "CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date , " & _
'            '            "lnloan_advance_tran.balance_amount , " & _
'            '            "lnloan_advance_tran.isloan , " & _
'            '            "lnloan_advance_tran.loanschemeunkid , " & _
'            '            "lnloan_advance_tran.emi_amount , " & _
'            '            "lnloan_advance_tran.advance_amount , " & _
'            '            "lnloan_advance_tran.loan_statusunkid " & _
'            '    "FROM    lnloan_advance_tran " & _
'            '            "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            '            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
'            '            "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
'            '            "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
'            '                                            "AND ISNULL(prpayment_tran.isvoid, " & _
'            '                                                       "0) = 0 " & _
'            '    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
'            '    "GROUP BY lnloan_advance_tran.loanvoucher_no , " & _
'            '            "ISNULL(hremployee_master.firstname, '') + ' ' " & _
'            '            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
'            '            "+ ISNULL(hremployee_master.surname, '') , " & _
'            '            "lnloan_advance_tran.isloan , " & _
'            '            "lnloan_advance_tran.loan_amount , " & _
'            '            "lnloan_advance_tran.advance_amount , " & _
'            '            "lnloan_scheme_master.name , " & _
'            '            "lnloan_advance_tran.net_amount , " & _
'            '            "cfcommon_period_tran.period_name , " & _
'            '            "lnloan_advance_tran.loan_statusunkid , " & _
'            '            "lnloan_advance_tran.periodunkid , " & _
'            '            "lnloan_advance_tran.loanadvancetranunkid , " & _
'            '            "hremployee_master.employeeunkid , " & _
'            '            "hremployee_master.employeecode , " & _
'            '            "lnloan_scheme_master.loanschemeunkid , " & _
'            '            "lnloan_advance_tran.effective_date , " & _
'            '            "lnloan_advance_tran.balance_amount , " & _
'            '            "lnloan_advance_tran.loanschemeunkid , " & _
'            '            "lnloan_advance_tran.emi_amount " & _
'            '    "HAVING  ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'            '                   "THEN ( loan_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
'            '                   "ELSE ( advance_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
'            '              "END ) > 0 "



'            strQ = "SELECT  UnpaidLoan = ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'                                            "THEN ( loan_amount " & _
'                                                   "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
'                                            "ELSE ( advance_amount " & _
'                                                   "- ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
'                                       "END ) , " & _
'                        "lnloan_advance_tran.loanvoucher_no AS VoucherNo , " & _
'                        "ISNULL(hremployee_master.firstname, '') + ' ' " & _
'                        "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
'                        "+ ISNULL(hremployee_master.surname, '') AS EmpName , " & _
'                        "CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'                            "THEN lnloan_scheme_master.name " & _
'                            "ELSE @Advance " & _
'                        "END AS LoanScheme , " & _
'                        "CASE WHEN lnloan_advance_tran.isloan = 1 THEN @Loan " & _
'                             "ELSE @Advance " & _
'                        "END AS Loan_Advance , " & _
'                        "CASE WHEN lnloan_advance_tran.isloan = 1 THEN 1 " & _
'                             "ELSE 2 " & _
'                        "END AS Loan_AdvanceUnkid , " & _
'                        "CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'                             "THEN lnloan_advance_tran.net_amount " & _
'                             "ELSE lnloan_advance_tran.advance_amount " & _
'                        "END AS Amount , " & _
'                        "cfcommon_period_tran.period_name AS PeriodName , " & _
'                        "lnloan_advance_tran.loan_statusunkid , " & _
'                        "lnloan_advance_tran.periodunkid , " & _
'                        "lnloan_advance_tran.loanadvancetranunkid , " & _
'                        "hremployee_master.employeeunkid , " & _
'                        "hremployee_master.employeecode , " & _
'                        "lnloan_advance_tran.isloan , " & _
'                        "CONVERT(CHAR(8), lnloan_advance_tran.effective_date, 112) AS effective_date , " & _
'                        "lnloan_advance_tran.balance_amount , " & _
'                        "ISNULL(lnloan_advance_tran.loanschemeunkid, 0) AS loanschemeunkid , " & _
'                        "lnloan_advance_tran.emi_amount , " & _
'                        "lnloan_advance_tran.advance_amount , " & _
'                        "lnloan_advance_tran.processpendingloanunkid " & _
'                "FROM    lnloan_advance_tran " & _
'                        "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
'                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_advance_tran.periodunkid " & _
'                        "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
'                        "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
'                                                        "AND ISNULL(prpayment_tran.isvoid, " & _
'                                                                   "0) = 0 " & _
'                "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "

'            'Anjan (24 Jun 2011)-Start
'            'Issue : According to privilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If

'            'S.SANDEEP [ 01 JUNE 2012 ] -- START
'            'ENHANCEMENT : TRA DISCIPLINE CHANGES
'            'Select Case ConfigParameter._Object._UserAccessModeSetting
'            '    Case enAllocation.BRANCH
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.TEAM
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOB_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOBS
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            'End Select
'            strQ &= UserAccessLevel._AccessLevelFilterString
'            'S.SANDEEP [ 01 JUNE 2012 ] -- END


'            'S.SANDEEP [ 04 FEB 2012 ] -- END

'            'Anjan (24 Jun 2011)-End 

'            strQ &= "GROUP BY lnloan_advance_tran.loanvoucher_no , " & _
'                        "ISNULL(hremployee_master.firstname, '') + ' ' " & _
'                        "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
'                        "+ ISNULL(hremployee_master.surname, '') , " & _
'                        "lnloan_advance_tran.isloan , " & _
'                        "lnloan_advance_tran.loan_amount , " & _
'                        "lnloan_advance_tran.advance_amount , " & _
'                        "lnloan_scheme_master.name , " & _
'                        "lnloan_advance_tran.net_amount , " & _
'                        "cfcommon_period_tran.period_name , " & _
'                        "lnloan_advance_tran.loan_statusunkid , " & _
'                        "lnloan_advance_tran.periodunkid , " & _
'                        "lnloan_advance_tran.loanadvancetranunkid , " & _
'                        "hremployee_master.employeeunkid , " & _
'                        "hremployee_master.employeecode , " & _
'                        "lnloan_scheme_master.loanschemeunkid , " & _
'                        "lnloan_advance_tran.effective_date , " & _
'                        "lnloan_advance_tran.balance_amount , " & _
'                        "lnloan_advance_tran.loanschemeunkid , " & _
'                        "lnloan_advance_tran.emi_amount , " & _
'                        "lnloan_advance_tran.processpendingloanunkid " & _
'                "HAVING  ( CASE WHEN lnloan_advance_tran.isloan = 1 " & _
'                               "THEN ( loan_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
'                               "ELSE ( advance_amount - ISNULL(SUM(prpayment_tran.amount), 0) ) " & _
'                          "END ) > 0 "

'            'Sohail (20 Aug 2010) -- End

'            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
'            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Advance"))

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_Unpaid_LoanAdvance_List; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    'Anjan (11 Jun 2011)-Start
'    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'    'Public Sub InsertAuditTrailForLoanAdvanceTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
'    Public Function InsertAuditTrailForLoanAdvanceTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, Optional ByVal iUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 29 May 2013 ] -- START -- END
'        'Public Function InsertAuditTrailForLoanAdvanceTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, Optional ByVal iUserUnkid As Integer = 0) As Boolean
'        'Anjan (11 Jun 2011)-End
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Try


'            'S.SANDEEP [ 19 JULY 2012 ] -- START
'            'Enhancement : TRA Changes

'            'strQ = "INSERT INTO atlnloan_advance_tran ( " & _
'            '            "  loanadvancetranunkid " & _
'            '            ", loanvoucher_no " & _
'            '            ", effective_date " & _
'            '            ", yearunkid " & _
'            '            ", periodunkid " & _
'            '            ", employeeunkid " & _
'            '            ", approverunkid " & _
'            '            ", loan_purpose " & _
'            '            ", balance_amount " & _
'            '            ", isbrought_forward " & _
'            '            ", isloan " & _
'            '            ", loanschemeunkid " & _
'            '            ", loan_amount " & _
'            '            ", interest_rate " & _
'            '            ", loan_duration " & _
'            '            ", interest_amount " & _
'            '            ", net_amount " & _
'            '            ", calctype_id " & _
'            '            ", loanscheduleunkid " & _
'            '            ", emi_tenure " & _
'            '            ", emi_amount " & _
'            '            ", advance_amount " & _
'            '            ", loan_statusunkid " & _
'            '            ", audittype " & _
'            '            ", audituserunkid " & _
'            '            ", auditdatetime " & _
'            '            ", ip " & _
'            '            ", machine_name " & _
'            '            ", processpendingloanunkid " & _
'            '      ") VALUES (" & _
'            '            "  @loanadvancetranunkid " & _
'            '            ", @loanvoucher_no " & _
'            '            ", @effective_date " & _
'            '            ", @yearunkid " & _
'            '            ", @periodunkid " & _
'            '            ", @employeeunkid " & _
'            '            ", @approverunkid " & _
'            '            ", @loan_purpose " & _
'            '            ", @balance_amount " & _
'            '            ", @isbrought_forward " & _
'            '            ", @isloan " & _
'            '            ", @loanschemeunkid " & _
'            '            ", @loan_amount " & _
'            '            ", @interest_rate " & _
'            '            ", @loan_duration " & _
'            '            ", @interest_amount " & _
'            '            ", @net_amount " & _
'            '            ", @calctype_id " & _
'            '            ", @loanscheduleunkid " & _
'            '            ", @emi_tenure " & _
'            '            ", @emi_amount " & _
'            '            ", @advance_amount " & _
'            '            ", @loan_statusunkid " & _
'            '            ", @audittype " & _
'            '            ", @audituserunkid " & _
'            '            ", @auditdatetime " & _
'            '            ", @ip " & _
'            '            ", @machine_name " & _
'            '            ", @processpendingloanunkid " & _
'            '        "); "

'            strQ = "INSERT INTO atlnloan_advance_tran ( " & _
'                        "  loanadvancetranunkid " & _
'                        ", loanvoucher_no " & _
'                        ", effective_date " & _
'                        ", yearunkid " & _
'                        ", periodunkid " & _
'                        ", employeeunkid " & _
'                        ", approverunkid " & _
'                        ", loan_purpose " & _
'                        ", balance_amount " & _
'                        ", isbrought_forward " & _
'                        ", isloan " & _
'                        ", loanschemeunkid " & _
'                        ", loan_amount " & _
'                        ", interest_rate " & _
'                        ", loan_duration " & _
'                        ", interest_amount " & _
'                        ", net_amount " & _
'                        ", calctype_id " & _
'                        ", loanscheduleunkid " & _
'                        ", emi_tenure " & _
'                        ", emi_amount " & _
'                        ", advance_amount " & _
'                        ", loan_statusunkid " & _
'                        ", audittype " & _
'                        ", audituserunkid " & _
'                        ", auditdatetime " & _
'                        ", ip " & _
'                        ", machine_name " & _
'                        ", processpendingloanunkid " & _
'                        ", form_name " & _
'                        ", module_name1 " & _
'                        ", module_name2 " & _
'                        ", module_name3 " & _
'                        ", module_name4 " & _
'                        ", module_name5 " & _
'                        ", isweb " & _
'                        ", bf_amount  " & _
'                        ", loginemployeeunkid " & _
'                        ", deductionperiodunkid" & _
'                  ") VALUES (" & _
'                        "  @loanadvancetranunkid " & _
'                        ", @loanvoucher_no " & _
'                        ", @effective_date " & _
'                        ", @yearunkid " & _
'                        ", @periodunkid " & _
'                        ", @employeeunkid " & _
'                        ", @approverunkid " & _
'                        ", @loan_purpose " & _
'                        ", @balance_amount " & _
'                        ", @isbrought_forward " & _
'                        ", @isloan " & _
'                        ", @loanschemeunkid " & _
'                        ", @loan_amount " & _
'                        ", @interest_rate " & _
'                        ", @loan_duration " & _
'                        ", @interest_amount " & _
'                        ", @net_amount " & _
'                        ", @calctype_id " & _
'                        ", @loanscheduleunkid " & _
'                        ", @emi_tenure " & _
'                        ", @emi_amount " & _
'                        ", @advance_amount " & _
'                        ", @loan_statusunkid " & _
'                        ", @audittype " & _
'                        ", @audituserunkid " & _
'                        ", @auditdatetime " & _
'                        ", @ip " & _
'                        ", @machine_name " & _
'                        ", @processpendingloanunkid " & _
'                        ", @form_name " & _
'                        ", @module_name1 " & _
'                        ", @module_name2 " & _
'                        ", @module_name3 " & _
'                        ", @module_name4 " & _
'                        ", @module_name5 " & _
'                        ", @isweb " & _
'                        ", @bf_amount  " & _
'                        ", @loginemployeeunkid " & _
'                        ", @deductionperiodunkid" & _
'                    "); "
'            'Sohail (08 Apr 2013) - [deductionperiodunkid]
'            'S.SANDEEP [ 19 JULY 2012 ] -- END

'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
'            objDataOperation.AddParameter("@loanvoucher_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanvoucher_No.ToString)
'            objDataOperation.AddParameter("@effective_date ", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
'            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
'            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Purpose.ToString)
'            objDataOperation.AddParameter("@balance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@isbrought_forward", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbrought_Forward.ToString)
'            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
'            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
'            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Rate.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
'            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
'            objDataOperation.AddParameter("@loanscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanscheduleunkid.ToString)
'            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
'            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdvance_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanStatus.ToString)
'            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)

'            'Sandeep [ 16 Oct 2010 ] -- Start
'            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
'            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, Now.ToString)

'            'S.SANDEEP [ 29 May 2013 ] -- START
'            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
'            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
'            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(iUserUnkid <= 0, User._Object._Userunkid, iUserUnkid))
'            'S.SANDEEP [ 29 May 2013 ] -- END

'            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
'            'Sandeep [ 16 Oct 2010 ] -- End 

'            'S.SANDEEP [ 13 AUG 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
'            'objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
'            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
'            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
'            'S.SANDEEP [ 13 AUG 2012 ] -- END

'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

'            'Anjan (08 Aug 2012)-Start
'            'Issue : Loan report was picking principal amount instead of loan balance in new year.
'            objDataOperation.AddParameter("@bf_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNet_Amount.ToString) '
'            'Anjan (08 Aug 2012)-End 




'            'S.SANDEEP [ 19 JULY 2012 ] -- START
'            'Enhancement : TRA Changes

'            If mstrWebFormName.Trim.Length <= 0 Then
'                'S.SANDEEP [ 11 AUG 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'Dim frm As Form
'                'For Each frm In Application.OpenForms
'                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
'                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
'                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
'                '    End If
'                'Next
'                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
'                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
'                'S.SANDEEP [ 11 AUG 2012 ] -- END
'            Else
'                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
'                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
'                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 6, "WEB"))
'            End If
'            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
'            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
'            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
'            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
'            'S.SANDEEP [ 19 JULY 2012 ] -- END

'            'S.SANDEEP [ 13 AUG 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
'            'S.SANDEEP [ 13 AUG 2012 ] -- END
'            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString) 'Sohail (08 Apr 2013)

'            objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'Anjan (11 Jun 2011)-Start
'            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'            Return True
'            'Anjan (11 Jun 2011)-End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForLoanAdvanceTran", mstrModuleName)
'            'Anjan (11 Jun 2011)-Start
'            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'            Return False
'            'Anjan (11 Jun 2011)-End
'        End Try
'    End Function

'    'Sohail (04 Apr 2011) -- Start
'    Public Function Is_Payment_Done(Optional ByVal intDeductionPeriodID As Integer = 0) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try



'            strQ = "SELECT  lnloan_advance_tran.loanadvancetranunkid " & _
'                          ", lnloan_advance_tran.loanvoucher_no " & _
'                          ", lnloan_advance_tran.effective_date " & _
'                          ", lnloan_advance_tran.yearunkid " & _
'                          ", lnloan_advance_tran.periodunkid " & _
'                          ", lnloan_advance_tran.employeeunkid " & _
'                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
'                          ", lnloan_scheme_master.name AS LoanSchemeName " & _
'                          ", lnloan_advance_tran.approverunkid " & _
'                          ", lnloan_advance_tran.loan_purpose " & _
'                          ", lnloan_advance_tran.balance_amount " & _
'                          ", lnloan_advance_tran.isbrought_forward " & _
'                          ", lnloan_advance_tran.isloan " & _
'                          ", lnloan_advance_tran.loanschemeunkid " & _
'                          ", lnloan_advance_tran.loan_amount " & _
'                          ", lnloan_advance_tran.interest_rate " & _
'                          ", lnloan_advance_tran.loan_duration " & _
'                          ", lnloan_advance_tran.interest_amount " & _
'                          ", lnloan_advance_tran.net_amount " & _
'                          ", lnloan_advance_tran.calctype_id " & _
'                          ", lnloan_advance_tran.loanscheduleunkid " & _
'                          ", lnloan_advance_tran.emi_tenure " & _
'                          ", lnloan_advance_tran.emi_amount " & _
'                          ", lnloan_advance_tran.advance_amount " & _
'                          ", lnloan_advance_tran.loan_statusunkid " & _
'                          ", lnloan_advance_tran.userunkid " & _
'                          ", lnloan_advance_tran.isvoid " & _
'                          ", lnloan_advance_tran.voiduserunkid " & _
'                          ", lnloan_advance_tran.voiddatetime " & _
'                          ", lnloan_advance_tran.voidreason " & _
'                          ", lnloan_advance_tran.processpendingloanunkid " & _
'                          ", lnloan_advance_tran.deductionperiodunkid " & _
'                    "FROM    lnloan_advance_tran " & _
'                            "LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
'                                    "AND prpayment_tran.referenceid IN ( 1, 2 ) " & _
'                                    "AND prpayment_tran.paytypeid = 1 " & _
'                            "LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid=lnloan_scheme_master.loanschemeunkid " & _
'                            "LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid=hremployee_master.employeeunkid " & _
'                            "LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
'                    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
'                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
'                            "AND lnloan_process_pending_loan.isexternal_entity = 0 " & _
'                            "AND prpayment_tran.paymenttranunkid IS NULL "


'            'Anjan (24 Jun 2011)-Start
'            'Issue : According to privilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If

'            'S.SANDEEP [ 01 JUNE 2012 ] -- START
'            'ENHANCEMENT : TRA DISCIPLINE CHANGES
'            'Select Case ConfigParameter._Object._UserAccessModeSetting
'            '    Case enAllocation.BRANCH
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.TEAM
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOB_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOBS
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            'End Select
'            strQ &= UserAccessLevel._AccessLevelFilterString
'            'S.SANDEEP [ 01 JUNE 2012 ] -- END


'            'S.SANDEEP [ 04 FEB 2012 ] -- END

'            'Anjan (24 Jun 2011)-End 



'            If intDeductionPeriodID > 0 Then
'                strQ &= "AND lnloan_advance_tran.deductionperiodunkid = @deductionperiodunkid "
'                objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeductionPeriodID)
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables("List").Rows.Count <= 0

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_Unpaid_LoanAdvance_List; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function
'    'Sohail (04 Apr 2011) -- End



'    'S.SANDEEP [ 06 SEP 2011 ] -- START
'    'ENHANCEMENT : MAKING COMMON FUNCTION
'    Public Function Calculate_LoanInfo(ByVal enCalc As enLoanCalcId, _
'                                                       ByVal decPrincipleAmt As Decimal, _
'                                                       ByVal intDuration As Integer, _
'                                                       ByVal dblRate As Double, _
'                                                       ByRef mdecNetAmount As Decimal, _
'                                                       ByRef mdecInterest_Amount As Decimal, _
'                                                       Optional ByRef decRedEMI As Decimal = 0) As Decimal
'        Try
'            Select Case enCalc
'                Case enLoanCalcId.Simple_Interest
'                    If intDuration > 12 Then
'                        mdecInterest_Amount = (decPrincipleAmt * dblRate * (intDuration / 12)) / 100
'                    Else
'                        mdecInterest_Amount = ((decPrincipleAmt * dblRate * intDuration) / 100) / 12
'                    End If
'                    mdecNetAmount = mdecInterest_Amount + decPrincipleAmt
'                Case enLoanCalcId.Compound_Interest
'                    If intDuration > 12 Then
'                        mdecInterest_Amount = decPrincipleAmt * CDec(Pow((1 + (dblRate / 100)), (intDuration / 12)))
'                    Else
'                        mdecInterest_Amount = decPrincipleAmt * CDec(Pow((1 + (dblRate / 100)), intDuration))
'                    End If
'                    mdecNetAmount = mdecInterest_Amount + decPrincipleAmt
'                Case enLoanCalcId.Reducing_Amount
'                    Dim mdecMonthlyRate, mdecTotalRate, mdecNewReduceAmt As Decimal

'                    mdecMonthlyRate = 0 : mdecTotalRate = 0 : mdecNewReduceAmt = 0

'                    mdecMonthlyRate = ((dblRate / 12) / 100) + 1
'                    mdecTotalRate = CDec(Pow(mdecMonthlyRate, intDuration)) - 1

'                    If mdecTotalRate > 0 Then
'                        mdecTotalRate = (mdecMonthlyRate - 1) / mdecTotalRate
'                    Else
'                        mdecTotalRate = 0
'                    End If

'                    mdecTotalRate = mdecTotalRate + (mdecMonthlyRate - 1)
'                    decRedEMI = decPrincipleAmt * mdecTotalRate

'                    For i As Integer = 1 To CInt(intDuration)
'                        If i = 1 Then
'                            mdecInterest_Amount += decPrincipleAmt * (mdecMonthlyRate - 1)
'                            mdecNewReduceAmt = (decPrincipleAmt - (decRedEMI - (decPrincipleAmt * (mdecMonthlyRate - 1))))
'                        Else
'                            mdecInterest_Amount += mdecNewReduceAmt * (mdecMonthlyRate - 1)
'                            mdecNewReduceAmt = (mdecNewReduceAmt - (decRedEMI - (mdecNewReduceAmt * (mdecMonthlyRate - 1))))
'                        End If
'                    Next

'                    mdecNetAmount = mdecInterest_Amount + decPrincipleAmt

'            End Select

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Calculate_LoanInfo", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Public Function Calculate_LoanEMI(ByVal intEMI_ModeId As Integer, ByVal decNetAmount As Decimal, ByRef decInstallmentAmt As Decimal, ByRef intNoOfEMI As Integer) As Decimal
'        Try
'            Select Case intEMI_ModeId
'                Case 1  'AMOUNT
'                    If decInstallmentAmt > 0 Then
'                        intNoOfEMI = CInt(IIf(decInstallmentAmt = 0, 0, decNetAmount / decInstallmentAmt))
'                    Else
'                        intNoOfEMI = 0
'                        Exit Try
'                    End If

'                    If CInt(CDec(IIf(decInstallmentAmt = 0, 0, decNetAmount / decInstallmentAmt))) > intNoOfEMI Then
'                        intNoOfEMI += 1
'                    End If
'                Case 2  'MONTHS
'                    If intNoOfEMI > 0 Then
'                        decInstallmentAmt = CDec(IIf(intNoOfEMI = 0, 0, decNetAmount / intNoOfEMI))
'                    Else
'                        decInstallmentAmt = 0
'                    End If
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Calculate_LoanEMI", mstrModuleName)
'        Finally
'        End Try
'    End Function
'    'S.SANDEEP [ 06 SEP 2011 ] -- END 

'    'Sohail (08 Jun 2012) -- Start
'    'TRA - ENHANCEMENT
'    Public Function GetDataForProcessPayroll(ByVal strTableName As String, ByVal intEmpUnkID As Integer, ByVal dtPeriodStart As DateTime) As DataTable
'        Dim dtTable As DataTable = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try

'            strQ = "SELECT   loanadvancetranunkid " & _
'                          ", lnloan_advance_tran.balance_amount " & _
'                          ", lnloan_advance_tran.isloan " & _
'                          ", lnloan_advance_tran.emi_amount " & _
'                          ", lnloan_advance_tran.advance_amount " & _
'                          ", lnloan_advance_tran.balance_amount " & _
'                    "FROM    lnloan_advance_tran " & _
'                    "LEFT JOIN cfcommon_period_tran AS TableDeductionPeriod ON TableDeductionPeriod.periodunkid = lnloan_advance_tran.deductionperiodunkid AND TableDeductionPeriod.modulerefid = " & enModuleReference.Payroll & " " & _
'                    "WHERE   ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
'                    "AND     balance_amount > 0 " & _
'                    "AND     loan_statusunkid = 1 " & _
'                    "AND     TableDeductionPeriod.start_date <= @startdate " & _
'                    "AND     employeeunkid = @employeeunkid "

'            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)

'            dtTable = objDataOperation.ExecQuery(strQ, strTableName).Tables(0)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetDataForProcessPayroll; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dtTable IsNot Nothing Then dtTable.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dtTable
'    End Function
'    'Sohail (08 Jun 2012) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.

    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Public Function getLoanVoucherNumberList(Optional ByVal blnNA As Boolean = False, _
                                             Optional ByVal strListName As String = "List", _
                                             Optional ByVal intEmployeeID As Integer = -1, _
                                             Optional ByVal intLoanSchemeID As Integer = -1, _
                                             Optional ByVal intProcessPendingLoanUnkid As Integer = -1 _
                                             ) As DataSet


        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try
            If blnNA Then
                strQ = "SELECT 0 AS processpendingloanunkid " & _
                          ",  ' ' + @Select AS loanvoucher_no " & _
                       "UNION "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1003, "Select"))

            End If

            strQ &= "SELECT " & _
                       "    processpendingloanunkid " & _
                       ",   loanvoucher_no " & _
                    "FROM lnloan_advance_tran " & _
                    "WHERE isvoid = 0 " & _
                    " AND loan_statusunkid IN(" & enLoanStatus.IN_PROGRESS & ", " & enLoanStatus.ON_HOLD & ") "

            If intEmployeeID > 0 Then
                strQ &= "AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID.ToString)
            End If

            If intLoanSchemeID > 0 Then
                strQ &= "AND loanschemeunkid = @loanschemeunkid "
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanSchemeID.ToString)
            End If

            If intProcessPendingLoanUnkid > 0 Then
                strQ &= "AND processpendingloanunkid = @processpendingloanunkid "
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessPendingLoanUnkid.ToString)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getLoanVoucherNumberList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function
    'Hemant (12 Nov 2021) -- End

#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
	    Language.setMessage("clsloanotherop_approval_tran", 1, "Pending")
			Language.setMessage("clsloanotherop_approval_tran", 2, "Approved")
			Language.setMessage("clsloanotherop_approval_tran", 7, "No Other Operation")
			Language.setMessage("clsMasterData", 96, "In Progress")
			Language.setMessage("clsMasterData", 97, "On Hold")
			Language.setMessage("clsMasterData", 98, "Written Off")
			Language.setMessage("clsMasterData", 100, "Completed")
            Language.setMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
            Language.setMessage(mstrModuleName, 2, "Loan")
            Language.setMessage(mstrModuleName, 3, "Advance")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Loan/Advance. Reason : This Loan/Advance is already linked to some transactions.")
            Language.setMessage(mstrModuleName, 5, "Advance")
            Language.setMessage(mstrModuleName, 6, "WEB")
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot void selected interest rate. Reason : selected interest rate is already linked with loan deduction.")
            Language.setMessage(mstrModuleName, 8, "Sorry, you cannot void selected installment amount or number. Reason : selected installment amount or number is already linked with loan deduction.")
            Language.setMessage(mstrModuleName, 9, "Sorry, you cannot void selected topup amount. Reason : selected topup amount is already linked with loan deduction.")
            Language.setMessage(mstrModuleName, 10, "Rate Change")
            Language.setMessage(mstrModuleName, 11, "Installment Change")
            Language.setMessage(mstrModuleName, 12, "Topup Added")
			Language.setMessage(mstrModuleName, 13, "Approved By :-")
            Language.setMessage(mstrModuleName, 14, "Simple Interest")
            Language.setMessage(mstrModuleName, 15, "Reducing Balance")
            Language.setMessage(mstrModuleName, 17, "No Interest")
            Language.setMessage(mstrModuleName, 18, "Reducing Balance With Fixed Pricipal EMI")
            Language.setMessage(mstrModuleName, 19, "Select")
            Language.setMessage(mstrModuleName, 20, "Daily")
            Language.setMessage(mstrModuleName, 21, "Monthly")
			Language.setMessage(mstrModuleName, 22, "Future Loan")
			Language.setMessage(mstrModuleName, 23, "Future Advance")
             Language.setMessage(mstrModuleName, 24, "By Tenure")
			Language.setMessage(mstrModuleName, 25, "Simple Interest")
			Language.setMessage(mstrModuleName, 26, "No Interest With Mapped Head")
			Language.setMessage("clsMasterData", 96, "In Progress")
			Language.setMessage("clsMasterData", 97, "On Hold")
			Language.setMessage("clsMasterData", 98, "Written Off")
			Language.setMessage("clsMasterData", 100, "Completed")
			Language.setMessage(mstrModuleName, 1, "This Voucher No is already defined. Please define new Voucher no.")
			Language.setMessage("clsloanotherop_approval_tran", 2, "Approved")
			Language.setMessage(mstrModuleName, 7, "Sorry, you cannot void selected interest rate. Reason : selected interest rate is already linked with loan deduction.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
