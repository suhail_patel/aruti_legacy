﻿'************************************************************************************************************************************
'Class Name : clslnloan_interest_Tran.vb
'Purpose    :
'Date       :04-Mar-2015
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clslnloan_interest_tran
    Private Shared ReadOnly mstrModuleName As String = "clslnloan_interest_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintLninteresttranunkid As Integer = 0
    Private mintLoanadvancetranunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mdtEffectivedate As Date = Nothing
    Private mdblInterest_Rate As Double = 0
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private xDataOpr As clsDataOperation
    Private mstrWebFormName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebHost As String = ""
    'Nilay (20-Nov-2015) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private mblnisdefault As Boolean = False
    Private mintapproverunkid As Integer = 0
    'Nilay (20-Nov-2015) -- End

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    Private mstrIdentifyGuid As String = String.Empty
    'Nilay (13-Sept-2016) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lninteresttranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Lninteresttranunkid() As Integer
        Get
            Return mintLninteresttranunkid
        End Get
        Set(ByVal value As Integer)
            mintLninteresttranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanadvancetranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Loanadvancetranunkid() As Integer
        Get
            Return mintLoanadvancetranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanadvancetranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>

    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property
  
    ''' <summary>
    ''' Purpose: Get or Set interest_rate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Interest_Rate() As Double
        Get
            Return mdblInterest_Rate
        End Get
        Set(ByVal value As Double)
            mdblInterest_Rate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Public WriteOnly Property _WebFormName() As String
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHost() As String
        Set(ByVal value As String)
            mstrWebHost = value
        End Set
    End Property

    'Nilay (20-Nov-2015) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Public Property _IsDefault() As Boolean
        Get
            Return mblnisdefault
        End Get
        Set(ByVal value As Boolean)
            mblnisdefault = value
        End Set
    End Property

    Public Property _Approverunkid() As Integer
        Get
            Return mintapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintapproverunkid = value
        End Set
    End Property
    'Nilay (20-Nov-2015) -- End

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    Public Property _Identify_Guid() As String
        Get
            Return mstrIdentifyGuid
        End Get
        Set(ByVal value As String)
            mstrIdentifyGuid = value
        End Set
    End Property
    'Nilay (13-Sept-2016) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "  lninteresttranunkid " & _
                   ", loanadvancetranunkid " & _
                   ", periodunkid " & _
                   ", effectivedate " & _
                   ", interest_rate " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   ", ISNULL(isdefault, 0) AS isdefault " & _
                   ", ISNULL(approverunkid, -1) AS approverunkid " & _
                   ", ISNULL(identify_guid,'') AS identify_guid " & _
                  "FROM lnloan_interest_tran " & _
                  "WHERE lninteresttranunkid = @lninteresttranunkid "
            'Nilay (13-Sept-2016) -- [identify_guid]
            'Nilay (20-Nov-2015) -- [isdefault], [approverunkid] -- ADDED

            objDataOperation.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLninteresttranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLninteresttranunkid = CInt(dtRow.Item("lninteresttranunkid"))
                mintLoanadvancetranunkid = CInt(dtRow.Item("loanadvancetranunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                'mdtEffectivedate = dtRow.Item("effectivedate")
                If IsDBNull(dtRow.Item("effectivedate")) = False Then
                mdtEffectivedate = dtRow.Item("effectivedate")
                Else
                    mdtEffectivedate = Nothing
                End If
                'Nilay (13-Sept-2016) -- End
                mdblInterest_Rate = CDbl(dtRow.Item("interest_rate"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Nilay (20-Nov-2015) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                mblnisdefault = CBool(dtRow.Item("isdefault"))
                mintapproverunkid = CInt(dtRow.Item("approverunkid"))
                'Nilay (20-Nov-2015) -- End

                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                mstrIdentifyGuid = dtRow.Item("identify_guid").ToString
                'Nilay (13-Sept-2016) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal intLoanAdvUnkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "  lnloan_interest_tran.lninteresttranunkid " & _
                   ", lnloan_interest_tran.loanadvancetranunkid " & _
                   ", lnloan_interest_tran.periodunkid " & _
                   ", lnloan_interest_tran.effectivedate " & _
                   ", lnloan_interest_tran.interest_rate " & _
                   ", lnloan_interest_tran.userunkid " & _
                   ", lnloan_interest_tran.isvoid " & _
                   ", lnloan_interest_tran.voiduserunkid " & _
                   ", lnloan_interest_tran.voiddatetime " & _
                   ", lnloan_interest_tran.voidreason " & _
                   ", CONVERT(CHAR(8),effectivedate,112) AS ddate " & _
                   ", cfcommon_period_tran.period_name AS dperiod " & _
                   ", cfcommon_period_tran.statusid AS pstatusid " & _
                   ", lnloan_interest_tran.isdefault " & _
                   ", lnloan_interest_tran.approverunkid " & _
                   ", lnloan_interest_tran.identify_guid " & _
                   "FROM lnloan_interest_tran " & _
                   "  JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
                   " WHERE lnloan_interest_tran.isvoid = 0 AND cfcommon_period_tran.isactive = 1 "
            'Nilay (13-Sept-2016) -- [isdefault,approverunkid,identify_guid]

            strQ &= " AND lnloan_interest_tran.loanadvancetranunkid = '" & intLoanAdvUnkid & "' "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each row As DataRow In dsList.Tables(0).Rows
                If row.Item("ddate").ToString <> "" Then
                    row.Item("ddate") = eZeeDate.convertDate(row.Item("ddate").ToString).ToShortDateString
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    ' <summary>
    ' Modify By: Sandeep Sharma
    ' </summary>
    ' <returns>Boolean</returns>
    ' <purpose> INSERT INTO Database Table (lnloan_interest_tran) </purpose>

    'Nilay (25-Mar-2016) -- Start
    'Public Function Insert(ByVal blnIncludeBalanceTran As Boolean, ByVal intYearUnkid As Integer) As Boolean
    Public Function Insert(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal xPeriodStart As Date, _
                           ByVal xPeriodEnd As Date, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal blnIncludeBalanceTran As Boolean) As Boolean
        'Nilay (25-Mar-2016) -- End

        If isExist(mdtEffectivedate, mintLoanadvancetranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Interest rate is already defined for the selected date.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim objLoanAdvance As New clsLoan_Advance
            Dim dsBalance As New DataSet
            If blnIncludeBalanceTran = True Then
                'Nilay (25-Mar-2016) -- Start
                'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo("List", mdtEffectivedate, "", mintLoanadvancetranunkid, , , , , True)
                'Hemant (30 Aug 2019) -- Start
                'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                '                                     xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                '                                     "List", mdtEffectivedate, "", mintLoanadvancetranunkid, , , , , True)
                dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                     xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                     "List", mdtEffectivedate, "", mintLoanadvancetranunkid, , , , , True, xDataOp:=objDataOperation)
                'Hemant (30 Aug 2019) -- End
                'Nilay (25-Mar-2016) -- End
            End If
            'Sohail (07 May 2015) -- End
            'Hemant (25 Mar 2019) -- Start
            objDataOperation.ClearParameters()
            'Hemant (25 Mar 2019) -- End
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DateTime.Parse(mdtEffectivedate & " " & Format(Now, "hh:mm:ss tt")))
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            'Nilay (13-Sept-2016) -- End
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblInterest_Rate.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Nilay (20-Nov-2015) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisdefault.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapproverunkid.ToString)
            'Nilay (20-Nov-2015) -- End

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentifyGuid)
            'Nilay (13-Sept-2016) -- End

            strQ = "INSERT INTO lnloan_interest_tran ( " & _
                      "  loanadvancetranunkid " & _
                      ", periodunkid " & _
                      ", effectivedate " & _
                      ", interest_rate " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                      ", isdefault " & _
                      ", approverunkid " & _
                      ", identify_guid " & _
                    ") VALUES (" & _
                      "  @loanadvancetranunkid " & _
                      ", @periodunkid " & _
                      ", @effectivedate " & _
                      ", @interest_rate " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                      ", @isdefault " & _
                      ", @approverunkid " & _
                      ", @identify_guid " & _
                    "); SELECT @@identity"
            'Nilay (13-Sept-2016) -- [identify_guid]
            'Nilay (20-Nov-2015) -- [isdefault], [approverunkid] -- ADDED
            
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLninteresttranunkid = dsList.Tables(0).Rows(0).Item(0)

            If blnIncludeBalanceTran = True Then
                'Dim objLoanAdvance As New clsLoan_Advance
                'Dim dsBalance As New DataSet
                'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo("List", mdtEffectivedate, "", mintLoanadvancetranunkid)
                If dsBalance.Tables(0).Rows.Count > 0 Then
                    Dim intPeriodID As Integer = 0

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtEffectivedate.AddDays(-1), 0, FinancialYear._Object._YearUnkid)
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                    ' intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtEffectivedate.AddDays(-1), xYearUnkid, 0) 'Nilay (25-Mar-2016) -- [xYearUnkid]
                    intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtEffectivedate.AddDays(-1), xYearUnkid, 0, xDataOperation:=objDataOperation)
                    'Hemant (30 Aug 2019) -- End
                    'S.SANDEEP [04 JUN 2015] -- END

                    If intPeriodID <= 0 Then intPeriodID = CInt(dsBalance.Tables(0).Rows(0).Item("periodunkid"))

                    '*** Do not change bf_amount (keep amount same as previous bf_amount amount) as it will be used in Interest Amount column for Simple interesr loan calculation
                    strQ = "INSERT  INTO lnloan_balance_tran " & _
                            "( loanschemeunkid  " & _
                            ", transaction_periodunkid " & _
                            ", transactiondate " & _
                            ", periodunkid " & _
                            ", end_date " & _
                            ", loanadvancetranunkid " & _
                            ", employeeunkid " & _
                            ", payrollprocesstranunkid " & _
                            ", paymenttranunkid " & _
                            ", bf_amount " & _
                            ", bf_amountpaidcurrency " & _
                            ", amount " & _
                            ", amountpaidcurrency " & _
                            ", cf_amount " & _
                            ", cf_amountpaidcurrency " & _
                            ", userunkid " & _
                            ", isvoid " & _
                            ", voiduserunkid " & _
                            ", voiddatetime " & _
                            ", voidreason " & _
                            ", lninteresttranunkid " & _
                            ", lnemitranunkid " & _
                            ", lntopuptranunkid " & _
                            ", days " & _
                            ", bf_balance " & _
                            ", bf_balancepaidcurrency " & _
                            ", principal_amount " & _
                            ", principal_amountpaidcurrency " & _
                            ", topup_amount " & _
                            ", topup_amountpaidcurrency " & _
                            ", repayment_amount " & _
                            ", repayment_amountpaidcurrency " & _
                            ", interest_rate " & _
                            ", interest_amount " & _
                            ", interest_amountpaidcurrency " & _
                            ", cf_balance " & _
                            ", cf_balancepaidcurrency " & _
                            ", isonhold " & _
                            ", isreceipt  " & _
                            ", nexteffective_days " & _
                            ", nexteffective_months " & _
                            ", exchange_rate " & _
                            ", isbrought_forward " & _
                            ", loanstatustranunkid " & _
                            ") " & _
                    "VALUES  ( " & dsBalance.Tables(0).Rows(0).Item("loanschemeunkid") & "  " & _
                            ", " & mintPeriodunkid & " " & _
                            ", '" & Format(mdtEffectivedate, "yyyyMMdd HH:mm:ss") & "' " & _
                            ", " & intPeriodID & " " & _
                            ", '" & Format(mdtEffectivedate.AddDays(-1), "yyyyMMdd HH:mm:ss") & "' " & _
                            ", " & mintLoanadvancetranunkid & " " & _
                            ", " & dsBalance.Tables(0).Rows(0).Item("employeeunkid") & " " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BF_Amount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BF_AmountPaidCurrency")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPMTAmount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPMTAmountPaidCurrency")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalance")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalancePaidCurrency")) & " " & _
                            ", " & mintUserunkid & " " & _
                            ", 0 " & _
                            ", -1 " & _
                            ", NULL " & _
                            ", '' " & _
                            ", " & mintLninteresttranunkid & " " & _
                            ", -1 " & _
                            ", -1 " & _
                            ", " & CInt(dsBalance.Tables(0).Rows(0).Item("DaysDiff")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BalanceAmount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BalanceAmountPaidCurrency")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency")) & " " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("interest_rate")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotInterestAmount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotInterestAmountPaidCurrency")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalance")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalancePaidCurrency")) & " " & _
                            ", " & CInt(Int(dsBalance.Tables(0).Rows(0).Item("isonhold"))) & " " & _
                            ", 0 " & _
                            ", " & DateDiff(DateInterval.Day, mdtEffectivedate, eZeeDate.convertDate(dsBalance.Tables(0).Rows(0).Item("LoanEndDate").ToString).AddDays(1)) & " " & _
                            ", " & DateDiff(DateInterval.Month, mdtEffectivedate, eZeeDate.convertDate(dsBalance.Tables(0).Rows(0).Item("LoanEndDate").ToString).AddDays(1)) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("exchange_rate")) & " " & _
                            ", 0 " & _
                            ", 0 " & _
                            ") "
                    'Sohail (15 Dec 2015) - [loanstatustranunkid,nexteffective_months]

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                    objLoanAdvance._DataOpr = objDataOperation
                    'Hemant (30 Aug 2019) -- End
                    objLoanAdvance._Loanadvancetranunkid = mintLoanadvancetranunkid
                    objLoanAdvance._Balance_Amount = objLoanAdvance._Balance_Amount - CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                    objLoanAdvance._Balance_AmountPaidCurrency = objLoanAdvance._Balance_AmountPaidCurrency - CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency"))
                    'Nilay (05-May-2016) -- Start
                    objLoanAdvance._WebClientIP = mstrWebIP
                    objLoanAdvance._WebFormName = mstrForm_Name
                    objLoanAdvance._WebHostName = mstrWebHost
                    'Nilay (05-May-2016) -- End

                    'Nilay (05-May-2016) -- Start
                    'If objLoanAdvance.Update(, objDataOperation) = False Then
                    If objLoanAdvance.Update(objDataOperation) = False Then
                        'Nilay (05-May-2016) -- End

                        exForce = New Exception(objLoanAdvance._Message)
                        Throw exForce
                    End If
                    'Sohail (07 May 2015) -- End

                End If
                objLoanAdvance = Nothing
            End If

            If InsertAuditInerestRate_Tran(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_interest_tran) </purpose>
    Public Function Update() As Boolean
        If isExist(mdtEffectivedate, mintLoanadvancetranunkid, mintLninteresttranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Interest rate is already defined for the selected date.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLninteresttranunkid.ToString)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DateTime.Parse(mdtEffectivedate & " " & Format(Now, "hh:mm:ss tt")))
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            'Nilay (13-Sept-2016) -- End
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblInterest_Rate.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE lnloan_interest_tran SET " & _
                      "  loanadvancetranunkid = @loanadvancetranunkid" & _
                      ", periodunkid = @periodunkid" & _
                      ", effectivedate = @effectivedate" & _
                      ", interest_rate = @interest_rate" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE lninteresttranunkid = @lninteresttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditInerestRate_Tran(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ' <summary>
    ' Modify By: Sandeep Sharma
    ' </summary>
    ' <returns>Boolean</returns>
    ' <purpose> Delete Database Table (lnloan_interest_tran) </purpose>

    'Nilay (25-Mar-2016) -- Start
    'Public Function Delete(ByVal intUnkid As Integer, ByVal blnIncludeBalanceTran As Boolean) As Boolean
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="xDatabaseName"></param>
    ''' <param name="xUserUnkid"></param>
    ''' <param name="xYearUnkid"></param>
    ''' <param name="xCompanyUnkid"></param>
    ''' <param name="xPeriodStart"></param>
    ''' <param name="xPeriodEnd"></param>
    ''' <param name="xUserModeSetting"></param>
    ''' <param name="xOnlyApproved"></param>
    ''' <param name="intUnkid"></param>
    ''' <param name="blnIncludeBalanceTran"></param>
    ''' <param name="blnIsFromLoanAdvanceList">Pass True only when from LoanAdvanceList Else False</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Delete(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal xPeriodStart As Date, _
                           ByVal xPeriodEnd As Date, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal intUnkid As Integer, _
                           ByVal blnIncludeBalanceTran As Boolean, _
                           ByVal blnIsFromLoanAdvanceList As Boolean) As Boolean
        'Nilay (20-Sept-2016) -- [blnIsFromLoanAdvanceList]

        'Nilay (25-Mar-2016) -- End

        'If isUsed(intUnkid) Then
        '    mstrMessage = ""
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot delete this rate. Reason : It is linked with loan balance transaction.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If blnIncludeBalanceTran = True Then
                Dim objLnAdv As New clsLoan_Advance
                Dim ds As DataSet = objLnAdv.GetLastLoanBalance("List", mintLoanadvancetranunkid)
                Dim strMsg As String = String.Empty
                If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) = intUnkid) Then
                    If ds.Tables("List").Rows.Count > 0 Then
                        If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 3, " Process Payroll.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                            strMsg = Language.getMessage(mstrModuleName, 4, " Loan Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                            strMsg = Language.getMessage(mstrModuleName, 5, " Receipt Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 6, " Loan Installment Change.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 7, " Loan Topup Added.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 8, " Loan Rate Change.")
                        End If
                    End If
                End If
                objLnAdv = Nothing
                If strMsg.Trim.Length > 0 Then
                    mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of") & strMsg
                    If xDataOpr Is Nothing Then
                        objDataOperation.ReleaseTransaction(False)
                    End If
                    Return False
                End If
            End If

            strQ = "UPDATE lnloan_interest_tran SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                   "WHERE lninteresttranunkid = @lninteresttranunkid "

            objDataOperation.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'Dim objlnOtherOpTran As New clsloanotherop_approval_tran
            'objlnOtherOpTran._Isvoid = mblnIsvoid
            'objlnOtherOpTran._Voiduserunkid = mintVoiduserunkid
            'objlnOtherOpTran._VoidDateTime = mdtVoiddatetime
            'objlnOtherOpTran._VoidReason = mstrVoidreason
            'If objlnOtherOpTran.Delete(mstrIdentifyGuid, objDataOperation) = False Then
            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If
            'Nilay (20-Sept-2016) -- End
            'Nilay (13-Sept-2016) -- End

            If blnIncludeBalanceTran = True Then

                objDataOperation.ClearParameters()

                strQ = "UPDATE lnloan_balance_tran SET " & _
                       "  isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason " & _
                       "WHERE lninteresttranunkid = @lninteresttranunkid "

                objDataOperation.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            _Lninteresttranunkid = intUnkid

            If InsertAuditInerestRate_Tran(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            If blnIsFromLoanAdvanceList = False Then
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim objLoan_Advance As New clsLoan_Advance
            Dim dsLoan As DataSet = Nothing
            'Nilay (25-Mar-2016) -- Start
            'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", mdtEffectivedate, "", mintLoanadvancetranunkid, , False, , , True)
            dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                               xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                               "List", mdtEffectivedate, "", mintLoanadvancetranunkid, , False, , , True)
            'Nilay (25-Mar-2016) -- End

            If dsLoan.Tables(0).Rows.Count > 0 Then
                    objLoan_Advance._DataOpr = objDataOperation 'Sohail (16 Oct 2019)
                objLoan_Advance._Loanadvancetranunkid = mintLoanadvancetranunkid
                objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + CDec(dsLoan.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency + CDec(dsLoan.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency"))

                'Nilay (05-May-2016) -- Start
                objLoan_Advance._WebClientIP = mstrWebIP
                objLoan_Advance._WebFormName = mstrWebFormName
                objLoan_Advance._WebHostName = mstrWebHost
                'Nilay (05-May-2016) -- End

                'Nilay (05-May-2016) -- Start
                'If objLoan_Advance.Update(, objDataOperation) = False Then
                If objLoan_Advance.Update(objDataOperation) = False Then
                    'Nilay (05-May-2016) -- End

                    exForce = New Exception(objLoan_Advance._Message)
                    Throw exForce
                End If
            End If
            'Sohail (07 May 2015) -- End

            End If
            'Nilay (20-Sept-2016) -- End

            
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        Try
            strQ = "SELECT 1 FROM lnloan_balance_tran WHERE isvoid = 0 AND lninteresttranunkid = @lninteresttranunkid "

            objDataOperation.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtEffDate As Date, ByVal intLoanAdvUnkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "  lninteresttranunkid " & _
                   ", loanadvancetranunkid " & _
                   ", periodunkid " & _
                   ", effectivedate " & _
                   ", interest_rate " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   "FROM lnloan_interest_tran " & _
                   "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate " & _
                   " AND loanadvancetranunkid = @loanadvancetranunkid "

            If intUnkid > 0 Then
                strQ &= " AND lninteresttranunkid <> @lninteresttranunkid"
            End If

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEffDate).ToString)
            objDataOperation.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    Public Function IsValidInterestRate(ByVal intLoanAdvUnkid As Integer, ByVal dtEffiveDate As Date, ByVal decRate As Decimal) As String
        Dim strMsg As String = String.Empty
        Dim StrQ As String = "" : Dim dsList As New DataSet
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        Try
            If xDataOpr Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOpr
            End If

                StrQ = "SELECT TOP 1 " & _
                       "    interest_rate " & _
                       "FROM lnloan_interest_tran " & _
                       "WHERE isvoid = 0 " & _
                       " AND loanadvancetranunkid = '" & intLoanAdvUnkid & "' " & _
                       " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffiveDate).ToString & "' " & _
                       "ORDER BY effectivedate DESC "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    If CDec(dsList.Tables(0).Rows(0).Item("interest_rate")) = decRate Then
                        strMsg = Language.getMessage(mstrModuleName, 11, "Sorry, you cannot enter same interest rate again for different date. Reason : Interest rate is same as previous rate defined.")
                    End If
                End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:IsValidInterestRate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return strMsg
    End Function

    'Public Function IsValidInterestRate(ByVal intLoanAdvUnkid As Integer, ByVal dtEffiveDate As Date, ByVal decRate As Decimal) As String
    '    Dim strMsg As String = String.Empty
    '    Dim StrQ As String = "" : Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        Using objDo As New clsDataOperation

    '            StrQ = "SELECT TOP 1 " & _
    '                   "    interest_rate " & _
    '                   "FROM lnloan_interest_tran " & _
    '                   "WHERE isvoid = 0 " & _
    '                   " AND loanadvancetranunkid = '" & intLoanAdvUnkid & "' " & _
    '                   " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffiveDate).ToString & "' " & _
    '                   "ORDER BY effectivedate DESC "

    '            dsList = objDo.ExecQuery(StrQ, "List")

    '            If objDo.ErrorMessage <> "" Then
    '                exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                If CDec(dsList.Tables(0).Rows(0).Item("interest_rate")) = decRate Then
    '                    strMsg = Language.getMessage(mstrModuleName, 11, "Sorry, you cannot enter same interest rate again for different date. Reason : Interest rate is same as previous rate defined.")
    '                End If
    '            End If

    '        End Using
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name:IsValidInterestRate; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return strMsg
    'End Function
    'Nilay (13-Sept-2016) -- End

    

    Private Function InsertAuditInerestRate_Tran(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atlnloan_interest_tran ( " & _
                       "  lninteresttranunkid " & _
                       ", loanadvancetranunkid " & _
                       ", periodunkid " & _
                       ", effectivedate " & _
                       ", interest_rate " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name " & _
                       ", form_name " & _
                       ", module_name1 " & _
                       ", module_name2 " & _
                       ", module_name3 " & _
                       ", module_name4 " & _
                       ", module_name5 " & _
                       ", isweb" & _
                       ", isdefault" & _
                       ", approverunkid " & _
                       ", identify_guid " & _
                   ") VALUES (" & _
                       "  @lninteresttranunkid " & _
                       ", @loanadvancetranunkid " & _
                       ", @periodunkid " & _
                       ", @effectivedate " & _
                       ", @interest_rate " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @form_name " & _
                       ", @module_name1 " & _
                       ", @module_name2 " & _
                       ", @module_name3 " & _
                       ", @module_name4 " & _
                       ", @module_name5 " & _
                       ", @isweb" & _
                       ", @isdefault" & _
                       ", @approverunkid " & _
                       ", @identify_guid " & _
                   ") "
            'Nilay (13-Sept-2016) -- [identify_guid]

            'Nilay (03-Feb-2016) -- Start
            'ENHANCEMENT - Add isdefault & approverunkid for Loan Parameters' approval process
            '[isdefault],[approverunkid]
            'Nilay (03-Feb-2016) -- End

            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLninteresttranunkid)
            xDataOpr.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid)
            xDataOpr.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            xDataOpr.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            xDataOpr.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblInterest_Rate)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            If mstrWebIP.ToString().Trim.Length <= 0 Then
                mstrWebIP = getIP()
            End If
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)

            If mstrWebHost.ToString().Length <= 0 Then
                mstrWebHost = getHostName()
            End If
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHost)

            If mstrWebFormName.Trim.Length <= 0 Then
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CType(0, Boolean).ToString)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CType(1, Boolean).ToString)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 10, "WEB"))
            End If
            xDataOpr.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            xDataOpr.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            xDataOpr.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            xDataOpr.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            'Nilay (03-Feb-2016) -- Start
            'ENHANCEMENT - Add isdefault & approverunkid for Loan Parameters' approval process
            xDataOpr.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisdefault.ToString)
            xDataOpr.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapproverunkid.ToString)
            'Nilay (03-Feb-2016) -- End

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            xDataOpr.AddParameter("@identify_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentifyGuid)
            'Nilay (13-Sept-2016) -- End

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditInerestRate_Tran; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Interest rate is already defined for the selected date.")
            Language.setMessage(mstrModuleName, 3, " Process Payroll.")
            Language.setMessage(mstrModuleName, 4, " Loan Payment List.")
            Language.setMessage(mstrModuleName, 5, " Receipt Payment List.")
            Language.setMessage(mstrModuleName, 6, " Loan Installment Change.")
            Language.setMessage(mstrModuleName, 7, " Loan Topup Added.")
            Language.setMessage(mstrModuleName, 8, " Loan Rate Change.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of")
            Language.setMessage(mstrModuleName, 10, "WEB")
            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot enter same interest rate again for different date. Reason : Interest rate is same as previous rate defined.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class clslnloan_interest_tran

'    Private Shared Readonly mstrModuleName As String = "clslnloan_interest_tran"
'    Dim mstrMessage As String = ""

'#Region " Private variables "

'    Private mintLninteresttranunkid As Integer
'    Private mintLoanadvancetranunkid As Integer
'    Private mdtTran As DataTable
'    Private mstrWebFormName As String = String.Empty
'    Private mstrWebIP As String = ""
'    Private mstrWebHost As String = ""

'#End Region

'#Region " Properties "

'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    Public Property _Loanadvancetranunkid() As Integer
'        Get
'            Return mintLoanadvancetranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanadvancetranunkid = value
'            Call GetInterestTran()
'        End Set
'    End Property

'    Public Property _DataTable() As DataTable
'        Get
'            Return mdtTran
'        End Get
'        Set(ByVal value As DataTable)
'            mdtTran = value
'        End Set
'    End Property

'    Public Property _WebFormName() As String
'        Get
'            Return mstrWebFormName
'        End Get
'        Set(ByVal value As String)
'            mstrWebFormName = value
'        End Set
'    End Property

'    Public Property _WebIP() As String
'        Get
'            Return mstrWebIP
'        End Get
'        Set(ByVal value As String)
'            mstrWebIP = value
'        End Set
'    End Property

'    Public Property _WebHost() As String
'        Get
'            Return mstrWebHost
'        End Get
'        Set(ByVal value As String)
'            mstrWebHost = value
'        End Set
'    End Property

'#End Region

'#Region " Contructor "

'    Public Sub New()
'        Try
'            mdtTran = New DataTable("InterestRate")

'            mdtTran.Columns.Add("lninteresttranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("loanadvancetranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            'mdtTran.Columns.Add("lntopuptranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("effectivedate", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
'            mdtTran.Columns.Add("interest_rate", System.Type.GetType("System.Decimal")).DefaultValue = 0
'            mdtTran.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
'            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
'            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""

'            '=========================================== DISPLAY & CHECKING PURPOSE
'            mdtTran.Columns.Add("dperiod", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtTran.Columns.Add("ddate", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtTran.Columns.Add("pstatusid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            '=========================================== DISPLAY & CHECKING PURPOSE

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Private/Public Methods "

'    Private Sub GetInterestTran()
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Dim objDataOperation As New clsDataOperation
'        Try
'            StrQ = "SELECT " & _
'                   "  lnloan_interest_tran.lninteresttranunkid " & _
'                   ", lnloan_interest_tran.loanadvancetranunkid " & _
'                   ", lnloan_interest_tran.periodunkid " & _
'                   ", lnloan_interest_tran.effectivedate " & _
'                   ", lnloan_interest_tran.interest_rate " & _
'                   ", lnloan_interest_tran.userunkid " & _
'                   ", lnloan_interest_tran.isvoid " & _
'                   ", lnloan_interest_tran.voiduserunkid " & _
'                   ", lnloan_interest_tran.voiddatetime " & _
'                   ", lnloan_interest_tran.voidreason " & _
'                   ", '' AS AUD " & _
'                   ", CONVERT(CHAR(8),effectivedate,112) AS ddate " & _
'                   ", cfcommon_period_tran.period_name AS dperiod " & _
'                   ", cfcommon_period_tran.statusid AS pstatusid " & _
'                   "FROM lnloan_interest_tran " & _
'                   "    JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_interest_tran.periodunkid " & _
'                   "WHERE lnloan_interest_tran.loanadvancetranunkid = @loanadvancetranunkid " & _
'                   "    AND lnloan_interest_tran.isvoid = 0 AND cfcommon_period_tran.isactive = 1 "

'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            mdtTran.Rows.Clear()

'            For Each xRow As DataRow In dsList.Tables(0).Rows
'                If xRow.Item("ddate").ToString.Trim.Length > 0 Then
'                    xRow.Item("ddate") = eZeeDate.convertDate(xRow.Item("ddate").ToString).ToShortDateString
'                End If
'                mdtTran.ImportRow(xRow)
'            Next

'            If mdtTran.Rows.Count <= 0 Then
'                mdtTran.Rows.Add(mdtTran.NewRow)
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetInterestTran", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Public Function InsertUpdateDelete_InerestRate_Tran(Optional ByVal xDataOpr As clsDataOperation = Nothing, _
'                                                        Optional ByVal blnIncludeBalanceTran As Boolean = False, Optional ByVal intUserId As Integer = 0) As Boolean
'        Dim i As Integer
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim exForce As Exception
'        Dim dsList As New DataSet
'        Dim objDataOperation As clsDataOperation

'        If xDataOpr IsNot Nothing Then
'            objDataOperation = xDataOpr
'        Else
'            objDataOperation = New clsDataOperation
'            objDataOperation.BindTransaction()
'        End If
'        Try

'            For i = 0 To mdtTran.Rows.Count - 1
'                With mdtTran.Rows(i)
'                    objDataOperation.ClearParameters()
'                    If Not IsDBNull(.Item("AUD")) Then
'                        Select Case .Item("AUD")
'                            Case "A"
'                                If .Item("periodunkid") <= 0 Then Continue For

'                                strQ = "INSERT INTO lnloan_interest_tran ( " & _
'                                           "  loanadvancetranunkid " & _
'                                           ", periodunkid " & _
'                                           ", effectivedate " & _
'                                           ", interest_rate " & _
'                                           ", userunkid " & _
'                                           ", isvoid " & _
'                                           ", voiduserunkid " & _
'                                           ", voiddatetime " & _
'                                           ", voidreason" & _
'                                       ") VALUES (" & _
'                                           "  @loanadvancetranunkid " & _
'                                           ", @periodunkid " & _
'                                           ", @effectivedate " & _
'                                           ", @interest_rate " & _
'                                           ", @userunkid " & _
'                                           ", @isvoid " & _
'                                           ", @voiduserunkid " & _
'                                           ", @voiddatetime " & _
'                                           ", @voidreason" & _
'                                       "); SELECT @@identity"

'                                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid)
'                                'objDataOperation.AddParameter("@lntopuptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lntopuptranunkid"))
'                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid"))
'                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("effectivedate"))
'                                objDataOperation.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("interest_rate"))
'                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
'                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
'                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
'                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
'                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

'                                dsList = objDataOperation.ExecQuery(strQ, "List")

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                                mintLninteresttranunkid = dsList.Tables(0).Rows(0).Item(0)

'                                If blnIncludeBalanceTran = True Then
'                                    Dim objLoanAdvance As New clsLoan_Advance
'                                    Dim dsBalance As New DataSet
'                                    dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo("List", CDate(.Item("effectivedate")).AddDays(-1), "", mintLoanadvancetranunkid)
'                                    If dsBalance.Tables(0).Rows.Count > 0 Then

'                                        strQ = "INSERT  INTO lnloan_balance_tran " & _
'                                                "( loanschemeunkid  " & _
'                                                ", periodunkid " & _
'                                                ", end_date " & _
'                                                ", loanadvancetranunkid " & _
'                                                ", employeeunkid " & _
'                                                ", payrollprocesstranunkid " & _
'                                                ", paymenttranunkid " & _
'                                                ", bf_amount " & _
'                                                ", amount " & _
'                                                ", cf_amount " & _
'                                                ", userunkid " & _
'                                                ", isvoid " & _
'                                                ", voiduserunkid " & _
'                                                ", voiddatetime " & _
'                                                ", voidreason " & _
'                                                ", lninteresttranunkid " & _
'                                                ", lnemitranunkid " & _
'                                                ", lntopuptranunkid " & _
'                                                ", days " & _
'                                                ", bf_balance " & _
'                                                ", principal_amount " & _
'                                                ", topup_amount " & _
'                                                ", repayment_amount " & _
'                                                ", interest_rate " & _
'                                                ", interest_amount " & _
'                                                ", cf_balance " & _
'                                                ", isonhold " & _
'                                                ", isreceipt  " & _
'                                                ", nexteffective_days " & _
'                                                ") " & _
'                                        "VALUES  ( " & dsBalance.Tables(0).Rows(0).Item("loanschemeunkid") & "  " & _
'                                                ", " & .Item("periodunkid") & " " & _
'                                                ", '" & Format(.Item("effectivedate").AddDays(-1), "yyyyMMdd HH:mm:ss") & "' " & _
'                                                ", " & mintLoanadvancetranunkid & " " & _
'                                                ", " & dsBalance.Tables(0).Rows(0).Item("employeeunkid") & " " & _
'                                                ", 0 " & _
'                                                ", 0 " & _
'                                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("PrincipalBalance")) & " " & _
'                                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPMTAmount")) & " " & _
'                                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalance")) & " " & _
'                                                ", " & intUserId & " " & _
'                                                ", 0 " & _
'                                                ", -1 " & _
'                                                ", NULL " & _
'                                                ", '' " & _
'                                                ", " & mintLninteresttranunkid & " " & _
'                                                ", -1 " & _
'                                                ", -1 " & _
'                                                ", " & CInt(dsBalance.Tables(0).Rows(0).Item("DaysDiff")) & " " & _
'                                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("PrincipalBalance")) & " " & _
'                                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmount")) & " " & _
'                                                ", 0 " & _
'                                                ", 0 " & _
'                                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("interest_rate")) & " " & _
'                                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotInterestAmount")) & " " & _
'                                                ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalance")) & " " & _
'                                                ", 0 " & _
'                                                ", 0 " & _
'                                                ", " & CInt(dsBalance.Tables(0).Rows(0).Item("nexteffective_days")) - CInt(dsBalance.Tables(0).Rows(0).Item("DaysDiff")) & " " & _
'                                                ") "

'                                        objDataOperation.ExecNonQuery(strQ)

'                                        If objDataOperation.ErrorMessage <> "" Then
'                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                            Throw exForce
'                                        End If


'                                    End If
'                                    objLoanAdvance = Nothing
'                                End If

'                                If InsertAuditInerestRate_Tran(objDataOperation, mdtTran.Rows(i), enAuditType.ADD, mintLninteresttranunkid) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            Case "U"

'                                If .Item("periodunkid") <= 0 Then Continue For

'                                strQ = "UPDATE lnloan_interest_tran SET " & _
'                                           "  loanadvancetranunkid = @loanadvancetranunkid" & _
'                                           ", periodunkid = @periodunkid" & _
'                                           ", effectivedate = @effectivedate" & _
'                                           ", interest_rate = @interest_rate" & _
'                                           ", userunkid = @userunkid" & _
'                                           ", isvoid = @isvoid" & _
'                                           ", voiduserunkid = @voiduserunkid" & _
'                                           ", voiddatetime = @voiddatetime" & _
'                                           ", voidreason = @voidreason " & _
'                                       "WHERE lninteresttranunkid = @lninteresttranunkid "

'                                objDataOperation.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lninteresttranunkid"))
'                                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loanadvancetranunkid"))
'                                'objDataOperation.AddParameter("@lntopuptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lntopuptranunkid"))
'                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid"))
'                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("effectivedate"))
'                                objDataOperation.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("interest_rate"))
'                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
'                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
'                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
'                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
'                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

'                                Call objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                                If InsertAuditInerestRate_Tran(objDataOperation, mdtTran.Rows(i), enAuditType.EDIT, .Item("lninteresttranunkid")) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            Case "D"

'                                If .Item("periodunkid") <= 0 Then Continue For

'                                If blnIncludeBalanceTran = True Then
'                                    Dim objLnAdv As New clsLoan_Advance
'                                    Dim ds As DataSet = objLnAdv.GetLastLoanBalance("List", mintLoanadvancetranunkid)
'                                    Dim strMsg As String = String.Empty
'                                    If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) = .Item("lninteresttranunkid")) Then
'                                        If ds.Tables("List").Rows.Count > 0 Then
'                                            If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
'                                                strMsg = Language.getMessage(mstrModuleName, 1, " Process Payroll.")
'                                            ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
'                                                strMsg = Language.getMessage(mstrModuleName, 2, " Loan Payment List.")
'                                            ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
'                                                strMsg = Language.getMessage(mstrModuleName, 3, " Receipt Payment List.")
'                                            ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
'                                                strMsg = Language.getMessage(mstrModuleName, 4, " Loan Operation (EMI).")
'                                            ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
'                                                strMsg = Language.getMessage(mstrModuleName, 5, " Loan Operation (Topup).")
'                                            ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
'                                                strMsg = Language.getMessage(mstrModuleName, 6, " Loan Operation (Rate).")
'                                            End If
'                                        End If
'                                    End If
'                                    objLnAdv = Nothing
'                                    If strMsg.Trim.Length > 0 Then
'                                        mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of") & strMsg
'                                        If xDataOpr Is Nothing Then
'                                            objDataOperation.ReleaseTransaction(False)
'                                        End If
'                                        Return False
'                                    End If
'                                End If

'                                strQ = "UPDATE lnloan_interest_tran SET " & _
'                                           "  isvoid = @isvoid" & _
'                                           ", voiduserunkid = @voiduserunkid" & _
'                                           ", voiddatetime = @voiddatetime" & _
'                                           ", voidreason = @voidreason " & _
'                                       "WHERE lninteresttranunkid = @lninteresttranunkid "

'                                objDataOperation.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lninteresttranunkid"))
'                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
'                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
'                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
'                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

'                                Call objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                                If blnIncludeBalanceTran = True Then

'                                    objDataOperation.ClearParameters()

'                                    strQ = "UPDATE lnloan_balance_tran SET " & _
'                                           "  isvoid = @isvoid" & _
'                                           ", voiduserunkid = @voiduserunkid" & _
'                                           ", voiddatetime = @voiddatetime" & _
'                                           ", voidreason = @voidreason " & _
'                                       "WHERE lninteresttranunkid = @lninteresttranunkid "

'                                    objDataOperation.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lninteresttranunkid"))
'                                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
'                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
'                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
'                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

'                                    Call objDataOperation.ExecNonQuery(strQ)

'                                    If objDataOperation.ErrorMessage <> "" Then
'                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                        Throw exForce
'                                    End If

'                                End If

'                                If InsertAuditInerestRate_Tran(objDataOperation, mdtTran.Rows(i), enAuditType.DELETE, .Item("lninteresttranunkid")) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If
'                        End Select
'                    End If
'                End With
'            Next

'            If xDataOpr Is Nothing Then
'                objDataOperation.ReleaseTransaction(True)
'            End If

'        Catch ex As Exception
'            If xDataOpr Is Nothing Then
'                objDataOperation.ReleaseTransaction(False)
'            End If
'            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_InerestRate_Tran", mstrModuleName)
'            Return False
'        Finally
'            If xDataOpr Is Nothing Then
'                objDataOperation = Nothing
'            End If
'        End Try
'        Return True
'    End Function

'    Private Function InsertAuditInerestRate_Tran(ByVal xDataOpr As clsDataOperation, _
'                                                         ByVal xRow As DataRow, _
'                                                         ByVal xAuditType As enAuditType, _
'                                                         ByVal xUnkid As Integer) As Boolean
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim exForce As Exception = Nothing
'        Try
'            strQ = "INSERT INTO atlnloan_interest_tran ( " & _
'                       "  lninteresttranunkid " & _
'                       ", loanadvancetranunkid " & _
'                       ", periodunkid " & _
'                       ", effectivedate " & _
'                       ", interest_rate " & _
'                       ", audittype " & _
'                       ", audituserunkid " & _
'                       ", auditdatetime " & _
'                       ", ip " & _
'                       ", machine_name " & _
'                       ", form_name " & _
'                       ", module_name1 " & _
'                       ", module_name2 " & _
'                       ", module_name3 " & _
'                       ", module_name4 " & _
'                       ", module_name5 " & _
'                       ", isweb" & _
'                   ") VALUES (" & _
'                       "  @lninteresttranunkid " & _
'                       ", @loanadvancetranunkid " & _
'                       ", @periodunkid " & _
'                       ", @effectivedate " & _
'                       ", @interest_rate " & _
'                       ", @audittype " & _
'                       ", @audituserunkid " & _
'                       ", @auditdatetime " & _
'                       ", @ip " & _
'                       ", @machine_name " & _
'                       ", @form_name " & _
'                       ", @module_name1 " & _
'                       ", @module_name2 " & _
'                       ", @module_name3 " & _
'                       ", @module_name4 " & _
'                       ", @module_name5 " & _
'                       ", @isweb" & _
'                   ") "

'            xDataOpr.ClearParameters()
'            xDataOpr.AddParameter("@lninteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnkid)
'            xDataOpr.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("loanadvancetranunkid"))
'            'xDataOpr.AddParameter("@lntopuptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("lntopuptranunkid"))
'            xDataOpr.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("periodunkid"))
'            xDataOpr.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, xRow.Item("effectivedate"))
'            xDataOpr.AddParameter("@interest_rate", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xRow.Item("interest_rate"))
'            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
'            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("userunkid"))
'            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
'            If mstrWebIP.ToString().Trim.Length <= 0 Then
'                mstrWebIP = getIP()
'            End If
'            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)

'            If mstrWebHost.ToString().Length <= 0 Then
'                mstrWebHost = getHostName()
'            End If
'            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHost)

'            If mstrWebFormName.Trim.Length <= 0 Then
'                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
'                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CType(0, Boolean).ToString)
'                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
'            Else
'                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
'                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CType(1, Boolean).ToString)
'                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
'            End If
'            xDataOpr.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
'            xDataOpr.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
'            xDataOpr.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
'            xDataOpr.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

'            xDataOpr.ExecNonQuery(strQ)

'            If xDataOpr.ErrorMessage <> "" Then
'                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditInerestRate_Tran; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'        End Try
'        Return True
'    End Function

'#End Region

'End Class

