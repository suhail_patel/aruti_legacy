﻿'************************************************************************************************************************************
'Class Name : clslnapproverlevel_master.vb
'Purpose    : All Approver Level Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :09/03/2015
'Written By :Nilay
'Modified   :

'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Nilay
''' </summary>
Public Class clslnapproverlevel_master


    Private Shared ReadOnly mstrModuleName As String = "clslnapproverlevel_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintlnLevelunkid As Integer
    Private mstrlnLevelname As String = String.Empty
    Private mbIsactive As Boolean = True
    Private mstrlnLevelname1 As String = String.Empty
    Private mstrlnLevelname2 As String = String.Empty
    Private mintPriority As Integer
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Nilay
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lnLevelunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _lnLevelunkid() As Integer
        Get
            Return mintlnLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintlnLevelunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lnLevelname
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _lnLevelname() As String
        Get
            Return mstrlnLevelname
        End Get
        Set(ByVal value As String)
            mstrlnLevelname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isactive
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mbIsactive
        End Get
        Set(ByVal value As Boolean)
            mbIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lnLevelname1
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _lnLevelname1() As String
        Get
            Return mstrlnLevelname1
        End Get
        Set(ByVal value As String)
            mstrlnLevelname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lnLevelname2
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _lnLevelname2() As String
        Get
            Return mstrlnLevelname2
        End Get
        Set(ByVal value As String)
            mstrlnLevelname2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Priority
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
            "  lnlevelunkid" & _
            ", lnlevelname" & _
            ", isactive" & _
            ", lnlevelname1" & _
            ", lnlevelname2" & _
            ", priority " & _
            "FROM lnapproverlevel_master " & _
            "WHERE lnlevelunkid=@lnlevelunkid "

            objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnLevelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintlnLevelunkid = CInt(dtRow.Item("lnlevelunkid"))
                mstrlnLevelname = dtRow.Item("lnlevelname").ToString
                mbIsactive = CBool(dtRow.Item("isactive"))
                mstrlnLevelname1 = dtRow.Item("lnlevelname1").ToString
                mstrlnLevelname2 = dtRow.Item("lnlevelname2").ToString
                If dtRow.Item("priority") IsNot DBNull.Value Then mintPriority = dtRow.Item("priority")
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Sub

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
            "  lnlevelunkid" & _
            ", lnlevelname" & _
            ", isactive" & _
            ", lnlevelname1" & _
            ", lnlevelname2" & _
            ", priority " & _
            "FROM lnapproverlevel_master"

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnapproverlevel_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrlnLevelname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Approver Level Name is already defined. Please define new Loan Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Loan Approver Level Priority is already assign. Please assign new Loan Approver Level Priority.")
            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Return False
            'Nilay (21-Jul-2016) -- End

        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@lnlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrlnLevelname.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mbIsactive.ToString)
            objDataOperation.AddParameter("@lnlevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrlnLevelname1.ToString)
            objDataOperation.AddParameter("@lnlevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrlnLevelname2.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)

            strQ = "INSERT INTO lnapproverlevel_master (" & _
                        "  lnlevelname" & _
                        ", isactive" & _
                        ", lnlevelname1" & _
                        ", lnlevelname2" & _
                        ", priority" & _
                    ") VALUES (" & _
                        "  @lnlevelname" & _
                        ", @isactive" & _
                        ", @lnlevelname1" & _
                        ", @lnlevelname2" & _
                        ", @priority" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintlnLevelunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lnapproverlevel_master", "lnlevelunkid", mintlnLevelunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvapproverlevel_master) </purpose>
    Public Function Update() As Boolean

        If isExist(mstrlnLevelname, mintlnLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Approver Level Name is already defined. Please define new Loan Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority, mintlnLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Loan Approver Level Priority is already assign. Please assign new Loan Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnLevelunkid.ToString)
            objDataOperation.AddParameter("@lnlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrlnLevelname.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mbIsactive.ToString)
            objDataOperation.AddParameter("@lnlevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrlnLevelname1.ToString)
            objDataOperation.AddParameter("@lnlevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrlnLevelname2.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)

            strQ = "UPDATE lnapproverlevel_master SET " & _
              "  lnlevelname = @lnlevelname " & _
              ", isactive = @isactive" & _
              ", lnlevelname1 = @lnlevelname1" & _
              ", lnlevelname2 = @lnlevelname2 " & _
              ", priority  = @priority  " & _
            "WHERE lnlevelunkid = @lnlevelunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lnapproverlevel_master", mintlnLevelunkid, "lnlevelunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnapproverlevel_master", "lnlevelunkid", mintlnLevelunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvapproverlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "Update lnapproverlevel_master set isactive = 0 " & _
            "WHERE lnlevelunkid = @lnlevelunkid "

            objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnapproverlevel_master", "lnlevelunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

   

    'Shani (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    objDataOperation = New clsDataOperation
    '    Try
    '        strQ = "select isnull(lnlevelunkid,0) from lnapproverlevel_master WHERE lnlevelunkid = @lnlevelunkid"
    '        objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    ''' <summary>
    ''' Create By: Nilay
    ''' modify by Shani
    ''' modify Date : 11 - AUG - 2016 </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS where COLUMNS.COLUMN_NAME = 'lnlevelunkid' AND COLUMNS.TABLE_NAME <> 'lnapproverlevel_master' "
            dsList = objDataOperation.ExecQuery(strQ, "List")
            For Each dRow As DataRow In dsList.Tables(0).Rows
                strQ = "SELECT lnlevelunkid FROM " & dRow.Item("TABLE_NAME") & " WHERE lnlevelunkid = '" & intUnkid & "'"
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function
    'Shani (21-Jul-2016) -- End

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strNme As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT" & _
                "  lnlevelunkid" & _
                ", lnlevelname " & _
                ", isactive" & _
                ", lnlevelname1" & _
                ", lnlevelname2" & _
                ", priority " & _
            "FROM lnapproverlevel_master " & _
            "WHERE isactive = 1 "

            If strNme.Length > 0 Then
                strQ &= " AND lnlevelname = @lnlevelname"
            End If

            If intUnkid > 0 Then
                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                'strQ &= " AND lnlevelunkid = @lnlevelunkid"
                strQ &= " AND lnlevelunkid <> @lnlevelunkid"
                'Shani (21-Jul-2016) -- End
            End If

            objDataOperation.AddParameter("@lnlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strNme)
            objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal mintPriority As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  lnlevelunkid " & _
              ", lnlevelname " & _
              ", isactive " & _
              ", lnlevelname1 " & _
              ", lnlevelname2 " & _
              ", priority " & _
             "FROM lnapproverlevel_master " & _
                   "WHERE isactive = 1 AND priority = @priority "
            'Nilay (21-Jul-2016) -- [isactive = 1]

            If intUnkid > 0 Then
                strQ &= " AND lnlevelunkid <> @lnlevelunkid"
            End If

            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as lnlevelunkid, ' ' +  @lnlevelname  as name UNION "
            End If
            strQ &= "SELECT lnlevelunkid, lnlevelname as name FROM lnapproverlevel_master where isactive = 1 ORDER BY name "

            objDataOperation.AddParameter("@lnlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApproverLevelUnkId(ByVal strLevelName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "Select isnull(lnlevelunkid,0) lnlevelunkid from lnapproverlevel_master" & _
                       " where 1 = 1 "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            If strLevelName <> "" Then
                strQ &= " AND lnlevelname = @lnlevelname"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@lnlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLevelName)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("lnlevelunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverLevelUnkId; Module Name: " & mstrModuleName)
        End Try
        Return -1
    End Function

    ' <summary>
    ' Modify By: Pinkal
    ' </summary>
    ' <purpose> Assign all Property variable </purpose>

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 

    Public Function GetLevelFromUserLogin(ByVal intUserLoginID As Integer) As DataSet
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            Dim strQCondition As String = String.Empty
            Dim strQFinal As String = String.Empty

            strQ = "SELECT " & _
                      "  #CODE# AS ApproverCode " & _
                      " ,#APPROVER_NAME# AS ApproverName " & _
                      " ,#APPROVER_NAME# + ' - ' +  ISNULL(lnapproverlevel_master.lnlevelname,'') AS Approver " & _
                      " ,ISNULL(lnapproverlevel_master.lnlevelunkid,0) AS lnlevelunkid " & _
                      " ,ISNULL(lnapproverlevel_master.lnlevelname,'') AS lnlevelname " & _
                      " ,ISNULL(lnapproverlevel_master.priority,0) AS priority " & _
                      " ,lnloanapprover_master.lnapproverunkid " & _
                      " ,lnloanapprover_master.approverempunkid " & _
                      " ,lnloanapprover_master.isexternalapprover " & _
                   " FROM lnloan_approver_mapping " & _
                      " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_approver_mapping.approvertranunkid AND lnloanapprover_master.isvoid = 0  " & _
                      " #EMPLOYEE_JOIN# " & _
                      " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid AND lnapproverlevel_master.isactive = 1 " & _
                      " LEFT JOIN hrmsconfiguration..cfuser_master AS UM ON UM.userunkid = lnloan_approver_mapping.userunkid " & _
                   " WHERE lnloan_approver_mapping.userunkid = @loginUserID AND lnloanapprover_master.isswap = 0 AND lnloanapprover_master.isvoid = 0 "
            'Nilay (23-Feb-2016) -- AND lnloanapprover_master.isswap = 0 AND lnloanapprover_master.isvoid = 0

            strQFinal = strQ

            strQCondition = " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            strQ &= strQCondition

            strQ = strQ.Replace("#CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid AND hremployee_master.isapproved = 1 ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserLoginID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow In dsCompany.Tables("Company").Rows
                strQ = strQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#CODE#", "'' ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanapprover_master.approverempunkid ")

                Else
                    strQ = strQ.Replace("#CODE#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN '' " & _
                                                       "ELSE ISNULL(hremployee_master.employeecode,'') END ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                End If

                strQ &= strQCondition
                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@loginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserLoginID)

                dsExtList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables("List").Copy)
                Else
                    dsList.Tables("List").Merge(dsExtList.Tables("List"), True)
                End If

            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority,ApproverName", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLevelFromUserLogin; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function GetLevelFromUserLogin(ByVal intUserLoginID As Integer) As DataSet
    '    Dim strQ As String = ""
    '    Dim dsList As DataSet = Nothing
    '    Dim exForce As Exception
    '    Try
    '        Dim objDataOperation As New clsDataOperation

    '        strQ = "SELECT " & _
    '                  " ISNULL(hremployee_master.employeecode,'') AS ApproverCode " & _
    '                  ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS ApproverName " & _
    '                  ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') + ' - ' +  ISNULL(lnapproverlevel_master.lnlevelname,'') AS Approver " & _
    '                  ",ISNULL(lnapproverlevel_master.lnlevelunkid,0) AS lnlevelunkid " & _
    '                  ",ISNULL(lnapproverlevel_master.lnlevelname,'') AS lnlevelname " & _
    '                  ",ISNULL(lnapproverlevel_master.priority,0) AS priority " & _
    '                  ",lnloanapprover_master.lnapproverunkid " & _
    '                  ",lnloanapprover_master.approverempunkid " & _
    '               " FROM lnloan_approver_mapping " & _
    '                  " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_approver_mapping.approvertranunkid AND lnloanapprover_master.isvoid = 0  " & _
    '                  " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                  " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid AND lnapproverlevel_master.isactive = 1 " & _
    '               " WHERE lnloan_approver_mapping.userunkid = @loginUserID AND lnloanapprover_master.isswap = 0 AND lnloanapprover_master.isvoid = 0 "
    '        'Nilay (23-Feb-2016) -- AND lnloanapprover_master.isswap = 0 AND lnloanapprover_master.isvoid = 0

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@loginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserLoginID)
    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetLevelFromUserLogin; Module Name: " & mstrModuleName)
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (01-Mar-2016) -- End

    

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLowerLevelPriority(ByVal intCurrentPriority As Integer ) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT  ISNULL(MAX(priority), -1) AS LowerLevelPriority " & _
                      " FROM    lnapproverlevel_master " & _
                      " WHERE  isactive = 1 " & _
                      " AND priority < @priority "
            strQ &= " ORDER BY LowerLevelPriority DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("LowerLevelPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLowerLevelPriority", mstrModuleName)
        End Try
        Return -1
    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Loan Approver Level Name is already defined. Please define new Loan Approver Level Name.")
			Language.setMessage(mstrModuleName, 2, "This Loan Approver Level Priority is already assign. Please assign new Loan Approver Level Priority.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class