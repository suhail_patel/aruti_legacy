﻿'************************************************************************************************************************************
'Class Name : clsLnloan_scheme.vb
'Purpose    :
'Date       :07/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsLoan_Scheme
    Private Shared ReadOnly mstrModuleName As String = "clsLoan_Scheme"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLoanschemeunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty

    'Anjan (11 May 2011)-Start
    'Private mdblMinnetsalary As Double
    Private mdecMinnetsalary As Decimal
    'Anjan (11 May 2011)-End 


    Private mdblMinhoursworked As Double
    Private mintLastnoofmonths As Integer
    'Private mblnIsinclude_Parttime As Boolean = False
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty


    'Pinkal (14-Apr-2015) -- Start
    'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
    Private mblnIsShowOnESS As Boolean = False
    'Pinkal (14-Apr-2015) -- End

    'Nilay (19-Oct-2016) -- Start
    'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
    Private mblnShowLoanBalOnPayslip As Boolean = False
    'Nilay (19-Oct-2016) -- End

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 52
    Private mdecMaxLoanAmountLimit As Decimal = 0
    'S.SANDEEP [20-SEP-2017] -- END

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Private mintLoanCalcTypeId As Integer = 0
    Private mintInterestCalctypeId As Integer = 0
    Private mdecInterestRate As Decimal = 0
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (22 Sep 2017) -- Start
    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
    Private mdecEmi_netpaypercentage As Decimal = 0
    'Sohail (22 Sep 2017) -- End


    'Varsha (25 Nov 2017) -- Start
    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
    Private mintMaxNoOfInstallment As Integer = 0
    'Varsha (25 Nov 2017) -- End

    'Sohail (02 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
    Private mintRefLoanSchemeUnkid As Integer = 0
    'Sohail (02 Apr 2018) -- End
    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private mintTranheadUnkid As Integer = 0
    'Sohail (11 Apr 2018) -- End
    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Private mintCostcenterunkid As Integer = 0
    'Sohail (14 Mar 2019) -- End
    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private mintMapped_TranheadUnkid As Integer = 0
    'Sohail (29 Apr 2019) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set minnetsalary
    '''' Modify By: Anjan
    '''' </summary>

    'Anjan (11 May 2011)-Start
    'Public Property _Minnetsalary() As Double
    Public Property _Minnetsalary() As Decimal
        'Anjan (11 May 2011)-End 

        Get
            Return mdecMinnetsalary
        End Get

        'Anjan (11 May 2011)-Start
        'Set(ByVal value As Double)
        Set(ByVal value As Decimal)
            'Anjan (11 May 2011)-End
            mdecMinnetsalary = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set minhoursworked
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Minhoursworked() As Double
        Get
            Return mdblMinhoursworked
        End Get
        Set(ByVal value As Double)
            mdblMinhoursworked = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lastnoofmonths
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Lastnoofmonths() As Integer
        Get
            Return mintLastnoofmonths
        End Get
        Set(ByVal value As Integer)
            mintLastnoofmonths = Value
        End Set
    End Property

    'Public Property _Isinclude_Parttime() As Boolean
    '    Get
    '        Return mblnIsinclude_Parttime
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsinclude_Parttime = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property


    'Pinkal (14-Apr-2015) -- Start
    'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

    ''' <summary>
    ''' Purpose: Get or Set isshowoness
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsShowonESS() As Boolean
        Get
            Return mblnIsShowOnESS
        End Get
        Set(ByVal value As Boolean)
            mblnIsShowOnESS = value
        End Set
    End Property

    'Pinkal (14-Apr-2015) -- End

     'Nilay (19-Oct-2016) -- Start
    'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
    Public Property _IsShowLoanBalOnPayslip() As Boolean
        Get
            Return mblnShowLoanBalOnPayslip
        End Get
        Set(ByVal value As Boolean)
            mblnShowLoanBalOnPayslip = value
        End Set
    End Property
    'Nilay (19-Oct-2016) -- End

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 52
    Public Property _MaxLoanAmountLimit() As Decimal
        Get
            Return mdecMaxLoanAmountLimit
        End Get
        Set(ByVal value As Decimal)
            mdecMaxLoanAmountLimit = value
        End Set
    End Property
    'S.SANDEEP [20-SEP-2017] -- END

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Public Property _LoanCalcTypeId() As Integer
        Get
            Return mintLoanCalcTypeId
        End Get
        Set(ByVal value As Integer)
            mintLoanCalcTypeId = value
        End Set
    End Property
    Public Property _InterestCalctypeId() As Integer
        Get
            Return mintInterestCalctypeId
        End Get
        Set(ByVal value As Integer)
            mintInterestCalctypeId = value
        End Set
    End Property
    Public Property _InterestRate() As Decimal
        Get
            Return mdecInterestRate
        End Get
        Set(ByVal value As Decimal)
            mdecInterestRate = value
        End Set
    End Property
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (22 Sep 2017) -- Start
    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
    Public Property _EMI_NetPayPercentage() As Decimal
        Get
            Return mdecEmi_netpaypercentage
        End Get
        Set(ByVal value As Decimal)
            mdecEmi_netpaypercentage = value
        End Set
    End Property
    'Sohail (22 Sep 2017) -- End


    'Varsha (25 Nov 2017) -- Start
    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
    Public Property _MaxNoOfInstallment() As Integer
        Get
            Return mintMaxNoOfInstallment
        End Get
        Set(ByVal value As Integer)
            mintMaxNoOfInstallment = value
        End Set
    End Property

    'Varsha (25 Nov 2017) -- End

    'Sohail (02 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
    Public Property _RefLoanSchemeUnkid() As Integer
        Get
            Return mintRefLoanSchemeUnkid
        End Get
        Set(ByVal value As Integer)
            mintRefLoanSchemeUnkid = value
        End Set
    End Property
    'Sohail (02 Apr 2018) -- End

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Public Property _TranheadUnkid() As Integer
        Get
            Return mintTranheadUnkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadUnkid = value
        End Set
    End Property
    'Sohail (11 Apr 2018) -- End

    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = value
        End Set
    End Property
    'Sohail (14 Mar 2019) -- End

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Public Property _Mapped_TranheadUnkid() As Integer
        Get
            Return mintMapped_TranheadUnkid
        End Get
        Set(ByVal value As Integer)
            mintMapped_TranheadUnkid = value
        End Set
    End Property
    'Sohail (29 Apr 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            'strQ = "SELECT " & _
            '   "  loanschemeunkid " & _
            '   ", code " & _
            '   ", name " & _
            '   ", description " & _
            '   ", minnetsalary " & _
            '   ", isactive " & _
            '  "FROM lnloan_scheme_master " & _
            '  "WHERE loanschemeunkid = @loanschemeunkid "

            strQ = "SELECT " & _
             "  loanschemeunkid " & _
             ", code " & _
             ", name " & _
             ", description " & _
             ", minnetsalary " & _
             ", isactive " & _
             ", isshowoness " & _
             ", isshowloanbalonpayslip " & _
             ", ISNULL(maxloanamount,0) AS maxloanamount " & _
             ", ISNULL(interest_rate,0) AS interest_rate " & _
             ", ISNULL(loancalctype_id,0) AS loancalctype_id " & _
             ", ISNULL(interest_calctype_id,0) AS interest_calctype_id " & _
             ", ISNULL(emi_netpaypercentage, 0) AS emi_netpaypercentage " & _
             ", ISNULL(maxnoofinstallment, 0) AS maxnoofinstallment " & _
             ", ISNULL(refloanschemeunkid, 0) AS refloanschemeunkid " & _
             ", ISNULL(tranheadunkid, 0) AS tranheadunkid " & _
             ", ISNULL(costcenterunkid, 0) AS costcenterunkid " & _
             ", ISNULL(mapped_tranheadunkid, 0) AS mapped_tranheadunkid " & _
            "FROM lnloan_scheme_master " & _
            "WHERE loanschemeunkid = @loanschemeunkid "
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'Sohail (11 Apr 2018) - [tranheadunkid]
            'Sohail (02 Apr 2018) - [refloanschemeunkid]
            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52, maxloanamount} -- END
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id} -- END

            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString

                'Anjan (11 May 2011)-Start
                'mdecMinnetsalary = CDbl(dtRow.Item("minnetsalary"))
                mdecMinnetsalary = CDec(dtRow.Item("minnetsalary"))
                'Anjan (11 May 2011)-End 

                'mblnIsinclude_Parttime = CBool(dtRow.Item("isinclude_parttime"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'Pinkal (14-Apr-2015) -- Start
                'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
                mblnIsShowOnESS = CBool(dtRow.Item("isshowoness"))
                'Pinkal (14-Apr-2015) -- End

                'Nilay (19-Oct-2016) -- Start
                'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
                mblnShowLoanBalOnPayslip = CBool(dtRow.Item("isshowloanbalonpayslip"))
                'Nilay (19-Oct-2016) -- End

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 52
                mdecMaxLoanAmountLimit = Convert.ToDecimal(dtRow.Item("maxloanamount"))
                'S.SANDEEP [20-SEP-2017] -- END

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                mintLoanCalcTypeId = Convert.ToInt32(dtRow.Item("loancalctype_id"))
                mintInterestCalctypeId = Convert.ToInt32(dtRow.Item("interest_calctype_id"))
                mdecInterestRate = Convert.ToDecimal(dtRow.Item("interest_rate"))
                'S.SANDEEP [20-SEP-2017] -- END
                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdecEmi_netpaypercentage = Convert.ToDecimal(dtRow.Item("emi_netpaypercentage"))
                'Sohail (22 Sep 2017) -- End

                'Varsha (25 Nov 2017) -- Start
                'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                mintMaxNoOfInstallment = Convert.ToInt32(dtRow.Item("maxnoofinstallment"))
                'Varsha (25 Nov 2017) -- End

                'Sohail (02 Apr 2018) -- Start
                'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
                mintRefLoanSchemeUnkid = Convert.ToInt32(dtRow.Item("refloanschemeunkid"))
                'Sohail (02 Apr 2018) -- End
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                mintTranheadUnkid = Convert.ToInt32(dtRow.Item("tranheadunkid"))
                'Sohail (11 Apr 2018) -- End
                'Sohail (14 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                mintCostcenterunkid = Convert.ToInt32(dtRow.Item("costcenterunkid"))
                'Sohail (14 Mar 2019) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                mintMapped_TranheadUnkid = Convert.ToInt32(dtRow.Item("mapped_tranheadunkid"))
                'Sohail (29 Apr 2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnAddLoanInterestScheme As Boolean = False) As DataSet
        'Sohail (02 Apr 2018) - [blnAddLoanInterestScheme]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objLoan As New clsLoan_Advance 'Sohail (29 Apr 2019)

        objDataOperation = New clsDataOperation

        Try

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            Dim dsLoanCalc As DataSet = objLoan.GetLoanCalculationTypeList("List", True, True, objDataOperation)
            Dim dicLoanCalc As Dictionary(Of Integer, String) = (From p In dsLoanCalc.Tables(0) Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("Name").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)

            Dim dsInterestCalc As DataSet = objLoan.GetLoan_Interest_Calculation_Type("List", True, True, True, True, objDataOperation)
            Dim dicInterestCalc As Dictionary(Of Integer, String) = (From p In dsInterestCalc.Tables("List") Select New With {.Id = CInt(p.Item("Id")), .Name = p.Item("NAME").ToString}).ToDictionary(Function(x) x.Id, Function(x) x.Name)
            'Sohail (29 Apr 2019) -- End


            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            'strQ = "SELECT " & _
            '  "  loanschemeunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", description " & _
            '  ", minnetsalary " & _
            '  ", isactive " & _
            '  "FROM lnloan_scheme_master "
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ' strQ = "SELECT " & _
            '"  loanschemeunkid " & _
            '", code " & _
            '", name " & _
            '", description " & _
            '", minnetsalary " & _
            '", isactive " & _
            '", isshowoness " & _
            '            ", isshowloanbalonpayslip " & _
            '         ", ISNULL(maxloanamount,0) AS maxloanamount " & _
            '         ", ISNULL(loancalctype_id,0) AS loancalctype_id " & _
            '         ", ISNULL(interest_rate,0) AS interest_rate " & _
            '         ", ISNULL(interest_calctype_id,0) AS interest_calctype_id " & _
            '         ", CASE WHEN loancalctype_id = " & enLoanCalcId.Simple_Interest & " THEN @Simple_Interest " & _
            '         "       WHEN loancalctype_id = " & enLoanCalcId.Reducing_Amount & " THEN @Reducing_Amount " & _
            '         "       WHEN loancalctype_id = " & enLoanCalcId.No_Interest & " THEN @No_Interest " & _
            '         "       WHEN loancalctype_id = " & enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI & " THEN @Reducing_Balance_With_Fixed_Pricipal_EMI " & _
            '         "   END AS loancalctype " & _
            '         ", CASE WHEN interest_calctype_id = " & enLoanInterestCalcType.DAILY & " THEN @DAILY " & _
            '         "       WHEN interest_calctype_id = " & enLoanInterestCalcType.MONTHLY & " THEN @MONTHLY " & _
            '         "       WHEN interest_calctype_id = " & enLoanInterestCalcType.BY_TENURE & " THEN @BY_TENURE " & _
            '         "       WHEN interest_calctype_id = " & enLoanInterestCalcType.SIMPLE_INTEREST & " THEN @SINTEREST " & _
            '         "   END AS intercalctype " & _
            '         ", ISNULL(emi_netpaypercentage, 0) AS emi_netpaypercentage " & _
            '         ", ISNULL(maxnoofinstallment, 0) AS maxnoofinstallment " & _
            '         ", ISNULL(refloanschemeunkid, 0) AS refloanschemeunkid " & _
            '         ", ISNULL(tranheadunkid, 0) AS tranheadunkid " & _
            '         ", ISNULL(costcenterunkid, 0) AS tranheadunkid " & _
            '"FROM lnloan_scheme_master " & _
            '"WHERE 1 = 1 "
            strQ = "SELECT " & _
           "  loanschemeunkid " & _
           ", code " & _
           ", name " & _
           ", description " & _
           ", minnetsalary " & _
           ", isactive " & _
           ", isshowoness " & _
                       ", isshowloanbalonpayslip " & _
                    ", ISNULL(maxloanamount,0) AS maxloanamount " & _
                    ", ISNULL(loancalctype_id,0) AS loancalctype_id " & _
                    ", ISNULL(interest_rate,0) AS interest_rate " & _
                    ", ISNULL(interest_calctype_id,0) AS interest_calctype_id " & _
                    ", ISNULL(emi_netpaypercentage, 0) AS emi_netpaypercentage " & _
                    ", ISNULL(maxnoofinstallment, 0) AS maxnoofinstallment " & _
                    ", ISNULL(refloanschemeunkid, 0) AS refloanschemeunkid " & _
                    ", ISNULL(tranheadunkid, 0) AS tranheadunkid " & _
                    ", ISNULL(costcenterunkid, 0) AS tranheadunkid " & _
                        ", ISNULL(mapped_tranheadunkid, 0) AS mapped_tranheadunkid "

            strQ &= "       ,CASE lnloan_scheme_master.loancalctype_id "
            For Each pair In dicLoanCalc
                strQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            strQ &= "         END AS loancalctype "

            strQ &= "       ,CASE lnloan_scheme_master.interest_calctype_id "
            For Each pair In dicInterestCalc
                strQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            strQ &= "         END AS intercalctype "

            strQ &= "FROM lnloan_scheme_master " & _
           "WHERE 1 = 1 "
            'Sohail (29 Apr 2019) -- End
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'Sohail (11 Apr 2018) - [tranheadunkid]
            'Sohail (02 Apr 2018) - [refloanschemeunkid, WHERE 1 = 1 ]
            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52,maxloanamount} -- END

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ''S.SANDEEP [20-SEP-2017] -- START
            ''ISSUE/ENHANCEMENT : {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id}
            'objDataOperation.AddParameter("@Simple_Interest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 14, "Simple Interest"))
            'objDataOperation.AddParameter("@Reducing_Amount", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 15, "Reducing Balance"))
            'objDataOperation.AddParameter("@No_Interest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 17, "No Interest"))
            'objDataOperation.AddParameter("@Reducing_Balance_With_Fixed_Pricipal_EMI", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 18, "Reducing Balance With Fixed Pricipal EMI"))

            'objDataOperation.AddParameter("@DAILY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 20, "Daily"))
            'objDataOperation.AddParameter("@MONTHLY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 21, "Monthly"))
            'objDataOperation.AddParameter("@BY_TENURE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 24, "By Tenure"))
            'objDataOperation.AddParameter("@SINTEREST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 24, "Simple Interest"))
            ''S.SANDEEP [20-SEP-2017] -- END
            'Sohail (29 Apr 2019) -- End

            If blnOnlyActive Then
                'Sohail (02 Apr 2018) -- Start
                'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
                'strQ &= " WHERE isactive = 1 "
                strQ &= " AND isactive = 1 "
                'Sohail (02 Apr 2018) -- End
            End If

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If blnAddLoanInterestScheme = False Then
                strQ &= " AND ISNULL(lnloan_scheme_master.refloanschemeunkid, 0) <= 0 "
            End If
            'Sohail (02 Apr 2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_scheme_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrName, ) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme is already defined. Please define new Loan Scheme.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)

            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@minnetsalary", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecMinnetsalary.ToString)
            objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
            'Anjan (11 May 2011)-End 


            'objDataOperation.AddParameter("@isinclude_parttime", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsinclude_Parttime.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            



            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowOnESS.ToString)

            'Nilay (19-Oct-2016) -- Start
            'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
            objDataOperation.AddParameter("@isshowloanbalonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowLoanBalOnPayslip.ToString)
            'Nilay (19-Oct-2016) -- End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 52
            objDataOperation.AddParameter("@maxloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxLoanAmountLimit)
            'S.SANDEEP [20-SEP-2017] -- END

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            objDataOperation.AddParameter("@loancalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanCalcTypeId)
            objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterestCalctypeId)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestRate)
            'S.SANDEEP [20-SEP-2017] -- END
            objDataOperation.AddParameter("@emi_netpaypercentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_netpaypercentage) 'Sohail (22 Sep 2017)

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            objDataOperation.AddParameter("@maxnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNoOfInstallment)
            'Varsha (25 Nov 2017) -- End

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRefLoanSchemeUnkid)
            'Sohail (02 Apr 2018) -- End
            'Sohail (11 Apr 2018) -- Start
            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadUnkid)
            'Sohail (11 Apr 2018) -- End
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
            'Sohail (14 Mar 2019) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
            'Sohail (29 Apr 2019) -- End

            'strQ = "INSERT INTO lnloan_scheme_master ( " & _
            '   "  code " & _
            '   ", name " & _
            '   ", description " & _
            '   ", minnetsalary " & _
            '   ", isactive " & _
            ' ") VALUES (" & _
            '   "  @code " & _
            '   ", @name " & _
            '   ", @description " & _
            '   ", @minnetsalary " & _
            '   ", @isactive " & _
            ' "); SELECT @@identity"


            strQ = "INSERT INTO lnloan_scheme_master ( " & _
            "  code " & _
            ", name " & _
            ", description " & _
            ", minnetsalary " & _
            ", isactive " & _
            ", isshowoness " & _
                        ", isshowloanbalonpayslip " & _
            ", maxloanamount " & _
            ", loancalctype_id " & _
            ", interest_rate " & _
            ", interest_calctype_id " & _
            ", emi_netpaypercentage " & _
            ", maxnoofinstallment " & _
            ", refloanschemeunkid " & _
            ", tranheadunkid " & _
            ", costcenterunkid " & _
            ", mapped_tranheadunkid " & _
          ") VALUES (" & _
            "  @code " & _
            ", @name " & _
            ", @description " & _
            ", @minnetsalary " & _
            ", @isactive " & _
            ", @isshowoness " & _
                        ", @isshowloanbalonpayslip " & _
            ", @maxloanamount " & _
            ", @loancalctype_id " & _
            ", @interest_rate " & _
            ", @interest_calctype_id " & _
            ", @emi_netpaypercentage " & _
            ", @maxnoofinstallment " & _
            ", @refloanschemeunkid " & _
            ", @tranheadunkid " & _
            ", @costcenterunkid " & _
            ", @mapped_tranheadunkid " & _
          "); SELECT @@identity"
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'Sohail (11 Apr 2018) - [tranheadunkid]
            'Sohail (02 Apr 2018) - [refloanschemeunkid]
            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52,maxloanamount} -- END
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoanschemeunkid = dsList.Tables(0).Rows(0).Item(0)

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lnloan_scheme_master", "loanschemeunkid", mintLoanschemeunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If mintRefLoanSchemeUnkid <= 0 Then
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest"))
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest"))
                objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
                objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
                objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowOnESS.ToString)
                objDataOperation.AddParameter("@isshowloanbalonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowLoanBalOnPayslip.ToString)
                objDataOperation.AddParameter("@maxloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxLoanAmountLimit)
                objDataOperation.AddParameter("@loancalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanCalcTypeId)
                objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterestCalctypeId)
                objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestRate)
                objDataOperation.AddParameter("@emi_netpaypercentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_netpaypercentage)
                objDataOperation.AddParameter("@maxnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNoOfInstallment)
                objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid)
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadUnkid)
                'Sohail (11 Apr 2018) -- End
                'Sohail (14 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
                'Sohail (14 Mar 2019) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
                'Sohail (29 Apr 2019) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim intUnkId As Integer = dsList.Tables(0).Rows(0).Item(0)

                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lnloan_scheme_master", "loanschemeunkid", intUnkId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Sohail (02 Apr 2018) -- End

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 

            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_scheme_master) </purpose>
    Public Function Update() As Boolean
        If isExist(, mstrName, mintLoanschemeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme is already defined. Please define new Loan Scheme.")
            Return False
        End If


        If isExist(mstrCode, , mintLoanschemeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Code is already defined. Please define new Code.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloanschemeunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)


            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@minnetsalary", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecMinnetsalary.ToString)
            objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
            'Anjan (11 May 2011)-End 



            'objDataOperation.AddParameter("@isinclude_parttime", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsinclude_Parttime.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)


            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowOnESS.ToString)

            'Nilay (19-Oct-2016) -- Start
            'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
            objDataOperation.AddParameter("@isshowloanbalonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowLoanBalOnPayslip.ToString)
            'Nilay (19-Oct-2016) -- End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 52
            objDataOperation.AddParameter("@maxloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxLoanAmountLimit)
            'S.SANDEEP [20-SEP-2017] -- END

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            objDataOperation.AddParameter("@loancalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanCalcTypeId)
            objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterestCalctypeId)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestRate)
            'S.SANDEEP [20-SEP-2017] -- END
            objDataOperation.AddParameter("@emi_netpaypercentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_netpaypercentage) 'Sohail (22 Sep 2017)

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            objDataOperation.AddParameter("@maxnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNoOfInstallment)
            'Varsha (25 Nov 2017) -- End

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRefLoanSchemeUnkid)
            'Sohail (02 Apr 2018) -- End
            'Sohail (11 Apr 2018) -- Start
            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadUnkid)
            'Sohail (11 Apr 2018) -- End
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
            'Sohail (14 Mar 2019) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
            'Sohail (29 Apr 2019) -- End

            'strQ = "UPDATE lnloan_scheme_master SET " & _
            '  "  code = @code " & _
            '  ", name = @name " & _
            '  ", description = @description " & _
            '  ", minnetsalary = @minnetsalary " & _
            '  ", isactive = @isactive " & _
            ' "WHERE loanschemeunkid = @loanschemeunkid "

            strQ = "UPDATE lnloan_scheme_master SET " & _
                     "  code = @code " & _
                     ", name = @name " & _
                     ", description = @description " & _
                     ", minnetsalary = @minnetsalary " & _
                     ", isactive = @isactive " & _
                     ", isshowoness = @isshowoness " & _
                     ", isshowloanbalonpayslip = @isshowloanbalonpayslip " & _
                     ", maxloanamount = @maxloanamount " & _
                     ", loancalctype_id = @loancalctype_id " & _
                     ", interest_rate = @interest_rate " & _
                     ", interest_calctype_id = @interest_calctype_id " & _
                     ", emi_netpaypercentage = @emi_netpaypercentage " & _
                     ", maxnoofinstallment = @maxnoofinstallment " & _
                     ", refloanschemeunkid = @refloanschemeunkid " & _
                     ", tranheadunkid = @tranheadunkid " & _
                     ", costcenterunkid = @costcenterunkid " & _
                     ", mapped_tranheadunkid = @mapped_tranheadunkid " & _
                    "WHERE loanschemeunkid = @loanschemeunkid "
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'Sohail (11 Apr 2018) - [tranheadunkid]
            'Sohail (02 Apr 2018) - [refloanschemeunkid]
            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52,maxloanamount} -- END
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lnloan_scheme_master", mintLoanschemeunkid, "loanschemeunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloan_scheme_master", "loanschemeunkid", mintLoanschemeunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If mintRefLoanSchemeUnkid <= 0 Then
                Dim intInterestLoanUnkId As Integer = GetLoanSchemeInterestID(mintLoanschemeunkid, objDataOperation)
                If intInterestLoanUnkId > 0 Then
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInterestLoanUnkId.ToString)
                    objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest"))
                    objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest"))
                    objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
                    objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
                    objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
                    objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowOnESS.ToString)
                    objDataOperation.AddParameter("@isshowloanbalonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowLoanBalOnPayslip.ToString)
                    objDataOperation.AddParameter("@maxloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxLoanAmountLimit)
                    objDataOperation.AddParameter("@loancalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanCalcTypeId)
                    objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterestCalctypeId)
                    objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestRate)
                    objDataOperation.AddParameter("@emi_netpaypercentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_netpaypercentage)
                    objDataOperation.AddParameter("@maxnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNoOfInstallment)
                    objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid)
                    'Sohail (11 Apr 2018) -- Start
                    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadUnkid)
                    'Sohail (11 Apr 2018) -- End
                    'Sohail (14 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
                    'Sohail (14 Mar 2019) -- End
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
                    'Sohail (29 Apr 2019) -- End

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lnloan_scheme_master", intInterestLoanUnkId, "loanschemeunkid", 2) Then
                        If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloan_scheme_master", "loanschemeunkid", intInterestLoanUnkId) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                Else
                    objDataOperation.ClearParameters()
                    mstrCode = mstrCode.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest")
                    mstrName = mstrName.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest")
                    mintRefLoanSchemeUnkid = mintLoanschemeunkid

                    If Insert() = False Then
                        Return False
                    End If
                End If
            End If
            'Sohail (02 Apr 2018) -- End

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_scheme_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 

        Try
            'strQ = "DELETE FROM lnloan_scheme_master " & _
            '"WHERE loanschemeunkid = @loanschemeunkid "

            strQ = "UPDATE lnloan_scheme_master " & _
                   " SET isactive = 0 " & _
                   "WHERE loanschemeunkid = @loanschemeunkid "

            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_scheme_master", "loanschemeunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If mintRefLoanSchemeUnkid <= 0 Then
                Dim intInterestLoanUnkId As Integer = GetLoanSchemeInterestID(mintLoanschemeunkid, objDataOperation)
                If intInterestLoanUnkId > 0 Then
                    objDataOperation.ClearParameters()

                    strQ = "UPDATE lnloan_scheme_master " & _
                          " SET isactive = 0 " & _
                          "WHERE loanschemeunkid = @loanschemeunkid "

                    objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInterestLoanUnkId)

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_scheme_master", "loanschemeunkid", intInterestLoanUnkId) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Sohail (02 Apr 2018) -- End

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 

            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "TABLE_NAME AS TableName " & _
                    ",COLUMN_NAME " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('loanschemeunkid') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "lnloan_scheme_master" Then Continue For
                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @loanschemeunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            'Sohail (14 Nov 2011) -- Start
            If blnIsUsed = False Then
                strQ = "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.LOAN & " " & _
                            "AND tranheadunkid = @loanschemeunkid " & _
                    "UNION " & _
                    "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration_employee " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.LOAN & " " & _
                            "AND tranheadunkid = @loanschemeunkid " & _
                    "UNION " & _
                    "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration_costcenter " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.LOAN & " " & _
                            "AND tranheadunkid = @loanschemeunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                blnIsUsed = dsList.Tables("List").Rows.Count > 0
            End If
            'Sohail (14 Nov 2011) -- End

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByRef intId As Integer = 0) As Boolean
        'Sohail (02 Aug 2017) - [intId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            intId = 0 'Sohail (02 Aug 2017)

            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            'strQ = "SELECT " & _
            '   "  loanschemeunkid " & _
            '   ", code " & _
            '   ", name " & _
            '   ", description " & _
            '   ", minnetsalary " & _
            '   ", isactive " & _
            ' "FROM lnloan_scheme_master " & _
            '  "WHERE 1 = 1"

            strQ = "SELECT " & _
                       "  loanschemeunkid " & _
                       ", code " & _
                       ", name " & _
                       ", description " & _
                       ", minnetsalary " & _
                       ", isactive " & _
                       ", isshowoness " & _
                       ", isshowloanbalonpayslip " & _
                       ", ISNULL(maxloanamount,0) AS maxloanamount " & _
                       ", ISNULL(loancalctype_id,0) AS loancalctype_id " & _
                       ", ISNULL(interest_rate,0) AS interest_rate " & _
                       ", ISNULL(interest_calctype_id,0) AS interest_calctype_id " & _
                       ", ISNULL(emi_netpaypercentage,0) AS emi_netpaypercentage " & _
                       ", ISNULL(maxnoofinstallment,0) AS maxnoofinstallment " & _
                     "FROM lnloan_scheme_master " & _
                      "WHERE 1 = 1"

            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52,maxloanamount} -- END
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id} -- END

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.Length > 0 Then
                strQ &= " AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND loanschemeunkid <> @loanschemeunkid"
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            If dsList.Tables(0).Rows.Count > 0 Then
                intId = CInt(dsList.Tables(0).Rows(0).Item("loanschemeunkid"))
            End If
            'Sohail (02 Aug 2017) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    ''' <summary>
    '''  Modify By: Anjan
    ''' </summary>
    ''' <param name="blnNA"></param>
    ''' <param name="strListName"></param>
    ''' <param name="intLanguageID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComboList(Optional ByVal blnNA As Boolean = False, _
                                    Optional ByVal strListName As String = "List", _
                                      Optional ByVal intLanguageID As Integer = -1, _
                                                Optional ByVal strFilter As String = "", _
                                                Optional ByVal blnSchemeshowOnEss As Boolean = False, _
                                                Optional ByVal blnAddLoanInterestScheme As Boolean = False, _
                                 Optional ByVal strOrderByQuery As String = "") As DataSet
        'Hemant (23 May 2019) -- [strOrderByQuery] 
        'Sohail (02 Apr 2018) - [blnAddLoanInterestScheme]
        'Sohail (03 Feb 2016) - [strFilter]

        'Pinkal (07-Dec-2017) --  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application[Optional ByVal blnSchemeshowOnEss As Boolean = False]

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try

            If blnNA Then
                strQ = "SELECT 0 AS loanschemeunkid " & _
                          ",  ' ' + @Select AS name " & _
                          ",  ' ' as Code " & _
                       "UNION "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            End If

            strQ &= "SELECT loanschemeunkid " & _
                        ", name AS name " & _
                        ", code as Code " & _
                     "FROM lnloan_scheme_master " & _
                     "WHERE isactive = 1 "




            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            If blnSchemeshowOnEss Then
                strQ &= " AND isshowoness = 1"
            End If

            'Pinkal (07-Dec-2017) -- End

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If blnAddLoanInterestScheme = False Then
                strQ &= " AND ISNULL(lnloan_scheme_master.refloanschemeunkid, 0) <= 0 "
            End If
            'Sohail (02 Apr 2018) -- End

            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'strQ &= "ORDER BY  name "
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Hemant (23 May 2019) -- Start
            'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
            'strQ &= " ORDER BY  name "
            If strOrderByQuery.Trim <> "" Then
                strQ &= strOrderByQuery
            Else
            strQ &= " ORDER BY  name "
            End If
            'Hemant (23 May 2019) -- End
            'Sohail (03 Feb 2016) -- End


            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    'Sohail (14 Nov 2011) -- Start
    Public Function GetLoanSchemeIDUsedInJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_employee" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_costcenter" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.LOAN)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    Public Function GetLoanSchemeIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration_costcenter"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_costcenter"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.LOAN)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function
    'Sohail (14 Nov 2011) -- End


    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLoanSchemeUnkid(ByVal mstrName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = " SELECT " & _
                      "  loanschemeunkid " & _
                      "  FROM lnloan_scheme_master " & _
                      " WHERE name = @name  AND isactive = 1 "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("loanschemeunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 29 DEC 2011 ] -- END

    'Sohail (02 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
    Public Function GetLoanSchemeInterestID(ByVal intLoanSchemeUnkid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intUnkId As Integer = -1
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                "  loanschemeunkid " & _
                " FROM lnloan_scheme_master " & _
                " WHERE isactive = 1 " & _
                " AND refloanschemeunkid = @refloanschemeunkid"

            objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanSchemeUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("loanschemeunkid").ToString)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeInterestID; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function
    'Sohail (02 Apr 2018) -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Loan Scheme is already defined. Please define new Loan Scheme.")
			Language.setMessage(mstrModuleName, 2, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
