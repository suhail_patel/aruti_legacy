﻿'************************************************************************************************************************************
'Class Name : clsProcess_pending_loan.vb
'Purpose    :
'Date       :23/4/2015
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System
Imports System.Web
Imports System.Threading 'Nilay (27-Dec-2016)

''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala.
''' </summary>
Public Class clsProcess_pending_loan
    Private Shared ReadOnly mstrModuleName As String = "clsProcess_pending_loan"
    Dim objDataOperation As clsDataOperation
    Dim objLoanAdvance As clsLoan_Advance
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintProcesspendingloanunkid As Integer
    Private mstrApplication_No As String = String.Empty
    Private mdtApplication_Date As Date
    Private mintEmployeeunkid As Integer
    Private mintLoanschemeunkid As Integer
    Private mdecLoan_Amount As Decimal
    Private mintCountryunkid As Integer = 0
    Private mintApproverunkid As Integer
    Private mintLoan_Statusunkid As Integer
    Private mdecApproved_Amount As Decimal
    Private mintLoanAdvanceId As Integer
    Private mdecInterest_Amount As Decimal
    Private mdecNet_Amount As Decimal
    Private mblnIsAdvance As Boolean
    Private mblnIsexternal_Entity As Boolean
    Private mstrExternal_Entity_Name As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private mstrEmp_Remark As String = String.Empty
    Private mintDurationInMonths As Integer = 0
    Private mintDeductionPeriodunkid As Integer = 0
    Private mdecInstallmentAmt As Decimal = 0
    Private mintNoofInstallment As Integer = 0
    Private mblnIsLoanApprover_ForLoanScheme As Boolean = False 'Nilay (15-Dec-2015) -- Private mblnIsLoanApprover_ForLoanScheme As Boolean = ConfigParameter._Object._IsLoanApprover_ForLoanScheme
    Private mblnIsloan As Boolean
    Private mblnIsvoid As Boolean
    Private mintUserunkid As Integer
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintLoginemployeeunkid As Integer = -1
    Private mintVoidloginemployeeunkid As Integer = -1
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private objLoanApproval As New clsloanapproval_process_Tran
    'Shani(26-Nov-2015) -- Start
    'ENHANCEMENT : Add Loan Import Form
    Private mblnIsImportedLoan As Boolean = False
    'Shani(26-Nov-2015) -- End

    'Nilay (08-Dec-2016) -- Start
    'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
    Private mintMinApprovedPriority As Integer = -1
    'Nilay (08-Dec-2016) -- End

    'Nilay (27-Dec-2016) -- Start
    'OPTIMIZATION: Sending email notification by Threading
    Private objThread As Thread
    'Nilay (27-Dec-2016) -- End

    'Nilay (01 Feb 2017) -- Start
    'Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
    Dim objEmailList As New List(Of clsEmailCollection)
    'Nilay (01 Feb 2017) -- End

    'Hemant (02 Jan 2019) -- Start
    'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
    Private mstrLoan_Account_No As String = String.Empty
    'Hemant (02 Jan 2019) -- End


#End Region


    'Shani (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Public Enum enNoticationLoanStatus
        APPROVE = 1
        REJECT = 2
        ASSIGN = 3
        'Nilay (23-Aug-2016) -- Start
        'Enhancement - Create New Loan Notification 
        DELETE_ASSIGNED = 4
        'Nilay (23-Aug-2016) -- End

        'Nilay (20-Sept-2016) -- Start
        'Enhancement : Cancel feature for approved but not assigned loan application
        CANCELLED = 5
        'Nilay (20-Sept-2016) -- End

    End Enum

    Public Enum enApproverEmailType
        Loan_Approver = 1
        Loan_Rate = 2
        Loan_Installment = 3
        Loan_Topup = 4
        Loan_Advance = 5
    End Enum
    'Shani (21-Jul-2016) -- End

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processpendingloanunkid() As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set application_no
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Application_No() As String
        Get
            Return mstrApplication_No
        End Get
        Set(ByVal value As String)
            mstrApplication_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set application_date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Application_Date() As Date
        Get
            Return mdtApplication_Date
        End Get
        Set(ByVal value As Date)
            mdtApplication_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loan_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecLoan_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecLoan_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loan_Statusunkid() As Integer
        Get
            Return mintLoan_Statusunkid
        End Get
        Set(ByVal value As Integer)
            mintLoan_Statusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approved_amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approved_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecApproved_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecApproved_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LoanAdvanceId() As Integer
        Get
            Return mintLoanAdvanceId
        End Get
        Set(ByVal value As Integer)
            mintLoanAdvanceId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Interest_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecInterest_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecInterest_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Net_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecNet_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecNet_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsAdvance() As Boolean
        Get
            Return mblnIsAdvance
        End Get
        Set(ByVal value As Boolean)
            mblnIsAdvance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isexternal_entity
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isexternal_Entity() As Boolean
        Get
            Return mblnIsexternal_Entity
        End Get
        Set(ByVal value As Boolean)
            mblnIsexternal_Entity = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set external_entity_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _External_Entity_Name() As String
        Get
            Return mstrExternal_Entity_Name
        End Get
        Set(ByVal value As String)
            mstrExternal_Entity_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Emp_Remark() As String
        Get
            Return mstrEmp_Remark
        End Get
        Set(ByVal value As String)
            mstrEmp_Remark = value
        End Set
    End Property

    '' <summary>
    '' Purpose: Get or Set durationinmonths
    '' Modify By: Pinkal
    '' </summary>
    'Nilay (21-Oct-2015) -- Start
    'ENHANCEMENT : NEW LOAN Given By Rutta
    'Public Property _DurationInMonths() As Integer
    '    Get
    '        Return mintDurationInMonths
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintDurationInMonths = value
    '    End Set
    'End Property
    'Nilay (21-Oct-2015) -- End

    

    ''' <summary>
    ''' Purpose: Get or Set deductionperiodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeductionPeriodunkid() As Integer
        Get
            Return mintDeductionPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintDeductionPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set installmentamount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _InstallmentAmount() As Decimal
        Get
            Return mdecInstallmentAmt
        End Get
        Set(ByVal value As Decimal)
            mdecInstallmentAmt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set NoofInstallment
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _NoOfInstallment() As Integer
        Get
            Return mintNoofInstallment
        End Get
        Set(ByVal value As Integer)
            mintNoofInstallment = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsLoanApprover_ForLoanScheme
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsLoanApprover_ForLoanScheme() As Boolean
        Get
            Return mblnIsLoanApprover_ForLoanScheme
        End Get
        Set(ByVal value As Boolean)
            mblnIsLoanApprover_ForLoanScheme = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isloan
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isloan() As Boolean
        Get
            Return mblnIsloan
        End Get
        Set(ByVal value As Boolean)
            mblnIsloan = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    'Shani(26-Nov-2015) -- Start
    'ENHANCEMENT : Add Loan Import Form
    Public Property _IsImportedLoan() As Boolean
        Get
            Return mblnIsImportedLoan
        End Get
        Set(ByVal value As Boolean)
            mblnIsImportedLoan = value
        End Set
    End Property
    'Shani(26-Nov-2015) -- End

    'Nilay (08-Dec-2016) -- Start
    'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
    Public Property _MinApprovedPriority() As Integer
        Get
            Return mintMinApprovedPriority
        End Get
        Set(ByVal value As Integer)
            mintMinApprovedPriority = value
        End Set
    End Property
    'Nilay (08-Dec-2016) -- End

    'Hemant (02 Jan 2019) -- Start
    'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
    ''' <summary>
    ''' Purpose: Get or Set Loan_Account_No
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loan_Account_No() As String
        Get
            Return mstrLoan_Account_No
        End Get
        Set(ByVal value As String)
            mstrLoan_Account_No = value
        End Set
    End Property

    'Hemant (02 Jan 2019) -- End

    'Hemant (30 Aug 2019) -- Start
    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
    Private xDataOp As clsDataOperation
    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property
    'Hemant (30 Aug 2019) -- End	

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (30 Aug 2019) -- Start
        'ISSUE#0004110(ZURI) :  Error on global assigning loans..
        'Dim objDataOperation As New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (30 Aug 2019) -- End	

        Try

            strQ = "SELECT " & _
                      "  processpendingloanunkid " & _
                      ", application_no " & _
                      ", application_date " & _
                      ", employeeunkid " & _
                      ", loanschemeunkid " & _
                      ", loan_amount " & _
                      ", approverunkid " & _
                      ", loan_statusunkid " & _
                      ", approved_amount " & _
                      ", remark " & _
                      ", isloan " & _
                      ", isvoid " & _
                      ", userunkid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", ISNULL(isexternal_entity,0) As isexternal_entity " & _
                      ", ISNULL(external_entity_name,'') As external_entity_name " & _
                      ", ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
                      ", ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
                      ", ISNULL(emp_remark,'') AS emp_remark " & _
                      ", ISNULL(duration,0) AS durationinmonths " & _
                      ", ISNULL(deductionperiodunkid,0) AS deductionperiodunkid  " & _
                      ", ISNULL(installmentamt,0.00) AS installmentamt " & _
                      ", ISNULL(noofinstallment,0) AS  noofinstallment " & _
                      ", ISNULL(countryunkid,0) AS countryunkid  " & _
                      ", ISNULL(isimport,0) AS isimport  " & _
                      ", ISNULL(loan_account_no,'') As loan_account_no " & _
                      "FROM lnloan_process_pending_loan " & _
                      "WHERE processpendingloanunkid = @processpendingloanunkid "

            'Hemant (02 Jan 2019) - [loan_account_no]
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrApplication_No = dtRow.Item("application_no").ToString
                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mdecLoan_Amount = CDec(dtRow.Item("loan_amount"))
                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
                mintLoan_Statusunkid = CInt(dtRow.Item("loan_statusunkid"))
                mdecApproved_Amount = CDec(dtRow.Item("approved_amount"))
                mstrRemark = dtRow.Item("remark").ToString
                mblnIsloan = CBool(dtRow.Item("isloan"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                If IsDBNull(dtRow.Item("application_date")) Then
                    mdtApplication_Date = Nothing
                Else
                    mdtApplication_Date = dtRow.Item("application_date")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIsexternal_Entity = CBool(dtRow.Item("isexternal_entity"))
                mstrExternal_Entity_Name = dtRow.Item("external_entity_name").ToString
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                mstrEmp_Remark = dtRow.Item("emp_remark").ToString
                mintDurationInMonths = CInt(dtRow.Item("durationinmonths"))
                mintDeductionPeriodunkid = CInt(dtRow.Item("deductionperiodunkid"))
                mdecInstallmentAmt = CDec(dtRow.Item("installmentamt"))
                mintNoofInstallment = CInt(dtRow.Item("noofinstallment"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))

                'Shani(26-Nov-2015) -- Start
                'ENHANCEMENT : Add Loan Import Form
                mblnIsImportedLoan = CBool(dtRow.Item("isimport"))
                'Shani(26-Nov-2015) -- End

                'Nilay (08-Dec-2016) -- Start
                'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
                mintMinApprovedPriority = -1
                'Nilay (08-Dec-2016) -- End

                'Hemant (02 Jan 2019) -- Start
                'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
                mstrLoan_Account_No = dtRow.Item("loan_account_no").ToString
                'Hemant (02 Jan 2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (30 Aug 2019) -- Start
            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (30 Aug 2019) -- End
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal intStatusID As Integer = 0, _
                            Optional ByVal mstrFilter As String = "", _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal IsUsedAsMSS As Boolean = True) As DataSet
        'Sohail (09 Apr 2016) -- [IsUsedAsMSS]
        'Sohail (06 Jan 2016) - [blnApplyUserAccessFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String 'Nilay (20-Sept-2016)
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = "" 'Nilay (20-Sept-2016)
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Nilay (20-Sept-2016) -- End

            'Sohail (06 Jan 2016) -- Start
            'Enhancement - Show Close Year Process Logs on Close Year Wizard.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If blnApplyUserAccessFilter = True Then
                'Sohail (09 Apr 2016) -- Start
                'Enhancement : 58.1 changes in SS
                'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                'Sohail (09 Apr 2016) -- End
            End If
            'Sohail (06 Jan 2016) -- End

            strQ = "SELECT " & _
                      " lnloan_process_pending_loan.processpendingloanunkid " & _
                      ",lnloan_process_pending_loan.application_no As Application_No " & _
                      ",convert(char(8),application_date,112) As application_date " & _
                      ",application_date As applicationdate " & _
                      ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                      ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
                      ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
                      ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
                      ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.PENDING & " THEN @Pending " & _
                      "      WHEN lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.APPROVED & " THEN @Approved " & _
                      "      WHEN lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.REJECTED & " THEN @Rejected " & _
                      "      WHEN lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.ASSIGNED & " THEN @Assigned " & _
                      "      WHEN lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.CANCELLED & " THEN @Cancelled " & _
                      " END As LoanStatus " & _
                      ",lnloan_process_pending_loan.loan_statusunkid " & _
                      ",lnloan_process_pending_loan.employeeunkid " & _
                      ",lnloan_process_pending_loan.loanschemeunkid " & _
                      ",lnloan_process_pending_loan.isloan " & _
                      ",lnloan_process_pending_loan.loan_amount As Amount " & _
                      ",lnloan_process_pending_loan.loanschemeunkid " & _
                      ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
                      ",lnloan_process_pending_loan.approverunkid " & _
                      ",lnloan_process_pending_loan.remark As Remark " & _
                      ",lnloan_process_pending_loan.isexternal_entity " & _
                      ",lnloan_process_pending_loan.external_entity_name " & _
                                "	,ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
                                "	,ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
                     ",hremployee_master.employeecode AS EmpCode " & _
                     ",ISNULL(lnloan_advance_tran.balance_amount,0) AS balance " & _
                     ",CASE WHEN lnloan_advance_tran.loan_statusunkid = " & enLoanStatus.IN_PROGRESS & " THEN @InProgress " & _
                     "      WHEN lnloan_advance_tran.loan_statusunkid = " & enLoanStatus.ON_HOLD & " THEN @OnHold " & _
                     "      WHEN lnloan_advance_tran.loan_statusunkid = " & enLoanStatus.WRITTEN_OFF & " THEN @WrittenOff " & _
                     "      WHEN lnloan_advance_tran.loan_statusunkid = " & enLoanStatus.COMPLETED & " THEN @Completed " & _
                     "       ELSE ''  END As finalStatus " & _
                     ",lnloan_process_pending_loan.emp_remark " & _
                     ",ISNULL(lnloan_process_pending_loan.duration,0) AS duration " & _
                     ",ISNULL(lnloan_process_pending_loan.deductionperiodunkid,0) AS deductionperiodunkid  " & _
                     ",ISNULL(lnloan_process_pending_loan.installmentamt,0.00) AS installmentamt " & _
                     ",ISNULL(lnloan_process_pending_loan.noofinstallment,0) AS  noofinstallment " & _
                     ",ISNULL(lnloan_process_pending_loan.countryunkid,0) AS countryunkid  " & _
                     ",cSign " & _
                     ", lnloan_scheme_master.interest_rate " & _
                     ", lnloan_scheme_master.loancalctype_id " & _
                     ", lnloan_scheme_master.interest_calctype_id " & _
                     ", ISNULL(lnloan_process_pending_loan.application_no,'') As loan_account_no " & _
                     ", ISNULL(lnloan_scheme_master.mapped_tranheadunkid, 0) As mapped_tranheadunkid " & _
                     "FROM lnloan_process_pending_loan " & _
                     "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                     "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                     "LEFT JOIN lnloan_advance_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid AND ISNULL(lnloan_advance_tran.isvoid,0) = 0 " & _
                     "LEFT JOIN " & _
                     "( " & _
                     "	SELECT " & _
                     "		 countryunkid AS cTid " & _
                     "		,currency_name AS cSign " & _
                     "		,ROW_NUMBER()OVER(PARTITION BY countryunkid ORDER BY exchange_date DESC) AS rno " & _
                     "	FROM cfexchange_rate WHERE isactive = 1 " & _
                     ")AS Cur ON Cur.cTid = lnloan_process_pending_loan.countryunkid AND Cur.rno = 1 "
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Hemant (02 Jan 2019) - [loan_account_no]
            'Sohail (06 Jul 2018) - [interest_rate, loancalctype_id, interest_calctype_id]
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'ADD Cancelled Status in [LoanStatus]
            'ADD enum in [finalStatus]
            'Nilay (20-Sept-2016) -- End

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Nilay (20-Sept-2016) -- End

            strQ &= " WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If intStatusID > 0 Then
                strQ &= "AND lnloan_process_pending_loan.loan_statusunkid = @loan_statusunkid"
                objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID.ToString)
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))
            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Assigned"))
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            objDataOperation.AddParameter("@Cancelled", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 96, "Cancelled"))
            'Nilay (20-Sept-2016) -- End
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, Optional ByVal intStatusID As Integer = 0 _
    '                        , Optional ByVal strIncludeInactiveEmployee As String = "" _
    '                        , Optional ByVal strEmployeeAsOnDate As String = "" _
    '                        , Optional ByVal strUserAccessLevelFilterString As String = "" _
    '                        , Optional ByVal mstrFilter As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        strQ = "SELECT " & _
    '                  " lnloan_process_pending_loan.processpendingloanunkid " & _
    '                  ",lnloan_process_pending_loan.application_no As Application_No " & _
    '                  ",convert(char(8),application_date,112) As application_date " & _
    '                  ",application_date As applicationdate " & _
    '                  ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                  ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
    '                  ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
    '                  ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
    '                  ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending " & _
    '                  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved " & _
    '                  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected " & _
    '                  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 4 THEN @Assigned " & _
    '                  " END As LoanStatus " & _
    '                  ",lnloan_process_pending_loan.loan_statusunkid " & _
    '                  ",lnloan_process_pending_loan.employeeunkid " & _
    '                  ",lnloan_process_pending_loan.loanschemeunkid " & _
    '                  ",lnloan_process_pending_loan.isloan " & _
    '                  ",lnloan_process_pending_loan.loan_amount As Amount " & _
    '                  ",lnloan_process_pending_loan.loanschemeunkid " & _
    '                  ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
    '                  ",lnloan_process_pending_loan.approverunkid " & _
    '                  ",lnloan_process_pending_loan.remark As Remark " & _
    '                  ",lnloan_process_pending_loan.isexternal_entity " & _
    '                  ",lnloan_process_pending_loan.external_entity_name " & _
    '                            "	,ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
    '                            "	,ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
    '                 ",hremployee_master.employeecode AS EmpCode " & _
    '                 ",ISNULL(lnloan_advance_tran.balance_amount,0) AS balance " & _
    '                 ",CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
    '                 "       WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
    '                 "       WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
    '                 "       WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed " & _
    '                 "       ELSE ''  END As finalStatus " & _
    '                 ",lnloan_process_pending_loan.emp_remark " & _
    '                 ",ISNULL(lnloan_process_pending_loan.duration,0) AS duration " & _
    '                 ",ISNULL(lnloan_process_pending_loan.deductionperiodunkid,0) AS deductionperiodunkid  " & _
    '                 ",ISNULL(lnloan_process_pending_loan.installmentamt,0.00) AS installmentamt " & _
    '                 ",ISNULL(lnloan_process_pending_loan.noofinstallment,0) AS  noofinstallment " & _
    '                 ",ISNULL(lnloan_process_pending_loan.countryunkid,0) AS countryunkid  " & _
    '                 ",cSign " & _
    '                 "FROM lnloan_process_pending_loan " & _
    '                 "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
    '                 "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                 "LEFT JOIN lnloan_advance_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid AND ISNULL(lnloan_advance_tran.isvoid,0) = 0 " & _
    '                 "LEFT JOIN " & _
    '                 "( " & _
    '                 "	SELECT " & _
    '                 "		 countryunkid AS cTid " & _
    '                 "		,currency_name AS cSign " & _
    '                 "		,ROW_NUMBER()OVER(PARTITION BY countryunkid ORDER BY exchange_date DESC) AS rno " & _
    '                 "	FROM cfexchange_rate WHERE isactive = 1 " & _
    '                 ")AS Cur ON Cur.cTid = lnloan_process_pending_loan.countryunkid AND Cur.rno = 1 " & _
    '                 "WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "

    '        If intStatusID > 0 Then
    '            strQ &= "AND lnloan_process_pending_loan.loan_statusunkid = @loan_statusunkid"
    '            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID.ToString)
    '        End If

    '        If strIncludeInactiveEmployee = "" Then strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        If CBool(strIncludeInactiveEmployee) = False Then

    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If
    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If

    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrFilter
    '        End If

    '        objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
    '        objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))
    '        objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))
    '        objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Assigned"))
    '        objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
    '        objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
    '        objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
    '        objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (10-Oct-2015) -- End

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_process_pending_loan) </purpose>

    Public Function Insert(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal intLoanApplicationNoType As Integer, _
                           ByVal strLoanApplicationPrifix As String, _
                           Optional ByVal intNextLoanApplicationNo As Integer = 0) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        If intLoanApplicationNoType = 0 Then
            If isExist(mintEmployeeunkid, mblnIsloan, mstrApplication_No, mdtApplication_Date, mintLoanschemeunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
        End If

        Try
            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
            objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            If mdtApplication_Date = Nothing Then
                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isexternal_entity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal_Entity.ToString)
            objDataOperation.AddParameter("@external_entity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExternal_Entity_Name.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)

            objDataOperation.AddParameter("@duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDurationInMonths.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionPeriodunkid.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInstallmentAmt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofInstallment.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)

            'Shani(26-Nov-2015) -- Start
            'ENHANCEMENT : Add Loan Import Form
            objDataOperation.AddParameter("@isimport", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImportedLoan.ToString)
            'Shani(26-Nov-2015) -- End

            'Hemant (02 Jan 2019) -- Start
            'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
            objDataOperation.AddParameter("@loan_account_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Account_No.ToString)
            'Hemant (02 Jan 2019) -- End


            strQ = "INSERT INTO lnloan_process_pending_loan ( " & _
                      "  application_no " & _
                      ", application_date " & _
                      ", employeeunkid " & _
                      ", loanschemeunkid " & _
                      ", loan_amount " & _
                      ", approverunkid " & _
                      ", loan_statusunkid " & _
                      ", approved_amount " & _
                      ", remark " & _
                      ", isloan" & _
                      ", isvoid " & _
                      ", userunkid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime" & _
                      ", voidreason" & _
                      ", isexternal_entity " & _
                      ", external_entity_name" & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid" & _
                      ", emp_remark " & _
                      ", duration " & _
                      ", deductionperiodunkid " & _
                      ", installmentamt " & _
                      ", noofinstallment " & _
                      ", countryunkid " & _
                      ", isimport " & _
                      ", loan_account_no " & _
                    ") VALUES (" & _
                      "  @application_no " & _
                      ", @application_date " & _
                      ", @employeeunkid " & _
                      ", @loanschemeunkid " & _
                      ", @loan_amount " & _
                      ", @approverunkid " & _
                      ", @loan_statusunkid " & _
                      ", @approved_amount " & _
                      ", @remark " & _
                      ", @isloan" & _
                      ", @isvoid " & _
                      ", @userunkid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime" & _
                      ", @voidreason" & _
                      ", @isexternal_entity " & _
                      ", @external_entity_name" & _
                      ", @loginemployeeunkid " & _
                      ", @voidloginemployeeunkid" & _
                      ", @emp_remark " & _
                      ", @duration " & _
                      ", @deductionperiodunkid " & _
                      ", @installmentamt " & _
                      ", @noofinstallment " & _
                      ", @countryunkid " & _
                      ", @isimport " & _
                      ", @loan_account_no " & _
                    "); SELECT @@identity"

            'Hemant (02 Jan 2019) - [loan_account_no]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintProcesspendingloanunkid = dsList.Tables(0).Rows(0).Item(0)
            If intLoanApplicationNoType = 1 Then
                If Set_AutoNumber(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", "NextLoanApplicationNo", strLoanApplicationPrifix, xCompanyUnkid) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                If Get_Saved_Number(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", mstrApplication_No) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If InsertAuditTrailForPendingLoan(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim objLoanApprover As New clsLoanApprover_master
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dtApprover As DataTable = objLoanApprover.GetEmployeeApprover(mintEmployeeunkid, mblnIsLoanApprover_ForLoanScheme, mintLoanschemeunkid)
            Dim dtApprover As DataTable = objLoanApprover.GetEmployeeApprover(xDatabaseName, _
                                                                              xUserUnkid, _
                                                                              xYearUnkid, _
                                                                              xCompanyUnkid, _
                                                                              mintEmployeeunkid, _
                                                                              mblnIsLoanApprover_ForLoanScheme, _
                                                                              mintLoanschemeunkid)
            'Hemant (30 Aug 2019) -- [FinancialYear._Object._DatabaseName --> xDatabaseName, User._Object._Userunkid --> xUserUnkid, FinancialYear._Object._YearUnkid --> xYearUnkid, Company._Object._Companyunkid --> xCompanyUnkid ]
            
            'Nilay (10-Oct-2015) -- End


            objLoanApprover = Nothing

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
            Dim blnEnableVisibility As Boolean = False
            Dim intMinPriority As Integer = -1
            Dim intApproverID As Integer = -1
            'Nilay (08-Dec-2016) -- End

            If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                For Each drRow As DataRow In dtApprover.Rows
                    objLoanApproval._Processpendingloanunkid = mintProcesspendingloanunkid
                    objLoanApproval._Employeeunkid = mintEmployeeunkid
                    objLoanApproval._Approverempunkid = CInt(drRow("approverempunkid"))
                    objLoanApproval._Approvertranunkid = CInt(drRow("lnapproverunkid"))
                    objLoanApproval._Approvaldate = mdtApplication_Date
                    objLoanApproval._Deductionperiodunkid = mintDeductionPeriodunkid
                    objLoanApproval._Countryunkid = mintCountryunkid
                    objLoanApproval._Priority = CInt(drRow("priority"))
                    objLoanApproval._Loan_Amount = mdecLoan_Amount
                    'Nilay (21-Oct-2015) -- Start
                    'ENHANCEMENT : NEW LOAN Given By Rutta
                    'objLoanApproval._Duration = mintDurationInMonths
                    'Nilay (21-Oct-2015) -- End
                    objLoanApproval._Installmentamt = mdecInstallmentAmt
                    objLoanApproval._Noofinstallment = mintNoofInstallment
                    objLoanApproval._Countryunkid = mintCountryunkid
                    objLoanApproval._Statusunkid = mintLoan_Statusunkid
                    objLoanApproval._Userunkid = mintUserunkid
                    objLoanApproval._WebClientIP = mstrWebClientIP
                    objLoanApproval._WebFormName = mstrWebFormName
                    objLoanApproval._WebHostName = mstrWebHostName

                    intMinPriority = CInt(dtApprover.Compute("MIN(priority)", "1=1"))

                    If intMinPriority = CInt(drRow("priority")) Then
                        'Nilay (08-Dec-2016) -- Start
                        'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
                        If mintUserunkid = CInt(drRow("MappedUserID")) Then
                            objLoanApproval._Statusunkid = enLoanApplicationStatus.APPROVED
                            mintMinApprovedPriority = CInt(drRow("priority"))
                            intApproverID = CInt(drRow("lnapproverunkid"))
                            blnEnableVisibility = True
                        End If

                        If blnEnableVisibility = True Then
                            objLoanApproval._VisibleId = enLoanApplicationStatus.APPROVED
                        Else
                            'Nilay (27-Dec-2016) -- Start
                            'objLoanApproval._VisibleId = mintLoan_Statusunkid
                            Dim dRow As DataRow() = dtApprover.Select("priority=" & intMinPriority & " AND MappedUserID=" & mintUserunkid & "")
                            If dRow.Length > 0 Then
                                objLoanApproval._VisibleId = enLoanApplicationStatus.APPROVED
                            Else
                        objLoanApproval._VisibleId = mintLoan_Statusunkid
                        End If
                            'Nilay (27-Dec-2016) -- End
                        End If
                        'Nilay (08-Dec-2016) -- End
                    Else
                        'Nilay (08-Dec-2016) -- Start
                        'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
                        'objLoanApproval._VisibleId = -1
                        If blnEnableVisibility = True Then
                            Dim intNextMinPriority As Integer = CInt(dtApprover.Compute("MIN(priority)", "priority > " & intMinPriority))
                            If intNextMinPriority = CInt(drRow("priority")) Then
                                objLoanApproval._VisibleId = enLoanApplicationStatus.PENDING
                    Else
                        objLoanApproval._VisibleId = -1
                    End If
                        Else
                            objLoanApproval._VisibleId = -1
                        End If
                        'Nilay (08-Dec-2016) -- End
                    End If

                    If objLoanApproval.Insert(mblnIsLoanApprover_ForLoanScheme, mintLoanschemeunkid, objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                'Nilay (08-Dec-2016) -- Start
                'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
                'NOTES : WHEN ONLY ONE APPROVER IS THERE AND LOAN APPLIED THEN TO UPDATE APPROVED STATUS IN LNLOAN_PROCESS_PENDING_LOAN OF APPLICATION
                If blnEnableVisibility = True Then
                    Dim intMaxPriority As Integer = CInt(dtApprover.Compute("MAX(priority)", "1=1"))
                    If intMaxPriority = intMinPriority Then
                        strQ = " UPDATE lnloan_process_pending_loan SET " & _
                               "     loan_statusunkid = " & enLoanApplicationStatus.APPROVED & " " & _
                               "    ,approverunkid = " & intApproverID & " " & _
                               "    ,approved_amount = " & mdecLoan_Amount & " " & _
                               " WHERE isvoid=0 AND processpendingloanunkid = @processpendingloanunkid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mintLoan_Statusunkid = enLoanApplicationStatus.APPROVED

                        If InsertAuditTrailForPendingLoan(objDataOperation, enAuditType.EDIT) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                End If
                'Nilay (08-Dec-2016) -- End

                dtApprover.Rows.Clear()
            End If
            dtApprover = Nothing

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Public Function Insert(Optional ByVal intCompanyUnkId As Integer = 0 _
    '                       , Optional ByVal intLoanApplicationNoType As Integer = 0 _
    '                       , Optional ByVal strLoanApplicationPrifix As String = "" _
    '                       , Optional ByVal intNextLoanApplicationNo As Integer = 0) As Boolean


    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    If intLoanApplicationNoType = 0 Then intLoanApplicationNoType = ConfigParameter._Object._LoanApplicationNoType
    '    If strLoanApplicationPrifix = "" Then strLoanApplicationPrifix = ConfigParameter._Object._LoanApplicationPrifix

    '    If intLoanApplicationNoType = 0 Then
    '        If isExist(mintEmployeeunkid, mblnIsloan, mstrApplication_No, mdtApplication_Date, mintLoanschemeunkid) Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
    '            objDataOperation.ReleaseTransaction(False)
    '            Return False
    '        End If
    '    End If

    '    Try
    '        objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
    '        objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString)
    '        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
    '        objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
    '        objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString)
    '        objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
    '        objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        If mdtApplication_Date = Nothing Then
    '            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
    '        End If
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
    '        objDataOperation.AddParameter("@isexternal_entity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal_Entity.ToString)
    '        objDataOperation.AddParameter("@external_entity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExternal_Entity_Name.ToString)
    '        objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
    '        objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
    '        objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)

    '        objDataOperation.AddParameter("@duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDurationInMonths.ToString)
    '        objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionPeriodunkid.ToString)
    '        objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInstallmentAmt.ToString)
    '        objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofInstallment.ToString)
    '        objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)

    '        strQ = "INSERT INTO lnloan_process_pending_loan ( " & _
    '                  "  application_no " & _
    '                  ", application_date " & _
    '                  ", employeeunkid " & _
    '                  ", loanschemeunkid " & _
    '                  ", loan_amount " & _
    '                  ", approverunkid " & _
    '                  ", loan_statusunkid " & _
    '                  ", approved_amount " & _
    '                  ", remark " & _
    '                  ", isloan" & _
    '                  ", isvoid " & _
    '                  ", userunkid " & _
    '                  ", voiduserunkid " & _
    '                  ", voiddatetime" & _
    '                  ", voidreason" & _
    '                  ", isexternal_entity " & _
    '                  ", external_entity_name" & _
    '                  ", loginemployeeunkid " & _
    '                  ", voidloginemployeeunkid" & _
    '                  ", emp_remark " & _
    '                  ", duration " & _
    '                  ", deductionperiodunkid " & _
    '                  ", installmentamt " & _
    '                  ", noofinstallment " & _
    '                  ", countryunkid " & _
    '                ") VALUES (" & _
    '                  "  @application_no " & _
    '                  ", @application_date " & _
    '                  ", @employeeunkid " & _
    '                  ", @loanschemeunkid " & _
    '                  ", @loan_amount " & _
    '                  ", @approverunkid " & _
    '                  ", @loan_statusunkid " & _
    '                  ", @approved_amount " & _
    '                  ", @remark " & _
    '                  ", @isloan" & _
    '                  ", @isvoid " & _
    '                  ", @userunkid " & _
    '                  ", @voiduserunkid " & _
    '                  ", @voiddatetime" & _
    '                  ", @voidreason" & _
    '                  ", @isexternal_entity " & _
    '                  ", @external_entity_name" & _
    '                  ", @loginemployeeunkid " & _
    '                  ", @voidloginemployeeunkid" & _
    '                  ", @emp_remark " & _
    '                  ", @duration " & _
    '                  ", @deductionperiodunkid " & _
    '                  ", @installmentamt " & _
    '                  ", @noofinstallment " & _
    '                  ", @countryunkid " & _
    '                "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintProcesspendingloanunkid = dsList.Tables(0).Rows(0).Item(0)
    '        If intLoanApplicationNoType = 1 Then
    '            If Set_AutoNumber(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", "NextLoanApplicationNo", strLoanApplicationPrifix, intCompanyUnkId) = False Then
    '                If objDataOperation.ErrorMessage <> "" Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If
    '            If Get_Saved_Number(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", mstrApplication_No) = False Then
    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '            End If
    '        End If

    '        If InsertAuditTrailForPendingLoan(objDataOperation, 1) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        Dim objLoanApprover As New clsLoanApprover_master
    '        Dim dtApprover As DataTable = objLoanApprover.GetEmployeeApprover(mintEmployeeunkid, mblnIsLoanApprover_ForLoanScheme, mintLoanschemeunkid)
    '        objLoanApprover = Nothing

    '        If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
    '            For Each drRow As DataRow In dtApprover.Rows
    '                objLoanApproval._Processpendingloanunkid = mintProcesspendingloanunkid
    '                objLoanApproval._Employeeunkid = mintEmployeeunkid
    '                objLoanApproval._Approverempunkid = CInt(drRow("approverempunkid"))
    '                objLoanApproval._Approvertranunkid = CInt(drRow("lnapproverunkid"))
    '                objLoanApproval._Approvaldate = mdtApplication_Date
    '                objLoanApproval._Deductionperiodunkid = mintDeductionPeriodunkid
    '                objLoanApproval._Countryunkid = mintCountryunkid
    '                objLoanApproval._Priority = CInt(drRow("priority"))
    '                objLoanApproval._Loan_Amount = mdecLoan_Amount
    '                objLoanApproval._Duration = mintDurationInMonths
    '                objLoanApproval._Installmentamt = mdecInstallmentAmt
    '                objLoanApproval._Noofinstallment = mintNoofInstallment
    '                objLoanApproval._Countryunkid = mintCountryunkid
    '                objLoanApproval._Statusunkid = mintLoan_Statusunkid
    '                objLoanApproval._Userunkid = mintUserunkid
    '                objLoanApproval._WebClientIP = mstrWebClientIP
    '                objLoanApproval._WebFormName = mstrWebFormName
    '                objLoanApproval._WebHostName = mstrWebHostName

    '                Dim intMinPriority As Integer = CInt(dtApprover.Compute("MIN(priority)", "1=1"))

    '                If intMinPriority = CInt(drRow("priority")) Then
    '                    objLoanApproval._VisibleId = mintLoan_Statusunkid
    '                Else
    '                    objLoanApproval._VisibleId = -1
    '                End If

    '                If objLoanApproval.Insert(mblnIsLoanApprover_ForLoanScheme, mintLoanschemeunkid, objDataOperation) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '            Next
    '            dtApprover.Rows.Clear()
    '        End If
    '        dtApprover = Nothing

    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Nilay (10-Oct-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_process_pending_loan) </purpose>
    Public Function Update(ByVal IsFromLoanApplication As Boolean, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        'Nilay (13-Sept-2016) -- [IsFromLoanApplication]

        If isExist(mintEmployeeunkid, mblnIsloan, mstrApplication_No, mdtApplication_Date, mintLoanschemeunkid, mintProcesspendingloanunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
            Return False
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        Try
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
            objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            If mdtApplication_Date = Nothing Then
                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isexternal_entity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal_Entity.ToString)
            objDataOperation.AddParameter("@external_entity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExternal_Entity_Name.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)
            objDataOperation.AddParameter("@duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDurationInMonths.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionPeriodunkid.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInstallmentAmt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofInstallment.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            'Hemant (02 Jan 2019) -- Start
            'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
            objDataOperation.AddParameter("@loan_account_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Account_No.ToString)
            'Hemant (02 Jan 2019) -- End
            strQ = "UPDATE lnloan_process_pending_loan SET " & _
              "  application_no = @application_no" & _
              ", application_date = @application_date" & _
              ", employeeunkid = @employeeunkid" & _
              ", loanschemeunkid = @loanschemeunkid" & _
              ", loan_amount = @loan_amount" & _
              ", approverunkid = @approverunkid" & _
              ", loan_statusunkid = @loan_statusunkid" & _
              ", approved_amount = @approved_amount" & _
              ", remark = @remark" & _
              ", isloan = @isloan " & _
              ", isvoid = @isvoid" & _
              ", userunkid = @userunkid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
              ", isexternal_entity = @isexternal_entity" & _
              ", external_entity_name = @external_entity_name " & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
              ", emp_remark = @emp_remark " & _
              ", duration = @duration " & _
              ", deductionperiodunkid = @deductionperiodunkid " & _
              ", installmentamt  = @installmentamt " & _
              ", noofinstallment = @noofinstallment " & _
              ", countryunkid = @countryunkid " & _
              ", loan_account_no = @loan_account_no " & _
            "WHERE processpendingloanunkid = @processpendingloanunkid "

            'Hemant (02 Jan 2019) - [loan_account_no]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForPendingLoan(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (05-May-2016) -- Start
            'objLoanApproval._Processpendingloanunkid = mintProcesspendingloanunkid
            'objLoanApproval._Employeeunkid = mintEmployeeunkid
            'objLoanApproval._Approverempunkid = -1
            'objLoanApproval._Approvertranunkid = -1
            'objLoanApproval._Approvaldate = mdtApplication_Date
            'objLoanApproval._Deductionperiodunkid = mintDeductionPeriodunkid
            'objLoanApproval._Loan_Amount = mdecLoan_Amount
            'objLoanApproval._Duration = mintDurationInMonths
            'objLoanApproval._Installmentamt = mdecInstallmentAmt
            'objLoanApproval._Noofinstallment = mintNoofInstallment
            'objLoanApproval._Countryunkid = mintCountryunkid
            'objLoanApproval._Statusunkid = mintLoan_Statusunkid
            'objLoanApproval._Userunkid = mintUserunkid
            'objLoanApproval._WebClientIP = mstrWebClientIP
            'objLoanApproval._WebFormName = mstrWebFormName
            'objLoanApproval._WebHostName = mstrWebHostName
            'If objLoanApproval.Update(objDataOperation) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            If IsFromLoanApplication = True Then
                objLoanApproval._Processpendingloanunkid = mintProcesspendingloanunkid
                objLoanApproval._Employeeunkid = mintEmployeeunkid
                objLoanApproval._Approvaldate = mdtApplication_Date
                objLoanApproval._Deductionperiodunkid = mintDeductionPeriodunkid
                objLoanApproval._Loan_Amount = mdecLoan_Amount
                objLoanApproval._Installmentamt = mdecInstallmentAmt
                objLoanApproval._Noofinstallment = mintNoofInstallment
                objLoanApproval._Remark = mstrRemark
                objLoanApproval._WebClientIP = mstrWebClientIP
                objLoanApproval._WebFormName = mstrWebFormName
                objLoanApproval._WebHostName = mstrWebHostName
                If objLoanApproval.UpdateLoanApplication(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Nilay (13-Sept-2016) -- End

            'Nilay (05-May-2016) -- End

            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_process_pending_loan) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "SELECT ISNULL(pendingloantranunkid,0) AS pendingloantranunkid FROM lnloanapproval_process_tran  WHERE processpendingloanunkid = @processpendingloanunkid AND isvoid =0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                strQ = " UPDATE lnloanapproval_process_tran SET " & _
               "  isvoid = @isvoid" & _
               ", voiduserunkid = @voiduserunkid" & _
               ", voiddatetime = @voiddatetime " & _
               ", voidreason = @voidreason " & _
               " WHERE pendingloantranunkid = @pendingloantranunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@pendingloantranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("pendingloantranunkid")))
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                If mintVoidloginemployeeunkid > 0 Then
                    objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
                End If
                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objLoanApproval._Pendingloantranunkid = CInt(dr("pendingloantranunkid"))
                objLoanApproval._WebClientIP = mstrWebClientIP
                objLoanApproval._WebFormName = mstrWebFormName
                objLoanApproval._WebHostName = mstrWebHostName
                If objLoanApproval.InsertAuditTrailForLoanApproval(objDataOperation, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            If mintVoidloginemployeeunkid <= -1 Then
                strQ = "UPDATE lnloan_process_pending_loan SET " & _
                        "  isvoid = @isvoid" & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                "WHERE processpendingloanunkid = @processpendingloanunkid "
            Else
                strQ = "UPDATE lnloan_process_pending_loan SET " & _
                                "  isvoid = @isvoid" & _
                                ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                                ", voiddatetime = @voiddatetime " & _
                                ", voidreason = @voidreason " & _
                        "WHERE processpendingloanunkid = @processpendingloanunkid "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mintVoidloginemployeeunkid > 0 Then
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            End If
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            _Processpendingloanunkid = intUnkid

            If InsertAuditTrailForPendingLoan(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpId As Integer, ByVal blnLoan As Boolean, ByVal strAppNo As String, ByVal dtAppDate As Date, ByVal intLoanSchemeId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  processpendingloanunkid " & _
              ", application_no " & _
              ", application_date " & _
              ", employeeunkid " & _
              ", loanschemeunkid " & _
              ", loan_amount " & _
              ", approverunkid " & _
              ", loan_statusunkid " & _
              ", approved_amount " & _
              ", remark " & _
              ", isloan " & _
              ", isvoid " & _
              ", userunkid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", ISNULL(duration,0) AS duration " & _
              ", ISNULL(deductionperiodunkid,0) AS deductionperiodunkid  " & _
              ", ISNULL(installmentamt,0.00) AS installmentamt " & _
              ", ISNULL(noofinstallment,0) AS  noofinstallment " & _
              ",  ISNULL(countryunkid,0) AS countryunkid  " & _
              ", ISNULL(loan_account_no,'') As loan_account_no " & _
            "FROM lnloan_process_pending_loan " & _
            "WHERE CONVERT(CHAR(8),application_date,112) = @application_date " & _
            "AND application_no = @application_no " & _
            "AND isloan = @isloan "

            'Hemant (02 Jan 2019) - [loan_account_no]
            If intEmpId > 0 Then
                strQ &= "AND employeeunkid = @employeeunkid "
            End If

            If intLoanSchemeId > 0 Then
                strQ &= "AND loanschemeunkid = @loanschemeunkid "
            End If

            If intUnkid > 0 Then
                strQ &= " AND processpendingloanunkid <> @processpendingloanunkid"
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnLoan)
            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strAppNo)
            objDataOperation.AddParameter("@application_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtAppDate).ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanSchemeId)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLoan_Status(Optional ByVal strListName As String = "List", _
                                   Optional ByVal blnFlag As Boolean = False, _
                                   Optional ByVal UseAssignedStatus As Boolean = False, _
                                   Optional ByVal IsShowCancelStatus As Boolean = False) As DataSet
        'Nilay (20-Sept-2016) -- [IsShowCancelStatus]

        Dim strQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            If blnFlag = True Then
                strQ = "SELECT 0 AS Id,@Select AS NAME UNION  "
            End If

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'strQ &= " SELECT 1 AS Id,@Pending AS NAME " & _
            '        "UNION SELECT 2 AS Id,@Approved AS NAME " & _
            '        "UNION SELECT 3 AS Id,@Rejected AS NAME "
            strQ &= " SELECT " & enLoanApplicationStatus.PENDING & " AS Id,@Pending AS NAME " & _
                    "UNION SELECT " & enLoanApplicationStatus.APPROVED & " AS Id,@Approved AS NAME " & _
                    "UNION SELECT " & enLoanApplicationStatus.REJECTED & " AS Id,@Rejected AS NAME "

            'Nilay (20-Sept-2016) -- End

            If UseAssignedStatus = True Then
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'strQ &= "UNION SELECT 4 AS Id, @Assigned AS NAME "
                strQ &= "UNION SELECT " & enLoanApplicationStatus.ASSIGNED & " AS Id, @Assigned AS NAME "
                'Nilay (20-Sept-2016) -- End
                objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Assigned"))
            End If

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            If IsShowCancelStatus = True Then
                strQ &= "UNION SELECT " & enLoanApplicationStatus.CANCELLED & " AS Id, @Cancelled AS NAME "
                objDataOperation.AddParameter("@Cancelled", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 96, "Cancelled"))
            End If
            'Nilay (20-Sept-2016) -- End

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoan_Status", mstrModuleName)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Get_Id_Used_In_Paymet(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   " prpayment_tran.referencetranunkid " & _
                   " FROM prpayment_tran " & _
                   " LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
                   " LEFT JOIN lnloan_process_pending_loan ON lnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
                   " WHERE lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid"

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Get_Id_Used_In_LoanAdvance(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try

            strQ = "Select " & _
                      " loanadvancetranunkid " & _
                      "From lnloan_advance_tran " & _
                      "WHERE processpendingloanunkid=@processpendingloanunkid " & _
                      "AND ISNULL(lnloan_advance_tran.isvoid,0) = 0 "
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Get_LoanAdvanceunkid(ByVal intUnkid As Integer, ByRef intLoanAdvanceId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "loanadvancetranunkid " & _
                   " FROM lnloan_advance_tran " & _
                   " WHERE processpendingloanunkid = @processpendingloanunkid"

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intLoanAdvanceId = CInt(dsList.Tables(0).Rows(0)("loanadvancetranunkid"))
            Else
                intLoanAdvanceId = 0
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function InsertAuditTrailForPendingLoan(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atlnloan_process_pending_loan ( " & _
                        "  processpendingloanunkid " & _
                        ", application_no " & _
                        ", application_date " & _
                        ", employeeunkid " & _
                        ", loanschemeunkid " & _
                        ", loan_amount " & _
                        ", approverunkid " & _
                        ", loan_statusunkid " & _
                        ", approved_amount " & _
                        ", remark " & _
                        ", isloan " & _
                        ", duration " & _
                        ", deductionperiodunkid " & _
                        ", installmentamt " & _
                        ", noofinstallment " & _
                        ", countryunkid " & _
                        ", isexternal_entity " & _
                        ", external_entity_name" & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", emp_remark " & _
                        ", form_name " & _
                        ", module_name1 " & _
                        ", module_name2 " & _
                        ", module_name3 " & _
                        ", module_name4 " & _
                        ", module_name5 " & _
                        ", isweb " & _
                        ", loginemployeeunkid " & _
                        ", isimport " & _
                        ", loan_account_no " & _
                   ") VALUES (" & _
                        "  @processpendingloanunkid " & _
                        ", @application_no " & _
                        ", @application_date " & _
                        ", @employeeunkid " & _
                        ", @loanschemeunkid " & _
                        ", @loan_amount " & _
                        ", @approverunkid " & _
                        ", @loan_statusunkid " & _
                        ", @approved_amount " & _
                        ", @remark " & _
                        ", @isloan " & _
                        ", @duration " & _
                        ", @deductionperiodunkid " & _
                        ", @installmentamt " & _
                        ", @noofinstallment " & _
                        ", @countryunkid " & _
                        ", @isexternal_entity " & _
                        ", @external_entity_name" & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name" & _
                        ", @emp_remark " & _
                         ", @form_name " & _
                         ", @module_name1 " & _
                         ", @module_name2 " & _
                         ", @module_name3 " & _
                         ", @module_name4 " & _
                         ", @module_name5 " & _
                         ", @isweb " & _
                        ", @loginemployeeunkid " & _
                        ", @isimport " & _
                        ", @loan_account_no " & _
                   "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
            objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
            objDataOperation.AddParameter("@duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDurationInMonths.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionPeriodunkid.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInstallmentAmt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofInstallment.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@isexternal_entity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal_Entity.ToString)
            objDataOperation.AddParameter("@external_entity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExternal_Entity_Name.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)

            'Shani(26-Nov-2015) -- Start
            'ENHANCEMENT : Add Loan Import Form
            objDataOperation.AddParameter("@isimport", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImportedLoan.ToString)
            'Shani(26-Nov-2015) -- End

            'Hemant (02 Jan 2019) -- Start
            'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
            objDataOperation.AddParameter("@loan_account_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoan_Account_No.ToString)
            'Hemant (02 Jan 2019) -- End


            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForPendingLoan", mstrModuleName)
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateLoanData(ByVal intLoanId As Integer, _
                                   ByVal decNewAmount As Decimal, _
                                   ByVal decIntrest_Amt As Decimal, _
                                   ByVal decNetAmt As Decimal, _
                                   ByVal blnIsAdvance As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanId.ToString)

            If blnIsAdvance = False Then
                strQ = "UPDATE lnloan_advance_tran SET " & _
                        "  loan_amount = @loan_amount" & _
                        ", interest_amount = @interest_amount" & _
                        ", net_amount = @net_amount" & _
                        " WHERE loanadvancetranunkid = @loanadvancetranunkid "

                objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decNewAmount.ToString)
                objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decIntrest_Amt.ToString)
                objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decNetAmt.ToString)
            Else
                strQ = "UPDATE lnloan_advance_tran SET " & _
                        "  advance_amount = @advance_amount" & _
                      " WHERE loanadvancetranunkid = @loanadvancetranunkid "

                objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decNewAmount.ToString)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>

    Public Function Get_UnAssigned_ProcessPending_List(ByVal xDatabaseName As String, _
                                                       ByVal xUserUnkid As Integer, _
                                                       ByVal xYearUnkid As Integer, _
                                                       ByVal xCompanyUnkid As Integer, _
                                                       ByVal xPeriodStart As DateTime, _
                                                       ByVal xPeriodEnd As DateTime, _
                                                       ByVal xUserModeSetting As String, _
                                                       ByVal xOnlyApproved As Boolean, _
                                                       ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                                       ByVal strTableName As String, _
                                                       Optional ByVal strFilter As String = "", _
                                                       Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                       ) As DataSet
        'Sohail (06 Jan 2016) - [blnApplyUserAccessFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            'Sohail (06 Jan 2016) -- Start
            'Enhancement - Show Close Year Process Logs on Close Year Wizard.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If blnApplyUserAccessFilter = True Then
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If
            'Sohail (06 Jan 2016) -- End

            strQ = "SELECT  lnloan_process_pending_loan.processpendingloanunkid " & _
                          ", lnloan_process_pending_loan.application_no " & _
                          ", convert(char(8),lnloan_process_pending_loan.application_date,112) As application_date " & _
                          ", lnloan_process_pending_loan.employeeunkid " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                          ", lnloan_process_pending_loan.loanschemeunkid " & _
                          ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
                          ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
                          ", lnloan_process_pending_loan.loan_amount As Amount " & _
                          ", lnloan_process_pending_loan.approverunkid " & _
                          ", lnloan_process_pending_loan.loan_statusunkid " & _
                          ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected END As LoanStatus " & _
                          ", lnloan_process_pending_loan.approved_amount " & _
                          ", lnloan_process_pending_loan.remark " & _
                          ", lnloan_process_pending_loan.isloan " & _
                          ", ISNULL(lnloan_process_pending_loan.duration,0) AS duration " & _
                          ", ISNULL(lnloan_process_pending_loan.deductionperiodunkid,0) AS deductionperiodunkid  " & _
                          ", ISNULL(lnloan_process_pending_loan.installmentamt,0.00) AS installmentamt " & _
                          ", ISNULL(lnloan_process_pending_loan.noofinstallment,0) AS  noofinstallment " & _
                           ",ISNULL(lnloan_process_pending_loan.countryunkid,0) AS countryunkid  " & _
                          ", lnloan_process_pending_loan.isvoid " & _
                          ", lnloan_process_pending_loan.userunkid " & _
                          ", lnloan_process_pending_loan.voiduserunkid " & _
                          ", lnloan_process_pending_loan.voiddatetime " & _
                          ", lnloan_process_pending_loan.voidreason " & _
                          ", lnloan_process_pending_loan.isexternal_entity " & _
                          ", lnloan_process_pending_loan.external_entity_name " & _
                          ", ISNULL(lnloan_process_pending_loan.application_no,'') As loan_account_no " & _
                    "FROM    lnloan_process_pending_loan " & _
                            "LEFT JOIN lnloan_advance_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
                            "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                            "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid "

            'Hemant (02 Jan 2019) - [loan_account_no]
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            strQ &= "WHERE   lnloan_advance_tran.loanadvancetranunkid IS NULL " & _
                            "AND ISNULL(lnloan_process_pending_loan.isvoid, 0) = 0 " & _
                            "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                            "AND lnloan_process_pending_loan.loan_statusunkid = 2 " '2=Approved

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= strFilter
            End If

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_UnAssigned_ProcessPending_List; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function Get_UnAssigned_ProcessPending_List(ByVal strTableName As String, Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try


    '        strQ = "SELECT  lnloan_process_pending_loan.processpendingloanunkid " & _
    '                      ", lnloan_process_pending_loan.application_no " & _
    '                      ", convert(char(8),lnloan_process_pending_loan.application_date,112) As application_date " & _
    '                      ", lnloan_process_pending_loan.employeeunkid " & _
    '                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                      ", lnloan_process_pending_loan.loanschemeunkid " & _
    '                      ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
    '                      ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
    '                      ", lnloan_process_pending_loan.loan_amount As Amount " & _
    '                      ", lnloan_process_pending_loan.approverunkid " & _
    '                      ", lnloan_process_pending_loan.loan_statusunkid " & _
    '                      ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected END As LoanStatus " & _
    '                      ", lnloan_process_pending_loan.approved_amount " & _
    '                      ", lnloan_process_pending_loan.remark " & _
    '                      ", lnloan_process_pending_loan.isloan " & _
    '                      ", ISNULL(lnloan_process_pending_loan.duration,0) AS duration " & _
    '                      ", ISNULL(lnloan_process_pending_loan.deductionperiodunkid,0) AS deductionperiodunkid  " & _
    '                      ", ISNULL(lnloan_process_pending_loan.installmentamt,0.00) AS installmentamt " & _
    '                      ", ISNULL(lnloan_process_pending_loan.noofinstallment,0) AS  noofinstallment " & _
    '                       ",ISNULL(lnloan_process_pending_loan.countryunkid,0) AS countryunkid  " & _
    '                      ", lnloan_process_pending_loan.isvoid " & _
    '                      ", lnloan_process_pending_loan.userunkid " & _
    '                      ", lnloan_process_pending_loan.voiduserunkid " & _
    '                      ", lnloan_process_pending_loan.voiddatetime " & _
    '                      ", lnloan_process_pending_loan.voidreason " & _
    '                      ", lnloan_process_pending_loan.isexternal_entity " & _
    '                      ", lnloan_process_pending_loan.external_entity_name " & _
    '                "FROM    lnloan_process_pending_loan " & _
    '                        "LEFT JOIN lnloan_advance_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
    '                        "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                "WHERE   lnloan_advance_tran.loanadvancetranunkid IS NULL " & _
    '                        "AND ISNULL(lnloan_process_pending_loan.isvoid, 0) = 0 " & _
    '                        "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                        "AND lnloan_process_pending_loan.loan_statusunkid = 2 " '2=Approved


    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then

    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        End If

    '        'Sohail (12 Jan 2015) -- Start
    '        'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
    '        'strQ &= UserAccessLevel._AccessLevelFilterString
    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'Sohail (12 Jan 2015) -- End

    '        objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
    '        objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))
    '        objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Get_UnAssigned_ProcessPending_List; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (10-Oct-2015) -- End

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Nilay (10-Oct-2015) -- Start
    'Public Function GetToAssignList(ByVal intStatusId As Integer, _
    '                                ByVal intDepartmentId As Integer, _
    '                                ByVal intBranchId As Integer, _
    '                                ByVal intSecId As Integer, _
    '                                ByVal intJobId As Integer, _
    '                                ByVal intEmpId As Integer, _
    '                                ByVal intApproverId As Integer, _
    '                                ByVal intMode As Integer, _
    '                                ByVal intSchemeId As Integer, _
    '                                Optional ByVal StrListName As String = "List", _
    '                                Optional ByVal strUserAccessFilter As String = "") As DataTable
    '    Dim dtTable As DataTable = Nothing
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Try

    '        objDataOperation = New clsDataOperation

    '        objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
    '        objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))

    '        '/* SCHEME MASTER LIST */
    '        StrQ = "SELECT " & _
    '                         " loanschemeunkid AS loanschemeunkid " & _
    '                         ",@Loan+' --> '+ISNULL(name,'') AS Schemes " & _
    '                   "FROM lnloan_scheme_master " & _
    '                   "UNION ALL " & _
    '                   "SELECT " & _
    '                         " 0 AS loanschemeunkid " & _
    '                         ",@Advance AS Schemes "

    '        Dim dsScheme As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsScheme.Tables("List").Rows.Count > 0 Then
    '            '/* TRANSACTION LIST */
    '            StrQ = "SELECT " & _
    '                             " ECode AS ECode " & _
    '                             ",EName AS EName " & _
    '                             ",Mode AS Mode " & _
    '                             ",AppNo AS AppNo " & _
    '                             ",Amount AS Amount " & _
    '                             ",LScheme AS LScheme " & _
    '                             ",LApp AS Approver " & _
    '                             ",PId AS PId " & _
    '                             ",ModeId AS ModeId " & _
    '                             ",IsEx AS IsEx " & _
    '                             ",BranchId AS BranchId " & _
    '                             ",DeptId AS DeptId " & _
    '                             ",SectionId AS SectionId " & _
    '                             ",JobId AS JobId " & _
    '                             ",EmpId AS EmpId " & _
    '                             ",ApprId AS ApprId " & _
    '                             ",SchemeId AS SchemeId " & _
    '                             ",StatusId AS StatusId " & _
    '                        "FROM " & _
    '                        "( " & _
    '                        "SELECT " & _
    '                              "ISNULL(hremployee_master.employeecode,'') AS ECode " & _
    '                             ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
    '                             ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
    '                             ",ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
    '                             ",ISNULL(lnloan_process_pending_loan.approved_amount,0) AS Amount " & _
    '                             ",lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
    '                             ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS ModeId " & _
    '                             ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan+' --> '+ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS LScheme " & _
    '                             ",ISNULL(LApp.firstname,'')+' '+ISNULL(LApp.othername,'')+' '+ISNULL(LApp.surname,'') AS LApp " & _
    '                             ",lnloan_process_pending_loan.isexternal_entity AS IsEx " & _
    '                             ",hremployee_master.stationunkid AS BranchId " & _
    '                             ",hremployee_master.departmentunkid  As DeptId " & _
    '                             ",hremployee_master.sectionunkid AS SectionId " & _
    '                             ",hremployee_master.jobunkid AS JobId " & _
    '                             ",hremployee_master.employeeunkid AS EmpId " & _
    '                             ",LApp.employeeunkid AS ApprId " & _
    '                             ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS SchemeId " & _
    '                             ",lnloan_process_pending_loan.loan_statusunkid AS StatusId " & _
    '                        "FROM lnloan_process_pending_loan " & _
    '                             "JOIN lnloan_approver_tran ON lnloan_approver_tran.approverunkid = lnloan_process_pending_loan.approverunkid " & _
    '                             "JOIN hremployee_master as LApp ON LApp.employeeunkid = lnloan_approver_tran.approverunkid " & _
    '                             "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                             "JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "WHERE isvoid = 0 "
    '            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            End If


    '            If strUserAccessFilter.Trim.Length > 0 Then
    '                StrQ &= strUserAccessFilter
    '                StrQ &= strUserAccessFilter.Replace("hremployee_master", "LApp")
    '            Else
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '                StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "LApp")
    '            End If

    '            StrQ &= ") AS ToAssign WHERE 1 = 1 AND StatusId = " & intStatusId

    '            If intDepartmentId > 0 Then
    '                StrQ &= " AND DeptId = '" & intDepartmentId & "'"
    '            End If

    '            If intApproverId > 0 Then
    '                StrQ &= " AND ApprId = '" & intApproverId & "'"
    '            End If

    '            If intBranchId > 0 Then
    '                StrQ &= " AND BranchId = '" & intBranchId & "'"
    '            End If

    '            If intEmpId > 0 Then
    '                StrQ &= " AND EmpId = '" & intEmpId & "'"
    '            End If

    '            If intJobId > 0 Then
    '                StrQ &= " AND JobId = '" & intJobId & "'"
    '            End If

    '            Select Case intMode
    '                Case 0
    '                    StrQ &= " AND ModeId = 1 "
    '                Case 1
    '                    StrQ &= " AND ModeId = 2 "
    '            End Select

    '            If intSchemeId > 0 Then
    '                StrQ &= " AND SchemeId = '" & intSchemeId & "'"
    '            End If

    '            If intSecId > 0 Then
    '                StrQ &= " AND SectionId = '" & intSecId & "'"
    '            End If

    '            Dim dsTrans As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If StrListName.Length > 0 Then
    '                dtTable = New DataTable(StrListName)
    '            Else
    '                dtTable = New DataTable("List")
    '            End If


    '            dtTable.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            dtTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Employee", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Approver", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Amount", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Pendingunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("IsEx", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("ApprId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1

    '            If dsTrans.Tables("List").Rows.Count > 0 Then

    '                Dim dtRow As DataRow = Nothing

    '                Dim dtFilter As DataTable

    '                For Each dsRow As DataRow In dsScheme.Tables("List").Rows
    '                    dtFilter = New DataView(dsTrans.Tables("List"), "SchemeId = '" & dsRow.Item("loanschemeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

    '                    If dtFilter.Rows.Count > 0 Then
    '                        dtRow = dtTable.NewRow

    '                        dtRow.Item("ECode") = dsRow.Item("Schemes")
    '                        dtRow.Item("IsGrp") = True
    '                        dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")

    '                        dtTable.Rows.Add(dtRow)

    '                        For Each dtlRow As DataRow In dtFilter.Rows
    '                            dtRow = dtTable.NewRow

    '                            dtRow.Item("ECode") = Space(5) & dtlRow.Item("ECode")
    '                            dtRow.Item("Employee") = dtlRow.Item("EName")
    '                            dtRow.Item("Approver") = dtlRow.Item("Approver")
    '                            dtRow.Item("Amount") = Format(CDec(dtlRow.Item("Amount")), GUI.fmtCurrency)
    '                            dtRow.Item("Pendingunkid") = dtlRow.Item("PId")
    '                            dtRow.Item("IsEx") = dtlRow.Item("IsEx")
    '                            dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
    '                            dtRow.Item("ApprId") = dtlRow.Item("ApprId")
    '                            dtRow.Item("EmpId") = dtlRow.Item("EmpId")

    '                            dtTable.Rows.Add(dtRow)
    '                        Next

    '                    End If

    '                Next

    '            End If

    '        End If


    '        Return dtTable

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetToAssignList", mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function
    'Nilay (10-Oct-2015) -- End

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Public Function GetToAssignList(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    ByVal xStatusId As Integer, _
                                    Optional ByVal xFilterString As String = "", _
                                    Optional ByVal strAdvanceFilter As String = "") As DataTable

        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation

            '/* SCHEME MASTER LIST */
            StrQ = "SELECT " & _
                   " loanschemeunkid AS loanschemeunkid " & _
                   ",@Loan+' --> '+ISNULL(name,'') AS Schemes " & _
                   "FROM lnloan_scheme_master " & _
                   "UNION ALL " & _
                   "SELECT " & _
                   " 0 AS loanschemeunkid " & _
                   ",@Advance AS Schemes "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))

            Dim dsScheme As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim intFirstOpenPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, StatusType.Open, False, True)
            objPeriod._Periodunkid(xDatabaseName) = intFirstOpenPeriodId
            objDataOperation.AddParameter("@FirstOpenPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intFirstOpenPeriodId.ToString)
            objDataOperation.AddParameter("@FirstOpenPeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._Start_Date))
            'Nilay (15-Dec-2015) -- End

            If dsScheme.Tables("List").Rows.Count > 0 Then
                '/* TRANSACTION LIST */
                StrQ = "SELECT " & _
                                 " ECode AS ECode " & _
                                 ",EName AS EName " & _
                                 ",Mode AS Mode " & _
                                 ",AppNo AS AppNo " & _
                                 ",Amount AS Amount " & _
                                 ",LScheme AS LScheme " & _
                                 ",LApp AS Approver " & _
                                 ",PId AS PId " & _
                                 ",ModeId AS ModeId " & _
                                 ",IsEx AS IsEx " & _
                                 ",BranchId AS BranchId " & _
                                 ",DeptId AS DeptId " & _
                                 ",SectionId AS SectionId " & _
                                 ",JobId AS JobId " & _
                                 ",EmpId AS EmpId " & _
                                 ",ApprId AS ApprId " & _
                                 ",SchemeId AS SchemeId " & _
                                 ",StatusId AS StatusId " & _
                                 ",duration " & _
                                 ",noofinstallment " & _
                                 ",installmentamt " & _
                                 ",deductionperiodunkid " & _
                                 ",deductionperiod " & _
                                 ",'' AS rate " & _
                                 ",countryunkid AS countryunkid " & _
                                 ",0 AS calctypeId " & _
                                 ",0 AS interest_calctype_id " & _
                                 ",dstartdate " & _
                                 ",0 AS mapped_tranheadunkid " & _
                            "FROM " & _
                            "( " & _
                            "SELECT " & _
                                 " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
                                 ",ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
                                 ",ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
                                 ",ISNULL(A.loan_amount,0) AS Amount " & _
                                 ",lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
                                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS ModeId " & _
                                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan+' --> '+ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS LScheme " & _
                                 "  ,#APPROVER_NAME# +' -> '+ lnlevelname  AS LApp " & _
                                 ",lnloan_process_pending_loan.isexternal_entity AS IsEx " & _
                                 ",ISNULL(ETT.stationunkid,0) AS BranchId " & _
                                 ",ISNULL(ETT.departmentunkid,0)  As DeptId " & _
                                 ",ISNULL(ETT.sectionunkid,0) AS SectionId " & _
                                 ",ISNULL(ECT.jobunkid,0) AS JobId " & _
                                 ",hremployee_master.employeeunkid AS EmpId " & _
                                 "  ,lnloanapprover_master.lnapproverunkid AS ApprId " & _
                                 ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS SchemeId " & _
                                 ",lnloan_process_pending_loan.loan_statusunkid AS StatusId " & _
                                 ",lnloan_process_pending_loan.duration " & _
                                 ",A.noofinstallment " & _
                                 ",A.installmentamt " & _
                                 ",CASE WHEN cfcommon_period_tran.statusid = 2 THEN @FirstOpenPeriodId ELSE lnloan_process_pending_loan.deductionperiodunkid END deductionperiodunkid " & _
                                 ",ISNULL(period_name,'') AS deductionperiod " & _
                                 ",lnloan_process_pending_loan.countryunkid " & _
                                 ",CASE WHEN cfcommon_period_tran.statusid = 2 THEN @FirstOpenPeriodStartDate ELSE CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) END AS dstartdate " & _
                            "FROM lnloan_process_pending_loan " & _
                                 "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_process_pending_loan.deductionperiodunkid AND modulerefid = 1 " & _
                                 "JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_process_pending_loan.approverunkid " & _
                                 "JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                                 "  #APPROVER_JOIN# " & _
                                 "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                                 "JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                                 "JOIN " & _
                                 "( " & _
                                 "      SELECT " & _
                                 "           loan_amount " & _
                                 "          ,installmentamt " & _
                                 "          ,noofinstallment " & _
                                 "          ,processpendingloanunkid " & _
                                 "          ,ROW_NUMBER()OVER(PARTITION BY processpendingloanunkid ORDER BY priority DESC) AS Rno " & _
                                 "      FROM lnloanapproval_process_tran " & _
                                 "      WHERE isvoid = 0 And statusunkid = 2 " & _
                                 ") " & _
                                 "AS A ON A.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid AND A.Rno = 1 " & _
                                 "LEFT JOIN " & _
                                 "( " & _
                                 "      SELECT " & _
                                 "           stationunkid " & _
                                 "          ,departmentunkid " & _
                                 "          ,sectionunkid " & _
                                 "          ,employeeunkid " & _
                                 "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 "      FROM hremployee_transfer_tran " & _
                                 "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                 ") AS ETT ON ETT.employeeunkid = hremployee_master.employeeunkid AND ETT.rno = 1 " & _
                                 "LEFT JOIN " & _
                                 "( " & _
                                 "      SELECT " & _
                                 "           jobunkid " & _
                                 "          ,employeeunkid " & _
                                 "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 "      FROM hremployee_categorization_tran " & _
                                 "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                 ") AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 "
                'Sohail (29 Apr 2019) - [mapped_tranheadunkid]

                StrFinalQurey = StrQ

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQCondition &= " WHERE lnloan_process_pending_loan.isvoid = 0 "

                StrQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrQ &= " AND " & xUACFiltrQry
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry.Replace("hremployee_master", "LApp")
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQDtFilters &= xDateFilterQry.Replace("hremployee_master", "LApp")
                    End If
                End If

                If xFilterString.Trim.Length > 0 Then
                    StrQCondition &= " AND " & xFilterString
                End If

                StrQ &= StrQCondition

                If strAdvanceFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & strAdvanceFilter
                End If

                StrQ &= StrQDtFilters

                StrQ &= ") AS ToAssign WHERE 1 = 1 AND StatusId = " & xStatusId

                StrQ = StrQ.Replace("#APPROVER_NAME#", "ISNULL(LApp.firstname, '') + ' ' + ISNULL(LApp.othername, '') + ' ' + ISNULL(LApp.surname, '') ")
                StrQ = StrQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master AS LApp ON LApp.employeeunkid = lnloanapprover_master.approverempunkid AND LApp.isapproved = 1 ")
                StrQ = StrQ.Replace("#EXT_APPROVER#", "0")

                Dim dsTrans As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dsCompany As DataSet
                Dim objlnApprover As New clsLoanApprover_master

                dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dsExtList As New DataSet

                For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                    StrQ = StrFinalQurey
                    StrQDtFilters = ""

                    If dRow("dbname").ToString.Trim.Length <= 0 Then
                        StrQ = StrQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                        StrQ = StrQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanapprover_master.approverempunkid ")

                        StrQ &= StrQCondition

                    Else
                        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dRow("effectivedate")), eZeeDate.convertDate(dRow("effectivedate")), , , dRow("dbname").ToString)
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(dRow("effectivedate")), xOnlyApproved, dRow("dbname").ToString, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                        Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dRow("effectivedate")), dRow("dbname").ToString)

                        StrQ = StrQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(LApp.firstname, '') + ' ' + ISNULL(LApp.othername, '') + ' ' + ISNULL(LApp.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(LApp.firstname, '') + ' ' + ISNULL(LApp.othername, '') + ' ' + ISNULL(LApp.surname, '') END ")
                        StrQ = StrQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                                                               "LEFT JOIN #DB_NAME#hremployee_master AS LApp ON LApp.employeeunkid = cfuser_master.employeeunkid ")
                        StrQ = StrQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")

                        If xDateJoinQry.Trim.Length > 0 Then
                            StrQ &= xDateJoinQry
                        End If

                        'S.SANDEEP [15 NOV 2016] -- START
                        'If xUACQry.Trim.Length > 0 Then
                        '    StrQ &= xUACQry
                        'End If
                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry.Replace("hremployee_master", "LApp")
                        End If
                        'S.SANDEEP [15 NOV 2016] -- END

                        If xAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xAdvanceJoinQry
                End If

                        StrQ &= StrQCondition

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry.Replace("hremployee_master", "LApp")
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xDateFilterQry.Replace("hremployee_master", "LApp") & " "
                            End If
                    End If

                End If

                    StrQ = StrQ.Replace("#EXT_APPROVER#", "1")

                    StrQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                    If dRow("dbname").ToString.Trim.Length > 0 Then
                        If strAdvanceFilter.Trim.Length > 0 Then
                            StrQ &= " AND " & strAdvanceFilter
                        End If
                End If

                StrQ &= ") AS ToAssign WHERE 1 = 1 AND StatusId = " & xStatusId

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
                    objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))
                    objDataOperation.AddParameter("@FirstOpenPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intFirstOpenPeriodId.ToString)
                    objDataOperation.AddParameter("@FirstOpenPeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._Start_Date))

                    dsExtList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                    If dsTrans.Tables.Count <= 0 Then
                        dsTrans.Tables.Add(dsExtList.Tables(0).Copy)
                    Else
                        dsTrans.Tables(0).Merge(dsExtList.Tables(0), True)
                    End If
                Next

                dtTable = New DataTable("List")

                dtTable.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
                'S.SANDEEP [01 AUG 2015] -- START
                dtTable.Columns.Add("AppNo", System.Type.GetType("System.String")).DefaultValue = ""
                'S.SANDEEP [01 AUG 2015] -- END
                dtTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Employee", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Approver", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Amount", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("deductionperiod", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("duration", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("noofinstallment", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("installmentamt", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("rate", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("deductionperiodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("Pendingunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("IsEx", System.Type.GetType("System.Boolean")).DefaultValue = False
                dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
                dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("ApprId", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("ischange", System.Type.GetType("System.Boolean")).DefaultValue = False
                dtTable.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("dstartdate", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("calctypeId", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("mdate", System.Type.GetType("System.String")).DefaultValue = ""
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                dtTable.Columns.Add("interest_calctype_id", System.Type.GetType("System.Int32")).DefaultValue = 0
                'Sohail (15 Dec 2015) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                dtTable.Columns.Add("mapped_tranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                'Sohail (29 Apr 2019) -- End
                '************************************************************************************************** MATCHING - START'
                dtTable.Columns.Add("instlnum", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("instlamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
                dtTable.Columns.Add("intrsamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
                dtTable.Columns.Add("netamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
                dtTable.Columns.Add("PaidExRate", System.Type.GetType("System.Decimal")).DefaultValue = 1
                dtTable.Columns.Add("orginstlamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
                '************************************************************************************************** MATCHING - END'

                If dsTrans.Tables("List").Rows.Count > 0 Then
                    Dim dtRow As DataRow = Nothing
                    Dim dtFilter As DataTable

                    For Each dsRow As DataRow In dsScheme.Tables("List").Rows
                        dtFilter = New DataView(dsTrans.Tables("List"), "SchemeId = '" & dsRow.Item("loanschemeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

                        If dtFilter.Rows.Count > 0 Then
                            dtRow = dtTable.NewRow

                            dtRow.Item("ECode") = dsRow.Item("Schemes")
                            dtRow.Item("Employee") = dsRow.Item("Schemes")
                            dtRow.Item("IsGrp") = True
                            dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
                            dtRow.Item("calctypeId") = -1
                            'Sohail (15 Dec 2015) -- Start
                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                            dtRow.Item("interest_calctype_id") = -1
                            'Sohail (15 Dec 2015) -- End
                            'Sohail (29 Apr 2019) -- Start
                            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                            dtRow.Item("mapped_tranheadunkid") = -1
                            'Sohail (29 Apr 2019) -- End
                            dtTable.Rows.Add(dtRow)

                            For Each dtlRow As DataRow In dtFilter.Rows
                                dtRow = dtTable.NewRow

                                'S.SANDEEP [01 AUG 2015] -- START
                                dtRow.Item("AppNo") = dtlRow.Item("AppNo")
                                'S.SANDEEP [01 AUG 2015] -- END

                                dtRow.Item("ECode") = Space(5) & dtlRow.Item("ECode")
                                dtRow.Item("Employee") = Space(5) & dtlRow.Item("EName")
                                dtRow.Item("Approver") = dtlRow.Item("Approver")
                                dtRow.Item("Amount") = Format(CDec(dtlRow.Item("Amount")), GUI.fmtCurrency)
                                dtRow.Item("Pendingunkid") = dtlRow.Item("PId")
                                dtRow.Item("IsEx") = dtlRow.Item("IsEx")
                                dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
                                dtRow.Item("ApprId") = dtlRow.Item("ApprId")
                                dtRow.Item("EmpId") = dtlRow.Item("EmpId")
                                dtRow.Item("deductionperiod") = dtlRow.Item("deductionperiod")
                                dtRow.Item("duration") = CInt(dtlRow.Item("duration"))
                                dtRow.Item("noofinstallment") = CInt(dtlRow.Item("noofinstallment"))
                                dtRow.Item("installmentamt") = Format(CDec(dtlRow.Item("installmentamt")), GUI.fmtCurrency)
                                dtRow.Item("rate") = dtlRow.Item("rate")
                                dtRow.Item("deductionperiodunkid") = dtlRow.Item("deductionperiodunkid")
                                dtRow.Item("countryunkid") = dtlRow.Item("countryunkid")
                                If dtlRow.Item("dstartdate").ToString.Length > 0 Then
                                    dtRow.Item("dstartdate") = eZeeDate.convertDate(dtlRow.Item("dstartdate").ToString).ToShortDateString
                                    dtRow.Item("mdate") = dtlRow.Item("dstartdate").ToString

                                    Dim objExRate As New clsExchangeRate : Dim dsList As New DataSet
                                    dsList = objExRate.GetList("ExRate", True, , , CInt(dtRow.Item("countryunkid")), True, eZeeDate.convertDate(dtlRow.Item("dstartdate").ToString).Date, True)
                                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                                        dtRow.Item("PaidExRate") = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                                    End If
                                    objExRate = Nothing

                                End If
                                dtRow.Item("calctypeId") = dtlRow.Item("calctypeId")
                                'Sohail (15 Dec 2015) -- Start
                                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                                dtRow.Item("interest_calctype_id") = dtlRow.Item("interest_calctype_id")
                                'Sohail (15 Dec 2015) -- End
                                'Sohail (29 Apr 2019) -- Start
                                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                                dtRow.Item("mapped_tranheadunkid") = dtlRow.Item("mapped_tranheadunkid")
                                'Sohail (29 Apr 2019) -- End

                                dtRow.Item("instlnum") = CInt(dtlRow.Item("noofinstallment"))
                                dtRow.Item("instlamt") = CDec(dtlRow.Item("installmentamt"))
                                dtRow.Item("orginstlamt") = Format(CDec(dtlRow.Item("installmentamt")), GUI.fmtCurrency)
                                dtTable.Rows.Add(dtRow)
                            Next

                        End If

                    Next

                End If
            End If

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetToAssignList", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Public Function GetToAssignList(ByVal xDatabaseName As String, _
    '                                ByVal xUserUnkid As Integer, _
    '                                ByVal xYearUnkid As Integer, _
    '                                ByVal xCompanyUnkid As Integer, _
    '                                ByVal xPeriodStart As DateTime, _
    '                                ByVal xPeriodEnd As DateTime, _
    '                                ByVal xUserModeSetting As String, _
    '                                ByVal xOnlyApproved As Boolean, _
    '                                ByVal xIncludeIn_ActiveEmployee As Boolean, _
    '                                ByVal xStatusId As Integer, _
    '                                Optional ByVal xFilterString As String = "") As DataTable

    '    Dim dtTable As DataTable = Nothing
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception

    '    Try
    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
    '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

    '        objDataOperation = New clsDataOperation

    '        objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
    '        objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))

    '        '/* SCHEME MASTER LIST */
    '        StrQ = "SELECT " & _
    '               " loanschemeunkid AS loanschemeunkid " & _
    '               ",@Loan+' --> '+ISNULL(name,'') AS Schemes " & _
    '               "FROM lnloan_scheme_master " & _
    '               "UNION ALL " & _
    '               "SELECT " & _
    '               " 0 AS loanschemeunkid " & _
    '               ",@Advance AS Schemes "

    '        Dim dsScheme As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Nilay (15-Dec-2015) -- Start
    '        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '        Dim objMaster As New clsMasterData
    '        Dim objPeriod As New clscommom_period_Tran
    '        Dim intFirstOpenPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, StatusType.Open, False, True)
    '        objPeriod._Periodunkid(xDatabaseName) = intFirstOpenPeriodId
    '        objDataOperation.AddParameter("@FirstOpenPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intFirstOpenPeriodId.ToString)
    '        objDataOperation.AddParameter("@FirstOpenPeriodStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._Start_Date))
    '        'Nilay (15-Dec-2015) -- End

    '        If dsScheme.Tables("List").Rows.Count > 0 Then
    '            '/* TRANSACTION LIST */

    '            'Nilay (18-Dec-2015) -- Start
    '            'LOAN ISSUE
    '            'StrQ = "SELECT " & _
    '            '                 " ECode AS ECode " & _
    '            '                 ",EName AS EName " & _
    '            '                 ",Mode AS Mode " & _
    '            '                 ",AppNo AS AppNo " & _
    '            '                 ",Amount AS Amount " & _
    '            '                 ",LScheme AS LScheme " & _
    '            '                 ",LApp AS Approver " & _
    '            '                 ",PId AS PId " & _
    '            '                 ",ModeId AS ModeId " & _
    '            '                 ",IsEx AS IsEx " & _
    '            '                 ",BranchId AS BranchId " & _
    '            '                 ",DeptId AS DeptId " & _
    '            '                 ",SectionId AS SectionId " & _
    '            '                 ",JobId AS JobId " & _
    '            '                 ",EmpId AS EmpId " & _
    '            '                 ",ApprId AS ApprId " & _
    '            '                 ",SchemeId AS SchemeId " & _
    '            '                 ",StatusId AS StatusId " & _
    '            '                 ",duration " & _
    '            '                 ",noofinstallment " & _
    '            '                 ",installmentamt " & _
    '            '                 ",deductionperiodunkid " & _
    '            '                 ",deductionperiod " & _
    '            '                 ",'' AS rate " & _
    '            '                 ",countryunkid AS countryunkid " & _
    '            '                 ",0 AS calctypeId " & _
    '            '                 ",dstartdate " & _
    '            '            "FROM " & _
    '            '            "( " & _
    '            '            "SELECT " & _
    '            '                 " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
    '            '                 ",ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
    '            '                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
    '            '                 ",ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
    '            '                 ",ISNULL(lnloan_process_pending_loan.approved_amount,0) AS Amount " & _
    '            '                 ",lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
    '            '                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS ModeId " & _
    '            '                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan+' --> '+ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS LScheme " & _
    '            '                 ",ISNULL(LApp.firstname, '') + ' ' + ISNULL(LApp.othername,'') + ' '+ ISNULL(LApp.surname, '')+' -> '+ lnlevelname  AS LApp " & _
    '            '                 ",lnloan_process_pending_loan.isexternal_entity AS IsEx " & _
    '            '                 ",ISNULL(ETT.stationunkid,0) AS BranchId " & _
    '            '                 ",ISNULL(ETT.departmentunkid,0)  As DeptId " & _
    '            '                 ",ISNULL(ETT.sectionunkid,0) AS SectionId " & _
    '            '                 ",ISNULL(ECT.jobunkid,0) AS JobId " & _
    '            '                 ",hremployee_master.employeeunkid AS EmpId " & _
    '            '                 ",LApp.employeeunkid AS ApprId " & _
    '            '                 ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS SchemeId " & _
    '            '                 ",lnloan_process_pending_loan.loan_statusunkid AS StatusId " & _
    '            '                 ",lnloan_process_pending_loan.duration " & _
    '            '                 ",lnloan_process_pending_loan.noofinstallment " & _
    '            '                 ",lnloan_process_pending_loan.installmentamt " & _
    '            '                 ",CASE WHEN cfcommon_period_tran.statusid = 2 THEN 0 ELSE lnloan_process_pending_loan.deductionperiodunkid END deductionperiodunkid " & _
    '            '                 ",ISNULL(period_name,'') AS deductionperiod " & _
    '            '                 ",lnloan_process_pending_loan.countryunkid " & _
    '            '                 ",CASE WHEN cfcommon_period_tran.statusid = 2 THEN '' ELSE CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) END AS dstartdate " & _
    '            '            "FROM lnloan_process_pending_loan " & _
    '            '                 "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_process_pending_loan.deductionperiodunkid AND modulerefid = 1 " & _
    '            '                 "JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_process_pending_loan.approverunkid " & _
    '            '                 "JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '            '                 "JOIN hremployee_master AS LApp ON LApp.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '            '                 "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '            '                 "JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                 "LEFT JOIN " & _
    '            '                 "( " & _
    '            '                 "      SELECT " & _
    '            '                 "           stationunkid " & _
    '            '                 "          ,departmentunkid " & _
    '            '                 "          ,sectionunkid " & _
    '            '                 "          ,employeeunkid " & _
    '            '                 "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '            '                 "      FROM hremployee_transfer_tran " & _
    '            '                 "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '            '                 ") AS ETT ON ETT.employeeunkid = hremployee_master.employeeunkid AND ETT.rno = 1 " & _
    '            '                 "LEFT JOIN " & _
    '            '                 "( " & _
    '            '                 "      SELECT " & _
    '            '                 "           jobunkid " & _
    '            '                 "          ,employeeunkid " & _
    '            '                 "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '            '                 "      FROM hremployee_categorization_tran " & _
    '            '                 "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '            '                 ") AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 "

    '            StrQ = "SELECT " & _
    '                             " ECode AS ECode " & _
    '                             ",EName AS EName " & _
    '                             ",Mode AS Mode " & _
    '                             ",AppNo AS AppNo " & _
    '                             ",Amount AS Amount " & _
    '                             ",LScheme AS LScheme " & _
    '                             ",LApp AS Approver " & _
    '                             ",PId AS PId " & _
    '                             ",ModeId AS ModeId " & _
    '                             ",IsEx AS IsEx " & _
    '                             ",BranchId AS BranchId " & _
    '                             ",DeptId AS DeptId " & _
    '                             ",SectionId AS SectionId " & _
    '                             ",JobId AS JobId " & _
    '                             ",EmpId AS EmpId " & _
    '                             ",ApprId AS ApprId " & _
    '                             ",SchemeId AS SchemeId " & _
    '                             ",StatusId AS StatusId " & _
    '                             ",duration " & _
    '                             ",noofinstallment " & _
    '                             ",installmentamt " & _
    '                             ",deductionperiodunkid " & _
    '                             ",deductionperiod " & _
    '                             ",'' AS rate " & _
    '                             ",countryunkid AS countryunkid " & _
    '                             ",0 AS calctypeId " & _
    '                             ",0 AS interest_calctype_id " & _
    '                             ",dstartdate " & _
    '                        "FROM " & _
    '                        "( " & _
    '                        "SELECT " & _
    '                             " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
    '                             ",ISNULL(hremployee_master.employeecode,'') +' - '+ ISNULL(hremployee_master.firstname,'') +' '+ ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'') AS EName " & _
    '                             ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
    '                             ",ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
    '                             ",ISNULL(A.loan_amount,0) AS Amount " & _
    '                             ",lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
    '                             ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS ModeId " & _
    '                             ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan+' --> '+ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS LScheme " & _
    '                             ",ISNULL(LApp.firstname, '') + ' ' + ISNULL(LApp.othername,'') + ' '+ ISNULL(LApp.surname, '')+' -> '+ lnlevelname  AS LApp " & _
    '                             ",lnloan_process_pending_loan.isexternal_entity AS IsEx " & _
    '                             ",ISNULL(ETT.stationunkid,0) AS BranchId " & _
    '                             ",ISNULL(ETT.departmentunkid,0)  As DeptId " & _
    '                             ",ISNULL(ETT.sectionunkid,0) AS SectionId " & _
    '                             ",ISNULL(ECT.jobunkid,0) AS JobId " & _
    '                             ",hremployee_master.employeeunkid AS EmpId " & _
    '                             ",LApp.employeeunkid AS ApprId " & _
    '                             ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS SchemeId " & _
    '                             ",lnloan_process_pending_loan.loan_statusunkid AS StatusId " & _
    '                             ",lnloan_process_pending_loan.duration " & _
    '                             ",A.noofinstallment " & _
    '                             ",A.installmentamt " & _
    '                             ",CASE WHEN cfcommon_period_tran.statusid = 2 THEN @FirstOpenPeriodId ELSE lnloan_process_pending_loan.deductionperiodunkid END deductionperiodunkid " & _
    '                             ",ISNULL(period_name,'') AS deductionperiod " & _
    '                             ",lnloan_process_pending_loan.countryunkid " & _
    '                             ",CASE WHEN cfcommon_period_tran.statusid = 2 THEN @FirstOpenPeriodStartDate ELSE CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) END AS dstartdate " & _
    '                        "FROM lnloan_process_pending_loan " & _
    '                             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_process_pending_loan.deductionperiodunkid AND modulerefid = 1 " & _
    '                             "JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_process_pending_loan.approverunkid " & _
    '                             "JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                             "JOIN hremployee_master AS LApp ON LApp.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                             "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                             "JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
    '                             "JOIN " & _
    '                             "( " & _
    '                             "      SELECT " & _
    '                             "           loan_amount " & _
    '                             "          ,installmentamt " & _
    '                             "          ,noofinstallment " & _
    '                             "          ,processpendingloanunkid " & _
    '                             "          ,ROW_NUMBER()OVER(PARTITION BY processpendingloanunkid ORDER BY priority DESC) AS Rno " & _
    '                             "      FROM lnloanapproval_process_tran " & _
    '                             "      WHERE isvoid = 0 And statusunkid = 2 " & _
    '                             ") " & _
    '                             "AS A ON A.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid AND A.Rno = 1 " & _
    '                             "LEFT JOIN " & _
    '                             "( " & _
    '                             "      SELECT " & _
    '                             "           stationunkid " & _
    '                             "          ,departmentunkid " & _
    '                             "          ,sectionunkid " & _
    '                             "          ,employeeunkid " & _
    '                             "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                             "      FROM hremployee_transfer_tran " & _
    '                             "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                             ") AS ETT ON ETT.employeeunkid = hremployee_master.employeeunkid AND ETT.rno = 1 " & _
    '                             "LEFT JOIN " & _
    '                             "( " & _
    '                             "      SELECT " & _
    '                             "           jobunkid " & _
    '                             "          ,employeeunkid " & _
    '                             "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                             "      FROM hremployee_categorization_tran " & _
    '                             "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                             ") AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 "

    '            'Nilay (15-Dec-2015) -- Start
    '            'OLD : CASE WHEN cfcommon_period_tran.statusid = 2 THEN 0 ELSE lnloan_process_pending_loan.deductionperiodunkid END deductionperiodunkid
    '            'NEW : CASE WHEN cfcommon_period_tran.statusid = 2 THEN @FirstOpenPeriodId ELSE lnloan_process_pending_loan.deductionperiodunkid END deductionperiodunkid
    '            'OLD : CASE WHEN cfcommon_period_tran.statusid = 2 THEN '' ELSE CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) END AS dstartdate
    '            'NEW : CASE WHEN cfcommon_period_tran.statusid = 2 THEN @FirstOpenPeriodStartDate ELSE CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) END AS dstartdate
    '            'Nilay (15-Dec-2015) -- End

    '            'Nilay (18-Dec-2015) -- End
    '            'Sohail (15 Dec 2015) - [interest_calctype_id]

    '            If xDateJoinQry.Trim.Length > 0 Then
    '                StrQ &= xDateJoinQry
    '            End If

    '            If xUACQry.Trim.Length > 0 Then
    '                StrQ &= xUACQry
    '            End If

    '            If xAdvanceJoinQry.Trim.Length > 0 Then
    '                StrQ &= xAdvanceJoinQry
    '            End If

    '            StrQ &= " WHERE lnloan_process_pending_loan.isvoid = 0 "

    '            If xUACFiltrQry.Trim.Length > 0 Then
    '                StrQ &= " AND " & xUACFiltrQry
    '            End If

    '            If xUACFiltrQry.Trim.Length > 0 Then
    '                StrQ &= " AND " & xUACFiltrQry.Replace("hremployee_master", "LApp")
    '            End If

    '            If xIncludeIn_ActiveEmployee = False Then
    '                If xDateFilterQry.Trim.Length > 0 Then
    '                    StrQ &= xDateFilterQry
    '                End If
    '            End If

    '            If xFilterString.Trim.Length > 0 Then
    '                StrQ &= " AND " & xFilterString
    '            End If

    '            StrQ &= ") AS ToAssign WHERE 1 = 1 AND StatusId = " & xStatusId

    '            Dim dsTrans As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            dtTable = New DataTable("List")

    '            dtTable.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            'S.SANDEEP [01 AUG 2015] -- START
    '            dtTable.Columns.Add("AppNo", System.Type.GetType("System.String")).DefaultValue = ""
    '            'S.SANDEEP [01 AUG 2015] -- END
    '            dtTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Employee", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Approver", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Amount", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("deductionperiod", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("duration", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("noofinstallment", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("installmentamt", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("rate", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("deductionperiodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("Pendingunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("IsEx", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("ApprId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("ischange", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            dtTable.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            dtTable.Columns.Add("dstartdate", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("calctypeId", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            dtTable.Columns.Add("mdate", System.Type.GetType("System.String")).DefaultValue = ""
    '            'Sohail (15 Dec 2015) -- Start
    '            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '            dtTable.Columns.Add("interest_calctype_id", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            'Sohail (15 Dec 2015) -- End
    '            '************************************************************************************************** MATCHING - START'
    '            dtTable.Columns.Add("instlnum", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            dtTable.Columns.Add("instlamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
    '            dtTable.Columns.Add("intrsamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
    '            dtTable.Columns.Add("netamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
    '            dtTable.Columns.Add("PaidExRate", System.Type.GetType("System.Decimal")).DefaultValue = 1
    '            dtTable.Columns.Add("orginstlamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
    '            '************************************************************************************************** MATCHING - END'

    '            If dsTrans.Tables("List").Rows.Count > 0 Then
    '                Dim dtRow As DataRow = Nothing
    '                Dim dtFilter As DataTable

    '                For Each dsRow As DataRow In dsScheme.Tables("List").Rows
    '                    dtFilter = New DataView(dsTrans.Tables("List"), "SchemeId = '" & dsRow.Item("loanschemeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

    '                    If dtFilter.Rows.Count > 0 Then
    '                        dtRow = dtTable.NewRow

    '                        dtRow.Item("ECode") = dsRow.Item("Schemes")
    '                        dtRow.Item("Employee") = dsRow.Item("Schemes")
    '                        dtRow.Item("IsGrp") = True
    '                        dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
    '                        dtRow.Item("calctypeId") = -1
    '                        'Sohail (15 Dec 2015) -- Start
    '                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '                        dtRow.Item("interest_calctype_id") = -1
    '                        'Sohail (15 Dec 2015) -- End
    '                        dtTable.Rows.Add(dtRow)

    '                        For Each dtlRow As DataRow In dtFilter.Rows
    '                            dtRow = dtTable.NewRow

    '                            'S.SANDEEP [01 AUG 2015] -- START
    '                            dtRow.Item("AppNo") = dtlRow.Item("AppNo")
    '                            'S.SANDEEP [01 AUG 2015] -- END

    '                            dtRow.Item("ECode") = Space(5) & dtlRow.Item("ECode")
    '                            dtRow.Item("Employee") = Space(5) & dtlRow.Item("EName")
    '                            dtRow.Item("Approver") = dtlRow.Item("Approver")
    '                            dtRow.Item("Amount") = Format(CDec(dtlRow.Item("Amount")), GUI.fmtCurrency)
    '                            dtRow.Item("Pendingunkid") = dtlRow.Item("PId")
    '                            dtRow.Item("IsEx") = dtlRow.Item("IsEx")
    '                            dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
    '                            dtRow.Item("ApprId") = dtlRow.Item("ApprId")
    '                            dtRow.Item("EmpId") = dtlRow.Item("EmpId")
    '                            dtRow.Item("deductionperiod") = dtlRow.Item("deductionperiod")
    '                            dtRow.Item("duration") = CInt(dtlRow.Item("duration"))
    '                            dtRow.Item("noofinstallment") = CInt(dtlRow.Item("noofinstallment"))
    '                            dtRow.Item("installmentamt") = Format(CDec(dtlRow.Item("installmentamt")), GUI.fmtCurrency)
    '                            dtRow.Item("rate") = dtlRow.Item("rate")
    '                            dtRow.Item("deductionperiodunkid") = dtlRow.Item("deductionperiodunkid")
    '                            dtRow.Item("countryunkid") = dtlRow.Item("countryunkid")
    '                            If dtlRow.Item("dstartdate").ToString.Length > 0 Then
    '                                dtRow.Item("dstartdate") = eZeeDate.convertDate(dtlRow.Item("dstartdate").ToString).ToShortDateString
    '                                dtRow.Item("mdate") = dtlRow.Item("dstartdate").ToString

    '                                Dim objExRate As New clsExchangeRate : Dim dsList As New DataSet
    '                                dsList = objExRate.GetList("ExRate", True, , , CInt(dtRow.Item("countryunkid")), True, eZeeDate.convertDate(dtlRow.Item("dstartdate").ToString).Date, True)
    '                                If dsList.Tables("ExRate").Rows.Count > 0 Then
    '                                    dtRow.Item("PaidExRate") = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
    '                                End If
    '                                objExRate = Nothing

    '                            End If
    '                            dtRow.Item("calctypeId") = dtlRow.Item("calctypeId")
    '                            'Sohail (15 Dec 2015) -- Start
    '                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    '                            dtRow.Item("interest_calctype_id") = dtlRow.Item("interest_calctype_id")
    '                            'Sohail (15 Dec 2015) -- End

    '                            dtRow.Item("instlnum") = CInt(dtlRow.Item("noofinstallment"))
    '                            dtRow.Item("instlamt") = CDec(dtlRow.Item("installmentamt"))
    '                            dtRow.Item("orginstlamt") = Format(CDec(dtlRow.Item("installmentamt")), GUI.fmtCurrency)
    '                            dtTable.Rows.Add(dtRow)
    '                        Next

    '                    End If

    '                Next

    '            End If
    '        End If

    '        Return dtTable

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetToAssignList", mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function
    'Nilay (01-Mar-2016) -- End

    'Public Function GetToAssignList(ByVal xStatusId As Integer, _
    '                                ByVal xDataBaseName As String, _
    '                                ByVal xUserId As Integer, _
    '                                ByVal xYearId As Integer, _
    '                                ByVal xCompanyId As Integer, _
    '                                ByVal xIncludeInactiveEmp As Boolean, _
    '                                ByVal xPeriodStartDate As DateTime, _
    '                                ByVal xPeriodEndDate As DateTime, _
    '                                ByVal xUserAccessFilterString As String, _
    '                                Optional ByVal xFilterString As String = "") As DataTable

    '    Dim dtTable As DataTable = Nothing
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation

    '        objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
    '        objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))

    '        '/* SCHEME MASTER LIST */
    '        StrQ = "SELECT " & _
    '               " loanschemeunkid AS loanschemeunkid " & _
    '               ",@Loan+' --> '+ISNULL(name,'') AS Schemes " & _
    '               "FROM lnloan_scheme_master " & _
    '               "UNION ALL " & _
    '               "SELECT " & _
    '               " 0 AS loanschemeunkid " & _
    '               ",@Advance AS Schemes "

    '        Dim dsScheme As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsScheme.Tables("List").Rows.Count > 0 Then
    '            '/* TRANSACTION LIST */
    '            StrQ = "SELECT " & _
    '                             " ECode AS ECode " & _
    '                             ",EName AS EName " & _
    '                             ",Mode AS Mode " & _
    '                             ",AppNo AS AppNo " & _
    '                             ",Amount AS Amount " & _
    '                             ",LScheme AS LScheme " & _
    '                             ",LApp AS Approver " & _
    '                             ",PId AS PId " & _
    '                             ",ModeId AS ModeId " & _
    '                             ",IsEx AS IsEx " & _
    '                             ",BranchId AS BranchId " & _
    '                             ",DeptId AS DeptId " & _
    '                             ",SectionId AS SectionId " & _
    '                             ",JobId AS JobId " & _
    '                             ",EmpId AS EmpId " & _
    '                             ",ApprId AS ApprId " & _
    '                             ",SchemeId AS SchemeId " & _
    '                             ",StatusId AS StatusId " & _
    '                             ",duration " & _
    '                             ",noofinstallment " & _
    '                             ",installmentamt " & _
    '                             ",deductionperiodunkid " & _
    '                             ",deductionperiod " & _
    '                             ",'' AS rate " & _
    '                             ",countryunkid AS countryunkid " & _
    '                             ",0 AS calctypeId " & _
    '                             ",dstartdate " & _
    '                        "FROM " & _
    '                        "( " & _
    '                        "SELECT " & _
    '                             " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
    '                             ",ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
    '                             ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
    '                             ",ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
    '                             ",ISNULL(lnloan_process_pending_loan.approved_amount,0) AS Amount " & _
    '                             ",lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
    '                             ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS ModeId " & _
    '                             ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan+' --> '+ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS LScheme " & _
    '                             ",ISNULL(LApp.firstname, '') + ' ' + ISNULL(LApp.othername,'') + ' '+ ISNULL(LApp.surname, '')+' -> '+ lnlevelname  AS LApp " & _
    '                             ",lnloan_process_pending_loan.isexternal_entity AS IsEx " & _
    '                             ",hremployee_master.stationunkid AS BranchId " & _
    '                             ",hremployee_master.departmentunkid  As DeptId " & _
    '                             ",hremployee_master.sectionunkid AS SectionId " & _
    '                             ",hremployee_master.jobunkid AS JobId " & _
    '                             ",hremployee_master.employeeunkid AS EmpId " & _
    '                             ",LApp.employeeunkid AS ApprId " & _
    '                             ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS SchemeId " & _
    '                             ",lnloan_process_pending_loan.loan_statusunkid AS StatusId " & _
    '                             ",lnloan_process_pending_loan.duration " & _
    '                             ",lnloan_process_pending_loan.noofinstallment " & _
    '                             ",lnloan_process_pending_loan.installmentamt " & _
    '                             ",CASE WHEN cfcommon_period_tran.statusid = 2 THEN 0 ELSE lnloan_process_pending_loan.deductionperiodunkid END deductionperiodunkid " & _
    '                             ",ISNULL(period_name,'') AS deductionperiod " & _
    '                             ",lnloan_process_pending_loan.countryunkid " & _
    '                             ",CASE WHEN cfcommon_period_tran.statusid = 2 THEN '' ELSE CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) END AS dstartdate " & _
    '                        "FROM lnloan_process_pending_loan " & _
    '                             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_process_pending_loan.deductionperiodunkid AND modulerefid = 1 " & _
    '                             "JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_process_pending_loan.approverunkid " & _
    '                             "JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                             "JOIN hremployee_master AS LApp ON LApp.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                             "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
    '                             "JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "WHERE lnloan_process_pending_loan.isvoid = 0 "

    '            If xIncludeInactiveEmp = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStartDate))
    '                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEndDate))
    '            End If

    '            If xFilterString.Trim.Length > 0 Then
    '                StrQ &= " AND " & xFilterString
    '            End If

    '            StrQ &= xUserAccessFilterString
    '            StrQ &= xUserAccessFilterString.Replace("hremployee_master", "LApp")

    '            StrQ &= ") AS ToAssign WHERE 1 = 1 AND StatusId = " & xStatusId

    '            Dim dsTrans As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            dtTable = New DataTable("List")

    '            dtTable.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            'S.SANDEEP [01 AUG 2015] -- START
    '            dtTable.Columns.Add("AppNo", System.Type.GetType("System.String")).DefaultValue = ""
    '            'S.SANDEEP [01 AUG 2015] -- END
    '            dtTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Employee", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Approver", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Amount", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("deductionperiod", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("duration", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("noofinstallment", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("installmentamt", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("rate", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("deductionperiodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("Pendingunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("IsEx", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("ApprId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            dtTable.Columns.Add("ischange", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            dtTable.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            dtTable.Columns.Add("dstartdate", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("calctypeId", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            dtTable.Columns.Add("mdate", System.Type.GetType("System.String")).DefaultValue = ""
    '            '************************************************************************************************** MATCHING - START'
    '            dtTable.Columns.Add("instlnum", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            dtTable.Columns.Add("instlamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
    '            dtTable.Columns.Add("intrsamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
    '            dtTable.Columns.Add("netamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
    '            dtTable.Columns.Add("PaidExRate", System.Type.GetType("System.Decimal")).DefaultValue = 1
    '            dtTable.Columns.Add("orginstlamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
    '            '************************************************************************************************** MATCHING - END'

    '            If dsTrans.Tables("List").Rows.Count > 0 Then
    '                Dim dtRow As DataRow = Nothing
    '                Dim dtFilter As DataTable

    '                For Each dsRow As DataRow In dsScheme.Tables("List").Rows
    '                    dtFilter = New DataView(dsTrans.Tables("List"), "SchemeId = '" & dsRow.Item("loanschemeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

    '                    If dtFilter.Rows.Count > 0 Then
    '                        dtRow = dtTable.NewRow

    '                        dtRow.Item("ECode") = dsRow.Item("Schemes")
    '                        dtRow.Item("Employee") = dsRow.Item("Schemes")
    '                        dtRow.Item("IsGrp") = True
    '                        dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
    '                        dtRow.Item("calctypeId") = -1
    '                        dtTable.Rows.Add(dtRow)

    '                        For Each dtlRow As DataRow In dtFilter.Rows
    '                            dtRow = dtTable.NewRow

    '                            'S.SANDEEP [01 AUG 2015] -- START
    '                            dtRow.Item("AppNo") = dtlRow.Item("AppNo")
    '                            'S.SANDEEP [01 AUG 2015] -- END

    '                            dtRow.Item("ECode") = Space(5) & dtlRow.Item("ECode")
    '                            dtRow.Item("Employee") = Space(5) & dtlRow.Item("EName")
    '                            dtRow.Item("Approver") = dtlRow.Item("Approver")
    '                            dtRow.Item("Amount") = Format(CDec(dtlRow.Item("Amount")), GUI.fmtCurrency)
    '                            dtRow.Item("Pendingunkid") = dtlRow.Item("PId")
    '                            dtRow.Item("IsEx") = dtlRow.Item("IsEx")
    '                            dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
    '                            dtRow.Item("ApprId") = dtlRow.Item("ApprId")
    '                            dtRow.Item("EmpId") = dtlRow.Item("EmpId")
    '                            dtRow.Item("deductionperiod") = dtlRow.Item("deductionperiod")
    '                            dtRow.Item("duration") = CInt(dtlRow.Item("duration"))
    '                            dtRow.Item("noofinstallment") = CInt(dtlRow.Item("noofinstallment"))
    '                            dtRow.Item("installmentamt") = Format(CDec(dtlRow.Item("installmentamt")), GUI.fmtCurrency)
    '                            dtRow.Item("rate") = dtlRow.Item("rate")
    '                            dtRow.Item("deductionperiodunkid") = dtlRow.Item("deductionperiodunkid")
    '                            dtRow.Item("countryunkid") = dtlRow.Item("countryunkid")
    '                            If dtlRow.Item("dstartdate").ToString.Length > 0 Then
    '                                dtRow.Item("dstartdate") = eZeeDate.convertDate(dtlRow.Item("dstartdate").ToString).ToShortDateString
    '                                dtRow.Item("mdate") = dtlRow.Item("dstartdate").ToString

    '                                Dim objExRate As New clsExchangeRate : Dim dsList As New DataSet
    '                                dsList = objExRate.GetList("ExRate", True, , , CInt(dtRow.Item("countryunkid")), True, eZeeDate.convertDate(dtlRow.Item("dstartdate").ToString).Date, True)
    '                                If dsList.Tables("ExRate").Rows.Count > 0 Then
    '                                    dtRow.Item("PaidExRate") = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
    '                                End If
    '                                objExRate = Nothing

    '                            End If
    '                            dtRow.Item("calctypeId") = dtlRow.Item("calctypeId")

    '                            dtRow.Item("instlnum") = CInt(dtlRow.Item("noofinstallment"))
    '                            dtRow.Item("instlamt") = CDec(dtlRow.Item("installmentamt"))
    '                            dtRow.Item("orginstlamt") = Format(CDec(dtlRow.Item("installmentamt")), GUI.fmtCurrency)
    '                            dtTable.Rows.Add(dtRow)
    '                        Next

    '                    End If

    '                Next

    '            End If
    '        End If

    '        Return dtTable

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetToAssignList", mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function
    'Nilay (10-Oct-2015) -- End

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>

    Public Function GetDataList(ByVal xDatabaseName As String, _
                                ByVal xUserUnkid As Integer, _
                                ByVal xYearUnkid As Integer, _
                                ByVal xCompanyUnkid As Integer, _
                                ByVal xPeriodStart As DateTime, _
                                ByVal xPeriodEnd As DateTime, _
                                ByVal xUserModeSetting As String, _
                                ByVal xOnlyApproved As Boolean, _
                                ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                ByVal strTableName As String, _
                                ByVal iApproverID As Integer, _
                                ByVal intModeId As Integer, _
                                Optional ByVal mstrFilter As String = "") As DataTable

        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception

        Try
            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))

            StrQ = "SELECT " & _
                    " loanschemeunkid AS loanschemeunkid " & _
                    ",@Loan+' --> '+ISNULL(name,'') AS Schemes " & _
                    "FROM lnloan_scheme_master " & _
                   "UNION ALL " & _
                    "SELECT " & _
                    " 0 AS loanschemeunkid " & _
                    ",@Advance AS Schemes "

            Dim dsScheme As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsScheme.Tables(0).Rows.Count > 0 Then

                'Nilay (21-Oct-2015) -- Start
                'ENHANCEMENT : NEW LOAN Given By Rutta 
                '              ADD Basic Salary Field & employeeunkid 
                'StrQ = "SELECT " & _
                '    "	 CAST(0 AS BIT) AS IsChecked " & _
                '    "	,ISNULL(hremployee_master.employeecode,'') AS ECode " & _
                '           "	,ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                '    "	,CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
                '    "	,ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
                '           "	,ISNULL(cfcommon_period_tran.period_name,'') AS DeductionPeriod " & _
                '           "	,ISNULL(lnloanapproval_process_tran.loan_amount,0) AS Amount " & _
                '           "	,ISNULL(lnloanapproval_process_tran.duration,0) AS duration " & _
                '           "	,ISNULL(lnloanapproval_process_tran.installmentamt,0) AS installmentamt " & _
                '           "	,ISNULL(lnloanapproval_process_tran.noofinstallment,0) AS noofinstallment " & _
                '    "	,ISNULL(lnloan_process_pending_loan.approved_amount,0) AS AppAmount " & _
                '    "	,lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
                '           "	,lnloanapproval_process_tran.pendingloantranunkid AS pendingloantranunkid " & _
                '    "   ,CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END AS ModeId " & _
                '    "   ,ISNULL(lnloan_process_pending_loan.loanschemeunkid,0) AS SchId " & _
                '           "   ,ISNULL(lnloanapproval_process_tran.deductionperiodunkid,0) AS DeductionPeriodID " & _
                '    "FROM lnloan_process_pending_loan " & _
                '           "    LEFT JOIN lnloanapproval_process_tran ON lnloanapproval_process_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid AND lnloanapproval_process_tran.isvoid = 0 " & _
                '           "    LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanapproval_process_tran.deductionperiodunkid " & _
                '           "	JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                '           "    JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapproval_process_tran.approvertranunkid "

                StrQ = "SELECT " & _
                    "	 CAST(0 AS BIT) AS IsChecked " & _
                    "   ,ISNULL(hremployee_master.employeeunkid,0) AS employeeunkid " & _
                    "	,ISNULL(hremployee_master.employeecode,'') AS ECode " & _
                    "	,ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                    "   ,ISNULL(SAL.BasicSal,0) AS BasicSal " & _
                    "	,CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
                    "	,ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
                    "	,ISNULL(cfcommon_period_tran.period_name,'') AS DeductionPeriod " & _
                    "	,ISNULL(lnloan_process_pending_loan.loan_amount,0) AS Amount " & _
                    "	,ISNULL(lnloanapproval_process_tran.duration,0) AS duration " & _
                    "	,ISNULL(lnloanapproval_process_tran.installmentamt,0) AS installmentamt " & _
                    "	,ISNULL(lnloanapproval_process_tran.noofinstallment,0) AS noofinstallment " & _
                    "	,ISNULL(lnloanapproval_process_tran.loan_amount,0) AS AppAmount " & _
                    "	,lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
                           "	,lnloanapproval_process_tran.pendingloantranunkid AS pendingloantranunkid " & _
                    "   ,CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END AS ModeId " & _
                    "   ,ISNULL(lnloan_process_pending_loan.loanschemeunkid,0) AS SchId " & _
                           "   ,ISNULL(lnloanapproval_process_tran.deductionperiodunkid,0) AS DeductionPeriodID " & _
                    "FROM lnloan_process_pending_loan " & _
                           "    LEFT JOIN lnloanapproval_process_tran ON lnloanapproval_process_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid AND lnloanapproval_process_tran.isvoid = 0 " & _
                           "    LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloanapproval_process_tran.deductionperiodunkid " & _
                           "	JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                           "    JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapproval_process_tran.approvertranunkid " & _
                           "    LEFT JOIN ( " & _
                           "                SELECT " & _
                           "                         ISNULL(prsalaryincrement_tran.newscale,0) AS BasicSal " & _
                           "                        ,employeeunkid " & _
                           "                        ,ROW_NUMBER () OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rnow " & _
                           "                FROM prsalaryincrement_tran where isvoid=0 and isapproved=1 " & _
                           "               ) AS SAL ON SAL.employeeunkid = hremployee_master.employeeunkid AND SAL.rnow=1 "


                'Nilay (15-Dec-2015) -- Start
                'ISNULL(lnloanapproval_process_tran.loan_amount,0) AS Amount -- REPLACED WITH -- ISNULL(lnloan_process_pending_loan.loan_amount,0) AS Amount
                'ISNULL(lnloan_process_pending_loan.approved_amount,0) AS AppAmount -- REPLACED WITH -- ISNULL(lnloanapproval_process_tran.loan_amount,0) AS AppAmount
                'Nilay (15-Dec-2015) -- End

                'Nilay (12-Dec-2015) -- Start
                '"    LEFT JOIN prsalaryincrement_tran ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " -- REMOVED
                '----------------------------------------------- ADDED ----------------------------------------------------------
                '"    LEFT JOIN ( " & _
                '"                SELECT " & _
                '"                         ISNULL(prsalaryincrement_tran.newscale,0) AS BasicSal " & _
                '"                        ,employeeunkid " & _
                '"                        ,ROW_NUMBER () OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rnow " & _
                '"                FROM prsalaryincrement_tran where isvoid=0 and isapproved=1 " & _
                '"               ) AS SAL ON SAL.employeeunkid = hremployee_master.employeeunkid AND SAL.rnow=1 "
                '-----------------------------------------------------------------------------------------------------------------
                'Nilay (12-Dec-2015) -- End

                'Nilay (21-Oct-2015) -- End

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= " WHERE  lnloan_process_pending_loan.isvoid = 0 AND lnloanapproval_process_tran.approvertranunkid = " & iApproverID & "  AND lnloanapproval_process_tran.statusunkid = 1 AND lnloanapproval_process_tran.visibleid = 1 " & _
                        " AND lnloan_approver_mapping.userunkid = " & xUserUnkid & " AND lnloan_process_pending_loan.loan_statusunkid = 1 "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If intModeId > -1 Then
                    Select Case intModeId
                        Case 0  'ADVANCE
                            StrQ &= " AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END = 0 "
                        Case 1  'LOAN
                            StrQ &= " AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END = 1 "
                    End Select
                End If

            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrFilter
            End If

            Dim dsTrans As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = New DataTable("List")

            dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            'Nilay (21-Oct-2015) -- End
            dtTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("EName", System.Type.GetType("System.String")).DefaultValue = ""
            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            dtTable.Columns.Add("BasicSal", System.Type.GetType("System.Decimal"))
            'Nilay (21-Oct-2015) -- End
            dtTable.Columns.Add("AppNo", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("Amount", System.Type.GetType("System.Decimal"))
            dtTable.Columns.Add("AppAmount", System.Type.GetType("System.Decimal"))
            dtTable.Columns.Add("DeductionPeriod", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("duration", System.Type.GetType("System.Int32"))
            dtTable.Columns.Add("installmentamt", System.Type.GetType("System.Decimal"))
            dtTable.Columns.Add("noofinstallment", System.Type.GetType("System.Int32"))
            dtTable.Columns.Add("DeductionPeriodID", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("PId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("pendingloantranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1

            If dsTrans.Tables(0).Rows.Count > 0 Then
                Dim dtRow As DataRow = Nothing

                Dim dtFilter As DataTable

                For Each dsRow As DataRow In dsScheme.Tables("List").Rows
                    dtFilter = New DataView(dsTrans.Tables("List"), "SchId = '" & dsRow.Item("loanschemeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

                    If dtFilter.Rows.Count > 0 Then
                        dtRow = dtTable.NewRow

                        dtRow.Item("EName") = dsRow.Item("Schemes")
                        dtRow.Item("IsGrp") = True
                        dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")

                        dtTable.Rows.Add(dtRow)

                        For Each dtlRow As DataRow In dtFilter.Rows
                            dtRow = dtTable.NewRow
                            'Nilay (21-Oct-2015) -- Start
                            'ENHANCEMENT : NEW LOAN Given By Rutta
                            dtRow.Item("employeeunkid") = dtlRow.Item("employeeunkid")
                            dtRow.Item("ECode") = dtlRow.Item("ECode")
                            'Nilay (21-Oct-2015) -- End
                            dtRow.Item("EName") = Space(5) & dtlRow.Item("EName")
                            'Nilay (21-Oct-2015) -- Start
                            'ENHANCEMENT : NEW LOAN Given By Rutta
                            dtRow.Item("BasicSal") = CDec(dtlRow.Item("BasicSal"))
                            'Nilay (21-Oct-2015) -- End
                            dtRow.Item("AppNo") = dtlRow.Item("AppNo")
                            dtRow.Item("DeductionPeriod") = dtlRow.Item("DeductionPeriod")
                            dtRow.Item("duration") = dtlRow.Item("duration")
                            dtRow.Item("installmentamt") = CDec(dtlRow.Item("installmentamt"))
                            dtRow.Item("noofinstallment") = dtlRow.Item("noofinstallment")
                            dtRow.Item("Amount") = CDec(dtlRow.Item("Amount"))
                            dtRow.Item("AppAmount") = CDec(dtlRow.Item("AppAmount"))
                            dtRow.Item("DeductionPeriodID") = dtlRow.Item("DeductionPeriodID")
                            dtRow.Item("PId") = dtlRow.Item("PId")
                            dtRow.Item("pendingloantranunkid") = dtlRow.Item("pendingloantranunkid")
                            dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
                            dtTable.Rows.Add(dtRow)
                        Next

                    End If
                Next

            End If

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDataList", mstrModuleName)
            Return Nothing
        Finally
            dtTable.Dispose()
        End Try
    End Function

    'Nilay (27-Dec-2016) -- Start
    'OPTIMIZATION: Sending email notification by Threading

    'Nilay (01 Feb 2017) -- Start
    'Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
    'Private Sub Send_Notification()
    '    Try
    '        If gobjEmailList.Count > 0 Then
    '            Dim objSendMail As New clsSendMail
    '            For Each objEmail In gobjEmailList
    '                objSendMail._ToEmail = objEmail._EmailTo
    '                objSendMail._Subject = objEmail._Subject
    '                objSendMail._Message = objEmail._Message
    '                objSendMail._Form_Name = objEmail._Form_Name
    '                objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
    '                objSendMail._OperationModeId = objEmail._OperationModeId
    '                objSendMail._UserUnkid = objEmail._UserUnkid
    '                objSendMail._SenderAddress = objEmail._SenderAddress
    '                objSendMail._ModuleRefId = objEmail._ModuleRefId

    '                objSendMail.SendMail()
    '            Next
    '            gobjEmailList.Clear()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub Send_Notification(ByVal intCompanyUnkid As Object)
        'Sohail (30 Nov 2017) - [intCompanyUnkid]
        Try
            If objEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In objEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'If objSendMail.SendMail().ToString.Length > 0 Then
                    'Sohail (13 Dec 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'If objSendMail.SendMail(CInt(intCompanyUnkid)).ToString.Length > 0 Then
                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyUnkid Is Integer Then
                        intCUnkId = intCompanyUnkid
                    Else
                        intCUnkId = CInt(intCompanyUnkid(0))
                    End If
                    If objSendMail.SendMail(intCUnkId).ToString.Length > 0 Then
                        'Sohail (13 Dec 2017) -- End
                        'Sohail (30 Nov 2017) -- End

                        'Varsha Rana (12-Sept-2017) -- Start
                        'Enhancement - Loan Topup in ESS
                        'objEmailList.Remove(objEmail)
                        ' Varsha Rana (12-Sept-2017) -- End


                        Continue For
                    End If
                Next
                objEmailList.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'Nilay (01 Feb 2017) -- End


    'Nilay (27-Dec-2016) -- End

    'Shani (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification
    ''' <summary>
    ''' Modify By: Shani
    ''' </summary>
    ''' <purpose> Send Loan Approver Notification </purpose>
    ''' 
    Public Sub Send_Notification_Approver(ByVal mblnLoanApproverForLoanScheme As Boolean, _
                                          ByVal intSchemeId As Integer, _
                                          ByVal iEmployeeId As Integer, _
                                          ByVal intCurrentPriority As Integer, _
                                          ByVal enEmailType As enApproverEmailType, _
                                          ByVal blnIsDeleteApplication As Boolean, _
                                          ByVal strUnkId As String, _
                                          ByVal strArutiSelfServiceURL As String, _
                                          ByVal intCompanyUnkid As Integer, _
                                          Optional ByVal blnIsLoanApplication As Boolean = False, _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0, _
                                          Optional ByVal blnIsSendMail As Boolean = True, _
                                          Optional ByRef lstEmailList As List(Of clsEmailCollection) = Nothing _
                                        )
        'Hemant (30 Aug 2019) -- [blnIsSendMail, lstEmailList]
        'Sohail (30 Nov 2017) - [intCompanyUnkid]
        Dim dtApprover As DataTable = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Dim blnIsLoanAppliction As Boolean = False
        Try
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            Select Case enEmailType
                Case enApproverEmailType.Loan_Approver, enApproverEmailType.Loan_Advance
                    blnIsLoanAppliction = True
                Case enApproverEmailType.Loan_Installment, enApproverEmailType.Loan_Rate, enApproverEmailType.Loan_Topup
                    blnIsLoanAppliction = False
            End Select


            dtApprover = (New clsLoanApprover_master).GetEmailNotification(strUnkId, iEmployeeId, mblnLoanApproverForLoanScheme, blnIsLoanApplication, intSchemeId)

            Dim intMinPriority As Integer

            If intCurrentPriority <= -1 Then
                intMinPriority = dtApprover.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Min()
            Else
                intMinPriority = dtApprover.AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") > intCurrentPriority AndAlso x.Field(Of Integer)("statusunkid") = 1) _
                                                          .Select(Function(x) x.Field(Of Integer)("priority")).Min()
            End If

            If intMinPriority <= -1 Then Exit Sub
            Dim strSubject As String = ""
            Dim strOtherLoanType As String = ""

            Select Case enEmailType
                Case enApproverEmailType.Loan_Approver
                    If blnIsDeleteApplication Then
                        strSubject = Language.getMessage(mstrModuleName, 11, "Loan Application Deleted")
                    Else
                        strSubject = Language.getMessage(mstrModuleName, 12, "Notification for Approving Loan Application")
                    End If
                Case enApproverEmailType.Loan_Installment
                    strSubject = Language.getMessage(mstrModuleName, 13, "Notification for Approving Loan Installment")
                    strOtherLoanType = Language.getMessage(mstrModuleName, 14, "Installment")
                Case enApproverEmailType.Loan_Rate
                    strSubject = Language.getMessage(mstrModuleName, 15, "Notification for Approving Loan Interest Rate")
                    strOtherLoanType = Language.getMessage(mstrModuleName, 16, "Intrest Rate")
                Case enApproverEmailType.Loan_Topup
                    strSubject = Language.getMessage(mstrModuleName, 17, "Notification for Approving Loan Topup")
                    strOtherLoanType = Language.getMessage(mstrModuleName, 18, "Topup")
                Case enApproverEmailType.Loan_Advance
                    If blnIsDeleteApplication Then
                        strSubject = Language.getMessage(mstrModuleName, 78, "Advance Application Deleted")
                    Else
                        strSubject = Language.getMessage(mstrModuleName, 79, "Notification for Approving Advance Application")
                    End If
            End Select

            Dim objMail As New clsSendMail
            'Dim dRow = dtApprover.AsEnumerable().Where(Function(X) X.Field(Of Integer)("priority") = intMinPriority).Distinct
            For Each dtRow As DataRow In dtApprover.Select("priority = " & intMinPriority).Distinct
                If dtRow.Item("app_email") = "" Then Continue For
                Dim strMessage As String = ""
                Dim strContain As String = ""

                objMail._Subject = strSubject

                strMessage = "<HTML> <BODY>"
                strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("app_name").ToString() & ", <BR><BR>"

                Select Case enEmailType
                    Case enApproverEmailType.Loan_Approver
                        If blnIsDeleteApplication Then

                            'Gajanan [27-Mar-2019] -- Start
                            'Enhancement - Change Email Language
                            'strContain &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                            '              Language.getMessage(mstrModuleName, 20, "This is to inform you that Loan application #") & _
                            strContain &= Language.getMessage(mstrModuleName, 20, "This is to inform you that Loan application #") & _
                                          " " & dtRow.Item("application_no") & _
                                          Language.getMessage(mstrModuleName, 86, " with Loan Scheme :") & _
                                          " " & dtRow.Item("scheme_name") & Language.getMessage(mstrModuleName, 22, " for Employee ") & _
                                          " " & dtRow.Item("emp_code") & " - " & dtRow.Item("emp_name") & _
                                          Language.getMessage(mstrModuleName, 23, " who was seeking your approval has been deleted. Kindly request to take a note of it.")
                            'Gajanan [27-Mar-2019] -- End

                        Else

                            'Gajanan [27-Mar-2019] -- Start
                            'Enhancement - Change Email Language

                            'strContain &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                            '              Language.getMessage(mstrModuleName, 24, "This is the notification for approving Loan application #") & _
                            strContain &= Language.getMessage(mstrModuleName, 24, "This is the notification for approving Loan application #") & _
                                          " " & dtRow.Item("application_no") & _
                                          Language.getMessage(mstrModuleName, 87, " with Loan Scheme :") & _
                                          " " & dtRow.Item("scheme_name") & Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                                          " " & dtRow.Item("emp_code") & "-" & dtRow.Item("emp_name") & _
                                          Language.getMessage(mstrModuleName, 27, " application date : ") & _
                                          " " & eZeeDate.convertDate(dtRow.Item("application_date").ToString).ToShortDateString & "."
                            'Gajanan [27-Mar-2019] -- End

                            strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApproval.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                                                             dtRow.Item("pendingloantranunkid").ToString & "|" & _
                                                                                                                                                             dtRow.Item("approverunkid").ToString & "|" & _
                                                                                                                                                             dtRow.Item("processpendingloanunkid").ToString & "|" & _
                                                                                                                                                            intCompanyUnkid & "|" & _
                                                                                                                                                             dtRow.Item("userunkid").ToString.Trim))
                            'Hemant (30 Aug 2019) -- [Company._Object._Companyunkid --> intCompanyUnkid]	

                            'Gajanan [27-Mar-2019] -- Start
                            'Enhancement - Change Email Language

                            'strContain &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                            '              Language.getMessage(mstrModuleName, 28, "Please click on the following link to approve Loan.") & _
                            '              "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" _
                            '              & strLink & "</a>"

                            strContain &= "<BR></BR><BR></BR>" & _
                                          Language.getMessage(mstrModuleName, 28, "Please click on the following link to approve Loan.") & _
                                         "<BR></BR><a href='" & strLink & "'>" _
                                          & strLink & "</a>"
                            'Gajanan [27-Mar-2019] -- End


                        End If
                    Case enApproverEmailType.Loan_Installment, enApproverEmailType.Loan_Rate, enApproverEmailType.Loan_Topup

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language

                        'strContain &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                        '              Language.getMessage(mstrModuleName, 29, "This is the notification for approving Voucher #") & _

                        strContain &= Language.getMessage(mstrModuleName, 29, "This is the notification for approving Voucher #") & _
                                      " " & dtRow.Item("loanvoucher_no") & _
                                      Language.getMessage(mstrModuleName, 21, " with Loan Scheme :") & _
                                      " " & dtRow.Item("scheme_name") & Language.getMessage(mstrModuleName, 31, " for ") & strOtherLoanType & Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                                      " " & dtRow.Item("emp_code") & " - " & dtRow.Item("emp_name") & "."
                        'Gajanan [27-Mar-2019] -- End

                        strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Assignment/wPg_LoanAdvanceOperationList.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                                                         dtRow.Item("emp_code").ToString & "-" & dtRow.Item("emp_name").ToString & "|" & _
                                                                                                                                                         dtRow.Item("scheme_name").ToString & "|" & _
                                                                                                                                                         dtRow.Item("loanvoucher_no").ToString & "|" & _
                                                                                                                                                         dtRow.Item("loanadvancetranunkid").ToString & "|" & _
                                                                                                                                                         dtRow.Item("lnotheroptranunkid").ToString() & "|" & _
                                                                                                                                 intCompanyUnkid & "|" & _
                                                                                                                                                         dtRow.Item("userunkid").ToString.Trim))
                        'Hemant (30 Aug 2019) -- [Company._Object._Companyunkid --> intCompanyUnkid]	

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language

                        'strContain &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                        '              Language.getMessage(mstrModuleName, 33, "Please click on the following link to approve ") & strOtherLoanType & "." & _
                        '              "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" _
                        '              & strLink & "</a>"

                        strContain &= "<BR></BR><BR></BR>" & _
                                      Language.getMessage(mstrModuleName, 33, "Please click on the following link to approve ") & strOtherLoanType & "." & _
                                      "<BR></BR><a href='" & strLink & "'>" _
                                      & strLink & "</a>"

                        'Gajanan [27-Mar-2019] -- End


                    Case enApproverEmailType.Loan_Advance
                        If blnIsDeleteApplication Then

                            'Gajanan [27-Mar-2019] -- Start
                            'Enhancement - Change Email Language

                            'strContain &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                            '              Language.getMessage(mstrModuleName, 80, "This is to inform you that Advance application #") & _

                            strContain &= Language.getMessage(mstrModuleName, 80, "This is to inform you that Advance application #") & _
                                          " " & dtRow.Item("application_no") & _
                                          Language.getMessage(mstrModuleName, 22, " for Employee ") & _
                                          " " & dtRow.Item("emp_code") & "-" & dtRow.Item("emp_name") & _
                                          Language.getMessage(mstrModuleName, 23, " who was seeking your approval has been deleted. Kindly request to take a note of it.")
                            'Gajanan [27-Mar-2019] -- End

                        Else

                            'Gajanan [27-Mar-2019] -- Start
                            'Enhancement - Change Email Language

                            'strContain &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                            '              Language.getMessage(mstrModuleName, 52, "This is the notification for approving Advance application #") & _


                            strContain &= Language.getMessage(mstrModuleName, 52, "This is the notification for approving Advance application #") & _
                                          " " & dtRow.Item("application_no") & _
                                          Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                                          " " & dtRow.Item("emp_code") & "-" & dtRow.Item("emp_name") & _
                                          Language.getMessage(mstrModuleName, 27, " application date : ") & _
                                          " " & eZeeDate.convertDate(dtRow.Item("application_date").ToString).ToShortDateString & "."
                            'Gajanan [27-Mar-2019] -- End

                            strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApproval.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                                                             dtRow.Item("pendingloantranunkid").ToString & "|" & _
                                                                                                                                                             dtRow.Item("approverunkid").ToString & "|"))
                            'Gajanan [27-Mar-2019] -- Start
                            'Enhancement - Change Email Language

                            'strContain &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                            '              Language.getMessage(mstrModuleName, 53, "Please click on the following link to approve Advance.") & _
                            '              "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"

                            strContain &= "<BR></BR><BR></BR>" & _
                                          Language.getMessage(mstrModuleName, 53, "Please click on the following link to approve Advance.") & _
                                          "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                            'Gajanan [27-Mar-2019] -- End
                        End If


                End Select

                strMessage &= strContain

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                'Gajanan [27-Mar-2019] -- End

                strMessage &= "</BODY></HTML>"

                objMail._Message = strMessage
                objMail._ToEmail = dtRow.Item("app_email")
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFormName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFormName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                objMail._SenderAddress = IIf(dtRow.Item("app_email").ToString = "", dtRow.Item("app_name").ToString, dtRow.Item("app_email").ToString)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
                'Nilay (27-Dec-2016) -- Start
                'OPTIMIZATION: Sending email notification by Threading
                'objMail.SendMail()

                'Varsha Rana (12-Sept-2017) -- Start
                'Enhancement - Loan Topup in ESS
                'Dim objUser As New clsUserAddEdit
                'objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                'Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                '                                           mstrWebClientIP, mstrWebHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.LOAN_MGT, _
                '                                           IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                ''Nilay (01 Feb 2017) -- Start
                ''Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
                ''gobjEmailList.Add(objEmailColl)
                'objEmailList.Add(objEmailColl)
                ''Nilay (01 Feb 2017) -- End

                'objUser = Nothing
                ''Nilay (27-Dec-2016) -- End
                If iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE Then
                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = iLoginEmployeeId
                    Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                                                               mstrWebClientIP, mstrWebHostName, 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.LOAN_MGT, _
                                                               IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email))

                    objEmailList.Add(objEmailColl)


                    objEmp = Nothing

                Else
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                                                           mstrWebClientIP, mstrWebHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.LOAN_MGT, _
                                                           IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                objEmailList.Add(objEmailColl)

                objUser = Nothing

                End If


                ' Varsha Rana (12-Sept-2017) -- End


                
            Next
            'Nilay (27-Dec-2016) -- Start
            'OPTIMIZATION: Sending email notification by Threading
            'Nilay (01 Feb 2017) -- Start
            'Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
            If blnIsSendMail = True Then 'Hemant (30 Aug 2019) -- End

            If objEmailList.Count > 0 Then
            If HttpContext.Current Is Nothing Then
                objThread = New Thread(AddressOf Send_Notification)
                objThread.IsBackground = True
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objThread.Start()
                    Dim arr(1) As Object
                    arr(0) = intCompanyUnkid
                    objThread.Start(arr)
                    'Sohail (30 Nov 2017) -- End
            Else
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'Call Send_Notification()
                    Call Send_Notification(intCompanyUnkid)
                    'Sohail (30 Nov 2017) -- End
            End If
            End If
                'Hemant (30 Aug 2019) -- Start
                'ISSUE#0004110(ZURI) :  Error on global assigning loans..
            Else
                lstEmailList = objEmailList
            End If 'Hemant (30 Aug 2019) -- End

            'Nilay (01 Feb 2017) -- End
            'Nilay (27-Dec-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Approver", mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Shani
    ''' </summary>
    ''' <purpose> Send Loan Approver Notification </purpose>
    ''' <param name="blnIsSendMail">Pass False If you want to add employee email to Email Collection along with other Send_Email Method and make sure that Send_Notification_Employee is called before any sending mail method.</param>

    Public Sub Send_Notification_Employee(ByVal iEmployeeId As Integer, _
                                          ByVal iloanUnkId As Integer, _
                                          ByVal enLoanStatus As enNoticationLoanStatus, _
                                          ByVal enEmailType As enApproverEmailType, _
                                          ByVal dtEmployeeAsOnDate As Date, _
                                          ByVal intCompanyUnkId As Integer, _
                                          Optional ByVal strRemark As String = "", _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0, _
                                          Optional ByVal blnIsSendMail As Boolean = True)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        'Nilay (27-Dec-2016) -- [blnIsSendMail]

        Dim dtApprover As DataTable = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub
            Dim objEmp As New clsEmployee_Master
            Dim strRefno As String = ""

            Select Case enEmailType
                'Nilay (23-Aug-2016) -- Start
                'Enhancement - Create New Loan Notification 
                'Case enApproverEmailType.Loan_Advance, enApproverEmailType.Loan_Approver
                '    mintProcesspendingloanunkid = iloanUnkId
                '    Call GetData()
                '    strRefno = mstrApplication_No
                Case enApproverEmailType.Loan_Advance, enApproverEmailType.Loan_Approver
                    If enLoanStatus = enNoticationLoanStatus.DELETE_ASSIGNED Then
                        Dim objLoanAdv As New clsLoan_Advance
                        objLoanAdv._Loanadvancetranunkid = iloanUnkId
                        strRefno = objLoanAdv._Loanvoucher_No
                    Else
                    mintProcesspendingloanunkid = iloanUnkId
                    Call GetData()
                    strRefno = mstrApplication_No
                    End If
                    'Nilay (23-Aug-2016) -- End
                Case enApproverEmailType.Loan_Installment, enApproverEmailType.Loan_Rate, enApproverEmailType.Loan_Topup
                    Dim objLnAdvance As New clsLoan_Advance
                    objLnAdvance._Loanadvancetranunkid = iloanUnkId
                    strRefno = objLnAdvance._Loanvoucher_No
            End Select

            objEmp._Employeeunkid(dtEmployeeAsOnDate) = iEmployeeId
            
            Dim objMail As New clsSendMail

            If objEmp._Email = "" Then Exit Sub
            Dim strMessage As String = ""
            Dim strSubject As String = ""

            'Nilay (27-Dec-2016) -- Start
            'If enEmailType <> enApproverEmailType.Loan_Advance Then
            '    strSubject = Language.getMessage(mstrModuleName, 34, "Loan Status Notification")
            'Else
            '    strSubject = Language.getMessage(mstrModuleName, 54, "Advance Status Notification")
            'End If
            Select Case enEmailType
                Case enApproverEmailType.Loan_Approver
                strSubject = Language.getMessage(mstrModuleName, 34, "Loan Status Notification")
                Case enApproverEmailType.Loan_Advance
                strSubject = Language.getMessage(mstrModuleName, 54, "Advance Status Notification")
                Case enApproverEmailType.Loan_Rate
                    strSubject = Language.getMessage(mstrModuleName, 105, "Loan Rate Status Notification")
                Case enApproverEmailType.Loan_Installment
                    strSubject = Language.getMessage(mstrModuleName, 106, "Loan Installment Status Notification")
                Case enApproverEmailType.Loan_Topup
                    strSubject = Language.getMessage(mstrModuleName, 107, "Loan Topup Status Notification")
            End Select
            'Nilay (27-Dec-2016) -- End

            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & objEmp._Firstname & "  " & objEmp._Surname & ", <BR><BR>"

            'Nilay (27-Dec-2016) -- Start
            'If enEmailType <> enApproverEmailType.Loan_Advance Then
            '    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 36, " This is to inform you that, the application you have applied for loan with") & " "
            'Else
            '    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 55, " This is to inform you that, the application you have applied for advance with") & " "
            'End If
            Select Case enEmailType
                Case enApproverEmailType.Loan_Approver
                strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 36, " This is to inform you that, the application you have applied for loan with") & " "
                Case enApproverEmailType.Loan_Advance
                strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 55, " This is to inform you that, the application you have applied for advance with") & " "
                Case enApproverEmailType.Loan_Rate
                    strMessage &= Language.getMessage(mstrModuleName, 108, " This is to inform you that, the other operation of Rate you have applied for loan with") & " "
                Case enApproverEmailType.Loan_Installment
                    strMessage &= Language.getMessage(mstrModuleName, 109, " This is to inform you that, the other operation of Installment you have applied for loan with") & " "
                Case enApproverEmailType.Loan_Topup
                    strMessage &= Language.getMessage(mstrModuleName, 110, " This is to inform you that, the other operation of Topup you have applied for loan with") & " "
            End Select
            'Nilay (27-Dec-2016) -- End

            Select Case enEmailType
                Case enApproverEmailType.Loan_Advance, enApproverEmailType.Loan_Approver
                    If enLoanStatus = enNoticationLoanStatus.DELETE_ASSIGNED Then
                        strMessage &= Language.getMessage(mstrModuleName, 57, " Voucher number")
                    Else
                    strMessage &= Language.getMessage(mstrModuleName, 56, "Application number ")
                    End If
                Case enApproverEmailType.Loan_Installment, enApproverEmailType.Loan_Rate, enApproverEmailType.Loan_Topup
                    strMessage &= Language.getMessage(mstrModuleName, 57, " Voucher number")
            End Select

            strMessage &= " " & "<b>" & strRefno & "</b>"

            strMessage &= Language.getMessage(mstrModuleName, 50, " has been ")

            Select Case enLoanStatus
                Case enNoticationLoanStatus.APPROVE
                    strMessage &= Language.getMessage(mstrModuleName, 6, "Approved")
                Case enNoticationLoanStatus.REJECT
                    strMessage &= Language.getMessage(mstrModuleName, 7, "Rejected")
                Case enNoticationLoanStatus.ASSIGN
                    strMessage &= Language.getMessage(mstrModuleName, 10, "Assigned")
                    'Nilay (23-Aug-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                Case enNoticationLoanStatus.DELETE_ASSIGNED
                    strMessage &= Language.getMessage(mstrModuleName, 72, "Deleted")
                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'strMessage &= "<BR></BR>" & Language.getMessage(mstrModuleName, 73, "Remarks by Assigner : ") & strRemark
                    'Nilay (20-Sept-2016) -- End
                    'Nilay (23-Aug-2016) -- End

                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                Case enNoticationLoanStatus.CANCELLED
                    strMessage &= Language.getMessage(mstrModuleName, 96, "Cancelled")
                    'Nilay (20-Sept-2016) -- End
            End Select
            strMessage &= "."
            If enLoanStatus = enNoticationLoanStatus.REJECT Then
                strMessage &= " " & Language.getMessage(mstrModuleName, 41, "Please refer to the comments below for the same.")
                strMessage &= "<BR></BR>"
                strMessage &= " " & Language.getMessage(mstrModuleName, 42, "Remarks/Comments:") & " " & ChrW(34) & strRemark & ChrW(34)
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
            ElseIf enLoanStatus = enNoticationLoanStatus.DELETE_ASSIGNED Then
                strMessage &= " " & Language.getMessage(mstrModuleName, 41, "Please refer to the comments below for the same.")
                strMessage &= "<BR></BR>"
                strMessage &= " " & Language.getMessage(mstrModuleName, 73, "Remarks by Assigner: ") & " " & ChrW(34) & strRemark & ChrW(34)
            ElseIf enLoanStatus = enNoticationLoanStatus.CANCELLED Then
                strMessage &= " " & Language.getMessage(mstrModuleName, 41, "Please refer to the comments below for the same.")
                strMessage &= "<BR></BR>"
                strMessage &= " " & Language.getMessage(mstrModuleName, 97, "Cancel Remarks: ") & " " & ChrW(34) & strRemark & ChrW(34)
                'Nilay (20-Sept-2016) -- End
            End If
            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"

            strMessage &= "</BODY></HTML>"
            objMail._Subject = strSubject
            objMail._Message = strMessage
            objMail._ToEmail = objEmp._Email
            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFormName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            objMail._SenderAddress = objEmp._Email
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
            'Nilay (27-Dec-2016) -- Start
            'OPTIMIZATION: Sending email notification by Threading
            'objMail.SendMail()
            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                                                       mstrWebClientIP, mstrWebHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.LOAN_MGT, _
                                                       IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

            'Nilay (01 Feb 2017) -- Start
            'Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
            'gobjEmailList.Add(objEmailColl)
            objEmailList.Add(objEmailColl)
            'Nilay (01 Feb 2017) -- End

            objUser = Nothing

            'Nilay (01 Feb 2017) -- Start
            'Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
            'If blnIsSendMail = True Then
            '    If HttpContext.Current Is Nothing Then
            '        objThread = New Thread(AddressOf Send_Notification)
            '        objThread.IsBackground = True
            '        objThread.Start()
            '    Else
            '        Call Send_Notification()
            '    End If
            'End If
            If blnIsSendMail = True AndAlso objEmailList.Count > 0 Then
                If HttpContext.Current Is Nothing Then
                    objThread = New Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objThread.Start()
                    Dim arr(1) As Object
                    arr(0) = intCompanyUnkId
                    objThread.Start(arr)
                    'Sohail (30 Nov 2017) -- End
                Else
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'Call Send_Notification()
                    Call Send_Notification(intCompanyUnkId)
                    'Sohail (30 Nov 2017) -- End
                End If
            End If
            'Nilay (01 Feb 2017) -- End

            'Nilay (27-Dec-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Shani
    ''' </summary>
    ''' <purpose> Send Loan Approver Notification </purpose>
    ''' 
    Public Sub Send_Notification_Assign(ByVal iEmployeeId As Integer, _
                                        ByVal intYearUnkId As Integer, _
                                        ByVal strArutiSelfServiceURL As String, _
                                        ByVal intProcessPendingUnkid As Integer, _
                                        ByVal xDatabaseName As String, _
                                        ByVal xCompanyUnkid As Integer, _
                                        ByVal xEmployeeAsOnDate As Date, _
                                        ByVal xUserModeSettings As String, _
                                        ByVal xIncludeInActiveEmp As Boolean, _
                                        ByVal blnIsDeleteAssigned As Boolean, _
                                        Optional ByVal iLoginTypeId As Integer = 0, _
                                        Optional ByVal iLoginEmployeeId As Integer = 0, _
                                        Optional ByVal iUserId As Integer = 0, _
                                        Optional ByVal strAssignerRemarks As String = "", _
                                        Optional ByVal xNotifyLoanAdvanceUsers As String = "" _
                                        )
        'Hemant (30 Aug 2019) -- [xNotifyLoanAdvanceUsers]
        'Nilay (23-Aug-2016) -- [intPendingTranUnkid REPLACED BY intProcessPendingUnkid], [blnIsDeleteAssigned,strAssignerRemarks]

        Dim dsAssignList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim objApprovalTran As New clsloanapproval_process_Tran
        Dim dsList As DataSet = Nothing
        Dim strUserIds As String = ""
        'Nilay (23-Aug-2016) -- Start
        'Enhancement - Create New Loan Notification 
        Dim intPendingApprovalTranID As Integer = 0
        'Nilay (23-Aug-2016) -- End

        objDataOperation = New clsDataOperation
        Try
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            'Hemant (30 Aug 2019) -- Start
            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
            'Dim strArray() As String = ConfigParameter._Object._Notify_LoanAdvance_Users.Split(CChar("||"))
            Dim strArray() As String = xNotifyLoanAdvanceUsers.Split(CChar("||"))
            'Hemant (30 Aug 2019) -- End
            If strArray.Length > 1 Then
                strUserIds = strArray(2).ToString
            End If

            If strUserIds.ToString = "" Then Exit Sub

            dsAssignList = (New clsUserAddEdit).Get_UserBy_PrivilegeId(253, intYearUnkId)

            'Nilay (23-Aug-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'dsList = objApprovalTran.GetApprovalTranList(xDatabaseName, iUserId, intYearUnkId, xCompanyUnkid, _
            '                                           xEmployeeAsOnDate, xEmployeeAsOnDate, xUserModeSettings, _
            '                                           True, xIncludeInActiveEmp, "List", iEmployeeId, _
            '                                           0, "lnloanapproval_process_tran.pendingloantranunkid = '" & intPendingTranUnkid & "'")
            dsList = objApprovalTran.GetApprovalTranList(xDatabaseName, iUserId, intYearUnkId, xCompanyUnkid, _
                                                         xEmployeeAsOnDate, xEmployeeAsOnDate, xUserModeSettings, _
                                                         True, xIncludeInActiveEmp, "List", iEmployeeId, _
                                                        intProcessPendingUnkid, "lnloanapproval_process_tran.statusunkid = 2")

            Dim dtList As DataTable = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dtList = New DataView(dsList.Tables(0), "", "priority desc", DataViewRowState.CurrentRows).ToTable
                If dtList.Rows.Count > 0 Then
                    intPendingApprovalTranID = CInt(dtList.Rows(0)("pendingloantranunkid"))
                End If
            End If
            dtList = Nothing
            'Nilay (23-Aug-2016) -- End

            Dim dRow As DataRow = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dRow = dsList.Tables(0).Rows(0)
            End If
            If dRow Is Nothing Then Exit Sub

            Dim objMail As New clsSendMail

            'Nilay (18-Oct-2016) -- Start
            If dsAssignList.Tables(0).Rows.Count > 0 Then
                'Nilay (18-Oct-2016) -- End
            For Each dtRow As DataRow In dsAssignList.Tables(0).Select("UId IN(" & strUserIds & ") ")
                If dtRow.Item("UEmail") = "" Then Continue For
                Dim strMessage As String = ""

                'Nilay (23-Aug-2016) -- Start
                'Enhancement - Create New Loan Notification 
                'objMail._Subject = Language.getMessage(mstrModuleName, 43, "Notification for Assign Loan Application.")
                'strMessage = "<HTML> <BODY>"
                'strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("UName").ToString() & ", <BR><BR>"
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                '                          Language.getMessage(mstrModuleName, 45, "This is the notification for assign Loan application #") & _
                '                          " " & dRow.Item("application_no") & _
                '                          Language.getMessage(mstrModuleName, 21, " with loan scheme :") & _
                '                          " " & dRow.Item("Scheme") & Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                '                          " " & dRow.Item("Employee") & _
                '                          Language.getMessage(mstrModuleName, 27, " application date : ") & _
                '                          " " & CDate(dRow.Item("application_date")).ToShortDateString & "."

                'strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx?" & _
                '                                   HttpUtility.UrlEncode(clsCrypto.Encrypt(intPendingTranUnkid & "|" & iEmployeeId & "|" & _
                '                                                                           Company._Object._Companyunkid & "|" & _
                '                                                                           dtRow.Item("UId").ToString.Trim))

                'strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                '              Language.getMessage(mstrModuleName, 49, "Please click on the following link to assign Loan.") & _
                '              "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" _
                '              & strLink & "</a>"

                'strMessage &= "</BODY></HTML>"

                If blnIsDeleteAssigned = False Then

                        'Nilay (08-Dec-2016) -- Start
                        'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
                        'objMail._Subject = Language.getMessage(mstrModuleName, 43, "Notification for Assign Loan/Advance Application.")
                        'strMessage = "<HTML> <BODY>"
                        'strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("UName").ToString() & ", <BR><BR>"
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        'Language.getMessage(mstrModuleName, 45, "This is the notification for assign Loan application #") & _
                        '                             " " & "<B>" & dRow.Item("application_no") & "</B>" & _
                        '                             Language.getMessage(mstrModuleName, 88, " with Loan scheme :") & _
                        '                         " " & dRow.Item("Scheme") & Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                        '                         " " & dRow.Item("Employee") & _
                        '                         Language.getMessage(mstrModuleName, 27, " application date : ") & _
                        '                         " " & CDate(dRow.Item("application_date")).ToShortDateString & "."
                        'strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx?" & _
                        '                                      HttpUtility.UrlEncode(clsCrypto.Encrypt(intPendingApprovalTranID & "|" & iEmployeeId & "|" & _
                        '                                                                          Company._Object._Companyunkid & "|" & _
                        '                                                                          dtRow.Item("UId").ToString.Trim))

                        'strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                        '              Language.getMessage(mstrModuleName, 49, "Please click on the following link to assign Loan.") & _
                        '              "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" _
                        '              & strLink & "</a>"

                        'strMessage &= "</BODY></HTML>"

                        If CBool(dRow.Item("isloan")) = True Then
                objMail._Subject = Language.getMessage(mstrModuleName, 43, "Notification for Assign Loan Application.")
                strMessage = "<HTML> <BODY>"
                strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("UName").ToString() & ", <BR><BR>"
                            strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                            strMessage &= Language.getMessage(mstrModuleName, 45, "This is the notification to assign Loan application #") & _
                                              " " & "<B>" & dRow.Item("application_no") & "</B>" & _
                                          Language.getMessage(mstrModuleName, 88, " with Loan scheme :") & " " & dRow.Item("Scheme")
                            strMessage &= Language.getMessage(mstrModuleName, 26, " of Employee ") & " " & dRow.Item("Employee") & _
                                          Language.getMessage(mstrModuleName, 27, " application date : ") & " " & _
                                          CDate(dRow.Item("application_date")).ToShortDateString & "."

                strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx?" & _
                                                       HttpUtility.UrlEncode(clsCrypto.Encrypt(intPendingApprovalTranID & "|" & iEmployeeId & "|" & _
                                                                                          xCompanyUnkid & "|" & _
                                                                                           dtRow.Item("UId").ToString.Trim))
                            'Hemant (30 Aug 2019) -- [Company._Object._Companyunkid --> xCompanyunkid]	

                strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                              Language.getMessage(mstrModuleName, 49, "Please click on the following link to assign Loan.") & _
                              "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" _
                              & strLink & "</a>"

                strMessage &= "</BODY></HTML>"

                        ElseIf CBool(dRow.Item("isloan")) = False Then
                            objMail._Subject = Language.getMessage(mstrModuleName, 100, "Notification for Assign Advance Application.")
                            strMessage = "<HTML> <BODY>"
                            strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("UName").ToString() & ", <BR><BR>"
                            strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                            strMessage &= Language.getMessage(mstrModuleName, 98, "This is the notification to assign Advance application #") & _
                                                     " " & "<B>" & dRow.Item("application_no") & "</B>"
                            strMessage &= Language.getMessage(mstrModuleName, 26, " of Employee ") & " " & dRow.Item("Employee") & _
                                      Language.getMessage(mstrModuleName, 27, " application date : ") & " " & CDate(dRow.Item("application_date")).ToShortDateString & "."

                            strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx?" & _
                                                              HttpUtility.UrlEncode(clsCrypto.Encrypt(intPendingApprovalTranID & "|" & iEmployeeId & "|" & _
                                                                                                 xCompanyUnkid & "|" & _
                                                                                                  dtRow.Item("UId").ToString.Trim))
                            'Hemant (30 Aug 2019) -- [Company._Object._Companyunkid --> xCompanyunkid]	

                            strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                                          Language.getMessage(mstrModuleName, 101, "Please click on the following link to assign Advance.") & _
                                          "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" _
                                          & strLink & "</a>"

                            strMessage &= "</BODY></HTML>"
                        End If
                        'Nilay (08-Dec-2016) -- End



                ElseIf blnIsDeleteAssigned = True Then

                    Dim objLoanAdvance As New clsLoan_Advance
                    Dim ds As DataSet

                    ds = objLoanAdvance.GetListByProcessPendingId(intProcessPendingUnkid)

                    Dim dR As DataRow = Nothing
                    If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                        dR = ds.Tables(0).Rows(0)
                    End If
                    If dR Is Nothing Then Exit Sub

                        'Nilay (08-Dec-2016) -- Start
                        'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
                        'objMail._Subject = Language.getMessage(mstrModuleName, 66, "Notification for Re-Assign Loan/Advance Application.")
                        'strMessage = "<HTML> <BODY>"
                        'strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("UName").ToString() & ", <BR><BR>"
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"

                        'If CBool(dR.Item("isloan")) = True Then
                        '    strMessage &= Language.getMessage(mstrModuleName, 67, "This is to inform you that, Loan assignment of voucher #") & _
                        '                          " " & "<B>" & dR.Item("loanvoucher_no") & "</B>" & " "
                        'Else
                        '    strMessage &= Language.getMessage(mstrModuleName, 90, "This is to inform you that, Advance assignment of voucher #") & _
                        '                          " " & "<B>" & dR.Item("loanvoucher_no") & "</B>" & " "
                        'End If

                        'strMessage &= Language.getMessage(mstrModuleName, 68, "is deleted") & " " & _
                        '              Language.getMessage(mstrModuleName, 26, " of Employee ") & " " & dRow.Item("Employee") & " " & _
                        '              Language.getMessage(mstrModuleName, 69, "on") & " " & CDate(dR.Item("voiddatetime")).ToShortDateString & "." & _
                        '              "<BR></BR>"

                        'If CBool(dR.Item("isloan")) = True Then
                        '    strMessage &= Language.getMessage(mstrModuleName, 70, "To Re-Assign Loan Application for application #") & _
                        '              " " & dRow.Item("application_no") & " "
                        'Else
                        '    strMessage &= Language.getMessage(mstrModuleName, 91, "To Re-Assign Advance Application for application #") & _
                        '              " " & dRow.Item("application_no") & " "
                        'End If

                        'strMessage &= Language.getMessage(mstrModuleName, 89, "with Loan Scheme :") & " " & dRow.Item("Scheme") & _
                        '              Language.getMessage(mstrModuleName, 26, " of Employee ") & " " & dRow.Item("Employee") & " " & _
                        '              Language.getMessage(mstrModuleName, 71, "for application date : ") & _
                        '              " " & CDate(dRow.Item("application_date")).ToShortDateString

                        'strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx?" & _
                        '                                   HttpUtility.UrlEncode(clsCrypto.Encrypt(intPendingApprovalTranID & "|" & iEmployeeId & "|" & _
                        '                                                                           Company._Object._Companyunkid & "|" & _
                        '                                                                           dtRow.Item("UId").ToString.Trim))

                        'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                        '              Language.getMessage(mstrModuleName, 92, "Please click on the following link to Re-Assign loan application.") & _
                        '              "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" _
                        '              & strLink & "</a>"

                        'strMessage &= "</BODY></HTML>"

                        If CBool(dR.Item("isloan")) = True Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 66, "Notification for Re-Assign Loan Application.")
                    strMessage = "<HTML> <BODY>"
                    strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("UName").ToString() & ", <BR><BR>"
                    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"

                        strMessage &= Language.getMessage(mstrModuleName, 67, "This is to inform you that, Loan assignment of voucher #") & _
                                              " " & "<B>" & dR.Item("loanvoucher_no") & "</B>" & " "

                            strMessage &= Language.getMessage(mstrModuleName, 68, "is deleted") & " " & _
                                          Language.getMessage(mstrModuleName, 26, " of Employee ") & " " & dRow.Item("Employee") & " " & _
                                          Language.getMessage(mstrModuleName, 69, "on") & " " & CDate(dR.Item("voiddatetime")).ToShortDateString & "." & _
                                          "<BR></BR>"

                            strMessage &= Language.getMessage(mstrModuleName, 70, "To Re-Assign Loan Application for application #") & _
                                          " " & dRow.Item("application_no") & " "

                            strMessage &= Language.getMessage(mstrModuleName, 89, "with Loan Scheme :") & " " & dRow.Item("Scheme") & _
                                          Language.getMessage(mstrModuleName, 26, " of Employee ") & " " & dRow.Item("Employee") & " " & _
                                          Language.getMessage(mstrModuleName, 71, "for application date : ") & _
                                          " " & CDate(dRow.Item("application_date")).ToShortDateString

                            strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx?" & _
                                                               HttpUtility.UrlEncode(clsCrypto.Encrypt(intPendingApprovalTranID & "|" & iEmployeeId & "|" & _
                                                                                                      xCompanyUnkid & "|" & _
                                                                                                       dtRow.Item("UId").ToString.Trim))
                            'Hemant (30 Aug 2019) -- [Company._Object._Companyunkid --> xCompanyunkid]	

                            strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                                          Language.getMessage(mstrModuleName, 92, "Please click on the following link to Re-Assign Loan application.") & _
                                          "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" _
                                          & strLink & "</a>"

                            strMessage &= "</BODY></HTML>"

                        ElseIf CBool(dR.Item("isloan")) = False Then

                            objMail._Subject = Language.getMessage(mstrModuleName, 102, "Notification for Re-Assign Advance Application.")
                            strMessage = "<HTML> <BODY>"
                            strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("UName").ToString() & ", <BR><BR>"
                            strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"

                        strMessage &= Language.getMessage(mstrModuleName, 90, "This is to inform you that, Advance assignment of voucher #") & _
                                              " " & "<B>" & dR.Item("loanvoucher_no") & "</B>" & " "

                    strMessage &= Language.getMessage(mstrModuleName, 68, "is deleted") & " " & _
                                  Language.getMessage(mstrModuleName, 26, " of Employee ") & " " & dRow.Item("Employee") & " " & _
                                  Language.getMessage(mstrModuleName, 69, "on") & " " & CDate(dR.Item("voiddatetime")).ToShortDateString & "." & _
                                  "<BR></BR>"

                        strMessage &= Language.getMessage(mstrModuleName, 91, "To Re-Assign Advance Application for application #") & _
                                  " " & dRow.Item("application_no") & " "

                            strMessage &= Language.getMessage(mstrModuleName, 26, " of Employee ") & " " & dRow.Item("Employee") & " " & _
                                  Language.getMessage(mstrModuleName, 71, "for application date : ") & _
                                  " " & CDate(dRow.Item("application_date")).ToShortDateString

                    strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx?" & _
                                                       HttpUtility.UrlEncode(clsCrypto.Encrypt(intPendingApprovalTranID & "|" & iEmployeeId & "|" & _
                                                                                                                           xCompanyUnkid & "|" & _
                                                                                               dtRow.Item("UId").ToString.Trim))

                            'Hemant (30 Aug 2019) -- [Company._Object._Companyunkid --> xCompanyunkid]	
                    strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                                          Language.getMessage(mstrModuleName, 103, "Please click on the following link to Re-Assign Advance application.") & _
                                  "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" _
                                  & strLink & "</a>"

                    strMessage &= "</BODY></HTML>"

                        End If
                        'Nilay (08-Dec-2016) -- End
                End If
                'Nilay (23-Aug-2016) -- End

                objMail._Message = strMessage
                objMail._ToEmail = dtRow.Item("UEmail")
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFormName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFormName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                objMail._SenderAddress = dtRow.Item("UEmail").ToString
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
                    'Nilay (27-Dec-2016) -- Start
                    'OPTIMIZATION: Sending email notification by Threading
                    'objMail.SendMail()
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                    Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                                                               mstrWebClientIP, mstrWebHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.LOAN_MGT, _
                                                               IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                    'Nilay (01 Feb 2017) -- Start
                    'Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
                    'gobjEmailList.Add(objEmailColl)
                    objEmailList.Add(objEmailColl)
                    'Nilay (01 Feb 2017) -- End
                    objUser = Nothing
                    'Nilay (27-Dec-2016) -- End
            Next
                'Nilay (27-Dec-2016) -- Start
                'OPTIMIZATION: Sending email notification by Threading

                'Nilay (01 Feb 2017) -- Start
                'Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
                If objEmailList.Count > 0 Then
                If HttpContext.Current Is Nothing Then
                    objThread = New Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objThread.Start()
                        Dim arr(1) As Object
                        arr(0) = xCompanyUnkid
                        objThread.Start(arr)
                        'Sohail (30 Nov 2017) -- End
                Else
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'Call Send_Notification()
                        Call Send_Notification(xCompanyUnkid)
                        'Sohail (30 Nov 2017) -- End
                End If
                End If
                'Nilay (01 Feb 2017) -- End
                'Nilay (27-Dec-2016) -- End
                'Nilay (18-Oct-2016) -- Start
            End If
            'Nilay (18-Oct-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Approver", mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Shani
    ''' </summary>
    ''' <purpose> Send Loan Approver Notification </purpose>
    ''' 
    Public Sub Send_Notification_After_Assign(ByVal xDatabaseName As String, _
                                              ByVal xUserId As Integer, _
                                              ByVal xYearUnkId As Integer, _
                                              ByVal xCompanyUnkid As Integer, _
                                              ByVal xIncludeInActiveEmp As Boolean, _
                                              ByVal xPeriodStartDate As Date, _
                                              ByVal xPeriodEndDate As Date, _
                                              ByVal xAsOnDate As Date, _
                                              ByVal xUserAccessFilterString As String, _
                                              ByVal strArutiSelfServiceURL As String, _
                                              ByVal intLoanUnkId As Integer, _
                                              ByVal enEmailType As enApproverEmailType, _
                                              Optional ByVal iLoginTypeId As Integer = 0, _
                                              Optional ByVal blnIsSendMail As Boolean = True, _
                                              Optional ByRef lstEmailList As List(Of clsEmailCollection) = Nothing, _
                                              Optional ByVal xNotifyLoanAdvanceUsers As String = "" _
                                              )
        'Hemant (30 Aug 2019) -- [blnIsSendMail, lstEmailList, xNotifyLoanAdvanceUsers]
        'Nilay (10-Sept-2016) -- [enEmailType]

        Dim dsAssignList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim objLoanAdvance As New clsLoan_Advance
        Dim dsList As DataSet = Nothing
        objDataOperation = New clsDataOperation
        Try
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub
            Dim decInstallmentAmount As Decimal = 0
            Dim decInstrAmount As Decimal = 0
            Dim decTotInstallmentAmount As Decimal = 0
            Dim decTotIntrstAmount As Decimal = 0
            Dim decPricipalAmount As Decimal = 0
            Dim strUserIds As String = ""

            'Hemant (30 Aug 2019) -- Start
            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
            'Dim strArray() As String = ConfigParameter._Object._Notify_LoanAdvance_Users.Split(CChar("||"))
            Dim strArray() As String = xNotifyLoanAdvanceUsers.Split(CChar("||"))
            'Hemant (30 Aug 2019) -- End	
            If strArray.Length > 1 Then
                strUserIds = strArray(2).ToString
            End If

            If strUserIds.ToString = "" Then Exit Sub

            dsAssignList = (New clsUserAddEdit).Get_UserBy_PrivilegeId(253, xYearUnkId)

            dsList = objLoanAdvance.GetList(xDatabaseName, xUserId, xYearUnkId, xCompanyUnkid, _
                                            xIncludeInActiveEmp, xPeriodStartDate, xPeriodEndDate, _
                                            xAsOnDate, xUserAccessFilterString, "List", " LN.loanadvancetranunkid = '" & intLoanUnkId & "'")


            Dim dRow As DataRow = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dRow = dsList.Tables(0).Rows(0)
            End If
            If dRow Is Nothing Then Exit Sub

            'Nilay (10-Sept-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'Dim dtEndDate As Date = DateAdd(DateInterval.Month, CDbl(dRow.Item("loan_duration")), eZeeDate.convertDate(dRow.Item("effective_date")))
            'objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(dRow.Item("Amount")), _
            '                                               CInt(DateDiff(DateInterval.Day, eZeeDate.convertDate(dRow.Item("effective_date")), dtEndDate.Date)), _
            '                                               CDec(dRow.Item("interest_rate")), _
            '                                               CType(CInt(dRow.Item("calctype_id")), enLoanCalcId), _
            '                                               CType(CInt(dRow.Item("interest_calctype_id")), enLoanInterestCalcType), _
            '                                               CInt(dRow.Item("loan_duration")), _
            '                                               CInt(DateDiff(DateInterval.Day, eZeeDate.convertDate(dRow.Item("Assign_Start_Date")), eZeeDate.convertDate(dRow.Item("Assign_End_Date")).AddDays(1))), _
            '                                               CDec(dRow.Item("emi_amount")), _
            '                                               decInstrAmount, decInstallmentAmount, decTotIntrstAmount, decTotInstallmentAmount)
            'decPricipalAmount = Format(decInstallmentAmount - decInstrAmount, GUI.fmtCurrency)
            Select Case enEmailType
                Case enApproverEmailType.Loan_Approver
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, CDbl(dRow.Item("loan_duration")), eZeeDate.convertDate(dRow.Item("effective_date")))
            objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(dRow.Item("Amount")), _
                                                           CInt(DateDiff(DateInterval.Day, eZeeDate.convertDate(dRow.Item("effective_date")), dtEndDate.Date)), _
                                                           CDec(dRow.Item("interest_rate")), _
                                                           CType(CInt(dRow.Item("calctype_id")), enLoanCalcId), _
                                                           CType(CInt(dRow.Item("interest_calctype_id")), enLoanInterestCalcType), _
                                                           CInt(dRow.Item("loan_duration")), _
                                                           CInt(DateDiff(DateInterval.Day, eZeeDate.convertDate(dRow.Item("Assign_Start_Date")), eZeeDate.convertDate(dRow.Item("Assign_End_Date")).AddDays(1))), _
                                                           CDec(dRow.Item("emi_amount")), _
                                                           decInstrAmount, decInstallmentAmount, decTotIntrstAmount, decTotInstallmentAmount)
            decPricipalAmount = Format(decInstallmentAmount - decInstrAmount, GUI.fmtCurrency)
            End Select
            'Nilay (10-Sept-2016) -- End

            Dim objMail As New clsSendMail

            'Nilay (18-Oct-2016) -- Start
            If dsAssignList.Tables(0).Rows.Count > 0 Then
                'Nilay (18-Oct-2016) -- End
            For Each dtRow As DataRow In dsAssignList.Tables(0).Select("UId IN(" & strUserIds & ") ")
                If dtRow.Item("UEmail") = "" Then Continue For
                Dim strMessage As String = ""

                    'Nilay (08-Dec-2016) -- Start
                    'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
                    'objMail._Subject = Language.getMessage(mstrModuleName, 99, "Notification for Assign Loan/Advance Application.")
                    'Nilay (08-Dec-2016) -- End

                'Nilay (10-Sept-2016) -- Start
                'Enhancement - Create New Loan Notification 
                If enEmailType = enApproverEmailType.Loan_Approver Then
                    'Nilay (10-Sept-2016) -- End

                        'Nilay (08-Dec-2016) -- Start
                        'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
                        objMail._Subject = Language.getMessage(mstrModuleName, 99, "Notification for Loan application assignment.")
                        'Nilay (08-Dec-2016) -- End

                strMessage = "<HTML> <BODY>"
                strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("UName").ToString() & ", <BR><BR>"
                strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                              Language.getMessage(mstrModuleName, 51, "This is to inform you that following are the details of Loan assignment.")
                strMessage &= "<BR></BR><BR></BR>"

                strMessage &= "<B>" & Language.getMessage(mstrModuleName, 81, "Loan Assignment Details :") & "</B><BR></BR>"

                strMessage &= "<table BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>"
                strMessage &= "<TR WIDTH='100%'>"
                strMessage &= "<TD WIDTH='40%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 82, "Particular") & "</B></FONT></TD>"
                strMessage &= "<TD WIDTH='60%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 83, "Details") & "</B></FONT></TD>"
                strMessage &= "</TR>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 84, "Voucher Number") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("VocNo") & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 85, "Employee Name") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("Employee") & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 74, "Deduction Period") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("DeductionPeriodName") & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 75, "Effective Date") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & eZeeDate.convertDate(dRow.Item("effective_date").ToString).ToShortDateString & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 76, "Loan Scheme") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("LoanScheme") & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 77, "Loan Amount") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & CDec(Format(dRow.Item("Amount"), GUI.fmtCurrency)) & " " & dRow.Item("cSign") & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 58, "Interest Amount") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & CDec(Format(dRow.Item("interest_amount"), GUI.fmtCurrency)) & " " & dRow.Item("cSign") & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 59, "Net Amount") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & CDec(Format(dRow.Item("net_amount"), GUI.fmtCurrency)) & " " & dRow.Item("cSign") & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 60, "Loan Calc. Type") & "</B></FONT></td>"
                strMessage &= "<td style='width: ALIGN='LEFT'><FONT SIZE=2>" & IIf(dRow.Item("LoanCalcType") = "", "&nbsp", dRow.Item("LoanCalcType")) & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 61, "Interest Calc. Type") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & IIf(dRow.Item("InterestCalcType") = "", "&nbsp", dRow.Item("InterestCalcType")) & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 62, "Rate") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & Format(dRow.Item("interest_rate"), GUI.fmtCurrency) & " %</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 63, "Intallment(In Months)") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & CInt(dRow.Item("installments")) & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 64, "Principle Amount") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & decPricipalAmount & " " & dRow.Item("cSign") & "</FONT></td>"
                strMessage &= "</tr>"

                strMessage &= "<tr style='width: 100%'>"
                strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 65, "Installment Amount") & "</B></FONT></td>"
                strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & CDec(Format(decInstallmentAmount, GUI.fmtCurrency)) & " " & dRow.Item("cSign") & "</FONT></td>"
                strMessage &= "</tr>"
                strMessage &= "</table>"

                strMessage &= "</BODY></HTML>"

                    'Nilay (10-Sept-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                ElseIf enEmailType = enApproverEmailType.Loan_Advance Then
                        'Nilay (08-Dec-2016) -- Start
                        'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
                        objMail._Subject = Language.getMessage(mstrModuleName, 104, "Notification for Advance application assignment.")
                        'Nilay (08-Dec-2016) -- End

                    strMessage = "<HTML> <BODY>"
                    strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & dtRow.Item("UName").ToString() & ", <BR><BR>"
                        strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                                      Language.getMessage(mstrModuleName, 93, "This is to inform you that the following are the details of Advance assignment.")
                    strMessage &= "<BR></BR><BR></BR>"

                    strMessage &= "<B>" & Language.getMessage(mstrModuleName, 94, "Advance Assignment Details :") & "</B><BR></BR>"

                    strMessage &= "<table BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>"
                    strMessage &= "<TR WIDTH='100%'>"
                    strMessage &= "<TD WIDTH='40%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 82, "Particular") & "</B></FONT></TD>"
                    strMessage &= "<TD WIDTH='60%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 83, "Details") & "</B></FONT></TD>"
                    strMessage &= "</TR>"

                    strMessage &= "<tr style='width: 100%'>"
                    strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 84, "Voucher Number") & "</B></FONT></td>"
                    strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("VocNo") & "</FONT></td>"
                    strMessage &= "</tr>"

                    strMessage &= "<tr style='width: 100%'>"
                    strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 85, "Employee Name") & "</B></FONT></td>"
                    strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("Employee") & "</FONT></td>"
                    strMessage &= "</tr>"

                    strMessage &= "<tr style='width: 100%'>"
                    strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 74, "Deduction Period") & "</B></FONT></td>"
                    strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("DeductionPeriodName") & "</FONT></td>"
                    strMessage &= "</tr>"

                    strMessage &= "<tr style='width: 100%'>"
                    strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 75, "Effective Date") & "</B></FONT></td>"
                    strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & eZeeDate.convertDate(dRow.Item("effective_date").ToString).ToShortDateString & "</FONT></td>"
                    strMessage &= "</tr>"

                    strMessage &= "<tr style='width: 100%'>"
                    strMessage &= "<td style='width: 40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 95, "Advance Amount") & "</B></FONT></td>"
                    strMessage &= "<td style='width: 60%' ALIGN='LEFT'><FONT SIZE=2>" & CDec(Format(dRow.Item("advance_amount"), GUI.fmtCurrency)) & " " & dRow.Item("cSign") & "</FONT></td>"
                    strMessage &= "</tr>"

                    strMessage &= "</table>"

                    strMessage &= "</BODY></HTML>"
                End If
                'Nilay (10-Sept-2016) -- End

                objMail._Message = strMessage
                objMail._ToEmail = dtRow.Item("UEmail")
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFormName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFormName
                End If
                objMail._LogEmployeeUnkid = 0
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = IIf(xUserId <= 0, User._Object._Userunkid, xUserId)
                objMail._SenderAddress = dtRow.Item("UEmail").ToString
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
                    'Nilay (27-Dec-2016) -- Start
                    'OPTIMIZATION: Sending email notification by Threading
                    'objMail.SendMail()
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = IIf(xUserId <= 0, User._Object._Userunkid, xUserId)
                    Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                                                               mstrWebClientIP, mstrWebHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.LOAN_MGT, _
                                                               IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                    'Nilay (01 Feb 2017) -- Start
                    'Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
                    'gobjEmailList.Add(objEmailColl)
                    objEmailList.Add(objEmailColl)
                    'Nilay (01 Feb 2017) -- End

                    objUser = Nothing
                    'Nilay (27-Dec-2016) -- End
            Next
                'Nilay (18-Oct-2016) -- Start

                'Nilay (27-Dec-2016) -- Start
                'OPTIMIZATION: Sending email notification by Threading
                'Nilay (01 Feb 2017) -- Start
                'Bug Fixes: Send Email Notification-Remove global collection list and use private colletion due to redundancy
                If blnIsSendMail = True Then 'Hemant (30 Aug 2019) -- End
                If objEmailList.Count > 0 Then
                If HttpContext.Current Is Nothing Then
                    objThread = New Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objThread.Start()
                        Dim arr(1) As Object
                        arr(0) = xCompanyUnkid
                        objThread.Start(arr)
                        'Sohail (30 Nov 2017) -- End
                Else
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'Call Send_Notification()
                        Call Send_Notification(xCompanyUnkid)
                        'Sohail (30 Nov 2017) -- End
                End If
                End If
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                Else
                    lstEmailList = objEmailList
                End If
                'Hemant (30 Aug 2019) -- End
                'Nilay (01 Feb 2017) -- End
                'Nilay (27-Dec-2016) -- End
            End If
            'Nilay (18-Oct-2016) -- End
            strUserIds = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_After_Assign", mstrModuleName)
        Finally
        End Try
    End Sub
    
    'Pinkal (26-Sep-2017) -- Start
    'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetApproverPendingLoanFormCount(ByVal intApproverID As Integer, ByVal mstrEmpID As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintCount As Integer = 0
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = " SELECT COUNT(*) PendingFormCount FROM lnloan_process_pending_loan  " & _
                      " JOIN lnloanapproval_process_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND approvertranunkid  = " & intApproverID & _
                      " WHERE lnloan_process_pending_loan.loan_statusunkid = 1 AND lnloan_process_pending_loan.isvoid = 0  AND lnloanapproval_process_tran.isvoid = 0 "

            If mstrEmpID.Trim.Length > 0 Then
                strQ &= " AND lnloan_process_pending_loan.employeeunkid IN (" & mstrEmpID & " )"
            End If

            dsList = objDataOperation.ExecQuery(strQ, "PedingCount")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCount = CInt(dsList.Tables(0).Rows(0)("PendingFormCount"))

            If mintCount > 0 Then Return mintCount

            mintCount = 0

            strQ = " SELECT Count(*) PendingFormCount FROM lnloanotherop_approval_tran " & _
                         " WHERE isvoid = 0 And lnloanotherop_approval_tran.approvertranunkid = " & intApproverID & _
                         " AND lnloanotherop_approval_tran.statusunkid  = 1 AND dbo.lnloanotherop_approval_tran.final_approved	 = 0 "

            If mstrEmpID.Trim.Length > 0 Then
                strQ &= " AND lnloanotherop_approval_tran.employeeunkid IN (" & mstrEmpID & " )"
            End If

            dsList = objDataOperation.ExecQuery(strQ, "PedingCount")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCount = CInt(dsList.Tables(0).Rows(0)("PendingFormCount"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverPendingLoanFormCount; Module Name: " & mstrModuleName)
        End Try
        Return mintCount
    End Function


    'Pinkal (26-Sep-2017) -- End


    'Shani (21-Jul-2016) -- End

    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Public Function getLoanApplicationNumberList(Optional ByVal blnNA As Boolean = False, _
                                                 Optional ByVal strListName As String = "List", _
                                                 Optional ByVal intEmployeeID As Integer = -1, _
                                                 Optional ByVal intLoanSchemeID As Integer = -1, _
                                                 Optional ByVal intProcessPendingLoanUnkid As Integer = -1 _
                                                 ) As DataSet


        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try
            If blnNA Then
                strQ = "SELECT 0 AS processpendingloanunkid " & _
                          ",  ' ' + @Select AS application_no " & _
                       "UNION "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1003, "Select"))

            End If

            strQ &= "SELECT " & _
                    "    processpendingloanunkid " & _
                    ",   application_no " & _
                    "FROM lnloan_process_pending_loan " & _
                    "WHERE isvoid = 0 " & _
                    " AND processpendingloanunkid IN ( " & _
                    "                   SELECT " & _
                    "                       processpendingloanunkid " & _
                    "                   FROM lnloan_advance_tran " & _
                    "                   WHERE isvoid = 0 " & _
                    "                       AND loan_statusunkid IN(" & enLoanStatus.IN_PROGRESS & ", " & enLoanStatus.ON_HOLD & ") " & _
                    ") "

            If intEmployeeID > 0 Then
                strQ &= "AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID.ToString)
            End If

            If intLoanSchemeID > 0 Then
                strQ &= "AND loanschemeunkid = @loanschemeunkid "
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanSchemeID.ToString)
            End If

            If intProcessPendingLoanUnkid > 0 Then
                strQ &= "AND processpendingloanunkid = @processpendingloanunkid "
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessPendingLoanUnkid.ToString)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getLoanApplicationNumberList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function
    'Hemant (12 Nov 2021) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
			Language.setMessage(mstrModuleName, 2, "WEB")
			Language.setMessage(mstrModuleName, 3, "Loan")
			Language.setMessage(mstrModuleName, 4, "Advance")
			Language.setMessage(mstrModuleName, 5, "Pending")
			Language.setMessage(mstrModuleName, 6, "Approved")
			Language.setMessage(mstrModuleName, 7, "Rejected")
			Language.setMessage(mstrModuleName, 9, "Select")
			Language.setMessage(mstrModuleName, 10, "Assigned")
			Language.setMessage(mstrModuleName, 11, "Loan Application Deleted")
			Language.setMessage(mstrModuleName, 12, "Notification for Approving Loan Application")
			Language.setMessage(mstrModuleName, 13, "Notification for Approving Loan Installment")
			Language.setMessage(mstrModuleName, 14, "Installment")
			Language.setMessage(mstrModuleName, 15, "Notification for Approving Loan Interest Rate")
			Language.setMessage(mstrModuleName, 16, "Intrest Rate")
			Language.setMessage(mstrModuleName, 17, "Notification for Approving Loan Topup")
			Language.setMessage(mstrModuleName, 18, "Topup")
			Language.setMessage(mstrModuleName, 19, "Dear")
			Language.setMessage(mstrModuleName, 20, "This is to inform you that Loan application #")
			Language.setMessage(mstrModuleName, 21, " with Loan Scheme :")
			Language.setMessage(mstrModuleName, 22, " for Employee")
			Language.setMessage(mstrModuleName, 23, " who was seeking your approval has been deleted. Kindly request to take a note of it.")
			Language.setMessage(mstrModuleName, 24, "This is the notification for approving Loan application #")
			Language.setMessage(mstrModuleName, 26, " of Employee")
			Language.setMessage(mstrModuleName, 27, " application date :")
			Language.setMessage(mstrModuleName, 28, "Please click on the following link to approve Loan.")
			Language.setMessage(mstrModuleName, 29, "This is the notification for approving Voucher #")
			Language.setMessage(mstrModuleName, 31, " for")
			Language.setMessage(mstrModuleName, 33, "Please click on the following link to approve")
			Language.setMessage(mstrModuleName, 34, "Loan Status Notification")
			Language.setMessage(mstrModuleName, 36, " This is to inform you that, the application you have applied for loan with")
			Language.setMessage(mstrModuleName, 41, "Please refer to the comments below for the same.")
			Language.setMessage(mstrModuleName, 42, "Remarks/Comments:")
			Language.setMessage(mstrModuleName, 43, "Notification for Assign Loan Application.")
			Language.setMessage(mstrModuleName, 45, "This is the notification to assign Loan application #")
			Language.setMessage(mstrModuleName, 49, "Please click on the following link to assign Loan.")
			Language.setMessage(mstrModuleName, 50, " has been")
			Language.setMessage(mstrModuleName, 51, "This is to inform you that following are the details of Loan assignment.")
			Language.setMessage(mstrModuleName, 52, "This is the notification for approving Advance application #")
			Language.setMessage(mstrModuleName, 53, "Please click on the following link to approve Advance.")
			Language.setMessage(mstrModuleName, 54, "Advance Status Notification")
			Language.setMessage(mstrModuleName, 55, " This is to inform you that, the application you have applied for advance with")
			Language.setMessage(mstrModuleName, 56, "Application number")
			Language.setMessage(mstrModuleName, 57, " Voucher number")
			Language.setMessage(mstrModuleName, 58, "Interest Amount")
			Language.setMessage(mstrModuleName, 59, "Net Amount")
			Language.setMessage(mstrModuleName, 60, "Loan Calc. Type")
			Language.setMessage(mstrModuleName, 61, "Interest Calc. Type")
			Language.setMessage(mstrModuleName, 62, "Rate")
			Language.setMessage(mstrModuleName, 63, "Intallment(In Months)")
			Language.setMessage(mstrModuleName, 64, "Principle Amount")
			Language.setMessage(mstrModuleName, 65, "Installment Amount")
			Language.setMessage(mstrModuleName, 66, "Notification for Re-Assign Loan Application.")
			Language.setMessage(mstrModuleName, 67, "This is to inform you that, Loan assignment of voucher #")
			Language.setMessage(mstrModuleName, 68, "is deleted")
			Language.setMessage(mstrModuleName, 69, "on")
			Language.setMessage(mstrModuleName, 70, "To Re-Assign Loan Application for application #")
			Language.setMessage(mstrModuleName, 71, "for application date :")
			Language.setMessage(mstrModuleName, 72, "Deleted")
			Language.setMessage(mstrModuleName, 73, "Remarks by Assigner:")
			Language.setMessage(mstrModuleName, 74, "Deduction Period")
			Language.setMessage(mstrModuleName, 75, "Effective Date")
			Language.setMessage(mstrModuleName, 76, "Loan Scheme")
			Language.setMessage(mstrModuleName, 77, "Loan Amount")
			Language.setMessage(mstrModuleName, 78, "Advance Application Deleted")
			Language.setMessage(mstrModuleName, 79, "Notification for Approving Advance Application")
			Language.setMessage(mstrModuleName, 80, "This is to inform you that Advance application #")
			Language.setMessage(mstrModuleName, 81, "Loan Assignment Details :")
			Language.setMessage(mstrModuleName, 82, "Particular")
			Language.setMessage(mstrModuleName, 83, "Details")
			Language.setMessage(mstrModuleName, 84, "Voucher Number")
			Language.setMessage(mstrModuleName, 85, "Employee Name")
			Language.setMessage(mstrModuleName, 86, " with Loan Scheme :")
			Language.setMessage(mstrModuleName, 87, " with Loan Scheme :")
			Language.setMessage(mstrModuleName, 88, " with Loan scheme :")
			Language.setMessage(mstrModuleName, 89, "with Loan Scheme :")
			Language.setMessage(mstrModuleName, 90, "This is to inform you that, Advance assignment of voucher #")
			Language.setMessage(mstrModuleName, 91, "To Re-Assign Advance Application for application #")
			Language.setMessage(mstrModuleName, 92, "Please click on the following link to Re-Assign Loan application.")
			Language.setMessage(mstrModuleName, 93, "This is to inform you that the following are the details of Advance assignment.")
			Language.setMessage(mstrModuleName, 94, "Advance Assignment Details :")
			Language.setMessage(mstrModuleName, 95, "Advance Amount")
			Language.setMessage(mstrModuleName, 96, "Cancelled")
			Language.setMessage("clsMasterData", 97, "On Hold")
			Language.setMessage("clsMasterData", 98, "Written Off")
			Language.setMessage(mstrModuleName, 99, "Notification for Loan application assignment.")
			Language.setMessage("clsMasterData", 100, "Completed")
			Language.setMessage(mstrModuleName, 101, "Please click on the following link to assign Advance.")
			Language.setMessage(mstrModuleName, 102, "Notification for Re-Assign Advance Application.")
			Language.setMessage(mstrModuleName, 103, "Please click on the following link to Re-Assign Advance application.")
			Language.setMessage(mstrModuleName, 104, "Notification for Advance application assignment.")
			Language.setMessage(mstrModuleName, 105, "Loan Rate Status Notification")
			Language.setMessage(mstrModuleName, 106, "Loan Installment Status Notification")
			Language.setMessage(mstrModuleName, 107, "Loan Topup Status Notification")
			Language.setMessage(mstrModuleName, 108, " This is to inform you that, the other operation of Rate you have applied for loan with")
			Language.setMessage(mstrModuleName, 109, " This is to inform you that, the other operation of Installment you have applied for loan with")
			Language.setMessage(mstrModuleName, 110, " This is to inform you that, the other operation of Topup you have applied for loan with")
			Language.setMessage("clsMasterData", 96, "In Progress")
			Language.setMessage(mstrModuleName, 97, "Cancel Remarks:")
			Language.setMessage(mstrModuleName, 98, "This is the notification to assign Advance application #")
			Language.setMessage(mstrModuleName, 100, "Notification for Assign Advance Application.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
'''' <summary>
'''' Purpose: 
'''' Developer: Vimal M. Gohil
'''' </summary>
'Public Class clsProcess_pending_loan
'    Private Shared ReadOnly mstrModuleName As String = "clsProcess_pending_loan"
'    Dim objDataOperation As clsDataOperation
'    Dim objLoanAdvance As clsLoan_Advance
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintProcesspendingloanunkid As Integer
'    Private mstrApplication_No As String = String.Empty
'    Private mdtApplication_Date As Date
'    Private mintEmployeeunkid As Integer
'    Private mintLoanschemeunkid As Integer
'    Private mdecLoan_Amount As Decimal 'Sohail (11 May 2011)
'    Private mintApproverunkid As Integer
'    Private mintLoan_Statusunkid As Integer
'    Private mdecApproved_Amount As Decimal 'Sohail (11 May 2011)
'    Private mblnIsloan As Boolean
'    Private mblnIsvoid As Boolean
'    Private mintUserunkid As Integer
'    Private mintVoiduserunkid As Integer
'    Private mdtVoiddatetime As Date
'    Private mstrRemark As String = String.Empty
'    Private mstrVoidreason As String = String.Empty

'    Private mintLoanAdvanceId As Integer
'    Private mdecInterest_Amount As Decimal 'Sohail (11 May 2011)
'    Private mdecNet_Amount As Decimal 'Sohail (11 May 2011)
'    Private mblnIsAdvance As Boolean


'    'Sandeep [ 21 Aug 2010 ] -- Start
'    Private mblnIsexternal_Entity As Boolean
'    Private mstrExternal_Entity_Name As String = String.Empty
'    'Sandeep [ 21 Aug 2010 ] -- End 

'    'S.SANDEEP [ 12 OCT 2011 ] -- START
'    Private mintLoginemployeeunkid As Integer = -1
'    Private mintVoidloginemployeeunkid As Integer = -1
'    'S.SANDEEP [ 12 OCT 2011 ] -- END 

'    'S.SANDEEP [ 18 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mstrEmp_Remark As String = String.Empty
'    'S.SANDEEP [ 18 APRIL 2012 ] -- END

'    'S.SANDEEP [ 19 JULY 2012 ] -- START
'    'Enhancement : TRA Changes
'    Private mstrWebFormName As String = String.Empty
'    'S.SANDEEP [ 19 JULY 2012 ] -- END

'    'S.SANDEEP [ 13 AUG 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mstrWebClientIP As String = String.Empty
'    Private mstrWebHostName As String = String.Empty
'    'S.SANDEEP [ 13 AUG 2012 ] -- END

'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set processpendingloanunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Processpendingloanunkid() As Integer
'        Get
'            Return mintProcesspendingloanunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintProcesspendingloanunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set application_no
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Application_No() As String
'        Get
'            Return mstrApplication_No
'        End Get
'        Set(ByVal value As String)
'            mstrApplication_No = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set application_date
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Application_Date() As Date
'        Get
'            Return mdtApplication_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtApplication_Date = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set employeeunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Employeeunkid() As Integer
'        Get
'            Return mintEmployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loanschemeunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Loanschemeunkid() As Integer
'        Get
'            Return mintLoanschemeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanschemeunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loan_amount
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Loan_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecLoan_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecLoan_Amount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set approverunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Approverunkid() As Integer
'        Get
'            Return mintApproverunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintApproverunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set loan_statusunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Loan_Statusunkid() As Integer
'        Get
'            Return mintLoan_Statusunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoan_Statusunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set approved_amount
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Approved_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecApproved_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecApproved_Amount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isloan
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Isloan() As Boolean
'        Get
'            Return mblnIsloan
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsloan = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set remark
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Remark() As String
'        Get
'            Return mstrRemark
'        End Get
'        Set(ByVal value As String)
'            mstrRemark = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = Value
'        End Set
'    End Property




'    Public Property _LoanAdvanceId() As Integer
'        Get
'            Return mintLoanAdvanceId
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanAdvanceId = value
'        End Set
'    End Property

'    Public Property _Interest_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecInterest_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecInterest_Amount = value
'        End Set
'    End Property

'    Public Property _Net_Amount() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecNet_Amount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecNet_Amount = value
'        End Set
'    End Property

'    Public Property _IsAdvance() As Boolean
'        Get
'            Return mblnIsAdvance
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsAdvance = value
'        End Set
'    End Property

'    'Sandeep [ 21 Aug 2010 ] -- Start
'    ''' <summary>
'    ''' Purpose: Get or Set isexternal_entity
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isexternal_Entity() As Boolean
'        Get
'            Return mblnIsexternal_Entity
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsexternal_Entity = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set external_entity_name
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _External_Entity_Name() As String
'        Get
'            Return mstrExternal_Entity_Name
'        End Get
'        Set(ByVal value As String)
'            mstrExternal_Entity_Name = Value
'        End Set
'    End Property
'    'Sandeep [ 21 Aug 2010 ] -- End 


'    'S.SANDEEP [ 12 OCT 2011 ] -- START
'    ''' <summary>
'    ''' Purpose: Get or Set loginemployeeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Loginemployeeunkid() As Integer
'        Get
'            Return mintLoginemployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoginemployeeunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidloginemployeeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voidloginemployeeunkid() As Integer
'        Get
'            Return mintVoidloginemployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoidloginemployeeunkid = Value
'        End Set
'    End Property
'    'S.SANDEEP [ 12 OCT 2011 ] -- END 

'    'S.SANDEEP [ 18 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    ''' <summary>
'    ''' Purpose: Get or Set emp_remark
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Emp_Remark() As String
'        Get
'            Return mstrEmp_Remark
'        End Get
'        Set(ByVal value As String)
'            mstrEmp_Remark = Value
'        End Set
'    End Property
'    'S.SANDEEP [ 18 APRIL 2012 ] -- END


'    'S.SANDEEP [ 19 JULY 2012 ] -- START
'    'Enhancement : TRA Changes

'    Public Property _WebFormName() As String
'        Get
'            Return mstrWebFormName
'        End Get
'        Set(ByVal value As String)
'            mstrWebFormName = value
'        End Set
'    End Property
'    'S.SANDEEP [ 19 JULY 2012 ] -- END

'    'S.SANDEEP [ 13 AUG 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _WebClientIP() As String
'        Set(ByVal value As String)
'            mstrWebClientIP = value
'        End Set
'    End Property

'    Public WriteOnly Property _WebHostName() As String
'        Set(ByVal value As String)
'            mstrWebHostName = value
'        End Set
'    End Property
'    'S.SANDEEP [ 13 AUG 2012 ] -- END

'#End Region

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            'Sandeep [ 21 Aug 2010 ] -- Start
'            'strQ = "SELECT " & _
'            '              "  processpendingloanunkid " & _
'            '              ", application_no " & _
'            '              ", application_date " & _
'            '              ", employeeunkid " & _
'            '              ", loanschemeunkid " & _
'            '              ", loan_amount " & _
'            '              ", approverunkid " & _
'            '              ", loan_statusunkid " & _
'            '              ", approved_amount " & _
'            '              ", remark " & _
'            '              ", isloan " & _
'            '              ", isvoid " & _
'            '              ", userunkid " & _
'            '              ", voiduserunkid " & _
'            '              ", voiddatetime " & _
'            '              ", voidreason " & _
'            '             "FROM lnloan_process_pending_loan " & _
'            '             "WHERE processpendingloanunkid = @processpendingloanunkid "


'            'S.SANDEEP [ 12 OCT 2011 ] -- START
'            'strQ = "SELECT " & _
'            '          "  processpendingloanunkid " & _
'            '          ", application_no " & _
'            '          ", application_date " & _
'            '          ", employeeunkid " & _
'            '          ", loanschemeunkid " & _
'            '          ", loan_amount " & _
'            '          ", approverunkid " & _
'            '          ", loan_statusunkid " & _
'            '          ", approved_amount " & _
'            '          ", remark " & _
'            '          ", isloan " & _
'            '          ", isvoid " & _
'            '          ", userunkid " & _
'            '          ", voiduserunkid " & _
'            '          ", voiddatetime " & _
'            '          ", voidreason " & _
'            '          ", ISNULL(isexternal_entity,0) As isexternal_entity " & _
'            '          ", ISNULL(external_entity_name,'') As external_entity_name " & _
'            ' "FROM lnloan_process_pending_loan " & _
'            ' "WHERE processpendingloanunkid = @processpendingloanunkid "

'            strQ = "SELECT " & _
'              "  processpendingloanunkid " & _
'              ", application_no " & _
'              ", application_date " & _
'              ", employeeunkid " & _
'              ", loanschemeunkid " & _
'              ", loan_amount " & _
'              ", approverunkid " & _
'              ", loan_statusunkid " & _
'              ", approved_amount " & _
'              ", remark " & _
'              ", isloan " & _
'              ", isvoid " & _
'              ", userunkid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'                          ", ISNULL(isexternal_entity,0) As isexternal_entity " & _
'                          ", ISNULL(external_entity_name,'') As external_entity_name " & _
'                      ", ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
'                      ", ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
'              ", ISNULL(emp_remark,'') AS emp_remark " & _
'             "FROM lnloan_process_pending_loan " & _
'             "WHERE processpendingloanunkid = @processpendingloanunkid "
'            'S.SANDEEP [ 12 OCT 2011 ] -- END 
'            'S.SANDEEP [ 18 APRIL 2012 emp_remark ] -- START -- END



'            'Sandeep [ 21 Aug 2010 ] -- End 

'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))
'                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
'                mstrApplication_No = dtRow.Item("application_no").ToString
'                ' mdtApplication_Date = dtRow.Item("application_date")
'                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
'                mdecLoan_Amount = CDec(dtRow.Item("loan_amount")) 'Sohail (11 May 2011)
'                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
'                mintLoan_Statusunkid = CInt(dtRow.Item("loan_statusunkid"))
'                mdecApproved_Amount = CDec(dtRow.Item("approved_amount")) 'Sohail (11 May 2011)
'                mstrRemark = dtRow.Item("remark").ToString
'                mblnIsloan = CBool(dtRow.Item("isloan"))
'                mblnIsvoid = CBool(dtRow.Item("isvoid"))
'                mintUserunkid = CInt(dtRow.Item("userunkid"))
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                If IsDBNull(dtRow.Item("voiddatetime")) Then
'                    mdtVoiddatetime = Nothing
'                Else
'                    mdtVoiddatetime = dtRow.Item("voiddatetime")
'                End If

'                If IsDBNull(dtRow.Item("application_date")) Then
'                    mdtApplication_Date = Nothing
'                Else
'                    mdtApplication_Date = dtRow.Item("application_date")
'                End If
'                mstrVoidreason = dtRow.Item("voidreason").ToString

'                'Sandeep [ 21 Aug 2010 ] -- Start
'                mblnIsexternal_Entity = CBool(dtRow.Item("isexternal_entity"))
'                mstrExternal_Entity_Name = dtRow.Item("external_entity_name").ToString
'                'Sandeep [ 21 Aug 2010 ] -- End 

'                'S.SANDEEP [ 12 OCT 2011 ] -- START
'                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
'                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
'                'S.SANDEEP [ 12 OCT 2011 ] -- END 

'                'S.SANDEEP [ 18 APRIL 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                mstrEmp_Remark = dtRow.Item("emp_remark").ToString
'                'S.SANDEEP [ 18 APRIL 2012 ] -- END

'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub


'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal intStatusID As Integer = 0 _
'                            , Optional ByVal strIncludeInactiveEmployee As String = "" _
'                            , Optional ByVal strEmployeeAsOnDate As String = "" _
'                            , Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet ', Optional ByVal blnOnlyActive As Boolean = True). [, Optional ByVal intStatusID As Integer = 0 : Sohail (21 Aug 2010)], 'Sohail (23 Apr 2012) - [strIncludeInactiveEmployee,strEmployeeAsOnDate,strUserAccessLevelFilterString]
'        'Public Function GetList(ByVal strTableName As String) As DataSet    ', Optional ByVal blnOnlyActive As Boolean = True)
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            'Sohail (21 Aug 2010) -- Start
'            '***Changes:Set LoanScheme = '@Advance' when isloan = 0, Filter for loan_statusunkid added.
'            'strQ = "SELECT " & _
'            '  " lnloan_process_pending_loan.processpendingloanunkid " & _
'            '  ",lnloan_process_pending_loan.application_no As Application_No " & _
'            '  ",convert(char(8),application_date,112) As application_date " & _
'            '  ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
'            '  ", lnloan_scheme_master.name AS LoanScheme " & _
'            '  ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
'            '  ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
'            '  ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending " & _
'            '  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved " & _
'            '  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected " & _
'            '  " END As LoanStatus" & _
'            '  ",lnloan_process_pending_loan.loan_statusunkid " & _
'            '  ",lnloan_process_pending_loan.employeeunkid " & _
'            '  ",lnloan_process_pending_loan.loanschemeunkid " & _
'            '  ",lnloan_process_pending_loan.isloan " & _
'            '  ",lnloan_process_pending_loan.loan_amount As Amount " & _
'            '  ",lnloan_process_pending_loan.loanschemeunkid " & _
'            '  ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
'            '  ",lnloan_process_pending_loan.approverunkid " & _
'            '  ",lnloan_process_pending_loan.remark As Remark " & _
'            ' "FROM lnloan_process_pending_loan " & _
'            ' "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
'            ' "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
'            ' "WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "



'            'S.SANDEEP [ 12 OCT 2011 ] -- START
'            'strQ = "SELECT " & _
'            '              " lnloan_process_pending_loan.processpendingloanunkid " & _
'            '              ",lnloan_process_pending_loan.application_no As Application_No " & _
'            '             ",convert(char(8),application_date,112) As application_date " & _
'            '              ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
'            '              ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
'            '              ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
'            '              ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
'            '              ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending " & _
'            '              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved " & _
'            '              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected " & _
'            '              " END As LoanStatus" & _
'            '              ",lnloan_process_pending_loan.loan_statusunkid " & _
'            '              ",lnloan_process_pending_loan.employeeunkid " & _
'            '              ",lnloan_process_pending_loan.loanschemeunkid " & _
'            '              ",lnloan_process_pending_loan.isloan " & _
'            '              ",lnloan_process_pending_loan.loan_amount As Amount " & _
'            '              ",lnloan_process_pending_loan.loanschemeunkid " & _
'            '              ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
'            '              ",lnloan_process_pending_loan.approverunkid " & _
'            '              ",lnloan_process_pending_loan.remark As Remark " & _
'            '              ",lnloan_process_pending_loan.isexternal_entity " & _
'            '              ",lnloan_process_pending_loan.external_entity_name " & _
'            '             "FROM lnloan_process_pending_loan " & _
'            '             "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
'            '             "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
'            '             "WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "

'            'Anjan (20 Mar 2012)-Start
'            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
'            'strQ = "SELECT " & _
'            '  " lnloan_process_pending_loan.processpendingloanunkid " & _
'            '  ",lnloan_process_pending_loan.application_no As Application_No " & _
'            ' ",convert(char(8),application_date,112) As application_date " & _
'            '  ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
'            '  ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
'            '  ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
'            '  ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
'            '  ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending " & _
'            '  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved " & _
'            '  "      WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected " & _
'            '  " END As LoanStatus" & _
'            '  ",lnloan_process_pending_loan.loan_statusunkid " & _
'            '  ",lnloan_process_pending_loan.employeeunkid " & _
'            '  ",lnloan_process_pending_loan.loanschemeunkid " & _
'            '  ",lnloan_process_pending_loan.isloan " & _
'            '  ",lnloan_process_pending_loan.loan_amount As Amount " & _
'            '  ",lnloan_process_pending_loan.loanschemeunkid " & _
'            '  ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
'            '  ",lnloan_process_pending_loan.approverunkid " & _
'            '  ",lnloan_process_pending_loan.remark As Remark " & _
'            '  ",lnloan_process_pending_loan.isexternal_entity " & _
'            '  ",lnloan_process_pending_loan.external_entity_name " & _
'            '            "	,ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
'            '            "	,ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
'            ' "FROM lnloan_process_pending_loan " & _
'            ' "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
'            ' "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
'            ' "WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "
'            strQ = "SELECT " & _
'              " lnloan_process_pending_loan.processpendingloanunkid " & _
'              ",lnloan_process_pending_loan.application_no As Application_No " & _
'             ",convert(char(8),application_date,112) As application_date " & _
'              ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
'              ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
'              ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
'              ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS Loan_AdvanceUnkid " & _
'              ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending " & _
'              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved " & _
'              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected " & _
'              "      WHEN lnloan_process_pending_loan.loan_statusunkid = 4 THEN @Assigned " & _
'              " END As LoanStatus " & _
'              ",lnloan_process_pending_loan.loan_statusunkid " & _
'              ",lnloan_process_pending_loan.employeeunkid " & _
'              ",lnloan_process_pending_loan.loanschemeunkid " & _
'              ",lnloan_process_pending_loan.isloan " & _
'              ",lnloan_process_pending_loan.loan_amount As Amount " & _
'              ",lnloan_process_pending_loan.loanschemeunkid " & _
'              ",lnloan_process_pending_loan.approved_amount As Approved_Amount " & _
'              ",lnloan_process_pending_loan.approverunkid " & _
'              ",lnloan_process_pending_loan.remark As Remark " & _
'              ",lnloan_process_pending_loan.isexternal_entity " & _
'              ",lnloan_process_pending_loan.external_entity_name " & _
'                        "	,ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
'                        "	,ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
'             ",hremployee_master.employeecode AS EmpCode " & _
'             ",ISNULL(lnloan_advance_tran.balance_amount,0) AS balance " & _
'             ",CASE WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
'             "       WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @OnHold " & _
'             "       WHEN lnloan_advance_tran.loan_statusunkid = 3 THEN @WrittenOff " & _
'             "       WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed " & _
'             "       ELSE ''  END As finalStatus " & _
'             ",lnloan_process_pending_loan.emp_remark " & _
'             "FROM lnloan_process_pending_loan " & _
'             "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
'             "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
'             "LEFT JOIN lnloan_advance_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid AND ISNULL(lnloan_advance_tran.isvoid,0) = 0 " & _
'             "WHERE ISNULL(lnloan_process_pending_loan.isvoid,0) = 0 "

'            'Anjan (20 Mar 2012)-End 
'            'S.SANDEEP [ 12 OCT 2011 ] -- END 

'            If intStatusID > 0 Then
'                strQ &= "AND lnloan_process_pending_loan.loan_statusunkid = @loan_statusunkid"
'                objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID.ToString)
'            End If
'            'Sohail (21 Aug 2010) -- End

'            'Anjan (09 Aug 2011)-Start
'            'Issue : For including setting of acitve and inactive employee.
'            'Sohail (23 Apr 2012) -- Start
'            'TRA - ENHANCEMENT
'            If strIncludeInactiveEmployee = "" Then strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
'            If CBool(strIncludeInactiveEmployee) = False Then
'                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                'Sohail (23 Apr 2012) -- End
'                'Sohail (06 Jan 2012) -- Start
'                'TRA - ENHANCEMENT
'                'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
'                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

'                'Sohail (23 Apr 2012) -- Start
'                'TRA - ENHANCEMENT
'                'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
'                'Sohail (23 Apr 2012) -- End
'                'Sohail (06 Jan 2012) -- End
'            End If
'            'Anjan (09 Aug 2011)-End 

'            'Anjan (24 Jun 2011)-Start
'            'Issue : According to privilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            'End If
'            'Sohail (23 Apr 2012) -- Start
'            'TRA - ENHANCEMENT
'            'Select Case ConfigParameter._Object._UserAccessModeSetting
'            '    Case enAllocation.BRANCH
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.TEAM
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOB_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOBS
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            '        End If
'            'End Select
'            If strUserAccessLevelFilterString = "" Then
'                strQ &= UserAccessLevel._AccessLevelFilterString
'            Else
'                strQ &= strUserAccessLevelFilterString
'                    End If
'            'Sohail (23 Apr 2012) -- End
'            'S.SANDEEP [ 04 FEB 2012 ] -- END

'            'Anjan (24 Jun 2011)-End 


'            'If blnOnlyActive Then
'            '    strQ &= " WHERE isactive = 1 "
'            'End If

'            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
'            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))
'            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
'            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))
'            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))

'            'Anjan (20 Mar 2012)-Start
'            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
'            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Assigned"))
'            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
'            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
'            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
'            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))
'            'Anjan (20 Mar 2012)-End 



'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function


'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (lnloan_process_pending_loan) </purpose>
'    Public Function Insert(Optional ByVal intCompanyUnkId As Integer = 0 _
'                           , Optional ByVal intLoanApplicationNoType As Integer = 0 _
'                           , Optional ByVal strLoanApplicationPrifix As String = "" _
'                           , Optional ByVal intNextLoanApplicationNo As Integer = 0) As Boolean




'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation
'        'Sandeep [ 16 Oct 2010 ] -- Start
'        objDataOperation.BindTransaction()
'        'Sandeep [ 16 Oct 2010 ] -- End 

'        'Anjan (21 Jan 2012)-Start
'        'ENHANCEMENT : TRA COMMENTS 
'        'Issue : To fix the issue on network of autocode concurrency when more than 1 user press save at same time.

'        'Sandeep [ 16 Oct 2010 ] -- Start
'        'Issue : Auto No. Generation



'        'S.SANDEEP [ 28 MARCH 2012 ] -- START
'        'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
'        'Dim intLoanAppNoType As Integer = 0
'        'Dim strLoanAppNoPrifix As String = ""
'        'Dim intNextLoanAppNo As Integer = 0

'        'intLoanAppNoType = ConfigParameter._Object._LoanApplicationNoType

'        'If intLoanAppNoType = 1 Then
'        '    strLoanAppNoPrifix = ConfigParameter._Object._LoanApplicationPrifix
'        '    intNextLoanAppNo = ConfigParameter._Object._NextLoanApplicationNo
'        '    mstrApplication_No = strLoanAppNoPrifix & intNextLoanAppNo
'        'End If

'        'If isExist(mintEmployeeunkid, mblnIsloan, mstrApplication_No, mdtApplication_Date, mintLoanschemeunkid) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
'        '    Return False
'        'End If
'        'Sandeep [ 16 Oct 2010 ] -- End 

'        'Sohail (23 Apr 2012) -- Start
'        'TRA - ENHANCEMENT
'        'Dim intLoanAppNoType As Integer = -1
'        'intLoanAppNoType = ConfigParameter._Object._LoanApplicationNoType
'        If intLoanApplicationNoType = 0 Then intLoanApplicationNoType = ConfigParameter._Object._LoanApplicationNoType
'        If strLoanApplicationPrifix = "" Then strLoanApplicationPrifix = ConfigParameter._Object._LoanApplicationPrifix
'        'Sohail (23 Apr 2012) -- End

'        'Sohail (23 Apr 2012) -- Start
'        'TRA - ENHANCEMENT
'        'If intLoanAppNoType = 0 Then
'        If intLoanApplicationNoType = 0 Then
'            'Sohail (23 Apr 2012) -- End
'        If isExist(mintEmployeeunkid, mblnIsloan, mstrApplication_No, mdtApplication_Date, mintLoanschemeunkid) Then
'                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
'                objDataOperation.ReleaseTransaction(False)
'            Return False
'        End If
'        End If
'        'S.SANDEEP [ 28 MARCH 2012 ] -- END


'        'If isExist(mstrApplication_No) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application No is already defined. Please define new Application No.")
'        'Return False
'        '  End If

'        'Anjan (21 Jan 2012)-End

'        Try
'            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
'            ' objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
'            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
'            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
'            objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If

'            If mdtApplication_Date = Nothing Then
'                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


'            'Sandeep [ 21 Aug 2010 ] -- Start
'            objDataOperation.AddParameter("@isexternal_entity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal_Entity.ToString)
'            objDataOperation.AddParameter("@external_entity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExternal_Entity_Name.ToString)
'            'strQ = "INSERT INTO lnloan_process_pending_loan ( " & _
'            '  "  application_no " & _
'            '  ", application_date " & _
'            '  ", employeeunkid " & _
'            '  ", loanschemeunkid " & _
'            '  ", loan_amount " & _
'            '  ", approverunkid " & _
'            '  ", loan_statusunkid " & _
'            '  ", approved_amount " & _
'            '  ", remark " & _
'            '  ", isloan" & _
'            '  ", isvoid " & _
'            '  ", userunkid " & _
'            '  ", voiduserunkid " & _
'            '  ", voiddatetime" & _
'            '  ", voidreason" & _
'            '") VALUES (" & _
'            '  "  @application_no " & _
'            '  ", @application_date " & _
'            '  ", @employeeunkid " & _
'            '  ", @loanschemeunkid " & _
'            '  ", @loan_amount " & _
'            '  ", @approverunkid " & _
'            '  ", @loan_statusunkid " & _
'            '  ", @approved_amount " & _
'            '  ", @remark " & _
'            '  ", @isloan" & _
'            '  ", @isvoid " & _
'            '  ", @userunkid " & _
'            '  ", @voiduserunkid " & _
'            '  ", @voiddatetime" & _
'            '  ", @voidreason" & _
'            '"); SELECT @@identity"


'            'S.SANDEEP [ 12 OCT 2011 ] -- START
'            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
'            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

'            'strQ = "INSERT INTO lnloan_process_pending_loan ( " & _
'            '              "  application_no " & _
'            '              ", application_date " & _
'            '              ", employeeunkid " & _
'            '              ", loanschemeunkid " & _
'            '              ", loan_amount " & _
'            '              ", approverunkid " & _
'            '              ", loan_statusunkid " & _
'            '              ", approved_amount " & _
'            '              ", remark " & _
'            '              ", isloan" & _
'            '              ", isvoid " & _
'            '              ", userunkid " & _
'            '              ", voiduserunkid " & _
'            '              ", voiddatetime" & _
'            '              ", voidreason" & _
'            '              ", isexternal_entity " & _
'            '              ", external_entity_name" & _
'            '          ") VALUES (" & _
'            '              "  @application_no " & _
'            '              ", @application_date " & _
'            '              ", @employeeunkid " & _
'            '              ", @loanschemeunkid " & _
'            '              ", @loan_amount " & _
'            '              ", @approverunkid " & _
'            '              ", @loan_statusunkid " & _
'            '              ", @approved_amount " & _
'            '              ", @remark " & _
'            '              ", @isloan" & _
'            '              ", @isvoid " & _
'            '              ", @userunkid " & _
'            '              ", @voiduserunkid " & _
'            '              ", @voiddatetime" & _
'            '              ", @voidreason" & _
'            '              ", @isexternal_entity " & _
'            '              ", @external_entity_name" & _
'            '            "); SELECT @@identity"

'            'S.SANDEEP [ 18 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)
'            'S.SANDEEP [ 18 APRIL 2012 ] -- END

'            strQ = "INSERT INTO lnloan_process_pending_loan ( " & _
'              "  application_no " & _
'              ", application_date " & _
'              ", employeeunkid " & _
'              ", loanschemeunkid " & _
'              ", loan_amount " & _
'              ", approverunkid " & _
'              ", loan_statusunkid " & _
'              ", approved_amount " & _
'              ", remark " & _
'              ", isloan" & _
'              ", isvoid " & _
'              ", userunkid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime" & _
'              ", voidreason" & _
'                                ", isexternal_entity " & _
'                                ", external_entity_name" & _
'                            ", loginemployeeunkid " & _
'                            ", voidloginemployeeunkid" & _
'                      ", emp_remark " & _
'            ") VALUES (" & _
'              "  @application_no " & _
'              ", @application_date " & _
'              ", @employeeunkid " & _
'              ", @loanschemeunkid " & _
'              ", @loan_amount " & _
'              ", @approverunkid " & _
'              ", @loan_statusunkid " & _
'              ", @approved_amount " & _
'              ", @remark " & _
'              ", @isloan" & _
'              ", @isvoid " & _
'              ", @userunkid " & _
'              ", @voiduserunkid " & _
'              ", @voiddatetime" & _
'              ", @voidreason" & _
'                                ", @isexternal_entity " & _
'                                ", @external_entity_name" & _
'                            ", @loginemployeeunkid " & _
'                            ", @voidloginemployeeunkid" & _
'                      ", @emp_remark " & _
'            "); SELECT @@identity"

'            'S.SANDEEP [ 12 OCT 2011 ] -- END 

'            'S.SANDEEP [ 18 APRIL 2012 emp_remark ] -- START -- END


'            'Sandeep [ 21 Aug 2010 ] -- End 

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintProcesspendingloanunkid = dsList.Tables(0).Rows(0).Item(0)

'            'S.SANDEEP [ 28 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
'            'Sohail (23 Apr 2012) -- Start
'            'TRA - ENHANCEMENT
'            If intLoanApplicationNoType = 1 Then
'                'If intLoanAppNoType = 1 Then
'                If Set_AutoNumber(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", "NextLoanApplicationNo", strLoanApplicationPrifix, intCompanyUnkId) = False Then
'                    'If Set_AutoNumber(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", "NextLoanApplicationNo", ConfigParameter._Object._LoanApplicationPrifix) = False Then
'                    'Sohail (23 Apr 2012) -- End

'                    'S.SANDEEP [ 17 NOV 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'If objDataOperation.ErrorMessage <> "" Then
'                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    '    Throw exForce
'                    'End If

'                    If objDataOperation.ErrorMessage <> "" Then
'                        objDataOperation.ReleaseTransaction(False)
'                        Return False
'                    End If
'                    'S.SANDEEP [ 17 NOV 2012 ] -- END
'                End If
'                'S.SANDEEP [ 20 APRIL 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If Get_Saved_Number(objDataOperation, mintProcesspendingloanunkid, "lnloan_process_pending_loan", "application_no", "processpendingloanunkid", mstrApplication_No) = False Then
'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If
'                End If
'                'S.SANDEEP [ 20 APRIL 2012 ] -- END
'            End If
'            'S.SANDEEP [ 28 MARCH 2012 ] -- END




'            'Sandeep [ 16 Oct 2010 ] -- Start
'            Call InsertAuditTrailForPendingLoan(objDataOperation, 1)



'            'S.SANDEEP [ 28 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
'            'If intLoanAppNoType = 1 Then
'            '    ConfigParameter._Object._NextLoanApplicationNo = intNextLoanAppNo + 1
'            '    ConfigParameter._Object.updateParam()
'            '    ConfigParameter._Object.Refresh()
'            'End If
'            'S.SANDEEP [ 28 MARCH 2012 ] -- END




'            objDataOperation.ReleaseTransaction(True)


'            'Sandeep [ 16 Oct 2010 ] -- End 

'            Return True
'        Catch ex As Exception
'            'Sandeep [ 16 Oct 2010 ] -- Start
'            objDataOperation.ReleaseTransaction(False)
'            'Sandeep [ 16 Oct 2010 ] -- End 
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (lnloan_process_pending_loan) </purpose>
'    Public Function Update(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
'        'Public Function Update() As Boolean
'        If isExist(mintEmployeeunkid, mblnIsloan, mstrApplication_No, mdtApplication_Date, mintLoanschemeunkid, mintProcesspendingloanunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Application is already defined. Please define new Application.")
'            Return False
'        End If

'        'If isExist(mstrApplication_No, mintProcesspendingloanunkid) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Application No is already defined. Please define new Application No.")
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'S.SANDEEP [ 04 DEC 2013 ] -- START
'        'Dim objDataOperation As New clsDataOperation

'        ''Sandeep [ 16 Oct 2010 ] -- Start
'        ''objDataOperation.BindTransaction()
'        'objDataOperation.BindTransaction()
'        ''Sandeep [ 16 Oct 2010 ] -- End 
'        Dim objDataOperation As clsDataOperation
'        If objDataOpr Is Nothing Then
'            objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        Else
'            objDataOperation = objDataOpr
'            objDataOperation.ClearParameters()
'        End If
'        'S.SANDEEP [ 04 DEC 2013 ] -- END
'        Try
'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
'            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
'            ' objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
'            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
'            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
'            objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If

'            If mdtApplication_Date = Nothing Then
'                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


'            'Sandeep [ 21 Aug 2010 ] -- Start
'            objDataOperation.AddParameter("@isexternal_entity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal_Entity.ToString)
'            objDataOperation.AddParameter("@external_entity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExternal_Entity_Name.ToString)

'            'strQ = "UPDATE lnloan_process_pending_loan SET " & _
'            '  "  application_no = @application_no" & _
'            '  ", application_date = @application_date" & _
'            '  ", employeeunkid = @employeeunkid" & _
'            '  ", loanschemeunkid = @loanschemeunkid" & _
'            '  ", loan_amount = @loan_amount" & _
'            '  ", approverunkid = @approverunkid" & _
'            '  ", loan_statusunkid = @loan_statusunkid" & _
'            '  ", approved_amount = @approved_amount" & _
'            '  ", remark = @remark" & _
'            '  ", isloan = @isloan " & _
'            '  ", isvoid = @isvoid" & _
'            '  ", userunkid = @userunkid" & _
'            '  ", voiduserunkid = @voiduserunkid" & _
'            '  ", voiddatetime = @voiddatetime " & _
'            '  ", voidreason = @voidreason " & _
'            '"WHERE processpendingloanunkid = @processpendingloanunkid "


'            'S.SANDEEP [ 12 OCT 2011 ] -- START
'            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
'            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
'            'strQ = "UPDATE lnloan_process_pending_loan SET " & _
'            '                "  application_no = @application_no" & _
'            '                ", application_date = @application_date" & _
'            '                ", employeeunkid = @employeeunkid" & _
'            '                ", loanschemeunkid = @loanschemeunkid" & _
'            '                ", loan_amount = @loan_amount" & _
'            '                ", approverunkid = @approverunkid" & _
'            '                ", loan_statusunkid = @loan_statusunkid" & _
'            '                ", approved_amount = @approved_amount" & _
'            '                ", remark = @remark" & _
'            '                ", isloan = @isloan " & _
'            '                ", isvoid = @isvoid" & _
'            '                ", userunkid = @userunkid" & _
'            '                ", voiduserunkid = @voiduserunkid" & _
'            '                ", voiddatetime = @voiddatetime " & _
'            '                ", voidreason = @voidreason " & _
'            '                ", isexternal_entity = @isexternal_entity" & _
'            '                ", external_entity_name = @external_entity_name " & _
'            '         "WHERE processpendingloanunkid = @processpendingloanunkid "

'            'S.SANDEEP [ 18 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)
'            'S.SANDEEP [ 18 APRIL 2012 ] -- END

'            strQ = "UPDATE lnloan_process_pending_loan SET " & _
'              "  application_no = @application_no" & _
'              ", application_date = @application_date" & _
'              ", employeeunkid = @employeeunkid" & _
'              ", loanschemeunkid = @loanschemeunkid" & _
'              ", loan_amount = @loan_amount" & _
'              ", approverunkid = @approverunkid" & _
'              ", loan_statusunkid = @loan_statusunkid" & _
'              ", approved_amount = @approved_amount" & _
'              ", remark = @remark" & _
'              ", isloan = @isloan " & _
'              ", isvoid = @isvoid" & _
'              ", userunkid = @userunkid" & _
'              ", voiduserunkid = @voiduserunkid" & _
'              ", voiddatetime = @voiddatetime " & _
'              ", voidreason = @voidreason " & _
'                            ", isexternal_entity = @isexternal_entity" & _
'                            ", external_entity_name = @external_entity_name " & _
'                            ", loginemployeeunkid = @loginemployeeunkid" & _
'                            ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
'              ", emp_remark = @emp_remark " & _
'            "WHERE processpendingloanunkid = @processpendingloanunkid "
'            'S.SANDEEP [ 12 OCT 2011 ] -- END 
'            'S.SANDEEP [ 18 APRIL 2012 emp_remark ] -- START -- END

'            'Sandeep [ 21 Aug 2010 ] -- End 


'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'Dim blnFlag As Boolean = False

'            'blnFlag = UpdateLoanData(mintLoanAdvanceId, mdblApproved_Amount, mdblInterest_Amount, mdblNet_Amount, mblnIsAdvance)
'            'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

'            'objDataOperation.ReleaseTransaction(True)

'            'Sandeep [ 16 Oct 2010 ] -- Start
'            Call InsertAuditTrailForPendingLoan(objDataOperation, 2)

'            'S.SANDEEP [ 04 DEC 2013 ] -- START
'            'objDataOperation.ReleaseTransaction(True)
'            If objDataOpr Is Nothing Then
'            objDataOperation.ReleaseTransaction(True)
'            End If
'            'S.SANDEEP [ 04 DEC 2013 ] -- END

'            'Sandeep [ 16 Oct 2010 ] -- End 

'            Return True
'        Catch ex As Exception
'            'Sandeep [ 16 Oct 2010 ] -- Start
'            objDataOperation.ReleaseTransaction(False)
'            'Sandeep [ 16 Oct 2010 ] -- End 
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'S.SANDEEP [ 04 DEC 2013 ] -- START
'            'objDataOperation = Nothing
'            'S.SANDEEP [ 04 DEC 2013 ] -- END
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (lnloan_process_pending_loan) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation
'        'Sandeep [ 16 Oct 2010 ] -- Start
'        objDataOperation.BindTransaction()
'        'Sandeep [ 16 Oct 2010 ] -- End 

'        Try

'            'S.SANDEEP [ 12 OCT 2011 ] -- START
'            'strQ = "UPDATE lnloan_process_pending_loan SET " & _
'            '                    "  isvoid = @isvoid" & _
'            '                    ", voiduserunkid = @voiduserunkid" & _
'            '                    ", voiddatetime = @voiddatetime " & _
'            '                    ", voidreason = @voidreason " & _
'            '            "WHERE processpendingloanunkid = @processpendingloanunkid "
'            'S.SANDEEP [ 12 OCT 2011 ] -- END 

'            If mintVoidloginemployeeunkid <= -1 Then
'            strQ = "UPDATE lnloan_process_pending_loan SET " & _
'                    "  isvoid = @isvoid" & _
'                    ", voiduserunkid = @voiduserunkid" & _
'                    ", voiddatetime = @voiddatetime " & _
'                    ", voidreason = @voidreason " & _
'            "WHERE processpendingloanunkid = @processpendingloanunkid "
'            Else
'                strQ = "UPDATE lnloan_process_pending_loan SET " & _
'                                "  isvoid = @isvoid" & _
'                                ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
'                                ", voiddatetime = @voiddatetime " & _
'                                ", voidreason = @voidreason " & _
'                        "WHERE processpendingloanunkid = @processpendingloanunkid "
'            End If

'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
'            'S.SANDEEP [ 12 OCT 2011 ] -- START
'            If mintVoidloginemployeeunkid > 0 Then
'                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
'            End If
'            'S.SANDEEP [ 12 OCT 2011 ] -- END 


'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'Sandeep [ 16 Oct 2010 ] -- Start
'            Call InsertAuditTrailForPendingLoan(objDataOperation, 3)
'            objDataOperation.ReleaseTransaction(True)
'            'Sandeep [ 16 Oct 2010 ] -- End 

'            Return True
'        Catch ex As Exception
'            'Sandeep [ 16 Oct 2010 ] -- Start
'            objDataOperation.ReleaseTransaction(False)
'            'Sandeep [ 16 Oct 2010 ] -- End 
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function


'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intEmpId As Integer, ByVal blnLoan As Boolean, ByVal strAppNo As String, ByVal dtAppDate As Date, ByVal intLoanSchemeId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'objDataOperation = New clsDataOperation
'        Dim objDataOperation As New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  processpendingloanunkid " & _
'              ", application_no " & _
'              ", application_date " & _
'              ", employeeunkid " & _
'              ", loanschemeunkid " & _
'              ", loan_amount " & _
'              ", approverunkid " & _
'              ", loan_statusunkid " & _
'              ", approved_amount " & _
'              ", remark " & _
'              ", isloan " & _
'              ", isvoid " & _
'              ", userunkid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'            "FROM lnloan_process_pending_loan " & _
'            "WHERE CONVERT(CHAR(8),application_date,112) = @application_date " & _
'            "AND application_no = @application_no " & _
'            "AND isloan = @isloan "

'            If intEmpId > 0 Then
'                strQ &= "AND employeeunkid = @employeeunkid "
'            End If

'            If intLoanSchemeId > 0 Then
'                strQ &= "AND loanschemeunkid = @loanschemeunkid "
'            End If




'            If intUnkid > 0 Then
'                strQ &= " AND processpendingloanunkid <> @processpendingloanunkid"
'            End If


'            '  objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
'            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnLoan)
'            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strAppNo)
'            objDataOperation.AddParameter("@application_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtAppDate).ToString)
'            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanSchemeId)
'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'objDataOperation = Nothing
'        End Try
'    End Function

'    'Public Function isExist(ByVal strAppNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
'    '    Dim dsList As DataSet = Nothing
'    '    Dim strQ As String = ""
'    '    Dim exForce As Exception

'    '    objDataOperation = New clsDataOperation

'    '    Try
'    '        strQ = "SELECT " & _
'    '          "  processpendingloanunkid " & _
'    '          ", application_no " & _
'    '          ", application_date " & _
'    '          ", employeeunkid " & _
'    '          ", loanschemeunkid " & _
'    '          ", loan_amount " & _
'    '          ", approverunkid " & _
'    '          ", loan_statusunkid " & _
'    '          ", approved_amount " & _
'    '          ", remark " & _
'    '          ", isloan " & _
'    '          ", isvoid " & _
'    '          ", userunkid " & _
'    '          ", voiduserunkid " & _
'    '          ", voiddatetime " & _
'    '        "FROM lnloan_process_pending_loan " & _
'    '         "WHERE application_no = @application_no "


'    '        If intUnkid > 0 Then
'    '            strQ &= " AND processpendingloanunkid <> @processpendingloanunkid"
'    '        End If


'    '        '  objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
'    '        objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strAppNo)
'    '        objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'    '        dsList = objDataOperation.ExecQuery(strQ, "List")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        Return dsList.Tables(0).Rows.Count > 0
'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'    '    Finally
'    '        exForce = Nothing
'    '        If dsList IsNot Nothing Then dsList.Dispose()
'    '        objDataOperation = Nothing
'    '    End Try
'    'End Function


'    'Sandeep [ 21 Aug 2010 ] -- Start
'    'Public Function GetLoan_Status(Optional ByVal strListName As String = "List") As DataSet
'    '    Dim strQ As String = String.Empty
'    '    Dim objDataOperation As New clsDataOperation
'    '    Dim dsList As New DataSet
'    '    Dim exForce As Exception
'    '    Try

'    '        strQ = "SELECT 0 AS Id,@Select AS NAME " & _
'    '                "UNION SELECT 1 AS Id,@Pending AS NAME " & _
'    '                "UNION SELECT 2 AS Id,@Approved AS NAME " & _
'    '                "UNION SELECT 3 AS Id,@Rejected AS NAME "

'    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 70, "Select"))
'    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 71, "Pending"))
'    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 72, "Approved"))
'    '        objDataOperation.AddParameter("Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Rejected"))

'    '        'If blnisSaving = False Then
'    '        '    objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "Written Off"))
'    '        'Else
'    '        '    objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 82, "Redemption"))
'    '        'End If

'    '        dsList = objDataOperation.ExecQuery(strQ, strListName)

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        Return dsList


'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "GetLoan_Status", mstrModuleName)
'    '        Return Nothing
'    '    End Try
'    'End Function

'    Public Function GetLoan_Status(Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal UseAssignedStatus As Boolean = False) As DataSet
'        Dim strQ As String = String.Empty
'        Dim objDataOperation As New clsDataOperation
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Try

'            If blnFlag = True Then
'                strQ = "SELECT 0 AS Id,@Select AS NAME UNION  "
'            End If

'            strQ &= " SELECT 1 AS Id,@Pending AS NAME " & _
'                    "UNION SELECT 2 AS Id,@Approved AS NAME " & _
'                    "UNION SELECT 3 AS Id,@Rejected AS NAME "

'            'Anjan (04 Apr 2011)-Start
'            'Issue : Included new Status of Assigned as per Rutta's suggestion
'            If UseAssignedStatus = True Then
'                strQ &= "UNION SELECT 4 AS Id, @Assigned AS NAME "
'                objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Assigned"))
'            End If

'            'Anjan (04 Apr 2011)-End

'            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))
'            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
'            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))

'            'Sandeep [ 23 Oct 2010 ] -- Start
'            'objDataOperation.AddParameter("Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Rejected"))
'            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))
'            'Sandeep [ 23 Oct 2010 ] -- End 

'            'If blnisSaving = False Then
'            '    objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "Written Off"))
'            'Else
'            '    objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 82, "Redemption"))
'            'End If

'            dsList = objDataOperation.ExecQuery(strQ, strListName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetLoan_Status", mstrModuleName)
'            Return Nothing
'        End Try
'    End Function
'    'Sandeep [ 21 Aug 2010 ] -- End 

'    Public Function Get_Id_Used_In_Paymet(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                   " prpayment_tran.referencetranunkid " & _
'                   " FROM prpayment_tran " & _
'                   " LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
'                   " LEFT JOIN lnloan_process_pending_loan ON lnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
'                   " WHERE lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid"

'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    Public Function Get_Id_Used_In_LoanAdvance(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            'Sandeep [ 16 Oct 2010 ] -- Start
'            'strQ = "Select " & _
'            '       " loanadvancetranunkid " & _
'            '       "From lnloan_advance_tran " & _
'            '       "WHERE processpendingloanunkid=@processpendingloanunkid "

'            strQ = "Select " & _
'                      " loanadvancetranunkid " & _
'                      "From lnloan_advance_tran " & _
'                      "WHERE processpendingloanunkid=@processpendingloanunkid " & _
'                      "AND ISNULL(lnloan_advance_tran.isvoid,0) = 0 "
'            'Sandeep [ 16 Oct 2010 ] -- End 

'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    Public Function Get_LoanAdvanceunkid(ByVal intUnkid As Integer, ByRef intLoanAdvanceId As Integer) As Integer
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                   "loanadvancetranunkid " & _
'                   " FROM lnloan_advance_tran " & _
'                   " WHERE processpendingloanunkid = @processpendingloanunkid"

'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                intLoanAdvanceId = CInt(dsList.Tables(0).Rows(0)("loanadvancetranunkid"))
'            Else
'                intLoanAdvanceId = 0
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    'Sandeep [ 16 Oct 2010 ] -- Start
'    Private Sub InsertAuditTrailForPendingLoan(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer)
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Try


'            'S.SANDEEP [ 19 JULY 2012 ] -- START
'            'Enhancement : TRA Changes

'            'strQ = "INSERT INTO atlnloan_process_pending_loan ( " & _
'            '            "  processpendingloanunkid " & _
'            '            ", application_no " & _
'            '            ", application_date " & _
'            '            ", employeeunkid " & _
'            '            ", loanschemeunkid " & _
'            '            ", loan_amount " & _
'            '            ", approverunkid " & _
'            '            ", loan_statusunkid " & _
'            '            ", approved_amount " & _
'            '            ", remark " & _
'            '            ", isloan " & _
'            '            ", audittype " & _
'            '            ", audituserunkid " & _
'            '            ", auditdatetime " & _
'            '            ", ip " & _
'            '            ", machine_name" & _
'            '            ", emp_remark " & _
'            '       ") VALUES (" & _
'            '            "  @processpendingloanunkid " & _
'            '            ", @application_no " & _
'            '            ", @application_date " & _
'            '            ", @employeeunkid " & _
'            '            ", @loanschemeunkid " & _
'            '            ", @loan_amount " & _
'            '            ", @approverunkid " & _
'            '            ", @loan_statusunkid " & _
'            '            ", @approved_amount " & _
'            '            ", @remark " & _
'            '            ", @isloan " & _
'            '            ", @audittype " & _
'            '            ", @audituserunkid " & _
'            '            ", @auditdatetime " & _
'            '            ", @ip " & _
'            '            ", @machine_name" & _
'            '            ", @emp_remark " & _
'            '       "); SELECT @@identity"


'            strQ = "INSERT INTO atlnloan_process_pending_loan ( " & _
'                        "  processpendingloanunkid " & _
'                        ", application_no " & _
'                        ", application_date " & _
'                        ", employeeunkid " & _
'                        ", loanschemeunkid " & _
'                        ", loan_amount " & _
'                        ", approverunkid " & _
'                        ", loan_statusunkid " & _
'                        ", approved_amount " & _
'                        ", remark " & _
'                        ", isloan " & _
'                        ", audittype " & _
'                        ", audituserunkid " & _
'                        ", auditdatetime " & _
'                        ", ip " & _
'                        ", machine_name" & _
'                        ", emp_remark " & _
'                     ", form_name " & _
'                     ", module_name1 " & _
'                     ", module_name2 " & _
'                     ", module_name3 " & _
'                     ", module_name4 " & _
'                     ", module_name5 " & _
'                     ", isweb " & _
'                        ", loginemployeeunkid " & _
'                   ") VALUES (" & _
'                        "  @processpendingloanunkid " & _
'                        ", @application_no " & _
'                        ", @application_date " & _
'                        ", @employeeunkid " & _
'                        ", @loanschemeunkid " & _
'                        ", @loan_amount " & _
'                        ", @approverunkid " & _
'                        ", @loan_statusunkid " & _
'                        ", @approved_amount " & _
'                        ", @remark " & _
'                        ", @isloan " & _
'                        ", @audittype " & _
'                        ", @audituserunkid " & _
'                        ", @auditdatetime " & _
'                        ", @ip " & _
'                        ", @machine_name" & _
'                        ", @emp_remark " & _
'                     ", @form_name " & _
'                     ", @module_name1 " & _
'                     ", @module_name2 " & _
'                     ", @module_name3 " & _
'                     ", @module_name4 " & _
'                     ", @module_name5 " & _
'                     ", @isweb " & _
'                        ", @loginemployeeunkid " & _
'                   "); SELECT @@identity"


'            'S.SANDEEP [ 19 JULY 2012 ] -- END

'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
'            objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplication_No.ToString)
'            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
'            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
'            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Statusunkid.ToString)
'            objDataOperation.AddParameter("@approved_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Amount.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@isloan", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloan.ToString)
'            objDataOperation.AddParameter("@audittype", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
'            'Sohail (23 Apr 2012) -- Start
'            'TRA - ENHANCEMENT
'            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString)
'            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
'            'Sohail (23 Apr 2012) -- End
'            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

'            'S.SANDEEP [ 13 AUG 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
'            'objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
'            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
'            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
'            'S.SANDEEP [ 13 AUG 2012 ] -- END


'            'S.SANDEEP [ 18 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@emp_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrEmp_Remark.ToString)
'            'S.SANDEEP [ 18 APRIL 2012 ] -- END


'            'S.SANDEEP [ 19 JULY 2012 ] -- START
'            'Enhancement : TRA Changes

'            If mstrWebFormName.Trim.Length <= 0 Then
'                'S.SANDEEP [ 11 AUG 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'Dim frm As Form
'                'For Each frm In Application.OpenForms
'                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
'                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
'                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
'                '    End If
'                'Next
'                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
'                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
'                'S.SANDEEP [ 11 AUG 2012 ] -- END
'            Else
'                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
'                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
'                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
'            End If
'            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
'            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
'            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
'            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
'            'S.SANDEEP [ 19 JULY 2012 ] -- END

'            'S.SANDEEP [ 13 AUG 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
'            'S.SANDEEP [ 13 AUG 2012 ] -- END


'            objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForPendingLoan", mstrModuleName)
'        End Try
'    End Sub
'    'Sandeep [ 16 Oct 2010 ] -- End 

'    Public Function UpdateLoanData(ByVal intLoanId As Integer, _
'                                   ByVal decNewAmount As Decimal, _
'                                   ByVal decIntrest_Amt As Decimal, _
'                                   ByVal decNetAmt As Decimal, _
'                                   ByVal blnIsAdvance As Boolean) As Boolean 'Sohail (11 May 2011)
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try
'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanId.ToString)

'            If blnIsAdvance = False Then
'                strQ = "UPDATE lnloan_advance_tran SET " & _
'                        "  loan_amount = @loan_amount" & _
'                        ", interest_amount = @interest_amount" & _
'                        ", net_amount = @net_amount" & _
'                        " WHERE loanadvancetranunkid = @loanadvancetranunkid "

'                objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decNewAmount.ToString) 'Sohail (11 May 2011)
'                objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decIntrest_Amt.ToString) 'Sohail (11 May 2011)
'                objDataOperation.AddParameter("@net_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decNetAmt.ToString) 'Sohail (11 May 2011)
'            Else
'                strQ = "UPDATE lnloan_advance_tran SET " & _
'                        "  advance_amount = @advance_amount" & _
'                      " WHERE loanadvancetranunkid = @loanadvancetranunkid "

'                objDataOperation.AddParameter("@advance_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decNewAmount.ToString) 'Sohail (11 May 2011)
'            End If

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'objDataOperation = Nothing
'        End Try
'    End Function

'    'Sohail (03 Nov 2010) -- Start
'    Public Function Get_UnAssigned_ProcessPending_List(ByVal strTableName As String) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try


'            strQ = "SELECT  lnloan_process_pending_loan.processpendingloanunkid " & _
'                          ", lnloan_process_pending_loan.application_no " & _
'                          ", convert(char(8),lnloan_process_pending_loan.application_date,112) As application_date " & _
'                          ", lnloan_process_pending_loan.employeeunkid " & _
'                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
'                          ", lnloan_process_pending_loan.loanschemeunkid " & _
'                          ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN lnloan_scheme_master.name ELSE @Advance END AS LoanScheme " & _
'                          ", CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Loan_Advance " & _
'                          ", lnloan_process_pending_loan.loan_amount As Amount " & _
'                          ", lnloan_process_pending_loan.approverunkid " & _
'                          ", lnloan_process_pending_loan.loan_statusunkid " & _
'                          ",CASE WHEN lnloan_process_pending_loan.loan_statusunkid = 1 THEN @Pending WHEN lnloan_process_pending_loan.loan_statusunkid = 2 THEN @Approved WHEN lnloan_process_pending_loan.loan_statusunkid = 3 THEN @Rejected END As LoanStatus " & _
'                          ", lnloan_process_pending_loan.approved_amount " & _
'                          ", lnloan_process_pending_loan.remark " & _
'                          ", lnloan_process_pending_loan.isloan " & _
'                          ", lnloan_process_pending_loan.isvoid " & _
'                          ", lnloan_process_pending_loan.userunkid " & _
'                          ", lnloan_process_pending_loan.voiduserunkid " & _
'                          ", lnloan_process_pending_loan.voiddatetime " & _
'                          ", lnloan_process_pending_loan.voidreason " & _
'                          ", lnloan_process_pending_loan.isexternal_entity " & _
'                          ", lnloan_process_pending_loan.external_entity_name " & _
'                    "FROM    lnloan_process_pending_loan " & _
'                            "LEFT JOIN lnloan_advance_tran ON lnloan_process_pending_loan.processpendingloanunkid = lnloan_advance_tran.processpendingloanunkid " & _
'                            "LEFT JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
'                            "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
'                    "WHERE   lnloan_advance_tran.loanadvancetranunkid IS NULL " & _
'                            "AND ISNULL(lnloan_process_pending_loan.isvoid, 0) = 0 " & _
'                            "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
'                            "AND lnloan_process_pending_loan.loan_statusunkid = 2 " '2=Approved


'            'Anjan (09 Aug 2011)-Start
'            'Issue : For including setting of acitve and inactive employee.
'            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                'Sohail (06 Jan 2012) -- Start
'                'TRA - ENHANCEMENT
'                'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
'                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                'Sohail (06 Jan 2012) -- End
'            End If
'            'Anjan (09 Aug 2011)-End 

'            'Anjan (24 Jun 2011)-Start
'            'Issue : According to privilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            'End If

'            'S.SANDEEP [ 01 JUNE 2012 ] -- START
'            'ENHANCEMENT : TRA DISCIPLINE CHANGES
'            'Select Case ConfigParameter._Object._UserAccessModeSetting
'            '    Case enAllocation.BRANCH
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.TEAM
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOB_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOBS
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            '        End If
'            'End Select
'            strQ &= UserAccessLevel._AccessLevelFilterString
'            'S.SANDEEP [ 01 JUNE 2012 ] -- END


'            'S.SANDEEP [ 04 FEB 2012 ] -- END            

'            'Anjan (24 Jun 2011)-End 


'            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
'            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))
'            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
'            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))
'            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected"))

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_UnAssigned_ProcessPending_List; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function
'    'Sohail (03 Nov 2010) -- End



'    'S.SANDEEP [ 06 SEP 2011 ] -- START
'    'ENHANCEMENT : MAKING COMMON FUNCTION
'    Public Function GetToAssignList(ByVal intStatusId As Integer, _
'                                    ByVal intDepartmentId As Integer, _
'                                    ByVal intBranchId As Integer, _
'                                    ByVal intSecId As Integer, _
'                                    ByVal intJobId As Integer, _
'                                    ByVal intEmpId As Integer, _
'                                    ByVal intApproverId As Integer, _
'                                    ByVal intMode As Integer, _
'                                    ByVal intSchemeId As Integer, _
'                                    Optional ByVal StrListName As String = "List", _
'                                    Optional ByVal strUserAccessFilter As String = "") As DataTable   'Pinkal (14-Jun-2013) [strUserAccessFilter]
'        Dim dtTable As DataTable = Nothing
'        Dim StrQ As String = ""
'        Dim exForce As Exception
'        Try

'            objDataOperation = New clsDataOperation

'            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
'            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))

'            '/* SCHEME MASTER LIST */
'            StrQ = "SELECT " & _
'                             " loanschemeunkid AS loanschemeunkid " & _
'                             ",@Loan+' --> '+ISNULL(name,'') AS Schemes " & _
'                       "FROM lnloan_scheme_master " & _
'                       "UNION ALL " & _
'                       "SELECT " & _
'                             " 0 AS loanschemeunkid " & _
'                             ",@Advance AS Schemes "

'            Dim dsScheme As DataSet = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsScheme.Tables("List").Rows.Count > 0 Then
'                '/* TRANSACTION LIST */
'                StrQ = "SELECT " & _
'                                 " ECode AS ECode " & _
'                                 ",EName AS EName " & _
'                                 ",Mode AS Mode " & _
'                                 ",AppNo AS AppNo " & _
'                                 ",Amount AS Amount " & _
'                                 ",LScheme AS LScheme " & _
'                                 ",LApp AS Approver " & _
'                                 ",PId AS PId " & _
'                                 ",ModeId AS ModeId " & _
'                                 ",IsEx AS IsEx " & _
'                                 ",BranchId AS BranchId " & _
'                                 ",DeptId AS DeptId " & _
'                                 ",SectionId AS SectionId " & _
'                                 ",JobId AS JobId " & _
'                                 ",EmpId AS EmpId " & _
'                                 ",ApprId AS ApprId " & _
'                                 ",SchemeId AS SchemeId " & _
'                                 ",StatusId AS StatusId " & _
'                            "FROM " & _
'                            "( " & _
'                            "SELECT " & _
'                                  "ISNULL(hremployee_master.employeecode,'') AS ECode " & _
'                                 ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
'                                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
'                                 ",ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
'                                 ",ISNULL(lnloan_process_pending_loan.approved_amount,0) AS Amount " & _
'                                 ",lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
'                                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END AS ModeId " & _
'                                 ",CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan+' --> '+ISNULL(lnloan_scheme_master.name,'') ELSE @Advance END AS LScheme " & _
'                                 ",ISNULL(LApp.firstname,'')+' '+ISNULL(LApp.othername,'')+' '+ISNULL(LApp.surname,'') AS LApp " & _
'                                 ",lnloan_process_pending_loan.isexternal_entity AS IsEx " & _
'                                 ",hremployee_master.stationunkid AS BranchId " & _
'                                 ",hremployee_master.departmentunkid  As DeptId " & _
'                                 ",hremployee_master.sectionunkid AS SectionId " & _
'                                 ",hremployee_master.jobunkid AS JobId " & _
'                                 ",hremployee_master.employeeunkid AS EmpId " & _
'                                 ",LApp.employeeunkid AS ApprId " & _
'                                 ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS SchemeId " & _
'                                 ",lnloan_process_pending_loan.loan_statusunkid AS StatusId " & _
'                            "FROM lnloan_process_pending_loan " & _
'                                 "JOIN lnloan_approver_tran ON lnloan_approver_tran.approverunkid = lnloan_process_pending_loan.approverunkid " & _
'                                 "JOIN hremployee_master as LApp ON LApp.employeeunkid = lnloan_approver_tran.approverunkid " & _
'                                 "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
'                                 "JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
'                            "WHERE isvoid = 0 "
'                If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                    'Sohail (06 Jan 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    'StrQ &= " AND hremployee_master.isactive = 1 "
'                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

'                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                    'Sohail (06 Jan 2012) -- End
'                End If

'                'S.SANDEEP [ 01 JUNE 2012 ] -- START
'                'ENHANCEMENT : TRA DISCIPLINE CHANGES
'                'Select Case ConfigParameter._Object._UserAccessModeSetting
'                '    Case enAllocation.BRANCH
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.DEPARTMENT_GROUP
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.DEPARTMENT
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.SECTION_GROUP
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.SECTION
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.UNIT_GROUP
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.UNIT
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.TEAM
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.JOB_GROUP
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.JOBS
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND  LApp.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                'End Select


'                'Pinkal (12-Jun-2013) -- Start
'                'Enhancement : TRA Changes

'                If strUserAccessFilter.Trim.Length > 0 Then
'                    StrQ &= strUserAccessFilter
'                    StrQ &= strUserAccessFilter.Replace("hremployee_master", "LApp")
'                Else
'                StrQ &= UserAccessLevel._AccessLevelFilterString
'                StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "LApp")
'                End If

'                'StrQ &= UserAccessLevel._AccessLevelFilterString
'                'StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "LApp")

'                'Pinkal (12-Jun-2013) -- End


'                'S.SANDEEP [ 01 JUNE 2012 ] -- END



'                StrQ &= ") AS ToAssign WHERE 1 = 1 AND StatusId = " & intStatusId

'                If intDepartmentId > 0 Then
'                    StrQ &= " AND DeptId = '" & intDepartmentId & "'"
'                End If

'                If intApproverId > 0 Then
'                    StrQ &= " AND ApprId = '" & intApproverId & "'"
'                End If

'                If intBranchId > 0 Then
'                    StrQ &= " AND BranchId = '" & intBranchId & "'"
'                End If

'                If intEmpId > 0 Then
'                    StrQ &= " AND EmpId = '" & intEmpId & "'"
'                End If

'                If intJobId > 0 Then
'                    StrQ &= " AND JobId = '" & intJobId & "'"
'                End If

'                'S.SANDEEP [ 04 FEB 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If UserAccessLevel._AccessLevel.Length > 0 Then
'                '    StrQ &= "  AND JobId IN (" & UserAccessLevel._AccessLevel & ") "
'                'End If
'                'S.SANDEEP [ 04 FEB 2012 ] -- END

'                Select Case intMode
'                    Case 0
'                        StrQ &= " AND ModeId = 1 "
'                    Case 1
'                        StrQ &= " AND ModeId = 2 "
'                End Select

'                If intSchemeId > 0 Then
'                    StrQ &= " AND SchemeId = '" & intSchemeId & "'"
'                End If

'                If intSecId > 0 Then
'                    StrQ &= " AND SectionId = '" & intSecId & "'"
'                End If

'                Dim dsTrans As DataSet = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If



'                'Pinkal (12-Jun-2013) -- Start
'                'Enhancement : TRA Changes

'                    If StrListName.Length > 0 Then
'                        dtTable = New DataTable(StrListName)
'                    Else
'                        dtTable = New DataTable("List")
'                    End If


'                    dtTable.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
'                    dtTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
'                    dtTable.Columns.Add("Employee", System.Type.GetType("System.String")).DefaultValue = ""
'                    dtTable.Columns.Add("Approver", System.Type.GetType("System.String")).DefaultValue = ""
'                    dtTable.Columns.Add("Amount", System.Type.GetType("System.String")).DefaultValue = ""
'                    dtTable.Columns.Add("Pendingunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
'                    dtTable.Columns.Add("IsEx", System.Type.GetType("System.Boolean")).DefaultValue = False
'                    dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
'                    dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
'                    dtTable.Columns.Add("ApprId", System.Type.GetType("System.Int32")).DefaultValue = -1
'                    dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1

'                'Pinkal (12-Jun-2013) -- End


'                If dsTrans.Tables("List").Rows.Count > 0 Then

'                    Dim dtRow As DataRow = Nothing

'                    Dim dtFilter As DataTable

'                    For Each dsRow As DataRow In dsScheme.Tables("List").Rows
'                        dtFilter = New DataView(dsTrans.Tables("List"), "SchemeId = '" & dsRow.Item("loanschemeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

'                        If dtFilter.Rows.Count > 0 Then
'                            dtRow = dtTable.NewRow

'                            dtRow.Item("ECode") = dsRow.Item("Schemes")
'                            dtRow.Item("IsGrp") = True
'                            dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")

'                            dtTable.Rows.Add(dtRow)

'                            For Each dtlRow As DataRow In dtFilter.Rows
'                                dtRow = dtTable.NewRow

'                                dtRow.Item("ECode") = Space(5) & dtlRow.Item("ECode")
'                                dtRow.Item("Employee") = dtlRow.Item("EName")
'                                dtRow.Item("Approver") = dtlRow.Item("Approver")
'                                dtRow.Item("Amount") = Format(CDec(dtlRow.Item("Amount")), GUI.fmtCurrency)
'                                dtRow.Item("Pendingunkid") = dtlRow.Item("PId")
'                                dtRow.Item("IsEx") = dtlRow.Item("IsEx")
'                                dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")
'                                dtRow.Item("ApprId") = dtlRow.Item("ApprId")
'                                dtRow.Item("EmpId") = dtlRow.Item("EmpId")

'                                dtTable.Rows.Add(dtRow)
'                            Next

'                        End If

'                    Next

'                End If

'            End If


'            Return dtTable

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetToAssignList", mstrModuleName)
'            Return Nothing
'        Finally
'        End Try
'    End Function
'    'S.SANDEEP [ 06 SEP 2011 ] -- END 


'    'S.SANDEEP [ 09 AUG 2011 ] -- START
'    'ENHANCEMENT : GLOBAL LOAN/ADVANCE APPROVEMENT
'    Public Function GetDataList(ByVal intStatusId As Integer, _
'                                ByVal intDepartmentId As Integer, _
'                                ByVal intBranchId As Integer, _
'                                ByVal intSecId As Integer, _
'                                ByVal intJobId As Integer, _
'                                ByVal intModeId As Integer) As DataTable
'        Dim dtTable As DataTable = Nothing
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation


'            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Loan"))
'            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Advance"))

'            StrQ = "SELECT " & _
'                    " loanschemeunkid AS loanschemeunkid " & _
'                    ",@Loan+' --> '+ISNULL(name,'') AS Schemes " & _
'                    "FROM lnloan_scheme_master " & _
'                   "UNION ALL " & _
'                    "SELECT " & _
'                    " 0 AS loanschemeunkid " & _
'                    ",@Advance AS Schemes "

'            Dim dsScheme As DataSet = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsScheme.Tables(0).Rows.Count > 0 Then

'                StrQ = "SELECT " & _
'                    "	 CAST(0 AS BIT) AS IsChecked " & _
'                    "	,ISNULL(hremployee_master.employeecode,'') AS ECode " & _
'                    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
'                    "	,CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN @Loan ELSE @Advance END AS Mode " & _
'                    "	,ISNULL(lnloan_process_pending_loan.application_no,'') AS AppNo " & _
'                    "	,ISNULL(lnloan_process_pending_loan.loan_amount,0) AS Amount " & _
'                    "	,ISNULL(lnloan_process_pending_loan.approved_amount,0) AS AppAmount " & _
'                    "	,lnloan_process_pending_loan.processpendingloanunkid AS PId " & _
'                    "   ,CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END AS ModeId " & _
'                    "   ,ISNULL(lnloan_process_pending_loan.loanschemeunkid,0) AS SchId " & _
'                    "FROM lnloan_process_pending_loan " & _
'                    "	JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
'                    "WHERE isvoid = 0 "

'                If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                    'Sohail (06 Jan 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    'StrQ &= " AND isactive = 1 "
'                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

'                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                    'Sohail (06 Jan 2012) -- End
'                End If

'                'S.SANDEEP [ 04 FEB 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If UserAccessLevel._AccessLevel.Length > 0 Then
'                '    StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'                'End If

'                'S.SANDEEP [ 01 JUNE 2012 ] -- START
'                'ENHANCEMENT : TRA DISCIPLINE CHANGES
'                'Select Case ConfigParameter._Object._UserAccessModeSetting
'                '    Case enAllocation.BRANCH
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.DEPARTMENT_GROUP
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.DEPARTMENT
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.SECTION_GROUP
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.SECTION
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.UNIT_GROUP
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.UNIT
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.TEAM
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.JOB_GROUP
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                '        End If
'                '    Case enAllocation.JOBS
'                '        If UserAccessLevel._AccessLevel.Length > 0 Then
'                '            StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'                '        End If
'                'End Select
'                StrQ &= UserAccessLevel._AccessLevelFilterString
'                'S.SANDEEP [ 01 JUNE 2012 ] -- END


'                'S.SANDEEP [ 04 FEB 2012 ] -- END

'                If intStatusId > 0 Then
'                    StrQ &= " AND lnloan_process_pending_loan.loan_statusunkid = " & intStatusId
'                End If

'                If intBranchId > 0 Then
'                    StrQ &= " AND hremployee_master.stationunkid = " & intBranchId
'                End If

'                If intDepartmentId > 0 Then
'                    StrQ &= " AND hremployee_master.departmentunkid = " & intDepartmentId
'                End If

'                If intJobId > 0 Then
'                    StrQ &= " AND hremployee_master.jobunkid = " & intJobId
'                End If

'                If intSecId > 0 Then
'                    StrQ &= " AND hremployee_master.sectionunkid = " & intSecId
'                End If

'                If intModeId > -1 Then
'                    Select Case intModeId
'                        Case 0  'ADVANCE
'                            StrQ &= " AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END = 0 "
'                        Case 1  'LOAN
'                            StrQ &= " AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 0 END = 1 "
'                    End Select
'                End If

'            End If


'            Dim dsTrans As DataSet = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            dtTable = New DataTable("List")


'            'Pinkal (12-Jun-2013) -- Start
'            'Enhancement : TRA Changes

'                dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
'                dtTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
'                dtTable.Columns.Add("EName", System.Type.GetType("System.String")).DefaultValue = ""
'                dtTable.Columns.Add("AppNo", System.Type.GetType("System.String")).DefaultValue = ""
'                dtTable.Columns.Add("Amount", System.Type.GetType("System.String")).DefaultValue = ""
'                dtTable.Columns.Add("AppAmount", System.Type.GetType("System.String")).DefaultValue = ""
'                dtTable.Columns.Add("PId", System.Type.GetType("System.Int32")).DefaultValue = -1
'                dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
'                dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1

'            'Pinkal (12-Jun-2013) -- End

'            If dsTrans.Tables(0).Rows.Count > 0 Then
'                Dim dtRow As DataRow = Nothing

'                Dim dtFilter As DataTable

'                For Each dsRow As DataRow In dsScheme.Tables("List").Rows
'                    dtFilter = New DataView(dsTrans.Tables("List"), "SchId = '" & dsRow.Item("loanschemeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

'                    If dtFilter.Rows.Count > 0 Then
'                        dtRow = dtTable.NewRow

'                        dtRow.Item("ECode") = dsRow.Item("Schemes")
'                        dtRow.Item("IsGrp") = True
'                        dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")

'                        dtTable.Rows.Add(dtRow)

'                        For Each dtlRow As DataRow In dtFilter.Rows
'                            dtRow = dtTable.NewRow

'                            dtRow.Item("ECode") = Space(5) & dtlRow.Item("ECode")
'                            dtRow.Item("EName") = dtlRow.Item("EName")
'                            dtRow.Item("AppNo") = dtlRow.Item("AppNo")
'                            dtRow.Item("Amount") = Format(CDec(dtlRow.Item("Amount")), GUI.fmtCurrency)
'                            dtRow.Item("AppAmount") = Format(CDec(dtlRow.Item("AppAmount")), GUI.fmtCurrency)
'                            dtRow.Item("PId") = dtlRow.Item("PId")
'                            dtRow.Item("GrpId") = dsRow.Item("loanschemeunkid")

'                            dtTable.Rows.Add(dtRow)
'                        Next

'                    End If
'                Next

'            End If

'            Return dtTable

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetDataList", mstrModuleName)
'            Return Nothing
'        Finally
'            dtTable.Dispose()
'        End Try
'    End Function
'    'S.SANDEEP [ 09 AUG 2011 ] -- END 



'    'Public Function Get_History_List(ByVal strTableName As String) As DataSet    ', Optional ByVal blnOnlyActive As Boolean = True)
'    '    Dim dsList As DataSet = Nothing
'    '    Dim strQ As String = ""
'    '    Dim exForce As Exception

'    '    objDataOperation = New clsDataOperation

'    '    Try
'    '        strQ = "SELECT " & _
'    '          "  processpendingloanunkid " & _
'    '          ", employeeunkid " & _
'    '          ", loanschemeunkid " & _
'    '          ", loan_amount " & _
'    '          ", approverunkid " & _
'    '          ", loan_statusunkid " & _
'    '          ", approved_amount " & _
'    '          ", isloan " & _
'    '         "FROM lnloan_process_pending_loan "

'    '        'If blnOnlyActive Then
'    '        '    strQ &= " WHERE isactive = 1 "
'    '        'End If

'    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'    '    Finally
'    '        exForce = Nothing
'    '        If dsList IsNot Nothing Then dsList.Dispose()
'    '        objDataOperation = Nothing
'    '    End Try
'    '    Return dsList
'    'End Function

'    'Public Sub GetLoanData()
'    '    Dim dsList As DataSet = Nothing
'    '    Dim strQ As String = ""
'    '    Dim exForce As Exception

'    '    objDataOperation = New clsDataOperation

'    '    Try
'    '        strQ = "SELECT " & _
'    '               " loan_amount " & _
'    '               ",interest_rate " & _
'    '               ",interest_amount " & _
'    '               ",net_amount " & _
'    '               ",loan_duration" & _
'    '               ",isloan " & _
'    '               ",calctype_id" & _
'    '               "FROM lnloan_advance_tran " & _
'    '               "WHERE loanadvancetranunkid = @loanadvancetranunkid"

'    '        objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

'    '        dsList = objDataOperation.ExecQuery(strQ, "List")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
'    '            mintLoanAdvaceunkid = CInt(dtRow.Item("loanadvancetranunkid"))
'    '            mdblLoan_Amount = cdec(dtRow.Item("loan_amount"))
'    '            mdblInterestRate = cdec(dtRow.Item("interest_rate"))
'    '            mdblInterestAmount = cdec(dtRow.Item("interest_amount"))
'    '            mdblNetAmount = cdec(dtRow.Item("net_amount"))
'    '            mintLoanDuration = CInt(dtRow.Item("loan_duration"))
'    '            mblnIsloan = CBool(dtRow.Item("isloan"))
'    '            mintCalcTypeid = CInt(dtRow.Item("calctype_id"))

'    '            Exit For
'    '        Next
'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'    '    Finally
'    '        exForce = Nothing
'    '        If dsList IsNot Nothing Then dsList.Dispose()
'    '        objDataOperation = Nothing
'    '    End Try
'    'End Sub


'End Class