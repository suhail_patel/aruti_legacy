﻿'************************************************************************************************************************************
'Class Name : clsStaffrequisition_Tran.vb
'Purpose    :
'Date       :05/06/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsStaffrequisition_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsStaffrequisition_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStaffrequisitiontranunkid As Integer
    Private mstrFormno As String = String.Empty
    Private mintStaffrequisitiontypeid As Integer
    Private mintStaffrequisitionbyid As Integer
    Private mintAllocationunkid As Integer
    Private mintClassgroupunkid As Integer
    Private mintClassunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintActionreasonunkid As Integer
    Private mstrAdditionalstaffreason As String = String.Empty
    Private mintJobunkid As Integer
    Private mstrJobdescrription As String = String.Empty
    Private mdtWorkstartdate As Date
    Private mintNoofposition As Integer
    Private mintForm_statusunkid As Integer = enApprovalStatus.PENDING
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sohail (12 Oct 2018) -- Start
    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
    Private mintGradeunkid As Integer
    Private mintGradelevelunkid As Integer
    Private mintApproved_headcount As Integer
    Private mintActual_headcount As Integer
    Private mintEmploymenttypeunkid As Integer
    Private mdecContract_duration_months As Decimal
    Private mdtRequisition_date As Date
    'Sohail (12 Oct 2018) -- End
    'Sohail (29 Sep 2021) -- Start
    'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
    Private mintJob_report_tounkid As Integer
    Private mintJobadvertid As Integer
    'Sohail (29 Sep 2021) -- End

    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set staffrequisitiontranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Staffrequisitiontranunkid() As Integer
        Get
            Return mintStaffrequisitiontranunkid
        End Get
        Set(ByVal value As Integer)
            mintStaffrequisitiontranunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formno
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Formno() As String
        Get
            Return mstrFormno
        End Get
        Set(ByVal value As String)
            mstrFormno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set staffrequisitiontypeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Staffrequisitiontypeid() As Integer
        Get
            Return mintStaffrequisitiontypeid
        End Get
        Set(ByVal value As Integer)
            mintStaffrequisitiontypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set staffrequisitionbyid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Staffrequisitionbyid() As Integer
        Get
            Return mintStaffrequisitionbyid
        End Get
        Set(ByVal value As Integer)
            mintStaffrequisitionbyid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Allocationunkid() As Integer
        Get
            Return mintAllocationunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classgroupunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Classgroupunkid() As Integer
        Get
            Return mintClassgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintClassgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Classunkid() As Integer
        Get
            Return mintClassunkid
        End Get
        Set(ByVal value As Integer)
            mintClassunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actionreasonunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Actionreasonunkid() As Integer
        Get
            Return mintActionreasonunkid
        End Get
        Set(ByVal value As Integer)
            mintActionreasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set additionalstaffreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Additionalstaffreason() As String
        Get
            Return mstrAdditionalstaffreason
        End Get
        Set(ByVal value As String)
            mstrAdditionalstaffreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobdescrription
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Jobdescrription() As String
        Get
            Return mstrJobdescrription
        End Get
        Set(ByVal value As String)
            mstrJobdescrription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workstartdate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Workstartdate() As Date
        Get
            Return mdtWorkstartdate
        End Get
        Set(ByVal value As Date)
            mdtWorkstartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set noofposition
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Noofposition() As Integer
        Get
            Return mintNoofposition
        End Get
        Set(ByVal value As Integer)
            mintNoofposition = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_statusunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Form_Statusunkid() As Integer
        Get
            Return mintForm_statusunkid
        End Get
        Set(ByVal value As Integer)
            mintForm_statusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Sohail (12 Oct 2018) -- Start
    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradelevelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Gradelevelunkid() As Integer
        Get
            Return mintGradelevelunkid
        End Get
        Set(ByVal value As Integer)
            mintGradelevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approved_headcount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Approved_Headcount() As Integer
        Get
            Return mintApproved_headcount
        End Get
        Set(ByVal value As Integer)
            mintApproved_headcount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actual_headcount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Actual_Headcount() As Integer
        Get
            Return mintActual_headcount
        End Get
        Set(ByVal value As Integer)
            mintActual_headcount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employmenttypeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employmenttypeunkid() As Integer
        Get
            Return mintEmploymenttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmploymenttypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contract_duration_months
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Contract_Duration_Months() As Decimal
        Get
            Return mdecContract_duration_months
        End Get
        Set(ByVal value As Decimal)
            mdecContract_duration_months = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set requisition_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Requisition_Date() As Date
        Get
            Return mdtRequisition_date
        End Get
        Set(ByVal value As Date)
            mdtRequisition_date = value
        End Set
    End Property
    'Sohail (12 Oct 2018) -- End

    'Sohail (29 Sep 2021) -- Start
    'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
    Public Property _Job_report_tounkid() As Integer
        Get
            Return mintJob_report_tounkid
        End Get
        Set(ByVal value As Integer)
            mintJob_report_tounkid = value
        End Set
    End Property

    Public Property _Jobadvertid() As Integer
        Get
            Return mintJobadvertid
        End Get
        Set(ByVal value As Integer)
            mintJobadvertid = value
        End Set
    End Property
    'Sohail (29 Sep 2021) -- End


    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

    'Hemant (17 Jul 2019) -- Start
    'ISSUE - Staff requisition is taking a lot of time to save on MSS.
    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
    'Hemant (17 Jul 2019) -- End


#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If

        Try
            strQ = "SELECT " & _
              "  staffrequisitiontranunkid " & _
              ", formno " & _
              ", staffrequisitiontypeid " & _
              ", staffrequisitionbyid " & _
              ", allocationunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", employeeunkid " & _
              ", actionreasonunkid " & _
              ", additionalstaffreason " & _
              ", jobunkid " & _
              ", jobdescrription " & _
              ", workstartdate " & _
              ", noofposition " & _
              ", form_statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(gradeunkid, 0) AS gradeunkid " & _
              ", ISNULL(gradelevelunkid, 0) AS gradelevelunkid " & _
              ", ISNULL(approved_headcount, 0) AS approved_headcount " & _
              ", ISNULL(actual_headcount, 0) AS actual_headcount " & _
              ", ISNULL(employmenttypeunkid, 0) AS employmenttypeunkid " & _
              ", ISNULL(contract_duration_months, 0) AS contract_duration_months " & _
              ", ISNULL(requisition_date, GETDATE()) AS requisition_date " & _
              ", ISNULL(job_report_tounkid, 0) AS job_report_tounkid " & _
              ", ISNULL(jobadvertid, 0) AS jobadvertid " & _
             "FROM rcstaffrequisition_tran " & _
             "WHERE staffrequisitiontranunkid = @staffrequisitiontranunkid "
            'Sohail (29 Sep 2021) - [job_report_tounkid, jobadvertid]
            'Sohail (12 Oct 2018) - [gradeunkid, gradelevelunkid, approved_headcount, actual_headcount, employmenttypeunkid, contract_duration_months, requisition_date]

            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStaffrequisitiontranunkid = CInt(dtRow.Item("staffrequisitiontranunkid"))
                mstrFormno = dtRow.Item("formno").ToString
                mintStaffrequisitiontypeid = CInt(dtRow.Item("staffrequisitiontypeid"))
                mintStaffrequisitionbyid = CInt(dtRow.Item("staffrequisitionbyid"))
                mintAllocationunkid = CInt(dtRow.Item("allocationunkid"))
                mintClassgroupunkid = CInt(dtRow.Item("classgroupunkid"))
                mintClassunkid = CInt(dtRow.Item("classunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintActionreasonunkid = CInt(dtRow.Item("actionreasonunkid"))
                mstrAdditionalstaffreason = dtRow.Item("additionalstaffreason").ToString
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mstrJobdescrription = dtRow.Item("jobdescrription").ToString
                mdtWorkstartdate = dtRow.Item("workstartdate")
                mintNoofposition = CInt(dtRow.Item("noofposition"))
                mintForm_statusunkid = CInt(dtRow.Item("form_statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintGradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                mintApproved_headcount = CInt(dtRow.Item("approved_headcount"))
                mintActual_headcount = CInt(dtRow.Item("actual_headcount"))
                mintEmploymenttypeunkid = CInt(dtRow.Item("employmenttypeunkid"))
                mdecContract_duration_months = CDec(dtRow.Item("contract_duration_months"))
                If IsDBNull(dtRow.Item("requisition_date")) = True Then
                    mdtRequisition_date = Nothing
                Else
                    mdtRequisition_date = dtRow.Item("requisition_date")
                End If
                'Sohail (12 Oct 2018) -- End
                'Sohail (29 Sep 2021) -- Start
                'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
                mintJob_report_tounkid = CInt(dtRow.Item("job_report_tounkid"))
                mintJobadvertid = CInt(dtRow.Item("jobadvertid"))
                'Sohail (29 Sep 2021) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal strFilter As String = "", Optional ByVal strOrderBy As String = "", Optional ByVal intOnlyThisUserunkIdApprovals As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                          ", rcstaffrequisition_tran.formno " & _
                          ", rcstaffrequisition_tran.staffrequisitiontypeid " & _
                          ", rcstaffrequisition_tran.staffrequisitionbyid " & _
                          ", rcstaffrequisition_tran.allocationunkid " & _
                          ", ISNULL(hrstation_master.name, '') AS Branch " & _
                          ", ISNULL(hrdepartment_group_master.name, '') AS DepartmentGroup " & _
                          ", ISNULL(hrdepartment_master.name, '') AS Department " & _
                          ", ISNULL(hrsectiongroup_master.name, '') AS SectionGroup " & _
                          ", ISNULL(hrsection_master.name, '') AS Section " & _
                          ", ISNULL(hrunitgroup_master.name, '') AS UnitGroup " & _
                          ", ISNULL(hrunit_master.name, '') AS Unit " & _
                          ", ISNULL(hrteam_master.name, '') AS Team " & _
                          ", ISNULL(hrjobgroup_master.name, '') AS JobGroup " & _
                          ", ISNULL(hrjob_master.job_name, '') AS Job " & _
                          ", ISNULL(hrclassgroup_master.name, '') AS ClassGroup " & _
                          ", ISNULL(hrclasses_master.name, '') AS Class " & _
                          ", rcstaffrequisition_tran.classgroupunkid " & _
                          ", ISNULL(cg.name, '') AS Staff_ClassGroup " & _
                          ", rcstaffrequisition_tran.classunkid " & _
                          ", ISNULL(c.name, '') AS Staff_Class " & _
                          ", rcstaffrequisition_tran.employeeunkid " & _
                          ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                          ", rcstaffrequisition_tran.actionreasonunkid " & _
                          ", ISNULL(STR.reason_action, '') AS actionreason " & _
                          ", rcstaffrequisition_tran.additionalstaffreason " & _
                          ", rcstaffrequisition_tran.jobunkid " & _
                          ", ISNULL(j.job_name, '') AS JobTitle " & _
                          ", rcstaffrequisition_tran.jobdescrription " & _
                          ", CONVERT(CHAR(8), rcstaffrequisition_tran.workstartdate, 112) AS workstartdate " & _
                          ", rcstaffrequisition_tran.noofposition " & _
                          ", rcstaffrequisition_tran.form_statusunkid " & _
                          ", rcstaffrequisition_tran.userunkid " & _
                          ", rcstaffrequisition_tran.isvoid " & _
                          ", rcstaffrequisition_tran.voiduserunkid " & _
                          ", rcstaffrequisition_tran.voiddatetime " & _
                          ", rcstaffrequisition_tran.voidreason " & _
                          ", ISNULL(rcstaffrequisition_tran.gradeunkid, 0) AS gradeunkid " & _
                          ", ISNULL(g.name, '') AS Grade " & _
                          ", ISNULL(rcstaffrequisition_tran.gradelevelunkid, 0) AS gradelevelunkid " & _
                          ", ISNULL(gl.name, '') AS GradeLevel " & _
                          ", ISNULL(rcstaffrequisition_tran.approved_headcount, 0) AS approved_headcount " & _
                          ", ISNULL(rcstaffrequisition_tran.actual_headcount, 0) AS actual_headcount " & _
                          ", ISNULL(rcstaffrequisition_tran.employmenttypeunkid, 0) AS employmenttypeunkid " & _
                          ", ISNULL(etype.name, '') AS employmenttype " & _
                          ", ISNULL(rcstaffrequisition_tran.contract_duration_months, 0) AS contract_duration_months " & _
                          ", ISNULL(rcstaffrequisition_tran.requisition_date, GETDATE()) AS requisition_date " & _
                          ", ISNULL(rcstaffrequisition_tran.job_report_tounkid, 0) AS job_report_tounkid " & _
                          ", ISNULL(rcstaffrequisition_tran.jobadvertid, 0) AS jobadvertid " & _
                          ", rcstaffrequisition_tran.formno + ' - ' + ISNULL(j.job_name, '') As FormNoWithJobTitle " & _
                    "FROM    rcstaffrequisition_tran " & _
                            "LEFT JOIN hrclassgroup_master AS cg ON cg.classgroupunkid = rcstaffrequisition_tran.classgroupunkid " & _
                            "LEFT JOIN hrclasses_master AS c ON c.classesunkid = rcstaffrequisition_tran.classunkid " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcstaffrequisition_tran.employeeunkid " & _
                            "LEFT JOIN hraction_reason_master AS STR ON STR.actionreasonunkid = rcstaffrequisition_tran.actionreasonunkid " & _
                            "LEFT JOIN hrjob_master AS j ON j.jobunkid = rcstaffrequisition_tran.jobunkid " & _
                            "LEFT JOIN hrgrade_master AS g ON g.gradeunkid = rcstaffrequisition_tran.gradeunkid " & _
                            "LEFT JOIN hrgradelevel_master AS gl ON gl.gradelevelunkid = rcstaffrequisition_tran.gradelevelunkid " & _
                            "LEFT JOIN cfcommon_master AS etype ON etype.masterunkid = rcstaffrequisition_tran.employmenttypeunkid AND etype.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE) & "' " & _
                            "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.DEPARTMENT) & " " & _
                            "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.JOBS) & " " & _
                            "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.BRANCH) & " " & _
                            "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.DEPARTMENT_GROUP) & " " & _
                            "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.SECTION) & " " & _
                            "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.UNIT) & " " & _
                            "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.JOB_GROUP) & " " & _
                            "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.CLASS_GROUP) & " " & _
                            "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.CLASSES) & " " & _
                            "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = rcstaffrequisition_tran.allocationunkid AND staffrequisitionbyid = " & CInt(enAllocation.TEAM) & " " & _
                            "LEFT JOIN hrunitgroup_master ON rcstaffrequisition_tran.allocationunkid = hrunitgroup_master.unitgroupunkid AND staffrequisitionbyid = " & CInt(enAllocation.UNIT_GROUP) & " " & _
                            "LEFT JOIN hrsectiongroup_master ON rcstaffrequisition_tran.allocationunkid = hrsectiongroup_master.sectiongroupunkid AND staffrequisitionbyid = " & CInt(enAllocation.SECTION_GROUP) & " "

            'Hemant (01 Nov 2021) -- [FormNoWithJobTitle]
            'Sohail (29 Sep 2021) - [job_report_tounkid, jobadvertid]
            'Hemant (06 Aug 2019) -- [", ISNULL(STR.name, '') AS actionreason " & _ --> ", ISNULL(STR.reason_action, '') AS actionreason " & _ , "LEFT JOIN cfcommon_master AS STR ON STR.masterunkid = rcstaffrequisition_tran.actionreasonunkid AND STR.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.TERMINATION) & "' " & _ --> "LEFT JOIN hraction_reason_master AS STR ON STR.actionreasonunkid = rcstaffrequisition_tran.actionreasonunkid " & _ ]
            'Sohail (12 Oct 2018) - [gradeunkid, Grade, gradelevelunkid, GradeLevel, approved_headcount, actual_headcount, employmenttypeunkid, contract_duration_months, requisition_date]

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            If intOnlyThisUserunkIdApprovals > 0 Then
                strQ &= "JOIN rcstaffrequisition_approver_mapping ON rcstaffrequisition_approver_mapping.userapproverunkid = @userapproverunkid AND rcstaffrequisition_approver_mapping.allocationid = staffrequisitionbyid AND rcstaffrequisition_approver_mapping.allocationunkid = rcstaffrequisition_tran.allocationunkid "

                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                strQ &= " AND rcstaffrequisition_approver_mapping.isvoid = 0 "
                'Pinkal (16-Nov-2021)-- End
                objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intOnlyThisUserunkIdApprovals)
            End If
            'Sohail (12 Oct 2018) -- End

            strQ &= "WHERE   ISNULL(rcstaffrequisition_tran.isvoid, 0) = 0 "
            'Sohail (12 Oct 2018) - [AND staffrequisitionbyid = " & enAllocation.]
            'S.SANDEEP [09 APR 2015] -- START
            ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = rcstaffrequisition_tran.actionreasonunkid ------ REMOVED
            ' LEFT JOIN cfcommon_master AS STR ON STR.masterunkid = rcstaffrequisition_tran.actionreasonunkid AND STR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            'S.SANDEEP [09 APR 2015] -- END


            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            If strOrderBy.Trim <> "" Then
                strQ &= " ORDER BY " & strOrderBy
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcstaffrequisition_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date, ByVal intStaffReqFormNoType As Integer, ByVal strStaffReqFormNoPrefix As String, Optional ByVal dtTable As DataTable = Nothing, _
                           Optional ByVal mdtAttachmentTable As DataTable = Nothing) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime, intStaffReqFormNoType, strStaffReqFormNoPrefix]
        'Sohail (18 Mar 2015) - [dtTable]

        'Gajanan [6-NOV-2019] -- [mdtAttachmentTable]


        If isExistSameDateRequisition(mintStaffrequisitionbyid, mintAllocationunkid, mintJobunkid, mdtWorkstartdate, mintClassunkid) Then
            'Sohail (16 Dec 2021) - [mintClassunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Staff Requisition is already defined. Please define new Staff Requisition.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'Dim intStaffReqFormNoType As Integer = 0
        'intStaffReqFormNoType = ConfigParameter._Object._StaffReqFormNoType
        'Sohail (21 Aug 2015) -- End
        If intStaffReqFormNoType = 0 Then
            If isExist(mstrFormno) Then
                mstrMessage = Language.getMessage(mstrModuleName, 2, "This Staff Requisition Form No. is already defined. Please define new Staff Requisition Form No.")
                Return False
            End If
        End If

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormno.ToString)
            objDataOperation.AddParameter("@staffrequisitiontypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontypeid.ToString)
            objDataOperation.AddParameter("@staffrequisitionbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitionbyid.ToString)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionreasonunkid.ToString)
            objDataOperation.AddParameter("@additionalstaffreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdditionalstaffreason.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            'objDataOperation.AddParameter("@jobdescrription", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJobdescrription.ToString)
            objDataOperation.AddParameter("@jobdescrription", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrJobdescrription.ToString)
            'Sohail (12 Oct 2018) -- End
            objDataOperation.AddParameter("@workstartdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWorkstartdate.ToString)
            objDataOperation.AddParameter("@noofposition", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofposition.ToString)
            objDataOperation.AddParameter("@form_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintForm_statusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@approved_headcount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproved_headcount.ToString)
            objDataOperation.AddParameter("@actual_headcount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActual_headcount.ToString)
            objDataOperation.AddParameter("@employmenttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmploymenttypeunkid.ToString)
            objDataOperation.AddParameter("@contract_duration_months", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecContract_duration_months.ToString)
            If mdtRequisition_date = Nothing Then
                objDataOperation.AddParameter("@requisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@requisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequisition_date.ToString)
            End If
            'Sohail (12 Oct 2018) -- End
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            objDataOperation.AddParameter("@job_report_tounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJob_report_tounkid.ToString)
            objDataOperation.AddParameter("@jobadvertid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobadvertid.ToString)
            'Sohail (29 Sep 2021) -- End

            strQ = "INSERT INTO rcstaffrequisition_tran ( " & _
              "  formno " & _
              ", staffrequisitiontypeid " & _
              ", staffrequisitionbyid " & _
              ", allocationunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", employeeunkid " & _
              ", actionreasonunkid " & _
              ", additionalstaffreason " & _
              ", jobunkid " & _
              ", jobdescrription " & _
              ", workstartdate " & _
              ", noofposition " & _
              ", form_statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", approved_headcount " & _
              ", actual_headcount " & _
              ", employmenttypeunkid " & _
              ", contract_duration_months " & _
              ", requisition_date " & _
              ", job_report_tounkid " & _
              ", jobadvertid " & _
            ") VALUES (" & _
              "  @formno " & _
              ", @staffrequisitiontypeid " & _
              ", @staffrequisitionbyid " & _
              ", @allocationunkid " & _
              ", @classgroupunkid " & _
              ", @classunkid " & _
              ", @employeeunkid " & _
              ", @actionreasonunkid " & _
              ", @additionalstaffreason " & _
              ", @jobunkid " & _
              ", @jobdescrription " & _
              ", @workstartdate " & _
              ", @noofposition " & _
              ", @form_statusunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @gradeunkid " & _
              ", @gradelevelunkid " & _
              ", @approved_headcount " & _
              ", @actual_headcount " & _
              ", @employmenttypeunkid " & _
              ", @contract_duration_months " & _
              ", @requisition_date " & _
              ", @job_report_tounkid " & _
              ", @jobadvertid " & _
            "); SELECT @@identity"
            'Sohail (29 Sep 2021) - [job_report_tounkid, jobadvertid]
            'Sohail (12 Oct 2018) - [gradeunkid, gradelevelunkid, approved_headcount, actual_headcount, employmenttypeunkid, contract_duration_months, requisition_date]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStaffrequisitiontranunkid = dsList.Tables(0).Rows(0).Item(0)


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
            If mdtAttachmentTable IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.STAFF_REQUISITION).Tables(0).Rows(0)("Name").ToString
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                Dim dr As DataRow
                For Each drow As DataRow In mdtAttachmentTable.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = mintStaffrequisitiontranunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("userunkid") = mintUserunkid
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")

                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Gajanan [6-NOV-2019] -- End




            If intStaffReqFormNoType = 1 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Set_AutoNumber(objDataOperation, mintStaffrequisitiontranunkid, "rcstaffrequisition_tran", "formno", "staffrequisitiontranunkid", "NextStaffReqFormNo", ConfigParameter._Object._StaffReqFormNoPrefix) = False Then
                'Hemant (17 Jul 2019) -- Start
                'ISSUE - Staff requisition is taking a lot of time to save on MSS.
                'If Set_AutoNumber(objDataOperation, mintStaffrequisitiontranunkid, "rcstaffrequisition_tran", "formno", "staffrequisitiontranunkid", "NextStaffReqFormNo", strStaffReqFormNoPrefix) = False Then
                If Set_AutoNumber(objDataOperation, mintStaffrequisitiontranunkid, "rcstaffrequisition_tran", "formno", "staffrequisitiontranunkid", "NextStaffReqFormNo", strStaffReqFormNoPrefix, mintCompanyUnkid) = False Then
                    'Hemant (17 Jul 2019) -- End
                    'Sohail (21 Aug 2015) -- End
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If Get_Saved_Number(objDataOperation, mintStaffrequisitiontranunkid, "rcstaffrequisition_tran", "formno", "staffrequisitiontranunkid", mstrFormno) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStaffRequisitionTran(objDataOperation, 1) = False Then
            If InsertAuditTrailForStaffRequisitionTran(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            If dtTable IsNot Nothing Then

                Dim objStaffEmp As New clsStaffrequisition_emp_tran
                'Sohail (11 Nov 2019) -- Start
                'NMB Issue # : Bind transaction issue (SqlConnection does not support parallel transactions).
                objStaffEmp._xDataOp = objDataOperation
                'Sohail (11 Nov 2019) -- End
                objStaffEmp._Staffrequisitiontranunkid = mintStaffrequisitiontranunkid
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objStaffEmp.InsertUpdateDelete(dtTable) = False Then
                If objStaffEmp.InsertUpdateDelete(dtTable, dtCurrentDateAndTime, objDataOperation) = False Then
                    'Sohail (11 Nov 2019) - [objDataOperation]
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If
            'Sohail (18 Mar 2015) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcstaffrequisition_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As Date, Optional ByVal objDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal dtTable As DataTable = Nothing, _
                           Optional ByVal mdtAttachmentTable As DataTable = Nothing) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
        'Sohail (18 Mar 2015) - [dtTable]
        If isExistSameDateRequisition(mintStaffrequisitionbyid, mintAllocationunkid, mintJobunkid, mdtWorkstartdate, mintClassunkid, mintStaffrequisitiontranunkid) Then
            'Sohail (16 Dec 2021) - [mintClassunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Staff Requisition is already defined. Please define new Staff Requisition.")
            If objDataOpr IsNot Nothing Then objDataOpr.ReleaseTransaction(False)
            Return False
        End If

        If isExist(mstrFormno, mintStaffrequisitiontranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Staff Requisition Form No. is already defined. Please define new Staff Requisition Form No.")
            If objDataOpr IsNot Nothing Then objDataOpr.ReleaseTransaction(False)
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If

        Try
            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormno.ToString)
            objDataOperation.AddParameter("@staffrequisitiontypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontypeid.ToString)
            objDataOperation.AddParameter("@staffrequisitionbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitionbyid.ToString)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionreasonunkid.ToString)
            objDataOperation.AddParameter("@additionalstaffreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdditionalstaffreason.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            'objDataOperation.AddParameter("@jobdescrription", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJobdescrription.ToString)
            objDataOperation.AddParameter("@jobdescrription", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrJobdescrription.ToString)
            'Sohail (12 Oct 2018) -- End
            objDataOperation.AddParameter("@workstartdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWorkstartdate.ToString)
            objDataOperation.AddParameter("@noofposition", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofposition.ToString)
            objDataOperation.AddParameter("@form_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintForm_statusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@approved_headcount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproved_headcount.ToString)
            objDataOperation.AddParameter("@actual_headcount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActual_headcount.ToString)
            objDataOperation.AddParameter("@employmenttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmploymenttypeunkid.ToString)
            objDataOperation.AddParameter("@contract_duration_months", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecContract_duration_months.ToString)
            If mdtRequisition_date = Nothing Then
                objDataOperation.AddParameter("@requisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@requisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequisition_date.ToString)
            End If
            'Sohail (12 Oct 2018) -- End
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            objDataOperation.AddParameter("@job_report_tounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJob_report_tounkid.ToString)
            objDataOperation.AddParameter("@jobadvertid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobadvertid.ToString)
            'Sohail (29 Sep 2021) -- End

            strQ = "UPDATE rcstaffrequisition_tran SET " & _
              "  formno = @formno" & _
              ", staffrequisitiontypeid = @staffrequisitiontypeid" & _
              ", staffrequisitionbyid = @staffrequisitionbyid" & _
              ", allocationunkid = @allocationunkid" & _
              ", classgroupunkid = @classgroupunkid" & _
              ", classunkid = @classunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", actionreasonunkid = @actionreasonunkid" & _
              ", additionalstaffreason = @additionalstaffreason" & _
              ", jobunkid = @jobunkid" & _
              ", jobdescrription = @jobdescrription" & _
              ", workstartdate = @workstartdate" & _
              ", noofposition = @noofposition" & _
              ", form_statusunkid = @form_statusunkid " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", gradeunkid = @gradeunkid" & _
              ", gradelevelunkid = @gradelevelunkid" & _
              ", approved_headcount = @approved_headcount" & _
              ", actual_headcount = @actual_headcount" & _
              ", employmenttypeunkid = @employmenttypeunkid" & _
              ", contract_duration_months = @contract_duration_months" & _
              ", requisition_date = @requisition_date " & _
              ", job_report_tounkid = @job_report_tounkid " & _
              ", jobadvertid = @jobadvertid " & _
            "WHERE staffrequisitiontranunkid = @staffrequisitiontranunkid "
            'Sohail (29 Sep 2021) - [job_report_tounkid, jobadvertid]
            'Sohail (12 Oct 2018) - [gradeunkid, gradelevelunkid, approved_headcount, actual_headcount, employmenttypeunkid, contract_duration_months, requisition_date]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   

            If mdtAttachmentTable IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.STAFF_REQUISITION).Tables(0).Rows(0)("Name").ToString
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                Dim dr As DataRow
                For Each drow As DataRow In mdtAttachmentTable.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("userunkid") = mintUserunkid
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")

                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Gajanan [6-NOV-2019] -- End



            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStaffRequisitionTran(objDataOperation, 2) = False Then
            If InsertAuditTrailForStaffRequisitionTran(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            If dtTable IsNot Nothing Then
                Dim objStaffEmp As New clsStaffrequisition_emp_tran
                'Sohail (11 Nov 2019) -- Start
                'NMB Issue # : Bind transaction issue (SqlConnection does not support parallel transactions).
                objStaffEmp._xDataOp = objDataOperation
                'Sohail (11 Nov 2019) -- End
                objStaffEmp._Staffrequisitiontranunkid = mintStaffrequisitiontranunkid
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objStaffEmp.InsertUpdateDelete(dtTable) = False Then
                If objStaffEmp.InsertUpdateDelete(dtTable, dtCurrentDateAndTime, objDataOperation) = False Then
                    'Hemant (13 Sep 2019) -- [objDataOperation]
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            'Sohail (18 Mar 2015) -- End

            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            'objDataOperation.ReleaseTransaction(False)
            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (13 Sep 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            ' objDataOperation = Nothing
            If objDataOpr Is Nothing Then objDataOperation = Nothing
            'Hemant (13 Sep 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcstaffrequisition_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal intVoiduserunkid As Integer, ByVal dtVoiddatetime As Date, ByVal strVoidreason As String) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE rcstaffrequisition_tran SET isvoid = 1, voiduserunkid = @voiduserunkid, voiddatetime = @voiddatetime, voidreason = @voidreason " & _
            "WHERE staffrequisitiontranunkid = @staffrequisitiontranunkid "

            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._Staffrequisitiontranunkid = intUnkid



            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
            Dim objDocument As New clsScan_Attach_Documents
            If objDocument.DeleteTransacation(-1, enScanAttactRefId.STAFF_REQUISITION, intUnkid, objDataOperation, _WebFormName) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Gajanan [6-NOV-2019] -- End


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStaffRequisitionTran(objDataOperation, 3) = False Then
            If InsertAuditTrailForStaffRequisitionTran(objDataOperation, 3, dtVoiddatetime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT rcstaffrequisition_approval_tran.staffrequisitionapprovaltranunkid " & _
                                "FROM   rcstaffrequisition_approval_tran " & _
                                "WHERE isvoid = 0 " & _
                                    "AND rcstaffrequisition_approval_tran.staffrequisitiontranunkid = @staffrequisitiontranunkid "


            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strFormNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  staffrequisitiontranunkid " & _
              ", formno " & _
              ", staffrequisitiontypeid " & _
              ", staffrequisitionbyid " & _
              ", allocationunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", employeeunkid " & _
              ", actionreasonunkid " & _
              ", additionalstaffreason " & _
              ", jobunkid " & _
              ", jobdescrription " & _
              ", workstartdate " & _
              ", noofposition " & _
              ", form_statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", approved_headcount " & _
              ", actual_headcount " & _
              ", employmenttypeunkid " & _
              ", contract_duration_months " & _
              ", requisition_date " & _
             "FROM rcstaffrequisition_tran " & _
             "WHERE ISNULL(isvoid, 0) = 0 " & _
             "AND formno = @formno "
            'Sohail (12 Oct 2018) - [gradeunkid, gradelevelunkid, approved_headcount, actual_headcount, employmenttypeunkid, contract_duration_months, requisition_date]

            If intUnkid > 0 Then
                strQ &= " AND staffrequisitiontranunkid <> @staffrequisitiontranunkid"
            End If

            objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strFormNo)
            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistSameDateRequisition(ByVal intStaffReqById As Integer, ByVal intAllocationUnkId As Integer, ByVal intJobUnkId As Integer, ByVal dtWorkStart As Date, ByVal intClassUnkId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        'Sohail (16 Dec 2021) - [intClassUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  staffrequisitiontranunkid " & _
              ", formno " & _
              ", staffrequisitiontypeid " & _
              ", staffrequisitionbyid " & _
              ", allocationunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", employeeunkid " & _
              ", actionreasonunkid " & _
              ", additionalstaffreason " & _
              ", jobunkid " & _
              ", jobdescrription " & _
              ", workstartdate " & _
              ", noofposition " & _
              ", form_statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", approved_headcount " & _
              ", actual_headcount " & _
              ", employmenttypeunkid " & _
              ", contract_duration_months " & _
              ", requisition_date " & _
             "FROM rcstaffrequisition_tran " & _
             "WHERE ISNULL(isvoid, 0) = 0 " & _
             "AND staffrequisitionbyid = @staffrequisitionbyid " & _
             "AND allocationunkid = @allocationunkid " & _
             "AND jobunkid = @jobunkid " & _
             "AND CONVERT(CHAR(8), workstartdate, 112) = @workstartdate "
            'Sohail (12 Oct 2018) - [gradeunkid, gradelevelunkid, approved_headcount, actual_headcount, employmenttypeunkid, contract_duration_months, requisition_date]

            'Sohail (16 Dec 2021) -- Start
            'Enhancement : : NMB - Allow to create staff requisition if class is not same.
            If intClassUnkId > 0 Then
                strQ &= " AND classunkid = @classunkid "
                objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassUnkId)
            End If
            'Sohail (16 Dec 2021) -- End

            If intUnkid > 0 Then
                strQ &= " AND staffrequisitiontranunkid <> @staffrequisitiontranunkid"
            End If

            objDataOperation.AddParameter("@staffrequisitionbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqById)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobUnkId)
            objDataOperation.AddParameter("@workstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtWorkStart))
            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistSameDateRequisition; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForStaffRequisitionTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormno.ToString)
            objDataOperation.AddParameter("@staffrequisitiontypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontypeid.ToString)
            objDataOperation.AddParameter("@staffrequisitionbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitionbyid.ToString)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionreasonunkid.ToString)
            objDataOperation.AddParameter("@additionalstaffreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdditionalstaffreason.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            'objDataOperation.AddParameter("@jobdescrription", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJobdescrription.ToString)
            objDataOperation.AddParameter("@jobdescrription", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrJobdescrription.ToString)
            'Sohail (12 Oct 2018) -- End
            objDataOperation.AddParameter("@workstartdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWorkstartdate.ToString)
            objDataOperation.AddParameter("@noofposition", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofposition.ToString)
            objDataOperation.AddParameter("@form_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintForm_statusunkid.ToString)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@approved_headcount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproved_headcount.ToString)
            objDataOperation.AddParameter("@actual_headcount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActual_headcount.ToString)
            objDataOperation.AddParameter("@employmenttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmploymenttypeunkid.ToString)
            objDataOperation.AddParameter("@contract_duration_months", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecContract_duration_months.ToString)
            If mdtRequisition_date = Nothing Then
                objDataOperation.AddParameter("@requisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@requisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequisition_date.ToString)
            End If
            'Sohail (12 Oct 2018) -- End
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            objDataOperation.AddParameter("@job_report_tounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJob_report_tounkid.ToString)
            objDataOperation.AddParameter("@jobadvertid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobadvertid.ToString)
            'Sohail (29 Sep 2021) -- End
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 3, "WEB"))
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            strQ = "INSERT INTO atrcstaffrequisition_tran ( " & _
                            "  staffrequisitiontranunkid " & _
                            ", formno " & _
                            ", staffrequisitiontypeid " & _
                            ", staffrequisitionbyid " & _
                            ", allocationunkid " & _
                            ", classgroupunkid " & _
                            ", classunkid " & _
                            ", employeeunkid " & _
                            ", actionreasonunkid " & _
                            ", additionalstaffreason " & _
                            ", jobunkid " & _
                            ", jobdescrription " & _
                            ", workstartdate " & _
                            ", noofposition " & _
                            ", form_statusunkid " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name" & _
                            ", form_name " & _
                            ", module_name1 " & _
                            ", module_name2 " & _
                            ", module_name3 " & _
                            ", module_name4 " & _
                            ", module_name5 " & _
                            ", isweb " & _
                            ", loginemployeeunkid " & _
                            ", gradeunkid " & _
                            ", gradelevelunkid " & _
                            ", approved_headcount " & _
                            ", actual_headcount " & _
                            ", employmenttypeunkid " & _
                            ", contract_duration_months " & _
                            ", requisition_date " & _
                            ", job_report_tounkid " & _
                            ", jobadvertid " & _
                    ") VALUES (" & _
                            "  @staffrequisitiontranunkid " & _
                            ", @formno " & _
                            ", @staffrequisitiontypeid " & _
                            ", @staffrequisitionbyid " & _
                            ", @allocationunkid " & _
                            ", @classgroupunkid " & _
                            ", @classunkid " & _
                            ", @employeeunkid " & _
                            ", @actionreasonunkid " & _
                            ", @additionalstaffreason " & _
                            ", @jobunkid " & _
                            ", @jobdescrription " & _
                            ", @workstartdate " & _
                            ", @noofposition " & _
                            ", @form_statusunkid " & _
                            ", @audittype " & _
                            ", @audituserunkid " & _
                            ", @auditdatetime " & _
                            ", @ip " & _
                            ", @machine_name" & _
                            ", @form_name " & _
                            ", @module_name1 " & _
                            ", @module_name2 " & _
                            ", @module_name3 " & _
                            ", @module_name4 " & _
                            ", @module_name5 " & _
                            ", @isweb " & _
                            ", @loginemployeeunkid " & _
                            ", @gradeunkid " & _
                            ", @gradelevelunkid " & _
                            ", @approved_headcount " & _
                            ", @actual_headcount " & _
                            ", @employmenttypeunkid " & _
                            ", @contract_duration_months " & _
                            ", @requisition_date " & _
                            ", @job_report_tounkid " & _
                            ", @jobadvertid " & _
                        "); SELECT @@identity"
            'Sohail (29 Sep 2021) - [job_report_tounkid, jobadvertid]
            'Sohail (12 Oct 2018) - [gradelevelunkid, approved_headcount, actual_headcount, employmenttypeunkid, contract_duration_months, requisition_date]

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForStaffRequisitionTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Staff Requisition is already defined. Please define new Staff Requisition.")
            Language.setMessage(mstrModuleName, 2, "This Staff Requisition Form No. is already defined. Please define new Staff Requisition Form No.")
            Language.setMessage(mstrModuleName, 3, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
