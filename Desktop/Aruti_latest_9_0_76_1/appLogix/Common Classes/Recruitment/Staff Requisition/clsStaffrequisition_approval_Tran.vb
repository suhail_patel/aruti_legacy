﻿'************************************************************************************************************************************
'Class Name : clsStaffrequisition_approval_Tran.vb
'Purpose    :
'Date       :09/06/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Text
Imports eZee.Common.eZeeForm
Imports System.Web


''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsStaffrequisition_approval_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsStaffrequisition_approval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStaffrequisitionapprovaltranunkid As Integer
    Private mintStaffrequisitiontranunkid As Integer
    Private mintLevelunkid As Integer
    Private mintPriority As Integer
    Private mdtApproval_Date As Date
    Private mintStatusunkid As Integer
    Private mstrDisapprovalreason As String = String.Empty
    Private mstrRemarks As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty

    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""

    Private mdtTable As DataTable
#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTable = New DataTable

            mdtTable.Columns.Add("Staffrequisitionapprovaltranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("Staffrequisitiontranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("levelunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("priority", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("approval_date", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("statusunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("remarks", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("disapprovalreason", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = String.Empty

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set staffrequisitionapprovaltranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Staffrequisitionapprovaltranunkid() As Integer
        Get
            Return mintStaffrequisitionapprovaltranunkid
        End Get
        Set(ByVal value As Integer)
            mintStaffrequisitionapprovaltranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set staffrequisitiontranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Staffrequisitiontranunkid() As Integer
        Get
            Return mintStaffrequisitiontranunkid
        End Get
        Set(ByVal value As Integer)
            mintStaffrequisitiontranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approval_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Approval_Date() As Date
        Get
            Return mdtApproval_Date
        End Get
        Set(ByVal value As Date)
            mdtApproval_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disapprovalreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Disapprovalreason() As String
        Get
            Return mstrDisapprovalreason
        End Get
        Set(ByVal value As String)
            mstrDisapprovalreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remarks
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

    'Sohail (23 Sep 2021) -- Start
    'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
    Private lstWebEmail As List(Of clsEmailCollection) = Nothing
    Public ReadOnly Property _WebEmailList() As List(Of clsEmailCollection)
        Get
            Return lstWebEmail
        End Get
    End Property
    'Sohail (23 Sep 2021) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  staffrequisitionapprovaltranunkid " & _
              ", staffrequisitiontranunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", approval_date " & _
              ", statusunkid " & _
              ", disapprovalreason " & _
              ", remarks " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM rcstaffrequisition_approval_tran " & _
             "WHERE staffrequisitionapprovaltranunkid = @staffrequisitionapprovaltranunkid "

            objDataOperation.AddParameter("@staffrequisitionapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitionapprovaltranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStaffrequisitionapprovaltranunkid = CInt(dtRow.Item("staffrequisitionapprovaltranunkid"))
                mintStaffrequisitiontranunkid = CInt(dtRow.Item("staffrequisitiontranunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mdtApproval_Date = dtRow.Item("approval_date")
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrDisapprovalreason = dtRow.Item("disapprovalreason").ToString
                mstrRemarks = dtRow.Item("remarks").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'Sohail (12 Oct 2018) -- Start
    'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String _
    '                        , ByVal intApproverUserUnkId As Integer _
    '                        , Optional ByVal intStaffrequisitionApprovalTranUnkId As Integer = 0 _
    '                        , Optional ByVal strStaffrequisitionTranUnkIDs As String = "" _
    '                        , Optional ByVal intAllocationID As Integer = 0 _
    '                        , Optional ByVal intAllocationUnkID As Integer = 0 _
    '                        , Optional ByVal intLevelUnkID As Integer = 0 _
    '                        , Optional ByVal intPriority As Integer = 0 _
    '                        , Optional ByVal blnAddPendingRecord As Boolean = False _
    '                        , Optional ByVal strFilter As String = "" _
    '                        ) As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim strStaffFilter As String = ""

    '    objDataOperation = New clsDataOperation

    '    Try

    '        If strStaffrequisitionTranUnkIDs.Trim <> "" Then
    '            strStaffFilter &= " AND rcstaffrequisition_tran.Staffrequisitiontranunkid IN ( " & strStaffrequisitionTranUnkIDs & " ) "
    '        End If

    '        If intAllocationID > 0 Then
    '            strStaffFilter &= " AND rcstaffrequisition_tran.staffrequisitionbyid = @staffrequisitionbyid "
    '            objDataOperation.AddParameter("@staffrequisitionbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
    '        End If

    '        If intAllocationUnkID > 0 Then
    '            strStaffFilter &= " AND rcstaffrequisition_tran.allocationunkid = @allocationunkid "
    '            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
    '        End If

    '        ''''''
    '            Dim objApproverMap As New clsStaffRequisition_approver_mapping
    '            Dim objApproverLevel As New clsStaffRequisitionApproverlevel_master
    '            Dim ds As DataSet
    '            Dim strLowerLevelUserIDs As String = ""
    '        Dim intCurrPriority As Integer = 0
    '        'Sohail (21 Aug 2015) -- Start
    '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '        'If intApproverUserUnkId <= 0 Then intApproverUserUnkId = User._Object._Userunkid
    '        'Sohail (21 Aug 2015) -- End
    '            ds = objApproverMap.GetList("ApproverLevel", intApproverUserUnkId, intAllocationID, intAllocationUnkID)
    '            If ds.Tables("ApproverLevel").Rows.Count <= 0 Then Exit Try

    '            intCurrPriority = CInt(ds.Tables("ApproverLevel").Rows(0).Item("priority"))
    '            Dim intLowerPriority As Integer = objApproverLevel.GetLowerLevelPriority(intCurrPriority, intAllocationID, intAllocationUnkID)
    '            If intLowerPriority >= 0 Then
    '                strLowerLevelUserIDs = objApproverMap.GetApproverUnkIDs(intAllocationID, intAllocationUnkID, intLowerPriority)
    '            Else
    '                'strLowerLevelUserIDs = "-1"
    '            End If
    '        '''''''

    '        If blnAddPendingRecord = True Then

    '            'Dim objApproverMap As New clsStaffRequisition_approver_mapping
    '            'Dim objApproverLevel As New clsStaffRequisitionApproverlevel_master
    '            'Dim ds As DataSet
    '            'Dim strLowerLevelUserIDs As String = ""
    '            'If intApproverUserUnkId <= 0 Then intApproverUserUnkId = User._Object._Userunkid
    '            'Dim intCurrPriority As Integer = 0
    '            'ds = objApproverMap.GetList("ApproverLevel", intApproverUserUnkId, intAllocationID, intAllocationUnkID)
    '            'If ds.Tables("ApproverLevel").Rows.Count <= 0 Then Exit Try

    '            'intCurrPriority = CInt(ds.Tables("ApproverLevel").Rows(0).Item("priority"))
    '            'Dim intLowerPriority As Integer = objApproverLevel.GetLowerLevelPriority(intCurrPriority, intAllocationID, intAllocationUnkID)
    '            'If intLowerPriority >= 0 Then
    '            '    strLowerLevelUserIDs = objApproverMap.GetApproverUnkIDs(intAllocationID, intAllocationUnkID, intLowerPriority)
    '            'Else
    '            '    'strLowerLevelUserIDs = "-1"
    '            'End If


    '            strQ = "SELECT  B.staffrequisitionapprovaltranunkid " & _
    '                          ", B.staffrequisitiontranunkid " & _
    '                          ", formno " & _
    '                          ", staffrequisitiontypeid " & _
    '                          ", staffrequisitionbyid " & _
    '                          ", allocationunkid " & _
    '                          ", Branch " & _
    '                          ", DepartmentGroup " & _
    '                          ", Department " & _
    '                          ", SectionGroup " & _
    '                          ", Section " & _
    '                          ", UnitGroup " & _
    '                          ", Unit " & _
    '                          ", Team " & _
    '                          ", JobGroup " & _
    '                          ", Job " & _
    '                          ", ClassGroup " & _
    '                          ", Class " & _
    '                          ", classgroupunkid " & _
    '                          ", Staff_ClassGroup " & _
    '                          ", classunkid " & _
    '                          ", Staff_Class " & _
    '                          ", employeeunkid " & _
    '                          ", employeecode " & _
    '                          ", EmpName " & _
    '                          ", actionreasonunkid " & _
    '                          ", actionreason " & _
    '                          ", additionalstaffreason " & _
    '                          ", jobunkid " & _
    '                          ", JobTitle " & _
    '                          ", jobdescrription " & _
    '                          ", workstartdate " & _
    '                          ", form_statusunkid " & _
    '                          ", levelunkid " & _
    '                          ", levelcode " & _
    '                          ", levelname " & _
    '                          ", priority " & _
    '                          ", '19000101' AS approval_date " & _
    '                          ", 1 AS statusunkid " & _
    '                          ", '' AS disapprovalreason " & _
    '                          ", '' AS remarks " & _
    '                          ", userunkid " & _
    '                          ", username " & _
    '                          ", 0 AS isvoid " & _
    '                          ", NULL AS voiddatetime " & _
    '                          ", -1 AS voiduserunkid " & _
    '                          ", '' AS voidreason " & _
    '                    "FROM    ( SELECT  DISTINCT " & _
    '                                        "0 AS staffrequisitionapprovaltranunkid " & _
    '                                          ", rcstaffrequisition_tran.staffrequisitiontranunkid " & _
    '                                          ", rcstaffrequisition_tran.formno " & _
    '                                          ", rcstaffrequisition_tran.staffrequisitiontypeid " & _
    '                                          ", rcstaffrequisition_tran.staffrequisitionbyid " & _
    '                                          ", rcstaffrequisition_tran.allocationunkid " & _
    '                                          ", ISNULL(hrstation_master.name, '') AS Branch " & _
    '                                          ", ISNULL(hrdepartment_group_master.name, '') AS DepartmentGroup " & _
    '                                          ", ISNULL(hrdepartment_master.name, '') AS Department " & _
    '                                          ", ISNULL(hrsectiongroup_master.name, '') AS SectionGroup " & _
    '                                          ", ISNULL(hrsection_master.name, '') AS Section " & _
    '                                          ", ISNULL(hrunitgroup_master.name, '') AS UnitGroup " & _
    '                                          ", ISNULL(hrunit_master.name, '') AS Unit " & _
    '                                          ", ISNULL(hrteam_master.name, '') AS Team " & _
    '                                          ", ISNULL(hrjobgroup_master.name, '') AS JobGroup " & _
    '                                          ", ISNULL(hrjob_master.job_name, '') AS Job " & _
    '                                          ", ISNULL(hrclassgroup_master.name, '') AS ClassGroup " & _
    '                                          ", ISNULL(hrclasses_master.name, '') AS Class " & _
    '                                          ", rcstaffrequisition_tran.classgroupunkid " & _
    '                                          ", ISNULL(cg.name, '') AS Staff_ClassGroup " & _
    '                                          ", rcstaffrequisition_tran.classunkid " & _
    '                                          ", ISNULL(c.name, '') AS Staff_Class " & _
    '                                          ", rcstaffrequisition_tran.employeeunkid " & _
    '                                          ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
    '                                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                                          ", rcstaffrequisition_tran.actionreasonunkid " & _
    '                                          ", ISNULL(STR.name, '') AS actionreason " & _
    '                                          ", rcstaffrequisition_tran.additionalstaffreason " & _
    '                                          ", rcstaffrequisition_tran.jobunkid " & _
    '                                          ", ISNULL(j.job_name, '') AS JobTitle " & _
    '                                          ", rcstaffrequisition_tran.jobdescrription " & _
    '                                          ", CONVERT(CHAR(8), rcstaffrequisition_tran.workstartdate, 112) AS workstartdate " & _
    '                                          ", rcstaffrequisition_tran.form_statusunkid " & _
    '                                          ", A.levelunkid " & _
    '                                          ", A.levelcode " & _
    '                                          ", A.levelname " & _
    '                                          ", A.priority " & _
    '                                          ", '19000101' AS approval_date " & _
    '                                          ", 1 AS statusunkid " & _
    '                                          ", '' AS disapprovalreason " & _
    '                                          ", '' AS remarks " & _
    '                                          ", A.userunkid " & _
    '                                          ", A.username " & _
    '                                          ", 0 AS isvoid " & _
    '                                          ", NULL AS voiddatetime " & _
    '                                          ", -1 AS voiduserunkid " & _
    '                                          ", '' AS voidreason " & _
    '                                    "FROM    rcstaffrequisition_tran " & _
    '                                            "LEFT JOIN rcstaffrequisition_approval_tran ON rcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid "

    '            If intCurrPriority > 0 Then
    '                strQ &= " AND rcstaffrequisition_approval_tran.priority = @currpriority "
    '                objDataOperation.AddParameter("@currpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrPriority)
    '            End If

    '            strQ &= "LEFT JOIN rcstaffrequisition_approver_mapping ON rcstaffrequisition_approver_mapping.allocationid = rcstaffrequisition_tran.staffrequisitionbyid " & _
    '                                                                                 "AND rcstaffrequisition_approver_mapping.allocationunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
    '                            "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
    '                            "LEFT JOIN hremployee_master ON rcstaffrequisition_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                            "LEFT JOIN hrclassgroup_master AS cg ON cg.classgroupunkid = rcstaffrequisition_tran.classgroupunkid " & _
    '                            "LEFT JOIN hrclasses_master AS c ON c.classesunkid = rcstaffrequisition_tran.classunkid " & _
    '                            "LEFT JOIN cfcommon_master AS STR ON STR.masterunkid = rcstaffrequisition_tran.actionreasonunkid AND STR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
    '                            "LEFT JOIN hrjob_master AS j ON j.jobunkid = rcstaffrequisition_tran.jobunkid " & _
    '                            "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                            "LEFT JOIN hrunitgroup_master ON rcstaffrequisition_tran.allocationunkid = hrunitgroup_master.unitgroupunkid " & _
    '                            "LEFT JOIN hrsectiongroup_master ON rcstaffrequisition_tran.allocationunkid = hrsectiongroup_master.sectiongroupunkid " & _
    '                            "LEFT JOIN ( SELECT DISTINCT " & _
    '                                                "rcstaffrequisitionlevel_master.levelunkid " & _
    '                                              ", rcstaffrequisitionlevel_master.levelcode " & _
    '                                              ", rcstaffrequisitionlevel_master.levelname " & _
    '                                              ", rcstaffrequisitionlevel_master.priority " & _
    '                                              ", rcstaffrequisition_approver_mapping.userapproverunkid AS userunkid " & _
    '                                              ", cfuser_master.username " & _
    '                                        "FROM    rcstaffrequisition_approver_mapping " & _
    '                                                "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
    '                                                "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
    '                                        "WHERE   ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(rcstaffrequisitionlevel_master.isactive, 1) = 1 " & _
    '                                                "AND rcstaffrequisition_approver_mapping.allocationid = @staffrequisitionbyid " & _
    '                                                "AND rcstaffrequisition_approver_mapping.allocationunkid = @allocationunkid " & _
    '                                                "AND rcstaffrequisition_approver_mapping.userapproverunkid = @userapproverunkid " & _
    '                                      ") AS A ON 1 = 1 " & _
    '                    "WHERE   ISNULL(rcstaffrequisition_tran.isvoid, 0) = 0 " & _
    '                            "AND ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 " & _
    '                            "AND ISNULL(rcstaffrequisitionlevel_master.isactive, 1) = 1 " & _
    '                            "AND ISNULL(rcstaffrequisition_approval_tran.statusunkid, 0) <> " & enApprovalStatus.PENDING & " " & _
    '                            "AND rcstaffrequisitionlevel_master.priority = A.priority " & _
    '                            "AND rcstaffrequisition_approval_tran.staffrequisitionapprovaltranunkid IS NULL "

    '            'S.SANDEEP [09 APR 2015] -- START
    '            ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = rcstaffrequisition_tran.actionreasonunkid ------ REMOVED
    '            ' LEFT JOIN cfcommon_master AS STR ON STR.masterunkid = rcstaffrequisition_tran.actionreasonunkid AND STR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
    '            'S.SANDEEP [09 APR 2015] -- END


    '            strQ &= strStaffFilter

    '            If strLowerLevelUserIDs.Trim <> "" Then
    '                'strQ &= " AND rcstaffrequisition_approver_mapping.userapproverunkid IN (" & strLowerLevelUserIDs & ") "
    '                '" AND rcstaffrequisition_approval_tran.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid "
    '            Else
    '                'strQ &= " AND rcstaffrequisition_approver_mapping.userapproverunkid = @userapproverunkid "

    '            End If

    '            strQ &= ") B "

    '            If intLowerPriority > 0 Then
    '                'strQ &= " AND rcstaffrequisitionlevel_master.priority = @lowerpriority "
    '                '
    '                strQ &= "JOIN ( SELECT   rcstaffrequisition_tran.staffrequisitiontranunkid " & _
    '                               "FROM     rcstaffrequisition_tran " & _
    '                                        "LEFT JOIN rcstaffrequisition_approval_tran ON rcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
    '                               "WHERE    ISNULL(dbo.rcstaffrequisition_tran.isvoid, 0) = 0 " & _
    '                                        "AND ISNULL(dbo.rcstaffrequisition_approval_tran.isvoid, 0) = 0 " & _
    '                                        "AND rcstaffrequisition_approval_tran.priority = @lowerpriority " & _
    '                                        "AND statusunkid <> 1 "

    '                strQ &= strStaffFilter

    '                strQ &= ") AS C ON C.staffrequisitiontranunkid = B.staffrequisitiontranunkid "

    '                'objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerPriority) 'Sohail (18 Mar 2015)

    '            End If





    '            strQ &= "UNION ALL "
    '        End If

    '        strQ &= "SELECT  rcstaffrequisition_approval_tran.staffrequisitionapprovaltranunkid " & _
    '                           ", rcstaffrequisition_approval_tran.staffrequisitiontranunkid " & _
    '                           ", rcstaffrequisition_tran.formno " & _
    '                           ", rcstaffrequisition_tran.staffrequisitiontypeid " & _
    '                           ", rcstaffrequisition_tran.staffrequisitionbyid " & _
    '                           ", rcstaffrequisition_tran.allocationunkid " & _
    '                           ", ISNULL(hrstation_master.name, '') AS Branch " & _
    '                           ", ISNULL(hrdepartment_group_master.name, '') AS DepartmentGroup " & _
    '                           ", ISNULL(hrdepartment_master.name, '') AS Department " & _
    '                           ", ISNULL(hrsectiongroup_master.name, '') AS SectionGroup " & _
    '                           ", ISNULL(hrsection_master.name, '') AS Section " & _
    '                           ", ISNULL(hrunitgroup_master.name, '') AS UnitGroup " & _
    '                           ", ISNULL(hrunit_master.name, '') AS Unit " & _
    '                           ", ISNULL(hrteam_master.name, '') AS Team " & _
    '                           ", ISNULL(hrjobgroup_master.name, '') AS JobGroup " & _
    '                           ", ISNULL(hrjob_master.job_name, '') AS Job " & _
    '                           ", ISNULL(hrclassgroup_master.name, '') AS ClassGroup " & _
    '                           ", ISNULL(hrclasses_master.name, '') AS Class " & _
    '                           ", rcstaffrequisition_tran.classgroupunkid " & _
    '                           ", ISNULL(cg.name, '') AS Staff_ClassGroup " & _
    '                           ", rcstaffrequisition_tran.classunkid " & _
    '                           ", ISNULL(c.name, '') AS Staff_Class " & _
    '                           ", rcstaffrequisition_tran.employeeunkid " & _
    '                           ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
    '                           ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                             "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                             "+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                           ", rcstaffrequisition_tran.actionreasonunkid " & _
    '                           ", ISNULL(STR.name, '') AS actionreason " & _
    '                           ", rcstaffrequisition_tran.additionalstaffreason " & _
    '                           ", rcstaffrequisition_tran.jobunkid " & _
    '                           ", ISNULL(j.job_name, '') AS JobTitle " & _
    '                           ", rcstaffrequisition_tran.jobdescrription " & _
    '                           ", CONVERT(CHAR(8), rcstaffrequisition_tran.workstartdate, 112) AS workstartdate " & _
    '                           ", rcstaffrequisition_tran.form_statusunkid " & _
    '                           ", rcstaffrequisition_approval_tran.levelunkid " & _
    '                           ", rcstaffrequisitionlevel_master.levelcode " & _
    '                           ", rcstaffrequisitionlevel_master.levelname " & _
    '                           ", rcstaffrequisition_approval_tran.priority " & _
    '                           ", CONVERT(CHAR(8), rcstaffrequisition_approval_tran.approval_date, 112) AS approval_date " & _
    '                           ", rcstaffrequisition_approval_tran.statusunkid " & _
    '                           ", rcstaffrequisition_approval_tran.disapprovalreason " & _
    '                           ", rcstaffrequisition_approval_tran.remarks " & _
    '                           ", rcstaffrequisition_approval_tran.userunkid " & _
    '                           ", cfuser_master.username " & _
    '                           ", rcstaffrequisition_approval_tran.isvoid " & _
    '                           ", rcstaffrequisition_approval_tran.voiddatetime " & _
    '                           ", rcstaffrequisition_approval_tran.voiduserunkid " & _
    '                           ", rcstaffrequisition_approval_tran.voidreason " & _
    '                     "FROM    rcstaffrequisition_approval_tran " & _
    '                             "LEFT JOIN rcstaffrequisition_tran ON rcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
    '                             "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisitionlevel_master.levelunkid = rcstaffrequisition_approval_tran.levelunkid " & _
    '                             "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = rcstaffrequisition_approval_tran.userunkid " & _
    '                             "LEFT JOIN hremployee_master ON rcstaffrequisition_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                             "LEFT JOIN hrclassgroup_master AS cg ON cg.classgroupunkid = rcstaffrequisition_tran.classgroupunkid " & _
    '                             "LEFT JOIN hrclasses_master AS c ON c.classesunkid = rcstaffrequisition_tran.classunkid " & _
    '                             "LEFT JOIN cfcommon_master AS STR ON STR.masterunkid = rcstaffrequisition_tran.actionreasonunkid AND STR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
    '                             "LEFT JOIN hrjob_master AS j ON j.jobunkid = rcstaffrequisition_tran.jobunkid " & _
    '                             "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = rcstaffrequisition_tran.allocationunkid " & _
    '                             "LEFT JOIN hrunitgroup_master ON rcstaffrequisition_tran.allocationunkid = hrunitgroup_master.unitgroupunkid " & _
    '                             "LEFT JOIN hrsectiongroup_master ON rcstaffrequisition_tran.allocationunkid = hrsectiongroup_master.sectiongroupunkid "

    '        'S.SANDEEP [09 APR 2015] -- START
    '        ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = rcstaffrequisition_tran.actionreasonunkid ------ REMOVED
    '        ' LEFT JOIN cfcommon_master AS STR ON STR.masterunkid = rcstaffrequisition_tran.actionreasonunkid AND STR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
    '        'S.SANDEEP [09 APR 2015] -- END


    '        If intLowerPriority > 0 Then

    '            strQ &= "JOIN ( SELECT   rcstaffrequisition_tran.staffrequisitiontranunkid " & _
    '                           "FROM     rcstaffrequisition_tran " & _
    '                                    "LEFT JOIN rcstaffrequisition_approval_tran ON rcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
    '                           "WHERE    ISNULL(dbo.rcstaffrequisition_tran.isvoid, 0) = 0 " & _
    '                                    "AND ISNULL(rcstaffrequisition_approval_tran.isvoid, 0) = 0 " & _
    '                                    "AND rcstaffrequisition_approval_tran.priority = @lowerpriority " & _
    '                                    "AND statusunkid <> 1 "

    '            strQ &= strStaffFilter

    '            strQ &= ") AS D ON D.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid "

    '            'objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerPriority)
    '        End If

    '        strQ &= "WHERE   ISNULL(rcstaffrequisition_approval_tran.isvoid, 0) = 0 " & _
    '                             "AND ISNULL(rcstaffrequisition_tran.isvoid, 0) = 0 " & _
    '                             "AND ISNULL(rcstaffrequisitionlevel_master.isactive, 1) = 1 "

    '        strQ &= strStaffFilter

    '        If intStaffrequisitionApprovalTranUnkId > 0 Then
    '            strQ &= " AND rcstaffrequisition_approval_tran.staffrequisitionapprovaltranunkid = @staffrequisitionapprovaltranunkid "
    '            objDataOperation.AddParameter("@staffrequisitionapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffrequisitionApprovalTranUnkId)
    '        End If

    '        If intLevelUnkID > 0 Then
    '            strQ &= " AND rcstaffrequisition_approval_tran.levelunkid = @levelunkid "
    '            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
    '        End If

    '        If intPriority > 0 Then
    '            strQ &= " AND rcstaffrequisition_approval_tran.priority = @priority "
    '        End If

    '        If intApproverUserUnkId > 0 Then
    '            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
    '        End If

    '        If intPriority > 0 Then
    '            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
    '        End If

    '        'Sohail (18 Mar 2015) -- Start
    '        'Enhancement - Allow more than one employee to be replaced in staff requisition.
    '        If intLowerPriority > 0 Then
    '            objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerPriority)
    '        End If
    '        'Sohail (18 Mar 2015) -- End

    '        If strFilter.Trim <> "" Then
    '            strQ &= " AND " & strFilter
    '        End If

    '        strQ &= " ORDER BY formno, priority DESC "

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetList(ByVal strTableName As String _
                            , ByVal intApproverUserUnkId As Integer _
                            , ByVal mstrEmpAsOnDate As String _
                            , Optional ByVal intStaffrequisitionApprovalTranUnkId As Integer = 0 _
                            , Optional ByVal strStaffrequisitionTranUnkIDs As String = "" _
                            , Optional ByVal intAllocationID As Integer = 0 _
                            , Optional ByVal intAllocationUnkID As Integer = 0 _
                            , Optional ByVal intLevelUnkID As Integer = 0 _
                            , Optional ByVal intPriority As Integer = 0 _
                            , Optional ByVal blnAddPendingRecord As Boolean = False _
                            , Optional ByVal strFilter As String = "" _
                            , Optional ByVal blnOnlyFirstLevelUser As Boolean = False _
                            , Optional ByVal intFinalStatusID As Integer = 0 _
                            , Optional ByVal blnMyApproval As Boolean = False _
                            ) As DataSet


        'Pinkal (16-Nov-2021)-- NMB Staff Requisition Approval Enhancements. [ByVal mdtEmpAsOnDate As Date]

        'Hemant (22 Aug 2019) [blnMyApproval]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strStaffFilter As String = ""

        objDataOperation = New clsDataOperation

        Try

            If strStaffrequisitionTranUnkIDs.Trim <> "" Then
                strStaffFilter &= " AND rcstaffrequisition_tran.Staffrequisitiontranunkid IN ( " & strStaffrequisitionTranUnkIDs & " ) "
            End If

            If intStaffrequisitionApprovalTranUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_approval_tran.staffrequisitionapprovaltranunkid = @staffrequisitionapprovaltranunkid "
                objDataOperation.AddParameter("@staffrequisitionapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffrequisitionApprovalTranUnkId)
            End If

            If intAllocationID > 0 Then
                strStaffFilter &= " AND rcstaffrequisition_tran.staffrequisitionbyid = @staffrequisitionbyid "
                objDataOperation.AddParameter("@staffrequisitionbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            End If

            If intAllocationUnkID > 0 Then
                strStaffFilter &= " AND rcstaffrequisition_tran.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
            End If


            'Pinkal (02-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 

            'strQ = "SELECT rcstaffrequisition_approver_mapping.* " & _
            '             ", rcstaffrequisitionlevel_master.priority " & _
            '        "INTO #usr " & _
            '        "FROM rcstaffrequisition_approver_mapping " & _
            '            "LEFT JOIN rcstaffrequisitionlevel_master " & _
            '                "ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
            '        "WHERE isvoid = 0 " & _
            '              "AND isactive = 1 "

            strQ = "SELECT rcstaffrequisition_approver_mapping.* " & _
                         ", rcstaffrequisitionlevel_master.priority " & _
                      ", rcstaffrequisition_approver_jobmapping.jobunkid " & _
                      " INTO #usr " & _
                      " FROM rcstaffrequisition_approver_mapping " & _
                      " JOIN rcstaffrequisition_approver_jobmapping ON rcstaffrequisition_approver_mapping.allocationid = rcstaffrequisition_approver_jobmapping.allocationid " & _
                      " AND rcstaffrequisition_approver_jobmapping.userapproverunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
                      " AND rcstaffrequisition_approver_jobmapping.levelunkid = rcstaffrequisition_approver_mapping.levelunkid " & _
                      " AND rcstaffrequisition_approver_jobmapping.isvoid= 0 " & _
                      " LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                      " WHERE rcstaffrequisition_approver_mapping.isvoid = 0 AND isactive = 1 "

            'Pinkal (02-Nov-2021) -- End

            If intApproverUserUnkId > 0 Then
                strQ &= "AND rcstaffrequisition_approver_mapping.userapproverunkid = @userapproverunkid "
            End If

            strQ &= "SELECT * " & _
                        ", DENSE_RANK() OVER (PARTITION BY AA.staffrequisitiontranunkid " & _
                                                      "ORDER BY AA.priority " & _
                                                     ") AS ROWNO " & _
                        "INTO #tmp " & _
                        "FROM " & _
                        "( "

            If blnAddPendingRecord = True Then

                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 

                'strQ &= " SELECT rcstaffrequisition_tran.* " & _
                '            ", rcstaffrequisition_approver_mapping.userapproverunkid " & _
                '            ", rcstaffrequisition_approver_mapping.levelunkid " & _
                '            ", rcstaffrequisitionlevel_master.priority " & _
                '            ", 0 AS staffrequisitionapprovaltranunkid " & _
                '            ", '19000101' AS approval_date " & _
                '            ", 1 AS statusunkid " & _
                '            ", '' AS disapprovalreason " & _
                '            ", '' AS remarks " & _
                '            " FROM rcstaffrequisition_tran " & _
                '            " LEFT JOIN rcstaffrequisition_approver_mapping " & _
                '                    "ON rcstaffrequisition_tran.staffrequisitionbyid = rcstaffrequisition_approver_mapping.allocationid " & _
                '                       "AND rcstaffrequisition_tran.allocationunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                '                "LEFT JOIN rcstaffrequisition_approval_tran " & _
                '                    "ON rcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                '                       "AND rcstaffrequisition_approval_tran.levelunkid = rcstaffrequisition_approver_mapping.levelunkid " & _
                '                       "AND rcstaffrequisition_approval_tran.isvoid = 0 " & _
                '                "LEFT JOIN rcstaffrequisitionlevel_master " & _
                '                    "ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                '            "WHERE rcstaffrequisition_tran.isvoid = 0 " & _
                '                  "AND rcstaffrequisition_approver_mapping.isvoid = 0 " & _
                '                  "AND rcstaffrequisition_approval_tran.staffrequisitionapprovaltranunkid IS NULL "

                strQ &= " SELECT rcstaffrequisition_tran.* " & _
                                 ", rcstaffrequisition_approver_mapping.userapproverunkid " & _
                                 ", rcstaffrequisition_approver_mapping.levelunkid " & _
                                 ", rcstaffrequisitionlevel_master.priority " & _
                                 ", 0 AS staffrequisitionapprovaltranunkid " & _
                                 ", '19000101' AS approval_date " & _
                                 ", 1 AS statusunkid " & _
                                 ", '' AS disapprovalreason " & _
                                 ", '' AS remarks " & _
                             " FROM rcstaffrequisition_tran " & _
                             " LEFT JOIN rcstaffrequisition_approver_mapping ON rcstaffrequisition_tran.staffrequisitionbyid = rcstaffrequisition_approver_mapping.allocationid " & _
                             " AND rcstaffrequisition_tran.allocationunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                             " JOIN rcstaffrequisition_approver_jobmapping ON rcstaffrequisition_approver_mapping.allocationid = rcstaffrequisition_approver_jobmapping.allocationid " & _
                             " AND rcstaffrequisition_approver_jobmapping.userapproverunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
                             " AND rcstaffrequisition_approver_jobmapping.levelunkid = rcstaffrequisition_approver_mapping.levelunkid " & _
                             " AND rcstaffrequisition_tran.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _
                             " AND rcstaffrequisition_approver_jobmapping.isvoid= 0 " & _
                             " LEFT JOIN rcstaffrequisition_approval_tran ON rcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                             " AND rcstaffrequisition_approval_tran.levelunkid = rcstaffrequisition_approver_mapping.levelunkid AND rcstaffrequisition_approval_tran.isvoid = 0 " & _
                             " LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
                             " LEFT JOIN ( " & _
                             "                   SELECT  " & _
                             "                      jobunkid " & _
                             "                     ,employeeunkid " & _
                             "                     ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno  " & _
                             "                  FROM hremployee_categorization_tran " & _
                             "                  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsOnDate " & _
                             "                )   AS Jobs ON Jobs.employeeunkid = cfuser_master.employeeunkid  AND rcstaffrequisition_tran.jobunkid = Jobs.jobunkid " & _
                             " LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                             " WHERE rcstaffrequisition_tran.isvoid = 0 AND rcstaffrequisition_approver_mapping.isvoid = 0 " & _
                             " AND rcstaffrequisition_approval_tran.staffrequisitionapprovaltranunkid IS NULL AND jobs.jobunkid IS NULL "

                'Pinkal (02-Nov-2021) -- End

                strQ &= strStaffFilter

                If intFinalStatusID > 0 Then
                    strQ &= "AND rcstaffrequisition_tran.form_statusunkid = @form_statusunkid "
                End If

                'If strFilter.Trim <> "" Then
                '    strQ &= " AND " & strFilter
                'End If

                strQ &= "UNION ALL "

            End If


            strQ &= "SELECT rcstaffrequisition_tran.* " & _
                            ", rcstaffrequisition_approval_tran.userunkid AS userapproverunkid " & _
                            ", rcstaffrequisition_approval_tran.levelunkid " & _
                            ", rcstaffrequisition_approval_tran.priority " & _
                            ", rcstaffrequisition_approval_tran.staffrequisitionapprovaltranunkid " & _
                            ", CONVERT(CHAR(8), rcstaffrequisition_approval_tran.approval_date, 112) AS approval_date " & _
                            ", rcstaffrequisition_approval_tran.statusunkid " & _
                            ", rcstaffrequisition_approval_tran.disapprovalreason " & _
                            ", rcstaffrequisition_approval_tran.remarks " & _
                       "FROM rcstaffrequisition_approval_tran " & _
                           "LEFT JOIN rcstaffrequisition_tran ON rcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                       "WHERE rcstaffrequisition_approval_tran.isvoid = 0 " & _
                             "AND rcstaffrequisition_tran.isvoid = 0 "

            strQ &= strStaffFilter

            If intFinalStatusID > 0 Then
                strQ &= "AND rcstaffrequisition_tran.form_statusunkid = @form_statusunkid "
                objDataOperation.AddParameter("@form_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFinalStatusID)
            End If

            If blnOnlyFirstLevelUser = True AndAlso intFinalStatusID = CInt(enApprovalStatus.PENDING) Then
                strQ &= "AND 1 = 2 "
            End If

            'If strFilter.Trim <> "" Then
            '    strQ &= " AND " & strFilter
            'End If

            strQ &= ") AS AA " & _
                    "WHERE 1 = 1 "

            strQ &= "SELECT A.staffrequisitionapprovaltranunkid " & _
                         ", A.staffrequisitiontranunkid " & _
                         ", A.formno " & _
                         ", A.staffrequisitiontypeid " & _
                         ", A.staffrequisitionbyid " & _
                         ", A.allocationunkid " & _
                         ", ISNULL(hrstation_master.name, '') AS Branch " & _
                         ", ISNULL(hrdepartment_group_master.name, '') AS DepartmentGroup " & _
                         ", ISNULL(hrdepartment_master.name, '') AS Department " & _
                         ", ISNULL(hrsectiongroup_master.name, '') AS SectionGroup " & _
                         ", ISNULL(hrsection_master.name, '') AS Section " & _
                         ", ISNULL(hrunitgroup_master.name, '') AS UnitGroup " & _
                         ", ISNULL(hrunit_master.name, '') AS Unit " & _
                         ", ISNULL(hrteam_master.name, '') AS Team " & _
                         ", ISNULL(hrjobgroup_master.name, '') AS JobGroup " & _
                         ", ISNULL(hrjob_master.job_name, '') AS Job " & _
                         ", ISNULL(hrclassgroup_master.name, '') AS ClassGroup " & _
                         ", ISNULL(hrclasses_master.name, '') AS Class " & _
                         ", A.classgroupunkid " & _
                         ", ISNULL(cg.name, '') AS Staff_ClassGroup " & _
                         ", A.classunkid " & _
                         ", ISNULL(c.name, '') AS Staff_Class " & _
                         ", A.employeeunkid " & _
                         ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                         ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' " & _
                           "+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
                         ", A.actionreasonunkid " & _
                         ", ISNULL(STR.name, '') AS actionreason " & _
                         ", A.additionalstaffreason " & _
                         ", A.jobunkid " & _
                         ", ISNULL(j.job_name, '') AS JobTitle " & _
                         ", A.jobdescrription " & _
                         ", CONVERT(CHAR(8), A.workstartdate, 112) AS workstartdate " & _
                         ", A.form_statusunkid " & _
                         ", A.levelunkid " & _
                         ", rcstaffrequisitionlevel_master.levelcode " & _
                         ", rcstaffrequisitionlevel_master.levelname " & _
                         ", A.priority " & _
                         ", CONVERT(CHAR(8), A.approval_date, 112) AS approval_date " & _
                         ", A.statusunkid " & _
                         ", A.disapprovalreason " & _
                         ", A.remarks " & _
                         ", A.userapproverunkid AS userunkid " & _
                         ", cfuser_master.username " & _
                         ", A.isvoid " & _
                         ", A.voiddatetime " & _
                         ", A.voiduserunkid " & _
                         ", A.voidreason " & _
                         " FROM #tmp AS A " & _
                         " JOIN #usr AS B ON A.staffrequisitionbyid = B.allocationid AND A.allocationunkid = B.allocationunkid AND A.jobunkid = B.jobunkid "


            'Pinkal (02-Nov-2021)-- NMB Staff Requisition Approval Enhancements. [AND A.jobunkid = B.jobunkid]

            strQ &= " LEFT JOIN rcstaffrequisitionlevel_master ON A.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                        " LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = A.userapproverunkid " & _
                        " LEFT JOIN hremployee_master ON A.employeeunkid = hremployee_master.employeeunkid "


            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            strQ &= " LEFT JOIN (  " & _
                         "                  SELECT  " & _
                         "                      jobunkid " & _
                         "                     ,employeeunkid " & _
                         "                     ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno  " & _
                         "                  FROM hremployee_categorization_tran " & _
                         "                  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsOnDate " & _
                         "              )  " & _
                         "  AS Jobs ON Jobs.employeeunkid = cfuser_master.employeeunkid " & _
                         " AND A.jobunkid = Jobs.jobunkid "
            'Pinkal (16-Nov-2021) -- End


            strQ &= " LEFT JOIN hrclassgroup_master AS cg ON cg.classgroupunkid = A.classgroupunkid " & _
                        " LEFT JOIN hrclasses_master AS c ON c.classesunkid = A.classunkid " & _
                        " LEFT JOIN cfcommon_master AS STR ON STR.masterunkid = A.actionreasonunkid AND STR.mastertype = " & CInt(clsCommon_Master.enCommonMaster.TERMINATION) & " " & _
                        " LEFT JOIN hrjob_master AS j ON j.jobunkid = A.jobunkid " & _
                        " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.DEPARTMENT) & " " & _
                        " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.JOBS) & " " & _
                        " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.BRANCH) & " " & _
                        " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.DEPARTMENT_GROUP) & " " & _
                        " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.SECTION) & " " & _
                        " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.UNIT) & " " & _
                        " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.JOB_GROUP) & " " & _
                        " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.CLASS_GROUP) & " " & _
                        " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.CLASSES) & " " & _
                        " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = A.allocationunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.TEAM) & " " & _
                        " LEFT JOIN hrunitgroup_master ON A.allocationunkid = hrunitgroup_master.unitgroupunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.UNIT_GROUP) & " " & _
                        " LEFT JOIN hrsectiongroup_master ON A.allocationunkid = hrsectiongroup_master.sectiongroupunkid AND A.staffrequisitionbyid = " & CInt(enAllocation.SECTION_GROUP) & " "

            strQ &= "WHERE   1 = 1 "

            If blnOnlyFirstLevelUser = False Then
                strQ &= "AND A.priority <= B.priority "
            Else
                strQ &= "AND A.priority = B.priority " & _
                        "AND A.ROWNO = 1 "
            End If

            If intApproverUserUnkId > 0 Then
                'Hemant (22 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                If blnMyApproval = True Then
                    strQ &= " AND A.userapproverunkid = @userapproverunkid "
                End If
                'Hemant (22 Aug 2019) -- End
                objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
            End If

            'If strFilter.Trim <> "" Then
            '    strQ &= " AND " & strFilter
            'End If


            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            strQ &= " 	AND jobs.jobunkid IS NULL "
            objDataOperation.AddParameter("@EmpAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAsOnDate)
            'Pinkal (16-Nov-2021) -- End


            strQ &= " ORDER BY formno, priority "

            strQ &= " DROP TABLE #usr " & _
                    " DROP TABLE #tmp "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (12 Oct 2018) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>    
    Public Function GetListForApproval(ByVal strTableName As String _
                                       , ByVal intStaffrequisitionById As Integer _
                                       , ByVal intAllocationUnkId As Integer _
                                       , ByVal intUserUnkId As Integer _
                                       , ByVal blnIsApprove As Boolean _
                                       , ByVal intCurrLevelPriority As Integer _
                                       , ByVal intLowerLevelPriority As Integer _
                                       , ByVal intMaxPriority As Integer _
                                       , Optional ByVal intIsApproved As Integer = -1 _
                                       , Optional ByVal strFilterQuery As String = "" _
                                       , Optional ByVal strAccessLevelFilterString As String = "" _
                                       ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strMainQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If strAccessLevelFilterString.Trim = "" Then strAccessLevelFilterString = UserAccessLevel._AccessLevelFilterString

            strQ = "SELECT  ISNULL(rcStaffrequisition_approval_tran.Staffrequisitionapprovaltranunkid, -999) AS staffrequisitionapprovaltranunkid " & _
                          ", rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                          ", rcstaffrequisition_tran.staffrequisitiontypeid " & _
                          ", rcstaffrequisition_tran.staffrequisitionbyid " & _
                          ", rcstaffrequisition_tran.allocationunkid " & _
                          ", ISNULL(hrstation_master.name, '') AS Branch " & _
                          ", ISNULL(hrdepartment_group_master.name, '') AS DepartmentGroup " & _
                          ", ISNULL(hrdepartment_master.name, '') AS Department " & _
                          ", ISNULL(hrsectiongroup_master.name, '') AS SectionGroup " & _
                          ", ISNULL(hrsection_master.name, '') AS Section " & _
                          ", ISNULL(hrunitgroup_master.name, '') AS UnitGroup " & _
                          ", ISNULL(hrunit_master.name, '') AS Unit " & _
                          ", ISNULL(hrteam_master.name, '') AS Team " & _
                          ", ISNULL(hrjobgroup_master.name, '') AS JobGroup " & _
                          ", ISNULL(hrjob_master.job_name, '') AS Job " & _
                          ", ISNULL(hrclassgroup_master.name, '') AS ClassGroup " & _
                          ", ISNULL(hrclasses_master.name, '') AS Class " & _
                          ", rcstaffrequisition_tran.classgroupunkid " & _
                          ", ISNULL(cg.name, '') AS Staff_ClassGroup " & _
                          ", rcstaffrequisition_tran.classunkid " & _
                          ", ISNULL(c.name, '') AS Staff_Class " & _
                          ", rcstaffrequisition_tran.employeeunkid " & _
                          ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                          ", rcstaffrequisition_tran.actionreasonunkid " & _
                          ", ISNULL(STR.name, '') AS actionreason " & _
                          ", rcstaffrequisition_tran.additionalstaffreason " & _
                          ", rcstaffrequisition_tran.jobunkid " & _
                          ", ISNULL(j.job_name, '') AS JobTitle " & _
                          ", rcstaffrequisition_tran.jobdescrription " & _
                          ", CONVERT(CHAR(8), rcstaffrequisition_tran.workstartdate, 112) AS workstartdate " & _
                          ", rcstaffrequisition_tran.isapproved " & _
                          ", rcstaffrequisition_tran.voidreason " & _
                          ", rcstaffrequisition_approval_tran.levelunkid " & _
                          ", rcstaffrequisitionlevel_master.levelcode " & _
                          ", rcstaffrequisitionlevel_master.levelname " & _
                          ", rcstaffrequisition_approval_tran.priority " & _
                          ", rcstaffrequisition_approval_tran.approval_date " & _
                          ", rcstaffrequisition_approval_tran.statusunkid " & _
                          ", rcstaffrequisition_approval_tran.userunkid " & _
                          ", rcstaffrequisition_approval_tran.isvoid " & _
                          ", rcstaffrequisition_approval_tran.voiddatetime " & _
                          ", rcstaffrequisition_approval_tran.voiduserunkid " & _
                          ", rcstaffrequisition_approval_tran.voidreason " & _
                          ", rcstaffrequisition_approval_tran.disapprovalreason " & _
                          ", ISNULL(rcstaffrequisition_approval_tran.remarks, '') AS remarks " & _
                    "FROM    rcstaffrequisition_tran " & _
                            "JOIN rcstaffrequisition_approver_mapping ON rcstaffrequisition_approver_mapping.allocationid = rcstaffrequisition_tran.staffrequisitionbyid " & _
                                                                          "AND rcstaffrequisition_approver_mapping.allocationunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN rcstaffrequisition_approval_tran ON rcstaffrequisition_tran.staffrequisitiontranunkid = rcstaffrequisition_approval_tran.staffrequisitiontranunkid " & _
                                                                          "AND ISNULL(rcstaffrequisition_approval_tran.isvoid, 0) = 0 " & _
                            "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approval_tran.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                            "LEFT JOIN hremployee_master ON rcstaffrequisition_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "LEFT JOIN hrclassgroup_master AS cg ON cg.classgroupunkid = rcstaffrequisition_tran.classgroupunkid " & _
                            "LEFT JOIN hrclasses_master AS c ON c.classesunkid = rcstaffrequisition_tran.classunkid " & _
                            "LEFT JOIN cfcommon_master AS STR ON STR.masterunkid = rcstaffrequisition_tran.actionreasonunkid AND STR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                            "LEFT JOIN hrjob_master AS j ON j.jobunkid = rcstaffrequisition_tran.jobunkid " & _
                            "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = rcstaffrequisition_tran.allocationunkid " & _
                            "LEFT JOIN hrunitgroup_master ON rcstaffrequisition_tran.allocationunkid = hrunitgroup_master.unitgroupunkid " & _
                            "LEFT JOIN hrsectiongroup_master ON rcstaffrequisition_tran.allocationunkid = hrsectiongroup_master.sectiongroupunkid " & _
                    "WHERE   ISNULL(rcstaffrequisition_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 " & _
                            "AND ISNULL(rcstaffrequisitionlevel_master.isactive, 1) = 1 " & _
                            "AND rcstaffrequisition_tran.staffrequisitionbyid = @staffrequisitionbyid " & _
                            "AND rcstaffrequisition_tran.allocationunkid = @allocationunkid " & _
                            "AND rcstaffrequisition_approver_mapping.userapproverunkid = @userapproverunkid "

            'S.SANDEEP [09 APR 2015] -- START
            ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = rcstaffrequisition_tran.actionreasonunkid ------ REMOVED
            ' LEFT JOIN cfcommon_master AS STR ON STR.masterunkid = rcstaffrequisition_tran.actionreasonunkid AND STR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            'S.SANDEEP [09 APR 2015] -- END


            '*** Now Access is based on approver mapping
            'strQ &= strAccessLevelFilterString


            If blnIsApprove = False AndAlso intMaxPriority = intCurrLevelPriority Then
                strQ &= " AND (rcstaffrequisition_approval_tran.priority = @lowerpriority OR rcstaffrequisition_approval_tran.priority = @currpriority) AND rcstaffrequisition_approval_tran.statusunkid IN (0, 1) "
                objDataOperation.AddParameter("@currpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)
            Else
                strQ &= " AND rcstaffrequisition_approval_tran.priority = @lowerpriority AND rcstaffrequisition_approval_tran.statusunkid IN (0, 1) "
            End If
            objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerLevelPriority)


            If intIsApproved = 0 OrElse intIsApproved = 1 Then
                strQ &= " AND ISNULL(rcstaffrequisition_tran.isapproved, 0) = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intIsApproved)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            objDataOperation.AddParameter("@staffrequisitionbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffrequisitionById)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListForApproval; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetApproverStatus(ByVal strTableName As String _
                                      , ByVal intStaffRequisitionById As Integer _
                                      , Optional ByVal intPriorityUpto As Integer = -1 _
                                      , Optional ByVal strFilter As String = "" _
                                      ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  a.rowno " & _
                          ", a.atstaffrequisitionapprovaltranunkid " & _
                          ", a.staffrequisitiontranunkid " & _
                          ", a.levelunkid " & _
                          ", a.levelname " & _
                          ", a.priority " & _
                          ", a.statusunkid " & _
                          ", a.audituserunkid " & _
                          ", a.auditusername " & _
                          ", a.approval_date " & _
                          ", a.remarks " & _
                    "FROM    ( SELECT    RANK() OVER ( PARTITION BY atrcstaffrequisition_approval_tran.staffrequisitiontranunkid, " & _
                                                      "audituserunkid ORDER BY atstaffrequisitionapprovaltranunkid DESC ) AS rowno " & _
                                      ", atrcstaffrequisition_approval_tran.atstaffrequisitionapprovaltranunkid " & _
                                      ", atrcstaffrequisition_approval_tran.staffrequisitiontranunkid " & _
                                      ", atrcstaffrequisition_approval_tran.levelunkid " & _
                                      ", rcstaffrequisitionlevel_master.levelname " & _
                                      ", atrcstaffrequisition_approval_tran.priority " & _
                                      ", atrcstaffrequisition_approval_tran.statusunkid " & _
                                      ", atrcstaffrequisition_approval_tran.audituserunkid " & _
                                      ", ISNULL(cfuser_master.username, '') AS auditusername " & _
                                      ", atrcstaffrequisition_approval_tran.approval_date " & _
                                      ", atrcstaffrequisition_approval_tran.remarks " & _
                              "FROM      atrcstaffrequisition_approval_tran " & _
                                        "JOIN rcstaffrequisition_tran ON atrcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                                        "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = atrcstaffrequisition_approval_tran.audituserunkid " & _
                                        "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisitionlevel_master.levelunkid = atrcstaffrequisition_approval_tran.levelunkid " & _
                              "WHERE     ISNULL(rcstaffrequisition_tran.isvoid, 0) = 0 " & _
                                        "AND rcstaffrequisition_tran.staffrequisitionbyid = @staffrequisitionbyid " & _
                                        "AND atrcstaffrequisition_approval_tran.levelunkid <> -1 " & _
                                        "AND atrcstaffrequisition_approval_tran.priority <> -1 "

            If intPriorityUpto >= 0 Then
                strQ &= " AND atrcstaffrequisition_approval_tran.priority <= @priorityupto "
                objDataOperation.AddParameter("@priorityupto", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriorityUpto)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "       ) AS a " & _
                    "WHERE   a.rowno = 1 " & _
                    "ORDER BY a.staffrequisitiontranunkid " & _
                          ", a.atstaffrequisitionapprovaltranunkid DESC "

            objDataOperation.AddParameter("@staffrequisitionbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffRequisitionById)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetTotalApprovedCount(ByVal intPeriodId As Integer _
                                          , Optional ByVal intLevelUnkID As Integer = 0 _
                                          , Optional ByVal intPriority As Integer = -1 _
                                          , Optional ByVal strFilter As String = "" _
                                          ) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  COUNT(a.rowno) AS TotalCount " & _
                     "FROM    ( SELECT    RANK() OVER ( PARTITION BY atrcstaffrequisition_approval_tran.staffrequisitiontranunkid, audituserunkid ORDER BY atstaffrequisitionapprovaltranunkid DESC ) AS rowno " & _
                               "FROM      atrcstaffrequisition_approval_tran " & _
                                         "JOIN rcstaffrequisition_tran ON atrcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                               "WHERE     ISNULL(rcstaffrequisition_tran.isvoid, 0) = 0 " & _
                                         "AND rcstaffrequisition_tran.periodunkid = @periodunkid " & _
                                         "AND atrcstaffrequisition_approval_tran.levelunkid <> -1 " & _
                                         "AND atrcstaffrequisition_approval_tran.priority <> -1 " & _
                                         "AND atrcstaffrequisition_approval_tran.statusunkid = 1 "


            If intLevelUnkID > 0 Then
                strQ &= " AND atrcstaffrequisition_approval_tran.levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
            End If

            If intPriority > -1 Then
                strQ &= " AND atrcstaffrequisition_approval_tran.priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "       ) AS a " & _
                  "WHERE   a.rowno = 1 "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows(0).Item("TotalCount")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalApprovedCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetLastApprovedDetail(ByVal strTableName As String _
                                          , ByVal intPeriodId As Integer _
                                          , Optional ByVal intLevelUnkID As Integer = 0 _
                                          , Optional ByVal intPriority As Integer = -1 _
                                          , Optional ByVal intApprovedUserUnkId As Integer = 0 _
                                          , Optional ByVal strFilter As String = "" _
                                          ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT a.rowno " & _
                          ", MAX(a.approval_date) AS approval_date " & _
                          ", a.audituserunkid " & _
                          ", ISNULL(cfuser_master.username, '') AS approvername " & _
                          ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS approverfullname " & _
                          ", a.levelname " & _
                     "FROM    ( SELECT    RANK() OVER ( PARTITION BY atrcstaffrequisition_approval_tran.staffrequisitiontranunkid, audituserunkid ORDER BY atstaffrequisitionapprovaltranunkid DESC ) AS rowno " & _
                                       ", atrcstaffrequisition_approval_tran.approval_date " & _
                                       ", atrcstaffrequisition_approval_tran.audituserunkid " & _
                                       ", rcstaffrequisitionapproverlevel_master.levelname " & _
                               "FROM      atrcstaffrequisition_approval_tran " & _
                                         "JOIN rcstaffrequisition_tran ON atrcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                                         "LEFT JOIN rcstaffrequisitionapproverlevel_master ON rcstaffrequisitionapproverlevel_master.levelunkid = atrcstaffrequisition_approval_tran.levelunkid " & _
                               "WHERE     ISNULL(rcstaffrequisition_tran.isvoid, 0) = 0 " & _
                                         "AND rcstaffrequisition_tran.periodunkid = @periodunkid " & _
                                         "AND atrcstaffrequisition_approval_tran.levelunkid <> -1 " & _
                                         "AND atrcstaffrequisition_approval_tran.priority <> -1 " & _
                                         "AND atrcstaffrequisition_approval_tran.statusunkid = 1 "


            If intLevelUnkID > 0 Then
                strQ &= " AND atrcstaffrequisition_approval_tran.levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
            End If

            If intPriority > -1 Then
                strQ &= " AND atrcstaffrequisition_approval_tran.priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            If intApprovedUserUnkId > 0 Then
                strQ &= " AND atrcstaffrequisition_approval_tran.audituserunkid = @audituserunkid "
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovedUserUnkId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= ") AS a " & _
                   "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = a.audituserunkid " & _
           "WHERE   a.rowno = 1 " & _
           "GROUP BY a.rowno " & _
                  ", a.audituserunkid " & _
                  ", ISNULL(cfuser_master.username, '') " & _
                  ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') " & _
                  ", a.levelname "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalApprovedCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcstaffrequisition_approval_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApproval_Date.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@disapprovalreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisapprovalreason.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO rcstaffrequisition_approval_tran ( " & _
              "  staffrequisitiontranunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", approval_date " & _
              ", statusunkid " & _
              ", disapprovalreason " & _
              ", remarks " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @staffrequisitiontranunkid " & _
              ", @levelunkid " & _
              ", @priority " & _
              ", @approval_date " & _
              ", @statusunkid " & _
              ", @disapprovalreason " & _
              ", @remarks " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voiduserunkid " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStaffrequisitionapprovaltranunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStaffRequisitionApprovalTran(objDataOperation, 1) = False Then
            If InsertAuditTrailForStaffRequisitionApprovalTran(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcstaffrequisition_approval_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        'If isExist(mstrName, mintStaffrequisitionapprovaltranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@staffrequisitionapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitionapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApproval_Date.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@disapprovalreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisapprovalreason.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE rcstaffrequisition_approval_tran SET " & _
              "  staffrequisitiontranunkid = @staffrequisitiontranunkid" & _
              ", levelunkid = @levelunkid" & _
              ", priority = @priority" & _
              ", approval_date = @approval_date" & _
              ", statusunkid = @statusunkid" & _
              ", disapprovalreason = @disapprovalreason" & _
              ", remarks = @remarks" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidreason = @voidreason " & _
            "WHERE staffrequisitionapprovaltranunkid = @staffrequisitionapprovaltranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStaffRequisitionApprovalTran(objDataOperation, 2) = False Then
            If InsertAuditTrailForStaffRequisitionApprovalTran(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <remarks></remarks>
    Public Function UpdatestaffrequisitionApproval(ByVal intStaffrequisitionapprovaltranunkid As Integer _
                                                   , ByVal dtCurrentDateAndTime As Date _
                                          , Optional ByVal intIsApproved As Integer = 0 _
                                          ) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim objDataOperation As clsDataOperation

        Dim StrQ As String = ""
        Dim dsList As New DataSet

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            'For Each dtRow As DataRow In mdtTable.Rows



            If intStaffrequisitionapprovaltranunkid > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Update() = False Then
                If Update(dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Insert() = False Then
                If Insert(dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            'If mintStatusunkid = 2 AndAlso mblnIsvoid = True Then
            '    mintLevelunkid = -1
            '    mintPriority = -1
            '    mintStatusunkid = 0
            '    mblnIsvoid = False
            '    mstrDisapprovalreason = ""
            '    mstrRemarks = ""
            '    If Insert() = False Then
            '        objDataOperation.ReleaseTransaction(False)
            '        Return False
            '    End If
            '    If blnIsApproved = 2 Then
            '        Dim objStaffrequisition As New clsStaffrequisition_Tran
            '        objStaffrequisition._Staffrequisitiontranunkid = mintStaffrequisitiontranunkid
            '        objStaffrequisition._Form_Statusunkid = 0

            '        If objStaffrequisition.Update(objDataOperation) = False Then
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '        End If
            '    End If
            'Else
            If intIsApproved = enApprovalStatus.APPROVED OrElse intIsApproved = enApprovalStatus.REJECTED OrElse intIsApproved = enApprovalStatus.CANCELLED Then
                Dim objStaffrequisition As New clsStaffrequisition_Tran
                objStaffrequisition._Staffrequisitiontranunkid = mintStaffrequisitiontranunkid
                objStaffrequisition._Form_Statusunkid = intIsApproved
                objStaffrequisition._WebFormName = "frmApproveDisapproveStaffRequisition"
                If mstrWebhostName.Trim <> "" Then
                    objStaffrequisition._WebHostName = mstrWebhostName
                Else
                    objStaffrequisition._WebHostName = getHostName()
                End If
                If mstrWebIP.Trim <> "" Then
                    objStaffrequisition._WebIP = mstrWebIP
                Else
                    objStaffrequisition._WebIP = getIP()
                End If


                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objStaffrequisition.Update(objDataOperation) = False Then
                If objStaffrequisition.Update(dtCurrentDateAndTime, objDataOperation) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                If intIsApproved = enApprovalStatus.APPROVED Then 'Final Approved
                    Dim objVacancy As New clsVacancy
                    'Hemant (30 Oct 2019) -- Start
                    Dim objJobs As New clsJobs
                    Dim objGrade As New clsGrade
                    objJobs._Jobunkid = CInt(objStaffrequisition._Jobunkid)
                    'Hemant (30 Oct 2019) -- End

                    With objVacancy
                        ._Vacancy_MasterUnkid = 0
                        ._Stationunkid = 0
                        ._Deptgroupunkid = 0
                        ._Departmentunkid = 0
                        ._Sectionunkid = 0
                        ._Unitunkid = 0
                        ._Jobgroupunkid = 0
                        ._Jobunkid = objStaffrequisition._Jobunkid
                        ._Gradegroupunkid = 0
                        ._Gradeunkid = 0

                        ._Employeementtypeunkid = 0
                        ._Openingdate = Nothing
                        ._Closingdate = Nothing
                        ._Interview_Startdate = Nothing
                        ._Interview_Closedate = Nothing
                        ._Paytypeunkid = 0
                        ._Shifttypeunkid = 0
                        ._Pay_From = 0
                        ._Pay_To = 0
                        ._Noofposition = objStaffrequisition._Noofposition
                        ._Experience = 0
                        ._Responsibilities_Duties = ""
                        ._Remark = ""
                        ._Userunkid = mintUserunkid
                        ._Isvoid = False
                        ._Voiduserunkid = 0
                        ._Voiddatetime = Nothing
                        ._Voidreason = ""
                        ._Is_External_Vacancy = False
                        ._Iswebexport = False
                        ._Staffrequisitiontranunkid = mintStaffrequisitiontranunkid
                        'Sohail (27 Sep 2019) -- Start
                        'NMB Enhancement # : They want option to bolden the content of the advert as per the needs of the advertised position..
                        ._Isskillbold = False
                        ._Isskillitalic = False
                        ._Isqualibold = False
                        ._Isqualiitalic = False
                        ._Isexpbold = False
                        ._Isexpitalic = False
                        'Sohail (27 Sep 2019) -- End
                        'Sohail (18 Feb 2020) -- Start
                        'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously.
                        ._Isbothintext = False
                        'Sohail (18 Feb 2020) -- End
                        'Sohail (29 Sep 2021) -- Start
                        'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
                        If objStaffrequisition._Jobadvertid = enJobAdvert.EXTERNALLY Then
                            ._Is_External_Vacancy = True
                        ElseIf objStaffrequisition._Jobadvertid = enJobAdvert.BOTH_INTERNALLY_AND_EXTERNALLY Then
                            ._Isbothintext = True
                        End If
                        'Sohail (29 Sep 2021) -- End


                        'Hemant (30 Oct 2019) -- Start
                        'Select Case objStaffrequisition._Staffrequisitionbyid

                        '    Case enAllocation.BRANCH
                        '        ._Stationunkid = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.DEPARTMENT_GROUP
                        '        ._Deptgroupunkid = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.DEPARTMENT
                        '        ._Departmentunkid = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.SECTION_GROUP
                        '        '._SECTION_GROUP = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.SECTION
                        '        ._Sectionunkid = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.UNIT_GROUP
                        '        '._UNIT_GROUP = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.UNIT
                        '        ._Unitunkid = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.TEAM
                        '        '._TEAM = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.JOB_GROUP
                        '        ._Jobgroupunkid = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.JOBS
                        '        '._Jobunkid = objStaffrequisition._Allocationunkid
                        '        ._Jobunkid = objStaffrequisition._Jobunkid

                        '    Case enAllocation.CLASS_GROUP
                        '        '._CLASS_GROUP = objStaffrequisition._Allocationunkid

                        '    Case enAllocation.CLASSES
                        '        '._CLASSES = objStaffrequisition._Allocationunkid

                        'End Select

                        ._Stationunkid = objJobs._JobBranchUnkid

                        ._Deptgroupunkid = objJobs._DepartmentGrpUnkId

                        ._Departmentunkid = objJobs._JobDepartmentunkid

                        ._SectionGrpUnkId = objJobs._SectionGrpUnkId

                        ._Sectionunkid = objJobs._Jobsectionunkid

                        ._UnitGrpUnkId = objJobs._UnitGrpUnkId

                        ._Unitunkid = objJobs._Jobunitunkid

                        ._Teamunkid = objJobs._Teamunkid

                        ._Jobgroupunkid = objJobs._Jobgroupunkid

                        ._ClassGroupunkid = objJobs._JobClassGroupunkid

                        ._ClassUnkid = objJobs._ClassUnkid

                        ._Gradeunkid = objJobs._Jobgradeunkid

                        objGrade._Gradeunkid = objJobs._Jobgradeunkid
                        ._Gradegroupunkid = objGrade._Gradegroupunkid

                        'Hemant (30 Oct 2019) -- End

                        If .Insert(dtCurrentDateAndTime, , , objDataOperation) = False Then
                            'Hemant (13 Sep 2019) -- [dtCurrentDateAndTime]
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If

                    End With

                ElseIf intIsApproved = enApprovalStatus.CANCELLED Then
                    Dim objVacancy As New clsVacancy
                    Dim ds As DataSet = objVacancy.GetList("List")
                    Dim dtTable As DataTable = New DataView(ds.Tables("List"), "vacancytitleunkid = 0 AND staffrequisitiontranunkid = " & mintStaffrequisitiontranunkid & " ", "", DataViewRowState.CurrentRows).ToTable
                    If dtTable.Rows.Count > 0 Then
                        objVacancy._Isvoid = True
                        objVacancy._Voiduserunkid = mintUserunkid
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objVacancy._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objVacancy._Voiddatetime = dtCurrentDateAndTime
                        'Sohail (21 Aug 2015) -- End
                        objVacancy._Voidreason = Language.getMessage(mstrModuleName, 1, "Staff Requisition Cancelled")
                        If objVacancy.Delete(CInt(dtTable.Rows(0).Item("vacancyunkid")), objDataOperation) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If
                End If
            End If
            'End If
            'Next

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateStaffrequisitionApproval; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcstaffrequisition_approval_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM rcstaffrequisition_approval_tran " & _
            "WHERE staffrequisitionapprovaltranunkid = @staffrequisitionapprovaltranunkid "

            objDataOperation.AddParameter("@staffrequisitionapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@staffrequisitionapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  staffrequisitionapprovaltranunkid " & _
              ", staffrequisitiontranunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", approval_date " & _
              ", statusunkid " & _
              ", disapprovalreason " & _
              ", remarks " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM rcstaffrequisition_approval_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND staffrequisitionapprovaltranunkid <> @staffrequisitionapprovaltranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@staffrequisitionapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Sub SendMailToApprover(ByVal intStatusID As Integer _
                                , ByVal intApproverUserUnkid As Integer _
                                , ByVal intStaffReqById As Integer _
                                , ByVal intAllocationUnkId As Integer _
                                , ByVal strJobTitle As String _
                                , ByVal intStaffReqApprovalTranUnkId As Integer _
                                , ByVal intStaffReqTranUnkId As Integer _
                                , ByVal strRemarks As String _
                                , ByVal intCompanyUnkId As Integer _
                                , ByVal blnIsArutiDemo As Boolean _
                                , ByVal strArutiSelfServiceURL As String _
                                , ByVal intYearUnkid As Integer _
                                , ByVal strUserAccessModeSetting As String _
                                , ByVal strDatabaseName As String _
                                , ByVal mstrEmployeeAsonDate As String _
                                , Optional ByVal iLoginTypeId As Integer = -1 _
                                , Optional ByVal iLoginEmployeeId As Integer = -1 _
                                , Optional ByVal strNotifyStaffReqFinalApprovedUserIDs As String = "" _
                                )
        'Pinkal (16-Nov-2021)-- NMB Staff Requisition Approval Enhancements. [ByVal mstrEmployeeAsonDate As String]

        'Sohail (11 Aug 2021) - [strDatabaseName]
        'Sohail (12 Oct 2018) - [strNotifyStaffReqFinalApprovedUserIDs]
        'Sohail (13 Jun 2016) - [intYearUnkid, strUserAccessModeSetting]
        'Sohail (21 Aug 2015) - [blnIsArutiDemo, blnIsArutiDemo]

        Dim objApproverLevel As New clsStaffRequisitionApproverlevel_master
        Dim objApproverMap As New clsStaffRequisition_approver_mapping
        Dim objUser As New clsUserAddEdit
        Dim objMail As New clsSendMail
        Dim strMessage As New StringBuilder
        Dim dsList As DataSet
        Dim intCurrLevelPriority As Integer = -1
        Dim intNextLevelPriority As Integer = -1
        Dim intMaxLevelPriority As Integer = -1
        Dim strApproverName As String
        Dim strApproverUnkIDs As String = ""
        Dim strArrayIDs() As String
        Dim blnFinalApproved As Boolean = False
        Dim strFinalApproverArrayIDs() As String 'Sohail (12 Oct 2018)
        'Hemant (06 Aug 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
        Dim objStaffrequisitionTran As New clsStaffrequisition_Tran
        Dim strRequisitionRaisedByName As String = ""
        Dim strRequisitionRaisedByEmail As String = "" 'Hemant (22 Aug 2019)
        'Hemant (06 Aug 2019) -- End   
        'Sohail (13 Jun 2016) -- Start
        'Enhancement - 61.1 - User access on On Staff Requisition Final Approved, All approvers are receiving Notifications even if department/branch not in his/her user access.
        Dim oUser As New clsUserAddEdit
        'Sohail (13 Jun 2016) -- End

        Try

            'Sohail (23 Sep 2021) -- Start
            'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
            If HttpContext.Current IsNot Nothing Then
                lstWebEmail = New List(Of clsEmailCollection)
            End If
            'Sohail (23 Sep 2021) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If intCompanyUnkId <= 0 Then intCompanyUnkId = Company._Object._Companyunkid
            'Sohail (21 Aug 2015) -- End


            If intStatusID = enApprovalStatus.PENDING Then
                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'intNextLevelPriority = objApproverLevel.GetMinPriority(intStaffReqById, intAllocationUnkId)

                'Pinkal (01-Dec-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'intNextLevelPriority = objApproverLevel.GetMinPriority(mstrEmployeeAsonDate, intStaffReqById, intAllocationUnkId)
                intNextLevelPriority = objApproverLevel.GetMinPriority(mstrEmployeeAsonDate, intStaffReqById, intAllocationUnkId, intStaffReqTranUnkId)
                'Pinkal (01-Dec-2021)-- End

                'Pinkal (16-Nov-2021)-- End
            Else
                dsList = objApproverMap.GetList("Approver", intApproverUserUnkid, intStaffReqById, intAllocationUnkId)
                If dsList.Tables("Approver").Rows.Count > 0 Then
                    intCurrLevelPriority = CInt(dsList.Tables("Approver").Rows(0).Item("priority"))
                    'Pinkal (16-Nov-2021)-- Start
                    'NMB Staff Requisition Approval Enhancements. 
                    'intNextLevelPriority = objApproverLevel.GetNextLevelPriority(intCurrLevelPriority, intStaffReqById, intAllocationUnkId)

                    'Pinkal (01-Dec-2021)-- Start
                    'NMB Staff Requisition Approval Enhancements. 
                    'intNextLevelPriority = objApproverLevel.GetNextLevelPriority(intCurrLevelPriority, mstrEmployeeAsonDate, intStaffReqById, intAllocationUnkId)
                    intNextLevelPriority = objApproverLevel.GetNextLevelPriority(intCurrLevelPriority, mstrEmployeeAsonDate, intStaffReqById, intAllocationUnkId, intStaffReqTranUnkId)
                    'Pinkal (01-Dec-2021)-- End

                    'Pinkal (16-Nov-2021)-- End
                    If intNextLevelPriority = -1 Then
                        'Pinkal (16-Nov-2021)-- Start
                        'NMB Staff Requisition Approval Enhancements.
                        'intMaxLevelPriority = objApproverLevel.GetMaxPriority(intStaffReqById, intAllocationUnkId)

                        'Pinkal (01-Dec-2021)-- Start
                        'NMB Staff Requisition Approval Enhancements. 
                        'intMaxLevelPriority = objApproverLevel.GetMaxPriority(mstrEmployeeAsonDate, intStaffReqById, intAllocationUnkId)
                        intMaxLevelPriority = objApproverLevel.GetMaxPriority(mstrEmployeeAsonDate, intStaffReqById, intAllocationUnkId, intStaffReqTranUnkId)
                        'Pinkal (01-Dec-2021)-- End

                        'Pinkal (16-Nov-2021)-- End
                    End If
                End If
            End If
            If Not (intStatusID = enApprovalStatus.CANCELLED OrElse intStatusID = enApprovalStatus.REJECTED) Then
                If intCurrLevelPriority > -1 AndAlso intCurrLevelPriority = intMaxLevelPriority AndAlso intNextLevelPriority = -1 Then
                    'Sohail (13 Jun 2016) -- Start
                    'Enhancement - 61.1 - User access on On Staff Requisition Final Approved, All approvers are receiving Notifications even if department/branch not in his/her user access.
                    'strApproverUnkIDs = objApproverMap.GetPublishStaffRequisitionUserUnkIDs()
                    'Hemant (03 Sep 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 20 : Upon final approval of staff requisition, system should send email to selected users configured on configuration. The email content should be as below.).
                    'Dim ds As DataSet = oUser.Get_UserBy_PrivilegeId(354) '354 = AllowtoAddVacancy
                    'Sohail (12 Nov 2020) -- Start
                    'NMB Enhancement # : - Stop notification to AllowtoAddVacancy privilege users and send notification to initiator and configured users only.
                    'Dim ds As DataSet = oUser.Get_UserBy_PrivilegeId(354, intYearUnkid) '354 = AllowtoAddVacancy
                    ''Hemant (03 Sep 2019) -- End
                    'strApproverUnkIDs = String.Join(",", (From p In ds.Tables(0) Select (p.Item("UId").ToString)).ToArray)
                    strApproverUnkIDs = ""
                    'Sohail (12 Nov 2020) -- End
                    'Sohail (13 Jun 2016) -- End

                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - On notification center on configuration, provide a section to configure notification to be sent to specified users when a vacancy requisition is final approved in 75.1.
                    'If strNotifyStaffReqFinalApprovedUserIDs.Trim <> "" Then
                    '    If strApproverUnkIDs.Trim <> "" Then
                    '        strApproverUnkIDs &= "," & strNotifyStaffReqFinalApprovedUserIDs
                    '    Else
                    '        strApproverUnkIDs = strNotifyStaffReqFinalApprovedUserIDs
                    '    End If

                    '    strApproverUnkIDs = String.Join(",", (From s In strApproverUnkIDs.Split(CChar(",")) Select (s)).Distinct().ToArray())
                    'End If
                    'Sohail (12 Oct 2018) -- End

                    blnFinalApproved = True
                Else
                    'Pinkal (16-Nov-2021)-- Start
                    'NMB Staff Requisition Approval Enhancements. 
                    'strApproverUnkIDs = objApproverMap.GetApproverUnkIDs(intStaffReqById, intAllocationUnkId, intNextLevelPriority)

                    'Pinkal (01-Dec-2021)-- Start
                    'NMB Staff Requisition Approval Enhancements. 
                    'strApproverUnkIDs = objApproverMap.GetApproverUnkIDs(mstrEmployeeAsonDate, intStaffReqById, intAllocationUnkId, intNextLevelPriority)
                    strApproverUnkIDs = objApproverMap.GetApproverUnkIDs(mstrEmployeeAsonDate, intStaffReqById, intAllocationUnkId, intNextLevelPriority, 0, "", intStaffReqTranUnkId)
                    'Pinkal (01-Dec-2021)-- End

                    'Pinkal (16-Nov-2021)-- End

                End If
            ElseIf intStatusID = enApprovalStatus.PUBLISHED Then
                'strApproverUnkIDs = objApproverMap.GetPublishStaffRequisitionUserUnkIDs()
            ElseIf intStatusID = enApprovalStatus.CANCELLED Then
                strApproverUnkIDs = objApproverMap.GetCancelStaffRequisitionUserUnkIDs(intStaffReqById, intAllocationUnkId, intApproverUserUnkid)
            Else
                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'strApproverUnkIDs = objApproverMap.GetLowerApproverUnkIDs(intCurrLevelPriority, intStaffReqById, intAllocationUnkId)

                'Pinkal (01-Dec-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'strApproverUnkIDs = objApproverMap.GetLowerApproverUnkIDs(mstrEmployeeAsonDate, intCurrLevelPriority, intStaffReqById, intAllocationUnkId)
                strApproverUnkIDs = objApproverMap.GetLowerApproverUnkIDs(mstrEmployeeAsonDate, intCurrLevelPriority, intStaffReqById, intAllocationUnkId, intStaffReqTranUnkId)
                'Pinkal (01-Dec-2021)-- End

                'Pinkal (16-Nov-2021)-- End
            End If

            'Hemant (22 Aug 2019) -- Start
             'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            'If strApproverUnkIDs.Trim = "" Then Exit Try

            'strArrayIDs = strApproverUnkIDs.Split(",")
            'Hemant (22 Aug 2019) -- End

            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            objStaffrequisitionTran._Staffrequisitiontranunkid = intStaffReqTranUnkId
            objUser._Userunkid = objStaffrequisitionTran._Userunkid
            strRequisitionRaisedByName = objUser._Firstname & " " & objUser._Lastname
            'Hemant (22 Aug 2019) -- Start
             'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
             strRequisitionRaisedByEmail = objUser._Email
            'Hemant (22 Aug 2019) -- End
            'Hemant (06 Aug 2019) -- End

            objUser._Userunkid = intApproverUserUnkid
            strApproverName = objUser._Firstname & " " & objUser._Lastname
            'Hemant (22 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            'If strApproverUnkIDs.Trim = "" Then Exit Try
            If strApproverUnkIDs.Trim <> "" Then

                strArrayIDs = strApproverUnkIDs.Split(",")
                'Hemant (22 Aug 2019) -- End
            Dim objMaster As New clsMasterData
                'Sohail (23 Sep 2021) -- Start
                'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
                'gobjEmailList = New List(Of clsEmailCollection)
                If HttpContext.Current Is Nothing Then
            gobjEmailList = New List(Of clsEmailCollection)
                End If
                'Sohail (23 Sep 2021) -- End
                'Sohail (11 Aug 2021) -- Start
                'NMB Enhancement :  : The staff requisition requests are sent to all level 1 approvers (respective cheifs) despite them not having access to the respective function.
                Dim objEmp As New clsEmployee_Master
                Dim intPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, DateAndTime.Today, intYearUnkid, 0, , False, objDataOperation, , True)
                If intPeriodId < 0 Then
                    intPeriodId = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, intYearUnkid, enStatusType.OPEN, False, False, objDataOperation)
                End If
                If intPeriodId < 0 Then
                    intPeriodId = 1
                End If
                'Sohail (11 Aug 2021) -- End

            For Each strApproverID As String In strArrayIDs

                'Sohail (13 Jun 2016) -- Start
                'Enhancement - 61.1 - User access on On Staff Requisition Final Approved, All approvers are receiving Notifications even if department/branch not in his/her user access.
                    'Sohail (11 Aug 2021) -- Start
                    'NMB Enhancement :  : The staff requisition requests are sent to all level 1 approvers (respective cheifs) despite them not having access to the respective function.
                    'Dim mblnFlag As Boolean = False
                    'If blnFinalApproved = True Then
                    '    Dim dtUserAccess As DataTable = oUser.GetUserAccessFromUser(CInt(strApproverID), intCompanyUnkId, intYearUnkid, strUserAccessModeSetting)
                    '    If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then

                    '        For Each AID As String In strUserAccessModeSetting.Trim.Split(CChar(","))
                    '            Dim drRow() As DataRow = Nothing

                    '            If strUserAccessModeSetting.Trim.Contains(intStaffReqById) = False Then
                    '                mblnFlag = True
                    '                Exit For
                    '            End If

                    '            Select Case CInt(AID)

                    '                Case enAllocation.DEPARTMENT
                    '                    drRow = dtUserAccess.Select("allocationunkid = " & intAllocationUnkId & " AND referenceunkid = " & enAllocation.DEPARTMENT)
                    '                    If drRow.Length > 0 Then
                    '                        If intStaffReqById = CInt(AID) Then mblnFlag = True
                    '                    Else
                    '                        mblnFlag = False
                    '                        Exit For
                    '                    End If

                    '                Case enAllocation.JOBS
                    '                    drRow = dtUserAccess.Select("allocationunkid = " & intAllocationUnkId & " AND referenceunkid = " & enAllocation.JOBS)
                    '                    If drRow.Length > 0 Then
                    '                        If intStaffReqById = CInt(AID) Then mblnFlag = True
                    '                    Else
                    '                        mblnFlag = False
                    '                        Exit For
                    '                    End If

                    '                Case enAllocation.CLASS_GROUP
                    '                    drRow = dtUserAccess.Select("allocationunkid = " & intAllocationUnkId & " AND referenceunkid = " & enAllocation.CLASS_GROUP)
                    '                    If drRow.Length > 0 Then
                    '                        If intStaffReqById = CInt(AID) Then mblnFlag = True
                    '                    Else
                    '                        mblnFlag = False
                    '                        Exit For
                    '                    End If

                    '                Case enAllocation.CLASSES
                    '                    drRow = dtUserAccess.Select("allocationunkid = " & intAllocationUnkId & " AND referenceunkid = " & enAllocation.CLASSES)
                    '                    If drRow.Length > 0 Then
                    '                        If intStaffReqById = CInt(AID) Then mblnFlag = True
                    '                    Else
                    '                        mblnFlag = False
                    '                        Exit For
                    '                    End If

                    '                Case Else
                    '                    mblnFlag = True
                    '            End Select
                    '        Next
                    '    End If
                    'End If

                    'If mblnFlag = False AndAlso blnFinalApproved = True Then Continue For
                    Dim dsEmp As DataSet = objEmp.Get_AllocationBasedEmployee(intStaffReqById, intAllocationUnkId, intPeriodId, strDatabaseName, strUserAccessModeSetting, intCompanyUnkId, CInt(strApproverID), True, objDataOperation)
                    If dsEmp.Tables(0).Rows.Count <= 0 Then Continue For
                    'Sohail (11 Aug 2021) -- End
                'Sohail (13 Jun 2016) -- End

                strMessage = New StringBuilder

                objUser = New clsUserAddEdit

                objUser._Userunkid = CInt(strApproverID)
                If objUser._Email.Trim = "" Then Continue For

                strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " <B>" & objUser._Firstname & " " & objUser._Lastname & "</B>, <BR><BR>")
                strMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " <B>" & getTitleCase(objUser._Firstname & " " & objUser._Lastname) & "</B>, <BR><BR>")
                'Gajanan [27-Mar-2019] -- End

                If blnFinalApproved = True Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 3, "Reminder to initiate the recruitment process")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 13, "This is to notify you that a staff requisition for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    'Hemant (06 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    'strMessage.Append(Language.getMessage(mstrModuleName, 13, "This is to notify you that a staff requisition for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    ''strMessage.Append(Language.getMessage(mstrModuleName, 5, "has been final approved. Please login to Aruti Recruitment Module to initiate the recruitment process."))
                    'strMessage.Append(" " & Language.getMessage(mstrModuleName, 5, "has been final approved. Please login to Aruti Recruitment Module to initiate the recruitment process."))
                    ''strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This staff requisition has been final approved by") & " <B>" & strApproverName & "</B>.")
                    'strMessage.Append("<BR><BR>" & Language.getMessage(mstrModuleName, 6, "This staff requisition has been final approved by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append(Language.getMessage(mstrModuleName, 29, "This is to notify you that your request to employ new staff for the position of ") & "  <B>" & strJobTitle & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 30, "has been final approved by") & " <B>" & strApproverName & "</B>.")
                    'Hemant (06 Aug 2019) -- End
                    'Gajanan [27-Mar-2019] -- End

                ElseIf intStatusID = enApprovalStatus.PENDING Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 7, "Reminder for approving Staff Requisition for") & " " & strJobTitle

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    'Gajanan [27-Mar-2019] -- End

                    strMessage.Append(Language.getMessage(mstrModuleName, 8, "has been placed and awaiting your approval. "))

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language


                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 9, "This staff requisition has been placed by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR>" & Language.getMessage(mstrModuleName, 9, "This staff requisition has been placed by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                    'Hemant (06 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    If 1 = 2 Then
                        'Hemant (06 Aug 2019) -- End
                    If strRemarks.Trim <> "" Then

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<I>" & Language.getMessage(mstrModuleName, 10, "*Remarks :") & " " & strRemarks & "</I>")
                        strMessage.Append("<BR><BR><I>" & Language.getMessage(mstrModuleName, 10, "*Remarks :") & " " & strRemarks & "</I>")
                        'Gajanan [27-Mar-2019] -- End
                    End If
                    End If 'Hemant (06 Aug 2019)

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = True Then
                    If blnIsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = True Then
                        'Sohail (21 Aug 2015) -- End
                        'Sohail (12 Oct 2018) -- Start
                        'NMB Enhancement - Ref. # :  - On notification center on configuration, provide a section to configure notification to be sent to specified users when a vacancy requisition is final approved in 75.1.
                        'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 100, "Clink the link below to approve/reject the requisition."))

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 23, "Clik the link below to approve/reject the requisition."))
                        strMessage.Append("<BR><BR>" & Language.getMessage(mstrModuleName, 23, "Clik the link below to approve/reject the requisition."))
                        'Gajanan [27-Mar-2019] -- End
                        'Sohail (12 Oct 2018) -- End
                        Dim strLink As String
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'strLink = ConfigParameter._Object._ArutiSelfServiceURL & "/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisition.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & strApproverID & "|" & intStaffReqApprovalTranUnkId.ToString & "|" & intStaffReqTranUnkId.ToString & "|" & enApprovalStatus.PENDING & ""))
                        strLink = strArutiSelfServiceURL & "/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisition.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & strApproverID & "|" & intStaffReqApprovalTranUnkId.ToString & "|" & intStaffReqTranUnkId.ToString & "|" & enApprovalStatus.PENDING & ""))
                        'Sohail (21 Aug 2015) -- End


                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append("<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>")
                        strMessage.Append("<BR><a href='" & strLink & "'>" & strLink & "</a>")
                        'Gajanan [27-Mar-2019] -- End
                    End If

                ElseIf intStatusID = enApprovalStatus.APPROVED Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 7, "Reminder for approving Staff Requisition for") & " " & strJobTitle

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B> ")
                    'Gajanan [27-Mar-2019] -- End
                    strMessage.Append(Language.getMessage(mstrModuleName, 8, "has been placed and awaiting your approval."))

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 11, "This staff requisition has been approved by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR>" & Language.getMessage(mstrModuleName, 11, "This staff requisition has been approved by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = True Then
                    If blnIsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = True Then
                        'Sohail (21 Aug 2015) -- End 
                        'Sohail (12 Oct 2018) -- Start
                        'NMB Enhancement - Ref. # :  - On notification center on configuration, provide a section to configure notification to be sent to specified users when a vacancy requisition is final approved in 75.1.
                        'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 100, "Clink the link below to approve/reject the requisition."))

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 23, "Clik the link below to approve/reject the requisition."))
                        strMessage.Append("<BR><BR>" & Language.getMessage(mstrModuleName, 23, "Clik the link below to approve/reject the requisition."))
                        'Gajanan [27-Mar-2019] -- End
                        'Sohail (12 Oct 2018) -- End
                        Dim strLink As String
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'strLink = ConfigParameter._Object._ArutiSelfServiceURL & "/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisition.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & strApproverID & "|" & intStaffReqApprovalTranUnkId.ToString & "|" & intStaffReqTranUnkId.ToString & "|" & enApprovalStatus.PENDING & ""))
                        strLink = strArutiSelfServiceURL & "/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisition.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & strApproverID & "|" & intStaffReqApprovalTranUnkId.ToString & "|" & intStaffReqTranUnkId.ToString & "|" & enApprovalStatus.PENDING & ""))
                        'Sohail (21 Aug 2015) -- End

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append("<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>")
                        strMessage.Append("<BR><a href='" & strLink & "'>" & strLink & "</a>")
                        'Gajanan [27-Mar-2019] -- End
                    End If

                ElseIf intStatusID = enApprovalStatus.REJECTED Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 12, "Staff Requisition rejected for") & " " & strJobTitle


                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language


                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B> ")
                    'Hemant (06 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    'strMessage.Append(Language.getMessage(mstrModuleName, 14, "has been rejected. "))
                    ''strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 15, "This staff requisition has been rejected by") & " <B>" & strApproverName & "</B>.")
                    'strMessage.Append("<BR><BR>" & Language.getMessage(mstrModuleName, 15, "This staff requisition has been rejected by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append(Language.getMessage(mstrModuleName, 27, "has been rejected by") & " <B>" & strApproverName & "</B>.")
                    If strRemarks.Trim <> "" Then
                        strMessage.Append("<BR><BR><I>" & Language.getMessage(mstrModuleName, 28, "Reason for Rejection:") & " " & strRemarks & "</I>")
                    End If
                    'Hemant (06 Aug 2019) -- End
                    'Gajanan [27-Mar-2019] -- End

                ElseIf intStatusID = enApprovalStatus.CANCELLED Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 16, "Staff Requisition cancelled for") & " " & strJobTitle

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 17, "has been cancelled. "))
                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 18, "This staff requisition has been cancelled by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR>" & Language.getMessage(mstrModuleName, 18, "This staff requisition has been cancelled by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                ElseIf intStatusID = enApprovalStatus.PUBLISHED Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 19, "Staff Requisition published for") & " " & strJobTitle

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 20, "has been published. "))
                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 21, "This staff requisition has been published by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR>" & Language.getMessage(mstrModuleName, 21, "This staff requisition has been published by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                End If


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage.Append("<BR><BR><BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</B>")
                strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>'POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.'</b></center></p>")
                'Gajanan [27-Mar-2019] -- End

                strMessage.Append("</BODY></HTML>")

                objMail._Message = strMessage.ToString
                objMail._ToEmail = objUser._Email

                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFormName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFormName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = intApproverUserUnkid
                Dim objUsr As New clsUserAddEdit
                objUsr._Userunkid = intApproverUserUnkid
                objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT

                    'Sohail (23 Sep 2021) -- Start
                    'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
                    'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    If HttpContext.Current Is Nothing Then
                gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    Else
                        lstWebEmail.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    End If
                    'Sohail (23 Sep 2021) -- End
                objUsr = Nothing
            Next

            End If 'Sohail (12 Nov 2020)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On notification center on configuration, provide a section to configure notification to be sent to specified users when a vacancy requisition is final approved in 75.1.
            If strNotifyStaffReqFinalApprovedUserIDs.Trim <> "" AndAlso blnFinalApproved = True Then
                strFinalApproverArrayIDs = strNotifyStaffReqFinalApprovedUserIDs.Split(",")


                objUser._Userunkid = intApproverUserUnkid
                strApproverName = objUser._Firstname & " " & objUser._Lastname

                For Each strApproverID As String In strFinalApproverArrayIDs


                    strMessage = New StringBuilder

                    objUser = New clsUserAddEdit

                    objUser._Userunkid = CInt(strApproverID)
                    If objUser._Email.Trim = "" Then Continue For

                    strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")

                    strMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " <B>" & objUser._Firstname & " " & objUser._Lastname & "</B>, <BR><BR>")

                    If blnFinalApproved = True Then

                        objMail._Subject = Language.getMessage(mstrModuleName, 24, "Staff Requisition Final Approved for") & " " & strJobTitle


                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language

                        'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 13, "This is to notify you that a staff requisition for the position of") & "  <B>" & strJobTitle & "</B>  ")
                        'Hemant (06 Aug 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                        'strMessage.Append(Language.getMessage(mstrModuleName, 13, "This is to notify you that a staff requisition for the position of") & "  <B>" & strJobTitle & "</B>  ")
                        'strMessage.Append(Language.getMessage(mstrModuleName, 25, "has been final approved. "))
                        ''strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 26, "This staff requisition has been final approved by") & " <B>" & strApproverName & "</B>.")
                        'strMessage.Append("<BR><BR>" & Language.getMessage(mstrModuleName, 26, "This staff requisition has been final approved by") & " <B>" & strApproverName & "</B>.")
                        strMessage.Append(Language.getMessage(mstrModuleName, 31, "This is to notify you that your request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B>  ")
                        strMessage.Append(Language.getMessage(mstrModuleName, 32, "raised by") & " <B>" & strRequisitionRaisedByName & "</B> ")
                        strMessage.Append(Language.getMessage(mstrModuleName, 33, "has been final approved by") & " <B>" & strApproverName & "</B>. ")
                        strMessage.Append(Language.getMessage(mstrModuleName, 34, "Please take note of it."))
                        'Hemant (06 Aug 2019) -- End
                        'Gajanan [27-Mar-2019] -- End

                    End If


                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage.Append("<BR><BR><BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</B>")
                    strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>""POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.""</b></center></p>")
                    'Gajanan [27-Mar-2019] -- End

                    strMessage.Append("</BODY></HTML>")

                    objMail._Message = strMessage.ToString
                    objMail._ToEmail = objUser._Email

                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrWebFormName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFormName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = intApproverUserUnkid
                    Dim objUsr As New clsUserAddEdit
                    objUsr._Userunkid = intApproverUserUnkid
                    objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT

                    'Sohail (23 Sep 2021) -- Start
                    'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
                    'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    If HttpContext.Current Is Nothing Then
                    gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    Else
                        lstWebEmail.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    End If
                    'Sohail (23 Sep 2021) -- End
                    objUsr = Nothing
                Next
            End If
            'Sohail (12 Oct 2018) -- End

                'Hemant (22 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            'End If 'Sohail (12 Nov 2020)

            If strRequisitionRaisedByEmail.Trim <> "" Then
                If intStatusID = enApprovalStatus.REJECTED Then

                    strMessage = New StringBuilder

                    strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")

                    strMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " <B>" & getTitleCase(strRequisitionRaisedByName) & "</B>, <BR><BR>")

                    objMail._Subject = Language.getMessage(mstrModuleName, 12, "Staff Requisition rejected for") & " " & strJobTitle

                    strMessage.Append(Language.getMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of") & "  <B>" & strJobTitle & "</B> ")

                    strMessage.Append(Language.getMessage(mstrModuleName, 27, "has been rejected by") & " <B>" & strApproverName & "</B>.")

                    If strRemarks.Trim <> "" Then
                        strMessage.Append("<BR><BR><I>" & Language.getMessage(mstrModuleName, 28, "Reason for Rejection:") & " " & strRemarks & "</I>")
                    End If

                    strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>'POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.'</b></center></p>")

                    strMessage.Append("</BODY></HTML>")

                    objMail._Message = strMessage.ToString
                    objMail._ToEmail = strRequisitionRaisedByEmail

                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrWebFormName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFormName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = intApproverUserUnkid
                    Dim objUsr As New clsUserAddEdit
                    objUsr._Userunkid = intApproverUserUnkid
                    objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT

                    'Sohail (23 Sep 2021) -- Start
                    'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
                    'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    If HttpContext.Current Is Nothing Then
                    gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    Else
                        lstWebEmail.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    End If
                    'Sohail (23 Sep 2021) -- End
                    objUsr = Nothing

                ElseIf blnFinalApproved = True Then

                    strMessage = New StringBuilder

                    strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")

                    strMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " <B>" & getTitleCase(strRequisitionRaisedByName) & "</B>, <BR><BR>")

                    objMail._Subject = Language.getMessage(mstrModuleName, 3, "Reminder to initiate the recruitment process")

                    strMessage.Append(Language.getMessage(mstrModuleName, 29, "This is to notify you that your request to employ new staff for the position of ") & "  <B>" & strJobTitle & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 30, "has been final approved by") & " <B>" & strApproverName & "</B>.")

                    strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>'POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.'</b></center></p>")

                    strMessage.Append("</BODY></HTML>")

                    objMail._Message = strMessage.ToString
                    objMail._ToEmail = strRequisitionRaisedByEmail

                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrWebFormName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFormName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = intApproverUserUnkid
                    Dim objUsr As New clsUserAddEdit
                    objUsr._Userunkid = intApproverUserUnkid
                    objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT

                    'Sohail (23 Sep 2021) -- Start
                    'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
                    'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    If HttpContext.Current Is Nothing Then
                    gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    Else
                        lstWebEmail.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    End If
                    'Sohail (23 Sep 2021) -- End
                    objUsr = Nothing

                End If
            End If
            'Hemant (22 Aug 2019) -- End

            'trd = New Thread(AddressOf Send_Notification)
            'trd.IsBackground = True
            'trd.Start()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToApprover; Module Name: " & mstrModuleName)
        Finally
            objApproverLevel = Nothing
            objApproverMap = Nothing
            objUser = Nothing
            objMail = Nothing
        End Try
    End Sub

    'Private Sub Send_Notification()
    '    Try
    '        If gobjEmailList.Count > 0 Then
    '            Dim objSendMail As New clsSendMail
    '            For Each obj In gobjEmailList
    '                objSendMail._ToEmail = obj._EmailTo
    '                objSendMail._Subject = obj._Subject
    '                objSendMail._Message = obj._Message
    '                objSendMail._Form_Name = obj._Form_Name
    '                objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
    '                objSendMail._OperationModeId = obj._OperationModeId
    '                objSendMail._UserUnkid = obj._UserUnkid
    '                objSendMail._SenderAddress = obj._SenderAddress
    '                objSendMail._ModuleRefId = obj._ModuleRefId
    '                Try
    '                    objSendMail.SendMail()
    '                Catch ex As Exception

    '                End Try
    '            Next
    '            gobjEmailList.Clear()
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
    '    Finally
    '        If gobjEmailList.Count > 0 Then
    '            gobjEmailList.Clear()
    '        End If
    '    End Try
    'End Sub

    Public Function InsertAuditTrailForStaffRequisitionApprovalTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@staffrequisitionapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitionapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApproval_Date.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@disapprovalreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisapprovalreason.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1) 'mintLogEmployeeUnkid
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 22, "WEB"))
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString)

            strQ = "INSERT INTO atrcstaffrequisition_approval_tran ( " & _
                "  staffrequisitionapprovaltranunkid " & _
                ", staffrequisitiontranunkid " & _
                ", levelunkid " & _
                ", priority " & _
                ", approval_date " & _
                ", statusunkid " & _
                ", disapprovalreason " & _
                ", remarks " & _
                ", audittype " & _
                ", audituserunkid " & _
                ", auditdatetime " & _
                ", ip " & _
                ", machine_name" & _
                ", form_name " & _
                ", module_name1 " & _
                ", module_name2 " & _
                ", module_name3 " & _
                ", module_name4 " & _
                ", module_name5 " & _
                ", isweb " & _
                ", loginemployeeunkid " & _
            ") VALUES (" & _
                "  @staffrequisitionapprovaltranunkid " & _
                ", @staffrequisitiontranunkid " & _
                ", @levelunkid " & _
                ", @priority " & _
                ", @approval_date " & _
                ", @statusunkid " & _
                ", @disapprovalreason " & _
                ", @remarks " & _
                ", @audittype " & _
                ", @audituserunkid " & _
                ", @auditdatetime " & _
                ", @ip " & _
                ", @machine_name" & _
                ", @form_name " & _
                ", @module_name1 " & _
                ", @module_name2 " & _
                ", @module_name3 " & _
                ", @module_name4 " & _
                ", @module_name5 " & _
                ", @isweb " & _
                ", @loginemployeeunkid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStaffrequisitionapprovaltranunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForStaffRequisitionApprovalTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Staff Requisition Cancelled")
			Language.setMessage(mstrModuleName, 2, "Dear")
			Language.setMessage(mstrModuleName, 3, "Reminder to initiate the recruitment process")
			Language.setMessage(mstrModuleName, 4, "This is to notify you that a request to employ new staff for the position of")
			Language.setMessage(mstrModuleName, 7, "Reminder for approving Staff Requisition for")
			Language.setMessage(mstrModuleName, 8, "has been placed and awaiting your approval.")
			Language.setMessage(mstrModuleName, 9, "This staff requisition has been placed by")
			Language.setMessage(mstrModuleName, 10, "*Remarks :")
			Language.setMessage(mstrModuleName, 11, "This staff requisition has been approved by")
			Language.setMessage(mstrModuleName, 12, "Staff Requisition rejected for")
			Language.setMessage(mstrModuleName, 16, "Staff Requisition cancelled for")
			Language.setMessage(mstrModuleName, 17, "has been cancelled.")
			Language.setMessage(mstrModuleName, 18, "This staff requisition has been cancelled by")
			Language.setMessage(mstrModuleName, 19, "Staff Requisition published for")
			Language.setMessage(mstrModuleName, 20, "has been published.")
			Language.setMessage(mstrModuleName, 21, "This staff requisition has been published by")
			Language.setMessage(mstrModuleName, 22, "WEB")
			Language.setMessage(mstrModuleName, 23, "Clik the link below to approve/reject the requisition.")
			Language.setMessage(mstrModuleName, 24, "Staff Requisition Final Approved for")
			Language.setMessage(mstrModuleName, 27, "has been rejected by")
			Language.setMessage(mstrModuleName, 28, "Reason for Rejection:")
			Language.setMessage(mstrModuleName, 29, "This is to notify you that your request to employ new staff for the position of")
			Language.setMessage(mstrModuleName, 30, "has been final approved by")
			Language.setMessage(mstrModuleName, 31, "This is to notify you that your request to employ new staff for the position of")
			Language.setMessage(mstrModuleName, 32, "raised by")
			Language.setMessage(mstrModuleName, 33, "has been final approved by")
			Language.setMessage(mstrModuleName, 34, "Please take note of it.")

		Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
