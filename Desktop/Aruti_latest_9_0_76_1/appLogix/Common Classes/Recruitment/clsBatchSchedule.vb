﻿'************************************************************************************************************************************
'Class Name : clsBatchSchedule.vb
'Purpose    :
'Date       :16/08/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web
Imports System.Threading
Imports System.Text

''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsBatchSchedule
    Private Shared ReadOnly mstrModuleName As String = "clsBatchSchedule"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

    
#Region " Private variables "
    Private mintBatchscheduleunkid As Integer
    Private mintVacancyunkid As Integer
    Private mstrBatchcode As String = String.Empty
    Private mstrBatchname As String = String.Empty
    Private mdtInterviewdate As Date
    Private mdtInterviewtime As Date
    Private mintInterviewtypeunkid As Integer
    Private mintResultgroupunkid As Integer
    Private mstrLocation As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mstrCancel_Remark As String = String.Empty
    Private mdtCancel_Date As Date
    Private mblnIsclosed As Boolean
    Private mblnIscancel As Boolean
    Private mintCancel_Userunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private objInterviewerTran As New clsBatchSchedule_interviewer_tran     'Hemant (27 Sep 2019)
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mintLoginemployeeunkid As Integer = -1
    Dim objEmailList As List(Of clsEmailCollection)
    Private objThread As Thread
    'Hemant (07 Oct 2019) -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchscheduleunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Batchscheduleunkid() As Integer
        Get
            Return mintBatchscheduleunkid
        End Get
        Set(ByVal value As Integer)
            mintBatchscheduleunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Vacancyunkid() As Integer
        Get
            Return mintVacancyunkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchcode
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Batchcode() As String
        Get
            Return mstrBatchcode
        End Get
        Set(ByVal value As String)
            mstrBatchcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchname
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Batchname() As String
        Get
            Return mstrBatchname
        End Get
        Set(ByVal value As String)
            mstrBatchname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interviewdate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Interviewdate() As Date
        Get
            Return mdtInterviewdate
        End Get
        Set(ByVal value As Date)
            mdtInterviewdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interviewtime
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Interviewtime() As Date
        Get
            Return mdtInterviewtime
        End Get
        Set(ByVal value As Date)
            mdtInterviewtime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interviewtypeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Interviewtypeunkid() As Integer
        Get
            Return mintInterviewtypeunkid
        End Get
        Set(ByVal value As Integer)
            mintInterviewtypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultgroupunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Resultgroupunkid() As Integer
        Get
            Return mintResultgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintResultgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set location
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Location() As String
        Get
            Return mstrLocation
        End Get
        Set(ByVal value As String)
            mstrLocation = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_remark
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Cancel_Remark() As String
        Get
            Return mstrCancel_Remark
        End Get
        Set(ByVal value As String)
            mstrCancel_Remark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_date
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Cancel_Date() As Date
        Get
            Return mdtCancel_Date
        End Get
        Set(ByVal value As Date)
            mdtCancel_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isclosed
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isclosed() As Boolean
        Get
            Return mblnIsclosed
        End Get
        Set(ByVal value As Boolean)
            mblnIsclosed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscancel
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Iscancel() As Boolean
        Get
            Return mblnIscancel
        End Get
        Set(ByVal value As Boolean)
            mblnIscancel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_userunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Cancel_Userunkid() As Integer
        Get
            Return mintCancel_Userunkid
        End Get
        Set(ByVal value As Integer)
            mintCancel_Userunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property
    'Hemant (07 Oct 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  batchscheduleunkid " & _
              ", vacancyunkid " & _
              ", batchcode " & _
              ", batchname " & _
              ", interviewdate " & _
              ", interviewtime " & _
              ", interviewtypeunkid " & _
              ", resultgroupunkid " & _
              ", location " & _
              ", description " & _
              ", cancel_remark " & _
              ", cancel_date " & _
              ", isclosed " & _
              ", iscancel " & _
              ", cancel_userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM rcbatchschedule_master " & _
             "WHERE batchscheduleunkid = @batchscheduleunkid "

            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchscheduleunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBatchscheduleunkid = CInt(dtRow.Item("batchscheduleunkid"))
                mintVacancyunkid = CInt(dtRow.Item("vacancyunkid"))
                mstrBatchcode = dtRow.Item("batchcode").ToString
                mstrBatchname = dtRow.Item("batchname").ToString

                If IsDBNull(dtRow.Item("interviewdate")) Then
                    mdtInterviewdate = Nothing
                Else
                    mdtInterviewdate = dtRow.Item("interviewdate")
                End If

                mdtInterviewtime = dtRow.Item("interviewtime")
                mintInterviewtypeunkid = CInt(dtRow.Item("interviewtypeunkid"))
                mintResultgroupunkid = CInt(dtRow.Item("resultgroupunkid"))
                mstrLocation = dtRow.Item("location").ToString
                mstrDescription = dtRow.Item("description").ToString
                mstrCancel_Remark = dtRow.Item("cancel_remark").ToString
                If IsDBNull(dtRow.Item("cancel_date")) Then
                    mdtCancel_Date = Nothing
                Else
                    mdtCancel_Date = dtRow.Item("cancel_date")
                End If

                mblnIsclosed = CBool(dtRow.Item("isclosed"))
                mblnIscancel = CBool(dtRow.Item("iscancel"))
                mintCancel_Userunkid = CInt(dtRow.Item("cancel_userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = False, Optional ByVal intBatchScheduleunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '           "  rcbatchschedule_master.batchscheduleunkid " & _
            '           ", rcbatchschedule_master.vacancyunkid " & _
            '           ", rcbatchschedule_master.batchcode " & _
            '           ", rcbatchschedule_master.batchname " & _
            '           ", CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) as  interviewdate " & _
            '           ", CONVERT(CHAR(5),rcbatchschedule_master.interviewtime,108) as interviewtime " & _
            '           ", rcbatchschedule_master.interviewtypeunkid " & _
            '           ", rcbatchschedule_master.resultgroupunkid " & _
            '           ", rcbatchschedule_master.location " & _
            '           ", rcbatchschedule_master.description " & _
            '           ", rcbatchschedule_master.cancel_remark " & _
            '           ", CONVERT(CHAR(8),rcbatchschedule_master.cancel_date,112) as cancel_date " & _
            '           ", rcbatchschedule_master.isclosed " & _
            '           ", rcbatchschedule_master.iscancel " & _
            '           ", rcbatchschedule_master.cancel_userunkid " & _
            '           ", rcbatchschedule_master.isvoid " & _
            '           ", rcbatchschedule_master.voiduserunkid " & _
            '           ", CONVERT(CHAR(8),rcbatchschedule_master.voiddatetime,112) as  voiddatetime " & _
            '           ", rcvacancy_master.vacancytitle AS job " & _
            '           ", resultcode.name AS resultcode " & _
            '           ", InterviewType.name as InterviewType " & _
            '     "FROM rcbatchschedule_master " & _
            '     "LEFT JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid=rcbatchschedule_master.vacancyunkid " & _
            '     "LEFT JOIN cfcommon_master as resultcode ON resultcode.masterunkid=rcbatchschedule_master.resultgroupunkid " & _
            '     "LEFT JOIN cfcommon_master as InterviewType ON InterviewType.masterunkid=rcbatchschedule_master.interviewtypeunkid "

            strQ = "SELECT " & _
                       "  rcbatchschedule_master.batchscheduleunkid " & _
                       ", rcbatchschedule_master.vacancyunkid " & _
                       ", rcbatchschedule_master.batchcode " & _
                       ", rcbatchschedule_master.batchname " & _
                       ", CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) as  interviewdate " & _
                       ", CONVERT(CHAR(5),rcbatchschedule_master.interviewtime,108) as interviewtime " & _
                       ", rcbatchschedule_master.interviewtypeunkid " & _
                       ", rcbatchschedule_master.resultgroupunkid " & _
                       ", rcbatchschedule_master.location " & _
                       ", rcbatchschedule_master.description " & _
                       ", rcbatchschedule_master.cancel_remark " & _
                       ", CONVERT(CHAR(8),rcbatchschedule_master.cancel_date,112) as cancel_date " & _
                       ", rcbatchschedule_master.isclosed " & _
                       ", rcbatchschedule_master.iscancel " & _
                       ", rcbatchschedule_master.cancel_userunkid " & _
                       ", rcbatchschedule_master.isvoid " & _
                       ", rcbatchschedule_master.voiduserunkid " & _
                       ", CONVERT(CHAR(8),rcbatchschedule_master.voiddatetime,112) as  voiddatetime " & _
                       ", cfcommon_master.name AS job " & _
                       ", resultcode.name AS resultcode " & _
                       ", InterviewType.name as InterviewType " & _
                       ", CONVERT(CHAR(8),openingdate,112) AS Odate " & _
                       ", CONVERT(CHAR(8),closingdate,112) AS Cdate " & _
                 "FROM rcbatchschedule_master " & _
                 "LEFT JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid=rcbatchschedule_master.vacancyunkid " & _
                 "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND dbo.cfcommon_master.mastertype ='" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "'  " & _
                 "LEFT JOIN cfcommon_master as resultcode ON resultcode.masterunkid=rcbatchschedule_master.resultgroupunkid " & _
                 "LEFT JOIN cfcommon_master as InterviewType ON InterviewType.masterunkid=rcbatchschedule_master.interviewtypeunkid "
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            If intBatchScheduleunkid > 0 Then
                strQ &= " WHERE rcbatchschedule_master.batchscheduleunkid= @batchscheduleunkid "
                objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchScheduleunkid.ToString)
            End If

            'If blnOnlyActive Then
            '    strQ &= " WHERE iscancel = 1 "
            'End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcbatchschedule_master) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date, Optional ByVal dtInterviewerTran As DataTable = Nothing) As Boolean
        'Hemant (27 Sep 2019) -- [dtCurrentDateAndTime, dtInterviewerTran]
        If isExist(, mstrBatchname, mintBatchscheduleunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Name is already defined. Please define new Name.")
            Return False
        End If

        If isExist(mstrBatchcode, , mintBatchscheduleunkid) Then
            'Sandeep [ 09 Oct 2010 ] -- Start
            'mstrMessage = Language.getMessage(mstrModuleName, 1, "This Batch Code is already defined. Please define new Batch Code.")
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Batch Code is already defined. Please define new Batch Code.")
            'Sandeep [ 09 Oct 2010 ] -- End 
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 



        Try
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
            objDataOperation.AddParameter("@batchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchcode.ToString)
            objDataOperation.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchname.ToString)
            If mdtInterviewdate = Nothing Then
                objDataOperation.AddParameter("@interviewdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@interviewdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterviewdate)
            End If

            objDataOperation.AddParameter("@interviewtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterviewtime)
            objDataOperation.AddParameter("@interviewtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterviewtypeunkid.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)
            objDataOperation.AddParameter("@location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLocation.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancel_Remark.ToString)
            If mdtCancel_Date = Nothing Then
                objDataOperation.AddParameter("@cancel_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cancel_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Date)
            End If

            objDataOperation.AddParameter("@isclosed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclosed.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@cancel_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancel_Userunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO rcbatchschedule_master ( " & _
              "  vacancyunkid " & _
              ", batchcode " & _
              ", batchname " & _
              ", interviewdate " & _
              ", interviewtime " & _
              ", interviewtypeunkid " & _
              ", resultgroupunkid " & _
              ", location " & _
              ", description " & _
              ", cancel_remark " & _
              ", cancel_date " & _
              ", isclosed " & _
              ", iscancel " & _
              ", cancel_userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @vacancyunkid " & _
              ", @batchcode " & _
              ", @batchname " & _
              ", @interviewdate " & _
              ", @interviewtime " & _
              ", @interviewtypeunkid " & _
              ", @resultgroupunkid " & _
              ", @location " & _
              ", @description " & _
              ", @cancel_remark " & _
              ", @cancel_date " & _
              ", @isclosed " & _
              ", @iscancel " & _
              ", @cancel_userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBatchscheduleunkid = dsList.Tables(0).Rows(0).Item(0)

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "rcbatchschedule_master", "batchscheduleunkid", mintBatchscheduleunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            Dim blnFlag As Boolean = False

            If dtInterviewerTran IsNot Nothing Then
                objInterviewerTran._BatchScheduleUnkid = mintBatchscheduleunkid
                objInterviewerTran._DataTable = dtInterviewerTran
                blnFlag = objInterviewerTran.InsertUpdateDelete_InterviewerTran(dtCurrentDateAndTime, objDataOperation)
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            End If
            'Hemant (27 Sep 2019) -- End

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 

            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcbatchschedule_master) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As Date, Optional ByVal dtInterviewerTran As DataTable = Nothing) As Boolean
        'Hemant (27 Sep 2019) -- [dtCurrentDateAndTime, dtInterviewerTran]
        If isExist(, mstrBatchname, mintBatchscheduleunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Name is already defined. Please define new Name.")
            Return False
        End If
        If isExist(mstrBatchcode, , mintBatchscheduleunkid) Then
            'Sandeep [ 09 Oct 2010 ] -- Start
            'mstrMessage = Language.getMessage(mstrModuleName, 1, "This Batch Code is already defined. Please define new Batch Code.")
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Batch Code is already defined. Please define new Batch Code.")
            'Sandeep [ 09 Oct 2010 ] -- End 
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchscheduleunkid.ToString)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
            objDataOperation.AddParameter("@batchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchcode.ToString)
            objDataOperation.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchname.ToString)
            If mdtInterviewdate = Nothing Then
                objDataOperation.AddParameter("@interviewdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@interviewdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterviewdate)
            End If

            objDataOperation.AddParameter("@interviewtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterviewtime)
            objDataOperation.AddParameter("@interviewtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterviewtypeunkid.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)
            objDataOperation.AddParameter("@location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLocation.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            'objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancel_Remark.ToString)
            'If mdtCancel_Date = Nothing Then
            '    objDataOperation.AddParameter("@cancel_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@cancel_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Date)
            'End If
            objDataOperation.AddParameter("@isclosed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclosed.ToString)
            'objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            'objDataOperation.AddParameter("@cancel_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancel_Userunkid.ToString)
            'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE rcbatchschedule_master SET " & _
                          "  vacancyunkid = @vacancyunkid" & _
                          ", batchcode = @batchcode" & _
                          ", batchname = @batchname" & _
                          ", interviewdate = @interviewdate" & _
                          ", interviewtime = @interviewtime" & _
                          ", interviewtypeunkid = @interviewtypeunkid" & _
                          ", resultgroupunkid = @resultgroupunkid" & _
                          ", location = @location" & _
                          ", description = @description" & _
                          ", isclosed = @isclosed " & _
                    "WHERE batchscheduleunkid = @batchscheduleunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
            'If clsCommonATLog.IsTableDataUpdate("atcommon_log", "rcbatchschedule_master", mintBatchscheduleunkid, "batchscheduleunkid", 2) Then
            '    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "rcbatchschedule_master", "batchscheduleunkid", mintBatchscheduleunkid) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If

            Dim blnFlag As Boolean = False
            Dim blnToInsert As Boolean = False
            If dtInterviewerTran IsNot Nothing Then
                If dtInterviewerTran.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtInterviewerTran.Select("AUD=''")
                    If dt.Length = dtInterviewerTran.Rows.Count Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcbatchschedule_master", mintBatchscheduleunkid, "batchscheduleunkid", 2, objDataOperation) Then
                            blnToInsert = True
                        End If

                    Else
                        objInterviewerTran._BatchScheduleUnkid = mintBatchscheduleunkid
                        objInterviewerTran._DataTable = dtInterviewerTran
                        blnFlag = objInterviewerTran.InsertUpdateDelete_InterviewerTran(dtCurrentDateAndTime, objDataOperation)
                        If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                        blnToInsert = False
                    End If
                End If
            End If
            'Hemant (27 Sep 2019) -- End

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 

            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcbatchschedule_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'Sandeep [ 09 Oct 2010 ] -- Start
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry you cannot delete this Batch. Reason : This Batch is linked with some transaction.")
            Return False
        End If
        'Sandeep [ 09 Oct 2010 ] -- End 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE rcbatchschedule_master SET " & _
                      "  isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
                    "WHERE batchscheduleunkid = @batchscheduleunkid "

            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "rcbatchschedule_master", "batchscheduleunkid", intUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            Dim blnFlag As Boolean

            blnFlag = objInterviewerTran.VoidAll(intUnkid, True, 1, CDate(mdtVoiddatetime), mstrVoidreason, objDataOperation)
            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            'Hemant (27 Sep 2019) -- End

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Cancel Database Table (rcbatchschedule_master) </purpose>
    Public Function Active_Cancel(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 

        Try

            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancel_Userunkid.ToString)
            If mdtCancel_Date = Nothing Then
                objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Date)
            End If

            objDataOperation.AddParameter("@cancelremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancel_Remark.ToString)

            strQ = "UPDATE rcbatchschedule_master SET " & _
                    "  iscancel=@iscancel " & _
                    ", cancel_userunkid = @canceluserunkid " & _
                    ", cancel_remark = @cancelremark " & _
                    ", cancel_date = @canceldatetime " & _
                    "WHERE batchscheduleunkid = @batchscheduleunkid "

            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "rcbatchschedule_master", mintBatchscheduleunkid, "batchscheduleunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "rcbatchschedule_master", "batchscheduleunkid", mintBatchscheduleunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 09 Oct 2010 ] -- Start
            'strQ = "<Query>"
            strQ = "SELECT batchscheduleunkid FROM rcapplicant_batchschedule_tran WHERE batchscheduleunkid = @batchscheduleunkid "
            'Sandeep [ 09 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrMessage = ""

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  batchscheduleunkid " & _
              ", vacancyunkid " & _
              ", batchcode " & _
              ", batchname " & _
              ", interviewdate " & _
              ", interviewtime " & _
              ", interviewtypeunkid " & _
              ", resultgroupunkid " & _
              ", location " & _
              ", description " & _
              ", cancel_remark " & _
              ", cancel_date " & _
              ", isclosed " & _
              ", iscancel " & _
              ", cancel_userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM rcbatchschedule_master " & _
             "WHERE isvoid=0 AND iscancel=0 "

            If strCode.Length > 0 Then
                strQ &= "AND batchcode = @code "
            End If
            If strName.Length > 0 Then
                strQ &= "AND batchname = @name"
            End If

            If intUnkid > 0 Then
                strQ &= " AND batchscheduleunkid <> @batchscheduleunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal strBatchunkid As String = "", Optional ByVal intVacancyukid As Integer = -1) As DataSet
    Public Function getListForCombo(ByVal dtDatabase_Start_Date As Date, Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal strBatchunkid As String = "", Optional ByVal intVacancyukid As Integer = -1) As DataSet
        'Shani(24-Aug-2015) -- End

        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If blnFlag = True Then
                strQ = "SELECT 0 as id, ' ' +  @name as name,'' as vacancyid, @FinstartDate as interviewdate  UNION "

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objDataOperation.AddParameter("@FinstartDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date))
                objDataOperation.AddParameter("@FinstartDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDatabase_Start_Date))
                'Shani(24-Aug-2015) -- End

            End If
            strQ &= "SELECT batchscheduleunkid as id, batchname as name,vacancyunkid as vacancyid,convert(char(8),interviewdate,112) as interviewdate  FROM rcbatchschedule_master WHERE ISNULL(isvoid,0) = 0 AND ISNULL(iscancel,0)=0 AND ISNULL(isclosed,0) = 0 "

            If strBatchunkid <> "" And intVacancyukid > 0 Then
                strQ &= " AND batchscheduleunkid NOT IN (" & strBatchunkid & ") AND vacancyunkid = @vacancyunkid "
                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyukid)
            End If


            strQ &= " ORDER BY name  "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    
    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes

    Public Function GetApplicantFromVacancy(ByVal intVacancyId As Integer, ByVal intApplicantId As Integer) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mstrBatchId As String = ""
        Dim exForce As Exception = Nothing
        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  applicantunkid " & _
                      " , vacancyunkid " & _
                      " FROM rcbatchschedule_master " & _
                      " JOIN rcapplicant_batchschedule_tran ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                      " AND rcapplicant_batchschedule_tran.isvoid = 0 AND rcapplicant_batchschedule_tran.applicantunkid = @applicantunkid " & _
                      " WHERE vacancyunkid = @vacancyunkid AND rcbatchschedule_master.isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intVacancyId)
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intApplicantId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing Then
                dtTable = dsList.Tables(0)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantFromVacancy", mstrModuleName)
        End Try
        Return dtTable
    End Function

    'Pinkal (12-May-2013) -- End

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
    Public Function Set_Notification_Interviewers(ByVal xDatabaseName As String, _
                                                 ByVal xUserId As Integer, _
                                                 ByVal xYearUnkId As Integer, _
                                                 ByVal xCompanyUnkid As Integer, _
                                                 ByVal intInterviewerId As Integer, _
                                                 ByVal xPeriodEnd As Date, _
                                                 ByVal strEmailContent As String, _
                                                 ByVal dtRow() As DataRow, _
                                                 Optional ByVal iLoginTypeId As Integer = 0, _
                                                 Optional ByVal iLoginEmployeeId As Integer = 0, _
                                                 Optional ByVal iUserId As Integer = 0, _
                                                 Optional ByVal blnIsSendMail As Boolean = True, _
                                                 Optional ByRef lstEmailList As List(Of clsEmailCollection) = Nothing _
                                                 )
        Dim objMail As New clsSendMail
        Dim strSubject As String = ""
        objEmailList = New List(Of clsEmailCollection)

        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master

        Try

            strSubject = Language.getMessage(mstrModuleName, 5, "Notification for Batch Interview Schedule")


            objMail._Subject = strSubject

            objEmp._Employeeunkid(xPeriodEnd) = CInt(intInterviewerId)

            objMail._Message = strEmailContent

            objMail._ToEmail = objEmp._Email
            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFormName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            objMail._SenderAddress = objEmp._Email
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT


            If iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE Then

                objEmp._Employeeunkid(xPeriodEnd) = iLoginEmployeeId
                Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                                                           mstrWebClientIP, mstrWebHostName, 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, _
                                                           IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email))

                objEmailList.Add(objEmailColl)

                objEmp = Nothing

            Else
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                                                           mstrWebClientIP, mstrWebHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, _
                                                           IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                objEmailList.Add(objEmailColl)

                objUser = Nothing

            End If


            If blnIsSendMail = True Then

                If objEmailList.Count > 0 Then
                    If HttpContext.Current Is Nothing Then
                        objThread = New Thread(AddressOf Send_Notification)
                        objThread.IsBackground = True
                        Dim arr(1) As Object
                        arr(0) = xCompanyUnkid
                        objThread.Start(arr)
                    Else
                        Call Send_Notification(xCompanyUnkid)
                    End If
                End If
            Else
                lstEmailList = objEmailList
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try

    End Function

    Private Sub Send_Notification(ByVal intCompanyUnkid As Object)
        Try
            If objEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In objEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId


                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyUnkid Is Integer Then
                        intCUnkId = intCompanyUnkid
                    Else
                        intCUnkId = CInt(intCompanyUnkid(0))
                    End If
                    If objSendMail.SendMail(intCUnkId).ToString.Length > 0 Then

                        Continue For
                    End If
                Next
                objEmailList.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'Hemant (07 Oct 2019) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Name is already defined. Please define new Name.")
			Language.setMessage(mstrModuleName, 2, "This Batch Code is already defined. Please define new Batch Code.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry you cannot delete this Batch. Reason : This Batch is linked with some transaction.")
			Language.setMessage(mstrModuleName, 5, "Notification for Batch Interview Schedule")
			Language.setMessage(mstrModuleName, 6, "Dear")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

