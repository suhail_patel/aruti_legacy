﻿'************************************************************************************************************************************
'Class Name : clsInteviewAnalysis_tran.vb
'Purpose    :
'Date       : 07/09/2010
'Written By : Anjan
'Modified   :
'************************************************************************************************************************************


Imports eZeeCommonLib
Imports Aruti.Data
Public Class clsInteviewAnalysis_tran

    Private Const mstrModuleName = "clsInteviewAnalysis_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAnalysisunkid As Integer
    Private mdtTran As DataTable
    Private mdtAnalysis_Date As Date
    
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _AnalysisUnkid() As Integer
        Get
            Return mintAnalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisunkid = value
            Call GetInterviewerAnalysis_tran()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DataTable
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
    '''' <summary>
    '''' Purpose: Get or Set analysis_date
    '''' Modify By: Anjan
    '''' </summary>
    Public Property _Analysis_Date() As Date
        Get
            Return mdtAnalysis_Date
        End Get
        Set(ByVal value As Date)
            mdtAnalysis_Date = value
        End Set
    End Property

    'Hemant (03 Jun 2020) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
    Private mintInterviewerTranUnkid As Integer
    Public Property _InterviewerTranUnkid() As Integer
        Get
            Return mintInterviewerTranUnkid
        End Get
        Set(ByVal value As Integer)
            mintInterviewerTranUnkid = value
        End Set
    End Property

    Private mintResultcodeUnkid As Integer
    Public Property _ResultcodeUnkid() As Integer
        Get
            Return mintResultcodeUnkid
        End Get
        Set(ByVal value As Integer)
            mintResultcodeUnkid = value
        End Set
    End Property

    Private mstrRemark As String
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Private mblnIsvoid As Boolean
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Private mintVoiduserunkid As Integer
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Private mdtVoiddatetime As Date
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Private mstrVoidreason As String = String.Empty
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property
    'Hemant (03 Jun 2020) -- End

#End Region


#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("")
        Dim dCol As DataColumn


        Try

            dCol = New DataColumn("analysistranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("analysisunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("interviewertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("analysis_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("resultcodeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("iscomplete")
            'dCol.DataType = System.Type.GetType("System.Boolean")
            'dCol.DefaultValue = False
            'mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("iseligible")
            'dCol.DataType = System.Type.GetType("System.Boolean")
            'dCol.DefaultValue = False
            'mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try

    End Sub
#End Region

    Public Sub GetInterviewerAnalysis_tran()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim dRowID_Tran As DataRow
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        Try
            strQ = "SELECT  analysistranunkid " & _
                     ",analysisunkid " & _
                     ",interviewertranunkid " & _
                     ",analysis_date " & _
                     ",resultcodeunkid " & _
                     ",remark " & _
                     ",isvoid " & _
                     ",voiduserunkid " & _
                     ",voiddatetime " & _
                     ",voidreason " & _
                     ",'' AUD " & _
             "FROM rcinterviewanalysis_tran " & _
             "WHERE analysisunkid=@analysisunkid " & _
             "AND ISNULL(isvoid,0)=0 "

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow
                    dRowID_Tran.Item("analysistranunkid") = .Item("analysistranunkid").ToString
                    dRowID_Tran.Item("analysisunkid") = .Item("analysisunkid").ToString
                    dRowID_Tran.Item("interviewertranunkid") = .Item("interviewertranunkid").ToString
                    dRowID_Tran.Item("analysis_date") = .Item("analysis_date")
                    dRowID_Tran.Item("resultcodeunkid") = .Item("resultcodeunkid").ToString
                    dRowID_Tran.Item("remark") = .Item("remark").ToString
                    dRowID_Tran.Item("isvoid") = .Item("isvoid").ToString
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid").ToString
                    dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    dRowID_Tran.Item("voidreason") = .Item("voidreason").ToString
                    dRowID_Tran.Item("AUD") = .Item("AUD")
                    mdtTran.Rows.Add(dRowID_Tran)
                End With

            Next


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetInterviewerAnalysis_tran", mstrModuleName)
        End Try

    End Sub



    Public Function InsertUpdateDelete_AnalysisTran(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        Try
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()


            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case (.Item("AUD"))
                            Case "A"
                                strQ = "INSERT INTO rcinterviewanalysis_tran ( " & _
                                             "  analysisunkid " & _
                                             ", interviewertranunkid " & _
                                             ", analysis_date " & _
                                             ", resultcodeunkid " & _
                                             ", remark " & _
                                             ", isvoid " & _
                                             ", voiduserunkid " & _
                                             ", voiddatetime " & _
                                             ", voidreason " & _
                                            ") VALUES (" & _
                                             "  @analysisunkid " & _
                                             ", @interviewertranunkid " & _
                                             ", @analysisdate " & _
                                             ", @resultcodeunkid " & _
                                             ", @remark " & _
                                             ", @isvoid " & _
                                             ", @voiduserunkid " & _
                                             ", @voiddatetime " & _
                                             ", @voidreason " & _
                                            "); SELECT @@identity"


                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid)
                                objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewertranunkid"))
                                objDataOperation.AddParameter("@analysisdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("analysis_date"))
                                objDataOperation.AddParameter("@resultcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultcodeunkid"))
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                'objDataOperation.AddParameter("@iseligible", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iseligible"))
                                'objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iscomplete"))


                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason").ToString)

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim mintAnalysistranunkid As Integer = dsList.Tables(0).Rows(0).Item(0)


                                'Pinkal (24-Jul-2012) -- Start
                                'Enhancement : TRA Changes

                                If .Item("analysisunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", .Item("analysisunkid"), "rcinterviewanalysis_tran", "analysistranunkid", mintAnalysistranunkid, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", mintAnalysistranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                'Pinkal (24-Jul-2012) -- End


                            Case "U"

                                strQ = "UPDATE rcinterviewanalysis_tran SET " & _
                                              "  analysisunkid = @analysisunkid " & _
                                              ", interviewertranunkid = @interviewertranunkid " & _
                                              ", analysis_date = @analysisdate " & _
                                              ", resultcodeunkid = @resultcodeunkid " & _
                                              ", remark = @remark " & _
                                              ", isvoid = @isvoid " & _
                                              ", voiduserunkid = @voiduserunkid " & _
                                              ", voiddatetime = @voiddatetime " & _
                                              ", voidreason = @voidreason " & _
                                         "WHERE analysistranunkid = @analysistranunkid "



                                objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewertranunkid"))
                                objDataOperation.AddParameter("@resultcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultcodeunkid"))
                                objDataOperation.AddParameter("@analysisdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("analysis_date").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
                                'objDataOperation.AddParameter("@iseligible", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iseligible"))
                                'objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iscomplete"))
                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)

                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason").ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Pinkal (24-Jul-2012) -- Start
                                'Enhancement : TRA Changes

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", CInt(.Item("analysistranunkid").ToString), 2, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Pinkal (24-Jul-2012) -- End



                            Case "D"




                                'Pinkal (24-Jul-2012) -- Start
                                'Enhancement : TRA Changes


                                'START FOR GET LEAVE APPROVER TRAN UNK ID

                                strQ = "Select isnull(analysistranunkid,0) as analysistranunkid From rcinterviewanalysis_tran " & _
                                       " WHERE analysistranunkid = @analysistranunkid AND interviewertranunkid = @interviewertranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid)
                                objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewertranunkid").ToString)
                                Dim dscount As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                Dim mintAnalysistranunkid As Integer = 0
                                If dscount.Tables("List").Rows.Count > 0 Then
                                    mintAnalysistranunkid = CInt(dscount.Tables("List").Rows(0)("analysistranunkid"))
                                End If


                                'END FOR GET LEAVE APPROVER TRAN UNK ID


                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", mintAnalysistranunkid, 2, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Hemant (03 Jun 2020) -- Start
                                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
                                'strQ = "DELETE FROM rcinterviewanalysis_tran " & _
                                '        "WHERE analysistranunkid = @analysistranunkid "
                                strQ = "UPDATE rcinterviewanalysis_tran SET " & _
                                                 "  isvoid = 1 " & _
                                                 ", voiduserunkid = @voiduserunkid" & _
                                                 ", voiddatetime =getdate() " & _
                                                 ", voidreason = @voidreason " & _
                                        "WHERE analysistranunkid = @analysistranunkid "

                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                'Hemant (03 Jun 2020) -- End
                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_AnalysisTran", mstrModuleName)
        End Try

    End Function

'Hemant (03 Jun 2020) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
    Public Function InsertUpdateDeleteAll_AnalysisTran(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        Try

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)

                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case (.Item("AUD"))
                            Case "A"

                                objDataOperation.ClearParameters()

                                strQ = "INSERT INTO rcinterviewanalysis_tran ( " & _
                                             "  analysisunkid " & _
                                             ", interviewertranunkid " & _
                                             ", analysis_date " & _
                                             ", resultcodeunkid " & _
                                             ", remark " & _
                                             ", isvoid " & _
                                             ", voiduserunkid " & _
                                             ", voiddatetime " & _
                                             ", voidreason " & _
                                            ") VALUES (" & _
                                             "  @analysisunkid " & _
                                             ", @interviewertranunkid " & _
                                             ", @analysisdate " & _
                                             ", @resultcodeunkid " & _
                                             ", @remark " & _
                                             ", @isvoid " & _
                                             ", @voiduserunkid " & _
                                             ", @voiddatetime " & _
                                             ", @voidreason " & _
                                            "); SELECT @@identity"


                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid)
                                objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewertranunkid"))
                                objDataOperation.AddParameter("@analysisdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("analysis_date"))
                                objDataOperation.AddParameter("@resultcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultcodeunkid"))
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))


                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason").ToString)

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim mintAnalysistranunkid As Integer = dsList.Tables(0).Rows(0).Item(0)



                                If .Item("analysisunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", .Item("analysisunkid"), "rcinterviewanalysis_tran", "analysistranunkid", mintAnalysistranunkid, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", mintAnalysistranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If


                            Case "U"

                               

                                objDataOperation.ClearParameters()
                                strQ = "UPDATE rcinterviewanalysis_tran SET " & _
                                              "  analysisunkid = @analysisunkid " & _
                                              ", interviewertranunkid = @interviewertranunkid " & _
                                              ", analysis_date = @analysisdate " & _
                                              ", resultcodeunkid = @resultcodeunkid " & _
                                              ", remark = @remark " & _
                                              ", isvoid = @isvoid " & _
                                              ", voiduserunkid = @voiduserunkid " & _
                                              ", voiddatetime = @voiddatetime " & _
                                              ", voidreason = @voidreason " & _
                                         "WHERE analysistranunkid = @analysistranunkid "



                                objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewertranunkid"))
                                objDataOperation.AddParameter("@resultcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultcodeunkid"))
                                objDataOperation.AddParameter("@analysisdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("analysis_date").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)

                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason").ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", CInt(.Item("analysistranunkid").ToString), 2, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If




                            Case "D"


                                objDataOperation.ClearParameters()

                                strQ = "Select isnull(analysistranunkid,0) as analysistranunkid From rcinterviewanalysis_tran " & _
                                       " WHERE analysistranunkid = @analysistranunkid AND interviewertranunkid = @interviewertranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid)
                                objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewertranunkid").ToString)
                                Dim dscount As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                Dim mintAnalysistranunkid As Integer = 0
                                If dscount.Tables("List").Rows.Count > 0 Then
                                    mintAnalysistranunkid = CInt(dscount.Tables("List").Rows(0)("analysistranunkid"))
                                End If




                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", mintAnalysistranunkid, 2, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                strQ = "UPDATE rcinterviewanalysis_tran SET " & _
                                              "  isvoid = 1 " & _
                                              ", voiduserunkid = @voiduserunkid" & _
                                              ", voiddatetime =getdate() " & _
                                              ", voidreason = @voidreason " & _
                                            "WHERE analysistranunkid = @analysistranunkid "

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDeleteAll_AnalysisTran", mstrModuleName)
        End Try

    End Function
    'Hemant (03 Jun 2020) -- End

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcinterviewanalysis_tran) </purpose>
    Public Function VoidAll(ByVal objDataOperation As clsDataOperation, ByVal intUnkid As Integer, ByVal blnIsVoid As Boolean, ByVal intVoiduserunkid As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            strQ = "Select isnull(analysistranunkid,0) as analysistranunkid From rcinterviewanalysis_tran  WHERE analysisunkid = @analysisunkid  "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then


                'strQ = "UPDATE rcinterviewanalysis_tran SET " & _
                '              ", isvoid = @isvoid" & _
                '              ", voiduserunkid = @voiduserunkid" & _
                '              ", voiddatetime = @voiddatetime" & _
                '              ", voidreason = @voidreason" & _
                '        "WHERE analysisunkid = @analysisunkid "

                'S.SANDEEP [ 14 May 2013 ] -- START
                'ENHANCEMENT : TRA ENHANCEMENT
                'strQ = "UPDATE rcinterviewanalysis_tran SET " & _
                '        ", isvoid = @isvoid" & _
                '        ", voiduserunkid = @voiduserunkid" & _
                '        ", voiddatetime = @voiddatetime" & _
                '        ", voidreason = @voidreason" & _
                '       "WHERE analysistranunkid = @analysistranunkid "

                strQ = "UPDATE rcinterviewanalysis_tran SET " & _
                           "  isvoid = @isvoid " & _
                              ", voiduserunkid = @voiduserunkid" & _
                              ", voiddatetime = @voiddatetime" & _
                              ", voidreason = @voidreason " & _
                      " WHERE analysistranunkid = @analysistranunkid "
                'S.SANDEEP [ 14 May 2013 ] -- END

                For Each dr As DataRow In dsList.Tables(0).Rows

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("analysistranunkid"))

                    'S.SANDEEP [ 14 May 2013 ] -- START
                    'ENHANCEMENT : TRA ENHANCEMENT
                    'objDataOperation.AddParameter("@voiddatetime", SqlDbType.SmallDateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                    'S.SANDEEP [ 14 May 2013 ] -- END

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                    'S.SANDEEP [ 14 May 2013 ] -- START
                    'ENHANCEMENT : TRA ENHANCEMENT
                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid)
                    'S.SANDEEP [ 14 May 2013 ] -- END
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsVoid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", intUnkid, "rcinterviewanalysis_tran", "analysistranunkid", CInt(dr("analysistranunkid")), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If

            'Pinkal (24-Jul-2012) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '''<summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcinterviewanalysis_tran) </purpose>
    Public Function Void_SingleAnalysis(ByVal intAnalysisBatchScheduleTranId As Integer, ByVal blnIsvoid As Boolean, ByVal intvoiduserunkid As Integer, _
                                               ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 21 APRIL 2011 ] -- Start
            objDataOperation.BindTransaction()
            strQ = "SELECT analysisunkid As AId FROM rcinterviewanalysis_tran WHERE analysistranunkid = @analysistranunkid  "

            objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisBatchScheduleTranId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sandeep [ 21 APRIL 2011 ] -- End 

            strQ = "UPDATE rcinterviewanalysis_tran SET " & _
                          "  isvoid = @isvoid " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voiddatetime = @voiddatetime " & _
                          ", voidreason = @voidreason " & _
                    "WHERE analysistranunkid = @analysistranunkid "


            'Sandeep [ 21 APRIL 2011 ] -- Start
            'objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisBatchScheduleTranId)
            'Sandeep [ 21 APRIL 2011 ] -- End 
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.SmallDateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intvoiduserunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsvoid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Sandeep [ 21 APRIL 2011 ] -- Start
            If dsList.Tables(0).Rows.Count > 0 Then
                If SetMasterEntryVoid(dsList.Tables(0).Rows(0)(0), blnIsvoid, intvoiduserunkid, dtVoidDateTime, strVoidReason, objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            'Sandeep [ 21 APRIL 2011 ] -- End 

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sandeep [ 21 APRIL 2011 ] -- Start
    Public Function SetMasterEntryVoid(ByVal intTranAnalysisId As Integer, ByVal blnIsvoid As Boolean, ByVal intvoiduserunkid As Integer, _
                                               ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            objDataOperation.ClearParameters()

            strQ = "SELECT analysisunkid As AId FROM rcinterviewanalysis_tran WHERE analysisunkid = @analysisunkid  And isvoid = 0 "

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranAnalysisId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables(0).Rows.Count = 0 Then
                strQ = "UPDATE rcinterviewanalysis_master SET " & _
                            "   isvoid = @isvoid" & _
                            "  ,voiddatetime = @voiddatetime" & _
                            "  ,voiduserunkid = @voiduserunkid" & _
                            "  ,voidreason = @voidreason " & _
                            " Where analysisunkid = @analysisunkid "

                objDataOperation.AddParameter("@voiddatetime", SqlDbType.SmallDateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intvoiduserunkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsvoid)

                'objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranAnalysisId.ToString)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "SetMasterEntryVoid", mstrModuleName)
            Return False
        End Try
    End Function
    'Sandeep [ 21 APRIL 2011 ] -- End 


End Class
