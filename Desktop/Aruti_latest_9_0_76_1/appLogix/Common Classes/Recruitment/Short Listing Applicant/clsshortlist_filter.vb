﻿'************************************************************************************************************************************
'Class Name : clsshortlist_filter.vb
'Purpose    :
'Date       :12/23/2011
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsshortlist_filter
    Private Shared ReadOnly mstrModuleName As String = "clsshortlist_filter"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintFilterunkid As Integer
    Private mintShortlistunkid As Integer
    Private mdtFilter As DataTable
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private objFinalApplicant As New clsshortlist_finalapplicant
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set filterunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Filterunkid() As Integer
        Get
            Return mintFilterunkid
        End Get
        Set(ByVal value As Integer)
            mintFilterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shortlistunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Shortlistunkid() As Integer
        Get
            Return mintShortlistunkid
        End Get
        Set(ByVal value As Integer)
            mintShortlistunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtFilter
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _DtFilter() As DataTable
        Get
            Return mdtFilter
        End Get
        Set(ByVal value As DataTable)
            mdtFilter = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        mdtFilter = New DataTable("ShortListFiler")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("filterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("shortlistunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("vacancyunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("vacancy")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("qlevel")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            'dCol = New DataColumn("qualificationgroupunkid")
            'dCol.DataType = System.Type.GetType("System.Int32")
            'mdtFilter.Columns.Add(dCol)
            dCol = New DataColumn("qualificationgroupunkid")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            'Hemant (30 Oct 2019) -- Start
            dCol = New DataColumn("qualificationgroup_condition")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("qualificationgroupcondition")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)
            'Hemant (30 Oct 2019) -- End

            dCol = New DataColumn("qlevel_condition")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("qlevelcondition")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            'Hemant (07 Oct 2019) -- End            

            dCol = New DataColumn("qualificationgrp")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("qualificationunkid")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("qualification")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("resultunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("resultcode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("result_level")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("result_lvl_condition")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("resultcondition")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes

            dCol = New DataColumn("other_qualificationgrp")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("other_qualification")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("other_resultcode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)


            'Pinkal (5-MAY-2012) -- End


            dCol = New DataColumn("gpacode")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("gpacode_string")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)



            dCol = New DataColumn("gpacode_condition")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("gpacondition")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("age")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("age_condition")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("agecondition")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            dCol = New DataColumn("award_year")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("year_condition")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("yearcondition")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            'Pinkal (06-Feb-2012) -- End


            dCol = New DataColumn("gender")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("gender_name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("skillcategoryunkid")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("skillcategory")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("skillunkid")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("skill")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)



            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes

            dCol = New DataColumn("other_skillcategory")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("other_skill")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            'Pinkal (5-MAY-2012) -- End

            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            dCol = New DataColumn("nationalityunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("nationality")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)
            'Hemant (02 Jul 2019) -- End

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
            dCol = New DataColumn("experience_days")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("experience")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("experience_condition")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("experiencecondition")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)
            'Hemant (07 Oct 2019) -- End

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtFilter.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFilter.Columns.Add(dCol)


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            dCol = New DataColumn("logicalcondition")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFilter.Columns.Add(dCol)

            'Pinkal (06-Feb-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intShortListunkid As Integer = -1, Optional ByVal intFilterunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            ' ", cfcommon_master.name + ' ( ' + CONVERT(CHAR(12),openingdate,103) +  ' - ' + CONVERT(CHAR(12),closingdate,103) + ' ) ' as vacancytitle " & _
            'Pinkal (16-Apr-2016) -- End


            strQ = "SELECT " & _
              "  filterunkid " & _
               ", rcshortlist_filter.shortlistunkid " & _
               ", rcshortlist_master.refno " & _
               ", cfcommon_master.name as vacancytitle " & _
               ", openingdate " & _
               ", closingdate " & _
               ", ISNULL(rcshortlist_filter.qlevel,0) AS qlevel " & _
               ", ISNULL(qualificationgroupunkid,0) AS qualificationgroupunkid  " & _
               ", '' AS 'qualificationgroup'  " & _
               ", ISNULL(qualificationunkid,'') qualificationunkid " & _
               ", '' AS 'Qualification' " & _
               ", ISNULL(rcshortlist_filter.resultunkid,0) AS resultunkid " & _
               ", ISNULL(resultname,'') AS 'resultname' " & _
               ", ISNULL(rcshortlist_filter.result_level,0) AS resultlevel " & _
              ", result_lvl_condition " & _
              ", '' AS resultcondition " & _
               ", gpacode " & _
               ", gpacode_condition " & _
               ", '' gpacodecondtion " & _
               ", ISNULL(other_qualificationgrp,'') other_qualificationgrp " & _
               ", ISNULL(other_qualification,'') other_qualification " & _
               ", ISNULL(other_resultcode,'') other_resultcode " & _
              ", age " & _
              ", age_condition " & _
               ", '' AS agecondition " & _
               ", ISNULL(award_year,0) award_year " & _
               ", ISNULL(award_year_condition,0) award_year_condition " & _
               ", '' AS award_yearcondition " & _
               ", gender " & _
               ", CASE WHEN gender = 1 THEN @Male " & _
               "           WHEN gender = 2 THEN @Female " & _
               " ELSE '' END as gender_name " & _
               ", rcshortlist_filter.skillcategoryunkid " & _
               ", '' AS 'Skill Category' " & _
              ", skillunkid " & _
               ", '' AS 'Skill' " & _
               ", ISNULL(other_skillcategory,'') other_skillcategory " & _
               ", ISNULL(other_skill,'') other_skill " & _
               ", rcshortlist_filter.userunkid " & _
               ", rcshortlist_filter.isvoid " & _
               ", rcshortlist_filter.voiduserunkid " & _
               ", rcshortlist_filter.voiddatetime " & _
               ", rcshortlist_filter.voidreason " & _
               ", ISNULL(nationalityunkid,0) AS nationalityunkid  " & _
               ", ISNULL(hrmsConfiguration..cfcountry_master.country_name, '') AS nationality  " & _
               ", ISNULL(qlevel_condition,0) qlevel_condition " & _
               ", '' AS qlevelcondition " & _
               ", experience_days " & _
               ", experience_condition " & _
               ", '' AS experiencecondition " & _
               ", qualificationgroup_condition " & _
               ", '' AS qualificationgroupcondition " & _
              ", ISNULL(logical_condition,0) logical_condition " & _
              ", '' AS 'logicalcondition' " & _
             " FROM rcshortlist_filter " & _
               " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid = rcshortlist_filter.shortlistunkid AND rcshortlist_master.isvoid = 0 " & _
               " JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcshortlist_master.vacancyunkid AND rcvacancy_master.isvoid = 0 " & _
               " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.isactive = 1 AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & _
               " /* LEFT JOIN cfcommon_master qgrp ON qgrp.masterunkid = rcshortlist_filter.qualificationgroupunkid AND qgrp.isactive = 1 AND qgrp.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " */ " & _
               " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcshortlist_filter.resultunkid AND hrresult_master.isactive = 1 " & _
               " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcshortlist_filter.nationalityunkid " & _
             " WHERE 1= 1"
            'Hemant (30 Oct 2019) -- [qualificationgroup_condition,qualificationgroupcondition]
            'Hemant (07 Oct 2019) -- [nationalityunkid,nationality,qlevel_condition,experience_days,experience_condition,experiencecondition,experiencecondition,ISNULL(qgrp.name, '') AS 'qualificationgroup' --> ,'' AS 'qualificationgroup', LEFT JOIN cfcommon_master qgrp ON qgrp.masterunkid = rcshortlist_filter.qualificationgroupunkid AND qgrp.isactive = 1 AND qgrp.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP --> /* LEFT JOIN cfcommon_master qgrp ON qgrp.masterunkid = rcshortlist_filter.qualificationgroupunkid AND qgrp.isactive = 1 AND qgrp.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " */ "]
            objDataOperation.ClearParameters()
            If intShortListunkid > 0 Then
                strQ &= " AND rcshortlist_filter.shortlistunkid = @shortlistunkid "
                objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShortListunkid)
            End If

            If intFilterunkid > 0 Then
                strQ &= " AND rcshortlist_filter.filterunkid = @filterunkid "
                objDataOperation.AddParameter("@filterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFilterunkid)
            End If

            If blnOnlyActive Then
                strQ &= " AND rcshortlist_filter.isvoid = 0 "
            End If
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim objQualification As New clsqualification_master
                Dim objSkill As New clsskill_master
                Dim objMaster As New clsMasterData
                Dim objCommonMaster As New clsCommon_Master 'Hemant (07 Oct 2019)
                'Nilay (10-Nov-2016) -- Start
                'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                'Dim dsCondition As DataSet = objMaster.GetCondition(False)
                Dim dsCondition As DataSet = objMaster.GetCondition(False, True, False, False, False)
                'Nilay (10-Nov-2016) -- End
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    Dim drRow() As DataRow = Nothing

                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    dsList.Tables(0).Rows(i)("vacancytitle") = dsList.Tables(0).Rows(i)("vacancytitle").ToString() & " ( " & CDate(dsList.Tables(0).Rows(i)("openingdate")).ToShortDateString() & " - " & CDate(dsList.Tables(0).Rows(i)("closingdate")).ToShortDateString() & " ) "
                    'Pinkal (16-Apr-2016) -- End

                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                    If dsList.Tables(0).Rows(i)("qualificationgroupunkid").ToString().Trim.Length > 0 AndAlso dsList.Tables(0).Rows(i)("qualificationgroupunkid").ToString() <> "0" Then
                        dsList.Tables(0).Rows(i)("qualificationgroup") = objCommonMaster.GetCommaSeparateCommonMasterData("name", dsList.Tables(0).Rows(i)("qualificationgroupunkid").ToString(), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)
                    End If
                    'Hemant (07 Oct 2019) -- End

                    If dsList.Tables(0).Rows(i)("qualificationunkid").ToString().Trim.Length > 0 Then
                        dsList.Tables(0).Rows(i)("qualification") = objQualification.GetQualification(dsList.Tables(0).Rows(i)("qualificationunkid").ToString())
                    End If

                    If dsList.Tables(0).Rows(i)("skillcategoryunkid").ToString().Trim.Length > 0 Then
                        dsList.Tables(0).Rows(i)("Skill Category") = objSkill.GetSkillCategory(dsList.Tables(0).Rows(i)("skillcategoryunkid").ToString())
                    End If

                    If dsList.Tables(0).Rows(i)("skillunkid").ToString().Trim.Length > 0 Then
                        dsList.Tables(0).Rows(i)("Skill") = objSkill.GetSkill(dsList.Tables(0).Rows(i)("skillunkid").ToString())
                    End If

                    If CInt(dsList.Tables(0).Rows(i)("result_lvl_condition")) > 0 Then
                        drRow = Nothing
                        drRow = dsCondition.Tables(0).Select("id = " & CInt(dsList.Tables(0).Rows(i)("result_lvl_condition")))
                        If drRow.Length > 0 Then

                            If dsList.Tables(0).Rows(i)("resultname").ToString().Trim.Length > 0 Then
                                dsList.Tables(0).Rows(i)("resultcondition") = drRow(0)("name").ToString() & " " & dsList.Tables(0).Rows(i)("resultname").ToString()
                            ElseIf dsList.Tables(0).Rows(i)("other_resultcode").ToString().Trim.Length > 0 Then
                                dsList.Tables(0).Rows(i)("resultcondition") = drRow(0)("name").ToString() & " " & dsList.Tables(0).Rows(i)("other_resultcode").ToString()
                            End If

                        End If
                    End If


                    If CInt(dsList.Tables(0).Rows(i)("gpacode_condition")) > 0 Then
                        drRow = Nothing
                        drRow = dsCondition.Tables(0).Select("id = " & CInt(dsList.Tables(0).Rows(i)("gpacode_condition")))
                        If drRow.Length > 0 Then
                            dsList.Tables(0).Rows(i)("gpacodecondtion") = drRow(0)("name").ToString() & " " & CDec(dsList.Tables(0).Rows(i)("gpacode").ToString()).ToString("#0.00")
                        End If
                    End If

                    If CInt(dsList.Tables(0).Rows(i)("age_condition")) > 0 Then
                        drRow = Nothing
                        drRow = dsCondition.Tables(0).Select("id = " & CInt(dsList.Tables(0).Rows(i)("age_condition")))
                        If drRow.Length > 0 Then
                            dsList.Tables(0).Rows(i)("agecondition") = drRow(0)("name").ToString() & " " & dsList.Tables(0).Rows(i)("age").ToString()
                        End If
                    End If

                    If CInt(dsList.Tables(0).Rows(i)("award_year_condition")) > 0 Then
                        drRow = Nothing
                        drRow = dsCondition.Tables(0).Select("id = " & CInt(dsList.Tables(0).Rows(i)("award_year_condition")))
                        If drRow.Length > 0 Then
                            dsList.Tables(0).Rows(i)("award_yearcondition") = drRow(0)("name").ToString() & " " & dsList.Tables(0).Rows(i)("award_year").ToString()
                        End If
                    End If


                    If CInt(dsList.Tables(0).Rows(i)("logical_condition")) > 0 Then

                        If CInt(dsList.Tables(0).Rows(i)("logical_condition")) = 1 Then
                            dsList.Tables(0).Rows(i)("logicalcondition") = Language.getMessage(mstrModuleName, 2, "AND")
                        ElseIf CInt(dsList.Tables(0).Rows(i)("logical_condition")) = 2 Then
                            dsList.Tables(0).Rows(i)("logicalcondition") = Language.getMessage(mstrModuleName, 3, "OR")
                        End If

                    End If

                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
                    If CInt(dsList.Tables(0).Rows(i)("qlevel_condition")) > 0 Then
                        drRow = Nothing
                        drRow = dsCondition.Tables(0).Select("id = " & CInt(dsList.Tables(0).Rows(i)("qlevel_condition")))
                        If drRow.Length > 0 Then
                            dsList.Tables(0).Rows(i)("qlevelcondition") = drRow(0)("name").ToString() & " " & dsList.Tables(0).Rows(i)("qlevel").ToString()
                        End If
                    End If

                    If CInt(dsList.Tables(0).Rows(i)("experience_condition")) > 0 Then
                        drRow = Nothing
                        drRow = dsCondition.Tables(0).Select("id = " & CInt(dsList.Tables(0).Rows(i)("experience_condition")))
                        If drRow.Length > 0 Then
                            Dim intYears As Integer = CInt(Math.Floor(CInt(dsList.Tables(0).Rows(i)("experience_days")) / 365))
                            Dim intMonths As Integer = CInt((CInt(dsList.Tables(0).Rows(i)("experience_days")) - (intYears * 365)) / 30)
                            dsList.Tables(0).Rows(i)("experiencecondition") = drRow(0)("name").ToString() & " " & IIf(intYears > 0, intYears.ToString() & " Years ", "").ToString & IIf(intMonths > 0, intMonths.ToString & " Months", "")
                        End If
                    End If
                    'Hemant (07 Oct 2019) -- End

                    'Hemant (30 Oct 2019) -- Start
                    If CInt(dsList.Tables(0).Rows(i)("qualificationgroup_condition")) > 0 Then
                        drRow = Nothing
                        drRow = dsCondition.Tables(0).Select("id = " & CInt(dsList.Tables(0).Rows(i)("qualificationgroup_condition")))
                        If drRow.Length > 0 Then
                            dsList.Tables(0).Rows(i)("qualificationgroupcondition") = drRow(0)("name").ToString() & " " & dsList.Tables(0).Rows(i)("qualificationgroup").ToString()
                        End If
                    Else
                        dsList.Tables(0).Rows(i)("qualificationgroupcondition") = dsList.Tables(0).Rows(i)("qualificationgroup").ToString()
                    End If
                    'Hemant (30 Oct 2019) -- End


                Next

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcshortlist_filter) </purpose>
    Public Function InsertDelete_Filter(ByVal objDataOperation As clsDataOperation, ByVal DtApplicant As DataTable) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim mstrGUID As String = ""
        Try

            If mdtFilter Is Nothing Then Return True

            For i = 0 To mdtFilter.Rows.Count - 1

                With mdtFilter.Rows(i)

                    objDataOperation.ClearParameters()

                    If Not IsDBNull(.Item("AUD")) Then

                        Select Case .Item("AUD")

                            Case "A"


                                'Pinkal (5-MAY-2012) -- Start
                                'Enhancement : TRA Changes


                                'strQ = "INSERT INTO rcshortlist_filter ( " & _
                                '         "  shortlistunkid " & _
                                '         ", qlevel " & _
                                '         ", qualificationgroupunkid " & _
                                '         ", qualificationunkid " & _
                                '         ", resultunkid " & _
                                '         ", result_lvl_condition " & _
                                '         ", gpacode " & _
                                '         ", gpacode_condition " & _
                                '         ", age " & _
                                '         ", age_condition " & _
                                '         ", gender " & _
                                '         ", skillcategoryunkid " & _
                                '         ", skillunkid " & _
                                '         ", award_year " & _
                                '         ", award_year_condition " & _
                                '         ", logical_condition " & _
                                '         ", userunkid " & _
                                '         ", isvoid " & _
                                '         ", voiduserunkid " & _
                                '         ", voiddatetime " & _
                                '         ", voidreason" & _
                                '       ") VALUES (" & _
                                '         "  @shortlistunkid " & _
                                '         ", @qlevel " & _
                                '         ", @qualificationgroupunkid " & _
                                '         ", @qualificationunkid " & _
                                '         ", @resultunkid " & _
                                '         ", @result_lvl_condition " & _
                                '         ", @gpacode " & _
                                '         ", @gpacode_condition " & _
                                '         ", @age " & _
                                '         ", @age_condition " & _
                                '         ", @gender " & _
                                '         ", @skillcategoryunkid " & _
                                '         ", @skillunkid " & _
                                '         ", @award_year " & _
                                '         ", @award_year_condition " & _
                                '         ", @logical_condition " & _
                                '         ", @userunkid " & _
                                '         ", @isvoid " & _
                                '         ", @voiduserunkid " & _
                                '         ", @voiddatetime " & _
                                '         ", @voidreason" & _
                                '       "); SELECT @@identity"


                                strQ = "INSERT INTO rcshortlist_filter ( " & _
                                         "  shortlistunkid " & _
                                         ", qlevel " & _
                                         ", qualificationgroupunkid " & _
                                         ", qualificationunkid " & _
                                         ", resultunkid " & _
                                         ", result_lvl_condition " & _
                                         ", gpacode " & _
                                         ", gpacode_condition " & _
                                         ", age " & _
                                         ", age_condition " & _
                                         ", gender " & _
                                         ", skillcategoryunkid " & _
                                         ", skillunkid " & _
                                         ", award_year " & _
                                         ", award_year_condition " & _
                                         ", logical_condition " & _
                                        ", other_qualificationgrp " & _
                                        ", other_qualification " & _
                                        ", other_resultcode " & _
                                        ", other_skillcategory " & _
                                        ", other_skill " & _
                                         ", userunkid " & _
                                         ", isvoid " & _
                                         ", voiduserunkid " & _
                                         ", voiddatetime " & _
                                         ", voidreason" & _
                                         ", nationalityunkid " & _
                                         ", qlevel_condition " & _
                                         ", experience_days " & _
                                         ", experience_condition " & _
                                         ", qualificationgroup_condition " & _
                                       ") VALUES (" & _
                                         "  @shortlistunkid " & _
                                         ", @qlevel " & _
                                         ", @qualificationgroupunkid " & _
                                         ", @qualificationunkid " & _
                                         ", @resultunkid " & _
                                         ", @result_lvl_condition " & _
                                         ", @gpacode " & _
                                         ", @gpacode_condition " & _
                                         ", @age " & _
                                         ", @age_condition " & _
                                         ", @gender " & _
                                         ", @skillcategoryunkid " & _
                                         ", @skillunkid " & _
                                         ", @award_year " & _
                                         ", @award_year_condition " & _
                                         ", @logical_condition " & _
                                        ", @other_qualificationgrp " & _
                                        ", @other_qualification " & _
                                        ", @other_resultcode " & _
                                        ", @other_skillcategory " & _
                                        ", @other_skill " & _
                                         ", @userunkid " & _
                                         ", @isvoid " & _
                                         ", @voiduserunkid " & _
                                         ", @voiddatetime " & _
                                         ", @voidreason" & _
                                         ", @nationalityunkid " & _
                                         ", @qlevel_condition " & _
                                         ", @experience_days " & _
                                         ", @experience_condition " & _
                                         ", @qualificationgroup_condition " & _
                                       "); SELECT @@identity"

                                'Hemant (30 Oct 2019) -- [qualificationgroup_condition]
                                'Hemant (07 Oct 2019) --[nationalityunkid,qlevel_condition,experience_days,experience_condition]
                                'Pinkal (5-MAY-2012) -- End


                                objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortlistunkid)


                                'Pinkal (5-MAY-2012) -- Start
                                'Enhancement : TRA Changes
                                'Hemant (30 Oct 2019) -- Start
                                'If Not IsDBNull(.Item("qlevel")) Then
                                '    objDataOperation.AddParameter("@qlevel", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qlevel").ToString)
                                'Else
                                '    objDataOperation.AddParameter("@qlevel", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
                                'End If
                                If Not IsDBNull(.Item("qlevel")) Then
                                    objDataOperation.AddParameter("@qlevel", SqlDbType.NVarChar, 4000, .Item("qlevel").ToString)
                                Else
                                    objDataOperation.AddParameter("@qlevel", SqlDbType.NVarChar, 4000, DBNull.Value)
                                End If
                                'Hemant (30 Oct 2019) -- End

                                'Pinkal (5-MAY-2012) -- End

                                'Hemant (07 Oct 2019) -- Start
                                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                                'objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationgroupunkid").ToString)
                                objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.NVarChar, 4000, .Item("qualificationgroupunkid").ToString)
                                'Hemant (07 Oct 2019) -- End

                                'Pinkal (12-Oct-2011) -- Start
                                'Enhancement : TRA Changes

                                'objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationunkid").ToString)
                                objDataOperation.AddParameter("@qualificationunkid", SqlDbType.NVarChar, 4000, .Item("qualificationunkid").ToString)

                                'Pinkal (12-Oct-2011) -- End


                                'Pinkal (06-Feb-2012) -- Start
                                'Enhancement : TRA Changes
                                objDataOperation.AddParameter("@award_year", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("award_year").ToString)
                                objDataOperation.AddParameter("@award_year_condition", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("year_condition").ToString)
                                objDataOperation.AddParameter("@logical_condition", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("logicalcondition").ToString) 
                                'Pinkal (06-Feb-2012) -- End


                                'Pinkal (5-MAY-2012) -- Start
                                'Enhancement : TRA Changes
                                objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_qualificationgrp").ToString)
                                objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_qualification").ToString)
                                objDataOperation.AddParameter("@other_resultcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_resultcode").ToString)
                                objDataOperation.AddParameter("@other_skillcategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_skillcategory").ToString)
                                objDataOperation.AddParameter("@other_skill", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_skill").ToString)
                                'Pinkal (5-MAY-2012) -- End



                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@result_lvl_condition", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("result_lvl_condition").ToString)
                                objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("gpacode").ToString)
                                objDataOperation.AddParameter("@gpacode_condition", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("gpacode_condition").ToString)
                                objDataOperation.AddParameter("@age", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("age").ToString)
                                objDataOperation.AddParameter("@age_condition", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("age_condition").ToString)
                                objDataOperation.AddParameter("@gender", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("gender").ToString)
                                objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, .Item("skillcategoryunkid").ToString)
                                objDataOperation.AddParameter("@skillunkid", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, .Item("skillunkid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                'Hemant (07 Oct 2019) -- Start
                                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
                                objDataOperation.AddParameter("@nationalityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("nationalityunkid").ToString)
                                objDataOperation.AddParameter("@qlevel_condition", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qlevel_condition").ToString)
                                objDataOperation.AddParameter("@experience_days", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("experience_days").ToString)
                                objDataOperation.AddParameter("@experience_condition", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("experience_condition").ToString)
                                'Hemant (07 Oct 2019) -- End

                                'Hemant (30 Oct 2019) -- Start
                                objDataOperation.AddParameter("@qualificationgroup_condition", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationgroup_condition").ToString)
                                'Hemant (30 Oct 2019) -- End

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintFilterunkid = dsList.Tables(0).Rows(0).Item(0)


                                If .Item("filterunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcshortlist_master", "shortlistunkid", .Item("shortlistunkid"), "rcshortlist_filter", "filterunkid", mintFilterunkid, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcshortlist_master", "shortlistunkid", mintShortlistunkid, "rcshortlist_filter", "filterunkid", mintFilterunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                objFinalApplicant._ShortListUnkid = mintShortlistunkid
                                objFinalApplicant._FilterUnkid = mintFilterunkid
                                objFinalApplicant._DtApplicant = New DataView(DtApplicant, "GUID  ='" & .Item("GUID").ToString() & "'", "", DataViewRowState.CurrentRows).ToTable
                                objFinalApplicant._Userunkid = mintUserunkid

                                If objFinalApplicant.Insert_ShortListedApplicant(objDataOperation) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Case "D"

                                '    strQ = " Update rcshortlist_filter set isvoid = @isvoid,voiduserunkid=@voiduserunkid,voiddatetime=@voiduserunkid,voidreason=@voidreason " & _
                                '              " WHERE filterunkid = @filterunkid AND shortlistunkid = @shortlistunkid"

                                '    objDataOperation.AddParameter("@filterunkid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mintFilterunkid.ToString)
                                '    objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mintShortlistunkid.ToString)
                                '    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                '    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                                '    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                '    objDataOperation.ExecNonQuery(strQ)

                                '    If objDataOperation.ErrorMessage <> "" Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete_Filter; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    '' <summary>
    '' Modify By: Pinkal Jariwala
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
    'Public Function GetORFilterApplicant(ByVal mstrFilter As String, ByVal mstrGUID As String, ByVal dtAsOnDate As Date) As DataSet 'Sohail (13 Mar 2020) - [dtAsOnDate]
    Public Function GetORFilterApplicant(ByVal xCompanyId As Integer, ByVal xDataBaseName As String, ByVal mstrFilter As String, ByVal mstrGUID As String, ByVal dtAsOnDate As Date) As DataSet
        'Pinkal (24-Sep-2020) -- End

        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()


            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            strQ = "SELECT datestranunkid " & _
                 ", employeeunkid " & _
                 ", effectivedate " & _
                 ", date1 " & _
                 ", date2 " & _
            "INTO #TableSusp " & _
            "FROM hremployee_dates_tran " & _
            "WHERE isvoid = 0 " & _
                  "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                  "AND 1 = CASE " & _
                              "WHEN date2 IS NULL THEN " & _
                                  "CASE " & _
                                      "WHEN CONVERT(CHAR(8), date1) <= @AsOnDate THEN " & _
                                          "1 " & _
                                      "ELSE " & _
                                          "0 " & _
                                  "END " & _
                              "ELSE " & _
                                  "CASE " & _
                                      "WHEN @AsOnDate " & _
                                           "BETWEEN date1 AND date2 THEN " & _
                                          "1 " & _
                                      "ELSE " & _
                                          "0 " & _
                                  "END " & _
                          "END "
            'Sohail (13 Mar 2020) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT DISTINCT applicantunkid,applicant_code,firstname,surname,applicantname,gender_name,birth_date " & _
            '           ",present_mobileno,email,GUID,'' qualificationgroup,qualificationunkid,qualificationname,gender,'' skillcategory,'' skill,employeecode, referenceno " & _
            '             "FROM   ( SELECT    rcapplicant_master.applicantunkid , " & _
            '                                "applicant_code " & _
            '                                ",ISNULL(firstname, '') firstname  " & _
            '                                ",ISNULL(surname, '') surname  " & _
            '                                ",ISNULL(firstname, '') + ' ' + ISNULL(surname, '') applicantname " & _
            '                                ",CASE WHEN gender = 1 THEN @Male " & _
            '                                     "WHEN gender = 2 THEN @Female " & _
            '                                "END gender_name  " & _
            '                                ",gender " & _
            '                                ",birth_date  " & _
            '                                ",present_mobileno  " & _
            '                                ",email  " & _
            '                                ",ISNULL(employeecode,'') employeecode" & _
            '                                ",ISNULL(referenceno,'') referenceno" & _
            '                                ",ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid  " & _
            '                                ",ISNULL(hrqualification_master.qualificationunkid,0) qualificationunkid " & _
            '                                ",ISNULL(hrqualification_master.qualificationname,'') qualificationname " & _
            '                                ",'" & mstrGUID & "' as GUID " & _
            '                                ",ISNULL(rcapplicant_master.vacancyunkid, 0) vacancyunkid " & _
            '                                ",ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
            '                                ",ISNULL(hrresult_master.resultunkid, 0) resultunkid  " & _
            '                                ",ISNULL(hrresult_master.result_level, 0) result_level  " & _
            '                                ",ISNULL(rcapplicantqualification_tran.gpacode, 0) gpacode  " & _
            '                                ",ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillcategoryunkid, 0) skillcategoryunkid  " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillunkid, 0) skillunkid " & _
            '                                " FROM rcapplicant_master " & _
            '                                " LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid " & _
            '                                " LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '                                " LEFT JOIN rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid " & _
            '                                " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & _
            '                                " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _            
            '                      "WHERE  rcapplicant_master.isimport = 0 " & _
            '                    ") AS a " & _
            '             "WHERE  1 = 1 "



            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT DISTINCT applicantunkid,applicant_code,firstname,surname,applicantname,gender_name,birth_date " & _
            '           ",present_mobileno,email,GUID,'' qualificationgroup,qualificationunkid,qualificationname,gender,gpacode,'' skillcategory,'' skill,employeecode, referenceno,award_start_date,award_end_date  " & _
            '           ",other_qualificationgrp,other_qualification,other_resultcode,other_skillcategory ,other_skill  " & _
            '             "FROM   ( SELECT    rcapplicant_master.applicantunkid , " & _
            '                                "applicant_code " & _
            '                                ",ISNULL(firstname, '') firstname  " & _
            '                                ",ISNULL(surname, '') surname  " & _
            '                                ",ISNULL(firstname, '') + ' ' + ISNULL(surname, '') applicantname " & _
            '                                ",CASE WHEN gender = 1 THEN @Male " & _
            '                                     "WHEN gender = 2 THEN @Female " & _
            '                                "END gender_name  " & _
            '                                ",gender " & _
            '                                ",birth_date  " & _
            '                                ",present_mobileno  " & _
            '                                ",email  " & _
            '                                ",ISNULL(employeecode,'') employeecode" & _
            '                                ",ISNULL(referenceno,'') referenceno" & _
            '                                ",ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid  " & _
            '                                ",ISNULL(hrqualification_master.qualificationunkid,0) qualificationunkid " & _
            '                                ",ISNULL(hrqualification_master.qualificationname,'') qualificationname " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_qualificationgrp, '') other_qualificationgrp " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_qualification, '') other_qualification " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_resultcode, '') other_resultcode " & _
            '                                ",ISNULL(rcapplicantskill_tran.other_skillcategory, '') other_skillcategory " & _
            '                                ",ISNULL(rcapplicantskill_tran.other_skill, '') other_skill " & _
            '                                ",'" & mstrGUID & "' as GUID " & _
            '                                ",ISNULL(Vac.vacancyunkid, 0) vacancyunkid  " & _
            '                                ",ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
            '                                ",ISNULL(hrresult_master.resultunkid, 0) resultunkid  " & _
            '                                ",ISNULL(hrresult_master.result_level, 0) result_level  " & _
            '                                ",'' gpacode  " & _
            '                                ",ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillcategoryunkid, 0) skillcategoryunkid  " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillunkid, 0) skillunkid " & _
            '                                ",rcapplicantqualification_tran.award_start_date " & _
            '                                ",rcapplicantqualification_tran.award_end_date " & _
            '                                " FROM rcapplicant_master " & _
            '                                " LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid " & _
            '                                " LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '                                " LEFT JOIN rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid " & _
            '                                " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & _
            '                                " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _
            '                                "LEFT JOIN " & _
            '                                 "( " & _
            '                                     "SELECT " & _
            '                                           "applicantunkid " & _
            '                                          ",CVac.vacancyunkid " & _
            '                                          ",cfcommon_master.name AS vacancy " & _
            '                                          ",CONVERT(CHAR(8),openingdate,112) AS ODate " & _
            '                                          ",CONVERT(CHAR(8),closingdate,112) AS CDate " & _
            '                                     "FROM " & _
            '                                     "( " & _
            '                                          "SELECT " & _
            '                                                "applicantunkid " & _
            '                                               ",vacancyunkid " & _
            '                                               ",ROW_NUMBER() OVER (PARTITION BY applicantunkid ORDER BY appvacancytranunkid DESC) AS Srno " & _
            '                                          "FROM rcapp_vacancy_mapping " & _
            '                                          "WHERE isactive = 1 " & _
            '                                     ") AS CVac " & _
            '                                     "JOIN rcvacancy_master ON CVac.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '                                     "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype ='" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
            '                                     "WHERE CVac.Srno = 1 " & _
            '                                 ") AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid " & _
            '                      "WHERE  rcapplicant_master.isimport = 0 " & _
            '                    ") AS a " & _
            '             "WHERE  1 = 1 "



            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.


            'strQ &= "SELECT DISTINCT applicantunkid,applicant_code,firstname,surname,applicantname,gender_name,birth_date " & _
            '           ",present_mobileno,email,GUID,'' qualificationgroup,qualificationunkid,qualificationname,gender,gpacode,'' skillcategory,'' skill,employeecode, referenceno,award_start_date,award_end_date  " & _
            '           ",other_qualificationgrp,other_qualification,other_resultcode,other_skillcategory ,other_skill,gpacode_string ,2 AS app_statusid " & _
            '           ",0 AS app_issent " & _
            '           ", nationalityunkid " & _
            '           ", nationality " & _
            '           ", experience_days " & _
            '           ", '' experience " & _
            '           "FROM   ( SELECT    rcapplicant_master.applicantunkid , " & _
            '                                "applicant_code " & _
            '                                ",ISNULL(rcapplicant_master.firstname, '') firstname  " & _
            '                                ",ISNULL(rcapplicant_master.surname, '') surname  " & _
            '                                ",ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') applicantname " & _
            '                                ",CASE WHEN rcapplicant_master.gender = 1 THEN @Male " & _
            '                                     "WHEN rcapplicant_master.gender = 2 THEN @Female " & _
            '                                "END gender_name  " & _
            '                                ",rcapplicant_master.gender " & _
            '                                ",rcapplicant_master.birth_date  " & _
            '                                ",rcapplicant_master.present_mobileno  " & _
            '                                ",rcapplicant_master.email  " & _
            '                                ",ISNULL(rcapplicant_master.employeecode,'') employeecode" & _
            '                                ", hremployee_master.employeeunkid " & _
            '                                ", #TableSusp.datestranunkid " & _
            '                                ",ISNULL(referenceno,'') referenceno" & _
            '                                ",ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid  " & _
            '                                ",ISNULL(hrqualification_master.qualificationunkid,0) qualificationunkid " & _
            '                                ",ISNULL(hrqualification_master.qualificationname,'') qualificationname " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_qualificationgrp, '') other_qualificationgrp " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_qualification, '') other_qualification " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_resultcode, '') other_resultcode " & _
            '                                ",ISNULL(rcapplicantskill_tran.other_skillcategory, '') other_skillcategory " & _
            '                                ",ISNULL(rcapplicantskill_tran.other_skill, '') other_skill " & _
            '                                ",'" & mstrGUID & "' as GUID " & _
            '                                ",ISNULL(rcapp_vacancy_mapping.vacancyunkid, 0) vacancyunkid  " & _
            '                                ",ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
            '                                ",ISNULL(hrresult_master.resultunkid, 0) resultunkid  " & _
            '                                ",ISNULL(hrresult_master.result_level, 0) result_level  " & _
            '                                ",rcapplicantqualification_tran.gpacode as gpacode  " & _
            '                                ",'' as gpacode_string  " & _
            '                                ",ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillcategoryunkid, 0) skillcategoryunkid  " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillunkid, 0) skillunkid " & _
            '                                ",rcapplicantqualification_tran.award_start_date " & _
            '                                ",rcapplicantqualification_tran.award_end_date " & _
            '                                ",ISNULL(rcapplicant_master.nationality,0) AS nationalityunkid " & _
            '                                ",hrmsConfiguration..cfcountry_master.country_name as nationality " & _
            '                                ",ISNULL(Experience_Days, 0) AS experience_days " & _
            '                                " FROM rcapplicant_master " & _
            '                                " LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
            '                                " LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '                                " LEFT JOIN rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid AND rcapplicantskill_tran.isvoid = 0 " & _
            '                                " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & _
            '                                " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _
            '                                " LEFT JOIN rcapp_vacancy_mapping ON rcapp_vacancy_mapping.applicantunkid = rcapplicant_master.applicantunkid AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
            '                                " JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '                                " JOIN cfcommon_master cm ON cm.masterunkid = rcvacancy_master.vacancytitle AND cm.mastertype ='" & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "' " & _
            '                                " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.nationality " & _
            '                                "LEFT JOIN hremployee_master ON hremployee_master.employeecode = rcapplicant_master.employeecode " & _
            '                                "LEFT JOIN #TableSusp ON hremployee_master.employeeunkid = #TableSusp.employeeunkid " & _
            '                                " LEFT JOIN ( SELECT " & _
            '                                                     "applicantunkid " & _
            '                                                   ",ISNULL(SUM(DATEDIFF(DAY, joiningdate, CASE " & _
            '                                                          "WHEN CONVERT(CHAR(8), terminationdate,112) = '19000101' THEN CONVERT(CHAR(8), GETDATE() ,112)  " & _
            '                                                          "WHEN  terminationdate IS NULL THEN CONVERT(CHAR(8), GETDATE(), 112) " & _
            '                                                          "ELSE terminationdate " & _
            '                                                     "END)), 0) AS Experience_Days " & _
            '                                                "FROM rcjobhistory " & _
            '                                                "WHERE isvoid = 0 " & _
            '                                                "AND applicantunkid > 0 " & _
            '                                                "GROUP BY applicantunkid ) AS JobExp on JobExp.applicantunkid = rcapplicant_master.applicantunkid " & _
            '                      "WHERE rcapp_vacancy_mapping.isimport = 0   " & _
            '                    ") AS a " & _
            '             "WHERE  1 = 1 "

            'Sohail (13 Mar 2020) - [LEFT JOIN hremployee_master, LEFT JOIN #TableSusp]
            'Hemant (07 Oct 2019) -- [nationality,experience_days,experience]
            'Hemant (02 Jul 2019) -- [nationalityunkid]
            'Sohail (09 Oct 2018) - [rcapp_vacancy_mapping.isactive = 1]=[ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]

            'Pinkal (09-May-2018) -- Short Listing applicant Count Issue.[ AND rcapplicantqualification_tran.isvoid = 0 AND rcapplicantskill_tran.isvoid = 0]


            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            '-> AND rcapplicantskill_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            '"WHERE  rcapplicant_master.isimport = 0 " & _ Removed
            '"WHERE  rcapp_vacancy_mapping.isimport = 0 " & _ Added
            'S.SANDEEP [ 14 May 2013 ] -- END


            'Pinkal (11-MAY-2012) -- End

            'If mstrFilter.Trim.Length > 0 Then
            '    strQ &= " AND " & mstrFilter
            'End If

            ''Sohail (13 Mar 2020) -- Start
            ''NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            'strQ &= " AND a.datestranunkid IS NULL "

            'strQ &= " DROP TABLE #TableSusp "
            ''Sohail (13 Mar 2020) -- End



            strQ &= "SELECT DISTINCT applicantunkid,applicant_code,firstname,surname,applicantname,gender_name,birth_date " & _
                       ",present_mobileno,email,GUID,'' qualificationgroup,qualificationunkid,qualificationname,gender,gpacode,'' skillcategory,'' skill,employeecode, referenceno,award_start_date,award_end_date  " & _
                       ",other_qualificationgrp,other_qualification,other_resultcode,other_skillcategory ,other_skill,gpacode_string ,2 AS app_statusid " & _
                       ",0 AS app_issent " & _
                       ", nationalityunkid " & _
                       ", nationality " & _
                       ", experience_days " & _
                       ", '' experience " & _
                    ", employeeunkid " & _
                         "FROM   ( SELECT    rcapplicant_master.applicantunkid , " & _
                                            "applicant_code " & _
                                            ",ISNULL(rcapplicant_master.firstname, '') firstname  " & _
                                            ",ISNULL(rcapplicant_master.surname, '') surname  " & _
                                            ",ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') applicantname " & _
                                            ",CASE WHEN rcapplicant_master.gender = 1 THEN @Male " & _
                                                 "WHEN rcapplicant_master.gender = 2 THEN @Female " & _
                                            "END gender_name  " & _
                                            ",rcapplicant_master.gender " & _
                                            ",rcapplicant_master.birth_date  " & _
                                            ",rcapplicant_master.present_mobileno  " & _
                                            ",rcapplicant_master.email  " & _
                                            ",ISNULL(rcapplicant_master.employeecode,'') employeecode" & _
                                         ", 0 AS employeeunkid " & _
                                         ", 0 AS datestranunkid " & _
                                            ",ISNULL(referenceno,'') referenceno" & _
                                            ",ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid  " & _
                                            ",ISNULL(hrqualification_master.qualificationunkid,0) qualificationunkid " & _
                                            ",ISNULL(hrqualification_master.qualificationname,'') qualificationname " & _
                                            ",ISNULL(rcapplicantqualification_tran.other_qualificationgrp, '') other_qualificationgrp " & _
                                            ",ISNULL(rcapplicantqualification_tran.other_qualification, '') other_qualification " & _
                                            ",ISNULL(rcapplicantqualification_tran.other_resultcode, '') other_resultcode " & _
                                            ",ISNULL(rcapplicantskill_tran.other_skillcategory, '') other_skillcategory " & _
                                            ",ISNULL(rcapplicantskill_tran.other_skill, '') other_skill " & _
                                            ",'" & mstrGUID & "' as GUID " & _
                                            ",ISNULL(rcapp_vacancy_mapping.vacancyunkid, 0) vacancyunkid  " & _
                                            ",ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
                                            ",ISNULL(hrresult_master.resultunkid, 0) resultunkid  " & _
                                            ",ISNULL(hrresult_master.result_level, 0) result_level  " & _
                                            ",rcapplicantqualification_tran.gpacode as gpacode  " & _
                                            ",'' as gpacode_string  " & _
                                            ",ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
                                            ",ISNULL(rcapplicantskill_tran.skillcategoryunkid, 0) skillcategoryunkid  " & _
                                            ",ISNULL(rcapplicantskill_tran.skillunkid, 0) skillunkid " & _
                                            ",rcapplicantqualification_tran.award_start_date " & _
                                            ",rcapplicantqualification_tran.award_end_date " & _
                                            ",ISNULL(rcapplicant_master.nationality,0) AS nationalityunkid " & _
                                            ",hrmsConfiguration..cfcountry_master.country_name as nationality " & _
                                            ",ISNULL(Experience_Days, 0) AS experience_days " & _
                                            " FROM rcapplicant_master " & _
                                            " LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
                                            " LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                                            " LEFT JOIN rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid AND rcapplicantskill_tran.isvoid = 0 " & _
                                            " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & _
                                            " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _
                                            " LEFT JOIN rcapp_vacancy_mapping ON rcapp_vacancy_mapping.applicantunkid = rcapplicant_master.applicantunkid AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
                                            " JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                                            " JOIN cfcommon_master cm ON cm.masterunkid = rcvacancy_master.vacancytitle AND cm.mastertype ='" & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "' " & _
                                            " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.nationality " & _
                                            " LEFT JOIN ( SELECT " & _
                                                                 "applicantunkid " & _
                                                               ",ISNULL(SUM(DATEDIFF(DAY, joiningdate, CASE " & _
                                                                      "WHEN CONVERT(CHAR(8), terminationdate,112) = '19000101' THEN CONVERT(CHAR(8), GETDATE() ,112)  " & _
                                                                      "WHEN  terminationdate IS NULL THEN CONVERT(CHAR(8), GETDATE(), 112) " & _
                                                                      "ELSE terminationdate " & _
                                                                 "END)), 0) AS Experience_Days " & _
                                                            "FROM rcjobhistory " & _
                                                            "WHERE isvoid = 0 " & _
                                                            "AND applicantunkid > 0 " & _
                                                            "GROUP BY applicantunkid ) AS JobExp on JobExp.applicantunkid = rcapplicant_master.applicantunkid " & _
                               " WHERE rcapp_vacancy_mapping.isimport = 0 AND ISNULL(rcapplicant_master.employeeunkid,0) <= 0  " & _
                             " ) AS a " & _
                            " WHERE  1 = 1 "



            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If



            strQ &= " UNION "

            
            strQ &= " SELECT DISTINCT " & _
                        " applicantunkid " & _
                        " ,applicant_code  " & _
                        " ,firstname " & _
                        " ,surname  " & _
                        " ,applicantname  " & _
                        " ,gender_name  " & _
                        " ,birth_date  " & _
                        " ,present_mobileno  " & _
                        " ,email  " & _
                        " ,GUID  " & _
                        " ,'' qualificationgroup  " & _
                        " ,qualificationunkid " & _
                        " ,qualificationname " & _
                        " ,gender " & _
                        " ,gpacode " & _
                        " ,'' skillcategory " & _
                        " ,'' skill " & _
                        " ,employeecode " & _
                        " ,referenceno " & _
                        " ,award_start_date " & _
                        " ,award_end_date " & _
                        " ,other_qualificationgrp " & _
                        " ,other_qualification " & _
                        " ,other_resultcode " & _
                        " ,other_skillcategory " & _
                        " ,other_skill " & _
                        " ,gpacode_string " & _
                        " ,2 AS app_statusid " & _
                        " ,0 AS app_issent " & _
                        " ,nationalityunkid" & _
                        " ,nationality " & _
                        " ,experience_days " & _
                        " ,'' experience" & _
                        ", employeeunkid " & _
                        " FROM (SELECT " & _
                        "               rcapplicant_master.applicantunkid " & _
                        "              ,CASE WHEN ISNULL(rcapplicant_master.employeeunkid,0) > 0  THEN ISNULL(hremployee_master.employeecode,'') ELSE ISNULL(rcapplicant_master.applicant_code,'') END applicant_code " & _
                        "              ,ISNULL(rcapplicant_master.firstname, '') firstname " & _
                        "              ,ISNULL(rcapplicant_master.surname, '') surname " & _
                        "              ,ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') applicantname " & _
                        "              ,CASE " & _
                        "                        WHEN rcapplicant_master.gender = 1 THEN  @Male  " & _
                        "                        WHEN rcapplicant_master.gender = 2 THEN  @Female  " & _
                        "               END gender_name " & _
                        "              ,rcapplicant_master.gender " & _
                        "              ,rcapplicant_master.birth_date " & _
                        "               ,rcapplicant_master.present_mobileno " & _
                        "               ,rcapplicant_master.email " & _
                        "               ,ISNULL(rcapplicant_master.employeecode, '') employeecode " & _
                        "               ,hremployee_master.employeeunkid " & _
                        "               ,#TableSusp.datestranunkid " & _
                        "               ,ISNULL(referenceno, '') referenceno " & _
                        "               ,ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid " & _
                        "               ,ISNULL(hrqualification_master.qualificationunkid, 0) qualificationunkid " & _
                        "               ,ISNULL(hrqualification_master.qualificationname, '') qualificationname" & _
                        "               ,ISNULL(hremp_qualification_tran.other_qualificationgrp, '') other_qualificationgrp " & _
                        "               ,ISNULL(hremp_qualification_tran.other_qualification, '') other_qualification " & _
                        "               ,ISNULL(hremp_qualification_tran.other_resultcode, '') other_resultcode " & _
                        "               ,'' AS other_skillcategory " & _
                        "               ,'' AS other_skill " & _
                        "               ,'" & mstrGUID & "' as GUID " & _
                        "               ,ISNULL(rcapp_vacancy_mapping.vacancyunkid, 0) vacancyunkid " & _
                        "               ,ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
                        "               ,ISNULL(hrresult_master.resultunkid, 0) resultunkid " & _
                        "               ,ISNULL(hrresult_master.result_level, 0) result_level " & _
                        "               ,hremp_qualification_tran.gpacode AS gpacode " & _
                        "               ,'' AS gpacode_string " & _
                        "               ,ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
                        "               ,ISNULL(hremp_app_skills_tran.skillcategoryunkid, 0) skillcategoryunkid " & _
                        "               ,ISNULL(hremp_app_skills_tran.skillunkid, 0) skillunkid " & _
                        "               ,hremp_qualification_tran.award_start_date " & _
                        "               ,hremp_qualification_tran.award_end_date " & _
                        "               ,ISNULL(rcapplicant_master.nationality, 0) AS nationalityunkid " & _
                        "               ,hrmsConfiguration..cfcountry_master.country_name AS nationality " & _
                        "               ,ISNULL(Experience_Days, 0) AS experience_days " & _
                        "    FROM rcapplicant_master " & _
                        "    LEFT JOIN hremp_qualification_tran ON rcapplicant_master.employeeunkid = hremp_qualification_tran.employeeunkid And hremp_qualification_tran.isvoid = 0 " & _
                        "    LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                        "    LEFT JOIN hremp_app_skills_tran 	ON rcapplicant_master.applicantunkid = hremp_app_skills_tran.emp_app_unkid AND hremp_app_skills_tran.isvoid = 0 " & _
                        "    LEFT JOIN cfcommon_master  ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid And cfcommon_master.mastertype =  " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & _
                        "    LEFT JOIN hrresult_master ON hrresult_master.resultunkid = hremp_qualification_tran.resultunkid " & _
                        "    LEFT JOIN rcapp_vacancy_mapping ON rcapp_vacancy_mapping.applicantunkid = rcapplicant_master.applicantunkid  AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
                        "    JOIN rcvacancy_master 	ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                        "    JOIN cfcommon_master cm ON cm.masterunkid = rcvacancy_master.vacancytitle AND cm.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "' " & _
                        "    LEFT JOIN hrmsConfiguration..cfcountry_master	ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.nationality " & _
                        "    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcapplicant_master.employeeunkid " & _
                        "    LEFT JOIN #TableSusp ON hremployee_master.employeeunkid = #TableSusp.employeeunkid " & _
                        "    LEFT JOIN (SELECT  " & _
                        "           employeeunkid " & _
                        "          ,ISNULL(SUM(DATEDIFF(DAY, start_date , CASE " & _
                        "                                                                                       WHEN CONVERT(CHAR(8), end_date, 112) = '19000101' THEN CONVERT(CHAR(8), GETDATE(), 112) " & _
                        "                                                                                        WHEN end_date IS NULL THEN CONVERT(CHAR(8), GETDATE(), 112) " & _
                        "                                                                       ELSE end_date END)), 0) AS Experience_Days " & _
                        "           FROM hremp_experience_tran " & _
                        "           WHERE isvoid = 0 	AND employeeunkid > 0 " & _
                        "           GROUP BY employeeunkid) AS JobExp	ON JobExp.employeeunkid = rcapplicant_master.employeeunkid " & _
                        "   WHERE rcapp_vacancy_mapping.isimport = 0 AND ISNULL(rcapplicant_master.employeeunkid,0) > 0) AS a " & _
                        "   WHERE 1 = 1 "

            'Pinkal (12-Nov-2020) -- Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.["              ,CASE WHEN ISNULL(rcapplicant_master.employeeunkid,0) > 0  THEN ISNULL(hremployee_master.employeecode,'') ELSE ISNULL(rcapplicant_master.applicant_code,'') END applicant_code " & _]

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " AND a.datestranunkid IS NULL "

            strQ &= " DROP TABLE #TableSusp "

            'Pinkal (24-Sep-2020) -- End




            objDataOperation.AddParameter("@QLvel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Group Level"))
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            objDataOperation.AddParameter("@AsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (13 Mar 2020) -- End
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") > 0).ToList()
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    Dim objEmpDates As New clsemployee_dates_tran
                    For i As Integer = 0 To drRow.Count - 1
                        Dim dsEmpList As DataSet = objEmpDates.GetEmployeeServiceDays(Nothing, xCompanyId, xDataBaseName, drRow(i)("employeeunkid").ToString(), dtAsOnDate)
                        If dsEmpList IsNot Nothing AndAlso dsEmpList.Tables(0).Rows.Count > 0 Then
                            drRow(i)("experience_days") = CInt(drRow(i)("experience_days")) + CInt(dsEmpList.Tables(0).Rows(0)("ServiceDays"))
                        Else
                            Continue For
                        End If
                        drRow(i).AcceptChanges()
                        If dsEmpList IsNot Nothing Then dsEmpList.Clear()
                        dsEmpList = Nothing
                    Next
                    objEmpDates = Nothing
                End If
            End If
            'Pinkal (24-Sep-2020) -- End


        Catch ex As Exception
            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            'DisplayError.Show("-1", ex.Message, "GetFilterApplicant", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetORFilterApplicant; Module Name: " & mstrModuleName)
            'Pinkal (24-Sep-2020) -- End
        End Try
        Return dsList
    End Function

    '' <summary>
    '' Modify By: Pinkal Jariwala
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    '' 
    'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
    'Public Function GetANDFilterApplicant(ByVal mstrFilter As String, ByVal mstrGUID As String, ByVal dtAsOnDate As Date) As DataSet
    Public Function GetANDFilterApplicant(ByVal xCompanyId As Integer, ByVal xDataBaseName As String, ByVal mstrFilter As String, ByVal mstrGUID As String, ByVal dtAsOnDate As Date) As DataSet
        'Pinkal (24-Sep-2020) -- End


        'Sohail (13 Mar 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            strQ = "SELECT datestranunkid " & _
                 ", employeeunkid " & _
                 ", effectivedate " & _
                 ", date1 " & _
                 ", date2 " & _
            "INTO #TableSusp " & _
            "FROM hremployee_dates_tran " & _
            "WHERE isvoid = 0 " & _
                  "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                  "AND 1 = CASE " & _
                              "WHEN date2 IS NULL THEN " & _
                                  "CASE " & _
                                      "WHEN CONVERT(CHAR(8), date1, 112) <= @AsOnDate THEN " & _
                                          "1 " & _
                                      "ELSE " & _
                                          "0 " & _
                                  "END " & _
                              "ELSE " & _
                                  "CASE " & _
                                      "WHEN @AsOnDate " & _
                                           "BETWEEN CONVERT(CHAR(8), date1, 112) AND CONVERT(CHAR(8), date2, 112) THEN " & _
                                          "1 " & _
                                      "ELSE " & _
                                          "0 " & _
                                  "END " & _
                          "END "
            'Sohail (13 May 2020) - [CONVERT(CHAR(8), date1]=[CONVERT(CHAR(8), date1, 112)], [BETWEEN date1 AND date2 THEN]=[]
            'Sohail (13 Mar 2020) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = " SELECT    rcapplicant_master.applicantunkid , " & _
            '                "applicant_code " & _
            '                ",ISNULL(firstname, '') firstname  " & _
            '                ",ISNULL(surname, '') surname  " & _
            '                ",ISNULL(firstname, '') + ' ' + ISNULL(surname, '') applicantname " & _
            '                ",CASE WHEN gender = 1 THEN @Male " & _
            '                     "WHEN gender = 2 THEN @Female " & _
            '                "END gender_name  " & _
            '                ",gender " & _
            '                ",birth_date  " & _
            '                ",present_mobileno  " & _
            '                ",email  " & _
            '                ",ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid  " & _
            '                ",'' qualificationgroup " & _
            '                ",ISNULL(hrqualification_master.qualificationunkid,0) qualificationunkid " & _
            '                ",ISNULL(hrqualification_master.qualificationname,'') qualificationname " & _
            '                ",'" & mstrGUID & "' as GUID " & _
            '                ",ISNULL(rcapplicant_master.vacancyunkid, 0) vacancyunkid  " & _
            '                ",ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
            '                ",ISNULL(hrresult_master.resultunkid, 0) resultunkid  " & _
            '                ",ISNULL(hrresult_master.result_level, 0) result_level  " & _
            '                ",ISNULL(rcapplicantqualification_tran.gpacode, 0) gpacode  " & _
            '                ",ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
            '                ",ISNULL(rcapplicantskill_tran.skillcategoryunkid, 0) skillcategoryunkid  " & _
            '                ",'' skillcategory " & _
            '                ",ISNULL(rcapplicantskill_tran.skillunkid, 0) skillunkid " & _
            '                ",'' skill " & _
            '                ",ISNULL(employeecode,'') employeecode" & _
            '                ",ISNULL(referenceno,'') referenceno" & _
            '                " FROM rcapplicant_master " & _
            '                " LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid " & _
            '                " LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '                " LEFT JOIN rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid " & _
            '                " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & _
            '                " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _            
            '                "WHERE  rcapplicant_master.isimport = 0 "



            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = " SELECT    rcapplicant_master.applicantunkid , " & _
            '                                "applicant_code " & _
            '                                ",ISNULL(firstname, '') firstname  " & _
            '                                ",ISNULL(surname, '') surname  " & _
            '                                ",ISNULL(firstname, '') + ' ' + ISNULL(surname, '') applicantname " & _
            '                                ",CASE WHEN gender = 1 THEN @Male " & _
            '                                     "WHEN gender = 2 THEN @Female " & _
            '                                "END gender_name  " & _
            '                                ",gender " & _
            '                                ",birth_date  " & _
            '                                ",present_mobileno  " & _
            '                                ",email  " & _
            '                                ",ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid  " & _
            '                                ",'' qualificationgroup " & _
            '                                ",ISNULL(hrqualification_master.qualificationunkid,0) qualificationunkid " & _
            '                                ",ISNULL(hrqualification_master.qualificationname,'') qualificationname " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_qualificationgrp, '') other_qualificationgrp " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_qualification, '') other_qualification " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_resultcode, '') other_resultcode " & _
            '                                ",'" & mstrGUID & "' as GUID " & _
            '                ",ISNULL(Vac.vacancyunkid, 0) vacancyunkid  " & _
            '                                ",ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
            '                                ",ISNULL(hrresult_master.resultunkid, 0) resultunkid  " & _
            '                                ",ISNULL(hrresult_master.result_level, 0) result_level  " & _
            '                ",'' gpacode  " & _
            '                                ",ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillcategoryunkid, 0) skillcategoryunkid  " & _
            '                                ",'' skillcategory " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillunkid, 0) skillunkid " & _
            '                                ",'' skill " & _
            '                                ",ISNULL(rcapplicantskill_tran.other_skillcategory, '') other_skillcategory " & _
            '                                ",ISNULL(rcapplicantskill_tran.other_skill, '') other_skill " & _
            '                                ",ISNULL(employeecode,'') employeecode" & _
            '                                ",ISNULL(referenceno,'') referenceno" & _
            '                ",rcapplicantqualification_tran.award_start_date " & _
            '                 ",rcapplicantqualification_tran.award_end_date " & _
            '                                " FROM rcapplicant_master " & _
            '                                " LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid " & _
            '                                " LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '                                " LEFT JOIN rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid " & _
            '                                " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & _
            '                                " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _
            '                "LEFT JOIN " & _
            '                "( " & _
            '                     "SELECT " & _
            '                           "applicantunkid " & _
            '                          ",CVac.vacancyunkid " & _
            '                          ",cfcommon_master.name AS vacancy " & _
            '                          ",CONVERT(CHAR(8),openingdate,112) AS ODate " & _
            '                          ",CONVERT(CHAR(8),closingdate,112) AS CDate " & _
            '                     "FROM " & _
            '                     "( " & _
            '                          "SELECT " & _
            '                                "applicantunkid " & _
            '                               ",vacancyunkid " & _
            '                               ",ROW_NUMBER() OVER (PARTITION BY applicantunkid ORDER BY appvacancytranunkid DESC) AS Srno " & _
            '                          "FROM rcapp_vacancy_mapping " & _
            '                          "WHERE isactive = 1 " & _
            '                     ") AS CVac " & _
            '                     "JOIN rcvacancy_master ON CVac.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '                     "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype ='" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
            '                     "WHERE CVac.Srno = 1 " & _
            '                ") AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid " & _
            '                      "WHERE  rcapplicant_master.isimport = 0 "


            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.

            'strQ &= " SELECT    DISTINCT rcapplicant_master.applicantunkid , " & _
            '                                "applicant_code " & _
            '                                ",ISNULL(rcapplicant_master.firstname, '') firstname  " & _
            '                                ",ISNULL(rcapplicant_master.surname, '') surname  " & _
            '                                ",ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') applicantname " & _
            '                                ",CASE WHEN rcapplicant_master.gender = 1 THEN @Male " & _
            '                                     "WHEN rcapplicant_master.gender = 2 THEN @Female " & _
            '                                "END gender_name  " & _
            '                                ",rcapplicant_master.gender " & _
            '                                ",rcapplicant_master.birth_date  " & _
            '                                ",rcapplicant_master.present_mobileno  " & _
            '                                ",rcapplicant_master.email  " & _
            '                                ",ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid  " & _
            '                                ",'' qualificationgroup " & _
            '                                ",ISNULL(hrqualification_master.qualificationunkid,0) qualificationunkid " & _
            '                                ",ISNULL(hrqualification_master.qualificationname,'') qualificationname " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_qualificationgrp, '') other_qualificationgrp " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_qualification, '') other_qualification " & _
            '                                ",ISNULL(rcapplicantqualification_tran.other_resultcode, '') other_resultcode " & _
            '                                ",'" & mstrGUID & "' as GUID " & _
            '                                ",ISNULL(rcapp_vacancy_mapping.vacancyunkid, 0) vacancyunkid  " & _
            '                                ",ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
            '                                ",ISNULL(hrresult_master.resultunkid, 0) resultunkid  " & _
            '                                ",ISNULL(hrresult_master.result_level, 0) result_level  " & _
            '                                 ",rcapplicantqualification_tran.gpacode as gpacode  " & _
            '                                ",'' as gpacode_string  " & _
            '                                ",ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillcategoryunkid, 0) skillcategoryunkid  " & _
            '                                ",'' skillcategory " & _
            '                                ",ISNULL(rcapplicantskill_tran.skillunkid, 0) skillunkid " & _
            '                                ",'' skill " & _
            '                                ",ISNULL(rcapplicantskill_tran.other_skillcategory, '') other_skillcategory " & _
            '                                ",ISNULL(rcapplicantskill_tran.other_skill, '') other_skill " & _
            '                                ",ISNULL(rcapplicant_master.employeecode,'') employeecode" & _
            '                                ",ISNULL(referenceno,'') referenceno" & _
            '                ",rcapplicantqualification_tran.award_start_date " & _
            '                 ",rcapplicantqualification_tran.award_end_date " & _
            '                                ",2 AS app_statusid " & _
            '                                ",0 AS app_issent " & _
            '                                ",ISNULL(rcapplicant_master.nationality,0) AS nationalityunkid " & _
            '                                ",ISNULL(hrmsConfiguration..cfcountry_master.country_name, '') as nationality " & _
            '                                ",ISNULL(Experience_Days, 0) AS experience_days " & _
            '                                ", '' experience " & _
            '                                " FROM rcapplicant_master " & _
            '                                " LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
            '                                " LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '                                " LEFT JOIN rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid AND rcapplicantskill_tran.isvoid = 0 " & _
            '                                " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & _
            '                                " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _
            '                                " LEFT JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid  AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
            '                                " JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '                                " JOIN cfcommon_master cm ON cm.masterunkid = rcvacancy_master.vacancytitle AND cm.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & _
            '                                " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.nationality " & _
            '                                "LEFT JOIN hremployee_master ON hremployee_master.employeecode = rcapplicant_master.employeecode " & _
            '                                "LEFT JOIN #TableSusp ON hremployee_master.employeeunkid = #TableSusp.employeeunkid " & _
            '                                " LEFT JOIN ( SELECT " & _
            '                                                     "applicantunkid " & _
            '                                                   ",ISNULL(SUM(DATEDIFF(DAY, joiningdate, CASE " & _
            '                                                          "WHEN CONVERT(CHAR(8), terminationdate,112) = '19000101' THEN CONVERT(CHAR(8), GETDATE() ,112)  " & _
            '                                                          "WHEN  terminationdate IS NULL THEN CONVERT(CHAR(8), GETDATE(), 112) " & _
            '                                                          "ELSE terminationdate " & _
            '                                                     "END)), 0) AS Experience_Days " & _
            '                                                "FROM rcjobhistory " & _
            '                                                "WHERE isvoid = 0 " & _
            '                                                "AND applicantunkid > 0 " & _
            '                                                "GROUP BY applicantunkid ) AS JobExp on JobExp.applicantunkid = rcapplicant_master.applicantunkid " & _
            '                      "WHERE rcapp_vacancy_mapping.isimport = 0   "

            'Sohail (13 Mar 2020) - [DISTINCT, LEFT JOIN hremployee_master, LEFT JOIN #TableSusp]
            'Hemant (07 Oct 2019) -- [nationality,experience_days,experience]
            'Hemant (02 Jul 2019) -- [nationalityunkid]
            'Sohail (09 Oct 2018) - [rcapp_vacancy_mapping.isactive = 1]=[ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]

            'Pinkal (09-May-2018) -- Short Listing applicant Count Issue.[ AND rcapplicantqualification_tran.isvoid = 0 AND rcapplicantskill_tran.isvoid = 0]

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            '-> AND rcapplicantskill_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END


            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            '"WHERE  rcapplicant_master.isimport = 0 "  Removed
            '"WHERE  rcapp_vacancy_mapping.isimport = 0 "  Added
            'S.SANDEEP [ 14 May 2013 ] -- END

            'Pinkal (11-MAY-2012) -- End

            'If mstrFilter.Trim.Length > 0 Then
            '    strQ &= " AND " & mstrFilter
            'End If

            ''Sohail (13 Mar 2020) -- Start
            ''NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            'strQ &= " AND #TableSusp.datestranunkid IS NULL "

            'strQ &= " DROP TABLE #TableSusp "
            ''Sohail (13 Mar 2020) -- End



            strQ &= " SELECT  DISTINCT rcapplicant_master.applicantunkid , " & _
                                            "applicant_code " & _
                                            ",ISNULL(rcapplicant_master.firstname, '') firstname  " & _
                                            ",ISNULL(rcapplicant_master.surname, '') surname  " & _
                                            ",ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') applicantname " & _
                                            ",CASE WHEN rcapplicant_master.gender = 1 THEN @Male " & _
                                                 "WHEN rcapplicant_master.gender = 2 THEN @Female " & _
                                            "END gender_name  " & _
                                            ",rcapplicant_master.gender " & _
                                            ",rcapplicant_master.birth_date  " & _
                                            ",rcapplicant_master.present_mobileno  " & _
                                            ",rcapplicant_master.email  " & _
                                            ",ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid  " & _
                                            ",'' qualificationgroup " & _
                                            ",ISNULL(hrqualification_master.qualificationunkid,0) qualificationunkid " & _
                                            ",ISNULL(hrqualification_master.qualificationname,'') qualificationname " & _
                                            ",ISNULL(rcapplicantqualification_tran.other_qualificationgrp, '') other_qualificationgrp " & _
                                            ",ISNULL(rcapplicantqualification_tran.other_qualification, '') other_qualification " & _
                                            ",ISNULL(rcapplicantqualification_tran.other_resultcode, '') other_resultcode " & _
                                            ",'" & mstrGUID & "' as GUID " & _
                                            ",ISNULL(rcapp_vacancy_mapping.vacancyunkid, 0) vacancyunkid  " & _
                                            ",ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
                                            ",ISNULL(hrresult_master.resultunkid, 0) resultunkid  " & _
                                            ",ISNULL(hrresult_master.result_level, 0) result_level  " & _
                                             ",rcapplicantqualification_tran.gpacode as gpacode  " & _
                                            ",'' as gpacode_string  " & _
                                            ",ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
                                            ",ISNULL(rcapplicantskill_tran.skillcategoryunkid, 0) skillcategoryunkid  " & _
                                            ",'' skillcategory " & _
                                            ",ISNULL(rcapplicantskill_tran.skillunkid, 0) skillunkid " & _
                                            ",'' skill " & _
                                            ",ISNULL(rcapplicantskill_tran.other_skillcategory, '') other_skillcategory " & _
                                            ",ISNULL(rcapplicantskill_tran.other_skill, '') other_skill " & _
                                            ",ISNULL(rcapplicant_master.employeecode,'') employeecode" & _
                                            ",ISNULL(referenceno,'') referenceno" & _
                            ",rcapplicantqualification_tran.award_start_date " & _
                             ",rcapplicantqualification_tran.award_end_date " & _
                                            ",2 AS app_statusid " & _
                                            ",0 AS app_issent " & _
                                            ",ISNULL(rcapplicant_master.nationality,0) AS nationalityunkid " & _
                                            ",ISNULL(hrmsConfiguration..cfcountry_master.country_name, '') as nationality " & _
                                            ",ISNULL(Experience_Days, 0) AS experience_days " & _
                                            ", '' experience " & _
                                         ", ISNULL(rcapplicant_master.employeeunkid,0) AS employeeunkid " & _
                                            " FROM rcapplicant_master " & _
                                            " LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
                                            " LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                                            " LEFT JOIN rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid AND rcapplicantskill_tran.isvoid = 0 " & _
                                            " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & _
                                            " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _
                                            " LEFT JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid  AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
                                            " JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                                            " JOIN cfcommon_master cm ON cm.masterunkid = rcvacancy_master.vacancytitle AND cm.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & _
                                            " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.nationality " & _
                                            " LEFT JOIN ( SELECT " & _
                                                                 "applicantunkid " & _
                                                               ",ISNULL(SUM(DATEDIFF(DAY, joiningdate, CASE " & _
                                                                      "WHEN CONVERT(CHAR(8), terminationdate,112) = '19000101' THEN CONVERT(CHAR(8), GETDATE() ,112)  " & _
                                                                      "WHEN  terminationdate IS NULL THEN CONVERT(CHAR(8), GETDATE(), 112) " & _
                                                                      "ELSE terminationdate " & _
                                                                 "END)), 0) AS Experience_Days " & _
                                                            "FROM rcjobhistory " & _
                                                            "WHERE isvoid = 0 " & _
                                                            "AND applicantunkid > 0 " & _
                                                            "GROUP BY applicantunkid ) AS JobExp on JobExp.applicantunkid = rcapplicant_master.applicantunkid " & _
                                        " WHERE rcapp_vacancy_mapping.isimport = 0 AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0   "


            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If


            strQ &= " UNION "


            strQ &= " SELECT  DISTINCT rcapplicant_master.applicantunkid , " & _
                                    " CASE WHEN ISNULL(rcapplicant_master.employeeunkid,0) > 0 THEN ISNULL(hremployee_master.employeecode,'') ELSE ISNULL(rcapplicant_master.applicant_code,'')  END  applicant_code " & _
                                    ",ISNULL(rcapplicant_master.firstname, '') firstname  " & _
                                    ",ISNULL(rcapplicant_master.surname, '') surname  " & _
                                    ",ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') applicantname " & _
                                    ",CASE WHEN rcapplicant_master.gender = 1 THEN @Male " & _
                                         "WHEN rcapplicant_master.gender = 2 THEN @Female " & _
                                    "END gender_name  " & _
                                    ",rcapplicant_master.gender " & _
                                    ",rcapplicant_master.birth_date  " & _
                                    ",rcapplicant_master.present_mobileno  " & _
                                    ",rcapplicant_master.email  " & _
                                    ",ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid  " & _
                                    ",'' qualificationgroup " & _
                                    ",ISNULL(hrqualification_master.qualificationunkid,0) qualificationunkid " & _
                                    ",ISNULL(hrqualification_master.qualificationname,'') qualificationname " & _
                                    ",ISNULL(hremp_qualification_tran.other_qualificationgrp, '') other_qualificationgrp " & _
                                    ",ISNULL(hremp_qualification_tran.other_qualification, '') other_qualification " & _
                                    ",ISNULL(hremp_qualification_tran.other_resultcode, '') other_resultcode " & _
                                    ",'" & mstrGUID & "' as GUID " & _
                                    ",ISNULL(rcapp_vacancy_mapping.vacancyunkid, 0) vacancyunkid  " & _
                                    ",ISNULL(cfcommon_master.qlevel, 0) qlevel " & _
                                    ",ISNULL(hrresult_master.resultunkid, 0) resultunkid  " & _
                                    ",ISNULL(hrresult_master.result_level, 0) result_level  " & _
                                     ",hremp_qualification_tran.gpacode as gpacode  " & _
                                    ",'' as gpacode_string  " & _
                                    ",ISNULL(YEAR(GETDATE()) - YEAR(rcapplicant_master.birth_date), 0) AS age " & _
                                    ",ISNULL(hremp_app_skills_tran.skillcategoryunkid, 0) skillcategoryunkid  " & _
                                    ",'' skillcategory " & _
                                    ",ISNULL(hremp_app_skills_tran.skillunkid, 0) skillunkid " & _
                                    ",'' skill " & _
                                    ",ISNULL(hremp_app_skills_tran.other_skillcategory, '') other_skillcategory " & _
                                    ",ISNULL(hremp_app_skills_tran.other_skill, '') other_skill " & _
                                    ",ISNULL(rcapplicant_master.employeecode,'') employeecode" & _
                                    ",ISNULL(referenceno,'') referenceno" & _
                                    ",hremp_qualification_tran.award_start_date " & _
                                    ",hremp_qualification_tran.award_end_date " & _
                                    ",2 AS app_statusid " & _
                                    ",0 AS app_issent " & _
                                    ",ISNULL(rcapplicant_master.nationality,0) AS nationalityunkid " & _
                                    ",ISNULL(hrmsConfiguration..cfcountry_master.country_name, '') as nationality " & _
                                    ",ISNULL(Experience_Days, 0) AS experience_days " & _
                                    ", '' experience " & _
                                    ", ISNULL(rcapplicant_master.employeeunkid,0) AS employeeunkid " & _
                                    " FROM rcapplicant_master " & _
                                    " LEFT JOIN hremp_qualification_tran ON rcapplicant_master.employeeunkid = hremp_qualification_tran.employeeunkid AND hremp_qualification_tran.isvoid = 0 " & _
                                    " LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                                    " LEFT JOIN hremp_app_skills_tran	ON rcapplicant_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid AND hremp_app_skills_tran.isvoid = 0 " & _
                                    " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & _
                                    " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = hremp_qualification_tran.resultunkid " & _
                                    " LEFT JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
                                    " JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                                    " JOIN cfcommon_master cm ON cm.masterunkid = rcvacancy_master.vacancytitle AND cm.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & _
                                    " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.nationality " & _
                                    " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcapplicant_master.employeeunkid " & _
                                    " LEFT JOIN #TableSusp ON hremployee_master.employeeunkid = #TableSusp.employeeunkid " & _
                                    " LEFT JOIN (SELECT  " & _
                                    "           employeeunkid " & _
                                    "          ,ISNULL(SUM(DATEDIFF(DAY, start_date , CASE " & _
                                    "                                                                                       WHEN CONVERT(CHAR(8), end_date, 112) = '19000101' THEN CONVERT(CHAR(8), GETDATE(), 112) " & _
                                    "                                                                                        WHEN end_date IS NULL THEN CONVERT(CHAR(8), GETDATE(), 112) " & _
                                    "                                                                       ELSE end_date END)), 0) AS Experience_Days " & _
                                    "           FROM hremp_experience_tran " & _
                                    "           WHERE isvoid = 0 	AND employeeunkid > 0 " & _
                                    "           GROUP BY employeeunkid) AS JobExp	ON JobExp.employeeunkid = rcapplicant_master.employeeunkid " & _
                                    " WHERE rcapp_vacancy_mapping.isimport = 0 AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0   "

            'Pinkal (12-Nov-2020) -- Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.[" CASE WHEN ISNULL(rcapplicant_master.employeeunkid,0) > 0 THEN ISNULL(hremployee_master.employeecode,'') ELSE ISNULL(rcapplicant_master.applicant_code,'')  END  applicant_code " & _]

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Replace("rcapplicantqualification_tran", "hremp_qualification_tran").Replace("rcapplicantskill_tran", "hremp_app_skills_tran")
            End If

            strQ &= " AND #TableSusp.datestranunkid IS NULL "

            strQ &= " DROP TABLE #TableSusp "

            'Pinkal (24-Sep-2020) -- End


            objDataOperation.AddParameter("@QLvel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Group Level"))
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            objDataOperation.AddParameter("@AsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (13 Mar 2020) -- End
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") > 0).ToList()
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    Dim objEmpDates As New clsemployee_dates_tran
                    For i As Integer = 0 To drRow.Count - 1
                        Dim dsEmpList As DataSet = objEmpDates.GetEmployeeServiceDays(Nothing, xCompanyId, xDataBaseName, drRow(i)("employeeunkid").ToString(), dtAsOnDate)
                        If dsEmpList IsNot Nothing AndAlso dsEmpList.Tables(0).Rows.Count > 0 Then
                            drRow(i)("experience_days") = CInt(drRow(i)("experience_days")) + CInt(dsEmpList.Tables(0).Rows(0)("ServiceDays"))
                        Else
                            Continue For
                        End If
                        drRow(i).AcceptChanges()
                        If dsEmpList IsNot Nothing Then dsEmpList.Clear()
                        dsEmpList = Nothing
                    Next
                    objEmpDates = Nothing
                End If
            End If
            'Pinkal (24-Sep-2020) -- End

        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "GetANDFilterApplicant", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetANDFilterApplicant; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    Public Function Delete_ShortListedFilterApplicant(ByVal objDataOperation As clsDataOperation, ByVal intParentAuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Try

            strQ = " Select * from rcshortlist_filter where shortlistunkid = @shortlistunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortlistunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                strQ = "UPDATE rcshortlist_filter SET isvoid = 1,voiduserunkid=@voiduserunkid,voiddatetime=@voiddatetime,voidreason = @voidreason " & _
                          "WHERE shortlistunkid = @shortlistunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortlistunkid)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dr As DataRow In dsList.Tables(0).Rows

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcshortlist_master", "shortlistunkid", mintShortlistunkid, "rcshortlist_filter", "filterunkid", CInt(dr("filterunkid")), intParentAuditType, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete_ShortListedApplicant; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    'S.SANDEEP [ 05 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Get_Tot_Applicant_Vac(ByVal intVacancy As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = -1
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT rcapp_vacancy_mapping.applicantunkid " & _
                   "FROM rcapplicant_master " & _
                        "JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid " & _
                   "WHERE rcapp_vacancy_mapping.vacancyunkid = '" & intVacancy & "' AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 "
            'Sohail (09 Oct 2018) - [isactive = 1]=[ISNULL(isvoid, 0) = 0]

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Return iCnt

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Get_Tot_Applicant_Vac", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 05 NOV 2012 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Group Level")
			Language.setMessage(mstrModuleName, 2, "AND")
			Language.setMessage(mstrModuleName, 3, "OR")
			Language.setMessage("clsMasterData", 73, "Male")
			Language.setMessage("clsMasterData", 74, "Female")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
