﻿'************************************************************************************************************************************
'Class Name : clsAdvertise_tran.vb
'Purpose    :
'Date       :02/08/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master

Public Class clsAdvertise_tran
    Private Const mstrModuleName = "clsAdvertise_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private Variables "
    Private mintAdvertisetranunkid As Integer
    Private mintVacancyunkid As Integer = -1
    Private mdtTran As DataTable
    'Hemant (13 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Hemant (13 Sep 2019) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interviewertranunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _VacancyUnkid() As Integer
        Get
            Return mintVacancyunkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyunkid = value
            Call GetAdvertise_Tran()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interviewertranunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property


    'Hemant (13 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property
    'Hemant (13 Sep 2019) -- End
#End Region

#Region "Constructor "
    Public Sub New()

       
        mdtTran = New DataTable("")
        Dim dcol As DataColumn

        Try
           
            dcol = New DataColumn("advertisetranunkid")
            dcol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("vacancyunkid")
            dcol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dcol)


            dcol = New DataColumn("advertisecategoryunkid")
            dcol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("advertiserunkid")
            dcol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("costing")

            'Anjan (11 May 2011)-Start
            'dcol.DataType = System.Type.GetType("System.Decimal")
            dcol.DataType = System.Type.GetType("System.Decimal")
            'Anjan (11 May 2011)-End 


            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("budget")

            'Anjan (11 May 2011)-Start
            'dcol.DataType = System.Type.GetType("System.Decimal")
            dcol.DataType = System.Type.GetType("System.Decimal")
            'Anjan (11 May 2011)-End 

            mdtTran.Columns.Add(dcol)


            dcol = New DataColumn("description")
            dcol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("notes")
            dcol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("userunkid")
            dcol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("isvoid")
            dcol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dcol)


            dcol = New DataColumn("voiduserunkid")
            dcol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("voiddatetime")
            dcol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("voidreason")
            dcol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("GUID")
            dcol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dcol)

            dcol = New DataColumn("AUD")
            dcol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dcol)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try

    End Sub
#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose>To Get Transaction Records </purpose>
    Public Sub GetAdvertise_Tran()
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim strErrorMessage As String = ""
        Dim strQ As String = ""
        Dim dRowID_Tran As DataRow

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                        "  advertisetranunkid " & _
                        ", vacancyunkid " & _
                        ", advertisecategoryunkid " & _
                        ", advertiserunkid " & _
                        ", costing " & _
                        ", budget " & _
                        ", description " & _
                        ", notes " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                        ", '' AUD " & _
                       "FROM rcadvertise_tran " & _
                       "WHERE vacancyunkid = @vacancyunkid "
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            strQ &= "AND ISNULL(isvoid, 0) = 0 "
            'Hemant (13 Sep 2019) -- End

            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables(0).Rows(i)
                    dRowID_Tran = mdtTran.NewRow
                    dRowID_Tran.Item("advertisetranunkid") = .Item("advertisetranunkid").ToString
                    dRowID_Tran.Item("vacancyunkid") = .Item("vacancyunkid").ToString
                    dRowID_Tran.Item("advertisecategoryunkid") = .Item("advertisecategoryunkid").ToString
                    dRowID_Tran.Item("advertiserunkid") = .Item("advertiserunkid").ToString
                    dRowID_Tran.Item("costing") = .Item("costing").ToString
                    dRowID_Tran.Item("budget") = .Item("budget").ToString
                    dRowID_Tran.Item("description") = .Item("description").ToString
                    dRowID_Tran.Item("notes") = .Item("notes").ToString
                    dRowID_Tran.Item("userunkid") = .Item("userunkid").ToString
                    dRowID_Tran.Item("isvoid") = .Item("isvoid").ToString
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid").ToString
                    dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    dRowID_Tran.Item("voidreason") = .Item("voidreason").ToString
                    dRowID_Tran.Item("AUD") = .Item("AUD")

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next

          
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAdvertise_Tran", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Sub

    Public Function InsertUpdateDelete_AdvertiseTran(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (13 Sep 2019) -- [dtCurrentDateAndTime, xDataOp]
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        Try
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            End If
            'Hemant (13 Sep 2019) -- End

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case (.Item("AUD"))
                            Case "A"
                                strQ = "INSERT INTO rcadvertise_tran ( " & _
                                            "  vacancyunkid " & _
                                            ", advertisecategoryunkid " & _
                                            ", advertiserunkid " & _
                                            ", costing " & _
                                            ", budget " & _
                                            ", description " & _
                                            ", notes " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                          ") VALUES (" & _
                                            "  @vacancyunkid " & _
                                            ", @advertisecategoryunkid " & _
                                            ", @advertiserunkid " & _
                                            ", @costing " & _
                                            ", @budget " & _
                                            ", @description " & _
                                            ", @notes " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                          "); SELECT @@identity"

                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid)
                                objDataOperation.AddParameter("@advertisecategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("advertisecategoryunkid").ToString)
                                objDataOperation.AddParameter("@advertiserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("advertiserunkid").ToString)


                                'Anjan (11 May 2011)-Start
                                'objDataOperation.AddParameter("@costing", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("costing").ToString)
                                'objDataOperation.AddParameter("@budget", SqlDbType.Int, eZeeDataType.MONEY_SIZE, .Item("budget").ToString)
                                objDataOperation.AddParameter("@costing", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("costing"))
                                objDataOperation.AddParameter("@budget", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("budget"))
                                'Anjan (11 May 2011)-End 




                                objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("description").ToString)
                                objDataOperation.AddParameter("@notes", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("notes").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)

                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason").ToString)


                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "list")
                                'Anjan (12 Oct 2011)-End 

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                mintAdvertisetranunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("vacancyunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", mintVacancyunkid, "rcadvertise_tran", "advertisetranunkid", mintAdvertisetranunkid, 2, 1) = False Then
                                        Return False
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", mintVacancyunkid, "rcadvertise_tran", "advertisetranunkid", mintAdvertisetranunkid, 1, 1) = False Then
                                        Return False
                                    End If
                                End If
                                'Anjan (12 Oct 2011)-End 

                            Case "U"

                                strQ = "UPDATE rcadvertise_tran SET " & _
                                        "  vacancyunkid = @vacancyunkid" & _
                                        ", advertisecategoryunkid = @advertisecategoryunkid" & _
                                        ", advertiserunkid = @advertiserunkid" & _
                                        ", costing = @costing" & _
                                        ", budget = @budget" & _
                                        ", description = @description" & _
                                        ", notes = @notes" & _
                                        ", userunkid = @userunkid " & _
                                     "WHERE advertisetranunkid = @advertisetranunkid "

                                objDataOperation.AddParameter("@advertisetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("advertisetranunkid").ToString)
                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid)
                                objDataOperation.AddParameter("@advertisecategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("advertisecategoryunkid").ToString)
                                objDataOperation.AddParameter("@advertiserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("advertiserunkid").ToString)


                                'Anjan (11 May 2011)-Start
                                'objDataOperation.AddParameter("@costing", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("costing").ToString)
                                'objDataOperation.AddParameter("@budget", SqlDbType.Int, eZeeDataType.MONEY_SIZE, .Item("budget").ToString)
                                objDataOperation.AddParameter("@costing", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("costing"))
                                objDataOperation.AddParameter("@budget", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("budget"))
                                'Anjan (11 May 2011)-End 



                                objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("description").ToString)
                                objDataOperation.AddParameter("@notes", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("advertisecategoryunkid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)


                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", .Item("vacancyunkid").ToString, "rcadvertise_tran", "advertisetranunkid", .Item("advertisetranunkid").ToString, 2, 2) = False Then
                                    Return False
                                End If
                                'Anjan (12 Oct 2011)-End 

                            Case "D"

                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                If .Item("advertisetranunkid") > 0 Then

                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", .Item("vacancyunkid").ToString, "rcadvertise_tran", "advertisetranunkid", .Item("advertisetranunkid").ToString, 2, 3) = False Then
                                        Return False
                                    End If


                                    'Hemant (13 Sep 2019) -- Start
                                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
                                    'strQ = "DELETE FROM rcadvertise_tran " & _
                                    '                                           "WHERE advertisetranunkid = @advertisetranunkid "
                                    strQ = "UPDATE rcadvertise_tran SET " & _
                                              "  isvoid = 1 " & _
                                              ", voiduserunkid = @voiduserunkid" & _
                                              ", voiddatetime = @voiddatetime" & _
                                              ", voidreason = @voidreason " & _
                                        "WHERE advertisetranunkid = @advertisetranunkid "
                                    'Hemant (13 Sep 2019) -- End

                                objDataOperation.AddParameter("@advertisetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("advertisetranunkid").ToString)
                                    'Hemant (13 Sep 2019) -- Start
                                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime.ToString)
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                    'Hemant (13 Sep 2019) -- End

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                End If
                                'Anjan (12 Oct 2011)-End 

                        End Select
                    End If
                End With
            Next
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            'objDataOperation.ReleaseTransaction(True)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (13 Sep 2019) -- End
            Return True
        Catch ex As Exception
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (13 Sep 2019) -- End
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_AdvertiseTran", mstrModuleName)
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcadvertise_tran) </purpose>
    Public Function VoidAll(ByVal intUnkid As Integer, ByVal blnIsVoid As Boolean, ByVal intVoiduserunkid As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", intUnkid, "rcadvertise_tran", "advertisetranunkid", 3, 3) = False Then
                Return False
            End If
            'Anjan (12 Oct 2011)-End 

            strQ = " UPDATE rcadvertise_tran SET " & _
                  "  isvoid = @isvoid " & _
                  ", voiduserunkid = @voiduserunkid " & _
                  ", voiddatetime = @voiddatetime  " & _
                  ", voidreason= @voidreason " & _
            "WHERE vacancyunkid = @vacancyunkid "

            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.SmallDateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsVoid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class
