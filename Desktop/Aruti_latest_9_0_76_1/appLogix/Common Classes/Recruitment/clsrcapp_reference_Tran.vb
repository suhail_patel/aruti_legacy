﻿'************************************************************************************************************************************
'Class Name : clsrcapp_reference_Tran.vb
'Purpose    :
'Date       :02/03/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsrcapp_reference_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsrcapp_reference_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintReferencetranunkid As Integer
    Private mintApplicantunkid As Integer
    Private mdtReferences As DataTable
    'Private mintUserunkid As Integer
    'Private mblnIsvoid As Boolean
    'Private mintVoiduserunkid As Integer
    'Private mdtVoiddatetime As Date
    'Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referencetranunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Referencetranunkid() As Integer
        Get
            Return mintReferencetranunkid
        End Get
        Set(ByVal value As Integer)
            mintReferencetranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set applicantunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Applicantunkid() As Integer
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = value
            Call GetApplicant_References()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtReferences
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dtReferences() As DataTable
        Get
            Return mdtReferences
        End Get
        Set(ByVal value As DataTable)
            mdtReferences = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set Userunkid
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    'Public Property _Userunkid() As Integer
    '    Get
    '        Return mintUserunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintUserunkid = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set isvoid
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    'Public Property _Isvoid() As Boolean
    '    Get
    '        Return mblnIsvoid
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsvoid = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiduserunkid
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    'Public Property _Voiduserunkid() As Integer
    '    Get
    '        Return mintVoiduserunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintVoiduserunkid = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiddatetime
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    'Public Property _Voiddatetime() As Date
    '    Get
    '        Return mdtVoiddatetime
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtVoiddatetime = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voidreason
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    'Public Property _Voidreason() As String
    '    Get
    '        Return mstrVoidreason
    '    End Get
    '    Set(ByVal value As String)
    '        mstrVoidreason = value
    '    End Set
    'End Property

#End Region

#Region "Constructor"

    Public Sub New()
        mdtReferences = New DataTable("References")
        mdtReferences.Columns.Add("referencetranunkid", Type.GetType("System.Int32"))
        mdtReferences.Columns.Add("applicantunkid", Type.GetType("System.Int32"))
        mdtReferences.Columns.Add("name", Type.GetType("System.String"))
        mdtReferences.Columns.Add("address", Type.GetType("System.String"))
        mdtReferences.Columns.Add("countryunkid", Type.GetType("System.Int32"))
        mdtReferences.Columns.Add("stateunkid", Type.GetType("System.Int32"))
        mdtReferences.Columns.Add("cityunkid", Type.GetType("System.Int32"))
        mdtReferences.Columns.Add("email", Type.GetType("System.String"))
        mdtReferences.Columns.Add("genderunkid", Type.GetType("System.Int32"))
        mdtReferences.Columns.Add("gender", Type.GetType("System.String"))
        mdtReferences.Columns.Add("position", Type.GetType("System.String"))
        mdtReferences.Columns.Add("telephone_no", Type.GetType("System.String"))
        mdtReferences.Columns.Add("mobile_no", Type.GetType("System.String"))
        mdtReferences.Columns.Add("relationunkid", Type.GetType("System.Int32"))
        mdtReferences.Columns.Add("relation", Type.GetType("System.String"))
        'mdtReferences.Columns.Add("isvoid", Type.GetType("System.Boolean"))
        'mdtReferences.Columns.Add("voiduserunkid", Type.GetType("System.Int32"))
        'mdtReferences.Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
        'mdtReferences.Columns.Add("voidreason", Type.GetType("System.String"))
        mdtReferences.Columns.Add("AUD", Type.GetType("System.String"))
        mdtReferences.Columns.Add("GUID", Type.GetType("System.String"))
        'Sohail (26 May 2017) -- Start
        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
        mdtReferences.Columns.Add("serverreferencetranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtReferences.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
        mdtReferences.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtReferences.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
        mdtReferences.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
        'Sohail (26 May 2017) -- End
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApplicant_References() As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  rcapp_reference_tran.referencetranunkid " & _
                      ", rcapp_reference_tran.applicantunkid " & _
                      ", rcapp_reference_tran.name " & _
                      ", rcapp_reference_tran.address " & _
                      ", rcapp_reference_tran.countryunkid " & _
                      ", rcapp_reference_tran.stateunkid " & _
                      ", rcapp_reference_tran.cityunkid " & _
                      ", rcapp_reference_tran.email " & _
                      ", rcapp_reference_tran.gender as genderunkid " & _
                      ", CASE WHEN rcapp_reference_tran.gender = 1 then @male  " & _
                      "           WHEN rcapp_reference_tran.gender = 2 then @female " & _
                      "           WHEN rcapp_reference_tran.gender = 3 then @other " & _
                      " END as gender " & _
                      ", rcapp_reference_tran.position " & _
                      ", rcapp_reference_tran.telephone_no " & _
                      ", rcapp_reference_tran.mobile_no " & _
                      ", rcapp_reference_tran.relationunkid " & _
                      ", cfcommon_master.name as relation " & _
                      ", '' As AUD " & _
                      ", ISNULL(rcapp_reference_tran.serverreferencetranunkid, 0) AS serverreferencetranunkid " & _
                      ", ISNULL(rcapp_reference_tran.isvoid,0) AS isvoid " & _
                      ", rcapp_reference_tran.voiduserunkid " & _
                      ", rcapp_reference_tran.voiddatetime " & _
                      ", ISNULL(rcapp_reference_tran.voidreason,'') AS voidreason " & _
                      " FROM rcapp_reference_tran " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapp_reference_tran.relationunkid AND cfcommon_master.isrefreeallowed = 1 AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & _
                      " WHERE  ISNULL(rcapp_reference_tran.isvoid, 0) = 0 AND  rcapp_reference_tran.applicantunkid = @applicantunkid "

            'Sohail (26 May 2017) - [serverreferencetranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]

            '", rcapp_reference_tran.isvoid " & _
            '        ", rcapp_reference_tran.voiduserunkid " & _
            '        ", rcapp_reference_tran.voiddatetime " & _
            '        ", rcapp_reference_tran.voidreason " & _

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
            objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 7, "Male"))
            objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 8, "Female"))
            objDataOperation.AddParameter("@other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 9, "Other"))

            dsList = objDataOperation.ExecQuery(strQ, "References")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dRow As DataRow = Nothing
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                dRow = mdtReferences.NewRow()
                dRow("referencetranunkid") = dsList.Tables(0).Rows(i)("referencetranunkid")
                dRow("applicantunkid") = dsList.Tables(0).Rows(i)("applicantunkid")
                dRow("name") = dsList.Tables(0).Rows(i)("name")
                dRow("address") = dsList.Tables(0).Rows(i)("address")
                dRow("countryunkid") = dsList.Tables(0).Rows(i)("countryunkid")
                dRow("stateunkid") = dsList.Tables(0).Rows(i)("stateunkid")
                dRow("cityunkid") = dsList.Tables(0).Rows(i)("cityunkid")
                dRow("email") = dsList.Tables(0).Rows(i)("email")
                dRow("genderunkid") = dsList.Tables(0).Rows(i)("genderunkid")
                dRow("gender") = dsList.Tables(0).Rows(i)("gender")
                dRow("position") = dsList.Tables(0).Rows(i)("position")
                dRow("telephone_no") = dsList.Tables(0).Rows(i)("telephone_no")
                dRow("mobile_no") = dsList.Tables(0).Rows(i)("mobile_no")
                dRow("relationunkid") = dsList.Tables(0).Rows(i)("relationunkid")
                dRow("relation") = dsList.Tables(0).Rows(i)("relation")
                'dRow("isvoid") = dsList.Tables(0).Rows(i)("isvoid")
                'dRow("voiduserunkid") = dsList.Tables(0).Rows(i)("voiduserunkid")
                'dRow("voiddatetime") = dsList.Tables(0).Rows(i)("voiddatetime")
                'dRow("voidreason") = dsList.Tables(0).Rows(i)("voidreason")


                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                dRow("serverreferencetranunkid") = dsList.Tables(0).Rows(i)("serverreferencetranunkid")
                dRow("isvoid") = dsList.Tables(0).Rows(i)("isvoid")
                dRow("voiduserunkid") = dsList.Tables(0).Rows(i)("voiduserunkid")
                If IsDBNull(dsList.Tables(0).Rows(i)("voiddatetime")) = True Then
                    dRow("voiddatetime") = DBNull.Value
                Else
                    dRow("voiddatetime") = dsList.Tables(0).Rows(i)("voiddatetime")
                End If
                dRow("voidreason") = dsList.Tables(0).Rows(i)("voidreason")
                'Sohail (26 May 2017) -- End
                dRow("AUD") = dsList.Tables(0).Rows(i)("AUD")
                mdtReferences.Rows.Add(dRow)

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicant_References; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcapp_reference_tran) </purpose>
    Public Function InsertUpdateDelete_ReferencesTran() As Boolean
        Dim i As Integer = 0
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception


        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            For i = 0 To mdtReferences.Rows.Count - 1
                With mdtReferences.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO rcapp_reference_tran ( " & _
                                         "  applicantunkid " & _
                                         ", name " & _
                                         ", address " & _
                                         ", countryunkid " & _
                                         ", stateunkid " & _
                                         ", cityunkid " & _
                                         ", email " & _
                                         ", gender " & _
                                         ", position " & _
                                         ", telephone_no " & _
                                         ", mobile_no " & _
                                         ", relationunkid " & _
                                         ", serverreferencetranunkid" & _
                                         ", isvoid" & _
                                         ", voiduserunkid" & _
                                         ", voiddatetime" & _
                                         ", voidreason" & _
                                       ") VALUES (" & _
                                         "  @applicantunkid " & _
                                         ", @name " & _
                                         ", @address " & _
                                         ", @countryunkid " & _
                                         ", @stateunkid " & _
                                         ", @cityunkid " & _
                                         ", @email " & _
                                         ", @gender " & _
                                         ", @position " & _
                                         ", @telephone_no " & _
                                         ", @mobile_no " & _
                                         ", @relationunkid " & _
                                         ", @serverreferencetranunkid" & _
                                         ", @isvoid" & _
                                         ", @voiduserunkid" & _
                                         ", @voiddatetime" & _
                                         ", @voidreason" & _
                                    "); SELECT @@identity"
                                'Sohail (26 May 2017) - [serverreferencetranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]

                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
                                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("name").ToString)
                                objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("address").ToString)
                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
                                objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("stateunkid").ToString)
                                objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("cityunkid").ToString)
                                objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("email").ToString)
                                objDataOperation.AddParameter("@gender", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("genderunkid").ToString)
                                objDataOperation.AddParameter("@position", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("position").ToString)
                                objDataOperation.AddParameter("@telephone_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("telephone_no").ToString)
                                objDataOperation.AddParameter("@mobile_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("mobile_no").ToString)
                                objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("relationunkid").ToString)
                                'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
                                'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
                                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtvoiddatetime <> Nothing, mdtvoiddatetime, DBNull.Value))
                                'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                                objDataOperation.AddParameter("@serverreferencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverreferencetranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Sohail (26 May 2017) -- End

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintReferencetranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("referencetranunkid") > 0 Then
                                    'Sohail (09 Oct 2018) -- Start
                                    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcapp_reference_tran", "referencetranunkid", mintReferencetranunkid, 2, 1) = False Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapp_reference_tran", "referencetranunkid", mintReferencetranunkid, 2, 1) = False Then
                                        'Sohail (09 Oct 2018) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapp_reference_tran", "referencetranunkid", mintReferencetranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"

                                strQ = "UPDATE rcapp_reference_tran SET " & _
                                            " name = @name" & _
                                            ", address = @address" & _
                                            ", countryunkid = @countryunkid" & _
                                            ", stateunkid = @stateunkid" & _
                                            ", cityunkid = @cityunkid" & _
                                            ", email = @email" & _
                                            ", gender = @gender" & _
                                            ", position = @position" & _
                                            ", telephone_no = @telephone_no" & _
                                            ", mobile_no = @mobile_no" & _
                                            ", relationunkid = @relationunkid " & _
                                            ", serverreferencetranunkid=@serverreferencetranunkid" & _
                                            ", isvoid = @isvoid" & _
                                            ", voiduserunkid = @voiduserunkid" & _
                                            ", voiddatetime = @voiddatetime" & _
                                            ", voidreason = @voidreason " & _
                                    " WHERE referencetranunkid = @referencetranunkid AND applicantunkid = @applicantunkid"
                                'Sohail (26 May 2017) - [serverreferencetranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]


                                objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("referencetranunkid").ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("applicantunkid").ToString)
                                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("name").ToString)
                                objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("address").ToString)
                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
                                objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("stateunkid").ToString)
                                objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("cityunkid").ToString)
                                objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("email").ToString)
                                objDataOperation.AddParameter("@gender", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("genderunkid").ToString)
                                objDataOperation.AddParameter("@position", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("position").ToString)
                                objDataOperation.AddParameter("@telephone_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("telephone_no").ToString)
                                objDataOperation.AddParameter("@mobile_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("mobile_no").ToString)
                                objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("relationunkid").ToString)
                                'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
                                'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
                                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtvoiddatetime <> Nothing, mdtvoiddatetime, DBNull.Value))
                                'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                                objDataOperation.AddParameter("@serverreferencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverreferencetranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Sohail (26 May 2017) -- End

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcapp_reference_tran", "referencetranunkid", .Item("referencetranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                If .Item("referencetranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcapp_reference_tran", "referencetranunkid", .Item("referencetranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                strQ = " DELETE FROM rcapp_reference_tran " & _
                                       " WHERE referencetranunkid = @referencetranunkid AND applicantunkid = @applicantunkid"

                                objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("referencetranunkid").ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("applicantunkid").ToString)
                                'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If



                        End Select
                    End If
                End With
            Next

            objDataOperation.ReleaseTransaction(True) 'Sohail (26 May 2017)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_ReferencesTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'objDataOperation = Nothing 'Sohail (26 May 2017)
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsEmployee_Master", 7, "Male")
			Language.setMessage("clsEmployee_Master", 8, "Female")
			Language.setMessage("clsEmployee_Master", 9, "Other")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class