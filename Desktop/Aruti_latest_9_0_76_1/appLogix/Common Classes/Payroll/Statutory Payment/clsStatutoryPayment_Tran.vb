﻿'************************************************************************************************************************************
'Class Name : clsStatutoryPayment_Tran.vb
'Purpose    :
'Date       :07/08/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.ComponentModel

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsStatutoryPayment_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsStatutoryPayment_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStatutorypaymenttranunkid As Integer
    Private mintStatutorypaymentmasterunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintMembershiptranunkid As Integer
    Private mdecEmp_Contribution As Decimal
    Private mdecEmpl_Contribution As Decimal
    Private mdecTotal_Contribution As Decimal
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mdtTable As DataTable

    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statutorypaymenttranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Statutorypaymenttranunkid() As Integer
        Get
            Return mintStatutorypaymenttranunkid
        End Get
        Set(ByVal value As Integer)
            mintStatutorypaymenttranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statutorypaymentmasterunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Statutorypaymentmasterunkid() As Integer
        Get
            Return mintStatutorypaymentmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintStatutorypaymentmasterunkid = value
            Call GetReceiptData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set membershiptranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Membershiptranunkid() As Integer
        Get
            Return mintMembershiptranunkid
        End Get
        Set(ByVal value As Integer)
            mintMembershiptranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_contribution
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Emp_Contribution() As Decimal
        Get
            Return mdecEmp_Contribution
        End Get
        Set(ByVal value As Decimal)
            mdecEmp_Contribution = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empl_contribution
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Empl_Contribution() As Decimal
        Get
            Return mdecEmpl_Contribution
        End Get
        Set(ByVal value As Decimal)
            mdecEmpl_Contribution = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set total_contribution
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Total_Contribution() As Decimal
        Get
            Return mdecTotal_Contribution
        End Get
        Set(ByVal value As Decimal)
            mdecTotal_Contribution = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public ReadOnly Property _Datatable() As DataTable
        Get
            Return mdtTable
        End Get
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  statutorypaymenttranunkid " & _
    ", statutorypaymentmasterunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", membershiptranunkid " & _
              ", emp_contribution " & _
              ", empl_contribution " & _
              ", total_contribution " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prstatutorypayment_tran " & _
             "WHERE statutorypaymenttranunkid = @statutorypaymenttranunkid "

            objDataOperation.AddParameter("@statutorypaymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymenttranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStatutorypaymenttranunkid = CInt(dtRow.Item("statutorypaymenttranunkid"))
                mintStatutorypaymentmasterunkid = CInt(dtRow.Item("statutorypaymentmasterunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintMembershiptranunkid = CInt(dtRow.Item("membershiptranunkid"))
                mdecEmp_Contribution = dtRow.Item("emp_contribution")
                mdecEmpl_Contribution = dtRow.Item("empl_contribution")
                mdecTotal_Contribution = dtRow.Item("total_contribution")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    Private Sub GetReceiptData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  prstatutorypayment_tran.statutorypaymenttranunkid " & _
              ", prstatutorypayment_tran.statutorypaymentmasterunkid " & _
              ", prstatutorypayment_tran.periodunkid " & _
              ", prstatutorypayment_tran.employeeunkid " & _
              ", prstatutorypayment_tran.membershiptranunkid " & _
              ", prstatutorypayment_tran.emp_contribution " & _
              ", prstatutorypayment_tran.empl_contribution " & _
              ", prstatutorypayment_tran.total_contribution " & _
              ", prstatutorypayment_tran.userunkid " & _
              ", prstatutorypayment_tran.isvoid " & _
              ", prstatutorypayment_tran.voiduserunkid " & _
              ", prstatutorypayment_tran.voiddatetime " & _
              ", prstatutorypayment_tran.voidreason " & _
             "FROM prstatutorypayment_tran " & _
             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prstatutorypayment_tran.periodunkid " & _
             "WHERE statutorypaymentmasterunkid = @statutorypaymentmasterunkid "

            strQ &= " ORDER BY cfcommon_period_tran.end_date "

            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymentmasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable = New DataView(dsList.Tables("List")).ToTable

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetReceiptData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                          , Optional ByVal intPeriodUnkId As Integer = 0 _
                          , Optional ByVal intEmployeeUnkId As Integer = 0 _
                          , Optional ByVal intMembershipUnkId As Integer = 0 _
                          , Optional ByVal strReceiptNo As String = "" _
                          , Optional ByVal dtReceiptDate As Date = Nothing _
                          , Optional ByVal strFilter As String = "" _
                          ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  prstatutorypayment_tran.statutorypaymenttranunkid  " & _
                          ", prstatutorypayment_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_code " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", prstatutorypayment_tran.employeeunkid " & _
                          ", hremployee_master.employeecode " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename  " & _
                          ", prstatutorypayment_tran.membershiptranunkid " & _
                          ", hrmembership_master.membershipunkid " & _
                          ", hrmembership_master.membershipcode " & _
                          ", hrmembership_master.membershipname " & _
                          ", hremployee_meminfo_tran.membershipno " & _
                          ", prstatutorypayment_tran.receiptno " & _
                          ", CONVERT(CHAR(8), prstatutorypayment_tran.receiptdate, 112) AS receiptdate " & _
                          ", prstatutorypayment_tran.emp_contribution " & _
                          ", prstatutorypayment_tran.empl_contribution " & _
                          ", prstatutorypayment_tran.total_contribution " & _
                          ", prstatutorypayment_tran.total_payableamount " & _
                          ", prstatutorypayment_tran.total_paidamount " & _
                          ", prstatutorypayment_tran.remark " & _
                          ", prstatutorypayment_tran.guid " & _
                          ", prstatutorypayment_tran.userunkid " & _
                          ", prstatutorypayment_tran.isvoid " & _
                          ", prstatutorypayment_tran.voiduserunkid " & _
                          ", prstatutorypayment_tran.voiddatetime " & _
                          ", prstatutorypayment_tran.voidreason " & _
                    "FROM    prstatutorypayment_tran " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prstatutorypayment_tran.employeeunkid " & _
                            "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershiptranunkid = prstatutorypayment_tran.membershiptranunkid " & _
                            "LEFT JOIN hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prstatutorypayment_tran.periodunkid " & _
                    "WHERE   ISNULL(prstatutorypayment_tran.isvoid, 0) = 0 "

            If intPeriodUnkId > 0 Then
                strQ &= " AND prstatutorypayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND prstatutorypayment_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If intMembershipUnkId > 0 Then
                strQ &= " AND hrmembership_master.membershipunkid = @membershipunkid "
                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMembershipUnkId)
            End If

            If strReceiptNo.Trim <> "" Then
                strQ &= " AND prstatutorypayment_tran.receiptno = @membershipunkid "
                objDataOperation.AddParameter("@membershipunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReceiptNo)
            End If

            If dtReceiptDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), prstatutorypayment_tran.receiptdate, 112) = @receiptdate "
                objDataOperation.AddParameter("@receiptdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtReceiptDate))
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prstatutorypayment_tran) </purpose>
    Public Function Insert(ByVal objDataOp As clsDataOperation, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        If isExist(mintPeriodunkid, mintEmployeeunkid, mintMembershiptranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Statutory Payment already Exists.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
            objDataOperation.BindTransaction()
        End If

        Try
            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymentmasterunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershiptranunkid.ToString)
            objDataOperation.AddParameter("@emp_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmp_Contribution.ToString)
            objDataOperation.AddParameter("@empl_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmpl_Contribution.ToString)
            objDataOperation.AddParameter("@total_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_Contribution.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO prstatutorypayment_tran ( " & _
                            "  statutorypaymentmasterunkid " & _
                            ", periodunkid " & _
                            ", employeeunkid " & _
                            ", membershiptranunkid " & _
                            ", emp_contribution " & _
                            ", empl_contribution " & _
                            ", total_contribution " & _
                            ", userunkid " & _
                            ", isvoid " & _
                            ", voiduserunkid " & _
                            ", voiddatetime " & _
                            ", voidreason" & _
                   ") VALUES (" & _
                            "  @statutorypaymentmasterunkid " & _
                            ", @periodunkid " & _
                            ", @employeeunkid " & _
                            ", @membershiptranunkid " & _
                            ", @emp_contribution " & _
                            ", @empl_contribution " & _
                            ", @total_contribution " & _
                            ", @userunkid " & _
                            ", @isvoid " & _
                            ", @voiduserunkid " & _
                            ", @voiddatetime " & _
                            ", @voidreason" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStatutorypaymenttranunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStatutoryPaymentTran(objDataOperation, 1, True) = False Then
            If InsertAuditTrailForStatutoryPaymentTran(objDataOperation, 1, dtCurrentDateAndTime, True) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAll(ByVal intStatutorypaymentmasterunkid As Integer, ByVal dtTable As DataTable, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        objDataOperation = New clsDataOperation

        Try

            objDataOperation.BindTransaction()


            For Each dtRow As DataRow In dtTable.Rows
                objDataOperation.ClearParameters()

                mintStatutorypaymentmasterunkid = intStatutorypaymentmasterunkid
                mintPeriodunkid = dtRow.Item("periodunkid").ToString
                mintEmployeeunkid = dtRow.Item("employeeunkid").ToString
                mintMembershiptranunkid = dtRow.Item("membershiptranunkid").ToString
                mdecEmp_Contribution = CDec(dtRow.Item("emp_contribution").ToString)
                mdecEmpl_Contribution = CDec(dtRow.Item("empl_contribution").ToString)
                mdecTotal_Contribution = CDec(dtRow.Item("total_contribution").ToString)

                mintUserunkid = dtRow.Item("userunkid").ToString
                mblnIsvoid = dtRow.Item("isvoid").ToString
                mintVoiduserunkid = dtRow.Item("voiduserunkid").ToString
                If dtRow.Item("Voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("Voiddatetime").ToString
                End If
                mstrVoidreason = dtRow.Item("Voidreason").ToString


                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Insert(objDataOperation) = False Then
                If Insert(objDataOperation, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If


            Next

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prstatutorypayment_tran) </purpose>
    Public Function Update(ByVal objDataOp As clsDataOperation, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        If isExist(mintPeriodunkid, mintEmployeeunkid, mintMembershiptranunkid, mintStatutorypaymenttranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Statutory Payment already Exists.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
            objDataOperation.BindTransaction()
        End If

        Try
            objDataOperation.AddParameter("@statutorypaymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymenttranunkid.ToString)
            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymentmasterunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershiptranunkid.ToString)
            objDataOperation.AddParameter("@emp_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmp_Contribution.ToString)
            objDataOperation.AddParameter("@empl_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmpl_Contribution.ToString)
            objDataOperation.AddParameter("@total_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_Contribution.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE prstatutorypayment_tran SET " & _
                        "  statutorypaymentmasterunkid = @statutorypaymentmasterunkid" & _
                        ", periodunkid = @periodunkid" & _
                        ", employeeunkid = @employeeunkid" & _
                        ", membershiptranunkid = @membershiptranunkid" & _
                        ", emp_contribution = @emp_contribution" & _
                        ", empl_contribution = @empl_contribution" & _
                        ", total_contribution = @total_contribution" & _
                        ", userunkid = @userunkid" & _
                        ", isvoid = @isvoid" & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voidreason = @voidreason " & _
                      "WHERE statutorypaymenttranunkid = @statutorypaymenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStatutoryPaymentTran(objDataOperation, 2) = False Then
            If InsertAuditTrailForStatutoryPaymentTran(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(True)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prstatutorypayment_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal objDataOp As clsDataOperation, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
            objDataOperation.BindTransaction()
        End If

        Try

            strQ = "UPDATE prstatutorypayment_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime " & _
                     ", voidreason = @voidreason " & _
           "WHERE statutorypaymenttranunkid = @statutorypaymenttranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@statutorypaymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mdtVoiddatetime = Nothing Then
            '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            'End If
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _DataOperation = objDataOperation
            Me._Statutorypaymenttranunkid = intUnkid

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStatutoryPaymentTran(objDataOperation, 3) = False Then
            If InsertAuditTrailForStatutoryPaymentTran(objDataOperation, 3, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidByMasterUnkID(ByVal intMasterUnkId As Integer _
                                    , ByVal UserId As Integer _
                                    , ByVal VoidDateTime As DateTime _
                                    , ByVal strVoidReason As String _
                                    , Optional ByVal objDataOp As clsDataOperation = Nothing _
                                    ) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
            objDataOperation.BindTransaction()
        End If

        Try

            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterUnkId)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

            strQ = "INSERT INTO atprstatutorypayment_tran " & _
                                "(     statutorypaymenttranunkid, statutorypaymentmasterunkid, periodunkid, employeeunkid, membershiptranunkid, emp_contribution, empl_contribution, total_contribution, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, loginemployeeunkid ) " & _
                    "SELECT         statutorypaymenttranunkid, statutorypaymentmasterunkid, periodunkid, employeeunkid, membershiptranunkid, emp_contribution, empl_contribution, total_contribution, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb, @loginemployeeunkid " & _
                                   "FROM prstatutorypayment_tran " & _
                    "WHERE isvoid = 0 " & _
                    "AND statutorypaymentmasterunkid = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE  prstatutorypayment_tran SET " & _
                         "  isvoid = @isvoid" & _
                         ", voiduserunkid = @voiduserunkid" & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                   "WHERE isvoid = 0 " & _
                   "AND statutorypaymentmasterunkid = @statutorypaymentmasterunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidByMasterUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(ByVal strUnkIdList As String, ByVal UserId As Integer, ByVal VoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal bw As BackgroundWorker = Nothing) As Boolean

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strQP As String = ""
        Dim strQS As String = ""
        Dim exForce As Exception
        Dim intUnkid As Integer
        Dim arrIDs() As String

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            arrIDs = strUnkIdList.Split(",")

            If bw IsNot Nothing Then
                bw.ReportProgress(0)
            End If
            Dim intCount As Integer = 0

            For i = 0 To arrIDs.Count - 1
                intUnkid = CInt(arrIDs(i))
                mblnIsvoid = True
                mintVoiduserunkid = UserId
                mdtVoiddatetime = VoidDateTime
                mstrVoidreason = strVoidReason

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Delete(intUnkid, objDataOperation) = False Then
                If Delete(intUnkid, objDataOperation, VoidDateTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                If bw IsNot Nothing Then
                    intCount = intCount + 1
                    bw.ReportProgress(intCount)
                End If
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@statutorypaymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intPeriodUnkId As Integer, ByVal intEmployeeUnkId As Integer, ByVal intMembershiptranunkid As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByRef intUnkIdIfExist As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  statutorypaymenttranunkid " & _
             "FROM prstatutorypayment_tran " & _
             "WHERE ISNULL(isvoid, 0) = 0 " & _
             "AND periodunkid = @periodunkid " & _
             "AND employeeunkid = @employeeunkid " & _
             "AND membershiptranunkid = @membershiptranunkid "

            If intUnkid > 0 Then
                strQ &= " AND statutorypaymenttranunkid <> @statutorypaymenttranunkid"
                objDataOperation.AddParameter("@statutorypaymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMembershiptranunkid)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkIdIfExist = CInt(dsList.Tables(0).Rows(0).Item("statutorypaymenttranunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrailForStatutoryPaymentTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal blnIsGlobal As Boolean = False) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

            strQ = "INSERT INTO atprstatutorypayment_tran ( " & _
                              "  statutorypaymenttranunkid " & _
                              ", statutorypaymentmasterunkid " & _
                              ", periodunkid " & _
                              ", employeeunkid " & _
                              ", membershiptranunkid " & _
                              ", emp_contribution " & _
                              ", empl_contribution " & _
                              ", total_contribution " & _
                              ", audittype " & _
                              ", audituserunkid " & _
                              ", auditdatetime " & _
                              ", ip " & _
                              ", machine_name" & _
                              ", form_name " & _
                              ", module_name1 " & _
                              ", module_name2 " & _
                              ", module_name3 " & _
                              ", module_name4 " & _
                              ", module_name5 " & _
                              ", isweb " & _
                              ", loginemployeeunkid " & _
                    ") VALUES (" & _
                              "  @statutorypaymenttranunkid " & _
                              ", @statutorypaymentmasterunkid " & _
                              ", @periodunkid " & _
                              ", @employeeunkid " & _
                              ", @membershiptranunkid " & _
                              ", @emp_contribution " & _
                              ", @empl_contribution " & _
                              ", @total_contribution " & _
                              ", @audittype " & _
                              ", @audituserunkid " & _
                              ", @auditdatetime " & _
                              ", @ip " & _
                              ", @machine_name" & _
                              ", @form_name " & _
                              ", @module_name1 " & _
                              ", @module_name2 " & _
                              ", @module_name3 " & _
                              ", @module_name4 " & _
                              ", @module_name5 " & _
                              ", @isweb " & _
                              ", @loginemployeeunkid " & _
                    "); SELECT @@identity"




            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@statutorypaymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymenttranunkid.ToString)
            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymentmasterunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershiptranunkid.ToString)
            objDataOperation.AddParameter("@emp_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmp_Contribution.ToString)
            objDataOperation.AddParameter("@empl_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmpl_Contribution.ToString)
            objDataOperation.AddParameter("@total_contribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_Contribution.ToString)

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)


            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForStatutoryPaymentTran", mstrModuleName)
            Return False
        Finally
            'objDataOperation = Nothing
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, This Statutory Payment already Exists.")
            Language.setMessage(mstrModuleName, 2, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class