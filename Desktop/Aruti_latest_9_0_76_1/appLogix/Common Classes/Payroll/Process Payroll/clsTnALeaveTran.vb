﻿'************************************************************************************************************************************
'Class Name : clsTnALeaveTran.vb
'Purpose    :
'Date       :17/07/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END
Imports System.ComponentModel 'Sohail (22 Aug 2012)
Imports System

''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsTnALeaveTran
    Private Shared ReadOnly mstrModuleName As String = "clsTnALeaveTran"
    Dim objDataOperation As clsDataOperation
    Dim objProcessPayroll As New clsPayrollProcessTran
    Dim mstrMessage As String = ""

    'Sohail (12 Oct 2011) -- Start
    Private strIP As String
    Private strHost As String
#Region " Constructor "
    Public Sub New()
        Try
            strIP = getIP()
            strHost = getHostName()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (12 Oct 2011) -- End

#Region " Private variables "
    Private mintTnaleavetranunkid As Integer
    Private mintPayperiodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrVoucherno As String = String.Empty
    Private mdtProcessdate As Date
    Private mdblDaysinperiod As Double
    Private mdblPaidholidays As Double
    Private mdblTotalpayabledays As Double
    Private mdblTotalpayablehours As Double
    Private mdblTotaldaysworked As Double
    Private mdblTotalhoursworked As Double
    Private mdblTotalabsentorleave As Double
    Private mdblTotalextrahours As Double
    Private mdblTotalshorthours As Double
    Private mdblTotalonholddays As Double
    'Sohail (11 May 2011) -- Start
    Private mdecTotal_amount As Decimal
    Private mdecBalanceamount As Decimal
    'Sohail (11 May 2011) -- End
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdecOpeningBalance As Decimal = 0 'Sohail (04 Mar 2011), 'Sohail (11 May 2011)-DECIMAL
    'Sohail (12 Oct 2011) -- Start
    Private mdtauditdatetime As DateTime
    Private mdecBasicsalary As Decimal
    Private mstrIP As String
    Private mstrHost As String
    Private mstrVoid_IP As String
    Private mstrVoid_Host As String
    'Sohail (12 Oct 2011) -- End
    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    Private mdtTnA_StartDate As Date
    Private mdtTnA_Processdate As Date
    'Sohail (07 Jan 2014) -- End
    Private mdtTnA_EndDate As Date 'Sohail (18 Jun 2014)

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    'Sohail (24 Apr 2018) -- Start
    'Voltamp Enhancement : Each head shoud be rounded before using in addition in formula in 71.1.
    Private mstrFmtCurrency As String = GUI.fmtCurrency
    'Sohail (24 Apr 2018) -- End
    'Sohail (28 Apr 2018) -- Start
    'Enhancement : Ref. No. 231 - WebAPI for seamless integration between Aruti and EPICOR in 72.1.
    Private mintCostcenterunkid As Integer
    'Sohail (28 Apr 2018) -- End
    'Sohail (03 May 2018) -- Start
    'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
    Private mintCountryunkid As Integer
    'Sohail (03 May 2018) -- End
    'Sohail (09 Oct 2019) -- Start
    'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
    Private mdtEmployee_enddate As Date
    'Sohail (09 Oct 2019) -- End

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END
    Private mdblRoundOff_Type As Double = ConfigParameter._Object._RoundOff_Type 'Sohail (01 Mar 2016)

    'Sohail (26 Aug 2016) -- Start
    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    Dim strB As New System.Text.StringBuilder
    'Sohail (26 Aug 2016) -- End
    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Private mintAdvance_CostCenterunkid As Integer = 0
    'Sohail (14 Mar 2019) -- End

    'Sohail (09 Oct 2019) -- Start
    'NMB Enhancement # : option Exclude employees from payroll during Suspension period to prorate salary and flat rate heads on employee suspension will be given on configuration.
    Private mblnExcludeEmpFromPayrollDuringSuspension As Boolean = False
    'Sohail (09 Oct 2019) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    'Sohail (02 Jan 2017) -- Start
    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
    ''' <summary>
    ''' Purpose: Get or Set tnaleavetranunkid
    ''' Modify By: Suhail
    ''' </summary>
    '''Public Property _Tnaleavetranunkid() As Integer
    '''    Get
    '''        Return mintTnaleavetranunkid
    '''    End Get
    '''    Set(ByVal value As Integer)
    '''        mintTnaleavetranunkid = value
    '''        Call GetData()
    '''    End Set
    '''End Property
    Public Property _Tnaleavetranunkid(ByVal xDataOp As clsDataOperation) As Integer
        Get
            Return mintTnaleavetranunkid
        End Get
        Set(ByVal value As Integer)
            mintTnaleavetranunkid = value
            Call GetData(xDataOp)
        End Set
    End Property
    'Sohail (02 Jan 2017) -- End

    ''' <summary>
    ''' Purpose: Get or Set payperiodunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Payperiodunkid() As Integer
        Get
            Return mintPayperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintPayperiodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voucherno
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voucherno() As String
        Get
            Return mstrVoucherno
        End Get
        Set(ByVal value As String)
            mstrVoucherno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processdate
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Processdate() As Date
        Get
            Return mdtProcessdate
        End Get
        Set(ByVal value As Date)
            mdtProcessdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set daysinperiod
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Daysinperiod() As Double
        Get
            Return mdblDaysinperiod
        End Get
        Set(ByVal value As Double)
            mdblDaysinperiod = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paidholidays
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Paidholidays() As Double
        Get
            Return mdblPaidholidays
        End Get
        Set(ByVal value As Double)
            mdblPaidholidays = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalpayabledays
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Totalpayabledays() As Double
        Get
            Return mdblTotalpayabledays
        End Get
        Set(ByVal value As Double)
            mdblTotalpayabledays = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalpayablehours
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Totalpayablehours() As Double
        Get
            Return mdblTotalpayablehours
        End Get
        Set(ByVal value As Double)
            mdblTotalpayablehours = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totaldaysworked
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Totaldaysworked() As Double
        Get
            Return mdblTotaldaysworked
        End Get
        Set(ByVal value As Double)
            mdblTotaldaysworked = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalhoursworked
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Totalhoursworked() As Double
        Get
            Return mdblTotalhoursworked
        End Get
        Set(ByVal value As Double)
            mdblTotalhoursworked = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalabsentorleave
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Totalabsentorleave() As Double
        Get
            Return mdblTotalabsentorleave
        End Get
        Set(ByVal value As Double)
            mdblTotalabsentorleave = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalextrahours
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Totalextrahours() As Double
        Get
            Return mdblTotalextrahours
        End Get
        Set(ByVal value As Double)
            mdblTotalextrahours = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalshorthours
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Totalshorthours() As Double
        Get
            Return mdblTotalshorthours
        End Get
        Set(ByVal value As Double)
            mdblTotalshorthours = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalonholddays
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Totalonholddays() As Double
        Get
            Return mdblTotalonholddays
        End Get
        Set(ByVal value As Double)
            mdblTotalonholddays = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set balanceamount
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Total_amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecTotal_amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecTotal_amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set balanceamount
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Balanceamount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecBalanceamount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecBalanceamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Sohail (04 Mar 2011) -- Start
    Public Property _OpeningBalance() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecOpeningBalance
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecOpeningBalance = value
        End Set
    End Property
    'Sohail (04 Mar 2011) -- End

    'Sohail (12 Oct 2011) -- Start
    Public Property _Auditdatetime() As DateTime
        Get
            Return mdtauditdatetime
        End Get
        Set(ByVal value As DateTime)
            mdtauditdatetime = value
        End Set
    End Property

    Public Property _BasicSalary() As Decimal
        Get
            Return mdecBasicsalary
        End Get
        Set(ByVal value As Decimal)
            mdecBasicsalary = value
        End Set
    End Property

    Public Property _Ip() As String
        Get
            Return mstrIP
        End Get
        Set(ByVal value As String)
            mstrIP = value
        End Set
    End Property

    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    Public Property _Void_Ip() As String
        Get
            Return mstrVoid_IP
        End Get
        Set(ByVal value As String)
            mstrVoid_IP = value
        End Set
    End Property

    Public Property _Void_Host() As String
        Get
            Return mstrVoid_Host
        End Get
        Set(ByVal value As String)
            mstrVoid_Host = value
        End Set
    End Property
    'Sohail (12 Oct 2011) -- End

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    ''' <summary>
    ''' Purpose: Get or Set tna_processdate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _TnA_Startdate() As Date
        Get
            Return mdtTnA_StartDate
        End Get
        Set(ByVal value As Date)
            mdtTnA_StartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tna_processdate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _TnA_Processdate() As Date
        Get
            Return mdtTnA_Processdate
        End Get
        Set(ByVal value As Date)
            mdtTnA_Processdate = value
        End Set
    End Property
    'Sohail (07 Jan 2014) -- End

    'Sohail (18 Jun 2014) -- Start
    'Enhancement - TOTAL DAYS IN TnA and Leave PERIOD for Voltamp. 
    Public Property _TnA_Enddate() As Date
        Get
            Return mdtTnA_EndDate
        End Get
        Set(ByVal value As Date)
            mdtTnA_EndDate = value
        End Set
    End Property
    'Sohail (18 Jun 2014) -- End

    'Sohail (01 Mar 2016) -- Start
    'Enhancement - Rounding issue for Net Pay on Payroll Report, not matching with Payslip Report.
    Public Property _RoundOff_Type() As Double
        Get
            Return mdblRoundOff_Type
        End Get
        Set(ByVal value As Double)
            mdblRoundOff_Type = value
        End Set
    End Property
    'Sohail (01 Mar 2016) -- End

    'Sohail (24 Apr 2018) -- Start
    'Voltamp Enhancement : Each head shoud be rounded before using in addition in formula in 71.1.
    Public Property _FmtCurrency() As String
        Get
            Return mstrFmtCurrency
        End Get
        Set(ByVal value As String)
            mstrFmtCurrency = value
        End Set
    End Property
    'Sohail (24 Apr 2018) -- End

    'Sohail (28 Apr 2018) -- Start
    'Enhancement : Ref. No. 231 - WebAPI for seamless integration between Aruti and EPICOR in 72.1.
    ''' <summary>
    ''' Purpose: Get or Set costcenterunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = value
        End Set
    End Property
    'Sohail (28 Apr 2018) -- End

    'Sohail (09 Oct 2019) -- Start
    'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
    ''' <summary>
    ''' Purpose: Get or Set employee_enddate
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Employee_enddate() As Date
        Get
            Return mdtEmployee_enddate
        End Get
        Set(ByVal value As Date)
            mdtEmployee_enddate = value
        End Set
    End Property
    'Sohail (09 Oct 2019) -- End

    'Sohail (03 May 2018) -- Start
    'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            'Sohail (09 Oct 2019) -- Start
            'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
            mintCountryunkid = value
            'Sohail (09 Oct 2019) -- End
        End Set
    End Property
    'Sohail (03 May 2018) -- End

    'Sohail (25 Jun 2018) -- Start
    'Internal Enhancement - Showing Progress count on Aruti Self Service for Process Payroll in 72.1.
    Private Shared mintProgressTotalCount As Integer
    Public Shared Property _ProgressTotalCount() As Integer
        Get
            Return mintProgressTotalCount
        End Get
        Set(ByVal value As Integer)
            mintProgressTotalCount = value
        End Set
    End Property

    Private Shared mintProgressCurrCount As Integer
    Public Shared Property _ProgressCurrCount() As Integer
        Get
            Return mintProgressCurrCount
        End Get
        Set(ByVal value As Integer)
            mintProgressCurrCount = value
        End Set
    End Property
    'Sohail (25 Jun 2018) -- End

    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Public WriteOnly Property _Advance_CostCenterunkid() As Integer
        Set(ByVal value As Integer)
            mintAdvance_CostCenterunkid = value
        End Set
    End Property
    'Sohail (14 Mar 2019) -- End

    'Sohail (09 Oct 2019) -- Start
    'NMB Enhancement # : option Exclude employees from payroll during Suspension period to prorate salary and flat rate heads on employee suspension will be given on configuration.
    Public WriteOnly Property _ExcludeEmpFromPayrollDuringSuspension() As Boolean
        Set(ByVal value As Boolean)
            mblnExcludeEmpFromPayrollDuringSuspension = value
        End Set
    End Property
    'Sohail (09 Oct 2019) -- End

#End Region

#Region " Issues & Solutions "
    'Sohail (21 Mar 2017) -- Start
    '#1. Issue - 65.1 - Value was either tool large or too small for a Decimal.
    '    Solutions : Edit Shift and click on Save button.
    '#2. Issue - 65.1 - Conversion from type DBNull to type Integer is not valid.
    '    Solutions : Missing allocations.
    'Sohail (21 Mar 2017) -- End
#End Region
    

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOp As clsDataOperation = Nothing)
        'Sohail (02 Jan 2017) - [xDataOp]
        Dim dsList As DataSet = Nothing
        'Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (02 Jan 2017) -- Start
        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (02 Jan 2017) -- End

        Try
            strB.Length = 0
            strB.Append("SELECT " & _
              "  tnaleavetranunkid " & _
              ", payperiodunkid " & _
              ", employeeunkid " & _
              ", voucherno " & _
              ", processdate " & _
              ", daysinperiod " & _
              ", paidholidays " & _
              ", totalpayabledays " & _
              ", totalpayablehours " & _
              ", totaldaysworked " & _
              ", totalhoursworked " & _
              ", totalabsentorleave " & _
              ", totalextrahours " & _
              ", totalshorthours " & _
              ", ISNULL(totalonholddays,0) AS totalonholddays " & _
              ", total_amount " & _
              ", balanceamount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", openingbalance " & _
              ", ISNULL(basicsalary, 0) AS basicsalary " & _
              ", ISNULL(auditdatetime, processdate) AS auditdatetime " & _
              ", ISNULL(ip, '') AS ip " & _
              ", ISNULL(host, '') AS host " & _
              ", ISNULL(void_ip, '') AS void_ip " & _
              ", ISNULL(void_host, '') AS void_host " & _
              ", ISNULL(tna_processdate, processdate) AS tna_processdate " & _
              ", ISNULL(costcenterunkid, 0) AS costcenterunkid " & _
              ", ISNULL(employee_enddate, processdate) AS employee_enddate " & _
             "FROM prtnaleave_tran " & _
             "WHERE tnaleavetranunkid = @tnaleavetranunkid " _
             )
            'Sohail (09 Oct 2019) - [employee_enddate]
            'Sohail (28 Apr 2018) - [costcenterunkid]
            'Sohail (07 Jan 2014) - [tna_processdate]
            'Sohail (04 Mar 2011) - [openingbalance], 'Sohail (12 Oct 2011) - [basicsalary, auditdatetime, ip, host, void_ip, void_host]

            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintTnaleaveTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                minttnaleavetranunkid = CInt(dtRow.Item("tnaleavetranunkid"))
                mintpayperiodunkid = CInt(dtRow.Item("payperiodunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrvoucherno = dtRow.Item("voucherno").ToString
                mdtProcessdate = dtRow.Item("processdate")
                mdblDaysinperiod = CDec(dtRow.Item("daysinperiod"))
                mdblPaidholidays = CDec(dtRow.Item("paidholidays"))
                mdblTotalpayabledays = CDec(dtRow.Item("totalpayabledays"))
                mdbltotalpayablehours = CDec(dtRow.Item("totalpayablehours"))
                mdbltotaldaysworked = CDec(dtRow.Item("totaldaysworked"))
                mdbltotalhoursworked = CDec(dtRow.Item("totalhoursworked"))
                mdblTotalabsentorleave = CDec(dtRow.Item("totalabsentorleave"))
                mdbltotalextrahours = CDec(dtRow.Item("totalextrahours"))
                mdblTotalshorthours = CDec(dtRow.Item("totalshorthours"))
                mdblTotalonholddays = CDec(dtRow.Item("totalonholddays"))
                mdecTotal_amount = CDec(dtRow.Item("total_amount")) 'Sohail (11 May 2011)
                mdecBalanceamount = CDec(dtRow.Item("balanceamount")) 'Sohail (11 May 2011)
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdecOpeningBalance = CDec(dtRow.Item("openingbalance")) 'Sohail (04 Mar 2011), 'Sohail (11 May 2011)
                'Sohail (12 Oct 2011) -- Start
                mdecBasicsalary = CDec(dtRow.Item("basicsalary"))
                mdtauditdatetime = dtRow.Item("auditdatetime")
                mstrIP = dtRow.Item("ip")
                mstrHost = dtRow.Item("host")
                mstrIP = dtRow.Item("void_ip")
                mstrHost = dtRow.Item("void_host")
                'Sohail (12 Oct 2011) -- End
                mdtTnA_Processdate = dtRow.Item("tna_processdate") 'Sohail (07 Jan 2014)
                'Sohail (28 Apr 2018) -- Start
                'Enhancement : Ref. No. 231 - WebAPI for seamless integration between Aruti and EPICOR in 72.1.
                mintCostcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                'Sohail (28 Apr 2018) -- End
                'Sohail (09 Oct 2019) -- Start
                'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
                If IsDBNull(dtRow.Item("employee_enddate")) = True Then
                    mdtEmployee_enddate = mdtProcessdate
                Else
                    mdtEmployee_enddate = dtRow.Item("employee_enddate")
                End If
                'Sohail (09 Oct 2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    '''Public Function GetList(ByVal strTableName As String, Optional ByVal intEmpUnkid As Integer = 0, _
    '''                        Optional ByVal intDeptID As Integer = 0, _
    '''                       Optional ByVal intSectionID As Integer = 0, _
    '''                        Optional ByVal intUnitID As Integer = 0, _
    '''                        Optional ByVal intGradeID As Integer = 0, _
    '''                        Optional ByVal intAccessID As Integer = 0, _
    '''                        Optional ByVal intClassID As Integer = 0, _
    '''                        Optional ByVal intCostCenterID As Integer = 0, _
    '''                       Optional ByVal intServiceID As Integer = 0, _
    '''                        Optional ByVal intJobID As Integer = 0, _
    '''                        Optional ByVal intPayPointID As Integer = 0, _
    '''                       Optional ByVal intYearID As Integer = 0, _
    '''                        Optional ByVal intPeriodID As Integer = 0, _
    '''                        Optional ByVal strSortField As String = "") As DataSet
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal intYearID As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal blnIncludeInActiveEmployee As Boolean _
                            , ByVal blnApplyUserAccessFilter As Boolean _
                            , ByVal strTableName As String _
                            , Optional ByVal intEmpUnkid As Integer = 0 _
                            , Optional ByVal intPeriodID As Integer = 0 _
                            , Optional ByVal strSortField As String = "" _
                            , Optional ByVal mstrAdvanceFilter As String = "" _
                            , Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False _
                            , Optional ByVal colSQLFilter As List(Of clsSQLFilterCollection) = Nothing _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            , Optional ByVal blnOnlyProcessedEmployees As Boolean = False _
                            ) As DataSet 'Sohail (12 Oct 2011) - [blnIncludeInActiveEmployee], 'Sohail (12 May 2012) - [blnExcludeTermEmp_PayProcess]
        'Sohail (08 Feb 2022) - [blnOnlyProcessedEmployees]
        'Sohail (03 May 2018) - [xDataOp]
        'Sohail (13 Oct 2017) - [colSQLFilter]
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, blnApplyUserAccessFilter]
        'Sohail (21 Aug 2015) - [Removed: intDeptID, intSectionID, intUnitID, intGradeID, intBranchId, intClassID, intCostCenterID, intServiceID, intJobID, intPayPointID]
        'Sohail (12 Jan 2015) - [strUserAccessLevelFilterString]
        'S.SANDEEP [ 17 AUG 2011 ] -- END 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , blnExcludeTermEmp_PayProcess, xDatabaseName)
        'Sohail (06 Jan 2016) -- Start
        'Enhancement - Show Close Year Process Logs on Close Year Wizard.
        'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, intYearID, xUserModeSetting)
        If blnApplyUserAccessFilter = True Then
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, intYearID, xUserModeSetting)
        End If
        'Sohail (06 Jan 2016) -- End
        'Sohail (12 Oct 2021) -- Start
        'NMB Issue :  : Performance issue on payslip report in new ui.
        'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        If mstrAdvanceFilter.Trim <> "" Then
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        End If
        'Sohail (12 Oct 2021) -- End
        'Sohail (21 Aug 2015) -- End

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            'Sohail (27 Aug 2010) -- Start
            'Changes:Get Year name from hrmsconfiguration database
            'strQ = "SELECT " & _
            '  "  prtnaleave_tran.tnaleavetranunkid " & _
            '  ", cffinancial_year_tran.yearunkid " & _
            '  ", ISNULL(cffinancial_year_tran.financialyear_name,'') AS YearName " & _
            '  ", prtnaleave_tran.payperiodunkid " & _
            '  ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
            '  ", prtnaleave_tran.employeeunkid " & _
            '  ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
            '  ", prtnaleave_tran.voucherno " & _
            '  ", prtnaleave_tran.processdate " & _
            '  ", prtnaleave_tran.daysinperiod " & _
            '  ", prtnaleave_tran.paidholidays " & _
            '  ", prtnaleave_tran.totalpayabledays " & _
            '  ", prtnaleave_tran.totalpayablehours " & _
            '  ", prtnaleave_tran.totaldaysworked " & _
            '  ", prtnaleave_tran.totalhoursworked " & _
            '  ", prtnaleave_tran.totalabsentorleave " & _
            '  ", prtnaleave_tran.totalextrahours " & _
            '  ", prtnaleave_tran.totalshorthours " & _
            '  ", ISNULL(prtnaleave_tran.totalonholddays,0) AS totalonholddays " & _
            '  ", prtnaleave_tran.total_amount " & _
            '  ", prtnaleave_tran.balanceamount " & _
            '  ", prtnaleave_tran.userunkid " & _
            '  ", prtnaleave_tran.isvoid " & _
            '  ", prtnaleave_tran.voiduserunkid " & _
            '  ", prtnaleave_tran.voiddatetime " & _
            '  ", prtnaleave_tran.voidreason " & _
            '"FROM prtnaleave_tran " & _
            '"LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '"LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid=prtnaleave_tran.payperiodunkid " & _
            '"LEFT JOIN cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
            '"WHERE prtnaleave_tran.isvoid = 0 "

            'S.SANDEEP [15 NOV 2016] -- START
            'ENHANCEMENT : QUERY OPTIMIZATION
            'strQ = "SELECT " & _
            '  "  prtnaleave_tran.tnaleavetranunkid " & _
            '  ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
            '  ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
            '  ", prtnaleave_tran.payperiodunkid " & _
            '  ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
            '  ", prtnaleave_tran.employeeunkid " & _
            '  ", hremployee_master.employeecode " & _
            '  ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
            '  ", prtnaleave_tran.voucherno " & _
            '  ", prtnaleave_tran.processdate " & _
            '  ", prtnaleave_tran.daysinperiod " & _
            '  ", prtnaleave_tran.paidholidays " & _
            '  ", prtnaleave_tran.totalpayabledays " & _
            '  ", prtnaleave_tran.totalpayablehours " & _
            '  ", prtnaleave_tran.totaldaysworked " & _
            '  ", prtnaleave_tran.totalhoursworked " & _
            '  ", prtnaleave_tran.totalabsentorleave " & _
            '  ", prtnaleave_tran.totalextrahours " & _
            '  ", prtnaleave_tran.totalshorthours " & _
            '  ", ISNULL(prtnaleave_tran.totalonholddays,0) AS totalonholddays " & _
            '  ", prtnaleave_tran.total_amount " & _
            '  ", prtnaleave_tran.balanceamount " & _
            '  ", prtnaleave_tran.userunkid " & _
            '  ", prtnaleave_tran.isvoid " & _
            '  ", prtnaleave_tran.voiduserunkid " & _
            '  ", prtnaleave_tran.voiddatetime " & _
            '  ", prtnaleave_tran.voidreason " & _
            '  ", prtnaleave_tran.openingbalance " & _
            '  ", ISNULL(prtnaleave_tran.basicsalary, 0) AS basicsalary " & _
            '  ", ISNULL(prtnaleave_tran.auditdatetime, processdate) AS processdatetime " & _
            '  ", ISNULL(prtnaleave_tran.ip, '') AS ip " & _
            '  ", ISNULL(prtnaleave_tran.host, '') AS host " & _
            '  ", ISNULL(prtnaleave_tran.void_ip, '') AS void_ip " & _
            '  ", ISNULL(prtnaleave_tran.void_host, '') AS void_host " & _
            '  ", ISNULL(prtnaleave_tran.tna_processdate, processdate) AS tna_processdate " & _
            '"FROM prtnaleave_tran " & _
            '"LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '"LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid=prtnaleave_tran.payperiodunkid " & _
            '"LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid "

            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            '    If xUACQry.Trim.Length > 0 Then
            '        strQ &= xUACQry
            '    End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            ''Sohail (21 Aug 2015) -- End

            'strQ &= "WHERE ISNULL(prtnaleave_tran.isvoid,0) = 0 "
            ''Sohail (07 Jan 2014) - [tna_processdate]
            ''Sohail (04 Mar 2011) - [new fields employeecode, openingbalance], 'Sohail (12 Oct 2011) - [basicsalary, ip, host, void_ip, void_host]
            ''Sohail (27 Aug 2010) -- Start

            ''Sohail (24 Jun 2011) -- Start
            ''Issue : According to privilege that lower level user should not see superior level employees.

            ''S.SANDEEP [ 04 FEB 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''End If

            ''S.SANDEEP [ 01 JUNE 2012 ] -- START
            ''ENHANCEMENT : TRA DISCIPLINE CHANGES
            ''Select Case ConfigParameter._Object._UserAccessModeSetting
            ''    Case enAllocation.BRANCH
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''    Case enAllocation.DEPARTMENT_GROUP
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''    Case enAllocation.DEPARTMENT
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''    Case enAllocation.SECTION_GROUP
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''    Case enAllocation.SECTION
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''    Case enAllocation.UNIT_GROUP
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''    Case enAllocation.UNIT
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''    Case enAllocation.TEAM
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''    Case enAllocation.JOB_GROUP
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''    Case enAllocation.JOBS
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''End Select
            ''Sohail (12 Jan 2015) -- Start
            ''Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            ''strQ &= UserAccessLevel._AccessLevelFilterString
            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            ''If strUserAccessLevelFilterString = "" Then
            ''    strQ &= UserAccessLevel._AccessLevelFilterString
            ''Else
            ''    strQ &= strUserAccessLevelFilterString
            ''End If
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If blnIncludeInActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If
            ''Sohail (21 Aug 2015) -- End
            ''Sohail (12 Jan 2015) -- End
            ''S.SANDEEP [ 01 JUNE 2012 ] -- END


            ''S.SANDEEP [ 04 FEB 2012 ] -- END

            ''Sohail (24 Jun 2011) -- End

            ''Anjan (09 Aug 2011)-Start
            ''Issue : For including setting of acitve and inactive employee.
            ''If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            ''    strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
            ''End If
            ''Anjan (09 Aug 2011)-End 

            ''Sohail (12 Oct 2011) -- Start
            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            ''If blnIncludeInActiveEmployee = False Then
            ''    'Sohail (06 Jan 2012) -- Start
            ''    'TRA - ENHANCEMENT
            ''    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
            ''    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            ''               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            ''               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            ''               " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

            ''    'Sohail (06 Mar 2012) -- Start
            ''    'TRA - ENHANCEMENT
            ''    'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            ''    'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            ''    If intPeriodID > 0 Then
            ''        Dim objPeriod As New clscommom_period_Tran
            ''        objPeriod._Periodunkid = intPeriodID

            ''        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._Start_Date))
            ''        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._End_Date))
            ''        'Sohail (12 May 2012) -- Start
            ''        'TRA - ENHANCEMENT
            ''        If blnExcludeTermEmp_PayProcess = True Then
            ''            'Sohail (24 Mar 2014) -- Start
            ''            'Issue - Employee is coming in process payroll list if EOC or leaving date is last date of period and excude from payroll is ticked.
            ''            'strQ &= " AND ( ( ISNULL(isexclude_payroll, 0) = 0 ) OR ISNULL(CONVERT(CHAR(8),termination_from_date,112),@nextdate) >= @enddate " & _
            ''            '    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@nextdate) >= @enddate " & _
            ''            '    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @nextdate) >= @enddate AND isexclude_payroll = 1 ) "
            ''            strQ &= " AND ( ( ISNULL(isexclude_payroll, 0) = 0 ) OR ( ISNULL(CONVERT(CHAR(8),termination_from_date,112),@nextdate) > @enddate " & _
            ''                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@nextdate) > @enddate " & _
            ''                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @nextdate) > @enddate AND isexclude_payroll = 1 ) ) "
            ''            'Sohail (24 Mar 2014) -- End

            ''            objDataOperation.AddParameter("@nextdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._End_Date.AddDays(1)))
            ''        End If
            ''        'Sohail (12 May 2012) -- End
            ''        objPeriod = Nothing
            ''    Else
            ''objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            ''objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            ''    End If
            ''    'Sohail (06 Mar 2012) -- End
            ''    'Sohail (06 Jan 2012) -- End
            ''End If
            ''Sohail (21 Aug 2015) -- End
            ''Sohail (12 Oct 2011) -- End

            'If intEmpUnkid > 0 Then
            '    strQ &= "AND prtnaleave_tran.employeeunkid = @employeeunkiid "
            '    objDataOperation.AddParameter("@employeeunkiid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid)
            'End If

            ''================ Pay Year
            'If intYearID > 0 Then
            '    strQ += " AND " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = @yearunkid"
            '    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearID) 'Sohail (04 Mar 2011)
            'End If

            ''================ Pay Period
            'If intPeriodID > 0 Then
            '    strQ += " AND prtnaleave_tran.payperiodunkid = @payperiodunkid"
            '    objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID) 'Sohail (04 Mar 2011)
            'End If

            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            ' ''================ PayPoint
            ''If intPayPointID > 0 Then
            ''    strQ += " AND hremployee_master.paypointunkid = @paypointunkid"
            ''    objDataOperation.AddParameter("@paypointunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayPointID) 'Sohail (04 Mar 2011)
            ''End If

            ' ''================ Department
            ''If intDeptID > 0 Then
            ''    strQ += " AND hremployee_master.departmentunkid = @departmentunkid"
            ''    objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeptID) 'Sohail (04 Mar 2011)
            ''End If

            ' ''================ Section
            ''If intSectionID > 0 Then
            ''    strQ += " AND hremployee_master.sectionunkid = @sectionunkid"
            ''    objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionID) 'Sohail (04 Mar 2011)
            ''End If

            ' ''================ Unit
            ''If intUnitID > 0 Then
            ''    strQ += " AND hremployee_master.unitunkid = @unitunkid"
            ''    objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnitID) 'Sohail (04 Mar 2011)
            ''End If

            ' ''================ Job
            ''If intJobID > 0 Then
            ''    strQ += " AND hremployee_master.jobunkid = @jobunkid"
            ''    objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID) 'Sohail (04 Mar 2011)
            ''End If

            ' ''================ Grade
            ''If intGradeID > 0 Then
            ''    strQ += " AND hremployee_master.gradeunkid = @gradeunkid"
            ''    objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeID) 'Sohail (04 Mar 2011)
            ''End If
            ''Sohail (21 Aug 2015) -- End

            ''S.SANDEEP [ 17 AUG 2011 ] -- START
            ''ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            ' ''================ Access
            ''If intBranchId > 0 Then
            ''    strQ += " AND hremployee_master.accessunkid = @accessunkid"
            ''    objDataOperation.AddParameter("@accessunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBranchId) 'Sohail (04 Mar 2011)
            ''End If

            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            ' ''================ Branch
            ''If intBranchId > 0 Then
            ''    strQ += " AND hremployee_master.stationunkid = @stationunkid"
            ''    objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBranchId)
            ''End If
            ' ''S.SANDEEP [ 17 AUG 2011 ] -- END 

            ' ''================ Class
            ''If intClassID > 0 Then
            ''    strQ += " AND hremployee_master.classunkid = @classunkid"
            ''    objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassID) 'Sohail (04 Mar 2011)
            ''End If

            ' ''================ Service
            ''If intServiceID > 0 Then
            ''    strQ += " AND hremployee_master.serviceunkid = @serviceunkid"
            ''    objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServiceID) 'Sohail (04 Mar 2011)
            ''End If

            ' ''================ CostCenter
            ''If intCostCenterID > 0 Then
            ''    strQ += " AND hremployee_master.costcenterunkid = @costcenterunkid"
            ''    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostCenterID) 'Sohail (04 Mar 2011)
            ''End If
            ''Sohail (21 Aug 2015) -- End

            ''Anjan (21 Nov 2011)-Start
            ''ENHANCEMENT : TRA COMMENTS
            'If mstrAdvanceFilter.Length > 0 Then
            '    strQ &= " AND " & mstrAdvanceFilter
            'End If
            ''Anjan (21 Nov 2011)-End 

            'Sohail (08 Nov 2017) -- Start
            'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
            'strQ = "DECLARE @EmpTab AS TABLE (employeeid int, employeename nvarchar(max), employeecode nvarchar(max)) " & _
            '       "INSERT INTO @EmpTab(employeeid,employeename,employeecode) " & _
            '       "SELECT " & _
            '       "     hremployee_master.employeeunkid " & _
            '       "    ,ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') " & _
            '       "    ,hremployee_master.employeecode " & _
            '       "FROM hremployee_master "


            'S.SANDEEP |22-OCT-2021| -- START
            'ISSUE : OLD-499 (Error - Employee Termination Screen) -- Advance Filter Changes
            'strQ = "SELECT  ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename" & _
            '             ", hremployee_master.employeecode " & _
            '             ", ADF.* " & _
            '       "INTO #EmpTab " & _
            '       "FROM hremployee_master "
            strQ = "SELECT  ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename" & _
                         ", hremployee_master.employeecode "
            If mstrAdvanceFilter.Trim.Length > 0 Then
                strQ &= ", ADF.* "
            Else
                strQ &= ", hremployee_master.employeeunkid "
            End If
            strQ &= "INTO #EmpTab " & _
                   "FROM hremployee_master "
            'S.SANDEEP |22-OCT-2021| -- END


            'Sohail (08 Nov 2017) -- End

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE 1 = 1 "

            If blnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If intEmpUnkid > 0 Then
                strQ &= "AND hremployee_master.employeeunkid = @employeeunkiid "
                objDataOperation.AddParameter("@employeeunkiid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid)
            End If

            If mstrAdvanceFilter.Length > 0 Then
                strQ &= " AND " & mstrAdvanceFilter
            End If

            strQ &= "ORDER BY hremployee_master.firstname "

            strQ &= "SELECT " & _
              "  prtnaleave_tran.tnaleavetranunkid " & _
              ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
              ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
              ", prtnaleave_tran.payperiodunkid " & _
              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
              ", prtnaleave_tran.employeeunkid " & _
              "/*, [@EmpTab].employeecode " & _
              ", [@EmpTab].employeename*/ " & _
              ", prtnaleave_tran.voucherno " & _
              ", prtnaleave_tran.processdate " & _
              ", prtnaleave_tran.daysinperiod " & _
              ", prtnaleave_tran.paidholidays " & _
              ", prtnaleave_tran.totalpayabledays " & _
              ", prtnaleave_tran.totalpayablehours " & _
              ", prtnaleave_tran.totaldaysworked " & _
              ", prtnaleave_tran.totalhoursworked " & _
              ", prtnaleave_tran.totalabsentorleave " & _
              ", prtnaleave_tran.totalextrahours " & _
              ", prtnaleave_tran.totalshorthours " & _
              ", ISNULL(prtnaleave_tran.totalonholddays,0) AS totalonholddays " & _
              ", prtnaleave_tran.total_amount " & _
              ", prtnaleave_tran.balanceamount " & _
              ", prtnaleave_tran.userunkid " & _
              ", prtnaleave_tran.isvoid " & _
              ", prtnaleave_tran.voiduserunkid " & _
              ", prtnaleave_tran.voiddatetime " & _
              ", prtnaleave_tran.voidreason " & _
              ", prtnaleave_tran.openingbalance " & _
              ", ISNULL(prtnaleave_tran.basicsalary, 0) AS basicsalary " & _
              ", ISNULL(prtnaleave_tran.auditdatetime, processdate) AS processdatetime " & _
              ", ISNULL(prtnaleave_tran.ip, '') AS ip " & _
              ", ISNULL(prtnaleave_tran.host, '') AS host " & _
              ", ISNULL(prtnaleave_tran.void_ip, '') AS void_ip " & _
              ", ISNULL(prtnaleave_tran.void_host, '') AS void_host " & _
              ", ISNULL(prtnaleave_tran.tna_processdate, processdate) AS tna_processdate " & _
              ", ISNULL(prtnaleave_tran.costcenterunkid, 0) AS costcenterunkid " & _
              ", ISNULL(CONVERT(CHAR(8), prtnaleave_tran.employee_enddate, 112), CONVERT(CHAR(8), processdate, 112)) AS employee_enddate " & _
              ", #EmpTab.* " & _
            "FROM prtnaleave_tran " & _
            "JOIN #EmpTab ON #EmpTab.employeeunkid = prtnaleave_tran.employeeunkid " & _
            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid=prtnaleave_tran.payperiodunkid " & _
            "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid "
            'Sohail (09 Oct 2019) - [employee_enddate]
            'Sohail (28 Apr 2018) - [costcenterunkid]
            'Sohail (08 Nov 2017) - [Removed - [@EmpTab].employeecode, [@EmpTab].employeename] [Added - , #EmpTab.*], [JOIN @EmpTab ON [@EmpTab].employeeid = prtnaleave_tran.employeeunkid] = [JOIN #EmpTab ON #EmpTab.employeeunkid = prtnaleave_tran.employeeunkid]

            strQ &= "WHERE ISNULL(prtnaleave_tran.isvoid,0) = 0 "

            'Sohail (08 Feb 2022) -- Start
            'Enhancement : OLD-548 : Give Option to apply Basic Salary Computation Setting on Flat Rate Heads.
            If blnOnlyProcessedEmployees = True Then
                strQ &= " AND CONVERT(CHAR(8), prtnaleave_tran.processdate, 112) = '" & eZeeDate.convertDate(xPeriodEnd) & "' "
            End If
            'Sohail ((08 Feb 2022) -- End

            '================ Pay Year
            If intYearID > 0 Then
                strQ += " AND " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = @yearunkid"
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearID)
            End If

            '================ Pay Period
            If intPeriodID > 0 Then
                strQ += " AND prtnaleave_tran.payperiodunkid = @payperiodunkid"
                objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            If colSQLFilter IsNot Nothing Then
                For Each item As clsSQLFilterCollection In colSQLFilter
                    strQ += " " & item._Query & " "
                    If item._ParameterName.ToString().Trim.Length > 0 Then
                        objDataOperation.AddParameter(item._ParameterName, item._SQLDbType, item._eZeeDataType, item._ParameterValue)
                    End If
                Next
            End If
            'Sohail (13 Oct 2017) -- End

            If strSortField.Trim.Length > 0 Then
                strQ += " ORDER BY " & strSortField
            End If

            'Sohail (08 Nov 2017) -- Start
            'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
            strQ += "; DROP TABLE #EmpTab; "
            'Sohail (08 Nov 2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function

    'Sohail (22 Sep 2017) -- Start
    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
    Public Function GetPreviousNetPay(ByVal xDatabaseName As String _
                                      , ByVal xUserUnkid As Integer _
                                      , ByVal intYearID As Integer _
                                      , ByVal xCompanyUnkid As Integer _
                                      , ByVal xPeriodStart As DateTime _
                                      , ByVal xPeriodEnd As DateTime _
                                      , ByVal xUserModeSetting As String _
                                      , ByVal xOnlyApproved As Boolean _
                                      , ByVal blnIncludeInActiveEmployee As Boolean _
                                      , ByVal blnApplyUserAccessFilter As Boolean _
                                      , ByVal dtAsOnDate As Date _
                                      , ByVal strTableName As String _
                                      , Optional ByVal strEmpUnkIDs As String = "" _
                                      , Optional ByVal strSortField As String = "" _
                                      , Optional ByVal mstrAdvanceFilter As String = "" _
                                      , Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False _
                                      , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                      ) As DataSet
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , blnExcludeTermEmp_PayProcess, xDatabaseName)
        If blnApplyUserAccessFilter = True Then
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, intYearID, xUserModeSetting)
        End If
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            strQ = "DECLARE @EmpTab AS TABLE (employeeid int, employeename nvarchar(max), employeecode nvarchar(max)) " & _
                   "INSERT INTO @EmpTab(employeeid,employeename,employeecode) " & _
                   "SELECT " & _
                   "     hremployee_master.employeeunkid " & _
                   "    ,ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') " & _
                   "    ,hremployee_master.employeecode " & _
                   "FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE 1 = 1 "

            If blnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strEmpUnkIDs.Trim.Length > 0 Then
                strQ &= "AND hremployee_master.employeeunkid IN (" & strEmpUnkIDs & ") "
            End If

            If mstrAdvanceFilter.Length > 0 Then
                strQ &= " AND " & mstrAdvanceFilter
            End If

            strQ &= "ORDER BY hremployee_master.firstname "

            strQ &= "SELECT * FROM ( "

            strQ &= "SELECT " & _
              "  prtnaleave_tran.tnaleavetranunkid " & _
              ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
              ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
              ", prtnaleave_tran.payperiodunkid " & _
              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
              ", prtnaleave_tran.employeeunkid " & _
              ", [@EmpTab].employeecode " & _
              ", [@EmpTab].employeename " & _
              ", prtnaleave_tran.voucherno " & _
              ", prtnaleave_tran.processdate " & _
              ", prtnaleave_tran.daysinperiod " & _
              ", prtnaleave_tran.paidholidays " & _
              ", prtnaleave_tran.totalpayabledays " & _
              ", prtnaleave_tran.totalpayablehours " & _
              ", prtnaleave_tran.totaldaysworked " & _
              ", prtnaleave_tran.totalhoursworked " & _
              ", prtnaleave_tran.totalabsentorleave " & _
              ", prtnaleave_tran.totalextrahours " & _
              ", prtnaleave_tran.totalshorthours " & _
              ", ISNULL(prtnaleave_tran.totalonholddays,0) AS totalonholddays " & _
              ", prtnaleave_tran.total_amount " & _
              ", prtnaleave_tran.balanceamount " & _
              ", prtnaleave_tran.userunkid " & _
              ", prtnaleave_tran.isvoid " & _
              ", prtnaleave_tran.voiduserunkid " & _
              ", prtnaleave_tran.voiddatetime " & _
              ", prtnaleave_tran.voidreason " & _
              ", prtnaleave_tran.openingbalance " & _
              ", ISNULL(prtnaleave_tran.basicsalary, 0) AS basicsalary " & _
              ", ISNULL(prtnaleave_tran.auditdatetime, processdate) AS processdatetime " & _
              ", ISNULL(prtnaleave_tran.ip, '') AS ip " & _
              ", ISNULL(prtnaleave_tran.host, '') AS host " & _
              ", ISNULL(prtnaleave_tran.void_ip, '') AS void_ip " & _
              ", ISNULL(prtnaleave_tran.void_host, '') AS void_host " & _
              ", ISNULL(prtnaleave_tran.tna_processdate, processdate) AS tna_processdate " & _
              ", ISNULL(prtnaleave_tran.costcenterunkid, 0) AS costcenterunkid " & _
              ", ISNULL(prtnaleave_tran.employee_enddate, processdate) AS employee_enddate " & _
              ", DENSE_RANK() OVER ( PARTITION BY prtnaleave_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
            "FROM prtnaleave_tran " & _
            "JOIN @EmpTab ON [@EmpTab].employeeid = prtnaleave_tran.employeeunkid " & _
            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid=prtnaleave_tran.payperiodunkid " & _
            "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid "
            'Sohail (09 Oct 2019) - [employee_enddate]
            'Sohail (28 Apr 2018) - [costcenterunkid]

            strQ &= "WHERE ISNULL(prtnaleave_tran.isvoid,0) = 0 "

            If dtAsOnDate <> Nothing Then
                strQ += " AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @enddate "
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If

            strQ += " ) AS A " & _
                   "WHERE A.ROWNO = 1 "

            'If strSortField.Trim.Length > 0 Then
            '    strQ += " ORDER BY " & strSortField
            'End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPreviousNetPay; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function
    'Sohail (22 Sep 2017) -- End


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Public Function InsertDelete(ByVal strEmployeeList As String, ByVal strEmpVoidList As String, ByVal intPeriodUnkID As Integer, Optional ByVal bw As BackgroundWorker = Nothing) As Boolean
    Public Function InsertDelete(ByVal xDatabaseName As String, _
                                 ByVal xUserUnkid As Integer, _
                                 ByVal xYearUnkid As Integer, _
                                 ByVal xCompanyUnkid As Integer, _
                                 ByVal xUserModeSetting As String, _
                                 ByVal strEmployeeList As String, _
                                 ByVal strEmpVoidList As String, _
                                 ByVal intPeriodUnkID As Integer, _
                                 ByVal intBasicSalaryComputation As Integer, _
                                 ByVal dtCurrentDateAndTime As Date, _
                                 ByVal blnApplyPayPerActivity As Boolean, _
                                 ByVal dtDatabase_Start_Date As Date, _
                                 ByVal dtDatabase_End_Date As Date, _
                                 ByVal intLeaveBalanceSetting As Integer, _
                                 ByVal blnIsFin_Close As Boolean, _
                                 ByVal intBase_CurrencyId As Integer, _
                                 ByVal xLeaveAccrueTenureSetting As Integer, _
                                 ByVal xLeaveAccrueDaysAfterEachMonth As Integer, _
                                 ByVal intFlatRateHeadsComputation As Integer, _
                                 Optional ByVal bw As BackgroundWorker = Nothing _
                                 ) As Boolean
        'Sohail (08 Feb 2022) - [intFlatRateHeadsComputation]
        'Sohail (21 Aug 2015) - [intBasicSalaryComputation, dtCurrentDateAndTime, blnApplyPayPerActivity, dtDatabase_Start_Date, dtDatabase_End_Date, intLeaveBalanceSetting, blnIsFin_Close, intBase_CurrencyId]
        'Shani(24-Aug-2015) -- End

        'Public Function InsertDelete(ByVal strEmployeeList As String, ByVal intPeriodUnkID As Integer) As Boolean 'Sohail (04 Mar 2011)
        'Sohail (22 Aug 2012) - [bw]
        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim dsCalc As DataTable
        'Dim strQ As String = ""
        Dim exForce As Exception
        Dim dblDailyHours As Double
        'Dim objED As New clsEarningDeduction
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmpShift As New clsEmployee_Shift_Tran 'Sohail (24 Sep 2013)
        Dim lngVoucherNo As Long = 0 'Sohail (04 Mar 2011)
        Dim blnFlag As Boolean = False
        Dim intWorkingDaysInPeriod As Integer = 0
        Dim dblWorkingHoursInPeriod As Double = 0 'Sohail (24 Sep 2013)
        Dim dblTotalDaysWorked As Double = 0
        Dim dblOnHoldDays As Double = 0
        Dim strEmpQuery As String = ""
        'Dim arrEmp() As String 'Sohail (20 Feb 2012)
        'Sohail (04 Mar 2011) -- Start
        Dim decPaidAmount As Decimal 'Sohail (11 May 2011)
        Dim dtProcessDt As DateTime = mdtProcessdate
        'Sohail (04 Mar 2011) -- End
        'Sohail (08 Nov 2011) -- Start
        'Dim objEmployee As clsEmployee_Master 'Sohail (13 Sep 2011), 'Sohail (08 Jun 2012)
        'Dim objMaster As clsMasterData 'Sohail (08 Jun 2012)
        Dim objWages As clsWagesTran
        'Sohail (02 Jan 2017) -- Start
        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
        'Dim objTranHead As clsTransactionHead 'Sohail (28 Jan 2012)
        Dim objTranHead As New clsTransactionHead
        'Sohail (02 Jan 2017) -- End
        Dim mdecBasicScale As Decimal
        Dim decDailyBasicSalary As Decimal
        Dim decHourlyBasicSalary As Decimal
        'Sohail (12 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        Dim decDailyBasicSalaryWeekendsIncluded As Decimal
        Dim decHourlyBasicSalaryWeekendsIncluded As Decimal
        'Sohail (12 Apr 2012) -- End
        'Sohail (10 Jul 2013) -- Start
        'TRA - ENHANCEMENT
        Dim decDailyBasicSalaryOnConstantDays As Decimal
        Dim decHourlyBasicSalaryOnConstantDays As Decimal
        'Sohail (10 Jul 2013) -- End
        Dim intEmpWorkingDaysInPeriod As Integer
        'Hemant (19 July 2018) -- Start
        'Support Issue Id # 0002391 -- Wrong hours worked posted on payroll,
        Dim dblEmpWorkingHoursInPeriod As Double
        'Hemant (19 July 2018) -- End
        'Sohail (11 Nov 2019) -- Start
        'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
        Dim intEmpWorkingDaysInTnAPeriod As Integer
        Dim dblEmpWorkingHoursInTnAPeriod As Double
        'Sohail (11 Nov 2019) -- End

        'Sohail (08 Nov 2011) -- End
        Dim intEmpTotalDaysInPeriod As Integer 'Sohail (24 Dec 2013)
        'Sohail (18 Jun 2014) -- Start
        'Enhancement - TOTAL DAYS IN TnA and Leave PERIOD for Voltamp. 
        Dim intTotalDaysInTnAPeriod As Integer = 0
        Dim intEmpTotalDaysInTnAPeriod As Integer = 0
        'Sohail (18 Jun 2014) -- End
        'Sohail (29 Oct 2013) -- Start
        'TRA - ENHANCEMENT
        Dim mdecBasicSalaryOnWages As Decimal
        Dim objShiftTran As New clsshift_tran
        Dim dicShiftDaysHours As New Dictionary(Of Integer, ArrayList)
        Dim arrEmpShift As New ArrayList
        'Sohail (29 Oct 2013) -- End

        'Sohail (02 Jan 2017) -- Start
        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
        Dim dsLoanCalc As DataSet
        'Sohail (02 Jan 2017) -- End
        'Sohail (26 Oct 2018) -- Start
        'FDRC Issue - Process Payroll performance issue on FDRC database in 75.1.
        Dim dsTranHead As DataSet
        'Sohail (26 Oct 2018) -- End
        'Sohail (25 Feb 2022) -- Start
        'Enhancement :  : Performance enhancement on process payroll.
        Dim objPPASummary As New clsPayActivity_Tran
        Dim objCRProcess As New clsclaim_process_Tran
        Dim objRetireProcess As New clsretire_process_tran
        Dim dsPPAtran As DataSet = Nothing
        Dim dsCRProcess As DataSet = Nothing
        Dim dsRetireProcess As DataSet = Nothing
        'Sohail ((25 Feb 2022) -- End

        'Sohail (08 Jun 2012) -- Start
        'TRA - ENHANCEMENT
        Dim dtAppointment As Date
        Dim dtTerminationFrom As Date
        Dim dtTerminationTo As Date
        Dim dtEmplEnd As Date
        Dim intGradeId As Integer
        Dim intGradeLevelId As Integer
        Dim intCostCenterId As Integer
        Dim intTranHeadId As Integer
        Dim intCalcTypeId As Integer
        Dim intShiftId As Integer
        Dim strShiftDays As String
        Dim intEdPeriodId As Integer
        Dim strCurrEmpName As String = "" 'Sohail (18 May 2013)
        'Sohail (24 Sep 2013) -- Start
        'TRA - ENHANCEMENT
        Dim dblYearOfService As Double = 0
        Dim dblMonthsOfService As Double = 0
        'Sohail (24 Sep 2013) -- End
        'Sohail (21 Nov 2013) -- Start
        'ENHANCEMENT - OMAN
        Dim dblDaysOfService As Double = 0
        Dim dblTotalYearOfService As Double = 0
        Dim dblTotalMonthsOfService As Double = 0
        Dim dblTotalDaysOfService As Double = 0
        'Sohail (21 Nov 2013) -- End
        'Sohail (02 Dec 2020) -- Start
        'NMB Enhancement : # OLD-215 : New Payroll Functions for Current Service Duration for Employee.
        Dim dblTotalYearOfCurrentService As Double = 0
        Dim dblTotalMonthsOfCurrentService As Double = 0
        Dim dblTotalDaysOfCurrentService As Double = 0
        'Sohail (02 Dec 2020) -- End
        'Sohail (24 Sep 2014) -- Start
        'Enhancement - Weekend Days and Public Holiday Days function in transaction head formula.
        'Sohail (26 Sep 2014) -- Start
        Dim intWorkingDaysInTnAPeriod As Integer = 0
        'Enhancement - Weekend Hours and Public Holiday Hours function in transaction head formula.
        'Dim objHoliday As New clsemployee_holiday
        'Dim intPHDaysInPeriod As Integer = 0
        'Dim intPHDaysInTnAPeriod As Integer = 0
        'Sohail (26 Sep 2014) -- End
        'Sohail (24 Sep 2014) -- End

        'Sohail (26 Aug 2016) -- Start
        'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
        Dim objExchangeRate As New clsExchangeRate
        Dim mdecDecimalPlaces As Decimal = 0
        'Sohail (26 Aug 2016) -- End

        'Sohail (07 Nov 2019) -- Start
        'NMB Enhancement # : Flatrate prorating issue on employee exemption.
        Dim dsEOC As DataSet = Nothing
        'Sohail (07 Nov 2019) -- End

        'Sohail (16 Jan 2020) -- Start
        'NMB Enhancement # : System should calculate allowance given percentage of each salary change and accumulate it if salary head is used in formula.
        Dim dtProrateDate As New DataTable
        dtProrateDate.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        dtProrateDate.Columns.Add("startdate", System.Type.GetType("System.String")).DefaultValue = ""
        dtProrateDate.Columns.Add("enddate", System.Type.GetType("System.String")).DefaultValue = ""
        'Sohail (16 Jan 2020) -- End
        'Sohail (07 Feb 2020) -- Start
        'NMB Enhancement # : Pick least amount benefit if head is mapped in more than one benefit and both benefits effective dates are same.
        dtProrateDate.Columns.Add("benefitallocationunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        dtProrateDate.Columns.Add("benefitallocationtranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        dtProrateDate.Columns.Add("tranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        dtProrateDate.Columns.Add("amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
        dtProrateDate.Columns.Add("days", System.Type.GetType("System.Decimal")).DefaultValue = 0
        'Sohail (07 Feb 2020) -- End

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'Dim intBasicSalaryComputation As Integer = ConfigParameter._Object._BasicSalaryComputation
        'Sohail (21 Aug 2015) -- End
        'Sohail (08 Jun 2012) -- End

        objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction() 'Sohail (26 Aug 2016)

        Try

            dblDailyHours = 0

            'Sohail (04 Mar 2011) -- Start
            'blnFlag = Void(strEmployeeList, intPeriodUnkID, mintVoiduserunkid, Now, "") 'Sohail (14 Oct 2010)
            'If blnFlag = False Then
            '    Return False
            'End If
            'Sohail (04 Mar 2011) -- End

            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'blnFlag = objProcessPayroll.Void(strEmployeeList, intPeriodUnkID, mintVoiduserunkid, Now, "") 'Sohail (14 Oct 2010)

            'If blnFlag = False Then
            '    Return False
            'End If
            objExchangeRate._ExchangeRateunkid = intBase_CurrencyId
            mdecDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (26 Aug 2016) -- End
            'Sohail (03 Nov 2021) -- Start
            'HJIF Issue : HJIF-AH-4234 : TRANSACTION HEADS PROBLEM NSSF (Bring difference of 0.003 when you compare with 10% of gross and total of NSSF).
            mdecDecimalPlaces = 6
            'Sohail (03 Nov 2021) -- End

            lngVoucherNo = getMaxVoucherNo()

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = intPeriodUnkID
            objPeriod._Periodunkid(xDatabaseName) = intPeriodUnkID
            'Sohail (21 Aug 2015) -- End
            mdtPeriodStartDate = objPeriod._Start_Date
            mdtPeriodEndDate = objPeriod._End_Date
            intTotalDaysInTnAPeriod = DateDiff(DateInterval.Day, mdtTnA_StartDate, mdtTnA_EndDate.AddDays(1)) 'Sohail (18 Jun 2014)

            'Sohail (11 Sep 2010) -- Start
            'arrEmp = strEmployeeList.Split(",")
            'For i = 0 To arrEmp.Length - 1
            '    If i = 0 Then
            '        strEmpQuery = "SELECT " & CInt(arrEmp(i)) & " AS EmpID "
            '    Else
            '        strEmpQuery &= "UNION SELECT " & CInt(arrEmp(i)) & " AS EmpID "
            '    End If
            'Next
            'Sohail (11 Sep 2010) -- End


            'strQ = " SELECT " & _
            '        " period AS period " & _
            '        ", ISNULL(A.EmpID,0) AS EmpID" & _
            '        ", ISNULL(DaysInPeriod,0) AS DaysInPeriod" & _
            '        ", ISNULL(PaidHoliday,0) AS PaidHoliday" & _
            '        ", ISNULL(DaysInPeriod,0) - ISNULL(PaidHoliday,0) AS TotPayableDays" & _
            '        ", ISNULL(DailyHours,0) AS DailyHours" & _
            '        ", (ISNULL(DaysInPeriod,0) - ISNULL(PaidHoliday,0)) * ISNULL(DailyHours,0)  AS TotPayableHours" & _
            '        ", ISNULL(TotalDaysWorked,0) AS TotalDaysWorked" & _
            '        ", ISNULL(TotHoursWorked,0) AS TotHoursWorked" & _
            '        ", ISNULL(TotalOvertime,0) AS TotalOvertime" & _
            '        ", ISNULL(TotShortHours,0) AS TotShortHours" & _
            '        ", ISNULL(Absent,0) AS Absent" & _
            '    " FROM" & _
            '    " (" & _
            '        " SELECT" & _
            '            " @periodid AS period " & _
            '            ", tnalogin_tran.employeeunkid AS EmpID" & _
            '            ", (" & _
            '                " SELECT " & _
            '                    " total_days" & _
            '                " FROM cfcommon_period_tran" & _
            '                " WHERE periodunkid = @periodid" & _
            '                    " AND isactive=1" & _
            '            " )" & _
            '            " AS DaysInPeriod" & _
            '            ", (" & _
            '                " SELECT" & _
            '                    " DISTINCT workinghours" & _
            '                " FROM tnashift_master" & _
            '                " INNER JOIN hremployee_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid" & _
            '                " WHERE hremployee_master.employeeunkid in (" & strEmployeeList & ")" & _
            '            " )" & _
            '            " AS DailyHours" & _
            '            ", SUM(day_type) AS TotalDaysWorked" & _
            '            ", SUM(total_hrs) AS TotHoursWorked" & _
            '            ", SUM(total_overtime) AS TotalOvertime" & _
            '            ", SUM(total_short_hrs) AS TotShortHours" & _
            '        " FROM tnalogin_summary" & _
            '        " LEFT JOIN tnalogin_tran ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid AND tnalogin_summary.login_date = tnalogin_tran.logindate" & _
            '        " LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid" & _
            '        " WHERE tnalogin_tran.holdunkid = 0" & _
            '            " AND tnalogin_summary.employeeunkid in (" & strEmployeeList & ")" & _
            '            " AND login_date between @startdate And @enddate" & _
            '        " GROUP BY tnalogin_tran.employeeunkid" & _
            '    " ) AS A" & _
            '    " LEFT JOIN" & _
            '    " (" & _
            '        " SELECT " & _
            '            " tnalogin_tran.employeeunkid AS EmpID" & _
            '            ", COUNT(ispaidleave) AS PaidHoliday" & _
            '            ", 0 AS Absent" & _
            '        " FROM tnalogin_summary" & _
            '        " LEFT JOIN tnalogin_tran ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid AND tnalogin_summary.login_date = tnalogin_tran.logindate" & _
            '        " LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid" & _
            '        " WHERE tnalogin_tran.holdunkid = 0 AND ispaidleave = 1 AND tnalogin_summary.employeeunkid in (" & strEmployeeList & ") and login_date between @startdate and @enddate" & _
            '        " GROUP BY tnalogin_tran.employeeunkid" & _
            '        " UNION" & _
            '        " SELECT " & _
            '            " tnalogin_tran.employeeunkid AS EmpID" & _
            '            ", 0 AS PaidHoliday" & _
            '            ", COUNT(isunpaidleave) AS Absent" & _
            '        " FROM tnalogin_summary" & _
            '        " LEFT JOIN tnalogin_tran ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid AND tnalogin_summary.login_date = tnalogin_tran.logindate" & _
            '        " LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid" & _
            '        " WHERE tnalogin_tran.holdunkid = 0 AND isunpaidleave = 1 AND tnalogin_summary.employeeunkid in (" & strEmployeeList & ") and login_date between @startdate and @enddate" & _
            '        " GROUP BY tnalogin_tran.employeeunkid" & _
            '    " ) AS Holiday" & _
            '    " ON Holiday.EmpId = A.EmpID"

            'Sohail (07 Aug 2010) -- Start
            'strQ = "SELECT period AS period , " & _
            '        "ISNULL(A.EmpID, 0) AS EmpID , " & _
            '        "ISNULL(DaysInPeriod, 0) AS DaysInPeriod , " & _
            '        "ISNULL(PaidHoliday, 0) AS PaidHoliday , " & _
            '        "ISNULL(DaysInPeriod, 0) - ISNULL(PaidHoliday, 0) AS TotPayableDays , " & _
            '        "ISNULL(DailyHours, 0) AS DailyHours , " & _
            '        "( ISNULL(DaysInPeriod, 0) - ISNULL(PaidHoliday, 0) ) " & _
            '        "* ISNULL(DailyHours, 0) AS TotPayableHours , " & _
            '        "ISNULL(TotalDaysWorked, 0) AS TotalDaysWorked , " & _
            '        "ISNULL(TotHoursWorked, 0) AS TotHoursWorked , " & _
            '        "ISNULL(TotalOvertime, 0) AS TotalOvertime , " & _
            '        "ISNULL(TotShortHours, 0) AS TotShortHours , " & _
            '        "ISNULL(Absent, 0) AS Absent " & _
            '        "FROM   ( SELECT    @periodid AS period , " & _
            '                    "tnalogin_summary.employeeunkid AS EmpID , " & _
            '                    "( SELECT    total_days " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                    ") AS DaysInPeriod , " & _
            '                    "( SELECT DISTINCT " & _
            '                                "workinghours " & _
            '                      "FROM      tnashift_master " & _
            '                                "INNER JOIN hremployee_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid " & _
            '                      "WHERE     hremployee_master.employeeunkid IN (" & strEmployeeList & ")" & _
            '                    ") AS DailyHours , " & _
            '                    "SUM(day_type) AS TotalDaysWorked , " & _
            '                    "SUM(total_hrs) AS TotHoursWorked , " & _
            '                    "SUM(total_overtime) AS TotalOvertime , " & _
            '                    "SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '          "WHERE     tnalogin_summary.employeeunkid IN " & _
            '                         "(SELECT DISTINCT employeeunkid FROM tnalogin_tran WHERE holdunkid = 0 AND  logindate BETWEEN @startdate AND @enddate AND employeeunkid IN (" & strEmployeeList & ")) " & _
            '                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '          "GROUP BY  tnalogin_summary.employeeunkid " & _
            '        ") AS A " & _
            '        "LEFT JOIN ( SELECT  tnalogin_tran.employeeunkid AS EmpID , " & _
            '                            "COUNT(ispaidleave) AS PaidHoliday , " & _
            '                            "0 AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                            "LEFT JOIN tnalogin_tran ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid " & _
            '                                                       "AND tnalogin_summary.login_date = tnalogin_tran.logindate " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "WHERE   tnalogin_tran.holdunkid = 0 " & _
            '                            "AND ispaidleave = 1 " & _
            '                            "AND tnalogin_summary.employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                    "GROUP BY tnalogin_tran.employeeunkid " & _
            '                    "UNION " & _
            '                    "SELECT  tnalogin_tran.employeeunkid AS EmpID , " & _
            '                            "0 AS PaidHoliday , " & _
            '                            "COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                            "LEFT JOIN tnalogin_tran ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid " & _
            '                                                       "AND tnalogin_summary.login_date = tnalogin_tran.logindate " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "WHERE   tnalogin_tran.holdunkid = 0 " & _
            '                            "AND isunpaidleave = 1 " & _
            '                            "AND tnalogin_summary.employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                    "GROUP BY tnalogin_tran.employeeunkid " & _
            '                  ") AS Holiday ON Holiday.EmpId = A.EmpID "
            'Sohail (16 Aug 2010) -- Start
            '***Problem:Return false value of totaldaysworked, absent, paidholiday because of join on tnalogin_tran for holdunkid
            'strQ = "SELECT period AS period , " & _
            '        "ISNULL(A.EmpID, 0) AS EmpID , " & _
            '        "ISNULL(DaysInPeriod, 0) AS DaysInPeriod , " & _
            '        "ISNULL(PaidHoliday, 0) AS PaidHoliday , " & _
            '        "ISNULL(DailyHours, 0) AS DailyHours , " & _
            '        "ISNULL(TotalDaysWorked, 0) AS TotalDaysWorked , " & _
            '        "ISNULL(TotHoursWorked, 0) AS TotHoursWorked , " & _
            '        "ISNULL(TotalOvertime, 0) AS TotalOvertime , " & _
            '        "ISNULL(TotShortHours, 0) AS TotShortHours , " & _
            '        "ISNULL(Absent, 0) AS Absent " & _
            '        "FROM   ( SELECT    @periodid AS period , " & _
            '                    "tnalogin_summary.employeeunkid AS EmpID , " & _
            '                    "( SELECT    total_days " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                    ") AS DaysInPeriod , " & _
            '                    "ISNULL(tnashift_master.workinghours,0) AS DailyHours , " & _
            '                    "SUM(day_type) AS TotalDaysWorked , " & _
            '                    "SUM(total_hrs) AS TotHoursWorked , " & _
            '                    "SUM(total_overtime) AS TotalOvertime , " & _
            '                    "SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '          "WHERE     tnalogin_summary.employeeunkid IN " & _
            '                         "(SELECT DISTINCT employeeunkid FROM tnalogin_tran WHERE holdunkid = 0 AND  logindate BETWEEN @startdate AND @enddate AND employeeunkid IN (" & strEmployeeList & ")) " & _
            '                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '          "GROUP BY  tnalogin_summary.employeeunkid, tnashift_master.workinghours " & _
            '        ") AS A " & _
            '        "LEFT JOIN ( SELECT  tnalogin_tran.employeeunkid AS EmpID , " & _
            '                            "COUNT(ispaidleave) AS PaidHoliday , " & _
            '                            "0 AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                            "LEFT JOIN tnalogin_tran ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid " & _
            '                                                       "AND tnalogin_summary.login_date = tnalogin_tran.logindate " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "WHERE   tnalogin_tran.holdunkid = 0 " & _
            '                            "AND ispaidleave = 1 " & _
            '                            "AND tnalogin_summary.employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                    "GROUP BY tnalogin_tran.employeeunkid " & _
            '                    "UNION " & _
            '                    "SELECT  tnalogin_tran.employeeunkid AS EmpID , " & _
            '                            "0 AS PaidHoliday , " & _
            '                            "COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                            "LEFT JOIN tnalogin_tran ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid " & _
            '                                                       "AND tnalogin_summary.login_date = tnalogin_tran.logindate " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "WHERE   tnalogin_tran.holdunkid = 0 " & _
            '                            "AND isunpaidleave = 1 " & _
            '                            "AND tnalogin_summary.employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                    "GROUP BY tnalogin_tran.employeeunkid " & _
            '                  ") AS Holiday ON Holiday.EmpId = A.EmpID "
            'Sohail (18 Aug 2010) -- Start
            'strQ = "SELECT  period AS period , " & _
            '                "ISNULL(TableA.EmpID, 0) AS EmpID , " & _
            '                "ISNULL(DaysInPeriod, 0) AS DaysInPeriod , " & _
            '                "ISNULL(PaidHoliday, 0) AS PaidHoliday , " & _
            '                "ISNULL(DailyHours, 0) AS DailyHours , " & _
            '                "ISNULL(TotHoursWorked, 0) AS TotHoursWorked , " & _
            '                "ISNULL(TotalOvertime, 0) AS TotalOvertime , " & _
            '                "ISNULL(TotShortHours, 0) AS TotShortHours , " & _
            '                "ISNULL(Absent, 0) AS ABSENT , " & _
            '                "ISNULL(OnHoldDays, 0) AS OnHoldDays " & _
            '        "FROM    ( SELECT   @periodid AS period , " & _
            '                            "tnalogin_summary.employeeunkid AS EmpID , " & _
            '                            "( SELECT    total_days " & _
            '                              "FROM      cfcommon_period_tran " & _
            '                              "WHERE     periodunkid = @periodid " & _
            '                                        "AND isactive = 1 " & _
            '                            ") AS DaysInPeriod , " & _
            '                            "ISNULL(tnashift_master.workinghours, 0) AS DailyHours , " & _
            '                            "SUM(total_hrs) AS TotHoursWorked , " & _
            '                            "SUM(total_overtime) AS TotalOvertime , " & _
            '                            "SUM(total_short_hrs) AS TotShortHours " & _
            '                  "FROM      tnalogin_summary " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                  "WHERE     tnalogin_summary.employeeunkid IN ( " & _
            '                            "SELECT DISTINCT " & _
            '                                    "employeeunkid " & _
            '                            "FROM    tnalogin_tran " & _
            '                            "WHERE   holdunkid = 0 " & _
            '                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") ) " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid , " & _
            '                            "tnashift_master.workinghours " & _
            '                ") AS TableA " & _
            '                "LEFT JOIN ( SELECT  TableAbsent.employeeunkid , " & _
            '                                    "ABSENT , " & _
            '                                    "ISNULL(TablePaidHoliday.PaidHoliday, 0) AS PaidHoliday , " & _
            '                                    "ISNULL(TableHold.hold, 0) AS OnHoldDays " & _
            '                            "FROM    ( SELECT    employeeunkid , " & _
            '                                                "COUNT(isunpaidleave) AS Absent " & _
            '                                      "FROM      tnalogin_summary " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                                "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                    ") AS TableAbsent " & _
            '                                    "LEFT JOIN ( SELECT  employeeunkid , " & _
            '                                                        "COUNT(ispaidleave) AS PaidHoliday " & _
            '                                                "FROM    tnalogin_summary " & _
            '                                                "WHERE   ispaidleave = 1 " & _
            '                                                        "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                        "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                                "GROUP BY employeeunkid " & _
            '                                              ") AS TablePaidHoliday ON TableAbsent.employeeunkid = TablePaidHoliday.employeeunkid " & _
            '                                    "LEFT JOIN ( SELECT  employeeunkid , " & _
            '                                                        "COUNT(holdunkid) AS hold " & _
            '                                                "FROM    ( SELECT    employeeunkid , " & _
            '                                                                    "holdunkid " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                                          "GROUP BY  employeeunkid , " & _
            '                                                                    "holdunkid " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                                              ") AS TableHold ON TableHold.employeeunkid = TableAbsent.employeeunkid " & _
            '                        ") AS TableB ON TableA.EmpID = TableB.employeeunkid "
            'Sohail (11 Sep 2010) -- Start
            'Changes:DaysWorked(TotalDaysWorkedWithAbsent) field removed and will be calculated in the following loop AND paidaccrueleave added.
            'strQ = "SELECT period AS period , " & _
            '                "ISNULL(TableA.EmpID, 0) AS EmpID , " & _
            '        "ISNULL(DaysInPeriod, 0) AS DaysInPeriod , " & _
            '        "ISNULL(PaidHoliday, 0) AS PaidHoliday , " & _
            '        "ISNULL(DailyHours, 0) AS DailyHours , " & _
            '        "ISNULL(TotHoursWorked, 0) AS TotHoursWorked , " & _
            '        "ISNULL(TotalOvertime, 0) AS TotalOvertime , " & _
            '        "ISNULL(TotShortHours, 0) AS TotShortHours , " & _
            '                "ISNULL(Absent, 0) AS ABSENT , " & _
            '                "ISNULL(hold, 0) AS OnHoldDays , " & _
            '                "ISNULL(DaysWorked, 0) AS TotalDaysWorkedWithAbsent " & _
            '        "FROM   ( SELECT    @periodid AS period , " & _
            '                    "tnalogin_summary.employeeunkid AS EmpID , " & _
            '                    "( SELECT    total_days " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                    ") AS DaysInPeriod , " & _
            '                    "ISNULL(tnashift_master.workinghours,0) AS DailyHours , " & _
            '                    "SUM(total_hrs) AS TotHoursWorked , " & _
            '                    "SUM(total_overtime) AS TotalOvertime , " & _
            '                    "SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                  "WHERE     tnalogin_summary.employeeunkid IN ( " & _
            '                            "SELECT DISTINCT " & _
            '                                    "employeeunkid " & _
            '                            "FROM    tnalogin_tran " & _
            '                            "WHERE   holdunkid = 0 " & _
            '                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") ) " & _
            '                            "AND login_date BETWEEN @startdate " & _
            '                                           "AND     @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid , " & _
            '                            "tnashift_master.workinghours " & _
            '                ") AS TableA " & _
            '                "LEFT JOIN ( SELECT  employeeunkid , " & _
            '                                                "COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate " & _
            '                                                   "AND     @enddate " & _
            '                                                "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                          ") AS TableAbsent ON TableAbsent.employeeunkid = TableA.EmpID " & _
            '                                    "LEFT JOIN ( SELECT  employeeunkid , " & _
            '                                                        "COUNT(ispaidleave) AS PaidHoliday " & _
            '                    "FROM    tnalogin_summary " & _
            '                                                "WHERE   ispaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate " & _
            '                                                   "AND     @enddate " & _
            '                                                        "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                                "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidHoliday ON TableA.EmpID = TablePaidHoliday.employeeunkid " & _
            '                                    "LEFT JOIN ( SELECT  employeeunkid , " & _
            '                                                        "COUNT(holdunkid) AS hold " & _
            '                                                "FROM    ( SELECT    employeeunkid , " & _
            '                                                                    "holdunkid " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                                          "GROUP BY  employeeunkid , " & _
            '                                                                    "holdunkid " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                          ") AS TableHold ON TableHold.employeeunkid = Tablea.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid , " & _
            '                                    "COUNT(employeeunkid) AS DaysWorked " & _
            '                            "FROM    tnalogin_summary " & _
            '                            "WHERE   login_date BETWEEN @startdate " & _
            '                                               "AND     @enddate " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TableDaysWorked ON TableA.EmpID = TableDaysWorked.employeeunkid "
            'Sohail (11 Sep 2010) -- Start
            '*** Changes : Now calculate Defined salary for if there is no single present in whole pay period
            'strQ = "SELECT  period AS period " & _
            '              ", ISNULL(TableA.EmpID, 0) AS EmpID " & _
            '              ", ISNULL(DaysInPeriod, 0) AS DaysInPeriod " & _
            '              ", ISNULL(PaidHoliday, 0) AS PaidHoliday " & _
            '              ", ISNULL(DailyHours, 0) AS DailyHours " & _
            '              ", ISNULL(TotHoursWorked, 0) AS TotHoursWorked " & _
            '              ", ISNULL(TotalOvertime, 0) AS TotalOvertime " & _
            '              ", ISNULL(TotShortHours, 0) AS TotShortHours " & _
            '              ", ISNULL(Absent, 0) AS ABSENT " & _
            '              ", ISNULL(hold, 0) AS OnHoldDays " & _
            '              ", ISNULL(DaysWorked, 0) AS TotalDaysWorked " & _
            '              ", ISNULL(PaidAccrueLeave, 0) AS PaidAccrueLeave " & _
            '        "FROM    ( SELECT    @periodid AS period " & _
            '                          ", tnalogin_summary.employeeunkid AS EmpID " & _
            '                          ", ( SELECT    total_days " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS DaysInPeriod " & _
            '                          ", ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                          ", SUM(total_hrs) AS TotHoursWorked " & _
            '                          ", SUM(total_overtime) AS TotalOvertime " & _
            '                          ", SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                  "WHERE     tnalogin_summary.employeeunkid IN ( " & _
            '                            "SELECT DISTINCT " & _
            '                                    "employeeunkid " & _
            '                            "FROM    tnalogin_tran " & _
            '                            "WHERE   holdunkid = 0 " & _
            '                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") ) " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ", tnashift_master.workinghours " & _
            '                ") AS TableA " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                          ") AS TableAbsent ON TableAbsent.employeeunkid = TableA.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(ispaidleave) AS PaidHoliday " & _
            '                    "FROM    tnalogin_summary " & _
            '                            "WHERE   ispaidleave = 1 AND isunpaidleave = 0 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                        "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                                "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidHoliday ON TableA.EmpID = TablePaidHoliday.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(holdunkid) AS hold " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                          ") AS TableHold ON TableHold.employeeunkid = Tablea.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(employeeunkid) AS DaysWorked " & _
            '                            "FROM    tnalogin_summary " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                                    "AND total_hrs <> total_overtime " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TableDaysWorked ON TableA.EmpID = TableDaysWorked.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", SUM(accrue_amount) AS PaidAccrueLeave " & _
            '                            "FROM    lvleavebalance_tran " & _
            '                            "WHERE   ispaid = 1 " & _
            '                                    "AND isvoid = 0 " & _
            '                                    "AND yearunkid = @yearunkid " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidAccrueLeave ON TableA.EmpID = TablePaidAccrueLeave.employeeunkid "
            'strQ = "SELECT  period AS period " & _
            '              ", ISNULL(TableA.EmpID, 0) AS EmpID " & _
            '              ", ISNULL(DaysInPeriod, 0) AS DaysInPeriod " & _
            '              ", ISNULL(PaidHoliday, 0) AS PaidHoliday " & _
            '              ", ISNULL(DailyHours, 0) AS DailyHours " & _
            '              ", ISNULL(TotHoursWorked, 0) AS TotHoursWorked " & _
            '              ", ISNULL(TotalOvertime, 0) AS TotalOvertime " & _
            '              ", ISNULL(TotShortHours, 0) AS TotShortHours " & _
            '              ", ISNULL(Absent, 0) AS ABSENT " & _
            '              ", ISNULL(hold, 0) AS OnHoldDays " & _
            '              ", ISNULL(DaysWorked, 0) AS TotalDaysWorked " & _
            '              ", ISNULL(PaidAccrueLeave, 0) AS PaidAccrueLeave " & _
            '        "FROM    ( SELECT    @periodid AS period " & _
            '                          ", tnalogin_summary.employeeunkid AS EmpID " & _
            '                          ", ( SELECT    total_days " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS DaysInPeriod " & _
            '                          ", ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                          ", SUM(total_hrs) AS TotHoursWorked " & _
            '                          ", SUM(total_overtime) AS TotalOvertime " & _
            '                          ", SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                  "WHERE     tnalogin_summary.employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ", tnashift_master.workinghours " & _
            '                ") AS TableA " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                          ") AS TableAbsent ON TableAbsent.employeeunkid = TableA.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(ispaidleave) AS PaidHoliday " & _
            '                    "FROM    tnalogin_summary " & _
            '                            "WHERE   ispaidleave = 1 " & _
            '                                    "AND isunpaidleave = 0 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                        "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                                "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidHoliday ON TableA.EmpID = TablePaidHoliday.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(holdunkid) AS hold " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                          ") AS TableHold ON TableHold.employeeunkid = Tablea.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(employeeunkid) AS DaysWorked " & _
            '                            "FROM    tnalogin_summary " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                                    "AND total_hrs <> total_overtime " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TableDaysWorked ON TableA.EmpID = TableDaysWorked.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", SUM(accrue_amount) AS PaidAccrueLeave " & _
            '                            "FROM    lvleavebalance_tran " & _
            '                            "WHERE   ispaid = 1 " & _
            '                                    "AND isvoid = 0 " & _
            '                                    "AND yearunkid = @yearunkid " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidAccrueLeave ON TableA.EmpID = TablePaidAccrueLeave.employeeunkid "

            'Sohail (04 Mar 2011) -- Start
            'Changes : New column openingbalance added.
            'strQ = "SELECT  TableEmp.EmpId " & _
            '              ", @periodid AS period " & _
            '              ", ( SELECT    ISNULL(total_days, 0) " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS DaysInPeriod " & _
            '              ", ISNULL(TableShift.DailyHours, 0) AS DailyHours " & _
            '              ", ISNULL(TableTnA.TotHoursWorked, 0) AS TotHoursWorked " & _
            '              ", ISNULL(TableTnA.TotalOvertime, 0) AS TotalOvertime " & _
            '              ", ISNULL(TableTnA.TotShortHours, 0) AS TotShortHours " & _
            '              ", ISNULL(TableAbsent.Absent, 0) AS ABSENT " & _
            '              ", ISNULL(TableHold.hold, 0) AS OnHoldDays " & _
            '              ", ISNULL(TablePaidHoliday.PaidHoliday, 0) AS PaidHoliday " & _
            '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS TotalDaysWorked " & _
            '              ", ISNULL(TablePaidAccrueLeave.PaidAccrueLeave, 0) AS PaidAccrueLeave " & _
            '        "FROM    ( " & strEmpQuery & " ) AS TableEmp " & _
            '                "LEFT JOIN ( SELECT  ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                                  ", hremployee_master.employeeunkid " & _
            '                            "FROM    tnashift_master " & _
            '                                    "LEFT JOIN hremployee_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                            "WHERE   tnashift_master.isactive = 1 " & _
            '                                    "AND hremployee_master.employeeunkid IN (" & strEmployeeList & ") " & _
            '                          ") AS TableShift ON TableShift.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                          ", ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                          ", SUM(total_hrs) AS TotHoursWorked " & _
            '                          ", SUM(total_overtime) AS TotalOvertime " & _
            '                          ", SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                  "WHERE     tnalogin_summary.employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ", tnashift_master.workinghours " & _
            '                          ") AS TableTnA ON TableEmp.EmpId = TableTnA.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                          ") AS TableAbsent ON TableAbsent.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(ispaidleave) AS PaidHoliday " & _
            '                    "FROM    tnalogin_summary " & _
            '                            "WHERE   ispaidleave = 1 " & _
            '                                    "AND isunpaidleave = 0 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                        "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                                "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidHoliday ON TableEmp.EmpID = TablePaidHoliday.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(holdunkid) AS hold " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                          ") AS TableHold ON TableHold.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(employeeunkid) AS DaysWorked " & _
            '                            "FROM    tnalogin_summary " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                                    "AND total_hrs <> total_overtime " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TableDaysWorked ON TableEmp.EmpID = TableDaysWorked.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", SUM(accrue_amount) AS PaidAccrueLeave " & _
            '                            "FROM    lvleavebalance_tran " & _
            '                            "WHERE   ispaid = 1 " & _
            '                                    "AND ISNULL(isvoid,0) = 0 " & _
            '                                    "AND yearunkid = @yearunkid " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidAccrueLeave ON TableEmp.EmpID = TablePaidAccrueLeave.employeeunkid " 'Sohail (16 Oct 2010)

            'Sohail (20 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT  TableEmp.EmpId " & _
            '              ", @periodid AS period " & _
            '              ", ( SELECT    ISNULL(total_days, 0) " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS DaysInPeriod " & _
            '              ", ISNULL(TableShift.DailyHours, 0) AS DailyHours " & _
            '              ", ISNULL(TableTnA.TotHoursWorked, 0) AS TotHoursWorked " & _
            '              ", ISNULL(TableTnA.TotalOvertime, 0) AS TotalOvertime " & _
            '              ", ISNULL(TableTnA.TotShortHours, 0) AS TotShortHours " & _
            '              ", ISNULL(TableAbsent.Absent, 0) AS ABSENT " & _
            '              ", ISNULL(TableHold.hold, 0) AS OnHoldDays " & _
            '              ", ISNULL(TablePaidHoliday.PaidHoliday, 0) AS PaidHoliday " & _
            '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS TotalDaysWorked " & _
            '              ", ISNULL(TablePaidAccrueLeave.PaidAccrueLeave, 0) AS PaidAccrueLeave " & _
            '              ", ISNULL(TableEmp.OpeningBalance, 0) AS OpeningBalance " & _
            '              ", ISNULL(TableEmp.paymenttranunkid, 0) AS paymenttranunkid " & _
            '              ", ISNULL(TableEmp.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '              ", ISNULL(TableEmp.voucherno, 0) AS voucherno " & _
            '              ", ISNULL(TableEmp.PaidAmount, 0) AS PaidAmount " & _
            '        "FROM    (  SELECT   A.EmpID " & _
            '                            ", ISNULL(prtnaleave_tran.OpeningBalance, 0) AS OpeningBalance " & _
            '                            " , ISNULL(prpayment_tran.paymenttranunkid, 0) AS paymenttranunkid" & _
            '                            ", ISNULL(prtnaleave_tran.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '                            ", ISNULL(prtnaleave_tran.voucherno, '') AS voucherno " & _
            '                            ", SUM(ISNULL(prpayment_tran.amount, 0)) AS PaidAmount " & _
            '                    "FROM    (" & strEmpQuery & " ) AS A " & _
            '                        "LEFT JOIN prtnaleave_tran ON A.EmpID = prtnaleave_tran.employeeunkid " & _
            '                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                            "AND ISNULL(prtnaleave_tran.payperiodunkid, @periodid) = @periodid " & _
            '                        "LEFT JOIN prpayment_tran ON prtnaleave_tran.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
            '                                "AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
            '                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
            '                                "AND referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
            '                                "AND paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
            '                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '                    "GROUP BY	A.EmpID, OpeningBalance, paymenttranunkid, tnaleavetranunkid, prtnaleave_tran.voucherno " & _
            '                ") AS TableEmp " & _
            '                "LEFT JOIN ( SELECT  ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                                  ", hremployee_master.employeeunkid " & _
            '                            "FROM    tnashift_master " & _
            '                                    "LEFT JOIN hremployee_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                            "WHERE   tnashift_master.isactive = 1 " & _
            '                                    "AND hremployee_master.employeeunkid IN (" & strEmployeeList & ") " & _
            '                          ") AS TableShift ON TableShift.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                          ", ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                          ", SUM(total_hrs) AS TotHoursWorked " & _
            '                          ", SUM(total_overtime) AS TotalOvertime " & _
            '                          ", SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                  "WHERE     tnalogin_summary.employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ", tnashift_master.workinghours " & _
            '                          ") AS TableTnA ON TableEmp.EmpId = TableTnA.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                          ") AS TableAbsent ON TableAbsent.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(ispaidleave) AS PaidHoliday " & _
            '                    "FROM    tnalogin_summary " & _
            '                            "WHERE   ispaidleave = 1 " & _
            '                                    "AND isunpaidleave = 0 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                        "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                                "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidHoliday ON TableEmp.EmpID = TablePaidHoliday.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(holdunkid) AS hold " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                          ") AS TableHold ON TableHold.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(employeeunkid) AS DaysWorked " & _
            '                            "FROM    tnalogin_summary " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                                    "AND total_hrs <> total_overtime " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TableDaysWorked ON TableEmp.EmpID = TableDaysWorked.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", SUM(accrue_amount) AS PaidAccrueLeave " & _
            '                            "FROM    lvleavebalance_tran " & _
            '                            "WHERE   ispaid = 1 " & _
            '                                    "AND ISNULL(isvoid,0) = 0 " & _
            '                                    "AND yearunkid = @yearunkid " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidAccrueLeave ON TableEmp.EmpID = TablePaidAccrueLeave.employeeunkid "

            'Sohail (24 Aug 2012) -- Start
            'TRA - ENHANCEMENT - The query processor ran out of internal resources and could not produce a query plan. This is a rare event and only expected for extremely complex queries or queries that reference a very large number of tables or partitions. Please simplify the query. If you believe you have received this message in error, contact Customer Support Services for more information.
            'strQ = "DECLARE @table AS TABLE ( EmpID INT ) " & _
            '        "INSERT  INTO @table " & _
            '                "( EmpID ) " & _
            '                "SELECT  employeeunkid " & _
            '                "FROM    hremployee_master " & _
            '                "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
            '        "SELECT  TableEmp.EmpId " & _
            '              ", @periodid AS period " & _
            '              ", ( SELECT    ISNULL(total_days, 0) " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS DaysInPeriod " & _
            '              ", ISNULL(TableShift.DailyHours, 0) AS DailyHours " & _
            '              ", ISNULL(TableTnA.TotHoursWorked, 0) AS TotHoursWorked " & _
            '              ", ISNULL(TableTnA.TotalOvertime, 0) AS TotalOvertime " & _
            '              ", ISNULL(TableTnA.TotShortHours, 0) AS TotShortHours " & _
            '              ", ISNULL(TableAbsent.Absent, 0) AS ABSENT " & _
            '              ", ISNULL(TableHold.hold, 0) AS OnHoldDays " & _
            '              ", ISNULL(TablePaidHoliday.PaidHoliday, 0) AS PaidHoliday " & _
            '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS TotalDaysWorked " & _
            '              ", ISNULL(TablePaidAccrueLeave.PaidAccrueLeave, 0) AS PaidAccrueLeave " & _
            '              ", ISNULL(TableEmp.OpeningBalance, 0) AS OpeningBalance " & _
            '              ", ISNULL(TableEmp.paymenttranunkid, 0) AS paymenttranunkid " & _
            '              ", ISNULL(TableEmp.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '              ", ISNULL(TableEmp.voucherno, 0) AS voucherno " & _
            '              ", ISNULL(TableEmp.PaidAmount, 0) AS PaidAmount " & _
            '        "FROM    (  SELECT   A.EmpID " & _
            '                            ", ISNULL(prtnaleave_tran.OpeningBalance, 0) AS OpeningBalance " & _
            '                            " , ISNULL(prpayment_tran.paymenttranunkid, 0) AS paymenttranunkid" & _
            '                            ", ISNULL(prtnaleave_tran.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '                            ", ISNULL(prtnaleave_tran.voucherno, '') AS voucherno " & _
            '                            ", SUM(ISNULL(prpayment_tran.amount, 0)) AS PaidAmount " & _
            '                    "FROM    ( SELECT  EmpID  FROM   @table) AS A " & _
            '                        "LEFT JOIN prtnaleave_tran ON A.EmpID = prtnaleave_tran.employeeunkid " & _
            '                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                            "AND ISNULL(prtnaleave_tran.payperiodunkid, @periodid) = @periodid " & _
            '                        "LEFT JOIN prpayment_tran ON prtnaleave_tran.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
            '                                "AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
            '                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
            '                                "AND referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
            '                                "AND paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
            '                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '                    "GROUP BY	A.EmpID, OpeningBalance, paymenttranunkid, tnaleavetranunkid, prtnaleave_tran.voucherno " & _
            '                ") AS TableEmp " & _
            '                "LEFT JOIN ( SELECT  ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                                  ", hremployee_master.employeeunkid " & _
            '                            "FROM    tnashift_master " & _
            '                                    "LEFT JOIN hremployee_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                            "WHERE   tnashift_master.isactive = 1 " & _
            '                                    "AND hremployee_master.employeeunkid IN (" & strEmployeeList & ") " & _
            '                          ") AS TableShift ON TableShift.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                          ", ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                          ", SUM(total_hrs) AS TotHoursWorked " & _
            '                          ", SUM(total_overtime) AS TotalOvertime " & _
            '                          ", SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                  "WHERE     tnalogin_summary.employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "AND login_date BETWEEN @startdate AND @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ", tnashift_master.workinghours " & _
            '                          ") AS TableTnA ON TableEmp.EmpId = TableTnA.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                          ") AS TableAbsent ON TableAbsent.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(ispaidleave) AS PaidHoliday " & _
            '                    "FROM    tnalogin_summary " & _
            '                            "WHERE   ispaidleave = 1 " & _
            '                                    "AND isunpaidleave = 0 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                        "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                                "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidHoliday ON TableEmp.EmpID = TablePaidHoliday.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(holdunkid) AS hold " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                          ") AS TableHold ON TableHold.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(employeeunkid) AS DaysWorked " & _
            '                            "FROM    tnalogin_summary " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                                    "AND total_hrs <> total_overtime " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TableDaysWorked ON TableEmp.EmpID = TableDaysWorked.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", SUM(accrue_amount) AS PaidAccrueLeave " & _
            '                            "FROM    lvleavebalance_tran " & _
            '                            "WHERE   ispaid = 1 " & _
            '                                    "AND ISNULL(isvoid,0) = 0 " & _
            '                                    "AND yearunkid = @yearunkid " & _
            '                                    "AND employeeunkid IN (" & strEmployeeList & ") " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidAccrueLeave ON TableEmp.EmpID = TablePaidAccrueLeave.employeeunkid "

            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT - [Daily Hours, TableWeekdays, TableWeekend, TablePublicHoliday] 
            'strQ = "CREATE TABLE #TableEmp ( EmpID INT ) " & _
            '        "INSERT  INTO #TableEmp " & _
            '                "( EmpID " & _
            '                ") " & _
            '                "SELECT  employeeunkid " & _
            '                "FROM    hremployee_master " & _
            '                "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
            '        "SELECT  TableEmp.EmpId " & _
            '              ", @periodid AS period " & _
            '              ", ( SELECT    ISNULL(total_days, 0) " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS DaysInPeriod " & _
            '              ", ISNULL(TableShift.DailyHours, 0) AS DailyHours " & _
            '              ", ISNULL(TableTnA.TotHoursWorked, 0) AS TotHoursWorked " & _
            '              ", ISNULL(TableTnA.TotalOvertime, 0) AS TotalOvertime " & _
            '              ", ISNULL(TableTnA.TotShortHours, 0) AS TotShortHours " & _
            '              ", ISNULL(TableAbsent.Absent, 0) AS ABSENT " & _
            '              ", ISNULL(TableHold.hold, 0) AS OnHoldDays " & _
            '              ", ISNULL(TablePaidHoliday.PaidHoliday, 0) AS PaidHoliday " & _
            '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS TotalDaysWorked " & _
            '              ", ISNULL(TablePaidAccrueLeave.PaidAccrueLeave, 0) AS PaidAccrueLeave " & _
            '              ", ISNULL(TablePayment.OpeningBalance, 0) AS OpeningBalance " & _
            '              ", ISNULL(TablePayment.paymenttranunkid, 0) AS paymenttranunkid " & _
            '              ", ISNULL(TablePayment.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '              ", ISNULL(TablePayment.voucherno, 0) AS voucherno " & _
            '              ", ISNULL(TablePayment.PaidAmount, 0) AS PaidAmount " & _
            '        "FROM    ( SELECT    EmpID " & _
            '                  "FROM      #TableEmp " & _
            '                ") AS TableEmp " & _
            '                "LEFT JOIN ( SELECT  A.EmpID " & _
            '                            ", ISNULL(prtnaleave_tran.OpeningBalance, 0) AS OpeningBalance " & _
            '                            " , ISNULL(prpayment_tran.paymenttranunkid, 0) AS paymenttranunkid" & _
            '                            ", ISNULL(prtnaleave_tran.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '                            ", ISNULL(prtnaleave_tran.voucherno, '') AS voucherno " & _
            '                            ", SUM(ISNULL(prpayment_tran.amount, 0)) AS PaidAmount " & _
            '                            "FROM    #TableEmp AS A " & _
            '                        "LEFT JOIN prtnaleave_tran ON A.EmpID = prtnaleave_tran.employeeunkid " & _
            '                        "LEFT JOIN prpayment_tran ON prtnaleave_tran.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
            '                                "AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
            '                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
            '                                "AND referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
            '                                "AND paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
            '                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '                            "WHERE   ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                                    "AND ISNULL(prtnaleave_tran.payperiodunkid, " & _
            '                                               "@periodid) = @periodid " & _
            '                            "GROUP BY A.EmpID " & _
            '                                  ", OpeningBalance " & _
            '                                  ", paymenttranunkid " & _
            '                                  ", tnaleavetranunkid " & _
            '                                  ", prtnaleave_tran.voucherno " & _
            '                          ") AS TablePayment ON TablePayment.EmpID = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                                  ", hremployee_master.employeeunkid " & _
            '                            "FROM    tnashift_master " & _
            '                                    "LEFT JOIN hremployee_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                                    "LEFT JOIN #TableEmp AS a ON a.EmpID = hremployee_master.employeeunkid " & _
            '                            "WHERE   tnashift_master.isactive = 1 " & _
            '                          ") AS TableShift ON TableShift.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                          ", ISNULL(tnashift_master.workinghours, 0) AS DailyHours " & _
            '                          ", SUM(total_hrs) AS TotHoursWorked " & _
            '                          ", SUM(total_overtime) AS TotalOvertime " & _
            '                          ", SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ", tnashift_master.workinghours " & _
            '                          ") AS TableTnA ON TableEmp.EmpId = TableTnA.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                          ") AS TableAbsent ON TableAbsent.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(ispaidleave) AS PaidHoliday " & _
            '                    "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   ispaidleave = 1 " & _
            '                                    "AND isunpaidleave = 0 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidHoliday ON TableEmp.EmpID = TablePaidHoliday.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(holdunkid) AS hold " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_tran.employeeunkid " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                          ") AS TableHold ON TableHold.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(employeeunkid) AS DaysWorked " & _
            '                            "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                                    "AND total_hrs <> total_overtime " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TableDaysWorked ON TableEmp.EmpID = TableDaysWorked.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", SUM(accrue_amount) AS PaidAccrueLeave " & _
            '                            "FROM    lvleavebalance_tran " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = lvleavebalance_tran.employeeunkid " & _
            '                            "WHERE   ispaid = 1 " & _
            '                                    "AND ISNULL(isvoid,0) = 0 " & _
            '                                    "AND yearunkid = @yearunkid " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidAccrueLeave ON TableEmp.EmpID = TablePaidAccrueLeave.employeeunkid "

            'Sohail (24 Sep 2013) -- Start
            'TRA - ENHANCEMENT - [Shift from Emp Shift tran, OT 1 to 4, PH OT 1 to 4, Weekend OT 1 to 4, Wekdays OT 1 to 4, Day OFF OT 1 to 4, DailyHours removed]
            'strQ = "CREATE TABLE #TableEmp ( EmpID INT, EmpName NVARCHAR(MAX) ) " & _
            '        "INSERT  INTO #TableEmp " & _
            '                "( EmpID " & _
            '                ", EmpName " & _
            '                ") " & _
            '                "SELECT  employeeunkid " & _
            '                      ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
            '                "FROM    hremployee_master " & _
            '                "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
            '        "SELECT  TableEmp.EmpId " & _
            '              ", TableEmp.EmpName " & _
            '              ", @periodid AS period " & _
            '              ", ( SELECT    ISNULL(total_days, 0) " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS DaysInPeriod " & _
            '              ", ( SELECT    ISNULL(constant_days, 0) " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS ConstantDaysInPeriod " & _
            '              ", ISNULL(TableShift.DailyHours, 0) AS DailyHours " & _
            '              ", ISNULL(TableTnA.TotHoursWorked, 0) AS TotHoursWorked " & _
            '              ", ISNULL(TableTnA.TotalOvertime, 0) AS TotalOvertime " & _
            '              ", ISNULL(TableTnA.TotShortHours, 0) AS TotShortHours " & _
            '              ", ISNULL(TableAbsent.Absent, 0) AS ABSENT " & _
            '              ", ISNULL(TableHold.hold, 0) AS OnHoldDays " & _
            '              ", ISNULL(TablePaidHoliday.PaidHoliday, 0) AS PaidHoliday " & _
            '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS TotalDaysWorked " & _
            '              ", ISNULL(TablePaidAccrueLeave.PaidAccrueLeave, 0) AS PaidAccrueLeave " & _
            '              ", ISNULL(TablePayment.OpeningBalance, 0) AS OpeningBalance " & _
            '              ", ISNULL(TablePayment.paymenttranunkid, 0) AS paymenttranunkid " & _
            '              ", ISNULL(TablePayment.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '              ", ISNULL(TablePayment.voucherno, 0) AS voucherno " & _
            '              ", ISNULL(TablePayment.PaidAmount, 0) AS PaidAmount " & _
            '              ", ISNULL(TableWeekdays.WeekdaysWorkedHours, 0) AS WeekdaysWorkedHours " & _
            '              ", ISNULL(TableWeekdays.WeekdaysOvertimeHours, 0) AS WeekdaysOvertimeHours " & _
            '              ", ISNULL(TableWeekdays.WeekdaysShortHours, 0) AS WeekdaysShortHours " & _
            '              ", ISNULL(TableWeekdays.WeekdaysNightHours, 0) AS WeekdaysNightHours " & _
            '              ", ISNULL(TableWeekend.WeekendWorkedHours, 0) AS WeekendWorkedHours " & _
            '              ", ISNULL(TableWeekend.WeekendOvertimeHours, 0) AS WeekendOvertimeHours " & _
            '              ", ISNULL(TableWeekend.WeekendShortHours, 0) AS WeekendShortHours " & _
            '              ", ISNULL(TableWeekend.WeekendNightHours, 0) AS WeekendNightHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayWorkedHours, 0) AS PublicHolidayWorkedHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours, 0) AS PublicHolidayOvertimeHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayShortHours, 0) AS PublicHolidayShortHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayNightHours, 0) AS PublicHolidayNightHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayDays, 0) AS PublicHolidayDays " & _
            '        "FROM    ( SELECT    EmpID " & _
            '                          ", EmpName " & _
            '                  "FROM      #TableEmp " & _
            '                ") AS TableEmp " & _
            '                "LEFT JOIN ( SELECT  A.EmpID " & _
            '                            ", ISNULL(prtnaleave_tran.OpeningBalance, 0) AS OpeningBalance " & _
            '                            " , ISNULL(prpayment_tran.paymenttranunkid, 0) AS paymenttranunkid" & _
            '                            ", ISNULL(prtnaleave_tran.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '                            ", ISNULL(prtnaleave_tran.voucherno, '') AS voucherno " & _
            '                            ", SUM(ISNULL(prpayment_tran.amount, 0)) AS PaidAmount " & _
            '                            "FROM    #TableEmp AS A " & _
            '                        "LEFT JOIN prtnaleave_tran ON A.EmpID = prtnaleave_tran.employeeunkid " & _
            '                        "LEFT JOIN prpayment_tran ON prtnaleave_tran.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
            '                                "AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
            '                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
            '                                "AND referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
            '                                "AND paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
            '                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '                            "WHERE   ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                                    "AND ISNULL(prtnaleave_tran.payperiodunkid, " & _
            '                                               "@periodid) = @periodid " & _
            '                            "GROUP BY A.EmpID " & _
            '                                  ", OpeningBalance " & _
            '                                  ", paymenttranunkid " & _
            '                                  ", tnaleavetranunkid " & _
            '                                  ", prtnaleave_tran.voucherno " & _
            '                          ") AS TablePayment ON TablePayment.EmpID = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  weekhours " & _
            '                                    "/ ( SELECT  COUNT(*) " & _
            '                                        "FROM    tnashift_tran " & _
            '                                        "WHERE   tnashift_tran.shiftunkid = tnashift_master.shiftunkid " & _
            '                                                "AND isweekend = 0 " & _
            '                                      ") AS DailyHours " & _
            '                                  ", hremployee_master.employeeunkid " & _
            '                            "FROM    tnashift_master " & _
            '                                    "LEFT JOIN hremployee_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
            '                                    "LEFT JOIN #TableEmp AS a ON a.EmpID = hremployee_master.employeeunkid " & _
            '                            "WHERE   tnashift_master.isactive = 1 " & _
            '                          ") AS TableShift ON TableShift.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                          ", SUM(total_hrs) AS TotHoursWorked " & _
            '                          ", SUM(total_overtime) AS TotalOvertime " & _
            '                          ", SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ") AS TableTnA ON TableEmp.EmpId = TableTnA.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                          ") AS TableAbsent ON TableAbsent.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(ispaidleave) AS PaidHoliday " & _
            '                    "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   ispaidleave = 1 " & _
            '                                    "AND isunpaidleave = 0 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidHoliday ON TableEmp.EmpID = TablePaidHoliday.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(holdunkid) AS hold " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_tran.employeeunkid " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                          ") AS TableHold ON TableHold.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(employeeunkid) AS DaysWorked " & _
            '                            "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                                    "AND total_hrs <> total_overtime " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TableDaysWorked ON TableEmp.EmpID = TableDaysWorked.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", SUM(accrue_amount) AS PaidAccrueLeave " & _
            '                            "FROM    lvleavebalance_tran " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = lvleavebalance_tran.employeeunkid " & _
            '                            "WHERE   ispaid = 1 " & _
            '                                    "AND ISNULL(isvoid,0) = 0 " & _
            '                                    "AND yearunkid = @yearunkid " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidAccrueLeave ON TableEmp.EmpID = TablePaidAccrueLeave.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                                  ", SUM(total_hrs) AS WeekdaysWorkedHours " & _
            '                                  ", SUM(total_overtime) AS WeekdaysOvertimeHours " & _
            '                                  ", SUM(total_short_hrs) AS WeekdaysShortHours " & _
            '                                  ", SUM(total_nighthrs) AS WeekdaysNightHours " & _
            '                            "FROM      tnalogin_summary " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                            "AND ISNULL(isweekend, 0) = 0 " & _
            '                            "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ") AS TableWeekdays ON TableEmp.EmpId = TableWeekdays.EmpID " & _
            '               "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                                  ", SUM(total_hrs) AS WeekendWorkedHours " & _
            '                                  ", SUM(total_overtime) AS WeekendOvertimeHours " & _
            '                                  ", SUM(total_short_hrs) AS WeekendShortHours " & _
            '                                  ", SUM(total_nighthrs) AS WeekendNightHours " & _
            '                            "FROM      tnalogin_summary " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                            "AND ISNULL(isweekend, 0) = 1 " & _
            '                            "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ") AS TableWeekend ON TableEmp.EmpId = TableWeekend.EmpID " & _
            '              "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                                  ", SUM(total_hrs) AS PublicHolidayWorkedHours " & _
            '                                  ", SUM(total_overtime) AS PublicHolidayOvertimeHours " & _
            '                                  ", SUM(total_short_hrs) AS PublicHolidayShortHours " & _
            '                                  ", SUM(total_nighthrs) AS PublicHolidayNightHours " & _
            '                                  ", COUNT(tnalogin_summary.employeeunkid) AS PublicHolidayDays " & _
            '                            "FROM      tnalogin_summary " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                            "AND ISNULL(isholiday, 0) = 1 " & _
            '                            "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ") AS TablePublicHoliday ON TableEmp.EmpId = TablePublicHoliday.EmpID "

            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            'strQ = "CREATE TABLE #TableEmp ( EmpID INT, EmpName NVARCHAR(MAX) ) " & _
            '        "INSERT  INTO #TableEmp " & _
            '                "( EmpID " & _
            '                ", EmpName " & _
            '                ") " & _
            '                "SELECT  employeeunkid " & _
            '                      ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
            '                "FROM    hremployee_master " & _
            '                "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
            '        "SELECT  TableEmp.EmpId " & _
            '              ", TableEmp.EmpName " & _
            '              ", @periodid AS period " & _
            '              ", ( SELECT    ISNULL(total_days, 0) " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS DaysInPeriod " & _
            '              ", ( SELECT    ISNULL(constant_days, 0) " & _
            '                      "FROM      cfcommon_period_tran " & _
            '                      "WHERE     periodunkid = @periodid " & _
            '                                "AND isactive = 1 " & _
            '                            ") AS ConstantDaysInPeriod " & _
            '              ", ISNULL(TableTnA.TotHoursWorked, 0) AS TotHoursWorked " & _
            '              ", ISNULL(TableTnA.TotalOvertime, 0) AS TotalOvertime " & _
            '              ", ISNULL(TableTnA.TotalOvertime2, 0) AS TotalOvertime2 " & _
            '              ", ISNULL(TableTnA.TotalOvertime3, 0) AS TotalOvertime3 " & _
            '              ", ISNULL(TableTnA.TotalOvertime4, 0) AS TotalOvertime4 " & _
            '              ", ISNULL(TableTnA.TotShortHours, 0) AS TotShortHours " & _
            '              ", ISNULL(TableAbsent.Absent, 0) AS ABSENT " & _
            '              ", ISNULL(TableHold.hold, 0) AS OnHoldDays " & _
            '              ", ISNULL(TablePaidHoliday.PaidHoliday, 0) AS PaidHoliday " & _
            '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS TotalDaysWorked " & _
            '              ", ISNULL(TablePaidAccrueLeave.PaidAccrueLeave, 0) AS PaidAccrueLeave " & _
            '              ", ISNULL(TablePayment.OpeningBalance, 0) AS OpeningBalance " & _
            '              ", ISNULL(TablePayment.paymenttranunkid, 0) AS paymenttranunkid " & _
            '              ", ISNULL(TablePayment.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '              ", ISNULL(TablePayment.voucherno, 0) AS voucherno " & _
            '              ", ISNULL(TablePayment.PaidAmount, 0) AS PaidAmount " & _
            '              ", ISNULL(TableWeekdays.WeekdaysWorkedHours, 0) AS WeekdaysWorkedHours " & _
            '              ", ISNULL(TableWeekdays.WeekdaysOvertimeHours, 0) AS WeekdaysOvertimeHours " & _
            '              ", ISNULL(TableWeekdays.WeekdaysOvertimeHours2, 0) AS WeekdaysOvertimeHours2 " & _
            '              ", ISNULL(TableWeekdays.WeekdaysOvertimeHours3, 0) AS WeekdaysOvertimeHours3 " & _
            '              ", ISNULL(TableWeekdays.WeekdaysOvertimeHours4, 0) AS WeekdaysOvertimeHours4 " & _
            '              ", ISNULL(TableWeekdays.WeekdaysShortHours, 0) AS WeekdaysShortHours " & _
            '              ", ISNULL(TableWeekdays.WeekdaysNightHours, 0) AS WeekdaysNightHours " & _
            '              ", ISNULL(TableWeekend.WeekendWorkedHours, 0) AS WeekendWorkedHours " & _
            '              ", ISNULL(TableWeekend.WeekendOvertimeHours, 0) AS WeekendOvertimeHours " & _
            '              ", ISNULL(TableWeekend.WeekendOvertimeHours2, 0) AS WeekendOvertimeHours2 " & _
            '              ", ISNULL(TableWeekend.WeekendOvertimeHours3, 0) AS WeekendOvertimeHours3 " & _
            '              ", ISNULL(TableWeekend.WeekendOvertimeHours4, 0) AS WeekendOvertimeHours4 " & _
            '              ", ISNULL(TableWeekend.WeekendShortHours, 0) AS WeekendShortHours " & _
            '              ", ISNULL(TableWeekend.WeekendNightHours, 0) AS WeekendNightHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayWorkedHours, 0) AS PublicHolidayWorkedHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours, 0) AS PublicHolidayOvertimeHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours2, 0) AS PublicHolidayOvertimeHours2 " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours3, 0) AS PublicHolidayOvertimeHours3 " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours4, 0) AS PublicHolidayOvertimeHours4 " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayShortHours, 0) AS PublicHolidayShortHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayNightHours, 0) AS PublicHolidayNightHours " & _
            '              ", ISNULL(TablePublicHoliday.PublicHolidayDays, 0) AS PublicHolidayDays " & _
            '              ", ISNULL(TableDayOff.DayOffDays, 0) AS DayOffDays " & _
            '              ", ISNULL(TableDayOff.DayOffWorkedHours, 0) AS DayOffWorkedHours " & _
            '              ", ISNULL(TableDayOff.DayOffOvertimeHours, 0) AS DayOffOvertimeHours " & _
            '              ", ISNULL(TableDayOff.DayOffOvertimeHours2, 0) AS DayOffOvertimeHours2 " & _
            '              ", ISNULL(TableDayOff.DayOffOvertimeHours3, 0) AS DayOffOvertimeHours3 " & _
            '              ", ISNULL(TableDayOff.DayOffOvertimeHours4, 0) AS DayOffOvertimeHours4 " & _
            '              ", ISNULL(TableDayOff.DayOffShortHours, 0) AS DayOffShortHours " & _
            '              ", ISNULL(TableDayOff.DayOffNightHours, 0) AS DayOffNightHours " & _
            '        "FROM    ( SELECT    EmpID " & _
            '                          ", EmpName " & _
            '                  "FROM      #TableEmp " & _
            '                ") AS TableEmp " & _
            '                "LEFT JOIN ( SELECT  A.EmpID " & _
            '                            ", ISNULL(prtnaleave_tran.OpeningBalance, 0) AS OpeningBalance " & _
            '                            " , ISNULL(prpayment_tran.paymenttranunkid, 0) AS paymenttranunkid" & _
            '                            ", ISNULL(prtnaleave_tran.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
            '                            ", ISNULL(prtnaleave_tran.voucherno, '') AS voucherno " & _
            '                            ", SUM(ISNULL(prpayment_tran.amount, 0)) AS PaidAmount " & _
            '                            "FROM    #TableEmp AS A " & _
            '                        "LEFT JOIN prtnaleave_tran ON A.EmpID = prtnaleave_tran.employeeunkid " & _
            '                        "LEFT JOIN prpayment_tran ON prtnaleave_tran.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
            '                                "AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
            '                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
            '                                "AND referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
            '                                "AND paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
            '                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '                            "WHERE   ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                                    "AND ISNULL(prtnaleave_tran.payperiodunkid, " & _
            '                                               "@periodid) = @periodid " & _
            '                            "GROUP BY A.EmpID " & _
            '                                  ", OpeningBalance " & _
            '                                  ", paymenttranunkid " & _
            '                                  ", tnaleavetranunkid " & _
            '                                  ", prtnaleave_tran.voucherno " & _
            '                          ") AS TablePayment ON TablePayment.EmpID = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                          ", SUM(total_hrs) AS TotHoursWorked " & _
            '                          ", SUM(total_overtime) AS TotalOvertime " & _
            '                          ", ISNULL(SUM(ot2), 0) AS TotalOvertime2 " & _
            '                          ", ISNULL(SUM(ot3), 0) AS TotalOvertime3 " & _
            '                          ", ISNULL(SUM(ot4), 0) AS TotalOvertime4 " & _
            '                          ", SUM(total_short_hrs) AS TotShortHours " & _
            '          "FROM      tnalogin_summary " & _
            '                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                  "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ") AS TableTnA ON TableEmp.EmpId = TableTnA.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(isunpaidleave) AS Absent " & _
            '                    "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                                      "WHERE     isunpaidleave = 1 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                          ") AS TableAbsent ON TableAbsent.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(ispaidleave) AS PaidHoliday " & _
            '                    "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   ispaidleave = 1 " & _
            '                                    "AND isunpaidleave = 0 " & _
            '                                    "AND login_date BETWEEN @startdate AND @enddate " & _
            '                                                "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidHoliday ON TableEmp.EmpID = TablePaidHoliday.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(holdunkid) AS hold " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                          "FROM      tnalogin_tran " & _
            '                                                "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_tran.employeeunkid " & _
            '                                                          "WHERE     holdunkid > 0 " & _
            '                                                                    "AND logindate BETWEEN @startdate AND @enddate " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                              ", holdunkid " & _
            '                                              ", logindate " & _
            '                                                        ") AS C " & _
            '                                                "GROUP BY C.employeeunkid " & _
            '                          ") AS TableHold ON TableHold.employeeunkid = TableEmp.EmpID " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", COUNT(employeeunkid) AS DaysWorked " & _
            '                            "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                                    "AND total_hrs <> total_overtime " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TableDaysWorked ON TableEmp.EmpID = TableDaysWorked.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  employeeunkid " & _
            '                                  ", SUM(accrue_amount) AS PaidAccrueLeave " & _
            '                            "FROM    lvleavebalance_tran " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = lvleavebalance_tran.employeeunkid " & _
            '                            "WHERE   ispaid = 1 " & _
            '                                    "AND ISNULL(isvoid,0) = 0 " & _
            '                                    "AND yearunkid = @yearunkid " & _
            '                            "GROUP BY employeeunkid " & _
            '                          ") AS TablePaidAccrueLeave ON TableEmp.EmpID = TablePaidAccrueLeave.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                                  ", SUM(total_hrs) AS WeekdaysWorkedHours " & _
            '                                  ", SUM(total_overtime) AS WeekdaysOvertimeHours " & _
            '                                  ", ISNULL(SUM(ot2), 0) AS WeekdaysOvertimeHours2 " & _
            '                                  ", ISNULL(SUM(ot3), 0) AS WeekdaysOvertimeHours3 " & _
            '                                  ", ISNULL(SUM(ot4), 0) AS WeekdaysOvertimeHours4 " & _
            '                                  ", SUM(total_short_hrs) AS WeekdaysShortHours " & _
            '                                  ", SUM(total_nighthrs) AS WeekdaysNightHours " & _
            '                            "FROM      tnalogin_summary " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                            "AND ISNULL(isweekend, 0) = 0 " & _
            '                            "AND ISNULL(isdayoff, 0) = 0 " & _
            '                            "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ") AS TableWeekdays ON TableEmp.EmpId = TableWeekdays.EmpID " & _
            '               "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                                  ", SUM(total_hrs) AS WeekendWorkedHours " & _
            '                                  ", SUM(total_overtime) AS WeekendOvertimeHours " & _
            '                                  ", ISNULL(SUM(ot2), 0) AS WeekendOvertimeHours2 " & _
            '                                  ", ISNULL(SUM(ot3), 0) AS WeekendOvertimeHours3 " & _
            '                                  ", ISNULL(SUM(ot4), 0) AS WeekendOvertimeHours4 " & _
            '                                  ", SUM(total_short_hrs) AS WeekendShortHours " & _
            '                                  ", SUM(total_nighthrs) AS WeekendNightHours " & _
            '                            "FROM      tnalogin_summary " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                            "AND ISNULL(isweekend, 0) = 1 " & _
            '                            "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ") AS TableWeekend ON TableEmp.EmpId = TableWeekend.EmpID " & _
            '              "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                                  ", SUM(total_hrs) AS PublicHolidayWorkedHours " & _
            '                                  ", SUM(total_overtime) AS PublicHolidayOvertimeHours " & _
            '                                  ", ISNULL(SUM(ot2), 0) AS PublicHolidayOvertimeHours2 " & _
            '                                  ", ISNULL(SUM(ot3), 0) AS PublicHolidayOvertimeHours3 " & _
            '                                  ", ISNULL(SUM(ot4), 0) AS PublicHolidayOvertimeHours4 " & _
            '                                  ", SUM(total_short_hrs) AS PublicHolidayShortHours " & _
            '                                  ", SUM(total_nighthrs) AS PublicHolidayNightHours " & _
            '                                  ", COUNT(tnalogin_summary.employeeunkid) AS PublicHolidayDays " & _
            '                            "FROM      tnalogin_summary " & _
            '                            "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                            "AND ISNULL(isholiday, 0) = 1 " & _
            '                            "GROUP BY  tnalogin_summary.employeeunkid " & _
            '                          ") AS TablePublicHoliday ON TableEmp.EmpId = TablePublicHoliday.EmpID " & _
            '              "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
            '                                  ", SUM(total_hrs) AS DayOffWorkedHours " & _
            '                                  ", SUM(total_overtime) AS DayOffOvertimeHours " & _
            '                                  ", ISNULL(SUM(ot2), 0) AS DayOffOvertimeHours2 " & _
            '                                  ", ISNULL(SUM(ot3), 0) AS DayOffOvertimeHours3 " & _
            '                                  ", ISNULL(SUM(ot4), 0) AS DayOffOvertimeHours4 " & _
            '                                  ", SUM(total_short_hrs) AS DayOffShortHours " & _
            '                                  ", SUM(total_nighthrs) AS DayOffNightHours " & _
            '                                  ", COUNT(tnalogin_summary.employeeunkid) AS DayOffDays " & _
            '                            "FROM    tnalogin_summary " & _
            '                                    "LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
            '                                    "LEFT JOIN #TableEmp AS A ON A.EmpId = tnalogin_summary.employeeunkid " & _
            '                            "WHERE   login_date BETWEEN @startdate AND @enddate " & _
            '                                    "AND ISNULL(isdayoff, 0) = 1 " & _
            '                            "GROUP BY tnalogin_summary.employeeunkid " & _
            '                          ") AS TableDayOff ON TableEmp.EmpId = TableDayOff.EmpID "
            strB.Length = 0
            strB.Append("CREATE TABLE #TableEmp ( EmpID INT, EmpName NVARCHAR(MAX) ) " & _
                    "INSERT  INTO #TableEmp " & _
                            "( EmpID " & _
                            ", EmpName " & _
                            ") " & _
                            "SELECT  employeeunkid " & _
                                  ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                            "FROM    hremployee_master " & _
                            "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
                    "SELECT  TableEmp.EmpId " & _
                          ", TableEmp.EmpName " & _
                          ", @periodid AS period " & _
                          ", ( SELECT    ISNULL(total_days, 0) " & _
                                  "FROM      cfcommon_period_tran " & _
                                  "WHERE     periodunkid = @periodid " & _
                                            "AND isactive = 1 " & _
                                        ") AS DaysInPeriod " & _
                          ", ( SELECT    ISNULL(constant_days, 0) " & _
                                  "FROM      cfcommon_period_tran " & _
                                  "WHERE     periodunkid = @periodid " & _
                                            "AND isactive = 1 " & _
                                        ") AS ConstantDaysInPeriod " & _
                          ", ISNULL(TableTnA.TotHoursWorked, 0) AS TotHoursWorked " & _
                          ", ISNULL(TableTnA.TotalOvertime, 0) AS TotalOvertime " & _
                          ", ISNULL(TableTnA.TotalOvertime2, 0) AS TotalOvertime2 " & _
                          ", ISNULL(TableTnA.TotalOvertime3, 0) AS TotalOvertime3 " & _
                          ", ISNULL(TableTnA.TotalOvertime4, 0) AS TotalOvertime4 " & _
                          ", ISNULL(TableTnA.TotShortHours, 0) AS TotShortHours " & _
                          ", ISNULL(TableAbsent.Absent, 0) AS ABSENT " & _
                          ", ISNULL(TableHold.hold, 0) AS OnHoldDays " & _
                          ", ISNULL(TablePaidHoliday.PaidHoliday, 0) AS PaidHoliday " & _
                          ", ISNULL(TableDaysWorked.DaysWorked, 0) AS TotalDaysWorked " & _
                          ", ISNULL(TablePaidAccrueLeave.PaidAccrueLeave, 0) AS PaidAccrueLeave " & _
                          ", ISNULL(TablePayment.OpeningBalance, 0) AS OpeningBalance " & _
                          ", ISNULL(TablePayment.paymenttranunkid, 0) AS paymenttranunkid " & _
                          ", ISNULL(TablePayment.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
                          ", ISNULL(TablePayment.voucherno, 0) AS voucherno " & _
                          ", ISNULL(TablePayment.PaidAmount, 0) AS PaidAmount " & _
                          ", ISNULL(TableWeekdays.WeekdaysWorkedHours, 0) AS WeekdaysWorkedHours " & _
                          ", ISNULL(TableWeekdays.WeekdaysOvertimeHours, 0) AS WeekdaysOvertimeHours " & _
                          ", ISNULL(TableWeekdays.WeekdaysOvertimeHours2, 0) AS WeekdaysOvertimeHours2 " & _
                          ", ISNULL(TableWeekdays.WeekdaysOvertimeHours3, 0) AS WeekdaysOvertimeHours3 " & _
                          ", ISNULL(TableWeekdays.WeekdaysOvertimeHours4, 0) AS WeekdaysOvertimeHours4 " & _
                          ", ISNULL(TableWeekdays.WeekdaysShortHours, 0) AS WeekdaysShortHours " & _
                          ", ISNULL(TableWeekdays.WeekdaysNightHours, 0) AS WeekdaysNightHours " & _
                          ", ISNULL(TableWeekend.WeekendWorkedHours, 0) AS WeekendWorkedHours " & _
                          ", ISNULL(TableWeekend.WeekendOvertimeHours, 0) AS WeekendOvertimeHours " & _
                          ", ISNULL(TableWeekend.WeekendOvertimeHours2, 0) AS WeekendOvertimeHours2 " & _
                          ", ISNULL(TableWeekend.WeekendOvertimeHours3, 0) AS WeekendOvertimeHours3 " & _
                          ", ISNULL(TableWeekend.WeekendOvertimeHours4, 0) AS WeekendOvertimeHours4 " & _
                          ", ISNULL(TableWeekend.WeekendShortHours, 0) AS WeekendShortHours " & _
                          ", ISNULL(TableWeekend.WeekendNightHours, 0) AS WeekendNightHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayWorkedHours, 0) AS PublicHolidayWorkedHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours, 0) AS PublicHolidayOvertimeHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours2, 0) AS PublicHolidayOvertimeHours2 " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours3, 0) AS PublicHolidayOvertimeHours3 " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours4, 0) AS PublicHolidayOvertimeHours4 " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayShortHours, 0) AS PublicHolidayShortHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayNightHours, 0) AS PublicHolidayNightHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayDays, 0) AS PublicHolidayDays " & _
                          ", ISNULL(TableDayOff.DayOffDays, 0) AS DayOffDays " & _
                          ", ISNULL(TableDayOff.DayOffWorkedHours, 0) AS DayOffWorkedHours " & _
                          ", ISNULL(TableDayOff.DayOffOvertimeHours, 0) AS DayOffOvertimeHours " & _
                          ", ISNULL(TableDayOff.DayOffOvertimeHours2, 0) AS DayOffOvertimeHours2 " & _
                          ", ISNULL(TableDayOff.DayOffOvertimeHours3, 0) AS DayOffOvertimeHours3 " & _
                          ", ISNULL(TableDayOff.DayOffOvertimeHours4, 0) AS DayOffOvertimeHours4 " & _
                          ", ISNULL(TableDayOff.DayOffShortHours, 0) AS DayOffShortHours " & _
                          ", ISNULL(TableDayOff.DayOffNightHours, 0) AS DayOffNightHours " & _
                    "FROM    ( SELECT    EmpID " & _
                                      ", EmpName " & _
                              "FROM      #TableEmp " & _
                            ") AS TableEmp " & _
                            "LEFT JOIN ( SELECT  A.EmpID " & _
                                        ", ISNULL(prtnaleave_tran.OpeningBalance, 0) AS OpeningBalance " & _
                                        " , ISNULL(prpayment_tran.paymenttranunkid, 0) AS paymenttranunkid" & _
                                        ", ISNULL(prtnaleave_tran.tnaleavetranunkid, 0) AS tnaleavetranunkid " & _
                                        ", ISNULL(prtnaleave_tran.voucherno, '') AS voucherno " & _
                                        ", SUM(ISNULL(prpayment_tran.amount, 0)) AS PaidAmount " & _
                                        "FROM    #TableEmp AS A " & _
                                    "LEFT JOIN prtnaleave_tran ON A.EmpID = prtnaleave_tran.employeeunkid " & _
                                    "LEFT JOIN prpayment_tran ON prtnaleave_tran.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
                                            "AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                            "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                            "AND referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                            "AND paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                        "WHERE   ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                "AND ISNULL(prtnaleave_tran.payperiodunkid, @periodid) = @periodid " & _
                                        "GROUP BY A.EmpID " & _
                                              ", OpeningBalance " & _
                                              ", paymenttranunkid " & _
                                              ", tnaleavetranunkid " & _
                                              ", prtnaleave_tran.voucherno " & _
                                      ") AS TablePayment ON TablePayment.EmpID = TableEmp.EmpID " & _
                            "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
                                      ", SUM(total_hrs) AS TotHoursWorked " & _
                                      ", SUM(total_overtime) AS TotalOvertime " & _
                                      ", ISNULL(SUM(ot2), 0) AS TotalOvertime2 " & _
                                      ", ISNULL(SUM(ot3), 0) AS TotalOvertime3 " & _
                                      ", ISNULL(SUM(ot4), 0) AS TotalOvertime4 " & _
                                      ", SUM(total_short_hrs) AS TotShortHours " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                              "GROUP BY  tnalogin_summary.employeeunkid " & _
                                      ") AS TableTnA ON TableEmp.EmpId = TableTnA.EmpID " & _
                            "LEFT JOIN ( SELECT  employeeunkid " & _
                                              ", SUM(lvdayfraction) AS Absent " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                                  "WHERE     isunpaidleave = 1 " & _
                                                "AND login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                                  "GROUP BY  employeeunkid " & _
                                      ") AS TableAbsent ON TableAbsent.employeeunkid = TableEmp.EmpID " & _
                            "LEFT JOIN ( SELECT  employeeunkid " & _
                                              ", SUM(lvdayfraction) AS PaidHoliday " & _
                                "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                        "WHERE   ispaidleave = 1 " & _
                                                "AND isunpaidleave = 0 " & _
                                                "AND login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                                            "GROUP BY employeeunkid " & _
                                      ") AS TablePaidHoliday ON TableEmp.EmpID = TablePaidHoliday.employeeunkid " & _
                            "LEFT JOIN ( SELECT  employeeunkid " & _
                                              ", COUNT(holdunkid) AS hold " & _
                                        "FROM    ( SELECT    employeeunkid " & _
                                                          ", holdunkid " & _
                                                          ", logindate " & _
                                                  "FROM      #TableEmp AS A " & _
                                                            "LEFT JOIN tnalogin_tran ON A.EmpId = tnalogin_tran.employeeunkid " & _
                                                                      "WHERE     holdunkid > 0 " & _
                                                            "AND logindate BETWEEN @tna_startdate AND @tna_enddate " & _
                                                  "GROUP BY  employeeunkid " & _
                                                          ", holdunkid " & _
                                                          ", logindate " & _
                                                                    ") AS C " & _
                                                            "GROUP BY C.employeeunkid " & _
                                      ") AS TableHold ON TableHold.employeeunkid = TableEmp.EmpID " & _
                            "LEFT JOIN ( SELECT  employeeunkid " & _
                                              ", COUNT(employeeunkid) - SUM(lvdayfraction) AS DaysWorked " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                                "AND total_hrs <> total_overtime " & _
                                        "GROUP BY employeeunkid " & _
                                      ") AS TableDaysWorked ON TableEmp.EmpID = TableDaysWorked.employeeunkid " & _
                            "LEFT JOIN ( SELECT  employeeunkid " & _
                                              ", SUM(accrue_amount) AS PaidAccrueLeave " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN lvleavebalance_tran ON A.EmpId = lvleavebalance_tran.employeeunkid " & _
                                        "WHERE   ispaid = 1 " & _
                                                "AND ISNULL(isvoid,0) = 0 " & _
                                                "AND yearunkid = @yearunkid " & _
                                        "GROUP BY employeeunkid " & _
                                      ") AS TablePaidAccrueLeave ON TableEmp.EmpID = TablePaidAccrueLeave.employeeunkid " & _
                            "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
                                              ", SUM(total_hrs) AS WeekdaysWorkedHours " & _
                                              ", SUM(total_overtime) AS WeekdaysOvertimeHours " & _
                                              ", ISNULL(SUM(ot2), 0) AS WeekdaysOvertimeHours2 " & _
                                              ", ISNULL(SUM(ot3), 0) AS WeekdaysOvertimeHours3 " & _
                                              ", ISNULL(SUM(ot4), 0) AS WeekdaysOvertimeHours4 " & _
                                              ", SUM(total_short_hrs) AS WeekdaysShortHours " & _
                                              ", SUM(total_nighthrs) AS WeekdaysNightHours " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                        "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                        "AND ISNULL(isweekend, 0) = 0 " & _
                                        "AND ISNULL(isdayoff, 0) = 0 " & _
                                        "GROUP BY  tnalogin_summary.employeeunkid " & _
                                      ") AS TableWeekdays ON TableEmp.EmpId = TableWeekdays.EmpID " & _
                           "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
                                              ", SUM(total_hrs) AS WeekendWorkedHours " & _
                                              ", SUM(total_overtime) AS WeekendOvertimeHours " & _
                                              ", ISNULL(SUM(ot2), 0) AS WeekendOvertimeHours2 " & _
                                              ", ISNULL(SUM(ot3), 0) AS WeekendOvertimeHours3 " & _
                                              ", ISNULL(SUM(ot4), 0) AS WeekendOvertimeHours4 " & _
                                              ", SUM(total_short_hrs) AS WeekendShortHours " & _
                                              ", SUM(total_nighthrs) AS WeekendNightHours " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                        "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                        "AND ISNULL(isweekend, 0) = 1 " & _
                                        "GROUP BY  tnalogin_summary.employeeunkid " & _
                                      ") AS TableWeekend ON TableEmp.EmpId = TableWeekend.EmpID " & _
                          "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
                                              ", SUM(total_hrs) AS PublicHolidayWorkedHours " & _
                                              ", SUM(total_overtime) AS PublicHolidayOvertimeHours " & _
                                              ", ISNULL(SUM(ot2), 0) AS PublicHolidayOvertimeHours2 " & _
                                              ", ISNULL(SUM(ot3), 0) AS PublicHolidayOvertimeHours3 " & _
                                              ", ISNULL(SUM(ot4), 0) AS PublicHolidayOvertimeHours4 " & _
                                              ", SUM(total_short_hrs) AS PublicHolidayShortHours " & _
                                              ", SUM(total_nighthrs) AS PublicHolidayNightHours " & _
                                              ", COUNT(tnalogin_summary.employeeunkid) AS PublicHolidayDays " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                        "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                        "AND ISNULL(isholiday, 0) = 1 " & _
                                        "GROUP BY  tnalogin_summary.employeeunkid " & _
                                      ") AS TablePublicHoliday ON TableEmp.EmpId = TablePublicHoliday.EmpID " & _
                          "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
                                              ", SUM(total_hrs) AS DayOffWorkedHours " & _
                                              ", SUM(total_overtime) AS DayOffOvertimeHours " & _
                                              ", ISNULL(SUM(ot2), 0) AS DayOffOvertimeHours2 " & _
                                              ", ISNULL(SUM(ot3), 0) AS DayOffOvertimeHours3 " & _
                                              ", ISNULL(SUM(ot4), 0) AS DayOffOvertimeHours4 " & _
                                              ", SUM(total_short_hrs) AS DayOffShortHours " & _
                                              ", SUM(total_nighthrs) AS DayOffNightHours " & _
                                              ", COUNT(tnalogin_summary.employeeunkid) AS DayOffDays " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                                "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                                "AND ISNULL(isdayoff, 0) = 1 " & _
                                        "GROUP BY tnalogin_summary.employeeunkid " & _
                                      ") AS TableDayOff ON TableEmp.EmpId = TableDayOff.EmpID " _
                                      )
            'Sohail (09 Jun 2014) - [COUNT(isunpaidleave) AS Absent = SUM(lvdayfraction) AS Absent,  SUM(lvdayfraction) AS PaidHoliday = COUNT(ispaidleave) AS PaidHoliday,   COUNT(employeeunkid) AS DaysWorked =  COUNT(employeeunkid) - SUM(lvdayfraction) AS DaysWorked]
            'Sohail (07 Jan 2014) -- End
            'Sohail (24 Sep 2013) -- End
            'Sohail (10 Jul 2013) - [constant_days]
            'Sohail (12 Nov 2012) - [PublicHolidayDays]
            'Sohail (29 Oct 2012) -- End

            strB.Append("ORDER BY TableEmp.EmpName; DROP TABLE #TableEmp ")

            'Sohail (24 Aug 2012) -- End
            'Sohail (20 Feb 2012) -- End
            'Sohail (04 Mar 2011) -- End
            'Sohail (11 Sep 2010) -- End
            'Sohail (11 Sep 2010) -- End
            'Sohail (18 Aug 2010) -- End
            'Sohail (16 Aug 2010) -- End
            'Sohail (07 Aug 2010) -- End

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@periodid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID.ToString)
            'objDataOperation.AddParameter("@strEmpList", SqlDbType.Text, eZeeDataType.NAME_SIZE, strEmployeeList.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPeriodStartDate)
            'Sohail (04 Mar 2011) -- Start
            If mdtProcessdate < mdtPeriodEndDate Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProcessdate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPeriodEndDate)
            End If
            'Sohail (04 Mar 2011) -- End
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            objDataOperation.AddParameter("@tna_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTnA_StartDate)
            objDataOperation.AddParameter("@tna_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTnA_Processdate)
            'Sohail (07 Jan 2014) -- End
            'Sohail (11 Sep 2010) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearUnkid)
            'Sohail (21 Aug 2015) -- End
            'Sohail (11 Sep 2010) -- End
            dsCalc = objDataOperation.ExecQuery(strB.ToString, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            strB.Length = 0
            strB.Append("SELECT  employeeunkid " & _
                              ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                        "INTO    #TableEmp " & _
                        "FROM    hremployee_master " & _
                        "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
                        " " & _
                        "SELECT  tnalogin_summary.employeeunkid  " & _
                              ", tnalogin_summary.shiftunkid " & _
                              ", CONVERT(CHAR(8), tnalogin_summary.login_date, 112) AS login_date " & _
                              ", ISNULL(tnalogin_summary.day_type, 0.00) AS day_type " & _
                              ", tnalogin_summary.total_hrs " & _
                              ", ISNULL(tnalogin_summary.ispaidleave, 0) AS ispaidleave " & _
                              ", ISNULL(tnalogin_summary.isunpaidleave, 0) AS isunpaidleave " & _
                              ", tnalogin_summary.total_overtime AS ot1 " & _
                              ", tnalogin_summary.ot2 " & _
                              ", tnalogin_summary.ot3 " & _
                              ", tnalogin_summary.ot4 " & _
                              ", tnalogin_summary.total_short_hrs " & _
                              ", tnalogin_summary.total_nighthrs " & _
                              ", ISNULL(tnalogin_summary.isweekend, 0) AS isweekend " & _
                              ", ISNULL(tnalogin_summary.isholiday, 0) AS isholiday " & _
                              ", ISNULL(tnalogin_summary.isdayoff, 0) AS isdayoff " & _
                              ", ISNULL(tnalogin_summary.dayid, -1) AS dayid " & _
                              ", tnalogin_summary.lvdayfraction " & _
                              ", tnalogin_summary.ot1_penalty " & _
                              ", tnalogin_summary.ot2_penalty " & _
                              ", tnalogin_summary.ot3_penalty " & _
                              ", tnalogin_summary.ot4_penalty " & _
                              ", tnalogin_summary.elrgoing_grace " & _
                              ", tnalogin_summary.ltcoming " & _
                              ", ISNULL(tnalogin_summary.ltcoming_grace, 0) AS ltcoming_grace " & _
                              ", ISNULL(tnalogin_summary.leavetypeunkid, 0) AS leavetypeunkid " & _
                        "FROM    #TableEmp AS A " & _
                                "LEFT JOIN tnalogin_summary ON A.employeeunkid = tnalogin_summary.employeeunkid " & _
                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " _
                        )
            'Sohail (10 Sep 2021) - [ltcoming_grace]
            'Hemant (16 Apr 2019) -- [elrgoing_grace,ltcoming]
            'Sohail (24 Aug 2017) - [leavetypeunkid]

            strB.Append(" DROP TABLE #TableEmp ")

            Dim dsAllTnA As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (18 Jul 2017) -- End

            'Sohail (05 Dec 2019) -- Start
            'NMB UAT Enhancement # TC010 : System should be able to link OT module with payroll and compute special overtime (Holidays and weekend) and normal overtime respectively.
            strB.Length = 0
            strB.Append("SELECT  employeeunkid " & _
                              ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                        "INTO    #TableEmp " & _
                        "FROM    hremployee_master " & _
                        "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
                        " " & _
                        "SELECT  tnaot_requisition_tran.employeeunkid  " & _
                              ", tnaot_requisition_tran.shiftunkid " & _
                              ", CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) AS login_date " & _
                              ", tnaot_requisition_tran.actualot_hours AS total_hrs " & _
                              ", tnaot_requisition_tran.actualnight_hrs AS total_nighthrs " & _
                              ", tnaot_requisition_tran.isweekend AS isweekend " & _
                              ", tnaot_requisition_tran.isholiday AS isholiday " & _
                              ", tnaot_requisition_tran.isdayoff AS isdayoff " & _
                              ", tnaot_requisition_tran.dayid AS dayid " & _
                        "FROM    #TableEmp AS A " & _
                                "LEFT JOIN tnaot_requisition_tran ON A.employeeunkid = tnaot_requisition_tran.employeeunkid " & _
                                "LEFT JOIN tnaotrequisition_process_tran ON tnaotrequisition_process_tran.otrequisitiontranunkid = tnaot_requisition_tran.otrequisitiontranunkid " & _
                        "WHERE   tnaot_requisition_tran.isvoid = 0 " & _
                                "AND tnaot_requisition_tran.statusunkid = 1 " & _
                                "AND tnaotrequisition_process_tran.isvoid = 0 " & _
                                "AND tnaotrequisition_process_tran.periodunkid = @periodunkid " & _
                                "AND tnaotrequisition_process_tran.isposted = 1 " & _
                                "AND tnaotrequisition_process_tran.isprocessed = 0 " _
                        )
            'Sohail (13 Feb 2020) -- Start
            'NMB Enhancement # : Now processed only those OT which are posted in given date range on OT post to payroll screen.
            'Added : LEFT JOIN tnaotrequisition_process_tran ON tnaotrequisition_process_tran.otrequisitiontranunkid = tnaot_requisition_tran.otrequisitiontranunkid
            '       AND tnaotrequisition_process_tran.isvoid = 0
            '       AND tnaotrequisition_process_tran.periodunkid = @periodunkid
            '       AND tnaotrequisition_process_tran.isposted = 1
            '       AND tnaotrequisition_process_tran.isprocessed = 0
            'Removed from WHERE clause : AND tnaot_requisition_tran.requestdate BETWEEN @tna_startdate AND @tna_enddate
            'Sohail (13 Feb 2020) -- End


            strB.Append(" DROP TABLE #TableEmp ")

            'Sohail (13 Feb 2020) -- Start
            'NMB Enhancement # : Now processed only those OT which are posted in given date range on OT post to payroll screen.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayperiodunkid)
            'Sohail (13 Feb 2020) -- End

            Dim dsAllOTReq As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (05 Dec 2019) -- End

            'Sohail (29 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            strB.Length = 0
            strB.Append("CREATE TABLE #TableEmp ( EmpID INT, EmpName NVARCHAR(MAX) ) " & _
                    "INSERT  INTO #TableEmp " & _
                            "( EmpID " & _
                            ", EmpName " & _
                            ") " & _
                            "SELECT  employeeunkid " & _
                                  ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                            "FROM    hremployee_master " & _
                            "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
                    "SELECT  TableEmp.EmpID " & _
                          ", TableEmp.EmpName " & _
                          ", TableEmp.shiftunkid " & _
                          ", ISNULL(TableTnA.TotHoursWorked, 0) AS TotHoursWorked " & _
                          ", ISNULL(TableTnA.TotalOvertime, 0) AS TotalOvertime " & _
                          ", ISNULL(TableTnA.TotalOvertime2, 0) AS TotalOvertime2 " & _
                          ", ISNULL(TableTnA.TotalOvertime3, 0) AS TotalOvertime3 " & _
                          ", ISNULL(TableTnA.TotalOvertime4, 0) AS TotalOvertime4 " & _
                          ", ISNULL(TableTnA.TotShortHours, 0) AS TotShortHours " & _
                          ", ISNULL(TableWeekdays.WeekdaysWorkedHours, 0) AS WeekdaysWorkedHours " & _
                          ", ISNULL(TableWeekdays.WeekdaysOvertimeHours, 0) AS WeekdaysOvertimeHours " & _
                          ", ISNULL(TableWeekdays.WeekdaysOvertimeHours2, 0) AS WeekdaysOvertimeHours2 " & _
                          ", ISNULL(TableWeekdays.WeekdaysOvertimeHours3, 0) AS WeekdaysOvertimeHours3 " & _
                          ", ISNULL(TableWeekdays.WeekdaysOvertimeHours4, 0) AS WeekdaysOvertimeHours4 " & _
                          ", ISNULL(TableWeekdays.WeekdaysShortHours, 0) AS WeekdaysShortHours " & _
                          ", ISNULL(TableWeekdays.WeekdaysNightHours, 0) AS WeekdaysNightHours " & _
                          ", ISNULL(TableWeekend.WeekendWorkedHours, 0) AS WeekendWorkedHours " & _
                          ", ISNULL(TableWeekend.WeekendOvertimeHours, 0) AS WeekendOvertimeHours " & _
                          ", ISNULL(TableWeekend.WeekendOvertimeHours2, 0) AS WeekendOvertimeHours2 " & _
                          ", ISNULL(TableWeekend.WeekendOvertimeHours3, 0) AS WeekendOvertimeHours3 " & _
                          ", ISNULL(TableWeekend.WeekendOvertimeHours4, 0) AS WeekendOvertimeHours4 " & _
                          ", ISNULL(TableWeekend.WeekendShortHours, 0) AS WeekendShortHours " & _
                          ", ISNULL(TableWeekend.WeekendNightHours, 0) AS WeekendNightHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayWorkedHours, 0) AS PublicHolidayWorkedHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours, 0) AS PublicHolidayOvertimeHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours2, 0) AS PublicHolidayOvertimeHours2 " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours3, 0) AS PublicHolidayOvertimeHours3 " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayOvertimeHours4, 0) AS PublicHolidayOvertimeHours4 " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayShortHours, 0) AS PublicHolidayShortHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayNightHours, 0) AS PublicHolidayNightHours " & _
                          ", ISNULL(TablePublicHoliday.PublicHolidayDays, 0) AS PublicHolidayDays " & _
                    "FROM    ( SELECT  DISTINCT " & _
                                        "tnalogin_summary.employeeunkid AS EmpID " & _
                                      ", EmpName " & _
                                      ", tnalogin_summary.shiftunkid " & _
                              "FROM      #TableEmp AS A " & _
                                        "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                        "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                              "WHERE     login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                            ") AS TableEmp " & _
                            "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
                                              ", SUM(total_hrs) AS TotHoursWorked " & _
                                              ", SUM(total_overtime) AS TotalOvertime " & _
                                              ", ISNULL(SUM(ot2), 0) AS TotalOvertime2 " & _
                                              ", ISNULL(SUM(ot3), 0) AS TotalOvertime3 " & _
                                              ", ISNULL(SUM(ot4), 0) AS TotalOvertime4 " & _
                                              ", SUM(total_short_hrs) AS TotShortHours " & _
                                              ", tnalogin_summary.shiftunkid " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                                "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                        "GROUP BY tnalogin_summary.employeeunkid, tnalogin_summary.shiftunkid " & _
                                      ") AS TableTnA ON TableEmp.EmpId = TableTnA.EmpID AND TableEmp.shiftunkid = TableTnA.shiftunkid " & _
                            "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
                                              ", SUM(total_hrs) AS WeekdaysWorkedHours " & _
                                              ", SUM(total_overtime) AS WeekdaysOvertimeHours " & _
                                              ", ISNULL(SUM(ot2), 0) AS WeekdaysOvertimeHours2 " & _
                                              ", ISNULL(SUM(ot3), 0) AS WeekdaysOvertimeHours3 " & _
                                              ", ISNULL(SUM(ot4), 0) AS WeekdaysOvertimeHours4 " & _
                                              ", SUM(total_short_hrs) AS WeekdaysShortHours " & _
                                              ", SUM(total_nighthrs) AS WeekdaysNightHours " & _
                                              ", tnalogin_summary.shiftunkid " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                                "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                                "AND ISNULL(isweekend, 0) = 0 " & _
                                                "AND ISNULL(isdayoff, 0) = 0 " & _
                                        "GROUP BY tnalogin_summary.employeeunkid, tnalogin_summary.shiftunkid " & _
                                      ") AS TableWeekdays ON TableEmp.EmpId = TableWeekdays.EmpID AND TableEmp.shiftunkid = TableWeekdays.shiftunkid " & _
                            "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
                                              ", SUM(total_hrs) AS WeekendWorkedHours " & _
                                              ", SUM(total_overtime) AS WeekendOvertimeHours " & _
                                              ", ISNULL(SUM(ot2), 0) AS WeekendOvertimeHours2 " & _
                                              ", ISNULL(SUM(ot3), 0) AS WeekendOvertimeHours3 " & _
                                              ", ISNULL(SUM(ot4), 0) AS WeekendOvertimeHours4 " & _
                                              ", SUM(total_short_hrs) AS WeekendShortHours " & _
                                              ", SUM(total_nighthrs) AS WeekendNightHours " & _
                                              ", tnalogin_summary.shiftunkid " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                                "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                                "AND ISNULL(isweekend, 0) = 1 " & _
                                        "GROUP BY tnalogin_summary.employeeunkid, tnalogin_summary.shiftunkid " & _
                                      ") AS TableWeekend ON TableEmp.EmpId = TableWeekend.EmpID AND TableEmp.shiftunkid = TableWeekend.shiftunkid " & _
                            "LEFT JOIN ( SELECT  tnalogin_summary.employeeunkid AS EmpID " & _
                                              ", SUM(total_hrs) AS PublicHolidayWorkedHours " & _
                                              ", SUM(total_overtime) AS PublicHolidayOvertimeHours " & _
                                              ", ISNULL(SUM(ot2), 0) AS PublicHolidayOvertimeHours2 " & _
                                              ", ISNULL(SUM(ot3), 0) AS PublicHolidayOvertimeHours3 " & _
                                              ", ISNULL(SUM(ot4), 0) AS PublicHolidayOvertimeHours4 " & _
                                              ", SUM(total_short_hrs) AS PublicHolidayShortHours " & _
                                              ", SUM(total_nighthrs) AS PublicHolidayNightHours " & _
                                              ", COUNT(tnalogin_summary.employeeunkid) AS PublicHolidayDays " & _
                                              ", tnalogin_summary.shiftunkid " & _
                                        "FROM    #TableEmp AS A " & _
                                                "LEFT JOIN tnalogin_summary ON A.EmpId = tnalogin_summary.employeeunkid " & _
                                                "/*LEFT JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                        "WHERE   login_date BETWEEN @tna_startdate AND @tna_enddate " & _
                                                "AND ISNULL(isholiday, 0) = 1 " & _
                                        "GROUP BY tnalogin_summary.employeeunkid, tnalogin_summary.shiftunkid " & _
                                      ") AS TablePublicHoliday ON TableEmp.EmpId = TablePublicHoliday.EmpID AND TableEmp.shiftunkid = TablePublicHoliday.shiftunkid " _
                                      )

            strB.Append(" DROP TABLE #TableEmp ")

            Dim dsShiftWiseData As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (29 Oct 2013) -- End

            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            strB.Length = 0
            strB.Append("CREATE TABLE #TableEmp ( EmpID INT, EmpName NVARCHAR(MAX) ) " & _
                    "INSERT  INTO #TableEmp " & _
                            "( EmpID " & _
                            ", EmpName " & _
                            ") " & _
                            "SELECT  employeeunkid " & _
                                  ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                            "FROM    hremployee_master " & _
                                "WHERE   employeeunkid IN ( " & strEmployeeList & " ) ")

            strB.Append("CREATE TABLE #tblformula " & _
                        "( " & _
                          "tranheadunkid INT  " & _
                        ", formula NVARCHAR(MAX) " & _
                        ", formulaid NVARCHAR(MAX) " & _
                        ") " & _
                    "INSERT  INTO #tblformula " & _
                            "( tranheadunkid  " & _
                            ", formula " & _
                            ", formulaid " & _
                            ") " & _
                    "SELECT  tranheadunkid " & _
                          ", formula " & _
                          ", formulaid " & _
                    "FROM    ( SELECT    tranheadunkid " & _
                                      ", formula " & _
                                      ", formulaid " & _
                                      ", DENSE_RANK() OVER ( PARTITION  BY prtranhead_formula_slab_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                              "FROM      prtranhead_formula_slab_tran " & _
                                        "JOIN cfcommon_period_tran ON prtranhead_formula_slab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND CONVERT(CHAR(8), end_date, 112) <= @end_date " & _
                            ") AS b " & _
                    "WHERE   ROWNO = 1 " _
                    )

            'Sohail (02 Jan 2017) -- Start
            'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
            'strB.Append("SELECT  a.edunkid " & _
            '              ", a.employeeunkid " & _
            '              ", a.tranheadunkid " & _
            '              ", a.trnheadtype_id " & _
            '              ", a.typeof_id " & _
            '              ", a.calctype_id " & _
            '              ", a.computeon_id " & _
            '              ", a.formulaid " & _
            '              ", a.amount " & _
            '              ", a.currencyunkid " & _
            '              ", a.vendorunkid " & _
            '              ", a.periodunkid " & _
            '              ", a.costcenterunkid " & _
            '              ", a.percentage " & _
            '              ", a.membershiptranunkid " & _
            '              ", a.cumulative_startdate " & _
            '              ", a.stop_date " & _
            '        "FROM    ( SELECT    edunkid " & _
            '                          ", prearningdeduction_master.employeeunkid " & _
            '                          ", prearningdeduction_master.tranheadunkid " & _
            '                          ", prtranhead_master.trnheadtype_id " & _
            '                          ", prtranhead_master.typeof_id " & _
            '                          ", prtranhead_master.calctype_id " & _
            '                          ", prtranhead_master.computeon_id " & _
            '                          ", ISNULL(#tblformula.formulaid, '') AS formulaid " & _
            '                          ", ISNULL(prearningdeduction_master.amount, 0) AS amount " & _
            '                          ", prearningdeduction_master.currencyunkid " & _
            '                          ", prearningdeduction_master.vendorunkid " & _
            '                          ", prearningdeduction_master.periodunkid " & _
            '                          ", ISNULL(premployee_costcenter_tran.costcenterunkid, 0) AS costcenterunkid " & _
            '                          ", ISNULL(premployee_costcenter_tran.percentage, 0)  AS percentage " & _
            '                          ", ISNULL(prearningdeduction_master.membershiptranunkid, 0)  AS membershiptranunkid " & _
            '                          ", prearningdeduction_master.cumulative_startdate " & _
            '                          ", prearningdeduction_master.stop_date " & _
            '                          ", DENSE_RANK() OVER ( PARTITION  BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
            '                    "FROM    prearningdeduction_master " & _
            '                            "JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                            "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
            '                            "LEFT JOIN premployee_costcenter_tran ON premployee_costcenter_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
            '                                      "AND premployee_costcenter_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
            '                                      "AND ISNULL(premployee_costcenter_tran.isvoid, 0) = 0 " & _
            '                            "LEFT JOIN premployee_exemption_tran ON prearningdeduction_master.employeeunkid = premployee_exemption_tran.employeeunkid " & _
            '                                    "AND prearningdeduction_master.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
            '                                    "AND premployee_exemption_tran.periodunkid = @periodunkid " & _
            '                                    "AND premployee_exemption_tran.isapproved = 1 " & _
            '                                    "AND ISNULL(premployee_exemption_tran.isvoid, 0) = 0 " & _
            '                           "LEFT JOIN  #tblformula ON #tblformula.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
            '                           "RIGHT JOIN #TableEmp ON #TableEmp.EmpId = prearningdeduction_master.employeeunkid " & _
            '                           "WHERE    prearningdeduction_master.isvoid = 0 " & _
            '                                "AND CONVERT(CHAR(8), end_date, 112) <= @end_date " & _
            '                                "AND premployee_exemption_tran.tranheadunkid IS NULL " & _
            '                                "AND prearningdeduction_master.isApproved = 1 " & _
            '                ") AS a " & _
            '        "WHERE   ROWNO = 1 " & _
            '        "ORDER BY amount DESC " & _
            '              ", trnheadtype_id " & _
            '              ", computeon_id " & _
            '              ", calctype_id DESC " & _
            '                  ", tranheadunkid " _
            '                  )
            strB.Append("SELECT  * " & _
                        "INTO    #tblED " & _
                    "FROM    ( SELECT    edunkid " & _
                                      ", prearningdeduction_master.employeeunkid " & _
                                      ", prearningdeduction_master.tranheadunkid " & _
                                      ", ISNULL(prearningdeduction_master.amount, 0) AS amount " & _
                                      ", prearningdeduction_master.currencyunkid " & _
                                      ", prearningdeduction_master.vendorunkid " & _
                                      ", prearningdeduction_master.periodunkid " & _
                                      ", ISNULL(prearningdeduction_master.membershiptranunkid, 0)  AS membershiptranunkid " & _
                                      ", prearningdeduction_master.cumulative_startdate " & _
                                      ", prearningdeduction_master.stop_date " & _
                                      ", prearningdeduction_master.edstart_date " & _
                                      ", DENSE_RANK() OVER ( PARTITION  BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                  "FROM      #TableEmp " & _
                                            "LEFT JOIN prearningdeduction_master ON #TableEmp.EmpId = prearningdeduction_master.employeeunkid " & _
                                            "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                       "WHERE    prearningdeduction_master.isvoid = 0 " & _
                                            "AND cfcommon_period_tran.isactive =  1 " & _
                                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                            "AND CONVERT(CHAR(8), end_date, 112) <= @end_date " & _
                                            "AND prearningdeduction_master.isApproved = 1 " & _
                                ") AS A " & _
                        "WHERE   A.ROWNO = 1 " _
                        )
            'Sohail (24 Aug 2019) - [edstart_date]

            'Sohail (03 Apr 2019) -- Start
            'NMB Issue - 76.1 - Distribute net pay head to cost center after net pay rounding.
            strB.Append("INSERT INTO #tblED " & _
                        "SELECT 0 AS edunkid " & _
                             ", #TableEmp.EmpId " & _
                             ", prtranhead_master.tranheadunkid " & _
                             ", 0 AS amount " & _
                             ", 1 AS currencyunkid " & _
                             ", 1 AS vendorunkid " & _
                             ", 54 AS periodunkid " & _
                             ", 0 AS membershiptranunkid " & _
                             ", NULL AS cumulative_startdate " & _
                             ", NULL AS stop_date " & _
                             ", NULL AS edstart_date " & _
                             ", 1 AS ROWNO " & _
                        "FROM #TableEmp " & _
                            "LEFT JOIN prtranhead_master ON 1 = 1 " & _
                            "LEFT JOIN #tblED ON #tblED.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                   "AND #TableEmp.EmpId = #tblED.employeeunkid " & _
                                   "/*AND #tblED.periodunkid = @periodunkid*/ " & _
                        "WHERE prtranhead_master.isvoid = 0 " & _
                              "AND prtranhead_master.calctype_id = " & enCalcType.NET_PAY & " " & _
                              "AND #tblED.edunkid IS NULL " _
                              )
            'Sohail (19 Nov 2019) - KVTC Issue : [/*AND #tblED.periodunkid = @periodunkid*/] Commented due to taking 2 Net Pay head in process payroll if not assigned on selected period on ED and causing unbalance in Combined JV
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (03 Apr 2019) -- End

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            strB.Append("SELECT  * INTO  #tblECC " & _
                         "FROM ( " & _
                                "SELECT  premployee_costcenter_tran.costcentertranunkid  " & _
                                      ", premployee_costcenter_tran.costcenterunkid " & _
                                      ", premployee_costcenter_tran.employeeunkid " & _
                                      ", premployee_costcenter_tran.tranheadunkid " & _
                                      ", cfcommon_period_tran.periodunkid " & _
                                      ", premployee_costcenter_tran.percentage " & _
                                      ", ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") AS allocationbyid " & _
                                      ", ISNULL(premployee_costcenter_tran.isinactive, 0) AS isinactive " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY premployee_costcenter_tran.employeeunkid, premployee_costcenter_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                "FROM    premployee_costcenter_tran " & _
                                        "JOIN #TableEmp ON #TableEmp.EmpId = premployee_costcenter_tran.employeeunkid " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = premployee_costcenter_tran.periodunkid " & _
                                "WHERE   premployee_costcenter_tran.isvoid = 0 " & _
                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.modulerefid = 1 " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                                ") AS A " & _
                         "WHERE   A.ROWNO = 1 " & _
                         "AND A.isinactive = 0 " _
                         )
            'Sohail (25 Jul 2020) = [isinactive]
            'Sohail (07 Feb 2019) - [allocationbyid, JOIN #TableEmp]
            'Sohail (29 Mar 2017) -- End

            strB.Append("SELECT  edunkid " & _
                              ", #tblED.employeeunkid " & _
                              ", #tblED.tranheadunkid " & _
                              ", prtranhead_master.trnheadtype_id " & _
                              ", prtranhead_master.typeof_id " & _
                              ", prtranhead_master.calctype_id " & _
                              ", prtranhead_master.computeon_id " & _
                              ", ISNULL(prtranhead_master.isproratedhead, 0) AS isproratedhead " & _
                              ", ISNULL(#tblformula.formulaid, '') AS formulaid " & _
                              ", ISNULL(#tblED.amount, 0) AS amount " & _
                              ", #tblED.currencyunkid " & _
                              ", #tblED.vendorunkid " & _
                              ", #tblED.periodunkid " & _
                              ", ISNULL(#tblECC.allocationbyid, " & enAllocation.COST_CENTER & ") AS allocationbyid " & _
                              ", CASE WHEN ISNULL(#tblECC.costcenterunkid, 0) = 0 THEN ISNULL(prtranhead_master.default_costcenterunkid, 0) " & _
                              "       ELSE ISNULL(#tblECC.costcenterunkid, 0) END AS costcenterunkid " & _
                              ", ISNULL(#tblECC.percentage, 0) AS percentage " & _
                              ", ISNULL(#tblED.membershiptranunkid, 0)  AS membershiptranunkid " & _
                              ", #tblED.cumulative_startdate " & _
                              ", #tblED.stop_date " & _
                              ", #tblED.edstart_date " & _
                              ", ISNULL(prtranhead_master.roundofftypeid, 200) AS roundofftypeid " & _
                              ", ISNULL(prtranhead_master.roundoffmodeid, 1) AS roundoffmodeid " & _
                              ", ISNULL(prtranhead_master.compute_priority, 0) AS compute_priority " & _
                        "FROM    #tblED " & _
                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = #tblED.tranheadunkid " & _
                                "LEFT JOIN #tblformula ON #tblformula.tranheadunkid = #tblED.tranheadunkid " & _
                                "LEFT JOIN #tblECC ON #tblECC.employeeunkid = #tblED.employeeunkid " & _
                                                                            "AND #tblECC.tranheadunkid = #tblED.tranheadunkid " & _
                                "LEFT JOIN premployee_exemption_tran ON #tblED.employeeunkid = premployee_exemption_tran.employeeunkid " & _
                                        "AND #tblED.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
                                        "AND premployee_exemption_tran.periodunkid = @periodunkid " & _
                                        "AND premployee_exemption_tran.isapproved = 1 " & _
                                        "AND premployee_exemption_tran.isvoid = 0 " & _
                               "WHERE    premployee_exemption_tran.tranheadunkid IS NULL " & _
                                        "AND prtranhead_master.isvoid = 0 " & _
                    "ORDER BY amount DESC " & _
                          ", compute_priority DESC, trnheadtype_id " & _
                          ", computeon_id " & _
                          ", calctype_id DESC " & _
                              ", #tblED.tranheadunkid " _
                              )
            'Sohail (08 May 2021) - [compute_priority]
            'Sohail (11 Dec 2020) - [roundoffmodeid]
            'Sohail (03 Dec 2020) - [roundofftypeid]
            'Hemant (06 Jul 2020) -- [ISNULL(#tblECC.costcenterunkid, 0) AS costcenterunkid  --> case when ISNULL(#tblECC.costcenterunkid, 0) = 0 then prtranhead_master.default_costcenterunkid  else ISNULL(#tblECC.costcenterunkid, 0) end AS costcenterunkid]
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (07 Feb 2019) - [allocationbyid]
            'Sohail (28 Jan 2019) - [isproratedhead]
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            '"LEFT JOIN premployee_costcenter_tran ON premployee_costcenter_tran.employeeunkid = #tblED.employeeunkid " & _
            '                                                                "AND premployee_costcenter_tran.tranheadunkid = #tblED.tranheadunkid " & _
            '                                                                "AND premployee_costcenter_tran.isvoid = 0 " & _
            'Sohail (29 Mar 2017) -- End

            'Sohail (02 Jan 2017) -- End

            strB.Append(" DROP TABLE #TableEmp " & _
                        " DROP TABLE #tblformula " & _
                        " DROP TABLE #tblED " & _
                        " DROP TABLE #tblECC " _
                        )
            'Sohail (29 Mar 2017) - [#tblECC]
            'Sohail (02 Jan 2017) - [DROP TABLE #tblED]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayperiodunkid)
            'objDataOperation.AddParameter("@defaultcostcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDefaultCostCenterUnkId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

            Dim dsEDHeads As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (14 Mar 2019) -- Start
            'NMB Issue - 76.1 - JV is picking employee cost center, not transaction cost center for net pay head mapped on employee cost center screen.
            strB.Length = 0
            strB.Append("SELECT  employeeunkid " & _
                                  ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                                  "INTO #TableEmp " & _
                            "FROM    hremployee_master " & _
                                "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
                        "SELECT * " & _
                        "FROM " & _
                        "( " & _
                            "SELECT premployee_costcenter_tran.costcentertranunkid " & _
                                 ", ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") AS allocationbyid " & _
                                 ", premployee_costcenter_tran.costcenterunkid " & _
                                 ", premployee_costcenter_tran.employeeunkid " & _
                                 ", premployee_costcenter_tran.tranheadunkid " & _
                                 ", cfcommon_period_tran.periodunkid " & _
                                 ", premployee_costcenter_tran.percentage " & _
                                 ", ISNULL(premployee_costcenter_tran.isinactive, 0) AS isinactive " & _
                                 ", DENSE_RANK() OVER (PARTITION BY premployee_costcenter_tran.employeeunkid, premployee_costcenter_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                            "FROM premployee_costcenter_tran " & _
                                "JOIN #TableEmp ON #TableEmp.employeeunkid = premployee_costcenter_tran.employeeunkid " & _
                                "LEFT JOIN prtranhead_master ON premployee_costcenter_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = premployee_costcenter_tran.periodunkid " & _
                            "WHERE premployee_costcenter_tran.isvoid = 0 " & _
                                  "AND prtranhead_master.isvoid = 0 " & _
                                  "AND typeof_id = " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & " " & _
                                  "AND cfcommon_period_tran.isactive = 1 " & _
                                  "AND cfcommon_period_tran.modulerefid = 1 " & _
                                  "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                        ") AS A " & _
                        "WHERE A.ROWNO = 1 " & _
                        "AND A.isinactive = 0 " _
                        )
            'Sohail (25 Jul 2020) = [isinactive]

            'Sohail (12 Mar 2020) -- Start
            'NMB Enhancement # : Performance issue.
            strB.Append(" DROP TABLE #TableEmp " _
                        )
            'Sohail (12 Mar 2020) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

            Dim dsInfoCCenter As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (14 Mar 2019) -- End

            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            strB.Length = 0
            strB.Append("SELECT loanschemeunkid " & _
                             ", costcenterunkid " & _
                        "FROM lnloan_scheme_master " & _
                        "WHERE isactive = 1 ")

            Dim dsLoanCCenter As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strB.Length = 0
            strB.Append("SELECT savingschemeunkid " & _
                             ", costcenterunkid " & _
                        "FROM svsavingscheme_master " & _
                        "WHERE isvoid = 0 ")

            Dim dsSavingCCenter As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (14 Mar 2019) -- End

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
            Dim dsBenefitMaster As DataSet = Nothing
            Dim dsBenefitTran As DataSet = Nothing
            Dim dsEmpAllocation As DataSet = Nothing
            strB.Length = 0
            strB.Append("SELECT  DISTINCT benefitallocationunkid " & _
                             ", stationunkid " & _
                             ", deptgroupunkid " & _
                             ", departmentunkid " & _
                             ", sectiongroupunkid " & _
                             ", sectionunkid " & _
                             ", unitgroupunkid " & _
                             ", unitunkid " & _
                             ", classgroupunkid " & _
                             ", classunkid " & _
                             ", teamunkid " & _
                             ", jobgroupunkid " & _
                             ", jobunkid " & _
                             ", gradegroupunkid " & _
                             ", gradeunkid " & _
                             ", gradelevelunkid " & _
                             ", costcenterunkid " & _
                             ", '' AS FilterString " & _
                        "FROM ( " & _
                                "SELECT hrbenefit_allocation_master.benefitallocationunkid " & _
                                        ", stationunkid " & _
                                         ", deptgroupunkid " & _
                                         ", departmentunkid " & _
                                         ", sectiongroupunkid " & _
                                         ", sectionunkid " & _
                                         ", unitgroupunkid " & _
                                         ", unitunkid " & _
                                         ", classgroupunkid " & _
                                         ", classunkid " & _
                                         ", teamunkid " & _
                                         ", jobgroupunkid " & _
                                         ", jobunkid " & _
                                         ", gradegroupunkid " & _
                                         ", gradeunkid " & _
                                         ", gradelevelunkid " & _
                                         ", hrbenefit_allocation_master.costcenterunkid " & _
                                         ", hrbenefit_allocation_tran.tranheadunkid " & _
                                         ", prtranhead_master.calctype_id " & _
                                         ", hrbenefit_allocation_tran.amount " & _
                                         ", ISNULL(hrbenefit_allocation_master.periodunkid, 1) AS periodunkid " & _
                                         ", ISNULL(hrbenefit_allocation_master.isactive, 1) AS isactive " & _
                                         ", '' AS FilterString " & _
                                         ", DENSE_RANK() OVER (PARTITION BY hrbenefit_allocation_master.benefitgroupunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                "FROM hrbenefit_allocation_master " & _
                                    "LEFT JOIN cfcommon_period_tran ON hrbenefit_allocation_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                    "LEFT JOIN hrbenefit_allocation_tran ON hrbenefit_allocation_master.benefitallocationunkid = hrbenefit_allocation_tran.benefitallocationunkid " & _
                                    "LEFT JOIN prtranhead_master ON hrbenefit_allocation_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                "WHERE hrbenefit_allocation_master.isvoid = 0 " & _
                                      "AND hrbenefit_allocation_tran.isvoid = 0 " & _
                                      "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                            " ) AS A " & _
                            "WHERE   A.ROWNO = 1 " & _
                            "AND A.isactive = 1 " & _
                            "AND A.tranheadunkid > 0 " & _
                            "AND NOT (A.calctype_id = " & enCalcType.FlatRate_Others & " AND A.amount = 0) " _
                            )
            'Sohail (27 Sep 2019) - [calctype_id], [AND A.amount <> 0]=[AND NOT (A.calctype_id = " & enCalcType.FlatRate_Others & " AND A.amount = 0)]
            'For benefits mapped with computed formula transaction head, system does not automatically assign as per the allocation - benefit mapping done 

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

            dsBenefitMaster = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsBenefitMaster.Tables(0).Rows.Count > 0 Then
                Dim arrAlloc() As String = {"stationunkid", "departmentunkid", "sectionunkid", "unitunkid", "jobunkid", "gradeunkid", "classunkid" _
                                            , "costcenterunkid", "deptgroupunkid", "sectiongroupunkid", "unitgroupunkid", "teamunkid", "jobgroupunkid" _
                                            , "gradegroupunkid", "gradelevelunkid", "classgroupunkid"}
                For Each dRow As DataRow In dsBenefitMaster.Tables(0).Rows
                    Dim strFilter As String = ""

                    For Each col As DataColumn In dsBenefitMaster.Tables(0).Columns
                        If arrAlloc.Contains(col.ColumnName.ToLower) = False Then Continue For

                        If CInt(dRow.Item(col.ColumnName)) > 0 Then
                            strFilter &= " AND " & col.ColumnName & " = " & CInt(dRow.Item(col.ColumnName)) & " "
                        End If
                    Next
                    dRow.Item("FilterString") = strFilter
                Next
                dsBenefitMaster.Tables(0).AcceptChanges()

                strB.Length = 0
                strB.Append("SELECT  employeeunkid " & _
                                  ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                            "INTO #TableEmp " & _
                            "FROM    hremployee_master " & _
                                "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " _
                                )

                strB.Append("SELECT A.benefitallocationunkid " & _
                                 ", A.benefitallocationtranunkid " & _
                                 ", A.benefitplanunkid " & _
                                 ", A.tranheadunkid " & _
                                 ", A.trnheadtype_id " & _
                                 ", A.typeof_id " & _
                                 ", A.calctype_id " & _
                                 ", A.computeon_id " & _
                                 ", A.isproratedhead " & _
                                 ", A.amount " & _
                                 ", A.default_costcenterunkid " & _
                                 ", A.roundofftypeid " & _
                                 ", A.roundoffmodeid " & _
                            "INTO #BNFT " & _
                            "FROM ( " & _
                                    "SELECT hrbenefit_allocation_master.benefitallocationunkid " & _
                                         ", hrbenefit_allocation_tran.benefitallocationtranunkid " & _
                                         ", hrbenefit_allocation_tran.benefitplanunkid " & _
                                         ", hrbenefit_allocation_tran.tranheadunkid " & _
                                         ", prtranhead_master.trnheadtype_id " & _
                                         ", prtranhead_master.typeof_id " & _
                                         ", prtranhead_master.calctype_id " & _
                                         ", prtranhead_master.computeon_id " & _
                                         ", ISNULL(prtranhead_master.isproratedhead, 0) AS isproratedhead " & _
                                         ", hrbenefit_allocation_tran.amount " & _
                                         ", ISNULL(hrbenefit_allocation_master.periodunkid, 1) AS periodunkid " & _
                                         ", ISNULL(hrbenefit_allocation_master.isactive, 1) AS isactive " & _
                                         ", ISNULL(prtranhead_master.default_costcenterunkid, 0) AS default_costcenterunkid " & _
                                         ", ISNULL(prtranhead_master.roundofftypeid, 200) AS roundofftypeid " & _
                                         ", ISNULL(prtranhead_master.roundoffmodeid, 1) AS roundoffmodeid " & _
                                         ", DENSE_RANK() OVER (PARTITION BY hrbenefit_allocation_master.benefitgroupunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                    "FROM hrbenefit_allocation_master " & _
                                        "LEFT JOIN cfcommon_period_tran ON hrbenefit_allocation_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                        "LEFT JOIN hrbenefit_allocation_tran ON hrbenefit_allocation_master.benefitallocationunkid = hrbenefit_allocation_tran.benefitallocationunkid " & _
                                        "LEFT JOIN prtranhead_master ON hrbenefit_allocation_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                    "WHERE hrbenefit_allocation_master.isvoid = 0 " & _
                                          "AND hrbenefit_allocation_tran.isvoid = 0 " & _
                                          "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                                          "AND prtranhead_master.isvoid = 0 " & _
                                " ) AS A " & _
                                    "WHERE   A.ROWNO = 1 " & _
                                    "AND A.isactive = 1 " & _
                                    "AND A.tranheadunkid > 0 " & _
                                    "AND NOT (A.calctype_id = " & enCalcType.FlatRate_Others & " AND A.amount = 0) " _
                                    )
                'Sohail (11 Dec 2020) - [roundoffmodeid]
                'Sohail (03 Dec 2020) - [default_costcenterunkid, roundofftypeid]
                'Sohail (07 Feb 2020) - [benefitallocationtranunkid]
                'Sohail (27 Sep 2019) - [AND A.amount <> 0]=[AND NOT (A.calctype_id = " & enCalcType.FlatRate_Others & " AND A.amount = 0)]
				'For benefits mapped with computed formula transaction head, system does not automatically assign as per the allocation - benefit mapping done

                strB.Append("SELECT  DISTINCT tranheadunkid " & _
                            "INTO #HEAD " & _
                            "FROM #BNFT " _
                            )
                strB.Append("SELECT  tranheadunkid " & _
                                  ", formula " & _
                                  ", formulaid " & _
                            "INTO #tblformula " & _
                            "FROM    ( SELECT    prtranhead_formula_slab_tran.tranheadunkid " & _
                                              ", prtranhead_formula_slab_tran.formula " & _
                                              ", prtranhead_formula_slab_tran.formulaid " & _
                                              ", DENSE_RANK() OVER ( PARTITION  BY prtranhead_formula_slab_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                      "FROM      prtranhead_formula_slab_tran " & _
                                                "JOIN #HEAD ON #HEAD.tranheadunkid = prtranhead_formula_slab_tran.tranheadunkid " & _
                                                "JOIN cfcommon_period_tran ON prtranhead_formula_slab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                      "WHERE     prtranhead_formula_slab_tran.isvoid = 0 " & _
                                                "AND CONVERT(CHAR(8), end_date, 112) <= @end_date " & _
                                    ") AS b " & _
                            "WHERE   ROWNO = 1 " _
                            )

                strB.Append("SELECT  * INTO  #tblECC " & _
                             "FROM ( " & _
                                    "SELECT  premployee_costcenter_tran.costcentertranunkid  " & _
                                          ", premployee_costcenter_tran.costcenterunkid " & _
                                          ", premployee_costcenter_tran.employeeunkid " & _
                                          ", premployee_costcenter_tran.tranheadunkid " & _
                                          ", cfcommon_period_tran.periodunkid " & _
                                          ", premployee_costcenter_tran.percentage " & _
                                          ", ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") AS allocationbyid " & _
                                          ", ISNULL(premployee_costcenter_tran.isinactive, 0) AS isinactive " & _
                                          ", DENSE_RANK() OVER ( PARTITION BY premployee_costcenter_tran.employeeunkid, premployee_costcenter_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                    "FROM    premployee_costcenter_tran " & _
                                            "JOIN #TableEmp ON #TableEmp.employeeunkid = premployee_costcenter_tran.employeeunkid " & _
                                            "JOIN #HEAD ON #HEAD.tranheadunkid = premployee_costcenter_tran.tranheadunkid " & _
                                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = premployee_costcenter_tran.periodunkid " & _
                                    "WHERE   premployee_costcenter_tran.isvoid = 0 " & _
                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                            "AND cfcommon_period_tran.modulerefid = 1 " & _
                                            "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                                    ") AS A " & _
                             "WHERE   A.ROWNO = 1 " & _
                             "AND A.isinactive = 0 " _
                             )
                'Sohail (25 Jul 2020) = [isinactive]

                strB.Append("SELECT  #BNFT.benefitallocationunkid " & _
                                  ", #TableEmp.employeeunkid " & _
                                  ", #BNFT.benefitallocationtranunkid " & _
                                  ", #BNFT.tranheadunkid " & _
                                  ", #BNFT.trnheadtype_id " & _
                                  ", #BNFT.typeof_id " & _
                                  ", #BNFT.calctype_id " & _
                                  ", #BNFT.computeon_id " & _
                                  ", #BNFT.isproratedhead " & _
                                  ", ISNULL(#tblformula.formulaid, '') AS formulaid " & _
                                  ", ISNULL(#BNFT.amount, 0) AS amount " & _
                                  ", ISNULL(#tblECC.allocationbyid, " & enAllocation.COST_CENTER & ") AS allocationbyid " & _
                                  ", ISNULL(#tblECC.costcenterunkid, 0) AS costcenterunkid " & _
                                  ", ISNULL(#tblECC.percentage, 0) AS percentage " & _
                                  ", 0 AS membershiptranunkid " & _
                                  ", #BNFT.default_costcenterunkid " & _
                                  ", #BNFT.roundofftypeid " & _
                                  ", #BNFT.roundoffmodeid " & _
                            "FROM    #BNFT " & _
                                    "LEFT JOIN #TableEmp ON 1 = 1 " & _
                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = #BNFT.tranheadunkid " & _
                                    "LEFT JOIN #tblformula ON #tblformula.tranheadunkid = #BNFT.tranheadunkid " & _
                                    "LEFT JOIN #tblECC ON #tblECC.employeeunkid = #TableEmp.employeeunkid " & _
                                                                                "AND #tblECC.tranheadunkid = #BNFT.tranheadunkid " & _
                                    "LEFT JOIN premployee_exemption_tran ON #TableEmp.employeeunkid = premployee_exemption_tran.employeeunkid " & _
                                            "AND #BNFT.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
                                            "AND premployee_exemption_tran.periodunkid = @periodunkid " & _
                                            "AND premployee_exemption_tran.isapproved = 1 " & _
                                            "AND premployee_exemption_tran.isvoid = 0 " & _
                                   "WHERE    premployee_exemption_tran.tranheadunkid IS NULL " & _
                                            "AND prtranhead_master.isvoid = 0 " & _
                        "ORDER BY amount DESC " & _
                              ", trnheadtype_id " & _
                              ", computeon_id " & _
                              ", calctype_id DESC " & _
                                  ", #BNFT.tranheadunkid " _
                                  )
                'Sohail (11 Dec 2020) - [roundoffmodeid]
                'Sohail (03 Dec 2020) - [default_costcenterunkid, roundofftypeid]
                'Sohail (07 Feb 2020) - [benefitallocationtranunkid]

                strB.Append(" DROP TABLE #TableEmp " & _
                            " DROP TABLE #tblformula " & _
                            " DROP TABLE #BNFT " & _
                            " DROP TABLE #HEAD " & _
                            " DROP TABLE #tblECC " _
                            )

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayperiodunkid)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

                dsBenefitTran = objDataOperation.ExecQuery(strB.ToString, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strB.Length = 0
                strB.Append("SELECT employeeunkid " & _
                             ", effectivedate " & _
                             ", stationunkid " & _
                             ", deptgroupunkid " & _
                             ", departmentunkid " & _
                             ", sectiongroupunkid " & _
                             ", sectionunkid " & _
                             ", unitgroupunkid " & _
                             ", unitunkid " & _
                             ", teamunkid " & _
                             ", classgroupunkid " & _
                             ", classunkid " & _
                             ", 0 AS jobgroupunkid " & _
                             ", 0 AS jobunkid " & _
                             ", 0 AS costcenterunkid " & _
                             ", 0 AS gradegroupunkid " & _
                             ", 0 AS gradeunkid " & _
                             ", 0 AS gradelevelunkid " & _
                        "FROM " & _
                        "( " & _
                            "SELECT ETT.employeeunkid " & _
                                 ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                                 ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                                 ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                                 ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                                 ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                                 ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                                 ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                                 ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                                 ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                                 ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
                                 ", CASE " & _
                                       "WHEN CONVERT(CHAR(8), ETT.effectivedate, 112) < @start_date THEN @start_date " & _
                                       "ELSE CONVERT(CHAR(8), ETT.effectivedate, 112) END AS effectivedate " & _
                                 ", ROW_NUMBER() OVER (PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC, ETT.transferunkid DESC) AS Rno " & _
                            "FROM hremployee_transfer_tran AS ETT " & _
                            "WHERE isvoid = 0 " & _
                                  "AND ETT.employeeunkid IN (" & strEmployeeList & ") " & _
                                  "AND CONVERT(CHAR(8), effectivedate, 112) <= @start_date " & _
                        ") AS TRF " & _
                        "WHERE TRF.Rno = 1 " & _
                        "UNION ALL " & _
                        "SELECT ETT.employeeunkid " & _
                             ", CONVERT(CHAR(8), ETT.effectivedate, 112) AS effectivedate " & _
                             ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                             ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                             ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                             ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                             ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                             ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                             ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                             ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                             ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                             ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
                             ", 0 AS jobgroupunkid " & _
                             ", 0 AS jobunkid " & _
                             ", 0 AS costcenterunkid " & _
                             ", 0 AS gradegroupunkid " & _
                             ", 0 AS gradeunkid " & _
                             ", 0 AS gradelevelunkid " & _
                        "FROM hremployee_transfer_tran AS ETT " & _
                        "WHERE isvoid = 0 " & _
                              "AND ETT.employeeunkid IN (" & strEmployeeList & ") " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) BETWEEN @start_date + 1 AND @end_date " _
                              )

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

                dsEmpAllocation = objDataOperation.ExecQuery(strB.ToString, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsBenefitMaster.Tables(0).Select("jobgroupunkid > 0 OR jobunkid > 0 ").Length > 0 Then

                    strB.Length = 0
                    strB.Append("SELECT employeeunkid " & _
                                     ", effectivedate " & _
                                     ", jobgroupunkid " & _
                                     ", jobunkid " & _
                                "FROM " & _
                                "( " & _
                                    "SELECT ECT.employeeunkid " & _
                                         ", ECT.jobgroupunkid " & _
                                         ", ECT.jobunkid " & _
                                         ", CASE WHEN CONVERT(CHAR(8), ECT.effectivedate, 112) < @start_date THEN @start_date " & _
                                               "ELSE CONVERT(CHAR(8), ECT.effectivedate, 112) " & _
                                           "END AS effectivedate " & _
                                         ", ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC, ECT.categorizationtranunkid DESC) AS Rno " & _
                                         ", ECT.categorizationtranunkid " & _
                                    "FROM hremployee_categorization_tran AS ECT " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND ECT.employeeunkid IN (" & strEmployeeList & ") " & _
                                          "AND CONVERT(CHAR(8), effectivedate, 112) <= @start_date " & _
                                ") AS Cat " & _
                                "WHERE Cat.Rno = 1 " & _
                                "UNION ALL " & _
                                "SELECT ECT.employeeunkid " & _
                                     ", CONVERT(CHAR(8), ECT.effectivedate, 112) AS effectivedate " & _
                                     ", ECT.jobgroupunkid " & _
                                     ", ECT.jobunkid " & _
                                "FROM hremployee_categorization_tran AS ECT " & _
                                "WHERE isvoid = 0 " & _
                                      "AND ECT.employeeunkid IN (" & strEmployeeList & ") " & _
                                      "AND CONVERT(CHAR(8), effectivedate, 112) BETWEEN @start_date + 1 AND @end_date " _
                                      )

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

                    Dim ds As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    For Each drJob As DataRow In ds.Tables(0).Select("1 = 1", "employeeunkid, effectivedate")

                        Dim drFirst() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drJob.Item("employeeunkid")) & " AND effectivedate = '" & drJob.Item("effectivedate").ToString & "' ")

                        If drFirst.Length <= 0 Then
                            Dim drLast() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drJob.Item("employeeunkid")) & " AND effectivedate <= '" & drJob.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate DESC")
                            If drLast.Length > 0 Then
                                Dim d As DataRow = dsEmpAllocation.Tables(0).NewRow
                                d.ItemArray = drLast(0).ItemArray

                                d.Item("effectivedate") = drJob.Item("effectivedate").ToString
                                d.Item("jobgroupunkid") = CInt(drJob.Item("jobgroupunkid"))
                                d.Item("jobunkid") = CInt(drJob.Item("jobunkid"))

                                dsEmpAllocation.Tables(0).Rows.Add(d)
                            End If
                        Else
                            For Each d As DataRow In drFirst
                                d.Item("jobgroupunkid") = CInt(drJob.Item("jobgroupunkid"))
                                d.Item("jobunkid") = CInt(drJob.Item("jobunkid"))
                            Next
                            dsEmpAllocation.Tables(0).AcceptChanges()
                        End If

                        Dim drNext() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drJob.Item("employeeunkid")) & " AND effectivedate > '" & drJob.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate")

                        For Each d As DataRow In drNext
                            d.Item("jobgroupunkid") = CInt(drJob.Item("jobgroupunkid"))
                            d.Item("jobunkid") = CInt(drJob.Item("jobunkid"))
                        Next
                        dsEmpAllocation.Tables(0).AcceptChanges()
                    Next
                End If

                If dsBenefitMaster.Tables(0).Select("costcenterunkid > 0 ").Length > 0 Then

                    strB.Length = 0
                    strB.Append("SELECT employeeunkid " & _
                                     ", effectivedate " & _
                                     ", costcenterunkid " & _
                                "FROM " & _
                                "( " & _
                                    "SELECT CCT.employeeunkid " & _
                                         ", CCT.cctranheadvalueid AS costcenterunkid " & _
                                         ", CASE WHEN CONVERT(CHAR(8), CCT.effectivedate, 112) < @start_date THEN @start_date " & _
                                               "ELSE CONVERT(CHAR(8), CCT.effectivedate, 112) " & _
                                           "END AS effectivedate " & _
                                         ", ROW_NUMBER() OVER (PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC, CCT.cctranheadunkid DESC) AS Rno " & _
                                    "FROM hremployee_cctranhead_tran AS CCT " & _
                                    "WHERE CCT.isvoid = 0 " & _
                                          "AND CCT.employeeunkid IN (" & strEmployeeList & ") " & _
                                          "AND CCT.istransactionhead = 0 " & _
                                          "AND CONVERT(CHAR(8), CCT.effectivedate, 112) <= @start_date " & _
                                ") AS CCT " & _
                                "WHERE CCT.Rno = 1 " & _
                                "UNION ALL " & _
                                "SELECT CCT.employeeunkid " & _
                                     ", CONVERT(CHAR(8), CCT.effectivedate, 112) AS effectivedate " & _
                                     ", CCT.cctranheadvalueid AS costcenterunkid " & _
                                "FROM hremployee_cctranhead_tran AS CCT " & _
                                "WHERE CCT.isvoid = 0 " & _
                                      "AND CCT.istransactionhead = 0 " & _
                                      "AND CCT.employeeunkid IN (" & strEmployeeList & ") " & _
                                      "AND CONVERT(CHAR(8), CCT.effectivedate, 112) BETWEEN @start_date + 1 AND @end_date " _
                                      )

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

                    Dim ds As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    For Each drCC As DataRow In ds.Tables(0).Select("1 = 1", "employeeunkid, effectivedate")

                        Dim drFirst() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drCC.Item("employeeunkid")) & " AND effectivedate = '" & drCC.Item("effectivedate").ToString & "' ")

                        If drFirst.Length <= 0 Then
                            Dim drLast() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drCC.Item("employeeunkid")) & " AND effectivedate <= '" & drCC.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate DESC")
                            If drLast.Length > 0 Then
                                Dim d As DataRow = dsEmpAllocation.Tables(0).NewRow
                                d.ItemArray = drLast(0).ItemArray

                                d.Item("effectivedate") = drCC.Item("effectivedate").ToString
                                d.Item("costcenterunkid") = CInt(drCC.Item("costcenterunkid"))

                                dsEmpAllocation.Tables(0).Rows.Add(d)
                            End If
                        Else
                            For Each d As DataRow In drFirst
                                d.Item("costcenterunkid") = CInt(drCC.Item("costcenterunkid"))
                            Next
                            dsEmpAllocation.Tables(0).AcceptChanges()
                        End If

                        Dim drNext() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drCC.Item("employeeunkid")) & " AND effectivedate > '" & drCC.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate")

                        For Each d As DataRow In drNext
                            d.Item("costcenterunkid") = CInt(drCC.Item("costcenterunkid"))
                        Next
                        dsEmpAllocation.Tables(0).AcceptChanges()
                    Next
                End If

                If dsBenefitMaster.Tables(0).Select("gradegroupunkid > 0 OR gradeunkid > 0 OR gradelevelunkid > 0 ").Length > 0 Then

                    strB.Length = 0
                    strB.Append("SELECT employeeunkid " & _
                                     ", effectivedate " & _
                                     ", gradegroupunkid " & _
                                     ", gradeunkid " & _
                                     ", gradelevelunkid " & _
                                "FROM " & _
                                "( " & _
                                    "SELECT gradegroupunkid " & _
                                         ", gradeunkid " & _
                                         ", gradelevelunkid " & _
                                         ", employeeunkid " & _
                                         ", CASE WHEN CONVERT(CHAR(8), incrementdate, 112) < @start_date THEN @start_date " & _
                                               "ELSE CONVERT(CHAR(8), incrementdate, 112) " & _
                                           "END AS effectivedate " & _
                                         ", ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                                    "FROM prsalaryincrement_tran " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND isapproved = 1 " & _
                                          "AND prsalaryincrement_tran.employeeunkid IN (" & strEmployeeList & ") " & _
                                          "AND CONVERT(CHAR(8), incrementdate, 112) <= @start_date " & _
                                ") AS GRD " & _
                                "WHERE GRD.Rno = 1 " & _
                                "UNION ALL " & _
                                "SELECT employeeunkid " & _
                                     ", CONVERT(CHAR(8), incrementdate, 112) AS effectivedate " & _
                                     ", gradegroupunkid " & _
                                     ", gradeunkid " & _
                                     ", gradelevelunkid " & _
                                "FROM prsalaryincrement_tran " & _
                                "WHERE isvoid = 0 " & _
                                      "AND isapproved = 1 " & _
                                      "AND prsalaryincrement_tran.employeeunkid IN (" & strEmployeeList & ") " & _
                                      "AND CONVERT(CHAR(8), incrementdate, 112) BETWEEN @start_date + 1 AND @end_date " _
                                      )

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

                    Dim ds As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    For Each drGrade As DataRow In ds.Tables(0).Select("1 = 1", "employeeunkid, effectivedate")

                        Dim drFirst() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drGrade.Item("employeeunkid")) & " AND effectivedate = '" & drGrade.Item("effectivedate").ToString & "' ")

                        If drFirst.Length <= 0 Then
                            Dim drLast() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drGrade.Item("employeeunkid")) & " AND effectivedate <= '" & drGrade.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate DESC")
                            If drLast.Length > 0 Then
                                Dim d As DataRow = dsEmpAllocation.Tables(0).NewRow
                                d.ItemArray = drLast(0).ItemArray

                                d.Item("effectivedate") = drGrade.Item("effectivedate").ToString
                                d.Item("gradegroupunkid") = CInt(drGrade.Item("gradegroupunkid"))
                                d.Item("gradeunkid") = CInt(drGrade.Item("gradeunkid"))
                                d.Item("gradelevelunkid") = CInt(drGrade.Item("gradelevelunkid"))

                                dsEmpAllocation.Tables(0).Rows.Add(d)
                            End If
                        Else
                            For Each d As DataRow In drFirst
                                d.Item("gradegroupunkid") = CInt(drGrade.Item("gradegroupunkid"))
                                d.Item("gradeunkid") = CInt(drGrade.Item("gradeunkid"))
                                d.Item("gradelevelunkid") = CInt(drGrade.Item("gradelevelunkid"))
                            Next
                            dsEmpAllocation.Tables(0).AcceptChanges()
                        End If

                        Dim drNext() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drGrade.Item("employeeunkid")) & " AND effectivedate > '" & drGrade.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate")

                        For Each d As DataRow In drNext
                            d.Item("gradegroupunkid") = CInt(drGrade.Item("gradegroupunkid"))
                            d.Item("gradeunkid") = CInt(drGrade.Item("gradeunkid"))
                            d.Item("gradelevelunkid") = CInt(drGrade.Item("gradelevelunkid"))
                        Next
                        dsEmpAllocation.Tables(0).AcceptChanges()
                    Next
                End If
            End If
            'Sohail (17 Sep 2019) -- End

            'Sohail (02 Jan 2017) -- Start
            'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
            Dim objLoanAdvance As New clsLoan_Advance
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'dsLoanCalc = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
            '                                      mdtPeriodStartDate, mdtPeriodEndDate, xUserModeSetting, True, _
            '                                      "Loan", mdtPeriodEndDate.AddDays(1), strEmployeeList, 0, _
            '                                      mdtPeriodStartDate, True, , , True, True, , True, True, , , , enLoanStatus.IN_PROGRESS)
            'Sohail (28 May 2018) -- Start
            'Issue : On hold not coming in process payroll in 72.1.
            dsLoanCalc = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                  mdtPeriodStartDate, mdtPeriodEndDate, xUserModeSetting, True, _
                                                  "Loan", mdtPeriodEndDate.AddDays(1), strEmployeeList, 0, _
                                                  mdtPeriodStartDate, True, , , True, True, , True, True, , , , 0, False, , True, , objDataOperation, , , False)
            'Sohail (12 Oct 2021) - [blnApplyDateFilter, blnIsMSS=False]
            'Sohail (28 May 2020) - [blnAddProjectedPeriodDeductions:= True]
            'Sohail (28 May 2018) -- End
            'Sohail (03 May 2018) -- End
            'Sohail (02 Jan 2017) -- End

            Dim objFormulaSlab As New clsTranhead_formula_slab_Tran
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim dsFormulaSlab As DataSet = objFormulaSlab.GetList("List", , mdtPeriodEndDate)
            Dim dsFormulaSlab As DataSet = objFormulaSlab.GetList("List", , mdtPeriodEndDate, objDataOperation)
            'Sohail (03 May 2018) -- End
            Dim objTrnFormula As New clsTranheadFormulaTran
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim dsTrnFormula As DataSet = objTrnFormula.GetCurrentSlabForProcessPayroll("TranHead", 0, mdtPeriodEndDate)
            Dim dsTrnFormula As DataSet = objTrnFormula.GetCurrentSlabForProcessPayroll("TranHead", 0, mdtPeriodEndDate, , objDataOperation)
            'Sohail (03 May 2018) -- End
            Dim objTrnFormulaCumulative As New clsTranheadFormulaCumulativeTran
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim dsTrnFormulaCumulative As DataSet = objTrnFormulaCumulative.GetCurrentSlabForProcessPayroll("TranHead", 0, mdtPeriodEndDate)
            Dim dsTrnFormulaCumulative As DataSet = objTrnFormulaCumulative.GetCurrentSlabForProcessPayroll("TranHead", 0, mdtPeriodEndDate, , objDataOperation)
            'Sohail (03 May 2018) -- End
            Dim objTrnSlab As New clsTranheadSlabTran
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim dtTrnSlab As DataTable = objTrnSlab.GetCurrentTaxSlab(0, mdtPeriodEndDate)
            Dim dtTrnSlab As DataTable = objTrnSlab.GetCurrentTaxSlab(0, mdtPeriodEndDate, objDataOperation)
            'Sohail (03 May 2018) -- End
            Dim objTrnTaxSlab As New clsTranheadInexcessslabTran
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim dtTrnTaxSlab As DataTable = objTrnTaxSlab.GetCurrentTaxSlab(0, mdtPeriodEndDate)
            Dim dtTrnTaxSlab As DataTable = objTrnTaxSlab.GetCurrentTaxSlab(0, mdtPeriodEndDate, objDataOperation)
            'Sohail (03 May 2018) -- End
            Dim objED As New clsEarningDeduction
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim dsEDHeadID As DataSet = objED.GetCurrentSlabEDHeadId(mdtPeriodEndDate)
            'Sohail (10 Feb 2020) -- Start
            'NMB Enhancement # : Pick least amount benefit if head is mapped in more than one benefit and both benefits effective dates are same.
            'Dim dsEDHeadID As DataSet = objED.GetCurrentSlabEDHeadId(mdtPeriodEndDate, , objDataOperation)
            Dim dsEDHeadID As DataSet = objED.GetCurrentSlabEDHeadId(mdtPeriodEndDate, , objDataOperation, strEmployeeList)
            'Sohail (10 Feb 2020) -- End
            'Sohail (03 May 2018) -- End
            'Sohail (26 Aug 2016) -- End

            'Sohail (26 Oct 2018) -- Start
            'FDRC Issue - Process Payroll performance issue on FDRC database in 75.1.
            dsTranHead = objTranHead.GetList("List", -1, -1, True, True, 0, False, "", True, True, True, 0)
            'Sohail (26 Oct 2018) -- End

            'Sohail (25 Feb 2022) -- Start
            'Enhancement :  : Performance enhancement on process payroll.
            If blnApplyPayPerActivity = True Then
                dsPPAtran = objPPASummary.GetList("PPA", mintEmployeeunkid, mintPayperiodunkid, , , , True, , " prpayactivity_tran.employeeunkid IN (" & strEmployeeList & ") ", objDataOperation)
            End If
            dsCRProcess = objCRProcess.GetList("CRProcess", True, 1, "cmclaim_process_tran.periodunkid = " & mintPayperiodunkid & " AND cmclaim_process_tran.employeeunkid IN (" & strEmployeeList & ") AND cmclaim_process_tran.isprocessed = 0 AND ( cmclaim_process_tran.qtytranheadunkid > 0 OR cmclaim_process_tran.amttranheadunkid > 0 ) ", objDataOperation)
            dsRetireProcess = objRetireProcess.GetList("RetireProcess", False, True, 1, "cmretire_process_tran.periodunkid = " & mintPayperiodunkid & " AND cmretire_process_tran.employeeunkid IN (" & strEmployeeList & ") AND cmretire_process_tran.base_amount <> 0 AND cmretire_process_tran.isprocessed = 0 ", objDataOperation)
            'Sohail ((25 Feb 2022) -- End

            'Sohail (04 Mar 2011) -- Start
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'If strEmpVoidList.Trim.Length > 0 Then
            '    blnFlag = Void(strEmpVoidList, intPeriodUnkID, mintVoiduserunkid, Now, "")
            '    If blnFlag = False Then
            '        Return False
            '    End If
            'End If
            'Sohail (26 Aug 2016) -- End
            'Sohail (04 Mar 2011) -- End

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim intBaseCountryunkid As Integer = Company._Object._Countryunkid 'Sohail (07 May 2015)
            Dim intBaseCountryunkid As Integer = mintCountryunkid
            'Sohail (03 May 2018) -- End
            Dim dicExRate As New Dictionary(Of Integer, ArrayList)
            Dim arrList As ArrayList
            Dim objExRate As New clsExchangeRate
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim dsExRate As DataSet = objExRate.GetList("Rate", , , , , True, mdtProcessdate, True)
            Dim dsExRate As DataSet = objExRate.GetList("Rate", , , , , True, mdtProcessdate, True, objDataOperation)
            'Sohail (03 May 2018) -- End
            If dsExRate.Tables("Rate").Rows.Count > 0 Then
                For Each dRow As DataRow In dsExRate.Tables("Rate").Rows
                    If dicExRate.ContainsKey(CInt(dRow.Item("countryunkid"))) = False Then
                        arrList = New ArrayList
                        arrList.Add(CDec(dRow.Item("exchange_rate1")))
                        arrList.Add(CDec(dRow.Item("exchange_rate2")))
                        dicExRate.Add(CInt(dRow.Item("countryunkid")), arrList)
                    End If
                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    If CBool(dRow.Item("isbasecurrency")) = True Then
                        intBaseCountryunkid = CInt(dRow.Item("countryunkid"))
                    End If
                    'Sohail (07 May 2015) -- End
                Next
            End If
            objProcessPayroll._Dic_Ex_Rate = dicExRate
            objExRate = Nothing
            'Sohail (03 Sep 2012) -- End

            'Sohail (29 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            'objShiftTran.GetShiftTran(0)
            'Dim dtShift As DataTable = objShiftTran._dtShiftday
            Dim iShiftId As Integer = 0, iWorkingDays As Integer = 0, dWHours As Double = 0
            Dim objShift As New clsNewshift_master
            Dim arr As ArrayList
            'dtShift = New DataView(dtShift, "", "shiftunkid", DataViewRowState.CurrentRows).ToTable
            Dim dtDate As Date = mdtPeriodStartDate
            Dim dtEnd As Date = mdtPeriodEndDate

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim dsShift As DataSet = objShift.getListForCombo("Shift", False)
            Dim dsShift As DataSet = objShift.getListForCombo("Shift", False, , objDataOperation)
            'Sohail (03 May 2018) -- End

            For Each dt_Row As DataRow In dsShift.Tables("Shift").Rows

                dtDate = mdtPeriodStartDate
                intShiftId = CInt(dt_Row.Item("shiftunkid"))

                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'objShiftTran.GetShiftTran(intShiftId)
                objShiftTran.GetShiftTran(intShiftId, objDataOperation)
                'Sohail (03 May 2018) -- End

                objShift = New clsNewshift_master
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'objShift._Shiftunkid = intShiftId
                objShift.GetData(intShiftId, objDataOperation)
                'Sohail (03 May 2018) -- End

                iWorkingDays = 0
                dWHours = 0

                While dtDate <= dtEnd
                    Dim intDay As Integer = GetWeekDayNumber(dtDate.DayOfWeek.ToString)
                    Dim drRow() As DataRow = Nothing

                    'If objShift._IsDayOffShift = True Then
                    '    drRow = objShiftTran._dtShiftday.Select("dayid = " & intDay & " ")
                    'Else
                    drRow = objShiftTran._dtShiftday.Select("dayid = " & intDay & " AND isweekend = 0")
                    'End If

                    If drRow.Length > 0 Then
                        iWorkingDays += 1 'Total Working Days
                        dWHours += CInt(drRow(0).Item("workinghrsinsec")) 'Total Working Hours
                    End If

                    dtDate = dtDate.AddDays(1)
                End While

                If dicShiftDaysHours.ContainsKey(intShiftId) = False Then
                    arr = New ArrayList
                    arr.Add(iWorkingDays) 'Total Working Days
                    arr.Add(dWHours) 'Total Working Hours
                    dicShiftDaysHours.Add(intShiftId, arr)
                End If
            Next
            'Sohail (29 Oct 2013) -- End

            'Sohail (08 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            Dim dtRow() As DataRow
            Dim dsRow As DataRow
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'Dim dtEmp As DataTable = GetEmployeeDetail("Emp", strEmployeeList, mdtPeriodEndDate)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim dtEmp As DataTable = GetEmployeeDetail(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtPeriodStartDate, mdtPeriodEndDate, xUserModeSetting, True, True, True, "Emp", strEmployeeList, mdtPeriodEndDate)
            Dim dtEmp As DataTable = GetEmployeeDetail(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtPeriodStartDate, mdtPeriodEndDate, xUserModeSetting, True, True, True, "Emp", strEmployeeList, mdtPeriodEndDate, objDataOperation)
            'Sohail (03 May 2018) -- End
            'Sohail (15 Dec 2015) -- End
            'Sohail (08 Jun 2012) -- End

            'Sohail (21 Oct 2016) -- Start
            'Issue - 57.4 & 63.1 - Conversion from type DBNull to type Integer is not valid due to cost center not found or salary details not found.
            Dim strEmpCodes As String = String.Join(",", (From p In dtEmp Where (IsDBNull(p.Item("employeeunkid"))) Select (p.Item("empcode").ToString)).ToArray)
            If strEmpCodes.Trim <> "" Then
                Throw New Exception("There is some issue on getting Salary change or Cost Center details for the following employee code(s) : " & vbCrLf & strEmpCodes & vbCrLf)
            End If
            'Sohail (21 Oct 2016) -- End
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            Dim dicEmpCCenter As Dictionary(Of Integer, Integer) = (From p In dtEmp Select New With {Key .EmpId = CInt(p.Item("employeeunkid")), Key .CCId = CInt(p.Item("costcenterunkid"))}).ToDictionary(Function(x) x.EmpId, Function(y) y.CCId)
            'Sohail (26 Aug 2016) -- End

            Dim decIncrementAmount As Decimal = 0 'Sohail (18 Nov 2013)

            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'Sohail (26 Aug 2021) -- Start
            'Enhancement :  : Process payroll data optimization.
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()

            'blnFlag = objProcessPayroll.Void(strEmployeeList, intPeriodUnkID, mintVoiduserunkid, Now, "", objDataOperation)

            'If blnFlag = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'End If

            'If strEmpVoidList.Trim.Length > 0 Then
            '    blnFlag = Void(strEmpVoidList, intPeriodUnkID, mintVoiduserunkid, Now, "", objDataOperation)
            '    If blnFlag = False Then
            '        objDataOperation.ReleaseTransaction(False)
            '        Return False
            '    End If
            'End If

            'objDataOperation.ReleaseTransaction(True)
            'Sohail (26 Aug 2021) -- End
            'Sohail (26 Aug 2016) -- End

            'Sohail (28 Oct 2017) -- Start
            'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
            strB.Length = 0
            strB.Append("CREATE TABLE #TableEmp ( EmpID INT, EmpName NVARCHAR(MAX) ) " & _
                   "INSERT  INTO #TableEmp " & _
                           "( EmpID " & _
                           ", EmpName " & _
                           ") " & _
                           "SELECT  employeeunkid " & _
                                 ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                           "FROM    hremployee_master " & _
                               "WHERE   employeeunkid IN ( " & strEmployeeList & " ) ")

            strB.Append("SELECT TRM.employeeunkid AS TEEmpId " & _
                             ", CONVERT(CHAR(8), TRM.reinstatment_date, 112) AS reinstatment_date " & _
                             ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                             ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                             ", ISNULL(RTE.name, '') AS TR_REASON " & _
                             ", TRM.changereasonunkid " & _
                        "FROM hremployee_rehire_tran AS TRM " & _
                            "JOIN #TableEmp ON #TableEmp.EmpID = TRM.employeeunkid " & _
                            "LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid " & _
                                   "AND RTE.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & " " & _
                        "WHERE isvoid = 0 " & _
                              "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @end_date " & _
                              "AND CONVERT(CHAR(8), TRM.reinstatment_date, 112) BETWEEN @start_date AND @end_date " _
                          )

            'Sohail (08 Aug 2019) -- Start
            'NMB Payroll UAT # TC002 - 76.1 - System to prorate and remove allowance during suspension.
            '*** Remove /*, */ to enable suspension proration 
            If mblnExcludeEmpFromPayrollDuringSuspension = True Then 'Sohail (09 Oct 2019)
                strB.Append("UNION ALL " & _
                        "SELECT TRM.employeeunkid AS TEEmpId " & _
                            ", CONVERT(CHAR(8), TRM.date1, 112) AS reinstatment_date " & _
                            ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                            ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                            ", ISNULL(RTE.name, '') AS TR_REASON " & _
                            ", TRM.changereasonunkid " & _
                       "FROM hremployee_dates_tran AS TRM " & _
                           "JOIN #TableEmp ON #TableEmp.EmpID = TRM.employeeunkid " & _
                           "LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid " & _
                                  "AND RTE.mastertype = " & clsCommon_Master.enCommonMaster.SUSPENSION & " " & _
                       "WHERE isvoid = 0 " & _
                             "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                             "AND TRM.date1 IS NOT NULL " & _
                             "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @end_date " & _
                                 "AND CONVERT(CHAR(8), TRM.date1, 112) BETWEEN @start_date AND @end_date " _
                         )

                'Sohail (07 Nov 2019) -- Start
                'NMB Enhancement # : Flatrate prorating issue on employee exemption.
                strB.Append("UNION ALL " & _
                            "SELECT TRM.employeeunkid AS TEEmpId " & _
                                ", CONVERT(CHAR(8), TRM.date2, 112) AS reinstatment_date " & _
                                ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                                ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                ", ISNULL(RTE.name, '') AS TR_REASON " & _
                                ", TRM.changereasonunkid " & _
                           "FROM hremployee_dates_tran AS TRM " & _
                               "JOIN #TableEmp ON #TableEmp.EmpID = TRM.employeeunkid " & _
                               "LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid " & _
                                      "AND RTE.mastertype = " & clsCommon_Master.enCommonMaster.SUSPENSION & " " & _
                           "WHERE isvoid = 0 " & _
                                 "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                                 "AND TRM.date2 IS NOT NULL " & _
                                 "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @end_date " & _
                                 "AND CONVERT(CHAR(8), TRM.date2, 112) BETWEEN @start_date AND @end_date " _
                             )
                'Sohail (07 Nov 2019) -- End

            End If 'Sohail (09 Oct 2019)
            'Sohail (08 Aug 2019) -- End

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : system will prorate salary and flat rate heads for employee exemption days for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            '*** Employee Exemption
            strB.Append("UNION ALL " & _
                            "SELECT TRM.employeeunkid AS TEEmpId " & _
                                ", CONVERT(CHAR(8), TRM.date1, 112) AS reinstatment_date " & _
                                ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                                ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                ", ISNULL(RTE.name, '') AS TR_REASON " & _
                                ", TRM.changereasonunkid " & _
                           "FROM hremployee_dates_tran AS TRM " & _
                               "JOIN #TableEmp ON #TableEmp.EmpID = TRM.employeeunkid " & _
                               "LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid " & _
                                      "AND RTE.mastertype = " & clsCommon_Master.enCommonMaster.EXEMPTION & " " & _
                           "WHERE isvoid = 0 " & _
                                 "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                                 "AND TRM.date1 IS NOT NULL " & _
                                 "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @end_date " & _
                                 "AND CONVERT(CHAR(8), TRM.date1, 112) BETWEEN @start_date AND @end_date " _
                             )
            'Sohail (21 Oct 2019) -- End

            'Sohail (07 Nov 2019) -- Start
            'NMB Enhancement # : Flatrate prorating issue on employee exemption.
            strB.Append("UNION ALL " & _
                            "SELECT TRM.employeeunkid AS TEEmpId " & _
                                ", CONVERT(CHAR(8), TRM.date2, 112) AS reinstatment_date " & _
                                ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                                ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                ", ISNULL(RTE.name, '') AS TR_REASON " & _
                                ", TRM.changereasonunkid " & _
                           "FROM hremployee_dates_tran AS TRM " & _
                               "JOIN #TableEmp ON #TableEmp.EmpID = TRM.employeeunkid " & _
                               "LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid " & _
                                      "AND RTE.mastertype = " & clsCommon_Master.enCommonMaster.EXEMPTION & " " & _
                           "WHERE isvoid = 0 " & _
                                 "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                                 "AND TRM.date2 IS NOT NULL " & _
                                 "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @end_date " & _
                                 "AND CONVERT(CHAR(8), TRM.date2, 112) BETWEEN @start_date AND @end_date " _
                             )
            'Sohail (07 Nov 2019) -- End

            strB.Append("DROP TABLE #TableEmp ")

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

            Dim dsRehire As DataSet = objDataOperation.ExecQuery(strB.ToString(), "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsRehireTnA As DataSet = Nothing
            If mdtTnA_StartDate <> mdtPeriodStartDate OrElse mdtTnA_EndDate <> mdtPeriodEndDate Then

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTnA_StartDate))
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTnA_EndDate))

                dsRehireTnA = objDataOperation.ExecQuery(strB.ToString(), "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'Sohail (28 Oct 2017) -- End

            'Sohail (22 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (25 Jun 2018) -- Start
            'Internal Enhancement - Showing Progress count on Aruti Self Service for Process Payroll in 72.1.
            'bw.ReportProgress(0)
            If bw IsNot Nothing Then
            bw.ReportProgress(0)
            End If
            clsTnALeaveTran._ProgressCurrCount = 0
            'Sohail (25 Jun 2018) -- End
            Dim intCount As Integer = 0
            'Sohail (22 Aug 2012) -- End

            'Sohail (19 Dec 2018) -- Start
            'NMB Enhancement - On Salary change screen. Allow salary proration for the new salary i.e for full basic salary, take old salary untill the effective date of the new salary + new salary from the effective date in 76.1.
            '*** Get list of employees having salary increment in current period.
            strB.Length = 0
            strB.Append("SELECT DISTINCT " & _
                            "prsalaryincrement_tran.employeeunkid " & _
                        "INTO #tblMultiSalary " & _
                        "FROM prsalaryincrement_tran " & _
                        "WHERE isvoid = 0 " & _
                              "AND prsalaryincrement_tran.employeeunkid IN ( " & strEmployeeList & " ) " & _
                              "AND CONVERT(CHAR(8), incrementdate, 112) BETWEEN @start_dateplusone AND @end_date " & _
                        " " & _
                        "SELECT * " & _
                        "FROM " & _
                        "( " & _
                            "SELECT prsalaryincrement_tran.salaryincrementtranunkid " & _
                                 ", prsalaryincrement_tran.employeeunkid " & _
                                 ", CONVERT(CHAR(8), prsalaryincrement_tran.incrementdate, 112) AS incrementdate " & _
                                 ", prsalaryincrement_tran.currentscale " & _
                                 ", prsalaryincrement_tran.increment " & _
                                 ", prsalaryincrement_tran.newscale " & _
                                 ", 0 AS WeekendIncludedDays " & _
                                 ", 0 AS WeekendExcludedDays " & _
                                 ", DENSE_RANK() OVER (PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY incrementdate DESC , salaryincrementtranunkid DESC ) AS ROWNO " & _
                            "FROM prsalaryincrement_tran " & _
                                "JOIN #tblMultiSalary ON #tblMultiSalary.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
                            "WHERE isvoid = 0 " & _
                                  "AND CONVERT(CHAR(8), incrementdate, 112) <= @start_date " & _
                        ") AS A " & _
                        "WHERE A.ROWNO = 1 " & _
                        "UNION " & _
                        "SELECT prsalaryincrement_tran.salaryincrementtranunkid " & _
                             ", prsalaryincrement_tran.employeeunkid " & _
                             ", CONVERT(CHAR(8), prsalaryincrement_tran.incrementdate, 112) AS incrementdate " & _
                             ", prsalaryincrement_tran.currentscale " & _
                             ", prsalaryincrement_tran.increment " & _
                             ", prsalaryincrement_tran.newscale " & _
                             ", 0 AS WeekendIncludedDays " & _
                             ", 0 AS WeekendExcludedDays " & _
                             ", 1 AS ROWNO " & _
                        "FROM prsalaryincrement_tran " & _
                            "JOIN #tblMultiSalary ON #tblMultiSalary.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND CONVERT(CHAR(8), incrementdate, 112) BETWEEN @start_date AND @end_date " & _
                        "ORDER BY employeeunkid " & _
                               ", incrementdate " & _
                               ", salaryincrementtranunkid " & _
                        " " & _
                        "DROP TABLE #tblMultiSalary ")

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
            objDataOperation.AddParameter("@start_dateplusone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate.AddDays(1)))
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

            Dim dsMultiSalary As DataSet = objDataOperation.ExecQuery(strB.ToString(), "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            '*** Removing Same new scale from dataset
            Dim strPrevKy As String = ""
            For Each d_r_MS As DataRow In dsMultiSalary.Tables(0).Rows

                If strPrevKy = d_r_MS.Item("employeeunkid").ToString & "_" & d_r_MS.Item("newscale").ToString Then
                    d_r_MS.Delete()
                Else
                    strPrevKy = d_r_MS.Item("employeeunkid").ToString & "_" & d_r_MS.Item("newscale").ToString
                End If
            Next
            dsMultiSalary.Tables(0).AcceptChanges()
            'Sohail (19 Dec 2018) -- End

            'Sohail (24 Dec 2018) -- Start
            'NMB Enhancement - 76.1 - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
            Dim objEmpDates As New clsemployee_dates_tran
            Dim dsService As DataSet = objEmpDates.GetEmployeeServiceDays(objDataOperation, xCompanyUnkid, xDatabaseName, strEmployeeList, mdtPeriodEndDate)
            'Sohail (24 Dec 2018) -- End

            Dim cnt As Integer = dsCalc.Rows.Count - 1
            For i As Integer = 0 To cnt
                'For Each dsRow As DataRow In dsCalc.Tables("List").Rows
                dsRow = dsCalc.Rows(i)
                strCurrEmpName = dsRow.Item("EmpName").ToString 'Sohail (18 May 2013)

                'Sohail (26 Aug 2016) -- Start
                'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
                'Sohail (26 Aug 2016) -- End

                'Sohail (04 Mar 2011) -- Start
                'lngVoucherNo += 1
                If CInt(dsRow.Item("paymenttranunkid").ToString) <= 0 Then
                    lngVoucherNo += 1
                End If
                mintTnaleavetranunkid = CInt(dsRow.Item("tnaleavetranunkid").ToString)
                mdecOpeningBalance = CDec(dsRow.Item("OpeningBalance").ToString) 'Sohail (11 May 2011)
                decPaidAmount = CDec(dsRow.Item("PaidAmount").ToString) 'Sohail (11 May 2011)
                'Sohail (04 Mar 2011) -- End

                'Sohail (08 Nov 2011) -- Start
                'TRA Requirement : Add feature to pay net salary based on New Employee Reporting date to work. the salary should be computed based on number of days remaining towards end of the period. This is for defined salaries.
                'Sohail (08 Jun 2012) -- Start
                'TRA - ENHANCEMENT
                'objEmployee = New clsEmployee_Master
                'objEmployee._Employeeunkid = CInt(dsRow.Item("EmpID").ToString)

                'objMaster = New clsMasterData
                'dsList = objMaster.Get_Current_Scale("Salary", CInt(dsRow.Item("EmpID").ToString), mdtPeriodEndDate)
                'If dsList.Tables("Salary").Rows.Count > 0 Then
                '    mdecBasicScale = CDec(dsList.Tables("Salary").Rows(0).Item("newscale").ToString) 'Sohail (11 May 2011)
                'Else
                '    objWages = New clsWagesTran
                '    dsList = objWages.getScaleInfo(objEmployee._Gradeunkid, objEmployee._Gradelevelunkid, "Scale")
                '    If dsList.Tables("Scale").Rows.Count > 0 Then
                '        mdecBasicScale = CDec(dsList.Tables("Scale").Rows(0).Item("Salary").ToString) 'Sohail (11 May 2011)
                '    Else
                '        mdecBasicScale = 0
                '    End If
                'End If

                dtAppointment = Nothing
                dtTerminationFrom = Nothing
                dtTerminationTo = Nothing
                dtEmplEnd = Nothing
                intGradeId = -1
                intGradeLevelId = -1
                intCostCenterId = -1
                intTranHeadId = -1
                intCalcTypeId = -1
                intShiftId = -1
                strShiftDays = String.Empty
                intEdPeriodId = -1
                decIncrementAmount = 0 'Sohail (18 Nov 2013)
                'Sohail (24 Sep 2014) -- Start
                'Enhancement - Weekend Days and Public Holiday Days function in transaction head formula.
                intWorkingDaysInTnAPeriod = 0
                'Sohail (26 Sep 2014) -- Start
                'Enhancement - Weekend Hours and Public Holiday Hours function in transaction head formula.
                'intPHDaysInPeriod = 0
                'intPHDaysInTnAPeriod = 0
                'Sohail (26 Sep 2014) -- End
                'Sohail (24 Sep 2014) -- End

                dtRow = dtEmp.Select("employeeunkid = " & CInt(dsRow.Item("EmpID").ToString) & " ")
                If dtRow.Length > 0 Then
                    If IsDBNull(dtRow(0).Item("newscale")) = False Then
                        mdecBasicScale = CDec(dtRow(0).Item("newscale").ToString)
                    Else
                        objWages = New clsWagesTran
                        'Sohail (03 May 2018) -- Start
                        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                        'dsList = objWages.getScaleInfo(intGradeId, intGradeLevelId, mdtPeriodEndDate, "Scale")
                        dsList = objWages.getScaleInfo(intGradeId, intGradeLevelId, mdtPeriodEndDate, "Scale", objDataOperation)
                        'Sohail (03 May 2018) -- End
                        If dsList.Tables("Scale").Rows.Count > 0 Then
                            mdecBasicScale = CDec(dsList.Tables("Scale").Rows(0).Item("Salary").ToString)
                        Else
                            mdecBasicScale = 0
                        End If
                    End If

                    If IsDBNull(dtRow(0).Item("appointeddate")) = False Then
                        dtAppointment = CDate(dtRow(0).Item("appointeddate"))
                    End If
                    If IsDBNull(dtRow(0).Item("termination_from_date")) = False Then
                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        'dtTerminationFrom = CDate(dtRow(0).Item("termination_from_date"))
                        dtTerminationFrom = eZeeDate.convertDate(dtRow(0).Item("termination_from_date").ToString)
                        'Sohail (15 Dec 2015) -- End
                    End If
                    If IsDBNull(dtRow(0).Item("termination_to_date")) = False Then
                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        'dtTerminationTo = CDate(dtRow(0).Item("termination_to_date"))
                        dtTerminationTo = eZeeDate.convertDate(dtRow(0).Item("termination_to_date").ToString)
                        'Sohail (15 Dec 2015) -- End
                    End If
                    If IsDBNull(dtRow(0).Item("empl_enddate")) = False Then
                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        'dtEmplEnd = CDate(dtRow(0).Item("empl_enddate"))
                        dtEmplEnd = eZeeDate.convertDate(dtRow(0).Item("empl_enddate").ToString)
                        'Sohail (15 Dec 2015) -- End
                    End If
                    intGradeId = CInt(dtRow(0).Item("gradeunkid"))
                    intGradeLevelId = CInt(dtRow(0).Item("gradelevelunkid"))
                    intCostCenterId = CInt(dtRow(0).Item("costcenterunkid"))
                    intTranHeadId = CInt(dtRow(0).Item("tranhedunkid"))
                    intCalcTypeId = CInt(dtRow(0).Item("calctype_id"))
                    intShiftId = CInt(dtRow(0).Item("shiftunkid"))
                    'strShiftDays = dtRow(0).Item("shiftdays").ToString
                    intEdPeriodId = CInt(dtRow(0).Item("CurrentEDSlabPeriodUnkId"))
                    decIncrementAmount = CDec(dtRow(0).Item("incrementamount")) 'Sohail (18 Nov 2013)
                Else
                    mdecBasicScale = 0
                End If
                'Sohail (08 Jun 2012) -- End
                'Sohail (08 Nov 2011) -- End
                mdecBasicSalaryOnWages = mdecBasicScale 'Sohail (29 Oct 2013)

                With dsRow

                    'Sohail (26 Aug 2016) -- Start
                    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                    'Sohail (02 Jan 2017) -- Start
                    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                    'blnFlag = objProcessPayroll.Void(.Item("EmpID").ToString, intPeriodUnkID, mintVoiduserunkid, Now, "", objDataOperation)

                    'If blnFlag = False Then
                    '    objDataOperation.ReleaseTransaction(False)
                    '    Return False
                    'End If

                    ''Sohail (26 Aug 2016) -- Start
                    ''Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                    'If strEmpVoidList.Trim.Length > 0 Then
                    '    blnFlag = Void(.Item("EmpID").ToString, intPeriodUnkID, mintVoiduserunkid, Now, "", objDataOperation)
                    '    If blnFlag = False Then
                    '        Return False
                    '    End If
                    'End If
                    'Sohail (02 Jan 2017) -- End
                    'Sohail (26 Aug 2016) -- End
                    'Sohail (26 Aug 2016) -- End

                    'Sohail (29 Oct 2012) -- Start
                    'TRA - ENHANCEMENT
                    'intWorkingDaysInPeriod = GetWorkingDays(CInt(.Item("EmpID").ToString), mdtPeriodStartDate, mdtPeriodEndDate, intShiftId, strShiftDays)
                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    'intWorkingDaysInPeriod = GetWorkingDays(CInt(.Item("EmpID").ToString), mdtPeriodStartDate, mdtPeriodEndDate, intShiftId)
                    'Sohail (29 Oct 2013) -- Start
                    'TRA - ENHANCEMENT
                    'objEmpShift.GetWorkingDaysAndHours(CInt(.Item("EmpID").ToString), mdtPeriodStartDate, mdtPeriodEndDate, intWorkingDaysInPeriod, dblWorkingHoursInPeriod)
                    'If intWorkingDaysInPeriod <> 0 Then
                    '    dblDailyHours = dblWorkingHoursInPeriod / intWorkingDaysInPeriod
                    'End If

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                    'arrEmpShift = objEmpShift.GetEmployeeDistinctShift(CInt(.Item("EmpID").ToString), mdtPeriodStartDate, mdtPeriodEndDate)
                    'Sohail (03 May 2018) -- Start
                    'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                    'arrEmpShift = objEmpShift.GetEmployeeDistinctShift(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, CInt(.Item("EmpID").ToString), mdtPeriodStartDate, mdtPeriodEndDate)
                    'Sohail (12 Oct 2021) -- Start
                    'NMB Issue :  : Performance issue on process payroll.
                    'arrEmpShift = objEmpShift.GetEmployeeDistinctShift(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, CInt(.Item("EmpID").ToString), mdtPeriodStartDate, mdtPeriodEndDate, objDataOperation)
                    arrEmpShift = objEmpShift.GetEmployeeDistinctShift(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, CInt(.Item("EmpID").ToString), mdtPeriodStartDate, mdtPeriodEndDate, objDataOperation, False, "")
                    'Sohail (12 Oct 2021) -- End
                    'Sohail (03 May 2018) -- End
                    'Shani(24-Aug-2015) -- End
                    If arrEmpShift.Count = 1 Then
                        arr = dicShiftDaysHours.Item(CInt(arrEmpShift(0)))
                        intWorkingDaysInPeriod = arr(0) 'Total Working Days
                        dblWorkingHoursInPeriod = arr(1) 'Total Working Hours
                    Else

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                        'objEmpShift.GetWorkingDaysAndHours(CInt(.Item("EmpID").ToString), mdtPeriodStartDate, mdtPeriodEndDate, intWorkingDaysInPeriod, dblWorkingHoursInPeriod)
                        'Sohail (03 May 2018) -- Start
                        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                        'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '                                   xUserUnkid, _
                        '                                   xYearUnkid, _
                        '                                   xCompanyUnkid, _
                        '                                   xUserModeSetting, _
                        '                                   CInt(.Item("EmpID").ToString), _
                        '                                   mdtPeriodStartDate, _
                        '                                   mdtPeriodEndDate, _
                        '                                   intWorkingDaysInPeriod, _
                        '                                   dblWorkingHoursInPeriod)
                        objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                           xUserUnkid, _
                                                           xYearUnkid, _
                                                           xCompanyUnkid, _
                                                           xUserModeSetting, _
                                                           CInt(.Item("EmpID").ToString), _
                                                           mdtPeriodStartDate, _
                                                           mdtPeriodEndDate, _
                                                           intWorkingDaysInPeriod, _
                                                          dblWorkingHoursInPeriod, _
                                                          objDataOperation, False, "")
                        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]
                        'Sohail (03 May 2018) -- End
                        'Shani(24-Aug-2015) -- End
                    End If
                    If intWorkingDaysInPeriod <> 0 Then
                        dblDailyHours = dblWorkingHoursInPeriod / intWorkingDaysInPeriod
                    End If
                    'Sohail (29 Oct 2013) -- End
                    'Sohail (24 Sep 2013) -- End
                    'Sohail (29 Oct 2012) -- End
                    dblOnHoldDays = CDec(.Item("OnHoldDays").ToString)

                    'Sohail (24 Sep 2014) -- Start
                    'Enhancement - Weekend Days and Public Holiday Days function in transaction head formula.

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                    'objEmpShift.GetWorkingDaysAndHours(CInt(.Item("EmpID").ToString), mdtTnA_StartDate, mdtTnA_EndDate, intWorkingDaysInTnAPeriod, 0)
                    'Sohail (03 May 2018) -- Start
                    'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                    'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                    '                                   xUserUnkid, _
                    '                                   xYearUnkid, _
                    '                                   xCompanyUnkid, _
                    '                                   xUserModeSetting, _
                    '                                   CInt(.Item("EmpID").ToString), _
                    '                                   mdtTnA_StartDate, _
                    '                                   mdtTnA_EndDate, _
                    '                                   intWorkingDaysInTnAPeriod, 0)
                    objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                       xUserUnkid, _
                                                       xYearUnkid, _
                                                       xCompanyUnkid, _
                                                       xUserModeSetting, _
                                                       CInt(.Item("EmpID").ToString), _
                                                       mdtTnA_StartDate, _
                                                       mdtTnA_EndDate, _
                                                       intWorkingDaysInTnAPeriod, 0, objDataOperation, False, "")
                    'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]
                    'Sohail (03 May 2018) -- End
                    'Shani(24-Aug-2015) -- End

                    'Sohail (26 Sep 2014) -- Start
                    'Enhancement - Weekend Hours and Public Holiday Hours function in transaction head formula.
                    'Dim ds As DataSet = objHoliday.GetList("Holiday", CInt(.Item("EmpID").ToString), , , , , "CONVERT(CHAR(8), lvholiday_master.holidaydate, 112) BETWEEN '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' ")
                    'intPHDaysInPeriod = ds.Tables("Holiday").Rows.Count
                    'ds = objHoliday.GetList("Holiday", CInt(.Item("EmpID").ToString), , , , , "CONVERT(CHAR(8), lvholiday_master.holidaydate, 112) BETWEEN '" & eZeeDate.convertDate(mdtTnA_StartDate) & "' AND '" & eZeeDate.convertDate(mdtTnA_EndDate) & "' ")
                    'intPHDaysInTnAPeriod = ds.Tables("Holiday").Rows.Count
                    'Sohail (26 Sep 2014) -- End
                    'Sohail (24 Sep 2014) -- End

                    'Sohail (08 Nov 2011) -- Start
                    decDailyBasicSalary = mdecBasicScale / intWorkingDaysInPeriod
                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    'decHourlyBasicSalary = decDailyBasicSalary / (CDec(dsRow.Item("DailyHours")) / 3600)
                    decHourlyBasicSalary = decDailyBasicSalary / (dblDailyHours / 3600)
                    'Sohail (24 Sep 2013) -- End
                    'Sohail (12 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    decDailyBasicSalaryWeekendsIncluded = mdecBasicScale / CDec(.Item("DaysInPeriod"))
                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    'decHourlyBasicSalaryWeekendsIncluded = decDailyBasicSalaryWeekendsIncluded / (CDec(dsRow.Item("DailyHours")) / 3600)
                    decHourlyBasicSalaryWeekendsIncluded = decDailyBasicSalaryWeekendsIncluded / (dblDailyHours / 3600)
                    'Sohail (24 Sep 2013) -- End
                    'Sohail (12 Apr 2012) -- End
                    'Sohail (10 Jul 2013) -- Start
                    'TRA - ENHANCEMENT
                    decDailyBasicSalaryOnConstantDays = mdecBasicScale / CInt(.Item("ConstantDaysInPeriod"))
                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    'decHourlyBasicSalaryOnConstantDays = decDailyBasicSalaryOnConstantDays / (CDec(dsRow.Item("DailyHours")) / 3600)
                    decHourlyBasicSalaryOnConstantDays = decDailyBasicSalaryOnConstantDays / (dblDailyHours / 3600)
                    'Sohail (24 Sep 2013) -- End
                    'Sohail (10 Jul 2013) -- End
                    'Sohail (30 Dec 2011) -- Start
                    'If intWorkingDaysInPeriod <> 0 AndAlso objEmployee._Appointeddate.Date > mdtPeriodStartDate Then
                    'intEmpWorkingDaysInPeriod = GetWorkingDays(CInt(dsRow.Item("EmpID").ToString), objEmployee._Appointeddate.Date, mdtPeriodEndDate)
                    'Sohail (08 Jun 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Dim StartDate As Date = IIf(objEmployee._Appointeddate.Date > mdtPeriodStartDate, objEmployee._Appointeddate.Date, mdtPeriodStartDate)
                    'Dim EndDate As Date = mdtPeriodEndDate
                    'If objEmployee._Termination_From_Date.Date <> Nothing Then
                    '    EndDate = IIf(objEmployee._Termination_From_Date.Date < mdtPeriodEndDate, objEmployee._Termination_From_Date.Date, mdtPeriodEndDate)
                    'End If
                    'If objEmployee._Termination_To_Date.Date <> Nothing Then
                    '    EndDate = IIf(objEmployee._Termination_To_Date.Date < EndDate, objEmployee._Termination_To_Date.Date, EndDate)
                    'End If
                    'If StartDate <> mdtPeriodStartDate OrElse EndDate <> mdtPeriodEndDate Then
                    '    'Sohail (30 Dec 2011) -- End
                    '    intEmpWorkingDaysInPeriod = GetWorkingDays(CInt(dsRow.Item("EmpID").ToString), StartDate, EndDate)
                    '    If ConfigParameter._Object._BasicSalaryComputation = enBasicSalaryComputation.WITH_WEEKENDS_EXCLUDED Then 'Sohail (17 Apr 2012)
                    '        mdecBasicScale = mdecBasicScale * intEmpWorkingDaysInPeriod / intWorkingDaysInPeriod
                    '        'Sohail (17 Apr 2012) -- Start
                    '        'TRA - ENHANCEMENT
                    '    ElseIf ConfigParameter._Object._BasicSalaryComputation = enBasicSalaryComputation.WITH_WEEKENDS_INCLUDED Then
                    '        mdecBasicScale = mdecBasicScale * (DateDiff(DateInterval.Day, StartDate, EndDate) + 1) / CInt(.Item("DaysInPeriod"))
                    '    End If
                    '    'Sohail (17 Apr 2012) -- End
                    'Else
                    '    intEmpWorkingDaysInPeriod = intWorkingDaysInPeriod
                    'End If
                    Dim StartDate As Date = IIf(dtAppointment.Date > mdtPeriodStartDate, dtAppointment.Date, mdtPeriodStartDate)
                    Dim EndDate As Date = mdtPeriodEndDate
                    If dtTerminationFrom.Date <> Nothing Then
                        EndDate = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
                    End If
                    If dtTerminationTo.Date <> Nothing Then
                        EndDate = IIf(dtTerminationTo.Date < EndDate, dtTerminationTo.Date, EndDate)
                    End If
                    'Sohail (10 Oct 2013) -- Start
                    'TRA - ENHANCEMENT
                    If dtEmplEnd.Date <> Nothing Then
                        EndDate = IIf(dtEmplEnd.Date < EndDate, dtEmplEnd.Date, EndDate)
                    End If
                    'Sohail (10 Oct 2013) -- End
                    'Sohail (09 Oct 2019) -- Start
                    'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
                    mdtEmployee_enddate = EndDate
                    'Sohail (09 Oct 2019) -- End

                    'Sohail (28 Oct 2017) -- Start
                    'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
                    Dim TnAStartDate As Date = IIf(dtAppointment.Date > mdtTnA_StartDate, dtAppointment.Date, mdtTnA_StartDate)
                    Dim TnAEndDate As Date = mdtTnA_EndDate
                    If dtTerminationFrom.Date <> Nothing Then
                        TnAEndDate = IIf(dtTerminationFrom.Date < mdtTnA_EndDate, dtTerminationFrom.Date, mdtTnA_EndDate)
                    End If
                    If dtTerminationTo.Date <> Nothing Then
                        TnAEndDate = IIf(dtTerminationTo.Date < TnAEndDate, dtTerminationTo.Date, TnAEndDate)
                    End If
                    If dtEmplEnd.Date <> Nothing Then
                        TnAEndDate = IIf(dtEmplEnd.Date < TnAEndDate, dtEmplEnd.Date, TnAEndDate)
                    End If

                    Dim iDays As Integer = 0
                    Dim dtS As Date = StartDate
                    Dim dtE As Date = EndDate
                    intEmpTotalDaysInPeriod = 0
                    intEmpWorkingDaysInPeriod = 0
                    'Hemant (19 July 2018) -- Start
                    'Support Issue Id # 0002391 -- Wrong hours worked posted on payroll,
                    dblEmpWorkingHoursInPeriod = 0
                    Dim dHours As Double = 0
                    'Hemant (19 July 2018) -- End

                    'Sohail (19 Dec 2018) -- Start
                    'NMB Enhancement - On Salary change screen. Allow salary proration for the new salary i.e for full basic salary, take old salary untill the effective date of the new salary + new salary from the effective date in 76.1.
                    Dim drMultiSalary() As DataRow = dsMultiSalary.Tables(0).Select("employeeunkid = " & CInt(.Item("EmpID")) & " ")
                    Dim drMultiSalaryDESC() As DataRow = dsMultiSalary.Tables(0).Select("employeeunkid = " & CInt(.Item("EmpID")) & " ", "employeeunkid ASC, incrementdate DESC, salaryincrementtranunkid DESC")
                    'Sohail (19 Dec 2018) -- End

                    'Sohail (04 Apr 2020) -- Start
                    'NMB Issue # : Benefit calculation coming wrong when processing for all employees when current employee Rehire transaction not there and there is a Rehire transaction for any previous processed employees.
                    dsEOC = Nothing
                    'Sohail (04 Apr 2020) -- End

                    Dim drRehire() As DataRow = dsRehire.Tables(0).Select("TEEmpID = " & CInt(.Item("EmpID")) & " ")
                    If drRehire.Length > 0 Then

                        'Sohail (18 Dec 2018) -- Start
                        'Internal Issue - Wrong salary amount was coming when rehired is done more than one time in a period in 76.1.
                        'For Each dRh As DataRow In drRehire

                        '    strB.Length = 0
                        '    strB.Append("SELECT A.TEEmpId " & _
                        '                     ", A.empl_enddate " & _
                        '                     ", A.termination_from_date " & _
                        '                     ", A.TEfDt " & _
                        '                     ", A.TR_REASON " & _
                        '                     ", A.changereasonunkid " & _
                        '                     ", A.rehiretranunkid " & _
                        '                "FROM " & _
                        '                "( " & _
                        '                    "SELECT TRM.employeeunkid AS TEEmpId " & _
                        '                         ", CONVERT(CHAR(8), TRM.date1, 112) AS empl_enddate " & _
                        '                         ", CONVERT(CHAR(8), TRM.date2, 112) AS termination_from_date " & _
                        '                         ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                        '                         ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                        '                         ", CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name, '') ELSE ISNULL(TEC.name, '') END AS TR_REASON " & _
                        '                         ", TRM.changereasonunkid " & _
                        '                         ", ISNULL(TRM.rehiretranunkid, 0) AS rehiretranunkid " & _
                        '                    "FROM hremployee_dates_tran AS TRM " & _
                        '                        "LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid " & _
                        '                               "AND RTE.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & " " & _
                        '                        "LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid " & _
                        '                               "AND TEC.mastertype = " & clsCommon_Master.enCommonMaster.TERMINATION & " " & _
                        '                    "WHERE isvoid = 0 " & _
                        '                          "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                        '                          "AND TRM.employeeunkid = @employeeunkid " & _
                        '                          "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @end_date " & _
                        '                ") AS A " & _
                        '                "WHERE A.Rno IN (1, 2) " _
                        '                )

                        '    objDataOperation.ClearParameters()
                        '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("EmpID")))
                        '    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, dRh("reinstatment_date").ToString())

                        '    Dim dsEOC As DataSet = objDataOperation.ExecQuery(strB.ToString(), "List")

                        '    If objDataOperation.ErrorMessage <> "" Then
                        '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '        Throw exForce
                        '    End If

                        '    If dsEOC.Tables(0).Rows.Count > 0 Then

                        '        Dim intSN As Integer = 0
                        '        Dim strPrevEOCTerm As String = ""
                        '        For Each d_r As DataRow In dsEOC.Tables(0).Rows

                        '            intSN += 1
                        '            If intSN = 1 OrElse CInt(d_r("rehiretranunkid")) <= 0 Then

                        '                If IsDBNull(d_r("empl_enddate")) = False OrElse IsDBNull(d_r("empl_enddate")) = False Then

                        '                    If IsDBNull(d_r("empl_enddate")) = False Then
                        '                        strPrevEOCTerm = d_r("empl_enddate").ToString()
                        '                    End If
                        '                    If IsDBNull(d_r("termination_from_date")) = False Then
                        '                        If d_r("termination_from_date").ToString() < strPrevEOCTerm Then
                        '                            strPrevEOCTerm = d_r("termination_from_date").ToString()
                        '                        End If
                        '                    End If

                        '                    If eZeeDate.convertDate(strPrevEOCTerm) >= mdtPeriodStartDate AndAlso eZeeDate.convertDate(strPrevEOCTerm) <= mdtPeriodEndDate Then
                        '                        dtE = eZeeDate.convertDate(strPrevEOCTerm)
                        '                    ElseIf CInt(d_r("rehiretranunkid")) <= 0 Then
                        '                        If intSN >= 2 Then Exit For
                        '                        Continue For
                        '                    End If
                        '                End If

                        '                If CInt(d_r("rehiretranunkid")) > 0 Then
                        '                    dtS = eZeeDate.convertDate(dRh("reinstatment_date").ToString())
                        '                Else
                        '                    dtS = StartDate
                        '                End If

                        '                If dtS <> mdtPeriodStartDate OrElse dtE <> mdtPeriodEndDate Then
                        '                    intEmpTotalDaysInPeriod += DateDiff(DateInterval.Day, dtS, dtE.AddDays(1))
                        '                    iDays = 0

                        '                    'Sohail (03 May 2018) -- Start
                        '                    'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                        '                    'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '                    '                           xUserUnkid, _
                        '                    '                           xYearUnkid, _
                        '                    '                           xCompanyUnkid, _
                        '                    '                           xUserModeSetting, _
                        '                    '                           CInt(dsRow.Item("EmpID").ToString), _
                        '                    '                           dtS, _
                        '                    '                           dtE, _
                        '                    '                           iDays, 0)
                        '                    'Hemant (19 July 2018) -- Start
                        '                    'Support Issue Id # 0002391 -- Wrong hours worked posted on payroll,

                        '                    'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '                    '                           xUserUnkid, _
                        '                    '                           xYearUnkid, _
                        '                    '                           xCompanyUnkid, _
                        '                    '                           xUserModeSetting, _
                        '                    '                           CInt(dsRow.Item("EmpID").ToString), _
                        '                    '                           dtS, _
                        '                    '                           dtE, _
                        '                    '                           iDays, 0, objDataOperation)
                        '                    objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '                                               xUserUnkid, _
                        '                                               xYearUnkid, _
                        '                                               xCompanyUnkid, _
                        '                                               xUserModeSetting, _
                        '                                               CInt(dsRow.Item("EmpID").ToString), _
                        '                                               dtS, _
                        '                                               dtE, _
                        '                                               iDays, dHours, objDataOperation)
                        '                    'Hemant (19 July 2018) -- End
                        '                    'Sohail (03 May 2018) -- End


                        '                    intEmpWorkingDaysInPeriod += iDays
                        '                    'Hemant (19 July 2018) -- Start
                        '                    'Support Issue Id # 0002391 -- Wrong hours worked posted on payroll,
                        '                    dblEmpWorkingHoursInPeriod += dHours
                        '                    'Hemant (19 July 2018) -- End


                        '                End If

                        '            End If

                        '            If intSN >= 2 Then Exit For
                        '        Next

                        '    Else
                        '        dtS = eZeeDate.convertDate(dRh("reinstatment_date").ToString())

                        '        If dtS <> mdtPeriodStartDate OrElse dtE <> mdtPeriodEndDate Then
                        '            intEmpTotalDaysInPeriod += DateDiff(DateInterval.Day, dtS, dtE.AddDays(1))
                        '            iDays = 0

                        '            'Sohail (03 May 2018) -- Start
                        '            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                        '            'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '            '                           xUserUnkid, _
                        '            '                           xYearUnkid, _
                        '            '                           xCompanyUnkid, _
                        '            '                           xUserModeSetting, _
                        '            '                           CInt(dsRow.Item("EmpID").ToString), _
                        '            '                           dtS, _
                        '            '                           dtE, _
                        '            '                           iDays, 0)
                        '            'Hemant (19 July 2018) -- Start
                        '            'Support Issue Id # 0002391 -- Wrong hours worked posted on payroll,

                        '            'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '            '                          xUserUnkid, _
                        '            '                          xYearUnkid, _
                        '            '                          xCompanyUnkid, _
                        '            '                          xUserModeSetting, _
                        '            '                          CInt(dsRow.Item("EmpID").ToString), _
                        '            '                          dtS, _
                        '            '                          dtE, _
                        '            '                          iDays, 0, objDataOperation)
                        '            objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '                                      xUserUnkid, _
                        '                                      xYearUnkid, _
                        '                                      xCompanyUnkid, _
                        '                                      xUserModeSetting, _
                        '                                      CInt(dsRow.Item("EmpID").ToString), _
                        '                                      dtS, _
                        '                                      dtE, _
                        '                                      iDays, dHours, objDataOperation)
                        '            'Hemant (19 July 2018) -- End
                        '            'Sohail (03 May 2018) -- End

                        '            intEmpWorkingDaysInPeriod += iDays

                        '            'Hemant (19 July 2018) -- Start
                        '            'Support Issue Id # 0002391 -- Wrong hours worked posted on payroll,
                        '            dblEmpWorkingHoursInPeriod += dHours
                        '            'Hemant (19 July 2018) -- End

                        '        End If
                        '    End If

                        'Next
                            strB.Length = 0
                        strB.Append("SELECT datestranunkid " & _
                                         ", 0 AS rehiretranunkid " & _
                                         ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                         ", CONVERT(CHAR(8), CASE WHEN hremployee_dates_tran.date1 < ISNULL(hremployee_dates_tran.date2, '99990101') THEN hremployee_dates_tran.date1 ELSE ISNULL(hremployee_dates_tran.date2, '99990101') END, 112) AS date1 " & _
                                    "FROM hremployee_dates_tran " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND employeeunkid = @employeeunkid " & _
                                          "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                                          "AND isexclude_payroll = 0 " & _
                                          "AND date1 IS NOT NULL " & _
                                          "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                          "AND CONVERT(CHAR(8), CASE WHEN hremployee_dates_tran.date1 < ISNULL(hremployee_dates_tran.date2, '99990101') THEN hremployee_dates_tran.date1 ELSE ISNULL(hremployee_dates_tran.date2, '99990101') END, 112) " & _
                                          "BETWEEN @start_date AND @end_date " & _
                                    "UNION " & _
                                    "SELECT datestranunkid " & _
                                         ", 0 AS rehiretranunkid " & _
                                         ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                         ", CONVERT(CHAR(8), CASE WHEN hremployee_dates_tran.date2 < ISNULL(hremployee_dates_tran.date1, '99990101') THEN hremployee_dates_tran.date2 ELSE ISNULL(hremployee_dates_tran.date1, '99990101') END, 112) AS date2 " & _
                                    "FROM hremployee_dates_tran " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND employeeunkid = @employeeunkid " & _
                                          "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                                          "AND isexclude_payroll = 0 " & _
                                          "AND date2 IS NOT NULL " & _
                                          "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                          "AND CONVERT(CHAR(8), CASE WHEN hremployee_dates_tran.date2 < ISNULL(hremployee_dates_tran.date1, '99990101') THEN hremployee_dates_tran.date2 ELSE ISNULL(hremployee_dates_tran.date1, '99990101') END, 112) " & _
                                          "BETWEEN @start_date AND @end_date ")
                        'Sohail (25 Feb 2022) - [date1=CASE WHEN hremployee_dates_tran.date1 < ISNULL(hremployee_dates_tran.date2, '99990101') THEN hremployee_dates_tran.date1 ELSE ISNULL(hremployee_dates_tran.date2, '99990101')] and date2 :: NMB Issue - System was picking employee previous termination date when employee is rehired.
                        'Sohail (07 Jan 2020)  - [AND isexclude_payroll = 0]

                        If mblnExcludeEmpFromPayrollDuringSuspension = True Then 'Sohail (09 Oct 2019)
                            strB.Append("UNION " & _
                                    "SELECT datestranunkid " & _
                                         ", 0 AS rehiretranunkid " & _
                                         ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                         ", CONVERT(CHAR(8), date1 - 1, 112) AS date1 " & _
                                    "FROM hremployee_dates_tran " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND employeeunkid = @employeeunkid " & _
                                          "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                                          "AND date1 IS NOT NULL " & _
                                          "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                          "AND CONVERT(CHAR(8), date1, 112) " & _
                                          "BETWEEN @start_date AND @end_date " & _
                                    "UNION " & _
                                    "SELECT 0 AS datestranunkid " & _
                                         ", datestranunkid AS rehiretranunkid " & _
                                         ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                         ", CONVERT(CHAR(8), date2 + 1, 112) AS date2 " & _
                                    "FROM hremployee_dates_tran " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND employeeunkid = @employeeunkid " & _
                                          "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                                          "AND date2 IS NOT NULL " & _
                                          "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                          "AND CONVERT(CHAR(8), date2, 112) " & _
                                              "BETWEEN @start_date AND @end_date ")
                        End If 'Sohail (09 Oct 2019)

                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : system will prorate salary and flat rate heads for employee exemption days for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        '*** Employee Exemption
                        strB.Append("UNION " & _
                                        "SELECT datestranunkid " & _
                                             ", 0 AS rehiretranunkid " & _
                                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                             ", CONVERT(CHAR(8), date1 - 1, 112) AS date1 " & _
                                        "FROM hremployee_dates_tran " & _
                                        "WHERE isvoid = 0 " & _
                                              "AND employeeunkid = @employeeunkid " & _
                                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                                              "AND date1 IS NOT NULL " & _
                                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                              "AND CONVERT(CHAR(8), date1, 112) " & _
                                              "BETWEEN @start_date AND @end_date " & _
                                        "UNION " & _
                                        "SELECT 0 AS datestranunkid " & _
                                             ", datestranunkid AS rehiretranunkid " & _
                                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                             ", CONVERT(CHAR(8), date2 + 1, 112) AS date2 " & _
                                        "FROM hremployee_dates_tran " & _
                                        "WHERE isvoid = 0 " & _
                                              "AND employeeunkid = @employeeunkid " & _
                                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                                              "AND date2 IS NOT NULL " & _
                                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                              "AND CONVERT(CHAR(8), date2, 112) " & _
                                              "BETWEEN @start_date AND @end_date " _
                                              )
                        'Sohail (21 Oct 2019) -- End

                        strB.Append("UNION " & _
                                    "SELECT 0 AS datestranunkid " & _
                                         ", rehiretranunkid " & _
                                         ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                         ", CONVERT(CHAR(8), reinstatment_date, 112) AS reinstatment_date " & _
                                    "FROM hremployee_rehire_tran " & _
                                            "WHERE isvoid = 0 " & _
                                          "AND employeeunkid = @employeeunkid " & _
                                          "AND reinstatment_date IS NOT NULL " & _
                                          "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                          "AND CONVERT(CHAR(8), reinstatment_date, 112) " & _
                                          "BETWEEN @start_date AND @end_date " & _
                                    "ORDER BY date1, rehiretranunkid ")
                        'Sohail (08 Aug 2019) -- Start
                        'NMB Payroll UAT # TC002 - 76.1 - System to prorate and remove allowance during suspension.
                        '[datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & "], [ORDER BY date1 = ORDER BY date1, rehiretranunkid]
                        '*** Remove /*, */ to enable suspension proration 
                        'Sohail (08 Aug 2019) -- End

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("EmpID")))
                        objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                        objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

                        'Sohail (07 Nov 2019) -- Start
                        'NMB Enhancement # : Flatrate prorating issue on employee exemption.
                        'Dim dsEOC As DataSet = objDataOperation.ExecQuery(strB.ToString(), "List")
                        dsEOC = objDataOperation.ExecQuery(strB.ToString(), "List")
                        'Sohail (07 Nov 2019) -- End

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dsEOC.Tables(0).Rows.Count > 0 Then

                            Dim intTotRows As Integer = dsEOC.Tables(0).Rows.Count - 1
                            Dim intRowIdx As Integer = -1
                            Dim blnCalcDone As Boolean = False

                            For Each dRow As DataRow In dsEOC.Tables(0).Rows
                                intRowIdx += 1

                                If CInt(dRow.Item("datestranunkid")) > 0 Then 'EOC / Leaving Date

                                    If intRowIdx < intTotRows Then 'If Next Row is Exist
                                        If CInt(dsEOC.Tables(0).Rows(intRowIdx + 1).Item("datestranunkid")) > 0 Then 'If Next Rows is also EOC / Leaving Date then Ignore Current Row
                                                Continue For
                                            End If
                                        End If

                                    dtE = eZeeDate.convertDate(dRow("date1").ToString())

                                        If dtS <> mdtPeriodStartDate OrElse dtE <> mdtPeriodEndDate Then
                                            intEmpTotalDaysInPeriod += DateDiff(DateInterval.Day, dtS, dtE.AddDays(1))
                                            iDays = 0
                                        dHours = 0

                                            objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                                       xUserUnkid, _
                                                                       xYearUnkid, _
                                                                       xCompanyUnkid, _
                                                                       xUserModeSetting, _
                                                                       CInt(dsRow.Item("EmpID").ToString), _
                                                                       dtS, _
                                                                       dtE, _
                                                                   iDays, dHours, objDataOperation, False, "")
                                        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]

                                            intEmpWorkingDaysInPeriod += iDays
                                            dblEmpWorkingHoursInPeriod += dHours

                                        blnCalcDone = True

                                        'Sohail (19 Dec 2018) -- Start
                                        'NMB Enhancement - On Salary change screen. Allow salary proration for the new salary i.e for full basic salary, take old salary untill the effective date of the new salary + new salary from the effective date in 76.1.
                                        If drMultiSalary.Length > 0 Then
                                            Dim dtIncr As Date
                                            Dim dtSSalary As Date
                                            Dim dtESalary As Date
                                            Dim dtPrevIncr As Date
                                            Dim i_Days As Integer = 0

                                            dtPrevIncr = dtE

                                            For Each dr_MS In drMultiSalaryDESC
                                                dtSSalary = dtS
                                                dtESalary = dtE

                                                dtIncr = eZeeDate.convertDate(dr_MS.Item("incrementdate").ToString)

                                                If dtSSalary <= dtIncr Then
                                                    dtSSalary = dtIncr
                                                End If
                                                If dtESalary > dtPrevIncr Then
                                                    dtESalary = dtPrevIncr
                                                End If
                                                If dtIncr > dtE OrElse (dtSSalary >= dtESalary AndAlso dr_MS.Item("WeekendIncludedDays") > 0) Then
                                                    dtPrevIncr = dtIncr
                                                    Continue For
                                                End If
                                                If dtESalary <> dtE Then
                                                    dtESalary = dtESalary.AddDays(-1)
                                                End If

                                                dr_MS.Item("WeekendIncludedDays") += DateDiff(DateInterval.Day, dtSSalary, dtESalary.AddDays(1))

                                                i_Days = 0
                                                objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                                           xUserUnkid, _
                                                                           xYearUnkid, _
                                                                           xCompanyUnkid, _
                                                                           xUserModeSetting, _
                                                                           CInt(dsRow.Item("EmpID").ToString), _
                                                                           dtSSalary, _
                                                                           dtESalary, _
                                                                          i_Days, 0, objDataOperation, False, "")
                                                'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]

                                                dr_MS.Item("WeekendExcludedDays") += i_Days

                                                dtPrevIncr = dtIncr
                                            Next
                                        End If
                                        'Sohail (19 Dec 2018) -- End

                                    End If

                                Else 'Rehire Date

                                    If intRowIdx < intTotRows Then 'If Next Row is Exist
                                        If CInt(dsEOC.Tables(0).Rows(intRowIdx + 1).Item("rehiretranunkid")) > 0 Then 'If Next Rows is also Rehire Date then Ignore Current Row
                                            Continue For
                                        End If
                                    End If

                                    dtS = eZeeDate.convertDate(dRow("date1").ToString())

                                    blnCalcDone = False
                                    End If
                                Next

                            If blnCalcDone = False Then
                                dtE = EndDate

                                    intEmpTotalDaysInPeriod += DateDiff(DateInterval.Day, dtS, dtE.AddDays(1))
                                    iDays = 0
                                dHours = 0

                                    objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                               xUserUnkid, _
                                                               xYearUnkid, _
                                                               xCompanyUnkid, _
                                                               xUserModeSetting, _
                                                               CInt(dsRow.Item("EmpID").ToString), _
                                                               dtS, _
                                                               dtE, _
                                                          iDays, dHours, objDataOperation, False, "")
                                'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]

                                    intEmpWorkingDaysInPeriod += iDays
                                    dblEmpWorkingHoursInPeriod += dHours

                                'Sohail (19 Dec 2018) -- Start
                                'NMB Enhancement - On Salary change screen. Allow salary proration for the new salary i.e for full basic salary, take old salary untill the effective date of the new salary + new salary from the effective date in 76.1.
                                If drMultiSalary.Length > 0 Then
                                    Dim dtIncr As Date
                                    Dim dtSSalary As Date
                                    Dim dtESalary As Date
                                    Dim dtPrevIncr As Date
                                    Dim i_Days As Integer = 0

                                    dtPrevIncr = dtE

                                    For Each dr_MS In drMultiSalaryDESC
                                        dtSSalary = dtS
                                        dtESalary = dtE

                                        dtIncr = eZeeDate.convertDate(dr_MS.Item("incrementdate").ToString)

                                        If dtSSalary <= dtIncr Then
                                            dtSSalary = dtIncr
                                        End If
                                        If dtESalary > dtPrevIncr Then
                                            dtESalary = dtPrevIncr
                                        End If
                                        If dtIncr > dtE OrElse (dtSSalary >= dtESalary AndAlso dr_MS.Item("WeekendIncludedDays") > 0) Then
                                            dtPrevIncr = dtIncr
                                            Continue For
                                        End If
                                        If dtESalary <> dtE Then
                                            dtESalary = dtESalary.AddDays(-1)
                                        End If

                                        dr_MS.Item("WeekendIncludedDays") += DateDiff(DateInterval.Day, dtSSalary, dtESalary.AddDays(1))

                                        i_Days = 0
                                        objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                                   xUserUnkid, _
                                                                   xYearUnkid, _
                                                                   xCompanyUnkid, _
                                                                   xUserModeSetting, _
                                                                   CInt(dsRow.Item("EmpID").ToString), _
                                                                   dtSSalary, _
                                                                   dtESalary, _
                                                                  i_Days, 0, objDataOperation, False, "")
                                        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]

                                        dr_MS.Item("WeekendExcludedDays") += i_Days

                                        dtPrevIncr = dtIncr
                                    Next
                                End If
                                'Sohail (19 Dec 2018) -- End
                            End If

                                End If
                        'Sohail (18 Dec 2018) -- End

                    Else
                        intEmpTotalDaysInPeriod = DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1))

                        'Sohail (30 Nov 2017) -- Start
                        'Issue - ABOOD GROUP#1670: Payable days showing zero for people who are appointed mid-month. in 70.1.
                        'Sohail (03 May 2018) -- Start
                        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                        'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '                                       xUserUnkid, _
                        '                                       xYearUnkid, _
                        '                                       xCompanyUnkid, _
                        '                                       xUserModeSetting, _
                        '                                       CInt(dsRow.Item("EmpID").ToString), _
                        '                                       StartDate, _
                        '                                       EndDate, _
                        '                                       intEmpWorkingDaysInPeriod, 0)
                        'Hemant (19 July 2018) -- Start
                        'Support Issue Id # 0002391 -- Wrong hours worked posted on payroll,

                        'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '                                       xUserUnkid, _
                        '                                       xYearUnkid, _
                        '                                       xCompanyUnkid, _
                        '                                       xUserModeSetting, _
                        '                                       CInt(dsRow.Item("EmpID").ToString), _
                        '                                       StartDate, _
                        '                                       EndDate, _
                        '                                       intEmpWorkingDaysInPeriod, 0, objDataOperation)
                        objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                               xUserUnkid, _
                                                               xYearUnkid, _
                                                               xCompanyUnkid, _
                                                               xUserModeSetting, _
                                                               CInt(dsRow.Item("EmpID").ToString), _
                                                               StartDate, _
                                                               EndDate, _
                                                              intEmpWorkingDaysInPeriod, dblEmpWorkingHoursInPeriod, objDataOperation, False, "")
                        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]
                        'dblEmpWorkingHoursInPeriod += dHours
                        'Hemant (19 July 2018) -- End
                        'Sohail (03 May 2018) -- End
                        'Sohail (30 Nov 2017) -- End

                        'Sohail (19 Dec 2018) -- Start
                        'NMB Enhancement - On Salary change screen. Allow salary proration for the new salary i.e for full basic salary, take old salary untill the effective date of the new salary + new salary from the effective date in 76.1.
                        If drMultiSalary.Length > 0 Then
                            Dim intTotRows As Integer = drMultiSalary.Length - 1
                            Dim intRowIdx As Integer = -1

                            Dim dtIncr As Date
                            Dim i_Days As Integer = 0

                            For Each dr_MS In drMultiSalary
                                intRowIdx += 1

                                dtIncr = eZeeDate.convertDate(dr_MS.Item("incrementdate").ToString)

                                If intRowIdx < intTotRows Then 'If Next Row is Exist
                                    'If CDec(dr_MS.Item("newscale")) = CDec(drMultiSalary(intRowIdx + 1).Item("newscale")) Then 'If current salary and next salary is same then ignore
                                    '    Continue For
                                    'Else
                                    '    dtIncr = eZeeDate.convertDate(dr_MS.Item("incrementdate").ToString)
                                    'End If

                                    If StartDate > dtIncr Then
                                        dr_MS.Item("WeekendIncludedDays") = DateDiff(DateInterval.Day, StartDate, eZeeDate.convertDate(drMultiSalary(intRowIdx + 1).Item("incrementdate").ToString))

                                        i_Days = 0
                                        objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                                   xUserUnkid, _
                                                                   xYearUnkid, _
                                                                   xCompanyUnkid, _
                                                                   xUserModeSetting, _
                                                                   CInt(dsRow.Item("EmpID").ToString), _
                                                                   StartDate, _
                                                                   eZeeDate.convertDate(drMultiSalary(intRowIdx + 1).Item("incrementdate").ToString).AddDays(-1), _
                                                                  i_Days, 0, objDataOperation, False, "")
                                        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]

                                        dr_MS.Item("WeekendExcludedDays") = i_Days
                                    Else
                                        dr_MS.Item("WeekendIncludedDays") = DateDiff(DateInterval.Day, dtIncr, eZeeDate.convertDate(drMultiSalary(intRowIdx + 1).Item("incrementdate").ToString))

                                        i_Days = 0
                                        objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                                   xUserUnkid, _
                                                                   xYearUnkid, _
                                                                   xCompanyUnkid, _
                                                                   xUserModeSetting, _
                                                                   CInt(dsRow.Item("EmpID").ToString), _
                                                                   dtIncr, _
                                                                   eZeeDate.convertDate(drMultiSalary(intRowIdx + 1).Item("incrementdate").ToString).AddDays(-1), _
                                                                  i_Days, 0, objDataOperation, False, "")
                                        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]

                                        dr_MS.Item("WeekendExcludedDays") = i_Days
                                    End If
                                Else
                                    dr_MS.Item("WeekendIncludedDays") = DateDiff(DateInterval.Day, dtIncr, EndDate.AddDays(1))

                                    i_Days = 0
                                    objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                               xUserUnkid, _
                                                               xYearUnkid, _
                                                               xCompanyUnkid, _
                                                               xUserModeSetting, _
                                                               CInt(dsRow.Item("EmpID").ToString), _
                                                               dtIncr, _
                                                               EndDate, _
                                                              i_Days, 0, objDataOperation, False, "")
                                    'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]

                                    dr_MS.Item("WeekendExcludedDays") = i_Days
                                End If

                                dsMultiSalary.Tables(0).AcceptChanges()
                            Next
                        End If
                        'Sohail (19 Dec 2018) -- End

                    End If

                    'Sohail (11 Nov 2019) -- Start
                    'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                    intEmpWorkingDaysInTnAPeriod = 0
                    dblEmpWorkingHoursInTnAPeriod = 0
                    'Sohail (11 Nov 2019) -- End
                    If mdtTnA_StartDate <> mdtPeriodStartDate OrElse mdtTnA_EndDate <> mdtPeriodEndDate Then
                        Dim dtSTnA As Date = TnAStartDate
                        Dim dtETnA As Date = TnAEndDate
                        intEmpTotalDaysInTnAPeriod = 0
                        Dim drRehireTnA() As DataRow = dsRehireTnA.Tables(0).Select("TEEmpID = " & CInt(.Item("EmpID")) & " ")
                        If drRehireTnA.Length > 0 Then

                            'Sohail (18 Dec 2018) -- Start
                            'Internal Issue - Wrong salary amount was coming when rehired is done more than one time in a period in 76.1.
                            'For Each dRh As DataRow In drRehireTnA

                            '    strB.Length = 0
                            '    strB.Append("SELECT A.TEEmpId " & _
                            '                     ", A.empl_enddate " & _
                            '                     ", A.termination_from_date " & _
                            '                     ", A.TEfDt " & _
                            '                     ", A.TR_REASON " & _
                            '                     ", A.changereasonunkid " & _
                            '                     ", A.rehiretranunkid " & _
                            '                "FROM " & _
                            '                "( " & _
                            '                    "SELECT TRM.employeeunkid AS TEEmpId " & _
                            '                         ", CONVERT(CHAR(8), TRM.date1, 112) AS empl_enddate " & _
                            '                         ", CONVERT(CHAR(8), TRM.date2, 112) AS termination_from_date " & _
                            '                         ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                            '                         ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                            '                         ", CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name, '') ELSE ISNULL(TEC.name, '') END AS TR_REASON " & _
                            '                         ", TRM.changereasonunkid " & _
                            '                         ", ISNULL(TRM.rehiretranunkid, 0) AS rehiretranunkid " & _
                            '                    "FROM hremployee_dates_tran AS TRM " & _
                            '                        "LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid " & _
                            '                               "AND RTE.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & " " & _
                            '                        "LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid " & _
                            '                               "AND TEC.mastertype = " & clsCommon_Master.enCommonMaster.TERMINATION & " " & _
                            '                    "WHERE isvoid = 0 " & _
                            '                          "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                            '                          "AND TRM.employeeunkid = @employeeunkid " & _
                            '                          "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @end_date " & _
                            '                ") AS A " & _
                            '                "WHERE A.Rno IN (1, 2) " _
                            '                )

                            '    objDataOperation.ClearParameters()
                            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("EmpID")))
                            '    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, dRh("reinstatment_date").ToString())

                            '    Dim dsEOC As DataSet = objDataOperation.ExecQuery(strB.ToString(), "List")

                            '    If objDataOperation.ErrorMessage <> "" Then
                            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            '        Throw exForce
                            '    End If

                            '    If dsEOC.Tables(0).Rows.Count > 0 Then

                            '        Dim intSN As Integer = 0
                            '        Dim strPrevEOCTerm As String = ""
                            '        For Each d_r As DataRow In dsEOC.Tables(0).Rows

                            '            intSN += 1
                            '            If intSN = 1 OrElse CInt(d_r("rehiretranunkid")) <= 0 Then

                            '                If IsDBNull(d_r("empl_enddate")) = False OrElse IsDBNull(d_r("empl_enddate")) = False Then

                            '                    If IsDBNull(d_r("empl_enddate")) = False Then
                            '                        strPrevEOCTerm = d_r("empl_enddate").ToString()
                            '                    End If
                            '                    If IsDBNull(d_r("termination_from_date")) = False Then
                            '                        If d_r("termination_from_date").ToString() < strPrevEOCTerm Then
                            '                            strPrevEOCTerm = d_r("termination_from_date").ToString()
                            '                        End If
                            '                    End If

                            '                    If eZeeDate.convertDate(strPrevEOCTerm) >= mdtTnA_StartDate AndAlso eZeeDate.convertDate(strPrevEOCTerm) <= mdtTnA_EndDate Then
                            '                        dtETnA = eZeeDate.convertDate(strPrevEOCTerm)
                            '                    ElseIf CInt(d_r("rehiretranunkid")) <= 0 Then
                            '                        If intSN >= 2 Then Exit For
                            '                        Continue For
                            '                    End If
                            '                End If

                            '                If CInt(d_r("rehiretranunkid")) > 0 Then
                            '                    dtSTnA = eZeeDate.convertDate(dRh("reinstatment_date").ToString())
                            '                Else
                            '                    dtSTnA = TnAStartDate
                            '                End If

                            '                If dtSTnA <> mdtTnA_StartDate OrElse dtETnA <> mdtTnA_EndDate Then
                            '                    intEmpTotalDaysInTnAPeriod += DateDiff(DateInterval.Day, dtSTnA, dtETnA.AddDays(1))

                            '                End If

                            '            End If

                            '            If intSN >= 2 Then Exit For
                            '        Next

                            '    Else
                            '        dtSTnA = eZeeDate.convertDate(dRh("reinstatment_date").ToString())

                            '        If dtSTnA <> mdtTnA_StartDate OrElse dtETnA <> mdtTnA_EndDate Then
                            '            intEmpTotalDaysInTnAPeriod += DateDiff(DateInterval.Day, dtSTnA, dtETnA.AddDays(1))

                            '        End If
                            '    End If

                            'Next
                                strB.Length = 0
                            strB.Append("SELECT datestranunkid " & _
                                             ", 0 AS rehiretranunkid " & _
                                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                             ", CONVERT(CHAR(8), date1, 112) AS date1 " & _
                                        "FROM hremployee_dates_tran " & _
                                        "WHERE isvoid = 0 " & _
                                              "AND employeeunkid = @employeeunkid " & _
                                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                                              "AND date1 IS NOT NULL " & _
                                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                              "AND CONVERT(CHAR(8), date1, 112) " & _
                                              "BETWEEN @start_date AND @end_date " & _
                                        "UNION " & _
                                        "SELECT datestranunkid " & _
                                             ", 0 AS rehiretranunkid " & _
                                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                             ", CONVERT(CHAR(8), date2, 112) AS date2 " & _
                                        "FROM hremployee_dates_tran " & _
                                        "WHERE isvoid = 0 " & _
                                              "AND employeeunkid = @employeeunkid " & _
                                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                                              "AND date2 IS NOT NULL " & _
                                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                              "AND CONVERT(CHAR(8), date2, 112) " & _
                                              "BETWEEN @start_date AND @end_date " _
                                              )

                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : system will prorate salary and flat rate heads for employee exemption days for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            If mblnExcludeEmpFromPayrollDuringSuspension = True Then
                                strB.Append("UNION " & _
                                            "SELECT datestranunkid " & _
                                                 ", 0 AS rehiretranunkid " & _
                                                 ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                                 ", CONVERT(CHAR(8), date1 - 1, 112) AS date1 " & _
                                            "FROM hremployee_dates_tran " & _
                                            "WHERE isvoid = 0 " & _
                                                  "AND employeeunkid = @employeeunkid " & _
                                                  "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                                                  "AND date1 IS NOT NULL " & _
                                                  "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                                  "AND CONVERT(CHAR(8), date1, 112) " & _
                                                  "BETWEEN @start_date AND @end_date " & _
                                            "UNION " & _
                                            "SELECT 0 AS datestranunkid " & _
                                                 ", datestranunkid AS rehiretranunkid " & _
                                                 ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                                 ", CONVERT(CHAR(8), date2 + 1, 112) AS date2 " & _
                                            "FROM hremployee_dates_tran " & _
                                            "WHERE isvoid = 0 " & _
                                                  "AND employeeunkid = @employeeunkid " & _
                                                  "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                                                  "AND date2 IS NOT NULL " & _
                                                  "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                                  "AND CONVERT(CHAR(8), date2, 112) " & _
                                                  "BETWEEN @start_date AND @end_date ")
                            End If

                            '*** Employee Exemption
                            strB.Append("UNION " & _
                                            "SELECT datestranunkid " & _
                                                 ", 0 AS rehiretranunkid " & _
                                                 ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                                 ", CONVERT(CHAR(8), date1 - 1, 112) AS date1 " & _
                                            "FROM hremployee_dates_tran " & _
                                            "WHERE isvoid = 0 " & _
                                                  "AND employeeunkid = @employeeunkid " & _
                                                  "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                                                  "AND date1 IS NOT NULL " & _
                                                  "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                                  "AND CONVERT(CHAR(8), date1, 112) " & _
                                              "BETWEEN @start_date AND @end_date " & _
                                        "UNION " & _
                                        "SELECT 0 AS datestranunkid " & _
                                                 ", datestranunkid AS rehiretranunkid " & _
                                                 ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                                 ", CONVERT(CHAR(8), date2 + 1, 112) AS date2 " & _
                                            "FROM hremployee_dates_tran " & _
                                            "WHERE isvoid = 0 " & _
                                                  "AND employeeunkid = @employeeunkid " & _
                                                  "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                                                  "AND date2 IS NOT NULL " & _
                                                  "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                                  "AND CONVERT(CHAR(8), date2, 112) " & _
                                                  "BETWEEN @start_date AND @end_date " _
                                                  )
                            'Sohail (21 Oct 2019) -- End

                            strB.Append("UNION " & _
                                        "SELECT 0 AS datestranunkid " & _
                                             ", rehiretranunkid " & _
                                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                             ", CONVERT(CHAR(8), reinstatment_date, 112) AS reinstatment_date " & _
                                        "FROM hremployee_rehire_tran " & _
                                                "WHERE isvoid = 0 " & _
                                              "AND employeeunkid = @employeeunkid " & _
                                              "AND reinstatment_date IS NOT NULL " & _
                                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @end_date " & _
                                              "AND CONVERT(CHAR(8), reinstatment_date, 112) " & _
                                              "BETWEEN @start_date AND @end_date " & _
                                        "ORDER BY date1 ")

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("EmpID")))
                            objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTnA_StartDate))
                            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTnA_EndDate))

                            Dim dsEOCTnA As DataSet = objDataOperation.ExecQuery(strB.ToString(), "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            If dsEOCTnA.Tables(0).Rows.Count > 0 Then

                                Dim intTotRows As Integer = dsEOCTnA.Tables(0).Rows.Count - 1
                                Dim intRowIdx As Integer = -1
                                Dim blnCalcDone As Boolean = False

                                For Each dRow As DataRow In dsEOCTnA.Tables(0).Rows
                                    intRowIdx += 1 'Sohail (21 Oct 2019)

                                    If CInt(dRow.Item("datestranunkid")) > 0 Then 'EOC / Leaving Date

                                        If intRowIdx < intTotRows Then 'If Next Row is Exist
                                            If CInt(dsEOCTnA.Tables(0).Rows(intRowIdx + 1).Item("datestranunkid")) > 0 Then 'If Next Rows is also EOC / Leaving Date then Ignore Current Row
                                                Continue For
                                                End If
                                                    End If

                                        dtETnA = eZeeDate.convertDate(dRow("date1").ToString())

                                        If dtSTnA <> mdtTnA_StartDate OrElse dtETnA <> mdtTnA_EndDate Then
                                            'Sohail (09 Oct 2019) -- Start
                                            'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
                                            'intEmpTotalDaysInPeriod += DateDiff(DateInterval.Day, dtSTnA, dtETnA.AddDays(1))
                                            intEmpTotalDaysInTnAPeriod += DateDiff(DateInterval.Day, dtSTnA, dtETnA.AddDays(1))
                                            'Sohail (09 Oct 2019) -- End
                                            iDays = 0
                                            dHours = 0

                                            objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                                       xUserUnkid, _
                                                                       xYearUnkid, _
                                                                       xCompanyUnkid, _
                                                                       xUserModeSetting, _
                                                                       CInt(dsRow.Item("EmpID").ToString), _
                                                                       dtSTnA, _
                                                                       dtETnA, _
                                                                       iDays, dHours, objDataOperation, False, "")
                                            'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]

                                            'Sohail (21 Oct 2019) -- Start
                                            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                                            'intEmpWorkingDaysInPeriod += iDays
                                            'dblEmpWorkingHoursInPeriod += dHours
                                            'Sohail (21 Oct 2019) -- End
                                            'Sohail (11 Nov 2019) -- Start
                                            'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                                            intEmpWorkingDaysInTnAPeriod += iDays
                                            dblEmpWorkingHoursInTnAPeriod += dHours
                                            'Sohail (11 Nov 2019) -- End

                                            blnCalcDone = True
                                                End If

                                    Else 'Rehire Date

                                        If intRowIdx < intTotRows Then 'If Next Row is Exist
                                            If CInt(dsEOCTnA.Tables(0).Rows(intRowIdx + 1).Item("rehiretranunkid")) > 0 Then 'If Next Rows is also Rehire Date then Ignore Current Row
                                                    Continue For
                                                End If
                                            End If

                                        dtSTnA = eZeeDate.convertDate(dRow("date1").ToString())

                                        blnCalcDone = False
                                            End If
                                Next

                                If blnCalcDone = False Then
                                    dtETnA = TnAEndDate

                                    'Sohail (09 Oct 2019) -- Start
                                    'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
                                    'intEmpTotalDaysInPeriod += DateDiff(DateInterval.Day, dtSTnA, dtETnA.AddDays(1))
                                    intEmpTotalDaysInTnAPeriod += DateDiff(DateInterval.Day, dtSTnA, dtETnA.AddDays(1))
                                    'Sohail (09 Oct 2019) -- End
                                    iDays = 0
                                    dHours = 0

                                    objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                               xUserUnkid, _
                                                               xYearUnkid, _
                                                               xCompanyUnkid, _
                                                               xUserModeSetting, _
                                                               CInt(dsRow.Item("EmpID").ToString), _
                                                               dtSTnA, _
                                                               dtETnA, _
                                                               iDays, dHours, objDataOperation, False, "")
                                    'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]

                                    'Sohail (21 Oct 2019) -- Start
                                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                                    'intEmpWorkingDaysInPeriod += iDays
                                    'dblEmpWorkingHoursInPeriod += dHours
                                    'Sohail (21 Oct 2019) -- End
                                    'Sohail (11 Nov 2019) -- Start
                                    'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                                    intEmpWorkingDaysInTnAPeriod += iDays
                                    dblEmpWorkingHoursInTnAPeriod += dHours
                                    'Sohail (11 Nov 2019) -- End
                                    End If

                                End If
                            'Sohail (18 Dec 2018) -- End

                        Else
                            intEmpTotalDaysInTnAPeriod = DateDiff(DateInterval.Day, TnAStartDate, TnAEndDate.AddDays(1))

                            'Sohail (11 Nov 2019) -- Start
                            'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                            objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                               xUserUnkid, _
                                                               xYearUnkid, _
                                                               xCompanyUnkid, _
                                                               xUserModeSetting, _
                                                               CInt(dsRow.Item("EmpID").ToString), _
                                                               TnAStartDate, _
                                                               TnAEndDate, _
                                                              intEmpWorkingDaysInTnAPeriod, dblEmpWorkingHoursInTnAPeriod, objDataOperation, False, "")
                            'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]
                            'Sohail (11 Nov 2019) -- End

                        End If
                        'Sohail (30 Nov 2017) -- Start
                        'Issue - ABOOD GROUP#1670: Payable days showing zero for people who are appointed mid-month. in 70.1.
                    Else
                        'Sohail (18 Dec 2018) -- Start
                        'Internal Issue - Wrong salary amount was coming when rehired is done more than one time in a period in 76.1.
                        'intEmpTotalDaysInTnAPeriod = DateDiff(DateInterval.Day, TnAStartDate, TnAEndDate.AddDays(1))
                        intEmpTotalDaysInTnAPeriod = intEmpTotalDaysInPeriod
                        'Sohail (11 Nov 2019) -- Start
                        'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                        intEmpWorkingDaysInTnAPeriod = intEmpWorkingDaysInPeriod
                        dblEmpWorkingHoursInTnAPeriod = dblEmpWorkingHoursInPeriod
                        'Sohail (11 Nov 2019) -- End
                        'Sohail (18 Dec 2018) -- End
                        'Sohail (30 Nov 2017) -- End
                    End If
                    'Sohail (28 Oct 2017) -- End

                    'Sohail (21 Feb 2022) -- Start
                    'Issue :  : NMB - Mid month rehired employee was getting old job benefits.
                    If dsEOC IsNot Nothing AndAlso dsEOC.Tables(0).Rows.Count > 0 Then
                        If CInt(dsEOC.Tables(0).Rows(0).Item("rehiretranunkid")) > 0 Then
                            StartDate = eZeeDate.convertDate(dsEOC.Tables(0).Rows(0).Item("date1").ToString)
                        End If
                    End If
                    'Sohail (21 Feb 2022) -- End

                    'Sohail (28 Oct 2017) -- Start
                    'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
                    'If StartDate <> mdtPeriodStartDate OrElse EndDate <> mdtPeriodEndDate Then
                    'Sohail (19 Dec 2018) -- Start
                    'NMB Enhancement - On Salary change screen. Allow salary proration for the new salary i.e for full basic salary, take old salary untill the effective date of the new salary + new salary from the effective date in 76.1.
                    If dtS <> mdtPeriodStartDate OrElse dtE <> mdtPeriodEndDate OrElse drMultiSalary.Length > 0 Then
                        'Sohail (19 Dec 2018) -- End
                        'Sohail (28 Oct 2017) -- End
                        'Sohail (28 Oct 2017) -- Start
                        'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
                        'intEmpTotalDaysInPeriod = DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1)) 'Sohail (24 Dec 2013)
                        'Sohail (28 Oct 2017) -- End
                        'Sohail (29 Oct 2012) -- Start
                        'TRA - ENHANCEMENT
                        'intEmpWorkingDaysInPeriod = GetWorkingDays(CInt(dsRow.Item("EmpID").ToString), StartDate, EndDate, intShiftId, strShiftDays)
                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        'intEmpWorkingDaysInPeriod = GetWorkingDays(CInt(dsRow.Item("EmpID").ToString), StartDate, EndDate, intShiftId)

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                        'objEmpShift.GetWorkingDaysAndHours(CInt(dsRow.Item("EmpID").ToString), StartDate, EndDate, intEmpWorkingDaysInPeriod, 0)
                        'Sohail (28 Oct 2017) -- Start
                        'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
                        'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '                                   xUserUnkid, _
                        '                                   xYearUnkid, _
                        '                                   xCompanyUnkid, _
                        '                                   xUserModeSetting, _
                        '                                   CInt(dsRow.Item("EmpID").ToString), _
                        '                                   StartDate, _
                        '                                   EndDate, _
                        '                                   intEmpWorkingDaysInPeriod, 0)
                        'Sohail (28 Oct 2017) -- End
                        'Shani(24-Aug-2015) -- End

                        'Sohail (24 Sep 2013) -- End
                        'Sohail (29 Oct 2012) -- End
                        If drMultiSalary.Length <= 0 Then 'Sohail (19 Dec 2018) -- End
                        If intBasicSalaryComputation = enBasicSalaryComputation.WITH_WEEKENDS_EXCLUDED Then 'Sohail (17 Apr 2012)
                            mdecBasicScale = mdecBasicScale * intEmpWorkingDaysInPeriod / intWorkingDaysInPeriod
                        ElseIf intBasicSalaryComputation = enBasicSalaryComputation.WITH_WEEKENDS_INCLUDED Then
                            'Sohail (28 Oct 2017) -- Start
                            'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
                            'mdecBasicScale = mdecBasicScale * (DateDiff(DateInterval.Day, StartDate, EndDate) + 1) / CDec(.Item("DaysInPeriod"))
                            mdecBasicScale = mdecBasicScale * intEmpTotalDaysInPeriod / CDec(.Item("DaysInPeriod"))
                            'Sohail (28 Oct 2017) -- End
                            'Sohail (10 Jul 2013) -- Start
                            'TRA - ENHANCEMENT
                        ElseIf intBasicSalaryComputation = enBasicSalaryComputation.WITH_CONSTANT_DAYS Then
                            'Sohail (01 Feb 2014) -- Start
                            'Enhancement - StJude case (employee present days 30 while constant days were 28 days. So employee was getting more than basic scale.
                            'mdecBasicScale = mdecBasicScale * (DateDiff(DateInterval.Day, StartDate, EndDate) + 1) / CInt(.Item("ConstantDaysInPeriod"))
                            'Sohail (07 Mar 2014) -- Start
                            'Enhancement - As per Rutta's comment for St Jude request (e.g. if Present days are [3 or 25 or 28] then DBSonConstantDays * PresentDays.   if PresentDays are [29,30,31] then DBSonConstantDays * 28 )
                            'If (CInt(.Item("DaysInPeriod")) - (DateDiff(DateInterval.Day, StartDate, EndDate) + 1)) > CInt(.Item("ConstantDaysInPeriod")) Then
                            '    mdecBasicScale = mdecBasicScale * (DateDiff(DateInterval.Day, StartDate, EndDate) + 1) / CInt(.Item("ConstantDaysInPeriod"))
                            'ElseIf (DateDiff(DateInterval.Day, StartDate, EndDate) + 1) > CInt(.Item("ConstantDaysInPeriod")) Then
                            '    mdecBasicScale = mdecBasicScale
                            'Else
                            '    mdecBasicScale -= mdecBasicScale * (CInt(.Item("DaysInPeriod")) - (DateDiff(DateInterval.Day, StartDate, EndDate) + 1)) / CInt(.Item("ConstantDaysInPeriod"))
                            'End If
                            'Sohail (02 Feb 2017) -- Start
                            'CCBRT Enhancement - 64.1 - Case if emp join on 4th Jan and constant days are 28 then he should get 4 (31 - 27) days deduction.
                            'If (DateDiff(DateInterval.Day, StartDate, EndDate) + 1) <= CInt(.Item("ConstantDaysInPeriod")) Then
                            '    mdecBasicScale = mdecBasicScale * (DateDiff(DateInterval.Day, StartDate, EndDate) + 1) / CInt(.Item("ConstantDaysInPeriod"))
                            'Else
                            '    mdecBasicScale = mdecBasicScale
                            'End If
                            'Sohail (28 Oct 2017) -- Start
                            'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
                            'If (CDec(.Item("DaysInPeriod")) - (DateDiff(DateInterval.Day, StartDate, EndDate) + 1)) >= CInt(.Item("ConstantDaysInPeriod")) Then
                            '    mdecBasicScale = 0
                            'Else
                            '    mdecBasicScale = mdecBasicScale - ((CDec(.Item("DaysInPeriod")) - (DateDiff(DateInterval.Day, StartDate, EndDate) + 1)) * (mdecBasicScale / CInt(.Item("ConstantDaysInPeriod"))))
                            'End If
                            If (CDec(.Item("DaysInPeriod")) - intEmpTotalDaysInPeriod) >= CInt(.Item("ConstantDaysInPeriod")) Then
                                mdecBasicScale = 0
                            Else
                                mdecBasicScale = mdecBasicScale - ((CDec(.Item("DaysInPeriod")) - intEmpTotalDaysInPeriod) * (mdecBasicScale / CInt(.Item("ConstantDaysInPeriod"))))
                            End If
                            'Sohail (28 Oct 2017) -- End
                            'Sohail (02 Feb 2017) -- End
                            'Sohail (07 Mar 2014) -- End
                            'Sohail (01 Feb 2014) -- End
                            'Sohail (10 Jul 2013) -- End
                        End If

                            'Sohail (19 Dec 2018) -- Start
                            'NMB Enhancement - On Salary change screen. Allow salary proration for the new salary i.e for full basic salary, take old salary untill the effective date of the new salary + new salary from the effective date in 76.1.
                        Else

                            Dim decBScale As Decimal = 0
                            For Each dr_Salary As DataRow In drMultiSalary

                                If intBasicSalaryComputation = enBasicSalaryComputation.WITH_WEEKENDS_EXCLUDED Then
                                    decBScale += CDec(dr_Salary.Item("newscale")) * CInt(dr_Salary.Item("WeekendExcludedDays")) / intWorkingDaysInPeriod
                                ElseIf intBasicSalaryComputation = enBasicSalaryComputation.WITH_WEEKENDS_INCLUDED Then
                                    decBScale += CDec(dr_Salary.Item("newscale")) * CInt(dr_Salary.Item("WeekendIncludedDays")) / CDec(.Item("DaysInPeriod"))
                                ElseIf intBasicSalaryComputation = enBasicSalaryComputation.WITH_CONSTANT_DAYS Then
                                    If (CDec(.Item("DaysInPeriod")) - intEmpTotalDaysInPeriod) >= CInt(.Item("ConstantDaysInPeriod")) Then
                                        decBScale += 0
                                    Else
                                        decBScale += CDec(dr_Salary.Item("newscale")) - ((CDec(.Item("DaysInPeriod")) - CInt(dr_Salary.Item("WeekendIncludedDays"))) * (CDec(dr_Salary.Item("newscale")) / CInt(.Item("ConstantDaysInPeriod"))))
                                    End If
                                End If
                            Next
                            mdecBasicScale = decBScale
                        End If
                        'Sohail (19 Dec 2018) -- End

                    Else
                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
                        'intEmpTotalDaysInPeriod = CDec(.Item("DaysInPeriod")) 'Sohail (24 Dec 2013) 
                        'intEmpWorkingDaysInPeriod = intWorkingDaysInPeriod
                        'Sohail (09 Oct 2019) -- End
                    End If
                    'Sohail (18 Jun 2014) -- Start
                    'Enhancement - TOTAL DAYS IN TnA and Leave PERIOD for Voltamp. 
                    'Sohail (28 Oct 2017) -- Start
                    'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
                    'Dim TnAStartDate As Date = IIf(dtAppointment.Date > mdtTnA_StartDate, dtAppointment.Date, mdtTnA_StartDate)
                    'Dim TnAEndDate As Date = mdtTnA_EndDate
                    'If dtTerminationFrom.Date <> Nothing Then
                    '    TnAEndDate = IIf(dtTerminationFrom.Date < mdtTnA_EndDate, dtTerminationFrom.Date, mdtTnA_EndDate)
                    'End If
                    'If dtTerminationTo.Date <> Nothing Then
                    '    TnAEndDate = IIf(dtTerminationTo.Date < TnAEndDate, dtTerminationTo.Date, TnAEndDate)
                    'End If
                    'If dtEmplEnd.Date <> Nothing Then
                    '    TnAEndDate = IIf(dtEmplEnd.Date < TnAEndDate, dtEmplEnd.Date, TnAEndDate)
                    'End If
                    'intEmpTotalDaysInTnAPeriod = DateDiff(DateInterval.Day, TnAStartDate, TnAEndDate.AddDays(1))
                    'Sohail (28 Oct 2017) -- End
                    'Sohail (18 Jun 2014) -- End


                    'Sohail (24 Dec 2018) -- Start
                    'NMB Enhancement - 76.1 - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    'dblTotalYearOfService = Math.Floor((CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000)
                    'dblTotalMonthsOfService = (Math.Floor(DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) / 12) * 12 + (DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12) - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), 1, 0))
                    ''Sohail (21 Nov 2013) -- Start
                    ''ENHANCEMENT - OMAN
                    'dblTotalDaysOfService = CDbl(DateDiff(DateInterval.Day, dtAppointment, EndDate.AddDays(1)))
                    'dblYearOfService = Math.Floor((CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000)
                    'dblMonthsOfService = (Math.Floor(DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12) - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), CInt(IIf((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 <= 0, -11, 1)), 0))
                    'dblDaysOfService = Math.Floor(DateDiff(DateInterval.Day, DateAdd(DateInterval.Month, ((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), CInt(IIf((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 <= 0, -11, 1)), 0))), DateAdd(DateInterval.Year, (CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000, dtAppointment.Date)), EndDate.AddDays(1)))
                    ''Sohail (21 Nov 2013) -- End
                    ''Sohail (24 Sep 2013) -- End
                    Dim dr_Service() As DataRow = dsService.Tables(0).Select("employeeunkid = " & CInt(.Item("EmpID")) & " ")
                    If dr_Service.Length <= 0 Then
                    dblTotalYearOfService = Math.Floor((CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000)
                    dblTotalMonthsOfService = (Math.Floor(DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) / 12) * 12 + (DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12) - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), 1, 0))
                    dblTotalDaysOfService = CDbl(DateDiff(DateInterval.Day, dtAppointment, EndDate.AddDays(1)))
                    dblYearOfService = Math.Floor((CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000)
                    dblMonthsOfService = (Math.Floor(DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12) - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), CInt(IIf((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 <= 0, -11, 1)), 0))
                    dblDaysOfService = Math.Floor(DateDiff(DateInterval.Day, DateAdd(DateInterval.Month, ((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), CInt(IIf((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 <= 0, -11, 1)), 0))), DateAdd(DateInterval.Year, (CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000, dtAppointment.Date)), EndDate.AddDays(1)))
                        'Sohail (02 Dec 2020) -- Start
                        'NMB Enhancement : # OLD-215 : New Payroll Functions for Current Service Duration for Employee.
                        dblTotalYearOfCurrentService = Math.Floor((CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000)
                        dblTotalMonthsOfCurrentService = (Math.Floor(DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) / 12) * 12 + (DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12) - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), 1, 0))
                        dblTotalDaysOfCurrentService = CDbl(DateDiff(DateInterval.Day, dtAppointment, EndDate.AddDays(1)))
                        'Sohail (02 Dec 2020) -- End
                    Else
                        Dim dblTotDays As Double = (From p In dr_Service Select (CDbl(p.Item("ServiceDays")))).Sum()
                        dblTotalYearOfService = Int(dblTotDays / 365) 'Math.Floor((CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000)
                        dblTotalMonthsOfService = Int(dblTotDays / 30) '(Math.Floor(DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) / 12) * 12 + (DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12) - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), 1, 0))
                        dblTotalDaysOfService = dblTotDays 'CDbl(DateDiff(DateInterval.Day, dtAppointment, EndDate.AddDays(1)))
                        dblYearOfService = Int(dblTotDays / 365) 'Math.Floor((CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000)
                        dblMonthsOfService = Int((dblTotDays Mod 365) / 30) '(Math.Floor(DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12) - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), CInt(IIf((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 <= 0, -11, 1)), 0))
                        dblDaysOfService = ((dblTotDays Mod 365) Mod 30) 'Math.Floor(DateDiff(DateInterval.Day, DateAdd(DateInterval.Month, ((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), CInt(IIf((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 <= 0, -11, 1)), 0))), DateAdd(DateInterval.Year, (CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000, dtAppointment.Date)), EndDate.AddDays(1)))

                        'Sohail (02 Dec 2020) -- Start
                        'NMB Enhancement : # OLD-215 : New Payroll Functions for Current Service Duration for Employee.
                        Dim dblTotCurrentServiceDays As Double = (From p In dr_Service.AsEnumerable Order By p.Item("effectivedate").ToString Descending, p.Item("date1").ToString Descending, CDec(p.Item("ServiceDays")) Descending Select (CDbl(p.Item("ServiceDays")))).FirstOrDefault()
                        dblTotalYearOfCurrentService = Int(dblTotCurrentServiceDays / 365) 'Math.Floor((CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000)
                        dblTotalMonthsOfCurrentService = Int(dblTotCurrentServiceDays / 30) '(Math.Floor(DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) / 12) * 12 + (DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12) - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), 1, 0))
                        dblTotalDaysOfCurrentService = dblTotCurrentServiceDays 'CDbl(DateDiff(DateInterval.Day, dtAppointment, EndDate.AddDays(1)))
                        'Sohail (02 Dec 2020) -- End

                    End If
                    'Sohail (21 Nov 2013) -- End
                    'Sohail (24 Sep 2013) -- End
                    'Sohail (24 Dec 2018) -- End

                    'Sohail (08 Jun 2012) -- End
                    'Sohail (08 Nov 2011) -- End
                    'Sohail (18 Aug 2010) -- Start
                    'intTotalDaysWorked = intWorkingDaysInPeriod - CInt(.Item("Absent").ToString) - intOnHoldDays
                    'Sohail (11 Sep 2010) -- Start
                    'intTotalDaysWorked = CInt(.Item("TotalDaysWorkedWithAbsent").ToString) - CInt(.Item("Absent").ToString) - intOnHoldDays
                    'intTotalDaysWorked = CInt(.Item("TotalDaysWorked").ToString) - intOnHoldDays
                    dblTotalDaysWorked = CDec(.Item("TotalDaysWorked").ToString)
                    'Sohail (11 Sep 2010) -- End
                    'Sohail (18 Aug 2010) -- End
                    'Sohail (04 Mar 2011) -- Start
                    'Sohail (24 Mar 2014) -- Start
                    'Issue - Absent count issue if tna start id different and current date period is not over
                    'If mdtProcessdate < mdtPeriodEndDate Then
                    If mdtProcessdate < mdtTnA_Processdate Then
                        'Sohail (24 Mar 2014) -- End
                        'Sohail (29 Oct 2012) -- Start
                        'TRA - ENHANCEMENT
                        'mintTotalabsentorleave = CInt(.Item("ABSENT").ToString) + GetWorkingDays(CInt(.Item("EmpID").ToString), mdtProcessdate.AddDays(1), mdtPeriodEndDate, intShiftId, strShiftDays)
                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        'mintTotalabsentorleave = CInt(.Item("ABSENT").ToString) + GetWorkingDays(CInt(.Item("EmpID").ToString), mdtProcessdate.AddDays(1), mdtPeriodEndDate, intShiftId)
                        Dim intWorkingDays As Integer = 0

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                        'objEmpShift.GetWorkingDaysAndHours(CInt(.Item("EmpID").ToString), mdtProcessdate.AddDays(1), mdtPeriodEndDate, intWorkingDays, 0)
                        'Sohail (03 May 2018) -- Start
                        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                        'objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                        '                                   xUserUnkid, _
                        '                                   xYearUnkid, _
                        '                                   xCompanyUnkid, _
                        '                                   xUserModeSetting, _
                        '                                   CInt(.Item("EmpID").ToString), _
                        '                                   mdtProcessdate.AddDays(1), _
                        '                                   mdtPeriodEndDate, _
                        '                                   intWorkingDays, 0)
                        objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                           xUserUnkid, _
                                                           xYearUnkid, _
                                                           xCompanyUnkid, _
                                                           xUserModeSetting, _
                                                           CInt(.Item("EmpID").ToString), _
                                                           mdtProcessdate.AddDays(1), _
                                                           mdtPeriodEndDate, _
                                                           intWorkingDays, 0, objDataOperation, False, "")
                        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strApplyAdvanceJoinFilter]
                        'Sohail (03 May 2018) -- End
                        'Shani(24-Aug-2015) -- End

                        mdblTotalabsentorleave = CDec(.Item("ABSENT").ToString) + intWorkingDays
                        'Sohail (24 Sep 2013) -- End
                        'Sohail (29 Oct 2012) -- End
                    Else
                        mdblTotalabsentorleave = CDec(.Item("ABSENT").ToString)
                    End If
                    'Sohail (04 Mar 2011) -- End

                    'Sohail (26 Aug 2021) -- Start
                    'Enhancement :  : Process payroll data optimization.
                    blnFlag = objProcessPayroll.Void(.Item("EmpID").ToString, intPeriodUnkID, mintVoiduserunkid, dtCurrentDateAndTime, "", objDataOperation, blnApplyPayPerActivity)

                    If blnFlag = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'Sohail (26 Aug 2021) -- End

                    'Sohail (26 Aug 2021) -- Start
                    'Enhancement :  : Process payroll data optimization.
                    'If CInt(dsRow.Item("paymenttranunkid").ToString) <= 0 Then 'Sohail (04 Mar 2011) *** If payment is not done.
                    If mintTnaleavetranunkid <= 0 Then
                        'Sohail (26 Aug 2021) -- End

                        strB.Length = 0
                        strB.Append("INSERT INTO prtnaleave_tran ( " & _
                          "  payperiodunkid " & _
                          ", employeeunkid " & _
                          ", voucherno " & _
                          ", processdate " & _
                          ", daysinperiod " & _
                          ", paidholidays " & _
                          ", totalpayabledays " & _
                          ", totalpayablehours " & _
                          ", totaldaysworked " & _
                          ", totalhoursworked " & _
                          ", totalabsentorleave " & _
                          ", totalextrahours " & _
                          ", totalshorthours " & _
                          ", totalonholddays " & _
                          ", total_amount " & _
                          ", balanceamount " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason" & _
                          ", openingbalance" & _
                          ", auditdatetime " & _
                          ", ip " & _
                          ", host " & _
                          ", void_ip " & _
                          ", void_host " & _
                          ", form_name " & _
                          ", module_name1 " & _
                          ", module_name2 " & _
                          ", module_name3 " & _
                          ", module_name4 " & _
                          ", module_name5 " & _
                          ", isweb " & _
                                      ", tna_processdate " & _
                          ", costcenterunkid " & _
                          ", employee_enddate " & _
                        ") VALUES (" & _
                          "  @payperiodunkid " & _
                          ", @employeeunkid " & _
                          ", @voucherno " & _
                          ", @processdate " & _
                          ", @daysinperiod " & _
                          ", @paidholidays " & _
                          ", @totalpayabledays " & _
                          ", @totalpayablehours " & _
                          ", @totaldaysworked " & _
                          ", @totalhoursworked " & _
                          ", @totalabsentorleave " & _
                          ", @totalextrahours " & _
                          ", @totalshorthours " & _
                          ", @totalonholddays " & _
                          ", @total_amount " & _
                          ", @balanceamount " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiduserunkid " & _
                          ", @voiddatetime " & _
                          ", @voidreason" & _
                              ", @openingbalance" & _
                          ", @auditdatetime " & _
                          ", @ip " & _
                          ", @host " & _
                          ", @void_ip " & _
                          ", @void_host " & _
                          ", @form_name " & _
                          ", @module_name1 " & _
                          ", @module_name2 " & _
                          ", @module_name3 " & _
                          ", @module_name4 " & _
                          ", @module_name5 " & _
                          ", @isweb " & _
                                      ", @tna_processdate " & _
                          ", @costcenterunkid " & _
                          ", @employee_enddate " & _
                          "); SELECT @@identity" _
                          )
                        'Sohail (09 Oct 2019) - [employee_enddate]
                        'Sohail (04 Mar 2011) - [openingbalance], 'Sohail (12 Oct 2011) - [auditdatetime, basicsalary, ip, host, void_ip, void_host]
                        'Sohail (07 Jan 2014) - [tna_processdate]

                        objDataOperation.ClearParameters()

                        'Sohail (04 Mar 2011) -- Start
                        objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, lngVoucherNo.ToString)
                    Else

                        'Sohail (12 Oct 2011) -- Start
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If InsertAuditTrailForTnALeaveTran_Update(mintTnaleavetranunkid) = False Then Return False
                        'Sohail (03 May 2018) -- Start
                        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                        'If InsertAuditTrailForTnALeaveTran_Update(mintTnaleavetranunkid, dtCurrentDateAndTime, xUserUnkid) = False Then Return False
                        'If InsertAuditTrailForTnALeaveTran_Update(mintTnaleavetranunkid, dtCurrentDateAndTime, xUserUnkid, objDataOperation) = False Then Return False 'Sohail (26 Aug 2021)
                        'Sohail (03 May 2018) -- End
                        'Sohail (21 Aug 2015) -- End
                        'Sohail (12 Oct 2011) -- End

                        strB.Length = 0
                        strB.Append("UPDATE prtnaleave_tran SET " & _
                          "  payperiodunkid = @payperiodunkid" & _
                          ", employeeunkid = @employeeunkid" & _
                          ", voucherno = @voucherno" & _
                          ", processdate = @processdate" & _
                          ", daysinperiod = @daysinperiod" & _
                          ", paidholidays = @paidholidays" & _
                          ", totalpayabledays = @totalpayabledays" & _
                          ", totalpayablehours = @totalpayablehours" & _
                          ", totaldaysworked = @totaldaysworked" & _
                          ", totalhoursworked = @totalhoursworked" & _
                          ", totalabsentorleave = @totalabsentorleave" & _
                          ", totalextrahours = @totalextrahours" & _
                          ", totalshorthours = @totalshorthours" & _
                          ", totalonholddays = @totalonholddays" & _
                          ", total_amount = @total_amount" & _
                          ", balanceamount = @balanceamount" & _
                          ", userunkid = @userunkid" & _
                          ", isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voidreason = @voidreason " & _
                          ", openingbalance = @openingbalance " & _
                          ", auditdatetime = @auditdatetime " & _
                          ", ip = @ip " & _
                          ", host = @host " & _
                          ", void_ip = @void_ip " & _
                          ", void_host = @void_host " & _
                          ", form_name = @form_name " & _
                          ", module_name1 = @module_name1 " & _
                          ", module_name2 = @module_name2 " & _
                          ", module_name3 = @module_name3 " & _
                          ", module_name4 = @module_name4 " & _
                          ", module_name5 = @module_name5 " & _
                          ", isweb = @isweb " & _
                          ", tna_processdate = @tna_processdate " & _
                          ", costcenterunkid = @costcenterunkid " & _
                          ", employee_enddate = @employee_enddate " & _
                         " WHERE tnaleavetranunkid = @tnaleavetranunkid " _
                         )
                        'Sohail (09 Oct 2019) - [employee_enddate]
                        'Sohail (28 Apr 2018) - [costcenterunkid]
                        'Sohail (12 Oct 2011) - [auditdatetime, ip, host, void_ip, void_host]
                        'Sohail (07 Jan 2014) - [tna_processdate]

                        objDataOperation.ClearParameters()

                        objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnaleavetranunkid.ToString)
                        objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voucherno").ToString)

                    End If
                    'Sohail (04 Mar 2011) -- End

                    objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("period").ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("EmpID").ToString)
                    'Sohail (04 Mar 2011) -- Start
                    'objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, lngVoucherNo.ToString) 
                    'objDataOperation.AddParameter("@processdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                    objDataOperation.AddParameter("@processdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProcessdate)
                    'Sohail (04 Mar 2011) -- End
                    'Sohail (07 Aug 2010) -- Start
                    'Sohail (11 Nov 2019) -- Start
                    'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                    'objDataOperation.AddParameter("@daysinperiod", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("DaysInPeriod").ToString)
                    objDataOperation.AddParameter("@daysinperiod", SqlDbType.Float, eZeeDataType.MONEY_SIZE, DateDiff(DateInterval.Day, mdtTnA_StartDate, mdtTnA_EndDate.AddDays(1)))
                    'Sohail (11 Nov 2019) -- End
                    'objDataOperation.AddParameter("@daysinperiod", SqlDbType.Int, eZeeDataType.INT_SIZE, intWorkingDaysInPeriod)
                    objDataOperation.AddParameter("@paidholidays", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("PaidHoliday").ToString)
                    'objDataOperation.AddParameter("@totalpayabledays", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("TotPayableDays").ToString)
                    'Sohail (08 Nov 2011) -- Start
                    'TRA Requirement : Add feature to pay net salary based on New Employee Reporting date to work. the salary should be computed based on number of days remaining towards end of the period. This is for defined salaries.
                    'objDataOperation.AddParameter("@totalpayabledays", SqlDbType.Int, eZeeDataType.INT_SIZE, intWorkingDaysInPeriod)
                    'Sohail (11 Nov 2019) -- Start
                    'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                    'objDataOperation.AddParameter("@totalpayabledays", SqlDbType.Float, eZeeDataType.MONEY_SIZE, intEmpWorkingDaysInPeriod)
                    objDataOperation.AddParameter("@totalpayabledays", SqlDbType.Float, eZeeDataType.MONEY_SIZE, intEmpWorkingDaysInTnAPeriod)
                    'Sohail (11 Nov 2019) -- End
                    'Sohail (08 Nov 2011) -- End
                    'objDataOperation.AddParameter("@totalpayablehours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("TotPayableHours").ToString)
                    'Sohail (08 Nov 2011) -- Start
                    'TRA Requirement : Add feature to pay net salary based on New Employee Reporting date to work. the salary should be computed based on number of days remaining towards end of the period. This is for defined salaries.
                    'objDataOperation.AddParameter("@totalpayablehours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, intWorkingDaysInPeriod * CInt(.Item("DailyHours")))
                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    'objDataOperation.AddParameter("@totalpayablehours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, intEmpWorkingDaysInPeriod * CDec(.Item("DailyHours")))
                    'Hemant (19 July 2018) -- Start
                    'Support Issue Id # 0002391 -- Wrong hours worked posted on payroll,
                    'objDataOperation.AddParameter("@totalpayablehours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, intEmpWorkingDaysInPeriod * dblDailyHours)
                    'Sohail (11 Nov 2019) -- Start
                    'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                    'objDataOperation.AddParameter("@totalpayablehours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, dblEmpWorkingHoursInPeriod)
                    objDataOperation.AddParameter("@totalpayablehours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, dblEmpWorkingHoursInTnAPeriod)
                    'Sohail (11 Nov 2019) -- End
                    'Hemant (19 July 2018) -- End
                    'Sohail (24 Sep 2013) -- End
                    'Sohail (08 Nov 2011) -- End
                    'Sohail (07 Aug 2010) -- End
                    'Sohail (16 Aug 2010) -- Start
                    'objDataOperation.AddParameter("@totaldaysworked", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("TotalDaysWorked").ToString)
                    objDataOperation.AddParameter("@totaldaysworked", SqlDbType.Float, eZeeDataType.MONEY_SIZE, dblTotalDaysWorked.ToString)
                    'Sohail (16 Aug 2010) -- End
                    'Sohail (18 Aug 2010) -- Start
                    'objDataOperation.AddParameter("@totalhoursworked", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("TotHoursWorked").ToString)
                    objDataOperation.AddParameter("@totalhoursworked", SqlDbType.Float, eZeeDataType.MONEY_SIZE, CInt(.Item("TotHoursWorked").ToString) - CInt(.Item("TotalOvertime").ToString))
                    'Sohail (18 Aug 2010) -- End
                    'Sohail (04 Mar 2011) -- Start
                    'objDataOperation.AddParameter("@totalabsentorleave", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("Absent").ToString)
                    objDataOperation.AddParameter("@totalabsentorleave", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblTotalabsentorleave.ToString)
                    'Sohail (04 Mar 2011) -- End
                    objDataOperation.AddParameter("@totalextrahours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("TotalOvertime").ToString)
                    objDataOperation.AddParameter("@totalshorthours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("TotShortHours").ToString)
                    objDataOperation.AddParameter("@totalonholddays", SqlDbType.Float, eZeeDataType.MONEY_SIZE, dblOnHoldDays.ToString)
                    objDataOperation.AddParameter("@total_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_amount.ToString) 'Sohail (11 May 2011)
                    objDataOperation.AddParameter("@balanceamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalanceamount.ToString) 'Sohail (11 May 2011)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    'Sohail (12 Oct 2011) -- Start
                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                    'Sohail (12 Oct 2011) -- End
                    If mdtVoiddatetime = Nothing Then
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    Else
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    End If
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@openingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOpeningBalance.ToString) 'Sohail (04 Mar 2011), 'Sohail (11 May 2011)
                    'Sohail (12 Oct 2011) -- Start
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                    objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP.ToString)
                    objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHost.ToString)
                    objDataOperation.AddParameter("@void_ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
                    objDataOperation.AddParameter("@void_host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
                    'Sohail (12 Oct 2011) -- End
                    objDataOperation.AddParameter("@tna_processdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTnA_Processdate)
                    'Sohail (28 Apr 2018) -- Start
                    'Enhancement : Ref. No. 231 - WebAPI for seamless integration between Aruti and EPICOR in 72.1.
                    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostCenterId)
                    'Sohail (28 Apr 2018) -- End
                    'Sohail (09 Oct 2019) -- Start
                    'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
                    objDataOperation.AddParameter("@employee_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmployee_enddate)
                    'Sohail (09 Oct 2019) -- End

                    'Sohail (27 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    If mstrWebFormName.Trim.Length <= 0 Then
                        'S.SANDEEP [ 11 AUG 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'Dim frm As Form
                        'For Each frm In Application.OpenForms
                        '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                        '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                        '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                        '    End If
                        'Next
                        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                        'S.SANDEEP [ 11 AUG 2012 ] -- END
                    Else
                        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
                    End If
                    objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
                    objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
                    objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
                    objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
                    'Sohail (27 Jul 2012) -- End

                    dsList = objDataOperation.ExecQuery(strB.ToString, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Sohail (26 Aug 2021) -- Start
                    'Enhancement :  : Process payroll data optimization.
                    'If CInt(dsRow.Item("paymenttranunkid").ToString) <= 0 Then 'Sohail (04 Mar 2011)
                    Dim blnTnAInsert As Boolean = False
                    If mintTnaleavetranunkid <= 0 Then
                        'Sohail (26 Aug 2021) -- End
                        mintTnaleavetranunkid = dsList.Tables(0).Rows(0).Item(0)

                        'Sohail (26 Aug 2021) -- Start
                        'Enhancement :  : Process payroll data optimization.
                        blnTnAInsert = True
                        'Sohail (26 Aug 2021) -- End

                    End If 'Sohail (04 Mar 2011)
                    'dblDailyHours = CDec(.Item("DailyHours").ToString) 'Sohail (24 Sep 2013)

                    With objProcessPayroll
                        ._Tnaleavetranunkid = mintTnaleavetranunkid
                        ._PayPeriodUnkID = CInt(dsRow.Item("period").ToString)
                        ._Employeeunkid = CInt(dsRow.Item("EmpID").ToString)
                        ._DaysInPeriod = CDec(dsRow.Item("DaysInPeriod").ToString)
                        'Sohail (08 Nov 2011) -- Start
                        'TRA Requirement : Add feature to pay net salary based on New Employee Reporting date to work. the salary should be computed based on number of days remaining towards end of the period. This is for defined salaries.
                        '._WorkingDaysInPeriod = intWorkingDaysInPeriod
                        'Sohail (19 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - On global assign ED screen / ED screen, when user sets the stop date, prorate the earning/ deduction upto the set stop date.
                        '._WorkingDaysInPeriod = intEmpWorkingDaysInPeriod
                        ._EmpWorkingDaysInPeriod = intEmpWorkingDaysInPeriod
                        'Sohail (19 Dec 2018) -- End
                        'Sohail (08 Nov 2011) -- End
                        ._PaidLeave = CDec(dsRow.Item("PaidHoliday").ToString)
                        'Sohail (16 Aug 2010) -- Start
                        '._DaysWorked = cdec(dsRow.Item("TotalDaysWorked").ToString)
                        ._DaysWorked = dblTotalDaysWorked
                        'Sohail (16 Aug 2010) -- End
                        ._HoursWorked = CDec(dsRow.Item("TotHoursWorked").ToString)
                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        '._DailyHours = CDec(dsRow.Item("DailyHours"))
                        ._DailyHours = dblDailyHours
                        'Sohail (24 Sep 2013) -- End
                        'Sohail (12 Mar 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        '._WorkingHoursInPeriod = intEmpWorkingDaysInPeriod * CDec(dsRow.Item("DailyHours"))
                        'Hemant (19 July 2018) -- Start
                        'Support Issue Id # 0002391 -- Wrong hours worked posted on payroll,
                        '._WorkingHoursInPeriod = intEmpWorkingDaysInPeriod * dblDailyHours
                        ._WorkingHoursInPeriod = dblEmpWorkingHoursInPeriod
                        'Hemant (19 July 2018) -- End

                        'Sohail (24 Sep 2013) -- End
                        'Sohail (12 Mar 2012) -- End
                        'Sohail (04 Mar 2011) -- Start
                        '._Absent = cdec(dsRow.Item("Absent").ToString)
                        ._Absent = mdblTotalabsentorleave
                        'Sohail (04 Mar 2011) -- End
                        ._ExtraHours = CDec(dsRow.Item("TotalOvertime").ToString)
                        ._ShortHours = CDec(dsRow.Item("TotShortHours").ToString)
                        'Sohail (11 Sep 2010) -- Start
                        ._PaidAccrueLeave = CDec(dsRow.Item("PaidAccrueLeave").ToString)
                        ._OnHoldDays = dblOnHoldDays
                        'Sohail (11 Sep 2010) -- End

                        ._PeriodStartDate = mdtPeriodStartDate
                        ._PeriodEndDate = mdtPeriodEndDate
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        '._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
                        ._Userunkid = xUserUnkid
                        'Sohail (21 Aug 2015) -- End
                        ._PaymentTranUnkID = CInt(dsRow.Item("paymenttranunkid").ToString) 'Sohail (04 Mar 2011)

                        'Sohail (13 Sep 2011) -- Start
                        'objEmployee._Employeeunkid = CInt(dsRow.Item("EmpID").ToString)'Sohail (08 Nov 2011) 
                        'Sohail (08 Jun 2012) -- Start
                        'TRA - ENHANCEMENT
                        '._DefaultCostCenterUnkID = objEmployee._Costcenterunkid
                        ._DefaultCostCenterUnkID = intCostCenterId
                        'Sohail (08 Jun 2012) -- End
                        'Sohail (13 Sep 2011) -- End
                        'Sohail (08 Nov 2011) -- Start
                        ._BasicScale = mdecBasicScale
                        ._DailyBasicSalary = decDailyBasicSalary
                        ._HourlyBasicSalary = decHourlyBasicSalary
                        'Sohail (08 Nov 2011) -- End
                        '._CurrEDSlabPeriodUnkID = objED.GetCurrentSlabEDPeriodUnkID(CInt(dsRow.Item("EmpID").ToString), mdtPeriodEndDate) 'Sohail (23 Jan 2012)
                        ._CurrEDSlabPeriodUnkID = intEdPeriodId
                        'Sohail (28 Jan 2012) -- Start
                        'TRA - ENHANCEMENT
                        ._ShiftWorkingDaysInPeriod = intWorkingDaysInPeriod
                        ._ShiftWorkingHoursInPeriod = dblWorkingHoursInPeriod 'Sohail (17 Jul 2014)
                        'Sohail (12 Apr 2012) -- Start
                        'TRA - ENHANCEMENT
                        ._DailyBasicSalaryWeekendsIncluded = decDailyBasicSalaryWeekendsIncluded
                        ._HourlyBasicSalaryWeekendsIncluded = decHourlyBasicSalaryWeekendsIncluded
                        'Sohail (12 Apr 2012) -- End
                        'Sohail (10 Jul 2013) -- Start
                        'TRA - ENHANCEMENT
                        ._ConstantDaysInPeriod = CInt(dsRow.Item("ConstantDaysInPeriod").ToString)
                        ._DailyBasicSalaryOnConstantDays = decDailyBasicSalaryOnConstantDays
                        ._HourlyBasicSalaryOnConstantDays = decHourlyBasicSalaryOnConstantDays
                        'Sohail (10 Jul 2013) -- End

                            'Sohail (29 Oct 2012) -- Start
                            'TRA - ENHANCEMENT
                        ._WeekdaysWorkedHours = CDec(dsRow.Item("WeekdaysWorkedHours").ToString)
                        ._WeekdaysOvertimeHours = CDec(dsRow.Item("WeekdaysOvertimeHours").ToString)
                        ._WeekdaysShortHours = CDec(dsRow.Item("WeekdaysShortHours").ToString)
                        ._WeekdaysNightHours = CDec(dsRow.Item("WeekdaysNightHours").ToString)
                        ._WeekendWorkedHours = CDec(dsRow.Item("WeekendWorkedHours").ToString)
                        ._WeekendOvertimeHours = CDec(dsRow.Item("WeekendOvertimeHours").ToString)
                        ._WeekendShortHours = CDec(dsRow.Item("WeekendShortHours").ToString)
                        ._WeekendNightHours = CDec(dsRow.Item("WeekendNightHours").ToString)
                        ._PublicHolidayWorkedHours = CDec(dsRow.Item("PublicHolidayWorkedHours").ToString)
                        ._PublicHolidayOvertimeHours = CDec(dsRow.Item("PublicHolidayOvertimeHours").ToString)
                        ._PublicHolidayShortHours = CDec(dsRow.Item("PublicHolidayShortHours").ToString)
                        ._PublicHolidayNightHours = CDec(dsRow.Item("PublicHolidayNightHours").ToString)
                            'Sohail (29 Oct 2012) -- End
                        ._PublicHolidayDays = CDec(dsRow.Item("PublicHolidayDays").ToString) 'Sohail (12 Nov 2012)
                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        ._OverTimeHours2 = CDec(dsRow.Item("TotalOvertime2").ToString)
                        ._OverTimeHours3 = CDec(dsRow.Item("TotalOvertime3").ToString)
                        ._OverTimeHours4 = CDec(dsRow.Item("TotalOvertime4").ToString)
                        ._WeekendOverTimeHours2 = CDec(dsRow.Item("WeekendOvertimeHours2").ToString)
                        ._WeekendOverTimeHours3 = CDec(dsRow.Item("WeekendOvertimeHours3").ToString)
                        ._WeekendOverTimeHours4 = CDec(dsRow.Item("WeekendOvertimeHours4").ToString)
                        ._WeekdaysOverTimeHours2 = CDec(dsRow.Item("WeekdaysOvertimeHours2").ToString)
                        ._WeekdaysOverTimeHours3 = CDec(dsRow.Item("WeekdaysOvertimeHours3").ToString)
                        ._WeekdaysOverTimeHours4 = CDec(dsRow.Item("WeekdaysOvertimeHours4").ToString)
                        ._DayOffOverTimeHours1 = CDec(dsRow.Item("DayOffOvertimeHours").ToString)
                        ._DayOffOverTimeHours2 = CDec(dsRow.Item("DayOffOvertimeHours2").ToString)
                        ._DayOffOverTimeHours3 = CDec(dsRow.Item("DayOffOvertimeHours3").ToString)
                        ._DayOffOverTimeHours4 = CDec(dsRow.Item("DayOffOvertimeHours4").ToString)
                        ._PublicHolidayOvertimeHours2 = CDec(dsRow.Item("PublicHolidayOvertimeHours2").ToString)
                        ._PublicHolidayOvertimeHours3 = CDec(dsRow.Item("PublicHolidayOvertimeHours3").ToString)
                        ._PublicHolidayOvertimeHours4 = CDec(dsRow.Item("PublicHolidayOvertimeHours4").ToString)
                        ._YearOfService = dblYearOfService
                        ._MonthsOfService = dblMonthsOfService
                        'Sohail (24 Sep 2013) -- End
                        'Sohail (24 Dec 2013) -- Start
                        'Enhancement - Oman
                        ._EmpTotalDaysInPeriod = intEmpTotalDaysInPeriod
                        'Sohail (24 Dec 2013) -- End
                        'Sohail (18 Jun 2014) -- Start
                        'Enhancement - TOTAL DAYS IN TnA and Leave PERIOD for Voltamp. 
                        ._TotalDaysInTnAPeriod = intTotalDaysInTnAPeriod
                        ._EmpTotalDaysInTnAPeriod = intEmpTotalDaysInTnAPeriod
                        'Sohail (18 Jun 2014) -- End
                        'Sohail (21 Nov 2013) -- Start
                        'ENHANCEMENT - OMAN
                        ._DaysOfService = dblDaysOfService
                        ._TotalYearOfService = dblTotalYearOfService
                        ._TotalMonthsOfService = dblTotalMonthsOfService
                        ._TotalDaysOfService = dblTotalDaysOfService
                        'Sohail (21 Nov 2013) -- End
                        'Sohail (02 Dec 2020) -- Start
                        'NMB Enhancement : # OLD-215 : New Payroll Functions for Current Service Duration for Employee.
                        ._TotalYearOfCurrentService = dblTotalYearOfCurrentService
                        ._TotalMonthsOfCurrentService = dblTotalMonthsOfCurrentService
                        ._TotalDaysOfCurrentService = dblTotalDaysOfCurrentService
                        'Sohail (02 Dec 2020) -- End
                        'Sohail (29 Oct 2013) -- Start
                        'TRA - ENHANCEMENT
                        ._BasicSalaryOnWages = mdecBasicSalaryOnWages
                        'Sohail (15 Jun 2020) -- Start
                        'NMB Issue # : Out of Memory issue on processing payroll.
                        '._ShiftDataTable = dsShiftWiseData.Tables(0)
                        ._ShiftDataTable = New DataView(dsShiftWiseData.Tables(0), "EmpId = " & CInt(dsRow.Item("EmpId")) & " ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (15 Jun 2020) -- End
                        'Sohail (18 Jul 2017) -- Start
                        'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
                        'Sohail (15 Jun 2020) -- Start
                        'NMB Issue # : Out of Memory issue on processing payroll.
                        '._AllTnATable = dsAllTnA.Tables(0)
                        ._AllTnATable = New DataView(dsAllTnA.Tables(0), "employeeunkid = " & CInt(dsRow.Item("EmpId")) & " ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (15 Jun 2020) -- End
                        'Sohail (18 Jul 2017) -- End
                        'Sohail (05 Dec 2019) -- Start
                        'NMB UAT Enhancement # TC010 : System should be able to link OT module with payroll and compute special overtime (Holidays and weekend) and normal overtime respectively.
                        'Sohail (15 Jun 2020) -- Start
                        'NMB Issue # : Out of Memory issue on processing payroll.
                        '._AllOTReq = dsAllOTReq.Tables(0)
                        ._AllOTReq = New DataView(dsAllOTReq.Tables(0), "employeeunkid = " & CInt(dsRow.Item("EmpId")) & " ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (15 Jun 2020) -- End
                        'Sohail (05 Dec 2019) -- End
                        'Sohail (16 Jan 2020) -- Start
                        'NMB Enhancement # : System should calculate allowance given percentage of each salary change and accumulate it if salary head is used in formula.
                        If drMultiSalary IsNot Nothing AndAlso drMultiSalary.Length > 0 Then
                            ._MultiSalaryDataTable = drMultiSalary.CopyToDataTable
                        Else
                            ._MultiSalaryDataTable = Nothing
                        End If
                        dtProrateDate.Rows.Clear()
                        ._ProrateDateDataTable = dtProrateDate
                        'Sohail (16 Jan 2020) -- End
                        'Sohail (26 Aug 2016) -- Start
                        'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                        'Sohail (15 Jun 2020) -- Start
                        'NMB Issue # : Out of Memory issue on processing payroll.
                        '._EDHeadsTable = dsEDHeads.Tables(0)
                        ._EDHeadsTable = New DataView(dsEDHeads.Tables(0), "employeeunkid = " & CInt(dsRow.Item("EmpId")) & " ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (15 Jun 2020) -- End
                        'Sohail (02 Jan 2017) -- Start
                        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                        'Sohail (04 Jun 2018) -- Start
                        'CCK Issue - Support Issue Id # 0002297 - Mismatch of amount deducted from loan between payroll report and loan report. Tried running the Written off loan tool but experienced an error on it. (Completed loans were coming in process payroll) in 72.1.
                        '._LoanCalcTable = dsLoanCalc.Tables(0)
                        'Sohail (15 Jun 2020) -- Start
                        'NMB Issue # : Out of Memory issue on processing payroll.
                        '._LoanCalcTable = New DataView(dsLoanCalc.Tables(0), "statusunkid IN (" & CInt(enLoanStatus.IN_PROGRESS) & ", " & CInt(enLoanStatus.ON_HOLD) & ")", "", DataViewRowState.CurrentRows).ToTable
                        ._LoanCalcTable = New DataView(dsLoanCalc.Tables(0), "employeeunkid = " & CInt(dsRow.Item("EmpId")) & " AND statusunkid IN (" & CInt(enLoanStatus.IN_PROGRESS) & ", " & CInt(enLoanStatus.ON_HOLD) & ")", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (15 Jun 2020) -- End
                        'Sohail (04 Jun 2018) -- End
                        'Sohail (02 Jan 2017) -- End
                        'Sohail (26 Oct 2018) -- Start
                        'FDRC Issue - Process Payroll performance issue on FDRC database in 75.1.
                        ._TranHeadTable = dsTranHead.Tables(0)
                        'Sohail (26 Oct 2018) -- End
                        'Sohail (14 Mar 2019) -- Start
                        'NMB Issue - 76.1 - JV is picking employee cost center, not transaction cost center for net pay head mapped on employee cost center screen.
                        'Sohail (15 Jun 2020) -- Start
                        'NMB Issue # : Out of Memory issue on processing payroll.
                        '._InfoCCentreTable = dsInfoCCenter.Tables(0)
                        ._InfoCCentreTable = New DataView(dsInfoCCenter.Tables(0), "employeeunkid = " & CInt(dsRow.Item("EmpId")) & " ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (15 Jun 2020) -- End
                        'Sohail (14 Mar 2019) -- End
                        'Sohail (14 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                        ._LoanCCentreTable = dsLoanCCenter.Tables(0)
                        ._SavingCCentreTable = dsSavingCCenter.Tables(0)
                        'Sohail (14 Mar 2019) -- End
                        'Sohail (17 Sep 2019) -- Start
                        'NMB Enhancement # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
                        ._BenefitMasterTable = dsBenefitMaster.Tables(0)
                        If dsBenefitTran IsNot Nothing Then
                            'Sohail (15 Jun 2020) -- Start
                            'NMB Issue # : Out of Memory issue on processing payroll.
                            '._BenefitTranTable = dsBenefitTran.Tables(0)
                            ._BenefitTranTable = New DataView(dsBenefitTran.Tables(0), "employeeunkid = " & CInt(dsRow.Item("EmpId")) & " ", "", DataViewRowState.CurrentRows).ToTable
                            'Sohail (15 Jun 2020) -- End
                        End If
                        If dsEmpAllocation IsNot Nothing Then
                            'Sohail (15 Jun 2020) -- Start
                            'NMB Issue # : Out of Memory issue on processing payroll.
                            '._EmpAllocationTable = dsEmpAllocation.Tables(0)
                            ._EmpAllocationTable = New DataView(dsEmpAllocation.Tables(0), "employeeunkid = " & CInt(dsRow.Item("EmpId")) & " ", "", DataViewRowState.CurrentRows).ToTable
                            'Sohail (15 Jun 2020) -- End
                        End If
                        'Sohail (17 Sep 2019) -- End
                        'Sohail (07 Nov 2019) -- Start
                        'NMB Enhancement # : Flatrate prorating issue on employee exemption.
                        If dsEOC IsNot Nothing Then
                            ._EmpEOCLeavingTable = dsEOC.Tables(0)
                            'Sohail (04 Apr 2020) -- Start
                            'NMB Issue # : Benefit calculation coming wrong when processing for all employees when current employee Rehire transaction not there and there is a Rehire transaction for any previous processed employees.
                        Else
                            ._EmpEOCLeavingTable = Nothing
                            'Sohail (04 Apr 2020) -- End
                        End If
                        'Sohail (07 Nov 2019) -- End
                        'Hemant (06 Jul 2020) -- Start
                        'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
                        '._Dic_EmpCCenter = dicEmpCCenter.Where(Function(x) x.Key = 1).ToDictionary(Function(x) x.Key, Function(x) x.Value)
                        ._Dic_EmpCCenter = dicEmpCCenter.Where(Function(x) x.Key = CInt(dsRow.Item("EmpId"))).ToDictionary(Function(x) x.Key, Function(x) x.Value)
                        'Hemant (06 Jul 2020) -- End                        
                        ._FormulaSlabTable = dsFormulaSlab.Tables(0)
                        ._TrnFormulaTable = dsTrnFormula.Tables(0)
                        ._TrnFormulaCumulativeTable = dsTrnFormulaCumulative.Tables(0)
                        ._TrnSlabTable = dtTrnSlab
                        ._TrnTaxSlabTable = dtTrnTaxSlab
                        'Sohail (15 Jun 2020) -- Start
                        'NMB Issue # : Out of Memory issue on processing payroll.
                        '._EDHeadIDTable = dsEDHeadID.Tables(0)
                        ._EDHeadIDTable = New DataView(dsEDHeadID.Tables(0), "employeeunkid = " & CInt(dsRow.Item("EmpId")) & " ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (15 Jun 2020) -- End
                        'Sohail (26 Aug 2016) -- End
                        'Sohail (25 Feb 2022) -- Start
                        'Enhancement :  : Performance enhancement on process payroll.
                        If blnApplyPayPerActivity = True Then
                            ._PPATranTable = dsPPAtran.Tables(0)
                        Else
                            ._PPATranTable = Nothing
                        End If
                        ._CRProcessTable = dsCRProcess.Tables(0)
                        ._RetireProcessTable = dsRetireProcess.Tables(0)
                        'Sohail ((25 Feb 2022) -- End
                        ._Arr_EmpShift = arrEmpShift
                        ._Dic_ShiftDaysHours = dicShiftDaysHours
                        'Sohail (29 Oct 2013) -- End
                        ._IncrementAmount = decIncrementAmount
                        'Sohail (07 Jan 2014) -- Start
                        'Enhancement - Separate TnA Periods from Payroll Periods
                        ._TnA_Processdate = mdtTnA_Processdate
                        ._TnA_Startdate = mdtTnA_StartDate
                        'Sohail (07 Jan 2014) -- End
                        'Sohail (09 Jun 2014) -- Start
                        'Enhancement - Salary as per Days Present for Flat rate or Formula based salary if employee join or leaving in between month. 
                        ._BasicSalaryComputation = intBasicSalaryComputation
                        ._Emp_StartDate = StartDate
                        ._Emp_EndDate = EndDate
                        'Sohail (09 Jun 2014) -- End
                        'Sohail (08 Feb 2022) -- Start
                        'Enhancement : OLD-548 : Give Option to apply Basic Salary Computation Setting on Flat Rate Heads.
                        ._FlatRateHeadsComputation = intFlatRateHeadsComputation
                        'Sohail ((08 Feb 2022) -- End
                        'Sohail (30 Aug 2019) -- Start
                        'NMB Payroll UAT # - 76.1 -  It should be possible to configure formula and pay year of service as per the    appointed month - New function "Is Appointed Month" to be given which will return 0/1 so user will match this function "Is Appointed Month" with 0 or 1 (constant value) 0 = False 1 = True
                        ._AppointedDate = dtAppointment
                        'Sohail (30 Aug 2019) -- End
                        'Sohail (24 Sep 2014) -- Start
                        'Enhancement - Weekend Days and Public Holiday Days function in transaction head formula.
                        ._WorkingDaysInTnAPeriod = intWorkingDaysInTnAPeriod
                        'Sohail (26 Sep 2014) -- Start
                        'Enhancement - Weekend Hours and Public Holiday Hours function in transaction head formula.
                        '._PHDaysInPeriod = intPHDaysInPeriod
                        '._PHDaysInTnAPeriod = intPHDaysInTnAPeriod
                        ._TnA_EndDate = mdtTnA_EndDate
                        'Sohail (26 Sep 2014) -- End
                        'Sohail (24 Sep 2014) -- End
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        ._BaseCountryUnkId = intBaseCountryunkid
                        'Sohail (07 May 2015) -- End


                        'Sohail (24 Apr 2018) -- Start
                        'Voltamp Enhancement : Each head shoud be rounded before using in addition in formula in 71.1.
                        ._FmtCurrency = mstrFmtCurrency
                        'Sohail (24 Apr 2018) -- End

                        'Sohail (26 Aug 2021) -- Start
                        'Enhancement :  : Process payroll data optimization.
                        ._AuditUserId = mintUserunkid
                        ._Loginemployeeunkid = mintLogEmployeeUnkid
                        ._AuditDate = dtCurrentDateAndTime
                        If mstrWebFormName.Trim.Length <= 0 Then
                            ._Isweb = False
                            ._FormName = mstrForm_Name
                        Else
                            ._Isweb = True
                            ._FormName = mstrWebFormName
                        End If
                        ._ClientIP = strIP
                        ._HostName = strHost
                        'Sohail (26 Aug 2021) -- End


                        'objTranHead = New clsTransactionHead
                        'Sohail (08 Jun 2012) -- Start
                        'TRA - ENHANCEMENT
                        'objTranHead._Tranheadunkid = objEmployee._Tranhedunkid
                        'objTranHead._Tranheadunkid = intTranHeadId
                        'Sohail (08 Jun 2012) -- End
                        'Sohail (03 Sep 2012) -- Start
                        'TRA - ENHANCEMENT - Now Salary can be informational as per FDRC requirement under type of SALARY and calc type is FLATE RATE.
                        'If Not (intCalcTypeId = enCalcType.DEFINED_SALARY OrElse intCalcTypeId = enCalcType.OnAttendance OrElse intCalcTypeId = enCalcType.OnHourWorked) Then
                        If Not (intCalcTypeId = enCalcType.DEFINED_SALARY OrElse intCalcTypeId = enCalcType.OnAttendance OrElse intCalcTypeId = enCalcType.OnHourWorked OrElse intCalcTypeId = enCalcType.FlatRate_Others) Then
                            'Sohail (03 Sep 2012) -- End
                            'If Not (objTranHead._Calctype_Id = enCalcType.DEFINED_SALARY OrElse objTranHead._Calctype_Id = enCalcType.OnAttendance OrElse objTranHead._Calctype_Id = enCalcType.OnHourWorked) Then
                            ._BasicScale = 0
                            ._DailyBasicSalary = 0
                            ._HourlyBasicSalary = 0
                        End If
                        'Sohail (28 Jan 2012) -- End
                    End With

                    'Sohail (04 Mar 2011) -- Start
                    'Dim objEmployee As New clsEmployee_Master
                    'objEmployee._Employeeunkid = CInt(dsRow.Item("EmpID").ToString)
                    'objProcessPayroll._Costcenterunkid = objEmployee._Costcenterunkid
                    'objEmployee = Nothing
                    'Sohail (04 Mar 2011) -- End

                    'Sohail (03 Mar 2011) -- Start
                    'Changes : Now first deduct loan/advance and saving no matter enough balance is available or not.

                    'Nilay (25-Mar-2016) -- Start
                    'blnFlag = objProcessPayroll.InsertLoanAdvanceData()

                    'Sohail (02 Jan 2017) -- Start
                    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                    'blnFlag = objProcessPayroll.InsertLoanAdvanceData(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                    '                                                  mdtPeriodStartDate, mdtPeriodEndDate, xUserModeSetting, True)
                    blnFlag = objProcessPayroll.InsertLoanAdvanceData(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                      mdtPeriodStartDate, mdtPeriodEndDate, xUserModeSetting, True, objDataOperation, blnInsertOnlyNoInterestMappedHeadLoan:=False)
                    'Sohail (29 Apr 2019) - [blnInsertNoInterestMappedHeadLoan:=False]
                    'Sohail (02 Jan 2017) -- End
                    'Nilay (25-Mar-2016) -- End

                    If blnFlag = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    'Sohail (02 Jan 2017) -- Start
                    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                    'blnFlag = objProcessPayroll.InsertSavingsData()
                    blnFlag = objProcessPayroll.InsertSavingsData(objDataOperation)
                    'Sohail (02 Jan 2017) -- End
                    If blnFlag = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'Sohail (03 Mar 2011) -- End

                    'Sohail (02 Jan 2017) -- Start
                    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                    'Sohail (03 May 2018) -- Start
                    'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                    'Dim intNetPayRoundingHeadId As Integer = objTranHead.GetNetPayRoundingAdjustmentHeadID()
                    Dim intNetPayRoundingHeadId As Integer = objTranHead.GetNetPayRoundingAdjustmentHeadID(objDataOperation)
                    'Sohail (03 May 2018) -- End
                    'Sohail (02 Jan 2017) -- End


                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                    'blnFlag = objProcessPayroll.InsertTranHeadData()
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objProcessPayroll.InsertTranHeadData(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting)

                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                    'blnFlag = objProcessPayroll.InsertTranHeadData(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, blnApplyPayPerActivity, dtDatabase_Start_Date, dtDatabase_End_Date, intLeaveBalanceSetting, blnIsFin_Close, dtCurrentDateAndTime)
                    blnFlag = objProcessPayroll.InsertTranHeadData(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, blnApplyPayPerActivity, dtDatabase_Start_Date, dtDatabase_End_Date, intLeaveBalanceSetting, blnIsFin_Close, dtCurrentDateAndTime, xLeaveAccrueTenureSetting, xLeaveAccrueDaysAfterEachMonth, intNetPayRoundingHeadId, objDataOperation)
                    'Pinkal (18-Nov-2016) -- End


                    'Sohail (21 Aug 2015) -- End
                    'Shani(24-Aug-2015) -- End

                    If blnFlag = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    'Sohail (03 Mar 2011) -- Start
                    'Changes : Now first deduct loan/advance and saving no matter enough balance is available or not.
                    'blnFlag = objProcessPayroll.InsertLoanAdvanceData()
                    'If blnFlag = False Then
                    '    Return False
                    'End If

                    'blnFlag = objProcessPayroll.InsertSavingsData()
                    'If blnFlag = False Then
                    '    Return False
                    'End If
                    'Sohail (03 Mar 2011) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = UpDateBalance(mintTnaleavetranunkid, CInt(.Item("EmpID").ToString), decPaidAmount, objProcessPayroll._BasicSalary) 'Sohail (04 Mar 2011) [parameter PaidAmt is added.]
                    'Sohail (26 Aug 2016) -- Start
                    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                    'blnFlag = UpDateBalance(mintTnaleavetranunkid, CInt(.Item("EmpID").ToString), decPaidAmount, objProcessPayroll._BasicSalary, intBase_CurrencyId)
                    'Sohail (02 Jan 2017) -- Start
                    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                    'blnFlag = UpDateBalance(mintTnaleavetranunkid, CInt(.Item("EmpID").ToString), decPaidAmount, objProcessPayroll._BasicSalary, mdecDecimalPlaces)
                    blnFlag = UpDateBalance(mintTnaleavetranunkid, CInt(.Item("EmpID").ToString), decPaidAmount, objProcessPayroll._BasicSalary, mdecDecimalPlaces, objDataOperation, dsTranHead.Tables(0))
                    'Sohail (19 May 2021) - [dsTranHead.Tables(0)]
                    'Sohail (02 Jan 2017) -- End
                    'Sohail (26 Aug 2016) -- End
                    'Sohail (21 Aug 2015) -- End
                    If blnFlag = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    'Sohail (26 Aug 2021) -- Start
                    'Enhancement :  : Process payroll data optimization.
                    If blnTnAInsert = True Then
                        If InsertAuditTrailForTnALeaveTran_Update(mintTnaleavetranunkid, dtCurrentDateAndTime, xUserUnkid, 1, objDataOperation) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    Else
                        If IsTableDataUpdate(mintTnaleavetranunkid, objDataOperation) = True Then
                            If InsertAuditTrailForTnALeaveTran_Update(mintTnaleavetranunkid, dtCurrentDateAndTime, xUserUnkid, 2, objDataOperation) = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                        End If
                    End If
                    'Sohail (26 Aug 2021) -- End

                End With

                objDataOperation.ReleaseTransaction(True) 'Sohail (26 Aug 2016)

                intCount = intCount + 1 'Sohail (25 Jun 2018)
                'Sohail (22 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                If bw IsNot Nothing Then
                    'intCount = intCount + 1 'Sohail (25 Jun 2018)
                    bw.ReportProgress(intCount)
                    'Sohail (26 Aug 2016) -- Start
                    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                    If bw.CancellationPending = True Then
                        Exit For
                    End If
                    'Sohail (26 Aug 2016) -- End
                End If
                'Sohail (22 Aug 2012) -- End
                'Sohail (25 Jun 2018) -- Start
                'Internal Enhancement - Showing Progress count on Aruti Self Service for Process Payroll in 72.1.
                clsTnALeaveTran._ProgressCurrCount = intCount
                'Sohail (25 Jun 2018) -- End

            Next

            'objDataOperation.ReleaseTransaction(True) 'Sohail (26 Aug 2016)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            'Sohail (07 Jan 2020) -- Start
            'Throw New Exception(ex.Message & " For Employee: " & strCurrEmpName & "; Procedure Name: InsertDelete; Module Name: " & mstrModuleName)
            Dim strMsg As String = ex.Message & " For Employee: " & strCurrEmpName & "; Procedure Name: InsertDelete; Module Name: " & mstrModuleName & "; " & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                strMsg &= "; " & ex.InnerException.Message
            End If
            Throw New Exception(strMsg)
            'Sohail (07 Jan 2020) -- End
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            'objED = Nothing
            objPeriod = Nothing
            'objEmployee = Nothing 'Sohail (08 Jun 2012)
            'Sohail (08 Nov 2011) -- Start
            'objMaster = Nothing 'Sohail (08 Jun 2012)
            objWages = Nothing
            'Sohail (08 Nov 2011) -- End
            'objTranHead = Nothing 'Sohail (28 Jan 2012) 
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Database Table (prtnaleave_tran) </purpose>
    Public Function Void(ByVal strEmployeeList As String, ByVal intPeriodUnkID As Integer, ByVal intVoidUserUnkID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String _
                         , ByVal objDataOperation As clsDataOperation _
                         ) As Boolean
        'Sohail (26 Aug 2016) - [objDataOperation]
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (26 Aug 2016) -- Start
        'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
        'objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        'Sohail (26 Aug 2016) -- End

        Try
            'strQ = "DELETE FROM prtnaleave_tran " & _
            '"WHERE tnaleavetranunkid = @tnaleavetranunkid "

            strQ = "UPDATE prtnaleave_tran SET " & _
              " isvoid = 1" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", void_ip = @void_ip " & _
              ", void_host = @void_host " & _
            "WHERE employeeunkid IN (" & strEmployeeList & ") " & _
            "AND ISNULL(isvoid,0) = 0 " & _
            "AND payperiodunkid = @payperiodunkid " 'Sohail (16 Oct 2010), 'Sohail (12 Oct 2011) - [void_ip, void_host]

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserUnkID.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            'Sohail (12 Oct 2011) -- Start
            objDataOperation.AddParameter("@void_ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP.ToString)
            objDataOperation.AddParameter("@void_host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHost.ToString)
            'Sohail (12 Oct 2011) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing 'Sohail (26 Aug 2016)
        End Try
    End Function

    Public Function getMaxVoucherNo(Optional ByVal xDataOp As clsDataOperation = Nothing) As Long
        'Sohail (18 Jan 2019) - [xDataOp]
        Dim dsTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'Dim objED As New clsEarningDeduction
        'Dim objPeriod As New clscommom_period_Tran
        'Sohail (18 Jan 2019) -- End

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try

            strQ = "SELECT ISNULL(MAX(CONVERT(FLOAT,voucherno)),0) AS MaxNo FROM prtnaleave_tran"

            dsTable = objDataOperation.ExecQuery(strQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return CInt(dsTable.Rows(0).Item("MaxNo").ToString)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getMaxVoucherNo; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsTable IsNot Nothing Then dsTable.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
    End Function

    Public Function UpDateBalance(ByVal intTnALeaveUnkID As Integer _
                                  , ByVal intEmpUnkID As Integer _
                                  , ByVal decPaidAmt As Decimal _
                                  , ByVal decBasicSalary As Decimal _
                                  , ByVal decDecimalPlaces As Decimal _
                                  , ByVal xDataOp As clsDataOperation _
                                  , ByVal dtHead As DataTable _
                                  ) As Boolean
        'Sohail (19 May 2021) - [dtHead]
        'Sohail (02 Jan 2017) - [xDataOp]
        'Sohail (26 Aug 2016) - [Removed : ByVal intBase_CurrencyId As Integer], [Added : ByVal decDecimalPlaces As Decimal]
        'Sohail (04 Mar 2011) (parameter dblPaidAmt added.), 'Sohail (13 Sep 2011) -> decBasicSalary parameter added.
        'Public Function UpDateBalance(ByVal intTnALeaveUnkID As Integer, ByVal intEmpUnkID As Integer) As Boolean 
        Dim dsList As DataSet = Nothing
        'Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (02 Jan 2017) -- Start
        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (02 Jan 2017) -- End

        Try

            'Sohail (14 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'Dim objExchangeRate As New clsExchangeRate
            'Dim decDecimalPlaces As Decimal = 0
            'Sohail (26 Aug 2016) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            'objExchangeRate._ExchangeRateunkid = intBase_CurrencyId 'Sohail (26 Aug 2016) 
            'Sohail (21 Aug 2015) -- End
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal 'Sohail (26 Aug 2016) 
            'Sohail (14 Aug 2012) -- End

            strB.Length = 0
            'Sohail (13 Dec 2016) -- Start
            'Enhancement - 64.1 - Net Pay Rounding issue - Net Pay does not match with payment amount.
            'strB.Append("Update prtnaleave_tran SET " & _
            '            "  total_amount = T.TotAdd - T.TotDeduct " & _
            '            ", balanceamount = T.TotAdd + openingbalance - T.TotDeduct - @PaidAmount " & _
            '            ", basicsalary = @basicsalary " & _
            '        "FROM " & _
            '        "( " & _
            '            "SELECT ISNULL(SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) ),0) AS TotAdd, " & _
            '                "(" & _
            '                    "SELECT ISNULL(SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) ),0) " & _
            '                    "FROM prpayrollprocess_tran " & _
            '                    "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
            '                    "AND employeeunkid = @employeeunkid " & _
            '                    "AND add_deduct = 2 " & _
            '                    "AND ISNULL(isvoid,0) = 0 " & _
            '                ") " & _
            '                "AS totDeduct " & _
            '            "FROM prpayrollprocess_tran " & _
            '            "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
            '            "AND employeeunkid = @employeeunkid " & _
            '            "AND add_deduct = 1 " & _
            '            "AND ISNULL(isvoid,0) = 0 " & _
            '        ") AS T " & _
            '        "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
            '        "AND employeeunkid = @employeeunkid " & _
            '        "AND ISNULL(isvoid,0) = 0 " _
            '        )
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'strB.Append("Update prtnaleave_tran SET " & _
            '            "  total_amount = T.TotAdd - T.TotDeduct " & _
            '            ", balanceamount = T.TotAdd + openingbalance - T.TotDeduct - @PaidAmount " & _
            '            ", basicsalary = @basicsalary " & _
            '        "FROM " & _
            '        "( " & _
            '           "SELECT ISNULL(SUM(CAST(amount AS DECIMAL(36, 6)) ),0) AS TotAdd, " & _
            '                "(" & _
            '                   "SELECT ISNULL(SUM(CAST(amount AS DECIMAL(36, 6)) ),0) " & _
            '                    "FROM prpayrollprocess_tran " & _
            '                    "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
            '                    "AND employeeunkid = @employeeunkid " & _
            '                    "AND add_deduct = 2 " & _
            '                    "AND ISNULL(isvoid,0) = 0 " & _
            '                ") " & _
            '                "AS totDeduct " & _
            '            "FROM prpayrollprocess_tran " & _
            '            "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
            '            "AND employeeunkid = @employeeunkid " & _
            '            "AND add_deduct = 1 " & _
            '            "AND ISNULL(isvoid,0) = 0 " & _
            '        ") AS T " & _
            '        "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
            '        "AND employeeunkid = @employeeunkid " & _
            '        "AND ISNULL(isvoid,0) = 0 " _
            '        )
            strB.Append("Update prtnaleave_tran SET " & _
                        "  total_amount = T.TotAdd - T.TotDeduct " & _
                        ", balanceamount = T.TotAdd + openingbalance - T.TotDeduct - @PaidAmount " & _
                        ", basicsalary = @basicsalary " & _
                    "FROM " & _
                    "( " & _
                       "SELECT ISNULL(SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) ),0) AS TotAdd, " & _
                            "(" & _
                               "SELECT ISNULL(SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) ),0) " & _
                                "FROM prpayrollprocess_tran " & _
                                "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
                                "AND employeeunkid = @employeeunkid " & _
                                "AND add_deduct = 2 " & _
                                "AND ISNULL(isvoid,0) = 0 " & _
                            ") " & _
                            "AS totDeduct " & _
                        "FROM prpayrollprocess_tran " & _
                        "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
                        "AND employeeunkid = @employeeunkid " & _
                        "AND add_deduct = 1 " & _
                        "AND ISNULL(isvoid,0) = 0 " & _
                    ") AS T " & _
                    "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
                    "AND employeeunkid = @employeeunkid " & _
                    "AND ISNULL(isvoid,0) = 0 " _
                    )
            'Sohail (19 May 2021) - [ISNULL(SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) ),0)] = [ISNULL(SUM(amount),0)]
            'Sohail (29 Nov 2017) -- End
            'Sohail (13 Dec 2016) -- End
            'Sohail (16 Oct 2010), 'Sohail (04 Mar 2011)- [opening balance is added and paid amount is deducted from balanceamount]



            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTnALeaveUnkID.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID.ToString)
            objDataOperation.AddParameter("@PaidAmount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decPaidAmt.ToString) 'Sohail (04 Mar 2011), 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@basicsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decBasicSalary.ToString) 'Sohail (13 Sep 2011)

            Call objDataOperation.ExecNonQuery(strB.ToString)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (19 May 2021) -- Start
            'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
            Dim dblRoundOff_Type As Double = mdblRoundOff_Type
            Dim drHead() As DataRow = dtHead.Select("calctype_id = " & enCalcType.NET_PAY_ROUNDING_ADJUSTMENT & " ") ' dtHead.Select("tranheadunkid = " & intNetPayRoundingHeadId & " ")

            If drHead.Length > 0 Then
                dblRoundOff_Type = CDbl(drHead(0).Item("roundofftypeid"))
            End If
            'Sohail (19 May 2021) -- End

            'Sohail (01 Mar 2016) -- Start
            'Enhancement - Rounding issue for Net Pay on Payroll Report, not matching with Payslip Report.
            'Sohail (02 Jan 2017) -- Start
            'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
            '_Tnaleavetranunkid = intTnALeaveUnkID
            _Tnaleavetranunkid(objDataOperation) = intTnALeaveUnkID
            'Sohail (02 Jan 2017) -- End
            'Sohail (23 Jun 2021) -- Start
            'NMB Iuuse : : Flex cube JV not balancing due to fmt currency.
            dblRoundOff_Type = mdblRoundOff_Type
            'Sohail (23 Jun 2021) -- End
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'Sohail (19 May 2021) -- Start
            'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
            'If _Total_amount <> Rounding.BRound(_Total_amount, mdblRoundOff_Type) Then
            If _Total_amount <> Rounding.BRound(_Total_amount, dblRoundOff_Type) Then
                'Sohail (19 May 2021) -- End
                'Sohail (02 Jan 2017) -- Start
                'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                'If objProcessPayroll.InsertNetPayRoundingAdjustment(intTnALeaveUnkID, _Total_amount, Rounding.BRound(_Total_amount, mdblRoundOff_Type)) = False Then
                'Sohail (19 May 2021) -- Start
                'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                'If objProcessPayroll.InsertNetPayRoundingAdjustment(intTnALeaveUnkID, _Total_amount, Rounding.BRound(_Total_amount, mdblRoundOff_Type), objDataOperation) = False Then
                If objProcessPayroll.InsertNetPayRoundingAdjustment(intTnALeaveUnkID, _Total_amount, Rounding.BRound(_Total_amount, dblRoundOff_Type), objDataOperation) = False Then
                    'Sohail (19 May 2021) -- End
                    'Sohail (02 Jan 2017) -- End
                    Return False
                End If
            End If
            'Sohail (18 Apr 2016) -- End

            'Sohail (19 May 2021) -- Start
            'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
            'dblRoundOff_Type = mdblRoundOff_Type 'Sohail (23 Jun 2021)
            'drHead = dtHead.Select("calctype_id = " & enCalcType.NET_PAY & " ")

            'If drHead.Length > 0 Then
            '    dblRoundOff_Type = CDbl(drHead(0).Item("roundofftypeid"))
            'End If
            'Sohail (19 May 2021) -- End

            'Sohail (02 Jan 2017) -- Start
            'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
            'objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            'Sohail (02 Jan 2017) -- End

            strB.Length = 0
            strB.Append("UPDATE prtnaleave_tran SET " & _
                          "  total_amount = @total_amount " & _
                          ", balanceamount = @balanceamount " & _
                "WHERE tnaleavetranunkid = @tnaleavetranunkid " & _
                "AND isvoid = 0 " _
                )

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTnALeaveUnkID.ToString)
            'Sohail (19 May 2021) -- Start
            'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
            'objDataOperation.AddParameter("@total_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Rounding.BRound(_Total_amount, mdblRoundOff_Type))
            'objDataOperation.AddParameter("@balanceamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Rounding.BRound(_Balanceamount, mdblRoundOff_Type))
            objDataOperation.AddParameter("@total_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Rounding.BRound(_Total_amount, dblRoundOff_Type))
            objDataOperation.AddParameter("@balanceamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Rounding.BRound(_Balanceamount, dblRoundOff_Type))
            'Sohail (19 May 2021) -- End

            Call objDataOperation.ExecNonQuery(strB.ToString)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (01 Mar 2016) -- End

            'Sohail (03 Apr 2019) -- Start
            'NMB Issue - 76.1 - Distribute net pay head to cost center after net pay rounding.
            'Sohail (19 May 2021) -- Start
            'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
            'If objProcessPayroll.InsertNetPay(intTnALeaveUnkID, Rounding.BRound(_Total_amount, mdblRoundOff_Type), objDataOperation) = False Then
            If objProcessPayroll.InsertNetPay(intTnALeaveUnkID, Rounding.BRound(_Total_amount, dblRoundOff_Type), objDataOperation) = False Then
                'Sohail (19 May 2021) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (03 Apr 2019) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpDateBalance; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Get_Balance_List(ByVal xDatabaseName As String _
                                     , ByVal xUserUnkid As Integer _
                                     , ByVal intYearID As Integer _
                                     , ByVal xCompanyUnkid As Integer _
                                     , ByVal xPeriodStart As DateTime _
                                     , ByVal xPeriodEnd As DateTime _
                                     , ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean _
                                     , ByVal blnIncludeInActiveEmployee As Boolean _
                                     , ByVal blnApplyUserAccessFilter As Boolean _
                                     , ByVal strFilerString As String _
                                     , ByVal strTableName As String _
                                     , Optional ByVal strEmpUnkIDs As String = "" _
                                     , Optional ByVal intPeriodID As Integer = 0 _
                                     , Optional ByVal strOrderby As String = "" _
                                     , Optional ByVal strAdvanceFilter As String = "" _
                                     ) As DataSet 'Sohail (16 Oct 2010)
        '                            'Sohail (24 Jun 2019) - [strAdvanceFilter]
        '                            'Sohail (18 May 2013) - [strIncludeInActiveEmployee, strEmployeeAsOnDate]
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, blnApplyUserAccessFilter, strFilerString]
        'Public Function Get_Balance_List(ByVal strTableName As String) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, intYearID, xUserModeSetting)
        If strAdvanceFilter.Trim.Length > 0 Then 'Sohail (26 Oct 2020)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        End If 'Sohail (26 Oct 2020)
        'Sohail (21 Aug 2015) -- End

        Try

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If strIncludeInActiveEmployee.Trim = "" Then strIncludeInActiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            'If strEmployeeAsOnDate.Trim = "" Then strEmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate.ToString
            'Sohail (21 Aug 2015) -- End
            'Sohail (18 May 2013) -- End

            'Sohail (16 Oct 2010) -- Start
            'Changes : New parameter added (empID, yearID, periodID, orderby)
            'strQ = "SELECT  tnaleavetranunkid , " & _
            '                "prtnaleave_tran.employeeunkid , " & _
            '                "ISNULL(hremployee_master.employeecode, '') AS employeecode , " & _
            '                "ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename , " & _
            '                "prtnaleave_tran.payperiodunkid , " & _
            '                "ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName , " & _
            '                "prtnaleave_tran.voucherno , " & _
            '                "prtnaleave_tran.processdate , " & _
            '                "prtnaleave_tran.daysinperiod , " & _
            '                "prtnaleave_tran.paidholidays , " & _
            '                "prtnaleave_tran.totalpayabledays , " & _
            '                "prtnaleave_tran.totalpayablehours , " & _
            '                "prtnaleave_tran.totaldaysworked , " & _
            '                "prtnaleave_tran.totalhoursworked , " & _
            '                "prtnaleave_tran.totalabsentorleave , " & _
            '                "prtnaleave_tran.totalextrahours , " & _
            '                "prtnaleave_tran.totalshorthours , " & _
            '                "ISNULL(prtnaleave_tran.totalonholddays,0) AS totalonholddays , " & _
            '                "prtnaleave_tran.total_amount , " & _
            '                "prtnaleave_tran.balanceamount , " & _
            '                "prtnaleave_tran.userunkid , " & _
            '                "prtnaleave_tran.isvoid " & _
            '        "FROM    prtnaleave_tran " & _
            '                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
            '        "WHERE   balanceamount > 0 " & _
            '                "AND isvoid = 0 " & _
            '        "ORDER BY employeeunkid "

            'Sohail (18 Dec 2010) -- Start
            'Issue : No List Zero Balance Salary
            'strQ = "SELECT  tnaleavetranunkid " & _
            '                ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
            '                ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
            '                ", prtnaleave_tran.payperiodunkid " & _
            '                ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
            '                ", prtnaleave_tran.employeeunkid " & _
            '                ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
            '                ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname, '') AS employeename " & _
            '                ", prtnaleave_tran.voucherno " & _
            '                ", prtnaleave_tran.processdate " & _
            '                ", prtnaleave_tran.daysinperiod " & _
            '                ", prtnaleave_tran.paidholidays " & _
            '                ", prtnaleave_tran.totalpayabledays " & _
            '                ", prtnaleave_tran.totalpayablehours " & _
            '                ", prtnaleave_tran.totaldaysworked " & _
            '                ", prtnaleave_tran.totalhoursworked " & _
            '                ", prtnaleave_tran.totalabsentorleave " & _
            '                ", prtnaleave_tran.totalextrahours " & _
            '                ", prtnaleave_tran.totalshorthours " & _
            '                ", ISNULL(prtnaleave_tran.totalonholddays,0) AS totalonholddays " & _
            '                ", prtnaleave_tran.total_amount " & _
            '                ", prtnaleave_tran.balanceamount " & _
            '                ", prtnaleave_tran.userunkid " & _
            '                ", prtnaleave_tran.isvoid " & _
            '        "FROM    prtnaleave_tran " & _
            '                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
            '                "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
            '        "WHERE   balanceamount > 0 " & _
            '                "AND ISNULL(isvoid,0) = 0 "
            'strQ = "SELECT  tnaleavetranunkid " & _
            '              ", hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
            '              ", ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name, '') AS YearName " & _
            '                ", prtnaleave_tran.payperiodunkid " & _
            '                ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
            '                ", prtnaleave_tran.employeeunkid " & _
            '                ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
            '              ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername " & _
            '                       "+ ' ' + hremployee_master.surname, '') AS employeename " & _
            '                ", prtnaleave_tran.voucherno " & _
            '                ", prtnaleave_tran.processdate " & _
            '                ", prtnaleave_tran.daysinperiod " & _
            '                ", prtnaleave_tran.paidholidays " & _
            '                ", prtnaleave_tran.totalpayabledays " & _
            '                ", prtnaleave_tran.totalpayablehours " & _
            '                ", prtnaleave_tran.totaldaysworked " & _
            '                ", prtnaleave_tran.totalhoursworked " & _
            '                ", prtnaleave_tran.totalabsentorleave " & _
            '                ", prtnaleave_tran.totalextrahours " & _
            '                ", prtnaleave_tran.totalshorthours " & _
            '                ", ISNULL(prtnaleave_tran.totalonholddays,0) AS totalonholddays " & _
            '                ", prtnaleave_tran.total_amount " & _
            '                ", prtnaleave_tran.balanceamount AS balanceamount " & _
            '                ", prtnaleave_tran.userunkid " & _
            '                ", prtnaleave_tran.isvoid " & _
            '                ", prtnaleave_tran.openingbalance " & _
            '        "FROM    prtnaleave_tran " & _
            '                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
            '                "LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
            '                "LEFT JOIN prpayment_tran ON prtnaleave_tran.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
            '                                            "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
            '                                            "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
            '                                            "AND prpayment_tran.isvoid = 0 "

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            strQ = "SELECT  hremployee_master.employeeunkid " & _
                         ", hremployee_master.employeecode " & _
                         ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
                         "INTO #tblEmp " & _
                   "FROM    hremployee_master "
            'Sohail (30 Oct 2018) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'strQ &= "WHERE   ( balanceamount > 0 " & _
            '                  "OR total_amount <= 0 " & _
            '                ") " & _
            '                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                "AND prpayment_tran.paymenttranunkid IS NULL " 'Sohail (25 Jan 2011), 'Sohail (04 Mar 2011) [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " "]
            strQ &= "WHERE 1 = 1 "
            'Sohail (30 Oct 2018) -- End
            'Sohail (18 Dec 2010) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If blnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'If strFilerString.Trim.Length > 0 Then
            '    strQ &= " AND " & strFilerString
            'End If
            'Sohail (30 Oct 2018) -- End
            'Sohail (21 Aug 2015) -- End

            'Sohail (27 May 2014) -- Start
            'Enhancement - performance issue 
            'If intEmpUnkid > 0 Then
            '    strQ &= "AND prtnaleave_tran.employeeunkid = @employeeunkiid "
            '    objDataOperation.AddParameter("@employeeunkiid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid)
            'End If
            If strEmpUnkIDs.Trim <> "" Then
                'Sohail (30 Oct 2018) -- Start
                'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                'strQ &= "AND prtnaleave_tran.employeeunkid IN (" & strEmpUnkIDs & ") "
                strQ &= "AND hremployee_master.employeeunkid IN (" & strEmpUnkIDs & ") "
                'Sohail (30 Oct 2018) -- End
            End If
            'Sohail (27 May 2014) -- End

            'Sohail (24 Jun 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            If strAdvanceFilter.Trim <> "" Then
                strQ &= " AND " & strAdvanceFilter & " "
            End If
            'Sohail (24 Jun 2019) -- End

            'Sohail (11 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If CBool(strIncludeInActiveEmployee) = False Then
            '    'Sohail (18 May 2013) -- End
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "

            '    If intPeriodID > 0 Then
            '        Dim objPeriod As New clscommom_period_Tran
            '        objPeriod._Periodunkid = intPeriodID

            '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._Start_Date))
            '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._End_Date))

            '        objPeriod = Nothing
            '    Else
            '        'Sohail (18 May 2013) -- Start
            '        'TRA - ENHANCEMENT
            '        'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '        'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
            '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
            '        'Sohail (18 May 2013) -- End
            '    End If
            'End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (11 Mar 2013) -- End

            'Sohail (24 Jun 2011) -- Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select
            'strQ &= UserAccessLevel._AccessLevelFilterString 'Sohail (21 Aug 2015)
            'S.SANDEEP [ 01 JUNE 2012 ] -- END


            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Sohail (24 Jun 2011) -- End

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            strQ &= "SELECT  tnaleavetranunkid " & _
                            ", hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                            ", ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name, '') AS YearName " & _
                            ", prtnaleave_tran.payperiodunkid " & _
                            ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                            ", prtnaleave_tran.employeeunkid " & _
                            ", emp.employeecode " & _
                            ", emp.employeename " & _
                            ", prtnaleave_tran.voucherno " & _
                            ", prtnaleave_tran.processdate " & _
                            ", prtnaleave_tran.daysinperiod " & _
                            ", prtnaleave_tran.paidholidays " & _
                            ", prtnaleave_tran.totalpayabledays " & _
                            ", prtnaleave_tran.totalpayablehours " & _
                            ", prtnaleave_tran.totaldaysworked " & _
                            ", prtnaleave_tran.totalhoursworked " & _
                            ", prtnaleave_tran.totalabsentorleave " & _
                            ", prtnaleave_tran.totalextrahours " & _
                            ", prtnaleave_tran.totalshorthours " & _
                            ", ISNULL(prtnaleave_tran.totalonholddays,0) AS totalonholddays " & _
                            ", prtnaleave_tran.total_amount " & _
                            ", prtnaleave_tran.balanceamount AS balanceamount " & _
                            ", prtnaleave_tran.userunkid " & _
                            ", prtnaleave_tran.isvoid " & _
                            ", prtnaleave_tran.openingbalance " & _
                            ", ISNULL(prtnaleave_tran.costcenterunkid, 0) AS costcenterunkid " & _
                    "FROM    prtnaleave_tran " & _
                            "JOIN #tblEmp AS emp ON prtnaleave_tran.employeeunkid = emp.employeeunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                            "LEFT JOIN prpayment_tran ON prtnaleave_tran.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
                                                        "AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                                        "AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " " & _
                                                        "AND prpayment_tran.isvoid = 0 "

            strQ &= "WHERE   prtnaleave_tran.isvoid = 0 " & _
                            "AND ( balanceamount > 0 " & _
                                    "OR total_amount <= 0 " & _
                                  ") " & _
                             "AND prpayment_tran.paymenttranunkid IS NULL "

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If
            'Sohail (30 Oct 2018) -- End

            '================ Pay Year
            If intYearID > 0 Then
                strQ += " AND " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = @yearunkid"
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearID) 'Sohail (04 Mar 2011)
            End If

            '================ Pay Period
            If intPeriodID > 0 Then
                strQ += " AND prtnaleave_tran.payperiodunkid = @payperiodunkid"
                objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID) 'Sohail (04 Mar 2011)
            End If

            If strOrderby.Trim.Length > 0 Then
                strQ += " ORDER BY " & strOrderby
            End If
            'Sohail (16 Oct 2010) -- End

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            strQ &= " DROP TABLE #tblEmp "
            'Sohail (30 Oct 2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Balance_List; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetWorkingDays(ByVal intEmployeeID As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, Optional ByVal intShiftUnkId As Integer = 0) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim intWorkingDays As Integer = 0

        'Dim objShift As New clsshift_master


        Try
            'If intShiftUnkId > 0 Then
            '    'objShift._Shiftunkid = intShiftUnkId

            '    'Sohail (08 Jun 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    If strShiftDays = "" Then
            '        Dim objShift As New clsshift_master
                'objShift._Shiftunkid = intShiftUnkId
            '        strShiftDays = objShift._Shiftdays
            '        objShift = Nothing
            '    End If
            '    'Sohail (08 Jun 2012) -- End

            'Else
            '    If strShiftDays = "" Then
            '        Dim objEmployee As New clsEmployee_Master
            '        objEmployee._Employeeunkid = intEmployeeID
            '        Dim objShift As New clsshift_master
            '        objShift._Shiftunkid = objEmployee._Shiftunkid
            '        strShiftDays = objShift._Shiftdays
            '        objEmployee = Nothing
            '        objShift = Nothing
            '    End If
            'End If

                'Sohail (08 Jun 2012) -- Start
                'TRA - ENHANCEMENT
            'Dim arr As Array = objShift._Shiftdays.Split("|")
            'Dim arr As Array = strShiftDays.Split("|")
                'Sohail (08 Jun 2012) -- End

            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objShifttran As New clsshift_tran
            objShifttran.GetShiftTran(intShiftUnkId)
            'Sohail (29 Oct 2012) -- End

            While dtStartDate <= dtEndDate
                'Sohail (29 Oct 2012) -- Start
                'TRA - ENHANCEMENT
                'For i = 0 To arr.Length - 1
                'If dtStartDate.DayOfWeek.ToString.ToUpper = arr.GetValue(i).ToString.ToUpper Then
                '    intWorkingDays += 1
                '    Exit For
                'End If
                Dim intDay As Integer = GetWeekDayNumber(dtStartDate.DayOfWeek.ToString)
                Dim drRow() As DataRow = objShifttran._dtShiftday.Select("dayid = " & intDay & " AND isweekend = 0")
                If drRow.Length > 0 Then
                        intWorkingDays += 1
                    End If
                ' Next
                'Sohail (29 Oct 2012) -- End
                dtStartDate = dtStartDate.AddDays(1)
            End While

            Return intWorkingDays

        Catch ex As Exception
            Return 0
            Throw New Exception(ex.Message & "; Procedure Name: GetWorkingDays; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            'objEmployee = Nothing
            'objShift = Nothing
        End Try
    End Function

    Public Function Get_TnALeaveTranUnkID(ByVal intEmpUnkID As Integer, ByVal intPeriodUnkID As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strFilter As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  MAX(tnaleavetranunkid) AS tnaleavetranunkid " & _
                    "FROM    ( SELECT    tnaleavetranunkid " & _
                              "FROM      prtnaleave_tran " & _
                              "WHERE     payperiodunkid = @payperiodunkid " & _
                                        "AND employeeunkid = @employeeunkid " & _
                                        "AND ISNULL(isvoid,0) = 0 " & _
                              "UNION " & _
                              "SELECT    0 AS tnaleavetranunkid " & _
                            ") AS A " 'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID.ToString)
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return CInt(dsList.Tables("List").Rows(0).Item("tnaleavetranunkid").ToString)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_TnALeaveTranUnkID; Module Name: " & mstrModuleName)
            Return 0
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (04 Mar 2011) -- Start
    Public Function Get_OpeningBalance(ByVal strEmpList As String, ByVal intPeriodUnkID As Integer) As Dictionary(Of Integer, Decimal) 'Sohail (11 May 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objDicOpenBalance As New Dictionary(Of Integer, Decimal) 'Sohail (11 May 2011)
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  MAX(tnaleavetranunkid) AS tnaleavetranunkid " & _
                    "FROM    ( SELECT    tnaleavetranunkid " & _
                              "FROM      prtnaleave_tran " & _
                              "WHERE     payperiodunkid = @payperiodunkid " & _
                                        "AND employeeunkid = @employeeunkid " & _
                                        "AND ISNULL(isvoid,0) = 0 " & _
                              "UNION " & _
                              "SELECT    0 AS tnaleavetranunkid " & _
                            ") AS A "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strEmpList.ToString)
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_OpeningBalance; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return objDicOpenBalance
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prtnaleave_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim dsList As DataSet = Nothing
        'Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try
            '*****************************************************************************************************
            '********       PLEASE ADD SAME FIELD IN   CF_TnALeaveTran()  IN clsColseYear CLASS   WITHOUT FAIL
            '*****************************************************************************************************
            '********       PLEASE ADD SAME FIELD IN   CarryForward_Balance()  IN clsColsePayroll CLASS   WITHOUT FAIL
            '*****************************************************************************************************

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayperiodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherno.ToString)
            objDataOperation.AddParameter("@processdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProcessdate.ToString)
            objDataOperation.AddParameter("@daysinperiod", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblDaysinperiod.ToString)
            objDataOperation.AddParameter("@paidholidays", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblPaidholidays.ToString)
            objDataOperation.AddParameter("@totalpayabledays", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblTotalpayabledays.ToString)
            objDataOperation.AddParameter("@totalpayablehours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblTotalpayablehours.ToString)
            objDataOperation.AddParameter("@totaldaysworked", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblTotaldaysworked.ToString)
            objDataOperation.AddParameter("@totalhoursworked", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblTotalhoursworked.ToString)
            objDataOperation.AddParameter("@totalabsentorleave", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblTotalabsentorleave.ToString)
            objDataOperation.AddParameter("@totalextrahours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblTotalextrahours.ToString)
            objDataOperation.AddParameter("@totalshorthours", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblTotalshorthours.ToString)
            objDataOperation.AddParameter("@totalonholddays", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblTotalonholddays.ToString)
            objDataOperation.AddParameter("@total_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@balanceamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalanceamount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@openingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOpeningBalance.ToString) 'Sohail (11 May 2011)
            'Sohail (12 Oct 2011) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHost.ToString)
            objDataOperation.AddParameter("@void_ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
            objDataOperation.AddParameter("@void_host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
            'Sohail (12 Oct 2011) -- End
            objDataOperation.AddParameter("@tna_processdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTnA_Processdate.ToString) 'Sohail (07 Jan 2014)
            'Sohail (28 Apr 2018) -- Start
            'Enhancement : Ref. No. 231 - WebAPI for seamless integration between Aruti and EPICOR in 72.1.
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            'Sohail (28 Apr 2018) -- End
            'Sohail (09 Oct 2019) -- Start
            'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
            objDataOperation.AddParameter("@employee_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmployee_enddate.ToString)
            'Sohail (09 Oct 2019) -- End

            'Sohail (27 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            'Sohail (27 Jul 2012) -- End

            strB.Length = 0
            strB.Append("INSERT INTO prtnaleave_tran ( " & _
                              "  payperiodunkid " & _
                              ", employeeunkid " & _
                              ", voucherno " & _
                              ", processdate " & _
                              ", daysinperiod " & _
                              ", paidholidays " & _
                              ", totalpayabledays " & _
                              ", totalpayablehours " & _
                              ", totaldaysworked " & _
                              ", totalhoursworked " & _
                              ", totalabsentorleave " & _
                              ", totalextrahours " & _
                              ", totalshorthours " & _
                              ", totalonholddays " & _
                              ", total_amount " & _
                              ", balanceamount " & _
                              ", userunkid " & _
                              ", isvoid " & _
                              ", voiduserunkid " & _
                              ", voiddatetime " & _
                              ", voidreason" & _
                              ", openingbalance" & _
                              ", auditdatetime " & _
                              ", ip " & _
                              ", host " & _
                              ", void_ip " & _
                              ", void_host " & _
                              ", form_name " & _
                              ", module_name1 " & _
                              ", module_name2 " & _
                              ", module_name3 " & _
                              ", module_name4 " & _
                              ", module_name5 " & _
                              ", isweb " & _
                              ", tna_processdate " & _
                              ", costcenterunkid " & _
                              ", employee_enddate " & _
                ") VALUES (" & _
                          "  @payperiodunkid " & _
                          ", @employeeunkid " & _
                          ", @voucherno " & _
                          ", @processdate " & _
                          ", @daysinperiod " & _
                          ", @paidholidays " & _
                          ", @totalpayabledays " & _
                          ", @totalpayablehours " & _
                          ", @totaldaysworked " & _
                          ", @totalhoursworked " & _
                          ", @totalabsentorleave " & _
                          ", @totalextrahours " & _
                          ", @totalshorthours " & _
                          ", @totalonholddays " & _
                          ", @total_amount " & _
                          ", @balanceamount " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiduserunkid " & _
                          ", @voiddatetime " & _
                          ", @voidreason" & _
                          ", @openingbalance" & _
                          ", @auditdatetime " & _
                          ", @ip " & _
                          ", @host " & _
                          ", @void_ip " & _
                          ", @void_host " & _
                          ", @form_name " & _
                          ", @module_name1 " & _
                          ", @module_name2 " & _
                          ", @module_name3 " & _
                          ", @module_name4 " & _
                          ", @module_name5 " & _
                          ", @isweb " & _
                          ", @tna_processdate " & _
                          ", @costcenterunkid " & _
                          ", @employee_enddate " & _
                "); SELECT @@identity" _
                )
            'Sohail (09 Oct 2019) - [employee_enddate]
            'Sohail (28 Apr 2018) - [costcenterunkid]
            'Sohail (12 Oct 2011) - [auditdatetime, ip, host, void_ip, void_host
            'Sohail (07 Jan 2014) - [tna_processdate]

            dsList = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTnaleavetranunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (26 Aug 2021) -- Start
            'Enhancement :  : Process payroll data optimization.
            If InsertAuditTrailForTnALeaveTran_Update(mintTnaleavetranunkid, dtCurrentDateAndTime, mintUserunkid, 1, objDataOperation) = False Then
                Return False
            End If
            'Sohail (26 Aug 2021) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
    End Function
    'Sohail (04 Mar 2011) -- End

    'Sohail (12 Oct 2011) -- Start
    Private Function InsertAuditTrailForTnALeaveTran_Update(ByVal intTnALeaveTranUnkId As Integer, ByVal dtCurrentDateAndTime As Date, ByVal intUserunkid As Integer, ByVal AuditType As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (26 Aug 2021) - [AuditType]
        'Sohail (03 May 2018) - [xDataOp]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime, intUserunkid]

        Dim dsList As DataSet = Nothing
        'Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try



            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atprtnaleave_tran " & _
            '                "( tnaleavetranunkid, payperiodunkid, employeeunkid, voucherno, processdate, daysinperiod, paidholidays, totalpayabledays, totalpayablehours, totaldaysworked, totalhoursworked, totalabsentorleave, totalextrahours, totalshorthours, total_amount, balanceamount, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, totalonholddays, openingbalance, basicsalary, auditdatetime ) " & _
            '        "SELECT " & _
            '                " tnaleavetranunkid, payperiodunkid, employeeunkid, voucherno, processdate, daysinperiod, paidholidays, totalpayabledays, totalpayablehours, totaldaysworked, totalhoursworked, totalabsentorleave, totalextrahours, totalshorthours, total_amount, balanceamount, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, totalonholddays, openingbalance, basicsalary, @auditdatetime " & _
            '        "FROM prtnaleave_tran " & _
            '        "WHERE tnaleavetranunkid = @tnaleavetranunkid "
            strB.Length = 0
            strB.Append("INSERT INTO atprtnaleave_tran " & _
                         "( tnaleavetranunkid, payperiodunkid, employeeunkid, voucherno, processdate, daysinperiod, paidholidays, totalpayabledays, totalpayablehours, totaldaysworked," & _
                         " totalhoursworked, totalabsentorleave, totalextrahours, totalshorthours, total_amount, balanceamount, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, " & _
                         " totalonholddays, openingbalance, basicsalary, auditdatetime , form_name , module_name1 , module_name2 , module_name3 , module_name4 , module_name5 , isweb, tna_processdate, ip, host, audittypeid, audituserunkid, costcenterunkid, employee_enddate ) " & _
                    "SELECT " & _
                         " tnaleavetranunkid, payperiodunkid, employeeunkid, voucherno, processdate, daysinperiod, paidholidays, totalpayabledays, totalpayablehours, totaldaysworked, totalhoursworked " & _
                         ", totalabsentorleave, totalextrahours, totalshorthours, total_amount, balanceamount, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, totalonholddays, openingbalance, basicsalary " & _
                         ", @auditdatetime,@form_name , @module_name1 , @module_name2 , @module_name3 , @module_name4 , @module_name5 , @isweb, tna_processdate, @ip, @host, @audittypeid, @audituserunkid, costcenterunkid, employee_enddate " & _
                    "FROM prtnaleave_tran " & _
                    "WHERE tnaleavetranunkid = @tnaleavetranunkid " _
                    )
            'Sohail (26 Aug 2021) - [audittypeid]
            'Sohail (09 Oct 2019) - [employee_enddate]
            'Sohail (28 Apr 2018) - [ip, host, costcenterunkid]
            'Sohail (07 Jan 2014) - [tna_processdate]
            'S.SANDEEP [ 19 JULY 2012 ] -- END


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTnALeaveTranUnkId.ToString)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            'Sohail (27 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            'Sohail (21 Aug 2015) -- End
            'Sohail (27 Jul 2012) -- End

            'Sohail (28 Apr 2018) -- Start
            'Enhancement : Ref. No. 231 - WebAPI for seamless integration between Aruti and EPICOR in 72.1.
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHost.ToString)
            'Sohail (28 Apr 2018) -- End
            'Sohail (26 Aug 2021) -- Start
            'Enhancement :  : Process payroll data optimization.
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            'Sohail (26 Aug 2021) -- End

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes
            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ExecNonQuery(strB.ToString)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForTnALeaveTran_Update; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try

    End Function

    'Sohail (26 Aug 2021) -- Start
    'Enhancement :  : Process payroll data optimization.
    Public Function IsTableDataUpdate(ByVal intUnkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strFields As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strFields = " payperiodunkid,employeeunkid,voucherno,processdate,daysinperiod,paidholidays,totalpayabledays,totalpayablehours,totaldaysworked,totalhoursworked,totalabsentorleave,totalextrahours,totalshorthours,total_amount,balanceamount,totalonholddays,openingbalance,basicsalary,tna_processdate,costcenterunkid,employee_enddate "

            strQ = "WITH CTE AS ( " & _
                        "SELECT * " & _
                        "FROM " & _
                        "( " & _
                            "SELECT TOP 1 " & strFields & " " & _
                            "FROM atprtnaleave_tran " & _
                            "WHERE atprtnaleave_tran.tnaleavetranunkid = @tnaleavetranunkid " & _
                            "/*AND atprtnaleave_tran.audittypeid <> 3*/ " & _
                            "ORDER BY atprtnaleave_tran.attnaleavetranunkid DESC " & _
                        ") AS A) " & _
                    " " & _
                    "SELECT " & strFields & " " & _
                    "FROM prtnaleave_tran " & _
                    "WHERE prtnaleave_tran.tnaleavetranunkid = @tnaleavetranunkid " & _
                    "EXCEPT " & _
                    "SELECT * FROM cte "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (26 Aug 2021) -- End

    'Sohail (22 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function VoidPayroll(ByVal strEmployeeList As String _
                                , ByVal intPeriodUnkID As Integer _
                                , ByVal dtPeriodStartDate As DateTime _
                                , ByVal intVoidUserUnkID As Integer _
                                , ByVal dtVoidDateTime As DateTime _
                                , ByVal strVoidReason As String _
                                , ByVal blnApplyPayPerActivity As Boolean _
                                ) As Boolean

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objPayroll As New clsPayrollProcessTran

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "UPDATE  prtnaleave_tran " & _
                    "SET     daysinperiod = 0 " & _
                          ", paidholidays = 0 " & _
                          ", totalpayabledays = 0 " & _
                          ", totalpayablehours = 0 " & _
                          ", totaldaysworked = 0 " & _
                          ", totalhoursworked = 0 " & _
                          ", totalabsentorleave = 0 " & _
                          ", totalextrahours = 0 " & _
                          ", totalshorthours = 0 " & _
                          ", totalonholddays = 0 " & _
                          ", total_amount = 0 " & _
                          ", balanceamount = openingbalance " & _
                          ", processdate = @startdate " & _
                          ", void_ip = @void_ip " & _
                          ", void_host = @void_host " & _
                          ", tna_processdate = @startdate " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND payperiodunkid = @perioidunkid " & _
                            "AND employeeunkid IN (" & strEmployeeList & ") " 'Sohail (27 Jul 2012) - [void_ip, void_host]
            'Sohail (07 Jan 2014) - [tna_processdate]

            objDataOperation.AddParameter("@perioidunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtPeriodStartDate)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserUnkID.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            'Sohail (27 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@void_ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP.ToString)
            objDataOperation.AddParameter("@void_host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHost.ToString)
            'Sohail (27 Jul 2012) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (26 Aug 2021) -- Start
            'Enhancement :  : Process payroll data optimization.
            strQ &= "INSERT INTO atprtnaleave_tran " & _
                         "( tnaleavetranunkid, payperiodunkid, employeeunkid, voucherno, processdate, daysinperiod, paidholidays, totalpayabledays, totalpayablehours, totaldaysworked," & _
                         " totalhoursworked, totalabsentorleave, totalextrahours, totalshorthours, total_amount, balanceamount, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, " & _
                         " totalonholddays, openingbalance, basicsalary, auditdatetime , form_name , module_name1 , module_name2 , module_name3 , module_name4 , module_name5 , isweb, tna_processdate, ip, host, audittypeid, audituserunkid, costcenterunkid, employee_enddate ) " & _
                    "SELECT " & _
                         " tnaleavetranunkid, payperiodunkid, employeeunkid, voucherno, processdate, daysinperiod, paidholidays, totalpayabledays, totalpayablehours, totaldaysworked, totalhoursworked " & _
                         ", totalabsentorleave, totalextrahours, totalshorthours, total_amount, balanceamount, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, totalonholddays, openingbalance, basicsalary " & _
                         ", @auditdatetime,@form_name , @module_name1 , @module_name2 , @module_name3 , @module_name4 , @module_name5 , @isweb, tna_processdate, @ip, @host, @audittypeid, @audituserunkid, costcenterunkid, employee_enddate " & _
                    "FROM prtnaleave_tran " & _
                    "WHERE isvoid = 0 " & _
                     "AND payperiodunkid = @perioidunkid " & _
                            "AND employeeunkid IN (" & strEmployeeList & ")"

            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHost.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, 3)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserUnkID)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (26 Aug 2021) -- End


            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'If objPayroll.Void(strEmployeeList, intPeriodUnkID, intVoidUserUnkID, dtVoidDateTime, strVoidReason) = False Then
            If objPayroll.Void(strEmployeeList, intPeriodUnkID, intVoidUserUnkID, dtVoidDateTime, strVoidReason, objDataOperation) = False Then
                'Sohail (26 Aug 2016) -- End
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objPayroll._Message)
                Throw exForce
            End If

            'Sohail (04 Jan 2014) -- Start
            'Enhancement - PPA
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If ConfigParameter._Object._ApplyPayPerActivity = True Then
            If blnApplyPayPerActivity = True Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ClearParameters()

                objDataOperation.AddParameter("@perioidunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID.ToString)
                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 2) 'Update
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                'Sohail (21 Aug 2015) -- End

                strQ = "UPDATE  prpayactivity_tran " & _
                        "SET     activity_rate = 0 " & _
                              ", amount = 0 " & _
                        "WHERE   ISNULL(isvoid, 0) = 0 " & _
                                "AND isposted = 1 " & _
                                "AND tranheadunkid > 0 " & _
                                "AND periodunkid = @perioidunkid " & _
                                "AND employeeunkid IN (" & strEmployeeList & ") "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "INSERT INTO atprpayactivity_tran " & _
                                "( payactivitytranunkid, employeeunkid, periodunkid, costcenterunkid, activityunkid, isformula_based, tranheadunkid, activity_date, activity_value, activity_rate, amount, userunkid, isapproved, approveruserunkid, isposted, isvoid, voiddatetime, voidreason, voiduserunkid, normal_factor, ph_factor, normal_hrs, ot_hrs, normal_amount, ot_amount, audittype, audituserunkid, auditdatetime, ip, host, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, loginemployeeunkid, ot_tranheadunkid, ph_tranheadunkid ) " & _
                        "SELECT    payactivitytranunkid " & _
                                  ", employeeunkid " & _
                                  ", periodunkid " & _
                                  ", costcenterunkid " & _
                                  ", activityunkid " & _
                                  ", isformula_based " & _
                                  ", tranheadunkid " & _
                                  ", activity_date " & _
                                  ", activity_value " & _
                                  ", activity_rate " & _
                                  ", amount " & _
                                  ", userunkid " & _
                                  ", isapproved " & _
                                  ", approveruserunkid " & _
                                  ", isposted " & _
                                  ", isvoid " & _
                                  ", voiddatetime " & _
                                  ", voidreason " & _
                                  ", voiduserunkid " & _
                                  ", normal_factor " & _
                                  ", ph_factor " & _
                                  ", normal_hrs " & _
                                  ", ot_hrs " & _
                                  ", normal_amount " & _
                                  ", ot_amount " & _
                                  ", @audittype " & _
                                  ", @audituserunkid " & _
                                  ", @auditdatetime " & _
                                  ", '" & getIP() & "' " & _
                                  ", '" & getHostName() & "' " & _
                                  ", '" & mstrForm_Name & "' " & _
                                  ", '" & StrModuleName1 & "' " & _
                                  ", '" & StrModuleName2 & "' " & _
                                  ", '" & StrModuleName3 & "' " & _
                                  ", '" & StrModuleName4 & "' " & _
                                  ", '" & StrModuleName5 & "' " & _
                                  ", 0 /*isweb - bit*/ " & _
                                  ", 0  /*loginemployeeunkid - int*/ " & _
                                  ", ot_tranheadunkid " & _
                                  ", ph_tranheadunkid " & _
                        "FROM     prpayactivity_tran " & _
                        "WHERE   ISNULL(isvoid, 0) = 0 " & _
                                "AND isposted = 1 " & _
                                "AND tranheadunkid > 0 " & _
                                "AND periodunkid = @perioidunkid " & _
                                "AND employeeunkid IN (" & strEmployeeList & ") "
                'Sohail (10 Jul 2018)  - [ot_tranheadunkid, ph_tranheadunkid], [FROM     atprpayactivity_tran = FROM     prpayactivity_tran]

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'Sohail (04 Jan 2014) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidPayroll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (22 May 2012) -- End

    'Sohail (08 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    'Public Function GetEmployeeDetail(ByVal strList As String, ByVal strEmployeeList As String, ByVal dtPeriodEnd As Date) As DataTable
    Public Function GetEmployeeDetail(ByVal xDatabaseName As String _
                                      , ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer _
                                      , ByVal xCompanyUnkid As Integer _
                                      , ByVal xPeriodStart As Date _
                                      , ByVal xPeriodEnd As Date _
                                      , ByVal xUserModeSetting As String _
                                      , ByVal xOnlyApproved As Boolean _
                                      , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                      , ByVal blnApplyUserAccessFilter As Boolean _
                                      , ByVal strList As String _
                                      , ByVal strEmployeeList As String _
                                      , ByVal dtPeriodEnd As Date _
                                      , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                      ) As DataTable
        'Sohail (03 May 2018) - [xDataOp]
        'Sohail (15 Dec 2015) -- End
        Dim dtTable As DataTable = Nothing
        'Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            'Sohail (24 Aug 2012) -- Start
            'TRA - ENHANCEMENT - The query processor ran out of internal resources and could not produce a query plan. This is a rare event and only expected for extremely complex queries or queries that reference a very large number of tables or partitions. Please simplify the query. If you believe you have received this message in error, contact Customer Support Services for more information.
            'strQ = "SELECT  hremployee_master.employeeunkid " & _
            '              ", hremployee_master.gradeunkid " & _
            '              ", hremployee_master.gradelevelunkid " & _
            '              ", hremployee_master.appointeddate " & _
            '              ", hremployee_master.termination_from_date " & _
            '              ", hremployee_master.termination_to_date " & _
            '              ", hremployee_master.empl_enddate " & _
            '              ", hremployee_master.gradeunkid " & _
            '              ", hremployee_master.gradelevelunkid " & _
            '              ", hremployee_master.costcenterunkid " & _
            '              ", hremployee_master.tranhedunkid " & _
            '              ", prtranhead_master.calctype_id " & _
            '              ", prsalaryincrement_tran.newscale " & _
            '              ", hremployee_master.shiftunkid " & _
            '              ", ISNULL(tnashift_master.shiftdays, '') AS shiftdays " & _
            '              ", ED.periodunkid AS CurrentEDSlabPeriodUnkId " & _
            '        "FROM    hremployee_master " & _
            '                "LEFT JOIN prsalaryincrement_tran ON hremployee_master.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
            '                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hremployee_master.tranhedunkid " & _
            '                "LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid " & _
            '                "LEFT JOIN ( SELECT  B.employeeunkid " & _
            '                                  ", cfcommon_period_tran.periodunkid " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", CONVERT(CHAR(8), MAX(end_date), 112) AS enddate " & _
            '                                      "FROM      prearningdeduction_master " & _
            '                                                "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                                                 "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            '                                                                 "AND cfcommon_period_tran.isactive = 1 " & _
            '                                      "WHERE     ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
            '                                                "AND prearningdeduction_master.employeeunkid IN ( " & strEmployeeList & " ) " & _
            '                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @enddate " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                    ") AS B " & _
            '                                    "JOIN cfcommon_period_tran ON B.enddate = CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) " & _
            '                                                                 "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            '                                                                 "AND cfcommon_period_tran.isactive = 1 " & _
            '                          ") AS ED ON ed.employeeunkid = hremployee_master.employeeunkid " & _
            '                "JOIN ( SELECT   employeeunkid " & _
            '                              ", MAX(incrementdate) AS incrementdate " & _
            '                              ", MAX(salaryincrementtranunkid) AS salaryincrementtranunkid " & _
            '                       "FROM     prsalaryincrement_tran " & _
            '                       "WHERE    ISNULL(isvoid, 0) = 0 " & _
            '                                "AND employeeunkid IN ( " & strEmployeeList & " ) " & _
            '                                "AND incrementdate <= @enddate " & _
            '                       "GROUP BY employeeunkid " & _
            '                     ") AS A ON prsalaryincrement_tran.salaryincrementtranunkid = A.salaryincrementtranunkid " & _
            '        "WHERE   ISNULL(prsalaryincrement_tran.isvoid, 0) = 0 " & _
            '        "AND hremployee_master.employeeunkid IN ( " & strEmployeeList & " ) "
            'Sohail (18 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'strQ = "CREATE TABLE #TableEmp ( EmpID INT ) " & _
            '                   "INSERT  INTO #TableEmp " & _
            '                           "( EmpID ) " & _
            '                           "SELECT  employeeunkid " & _
            '                           "FROM    hremployee_master " & _
            '                           "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
            '        "SELECT  hremployee_master.employeeunkid " & _
            '              ", hremployee_master.gradeunkid " & _
            '              ", hremployee_master.gradelevelunkid " & _
            '              ", hremployee_master.appointeddate " & _
            '              ", hremployee_master.termination_from_date " & _
            '              ", hremployee_master.termination_to_date " & _
            '              ", hremployee_master.empl_enddate " & _
            '              ", hremployee_master.costcenterunkid " & _
            '              ", hremployee_master.tranhedunkid " & _
            '              ", prtranhead_master.calctype_id " & _
            '              ", prsalaryincrement_tran.newscale " & _
            '              ", hremployee_master.shiftunkid " & _
            '              ", ED.periodunkid AS CurrentEDSlabPeriodUnkId " & _
            '        "FROM    hremployee_master " & _
            '                "LEFT JOIN prsalaryincrement_tran ON hremployee_master.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
            '                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hremployee_master.tranhedunkid " & _
            '                "LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid " & _
            '                "LEFT JOIN ( SELECT  B.employeeunkid " & _
            '                                  ", cfcommon_period_tran.periodunkid " & _
            '                            "FROM    ( SELECT    employeeunkid " & _
            '                                              ", CONVERT(CHAR(8), MAX(end_date), 112) AS enddate " & _
            '                                      "FROM      prearningdeduction_master " & _
            '                                                "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                                           "RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  prearningdeduction_master.employeeunkid " & _
            '                                                 "WHERE     ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
            '                                                                 "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            '                                                                 "AND cfcommon_period_tran.isactive = 1 " & _
            '                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @enddate " & _
            '                                      "GROUP BY  employeeunkid " & _
            '                                    ") AS B " & _
            '                                    "JOIN cfcommon_period_tran ON B.enddate = CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) " & _
            '                                                                 "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            '                                                                 "AND cfcommon_period_tran.isactive = 1 " & _
            '                          ") AS ED ON ed.employeeunkid = hremployee_master.employeeunkid " & _
            '                "JOIN ( SELECT   employeeunkid " & _
            '                              ", MAX(incrementdate) AS incrementdate " & _
            '                              ", MAX(salaryincrementtranunkid) AS salaryincrementtranunkid " & _
            '                       "FROM     prsalaryincrement_tran " & _
            '                                  "RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  prsalaryincrement_tran.employeeunkid " & _
            '                       "WHERE    ISNULL(isvoid, 0) = 0 " & _
            '                                "AND CONVERT(CHAR(8),incrementdate,112) <= @enddate " & _
            '                                "AND prsalaryincrement_tran.isapproved = 1 " & _
            '                       "GROUP BY employeeunkid " & _
            '                     ") AS A ON prsalaryincrement_tran.salaryincrementtranunkid = A.salaryincrementtranunkid " & _
            '                   "RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  hremployee_master.employeeunkid " & _
            '                   "WHERE   ISNULL(prsalaryincrement_tran.isvoid, 0) = 0 "
            'strQ &= " DROP TABLE #TableEmp "
            strB.Length = 0
            strB.Append("CREATE TABLE #TableEmp ( EmpID INT, empcode NVARCHAR(510) ) " & _
                                         "INSERT  INTO #TableEmp " & _
                                                 "( EmpID, empcode ) " & _
                                                 "SELECT  employeeunkid, employeecode " & _
                                                 "FROM    hremployee_master " & _
                                                 "WHERE   employeeunkid IN ( " & strEmployeeList & " ) " & _
                              "SELECT  hremployee_master.employeeunkid " & _
                                    ", #TableEmp.empcode " & _
                                    ", G.gradeunkid " & _
                                    ", G.gradelevelunkid " & _
                                    ", hremployee_master.appointeddate " & _
                                    ", ETERM.termination_from_date " & _
                                    ", ERET.termination_to_date " & _
                                    ", ETERM.empl_enddate " & _
                                    ", C.costcenterunkid " & _
                                    ", hremployee_master.tranhedunkid " & _
                                    ", prtranhead_master.calctype_id " & _
                                    ", prsalaryincrement_tran.newscale " & _
                                    ", hremployee_master.shiftunkid " & _
                                    ", ED.periodunkid AS CurrentEDSlabPeriodUnkId " & _
                                    ", ISNULL(prsalaryincrement_tran.newscale, 0) - ISNULL(b.oldscale, prsalaryincrement_tran.newscale) AS incrementamount " & _
                              "FROM    hremployee_master " & _
                                      "LEFT JOIN prsalaryincrement_tran ON hremployee_master.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
                                      "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hremployee_master.tranhedunkid " & _
                                      "LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid " & _
                                      "LEFT JOIN ( SELECT  B.employeeunkid " & _
                                                        ", cfcommon_period_tran.periodunkid " & _
                                                  "FROM    ( SELECT    employeeunkid " & _
                                                                    ", CONVERT(CHAR(8), MAX(end_date), 112) AS enddate " & _
                                                            "FROM      prearningdeduction_master " & _
                                                                      "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                 "RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  prearningdeduction_master.employeeunkid " & _
                                                                       "WHERE     ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                                                                                       "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                                                                       "AND cfcommon_period_tran.isactive = 1 " & _
                                                                      "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @enddate " & _
                                                            "GROUP BY  employeeunkid " & _
                                                          ") AS B " & _
                                                          "JOIN cfcommon_period_tran ON B.enddate = CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) " & _
                                                                                       "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                                                                       "AND cfcommon_period_tran.isactive = 1 " & _
                                                ") AS ED ON ed.employeeunkid = hremployee_master.employeeunkid " & _
                                      "LEFT JOIN ( SELECT   employeeunkid " & _
                                                    ", MAX(incrementdate) AS incrementdate " & _
                                                    ", MAX(salaryincrementtranunkid) AS salaryincrementtranunkid " & _
                                             "FROM     prsalaryincrement_tran " & _
                                                        "RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  prsalaryincrement_tran.employeeunkid " & _
                                             "WHERE    ISNULL(isvoid, 0) = 0 " & _
                                                      "AND CONVERT(CHAR(8),incrementdate,112) <= @enddate " & _
                                                      "AND prsalaryincrement_tran.isapproved = 1 " & _
                                             "GROUP BY employeeunkid " & _
                                           ") AS A ON prsalaryincrement_tran.salaryincrementtranunkid = A.salaryincrementtranunkid " & _
                                         "RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  hremployee_master.employeeunkid " & _
                                                "AND A.employeeunkid = #TableEmp.EmpID " & _
                                        "LEFT JOIN ( SELECT  c.employeeunkid " & _
                                                          ", newscale AS oldscale " & _
                                                    "FROM    ( SELECT    employeeunkid " & _
                                                                      ", MAX(incrementdate) AS incrementdate " & _
                                                                      ", MAX(salaryincrementtranunkid) AS salaryincrementtranunkid " & _
                                                              "FROM      prsalaryincrement_tran " & _
                                                                        "RIGHT JOIN #TableEmp ON #TableEmp.EmpId = prsalaryincrement_tran.employeeunkid " & _
                                                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                                                        "AND CONVERT(CHAR(8), incrementdate, 112) < @startdate " & _
                                                                        "AND prsalaryincrement_tran.isapproved = 1 " & _
                                                              "GROUP BY  employeeunkid " & _
                                                            ") AS c " & _
                                                            "JOIN prsalaryincrement_tran ON c.salaryincrementtranunkid = dbo.prsalaryincrement_tran.salaryincrementtranunkid " & _
                                                  ") AS b ON b.employeeunkid = hremployee_master.employeeunkid " & _
                                         "LEFT JOIN " & _
                                                "( " & _
                                                "	SELECT " & _
                                                "		 gradegroupunkid " & _
                                                "		,gradeunkid " & _
                                                "		,gradelevelunkid " & _
                                                "		,employeeunkid " & _
                                                "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                                                "	FROM prsalaryincrement_tran " & _
                                                "   RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  prsalaryincrement_tran.employeeunkid " & _
                                                "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                                ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                                         "LEFT JOIN " & _
                                                "( " & _
                                                "    SELECT " & _
                                                "         cctranheadvalueid AS costcenterunkid " & _
                                                "        ,employeeunkid " & _
                                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                                "    FROM hremployee_cctranhead_tran " & _
                                                "    RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  hremployee_cctranhead_tran.employeeunkid " & _
                                                "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                                ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                                         "LEFT JOIN " & _
                                                "( " & _
                                                "   SELECT " & _
                                                "		 TERM.TEEmpId " & _
                                                "		,TERM.empl_enddate " & _
                                                "		,TERM.termination_from_date " & _
                                                "		,TERM.TEfDt " & _
                                                "		,TERM.isexclude_payroll " & _
                                                "		,TERM.TR_REASON " & _
                                                "		,TERM.changereasonunkid " & _
                                                "   FROM " & _
                                                "   ( " & _
                                                "       SELECT " & _
                                                "            TRM.employeeunkid AS TEEmpId " & _
                                                "           ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                                                "           ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                                                "           ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                                                "           ,TRM.isexclude_payroll " & _
                                                "			,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                                "			,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                                                "           ,TRM.changereasonunkid " & _
                                                "       FROM hremployee_dates_tran AS TRM " & _
                                                "			LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                                "			LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                                                "           RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  TRM.employeeunkid " & _
                                                "		WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                                "   ) AS TERM WHERE TERM.Rno = 1 " & _
                                                ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                                         "LEFT JOIN " & _
                                                "( " & _
                                                "   SELECT " & _
                                                "		 RET.REmpId " & _
                                                "		,RET.termination_to_date " & _
                                                "		,RET.REfDt " & _
                                                "		,RET.RET_REASON " & _
                                                "   FROM " & _
                                                "   ( " & _
                                                "       SELECT " & _
                                                "            RTD.employeeunkid AS REmpId " & _
                                                "           ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                                                "           ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                                                "           ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                                                "			,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                                                "       FROM hremployee_dates_tran AS RTD " & _
                                                "			LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                                "			LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                                                "           RIGHT JOIN #TableEmp ON #TableEmp.EmpId =  RTD.employeeunkid " & _
                                                "		WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                                "   ) AS RET WHERE RET.Rno = 1 " & _
                                                ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                                         "WHERE   ISNULL(prsalaryincrement_tran.isvoid, 0) = 0 " _
                                         )
            strB.Append(" DROP TABLE #TableEmp ")
            'Sohail (02 Jan 2017) - [AND A.employeeunkid = #TableEmp.EmpID]
            'Sohail (21 Oct 2016) - [#TableEmp.empcode]
            'Sohail (18 Nov 2013) -- End
            'Sohail (24 Sep 2012) - [isapproved]
            'Sohail (24 Aug 2012) -- End

            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))

            Dim ds As DataSet = objDataOperation.ExecQuery(strB.ToString, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = ds.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeDetail; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dtTable IsNot Nothing Then dtTable.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dtTable
    End Function
    'Sohail (08 Jun 2012) -- End

    'Sohail (29 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetOverDeductionList(ByVal strList As String _
                                         , ByVal intPeriodId As Integer _
                                         , ByVal blnIncludeZeroBalance As Boolean _
                                         , Optional ByVal strEmployeeList As String = "" _
                                         , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                         ) As DataTable
        'Sohail (03 May 2018) - [xDataOp]
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            strQ = "SELECT DISTINCT " & _
                            "prtnaleave_tran.employeeunkid " & _
                          ", balanceamount " & _
                    "FROM    prtnaleave_tran " & _
                            "JOIN prpayrollprocess_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "LEFT JOIN prpayment_tran ON prpayment_tran.referencetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "AND prpayment_tran.referenceid = 3 " & _
                                                        "AND prpayment_tran.paytypeid = 1 " & _
                    "WHERE   ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND prpayment_tran.paymenttranunkid IS NULL "

            If intPeriodId > 0 Then
                strQ &= "AND payperiodunkid = @payperiodunkid "
                objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            If blnIncludeZeroBalance = True Then
                strQ &= "AND balanceamount <= 0 "
            Else
                strQ &= "AND balanceamount < 0 "
            End If

            If strEmployeeList.Trim <> "" Then
                strQ &= "AND prtnaleave_tran.employeeunkid IN (" & strEmployeeList & ") "
            End If


            dtTable = objDataOperation.ExecQuery(strQ, strList).Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetOverDeductionList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dtTable IsNot Nothing Then dtTable.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dtTable
    End Function
    'Sohail (29 Jun 2012) -- End

    'Sohail (24 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function IsPayrollProcessDone(ByVal intPeriodUnkID As Integer _
                                        , ByVal strEmployeeUnkIDs As String _
                                        , ByVal dtPeriodEndDate As DateTime _
                                        , Optional ByVal intModuleReference As Integer = enModuleReference.Payroll _
                                        , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                        , Optional ByRef dtEmpList As DataTable = Nothing _
                                        ) As Boolean
        'Sohail (03 May 2018) - [xDataOp]
        '                               'Sohail (07 Jan 2014) - [intModuleReference]
        '                               'Sohail (24 Sep 2012) - [intEmployeeUnkID = strEmployeeUnkIDs]

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        'Sohail (12 Mar 2020) -- Start
        'NMB Enhancement # : Performance enhancement on employee cost center screen.
        dtEmpList = Nothing
        'Sohail (12 Mar 2020) -- End

        Try

            strQ = "SELECT  prtnaleave_tran.employeeunkid " & _
                            ", REPLACE(ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, ''), '  ',' ') AS employeename " & _
                            ", hremployee_master.employeecode " & _
                    "FROM    prtnaleave_tran " & _
                            "JOIN hremployee_master ON hremployee_master.employeeunkid = prtnaleave_tran.employeeunkid " & _
                    "WHERE   ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @payperiodunkid "

            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            If strEmployeeUnkIDs.Trim.Length > 0 Then
                strQ &= "AND prtnaleave_tran.employeeunkid IN (" & strEmployeeUnkIDs & ") "
            End If
            'Pinkal (30-Jun-2021) -- End


            'Sohail (12 Mar 2020) - Removed =[tnaleavetranunkid], Added =[employeename, employeecode, JOIN hremployee_master]
            '"AND CONVERT(CHAR(8), processdate, 112) = @processdate "
            '               'Sohail (24 Sep 2012) - [employeeunkid = @employeeunkid = employeeunkid IN (strEmployeeUnkIDs)]

            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods

            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            If dtPeriodEndDate <> Nothing Then
            If intModuleReference = enModuleReference.Payroll Then
                strQ &= " AND CONVERT(CHAR(8), processdate, 112) = @processdate "
            ElseIf intModuleReference = enModuleReference.TnA Then
                strQ &= " AND CONVERT(CHAR(8), tna_processdate, 112) = @processdate "
            End If
            End If
            'Pinkal (30-Jun-2021) -- End

            'Sohail (07 Jan 2014) -- End


            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID.ToString)
            'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID.ToString) 'Sohail (24 Sep 2012)
            'Sohail (12 Mar 2020) -- Start
            'NMB Enhancement # : Performance enhancement on employee cost center screen.
            'objDataOperation.AddParameter("@processdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtPeriodEndDate)
            objDataOperation.AddParameter("@processdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))
            'Sohail (12 Mar 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Mar 2020) -- Start
            'NMB Enhancement # : Performance enhancement on employee cost center screen.
            If dsList.Tables("List").Rows.Count > 0 Then
                dtEmpList = dsList.Tables(0).Copy
                dtEmpList.Columns.Remove("employeeunkid")
            End If
            'Sohail (12 Mar 2020) -- End

            Return dsList.Tables("List").Rows.Count > 0
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: IsPayrollProcessDone; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function
    'Sohail (24 Aug 2012) -- End


    'Sohail (12 Oct 2011) -- End


    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prtnaleave_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintTnaleavetranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttnaleavetranunkid.ToString)
    '        objDataOperation.AddParameter("@payperiodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpayperiodunkid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
    '        objDataOperation.AddParameter("@voucherno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoucherno.ToString)
    '        objDataOperation.AddParameter("@processdate", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtprocessdate)
    '        objDataOperation.AddParameter("@daysinperiod", SqlDbType.int, eZeeDataType.INT_SIZE, mintdaysinperiod.ToString)
    '        objDataOperation.AddParameter("@paidholidays", SqlDbType.int, eZeeDataType.INT_SIZE, mintpaidholidays.ToString)
    '        objDataOperation.AddParameter("@totalpayabledays", SqlDbType.int, eZeeDataType.INT_SIZE, minttotalpayabledays.ToString)
    '        objDataOperation.AddParameter("@totalpayablehours", SqlDbType.float, eZeeDataType.MONEY_SIZE, mdbltotalpayablehours.ToString)
    '        objDataOperation.AddParameter("@totaldaysworked", SqlDbType.float, eZeeDataType.MONEY_SIZE, mdbltotaldaysworked.ToString)
    '        objDataOperation.AddParameter("@totalhoursworked", SqlDbType.float, eZeeDataType.MONEY_SIZE, mdbltotalhoursworked.ToString)
    '        objDataOperation.AddParameter("@totalabsentorleave", SqlDbType.int, eZeeDataType.INT_SIZE, minttotalabsentorleave.ToString)
    '        objDataOperation.AddParameter("@totalextrahours", SqlDbType.float, eZeeDataType.MONEY_SIZE, mdbltotalextrahours.ToString)
    '        objDataOperation.AddParameter("@totalshorthours", SqlDbType.float, eZeeDataType.MONEY_SIZE, mdbltotalshorthours.ToString)
    '        objDataOperation.AddParameter("@balanceamount", SqlDbType.float, eZeeDataType.MONEY_SIZE, mdblbalanceamount.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

    '        StrQ = "UPDATE prtnaleave_tran SET " & _
    '          "  payperiodunkid = @payperiodunkid" & _
    '          ", employeeunkid = @employeeunkid" & _
    '          ", voucherno = @voucherno" & _
    '          ", processdate = @processdate" & _
    '          ", daysinperiod = @daysinperiod" & _
    '          ", paidholidays = @paidholidays" & _
    '          ", totalpayabledays = @totalpayabledays" & _
    '          ", totalpayablehours = @totalpayablehours" & _
    '          ", totaldaysworked = @totaldaysworked" & _
    '          ", totalhoursworked = @totalhoursworked" & _
    '          ", totalabsentorleave = @totalabsentorleave" & _
    '          ", totalextrahours = @totalextrahours" & _
    '          ", totalshorthours = @totalshorthours" & _
    '          ", balanceamount = @balanceamount" & _
    '          ", userunkid = @userunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason " & _
    '        "WHERE tnaleavetranunkid = @tnaleavetranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tnaleavetranunkid " & _
    '          ", payperiodunkid " & _
    '          ", employeeunkid " & _
    '          ", voucherno " & _
    '          ", processdate " & _
    '          ", daysinperiod " & _
    '          ", paidholidays " & _
    '          ", totalpayabledays " & _
    '          ", totalpayablehours " & _
    '          ", totaldaysworked " & _
    '          ", totalhoursworked " & _
    '          ", totalabsentorleave " & _
    '          ", totalextrahours " & _
    '          ", totalshorthours " & _
    '          ", balanceamount " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM prtnaleave_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND tnaleavetranunkid <> @tnaleavetranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class