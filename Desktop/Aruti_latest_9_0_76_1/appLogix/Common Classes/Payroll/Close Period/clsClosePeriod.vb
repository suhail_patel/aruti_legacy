﻿Imports eZeeCommonLib
Imports System.ComponentModel

Public Class clsClosePeriod

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsClosePeriod"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

    Private mintPeriodUnkId As Integer
    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
    Private mintNextPeriodUnkId As Integer = 0
    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtNextPeriodStartDate As DateTime
    Private mdtNextPeriodEndDate As DateTime
    Private mblnCopyClosingPeriodEDSlab As Boolean = False
    'Sohail (21 Jul 2012) -- End

    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    Private mdtTnAStartDate As Date
    Private mdtTnAEndDate As Date
    'Sohail (07 Jan 2014) -- End

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Showing Progress of closing period.
    Private Shared mintProgressTotalCount As Integer
    Private Shared mintProgressCurrCount As Integer
    'Sohail (12 Dec 2015) -- End

#End Region

#Region " Public Properties "

    Public WriteOnly Property _PeriodUnkId() As Integer
        Set(ByVal value As Integer)
            mintPeriodUnkId = value
        End Set
    End Property

    Public WriteOnly Property _NexPeriodUnkId() As Integer
        Set(ByVal value As Integer)
            mintNextPeriodUnkId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodEndDate = value
        End Set
    End Property

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _NextPeriodStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtNextPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _NextPeriodEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtNextPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _CopyClosingPeriodEDSlab() As Boolean
        Set(ByVal value As Boolean)
            mblnCopyClosingPeriodEDSlab = value
        End Set
    End Property
    'Sohail (21 Jul 2012) -- End

    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    Public WriteOnly Property _TnAStartDate() As Date
        Set(ByVal value As DateTime)
            mdtTnAStartDate = value
        End Set
    End Property

    Public WriteOnly Property _TnAEndDate() As Date
        Set(ByVal value As DateTime)
            mdtTnAEndDate = value
        End Set
    End Property
    'Sohail (07 Jan 2014) -- End

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Showing Progress of closing period.
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Shared Property _ProgressTotalCount() As Integer
        Get
            Return mintProgressTotalCount
        End Get
        Set(ByVal value As Integer)
            mintProgressTotalCount = value
        End Set
    End Property

    Public Shared Property _ProgressCurrCount() As Integer
        Get
            Return mintProgressCurrCount
        End Get
        Set(ByVal value As Integer)
            mintProgressCurrCount = value
        End Set
    End Property
    'Sohail (12 Dec 2015) -- End

#End Region

#Region " Public Functions "

    Public Function Close_Period(ByVal strDatabaseName As String _
                                 , ByVal xUserUnkid As Integer _
                                 , ByVal xYearUnkid As Integer _
                                 , ByVal xCompanyUnkid As Integer _
                                 , ByVal xPeriodStart As DateTime _
                                 , ByVal xPeriodEnd As DateTime _
                                 , ByVal xUserModeSetting As String _
                                 , ByVal xOnlyApproved As Boolean _
                                 , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                 , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                 , ByVal dtCurrentDateAndTime As Date _
                                 , ByVal dtDatabaseStartDate As Date _
                                 , ByVal dtDatabaseEndDate As Date _
                                 , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                 , Optional ByVal strFilerString As String = "" _
                                 , Optional ByVal bw As BackgroundWorker = Nothing _
                                 ) As Boolean
        'Sohail (12 Dec 2015) - [bw]
        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtDatabaseStartDate, dtDatabaseEndDate, blnApplyUserAccessFilter, strFilerString]
        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        Try

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.

            '*** Make current year database SINGLE USER only. 
            'Sohail (05 Oct 2016) -- Start
            'Enhancement - 63.1 - Common Function to check if Aruti is running on other machines. (Now please call this query from close perid From)
            'strQ = "SELECT   hostname " & _
            '       "FROM    sys.sysprocesses " & _
            '       "WHERE   dbid > 0 " & _
            '               "AND DB_NAME(dbid) = '" & strDatabaseName & "' " & _
            '               "AND hostname <> HOST_NAME() "

            'dsList = objDataOperation.ExecQuery(strQ, "ConnectedHost")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If dsList.Tables("ConnectedHost").Rows.Count > 0 Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot close this Period now.") & vbCrLf & Language.getMessage(mstrModuleName, 3, "Reason : The Period to be closed is in use on ") & dsList.Tables("ConnectedHost").Rows(0).Item("hostname").ToString.Trim & " " & Language.getMessage(mstrModuleName, 4, "Machine.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 5, "Please close the Application on that Machine.")
            '    Return False
            'End If
            'Sohail (05 Oct 2016) -- End

            'Sohail (31 Oct 2019) -- Start
            'NMB PAYROLL UAT - Enhancement # 16 : During closing of payroll period, users should be able to login and access other aruti modules.
            'objDataOperation.ExecNonQuery("ALTER DATABASE " & strDatabaseName & " SET SINGLE_USER WITH ROLLBACK IMMEDIATE")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'Sohail (31 Oct 2019) -- End

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            If bw IsNot Nothing Then
                bw.ReportProgress(0)
            End If
            'Sohail (12 Dec 2015) -- End

            'Sohail (20 Apr 2011) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = UpdateNonRecurrentHeadOnED()
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            'blnFlag = UpdateNonRecurrentHeadOnED(strDatabaseName, dtCurrentDateAndTime)
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'blnFlag = UpdateNonRecurrentHeadOnED(strDatabaseName, dtCurrentDateAndTime, xUserUnkid)
            blnFlag = UpdateNonRecurrentHeadOnED(strDatabaseName, dtCurrentDateAndTime, xUserUnkid, objDataOperation)
            'Sohail (18 Jan 2019) -- End
            'Sohail (12 Dec 2015) -- End
            'Sohail (21 Aug 2015) -- End
            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (20 Apr 2011) -- End

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                bw.ReportProgress(1)
            End If
            'Sohail (12 Dec 2015) -- End

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'blnFlag = UpdateLoanAdvance_Balance(bw)

            'Nilay (25-Mar-2016) -- Start
            'blnFlag = UpdateLoanAdvance_Balance(strDatabaseName, xYearUnkid, bw)

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'blnFlag = UpdateLoanAdvance_Balance(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
            '                                    xUserModeSetting, xOnlyApproved, bw)
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'blnFlag = UpdateLoanAdvance_Balance(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
            '                                    xUserModeSetting, xOnlyApproved, dtCurrentDateAndTime, bw)
            blnFlag = UpdateLoanAdvance_Balance(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
                                                xUserModeSetting, xOnlyApproved, dtCurrentDateAndTime, bw, objDataOperation)
            'Sohail (18 Jan 2019) -- End
            'Nilay (20-Sept-2016) -- End

            
            'Nilay (25-Mar-2016) -- End

            'Sohail (15 Dec 2015) -- End

            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                bw.ReportProgress(2)
            End If
            'Sohail (12 Dec 2015) -- End

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = Insert_Saving_Balance_Data()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'blnFlag = Insert_Saving_Balance_Data(xUserUnkid, dtDatabaseStartDate, dtDatabaseEndDate, bw)
            blnFlag = Insert_Saving_Balance_Data(xUserUnkid, dtDatabaseStartDate, dtDatabaseEndDate, bw, objDataOperation)
            'Sohail (18 Jan 2019) -- End
            'Sohail (21 Aug 2015) -- End
            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (12 Jan 2015) -- End

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                bw.ReportProgress(3)
            End If
            'Sohail (12 Dec 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = UpdateSaving_Balance()

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'blnFlag = UpdateSaving_Balance(xUserUnkid)
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'blnFlag = UpdateSaving_Balance(xUserUnkid, dtCurrentDateAndTime, bw)
            blnFlag = UpdateSaving_Balance(xUserUnkid, dtCurrentDateAndTime, bw, objDataOperation)
            'Sohail (18 Jan 2019) -- End
            'Shani(24-Aug-2015) -- End

            'Sohail (21 Aug 2015) -- End
            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                bw.ReportProgress(4)
            End If
            'Sohail (12 Dec 2015) -- End

            'Sohail (24 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'blnFlag = UpdateLeave_Issued()
            blnFlag = UpdateLeave_Issued(objDataOperation)
            'Sohail (18 Jan 2019) -- End
            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (24 Sep 2013) -- End

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                bw.ReportProgress(5)
            End If
            'Sohail (12 Dec 2015) -- End

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = UpdateClaimRequest_Posted()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'blnFlag = UpdateClaimRequest_Posted(xUserUnkid)
            blnFlag = UpdateClaimRequest_Posted(xUserUnkid, objDataOperation)
            'Sohail (18 Jan 2019) -- End
            'Sohail (21 Aug 2015) -- End
            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (12 Nov 2014) -- End

            'Sohail (13 Feb 2020) -- Start
            'NMB Enhancement # : Now processed only those OT which are posted in given date range on OT post to payroll screen.
            blnFlag = UpdateOTRequisition_Posted(xUserUnkid, objDataOperation)
            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (13 Feb 2020) -- End

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                bw.ReportProgress(6)
            End If
            'Sohail (12 Dec 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = CarryForward_Balance()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'blnFlag = CarryForward_Balance(strDatabaseName, xUserUnkid, xYearUnkid, dtCurrentDateAndTime, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter)
            blnFlag = CarryForward_Balance(strDatabaseName, xUserUnkid, xYearUnkid, dtCurrentDateAndTime, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, objDataOperation)
            'Sohail (18 Jan 2019) -- End
            'Sohail (21 Aug 2015) -- End
            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                bw.ReportProgress(7)
            End If
            'Sohail (12 Dec 2015) -- End

            'Sohail (21 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnCopyClosingPeriodEDSlab = True Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = CopyClosingPeriodEdSlab()
                'Sohail (18 Jan 2019) -- Start
                'Issue - 74.1 - Bind Transaction issue.
                'blnFlag = CopyClosingPeriodEdSlab(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, , blnApplyUserAccessFilter, strFilerString, bw)
                blnFlag = CopyClosingPeriodEdSlab(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, , blnApplyUserAccessFilter, strFilerString, bw, objDataOperation)
                'Sohail (18 Jan 2019) -- End
                'Sohail (21 Aug 2015) -- End
                If blnFlag = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = CopyClosingPeriodEdSlab(True)
                'Sohail (18 Jan 2019) -- Start
                'Issue - 74.1 - Bind Transaction issue.
                'blnFlag = CopyClosingPeriodEdSlab(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, True, blnApplyUserAccessFilter, strFilerString, bw)
                blnFlag = CopyClosingPeriodEdSlab(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, True, blnApplyUserAccessFilter, strFilerString, bw, objDataOperation)
                'Sohail (18 Jan 2019) -- End
                'Sohail (21 Aug 2015) -- End
                If blnFlag = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'Sohail (09 Nov 2013) -- End
            End If
            'Sohail (21 Jul 2012) -- End

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                bw.ReportProgress(8)
            End If
            'Sohail (12 Dec 2015) -- End

            'Sohail (27 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = Insert_Loan_Balance_Data()

            'Nilay (18-Nov-2015) -- Start
            'blnFlag = Insert_Loan_Balance_Data(xUserUnkid, dtDatabaseStartDate, dtDatabaseEndDate)

            'Nilay (25-Mar-2016) -- Start
            'blnFlag = Insert_Loan_Balance_Data(xUserUnkid, dtDatabaseStartDate, dtDatabaseEndDate, xYearUnkid, bw)
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'blnFlag = Insert_Loan_Balance_Data(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
            '                                   xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
            '                                   dtDatabaseStartDate, dtDatabaseEndDate, xYearUnkid, bw)
            blnFlag = Insert_Loan_Balance_Data(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                               xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                              dtDatabaseStartDate, dtDatabaseEndDate, xYearUnkid, bw, objDataOperation)
            'Sohail (18 Jan 2019) -- End
            'Nilay (25-Mar-2016) -- End

            'Nilay (18-Nov-2015) -- End

            'Sohail (21 Aug 2015) -- End
            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (27 Sep 2012) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            'Sohail (31 Oct 2019) -- Start
            'NMB PAYROLL UAT - Enhancement # 16 : During closing of payroll period, users should be able to login and access other aruti modules.
            'objDataOperation.ExecNonQuery("ALTER DATABASE " & strDatabaseName & " SET MULTI_USER")
            'Sohail (31 Oct 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Close_Period; Module Name: " & mstrModuleName)
        Finally
            'Sohail (31 Oct 2019) -- Start
            'NMB PAYROLL UAT - Enhancement # 16 : During closing of payroll period, users should be able to login and access other aruti modules.
            'objDataOperation.ExecNonQuery("ALTER DATABASE " & strDatabaseName & " SET MULTI_USER")
            'Sohail (31 Oct 2019) -- End
            clsClosePeriod._ProgressTotalCount = 0
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

#End Region

#Region " Private Functions "

    'Sohail (20 Apr 2011) -- Start
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Private Function UpdateNonRecurrentHeadOnED() As Boolean
    Private Function UpdateNonRecurrentHeadOnED(ByVal strDatabaseName As String, ByVal dtCurrentDateAndTime As Date, ByVal intUserunkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]
        'Sohail (12 Dec 2015) - [intUserunkid]
        'Sohail (21 Aug 2015) -- End

        Dim objEd As New clsEarningDeduction 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End

        Try
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            'strQ = "UPDATE  prearningdeduction_master " & _
            '        "SET     prearningdeduction_master.amount = 0 " & _
            '              ", prearningdeduction_master.medicalrefno = '' " & _
            '        "FROM    prtranhead_master " & _
            '        "WHERE   prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                "AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
            '                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '                "AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others & " " & _
            '                "AND prtranhead_master.isrecurrent = 0 " & _
            '                "AND prearningdeduction_master.periodunkid = " & mintPeriodUnkId & " " 'Sohail (18 Jan 2012) - [medicalrefno, periodunkid]

            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            ''Sohail (12 Oct 2011) -- Start
            ''*** Insert AT Log for earning deduction for above Update transactions.
            'strQ = "SELECT edunkid FROM prearningdeduction_master " & _
            '        "INNER JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '        "WHERE ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
            '        "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '        "AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others & " " & _
            '        "AND prtranhead_master.isrecurrent = 0 " & _
            '        "AND prearningdeduction_master.amount = 0 " & _
            '        "AND prearningdeduction_master.periodunkid = " & mintPeriodUnkId & " " 'Sohail (18 Jan 2012) - [periodunkid]

            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'For Each dsRow As DataRow In dsList.Tables("List").Rows
            '    objEd = New clsEarningDeduction
            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'objEd._Edunkid = CInt(dsRow.Item("edunkid"))
            '    objEd._Edunkid(objDataOperation, strDatabaseName) = CInt(dsRow.Item("edunkid"))
            '    'Sohail (21 Aug 2015) -- End
            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'If objEd.InsertAuditTrailForEarningDeduction(objDataOperation, 2) = False Then
            '    If objEd.InsertAuditTrailForEarningDeduction(objDataOperation, 2, dtCurrentDateAndTime) = False Then
            '        'Sohail (21 Aug 2015) -- End
            '        Return False
            '    End If
            'Next
            ''Sohail (12 Oct 2011) -- End
            If objEd.UpdateNonRecurrentHeadOnED(objDataOperation, strDatabaseName, mintPeriodUnkId, intUserunkid, dtCurrentDateAndTime, "", "", -1, "", "", "", "", "", "") = False Then
                If objEd._Message <> "" Then
                    mstrMessage = objEd._Message
                Else
                    mstrMessage = "Error from UpdateNonRecurrentHeadOnED from clsearning_deduction_tran."
                End If
                Return False
            End If
            'Sohail (12 Dec 2015) -- End
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateNonRecurrentHeadOnED; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
    End Function
    'Sohail (20 Apr 2011) -- End

    'Nilay (25-Mar-2016) -- Start
    'Private Function UpdateLoanAdvance_Balance(ByVal xDatabaseName As String, ByVal intYearUnkid As Integer, Optional ByVal bw As BackgroundWorker = Nothing) As Boolean
    Private Function UpdateLoanAdvance_Balance(ByVal xDatabaseName As String, _
                                               ByVal xUserUnkid As Integer, _
                                               ByVal xYearUnkid As Integer, _
                                               ByVal xCompanyUnkid As Integer, _
                                               ByVal xPeriodStart As Date, _
                                               ByVal xPeriodEnd As Date, _
                                               ByVal xUserModeSetting As String, _
                                               ByVal xOnlyApproved As Boolean, _
                                               ByVal xdtCurrentDateTime As DateTime, _
                                               Optional ByVal bw As BackgroundWorker = Nothing, _
                                               Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]
        'Nilay (20-Sept-2016) -- [xdtCurrentDateTime]

        'Nilay (25-Mar-2016) -- End


        'Sohail (15 Dec 2015) -- [xDatabaseName, intYearUnkid]
        'Sohail (12 Dec 2015) - [bw]

        Dim objLoan As clsLoan_Advance 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            'strQ = "SELECT  vwPayroll.tnaleavetranunkid " & _
            '              ", vwPayroll.payperiodunkid " & _
            '              ", vwPayroll.period_name " & _
            '              ", vwPayroll.employeeunkid " & _
            '              ", vwPayroll.employeecode " & _
            '              ", vwPayroll.employeename " & _
            '              ", vwPayroll.processdate " & _
            '              ", vwPayroll.voucherno " & _
            '              ", vwPayroll.daysinperiod " & _
            '              ", vwPayroll.paidholidays " & _
            '              ", vwPayroll.totalpayabledays " & _
            '              ", vwPayroll.totalpayablehours " & _
            '              ", vwPayroll.totaldaysworked " & _
            '              ", vwPayroll.totalhoursworked " & _
            '              ", vwPayroll.totalabsentorleave " & _
            '              ", vwPayroll.totalextrahours " & _
            '              ", vwPayroll.totalshorthours " & _
            '              ", vwPayroll.total_amount " & _
            '              ", vwPayroll.balanceamount " & _
            '              ", vwPayroll.totalonholddays " & _
            '              ", vwPayroll.tranheadunkid " & _
            '              ", vwPayroll.GroupID " & _
            '              ", vwPayroll.GroupName " & _
            '              ", vwPayroll.trnheadname " & _
            '              ", vwPayroll.trnheadtype_id " & _
            '              ", vwPayroll.typeof_id " & _
            '              ", vwPayroll.amount " & _
            '              ", vwPayroll.add_deduct " & _
            '              ", vwPayroll.costcenterunkid " & _
            '              ", vwPayroll.costcentername " & _
            '              ", vwPayroll.membershipunkid " & _
            '              ", vwPayroll.membershipno " & _
            '              ", vwPayroll.membershipname " & _
            '              ", vwPayroll.broughtforward " & _
            '        "FROM    vwPayroll " & _
            '                "LEFT JOIN prpayment_tran ON vwPayroll.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
            '                            "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
            '                            "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
            '                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '        "WHERE   payperiodunkid = @payperiodunkid " & _
            '                "AND prpayment_tran.paymenttranunkid IS NULL " & _
            '                "AND GroupID IN ( 5, 6 ) " '5=Loan, 6=Advance
            strQ = "SELECT  prpayrollprocess_tran.loanadvancetranunkid AS tranheadunkid  " & _
                          ", prpayrollprocess_tran.amount " & _
                          ", prpayrollprocess_tran.amountpaidcurrency " & _
                    "FROM    prpayrollprocess_tran " & _
                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN prpayment_tran ON prtnaleave_tran.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
                                                        "AND prpayment_tran.isvoid = 0 " & _
                                        "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                        "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                    "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @payperiodunkid " & _
                            "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                            "AND prpayment_tran.paymenttranunkid IS NULL "
            'Sohail (07 May 2015) -- End

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            objLoan = New clsLoan_Advance

            'Nilay (25-Mar-2016) -- Start
            'Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo("List", mdtPeriodEndDate.AddDays(1), "", 0, , True)

            'Nilay (23-Aug-2016) -- Start
            'Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
            '                                                          xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
            '                                                          "List", mdtPeriodEndDate.AddDays(1), "", 0, , True)
            'Sohail (08 Feb 2018) -- Start
            'SUPPORT KBC#1869 | LOAN BALANCE ON PAYSLIP DOES NOT SHOW in 70.1.
            'Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
            '                                                          xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
            '                                                          "List", mdtPeriodEndDate.AddDays(1), "", 0, mdtPeriodStartDate, , True)
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
            '                                                          xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
            '                                                          "List", mdtPeriodEndDate.AddDays(1), "", 0, mdtPeriodStartDate, True)
            Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                      xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                                     "List", mdtPeriodEndDate.AddDays(1), "", 0, mdtPeriodStartDate, True, , , , , , , , , , , , , , , , objDataOperation)
            'Sohail (18 Jan 2019) -- End
            'Sohail (08 Feb 2018) -- End
            'Nilay (23-Aug-2016) -- END

            
            'Nilay (25-Mar-2016) -- End


            'Sohail (07 May 2015) -- End

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                clsClosePeriod._ProgressTotalCount = dsList.Tables("List").Rows.Count
                bw.ReportProgress(1)
            End If
            'Sohail (12 Dec 2015) -- End

            For Each dsRow As DataRow In dsList.Tables("List").Rows

                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                'strQ = " Update lnloan_advance_tran set " & _
                '       "  balance_amount = balance_amount - @amount " & _
                '       " WHERE loanadvancetranunkid = @loanadvancetranunkid "

                'objDataOperation.ClearParameters()
                'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dsRow("amount").ToString)
                'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, dsRow("tranheadunkid").ToString)
                'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dsRow("amountPaidCurrency")) 'Sohail (07 May 2015)

                'objDataOperation.ExecNonQuery(strQ)

                'If objDataOperation.ErrorMessage <> "" Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If
                'Sohail (07 May 2015) -- End

                'Sohail (12 Oct 2011) -- Start
                objLoan = New clsLoan_Advance
                'Sohail (18 Jan 2019) -- Start
                'Issue - 74.1 - Bind Transaction issue.
                objLoan._DataOpr = objDataOperation
                'Sohail (18 Jan 2019) -- End
                objLoan._Loanadvancetranunkid = CInt(dsRow("tranheadunkid"))

                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                Dim dr_Row() As DataRow = dsLoan.Tables("List").Select("loanadvancetranunkid = " & CInt(dsRow("tranheadunkid")) & " ")
                If dr_Row.Length > 0 Then
                    objLoan._Balance_Amount = objLoan._Balance_Amount - CDec(dr_Row(0).Item("TotPrincipalAmount"))
                    objLoan._Balance_AmountPaidCurrency = objLoan._Balance_AmountPaidCurrency - CDec(dr_Row(0).Item("TotPrincipalAmountPaidCurrency"))

                    'Nilay (05-May-2016) -- Start
                    'If objLoan.Update(, objDataOperation) = False Then
                    If objLoan.Update(objDataOperation) = False Then
                        'Nilay (05-May-2016) -- End

                        exForce = New Exception(objLoan._Message)
                        Throw exForce
                    End If
                End If
                'Sohail (07 May 2015) -- End

                'Nilay (05-May-2016) -- Start
                'If objLoan.InsertAuditTrailForLoanAdvanceTran(objDataOperation, 2) = False Then
                If objLoan.InsertAuditTrailForLoanAdvanceTran(objDataOperation, 2, xUserUnkid) = False Then
                    'Nilay (05-May-2016) -- End

                    If objLoan._Message <> "" Then
                        mstrMessage = objLoan._Message
                    Else
                        mstrMessage = "Error from InsertAuditTrailForLoanAdvanceTran from clsloan_advance_tran."
                    End If
                    Return False
                End If
                'Sohail (12 Oct 2011) -- End

                strQ = "SELECT balance_amount, balance_amountPaidCurrency FROM lnloan_advance_tran  " & _
                        " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                'Sohail (07 May 2015) - [balance_amountPaidCurrency]

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, dsRow("tranheadunkid").ToString())
                dsList = objDataOperation.ExecQuery(strQ, "Balance")

                If dsList IsNot Nothing Then
                    If dsList.Tables("Balance").Rows.Count > 0 Then
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        'If CDec(dsList.Tables("Balance").Rows(0)("balance_amount")) < 0.01 Then 'Sohail (11 May 2011)
                        'Hemant (04 June 2019) -- Start
                        'ISSUE/ENHANCEMENT : The employee loan still gets deducted even after loan tenure is over
                        'If CDec(dsList.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) < 0.01 Then
                        'Hemant (08 Nov 2019) -- Start
                        'ISSUE/ENHANCEMENT#4271(TUJIJENGE- TZ) - Not able to close Payroll period due to Loan deduction mismatch.
                        'If CDec(dsList.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) < 0.1 Then
                        If CDec(dsList.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) <= 1 Then
                            'Hemant (08 Nov 2019) -- End
                            'Hemant (04 June 2019) -- End
                            'Sohail (07 May 2015) -- End

                            strQ = " Update lnloan_advance_tran set " & _
                                  " loan_statusunkid = 4 " & _
                                  " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, dsRow("tranheadunkid").ToString)
                            objDataOperation.ExecNonQuery(strQ)

                            Dim objLoanStatusTran As New clsLoan_Status_tran
                            objLoanStatusTran._Loanadvancetranunkid = CInt(dsRow("tranheadunkid"))
                            'Nilay (20-Sept-2016) -- Start
                            'Enhancement : Cancel feature for approved but not assigned loan application
                            'objLoanStatusTran._Staus_Date = Now
                            objLoanStatusTran._Staus_Date = xdtCurrentDateTime
                            'Nilay (20-Sept-2016) -- End
                            objLoanStatusTran._Isvoid = False
                            objLoanStatusTran._Voiddatetime = Nothing
                            objLoanStatusTran._Voiduserunkid = -1
                            objLoanStatusTran._Statusunkid = 4
                            objLoanStatusTran._Periodunkid = mintPeriodUnkId 'Sohail (07 May 2015)
                            'Nilay (01-Apr-2016) -- Start
                            'ENHANCEMENT - Approval Process in Loan Other Operations
                            objLoanStatusTran._Userunkid = xUserUnkid
                            'Nilay (01-Apr-2016) -- End

                            'Sohail (15 Dec 2015) -- Start
                            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                            'blnFlag = objLoanStatusTran.Insert()

                            'Nilay (25-Mar-2016) -- Start
                            'blnFlag = objLoanStatusTran.Insert(xDatabaseName, intYearUnkid, objDataOperation, True)
                            blnFlag = objLoanStatusTran.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
                                                               xUserModeSetting, xOnlyApproved, objDataOperation, True)
                            'Nilay (25-Mar-2016) -- End

                            'Sohail (15 Dec 2015) -- End

                            If blnFlag = False Then
                                If objLoanStatusTran._Message <> "" Then
                                    mstrMessage = objLoanStatusTran._Message
                                Else
                                    mstrMessage = "Error from Insert from clsloan_status_tran."
                                End If
                                Return False
                            End If

                            objLoanStatusTran = Nothing
                        End If
                    End If
                End If
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoanAdvance_Balance; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
    End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function UpdateSaving_Balance(ByVal intUserunkid As Integer) As Boolean
    Private Function UpdateSaving_Balance(ByVal intUserunkid As Integer _
                                          , ByVal xCurrentDateAndTime As DateTime _
                                          , Optional ByVal bw As BackgroundWorker = Nothing _
                                          , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                          ) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]
        'Sohail (12 Dec 2015) - [bw]
        'Shani(24-Aug-2015) -- End

        'Sohail (21 Aug 2015) - [intUserunkid]

        Dim objSaving As clsSaving_Tran 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'strQ = "SELECT  vwPayroll.tnaleavetranunkid " & _
            '              ", vwPayroll.payperiodunkid " & _
            '              ", vwPayroll.period_name " & _
            '              ", vwPayroll.employeeunkid " & _
            '              ", vwPayroll.employeecode " & _
            '              ", vwPayroll.employeename " & _
            '              ", vwPayroll.processdate " & _
            '              ", vwPayroll.voucherno " & _
            '              ", vwPayroll.daysinperiod " & _
            '              ", vwPayroll.paidholidays " & _
            '              ", vwPayroll.totalpayabledays " & _
            '              ", vwPayroll.totalpayablehours " & _
            '              ", vwPayroll.totaldaysworked " & _
            '              ", vwPayroll.totalhoursworked " & _
            '              ", vwPayroll.totalabsentorleave " & _
            '              ", vwPayroll.totalextrahours " & _
            '              ", vwPayroll.totalshorthours " & _
            '              ", vwPayroll.total_amount " & _
            '              ", vwPayroll.balanceamount " & _
            '              ", vwPayroll.totalonholddays " & _
            '              ", vwPayroll.tranheadunkid " & _
            '              ", vwPayroll.GroupID " & _
            '              ", vwPayroll.GroupName " & _
            '              ", vwPayroll.trnheadname " & _
            '              ", vwPayroll.trnheadtype_id " & _
            '              ", vwPayroll.typeof_id " & _
            '              ", vwPayroll.amount " & _
            '              ", vwPayroll.add_deduct " & _
            '              ", vwPayroll.costcenterunkid " & _
            '              ", vwPayroll.costcentername " & _
            '              ", vwPayroll.membershipunkid " & _
            '              ", vwPayroll.membershipno " & _
            '              ", vwPayroll.membershipname " & _
            '              ", vwPayroll.broughtforward " & _
            '        "FROM    vwPayroll " & _
            '                "LEFT JOIN prpayment_tran ON vwPayroll.tnaleavetranunkid = prpayment_tran.referencetranunkid " & _
            '                            "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
            '                            "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
            '                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '        "WHERE   payperiodunkid = @payperiodunkid " & _
            '                "AND prpayment_tran.paymenttranunkid IS NULL " & _
            '                "AND GroupID IN ( 7 ) " '7=Saving

            'objDataOperation.ClearParameters()

            'objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId)

            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'For Each dsRow As DataRow In dsList.Tables("List").Rows

            '    strQ = " Update svsaving_tran set " & _
            '          " balance_amount = balance_amount + @amount " & _
            '          ",total_contribution = total_contribution + @amount " & _
            '          " WHERE savingtranunkid = @savingtranunkid "

            '    objDataOperation.ClearParameters()
            '    objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, dsRow("amount").ToString)
            '    objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, dsRow("tranheadunkid").ToString)

            '    objDataOperation.ExecNonQuery(strQ)

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    'Sohail (12 Oct 2011) -- Start
            '    objSaving = New clsSaving_Tran
            '    objSaving._Savingtranunkid = CInt(dsRow("tranheadunkid"))
            '    If objSaving.InsertAuditTrailForSavingTran(objDataOperation, 2) = False Then
            '        Return False
            '    End If
            '    'Sohail (12 Oct 2011) -- End

            'Next

            strQ = "SELECT  a.savingtranunkid  " & _
                          ", a.cf_amount " & _
                          ", a.cf_balance " & _
                          ", ( a.cf_balance - a.cf_amount ) AS total_interest_amount " & _
                    "FROM    ( SELECT    savingtranunkid  " & _
                                      ", cf_amount " & _
                                      ", cf_balance " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY savingtranunkid ORDER BY effective_date DESC, savingbalancetranunkid DESC ) AS ROWNO " & _
                              "FROM      svsaving_balance_tran " & _
                              "WHERE     isvoid = 0 " & _
                                        "AND periodunkid = @payperiodunkid " & _
                            ") AS a " & _
                    "WHERE a.ROWNO = 1 "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                clsClosePeriod._ProgressTotalCount = dsList.Tables("List").Rows.Count
                bw.ReportProgress(3)
            End If
            'Sohail (12 Dec 2015) -- End

            For Each dsRow As DataRow In dsList.Tables("List").Rows

                strQ = " UPDATE svsaving_tran SET " & _
                             " balance_amount = " & CDec(dsRow.Item("cf_balance")) & " " & _
                             ",total_contribution = " & CDec(dsRow.Item("cf_amount")) & " " & _
                             ",interest_amount = " & CDec(dsRow.Item("total_interest_amount")) & " " & _
                      " WHERE savingtranunkid = @savingtranunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, dsRow("savingtranunkid").ToString)

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objSaving = New clsSaving_Tran
                objSaving._Savingtranunkid = CInt(dsRow("savingtranunkid"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objSaving.InsertAuditTrailForSavingTran(objDataOperation, 2) = False Then
                If objSaving.InsertAuditTrailForSavingTran(objDataOperation, 2, xCurrentDateAndTime) = False Then
                    'Shani(24-Aug-2015) -- End
                    If objSaving._Message <> "" Then
                        mstrMessage = objSaving._Message
                    Else
                        mstrMessage = "Error from InsertAuditTrailForSavingTran from clssaving_tran."
                    End If
                    Return False
                End If

            Next
            'Sohail (12 Jan 2015) -- End

            'Sohail (23 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objSavingStatus As clsSaving_Status_Tran
            strQ = "SELECT  svsaving_tran.savingtranunkid " & _
                          ", svsaving_tran.voucherno " & _
                          ", svsaving_tran.effectivedate " & _
                          ", svsaving_tran.payperiodunkid " & _
                          ", svsaving_tran.employeeunkid " & _
                          ", svsaving_tran.savingschemeunkid " & _
                          ", svsaving_tran.contribution " & _
                          ", svsaving_tran.savingstatus " & _
                          ", svsaving_tran.total_contribution " & _
                          ", svsaving_tran.interest_rate " & _
                          ", svsaving_tran.interest_amount " & _
                          ", svsaving_tran.balance_amount " & _
                          ", svsaving_tran.broughtforward " & _
                          ", svsaving_tran.userunkid " & _
                          ", svsaving_tran.isvoid " & _
                          ", svsaving_tran.voiduserunkid " & _
                          ", svsaving_tran.voiddatetime " & _
                          ", svsaving_tran.voidreason " & _
                          ", CONVERT(CHAR(8), svsaving_tran.stopdate, 112) AS stopdate " & _
                    "FROM    svsaving_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND CONVERT(CHAR(8), stopdate, 112) >= @start_date " & _
                            "AND CONVERT(CHAR(8), stopdate, 112) <= @end_date " & _
                            "AND savingstatus <> 4 "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                clsClosePeriod._ProgressTotalCount = dsList.Tables("List").Rows.Count
                bw.ReportProgress(3)
            End If
            'Sohail (12 Dec 2015) -- End

            For Each dsRow As DataRow In dsList.Tables("List").Rows
                objSavingStatus = New clsSaving_Status_Tran

                With objSavingStatus
                    ._Savingtranunkid = CInt(dsRow.Item("savingtranunkid"))
                    ._Status_Date = eZeeDate.convertDate(dsRow.Item("stopdate").ToString)
                    ._Statusunkid = 4
                    ._Remark = ""
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    '._Userunkid = User._Object._Userunkid
                    ._Userunkid = intUserunkid
                    'Sohail (21 Aug 2015) -- End
                    ._Isvoid = False
                    ._Voiddatetime = Nothing
                    ._Voiduserunkid = -1
                    'Sohail [17 November 2015] -- Start
                    'Issue : was storing -1 in Period id while closing period.
                    ._PeriodID = mintPeriodUnkId
                    'Sohail [17 November 2015] -- End

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If .Insert(False) = False Then
                    'Sohail (18 Jan 2019) -- Start
                    'Issue - 74.1 - Bind Transaction issue.
                    'If .Insert(xCurrentDateAndTime, False) = False Then
                    If .Insert(xCurrentDateAndTime, False, objDataOperation) = False Then
                        'Sohail (18 Jan 2019) -- End
                        'Shani(24-Aug-2015) -- End
                        If ._Message <> "" Then
                            mstrMessage = ._Message
                        Else
                            mstrMessage = "Error from Insert from clssaving_status_tran."
                        End If
                        Return False
                    End If
                End With
            Next
            'Sohail (23 Jan 2012) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateSaving_Balance; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
            clsClosePeriod._ProgressTotalCount = 0
        End Try
    End Function

    'Sohail (24 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Private Function UpdateLeave_Issued(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try
            'Sohail (28 Dec 2018) -- Start
            'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
            'strQ = "UPDATE  lvleaveIssue_tran " & _
            '        "SET     isprocess = 1 " & _
            '        "WHERE   isvoid = 0 " & _
            '                "AND ISNULL(isprocess, 0) = 0 " & _
            '                "AND CONVERT(CHAR(8), leavedate, 112) <= @enddate "
            strQ = "UPDATE  lvleaveIssue_tran " & _
                    "SET     isprocess = 1 " & _
                    "FROM lvleaveIssue_master " & _
                        "LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                    "WHERE lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                          "AND lvleaveIssue_tran.isvoid = 0 " & _
                          "AND lvleaveIssue_master.isvoid = 0 " & _
                            "AND ISNULL(isprocess, 0) = 0 " & _
                          "AND ( " & _
                                  "( lvleavetype_master.istnaperiodlinked = 0 AND CONVERT(CHAR(8), leavedate, 112) <= @payrollenddate ) " & _
                                  "OR " & _
                                  "( lvleavetype_master.istnaperiodlinked = 1 AND CONVERT(CHAR(8), leavedate, 112) <= @enddate ) " & _
                              ") "
            'Sohail (28 Dec 2018) -- End

            objDataOperation.ClearParameters()

            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtPeriodEndDate)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTnAEndDate))
            'Sohail (07 Jan 2014) -- End
            'Sohail (28 Dec 2018) -- Start
            'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
            objDataOperation.AddParameter("@payrollenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            'Sohail (28 Dec 2018) -- End

            'dsList = objDataOperation.ExecQuery(strQ, "List")
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLeave_Issued; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
    End Function
    'Sohail (24 Sep 2013) -- End

    Private Function CarryForward_Balance(ByVal xDatabaseName As String _
                                          , ByVal intUserunkid As Integer _
                                          , ByVal intYearID As Integer _
                                          , ByVal dtCurrentDateAndTime As Date _
                                          , ByVal xCompanyUnkid As Integer _
                                          , ByVal xPeriodStart As Date _
                                          , ByVal xPeriodEnd As Date _
                                          , ByVal xUserModeSetting As String _
                                          , ByVal xOnlyApproved As Boolean _
                                          , ByVal blnIncludeInActiveEmployee As Boolean _
                                          , ByVal blnApplyUserAccessFilter As String _
                                          , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                          ) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim objTnALeave As New clsTnALeaveTran

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try

            If mintNextPeriodUnkId > 0 Then

                '*** deleting all entries of next period
                strQ = "UPDATE prtnaleave_tran SET " & _
                              " isvoid = 1" & _
                              ", voiduserunkid = @voiduserunkid" & _
                              ", voiddatetime = @voiddatetime" & _
                              ", voidreason = @voidreason " & _
                    "WHERE ISNULL(isvoid,0) = 0 " & _
                    "AND payperiodunkid = @payperiodunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNextPeriodUnkId.ToString)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString)
                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                'Sohail (21 Aug 2015) -- End
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Void while closing period to make new entry for opening balance")

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "UPDATE prpayrollprocess_tran SET " & _
                                " isvoid = 1" & _
                                ", voiduserunkid = @voiduserunkid" & _
                                ", voiddatetime = @voiddatetime" & _
                                ", voidreason = @voidreason " & _
                        "FROM    prpayrollprocess_tran " & _
                                "INNER JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                        "WHERE   prtnaleave_tran.payperiodunkid = @payperiodunkid " & _
                                "AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNextPeriodUnkId.ToString)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString)
                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                'Sohail (21 Aug 2015) -- End
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Void while closing period to make new entry for opening balance")

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                '*** Carry Forward closing balance to new period
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objTnALeave.GetList("OldBalance", , , , , , , , , , , , , mintPeriodUnkId)
                'Sohail (18 Jan 2019) -- Start
                'Issue - 74.1 - Bind Transaction issue.
                'dsList = objTnALeave.GetList(xDatabaseName, intUserunkid, intYearID, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, blnIncludeInActiveEmployee, blnApplyUserAccessFilter, "OldBalance", , mintPeriodUnkId)
                dsList = objTnALeave.GetList(xDatabaseName, intUserunkid, intYearID, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, blnIncludeInActiveEmployee, blnApplyUserAccessFilter, "OldBalance", , mintPeriodUnkId, , , , , objDataOperation)
                'Sohail (18 Jan 2019) -- End
                'Sohail (21 Aug 2015) -- End

                'Sohail (18 Jan 2019) -- Start
                'Issue - 74.1 - Bind Transaction issue.
                'Dim lngVoucherNo As Long = objTnALeave.getMaxVoucherNo()
                Dim lngVoucherNo As Long = objTnALeave.getMaxVoucherNo(objDataOperation)
                'Sohail (18 Jan 2019) -- End

                Dim objPeriod As New clscommom_period_Tran
                'Sohail (18 Jan 2019) -- Start
                'Issue - 74.1 - Bind Transaction issue.
                objPeriod._xDataOperation = objDataOperation
                'Sohail (18 Jan 2019) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = mintNextPeriodUnkId
                objPeriod._Periodunkid(xDatabaseName) = mintNextPeriodUnkId
                'Sohail (21 Aug 2015) -- End
                Dim mdtNextPeriodStartDate As DateTime = objPeriod._Start_Date
                objPeriod = Nothing

                For Each dsRow As DataRow In dsList.Tables("OldBalance").Rows
                    lngVoucherNo += 1

                    With objTnALeave
                        ._Payperiodunkid = mintNextPeriodUnkId
                        ._Employeeunkid = dsRow.Item("Employeeunkid")
                        ._Voucherno = lngVoucherNo.ToString
                        ._Processdate = mdtNextPeriodStartDate
                        ._Daysinperiod = 0
                        ._Paidholidays = 0
                        ._Totalpayabledays = 0
                        ._Totalpayablehours = 0
                        ._Totaldaysworked = 0
                        ._Totalhoursworked = 0
                        ._Totalabsentorleave = 0
                        ._Totalextrahours = 0
                        ._Totalshorthours = 0
                        ._Totalonholddays = 0
                        ._Total_amount = 0
                        ._Balanceamount = dsRow.Item("balanceamount") 'Balance amount / Opening balance
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        '._Userunkid = User._Object._Userunkid
                        ._Userunkid = intUserunkid
                        'Sohail (21 Aug 2015) -- End
                        ._Isvoid = False
                        ._Voiduserunkid = 0
                        ._Voiddatetime = Nothing
                        ._Voidreason = ""
                        ._OpeningBalance = dsRow.Item("balanceamount") 'Balance amount / Opening balance
                        'Sohail (10 Feb 2012) -- Start
                        'TRA - ENHANCEMENT
                        ._BasicSalary = CDec(dsRow.Item("basicsalary"))
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        '._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
                        ._Auditdatetime = dtCurrentDateAndTime
                        'Sohail (21 Aug 2015) -- End
                        ._Ip = getIP()
                        ._Host = getHostName()
                        ._Void_Ip = String.Empty
                        ._Void_Host = String.Empty
                        'Sohail (10 Feb 2012) -- End
                        ._TnA_Processdate = mdtNextPeriodStartDate 'Sohail (07 Jan 2014)
                        'Sohail (28 Apr 2018) -- Start
                        'Enhancement : Ref. No. 231 - WebAPI for seamless integration between Aruti and EPICOR in 72.1.
                        ._Costcenterunkid = 0
                        'Sohail (28 Apr 2018) -- End
                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
                        ._Employee_enddate = mdtNextPeriodStartDate
                        'Sohail (09 Oct 2019) -- End

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'blnFlag = .Insert()
                        'Sohail (18 Jan 2019) -- Start
                        'Issue - 74.1 - Bind Transaction issue.
                        'blnFlag = .Insert(dtCurrentDateAndTime)
                        blnFlag = .Insert(dtCurrentDateAndTime, objDataOperation)
                        'Sohail (18 Jan 2019) -- End

                        'Sohail (21 Aug 2015) -- End

                        If blnFlag = False Then
                            If ._Message <> "" Then
                                mstrMessage = ._Message
                            Else
                                mstrMessage = "Error from Insert from clstan_leave_tran."
                            End If
                            Return False
                        End If


                    End With

                Next

            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CarryForward_Balance; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
    End Function

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Function CopyClosingPeriodEdSlab(ByVal strDatabaseName As String _
                                             , ByVal xUserUnkid As Integer _
                                             , ByVal xYearUnkid As Integer _
                                             , ByVal xCompanyUnkid As Integer _
                                             , ByVal xPeriodStart As DateTime _
                                             , ByVal xPeriodEnd As DateTime _
                                             , ByVal xUserModeSetting As String _
                                             , ByVal xOnlyApproved As Boolean _
                                             , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                             , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                             , ByVal dtCurrentDateAndTime As Date _
                                             , Optional ByVal blnCopyWithStopdateOnly As Boolean = False _
                                             , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                             , Optional ByVal strFilerString As String = "" _
                                             , Optional ByVal bw As BackgroundWorker = Nothing _
                                             , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                             ) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]
        'Sohail (12 Dec 2015) - [bw]
        'Sohail (21 Aug 2015) - [strDatabaseName, dtCurrentDateAndTime]
        'Sohail (09 Nov 2013) - [blnCopyWithStopdateOnly]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim strEmployeeIDs As String = ""

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , strDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, strDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, strDatabaseName)
        'Sohail (21 Aug 2015) -- End

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try

            If mintNextPeriodUnkId > 0 Then
                '*** Please do same changes in clsCloseYear 'CF_EarningDeduction' method

                If blnCopyWithStopdateOnly = False Then 'Sohail (09 Nov 2013)


                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'strQ = "DECLARE @ids VARCHAR(MAX) " & _
                    '        "SELECT  @ids = COALESCE(@ids + ',', '') + CAST(employeeunkid AS VARCHAR(MAX)) " & _
                    '        "FROM    hremployee_master " & _
                    '        "WHERE   1 = 1 " & _
                    '            " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                    '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                    '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                    '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " & _
                    '        " ;SELECT  ISNULL(@ids,'') AS EmpIDs  "
                    strQ = "DECLARE @ids VARCHAR(MAX) " & _
                                "SELECT  @ids = COALESCE(@ids + ',', '') + CAST(hremployee_master.employeeunkid AS VARCHAR(MAX)) " & _
                                "FROM    hremployee_master "

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If
                    If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    strQ &= "WHERE   1 = 1 "

                    If blnApplyUserAccessFilter = True Then
                        If xUACFiltrQry.Trim.Length > 0 Then
                            strQ &= " AND " & xUACFiltrQry
                        End If
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry
                        End If
                    End If

                    If strFilerString.Trim.Length > 0 Then
                        strQ &= " AND " & strFilerString
                    End If

                    strQ &= " ;SELECT  ISNULL(@ids,'') AS EmpIDs  "
                    'Sohail (21 Aug 2015) -- End

                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtNextPeriodStartDate))
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtNextPeriodEndDate))

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables("List").Rows.Count > 0 Then
                        strEmployeeIDs = dsList.Tables("List").Rows(0).Item("EmpIDs").ToString
                    End If

                    'Sohail (09 Nov 2013) -- Start
                    'TRA - ENHANCEMENT
                Else

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'strQ = "DECLARE @ids VARCHAR(MAX) " & _
                    '        "SELECT  @ids = COALESCE(@ids + ',', '') + CAST(employeeunkid AS VARCHAR(MAX)) " & _
                    '        "FROM    ( SELECT DISTINCT " & _
                    '                            "employeeunkid " & _
                    '                  "FROM      ( SELECT    DENSE_RANK() OVER ( PARTITION  BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                    '                                      ", prearningdeduction_master.employeeunkid " & _
                    '                                      ", prearningdeduction_master.stop_date " & _
                    '                              "FROM      prearningdeduction_master " & _
                    '                                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
                    '                                        "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                    '                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                    '                                        "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= @enddate " & _
                    '                                        "AND ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), " & _
                    '                                                   "@startdate) >= @startdate " & _
                    '                                        "AND ISNULL(CONVERT(CHAR(8), hremployee_master.termination_to_date, 112), " & _
                    '                                                   "@startdate) >= @startdate " & _
                    '                                        "AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate, 112), " & _
                    '                                                   "@startdate) >= @startdate " & _
                    '                            ") AS a " & _
                    '                  "WHERE     a.ROWNO = 1 " & _
                    '                            "AND stop_date >= @prevstartdate " & _
                    '                ") AS b  " & _
                    '        " ; SELECT  ISNULL(@ids, '') AS EmpIDs "
                    strQ = "DECLARE @ids VARCHAR(MAX) " & _
                            "SELECT  @ids = COALESCE(@ids + ',', '') + CAST(employeeunkid AS VARCHAR(MAX)) " & _
                            "FROM    ( SELECT DISTINCT " & _
                                                "employeeunkid " & _
                                      "FROM      ( SELECT    DENSE_RANK() OVER ( PARTITION  BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                                          ", prearningdeduction_master.employeeunkid " & _
                                                          ", prearningdeduction_master.stop_date " & _
                                                          ", prearningdeduction_master.edstart_date " & _
                                                  "FROM      prearningdeduction_master " & _
                                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
                                                            "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid "
                    'Sohail (24 Aug 2019) - [edstart_date]

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If
                    If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    strQ &= "                      WHERE     ISNULL(isvoid, 0) = 0 "

                    If blnApplyUserAccessFilter = True Then
                        If xUACFiltrQry.Trim.Length > 0 Then
                            strQ &= " AND " & xUACFiltrQry
                        End If
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry
                        End If
                    End If

                    If strFilerString.Trim.Length > 0 Then
                        strQ &= " AND " & strFilerString
                    End If

                    strQ &= ") AS a " & _
                                      "WHERE     a.ROWNO = 1 " & _
                                                "AND ( CONVERT(CHAR(8), stop_date, 112) >= @prevstartdate ) AND ( CONVERT(CHAR(8), edstart_date, 112) <= @prevenddate ) " & _
                                    ") AS b  " & _
                            " ; SELECT  ISNULL(@ids, '') AS EmpIDs "
                    'Sohail (24 Aug 2019) - [AND stop_date >= @prevstartdate]=[AND ( CONVERT(CHAR(8), stop_date, 112) >= @prevstartdate ) AND ( CONVERT(CHAR(8), edstart_date, 112) <= @prevenddate )]
                    'Sohail (21 Aug 2015) -- End

                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtNextPeriodStartDate))
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtNextPeriodEndDate))
                    objDataOperation.AddParameter("@prevstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                    'Sohail (24 Aug 2019) -- Start
                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                    objDataOperation.AddParameter("@prevenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
                    'Sohail (24 Aug 2019) -- End

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables("List").Rows.Count > 0 Then
                        strEmployeeIDs = dsList.Tables("List").Rows(0).Item("EmpIDs").ToString
                    End If

                End If
                'Sohail (09 Nov 2013) -- End

                If strEmployeeIDs.Trim <> "" Then

                    Dim objED As New clsEarningDeduction

                    'Sohail (09 Nov 2013) -- Start
                    'TRA - ENHANCEMENT
                    'blnFlag = objED.InsertAllByEmployeeList(strEmployeeIDs, 0, 0, 0, -1, 0, False, False, mintNextPeriodUnkId, "", True, True)
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objED.InsertAllByEmployeeList(strEmployeeIDs, 0, 0, 0, -1, 0, False, False, mintNextPeriodUnkId, "", Nothing, Nothing, True, True)
                    'Sohail (12 Dec 2015) -- Start
                    'Enhancement - Showing Progress of closing period.
                    'blnFlag = objED.InsertAllByEmployeeList(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIDs, 0, 0, 0, -1, 0, False, False, mintNextPeriodUnkId, "", Nothing, Nothing, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, True, True, , blnApplyUserAccessFilter, strFilerString)
                    'Sohail (02 Dec 2016) -- Start
                    'Enhancement - 64.1 - Insert Membership heads on Ed if not assigned.
                    'blnFlag = objED.InsertAllByEmployeeList(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIDs, 0, 0, 0, -1, 0, False, False, mintNextPeriodUnkId, "", Nothing, Nothing, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, True, True, , blnApplyUserAccessFilter, strFilerString, bw)
                    'Sohail (25 Jun 2020) -- Start
                    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                    'blnFlag = objED.InsertAllByEmployeeList(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIDs, 0, 0, 0, -1, 0, False, False, mintNextPeriodUnkId, "", Nothing, Nothing, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, True, True, , blnApplyUserAccessFilter, strFilerString, bw, False)
                    blnFlag = objED.InsertAllByEmployeeList(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIDs, 0, 0, 0, -1, 0, False, False, mintNextPeriodUnkId, "", Nothing, Nothing, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, True, True, , blnApplyUserAccessFilter, strFilerString, bw, False, 0, , True)
                    'Sohail (25 Jun 2020) -- End
                    'Sohail (02 Dec 2016) -- End
                    'Sohail (12 Dec 2015) -- End
                    'Sohail (21 Aug 2015) -- End
                    'Sohail (09 Nov 2013) -- End

                    If blnFlag = False Then
                        If objED._Message <> "" Then
                        exForce = New Exception(objED._Message)
                        Throw exForce
                        Else
                            mstrMessage = "Error from InsertAllByEmployeeList in clsEarningDeduction"
                        End If
                        Return False
                    End If
                End If

            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CopyClosingPeriodEdSlab; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
    End Function
    'Sohail (21 Jul 2012) -- End

    'Sohail (27 Sep 2012) -- Start
    'TRA - ENHANCEMENT

    'Nilay (25-Mar-2016) -- Start
    'Private Function Insert_Loan_Balance_Data(ByVal intUserunkid As Integer, _
    '                                          ByVal dtDatabaseStartDate As Date, _
    '                                          ByVal dtDatabaseEndDate As Date, _
    '                                          ByVal intYearunkid As Integer, _
    '                                          Optional ByVal bw As BackgroundWorker = Nothing _
    '                                          ) As Boolean

    Private Function Insert_Loan_Balance_Data(ByVal xDatabaseName As String, _
                                              ByVal xUserUnkid As Integer, _
                                              ByVal xYearUnkid As Integer, _
                                              ByVal xCompanyUnkid As Integer, _
                                              ByVal xPeriodStart As Date, _
                                              ByVal xPeriodEnd As Date, _
                                              ByVal xUserModeSetting As String, _
                                              ByVal xOnlyApproved As Boolean, _
                                              ByVal dtDatabaseStartDate As Date, _
                                              ByVal dtDatabaseEndDate As Date, _
                                              ByVal intYearunkid As Integer, _
                                              Optional ByVal bw As BackgroundWorker = Nothing, _
                                              Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]
        'Nilay (25-Mar-2016) -- End


        'Sohail (12 Dec 2015) - [bw]
        'Sohail (21 Aug 2015) - [intUserunkid, dtDatabaseStartDate, dtDatabaseEndDate]
        'Nilay (18-Nov-2015) - [intYearunkid]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim strEmployeeIDs As String = ""
        'Sohail (07 May 2015) -- Start
        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
        Dim objLoanAdvance As New clsLoan_Advance
        Dim objProcess As New clsPayrollProcessTran
        Dim dsLoan As DataSet
        'Sohail (07 May 2015) -- End
        'Sohail (08 Mar 2016) -- Start
        'Enhancement - Show Report Name on the reports when Logo only option ticked.
        Dim strEmpName As String = ""
        Dim strLoanScheme As String = ""
        'Sohail (08 Mar 2016) -- End

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            'strQ = "CREATE TABLE #tbl " & _
            '        "( " & _
            '          "unkid INT IDENTITY(1, 1) " & _
            '        ", loanschemeunkid INT " & _
            '        ", periodunkid INT " & _
            '        ", loanadvancetranunkid INT " & _
            '        ", employeeunkid INT " & _
            '        ", payrollprocesstranunkid INT " & _
            '        ", paymenttranunkid INT " & _
            '        ", loanbf_amount DECIMAL(36, 6) " & _
            '        ", amount DECIMAL(36, 6) " & _
            '        ", enddate DATETIME " & _
            '        ") " & _
            '    " " & _
            '    "INSERT  INTO #tbl " & _
            '            "( loanschemeunkid " & _
            '            ", periodunkid " & _
            '            ", loanadvancetranunkid " & _
            '            ", employeeunkid " & _
            '            ", payrollprocesstranunkid " & _
            '            ", paymenttranunkid " & _
            '            ", loanbf_amount " & _
            '            ", amount " & _
            '            ", enddate " & _
            '            ") " & _
            '            "SELECT  lnloan_scheme_master.loanschemeunkid " & _
            '                  ", TableLoan.payperiodunkid " & _
            '                  ", TableLoan.loanadvancetranunkid " & _
            '                  ", TableLoan.employeeunkid " & _
            '                  ", TableLoan.payrollprocesstranunkid " & _
            '                  ", TableLoan.paymenttranunkid " & _
            '                  ", ISNULL(TableBF.cf_amount, lnloan_advance_tran.bf_amount) AS bf_amount " & _
            '                  ", TableLoan.amount " & _
            '                  ", TableLoan.enddate " & _
            '            "FROM    ( SELECT    prtnaleave_tran.payperiodunkid " & _
            '                              ", prpayrollprocess_tran.employeeunkid " & _
            '                              ", prpayrollprocess_tran.loanadvancetranunkid AS loanadvancetranunkid " & _
            '                              ", prpayrollprocess_tran.payrollprocesstranunkid " & _
            '                              ", 0 AS paymenttranunkid " & _
            '                              ", prpayrollprocess_tran.amount " & _
            '                              ", cfcommon_period_tran.end_date AS enddate " & _
            '                      "FROM      prpayrollprocess_tran " & _
            '                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '                                "JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
            '                                "INNER JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
            '                      "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                                "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
            '                                "AND cfcommon_period_tran.isactive = 1 " & _
            '                                "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
            '                                "AND lnloan_advance_tran.isloan = 1 " & _
            '                                "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            '                                "AND cfcommon_period_tran.periodunkid = @ClosingPeriodId " & _
            '                                "AND lnloan_advance_tran.bf_amount IS NOT NULL " & _
            '                      "UNION ALL " & _
            '                      "SELECT    Payment.periodunkid " & _
            '                              ", employeeunkid " & _
            '                              ", loanadvancetranunkid " & _
            '                              ", 0 AS payrollprocesstranunkid " & _
            '                              ", Payment.paymenttranunkid " & _
            '                              ", amount " & _
            '                              ", cfcommon_period_tran.end_date AS enddate " & _
            '                      "FROM      ( SELECT    ( SELECT    C.periodunkid " & _
            '                                              "FROM      cfcommon_period_tran " & _
            '                                                        "AS C " & _
            '                                              "WHERE     CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) BETWEEN C.start_date AND C.end_date " & _
            '                                                        "AND c.modulerefid = " & enModuleReference.Payroll & " " & _
            '                                                        "AND c.isactive = 1 " & _
            '                                            ") AS periodunkid " & _
            '                                          ", prpayment_tran.paymenttranunkid " & _
            '                                          ", prpayment_tran.employeeunkid " & _
            '                                          ", prpayment_tran.referencetranunkid AS loanadvancetranunkid " & _
            '                                          ", prpayment_tran.amount " & _
            '                                  "FROM      prpayment_tran " & _
            '                                            "JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
            '                                  "WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '                                            "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.LOAN & " " & _
            '                                            "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.RECEIVED & " " & _
            '                                            "AND prpayment_tran.paymentdate BETWEEN @db_start_date AND @db_end_date " & _
            '                                ") AS Payment " & _
            '                                "JOIN cfcommon_period_tran ON Payment.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                                                  "AND cfcommon_period_tran.periodunkid = @ClosingPeriodId " & _
            '                    ") AS TableLoan " & _
            '                    "INNER JOIN lnloan_advance_tran ON TableLoan.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
            '                    "INNER JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
            '                    "LEFT JOIN ( SELECT  loanadvancetranunkid " & _
            '                                      ", MIN(cf_amount) AS cf_amount " & _
            '                                "FROM    lnloan_balance_tran " & _
            '                                "WHERE   ISNULL(isvoid, 0) = 0 " & _
            '                                "GROUP BY loanadvancetranunkid " & _
            '                              ") AS TableBF ON TableBF.loanadvancetranunkid = TableLoan.loanadvancetranunkid " & _
            '            "ORDER BY TableLoan.loanadvancetranunkid " & _
            '                  ", TableLoan.enddate " & _
            '    " " & _
            '    "INSERT  INTO lnloan_balance_tran " & _
            '            "( loanschemeunkid " & _
            '            ", periodunkid " & _
            '            ", end_date " & _
            '            ", loanadvancetranunkid " & _
            '            ", employeeunkid " & _
            '            ", payrollprocesstranunkid " & _
            '            ", paymenttranunkid " & _
            '            ", bf_amount " & _
            '            ", amount " & _
            '            ", cf_amount " & _
            '            ", userunkid " & _
            '            ", isvoid " & _
            '            ", voiduserunkid " & _
            '            ", voiddatetime " & _
            '            ", voidreason " & _
            '            ") " & _
            '            "SELECT  a.loanschemeunkid " & _
            '                  ", a.periodunkid " & _
            '                  ", a.enddate " & _
            '                  ", a.loanadvancetranunkid " & _
            '                  ", a.employeeunkid " & _
            '                  ", a.payrollprocesstranunkid " & _
            '                  ", a.paymenttranunkid " & _
            '                  ", a.loanbf_amount - SUM(b.amount) + a.amount AS BFAmount " & _
            '                  ", a.amount AS Amount " & _
            '                  ", a.loanbf_amount - SUM(b.amount) AS CFAmount " & _
            '                  ", @userunkid AS userunkid " & _
            '                  ", 0 AS isvoid " & _
            '                  ", -1 AS voiduserunkid " & _
            '                  ", NULL AS voiddatetime " & _
            '                  ", '' AS voidreason " & _
            '            "FROM    #tbl a " & _
            '                    "JOIN #tbl AS b ON a.employeeunkid = b.employeeunkid " & _
            '                                      "AND a.loanadvancetranunkid = b.loanadvancetranunkid " & _
            '                                      "AND b.unkid <= a.unkid " & _
            '            "GROUP BY a.loanadvancetranunkid " & _
            '                  ", a.loanschemeunkid " & _
            '                  ", a.employeeunkid " & _
            '                  ", a.loanbf_amount " & _
            '                  ", a.enddate " & _
            '                  ", a.periodunkid " & _
            '                  ", a.payrollprocesstranunkid " & _
            '                  ", a.paymenttranunkid " & _
            '                  ", a.amount " & _
            '            "ORDER BY a.loanadvancetranunkid " & _
            '                  ", CONVERT(CHAR(8), a.enddate, 112) " & _
            '                  ", a.loanbf_amount - SUM(b.amount) + a.amount DESC " & _
            '    " " & _
            '    "DROP TABLE #tbl "

            'objDataOperation.AddParameter("@ClosingPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId)
            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            'objDataOperation.AddParameter("@db_start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date))
            'objDataOperation.AddParameter("@db_end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(FinancialYear._Object._Database_End_Date))

            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'Sohail (29 Jan 2016) -- Start
            'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
            'dsLoan = objLoanAdvance.Calculate_LoanBalanceInfo("Loan", mdtPeriodEndDate.AddDays(1), "", 0, mdtPeriodStartDate, True, , , True)

            'Nilay (25-Mar-2016) -- Start
            'dsLoan = objLoanAdvance.Calculate_LoanBalanceInfo("Loan", mdtPeriodEndDate.AddDays(1), "", 0, mdtPeriodStartDate, True, , , True, True, , , True)
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'dsLoan = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
            '                                                  xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
            '                                                  "Loan", mdtPeriodEndDate.AddDays(1), "", 0, mdtPeriodStartDate, True, , , True, True, , , True)
            dsLoan = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                              xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                             "Loan", mdtPeriodEndDate.AddDays(1), "", 0, mdtPeriodStartDate, True, , , True, True, , , True, , , , , , , , , objDataOperation)
            'Sohail (18 Jan 2019) -- End
            'Nilay (25-Mar-2016) -- End

            'Sohail (29 Jan 2016) -- End
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'Dim dsPayrolLoan As DataSet = objProcess.GetProcessedLoanData("List", mintPeriodUnkId.ToString, "", "")
            Dim dsPayrolLoan As DataSet = objProcess.GetProcessedLoanData("List", mintPeriodUnkId.ToString, "", "", objDataOperation)
            'Sohail (18 Jan 2019) -- End

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            If bw IsNot Nothing Then
                clsClosePeriod._ProgressTotalCount = dsLoan.Tables("Loan").Rows.Count
                bw.ReportProgress(8)
            End If
            'Sohail (12 Dec 2015) -- End

            For Each dsRow As DataRow In dsLoan.Tables("Loan").Rows

                'Sohail (21 Sep 2019) -- Start
                'NMB Enhancement # : Show update loan balance count on close period.
                clsClosePeriod._ProgressCurrCount = dsLoan.Tables("Loan").Rows.IndexOf(dsRow) + 1
                bw.ReportProgress(8)
                'Sohail (21 Sep 2019) -- End

                'S.SANDEEP [16 JUN 2015] -- START
                Dim dtmp() As DataRow = dsPayrolLoan.Tables(0).Select("loanadvancetranunkid = '" & dsRow.Item("loanadvancetranunkid") & "' AND isloan = True")
                'Sohail (09 Mar 2018) -- Start
                'Support issue Id # 0002089 : (Loan End Date and tenure were not getting incremented by 1 month when they are ON HOLD). Loan top up issue there is a loan supposed to end in march, but in loan topup, the system is showing that the loan end date is 28 February, this is causing the system to refuse installment amount change It's a similar case to PRESIDENTIAL TRUST FUND issue # 0002084: Unable to add loan installment in 70.2.
                'If dtmp.Length <= 0 Then Continue For
                If CBool(dsRow.Item("isloan")) = False Then Continue For
                If CInt(dsRow.Item("statusunkid")) <> CInt(enLoanStatus.ON_HOLD) AndAlso dtmp.Length <= 0 Then Continue For
                'Sohail (09 Mar 2018) -- End
                'S.SANDEEP [16 JUN 2015] -- END


                Dim intLoanAdvanceUnkId As Integer = 0
                Dim decBalanceAmount As Decimal = 0
                Dim decBalanceAmountPaidCurrency As Decimal = 0
                Dim intDaysDiff As Integer = 0
                Dim decIntRate As Decimal = 0
                Dim decTotPMT As Decimal = 0
                Dim decTotPMTPaidCurrency As Decimal = 0
                Dim decTotIntAmt As Decimal = 0
                Dim decTotIntAmtPaidCurrency As Decimal = 0
                Dim decTotPrincipalAmt As Decimal = 0
                Dim decTotPrincipalAmtPaidCurrency As Decimal = 0
                Dim intLoanSchemeUnkId As Integer = 0
                Dim intNextEffectiveDays As Integer = 0
                Dim intNextEffectiveMonths As Integer = 0 'Sohail (15 Dec 2015)
                Dim decCFAmt As Decimal = 0
                Dim decCFAmtPaidCurrency As Decimal = 0
                Dim intProcessPayrollID As Integer = 0

                With dsRow
                    intLoanAdvanceUnkId = CInt(.Item("loanadvancetranunkid"))
                    decBalanceAmount = CDec(.Item("BalanceAmount"))
                    decBalanceAmountPaidCurrency = CDec(.Item("BalanceAmountPaidCurrency"))
                    intDaysDiff = CInt(.Item("DaysDiff"))
                    decIntRate = CDec(.Item("interest_rate"))
                    decTotPMT = CDec(.Item("TotalMonthlyDeduction").ToString) + CDec(.Item("TotPMTAmount").ToString)
                    decTotPMTPaidCurrency = CDec(.Item("TotalMonthlyDeductionPaidCurrency").ToString) + CDec(.Item("TotPMTAmountPaidCurrency").ToString)
                    decTotIntAmt = CDec(.Item("TotInterestAmount"))
                    decTotIntAmtPaidCurrency = CDec(.Item("TotInterestAmountPaidCurrency"))
                    decTotPrincipalAmt = CDec(.Item("TotPrincipalAmount"))
                    decTotPrincipalAmtPaidCurrency = CDec(.Item("TotPrincipalAmountPaidCurrency"))
                    decCFAmt = CDec(.Item("LastProjectedBalance"))
                    decCFAmtPaidCurrency = CDec(.Item("LastProjectedBalancePaidCurrency"))
                    intNextEffectiveDays = CInt(.Item("nexteffective_days"))
                    intNextEffectiveMonths = CInt(.Item("nexteffective_months")) 'Sohail (15 Dec 2015)
                    intLoanSchemeUnkId = CInt(.Item("loanschemeunkid"))

                    'Sohail (29 Jan 2016) -- Start
                    'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
                    'If decTotPMTPaidCurrency > CDec(.Item("PrincipalBalancePaidCurrency").ToString) Then
                    '    decTotPMT = CDec(.Item("PrincipalBalance").ToString)
                    '    decTotPMTPaidCurrency = CDec(.Item("PrincipalBalancePaidCurrency").ToString)
                    'End If
                    If CInt(.Item("statusunkid")) = enLoanStatus.WRITTEN_OFF OrElse CInt(.Item("statusunkid")) = enLoanStatus.COMPLETED Then
                        'If decTotPMTPaidCurrency > CDec(.Item("PrincipalBalancePaidCurrency").ToString) Then
                        '    decTotPMT = CDec(.Item("PrincipalBalance").ToString)
                        '    decTotPMTPaidCurrency = CDec(.Item("PrincipalBalancePaidCurrency").ToString)
                        'End If
                    Else
                        'Sohail (07 Jan 2020) -- Start
                        'Le Grand Cassino issue : loan balance mismatch issue on close period when topup is taken on last emi period.
                        'If decTotPMTPaidCurrency > CDec(.Item("PrincipalBalancePaidCurrency").ToString) Then
                        '    decTotPMT = CDec(.Item("PrincipalBalance").ToString)
                        '    decTotPMTPaidCurrency = CDec(.Item("PrincipalBalancePaidCurrency").ToString)
                        'End If
                        If decTotPMTPaidCurrency > (CDec(.Item("TotalMonthlyDeductionPaidCurrency").ToString) + CDec(.Item("TotPMTAmountPaidCurrency").ToString)) Then
                            decTotPMT = CDec(.Item("TotalMonthlyDeduction").ToString) + CDec(.Item("TotPMTAmount").ToString)
                            decTotPMTPaidCurrency = CDec(.Item("TotalMonthlyDeductionPaidCurrency").ToString) + CDec(.Item("TotPMTAmountPaidCurrency").ToString)
                    End If
                        'Sohail (07 Jan 2020) -- End
                    End If
                    'Sohail (29 Jan 2016) -- End
                   
                    If decCFAmtPaidCurrency < 0 Then
                        decCFAmt = 0
                        decCFAmtPaidCurrency = 0
                    End If

                    'Sohail (08 Mar 2016) -- Start
                    'Dim strEmpName As String = ""
                    'Dim strLoanScheme As String = ""
                    strEmpName = ""
                    strLoanScheme = ""
                    'Sohail (08 Mar 2016) -- End
                    Dim decPayrollAmt As Decimal = 0
                    Dim decPayrollAmtPaidCurrency As Decimal = 0

                    Dim dr_Row As List(Of DataRow) = (From p In dsPayrolLoan.Tables("List") Where (CInt(p.Item("loanadvancetranunkid")) = intLoanAdvanceUnkId) Select (p)).ToList

                    If dr_Row.Count > 0 Then
                        strEmpName = dr_Row(0).Item("employeename").ToString
                        strLoanScheme = dr_Row(0).Item("SchemeName").ToString
                        intProcessPayrollID = CInt(dr_Row(0).Item("payrollprocesstranunkid"))
                        decPayrollAmt = (From p In dr_Row Select (CDec(p.Item("Amount")))).DefaultIfEmpty.Sum
                        decPayrollAmtPaidCurrency = (From p In dr_Row Select (CDec(p.Item("AmountPaidCurrency")))).DefaultIfEmpty.Sum
                    End If

                    'Sohail (03 Feb 2016) -- Start
                    'If decPayrollAmtPaidCurrency <> Format(decTotPMTPaidCurrency, "###.00####") Then
                    'Hemant (17 Apr 2019) -- Start
                    'isssue # 0003757(ENGENDER HEALTH TANZANIA): Unable to close march payroll period
                    'If Math.Abs(decPayrollAmtPaidCurrency - CDec(Format(decTotPMTPaidCurrency, "###.00####"))) > 0.05 Then
                    'Hemant (16 Aug 2019) -- Start
		            'ISSUE(PACRA): We are processing and closing PACRA periods on test server. we have managed to close August successfully but September is failing.
                    'If Math.Abs(CDec(Format(decPayrollAmtPaidCurrency, "###.00####")) - CDec(Format(decTotPMTPaidCurrency, "###.00####"))) > 0.05 Then
                    If Math.Abs(CDec(Format(decPayrollAmtPaidCurrency, "###.00####")) - CDec(Format(decTotPMTPaidCurrency, "###.00####"))) > 1 Then
                        'Hemant (16 Aug 2019) -- End
                        'Hemant (17 Apr 2019) -- End
                        'Sohail (03 Feb 2016) -- End
                        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Current loan deduction does not match with process payroll loan deduction. Please do process payroll again for the following employee.") & vbCrLf & "Employee : " & strEmpName & vbCrLf & "Loan Scheme : " & strLoanScheme & vbCrLf & "Current Deduction : " & Format(decTotPMTPaidCurrency, GUI.fmtCurrency) & vbCrLf & "Processed Loan Deduction : " & Format(decPayrollAmtPaidCurrency, GUI.fmtCurrency)
                        Return False
                        Exit Function
                    End If

                End With

                '*** Do not change cf_amount (keep amount same as previous cf_amount amount) as it will be used in PMT function for Reducing balance loan calculation
                strQ = "INSERT  INTO lnloan_balance_tran " & _
                        "( loanschemeunkid " & _
                            ", transaction_periodunkid " & _
                            ", transactiondate " & _
                        ", periodunkid " & _
                        ", end_date " & _
                        ", loanadvancetranunkid " & _
                        ", employeeunkid " & _
                        ", payrollprocesstranunkid " & _
                        ", paymenttranunkid " & _
                        ", bf_amount " & _
                            ", bf_amountpaidcurrency " & _
                        ", amount " & _
                            ", amountpaidcurrency " & _
                        ", cf_amount " & _
                            ", cf_amountpaidcurrency " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                            ", lninteresttranunkid " & _
                            ", lnemitranunkid " & _
                            ", lntopuptranunkid " & _
                            ", days " & _
                            ", bf_balance " & _
                            ", bf_balancepaidcurrency " & _
                            ", principal_amount " & _
                            ", principal_amountpaidcurrency " & _
                            ", topup_amount " & _
                            ", topup_amountpaidcurrency " & _
                            ", repayment_amount " & _
                            ", repayment_amountpaidcurrency " & _
                            ", interest_rate " & _
                            ", interest_amount " & _
                            ", interest_amountpaidcurrency " & _
                            ", cf_balance " & _
                            ", cf_balancepaidcurrency " & _
                            ", isonhold " & _
                            ", isreceipt  " & _
                            ", nexteffective_days " & _
                            ", nexteffective_months " & _
                            ", exchange_rate " & _
                            ", isbrought_forward " & _
                            ", loanstatustranunkid " & _
                        ") " & _
                    "VALUES  ( " & intLoanSchemeUnkId & "  " & _
                            ", " & mintPeriodUnkId & " " & _
                            ", '" & Format(mdtPeriodEndDate, "yyyyMMdd HH:mm:ss") & "' " & _
                            ", " & mintPeriodUnkId & " " & _
                            ", '" & Format(mdtPeriodEndDate, "yyyyMMdd HH:mm:ss") & "' " & _
                            ", " & intLoanAdvanceUnkId & " " & _
                            ", " & CInt(dsRow.Item("employeeunkid")) & " " & _
                            ", " & intProcessPayrollID & " " & _
                            ", 0 " & _
                            ", " & CDec(dsRow.Item("BF_Amount").ToString) & " " & _
                            ", " & CDec(dsRow.Item("BF_AmountPaidCurrency").ToString) & " " & _
                            ", " & CDec(dsRow.Item("TotPMTAmount").ToString) & " " & _
                            ", " & CDec(dsRow.Item("TotPMTAmountPaidCurrency").ToString) & " " & _
                            ", " & CDec(dsRow.Item("PrincipalBalance")) & " " & _
                            ", " & CDec(dsRow.Item("PrincipalBalancePaidCurrency")) & " " & _
                            ", " & xUserUnkid & " " & _
                            ", 0 " & _
                            ", -1 " & _
                            ", NULL " & _
                            ", '' " & _
                            ", -1 " & _
                            ", -1 " & _
                            ", -1 " & _
                            ", " & intDaysDiff & " " & _
                            ", " & CDec(IIf(CInt(Int(dsRow.Item("isonhold"))) = 1, decCFAmt, decBalanceAmount)) & " " & _
                            ", " & CDec(IIf(CInt(Int(dsRow.Item("isonhold"))) = 1, decCFAmtPaidCurrency, decBalanceAmountPaidCurrency)) & " " & _
                            ", " & decTotPrincipalAmt & " " & _
                            ", " & decTotPrincipalAmtPaidCurrency & " " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", " & decIntRate & " " & _
                            ", " & decTotIntAmt & " " & _
                            ", " & decTotIntAmtPaidCurrency & " " & _
                            ", " & decCFAmt & " " & _
                            ", " & decCFAmtPaidCurrency & " " & _
                            ", " & CInt(Int(dsRow.Item("isonhold"))) & " " & _
                            ", 0 " & _
                            ", " & intNextEffectiveDays & " " & _
                            ", " & intNextEffectiveMonths & " " & _
                            ", " & CDec(dsRow.Item("exchange_rate")) & " " & _
                            ", 0 " & _
                            ", 0 " & _
                        ") "
                'Nilay (25-Mar-2016) -- [xUserUnkid]

                'Sohail (03 Feb 2016) - [cf_amount = CDec(dsRow.Item("BalanceAmount")) ], [cf_amountpaidcurrency = CDec(dsRow.Item("BalanceAmountPaidCurrency")) ]
                'Sohail (15 Dec 2015) - [loanstatustranunkid,nexteffective_months]

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If CInt(dsRow.Item("statusunkid")) = enLoanStatus.ON_HOLD Then
                    Dim objlnEMI As New clslnloan_emitenure_tran
                    Dim objExRate As New clsExchangeRate

                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    Dim intEmiUnkId As Integer = 0
                    'Sohail (18 Jan 2019) -- Start
                    'Issue - 74.1 - Bind Transaction issue.
                    objlnEMI._DataOperation = objDataOperation
                    'Sohail (18 Jan 2019) -- End
                    If objlnEMI.isExist(mdtPeriodStartDate, intLoanAdvanceUnkId, , intEmiUnkId) = True Then 'For First Period (Assigned Period) On Hold
                        objlnEMI._Lnemitranunkid = intEmiUnkId
                    End If
                    'Sohail (15 Dec 2015) -- End

                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    'objlnEMI._Effectivedate = mdtNextPeriodStartDate
                    objlnEMI._Effectivedate = mdtPeriodStartDate
                    'Sohail (07 May 2015) -- End
                    objlnEMI._Emi_Tenure = CInt(dsRow.Item("emi_tenure")) + 1
                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    'objlnEMI._End_Date = eZeeDate.convertDate(dsRow.Item("LoanEndDate").ToString).AddMonths(1)
                    objlnEMI._End_Date = eZeeDate.convertDate(dsRow.Item("LoanEndDate").ToString).AddDays(1).AddMonths(1).AddDays(-1)
                    'Sohail (07 May 2015) -- End

                    'Dim dsExRate As DataSet = objExRate.GetList("ExRate", True, , , CInt(dsRow.Item("countryunkid")), True, mdtNextPeriodStartDate, True)
                    'If dsExRate.Tables("ExRate").Rows.Count > 0 Then
                    '    objlnEMI._Emi_Amount = CDec(dsRow.Item("emi_amount")) * CDec(dsExRate.Tables("ExRate").Rows(0).Item("exchange_rate"))
                    '    objlnEMI._Exchange_rate = CDec(dsExRate.Tables("ExRate").Rows(0).Item("exchange_rate"))
                    '    objlnEMI._Basecurrency_amount = CDec(dsRow.Item("emi_amount"))
                    'Else
                    '    objlnEMI._Emi_Amount = CDec(dsRow.Item("emi_amount"))
                    '    objlnEMI._Exchange_rate = 1
                    '    objlnEMI._Basecurrency_amount = CDec(dsRow.Item("emi_amount"))
                    'End If
                    objlnEMI._Emi_Amount = CDec(dsRow.Item("emi_amount")) * CDec(dsRow.Item("exchange_rate"))
                    objlnEMI._Exchange_rate = CDec(dsRow.Item("exchange_rate"))
                    objlnEMI._Basecurrency_amount = CDec(dsRow.Item("emi_amount"))
                    objlnEMI._Loan_Duration = CInt(dsRow.Item("emi_tenure")) + 1
                    objlnEMI._Isvoid = False
                    objlnEMI._Voiddatetime = Nothing
                    objlnEMI._Voidreason = ""
                    objlnEMI._Voiduserunkid = -1
                    objlnEMI._Userunkid = User._Object._Userunkid
                    objlnEMI._Loanadvancetranunkid = intLoanAdvanceUnkId
                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    'objlnEMI._Periodunkid = mintNextPeriodUnkId
                    objlnEMI._Periodunkid = mintPeriodUnkId
                    'Sohail (07 May 2015) -- End

                    Dim bln_Flag As Boolean = False
                    objlnEMI._DataOperation = objDataOperation
                    'Nilay (18-Nov-2015) -- Start
                    'bln_Flag = objlnEMI.Insert(True)
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'bln_Flag = objlnEMI.Insert(True, intYearunkid)
                    If intEmiUnkId <= 0 Then
                        Dim decLastInstlAmt As Decimal = 0
                        Dim ds = objlnEMI.GetLastEMI("LastEMI", intLoanAdvanceUnkId, mdtPeriodStartDate)
                        If ds.Tables("LastEMI").Rows.Count > 0 Then
                            decLastInstlAmt = CDec(ds.Tables("LastEMI").Rows(0).Item("principal_amount"))
                        End If
                        Dim decInstlAmt As Decimal = 0
                        Dim decIntrstAmt As Decimal = 0
                        objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(dsRow.Item("BalanceAmountPaidCurrency")), intNextEffectiveDays - DateDiff(DateInterval.Day, mdtPeriodStartDate, mdtPeriodEndDate.AddDays(1)), decIntRate, CType(CInt(dsRow.Item("calctype_id")), enLoanCalcId), CType(CInt(dsRow.Item("interest_calctype_id")), enLoanInterestCalcType), intNextEffectiveMonths - 1, DateDiff(DateInterval.Day, mdtNextPeriodStartDate, mdtNextPeriodEndDate.AddDays(1)), decLastInstlAmt, decIntrstAmt, decInstlAmt, 0, 0)
                        objlnEMI._Principal_amount = decInstlAmt - decIntrstAmt
                        objlnEMI._Interest_amount = decIntrstAmt
                        objlnEMI._Emi_Amount = decInstlAmt
                        objlnEMI._Basecurrency_amount = decInstlAmt / CDec(dsRow.Item("exchange_rate"))
                        'Sohail (18 Jan 2019) -- Start
                        'Issue - 74.1 - Bind Transaction issue.
                        objlnEMI._DataOperation = objDataOperation
                        'Sohail (18 Jan 2019) -- End
                        'Nilay (25-Mar-2016) -- Start
                        'bln_Flag = objlnEMI.Insert(True, intYearunkid)
                        'Hemant (04 Aug 2018) -- Start
                        'EnhanceMent : To Stop Entry in lnLoan_balance_TRan while Close period of ONHOLD loan as Entry already done in lnLoan_balance_TRan in above code when loan is deducted in lnLoan_balance_TRan
                        'bln_Flag = objlnEMI.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                        '                           xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, True)
                        bln_Flag = objlnEMI.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                  xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, False)
                        'Hemant (04 Aug 2018) -- End
                        'Nilay (25-Mar-2016) -- End
                    Else
                        bln_Flag = objlnEMI.Update()
                    End If
                    'Sohail (15 Dec 2015) -- End
                    'Nilay (18-Nov-2015) -- End

                    If bln_Flag = False Then
                        If objlnEMI._Message <> "" Then
                        exForce = New Exception(objlnEMI._Message)
                        Throw exForce
                        Else
                            mstrMessage = "Error from Insert from clsloan_emi_tran."
                        End If
                        Return False
                    End If

                    objlnEMI = Nothing
                    'objExRate = Nothing

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                ElseIf CInt(dsRow.Item("statusunkid")) = enLoanStatus.IN_PROGRESS AndAlso CInt(dsRow.Item("calctype_id")) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                    Dim objlnEMI As New clslnloan_emitenure_tran
                    Dim objExRate As New clsExchangeRate

                    Dim intEmiUnkId As Integer = 0
                    objlnEMI._DataOperation = objDataOperation
                    If objlnEMI.isExist(mdtPeriodStartDate, intLoanAdvanceUnkId, , intEmiUnkId) = True Then 'For First Period (Assigned Period) On Hold
                        objlnEMI._Lnemitranunkid = intEmiUnkId
                    Else
                        Dim ds = objlnEMI.GetLastEMI("LastEMI", intLoanAdvanceUnkId, mdtPeriodStartDate)
                        If ds.Tables("LastEMI").Rows.Count > 0 Then
                            intEmiUnkId = CInt(ds.Tables("LastEMI").Rows(0).Item("lnemitranunkid"))
                            objlnEMI._Lnemitranunkid = intEmiUnkId
                        End If
                    End If

                    objlnEMI._Effectivedate = mdtPeriodStartDate
                    objlnEMI._Emi_Tenure = CInt(dsRow.Item("emi_tenure")) + 1
                    objlnEMI._End_Date = eZeeDate.convertDate(dsRow.Item("LoanEndDate").ToString).AddDays(1).AddMonths(1).AddDays(-1)
                    
                    objlnEMI._Emi_Amount = CDec(dsRow.Item("emi_amount")) * CDec(dsRow.Item("exchange_rate"))
                    objlnEMI._Exchange_rate = CDec(dsRow.Item("exchange_rate"))
                    objlnEMI._Basecurrency_amount = CDec(dsRow.Item("emi_amount"))
                    objlnEMI._Loan_Duration = CInt(dsRow.Item("emi_tenure")) + 1
                    objlnEMI._Isvoid = False
                    objlnEMI._Voiddatetime = Nothing
                    objlnEMI._Voidreason = ""
                    objlnEMI._Voiduserunkid = -1
                    objlnEMI._Userunkid = User._Object._Userunkid
                    objlnEMI._Loanadvancetranunkid = intLoanAdvanceUnkId
                    objlnEMI._Periodunkid = mintPeriodUnkId

                    Dim bln_Flag As Boolean = False
                    objlnEMI._DataOperation = objDataOperation
                    If intEmiUnkId <= 0 Then
                        Dim decLastInstlAmt As Decimal = 0
                        Dim ds = objlnEMI.GetLastEMI("LastEMI", intLoanAdvanceUnkId, mdtPeriodStartDate)
                        If ds.Tables("LastEMI").Rows.Count > 0 Then
                            decLastInstlAmt = CDec(ds.Tables("LastEMI").Rows(0).Item("principal_amount"))
                        End If
                        Dim decInstlAmt As Decimal = 0
                        Dim decIntrstAmt As Decimal = 0
                        objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(dsRow.Item("BalanceAmountPaidCurrency")), intNextEffectiveDays - DateDiff(DateInterval.Day, mdtPeriodStartDate, mdtPeriodEndDate.AddDays(1)), decIntRate, CType(CInt(dsRow.Item("calctype_id")), enLoanCalcId), CType(CInt(dsRow.Item("interest_calctype_id")), enLoanInterestCalcType), intNextEffectiveMonths - 1, DateDiff(DateInterval.Day, mdtNextPeriodStartDate, mdtNextPeriodEndDate.AddDays(1)), decLastInstlAmt, decIntrstAmt, decInstlAmt, 0, 0)
                        objlnEMI._Principal_amount = decInstlAmt - decIntrstAmt
                        objlnEMI._Interest_amount = decIntrstAmt
                        objlnEMI._Emi_Amount = decInstlAmt
                        objlnEMI._Basecurrency_amount = decInstlAmt / CDec(dsRow.Item("exchange_rate"))
                        objlnEMI._DataOperation = objDataOperation
                        bln_Flag = objlnEMI.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                  xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, False)
                    Else
                        bln_Flag = objlnEMI.Update()
                    End If

                    If bln_Flag = False Then
                        If objlnEMI._Message <> "" Then
                            exForce = New Exception(objlnEMI._Message)
                            Throw exForce
                        Else
                            mstrMessage = "Error from Insert from clsloan_emi_tran."
                        End If
                        Return False
                    End If

                    objlnEMI = Nothing
                    'Sohail (29 Apr 2019) -- End

                End If
            Next
            'Sohail (07 May 2015) -- End
            'Sohail (26 Aug 2013) - [WHERE   ISNULL(isvoid, 0) = 0 ]
            'Hemant (29 Jan 2019) -- Start
            'PACRA Issue - 74.1 - Full EMI deducted for simple interest loans even if loan balance is less than EMI.
            strQ = "SELECT * INTO #Balance " & _
                    "FROM " & _
                    "( " & _
                        "SELECT loanadvancetranunkid " & _
                             ", cf_balance " & _
                             ", cf_balancepaidcurrency " & _
                             ", DENSE_RANK() OVER (PARTITION BY loanadvancetranunkid ORDER BY transactiondate DESC, loanbalancetranunkid DESC ) AS ROWNO " & _
                        "FROM lnloan_balance_tran " & _
                        "WHERE isvoid = 0 " & _
                    ") AS A " & _
                    "WHERE A.ROWNO = 1 " & _
                    " " & _
                    ";UPDATE lnloan_advance_tran " & _
                    "SET balance_amount = #Balance.cf_balance " & _
                        ", balance_amountpaidcurrency = #Balance.cf_balancepaidcurrency " & _
                    "FROM #Balance " & _
                    "WHERE lnloan_advance_tran.loanadvancetranunkid = #Balance.loanadvancetranunkid " & _
                          "AND lnloan_advance_tran.isvoid = 0 " & _
                          "AND ( " & _
                                  "lnloan_advance_tran.balance_amount <> #Balance.cf_balance " & _
                                  "OR lnloan_advance_tran.balance_amountpaidcurrency <> #Balance.cf_balancepaidcurrency " & _
                              ") " & _
                    " " & _
                    "DROP TABLE #Balance "
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Hemant (29 Jan 2019) -- End
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Loan_Balance_Data; Module Name: " & mstrModuleName & " [" & strEmpName & "; " & strLoanScheme & "]")
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
            clsClosePeriod._ProgressTotalCount = 0
        End Try
    End Function
    'Sohail (27 Sep 2012) -- End

    'Sohail (12 Nov 2014) -- Start
    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
    Private Function UpdateClaimRequest_Posted(ByVal intUserunkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]
        'Sohail (21 Aug 2015) - [intUserunkid]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try

            strQ = "UPDATE  cmclaim_process_tran " & _
                    "SET     isprocessed = 1  " & _
                          ", processuserunkid = @processuserunkid " & _
                          ", processdatetime = GETDATE() " & _
                    "WHERE   isvoid = 0 " & _
                            "AND periodunkid = @periodunkid " & _
                            "AND isposted = 1 " & _
                            "AND ISNULL(isprocessed, 0) = 0 "

            objDataOperation.ClearParameters()

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@processuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@processuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateClaimRequest_Posted; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
    End Function
    'Sohail (12 Nov 2014) -- End

    'Sohail (12 Jan 2015) -- Start
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    Private Function Insert_Saving_Balance_Data(ByVal intUserunkid As Integer _
                                                , ByVal dtDatabaseStartDate As Date _
                                                , ByVal dtDatabaseEndDate As Date _
                                                , Optional ByVal bw As BackgroundWorker = Nothing _
                                                , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                ) As Boolean
        'Sohail (18 Jan 2019) - [xDataOp]
        'Sohail (12 Dec 2015) - [bw]
        'Sohail (21 Aug 2015) - [intUserunkid, dtDatabaseStartDate, dtDatabaseEndDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim strEmployeeIDs As String = ""

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End


        Try

            'Sohail (05 Mar 2019) -- Start
            'KBC Issue - 74.1 - Voiding saving balance daa if close period was not done successfully.
            strQ = "UPDATE svsaving_balance_tran SET isvoid = 1, voiduserunkid = " & intUserunkid & ", voiddatetime = GETDATE(), voidreason = 'close period roll back' WHERE isvoid = 0 AND periodunkid = " & mintPeriodUnkId & " "

            Dim intRowAffect As Integer = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (05 Mar 2019) -- End

            'Sohail (23 Jun 2016) -- Start
            'Issue - 61.1 - [Issue in AND b.ROWNO < a.ROWNO line] Taking too much time at CCBRT (more than 2-3 hours) AS they don't have any other saving transactions (withdrawal, deposit, repayment) other than saving deduction.
            strQ = "CREATE TABLE #t " & _
                    "( " & _
                      "savingschemeunkid INT  " & _
                    ", periodunkid INT " & _
                    ", savingtranunkid INT " & _
                    ", employeeunkid INT " & _
                    ", bf_balance DECIMAL(36, 6) " & _
                    ", bf_amount DECIMAL(36, 6) " & _
                    ", amount DECIMAL(36, 6) " & _
                    ", Withdrawal DECIMAL(36, 6) " & _
                    ", Repayment DECIMAL(36, 6) " & _
                    ", Deposit DECIMAL(36, 6) " & _
                    ", effective_date DATETIME " & _
                    ", interest_rate DECIMAL(36, 6) " & _
                    ", ROWNO INT NOT NULL IDENTITY(1, 1)  " & _
                    ", cf_amount DECIMAL(36, 6) " & _
                    ", days INT " & _
                    ", interest_amount DECIMAL(36, 6) " & _
                    ", cf_balance DECIMAL(36, 6) " & _
                    ", startdate DATETIME " & _
                    ", payrollprocesstranunkid INT " & _
                    ", paymenttranunkid INT " & _
                    ", savingcontributiontranunkid INT " & _
                    ", savinginterestratetranunkid INT " & _
                    ") ; "
            'Sohail (23 Jun 2016) -- End

            strQ &= "WITH    ce " & _
                             "AS ( SELECT   svsaving_tran.savingschemeunkid " & _
                                         ", TableSaving.payperiodunkid AS periodunkid " & _
                                         ", TableSaving.savingtranunkid " & _
                                         ", TableSaving.employeeunkid " & _
                                         ", payrollprocesstranunkid " & _
                                         ", paymenttranunkid " & _
                                         ", savingcontributiontranunkid " & _
                                         ", savinginterestratetranunkid " & _
                                         ", ISNULL(TableBF.cf_balance, svsaving_tran.bf_balance) AS bf_balance " & _
                                         ", ISNULL(TableBF.cf_amount, svsaving_tran.bf_amount) AS bf_amount " & _
                                         ", SUM(TableSaving.amount) AS amount " & _
                                         ", SUM(TableSaving.Withdrawal) AS Withdrawal " & _
                                         ", SUM(TableSaving.Repayment) AS Repayment " & _
                                         ", SUM(TableSaving.Deposit) AS Deposit " & _
                                         ", TableSaving.effectivedate AS effective_date " & _
                                         ", TableSaving.interest_rate " & _
                                         ", cfcommon_period_tran.start_date AS startdate " & _
                                         ", ROW_NUMBER() OVER ( PARTITION BY TableSaving.savingtranunkid ORDER BY TableSaving.effectivedate ) AS ROWNO " & _
                                  "FROM     ( SELECT    prtnaleave_tran.payperiodunkid " & _
                                                     ", prpayrollprocess_tran.employeeunkid " & _
                                                     ", prpayrollprocess_tran.savingtranunkid AS savingtranunkid " & _
                                                     ", prpayrollprocess_tran.amount " & _
                                                     ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                                                     ", 0 AS paymenttranunkid " & _
                                                     ", 0 AS savingcontributiontranunkid " & _
                                                     ", 0 AS savinginterestratetranunkid " & _
                                                     ", 0 AS Withdrawal " & _
                                                     ", 0 AS Repayment " & _
                                                     ", 0 AS Deposit " & _
                                                     ", cfcommon_period_tran.end_date AS effectivedate " & _
                                                     ", svsaving_interest_rate_tran.interest_rate " & _
                                                     ", DENSE_RANK() OVER ( PARTITION BY svsaving_tran.savingtranunkid ORDER BY intrateperiod.end_date DESC, svsaving_interest_rate_tran.effectivedate DESC ) AS ROWNO " & _
                                             "FROM      prpayrollprocess_tran " & _
                                                       "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                       "JOIN svsaving_tran ON prpayrollprocess_tran.savingtranunkid = svsaving_tran.savingtranunkid " & _
                                                       "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
                                                       "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "LEFT JOIN svsaving_interest_rate_tran ON svsaving_tran.savingtranunkid = svsaving_interest_rate_tran.savingtranunkid " & _
                                                       "LEFT JOIN cfcommon_period_tran AS intrateperiod ON intrateperiod.periodunkid = svsaving_interest_rate_tran.periodunkid " & _
                                                       "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                             "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                       "AND prtnaleave_tran.isvoid = 0 " & _
                                                       "AND svsaving_tran.isvoid = 0 " & _
                                                       "AND svsavingscheme_master.isvoid = 0 " & _
                                                       "AND cfcommon_period_tran.isactive = 1 " & _
                                                       "AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                                                       "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                                       "AND cfcommon_period_tran.periodunkid = @ClosingPeriodId " & _
                                                       "AND intrateperiod.end_date <= cfcommon_period_tran.end_date " & _
                                                       "AND svsaving_tran.bf_amount IS NOT NULL " & _
                                             "UNION ALL " & _
                                             "SELECT    Payment.periodunkid " & _
                                                     ", employeeunkid " & _
                                                     ", Payment.savingtranunkid " & _
                                                     ", 0 AS payrollprocesstranunkid " & _
                                                     ", Payment.paymenttranunkid AS paymenttranunkid " & _
                                                     ", 0 AS savingcontributiontranunkid " & _
                                                     ", 0 AS savinginterestratetranunkid " & _
                                                     ", 0 AS amount " & _
                                                     ", CASE Payment.paytypeid WHEN " & clsPayment_tran.enPayTypeId.WITHDRAWAL & " THEN amount ELSE 0 END AS Withdrawal " & _
                                                     ", CASE Payment.paytypeid WHEN " & clsPayment_tran.enPayTypeId.REPAYMENT & " THEN amount ELSE 0 END AS Repayment " & _
                                                     ", CASE Payment.paytypeid WHEN " & clsPayment_tran.enPayTypeId.DEPOSIT & " THEN amount ELSE 0 END AS Deposit " & _
                                                     ", Payment.paymentdate " & _
                                                     ", svsaving_interest_rate_tran.interest_rate " & _
                                                     ", DENSE_RANK() OVER ( PARTITION BY Payment.savingtranunkid ORDER BY intrateperiod.end_date DESC, svsaving_interest_rate_tran.effectivedate DESC ) AS ROWNO " & _
                                             "FROM      ( SELECT    ( SELECT    C.periodunkid " & _
                                                                     "FROM      cfcommon_period_tran AS C " & _
                                                                     "WHERE     CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) BETWEEN C.start_date AND C.end_date " & _
                                                                               "AND c.modulerefid = " & enModuleReference.Payroll & " " & _
                                                                               "AND c.isactive = 1 " & _
                                                                   ") AS periodunkid " & _
                                                                 ", prpayment_tran.employeeunkid " & _
                                                                 ", prpayment_tran.paymenttranunkid " & _
                                                                 ", prpayment_tran.referencetranunkid AS savingtranunkid " & _
                                                                 ", prpayment_tran.amount " & _
                                                                 ", prpayment_tran.paymentdate " & _
                                                                 ", prpayment_tran.paytypeid " & _
                                                         "FROM      prpayment_tran " & _
                                                                   "JOIN svsaving_tran ON svsaving_tran.savingtranunkid = prpayment_tran.referencetranunkid " & _
                                                                   "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_tran.employeeunkid " & _
                                                                   "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
                                                         "WHERE     prpayment_tran.isvoid  = 0 " & _
                                                                   "AND svsaving_tran.isvoid = 0 " & _
                                                                    "AND svsavingscheme_master.isvoid = 0 " & _
                                                                   "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.SAVINGS & " " & _
                                                                   "AND prpayment_tran.paytypeid IN ( " & clsPayment_tran.enPayTypeId.WITHDRAWAL & ", " & clsPayment_tran.enPayTypeId.REPAYMENT & ", " & clsPayment_tran.enPayTypeId.DEPOSIT & " ) " & _
                                                                   "AND prpayment_tran.paymentdate BETWEEN @db_start_date AND @db_end_date " & _
                                                       ") AS Payment " & _
                                                       "JOIN cfcommon_period_tran ON Payment.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                 "AND cfcommon_period_tran.periodunkid = @ClosingPeriodId " & _
                                                       "LEFT JOIN svsaving_interest_rate_tran ON Payment.savingtranunkid = svsaving_interest_rate_tran.savingtranunkid " & _
                                                       "LEFT JOIN cfcommon_period_tran AS intrateperiod ON intrateperiod.periodunkid = svsaving_interest_rate_tran.periodunkid " & _
                                           ") AS TableSaving " & _
                                                "INNER JOIN svsaving_tran ON TableSaving.savingtranunkid = svsaving_tran.savingtranunkid " & _
                                                "LEFT JOIN ( SELECT   TableCF.savingtranunkid  " & _
                                                                   ", TableCF.cf_balance " & _
                                                                   ", TableCF.cf_amount " & _
                                                            "FROM    ( SELECT    svsaving_balance_tran.savingtranunkid  " & _
                                                                              ", svsaving_balance_tran.cf_balance " & _
                                                                              ", svsaving_balance_tran.cf_amount " & _
                                                                              ", DENSE_RANK() OVER ( PARTITION BY svsaving_balance_tran.savingtranunkid ORDER BY svsaving_balance_tran.savingbalancetranunkid DESC ) AS ROWNO " & _
                                                            "FROM    svsaving_balance_tran " & _
                                                            "JOIN svsaving_tran ON svsaving_tran.savingtranunkid = svsaving_balance_tran.savingtranunkid " & _
                                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = svsaving_balance_tran.employeeunkid " & _
                                                            "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
                                                            "WHERE   svsaving_balance_tran.isvoid = 0 " & _
                                                            "AND svsaving_tran.isvoid = 0 " & _
                                                            "AND svsavingscheme_master.isvoid = 0 " & _
                                                                    ") AS TableCF " & _
                                                            "WHERE   TableCF.ROWNO = 1 " & _
                                                          ") AS TableBF ON TableBF.savingtranunkid = TableSaving.savingtranunkid " & _
                                                      "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = TableSaving.payperiodunkid " & _
                                         "WHERE    TableSaving.ROWNO = 1 " & _
                                         "GROUP BY svsaving_tran.savingschemeunkid " & _
                                                ", TableSaving.payperiodunkid " & _
                                                ", cfcommon_period_tran.start_date " & _
                                                ", TableSaving.savingtranunkid " & _
                                                ", TableSaving.employeeunkid " & _
                                                ", payrollprocesstranunkid " & _
                                                ", paymenttranunkid " & _
                                                ", savingcontributiontranunkid " & _
                                                ", savinginterestratetranunkid " & _
                                                ", ISNULL(TableBF.cf_balance, svsaving_tran.bf_balance) " & _
                                                ", ISNULL(TableBF.cf_amount, svsaving_tran.bf_amount) " & _
                                                ", TableSaving.effectivedate " & _
                                                ", TableSaving.interest_rate " & _
                                        ") "

            'Sohail (23 Jun 2016) -- Start
            'Issue - 61.1 - [Issue in AND b.ROWNO < a.ROWNO line] Taking too much time at CCBRT (more than 2-3 hours) AS they don't have any other saving transactions (withdrawal, deposit, repayment) other than saving deduction.
            '    ", cte " & _
            '    "AS ( SELECT   a.savingschemeunkid " & _
            '                ", a.periodunkid " & _
            '                ", a.savingtranunkid " & _
            '                ", a.employeeunkid " & _
            '                ", a.payrollprocesstranunkid " & _
            '                ", a.paymenttranunkid " & _
            '                ", a.savingcontributiontranunkid " & _
            '                ", a.savinginterestratetranunkid " & _
            '                ", a.bf_balance " & _
            '                ", a.bf_amount + SUM(ISNULL(b.amount, 0)) - SUM(ISNULL(b.Withdrawal, 0)) - SUM(ISNULL(b.Repayment, 0)) + SUM(ISNULL(b.Deposit, 0)) AS bf_amount " & _
            '                ", a.amount " & _
            '                ", ISNULL(a.Withdrawal, 0) AS Withdrawal " & _
            '                ", ISNULL(a.Repayment, 0) AS Repayment " & _
            '                ", ISNULL(a.Deposit, 0) AS Deposit " & _
            '                ", (a.bf_amount + SUM(ISNULL(b.amount, 0)) - SUM(ISNULL(b.Withdrawal, 0)) - SUM(ISNULL(b.Repayment, 0)) + SUM(ISNULL(b.Deposit, 0))) + a.amount - a.Withdrawal - a.Repayment + a.Deposit AS cf_amount " & _
            '                ", a.effectivedate " & _
            '                ", a.interest_rate " & _
            '                ", ROW_NUMBER() OVER ( PARTITION BY a.savingtranunkid ORDER BY a.effectivedate ) AS ROWNO " & _
            '         "FROM     ce AS a " & _
            '                  "LEFT JOIN ce AS b ON a.employeeunkid = b.employeeunkid " & _
            '                                       "AND a.savingtranunkid = b.savingtranunkid " & _
            '                                       "AND b.ROWNO < a.ROWNO " & _
            '         "GROUP BY a.savingschemeunkid " & _
            '                ", a.periodunkid " & _
            '                ", a.savingtranunkid " & _
            '                ", a.employeeunkid " & _
            '                ", a.payrollprocesstranunkid " & _
            '                ", a.paymenttranunkid " & _
            '                ", a.savingcontributiontranunkid " & _
            '                ", a.savinginterestratetranunkid " & _
            '                ", a.bf_balance " & _
            '                ", a.bf_amount " & _
            '                ", a.amount " & _
            '                ", a.Withdrawal " & _
            '                ", a.Repayment " & _
            '                ", a.Deposit " & _
            '                ", a.effectivedate " & _
            '                ", a.interest_rate " & _
            '       ") " & _
            '", inttbl " & _
            '    "AS ( SELECT   a.* " & _
            '                ", ISNULL(DATEDIFF(DAY, ISNULL(b.effectivedate, start_date - 1), a.effectivedate), 0) AS days " & _
            '                ", ISNULL(( a.bf_amount * DATEDIFF(DAY, ISNULL(b.effectivedate, start_date - 1), a.effectivedate) * a.interest_rate ) / 36500, 0) AS interest_amount " & _
            '         "FROM     cte AS a " & _
            '                  "LEFT JOIN cte AS b ON a.savingtranunkid = b.savingtranunkid " & _
            '                                        "AND a.ROWNO = b.ROWNO + 1 " & _
            '                  "LEFT JOIN cfcommon_period_tran ON a.periodunkid = cfcommon_period_tran.periodunkid " & _
            '       ") " 
            strQ &= "INSERT  INTO #t " & _
                           "( savingschemeunkid  " & _
                           ", periodunkid " & _
                           ", savingtranunkid " & _
                           ", employeeunkid " & _
                           ", bf_balance " & _
                           ", bf_amount " & _
                           ", amount " & _
                           ", Withdrawal " & _
                           ", Repayment " & _
                           ", Deposit " & _
                           ", effective_date " & _
                           ", interest_rate " & _
                           ", days " & _
                           ", cf_amount " & _
                           ", interest_amount " & _
                           ", cf_balance " & _
                           ", startdate " & _
                           ", payrollprocesstranunkid " & _
                           ", paymenttranunkid " & _
                           ", savingcontributiontranunkid " & _
                           ", savinginterestratetranunkid " & _
                                        ") " & _
                   "SELECT  savingschemeunkid  " & _
                         ", periodunkid " & _
                         ", savingtranunkid " & _
                         ", employeeunkid " & _
                         ", bf_balance " & _
                         ", bf_amount " & _
                         ", amount " & _
                         ", Withdrawal " & _
                         ", Repayment " & _
                         ", Deposit " & _
                         ", effective_date " & _
                         ", interest_rate " & _
                         ", 0 " & _
                         ", 0 " & _
                         ", 0 " & _
                         ", 0 " & _
                         ", startdate " & _
                         ", payrollprocesstranunkid " & _
                           ", paymenttranunkid " & _
                           ", savingcontributiontranunkid " & _
                           ", savinginterestratetranunkid " & _
                   "FROM    ce "
            'Sohail (05 Mar 2019) -- KBC Issue - 74.1 - Payroll process tranunkids were not coming in saving balance tran.

            strQ &= "ORDER BY savingtranunkid, effective_date "

            strQ &= "DECLARE @savingschemeunkid INT " & _
                    "DECLARE @periodunkid INT " & _
                    "DECLARE @savingtranunkid INT " & _
                    "DECLARE @employeeunkid INT " & _
                    "DECLARE @bf_balance DECIMAL(36, 6) " & _
                    "DECLARE @bf_amount DECIMAL(36, 6) " & _
                    "DECLARE @amount DECIMAL(36, 6) " & _
                    "DECLARE @Withdrawal DECIMAL(36, 6) " & _
                    "DECLARE @Repayment DECIMAL(36, 6) " & _
                    "DECLARE @Deposit DECIMAL(36, 6) " & _
                    "DECLARE @effectivedate DATETIME " & _
                    "DECLARE @pstartdate DATETIME " & _
                    "DECLARE @interest_rate DECIMAL(36, 6) " & _
                    "DECLARE @ROWNO INT " & _
                    " " & _
                    "DECLARE @prevsavingtranunkid INT " & _
                    "DECLARE @preveffdate DATETIME " & _
                    "DECLARE @cf_amount DECIMAL(36, 6) " & _
                    "DECLARE @cf_balance DECIMAL(36, 6) " & _
                    "DECLARE @lastcf_amount DECIMAL(36, 6) " & _
                    "DECLARE @lastcf_balance DECIMAL(36, 6) " & _
                    "SET @prevsavingtranunkid = 0 " & _
                    "SET @preveffdate = '19000101' " & _
                    "SET @cf_amount = 0 " & _
                    "SET @cf_balance = 0 " & _
                    "SET @lastcf_amount = 0 " & _
                    "SET @lastcf_balance = 0 " & _
                    " " & _
                    "DECLARE c1 CURSOR " & _
                    "FOR " & _
                        "SELECT  savingschemeunkid  " & _
                              ", periodunkid " & _
                              ", savingtranunkid " & _
                              ", employeeunkid " & _
                              ", bf_balance " & _
                              ", bf_amount " & _
                              ", amount " & _
                              ", Withdrawal " & _
                              ", Repayment " & _
                              ", Deposit " & _
                              ", effective_date " & _
                              ", interest_rate " & _
                              ", ROWNO " & _
                              ", startdate " & _
                        "FROM    #t " & _
                        "ORDER BY savingtranunkid  " & _
                              ", effective_date ASC " & _
                    "OPEN c1 " & _
                    "FETCH NEXT FROM c1 INTO @savingschemeunkid, @periodunkid, @savingtranunkid " & _
                      ", @employeeunkid, @bf_balance, @bf_amount, @amount, @Withdrawal, @Repayment " & _
                      ", @Deposit, @effectivedate, @interest_rate, @ROWNO, @pstartdate " & _
                    " " & _
                    "WHILE @@FETCH_STATUS = 0 " & _
                        "BEGIN " & _
                        " " & _
                            "IF @prevsavingtranunkid <> @savingtranunkid " & _
                                "BEGIN " & _
                                    "SET @cf_amount = 0 " & _
                                    "SET @cf_balance = 0 " & _
                                    "SET @lastcf_amount = @bf_amount " & _
                                    "SET @lastcf_balance = @bf_balance " & _
                                    "SET @preveffdate = @effectivedate " & _
                                "END " & _
                           " " & _
                            "SET @bf_amount = @lastcf_amount " & _
                            "SET @bf_balance = @lastcf_balance " & _
                            " " & _
                            "SET @cf_amount = @bf_amount + @amount + @Deposit - @Withdrawal - @Repayment " & _
                            "SET @cf_balance = @bf_amount + @amount + @Deposit - @Withdrawal - @Repayment + ISNULL(( @bf_amount * DATEDIFF(DAY , ISNULL(@preveffdate , @pstartdate - 1) , @effectivedate) * @interest_rate ) / 36500, 0) " & _
                           " " & _
                            "UPDATE  #t " & _
                            "SET     bf_balance = @bf_balance  " & _
                                  ", bf_amount = @bf_amount " & _
                                  ", cf_amount = @cf_amount " & _
                                  ", cf_balance = @cf_balance " & _
                                  ", days = ISNULL(DATEDIFF(DAY , ISNULL(@preveffdate, @pstartdate - 1) , @effectivedate), 0) " & _
                                  ", interest_amount = ISNULL(( @bf_amount * ISNULL(DATEDIFF(DAY , ISNULL(@preveffdate , @pstartdate - 1) , @effectivedate) , 0) * @interest_rate ) / 36500, 0) " & _
                            "WHERE   #t.ROWNO = @ROWNO " & _
                    " " & _
                            "SET @lastcf_amount = @cf_amount " & _
                            "SET @lastcf_balance = @cf_balance " & _
                    " " & _
                            "SET @prevsavingtranunkid = @savingtranunkid " & _
                            "SET @preveffdate = @effectivedate " & _
                    " " & _
                            "FETCH NEXT FROM c1 INTO @savingschemeunkid, @periodunkid " & _
                              ", @savingtranunkid, @employeeunkid, @bf_balance, @bf_amount, @amount " & _
                              ", @Withdrawal, @Repayment, @Deposit, @effectivedate, @interest_rate " & _
                              ", @ROWNO, @pstartdate " & _
                        "END " & _
                    "CLOSE c1 " & _
                    "DEALLOCATE c1 "
            'Sohail (23 Jun 2016) -- End

            'Sohail (23 Jun 2016) -- Start
            'Issue - 61.1 - [Issue in AND b.ROWNO < a.ROWNO line] Taking too much time at CCBRT (more than 2-3 hours) AS they don't have any other saving transactions (withdrawal, deposit, repayment) other than saving deduction.
            '"INSERT  INTO svsaving_balance_tran " & _
            '            "( savingschemeunkid " & _
            '            ", periodunkid " & _
            '            ", effective_date " & _
            '            ", days " & _
            '            ", savingtranunkid " & _
            '            ", employeeunkid " & _
            '            ", payrollprocesstranunkid " & _
            '            ", paymenttranunkid " & _
            '            ", savingcontributiontranunkid " & _
            '            ", savinginterestratetranunkid " & _
            '            ", bf_balance " & _
            '            ", bf_amount " & _
            '            ", contribution_amount " & _
            '            ", withdrawal_amount " & _
            '            ", repayment_amount " & _
            '            ", deposit_amount " & _
            '            ", interest_rate " & _
            '            ", interest_amount " & _
            '            ", cf_amount " & _
            '            ", cf_balance " & _
            '            ", isonohold " & _
            '            ", userunkid " & _
            '            ", isvoid " & _
            '            ", voiduserunkid " & _
            '            ", voiddatetime " & _
            '            ", voidreason " & _
            '            ") " & _
            '"SELECT        a.savingschemeunkid " & _
            '            ", a.periodunkid " & _
            '            ", a.effectivedate " & _
            '            ", a.days " & _
            '            ", a.savingtranunkid " & _
            '            ", a.employeeunkid " & _
            '            ", a.payrollprocesstranunkid " & _
            '            ", a.paymenttranunkid " & _
            '            ", a.savingcontributiontranunkid " & _
            '            ", a.savinginterestratetranunkid " & _
            '            ", a.bf_balance + SUM(ISNULL(b.amount, 0)) - SUM(ISNULL(b.Withdrawal, 0)) - SUM(ISNULL(b.Repayment, 0)) + SUM(ISNULL(b.Deposit, 0)) + SUM(ISNULL(b.interest_amount, 0)) AS bf_balance " & _
            '            ", a.bf_amount " & _
            '            ", a.amount " & _
            '            ", a.Withdrawal " & _
            '            ", a.Repayment " & _
            '            ", a.Deposit " & _
            '            ", a.interest_rate " & _
            '            ", a.interest_amount " & _
            '            ", a.cf_amount " & _
            '            ", a.bf_balance + a.amount - a.Withdrawal - a.Repayment + a.Deposit + a.interest_amount - SUM(ISNULL(b.Withdrawal, 0)) - SUM(ISNULL(b.Repayment, 0)) + SUM(ISNULL(b.Deposit, 0)) + SUM(ISNULL(b.amount, 0)) + SUM(ISNULL(b.interest_amount, 0)) AS cf_balance " & _
            '            ", 0 AS isonohold " & _
            '            ", @userunkid AS userunkid " & _
            '            ", 0 AS isvoid " & _
            '            ", -1 AS voiduserunkid " & _
            '            ", NULL AS voiddatetime " & _
            '            ", '' AS voidreason " & _
            '"FROM    inttbl AS a " & _
            '"LEFT JOIN inttbl AS b ON a.savingtranunkid = b.savingtranunkid " & _
            '                                    "AND a.employeeunkid = b.employeeunkid " & _
            '                                    "AND b.ROWNO < a.ROWNO " & _
            '"GROUP BY   a.savingschemeunkid " & _
            '            ", a.effectivedate " & _
            '            ", a.periodunkid " & _
            '            ", a.savingtranunkid " & _
            '            ", a.employeeunkid " & _
            '            ", a.days " & _
            '            ", a.payrollprocesstranunkid " & _
            '            ", a.paymenttranunkid " & _
            '            ", a.savingcontributiontranunkid " & _
            '            ", a.savinginterestratetranunkid " & _
            '            ", a.bf_balance " & _
            '            ", a.bf_amount " & _
            '            ", a.amount " & _
            '            ", a.Withdrawal " & _
            '            ", a.Repayment " & _
            '            ", a.Deposit " & _
            '            ", a.interest_rate " & _
            '            ", a.interest_amount " & _
            '            ", a.cf_amount " & _
            '"ORDER BY      a.savingschemeunkid, A.savingtranunkid, a.effectivedate, a.payrollprocesstranunkid, a.paymenttranunkid, a.savingcontributiontranunkid, a.savinginterestratetranunkid "
            strQ &= "               INSERT  INTO svsaving_balance_tran " & _
                                                "( savingschemeunkid " & _
                                                ", periodunkid " & _
                                                ", effective_date " & _
                                                ", days " & _
                                                ", savingtranunkid " & _
                                                ", employeeunkid " & _
                                                ", payrollprocesstranunkid " & _
                                                ", paymenttranunkid " & _
                                                ", savingcontributiontranunkid " & _
                                                ", savinginterestratetranunkid " & _
                                                ", bf_balance " & _
                                                ", bf_amount " & _
                                                ", contribution_amount " & _
                                                ", withdrawal_amount " & _
                                                ", repayment_amount " & _
                                                ", deposit_amount " & _
                                                ", interest_rate " & _
                                                ", interest_amount " & _
                                                ", cf_amount " & _
                                                ", cf_balance " & _
                                                ", isonohold " & _
                                                ", userunkid " & _
                                                ", isvoid " & _
                                                ", voiduserunkid " & _
                                                ", voiddatetime " & _
                                                ", voidreason " & _
                                                ") " & _
                                    "SELECT        a.savingschemeunkid " & _
                                                ", a.periodunkid " & _
                                                ", a.effective_date " & _
                                                ", a.days " & _
                                                ", a.savingtranunkid " & _
                                                ", a.employeeunkid " & _
                                                ", a.payrollprocesstranunkid " & _
                                                ", a.paymenttranunkid " & _
                                                ", a.savingcontributiontranunkid " & _
                                                ", a.savinginterestratetranunkid " & _
                                                ", ISNULL(A.bf_balance, 0)  AS bf_balance " & _
                                                ", ISNULL(A.bf_amount, 0) AS bf_amount " & _
                                                ", A.amount " & _
                                                ", A.Withdrawal " & _
                                                ", a.Repayment " & _
                                                ", a.Deposit " & _
                                                ", a.interest_rate " & _
                                                ", a.interest_amount " & _
                                                ", ISNULL(a.cf_amount, 0) AS cf_amount " & _
                                                ", ISNULL(A.cf_balance, 0) AS cf_balance " & _
                                                ", 0 AS isonohold " & _
                                                ", @userunkid AS userunkid " & _
                                                ", 0 AS isvoid " & _
                                                ", -1 AS voiduserunkid " & _
                                                ", NULL AS voiddatetime " & _
                                                ", '' AS voidreason " & _
                                    "FROM    #t AS A " & _
                                    "ORDER BY      a.savingschemeunkid, A.savingtranunkid, a.effective_date, a.payrollprocesstranunkid, a.paymenttranunkid, a.savingcontributiontranunkid, a.savinginterestratetranunkid "

            strQ &= " DROP TABLE #t "
            'Sohail (23 Jun 2016) -- End
            'Sohail (17 May 2015) - [ORDER BY a.payrollprocesstranunkid, a.paymenttranunkid, a.savingcontributiontranunkid, a.savinginterestratetranunkid]
            'Sohail (21 Nov 2015) - [Deposit] - [Provide Deposit feaure in Employee Saving.]
            'Sohail (10 Jun 2015) - Issue : C/f amount was not coming correct if saving payment is done on last date of period. [", a.bf_amount - SUM(ISNULL(b.Withdrawal, 0)) - SUM(ISNULL(b.Repayment, 0)) + a.amount - a.Withdrawal - a.Repayment AS cf_amount "]
            'Sohail (10 Jun 2015) -- Start
            'Issue : C/f amount was not coming correct if saving payment is done on last date of period.
            '"LEFT JOIN ( SELECT  svsaving_balance_tran.savingtranunkid " & _
            '                 ", MAX(cf_balance) AS cf_balance " & _
            '                 ", MAX(cf_amount) AS cf_amount " & _
            '            "FROM    svsaving_balance_tran " & _
            '            "JOIN svsaving_tran ON svsaving_tran.savingtranunkid = svsaving_balance_tran.savingtranunkid " & _
            '            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = svsaving_balance_tran.employeeunkid " & _
            '            "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
            '            "WHERE   ISNULL(svsaving_balance_tran.isvoid, 0) = 0 " & _
            '            "AND ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
            '            "AND svsavingscheme_master.isvoid = 0 " & _
            '            "GROUP BY svsaving_balance_tran.savingtranunkid " & _
            '          ") AS TableBF ON TableBF.savingtranunkid = TableSaving.savingtranunkid " & _
            'Sohail (10 Jun 2015) -- End


            objDataOperation.AddParameter("@ClosingPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            'objDataOperation.AddParameter("@db_start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date))
            'objDataOperation.AddParameter("@db_end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(FinancialYear._Object._Database_End_Date))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            objDataOperation.AddParameter("@db_start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDatabaseStartDate))
            objDataOperation.AddParameter("@db_end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDatabaseEndDate))
            'Sohail (21 Aug 2015) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Saving_Balance_Data; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
    End Function
    'Sohail (12 Jan 2015) -- End

    'Sohail (13 Feb 2020) -- Start
    'NMB Enhancement # : Now processed only those OT which are posted in given date range on OT post to payroll screen.
    Private Function UpdateOTRequisition_Posted(ByVal intUserunkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "UPDATE  tnaotrequisition_process_tran " & _
                    "SET     isprocessed = 1  " & _
                          ", userunkid = @userunkid " & _
                    "WHERE   isvoid = 0 " & _
                            "AND periodunkid = @periodunkid " & _
                            "AND isposted = 1 " & _
                            "AND ISNULL(isprocessed, 0) = 0 "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateOTRequisition_Posted; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (13 Feb 2020) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Current loan deduction does not match with process payroll loan deduction. Please do process payroll again for the following employee.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot close this Period now.")
			Language.setMessage(mstrModuleName, 3, "Reason : The Period to be closed is in use on")
			Language.setMessage(mstrModuleName, 4, "Machine.")
			Language.setMessage(mstrModuleName, 5, "Please close the Application on that Machine.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
