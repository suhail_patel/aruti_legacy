﻿'************************************************************************************************************************************
'Class Name : clstsapprover_Tran.vb
'Purpose    :
'Date       :28/10/2016
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clstsapprover_Tran
    Private Const mstrModuleName = "clstsapprover_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintTsapprovertranunkid As Integer = -1
    Private mintTsApproverunkid As Integer = -1
    Private mintUserunkid As Integer = -1
    Private mdtTran As DataTable
    Private mdtEmployeeAsonDate As Date = Nothing
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""

#End Region

#Region " Properties "


    ''' <summary>
    ''' Purpose: Get or Set tsapprovertranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tsapprovertranunkid(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTsapprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintTsapprovertranunkid = value
            GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Tsapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tsapproverunkid() As Integer
        Get
            Return mintTsApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintTsApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DataList
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmployeeAsonDate() As Date
        Get
            Return mdtEmployeeAsonDate
        End Get
        Set(ByVal value As Date)
            mdtEmployeeAsonDate = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property


#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("ApproverTran")
        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("ischeck")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("tsapprovertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("tsapproverunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)


            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeecode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("departmentname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("jobname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDOperation As clsDataOperation = Nothing)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            If objDOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDOperation
            End If
            objDataOperation.ClearParameters()

            StrQ = "SELECT " & _
                   "	 CAST(0 AS BIT) AS ischeck " & _
                   "	,tsapprover_tran.tsapprovertranunkid " & _
                   "	,tsapprover_tran.tsapproverunkid " & _
                   "	,tsapprover_tran.employeeunkid " & _
                   "    ,ISNULL(hremployee_master.employeecode, '' ) AS employeecode " & _
                   "    ,ISNULL(hremployee_master.firstname, '' ) + '  ' + ISNULL(hremployee_master.othername,'') + '  ' + ISNULL(hremployee_master.surname,'') AS name " & _
                   "	,'' AS AUD " & _
                   "	,ISNULL(hrdepartment_master.name,'') AS departmentname " & _
                   "	,ISNULL(hrjob_master.job_name,'')  AS jobname " & _
                   "    , '' AS AUD " & _
                   "    , '' AS GUID " & _
                   " FROM tsapprover_tran " & _
                   "	JOIN hremployee_master ON tsapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "    LEFT JOIN " & _
                    "   ( " & _
                    "        SELECT " & _
                    "             jobunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                    " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "	JOIN hrjob_master ON  hrjob_master.jobunkid = Jobs.jobunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "        SELECT " & _
                    "         departmentunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "	JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                   "	JOIN tsapprover_master ON tsapprover_tran.tsapproverunkid = tsapprover_master.tsapproverunkid AND tsapprover_master.isvoid = 0 " & _
                   "WHERE tsapprover_master.tsapproverunkid = @tsapproverunkid AND tsapprover_tran.isvoid = 0 "

            If mintTsapprovertranunkid > 0 Then
                StrQ &= " AND tsapprover_tran.tsapprovertranunkid = @tsapprovertranunkid"
                objDataOperation.AddParameter("@tsapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapprovertranunkid)
            End If
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsonDate))
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsApproverunkid)
            If mintTsapprovertranunkid > 0 Then

            End If
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dRow)
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  tsapprovertranunkid " & _
                      ", tsapproverunkid " & _
                      ", employeeunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                     "FROM tsapprover_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetApproverTran(ByVal mdtEmployeeAsonDate As Date _
                                             , Optional ByVal intApproverunkid As Integer = 0 _
                                             , Optional ByVal objDOperation As clsDataOperation = Nothing)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try

            strQ = "SELECT " & _
                      "  tsapprover_tran.tsapprovertranunkid " & _
                      ", tsapprover_tran.tsapproverunkid " & _
                      ", '' as approvername" & _
                      ", tsapprover_tran.employeeunkid " & _
                      ", isnull(Emp2.employeecode,'')  employeecode " & _
                      ", isnull(Emp2.firstname,'') + ' ' + isnull(Emp2.surname,'') as name" & _
                      ", Alloc.departmentunkid " & _
                      ", hrdepartment_master.name as departmentname " & _
                      ", Jobs.jobunkid " & _
                      ", hrjob_master.job_name as jobname " & _
                      ", '' as AUD ,'' as GUID " & _
                      " FROM tsapprover_tran " & _
                      " LEFT JOIN tsapprover_master on tsapprover_master.tsapproverunkid = tsapprover_tran.tsapproverunkid " & _
                      " LEFT JOIN hremployee_master Emp2 on tsapprover_tran.employeeunkid = Emp2.employeeunkid " & _
                      " LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                            ") AS Alloc ON Alloc.employeeunkid = Emp2.employeeunkid AND Alloc.rno = 1 " & _
                      " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                      " LEFT JOIN hrsection_master on hrsection_master.sectionunkid = Alloc.sectionunkid " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         jobunkid " & _
                      "        ,jobgroupunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                      " ) AS Jobs ON Jobs.employeeunkid = Emp2.employeeunkid AND Jobs.rno = 1 " & _
                      " LEFT JOIN hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid " & _
                      " WHERE ISNULL(tsapprover_tran.isvoid,0) = 0"

            If intApproverunkid > 0 Then
                strQ &= " AND tsapprover_tran.tsapproverunkid = @tsapproverunkid"
                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            End If

            strQ &= " AND Emp2.isapproved = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")
            mdtTran = dsList.Tables("List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try

    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_tran) </purpose>
    Public Function InsertDelete_TimesheetApproverData(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If mdtTran Is Nothing Then Return True

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "Select count(*) as 'Countemp' From tsapprover_tran " & _
                                       " where tsapproverunkid = @tsapproverunkid AND employeeunkid = @empunkid AND tsapprover_tran.isvoid = 0"


                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsApproverunkid)
                                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                Dim dscount As DataSet = objDataOperation.ExecQuery(strQ, "Count")
                                If Not dscount Is Nothing And CInt(dscount.Tables("Count").Rows(0)("Countemp")) > 0 Then Continue For

                                strQ = "INSERT INTO tsapprover_tran ( " & _
                                            " tsapproverunkid " & _
                                            ", employeeunkid " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason " & _
                                            " ) VALUES (" & _
                                            "  @tsapproverunkid " & _
                                            ", @employeeunkid " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason " & _
                                            "); SELECT @@identity"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsApproverunkid.ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintTsapprovertranunkid = dsList.Tables(0).Rows(0).Item(0)


                                If InsertATTimesheet_ApproverData(objDataOperation, 1, CInt(.Item("employeeunkid"))) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                strQ = " Update tsapprover_tran set " & _
                                            " isvoid = 1,voiddatetime=GetDate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                                            " WHERE tsapproverunkid = @tsapproverunkid AND employeeunkid = @employeeunkid"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsApproverunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 2, "UnAssign"))
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertATTimesheet_ApproverData(objDataOperation, 3, CInt(.Item("employeeunkid"))) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete_TimesheetApproverData; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_tran) </purpose>
    Public Function InsertATTimesheet_ApproverData(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal xEmployeeunkid As Integer) As Boolean
        Dim strQ As String = ""
        Try
            strQ = "INSERT INTO attsapprover_tran ( " & _
                                          " tsapprovertranunkid" & _
                                          ", tsapproverunkid " & _
                                          ", employeeunkid " & _
                                          ", audittype " & _
                                          ", audituserunkid " & _
                                          ", auditdatetime " & _
                                          ", ip " & _
                                          ", machine_name " & _
                                          ", form_name " & _
                                          ", module_name1 " & _
                                          ", module_name2 " & _
                                          ", module_name3 " & _
                                          ", module_name4 " & _
                                          ", module_name5 " & _
                                          ", isweb " & _
                                      ") VALUES (" & _
                                          "  @tsapprovertranunkid " & _
                                          ", @tsapproverunkid " & _
                                          ", @employeeunkid " & _
                                          ", @audittype " & _
                                          ", @audituserunkid " & _
                                          ", GetDate() " & _
                                          ", @ip " & _
                                          ", @machine_name " & _
                                          ", @form_name " & _
                                          ", @module_name1 " & _
                                          ", @module_name2 " & _
                                          ", @module_name3 " & _
                                          ", @module_name4 " & _
                                          ", @module_name5 " & _
                                          ", @isweb " & _
                                        "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapprovertranunkid.ToString)
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsApproverunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 5, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            objDataOperation.ExecNonQuery(strQ)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATTimesheet_ApproverData; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function Approver_Migration(ByVal dtTable As DataTable, ByVal xNewApproverId As Integer, ByVal xNewEmpapproverId As Integer, ByVal xEmployeeAsonDate As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [23-SEP-2019] -- Add {xDataOpr}    
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""
        Try



            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'Dim objDataOperation As New clsDataOperation
            'objDataOperation.BindTransaction()
            Dim objDataOperation As clsDataOperation
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            End If
            'Gajanan [23-SEP-2019] -- End


            If dtTable Is Nothing Then Return True

            Dim mintOldApproverID As Integer = 0

            For i = 0 To dtTable.Rows.Count - 1


                strQ = " Update tsapprover_tran set " & _
                          " isvoid = 1,voiddatetime=GetDate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                          " WHERE tsapproverunkid = @tsapproverunkid AND employeeunkid = @employeeunkid"


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(i)("tsapproverunkid")))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 4, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mdtEmployeeAsonDate = xEmployeeAsonDate
                mintTsApproverunkid = CInt(dtTable.Rows(i)("tsapproverunkid"))
                mintOldApproverID = mintTsApproverunkid
                _Tsapprovertranunkid(objDataOperation) = CInt(dtTable.Rows(i)("tsapprovertranunkid"))

                If InsertATTimesheet_ApproverData(objDataOperation, 3, CInt(dtTable.Rows(i)("employeeunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                '===START-- GETTING ALREADY EXIST EMPLOYEE FROM NEW TIMESHEET APPROVER AND ADDED TO MIGRATED EMPLOYEE VARIABLE.IT IS USED WHEN ALL OLD TIMESHEET APPROVER'S EMPLOYEE TO NEW TIMESHEET APPROVER.
                strQ = " SELECT ISNULL(employeeunkid,0) employeeunkid FROM tsapprover_tran WHERE tsapproverunkid = @tsapproverunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                Dim dtEmpCount As DataSet = objDataOperation.ExecQuery(strQ, "List")
                If dtEmpCount.Tables(0).Rows.Count > 0 Then
                    If CInt(dtEmpCount.Tables(0).Rows(0)("employeeunkid")) > 0 Then
                        mstrEmployeeID &= dtTable.Rows(i)("employeeunkid").ToString() & ","
                        Continue For
                    End If
                End If
                '===END-- GETTING ALREADY EXIST EMPLOYEE FROM NEW TIMESHEET APPROVER AND ADDED TO MIGRATED EMPLOYEE VARIABLE.IT IS USED WHEN ALL OLD TIMESHEET APPROVER'S EMPLOYEE TO NEW TIMESHEET APPROVER.


                strQ = "INSERT INTO tsapprover_tran ( " & _
                             " tsapproverunkid " & _
                             ", employeeunkid " & _
                             ", userunkid " & _
                             ", isvoid " & _
                             ", voiduserunkid " & _
                         ") VALUES (" & _
                             "  @tsapproverunkid " & _
                             ", @employeeunkid " & _
                             ", @userunkid " & _
                             ", @isvoid " & _
                             ", @voiduserunkid " & _
                             "); SELECT @@identity"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mdtEmployeeAsonDate = xEmployeeAsonDate
                mintTsApproverunkid = xNewApproverId
                _Tsapprovertranunkid(objDataOperation) = dsList.Tables(0).Rows(0).Item(0)

                If InsertATTimesheet_ApproverData(objDataOperation, 1, CInt(dtTable.Rows(i)("employeeunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                Dim objEmpTimesheet As New clsBudgetEmp_timesheet
                Dim mstrTimesheetId As String = objEmpTimesheet.GetApproverPendingTimesheet(mintOldApproverID, dtTable.Rows(i)("employeeunkid").ToString, objDataOperation)

                If mstrTimesheetId.Trim.Length > 0 Then

                    Dim objtsApprovalTran As New clstsemptimesheet_approval

                    strQ = " Update tsemptimesheet_approval set tsapproverunkid  = @newtsapproverunkid,approveremployeeunkid = @newapproveremployeeunkid  " & _
                              " WHERE emptimesheetunkid in (" & mstrTimesheetId & ") AND tsapproverunkid = @tsapproverunkid AND tsemptimesheet_approval.isvoid = 0 AND iscancel = 0 "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newapproveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewEmpapproverId)
                    objDataOperation.AddParameter("@newtsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverId)
                    objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverID)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "Select ISNULL(timesheetapprovalunkid,0) timesheetapprovalunkid FROM tsemptimesheet_approval  " & _
                               " WHERE tsapproverunkid = @tsapproverunkid AND approveremployeeunkid = @approveremployeeunkid " & _
                               " AND emptimesheetunkid  in (" & mstrTimesheetId & ") AND isvoid = 0 AND iscancel = 0 "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewEmpapproverId)
                    objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverId)
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If dsList.Tables(0).Rows.Count > 0 Then

                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                            objtsApprovalTran._Timesheetapprovalunkid(objDataOperation) = CInt(dsList.Tables(0).Rows(k)("timesheetapprovalunkid"))

                            If objtsApprovalTran.InsertATEmpTimesheet_Approval(objDataOperation, 2) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        Next

                    End If   '  If dsList.Tables(0).Rows.Count > 0 Then

                    objtsApprovalTran = Nothing

                End If  ' If mstrTimesheetId.Trim.Length > 0 Then

                mstrEmployeeID &= dtTable.Rows(i)("employeeunkid").ToString() & ","

            Next


            If mstrEmployeeID.Trim.Length > 0 Then
                mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
            End If

            Dim objMigration As New clsMigration
            objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
            objMigration._Migrationfromunkid = mintOldApproverID
            objMigration._Migrationtounkid = xNewApproverId
            objMigration._Usertypeid = enUserType.Timesheet_Approver
            objMigration._Userunkid = mintUserunkid
            objMigration._Migratedemployees = mstrEmployeeID

            If objMigration.Insert(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT ISNULL(employeeunkid,0) employeeunkid FROM tsapprover_tran WHERE tsapproverunkid = @tsapproverunkid AND isvoid = 0  "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverID)
            Dim dtCount As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dtCount IsNot Nothing AndAlso dtCount.Tables(0).Rows.Count <= 0 Then

                Dim objtsApprover As New clstsapprover_master

                strQ = " Update tsapprover_master set " & _
                          " isvoid = 1,voiddatetime=GetDate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                          " WHERE tsapproverunkid = @tsapproverunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverID)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 4, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objtsApprover._Tsapproverunkid = mintOldApproverID
                If objtsApprover.InsertATApprover_Master(objDataOperation, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "SELECT ISNULL(mappingunkid,0) mappingunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.Timesheet_Approver
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverID)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(i)("mappingunkid")), False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                strQ = "Delete from hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.Timesheet_Approver
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverID)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [23-SEP-2019] -- End

            Return True
        Catch ex As Exception

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [23-SEP-2019] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Approver_Migration, Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [23-SEP-2019] -- End
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function
    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function GetApproverTranIdFromEmployeeAndApprover(ByVal ApproverId As Integer, ByVal EmployeeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT ISNULL(tsapprovertranunkid,0) as tsapprovertranunkid FROM tsapprover_tran WHERE tsapproverunkid = @tsapproverunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ApproverId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, EmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0)("tsapprovertranunkid").ToString())
            End If

            Return 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTranIdFromEmployeeAndApprover; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [23-SEP-2019] -- End


    'Pinkal (25-Nov-2020) -- Start
    'Enhancement IHI - Working on Import Timesheet Approver.
#Region " Import Employee With Approver"

    Public Function ImportInsertEmployee(ByVal mstrEmployeeId As String, ByVal xUserId As Integer, ByRef intApprTranId As Integer) As Boolean
        Dim strQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            mstrMessage = ""
            Dim objDataOperation As New clsDataOperation

            If mstrEmployeeId.Trim.Length <= 0 Then Return True

            strQ = "SELECT tsapprovertranunkid AS Id FROM tsapprover_tran WHERE tsapproverunkid = '" & mintTsApproverunkid & "' AND employeeunkid = '" & CInt(mstrEmployeeId) & "' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                mstrMessage = objDataOperation.ErrorMessage
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intApprTranId = dsList.Tables(0).Rows(0).Item("Id")
                Return False
            End If

            strQ = "INSERT INTO tsapprover_tran ( " & _
                              " tsapproverunkid " & _
                              ", employeeunkid " & _
                              ", userunkid " & _
                              ", isvoid " & _
                              ", voiduserunkid " & _
                          ") VALUES (" & _
                              "  @tsapproverunkid " & _
                              ", @employeeunkid " & _
                              ", @userunkid " & _
                              ", @isvoid " & _
                              ", @voiduserunkid " & _
                              "); SELECT @@identity"


            Dim arEmployee() As String = mstrEmployeeId.Trim.Split(",")

            For i As Integer = 0 To arEmployee.Length - 1

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsApproverunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arEmployee(i).ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                dsList = objDataOperation.ExecQuery(strQ, "List")


                If objDataOperation.ErrorMessage <> "" Then
                    mstrMessage = objDataOperation.ErrorMessage
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintTsapprovertranunkid = dsList.Tables(0).Rows(0).Item(0)

                If InsertATTimesheet_ApproverData(objDataOperation, 1, CInt(arEmployee(i))) = False Then
                    mstrMessage = objDataOperation.ErrorMessage
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ImportInsertEmloyee; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

#End Region
    'Pinkal (25-Nov-2020) -- End


#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Migration")
            Language.setMessage(mstrModuleName, 2, "UnAssign")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class