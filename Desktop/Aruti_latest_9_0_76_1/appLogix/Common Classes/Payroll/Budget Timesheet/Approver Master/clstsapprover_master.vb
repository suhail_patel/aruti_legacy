﻿'************************************************************************************************************************************
'Class Name : clstsapprover_master.vb
'Purpose    :
'Date       :28/10/2016
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clstsapprover_master
    Private Shared ReadOnly mstrModuleName As String = "clstsapprover_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintTsapproverunkid As Integer
    Private mintTslevelunkid As Integer
    Private mintEmployeeapproverunkid As Integer
    Private mstrEmployeeName As String = String.Empty
    Private mblnIsswap As Boolean
    Private mblnIsexternalapprover As Boolean
    Private mblnIsactive As Boolean = True
    Private mintUserunkid As Integer
    Private mintMapUserId As Integer = 0
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mdtEmployeeTran As DataTable = Nothing
    Private objApproverTran As New clstsapprover_Tran

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tsapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tsapproverunkid(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTsapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintTsapproverunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tslevelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tslevelunkid() As Integer
        Get
            Return mintTslevelunkid
        End Get
        Set(ByVal value As Integer)
            mintTslevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeapproverunkid() As Integer
        Get
            Return mintEmployeeapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Employee Name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmployeeName() As String
        Get
            Return mstrEmployeeName
        End Get
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isswap
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isswap() As Boolean
        Get
            Return mblnIsswap
        End Get
        Set(ByVal value As Boolean)
            mblnIsswap = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isexternalapprover
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isexternalapprover() As Boolean
        Get
            Return mblnIsexternalapprover
        End Get
        Set(ByVal value As Boolean)
            mblnIsexternalapprover = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set MapUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MapUserId() As Integer
        Get
            Return mintMapUserId
        End Get
        Set(ByVal value As Integer)
            mintMapUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EmployeeTran
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmployeeTran() As DataTable
        Get
            Return mdtEmployeeTran
        End Get
        Set(ByVal value As DataTable)
            mdtEmployeeTran = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim StrFinalQurey As String = String.Empty
        Dim StrQCondition As String = String.Empty
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  tsapprover_master.tsapproverunkid " & _
                      ", tsapprover_master.tslevelunkid " & _
                      ", tsapprover_master.employeeapproverunkid " & _
                      " ,#APPR_NAME#  AS employee " & _
                      ", tsapprover_master.isswap " & _
                      ", tsapprover_master.isexternalapprover " & _
                      ", tsapprover_master.isactive " & _
                      ", tsapprover_master.userunkid " & _
                      ", tsapprover_master.isvoid " & _
                      ", tsapprover_master.voiddatetime " & _
                      ", tsapprover_master.voiduserunkid " & _
                      ", tsapprover_master.voidreason " & _
                      " ,ISNULL(hrapprover_usermapping.userunkid,0) AS mapuserid " & _
                      " FROM tsapprover_master " & _
                      " LEFT JOIN hrapprover_usermapping ON tsapprover_master.tsapproverunkid = hrapprover_usermapping.approverunkid AND usertypeid = '" & enUserType.Timesheet_Approver & "' " & _
                      " #EMPL_JOIN# "

            StrFinalQurey = strQ

            StrQCondition &= "WHERE tsapprover_master.tsapproverunkid = @tsapproverunkid  AND tsapprover_master.isexternalapprover = #isExternal# "

            strQ = strQ.Replace("#APPR_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master on hremployee_master.employeeunkid = tsapprover_master.employeeapproverunkid ")
            strQ &= StrQCondition.Replace("#isExternal# ", "0")

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapproverunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As New DataSet
            dsCompany = GetExternalApproverList(objDOperation, "List", False)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tsapprover_master.employeeapproverunkid  ")
                Else
                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tsapprover_master.employeeapproverunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")

                End If

                strQ &= StrQCondition.Replace("#isExternal# ", "1")

                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapproverunkid.ToString)

                dstmp = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTsapproverunkid = CInt(dtRow.Item("tsapproverunkid"))
                mintTslevelunkid = CInt(dtRow.Item("tslevelunkid"))
                mintEmployeeapproverunkid = CInt(dtRow.Item("employeeapproverunkid"))
                mstrEmployeeName = dtRow.Item("employee").ToString()
                mblnIsswap = CBool(dtRow.Item("isswap"))
                mblnIsexternalapprover = CBool(dtRow.Item("isexternalapprover"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mdtVoiddatetime = IIf(IsDBNull(dtRow.Item("voiddatetime")), Nothing, dtRow.Item("voiddatetime"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintMapUserId = CInt(dtRow.Item("mapuserid"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                      , ByVal strEmployeeAsOnDate As String _
                                      , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                      , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                      , Optional ByVal intEmpId As Integer = -1, Optional ByVal objDOperation As clsDataOperation = Nothing _
                                      , Optional ByVal mstrFilter As String = "" _
                                      , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                      , Optional ByVal intApproverTranUnkid As Integer = 0) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If
        objDataOperation.ClearParameters()

        Try

            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)


            strQ = "SELECT " & _
                      "  tsapprover_master.tsapproverunkid " & _
                      ", tsapprover_master.tslevelunkid " & _
                      ", tsapproverlevel_master.levelname " & _
                      ", tsapproverlevel_master.priority " & _
                      ", tsapprover_master.employeeapproverunkid " & _
                      ", #APPR_NAME# as name" & _
                      ", #DEPT_NAME# as departmentname " & _
                      ", #SEC_NAME# as sectionname " & _
                      ", #JOB_NAME# As  jobname " & _
                      ", tsapprover_master.isswap " & _
                      ", tsapprover_master.isexternalapprover  " & _
                      ", CASE WHEN tsapprover_master.isexternalapprover = 1 THEN @YES ELSE @NO END AS ExAppr " & _
                      ", tsapprover_master.userunkid " & _
                      ", tsapprover_master.isvoid " & _
                      ", tsapprover_master.voiddatetime " & _
                      ", tsapprover_master.voiduserunkid " & _
                      ",tsapprover_master. voidreason " & _
                      ",ISNULL(Usr.username,'') AS usermapped " & _
                      " FROM tsapprover_master " & _
                      " LEFT JOIN tsapproverlevel_master on tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid " & _
                      "	LEFT JOIN hrapprover_usermapping ON tsapprover_master.tsapproverunkid = hrapprover_usermapping.approverunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Timesheet_Approver & " " & _
                      "	LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON hrapprover_usermapping.userunkid = Usr.userunkid " & _
                      " #EMPL_JOIN# " & _
                      " #DATA_JOIN# "

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            StrQCondition &= " WHERE 1 = 1 AND tsapprover_master.isexternalapprover = #ExAppr# AND tsapprover_master.isswap = 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry & " "
                End If
            End If

            If blnOnlyActive Then
                StrQCondition &= " AND tsapprover_master.isvoid = 0 AND tsapprover_master.isactive = 1 "
            Else
                StrQCondition &= " AND tsapprover_master.isvoid = 0  AND tsapprover_master.isactive = 0 "
            End If


            If intApproverTranUnkid > 0 Then
                StrQCondition &= " AND tsapprover_master.tsapproverunkid = '" & intApproverTranUnkid & "' "
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= mstrFilter
            End If

            Dim StrDataJoin As String = " LEFT JOIN " & _
                                         " ( " & _
                                         "    SELECT " & _
                                         "        departmentunkid " & _
                                         "        ,sectionunkid " & _
                                         "        ,employeeunkid " & _
                                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                         "    FROM #DB_Name#hremployee_transfer_tran " & _
                                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                         " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                                         " LEFT JOIN #DB_Name#hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                                         " LEFT JOIN #DB_Name#hrsection_master on hrsection_master.sectionunkid = Alloc.sectionunkid " & _
                                         " LEFT JOIN " & _
                                         " ( " & _
                                         "    SELECT " & _
                                         "         jobunkid " & _
                                         "        ,jobgroupunkid " & _
                                         "        ,employeeunkid " & _
                                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                         "    FROM #DB_Name#hremployee_categorization_tran " & _
                                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                         " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                         " LEFT JOIN #DB_Name#hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid "

            strQ = strQ.Replace("#APPR_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master on hremployee_master.employeeunkid = tsapprover_master.employeeapproverunkid ")
            strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
            strQ = strQ.Replace("#DB_Name#", "")
            strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
            strQ = strQ.Replace("#SEC_NAME#", "ISNULL(hrsection_master.name,'') ")
            strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")
            strQ &= StrQCondition
            strQ &= StrQDtFilters
            strQ = strQ.Replace("#ExAppr#", "0")

            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "No"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As New DataSet
            dsCompany = GetExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey : StrQDtFilters = ""
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tsapprover_master.employeeapproverunkid  ")
                    strQ = strQ.Replace("#ExAppr#", "1")
                    strQ = strQ.Replace("#DB_Name#", "")
                    strQ = strQ.Replace("#DATA_JOIN#", "")
                    strQ = strQ.Replace("#DEPT_NAME#", "'' ")
                    strQ = strQ.Replace("#SEC_NAME#", "'' ")
                    strQ = strQ.Replace("#JOB_NAME#", "'' ")
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EDate")), eZeeDate.convertDate(dr("EDate")), , , dr("DName"))
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dr("EDate")), dr("DName"))

                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tsapprover_master.employeeapproverunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")
                    strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
                    strQ = strQ.Replace("#SEC_NAME#", "ISNULL(hrsection_master.name,'') ")
                    strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQDtFilters &= xDateFilterQry & " "
                        End If
                    End If

                End If

                strQ &= StrQCondition
                strQ &= StrQDtFilters
                strQ = strQ.Replace("#ExAppr#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
                objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "No"))


                dstmp = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tsapprover_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintEmployeeapproverunkid, mintTslevelunkid, , mblnIsexternalapprover) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Timesheet Approver already exists. Please define new Timesheet Approver.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tslevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTslevelunkid.ToString)
            objDataOperation.AddParameter("@employeeapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeapproverunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap.ToString)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO tsapprover_master ( " & _
                      "  tslevelunkid " & _
                      ", employeeapproverunkid " & _
                      ", isswap " & _
                      ", isexternalapprover " & _
                      ", isactive " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @tslevelunkid " & _
                      ", @employeeapproverunkid " & _
                      ", @isswap " & _
                      ", @isexternalapprover " & _
                      ", @isactive " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTsapproverunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertATApprover_Master(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objApproverTran._Tsapproverunkid = mintTsapproverunkid
            objApproverTran._DataList = mdtEmployeeTran
            objApproverTran._Userunkid = mintUserunkid
            If objApproverTran.InsertDelete_TimesheetApproverData(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertUpdate_UserMapping(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tsapprover_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintEmployeeapproverunkid, mintTslevelunkid, mintTsapproverunkid, mblnIsexternalapprover) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Timesheet Approver already exists. Please define new Timesheet Approver.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapproverunkid.ToString)
            objDataOperation.AddParameter("@tslevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTslevelunkid.ToString)
            objDataOperation.AddParameter("@employeeapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeapproverunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap.ToString)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE tsapprover_master SET " & _
                      "  tslevelunkid = @tslevelunkid" & _
                      ", employeeapproverunkid = @employeeapproverunkid" & _
                      ", isswap = @isswap" & _
                      ", isexternalapprover = @isexternalapprover" & _
                      ", isactive = @isactive" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                    "WHERE tsapproverunkid = @tsapproverunkid AND isvoid = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertATApprover_Master(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objApproverTran._Tsapproverunkid = mintTsapproverunkid
            objApproverTran._DataList = mdtEmployeeTran
            objApproverTran._Userunkid = mintUserunkid
            If objApproverTran.InsertDelete_TimesheetApproverData(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertUpdate_UserMapping(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tsapprover_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal mdtEmployeeAsonDate As Date) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            'FOR VOID IN TIMESHEET APPROVER TRAN

            strQ = " UPDATE tsapprover_tran SET " & _
                  " isvoid = 1,voiddatetime = GetDate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                  " WHERE tsapproverunkid = @tsapproverunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objApproverTran._EmployeeAsonDate = mdtEmployeeAsonDate
            objApproverTran._Tsapproverunkid = intUnkid
            objApproverTran.GetData(objDataOperation)
            objApproverTran._Userunkid = mintVoiduserunkid
            If objApproverTran._DataList IsNot Nothing AndAlso objApproverTran._DataList.Rows.Count > 0 Then

                For Each dr As DataRow In objApproverTran._DataList.Rows
                    objApproverTran._Tsapprovertranunkid = CInt(dr("tsapprovertranunkid"))
                    If objApproverTran.InsertATTimesheet_ApproverData(objDataOperation, 3, CInt(dr("employeeunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

            End If


            'FOR VOID IN TIMESHEET APPROVER MASTER

            strQ = " Update tsapprover_master set " & _
                       " isvoid = 1,voiddatetime = GetDate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                       " WHERE tsapproverunkid = @tsapproverunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.ExecNonQuery(strQ)

            _Tsapproverunkid(objDataOperation) = intUnkid

            If InsertATApprover_Master(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'FOR DELETE FROM TIMESHEET APPROVER WITH USER MAPPING 

            strQ = "SELECT ISNULL(mappingunkid,0) mappingunkid FROM hrapprover_usermapping WHERE approverunkid = @tsapproverunkid and Usertypeid =" & enUserType.Timesheet_Approver
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(i)("mappingunkid")), False, mintVoiduserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            strQ = "Delete from hrapprover_usermapping WHERE approverunkid = @tsapproverunkid and Usertypeid =" & enUserType.Timesheet_Approver
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
                      "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='tsapproverunkid' "


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = ""
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)


            For Each dtRow As DataRow In dsList.Tables("List").Rows

                If dtRow.Item("TableName").ToString.StartsWith("ts") Then

                    If dtRow.Item("TableName") = "tsapprover_master" OrElse dtRow.Item("TableName") = "attsapprover_master" _
                       OrElse dtRow.Item("TableName") = "tsapprover_tran" OrElse dtRow.Item("TableName") = "attsapprover_tran" _
                       OrElse dtRow.Item("TableName") = "hrapprover_usermapping" Then Continue For

                    objDataOperation.ClearParameters()
                    strQ = "SELECT tsapproverunkid FROM " & dtRow.Item("TableName").ToString & " WHERE tsapproverunkid = @tsapproverunkid  and isvoid = 0"
                    objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    dsList = objDataOperation.ExecQuery(strQ, "Used")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables("Used").Rows.Count > 0 Then
                        blnIsUsed = True
                        Exit For
                    End If
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intApproverEmployeeunkid As Integer, ByVal intApproveLevelunkid As Integer, Optional ByVal intunkid As Integer = -1, Optional ByVal blnIsExAppr As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  tsapproverunkid " & _
                      ", tslevelunkid " & _
                      ", employeeapproverunkid " & _
                      ", isswap " & _
                      ", isexternalapprover " & _
                      ", isactive " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM tsapprover_master " & _
                      " WHERE employeeapproverunkid=@employeeapproverunkid AND tslevelunkid = @tslevelunkid and isvoid = 0 AND isexternalapprover = @isexternalapprover  AND isswap = 0 "

            If intunkid > 0 Then
                strQ &= " AND tsapproverunkid <> @tsapproverunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tslevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproveLevelunkid)
            objDataOperation.AddParameter("@employeeapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverEmployeeunkid)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExAppr)
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintTsapproverunkid = dsList.Tables(0).Rows(0)("tsapproverunkid")
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertUpdate_UserMapping(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim exForce As Exception
        Try
            Dim objUserMapping As New clsapprover_Usermapping
            objUserMapping.GetData(enUserType.Timesheet_Approver, mintTsapproverunkid, -1, -1)
            objUserMapping._Approverunkid = mintTsapproverunkid
            objUserMapping._Userunkid = mintMapUserId
            objUserMapping._UserTypeid = enUserType.Timesheet_Approver
            objUserMapping._AuditUserunkid = mintUserunkid

            If objUserMapping._Mappingunkid > 0 Then
                If objUserMapping.Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If objUserMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdate_UserMapping; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertATApprover_Master(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO attsapprover_master ( " & _
                        "  tsapproverunkid " & _
                        ",  tslevelunkid " & _
                        ", employeeapproverunkid " & _
                        ", isswap " & _
                        ", isexternalapprover " & _
                        ", isactive " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name " & _
                        ", form_name " & _
                        ", module_name1 " & _
                        ", module_name2 " & _
                        ", module_name3 " & _
                        ", module_name4 " & _
                        ", module_name5 " & _
                        ", isweb " & _
                      ") VALUES (" & _
                        "  @tsapproverunkid " & _
                        ", @tslevelunkid " & _
                        ", @employeeapproverunkid " & _
                        ", @isswap " & _
                        ", @isexternalapprover " & _
                        ", @isactive " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", GetDate() " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @form_name " & _
                        ", @module_name1 " & _
                        ", @module_name2 " & _
                        ", @module_name3 " & _
                        ", @module_name4 " & _
                        ", @module_name5 " & _
                        ", @isweb " & _
                      "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapproverunkid.ToString)
            objDataOperation.AddParameter("@tslevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTslevelunkid.ToString)
            objDataOperation.AddParameter("@employeeapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeapproverunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap.ToString)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 5, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATApprover_Master; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetExternalApproverList(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal strList As String = "List", Optional ByVal blnIncludeDelete As Boolean = False) As DataSet
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsApprover As New DataSet

        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If strList.Trim.Length <= 0 Then strList = "List"

            StrQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                   "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                   "    ,ISNULL(UC.key_value,'') AS ModeSet " & _
                   "FROM tsapprover_master " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tsapprover_master.employeeapproverunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                   ") AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                   ") AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "WHERE  tsapprover_master.isswap = 0 " & _
                   " AND tsapprover_master.isexternalapprover = 1 "


            If blnIncludeDelete = False Then
                StrQ &= " AND tsapprover_master.isvoid = 0 "
            End If

            dsApprover = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExternalApproverList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsApprover
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    ''' 
    Public Function InActiveApprover(ByVal intApproverunkid As Integer, ByVal blnIsActive As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If blnIsActive Then
                strQ = " Update tsapprover_master set isactive = 1 where tsapproverunkid = @tsapproverunkid AND isvoid = 0 AND isswap = 0 "
            Else
                strQ = " Update tsapprover_master set isactive = 0 where tsapproverunkid = @tsapproverunkid AND isvoid = 0 AND isswap = 0 "
            End If
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Tsapproverunkid(objDataOperation) = intApproverunkid

            If InsertATApprover_Master(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InActiveApprover; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", _
                                Optional ByVal mblFlag As Boolean = False, _
                                Optional ByVal blnIsActive As Boolean = True, _
                                Optional ByVal mblnShowLevels As Boolean = False) As DataSet

        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception

        Try
            Dim strFinal As String = String.Empty
            Dim strQCondition As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strCombine As String = String.Empty
           

            If mblFlag = True Then
                strSelect = "SELECT  0 AS tsapproverunkid, 0 AS approveremployeeunkid, ' ' +  @approvername  as name, 0 AS tslevelunkid, '' AS isexternalapprover "
                strSelect &= " UNION "
            End If

            strQ = "SELECT " & _
                    "   tsapprover_master.tsapproverunkid " & _
                    "  ,tsapprover_master.employeeapproverunkid AS approveremployeeunkid "

            If mblnShowLevels = True Then
                strQ &= "  ,#APPROVER_NAME# + ' - ' + ISNULL(tsapproverlevel_master.levelname,'') AS name,tsapproverlevel_master.tslevelunkid AS tslevelunkid "
            Else
                strQ &= "  ,#APPROVER_NAME# AS name, 0 AS tslevelunkid "
            End If

            strQ &= "  ,tsapprover_master.isexternalapprover AS isexternalapprover " & _
                        " FROM tsapprover_master " & _
                        " #EMPLOYEE_JOIN# "

            If mblnShowLevels = True Then
                strQ &= " JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid "
            End If

            strFinal = strQ

            strCombine = strSelect
            strCombine &= strQ
            strQ = strCombine
            strCombine = ""


            strQCondition &= " WHERE   tsapprover_master.isvoid = 0 AND tsapprover_master.isswap = 0 " & _
                                      " AND tsapprover_master.isactive = " & IIf(blnIsActive, 1, 0) & ""

            strQCondition &= " AND tsapprover_master.isexternalapprover = #EXT_APPROVER# "

            strQ &= strQCondition


            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tsapprover_master.employeeapproverunkid AND hremployee_master.isapproved = 1 ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet = GetExternalApproverList(objDataOperation, "Company")

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = strFinal
                If dRow("DName").ToString.Trim.Length <= 0 Then 'Nilay (27 Feb 2017)
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = tsapprover_master.employeeapproverunkid ")
                Else
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tsapprover_master.employeeapproverunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid ")

                    'Nilay (27 Feb 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    'strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                    strQ = strQ.Replace("#DB_NAME#", dRow("DName").ToString & "..")
                    'Nilay (27 Feb 2017) -- End


                End If

                strQ &= strQCondition
                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approvername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

                dsExtList = objDataOperation.ExecQuery(strQ, strListName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(strListName).Copy)
                Else
                    dsList.Tables(strListName).Merge(dsExtList.Tables(strListName), True)
                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables(strListName), "", "tslevelunkid, name", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    Public Function GetApproverUnkid(ByVal intApproverEmpunkid As Integer, ByVal blnIsExternalApprover As Boolean, _
                                    Optional ByVal objDataOp As clsDataOperation = Nothing) As String
        Dim strApproverunkid As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT  ISNULL " & _
                                "(STUFF " & _
                                        "(" & _
                                            "(SELECT ',' + CAST(tsapproverunkid AS VARCHAR(MAX)) " & _
                                            " FROM     tsapprover_master " & _
                                            " WHERE tsapprover_master.employeeapproverunkid = " & intApproverEmpunkid & " " & _
                                            "   AND tsapprover_master.isvoid = 0 " & _
                                            "   AND tsapprover_master.isswap = 0 " & _
                                            "   AND tsapprover_master.isactive = 1 " & _
                                            "   AND tsapprover_master.isexternalapprover = @isexternalapprover " & _
                                            " FOR XML PATH('') " & _
                                            ") , 1, 1, '' " & _
                                        "), '' " & _
                                ") ApproverIds "

            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternalApprover)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                strApproverunkid = dsList.Tables(0).Rows(0)("ApproverIds").ToString()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrMessage)
        Finally
            If objDataOp Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try

        Return strApproverunkid
    End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>

    '''Nilay (10 Jan 2017) -- Start
    '''ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
    '''Public Function GetLevelFromLoanApprover(ByVal xApproverEmpunkid As Integer, ByVal blnIsExternalApprover As Boolean, Optional ByVal blnFlag As Boolean = True) As DataSet
    Public Function GetLevelFromTimesheetApprover(ByVal xApproverEmpunkid As Integer, ByVal blnIsExternalApprover As Boolean, Optional ByVal blnFlag As Boolean = True) As DataSet
        'Nilay (10 Jan 2017) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            Dim strApproverunkid As String = ""
            strApproverunkid = GetApproverUnkid(xApproverEmpunkid, blnIsExternalApprover, objDataOperation)

            If blnFlag Then
                strQ = " Select 0 as tslevelunkid , @name as name UNION "
            End If

            strQ &= " SELECT  tsapprover_master.tslevelunkid,levelname as name " & _
                      " FROM tsapprover_master " & _
                      " JOIN tsapproverlevel_master ON tsapprover_master.tslevelunkid = tsapproverlevel_master.tslevelunkid AND tsapproverlevel_master.isactive = 1 " & _
                      " WHERE  tsapprover_master.isvoid = 0 AND tsapprover_master.isactive = 1 AND tsapprover_master.isswap = 0 "

            If xApproverEmpunkid > 0 Then
                strQ &= " AND employeeapproverunkid = " & xApproverEmpunkid
            End If

            If strApproverunkid.Trim.Length > 0 Then
                strQ &= " AND tsapprover_master.tsapproverunkid IN (" & strApproverunkid & ") "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLevelFromTimesheetApprover", mstrMessage)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetTimesheetApproverEmployeeId(ByVal ApproveID As Integer, ByVal blnExternalApprover As Boolean) As String
        Dim mstrEmployeeIds As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT  ISNULL(STUFF(( SELECT   ','  + CAST(tsapprover_tran.employeeunkid AS VARCHAR(MAX)) " & _
                      " FROM  tsapprover_master " & _
                      " JOIN tsapprover_tran ON tsapprover_master.tsapproverunkid = tsapprover_tran.tsapproverunkid AND tsapprover_tran.isvoid = 0" & _
                      " WHERE tsapprover_master.employeeapproverunkid = " & ApproveID & " AND tsapprover_master.isvoid = 0 AND tsapprover_master.isswap = 0 AND tsapprover_master.isactive = 1 " & _
                      " AND tsapprover_master.isexternalapprover = @isexternalapprover " & _
                      " ORDER BY tsapprover_tran.employeeunkid " & _
                      " FOR XML PATH('') ), 1, 1, ''), '') EmployeeIds "

            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnExternalApprover)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTimesheetApproverEmployeeId", mstrModuleName)
        End Try
        Return mstrEmployeeIds
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeApprover(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                             , ByVal mdtEmployeeAsonDate As Date, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                      , Optional ByVal intUserunkid As Integer = -1, Optional ByVal strEmployeeIDs As String = "", Optional ByVal intApproverEmpId As Integer = -1 _
                                                             , Optional ByVal objDoOperation As clsDataOperation = Nothing, Optional ByVal blnExternalApprover As Boolean = False _
                                                             , Optional ByVal mstrFilter As String = "") As DataTable
        'Nilay (10 Jan 2017) -- [intEmployeeunkid REPLACED BY strEmployeeIDs]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try

            Dim StrQCondtion As String = ""
            Dim StrQCommJoin As String = ""
            Dim StrFinalQry As String = ""


            Dim xDateJoinQry, xDateFilterQry
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)

            Dim dsCompany As New DataSet

            dsCompany = GetExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            

            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'strQ = "SELECT " & _
            '       "     tsapprover_master.tsapproverunkid " & _
            '       "    ,#NameValue# AS employeename " & _
            '       "    ,#EmailValue# AS approveremail " & _
            '       "    ,tsapproverlevel_master.priority " & _
            '       "    ,tsapprover_master.employeeapproverunkid " & _
            '       "    ,tsapprover_master.isexternalapprover "
            'If intEmployeeunkid > 0 Then

            strQ = "SELECT " & _
                   "     tsapprover_master.tsapproverunkid " & _
                   "    ,#NameValue# AS employeename " & _
                   "    ,#EmailValue# AS approveremail " & _
                   "    ,tsapproverlevel_master.priority " & _
                   "    ,tsapprover_master.employeeapproverunkid " & _
                   "    ,tsapprover_master.isexternalapprover "
            'Nilay (27 Feb 2017) -- [employeename: Level Name Removed]

            If strEmployeeIDs.Trim.Length > 0 Then
               
                strQ &= "   ,ISNULL(h2.employeecode,'') AS employeecode " & _
                        "   ,ISNULL(h2.employeeunkid, 0) AS employeeunkid "
                'Nilay (10 Jan 2017) -- End
                strQ &= "   ,ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') AS assigndemployeename "
                'Nilay (27 Feb 2017) -- Start
                strQ &= "   ,hrapprover_usermapping.userunkid AS MappedUserID "
                'Nilay (27 Feb 2017) -- End

            End If

            strQ &= "FROM tsapprover_master " & _
                    "  LEFT JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid "

            StrQCommJoin = " #EMP_JOIN#  " & _
                                      " #DATE_JOIN# "

            If intUserunkid > 0 Then
                StrQCommJoin &= " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = tsapprover_master.tsapproverunkid AND hrapprover_usermapping.usertypeid=" & enUserType.Timesheet_Approver
            End If

            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'If intEmployeeunkid > 0 Then
            If strEmployeeIDs.Trim.Length > 0 Then
                'Nilay (10 Jan 2017) -- End
                StrQCommJoin &= " JOIN tsapprover_tran ON tsapprover_tran.tsapproverunkid = tsapprover_master.tsapproverunkid AND tsapprover_tran.isvoid= 0 " & _
                                            " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = tsapprover_master.tsapproverunkid AND hrapprover_usermapping.usertypeid= " & enUserType.Timesheet_Approver & _
                                            " LEFT JOIN hremployee_master h2 ON h2.employeeunkid = tsapprover_tran.employeeunkid "
            End If


            'Pinkal (07-Oct-2019) -- Start
            'Defect KNCV - Excluding Swapped and take only active approver.
            'StrQCondtion = " WHERE tsapprover_master.isvoid = 0 "
            StrQCondtion = " WHERE tsapprover_master.isvoid = 0  AND tsapprover_master.isactive = 1 AND tsapprover_master.isswap  = 0 "
            'Pinkal (07-Oct-2019) -- End



            If intUserunkid > 0 Then
                StrQCondtion &= " AND hrapprover_usermapping.userunkid = @Userunkid "

                If intApproverEmpId > 0 Then

                    If blnExternalApprover Then
                        StrQCondtion &= " AND tsapprover_master.employeeapproverunkid = @approverEmpId "
                    Else
                        StrQCondtion &= " AND hremployee_master.employeeunkid = @approverEmpId "
                    End If

                    objDataOperation.AddParameter("@approverEmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverEmpId.ToString)
                End If
                objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid.ToString)
            End If


            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'If intEmployeeunkid > 0 Then
            '    StrQCondtion &= " AND tsapprover_tran.employeeunkid = @employeeunkid "
            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)
            'End If
            If strEmployeeIDs.Trim.Length > 0 Then
                StrQCondtion &= " AND tsapprover_tran.employeeunkid IN (" & strEmployeeIDs & ") "
            End If
            'Nilay (10 Jan 2017) -- End

            StrQCondtion &= " AND tsapprover_master.isexternalapprover = #ExApr# "

            If mstrFilter.Trim.Length > 0 Then
                StrQCondtion &= " AND " & mstrFilter & " "
            End If

            StrFinalQry = strQ

            StrFinalQry &= StrQCommJoin & " "
            StrFinalQry &= StrQCondtion & " "
            StrFinalQry = StrFinalQry.Replace("#EMP_JOIN#", "LEFT JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = tsapprover_master.employeeapproverunkid ")
            StrFinalQry = StrFinalQry.Replace("#DBName#", "")
            StrFinalQry = StrFinalQry.Replace("#NameValue#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')")
            StrFinalQry = StrFinalQry.Replace("#EmailValue#", "ISNULL(hremployee_master.email,'')")
            StrFinalQry = StrFinalQry.Replace("#DATE_JOIN#", xDateJoinQry)
            StrFinalQry = StrFinalQry.Replace("#ExApr#", "0")

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrFinalQry &= xDateFilterQry & " "
                End If
            End If

            dsList = objDataOperation.ExecQuery(StrFinalQry, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                Dim dstmp As New DataSet

                xDateJoinQry = "" : xDateFilterQry = ""
                Dim StrDBName As String : StrFinalQry = "" : StrDBName = ""
                StrDBName = dr("DName")



                StrFinalQry = strQ

                StrFinalQry &= " LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tsapprover_master.employeeapproverunkid " & vbCrLf & _
                               StrQCommJoin & " "

                StrFinalQry = StrFinalQry.Replace("#EMP_JOIN#", " LEFT JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid AND hremployee_master.isapproved = 1 ")

                StrFinalQry &= StrQCondtion & " AND tsapprover_master.isexternalapprover = 1 AND cfuser_master.companyunkid = #CoyId# "

                StrFinalQry = StrFinalQry.Replace("#CoyId#", dr("companyunkid"))
                StrFinalQry = StrFinalQry.Replace("#ExApr#", "1")

                If StrDBName.Trim.Length <= 0 Then
                    StrFinalQry = StrFinalQry.Replace("#DBName#", "")
                Else
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EDate")), eZeeDate.convertDate(dr("EDate")), , , dr("DName"))
                    StrFinalQry = StrFinalQry.Replace("#DBName#", StrDBName & "..")
                End If

                StrFinalQry = StrFinalQry.Replace("#NameValue#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                StrFinalQry = StrFinalQry.Replace("#EmailValue#", "CASE WHEN ISNULL(hremployee_master.email,'') = '' THEN ISNULL(cfuser_master.email,'') ELSE ISNULL(hremployee_master.email,'') END ")

                StrFinalQry = StrFinalQry.Replace("#DATE_JOIN#", xDateJoinQry)

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrFinalQry &= xDateFilterQry & " "
                    End If
                End If

                dstmp = objDataOperation.ExecQuery(StrFinalQry, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next

            Return dsList.Tables(0)


        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprover; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeFromTimesheetApprover(ByVal xDatabaseName As String _
                                                                                   , ByVal xUserUnkid As Integer _
                                                                                   , ByVal xYearUnkid As Integer _
                                                                                   , ByVal xCompanyUnkid As Integer _
                                                                                   , ByVal xPeriodStart As DateTime _
                                                                                   , ByVal xPeriodEnd As DateTime _
                                                                                   , ByVal xUserModeSetting As String _
                                                                                   , ByVal xOnlyApproved As Boolean _
                                                                                   , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                                                   , ByVal strTableName As String _
                                                                                   , ByVal intEmpApproveunkid As Integer _
                                                                                   , ByVal intLevelId As Integer _
                                                                                   , ByVal blnIsExternalApprover As Boolean) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            Dim strApproverUnkids As String = ""
            strApproverUnkids = GetApproverUnkid(intEmpApproveunkid, blnIsExternalApprover, objDataOperation)

            strQ = "SELECT  " & _
                      " 0 as 'select' " & _
                      ", tsapprover_master.tsapproverunkid " & _
                      ", tsapprover_tran.tsapprovertranunkid " & _
                      ", tsapprover_master.employeeapproverunkid " & _
                      ", tsapprover_tran.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode,'') as employeecode " & _
                      ", ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername,'')  + ' '  + ISNULL(hremployee_master.surname,'') as employeename " & _
                      ", hremployee_master.gender " & _
                      ", hremployee_master.maritalstatusunkid " & _
                      ", hremployee_master.employmenttypeunkid " & _
                      ", hremployee_master.paytypeunkid " & _
                      ", hremployee_master.paypointunkid " & _
                      ", hremployee_master.nationalityunkid " & _
                      ", hremployee_master.religionunkid " & _
                      ", ETT.stationunkid " & _
                      ", ETT.deptgroupunkid " & _
                      ", ETT.departmentunkid " & _
                      ", ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                      ", ETT.sectionunkid " & _
                      ", ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                      ", ETT.unitunkid " & _
                      ", ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                      ", ETT.classgroupunkid " & _
                      ", ETT.classunkid " & _
                      ", ECT.jobgroupunkid " & _
                      ", ECT.jobunkid " & _
                      ", GRD.gradegroupunkid " & _
                      ", GRD.gradeunkid " & _
                      ", GRD.gradelevelunkid " & _
                      ", CC.cctranheadvalueid " & _
                      ", tsapproverlevel_master.tslevelunkid " & _
                      ", tsapproverlevel_master.levelname " & _
                      ", tsapproverlevel_master.priority " & _
                      ", tsapprover_master.isexternalapprover " & _
                " FROM  tsapprover_master " & _
                      " JOIN tsapprover_tran ON tsapprover_master.tsapproverunkid = tsapprover_tran.tsapproverunkid AND tsapprover_tran.isvoid = 0  " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = tsapprover_tran.employeeunkid " & _
                      " JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid  " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        stationunkid " & _
                      "       ,deptgroupunkid " & _
                      "       ,departmentunkid " & _
                      "       ,sectiongroupunkid " & _
                      "       ,sectionunkid " & _
                      "       ,unitgroupunkid " & _
                      "       ,unitunkid " & _
                      "       ,teamunkid " & _
                      "       ,classgroupunkid " & _
                      "       ,classunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "   FROM hremployee_transfer_tran " & _
                      "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ETT ON ETT.employeeunkid = hremployee_master.employeeunkid AND ETT.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        jobgroupunkid " & _
                      "       ,jobunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "   FROM hremployee_categorization_tran " & _
                      "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        gradegroupunkid " & _
                      "       ,gradeunkid " & _
                      "       ,gradelevelunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                      "   FROM prsalaryincrement_tran " & _
                      "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS GRD ON GRD.employeeunkid = hremployee_master.employeeunkid AND GRD.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "     SELECT " & _
                      "         cctranheadvalueid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "     FROM hremployee_cctranhead_tran " & _
                      "     WHERE istransactionhead = 0 AND isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                      " WHERE tsapprover_master.isvoid = 0 AND tsapprover_master.isactive = 1  AND hremployee_master.isapproved = 1 " & _
                      " AND tsapprover_master.isswap = 0  AND tsapprover_master.employeeapproverunkid = " & intEmpApproveunkid

            If strApproverUnkids.Trim.Length > 0 Then
                strQ &= " AND tsapprover_master.tsapproverunkid IN (" & strApproverUnkids & ") "
            End If

            If intLevelId >= 0 Then
                strQ &= " AND tsapprover_master.tslevelunkid = " & intLevelId
            End If

            strQ &= " ORDER By levelname,isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeFromTimesheetApprover", mstrMessage)
        End Try
        Return dsList
    End Function

    'Nilay (10 Jan 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
    Public Function GetLevelFromUserLogin(ByVal intUserLoginID As Integer) As DataSet
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            Dim strQCondition As String = String.Empty
            Dim strQFinal As String = String.Empty

            strQ = "SELECT " & _
                      "  #CODE# AS ApproverCode " & _
                      " ,#APPROVER_NAME# AS ApproverName " & _
                      " ,#APPROVER_NAME# + ' - ' +  ISNULL(tsapproverlevel_master.levelname,'') AS Approver " & _
                      " ,ISNULL(tsapproverlevel_master.tslevelunkid,0) AS tslevelunkid " & _
                      " ,ISNULL(tsapproverlevel_master.levelname,'') AS levelname " & _
                      " ,ISNULL(tsapproverlevel_master.priority,0) AS priority " & _
                      " ,tsapprover_master.tsapproverunkid " & _
                      " ,tsapprover_master.employeeapproverunkid " & _
                      " ,tsapprover_master.isexternalapprover " & _
                   " FROM hrapprover_usermapping " & _
                      " LEFT JOIN tsapprover_master ON tsapprover_master.tsapproverunkid = hrapprover_usermapping.approverunkid " & _
                      "     AND tsapprover_master.isvoid = 0  " & _
                      " #EMPLOYEE_JOIN# " & _
                      " LEFT JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid " & _
                      "     AND tsapproverlevel_master.isactive=1 " & _
                      " LEFT JOIN hrmsconfiguration..cfuser_master AS UM ON UM.userunkid = hrapprover_usermapping.userunkid " & _
                   " WHERE hrapprover_usermapping.usertypeid=5 " & _
                      " AND hrapprover_usermapping.userunkid=@loginUserID "

            strQFinal = strQ

            strQCondition = " AND tsapprover_master.isexternalapprover = #EXT_APPROVER# "

            strQ &= strQCondition

            strQ = strQ.Replace("#CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tsapprover_master.employeeapproverunkid " & _
                                " AND hremployee_master.isapproved=1 ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserLoginID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet


            'Pinkal (16-Feb-2017) -- Start
            'Enhancement - Working on Employee Budget Timesheet Report for Aga Khan.
            'Dim objlnApprover As New clsLoanApprover_master
            'dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")
            dsCompany = GetExternalApproverList(objDataOperation, "Company", False)
            'Pinkal (16-Feb-2017) -- End

            
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow In dsCompany.Tables("Company").Rows
                strQ = strQFinal


                'Pinkal (16-Feb-2017) -- Start
                'Enhancement - Working on Employee Budget Timesheet Report for Aga Khan.
                'If dRow("dbname").ToString.Trim.Length <= 0 Then
                If dRow("DName").ToString.Trim.Length <= 0 Then
                    'Pinkal (16-Feb-2017) -- End
                    strQ = strQ.Replace("#CODE#", "'' ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = tsapprover_master.employeeapproverunkid ")

                Else
                    strQ = strQ.Replace("#CODE#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN '' " & _
                                                       "ELSE ISNULL(hremployee_master.employeecode,'') END ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tsapprover_master.employeeapproverunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")

                    'Pinkal (16-Feb-2017) -- Start
                    'Enhancement - Working on Employee Budget Timesheet Report for Aga Khan.
                    'strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                    strQ = strQ.Replace("#DB_NAME#", dRow("DName").ToString & "..")
                    'Pinkal (16-Feb-2017) -- End


                End If

                strQ &= strQCondition
                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@loginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserLoginID)

                dsExtList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables("List").Copy)
                Else
                    dsList.Tables("List").Merge(dsExtList.Tables("List"), True)
                End If

            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority,ApproverName", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLevelFromUserLogin; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Nilay (10 Jan 2017) -- End


    'Pinkal (25-Nov-2020) -- Start
    'Enhancement IHI - Working on Import Timesheet Approver.
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    Public Function ImportApproverInsert() As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@tslevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTslevelunkid.ToString)
            objDataOperation.AddParameter("@employeeapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeapproverunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap.ToString)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)
            
            strQ = "INSERT INTO tsapprover_master ( " & _
                      "  tslevelunkid " & _
                      ", employeeapproverunkid " & _
                      ", isswap" & _
                      ", isexternalapprover" & _
                      ", isactive" & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", userunkid " & _
                      ", voiduserunkid" & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @tslevelunkid " & _
                      ", @employeeapproverunkid " & _
                      ", @isswap " & _
                      ", @isexternalapprover " & _
                      ", @isactive " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @userunkid " & _
                      ", @voiduserunkid" & _
                      ", @voidreason" & _
                    "); SELECT @@identity"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTsapproverunkid = dsList.Tables(0).Rows(0).Item(0)


            If InsertATApprover_Master(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mblnIsexternalapprover = True Then
                Dim objUserMapping As New clsapprover_Usermapping
                objUserMapping.GetData(enUserType.Timesheet_Approver, mintTsapproverunkid, -1, -1)
                objUserMapping._Approverunkid = mintTsapproverunkid
                objUserMapping._Userunkid = mintEmployeeapproverunkid
                objUserMapping._UserTypeid = enUserType.Timesheet_Approver
                objUserMapping._AuditUserunkid = mintUserunkid

                If objUserMapping._Mappingunkid > 0 Then
                    If objUserMapping.Update(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If objUserMapping.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Else
                strQ = "SELECT ISNULL(userunkid,0) AS UsrId " & _
                       "FROM hremployee_master " & _
                       "JOIN hrmsConfiguration..cfuser_master ON hremployee_master.displayname = hrmsConfiguration..cfuser_master.username " & _
                       "WHERE hremployee_master.employeeunkid = '" & mintEmployeeapproverunkid & "' "

                dsList = objDataOperation.ExecQuery(strQ, "Usr")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Usr").Rows.Count > 0 Then
                    If dsList.Tables("Usr").Rows(0).Item("UsrId") > 0 Then
                        Dim objUserMapping As New clsapprover_Usermapping
                        objUserMapping.GetData(enUserType.Timesheet_Approver, mintTsapproverunkid, -1, -1)
                        objUserMapping._Approverunkid = mintTsapproverunkid
                        objUserMapping._Userunkid = dsList.Tables("Usr").Rows(0).Item("UsrId")
                        objUserMapping._UserTypeid = enUserType.Timesheet_Approver
                        objUserMapping._AuditUserunkid = mintUserunkid

                        If objUserMapping._Mappingunkid > 0 Then
                            If objUserMapping.Update(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Else
                            If objUserMapping.Insert(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If
                End If
            End If

            Return mintTsapproverunkid

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ImportApproverInsert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetImportFileStructure() As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = " SELECT " & _
                          " '' ApproverCode " & _
                          ", '' ApproverName " & _
                          ", '' LevelName " & _
                          ", '' EmployeeCode " & _
                          ", '' EmployeeName "

            dsList = objDataOperation.ExecQuery(strQ, "AccrueLeave")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetImportFileStructure", mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (25-Nov-2020) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Timesheet Approver already exists. Please define new Timesheet Approver.")
            Language.setMessage(mstrModuleName, 3, "Yes")
            Language.setMessage(mstrModuleName, 4, "No")
            Language.setMessage(mstrModuleName, 5, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class