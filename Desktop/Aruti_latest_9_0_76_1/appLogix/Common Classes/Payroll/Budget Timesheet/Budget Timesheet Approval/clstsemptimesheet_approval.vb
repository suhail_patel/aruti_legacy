﻿'************************************************************************************************************************************
'Class Name : clstsemptimesheet_approval.vb
'Purpose    :
'Date       :03/11/2016
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm


''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clstsemptimesheet_approval
    Private Shared ReadOnly mstrModuleName As String = "clstsemptimesheet_approval"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintTimesheetapprovalunkid As Integer
    Private mintEmptimesheetunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintFundsourceunkid As Integer
    Private mintProjectcodeunkid As Integer
    Private mintActivityunkid As Integer
    Private mdtApprovaldate As Date
    Private mintTsapproverunkid As Integer
    Private mintApproveremployeeunkid As Integer
    'Private mintPriority As Integer

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
    'Private mintActivity_Hrs As Integer
    Private mdecActivity_Hrs As Decimal
    'Pinkal (28-Mar-2018) -- End

    Private mintStatusunkid As Integer
    Private mintVisibleunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mblnIsCancel As Boolean = False
    Private mintCanceluserunkid As Integer
    Private mdtCanceldatetime As Date
    Private mstrCancelReason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mdtApprovalData As DataTable = Nothing

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set timesheetapprovalunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Timesheetapprovalunkid(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTimesheetapprovalunkid
        End Get
        Set(ByVal value As Integer)
            mintTimesheetapprovalunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emptimesheetunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Emptimesheetunkid() As Integer
        Get
            Return mintEmptimesheetunkid
        End Get
        Set(ByVal value As Integer)
            mintEmptimesheetunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundsourceunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fundsourceunkid() As Integer
        Get
            Return mintFundsourceunkid
        End Get
        Set(ByVal value As Integer)
            mintFundsourceunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set projectcodeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Projectcodeunkid() As Integer
        Get
            Return mintProjectcodeunkid
        End Get
        Set(ByVal value As Integer)
            mintProjectcodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activityunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Activityunkid() As Integer
        Get
            Return mintActivityunkid
        End Get
        Set(ByVal value As Integer)
            mintActivityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tsapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tsapproverunkid() As Integer
        Get
            Return mintTsapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintTsapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveremployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approveremployeeunkid() As Integer
        Get
            Return mintApproveremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveremployeeunkid = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set Priority
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Priority() As Integer
    '    Get
    '        Return mintPriority
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintPriority = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set activity_hrs
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Activity_Hrs() As Integer
    '    Get
    '        Return mintActivity_Hrs
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintActivity_Hrs = value
    '    End Set
    'End Property


    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
    ''' <summary>
    ''' Purpose: Get or Set activity_hrs
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Activity_Hrs() As Decimal
        Get
            Return mdecActivity_Hrs
        End Get
        Set(ByVal value As Decimal)
            mdecActivity_Hrs = value
        End Set
    End Property
    'Pinkal (28-Mar-2018) -- End

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visibleId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Visibleunkid() As Integer
        Get
            Return mintVisibleunkid
        End Get
        Set(ByVal value As Integer)
            mintVisibleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isCancel
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsCancel() As Boolean
        Get
            Return mblnIsCancel
        End Get
        Set(ByVal value As Boolean)
            mblnIsCancel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceluserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Canceluserunkid() As Integer
        Get
            Return mintCanceluserunkid
        End Get
        Set(ByVal value As Integer)
            mintCanceluserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceldatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Canceldatetime() As Date
        Get
            Return mdtCanceldatetime
        End Get
        Set(ByVal value As Date)
            mdtCanceldatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cancelreason() As String
        Get
            Return mstrCancelReason
        End Get
        Set(ByVal value As String)
            mstrCancelReason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApprovalData
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ApprovalData() As DataTable
        Get
            Return mdtApprovalData
        End Get
        Set(ByVal value As DataTable)
            mdtApprovalData = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  timesheetapprovalunkid " & _
                      ", emptimesheetunkid " & _
                      ", periodunkid " & _
                      ", employeeunkid " & _
                      ", fundsourceunkid " & _
                      ", projectcodeunkid " & _
                      ", activityunkid " & _
                      ", approvaldate " & _
                      ", tsapproverunkid " & _
                      ", approveremployeeunkid " & _
                      ", activity_hrs " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", description " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", iscancel " & _
                      ", canceluserunkid " & _
                      ", canceldatetime " & _
                      ", cancelreason " & _
                     "FROM tsemptimesheet_approval " & _
                     "WHERE timesheetapprovalunkid = @timesheetapprovalunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@timesheetapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTimesheetapprovalunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTimesheetapprovalunkid = CInt(dtRow.Item("timesheetapprovalunkid"))
                mintEmptimesheetunkid = CInt(dtRow.Item("emptimesheetunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintFundsourceunkid = CInt(dtRow.Item("fundsourceunkid"))
                mintProjectcodeunkid = CInt(dtRow.Item("projectcodeunkid"))
                mintActivityunkid = CInt(dtRow.Item("activityunkid"))
                mdtApprovaldate = dtRow.Item("approvaldate")
                mintTsapproverunkid = CInt(dtRow.Item("tsapproverunkid"))
                mintApproveremployeeunkid = CInt(dtRow.Item("approveremployeeunkid"))

                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                'mintActivity_Hrs = CInt(dtRow.Item("activity_hrs"))
                mdecActivity_Hrs = CDec(dtRow.Item("activity_hrs"))
                'Pinkal (28-Mar-2018) -- End


                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintVisibleunkid = CInt(dtRow.Item("visibleunkid"))
                mstrDescription = dtRow.Item("description").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = IIf(IsDBNull(dtRow.Item("voiddatetime")), Nothing, dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIsCancel = CBool(dtRow.Item("iscancel"))
                mintCanceluserunkid = CInt(dtRow.Item("canceluserunkid"))
                mdtCanceldatetime = IIf(IsDBNull(dtRow.Item("canceldatetime")), Nothing, dtRow.Item("canceldatetime"))
                mstrCancelReason = dtRow.Item("cancelreason").ToString()
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                                    , ByVal xDatabaseName As String _
                                    , ByVal mdtEmployeeAsOnDate As Date _
                                    , ByVal mstrDateFormat As String _
                                    , ByVal xUserID As Integer _
                                    , ByVal mblnMyApproval As Boolean _
                                    , ByVal mblnAllowOverTimeToEmpTimesheet As Boolean _
                                    , Optional ByVal objDOperation As clsDataOperation = Nothing _
                                    , Optional ByVal blnOnlyActive As Boolean = True _
                                    , Optional ByVal xEmpTimesheetID As Integer = 0 _
                                    , Optional ByVal mblnIsGrp As Boolean = False _
                                    , Optional ByVal mstrFilter As String = "") As DataSet


        'Pinkal (13-Aug-2018) --  'Enhancement - Changes For PACT [Ref #249,252] [, ByVal mblnAllowOverTimeToEmpTimesheet As Boolean _]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim StrQCondition As String = String.Empty
        Dim StrQCommonQry As String = String.Empty
        Dim dsCompany As New DataSet

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If
        objDataOperation.ClearParameters()

        Try


            strQ = "SELECT DISTINCT " & _
                 "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                 "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                 "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                 "FROM tsapprover_master " & _
                 "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tsapprover_master.employeeapproverunkid " & _
                 "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                 "    LEFT JOIN " & _
                 "    ( " & _
                 "        SELECT " & _
                 "             cfconfiguration.companyunkid " & _
                 "            ,cfconfiguration.key_value " & _
                 "        FROM hrmsConfiguration..cfconfiguration " & _
                 "        WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                 "    ) AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                 "WHERE tsapprover_master.isvoid = 0 AND tsapprover_master.isexternalapprover = 1 "

            dsCompany = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsOnDate, xDatabaseName)


            strQ = ""


            'Pinkal (13-Oct-2017) -- Ref Id 62 Working on Global Budget Timesheet Change.[CAST (0 AS bit) AS ischeck]

            If mblnIsGrp Then
                strQ = " SELECT DISTINCT " & _
                          " CAST (0 AS bit) AS ischecked " & _
                           " , -1 AS timesheetapprovalunkid " & _
                           " , -1 AS emptimesheetunkid " & _
                           " , -1 AS periodunkid " & _
                           " , '' AS period " & _
                           " , tsemptimesheet_approval.employeeunkid " & _
                           " , NULL AS activitydate " & _
                           " , CONVERT(CHAR(8),ltbemployee_timesheet.activitydate,112) AS ADate " & _
                           " , ' [ '  + @ActivityDate + ' : '  + REPLACE(RTRIM(CONVERT(char(15),ltbemployee_timesheet.activitydate,106)),' ','-') + ' ]' + ' : ' + ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Particulars " & _
                           " , ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee  " & _
                           " , -1 AS fundsourceunkid " & _
                           " , '' AS fundname  " & _
                           " , -1 AS projectcodeunkid " & _
                           " , '' AS fundprojectname " & _
                           " , -1 AS activityunkid " & _
                           " , '' AS activity_name " & _
                           " , NULL AS approvaldate " & _
                           " , -1 AS tsapproverunkid " & _
                           " , -1 AS approveremployeeunkid " & _
                           " , '' AS Approver " & _
                           " , '' AS ApproverName " & _
                           " , '' AS activity_hrs " & _
                           " , 0 AS activity_hrsinMins " & _
                           " , -1 AS EmptimesheetStatusId" & _
                           " , -1 as ApprovalStatusId " & _
                           " , -1 as visibleunkid " & _
                           " , '' AS status " & _
                           " , '' AS description " & _
                           " , '' AS remark " & _
                           " , -1 AS userunkid " & _
                           " , 0 AS isvoid " & _
                           " , -1 AS voiduserunkid " & _
                           " , NULL AS voiddatetime " & _
                           " , '' AS voidreason " & _
                           " , -1 AS canceluserunkid " & _
                           " , NULL AS canceldatetime " & _
                           " , '' AS cancelreason " & _
                           " , CAST (1 AS bit) AS IsGrp " & _
                           " ,-1 AS sectiongroupunkid " & _
                           " ,-1 AS unitgroupunkid " & _
                           " ,-1 AS teamunkid " & _
                           " ,-1 AS stationunkid " & _
                           " ,-1 AS deptgroupunkid " & _
                           " ,-1 AS departmentunkid " & _
                           " ,-1 AS sectionunkid " & _
                           " ,-1 AS unitunkid " & _
                           " ,-1 AS jobunkid " & _
                           " ,-1 AS classgroupunkid " & _
                           " ,-1 AS classunkid " & _
                           " ,-1 AS jobgroupunkid " & _
                           " ,-1 AS gradegroupunkid " & _
                           " ,-1 AS gradeunkid " & _
                           " ,-1 AS gradelevelunkid " & _
                           " ,-1 AS mapuserunkid " & _
                           " ,-1 AS priority " & _
                           " ,-1 AS tslevelunkid " & _
                           " ,0 AS percentage " & _
                           " ,#ExValue# AS isexternalapprover " & _
                           " ,'00:00' AS ShiftHours " & _
                           " ,0 AS ShiftHoursInSec " & _
                           " ,'' AS TotalActivityHours " & _
                           " ,'' AS TotalActivityHoursInMin " & _
                           " ,'' AS submission_remark " & _
                           " ,CAST(0 AS BIT) AS isholiday " & _
                          " FROM  ltbemployee_timesheet  " & _
                          " LEFT JOIN tsemptimesheet_approval ON ltbemployee_timesheet.emptimesheetunkid = tsemptimesheet_approval.emptimesheetunkid AND tsemptimesheet_approval.isvoid  =  0 AND iscancel = 0 " & _
                          " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = tsemptimesheet_approval.periodunkid " & _
                          " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tsemptimesheet_approval.employeeunkid " & _
                           " #APPR_NAME_JOIN#  " & _
                          " LEFT JOIN tsapprover_master ON tsapprover_master.tsapproverunkid = tsemptimesheet_approval.tsapproverunkid  AND tsapprover_master.employeeapproverunkid = tsemptimesheet_approval.approveremployeeunkid " & _
                          " LEFT JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid	" & _
                          " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = tsapprover_master.tsapproverunkid  AND hrapprover_usermapping.usertypeid = " & enUserType.Timesheet_Approver & _
                          " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = tsemptimesheet_approval.activityunkid "

                'Pinkal (13-Aug-2018) --  'Enhancement - Changes For PACT [Ref #249,252] [ " ,CAST(0 AS BIT) AS isholiday " & _] 

                'Pinkal (28-Jul-2018) -- 'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251][" ,'' AS submission_remark " & _]

                If blnOnlyActive Then
                    strQ &= " WHERE  ltbemployee_timesheet.isvoid = 0  "
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                If mblnMyApproval Then
                    If xUserID > 0 Then
                    strQ &= " AND  hrapprover_usermapping.userunkid = " & xUserID
                End If
                End If

                strQ &= " UNION "

            End If

            'Pinkal (16 Jun 2017) -- Start [With sohail Guidance as change in Budget Module Table Structure]
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            'USE DISTINCT IN BELOW QUERY.
            'Pinkal (16 Jun 2017) -- End


            'Pinkal (13-Oct-2017) -- Ref Id 62 Working on Global Budget Timesheet Change.[CAST (0 AS bit) AS ischeck]

            strQ &= "SELECT DISTINCT " & _
                          " CAST (0 AS bit) AS ischecked " & _
                          " , tsemptimesheet_approval.timesheetapprovalunkid " & _
                          " , tsemptimesheet_approval.emptimesheetunkid " & _
                          " , tsemptimesheet_approval.periodunkid " & _
                          " , ISNULL(cfcommon_period_tran.period_name,'') AS period " & _
                          " , tsemptimesheet_approval.employeeunkid " & _
                          " , ltbemployee_timesheet.activitydate " & _
                          " , CONVERT(CHAR(8),ltbemployee_timesheet.activitydate,112) AS ADate " & _
                          " , '' AS Particulars " & _
                          " , ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee  " & _
                          " , tsemptimesheet_approval.fundsourceunkid " & _
                          " , ISNULL(bgfundsource_master.fundname,'') AS fundname  " & _
                          " , tsemptimesheet_approval.projectcodeunkid " & _
                          " , ISNULL(bgfundprojectcode_master.fundprojectname,'') AS fundprojectname " & _
                          " , tsemptimesheet_approval.activityunkid " & _
                          " , ISNULL(bgfundactivity_tran.activity_name,'') AS activity_name " & _
                          " , tsemptimesheet_approval.approvaldate " & _
                          " , tsemptimesheet_approval.tsapproverunkid " & _
                          " , tsemptimesheet_approval.approveremployeeunkid " & _
                          " , #APPR_NAME_VALUE# AS Approver " & _
                          " , #APPR_NAME# AS ApproverName " & _
                          " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),CAST(tsemptimesheet_approval.activity_hrs AS INT) / 60), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( CAST(tsemptimesheet_approval.activity_hrs AS DECIMAL) % 60)), 2), '00:00') AS activity_hrs " & _
                          " , tsemptimesheet_approval.activity_hrs AS activity_hrsinMins " & _
                          " , ltbemployee_timesheet.statusunkid AS EmptimesheetStatusId" & _
                          " , tsemptimesheet_approval.statusunkid as ApprovalStatusId " & _
                          " , tsemptimesheet_approval.visibleunkid " & _
                          " , CASE WHEN tsemptimesheet_approval.statusunkid = 1 THEN @Approve " & _
                          "            WHEN tsemptimesheet_approval.statusunkid = 2 THEN @Pending  " & _
                          "            WHEN tsemptimesheet_approval.statusunkid = 3 THEN @Reject " & _
                          "            WHEN tsemptimesheet_approval.statusunkid = 6 THEN @Cancel End AS status" & _
                          " , ltbemployee_timesheet.description " & _
                          " , tsemptimesheet_approval.description AS remark " & _
                          " , tsemptimesheet_approval.userunkid " & _
                          " , tsemptimesheet_approval.isvoid " & _
                          " , tsemptimesheet_approval.voiduserunkid " & _
                          " , tsemptimesheet_approval.voiddatetime " & _
                          " , tsemptimesheet_approval.voidreason " & _
                          " , tsemptimesheet_approval.canceluserunkid " & _
                          " , tsemptimesheet_approval.canceldatetime " & _
                          " , tsemptimesheet_approval.cancelreason " & _
                          " , CAST (0 AS bit) AS IsGrp " & _
                          " , ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                          " , ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                          " , ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                          " , ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
                          " , ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
                          " , ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
                          " , ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
                          " , ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
                          " , ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
                          " , ISNULL(Alloc.classunkid,0) AS classunkid " & _
                          " , ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
                          " , ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
                          " , ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
                          " , ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
                          " , ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
                          " , ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
                          " , tsapproverlevel_master.priority " & _
                          " , tsapproverlevel_master.tslevelunkid " & _
                          " , bgbudgetcodesfundsource_tran.percentage " & _
                          " , #ExValue#  AS isexternalapprover " & _
                          " , '00:00' AS ShiftHours " & _
                          " , 0 AS ShiftHoursInSec " & _
                          " , '00:00' AS TotalActivityHours " & _
                          " , 0 AS TotalActivityHoursInMin " & _
                          " , ISNULL(ltbemployee_timesheet.submission_remark,'') AS submission_remark "

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            If mblnAllowOverTimeToEmpTimesheet Then
                strQ &= ", CASE WHEN ISNULL(lvemployee_holiday.holidaytranunkid,0) > 0  THEN 1 ELSE 0 END isholiday "
            Else
                strQ &= ", CAST(0 AS BIT) AS  isholiday "
            End If

            'Pinkal (13-Aug-2018) -- End

            strQ &= " FROM tsemptimesheet_approval " & _
                          " LEFT JOIN ltbemployee_timesheet ON ltbemployee_timesheet.emptimesheetunkid = tsemptimesheet_approval.emptimesheetunkid AND ltbemployee_timesheet.isvoid = 0 " & _
                          " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = tsemptimesheet_approval.periodunkid " & _
                          " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tsemptimesheet_approval.employeeunkid " & _
                          " #APPR_NAME_JOIN#  " & _
                          " LEFT JOIN tsapprover_master ON tsapprover_master.tsapproverunkid = tsemptimesheet_approval.tsapproverunkid  AND tsapprover_master.employeeapproverunkid = tsemptimesheet_approval.approveremployeeunkid " & _
                          " LEFT JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid	" & _
                           " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = tsapprover_master.tsapproverunkid  AND hrapprover_usermapping.usertypeid = " & enUserType.Timesheet_Approver & _
                          " LEFT JOIN bgfundsource_master ON bgfundsource_master.fundsourceunkid = tsemptimesheet_approval.fundsourceunkid  " & _
                          " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid =  tsemptimesheet_approval.projectcodeunkid " & _
                          " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = tsemptimesheet_approval.activityunkid "


            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            '" , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),tsemptimesheet_approval.activity_hrs  / 60), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( tsemptimesheet_approval.activity_hrs AS DECIMAL % 60)), 2), '00:00') AS activity_hrs " & _
            'Pinkal (28-Mar-2018) -- End


            'Pinkal (16 Jun 2017) -- Start [With sohail Guidance as change in Budget Module Table Structure]
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            '   " LEFT JOIN bgbudgetcodesfundsource_tran ON ltbemployee_timesheet.periodunkid = bgbudgetcodesfundsource_tran.periodunkid " & _
            '   " AND ltbemployee_timesheet.activityunkid= bgbudgetcodesfundsource_tran.fundactivityunkid  AND bgbudgetcodesfundsource_tran.isvoid =0 AND bgbudgetcodesfundsource_tran.percentage > 0 " & _
            '   " LEFT JOIN bgbudget_tran ON bgbudgetcodesfundsource_tran.budgettranunkid =  bgbudget_tran.budgettranunkid AND ltbemployee_timesheet.employeeunkid = bgbudget_tran.allocationtranunkid   " & _
            '   " LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid=bgbudget_tran.budgetunkid  " & _
            'Pinkal (16 Jun 2017) -- Start


            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]

            If mblnAllowOverTimeToEmpTimesheet Then
                strQ &= " LEFT JOIN lvholiday_master ON  CONVERT(char(8), lvholiday_master.holidaydate,112) = CONVERT(char(8), ltbemployee_timesheet.activitydate,112)  " & _
                            " LEFT JOIN lvemployee_holiday ON lvemployee_holiday.employeeunkid = tsemptimesheet_approval.employeeunkid AND lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid "
            End If
            'Pinkal (13-Aug-2018) -- End


            strQ &= " LEFT JOIN bgbudgetcodesfundsource_tran ON ltbemployee_timesheet.periodunkid = bgbudgetcodesfundsource_tran.periodunkid " & _
                        " AND ltbemployee_timesheet.activityunkid= bgbudgetcodesfundsource_tran.fundactivityunkid   " & _
                        " LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodesfundsource_tran.budgettranunkid = bgbudgetcodes_tran.budgettranunkid  And ltbemployee_timesheet.employeeunkid = bgbudgetcodes_tran.allocationtranunkid " & _
                        " AND bgbudgetcodes_tran.periodunkid = bgbudgetcodesfundsource_tran.periodunkid	AND bgbudgetcodes_tran.budgetcodestranunkid = bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                        " LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid=bgbudgetcodes_tran.budgetunkid  " & _
                          " LEFT JOIN " & _
                          "( " & _
                          "    SELECT " & _
                          "         stationunkid " & _
                          "        ,deptgroupunkid " & _
                          "        ,departmentunkid " & _
                          "        ,sectiongroupunkid " & _
                          "        ,sectionunkid " & _
                          "        ,unitgroupunkid " & _
                          "        ,unitunkid " & _
                          "        ,teamunkid " & _
                          "        ,classgroupunkid " & _
                          "        ,classunkid " & _
                          "        ,employeeunkid " & _
                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                          "    FROM hremployee_transfer_tran " & _
                          "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
                          " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                          " LEFT JOIN " & _
                          "( " & _
                          "    SELECT " & _
                          "         jobunkid " & _
                          "        ,jobgroupunkid " & _
                          "        ,employeeunkid " & _
                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                          "    FROM hremployee_categorization_tran " & _
                          "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
                          ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                          " JOIN " & _
                          " ( " & _
                          "    SELECT " & _
                          "         gradegroupunkid " & _
                          "        ,gradeunkid " & _
                          "        ,gradelevelunkid " & _
                          "        ,employeeunkid " & _
                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                          "    FROM prsalaryincrement_tran " & _
                          "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
                          " ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 "
            StrQCommonQry = strQ

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            If blnOnlyActive Then
                StrQCondition &= " WHERE tsemptimesheet_approval.isvoid  =  0 "
            End If

            If xEmpTimesheetID > 0 Then
                StrQCondition &= " AND tsemptimesheet_approval.emptimesheetunkid = @emptimesheetunkid "
                objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmpTimesheetID)
            End If

            StrQCondition &= " AND tsapprover_master.isexternalapprover = #ExValue# AND hremployee_master.isapproved = 1 AND iscancel = 0  AND bgbudget_master.allocationbyid = 0 AND bgbudget_master.isdefault = 1 "
            'AND hrapprover_usermapping.userunkid = " & xUserID

            'Pinkal (16 Jun 2017) -- Start [With sohail Guidance as change in Budget Module Table Structure]
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            StrQCondition &= " AND bgbudgetcodesfundsource_tran.isvoid =0 AND bgbudgetcodesfundsource_tran.percentage > 0 AND bgbudgetcodes_tran.isvoid = 0 "
            'Pinkal (16 Jun 2017) -- End

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrFilter
            End If
            'End If

            strQ &= StrQCondition

            strQ = strQ.Replace("#APPR_NAME_VALUE#", " ISNULL(App.firstname,'') + '  ' + ISNULL(App.othername,'') + '  ' + ISNULL(App.surname,'') + ' - ' + ISNULL(tsapproverlevel_master.levelname,'')")
            strQ = strQ.Replace("#APPR_NAME#", " ISNULL(App.firstname,'') + '  ' + ISNULL(App.othername,'') + '  ' + ISNULL(App.surname,'')")
            strQ = strQ.Replace("#APPR_NAME_JOIN#", "LEFT JOIN hremployee_master App ON App.employeeunkid = tsemptimesheet_approval.approveremployeeunkid")
            strQ = strQ.Replace("#ExValue#", "0")

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            'strQ &= " ORDER BY emptimesheetunkid,IsGrp DESC,activitydate "
            strQ &= " ORDER BY ADate, activity_name, IsGrp DESC, emptimesheetunkid "
            'Nilay (07 Feb 2017) -- End

            objDataOperation.AddParameter("@ActivityDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Activity Date"))
            objDataOperation.AddParameter("@Activity", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Activity"))
            objDataOperation.AddParameter("@DateFormat", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDateFormat)
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                Dim StrExJoin As String = String.Empty

                xDateJoinQry = "" : xAdvanceJoinQry = "" : xDateFilterQry = ""

                strQ = StrQCommonQry
                StrExJoin = " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = tsemptimesheet_approval.approveremployeeunkid "
                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    strQ = strQ.Replace("#APPR_NAME_VALUE#", "ISNULL(UEmp.username,'')  + ' - ' + ISNULL(tsapproverlevel_master.levelname,'') ")
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(UEmp.username,'')")
                    strQ = strQ.Replace("#APPR_NAME_JOIN#", StrExJoin)
                Else
                    StrExJoin &= " JOIN " & dr("DName") & "..hremployee_master App on App.employeeunkid = UEmp.employeeunkid AND App.isapproved = 1"
                    strQ = strQ.Replace("#APPR_NAME_JOIN#", StrExJoin)
                    'Nilay (10 Jan 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
                    strQ = strQ.Replace("#APPR_NAME_VALUE#", "CASE WHEN ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') = '' THEN ISNULL(UEmp.username,'')  + ' - ' + ISNULL(tsapproverlevel_master.levelname,'')" & _
                                                                  "ELSE ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') + ' - ' + ISNULL(tsapproverlevel_master.levelname,'') END ")
                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') = '' THEN ISNULL(UEmp.username,'') " & _
                                                                  "ELSE ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') END ")
                    'Nilay (10 Jan 2017) -- End
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                End If

                'strQ = strQ.Replace("#DATA_VALUE#", " ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                '                               ",ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                '                               ",ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                '                               ",ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
                '                               ",ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
                '                               ",ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
                '                               ",ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
                '                               ",ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
                '                               ",ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
                '                               ",ISNULL(Alloc.classunkid,0) AS classunkid " & _
                '                               ",ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
                '                               ",ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
                '                               ",ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
                '                               ",ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
                '                               ",ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid ")



                strQ &= StrQCondition

                strQ = strQ.Replace("#ExValue#", "1")
                strQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                objDataOperation.ClearParameters()

                If xEmpTimesheetID > 0 Then
                    objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmpTimesheetID)
                End If

                objDataOperation.AddParameter("@ActivityDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Activity Date"))
                objDataOperation.AddParameter("@Activity", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Activity"))
                objDataOperation.AddParameter("@DateFormat", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDateFormat)
                objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
                objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
                objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

                dstemp = objDataOperation.ExecQuery(strQ, "ExList")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                Else
                    Dim dtExtList As DataTable = New DataView(dstemp.Tables("ExList"), "isgrp = 0", "", DataViewRowState.CurrentRows).ToTable
                    dstemp.Tables.RemoveAt(0)
                    dstemp.Tables.Add(dtExtList.Copy)
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If

            Next

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            dsList.Tables(0).AsEnumerable().ToList.ForEach(Function(x) UpdateShiftAndTotalActivityHours(x))
            'Nilay (07 Feb 2017) -- End

            Dim dtTable As DataTable

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            'dtTable = New DataView(dsList.Tables(0), "", "emptimesheetunkid DESC, priority ,isgrp DESC", DataViewRowState.CurrentRows).ToTable.Copy
            dtTable = New DataView(dsList.Tables(0), "", "ADate DESC, employeeunkid", DataViewRowState.CurrentRows).ToTable.Copy
            'Nilay (07 Feb 2017) -- End

            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tsemptimesheet_approval) </purpose>
    Public Function Insert(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmptimesheetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProjectcodeunkid.ToString)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapproverunkid.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivity_Hrs.ToString)
            objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecActivity_Hrs.ToString)
            'Pinkal (28-Mar-2018) -- End

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCanceldatetime <> Nothing, mdtCanceldatetime, DBNull.Value))
            objDataOperation.AddParameter("@cancelreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelReason.ToString)

            strQ = "INSERT INTO tsemptimesheet_approval ( " & _
                      "  emptimesheetunkid " & _
                      ", periodunkid " & _
                      ", employeeunkid " & _
                      ", fundsourceunkid " & _
                      ", projectcodeunkid " & _
                      ", activityunkid " & _
                      ", approvaldate " & _
                      ", tsapproverunkid " & _
                      ", approveremployeeunkid " & _
                      ", activity_hrs " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", description " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", iscancel " & _
                      ", canceluserunkid " & _
                      ", canceldatetime" & _
                      ", cancelreason " & _
                    ") VALUES (" & _
                      "  @emptimesheetunkid " & _
                      ", @periodunkid " & _
                      ", @employeeunkid " & _
                      ", @fundsourceunkid " & _
                      ", @projectcodeunkid " & _
                      ", @activityunkid " & _
                      ", @approvaldate " & _
                      ", @tsapproverunkid " & _
                      ", @approveremployeeunkid " & _
                      ", @activity_hrs " & _
                      ", @statusunkid " & _
                      ", @visibleunkid " & _
                      ", @description " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
                      ", @iscancel " & _
                      ", @canceluserunkid " & _
                      ", @canceldatetime" & _
                      ", @cancelreason " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTimesheetapprovalunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertATEmpTimesheet_Approval(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tsemptimesheet_approval) </purpose>
    Public Function Update(ByVal xDatabaseName As String, ByVal xYearID As Integer, ByVal xUserID As Integer _
                                     , ByVal xCompanyID As Integer, ByVal mdtEmpAsonDate As Date _
                                     , ByVal xIncludeInActiveEmployee As Boolean, ByVal mblnIsFromApproval As Boolean _
                                     , ByVal mintPriority As Integer _
                                     , Optional ByVal objDOperation As clsDataOperation = Nothing _
                                     , Optional ByVal mblnIsExternalApprover As Boolean = False) As Boolean
        'If isExist(mstrName, mintTimesheetapprovalunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@timesheetapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTimesheetapprovalunkid.ToString)
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmptimesheetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProjectcodeunkid.ToString)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapproverunkid.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivity_Hrs.ToString)
            objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecActivity_Hrs.ToString)
            'Pinkal (28-Mar-2018) -- End

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCanceldatetime <> Nothing, mdtCanceldatetime, DBNull.Value))
            objDataOperation.AddParameter("@cancelreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelReason.ToString)

            strQ = "UPDATE tsemptimesheet_approval SET " & _
                      "  emptimesheetunkid = @emptimesheetunkid" & _
                      ", periodunkid = @periodunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", fundsourceunkid = @fundsourceunkid" & _
                      ", projectcodeunkid = @projectcodeunkid" & _
                      ", activityunkid = @activityunkid" & _
                      ", approvaldate = @approvaldate" & _
                      ", tsapproverunkid = @tsapproverunkid" & _
                      ", approveremployeeunkid = @approveremployeeunkid" & _
                      ", activity_hrs = @activity_hrs " & _
                      ", statusunkid = @statusunkid" & _
                      ", visibleunkid = @visibleunkid" & _
                      ", description = @description" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason" & _
                      ", iscancel = @iscancel " & _
                      ", canceluserunkid = @canceluserunkid" & _
                      ", canceldatetime = @canceldatetime " & _
                      ", cancelreason = @cancelreason " & _
                      " WHERE isvoid =  0 AND iscancel = 0 "

            'Nilay (02-Jan-2017) -- Start
            'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
            'If mblnIsFromApproval Then
                strQ &= " AND  timesheetapprovalunkid = @timesheetapprovalunkid "
            'Else
            'strQ &= " AND emptimesheetunkid = @emptimesheetunkid "
            'End If
            'Nilay (02-Jan-2017) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertATEmpTimesheet_Approval(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (02-Jan-2017) -- Start
            'Issue #33: Enhancement - Implementing Budget Employee Time Sheet

            'strQ = " SELECT tsemptimesheet_approval.timesheetapprovalunkid,tsemptimesheet_approval.emptimesheetunkid " & _
            '          " ,tsemptimesheet_approval.tsapproverunkid, tsemptimesheet_approval.approveremployeeunkid,tsapproverlevel_master.priority,tsemptimesheet_approval.visibleunkid " & _
            '          " FROM tsemptimesheet_approval  " & _
            '          " JOIN tsapprover_master ON tsapprover_master.tsapproverunkid =  tsemptimesheet_approval.tsapproverunkid " & _
            '          " JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid " & _
            '          " WHERE tsemptimesheet_approval.isvoid = 0 AND tsemptimesheet_approval.emptimesheetunkid = @emptimesheetunkid "

            'objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmptimesheetunkid.ToString)
            'Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

            '    strQ = " UPDATE tsemptimesheet_approval set " & _
            '              " visibleunkid = " & mintStatusunkid & _
            '              ", fundsourceunkid = @fundsourceunkid" & _
            '              ", projectcodeunkid = @projectcodeunkid" & _
            '              ", activityunkid = @activityunkid" & _
            '              ", activity_hrs = @activity_hrs " & _
            '              " WHERE  emptimesheetunkid = @emptimesheetunkid and employeeunkid = @employeeunkid " & _
            '              " AND tsapproverunkid = @tsapproverunkid AND approveremployeeunkid = @approveremployeeunkid AND isvoid = 0 AND iscancel = 0  "

            '    Dim dtVisibility As DataTable = New DataView(dsApprover.Tables(0), "visibleunkid <> 1 AND priority = " & mintPriority, "", DataViewRowState.CurrentRows).ToTable

            '    For i As Integer = 0 To dtVisibility.Rows.Count - 1
            '        objDataOperation.ClearParameters()
            '        objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("emptimesheetunkid").ToString)
            '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            '        objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("tsapproverunkid").ToString)
            '        objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approveremployeeunkid").ToString)
            '        objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            '        objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProjectcodeunkid.ToString)
            '        objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString)
            '        objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivity_Hrs.ToString)
            '        objDataOperation.ExecNonQuery(strQ)

            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If

            '    Next

            '    If mintStatusunkid = 1 Then 'Approved


            '        Dim intMinPriority As Integer = IIf(IsDBNull(dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority)), -1, dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority))

            '        dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

            '        If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

            '            strQ = " UPDATE tsemptimesheet_approval set " & _
            '                      " visibleunkid = 2 " & _
            '                      " WHERE  emptimesheetunkid = @emptimesheetunkid and employeeunkid = @employeeunkid  " & _
            '                      " AND tsapproverunkid = @tsapproverunkid AND approveremployeeunkid = @approveremployeeunkid AND isvoid = 0 AND iscancel = 0  "

            '            For i As Integer = 0 To dtVisibility.Rows.Count - 1
            '                objDataOperation.ClearParameters()
            '                objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("emptimesheetunkid").ToString)
            '                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            '                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("tsapproverunkid").ToString)
            '                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approveremployeeunkid").ToString)
            '                objDataOperation.ExecNonQuery(strQ)

            '                If objDataOperation.ErrorMessage <> "" Then
            '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                    Throw exForce
            '                End If

            '            Next

            '        End If  ' If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then


            '        Dim drRow() As DataRow = dsApprover.Tables(0).Select("priority > " & mintPriority, "")

            '        If drRow.Length > 0 Then

            '            strQ = " UPDATE tsemptimesheet_approval set " & _
            '                        " fundsourceunkid = @fundsourceunkid" & _
            '                        ", projectcodeunkid = @projectcodeunkid" & _
            '                        ", activityunkid = @activityunkid" & _
            '                        ", activity_hrs = @activity_hrs " & _
            '                        " WHERE  emptimesheetunkid = @emptimesheetunkid and employeeunkid = @employeeunkid  " & _
            '                        " AND tsapproverunkid = @tsapproverunkid AND approveremployeeunkid = @approveremployeeunkid AND isvoid = 0 AND iscancel =0 "


            '            For i As Integer = 0 To drRow.Length - 1

            '                objDataOperation.ClearParameters()
            '                objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(i)("emptimesheetunkid").ToString)
            '                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            '                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(i)("tsapproverunkid").ToString)
            '                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(i)("approveremployeeunkid").ToString)
            '                objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            '                objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProjectcodeunkid.ToString)
            '                objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString)
            '                objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivity_Hrs.ToString)
            '                objDataOperation.ExecNonQuery(strQ)

            '                If objDataOperation.ErrorMessage <> "" Then
            '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                    Throw exForce
            '                End If

            '            Next

            '        End If

            '    End If '  If mintStatusunkid = 1 Then 'Approved

            'End If  ' If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then


            'If UpdateEmployeeTimesheetFinalStatus(objDataOperation, xDatabaseName, xYearID, xUserID, xCompanyID, mdtEmpAsonDate, xIncludeInActiveEmployee, mblnIsExternalApprover, mintPriority) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Nilay (02-Jan-2017) -- End


            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tsemptimesheet_approval) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "DELETE FROM tsemptimesheet_approval " & _
                       "WHERE timesheetapprovalunkid = @timesheetapprovalunkid "

            objDataOperation.AddParameter("@timesheetapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@timesheetapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEmployeeId As Integer, ByVal xActivityunkid As Integer, ByVal xActivityDate As Date _
                                     , ByVal xEmptimesheetunkid As Integer, ByVal xApproverID As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
'Nilay (02-Jan-2017) -- [xEmptimesheetunkid]
    
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  timesheetapprovalunkid " & _
                      ", emptimesheetunkid " & _
                      ", periodunkid " & _
                      ", employeeunkid " & _
                      ", fundsourceunkid " & _
                      ", projectcodeunkid " & _
                      ", activityunkid " & _
                      ", approvaldate " & _
                      ", tsapproverunkid " & _
                      ", approveremployeeunkid " & _
                      ", activity_hrs " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", description " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", iscancel " & _
                      ", canceluserunkid " & _
                      ", canceldatetime " & _
                      ", cancelreason " & _
                      " FROM tsemptimesheet_approval " & _
                      " WHERE isvoid = 0  AND CONVERT(char(8),approvaldate,112) = @approvaldate " & _
                      " AND employeeunkid = @employeeunkid AND activityunkid = @activityunkid  " & _
                      " AND iscancel = 0 AND tsapproverunkid = @tsapproverunkid AND emptimesheetunkid = @emptimesheetunkid "
'Nilay (02-Jan-2017) -- [AND emptimesheetunkid = @emptimesheetunkid]

            'If intUnkid > 0 Then
            '    strQ &= " AND timesheetapprovalunkid <> @timesheetapprovalunkid"
            'End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvaldate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xActivityDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xActivityunkid)
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xApproverID)
            'Nilay (02-Jan-2017) -- Start
            'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmptimesheetunkid)
            'Nilay (02-Jan-2017) -- End



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintTimesheetapprovalunkid = CInt(dsList.Tables(0).Rows(0)("timesheetapprovalunkid"))
            End If


            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (ltbemployee_timesheet) </purpose>
    Public Function InsertATEmpTimesheet_Approval(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = " INSERT INTO attsemptimesheet_approval ( " & _
                      " timesheetapprovalunkid " & _
                      ",emptimesheetunkid " & _
                      ",periodunkid " & _
                      ",employeeunkid " & _
                      ",fundsourceunkid " & _
                      ",projectcodeunkid " & _
                      ",activityunkid " & _
                      ",approvaldate " & _
                      ",tsapproverunkid " & _
                      ",approveremployeeunkid " & _
                      ", activity_hrs " & _
                      ",statusunkid " & _
                      ", visibleunkid " & _
                      ",description " & _
                      ", iscancel " & _
                      ", AuditType " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      " @timesheetapprovalunkid " & _
                      ",@emptimesheetunkid " & _
                      ",@periodunkid " & _
                      ",@employeeunkid " & _
                      ",@fundsourceunkid " & _
                      ",@projectcodeunkid " & _
                      ",@activityunkid " & _
                      ",@approvaldate " & _
                      ",@tsapproverunkid " & _
                      ",@approveremployeeunkid " & _
                      ",@activity_hrs " & _
                      ",@statusunkid " & _
                      ",@visibleunkid " & _
                      ",@description " & _
                      ",@iscancel " & _
                      ",@auditType " & _
                      ",@audituserunkid " & _
                      ",GetDate() " & _
                      ",@ip " & _
                      ",@machine_name " & _
                      ",@form_name " & _
                      ",@module_name1 " & _
                      ",@module_name2 " & _
                      ",@module_name3 " & _
                      ",@module_name4 " & _
                      ",@module_name5 " & _
                      ",@isweb " & _
                    "); SELECT @@identity"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@timesheetapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTimesheetapprovalunkid.ToString())
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmptimesheetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProjectcodeunkid.ToString)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapproverunkid.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivity_Hrs.ToString)
            objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecActivity_Hrs.ToString)
            'Pinkal (28-Mar-2018) -- End

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancel.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATEmpTimesheet_Approval; Module Name: " & mstrModuleName)
            Return False
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function UpdateEmployeeTimesheetFinalStatus(ByVal objDataOperation As clsDataOperation, ByVal xDatabaseName As String _
                                                                                   , ByVal xYearID As Integer, ByVal xUserID As Integer, ByVal xCompanyID As Integer _
                                                                                   , ByVal mdtEmpAsonDate As Date, ByVal xIncludeInActiveEmployee As Boolean _
                                                                                   , ByVal mblnIsExternalApprover As Boolean _
                                                                                   , ByVal mintPriority As Integer) As Boolean
        Dim exForce As Exception
        Try
            If mintStatusunkid <> 3 AndAlso mintStatusunkid <> 6 Then  'Rejected And Cancelled

                Dim objApprover As New clstsapprover_master
                Dim dtApprover As DataTable = objApprover.GetEmployeeApprover(xDatabaseName, xYearID, xCompanyID, mdtEmpAsonDate, xIncludeInActiveEmployee, _
                                                                              -1, mintEmployeeunkid, -1, objDataOperation, mblnIsExternalApprover, _
                                                                              "priority > " & mintPriority)

                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count <= 0 Then

                    Dim objEmpTimesheet As New clsBudgetEmp_timesheet
                    objEmpTimesheet._Emptimesheetunkid = mintEmptimesheetunkid
                    'Pinkal (28-Mar-2018) -- Start
                    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                    'objEmpTimesheet._ApprovedActivity_Hrs = mintActivity_Hrs
                    objEmpTimesheet._ApprovedActivity_Hrs = mdecActivity_Hrs
                    'Pinkal (28-Mar-2018) -- End


                    'Pinkal (27-Jun-2020) -- Start
                    'Enhancement NMB -   Working on Employee Signature Report.
                    objEmpTimesheet._Userunkid = xUserID
                    'Pinkal (27-Jun-2020) -- End

                    objEmpTimesheet._Statusunkid = 1
                    objEmpTimesheet._WebClientIP = mstrWebClientIP
                    objEmpTimesheet._WebFormName = mstrWebFormName
                    objEmpTimesheet._WebHostName = mstrWebHostName
                    If objEmpTimesheet.UpdateEmployeeTimesheetStatus(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objEmpTimesheet = Nothing
                End If
                dtApprover.Rows.Clear()
                dtApprover = Nothing

            ElseIf mintStatusunkid = 3 Then 'Rejected

                Dim objEmpTimesheet As New clsBudgetEmp_timesheet

                'Pinkal (13-Apr-2017) -- Start
                'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                'objEmpTimesheet._Emptimesheetunkid = mintEmptimesheetunkid
                objEmpTimesheet._Emptimesheetunkid(objDataOperation) = mintEmptimesheetunkid
                'Pinkal (13-Apr-2017) -- End

                'Pinkal (27-Jun-2020) -- Start
                'Enhancement NMB -   Working on Employee Signature Report.
                objEmpTimesheet._Userunkid = xUserID
                'Pinkal (27-Jun-2020) -- End

                objEmpTimesheet._Statusunkid = 3
                objEmpTimesheet._WebClientIP = mstrWebClientIP
                objEmpTimesheet._WebFormName = mstrWebFormName
                objEmpTimesheet._WebHostName = mstrWebHostName
                If objEmpTimesheet.UpdateEmployeeTimesheetStatus(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Nilay (01 Apr 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes

                'Pinkal (13-Apr-2017) -- Start
                'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                'objEmpTimesheet._Emptimesheetunkid = mintEmptimesheetunkid
                objEmpTimesheet._Emptimesheetunkid(objDataOperation) = mintEmptimesheetunkid
                'Pinkal (13-Apr-2017) -- End

                'Pinkal (27-Jun-2020) -- Start
                'Enhancement NMB -   Working on Employee Signature Report.
                objEmpTimesheet._Userunkid = xUserID
                'Pinkal (27-Jun-2020) -- End

                objEmpTimesheet._Statusunkid = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.PENDING
                objEmpTimesheet._IsSubmitForApproval = False
                objEmpTimesheet._WebClientIP = mstrWebClientIP
                objEmpTimesheet._WebFormName = mstrWebFormName
                objEmpTimesheet._WebHostName = mstrWebHostName
                If objEmpTimesheet.UpdateEmployeeTimesheetStatus(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Nilay (01 Apr 2017) -- End

                objEmpTimesheet = Nothing
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateEmployeeTimesheetFinalStatus; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateGlobalApproval(ByVal xDatabaseName As String, ByVal xYearID As Integer, ByVal xUserID As Integer _
                                                          , ByVal xCompanyID As Integer, ByVal xIncludeInActiveEmployee As Boolean) As Boolean

        'Nilay (02-Jan-2017) -- Start
        'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
        'ByVal mblnIsFromApproval As Boolean , Optional ByVal mblnIsExternalApprover As Boolean = False
        'Nilay (02-Jan-2017) -- End

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintPriority As Integer = -1
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If mdtApprovalData Is Nothing OrElse mdtApprovalData.Rows.Count <= 0 Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            For Each dr As DataRow In mdtApprovalData.Rows

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@timesheetapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("timesheetapprovalunkid").ToString))
                objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("emptimesheetunkid").ToString()))
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid").ToString))
                objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("fundsourceunkid").ToString))
                objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("projectcodeunkid").ToString()))
                objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("activityunkid").ToString()))
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
                'Nilay (02-Jan-2017) -- Start
                'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
                'objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTsapproverunkid.ToString)
                'objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("tsapproverunkid")).ToString)
                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("approveremployeeunkid")).ToString)
                'Nilay (02-Jan-2017) -- End


                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("activity_hrsinMins").ToString))
                objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr("activity_hrsinMins").ToString))
                'Pinkal (28-Mar-2018) -- End

                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
                objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancel.ToString)
                objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
                objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCanceldatetime <> Nothing, mdtCanceldatetime, DBNull.Value))
                objDataOperation.AddParameter("@cancelreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelReason.ToString)

                strQ = "UPDATE tsemptimesheet_approval SET " & _
                          "  emptimesheetunkid = @emptimesheetunkid" & _
                          ", periodunkid = @periodunkid" & _
                          ", employeeunkid = @employeeunkid" & _
                          ", fundsourceunkid = @fundsourceunkid" & _
                          ", projectcodeunkid = @projectcodeunkid" & _
                          ", activityunkid = @activityunkid" & _
                          ", approvaldate = @approvaldate" & _
                          ", tsapproverunkid = @tsapproverunkid" & _
                          ", approveremployeeunkid = @approveremployeeunkid" & _
                          ", activity_hrs = @activity_hrs " & _
                          ", statusunkid = @statusunkid" & _
                          ", visibleunkid = @visibleunkid" & _
                          ", description = @description" & _
                          ", userunkid = @userunkid" & _
                          ", isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voidreason = @voidreason" & _
                          ", iscancel = @iscancel " & _
                          ", canceluserunkid = @canceluserunkid" & _
                          ", canceldatetime = @canceldatetime " & _
                          ", cancelreason = @cancelreason " & _
                          " WHERE isvoid =  0 AND iscancel = 0 "


                'Nilay (02-Jan-2017) -- Start
                'Issue #33: Enhancement - Implementing Budget Employee Time Sheet

                'If mblnIsFromApproval Then
                    strQ &= " AND  timesheetapprovalunkid = @timesheetapprovalunkid "
                'Else
                'strQ &= " AND emptimesheetunkid = @emptimesheetunkid "
                'End If

                'Nilay (02-Jan-2017) -- End

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                _Timesheetapprovalunkid(objDataOperation) = CInt(dr("timesheetapprovalunkid"))

                'Nilay (02-Jan-2017) -- Start
                'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
                mintPriority = CInt(dr("Priority"))
                'Nilay (02-Jan-2017) -- End


                'Pinkal (27-Jun-2020) -- Start
                'Enhancement NMB -   Working on Employee Signature Report.
                mintUserunkid = xUserID
                'Pinkal (27-Jun-2020) -- End

                If InsertATEmpTimesheet_Approval(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                strQ = " SELECT tsemptimesheet_approval.timesheetapprovalunkid,tsemptimesheet_approval.emptimesheetunkid " & _
                          " ,tsemptimesheet_approval.tsapproverunkid, tsemptimesheet_approval.approveremployeeunkid,tsapproverlevel_master.priority,tsemptimesheet_approval.visibleunkid " & _
                          " FROM tsemptimesheet_approval  " & _
                          " JOIN tsapprover_master ON tsapprover_master.tsapproverunkid =  tsemptimesheet_approval.tsapproverunkid " & _
                          " JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid " & _
                          " WHERE tsemptimesheet_approval.isvoid = 0 AND tsemptimesheet_approval.emptimesheetunkid = @emptimesheetunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("emptimesheetunkid").ToString))
                Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                    strQ = " UPDATE tsemptimesheet_approval set " & _
                              " visibleunkid = " & mintStatusunkid & _
                              ", fundsourceunkid = @fundsourceunkid" & _
                              ", projectcodeunkid = @projectcodeunkid" & _
                              ", activityunkid = @activityunkid" & _
                              ", activity_hrs = @activity_hrs " & _
                              " WHERE  emptimesheetunkid = @emptimesheetunkid and employeeunkid = @employeeunkid " & _
                              " AND tsapproverunkid = @tsapproverunkid AND approveremployeeunkid = @approveremployeeunkid AND isvoid = 0 AND iscancel = 0  "

                    Dim dtVisibility As DataTable = New DataView(dsApprover.Tables(0), "visibleunkid <> 1 AND priority = " & mintPriority, "", DataViewRowState.CurrentRows).ToTable

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("emptimesheetunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid").ToString()))
                        objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("tsapproverunkid").ToString)
                        objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approveremployeeunkid").ToString)
                        objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("fundsourceunkid").ToString))
                        objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("projectcodeunkid").ToString()))
                        objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("activityunkid").ToString()))

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("activity_hrsinMins").ToString))
                        objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr("activity_hrsinMins").ToString))
                        'Pinkal (28-Mar-2018) -- End
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                    If mintStatusunkid = 1 Then 'Approved


                        Dim intMinPriority As Integer = IIf(IsDBNull(dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority)), -1, dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority))

                        dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

                        If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                            strQ = " UPDATE tsemptimesheet_approval set " & _
                                      " visibleunkid = 2 " & _
                                      " WHERE  emptimesheetunkid = @emptimesheetunkid and employeeunkid = @employeeunkid  " & _
                                      " AND tsapproverunkid = @tsapproverunkid AND approveremployeeunkid = @approveremployeeunkid AND isvoid = 0 AND iscancel = 0  "

                            For i As Integer = 0 To dtVisibility.Rows.Count - 1
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("emptimesheetunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid").ToString()))
                                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("tsapproverunkid").ToString)
                                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approveremployeeunkid").ToString)
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Next

                        End If  ' If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then


                        Dim drRow() As DataRow = dsApprover.Tables(0).Select("priority > " & mintPriority, "")

                        If drRow.Length > 0 Then

                            strQ = " UPDATE tsemptimesheet_approval set " & _
                                        " fundsourceunkid = @fundsourceunkid" & _
                                        ", projectcodeunkid = @projectcodeunkid" & _
                                        ", activityunkid = @activityunkid" & _
                                        ", activity_hrs = @activity_hrs " & _
                                        " WHERE  emptimesheetunkid = @emptimesheetunkid and employeeunkid = @employeeunkid  " & _
                                        " AND tsapproverunkid = @tsapproverunkid AND approveremployeeunkid = @approveremployeeunkid AND isvoid = 0 AND iscancel =0 "


                            For i As Integer = 0 To drRow.Length - 1

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(i)("emptimesheetunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid").ToString()))
                                objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(i)("tsapproverunkid").ToString)
                                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(i)("approveremployeeunkid").ToString)
                                objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("fundsourceunkid").ToString))
                                objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("projectcodeunkid").ToString()))
                                objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("activityunkid").ToString()))

                                'Pinkal (28-Mar-2018) -- Start
                                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                                'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("activity_hrsinMins").ToString))
                                objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr("activity_hrsinMins").ToString))
                                'Pinkal (28-Mar-2018) -- End

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Next

                        End If

                        'Nilay (01 Apr 2017) -- Start
                        'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    ElseIf mintStatusunkid = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED Then
                        Dim strDescription As String = mstrDescription
                        Dim intStatusID As Integer = mintStatusunkid
                        For Each row As DataRow In dsApprover.Tables(0).Rows
                            strQ = " UPDATE tsemptimesheet_approval set " & _
                                       "  isvoid = 1 " & _
                                       ", voiduserunkid = @voiduserunkid " & _
                                       ", voiddatetime = GetDate() " & _
                                       ", voidreason = @voidreason " & _
                                   " WHERE timesheetapprovalunkid=@timesheetapprovalunkid " & _
                                       "  AND emptimesheetunkid = @emptimesheetunkid " & _
                                       "  AND isvoid = 0 AND iscancel = 0  "

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@timesheetapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(row("timesheetapprovalunkid").ToString))
                            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(row("emptimesheetunkid").ToString))
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDescription)

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            _Timesheetapprovalunkid(objDataOperation) = CInt(row("timesheetapprovalunkid"))
                            _Description = strDescription

                            'Pinkal (27-Jun-2020) -- Start
                            'Enhancement NMB -   Working on Employee Signature Report.
                            _Userunkid = xUserID
                            'Pinkal (27-Jun-2020) -- End


                            If InsertATEmpTimesheet_Approval(objDataOperation, 2) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Next
                        mintStatusunkid = intStatusID
                        'Nilay (01 Apr 2017) -- End

                    End If '  If mintStatusunkid = 1 Then 'Approved

                End If  ' If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then


                'Nilay (02-Jan-2017) -- Start
                'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
                'If UpdateEmployeeTimesheetFinalStatus(objDataOperation, xDatabaseName, xYearID, xUserID, xCompanyID, CDate(dr("activitydate")).Date, xIncludeInActiveEmployee, mblnIsExternalApprover, mintPriority) = False Then
                If UpdateEmployeeTimesheetFinalStatus(objDataOperation, xDatabaseName, xYearID, xUserID, xCompanyID, CDate(dr("activitydate")).Date, xIncludeInActiveEmployee, CBool(dr("isexternalapprover")), mintPriority) = False Then
                    'Nilay (02-Jan-2017) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

       
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateGlobalApproval; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Nilay (07 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
    Public Function ComputeActivityHours(ByVal intEmployeID As Integer, ByVal dtActivityDate As Date, ByVal intPeriodID As Integer, _
                                         Optional ByVal strFilter As String = "") As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim dsActivity As DataSet = Nothing
        Dim exForce As Exception
        Dim intSumActivityHoursInMin As Integer = 0
        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                       "  ltbemployee_timesheet.emptimesheetunkid " & _
                       " ,ISNULL(tsemptimesheet_approval.timesheetapprovalunkid, -1) AS timesheetapprovalunkid " & _
                       " ,tsemptimesheet_approval.emptimesheetunkid AS emptimesheetunkid " & _
                       " ,ltbemployee_timesheet.periodunkid AS periodunkid " & _
                       " ,ltbemployee_timesheet.employeeunkid AS employeeunkid " & _
                       " ,ltbemployee_timesheet.activitydate AS activitydate " & _
                       " ,tsemptimesheet_approval.approvaldate AS approvaldate " & _
                       " ,ltbemployee_timesheet.activityunkid " & _
                       " ,bgfundactivity_tran.activity_name AS activity_name " & _
                       " ,ISNULL(tsemptimesheet_approval.tsapproverunkid, -1) AS tsapproverunkid " & _
                       " ,ISNULL(tsemptimesheet_approval.activity_hrs, ltbemployee_timesheet.activity_hrs) AS activity_hrs " & _
                       " ,ISNULL(tsapproverlevel_master.priority, -1) AS priority " & _
                       " ,ISNULL(tsemptimesheet_approval.statusunkid, -1) AS statusunkid " & _
                       " ,ISNULL(tsemptimesheet_approval.visibleunkid, 2) AS visibleunkid " & _
                       " ,ISNULL(tsapprover_master.isexternalapprover, -1) AS isexternalapprover " & _
                       " ,ltbemployee_timesheet.issubmit_approval " & _
                       " ,CASE WHEN ltbemployee_timesheet.statusunkid=1 THEN 1 ELSE 0 END AS isfinalapproved " & _
                   " FROM ltbemployee_timesheet " & _
                       " LEFT JOIN tsemptimesheet_approval ON ltbemployee_timesheet.emptimesheetunkid = tsemptimesheet_approval.emptimesheetunkid " & _
                       "    AND tsemptimesheet_approval.isvoid = 0 AND tsemptimesheet_approval.visibleunkid <> -1 " & _
                       " LEFT JOIN tsapprover_master ON tsapprover_master.tsapproverunkid = tsemptimesheet_approval.tsapproverunkid " & _
                       "    AND tsapprover_master.isvoid = 0 " & _
                       " LEFT JOIN tsapproverlevel_master ON tsapprover_master.tslevelunkid = tsapproverlevel_master.tslevelunkid " & _
                       " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = ltbemployee_timesheet.activityunkid " & _
                   " WHERE   ltbemployee_timesheet.isvoid=0 " & _
                       " AND ltbemployee_timesheet.statusunkid <> 3 " & _
                       " AND ltbemployee_timesheet.employeeunkid = @employeeunkid " & _
                       " AND CONVERT(CHAR(8),ltbemployee_timesheet.activitydate, 112) = @activitydate " & _
                       " AND ltbemployee_timesheet.periodunkid = @periodunkid "

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            strQ &= " ORDER BY tsemptimesheet_approval.emptimesheetunkid, tsemptimesheet_approval.approvaldate DESC " & _
                   "         ,tsapproverlevel_master.priority DESC, tsemptimesheet_approval.timesheetapprovalunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeID.ToString)
            objDataOperation.AddParameter("@activitydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtActivityDate))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            strQ = ""

            strQ = " SELECT " & _
                   "    activityunkid " & _
                   " FROM ltbemployee_timesheet " & _
                   " WHERE  ltbemployee_timesheet.isvoid=0 " & _
                   "    AND ltbemployee_timesheet.statusunkid <> 3 " & _
                   "    AND ltbemployee_timesheet.employeeunkid=@employeeunkid " & _
                   "    AND CONVERT(CHAR(8),ltbemployee_timesheet.activitydate, 112) = @activitydate " & _
                   "    AND ltbemployee_timesheet.periodunkid=@periodunkid "
            'Nilay (15 Feb 2017) -- [AND ltbemployee_timesheet.statusunkid <> 3]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeID.ToString)
            objDataOperation.AddParameter("@activitydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtActivityDate))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID.ToString)

            dsActivity = objDataOperation.ExecQuery(strQ, "Activity")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsActivity.Tables("Activity").Rows
                Dim dR As DataRow() = dsList.Tables("List").Select("activityunkid=" & CInt(dRow("activityunkid")))
                If dR.Length > 0 Then
                    intSumActivityHoursInMin += CInt(dR(0).Item("activity_hrs"))
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ComputeActivityHours; Module Name: " & mstrModuleName)
        End Try
        Return intSumActivityHoursInMin
    End Function

    Public Function UpdateShiftAndTotalActivityHours(ByVal xRow As DataRow) As Boolean
        Try


            Dim objEmpshift As New clsEmployee_Shift_Tran
            Dim intShiftID As Integer = objEmpshift.GetEmployee_Current_ShiftId(CDate(eZeeDate.convertDate(xRow.Item("ADate").ToString)).Date, _
                                                                                CInt(xRow.Item("employeeunkid")))
            objEmpshift = Nothing

            Dim objShiftTran As New clsshift_tran
            objShiftTran.GetShiftTran(intShiftID)
            Dim row() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(CDate(eZeeDate.convertDate(xRow.Item("ADate").ToString)).Date, FirstDayOfWeek.Sunday) - 1)
            If row.Length > 0 Then
                Dim tShift As TimeSpan = TimeSpan.FromSeconds(CDbl(row(0)("workinghrsinsec")))
                xRow.Item("ShiftHours") = CalculateTime(True, CInt(row(0)("workinghrsinsec"))).ToString("#00.00").Replace(".", ":")
                xRow.Item("ShiftHoursInSec") = CInt(row(0)("workinghrsinsec"))
            End If

            If CBool(xRow.Item("IsGrp")) = False Then
                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

                'Dim intTotalActivityHours As Integer = ComputeActivityHours(CInt(xRow.Item("employeeunkid")), _
                '                                                            CDate(eZeeDate.convertDate(xRow.Item("ADate").ToString)).Date, _
                '                                                            CInt(xRow.Item("periodunkid")))
                'xRow.Item("TotalActivityHours") = CalculateTime(True, intTotalActivityHours * 60).ToString("#00.00").Replace(".", ":")
                'xRow.Item("TotalActivityHoursInMin") = intTotalActivityHours

                Dim mdecTotalActivityHours As Decimal = ComputeActivityHours(CInt(xRow.Item("employeeunkid")), _
                                                                            CDate(eZeeDate.convertDate(xRow.Item("ADate").ToString)).Date, _
                                                                            CInt(xRow.Item("periodunkid")))
                xRow.Item("TotalActivityHours") = CalculateTime(True, mdecTotalActivityHours * 60).ToString("#00.00").Replace(".", ":")
                xRow.Item("TotalActivityHoursInMin") = mdecTotalActivityHours

                'Pinkal (28-Mar-2018) -- End


            End If

            xRow.AcceptChanges()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateShiftAndTotalActivityHours; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function
    'Nilay (07 Feb 2017) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage(mstrModuleName, 1, "Activity Date")
            Language.setMessage(mstrModuleName, 2, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class