﻿'************************************************************************************************************************************
'Class Name : clsTransactionHead.vb
'Purpose    :
'Date       :25/06/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Text

''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsTransactionHead
    Private Shared ReadOnly mstrModuleName As String = "clsTransactionHead"
    Dim objDataOperation As clsDataOperation
    Dim objSimpleSlab As New clsTranheadSlabTran
    Dim objTaxSlab As New clsTranheadInexcessslabTran
    Dim objFormula As New clsTranheadFormulaTran
    Dim objFormulaCurrency As New clsTranheadFormulaCurrencyTran 'Sohail (03 Sep 2012)
    Dim objFormulaLeave As New clsTranheadFormulaLeaveTran 'Sohail (09 Oct 2012)
    Dim objFormulaSlab As New clsTranhead_formula_slab_Tran 'Sohail (12 Apr 2013)
    'Sohail (21 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Dim objFormulaMeasurementUnit As New clsTranhead_formula_measurement_unit_Tran
    Dim objFormulaActivityUnit As New clsTranhead_formula_activity_unit_Tran
    Dim objFormulaMeasurementAmount As New clsTranhead_formula_measurement_amount_Tran
    Dim objFormulaActivityAmount As New clsTranhead_formula_activity_amount_Tran
    'Sohail (21 Jun 2013) -- End
    Dim objFormulaShift As New clsTranheadFormulaShiftTran 'Sohail (29 Oct 2013)
    Dim objFormulaCumulative As New clsTranheadFormulaCumulativeTran 'Sohail (09 Nov 2013)
    Dim objFormulaCMaster As New clsTranheadFormulaCMasterTran 'Sohail (02 Oct 2014)
    Dim objFormulaCRExpense As New clsTranheadFormulaCRExpenseTran 'Sohail (12 Nov 2014)
    Dim objFormulaLoanScheme As New clsTranheadFormulaLoanSchemeTran 'Sohail (24 Oct 2017)

    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintTranheadunkid As Integer
    Private mstrTrnheadcode As String = String.Empty
    Private mstrTrnheadname As String = String.Empty
    Private mintTrnheadtype_Id As Integer
    Private mstrTypeof_id As Integer
    Private mintCalctype_Id As Integer
    Private mintComputeon_Id As Integer
    Private mstrFormula As String = String.Empty
    Private mstrFormulaid As String = String.Empty
    Private mblnIsappearonpayslip As Boolean = True
    Private mblnIsrecurrent As Boolean = True
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrTrnheadname1 As String = String.Empty
    Private mstrTrnheadname2 As String = String.Empty
    Private mblnIstaxable As Boolean 'Sohail (18 Nov 2010)
    Private mblnIstaxrelief As Boolean 'Sohail (19 Nov 2010)
    Private mblnIscumulative As Boolean 'Sohail (23 Apr 2011)
    Private mintReftranheadId As Integer = -1 'Sohail (21 Nov 2011)
    Private mblnIsnoncashbenefit As Boolean 'Sohail (18 May 2013)
    Private mblnIsmonetary As Boolean 'Sohail (18 Jun 2013)
    Private mblnIsactive As Boolean = True 'Sohail (09 Nov 2013)
    Private mintActivityunkid As Integer = 0 'Sohail (17 Sep 2014)
    Private mblnIsbasicsalaryasotherearning As Boolean = False 'Sohail (09 Sep 2017)
    'Sohail (28 Jan 2019) -- Start
    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
    Private mblnIsproratedhead As Boolean = False
    Private mintOldTrnheadtype_Id As Integer
    Private mintOldTypeof_id As Integer
    Private mintOldCalctype_Id As Integer
    Private mintOldComputeon_Id As Integer
    'Sohail (28 Jan 2019) -- End
    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Private mblnIsdefault As Boolean = False
    'Sohail (18 Feb 2019) -- End
    'Hemant (06 Jul 2020) -- Start
    'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
    Private mintCostCenterUnkId As Integer
    'Hemant (06 Jul 2020) -- End
    '******  WHILE ADDING NEW FIELD IN PRTRANSACTIONHEAD_MASTER, PLEASE PASS THAT FIELD TO INSERT SYSTEM GENERATED HEADS QUERY IN CLSMASTERDATA CLASS WITHOUT FAIL

    Private mdtFormula As DataTable
    Private mdtFormulaCurrency As DataTable 'Sohail (03 Sep 2012)
    Private mdtFormulaLeave As DataTable 'Sohail (09 Oct 2012)
    Private mdtFormulaPeriodWiseSlab As DataTable 'Sohail (12 Apr 2013)
    'Sohail (21 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Private mdtFormulaMeasurementUnit As DataTable
    Private mdtFormulaActivityUnit As DataTable
    Private mdtFormulaMeasurementAmount As DataTable
    Private mdtFormulaActivityAmount As DataTable
    'Sohail (21 Jun 2013) -- End
    Private mdtFormulaShift As DataTable 'Sohail (29 Oct 2013)
    Private mdtFormulaCumulative As DataTable 'Sohail (09 Nov 2013)
    Private mdtFormulaCMaster As DataTable 'Sohail (02 Oct 2014)
    Private mdtFormulaCRExpense As DataTable 'Sohail (12 Nov 2014)
    Private mdtFormulaLoanScheme As DataTable 'Sohail (24 Oct 2017)

    Private mstrDatabaseName As String = String.Empty  'Sohail (21 Jul 2012)
    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Private xDataOpr As clsDataOperation = Nothing
    'S.SANDEEP [19 OCT 2016] -- END

    'S.SANDEEP [12-JUL-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
    Private mblnIsPerformanceAppraisalHead As Boolean = False
    'S.SANDEEP [12-JUL-2018] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _FormulaDatasource() As DataTable
        Get
            Return mdtFormula
        End Get
        Set(ByVal value As DataTable)
            mdtFormula = value
        End Set
    End Property

    'Sohail (03 Sep 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _FormulaCurrencyDatasource() As DataTable
        Get
            Return mdtFormulaCurrency
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaCurrency = value
        End Set
    End Property
    'Sohail (03 Sep 2012) -- End

    'Sohail (09 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _FormulaLeaveDatasource() As DataTable
        Get
            Return mdtFormulaLeave
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaLeave = value
        End Set
    End Property
    'Sohail (09 Oct 2012) -- End

    'Sohail (12 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _FormulaPeriodWiseSlabDatasource() As DataTable
        Get
            Return mdtFormulaPeriodWiseSlab
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaPeriodWiseSlab = value
        End Set
    End Property
    'Sohail (12 Apr 2013) -- End

    'Sohail (21 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _FormulaMeasurementUnitDatasource() As DataTable
        Get
            Return mdtFormulaMeasurementUnit
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaMeasurementUnit = value
        End Set
    End Property

    Public Property _FormulaActivityUnitDatasource() As DataTable
        Get
            Return mdtFormulaActivityUnit
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaActivityUnit = value
        End Set
    End Property

    Public Property _FormulaMeasurementAmountDatasource() As DataTable
        Get
            Return mdtFormulaMeasurementAmount
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaMeasurementAmount = value
        End Set
    End Property

    Public Property _FormulaActivityAmountDatasource() As DataTable
        Get
            Return mdtFormulaActivityAmount
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaActivityAmount = value
        End Set
    End Property
    'Sohail (21 Jun 2013) -- End

    'Sohail (29 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _FormulaShiftDatasource() As DataTable
        Get
            Return mdtFormulaShift
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaShift = value
        End Set
    End Property
    'Sohail (29 Oct 2013) -- End

    'Sohail (09 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _FormulaCumulativeDatasource() As DataTable
        Get
            Return mdtFormulaCumulative
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaCumulative = value
        End Set
    End Property
    'Sohail (09 Nov 2013) -- End

    'Sohail (02 Oct 2014) -- Start
    'Enhancement - No Of Active Inactive Dependants RELATION WISE.
    Public Property _FormulaCMasterDatasource() As DataTable
        Get
            Return mdtFormulaCMaster
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaCMaster = value
        End Set
    End Property
    'Sohail (02 Oct 2014) -- End

    'Sohail (12 Nov 2014) -- Start
    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
    Public Property _FormulaCRExpenseDatasource() As DataTable
        Get
            Return mdtFormulaCRExpense
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaCRExpense = value
        End Set
    End Property
    'Sohail (12 Nov 2014) -- End

    'Sohail (24 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
    Public Property _FormulaLoanSchemeDatasource() As DataTable
        Get
            Return mdtFormulaLoanScheme
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaLoanScheme = value
        End Set
    End Property
    'Sohail (24 Oct 2017) -- End

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadunkid(ByVal strDatabaseName As String) As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call GetData()
            Call GetData(strDatabaseName)
            'Sohail (21 Aug 2015) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trnheadcode
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trnheadcode() As String
        Get
            Return mstrTrnheadcode
        End Get
        Set(ByVal value As String)
            mstrTrnheadcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trnheadname
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trnheadname() As String
        Get
            Return mstrTrnheadname
        End Get
        Set(ByVal value As String)
            mstrTrnheadname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trnheadtype_id
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trnheadtype_Id() As Integer
        Get
            Return mintTrnheadtype_Id
        End Get
        Set(ByVal value As Integer)
            mintTrnheadtype_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set typeof_id
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Typeof_id() As Integer
        Get
            Return mstrTypeof_id
        End Get
        Set(ByVal value As Integer)
            mstrTypeof_id = value

        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calctype_id
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Calctype_Id() As Integer
        Get
            Return mintCalctype_Id
        End Get
        Set(ByVal value As Integer)
            mintCalctype_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set computeon_id
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Computeon_Id() As Integer
        Get
            Return mintComputeon_Id
        End Get
        Set(ByVal value As Integer)
            mintComputeon_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formula
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Formula() As String
        Get
            Return mstrFormula
        End Get
        Set(ByVal value As String)
            mstrFormula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formulaid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Formulaid() As String
        Get
            Return mstrFormulaid
        End Get
        Set(ByVal value As String)
            mstrFormulaid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isappearonpayslip
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isappearonpayslip() As Boolean
        Get
            Return mblnIsappearonpayslip
        End Get
        Set(ByVal value As Boolean)
            mblnIsappearonpayslip = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isrecurrent
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isrecurrent() As Boolean
        Get
            Return mblnIsrecurrent
        End Get
        Set(ByVal value As Boolean)
            mblnIsrecurrent = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trnheadname1
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trnheadname1() As String
        Get
            Return mstrTrnheadname1
        End Get
        Set(ByVal value As String)
            mstrTrnheadname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trnheadname2
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trnheadname2() As String
        Get
            Return mstrTrnheadname2
        End Get
        Set(ByVal value As String)
            mstrTrnheadname2 = value
        End Set
    End Property

    'Sohail (18 Nov 2010) -- Start
    ''' <summary>
    ''' Purpose: Get or Set istaxable
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Istaxable() As Boolean
        Get
            Return mblnIstaxable
        End Get
        Set(ByVal value As Boolean)
            mblnIstaxable = value
        End Set
    End Property
    'Sohail (18 Nov 2010) -- End

    'Sohail (19 Nov 2010) -- Start
    ''' <summary>
    ''' Purpose: Get or Set istaxrelief
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Istaxrelief() As Boolean
        Get
            Return mblnIstaxrelief
        End Get
        Set(ByVal value As Boolean)
            mblnIstaxrelief = value
        End Set
    End Property
    'Sohail (19 Nov 2010) -- End

    'Sohail (23 Apr 2011) -- Start
    ''' <summary>
    ''' Purpose: Get or Set iscumulative
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Iscumulative() As Boolean
        Get
            Return mblnIscumulative
        End Get
        Set(ByVal value As Boolean)
            mblnIscumulative = value
        End Set
    End Property
    'Sohail (23 Apr 2011) -- End

    'Sohail (21 Nov 2011) -- Start
    ''' <summary>
    ''' Purpose: Get or Set reftranheadid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _ReftranheadId() As Integer
        Get
            Return mintReftranheadId
        End Get
        Set(ByVal value As Integer)
            mintReftranheadId = value
        End Set
    End Property
    'Sohail (21 Nov 2011) -- End

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set isnoncashbenefit
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isnoncashbenefit() As Boolean
        Get
            Return mblnIsnoncashbenefit
        End Get
        Set(ByVal value As Boolean)
            mblnIsnoncashbenefit = value
        End Set
    End Property
    'Sohail (18 May 2013) -- End

    'Sohail (18 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _Ismonetary() As Boolean
        Get
            Return mblnIsmonetary
        End Get
        Set(ByVal value As Boolean)
            mblnIsmonetary = value
        End Set
    End Property
    'Sohail (18 Jun 2013) -- End

    'Sohail (09 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property
    'Sohail (09 Nov 2013) -- End

    'Sohail (17 Sep 2014) -- Start
    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
    ''' <summary>
    ''' Purpose: Get or Set activityunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Activityunkid() As Integer
        Get
            Return mintActivityunkid
        End Get
        Set(ByVal value As Integer)
            mintActivityunkid = value
        End Set
    End Property
    'Sohail (17 Sep 2014) -- End

    'Sohail (09 Sep 2017) -- Start
    'Enhancement - 69.1 - Option to set transaction head basic salary as other earning.
    Public Property _Isbasicsalaryasotherearning() As Boolean
        Get
            Return mblnIsbasicsalaryasotherearning
        End Get
        Set(ByVal value As Boolean)
            mblnIsbasicsalaryasotherearning = value
        End Set
    End Property
    'Sohail (09 Sep 2017) -- End


    'S.SANDEEP [12-JUL-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
    Public Property _IsPerformanceAppraisalHead() As Boolean
        Get
            Return mblnIsPerformanceAppraisalHead
        End Get
        Set(ByVal value As Boolean)
            mblnIsPerformanceAppraisalHead = value
        End Set
    End Property
    'S.SANDEEP [12-JUL-2018] -- END

    'Sohail (28 Jan 2019) -- Start
    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
    Public Property _Isproratedhead() As Boolean
        Get
            Return mblnIsproratedhead
        End Get
        Set(ByVal value As Boolean)
            mblnIsproratedhead = value
        End Set
    End Property
    'Sohail (28 Jan 2019) -- End

    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Public Property _IsDefault() As Boolean
        Get
            Return mblnIsdefault
        End Get
        Set(ByVal value As Boolean)
            mblnIsdefault = value
        End Set
    End Property
    'Sohail (18 Feb 2019) -- End

    'Hemant (06 Jul 2020) -- Start
    'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
    Public Property _CostCenterUnkId() As Integer
        Get
            Return mintCostCenterUnkId
        End Get
        Set(ByVal value As Integer)
            mintCostCenterUnkId = value
        End Set
    End Property
    'Hemant (06 Jul 2020) -- End

    'Sohail (03 Dec 2020) -- Start
    'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
    Private mdblRoundOffTypeId As Double = 200 '200 := -0.005 <-> 0.005
    Public Property _RoundOffTypeId() As Double
        Get
            Return mdblRoundOffTypeId
        End Get
        Set(ByVal value As Double)
            mdblRoundOffTypeId = value
        End Set
    End Property
    'Sohail (03 Dec 2020) -- End

    'Sohail (11 Dec 2020) -- Start
    'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
    Private mintRoundOffModeId As Integer = enPaymentRoundingType.AUTOMATIC
    Public Property _RoundOffModeId() As Integer
        Get
            Return mintRoundOffModeId
        End Get
        Set(ByVal value As Integer)
            mintRoundOffModeId = value
        End Set
    End Property
    'Sohail (11 Dec 2020) -- End

    'Sohail (08 May 2021) -- Start
    'Scania Kenya Issue : OLD-383 : Wrong payroll computation when deductions are passed as earnings.
    Private mintCompute_priority As Integer = 0 '200 := -0.005 <-> 0.005
    Public Property _Compute_Priority() As Integer
        Get
            Return mintCompute_priority
        End Get
        Set(ByVal value As Integer)
            mintCompute_priority = value
        End Set
    End Property
    'Sohail (08 May 2021) -- End

#End Region

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Other Properties "
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Public WriteOnly Property _xDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'S.SANDEEP [19 OCT 2016] -- END


#End Region
    'Sohail (21 Jul 2012) -- End

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal strDatabaseName As String)
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'objDataOperation = New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END


        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadunkid " & _
              ", trnheadcode " & _
              ", trnheadname " & _
              ", trnheadtype_id " & _
              ", typeof_id " & _
              ", calctype_id " & _
              ", computeon_id " & _
              ", formula " & _
              ", formulaid " & _
              ", isappearonpayslip " & _
              ", isrecurrent " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", trnheadname1 " & _
              ", trnheadname2 " & _
              ", istaxable " & _
              ", istaxrelief " & _
              ", ISNULL(iscumulative,0) as iscumulative  " & _
              ", ISNULL(reftranheadid, -1) AS reftranheadid " & _
              ", ISNULL(isnoncashbenefit, 0) as isnoncashbenefit  " & _
              ", ISNULL(ismonetary, 0) as ismonetary  " & _
              ", ISNULL(isactive, 1) as isactive  " & _
              ", ISNULL(activityunkid, 0) as activityunkid  " & _
              ", ISNULL(isbasicsalaryasotherearning, 0) as isbasicsalaryasotherearning  " & _
              ", ISNULL(isperformance,0) AS isperformance " & _
              ", ISNULL(isproratedhead, 0) AS isproratedhead " & _
              ", ISNULL(isdefault, 0) AS isdefault " & _
              ", ISNULL(default_costcenterunkid, 0) AS default_costcenterunkid " & _
              ", ISNULL(roundofftypeid, 200) AS roundofftypeid " & _
              ", ISNULL(roundoffmodeid, 1) AS roundoffmodeid " & _
              ", ISNULL(compute_priority, 0) AS compute_priority " & _
             "FROM " & mstrDatabaseName & "..prtranhead_master " & _
             "WHERE tranheadunkid = @tranheadunkid "
            'Sohail (08 May 2021) - [compute_priority]
            'Sohail (11 Dec 2020) - [roundoffmodeid]
            'Sohail (03 Dec 2020) - [roundofftypeid]
            'Hemant (06 Jul 2020) -- [default_costcenterunkid]
            'Sohail (18 Feb 2019) - [isdefault]
            'Sohail (28 Jan 2019) - [isproratedhead]
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]
            'Sohail (17 Sep 2014) - [activityunkid]
            'Sohail (09 Nov 2013) - [isactive]
            'Sohail (19 Nov 2010) Changes : istaxrelief field added., 'Sohail (21 Nov 2011) - [reftranheadid]
            'Sohail (18 Jun 2013) - [ismonetary]
            'Sohail (18 May 2013) - [isnoncashbenefit]
            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239} (isperformance) -- END

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))

                mstrTrnheadcode = dtRow.Item("trnheadcode").ToString
                mstrTrnheadname = dtRow.Item("trnheadname").ToString
                mintTrnheadtype_Id = CInt(dtRow.Item("trnheadtype_id"))
                mstrTypeof_id = CInt(dtRow.Item("typeof_id"))
                mintCalctype_Id = CInt(dtRow.Item("calctype_id"))
                mintComputeon_Id = CInt(dtRow.Item("computeon_id"))
                mstrFormula = dtRow.Item("formula").ToString
                mstrFormulaid = dtRow.Item("formulaid").ToString
                mblnIsappearonpayslip = CBool(dtRow.Item("isappearonpayslip"))
                mblnIsrecurrent = CBool(dtRow.Item("isrecurrent"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mstrTrnheadname1 = dtRow.Item("trnheadname1").ToString
                mstrTrnheadname2 = dtRow.Item("trnheadname2").ToString
                mblnIstaxable = CBool(dtRow.Item("istaxable")) 'Sohail (18 Nov 2010)
                mblnIstaxrelief = CBool(dtRow.Item("istaxrelief")) 'Sohail (19 Nov 2010)
                mblnIscumulative = CBool(dtRow.Item("iscumulative")) 'Sohail (23 Apr 2011)
                mintReftranheadId = CInt(dtRow.Item("reftranheadid")) 'Sohail (21 Nov 2011)
                mblnIsnoncashbenefit = CBool(dtRow.Item("isnoncashbenefit")) 'Sohail (18 May 2013)
                mblnIsmonetary = CBool(dtRow.Item("ismonetary"))   'Sohail (18 Jun 2013)
                mblnIsactive = CBool(dtRow.Item("isactive")) 'Sohail (09 Nov 2013)
                mintActivityunkid = CInt(dtRow.Item("activityunkid")) 'Sohail (17 Sep 2014)
                mblnIsbasicsalaryasotherearning = CBool(dtRow.Item("isbasicsalaryasotherearning")) 'Sohail (09 Sep 2017)
                'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
                mblnIsPerformanceAppraisalHead = CBool(dtRow.Item("isperformance"))
                'S.SANDEEP [12-JUL-2018] -- END
                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                mblnIsproratedhead = CBool(dtRow.Item("isproratedhead"))
                mintOldTrnheadtype_Id = CInt(dtRow.Item("trnheadtype_id"))
                mintOldTypeof_id = CInt(dtRow.Item("typeof_id"))
                mintOldCalctype_Id = CInt(dtRow.Item("calctype_id"))
                mintOldComputeon_Id = CInt(dtRow.Item("computeon_id"))
                'Sohail (28 Jan 2019) -- End
                'Sohail (18 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                mblnIsdefault = CBool(dtRow.Item("isdefault"))
                'Sohail (18 Feb 2019) -- End
                'Hemant (06 Jul 2020) -- Start
                'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
                mintCostCenterUnkId = CInt(dtRow.Item("default_costcenterunkid"))
                'Hemant (06 Jul 2020) -- End
                'Sohail (03 Dec 2020) -- Start
                'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                mdblRoundOffTypeId = CDbl(dtRow.Item("roundofftypeid"))
                'Sohail (03 Dec 2020) -- End
                'Sohail (11 Dec 2020) -- Start
                'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
                mintRoundOffModeId = CInt(dtRow.Item("roundoffmodeid"))
                'Sohail (11 Dec 2020) -- End
                'Sohail (08 May 2021) -- Start
                'Scania Kenya Issue : OLD-383 : Wrong payroll computation when deductions are passed as earnings.
                mintCompute_priority = CInt(dtRow.Item("compute_priority"))
                'Sohail (08 May 2021) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Suhail
    '''  Get List of Transaction Head Based on Filter Crieteria
    ''' </summary>
    ''' <purpose>  </purpose>
    ''' <param name="intPerformanceHeadSetting">0 = All Head, 1 = Only Performance Head ,2 = All Head Except Peformance Head</param>
    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal intTranHeadUnkID As Integer = -1 _
                            , Optional ByVal intTranHeadTypeID As Integer = -1, Optional ByVal IsForImport As Boolean = False _
                            , Optional ByVal blnAddEmpContribPayable As Boolean = False _
                            , Optional ByVal intActiveInActive As Integer = enTranHeadActiveInActive.ACTIVE _
                            , Optional ByVal blnExcludeMembershipHeads As Boolean = False _
                            , Optional ByVal strFilter As String = "" _
                            , Optional ByVal blnAddRoundingAdjustment As Boolean = False _
                            , Optional ByVal blnAddPPAHeads As Boolean = False _
                            , Optional ByVal blnAddNetPayRoundingAdjustment As Boolean = False _
                            , Optional ByVal intPerformanceHeadSetting As Integer = 2 _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            , Optional ByVal dtAsOnDate As Date = Nothing _
                            , Optional ByVal strFilterOuter As String = "" _
                            ) As DataSet
        '                   'Sohail (25 Jun 2020) - [dtAsOnDate, strFilterOuter]
        '                   'Sohail (18 Jan 2019) - [xDataOp]
        '                   'Sohail (18 Apr 2016) - [blnAddNetPayRoundingAdjustment]
        '                   'Sohail (17 Sep 2014) - [blnAddPPAHeads]
        '                   'Sohail (21 Mar 2014) - [blnAddRoundingAdjustment]
        '                   'Sohail (10 Dec 2013) - [strFilter]
        '                   'Sohail (02 Aug 2011) - blnAddEmpContribPayable,  'Sohail (15 Feb 2012) - [enActiveInActive]
        '                   'Sohail (29 Oct 2012) - [blnExcludeMembershipHeads]
        '                    S.SANDEEP [12-JUL-2018] -- START {Ref#223|#ARUTI-} (intPerformanceHeadSetting) -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 Jan 2019) -- End

        Try

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            Dim objMaster As New clsMasterData
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'Dim ds As DataSet = objMaster.getComboListForHeadType("HeadType")
            Dim ds As DataSet = objMaster.getComboListForHeadType("HeadType", objDataOperation)
            'Sohail (18 Jan 2019) -- End
            Dim dicHeadType As Dictionary(Of Integer, String) = (From p In ds.Tables("HeadType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'ds = objMaster.getComboListTypeOf("TypeOf", , True)
            ds = objMaster.getComboListTypeOf("TypeOf", , True, , , objDataOperation)
            'Sohail (18 Jan 2019) -- End
            Dim dicTypeOf As Dictionary(Of Integer, String) = (From p In ds.Tables("TypeOf") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'ds = objMaster.getComboListCalcType("CalcType", , True, , True)
            ds = objMaster.getComboListCalcType("CalcType", , True, , True, , objDataOperation)
            'Sohail (18 Jan 2019) -- End
            Dim dicCalcType As Dictionary(Of Integer, String) = (From p In ds.Tables("CalcType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (18 Apr 2016) -- End

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            'Sohail (25 Jun 2020) -- End

            'Pinkal (10-Mar-2011) -- Start

            'strQ = "SELECT " & _
            '    "  tranheadunkid " & _
            '    ", trnheadcode " & _
            '    ", trnheadname " & _
            '    ", trnheadtype_id " & _
            '    ", typeof_id " & _
            '    ", calctype_id " & _
            '    ", computeon_id " & _
            '    ", formula " & _
            '    ", formulaid " & _
            '    ", isappearonpayslip " & _
            '    ", isrecurrent " & _
            '    ", userunkid " & _
            '    ", isvoid " & _
            '    ", voiduserunkid " & _
            '    ", voiddatetime " & _
            '    ", voidreason " & _
            '    ", trnheadname1 " & _
            '    ", trnheadname2 " & _
            '    ", istaxable " & _
            '    ", istaxrelief " & _
            '    " FROM prtranhead_master " & _
            '    " WHERE ISNULL(isvoid,0) = 0 " 'Sohail (19 Nov 2010) Changes : istaxrelief field added.

            'Pinkal (10-Mar-2011) -- End

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            strQ = "SELECT * FROM ( "
            'Sohail (25 Jun 2020) -- End

            strQ &= "SELECT " & _
                "  prtranhead_master.tranheadunkid " & _
                ", prtranhead_master.trnheadcode " & _
                ", prtranhead_master.trnheadname " & _
                ", prtranhead_master.trnheadtype_id " & _
                ", prtranhead_master.typeof_id " & _
                ", prtranhead_master.calctype_id " & _
                ", prtranhead_master.computeon_id " & _
                ", ISNULL(prtranhead_formula_slab_tran.formula, '') AS formula " & _
                ", ISNULL(prtranhead_formula_slab_tran.formulaid, '') AS formulaid " & _
                ", prtranhead_master.isappearonpayslip " & _
                ", prtranhead_master.isrecurrent " & _
                ", prtranhead_master.userunkid " & _
                ", prtranhead_master.isvoid " & _
                ", prtranhead_master.voiduserunkid " & _
                ", prtranhead_master.voiddatetime " & _
                ", prtranhead_master.voidreason " & _
                ", prtranhead_master.trnheadname1 " & _
                ", prtranhead_master.trnheadname2 " & _
                ", prtranhead_master.istaxable " & _
                ", prtranhead_master.istaxrelief " & _
                ", ISNULL(prtranhead_master.iscumulative,0) as iscumulative " & _
                ", ISNULL(prtranhead_master.reftranheadid, -1) as reftranheadid " & _
                ", ISNULL(prtranhead_master.isnoncashbenefit, 0) as isnoncashbenefit " & _
                ", ISNULL(prtranhead_master.ismonetary, 0) as ismonetary " & _
                ", ISNULL(prtranhead_master.isactive, 1) as isactive  " & _
                ", ISNULL(prtranhead_master.activityunkid, 0) as activityunkid " & _
                ", ISNULL(prtranhead_master.isbasicsalaryasotherearning, 0) as isbasicsalaryasotherearning " & _
                ", ISNULL(prtranhead_master.isproratedhead, 0) as isproratedhead " & _
                ", ISNULL(prtranhead_master.isdefault, 0) as isdefault " & _
                ", CASE ISNULL(prtranhead_master.isdefault, 0) WHEN 0 THEN 'No' ELSE 'Yes' END AS isdefaultYesNo " & _
                ", DENSE_RANK() OVER (PARTITION BY prtranhead_master.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC, prtranhead_formula_slab_tran.tranheadformulaslabtranunkid DESC) AS ROWNO " & _
                ", ISNULL(prtranhead_master.default_costcenterunkid, 0) as default_costcenterunkid " & _
                ", ISNULL(prtranhead_master.roundofftypeid, 200) as roundofftypeid " & _
                ", ISNULL(prtranhead_master.roundoffmodeid, 1) as roundoffmodeid " & _
                ", ISNULL(prtranhead_master.compute_priority, 0) as compute_priority "
            'Sohail (08 May 2021) - [compute_priority]
            'Sohail (11 Dec 2020) - [roundoffmodeid]
            'Sohail (03 Dec 2020) - [roundofftypeid]
            'Hemant (06 Jul 2020) -- [default_costcenterunkid]
            'Sohail (25 Jun 2020) - [ROWNO]
            'Sohail (18 Feb 2019) - [isdefault, isdefaultYesNo]
            'Sohail (28 Jan 2019) - [isproratedhead]
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]
            'Sohail (17 Sep 2014) - [activityunkid]
            'Sohail (09 Nov 2013) - [isactive]
            'Sohail (18 Jun 2013) - [ismonetary]
            'Sohail (18 May 2013) - [isnoncashbenefit]

            ' Anjan (05 May 2011)- Start 
            'Issue : If you change any of this series order below please give effect on clsmaster data on related function (getComboListForHeadType,getComboListTypeOf,getComboListCalcType)

            If IsForImport Then
                'Sohail (18 Apr 2016) -- Start
                'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                'strQ &= "	,CASE WHEN trnheadtype_id = 1 THEN @EarningforEmployee " & _
                '            "			  WHEN trnheadtype_id = 2 THEN @DeductionFromEmployee " & _
                '            "			  WHEN trnheadtype_id = 3 THEN @EmployeesStatutoryDeductions " & _
                '            "			  WHEN trnheadtype_id = 4 THEN @EmployersStatutoryContributions " & _
                '            "			  WHEN trnheadtype_id = 5 THEN @Information " & _
                '            "	END AS HeadType " & _
                '            "	,CASE WHEN typeof_id = 1 THEN @Salary " & _
                '            "		      WHEN typeof_id = 2 THEN @Allowance " & _
                '            "		      WHEN typeof_id = 3 THEN @Bonus " & _
                '            "		      WHEN typeof_id = 4 THEN @Commission " & _
                '            "		      WHEN typeof_id = 5 THEN @Benefit " & _
                '            "		      WHEN typeof_id = 6 THEN @OtherEarnings " & _
                '            "		      WHEN typeof_id = 7 THEN @LeaveDeduction " & _
                '            "		      WHEN typeof_id = 8 THEN @OtherDeductionsEmp " & _
                '            "		      WHEN typeof_id = 9 THEN @Taxes " & _
                '            "		      WHEN typeof_id = 10 THEN @TEmployeeStatutoryContributions " & _
                '            "		      WHEN typeof_id = 11 THEN @OtherDeductionsSatutary " & _
                '            "		      WHEN typeof_id = 12 THEN @TEmployersStatutoryContributions " & _
                '            "		      WHEN typeof_id = 13 THEN @TInformation " & _
                '            "		      WHEN typeof_id = " & enTypeOf.PAY_PER_ACTIVITY & " THEN @PAY_PER_ACTIVITY " & _
                '            "	END AS TypeOf  " & _
                '            "  , CASE WHEN calctype_id = 1 THEN @CompValue  " & _
                '            "             WHEN calctype_id = 2 THEN @CompExcessSlab  " & _
                '            "             WHEN calctype_id = 4 THEN @DefinedSalary  " & _
                '            "             WHEN calctype_id = 5 THEN @Attendance  " & _
                '            "             WHEN calctype_id = 7 THEN @FlatRateAll  " & _
                '            "             WHEN calctype_id = 8 THEN @OverTimeHours " & _
                '            "             WHEN calctype_id = 9 THEN @ShortHours  " & _
                '            "             WHEN calctype_id = 10 THEN @NetPay  " & _
                '            "             WHEN calctype_id = 11 THEN @TotalEarning  " & _
                '            "             WHEN calctype_id = 12 THEN @TotalDeduction  "
                ''Sohail (18 Jan 2012) -- Start
                ''TRA - ENHANCEMENT
                'strQ &= "                 WHEN calctype_id = 14 THEN @TaxableEarningTotal  " & _
                '                        " WHEN calctype_id = 15 THEN @NonTaxableEarningTotal  " & _
                '                        " WHEN calctype_id = 16 THEN @NonCashBenefitTotal  " & _
                '                        " WHEN calctype_id = 13 THEN @EMPLOYER_CONTRIBUTION_PAYABLE  " & _
                '                        " WHEN calctype_id = 17 THEN @ROUNDING_ADJUSTMENT  " & _
                '                        " WHEN calctype_id = " & enCalcType.PAY_PER_ACTIVITY & " THEN @PAY_PER_ACTIVITY_CALC  " & _
                '                        " WHEN calctype_id = " & enCalcType.PAY_PER_ACTIVITY_OT & " THEN @PAY_PER_ACTIVITY_OT  " & _
                '                        " WHEN calctype_id = " & enCalcType.PAY_PER_ACTIVITY_PH & " THEN @PAY_PER_ACTIVITY_PH  "
                ''Sohail (21 Mar 2014) - [EMPLOYER_CONTRIBUTION_PAYABLE, ROUNDING_ADJUSTMENT]
                ''Sohail (18 Jan 2012) -- End

                'strQ &= " END AS calctype " & _
                '            "  , CASE WHEN computeon_id = 1 THEN @ComputeFormula  ELSE  '' END AS computeon "
                strQ &= ", CASE prtranhead_master.trnheadtype_id "
                For Each pair In dicHeadType
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                strQ &= " END AS HeadType "

                strQ &= ", CASE prtranhead_master.typeof_id "
                For Each pair In dicTypeOf
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                strQ &= " END AS TypeOf "

                strQ &= ", CASE prtranhead_master.calctype_id "
                For Each pair In dicCalcType
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                strQ &= " END AS calctype "

                strQ &= "  , CASE WHEN prtranhead_master.computeon_id = 1 THEN @ComputeFormula  ELSE  '' END AS computeon "
                'Sohail (18 Apr 2016) -- End

            End If


            strQ &= " FROM prtranhead_master " & _
                    "LEFT JOIN prtranhead_formula_slab_tran ON prtranhead_formula_slab_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "AND prtranhead_formula_slab_tran.isvoid = 0 " & _
                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_slab_tran.periodunkid "
            'Sohail (25 Jun 2020) - [LEFT JOIN prtranhead_formula_slab_tran, LEFT JOIN cfcommon_period_tran]

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            If dtAsOnDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If
            'Sohail (25 Jun 2020) -- End

            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If blnExcludeMembershipHeads = True Then
                strQ += "LEFT JOIN hrmembership_master AS EmpContrib ON EmpContrib.emptranheadunkid = prtranhead_master.tranheadunkid AND EmpContrib.isactive = 1 " & _
                        "LEFT JOIN hrmembership_master AS CoContrib ON CoContrib.cotranheadunkid = prtranhead_master.tranheadunkid AND CoContrib.isactive = 1 "
            End If
            'Sohail (29 Oct 2012) -- End

            'Sohail (15 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            '" WHERE ISNULL(isvoid,0) = 0 "
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'strQ &= " WHERE 1 = 1 "
            strQ &= " WHERE prtranhead_master.isvoid = 0 "

            'Sohail (09 Nov 2013) -- End
            If intActiveInActive = enTranHeadActiveInActive.ACTIVE Then
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                'strQ &= " AND ISNULL(isvoid, 0) = 0 "
                strQ &= " AND ISNULL(prtranhead_master.isactive, 1) = 1 "
                'Sohail (09 Nov 2013) -- End
            ElseIf intActiveInActive = enTranHeadActiveInActive.INACTIVE Then
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                'strQ &= " AND ISNULL(isvoid, 0) = 1 "
                strQ &= " AND ISNULL(prtranhead_master.isactive, 1) = 0 "
                'Sohail (09 Nov 2013) -- End
            End If
            'Sohail (15 Feb 2012) -- End

            If IsForImport Then

                'Sohail (18 Apr 2016) -- Start
                'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                'objDataOperation.AddParameter("@EarningforEmployee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 2, "Earning for Employee"))
                'objDataOperation.AddParameter("@DeductionFromEmployee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 3, "Deduction From Employee"))
                'objDataOperation.AddParameter("@EmployeesStatutoryDeductions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 4, "Employees Statutory Deductions"))
                'objDataOperation.AddParameter("@EmployersStatutoryContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 5, "Employers Statutory Contributions"))
                'objDataOperation.AddParameter("@Information", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 6, "Informational"))
                'objDataOperation.AddParameter("@Salary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 8, "Salary"))
                'objDataOperation.AddParameter("@Allowance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 9, "Allowance"))
                'objDataOperation.AddParameter("@Bonus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 10, "Bonus"))
                'objDataOperation.AddParameter("@Commission", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 11, "Commission"))
                'objDataOperation.AddParameter("@OtherEarnings", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 12, "Other Earnings"))
                'objDataOperation.AddParameter("@Taxes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 13, "Taxes"))
                'objDataOperation.AddParameter("@OtherDeductionsEmp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 14, "Other Deductions"))
                'objDataOperation.AddParameter("@TEmployersStatutoryContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 15, "Employers Contributions"))
                'objDataOperation.AddParameter("@OtherDeductionsSatutary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 16, "Other Deductions"))
                'objDataOperation.AddParameter("@TInformation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 6, "Informational"))
                'objDataOperation.AddParameter("@TEmployeeStatutoryContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 17, "Employee Statutory Contributions"))
                'objDataOperation.AddParameter("@LeaveDeduction", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 18, "Leave Deduction"))
                'objDataOperation.AddParameter("@Benefit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 19, "Benefit"))
                'objDataOperation.AddParameter("@PAY_PER_ACTIVITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 554, "PAY PER ACTIVITY")) 'Sohail (17 Sep 2014)

                'objDataOperation.AddParameter("@CompValue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 21, "As Computed Value"))
                'objDataOperation.AddParameter("@CompExcessSlab", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 22, "As Computed On with In Excess of Tax Slab"))
                'objDataOperation.AddParameter("@DefinedSalary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 23, "Defined Salary"))
                'objDataOperation.AddParameter("@Attendance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 24, "On Attendance"))
                'objDataOperation.AddParameter("@FlatRateAll", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 25, "Flat Rate"))
                'objDataOperation.AddParameter("@OverTimeHours", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 26, "OverTime Hours"))
                'objDataOperation.AddParameter("@ShortHours", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 27, "Short Hours"))
                'objDataOperation.AddParameter("@NetPay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 28, "Net Pay"))
                'objDataOperation.AddParameter("@TotalEarning", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 29, "Total Earning"))
                'objDataOperation.AddParameter("@TotalDeduction", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 30, "Total Deduction"))
                ''Sohail (18 Jan 2012) -- Start
                ''TRA - ENHANCEMENT
                'objDataOperation.AddParameter("@TaxableEarningTotal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Taxable Earning Total"))
                'objDataOperation.AddParameter("@NonTaxableEarningTotal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Non-Taxable Earning Total"))
                ''Sohail (18 Jan 2012) -- End
                'objDataOperation.AddParameter("@NonCashBenefitTotal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 344, "Non-Cash Benefit Total")) 'Sohail (18 May 2013)

                'objDataOperation.AddParameter("@ComputeFormula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 37, "Compute On Specified Formula"))

                ''Sohail (21 Mar 2014) -- Start
                ''Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                'objDataOperation.AddParameter("@EMPLOYER_CONTRIBUTION_PAYABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 345, "Employer Contribution Payable"))
                'objDataOperation.AddParameter("@ROUNDING_ADJUSTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rounding Adjustment"))
                ''Sohail (21 Mar 2014) -- End
                ''Sohail (17 Sep 2014) -- Start
                ''Enhancement - System generated Base, OT and PH heads for PPA Activity.
                'objDataOperation.AddParameter("@PAY_PER_ACTIVITY_CALC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 555, "PAY PER ACTIVITY"))
                'objDataOperation.AddParameter("@PAY_PER_ACTIVITY_OT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 556, "PAY PER ACTIVITY OT"))
                'objDataOperation.AddParameter("@PAY_PER_ACTIVITY_PH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 557, "PAY PER ACTIVITY PH"))
                ''Sohail (17 Sep 2014) -- End
                objDataOperation.AddParameter("@ComputeFormula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 37, "Compute On Specified Formula"))
            End If
            'Sohail (18 Apr 2016) -- End

            'Pinkal (10-Mar-2011) -- End

            If intTranHeadUnkID > 0 Then
                strQ += " AND prtranhead_master.tranheadunkid = @trnheadunkid"
                objDataOperation.AddParameter("@trnheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID.ToString)
            End If
            If intTranHeadTypeID > 0 Then
                strQ += " AND prtranhead_master.trnheadtype_id = @trnheadtype_id"
                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadTypeID.ToString)
            End If

            'Sohail (02 Aug 2011) -- Start
            If blnAddEmpContribPayable = False Then
                strQ += " AND prtranhead_master.calctype_id <> @calctype_id_payable "
                objDataOperation.AddParameter("@calctype_id_payable", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE)
            End If
            'Sohail (02 Aug 2011) -- End

            'Sohail (21 Mar 2014) -- Start
            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            If blnAddRoundingAdjustment = False Then
                strQ += " AND prtranhead_master.calctype_id <> @calctype_id_adjustment "
                objDataOperation.AddParameter("@calctype_id_adjustment", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.ROUNDING_ADJUSTMENT)
            End If
            'Sohail (21 Mar 2014) -- End

            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If blnExcludeMembershipHeads = True Then
                strQ += " AND EmpContrib.membershipunkid IS NULL " & _
                        " AND CoContrib.membershipunkid IS NULL "
            End If
            'Sohail (29 Oct 2012) -- End

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            If blnAddPPAHeads = False Then
                strQ &= " AND prtranhead_master.typeof_id <> @PPA "
                objDataOperation.AddParameter("@PPA", SqlDbType.Int, eZeeDataType.INT_SIZE, enTypeOf.PAY_PER_ACTIVITY)
            End If
            'Sohail (17 Sep 2014) -- End

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            If blnAddNetPayRoundingAdjustment = False Then
                strQ += " AND prtranhead_master.calctype_id <> @calctype_id_netpayadjustment "
                objDataOperation.AddParameter("@calctype_id_netpayadjustment", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.NET_PAY_ROUNDING_ADJUSTMENT)
            End If
            'Sohail (18 Apr 2016) -- End

            'Sohail (10 Dec 2013) -- Start
            'Enhancement
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Sohail (10 Dec 2013) -- End

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            Select Case intPerformanceHeadSetting
                Case 1
                    strQ &= " AND prtranhead_master.isperformance = 1 "
                Case 2
                    strQ &= " AND prtranhead_master.tranheadunkid NOT IN (SELECT tranheadunkid FROM prtranhead_master WHERE ISNULL(isvoid, 0) = 0 AND prtranhead_master.isperformance = 1 ) "
            End Select
            'S.SANDEEP [12-JUL-2018] -- END

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            strQ &= " ) AS A " & _
                    " WHERE 1 = 1 "

            If dtAsOnDate <> Nothing Then
                strQ &= " AND A.ROWNO = 1 "
            End If

            If strFilterOuter.Trim <> "" Then
                strQ &= " " & strFilterOuter & " "
            End If
            'Sohail (25 Jun 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 Jan 2019) -- End
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prtranhead_master) </purpose>
    Public Function Insert(ByVal intCalcType As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal dtTable As DataTable = Nothing, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (28 Jan 2019) - [xDataOp]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        If xDataOp IsNot Nothing Then
            Me._xDataOperation = xDataOp
        End If
        'Sohail (28 Jan 2019) -- End

        'If isExist(mstrTrnheadcode, mstrTrnheadname) Then
        If isExist("", mstrTrnheadname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Transaction Head Name is already defined. Please define new Transaction Head Name.")
            Return False
        End If
        If isExist(mstrTrnheadcode, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Transaction Head Code is already defined. Please define new Transaction Head Code.")
            Return False
        End If

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'Dim objDataOperation As clsDataOperation
        'Sohail (28 Jan 2019) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtFormula As DataTable = Nothing
        Dim dtFormulaCurrency As DataTable = Nothing 'Sohail (03 Sep 2012)
        Dim dtFormulaLeave As DataTable = Nothing 'Sohail (09 Oct 2012)
        Dim dtFormulaPeriodWiseSlab As DataTable 'Sohail (12 Apr 2013)
        'Sohail (21 Jun 2013) -- Start
        'TRA - ENHANCEMENT
        Dim dtFormulaMeasurementUnit As DataTable = Nothing
        Dim dtFormulaActivityUnit As DataTable = Nothing
        Dim dtFormulaMeasurementAmount As DataTable = Nothing
        Dim dtFormulaActivityAmount As DataTable = Nothing
        'Sohail (21 Jun 2013) -- End
        Dim dtFormulaShift As DataTable = Nothing 'Sohail (29 Oct 2013)
        Dim dtFormulaCumulative As DataTable = Nothing   'Sohail (09 Nov 2013)
        Dim dtFormulaCMaster As DataTable = Nothing 'Sohail (02 Oct 2014)
        Dim dtFormulaCRExpense As DataTable = Nothing 'Sohail (12 Nov 2014)
        Dim dtFormulaLoanScheme As DataTable = Nothing 'Sohail (24 Oct 2017)

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            objDataOperation.AddParameter("@trnheadcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadcode.ToString)
            objDataOperation.AddParameter("@trnheadname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname.ToString)
            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrnheadtype_Id.ToString)
            objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrTypeof_id.ToString)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputeon_Id.ToString)
            objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormula.ToString)
            objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormulaid.ToString)
            objDataOperation.AddParameter("@isappearonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsappearonpayslip.ToString)
            objDataOperation.AddParameter("@isrecurrent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsrecurrent.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@trnheadname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname1.ToString)
            objDataOperation.AddParameter("@trnheadname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname2.ToString)
            objDataOperation.AddParameter("@istaxable", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstaxable.ToString) 'Sohail (18 Nov 2010)
            objDataOperation.AddParameter("@istaxrelief", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstaxrelief.ToString) 'Sohail (19 Nov 2010)
            objDataOperation.AddParameter("@iscumulative", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscumulative.ToString) 'Sohail (23 Apr 2011)
            objDataOperation.AddParameter("@reftranheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReftranheadId.ToString) 'Sohail (21 Nov 2011)
            objDataOperation.AddParameter("@isnoncashbenefit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsnoncashbenefit.ToString) 'Sohail (18 May 2013)
            objDataOperation.AddParameter("@ismonetary", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmonetary.ToString) 'Sohail (18 Jun 2013)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString) 'Sohail (09 Nov 2013)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString) 'Sohail (17 Sep 2014)
            objDataOperation.AddParameter("@isbasicsalaryasotherearning", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbasicsalaryasotherearning.ToString) 'Sohail (09 Sep 2017)
            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            objDataOperation.AddParameter("@isperformance", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPerformanceAppraisalHead.ToString)
            'S.SANDEEP [12-JUL-2018] -- END
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            objDataOperation.AddParameter("@isproratedhead", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsproratedhead.ToString)
            'Sohail (28 Jan 2019) -- End
            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdefault.ToString)
            'Sohail (18 Feb 2019) -- End
            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            objDataOperation.AddParameter("@default_costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterUnkId.ToString)
            'Hemant (06 Jul 2020) -- End
            'Sohail (03 Dec 2020) -- Start
            'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
            objDataOperation.AddParameter("@roundofftypeid", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblRoundOffTypeId)
            'Sohail (03 Dec 2020) -- End
            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            objDataOperation.AddParameter("@roundoffmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundOffModeId)
            'Sohail (11 Dec 2020) -- End
            'Sohail (08 May 2021) -- Start
            'Scania Kenya Issue : OLD-383 : Wrong payroll computation when deductions are passed as earnings.
            objDataOperation.AddParameter("@compute_priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompute_priority)
            'Sohail (08 May 2021) -- End

            strQ = "INSERT INTO prtranhead_master ( " & _
              "  trnheadcode " & _
              ", trnheadname " & _
              ", trnheadtype_id " & _
              ", typeof_id " & _
              ", calctype_id " & _
              ", computeon_id " & _
              ", formula " & _
              ", formulaid " & _
              ", isappearonpayslip " & _
              ", isrecurrent " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", trnheadname1 " & _
              ", trnheadname2" & _
              ", istaxable" & _
              ", istaxrelief" & _
              ", iscumulative " & _
              ", reftranheadid " & _
              ", isnoncashbenefit" & _
              ", ismonetary" & _
              ", isactive" & _
              ", activityunkid" & _
              ", isbasicsalaryasotherearning" & _
              ", isperformance " & _
              ", isproratedhead " & _
              ", isdefault " & _
              ", default_costcenterunkid " & _
              ", roundofftypeid " & _
              ", roundoffmodeid " & _
              ", compute_priority " & _
            ") VALUES (" & _
              "  @trnheadcode " & _
              ", @trnheadname " & _
              ", @trnheadtype_id " & _
              ", @typeof_id " & _
              ", @calctype_id " & _
              ", @computeon_id " & _
              ", @formula " & _
              ", @formulaid " & _
              ", @isappearonpayslip " & _
              ", @isrecurrent " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @trnheadname1 " & _
              ", @trnheadname2" & _
              ", @istaxable" & _
              ", @istaxrelief" & _
              ", @iscumulative " & _
              ", @reftranheadid " & _
              ", @isnoncashbenefit" & _
              ", @ismonetary" & _
              ", @isactive" & _
              ", @activityunkid" & _
              ", @isbasicsalaryasotherearning " & _
              ", @isperformance " & _
              ", @isproratedhead " & _
              ", @isdefault " & _
              ", @default_costcenterunkid " & _
              ", @roundofftypeid " & _
              ", @roundoffmodeid " & _
              ", @compute_priority " & _
            "); SELECT @@identity"
            'Sohail (08 May 2021) - [compute_priority]
            'Sohail (11 Dec 2020) - [roundoffmodeid]
            'Sohail (03 Dec 2020) - [roundofftypeid]
            'Hemant (06 Jul 2020) --  [default_costcenterunkid]
            'Sohail (18 Feb 2019) - [isdefault]
            'Sohail (28 Jan 2019) - [isproratedhead]
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]
            'Sohail (17 Sep 2014) - [activityunkid]
            'Sohail (09 Nov 2013) - [isactive]
            'Sohail (19 Nov 2010) Changes : istaxrelief field added., 'Sohail (21 Nov 2011) - [reftranheadid]
            'Sohail (18 Jun 2013) - [ismonetary]
            'Sohail (18 May 2013) - [isnoncashbenefit]
            'S.SANDEEP [12-JUL-2018] -- START {Ref#223|#ARUTI-} (isperformance) -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTranheadunkid = dsList.Tables(0).Rows(0).Item(0)

            If intCalcType = enCalcType.AsComputedValue OrElse intCalcType = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse intCalcType = enCalcType.OverTimeHours OrElse intCalcType = enCalcType.ShortHours Then
                dtFormula = Me._FormulaDatasource
                dtFormulaCurrency = Me._FormulaCurrencyDatasource 'Sohail (03 Sep 2012)
                dtFormulaLeave = Me._FormulaLeaveDatasource   'Sohail (09 Oct 2012)
                dtFormulaPeriodWiseSlab = Me._FormulaPeriodWiseSlabDatasource  'Sohail (12 Apr 2013)
                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                dtFormulaMeasurementUnit = Me._FormulaMeasurementUnitDatasource
                dtFormulaActivityUnit = Me._FormulaActivityUnitDatasource
                dtFormulaMeasurementAmount = Me._FormulaMeasurementAmountDatasource
                dtFormulaActivityAmount = Me._FormulaActivityAmountDatasource
                'Sohail (21 Jun 2013) -- End
                dtFormulaShift = Me._FormulaShiftDatasource  'Sohail (29 Oct 2013)
                dtFormulaCumulative = Me._FormulaCumulativeDatasource 'Sohail (09 Nov 2013)
                dtFormulaCMaster = Me._FormulaCMasterDatasource
                dtFormulaCRExpense = Me._FormulaCRExpenseDatasource 'Sohail (12 Nov 2014)
                dtFormulaLoanScheme = Me._FormulaLoanSchemeDatasource 'Sohail (24 Oct 2017)

                'Pinkal (10-Mar-2011) -- Start
                'If dtFormula.Rows.Count > 0 Then
                If dtFormula IsNot Nothing AndAlso dtFormula.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormula._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormula._Formulatranheadunkid = mintTranheadunkid
                    objFormula._Datasource = dtFormula
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormula.InsertDelete(0, 0, ) = False Then
                    If objFormula.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaCurrency IsNot Nothing AndAlso dtFormulaCurrency.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaCurrency._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaCurrency._Formulatranheadunkid = mintTranheadunkid
                    objFormulaCurrency._Datasource = dtFormulaCurrency
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaCurrency.InsertDelete(0, 0) = False Then
                    If objFormulaCurrency.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (03 Sep 2012) -- End
                'Sohail (09 Oct 2012) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaLeave IsNot Nothing AndAlso dtFormulaLeave.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaLeave._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaLeave._Formulatranheadunkid = mintTranheadunkid
                    objFormulaLeave._Datasource = dtFormulaLeave
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaLeave.InsertDelete(0, 0) = False Then
                    If objFormulaLeave.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (09 Oct 2012) -- End

                'Sohail (12 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaPeriodWiseSlab IsNot Nothing AndAlso dtFormulaPeriodWiseSlab.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaSlab._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaSlab._Tranheadunkid = mintTranheadunkid
                    objFormulaSlab._Datasource = dtFormulaPeriodWiseSlab
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaSlab.InserUpdateDelete() = False Then
                    If objFormulaSlab.InserUpdateDelete(mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (12 Apr 2013) -- End

                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaMeasurementUnit IsNot Nothing AndAlso dtFormulaMeasurementUnit.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaMeasurementUnit._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaMeasurementUnit._Formulatranheadunkid = mintTranheadunkid
                    objFormulaMeasurementUnit._Datasource = dtFormulaMeasurementUnit
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaMeasurementUnit.InsertDelete(0, 0) = False Then
                    If objFormulaMeasurementUnit.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If dtFormulaActivityUnit IsNot Nothing AndAlso dtFormulaActivityUnit.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaActivityUnit._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaActivityUnit._Formulatranheadunkid = mintTranheadunkid
                    objFormulaActivityUnit._Datasource = dtFormulaActivityUnit
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaActivityUnit.InsertDelete(0, 0) = False Then
                    If objFormulaActivityUnit.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If dtFormulaMeasurementAmount IsNot Nothing AndAlso dtFormulaMeasurementAmount.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaMeasurementAmount._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaMeasurementAmount._Formulatranheadunkid = mintTranheadunkid
                    objFormulaMeasurementAmount._Datasource = dtFormulaMeasurementAmount
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaMeasurementAmount.InsertDelete(0, 0) = False Then
                    If objFormulaMeasurementAmount.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If dtFormulaActivityAmount IsNot Nothing AndAlso dtFormulaActivityAmount.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaActivityAmount._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaActivityAmount._Formulatranheadunkid = mintTranheadunkid
                    objFormulaActivityAmount._Datasource = dtFormulaActivityAmount
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaActivityAmount.InsertDelete(0, 0) = False Then
                    If objFormulaActivityAmount.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (21 Jun 2013) -- End

                'Sohail (29 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaShift IsNot Nothing AndAlso dtFormulaShift.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaShift._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaShift._Formulatranheadunkid = mintTranheadunkid
                    objFormulaShift._Datasource = dtFormulaShift
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaShift.InsertDelete(0, 0) = False Then
                    If objFormulaShift.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (29 Oct 2013) -- End

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaCumulative IsNot Nothing AndAlso dtFormulaCumulative.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaCumulative._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaCumulative._Formulatranheadunkid = mintTranheadunkid
                    objFormulaCumulative._Datasource = dtFormulaCumulative
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaCumulative.InsertDelete(0, 0) = False Then
                    If objFormulaCumulative.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (09 Nov 2013) -- End

                'Sohail (02 Oct 2014) -- Start
                'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                If dtFormulaCMaster IsNot Nothing AndAlso dtFormulaCMaster.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaCMaster._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaCMaster._Formulatranheadunkid = mintTranheadunkid
                    objFormulaCMaster._Datasource = dtFormulaCMaster
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaCMaster.InsertDelete(0, 0) = False Then
                    If objFormulaCMaster.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (02 Oct 2014) -- End

                'Sohail (12 Nov 2014) -- Start
                'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                If dtFormulaCRExpense IsNot Nothing AndAlso dtFormulaCRExpense.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaCRExpense._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaCRExpense._Formulatranheadunkid = mintTranheadunkid
                    objFormulaCRExpense._Datasource = dtFormulaCRExpense
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objFormulaCRExpense.InsertDelete(0, 0) = False Then
                    If objFormulaCRExpense.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (12 Nov 2014) -- End

                'Sohail (24 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                If dtFormulaLoanScheme IsNot Nothing AndAlso dtFormulaLoanScheme.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaLoanScheme._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objFormulaLoanScheme._Formulatranheadunkid = mintTranheadunkid
                    objFormulaLoanScheme._Datasource = dtFormulaLoanScheme
                    If objFormulaLoanScheme.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'Sohail (24 Oct 2017) -- End

            End If

            'Pinkal (10-Mar-2011) -- End

            If intCalcType = enCalcType.AsComputedValue OrElse intCalcType = enCalcType.OverTimeHours OrElse intCalcType = enCalcType.ShortHours Then 'As Computed Value,OverTime Hour, Short Hours
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objSimpleSlab._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objSimpleSlab._Tranheadunkid = mintTranheadunkid
                    objSimpleSlab._Datasource = dtTable
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objSimpleSlab._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
                    objSimpleSlab._Userunkid = mintUserunkid
                    'Sohail (21 Aug 2015) -- End

                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    objSimpleSlab._FormulaDatasource = dtFormula
                    objSimpleSlab._FormulaLeaveDatasource = dtFormulaLeave
                    objSimpleSlab._FormulaCurrencyDatasource = dtFormulaCurrency
                    objSimpleSlab._FormulaMeasurementUnitDatasource = dtFormulaMeasurementUnit
                    objSimpleSlab._FormulaMeasurementAmountDatasource = dtFormulaMeasurementAmount
                    objSimpleSlab._FormulaActivityUnitDatasource = dtFormulaActivityUnit
                    objSimpleSlab._FormulaActivityAmountDatasource = dtFormulaActivityAmount
                    'Sohail (24 Sep 2013) -- End
                    objSimpleSlab._FormulaShiftDatasource = dtFormulaShift 'Sohail (29 Oct 2013)
                    objSimpleSlab._FormulaCumulativeDatasource = dtFormulaCumulative 'Sohail (09 Nov 2013)
                    objSimpleSlab._FormulaCMasterDatasource = dtFormulaCMaster 'Sohail (02 Oct 2014)
                    objSimpleSlab._FormulaCRExpenseDatasource = dtFormulaCRExpense 'Sohail (12 Nov 2014)
                    objSimpleSlab._FormulaLoanSchemeDatasource = dtFormulaLoanScheme 'Sohail (24 Oct 2017)

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objSimpleSlab.InserUpdateDeleteSimpleSlab() = False Then
                    If objSimpleSlab.InserUpdateDeleteSimpleSlab(dtCurrentDateAndTime) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            ElseIf intCalcType = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then 'As Computed In Access of Tax Slab Value
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objTaxSlab._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                    objTaxSlab._Tranheadunkid = mintTranheadunkid
                    objTaxSlab._Datasource = dtTable
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objTaxSlab._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
                    objTaxSlab._Userunkid = mintUserunkid
                    'Sohail (21 Aug 2015) -- End
                    If objTaxSlab.InserUpdateDeleteTaxSlab = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            'Sohail (12 Oct 2011) -- Start
            'Purpose : If no child is inserted
            If Not (mintCalctype_Id = enCalcType.AsComputedValue OrElse mintCalctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse mintCalctype_Id = enCalcType.OverTimeHours OrElse mintCalctype_Id = enCalcType.ShortHours) Then
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "", "", -1, 1, 0) = False Then
                    Return False
                End If
            End If
            'Sohail (12 Oct 2011) -- End

            'Sohail (21 Nov 2011) -- Start
            If mintTrnheadtype_Id = enTranHeadType.EmployersStatutoryContributions Then
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@trnheadcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadcode.ToString + " Payable")
                objDataOperation.AddParameter("@trnheadname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname.ToString + " Payable")
                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, enTranHeadType.Informational)
                objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, enTypeOf.Informational)
                objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE)
                objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
                objDataOperation.AddParameter("@isappearonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@isrecurrent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                objDataOperation.AddParameter("@trnheadname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname.ToString + " Payable")
                objDataOperation.AddParameter("@trnheadname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname.ToString + " Payable")
                objDataOperation.AddParameter("@istaxable", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@istaxrelief", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@iscumulative", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@reftranheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
                objDataOperation.AddParameter("@isnoncashbenefit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False) 'Sohail (18 May 2013)
                objDataOperation.AddParameter("@ismonetary", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False) 'Sohail (18 Jun 2013)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString) 'Sohail (09 Nov 2013)
                objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0) 'Sohail (17 Sep 2014)
                objDataOperation.AddParameter("@isbasicsalaryasotherearning", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False) 'Sohail (09 Sep 2017)
                objDataOperation.AddParameter("@isperformance", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False) 'S.SANDEEP [12-JUL-2018] -- START {Ref#223|#ARUTI-} (isperformance) -- END
                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                objDataOperation.AddParameter("@isproratedhead", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                'Sohail (28 Jan 2019) -- End
                'Sohail (18 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                'Sohail (18 Feb 2019) -- End
                'Hemant (06 Jul 2020) -- Start
                'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
                objDataOperation.AddParameter("@default_costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterUnkId.ToString)
                'Hemant (06 Jul 2020) -- End
                'Sohail (03 Dec 2020) -- Start
                'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                objDataOperation.AddParameter("@roundofftypeid", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblRoundOffTypeId)
                'Sohail (03 Dec 2020) -- End
                'Sohail (11 Dec 2020) -- Start
                'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
                objDataOperation.AddParameter("@roundoffmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundOffModeId)
                'Sohail (11 Dec 2020) -- End
                'Scania Kenya Issue : OLD-383 : Wrong payroll computation when deductions are passed as earnings.
                objDataOperation.AddParameter("@compute_priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompute_priority)
                'Sohail (08 May 2021) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintTranheadunkid = dsList.Tables(0).Rows(0).Item(0)

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "", "", -1, 1, 0) = False Then
                    Return False
                End If
            End If
            'Sohail (21 Nov 2011) -- End

            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Sohail (28 Jan 2019) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prtranhead_master) </purpose>
    Public Function Update(ByVal strDatabaseName As String, ByVal intCalcType As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal dtTable As DataTable = Nothing, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByRef isSendNotificationToUser As Boolean = False) As Boolean
        'Sohail (28 Jan 2019) - [xDataOp]
        'Sohail (21 Aug 2015) - [strDatabaseName]

        'Gajanan [1-July-2020] -- [IsSendNotificationToUser]
        'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        If xDataOp IsNot Nothing Then
            Me._xDataOperation = xDataOp
        End If
        'Sohail (28 Jan 2019) -- End

        If isExist(mstrTrnheadcode, "", mintTranheadunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Transaction Head Code is already defined. Please define new Transaction Head Code.")
            Return False
        End If
        If isExist("", mstrTrnheadname, mintTranheadunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Transaction Head Name is already defined. Please define new Transaction Head Name.")
            Return False
        End If

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'Dim objDataOperation As clsDataOperation
        'Sohail (28 Jan 2019) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtFormula As DataTable = Nothing
        Dim dtFormulaCurrency As DataTable = Nothing 'Sohail (03 Sep 2012)
        Dim dtFormulaLeave As DataTable = Nothing 'Sohail (09 Oct 2012)
        Dim dtFormulaPeriodWiseSlab As DataTable 'Sohail (12 Apr 2013)
        'Sohail (21 Jun 2013) -- Start
        'TRA - ENHANCEMENT
        Dim dtFormulaMeasurementUnit As DataTable = Nothing
        Dim dtFormulaActivityUnit As DataTable = Nothing
        Dim dtFormulaMeasurementAmount As DataTable = Nothing
        Dim dtFormulaActivityAmount As DataTable = Nothing
        'Sohail (21 Jun 2013) -- End
        Dim dtFormulaShift As DataTable = Nothing 'Sohail (29 Oct 2013)
        Dim dtFormulaCumulative As DataTable = Nothing 'Sohail (09 Nov 2013)
        Dim dtFormulaCMaster As DataTable = Nothing 'Sohail (02 Oct 2014)
        Dim dtFormulaCRExpense As DataTable = Nothing 'Sohail (12 Nov 2014)
        Dim dtFormulaLoanScheme As DataTable = Nothing 'Sohail (24 Oct 2017)

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try

            'Sohail (22 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'dsList = GetList("Head", mintTranheadunkid)
            'If dsList.Tables("Head").Rows.Count > 0 Then
            'If Not (mintTrnheadtype_Id = CInt(dsList.Tables("Head").Rows(0).Item("trnheadtype_id")) AndAlso mstrTypeof_id = CInt(dsList.Tables("Head").Rows(0).Item("typeof_id")) AndAlso mintCalctype_Id = CInt(dsList.Tables("Head").Rows(0).Item("calctype_id"))) Then
            'If CInt(dsList.Tables("Head").Rows(0).Item("computeon_id")) = enComputeOn.ComputeOnSpecifiedFormula Then
            If Not (mintTrnheadtype_Id = mintOldTrnheadtype_Id AndAlso mstrTypeof_id = mintOldTypeof_id AndAlso mintCalctype_Id = mintOldCalctype_Id) Then
                If mintOldComputeon_Id = enComputeOn.ComputeOnSpecifiedFormula Then
                    'Sohail (28 Jan 2019) -- End
                        Dim objFormula As New clsTranheadFormulaTran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormula._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormula.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormula.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (03 Sep 2012) -- Start
                        'TRA - ENHANCEMENT
                        Dim objFormulaCurrency As New clsTranheadFormulaCurrencyTran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaCurrency._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Jun 2013) -- Start
                        'TRA - ENHANCEMENT
                        'If objFormula.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        '    objDataOperation.ReleaseTransaction(False)
                        '    Return False
                        'End If
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaCurrency.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaCurrency.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (21 Jun 2013) -- End
                        'Sohail (03 Sep 2012) -- End
                        'Sohail (09 Oct 2012) -- Start
                        'TRA - ENHANCEMENT
                        Dim objFormulaLeave As New clsTranheadFormulaLeaveTran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaLeave._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Jun 2013) -- Start
                        'TRA - ENHANCEMENT
                        'If objFormula.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        '    objDataOperation.ReleaseTransaction(False)
                        '    Return False
                        'End If
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaLeave.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaLeave.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (21 Jun 2013) -- End
                        'Sohail (09 Oct 2012) -- End

                        'Sohail (21 Jun 2013) -- Start
                        'TRA - ENHANCEMENT
                        Dim objFormulaMeasurementUnit As New clsTranhead_formula_measurement_unit_Tran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaMeasurementUnit._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaMeasurementUnit.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaMeasurementUnit.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If

                        Dim objFormulaActivityUnit As New clsTranhead_formula_activity_unit_Tran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaActivityUnit._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaActivityUnit.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaActivityUnit.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If

                        Dim objFormulaMeasurementAmount As New clsTranhead_formula_measurement_amount_Tran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaMeasurementAmount._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaMeasurementAmount.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaMeasurementAmount.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If

                        Dim objFormulaActivityAmount As New clsTranhead_formula_activity_amount_Tran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaActivityAmount._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaActivityAmount.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaActivityAmount.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (21 Jun 2013) -- End

                        'Sohail (29 Oct 2013) -- Start
                        'TRA - ENHANCEMENT
                        Dim objFormulaShift As New clsTranheadFormulaShiftTran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaShift._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaShift.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaShift.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (29 Oct 2013) -- End

                        'Sohail (09 Nov 2013) -- Start
                        'TRA - ENHANCEMENT
                        Dim objFormulaCumulative As New clsTranheadFormulaCumulativeTran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaCumulative._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaCumulative.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaCumulative.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (09 Nov 2013) -- End

                        'Sohail (02 Oct 2014) -- Start
                        'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                        Dim objFormulaCMaster As New clsTranheadFormulaCMasterTran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaCMaster._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaCMaster.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaCMaster.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (02 Oct 2014) -- End

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                        Dim objFormulaCRExpense As New clsTranheadFormulaCRExpenseTran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaCRExpense._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaCRExpense.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                        If objFormulaCRExpense.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (12 Nov 2014) -- End

                        'Sohail (24 Oct 2017) -- Start
                        'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                        Dim objFormulaLoanScheme As New clsTranheadFormulaLoanSchemeTran
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objFormulaLoanScheme._DataOperation = objDataOperation
                    'Sohail (28 Jan 2019) -- End
                        If objFormulaLoanScheme.VoidByTranHeadUnkID(mintTranheadunkid, mintUserunkid, dtCurrentDateAndTime, "CALCULATION TYPE CHANGED.") = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (24 Oct 2017) -- End

                    End If
                End If
            'End If 'Sohail (28 Jan 2019)
            'Sohail (22 Feb 2012) -- End
            'Hemant (23 Jun 2020) -- Start
            'ISSUE - The variable name '@voiduserunkid' has already been declared. Variable names must be unique within a query batch or stored procedure. Must declare the scalar variable "@trnheadname1".
            objDataOperation.ClearParameters()
            'Hemant (23 Jun 2020) -- End
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@trnheadcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadcode.ToString)
            objDataOperation.AddParameter("@trnheadname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname.ToString)
            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrnheadtype_Id.ToString)
            objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrTypeof_id.ToString)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputeon_Id.ToString)
            objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormula.ToString)
            objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormulaid.ToString)
            objDataOperation.AddParameter("@isappearonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsappearonpayslip.ToString)
            objDataOperation.AddParameter("@isrecurrent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsrecurrent.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@trnheadname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname1.ToString)
            objDataOperation.AddParameter("@trnheadname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname2.ToString)
            objDataOperation.AddParameter("@istaxable", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstaxable.ToString) 'Sohail (18 Nov 2010)
            objDataOperation.AddParameter("@istaxrelief", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstaxrelief.ToString) 'Sohail (19 Nov 2010)
            objDataOperation.AddParameter("@iscumulative", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscumulative.ToString) 'Sohail (23 Apr 2011)
            objDataOperation.AddParameter("@reftranheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReftranheadId.ToString) 'Sohail (21 Nov 2011)
            objDataOperation.AddParameter("@isnoncashbenefit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsnoncashbenefit.ToString) 'Sohail (18 May 2013)
            objDataOperation.AddParameter("@ismonetary", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmonetary.ToString) 'Sohail (18 Jun 2013)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString) 'Sohail (09 Nov 2013)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString) 'Sohail (17 Sep 2014)
            objDataOperation.AddParameter("@isbasicsalaryasotherearning", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbasicsalaryasotherearning.ToString) 'Sohail (09 Sep 2017)
            objDataOperation.AddParameter("@isperformance", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPerformanceAppraisalHead.ToString) 'S.SANDEEP [12-JUL-2018] -- START {Ref#223|#ARUTI-} (isperformance) -- END
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            objDataOperation.AddParameter("@isproratedhead", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsproratedhead.ToString)
            'Sohail (28 Jan 2019) -- End
            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdefault.ToString)
            'Sohail (18 Feb 2019) -- End
            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            objDataOperation.AddParameter("@default_costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterUnkId.ToString)
            'Hemant (06 Jul 2020) -- End
            'Sohail (03 Dec 2020) -- Start
            'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
            objDataOperation.AddParameter("@roundofftypeid", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblRoundOffTypeId)
            'Sohail (03 Dec 2020) -- End
            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            objDataOperation.AddParameter("@roundoffmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundOffModeId)
            'Sohail (11 Dec 2020) -- End
            'Scania Kenya Issue : OLD-383 : Wrong payroll computation when deductions are passed as earnings.
            objDataOperation.AddParameter("@compute_priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompute_priority)
            'Sohail (08 May 2021) -- End

            strQ = "UPDATE prtranhead_master SET " & _
              "  trnheadcode = @trnheadcode" & _
              ", trnheadname = @trnheadname" & _
              ", trnheadtype_id = @trnheadtype_id" & _
              ", typeof_id = @typeof_id" & _
              ", calctype_id = @calctype_id" & _
              ", computeon_id = @computeon_id" & _
              ", formula = @formula" & _
              ", formulaid = @formulaid" & _
              ", isappearonpayslip = @isappearonpayslip" & _
              ", isrecurrent = @isrecurrent" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", trnheadname1 = @trnheadname1" & _
              ", trnheadname2 = @trnheadname2 " & _
              ", istaxable = @istaxable " & _
              ", istaxrelief = @istaxrelief " & _
              ", iscumulative = @iscumulative " & _
              ", reftranheadid = @reftranheadid " & _
              ", isnoncashbenefit = @isnoncashbenefit " & _
              ", ismonetary = @ismonetary " & _
              ", isactive = @isactive " & _
              ", activityunkid = @activityunkid " & _
              ", isbasicsalaryasotherearning = @isbasicsalaryasotherearning " & _
              ", isperformance = @isperformance  " & _
              ", isproratedhead = @isproratedhead  " & _
              ", isdefault = @isdefault  " & _
              ", default_costcenterunkid = @default_costcenterunkid " & _
              ", roundofftypeid = @roundofftypeid " & _
              ", roundoffmodeid = @roundoffmodeid " & _
              ", compute_priority = @compute_priority " & _
            "WHERE tranheadunkid = @tranheadunkid "
            'Sohail (08 May 2021) - [compute_priority]
            'Sohail (11 Dec 2020) - [roundoffmodeid]
            'Sohail (03 Dec 2020) - [roundofftypeid]
            'Hemant (06 Jul 2020) -- [default_costcenterunkid]
            'Sohail (18 Feb 2019) - [isdefault]
            'Sohail (28 Jan 2019) - [isproratedhead]
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]
            'Sohail (17 Sep 2014) - [activityunkid]
            'Sohail (09 Nov 2013) - [isactive]
            'Sohail (19 Nov 2010) Changes : istaxrelief field added., 'Sohail (21 Nov 2011) - [reftranheadid]
            'Sohail (18 Jun 2013) - [ismonetary]
            'Sohail (18 May 2013) - [isnoncashbenefit]
            'S.SANDEEP [12-JUL-2018] -- START {Ref#223|#ARUTI-} (isperformance) -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnToInsert As Boolean

            If intCalcType = enCalcType.AsComputedValue OrElse intCalcType = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse intCalcType = enCalcType.OverTimeHours OrElse intCalcType = enCalcType.ShortHours Then
                dtFormula = Me._FormulaDatasource
                dtFormulaCurrency = Me._FormulaCurrencyDatasource 'Sohail (03 Sep 2012)
                dtFormulaLeave = Me._FormulaLeaveDatasource 'Sohail (09 Oct 2012)
                dtFormulaPeriodWiseSlab = Me._FormulaPeriodWiseSlabDatasource 'Sohail (12 Apr 2013)
                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                dtFormulaMeasurementUnit = Me._FormulaMeasurementUnitDatasource
                dtFormulaActivityUnit = Me._FormulaActivityUnitDatasource
                dtFormulaMeasurementAmount = Me._FormulaMeasurementAmountDatasource
                dtFormulaActivityAmount = Me._FormulaActivityAmountDatasource
                'Sohail (21 Jun 2013) -- End
                dtFormulaShift = Me._FormulaShiftDatasource 'Sohail (29 Oct 2013)
                dtFormulaCumulative = Me._FormulaCumulativeDatasource 'Sohail (09 Nov 2013)
                dtFormulaCMaster = Me._FormulaCMasterDatasource 'Sohail (02 Oct 2014)
                dtFormulaCRExpense = Me._FormulaCRExpenseDatasource 'Sohail (12 Nov 2014)
                dtFormulaLoanScheme = Me._FormulaLoanSchemeDatasource 'Sohail (24 Oct 2017)

                If dtFormula.Rows.Count > 0 Then
                    'Sohail (12 Oct 2011) -- Start
                    Dim dt() As DataRow = dtFormula.Select("AUD=''")
                    If dt.Length = dtFormula.Rows.Count Then 'Sohail (22 Feb 2012)
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_formula_tran", "tranheadformulatranunkid", -1, 2, 0) = False Then
                        '    Return False
                        'End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormula._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormula._Formulatranheadunkid = mintTranheadunkid
                        objFormula._Datasource = dtFormula
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormula.InsertDelete(0, 0) = False Then
                        If objFormula.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                    'Sohail (12 Oct 2011) -- Start
                End If

                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaCurrency.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaCurrency.Select("AUD=''")
                    If dt.Length = dtFormulaCurrency.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaCurrency._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaCurrency._Formulatranheadunkid = mintTranheadunkid
                        objFormulaCurrency._Datasource = dtFormulaCurrency
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaCurrency.InsertDelete(0, 0) = False Then
                        If objFormulaCurrency.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If
                'Sohail (03 Sep 2012) -- End

                'Sohail (09 Oct 2012) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaLeave.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaLeave.Select("AUD=''")
                    If dt.Length = dtFormulaLeave.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaLeave._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaLeave._Formulatranheadunkid = mintTranheadunkid
                        objFormulaLeave._Datasource = dtFormulaLeave
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaLeave.InsertDelete(0, 0) = False Then
                        If objFormulaLeave.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If
                'Sohail (09 Oct 2012) -- End

                'Sohail (12 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaPeriodWiseSlab.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaPeriodWiseSlab.Select("AUD=''")
                    If dt.Length = dtFormulaPeriodWiseSlab.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaSlab._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaSlab._Tranheadunkid = mintTranheadunkid
                        objFormulaSlab._Datasource = dtFormulaPeriodWiseSlab
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaSlab.InserUpdateDelete() = False Then
                        If objFormulaSlab.InserUpdateDelete(mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If
                'Sohail (12 Apr 2013) -- End

                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaMeasurementUnit.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaMeasurementUnit.Select("AUD=''")
                    If dt.Length = dtFormulaMeasurementUnit.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaMeasurementUnit._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaMeasurementUnit._Formulatranheadunkid = mintTranheadunkid
                        objFormulaMeasurementUnit._Datasource = dtFormulaMeasurementUnit
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaMeasurementUnit.InsertDelete(0, 0) = False Then
                        If objFormulaMeasurementUnit.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If

                If dtFormulaActivityUnit.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaActivityUnit.Select("AUD=''")
                    If dt.Length = dtFormulaActivityUnit.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaActivityUnit._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaActivityUnit._Formulatranheadunkid = mintTranheadunkid
                        objFormulaActivityUnit._Datasource = dtFormulaActivityUnit
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaActivityUnit.InsertDelete(0, 0) = False Then
                        If objFormulaActivityUnit.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If

                If dtFormulaMeasurementAmount.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaMeasurementAmount.Select("AUD=''")
                    If dt.Length = dtFormulaMeasurementAmount.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaMeasurementAmount._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaMeasurementAmount._Formulatranheadunkid = mintTranheadunkid
                        objFormulaMeasurementAmount._Datasource = dtFormulaMeasurementAmount
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaMeasurementAmount.InsertDelete(0, 0) = False Then
                        If objFormulaMeasurementAmount.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If

                If dtFormulaActivityAmount.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaActivityAmount.Select("AUD=''")
                    If dt.Length = dtFormulaActivityAmount.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaActivityAmount._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaActivityAmount._Formulatranheadunkid = mintTranheadunkid
                        objFormulaActivityAmount._Datasource = dtFormulaActivityAmount
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaActivityAmount.InsertDelete(0, 0) = False Then
                        If objFormulaActivityAmount.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If
                'Sohail (21 Jun 2013) -- End

                'Sohail (29 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaShift.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaShift.Select("AUD=''")
                    If dt.Length = dtFormulaShift.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaShift._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaShift._Formulatranheadunkid = mintTranheadunkid
                        objFormulaShift._Datasource = dtFormulaShift
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaShift.InsertDelete(0, 0) = False Then
                        If objFormulaShift.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If
                'Sohail (29 Oct 2013) -- End

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If dtFormulaCumulative.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaCumulative.Select("AUD=''")
                    If dt.Length = dtFormulaCumulative.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaCumulative._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaCumulative._Formulatranheadunkid = mintTranheadunkid
                        objFormulaCumulative._Datasource = dtFormulaCumulative
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaCumulative.InsertDelete(0, 0) = False Then
                        If objFormulaCumulative.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If
                'Sohail (09 Nov 2013) -- End

                'Sohail (02 Oct 2014) -- Start
                'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                If dtFormulaCMaster.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaCMaster.Select("AUD=''")
                    If dt.Length = dtFormulaCMaster.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaCMaster._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaCMaster._Formulatranheadunkid = mintTranheadunkid
                        objFormulaCMaster._Datasource = dtFormulaCMaster
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaCMaster.InsertDelete(0, 0) = False Then
                        If objFormulaCMaster.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If
                'Sohail (02 Oct 2014) -- End

                'Sohail (12 Nov 2014) -- Start
                'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                If dtFormulaCRExpense.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaCRExpense.Select("AUD=''")
                    If dt.Length = dtFormulaCRExpense.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaCRExpense._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaCRExpense._Formulatranheadunkid = mintTranheadunkid
                        objFormulaCRExpense._Datasource = dtFormulaCRExpense
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objFormulaCRExpense.InsertDelete(0, 0) = False Then
                        If objFormulaCRExpense.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If
                'Sohail (12 Nov 2014) -- End

                'Sohail (24 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                If dtFormulaLoanScheme.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtFormulaLoanScheme.Select("AUD=''")
                    If dt.Length = dtFormulaLoanScheme.Rows.Count Then
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                            'Sohail (28 Jan 2019) -- End
                            blnToInsert = True
                        End If
                    Else
                        'Sohail (28 Jan 2019) -- Start
                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                        objFormulaLoanScheme._DataOperation = objDataOperation
                        'Sohail (28 Jan 2019) -- End
                        objFormulaLoanScheme._Formulatranheadunkid = mintTranheadunkid
                        objFormulaLoanScheme._Datasource = dtFormulaLoanScheme
                        If objFormulaLoanScheme.InsertDelete(0, 0, mintUserunkid, dtCurrentDateAndTime) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        blnToInsert = False
                    End If
                End If
                'Sohail (24 Oct 2017) -- End


                If intCalcType = enCalcType.AsComputedValue OrElse intCalcType = enCalcType.OverTimeHours OrElse intCalcType = enCalcType.ShortHours Then 'As Computed Value,OverTime Hour, Short Hours
                    If dtTable IsNot Nothing And dtTable.Rows.Count > 0 Then
                        'Sohail (12 Oct 2011) -- Start
                        Dim dt() As DataRow = dtTable.Select("AUD=''")
                        If dt.Length = dtTable.Rows.Count Then
                            'Sohail (28 Jan 2019) -- Start
                            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                            'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                            If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                                'Sohail (28 Jan 2019) -- End
                                blnToInsert = True
                            End If
                            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_slab_tran", "tranheadslabtranunkid", -1, 2, 0) = False Then
                            '    Return False
                            'End If
                        Else
                            'Sohail (28 Jan 2019) -- Start
                            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                            objSimpleSlab._DataOperation = objDataOperation
                            'Sohail (28 Jan 2019) -- End
                            objSimpleSlab._Tranheadunkid = mintTranheadunkid
                            objSimpleSlab._Datasource = dtTable
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objSimpleSlab._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
                            objSimpleSlab._Userunkid = mintUserunkid
                            'Sohail (21 Aug 2015) -- End

                            'Sohail (24 Sep 2013) -- Start
                            'TRA - ENHANCEMENT
                            objSimpleSlab._FormulaDatasource = dtFormula
                            objSimpleSlab._FormulaLeaveDatasource = dtFormulaLeave
                            objSimpleSlab._FormulaCurrencyDatasource = dtFormulaCurrency
                            objSimpleSlab._FormulaMeasurementUnitDatasource = dtFormulaMeasurementUnit
                            objSimpleSlab._FormulaMeasurementAmountDatasource = dtFormulaMeasurementAmount
                            objSimpleSlab._FormulaActivityUnitDatasource = dtFormulaActivityUnit
                            objSimpleSlab._FormulaActivityAmountDatasource = dtFormulaActivityAmount
                            'Sohail (24 Sep 2013) -- End
                            objSimpleSlab._FormulaShiftDatasource = dtFormulaShift 'Sohail (29 Oct 2013)
                            objSimpleSlab._FormulaCumulativeDatasource = dtFormulaCumulative 'Sohail (09 Nov 2013)
                            objSimpleSlab._FormulaCMasterDatasource = dtFormulaCMaster 'Sohail (02 Oct 2014)
                            objSimpleSlab._FormulaCRExpenseDatasource = dtFormulaCRExpense 'Sohail (12 Nov 2014)
                            objSimpleSlab._FormulaLoanSchemeDatasource = dtFormulaLoanScheme 'Sohail (24 Oct 2017)

                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'If objSimpleSlab.InserUpdateDeleteSimpleSlab() = False Then
                            If objSimpleSlab.InserUpdateDeleteSimpleSlab(dtCurrentDateAndTime) = False Then
                                'Sohail (21 Aug 2015) -- End
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                            blnToInsert = False
                        End If
                        'Sohail (12 Oct 2011) -- End
                    End If
                ElseIf intCalcType = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then 'As Computed In Access of Tax Slab Value
                    If dtTable IsNot Nothing And dtTable.Rows.Count > 0 Then
                        If dtTable IsNot Nothing And dtTable.Rows.Count > 0 Then
                            'Sohail (12 Oct 2011) -- Start
                            Dim dt() As DataRow = dtTable.Select("AUD=''")
                            If dt.Length = dtTable.Rows.Count Then
                                'Sohail (28 Jan 2019) -- Start
                                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2) Then
                                If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
                                    'Sohail (28 Jan 2019) -- End
                                    blnToInsert = True
                                End If
                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_inexcessslab_tran", "tranheadtaxslabtranunkid", -1, 2, 0) = False Then
                                '    Return False
                                'End If
                            Else
                                'Sohail (28 Jan 2019) -- Start
                                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                objTaxSlab._DataOperation = objDataOperation
                                'Sohail (28 Jan 2019) -- End
                                objTaxSlab._Tranheadunkid = mintTranheadunkid
                                objTaxSlab._Datasource = dtTable
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'objTaxSlab._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
                                objTaxSlab._Userunkid = mintUserunkid
                                'Sohail (21 Aug 2015) -- End
                                If objTaxSlab.InserUpdateDeleteTaxSlab = False Then
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If
                                blnToInsert = False
                            End If
                        End If
                    End If
                End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If UpdateOtherTables() = False Then
                'Sohail (14 Nov 2017) -- Start
                'Issue - 70.1 - Previous transaction user id was passed to current period transaction.
                'If UpdateOtherTables(strDatabaseName, dtCurrentDateAndTime) = False Then

                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                'If UpdateOtherTables(strDatabaseName, dtCurrentDateAndTime, mintUserunkid) = False Then
                If UpdateOtherTables(strDatabaseName, dtCurrentDateAndTime, mintUserunkid, objDataOperation) = False Then
                    'Sohail (28 Jan 2019) -- End
                    'Sohail (14 Nov 2017) -- End
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            'Sohail (12 Oct 2011) -- Start
            'Purpose : If no child is inserted
            If Not (mintCalctype_Id = enCalcType.AsComputedValue OrElse mintCalctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse mintCalctype_Id = enCalcType.OverTimeHours OrElse mintCalctype_Id = enCalcType.ShortHours) _
                AndAlso clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", mintTranheadunkid, "tranheadunkid", 2, objDataOperation) Then
               'Sohail (28 Jan 2019) - [objDataOperation]
               blnToInsert = True
            End If
            If blnToInsert = True Then

                'Gajanan [1-July-2020] -- Start
                'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
                isSendNotificationToUser = True
                'Gajanan [1-July-2020] -- End

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "", "", -1, 2, 0) = False Then
                    Return False
                End If
            End If
            'Sohail (12 Oct 2011) -- End

            'Sohail (21 Nov 2011) -- Start
            If mintTrnheadtype_Id = enTranHeadType.EmployersStatutoryContributions Then
                objDataOperation.ClearParameters()

                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                Me._xDataOperation = objDataOperation
                'Sohail (28 Jan 2019) -- End
                Dim intPayableHeadUnkId As Integer = GetEmpContribPayableHeadID(mintTranheadunkid)
                If intPayableHeadUnkId > 0 Then
                    'Hemant (09 Apr 2019) -- Start
                    objDataOperation.ClearParameters()
                    'Hemant (09 Apr 2019) -- End
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayableHeadUnkId.ToString)
                    objDataOperation.AddParameter("@trnheadcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadcode.ToString + " Payable")
                    objDataOperation.AddParameter("@trnheadname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname.ToString + " Payable")
                    objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, enTranHeadType.Informational)
                    objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, enTypeOf.Informational)
                    objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE)
                    objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                    objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
                    objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
                    objDataOperation.AddParameter("@isappearonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@isrecurrent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                    objDataOperation.AddParameter("@trnheadname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname.ToString + " Payable")
                    objDataOperation.AddParameter("@trnheadname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrnheadname.ToString + " Payable")
                    objDataOperation.AddParameter("@istaxable", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@istaxrelief", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@iscumulative", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@reftranheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
                    objDataOperation.AddParameter("@isnoncashbenefit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False) 'Sohail (18 May 2013)
                    objDataOperation.AddParameter("@ismonetary", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)  'Sohail (18 Jun 2013)
                    objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString) 'Sohail (09 Nov 2013)
                    objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0) 'Sohail (17 Sep 2014)
                    objDataOperation.AddParameter("@isbasicsalaryasotherearning", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False) 'Sohail (09 Sep 2017)
                    objDataOperation.AddParameter("@isperformance", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False) 'S.SANDEEP [12-JUL-2018] -- START {Ref#223|#ARUTI-} (isperformance) -- END
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    objDataOperation.AddParameter("@isproratedhead", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    'Sohail (28 Jan 2019) -- End
                    'Sohail (18 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                    objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    'Sohail (18 Feb 2019) -- End
                    'Hemant (06 Jul 2020) -- Start
                    'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
                    objDataOperation.AddParameter("@default_costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterUnkId.ToString)
                    'Hemant (06 Jul 2020) -- End
                    'Sohail (03 Dec 2020) -- Start
                    'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                    objDataOperation.AddParameter("@roundofftypeid", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblRoundOffTypeId)
                    'Sohail (03 Dec 2020) -- End
                    'Sohail (11 Dec 2020) -- Start
                    'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
                    objDataOperation.AddParameter("@roundoffmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundOffModeId)
                    'Sohail (11 Dec 2020) -- End
                    'Scania Kenya Issue : OLD-383 : Wrong payroll computation when deductions are passed as earnings.
                    objDataOperation.AddParameter("@compute_priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompute_priority)
                    'Sohail (08 May 2021) -- End

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", intPayableHeadUnkId, "tranheadunkid", 2) Then
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prtranhead_master", intPayableHeadUnkId, "tranheadunkid", 2, objDataOperation) Then
                        'Sohail (28 Jan 2019) -- End
        		        'Gajanan [1-July-2020] -- Start
                		'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
                        isSendNotificationToUser = True
		                'Gajanan [1-July-2020] -- Start

                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intPayableHeadUnkId, "", "", -1, 2, 0) = False Then
                            Return False
                        End If
                    End If
                Else
                    mstrTrnheadcode = mstrTrnheadcode + " Payable"
                    mstrTrnheadname = mstrTrnheadname + " Payable"
                    mintTrnheadtype_Id = enTranHeadType.Informational
                    mstrTypeof_id = enTypeOf.Informational
                    mintCalctype_Id = enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE
                    mintComputeon_Id = 0
                    mstrFormula = ""
                    mstrFormulaid = ""
                    mblnIsappearonpayslip = False
                    mblnIsrecurrent = False
                    mstrTrnheadname1 = mstrTrnheadname
                    mstrTrnheadname2 = mstrTrnheadname
                    mblnIstaxable = False
                    mblnIstaxrelief = False
                    mblnIscumulative = False
                    mintReftranheadId = mintTranheadunkid
                    mblnIsnoncashbenefit = False 'Sohail (18 May 2013)
                    mblnIsmonetary = False 'Sohail (18 Jun 2013)
                    mblnIsactive = True 'Sohail (09 Nov 2013)
                    mintActivityunkid = 0 'Sohail (17 Sep 2014)
                    mblnIsbasicsalaryasotherearning = False 'Sohail (09 Sep 2017)
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    mblnIsPerformanceAppraisalHead = False
                    mblnIsproratedhead = False
                    'Sohail (28 Jan 2019) -- End
                    'Sohail (18 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                    mblnIsdefault = False
                    'Sohail (18 Feb 2019) -- End
                    'Hemant (06 Jul 2020) -- Start
                    'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
                    mintCostCenterUnkId = 0
                    'Hemant (06 Jul 2020) -- End
                    'Sohail (03 Dec 2020) -- Start
                    'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                    mdblRoundOffTypeId = 200
                    'Sohail (03 Dec 2020) -- End
                    'Sohail (11 Dec 2020) -- Start
                    'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
                    mintRoundOffModeId = enPaymentRoundingType.AUTOMATIC
                    'Sohail (11 Dec 2020) -- End
                    'Sohail (08 May 2021) -- Start
                    'Scania Kenya Issue : OLD-383 : Wrong payroll computation when deductions are passed as earnings.
                    mintCompute_priority = 0
                    'Sohail (08 May 2021) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If Insert(mintCalctype_Id) = False Then
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    'If Insert(mintCalctype_Id, dtCurrentDateAndTime) = False Then
                    If Insert(mintCalctype_Id, dtCurrentDateAndTime, , objDataOperation) = False Then
                        'Sohail (28 Jan 2019) -- End
                        'Sohail (21 Aug 2015) -- End
                        Return False
                    End If
                End If
            End If
            'Sohail (21 Nov 2011) -- End

            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Sohail (28 Jan 2019) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Database Table (prtranhead_master) </purpose>
    Public Function Void(ByVal strDatabaseName As String, ByVal intUnkid As Integer, ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName]

        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Transaction Head is already in use.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intCalcType As Integer
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            Me._xDataOperation = objDataOperation
            'Sohail (28 Jan 2019) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Me._Tranheadunkid = intUnkid
            Me._Tranheadunkid(strDatabaseName) = intUnkid
            'Sohail (21 Aug 2015) -- End
            intCalcType = Me._Calctype_Id

            'StrQ = "DELETE FROM prtranhead_master " & _
            '"WHERE tranheadunkid = @tranheadunkid "
            strQ = "UPDATE prtranhead_master SET " & _
                " isvoid = 1" & _
                ", voiduserunkid = @voiduserunkid" & _
                ", voiddatetime = @voiddatetime " & _
                ", voidreason = @voidreason " & _
            "WHERE tranheadunkid = @tranheadunkid "

            'Hemant (26 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objDataOperation.ClearParameters()
            'Hemant (26 Feb 2019) -- End
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If intCalcType = enCalcType.AsComputedValue OrElse intCalcType = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse intCalcType = enCalcType.OverTimeHours OrElse intCalcType = enCalcType.ShortHours Then

                'Sohail (12 Oct 2011) -- Start
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_tran", "tranheadformulatranunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If
                'Sohail (12 Oct 2011) -- End

                'Sohail (15 Feb 2012) -- Start
                'TRA - ENHANCEMENT
                'strQ = "DELETE FROM prtranhead_formula_tran " & _
                '        "WHERE formulatranheadunkid = @formulatranheadunkid "
                strQ = "UPDATE prtranhead_formula_tran SET " & _
                          " isvoid = 1 " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voiddatetime = @voiddatetime " & _
                          ", voidreason = @voidreason " & _
                        "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                        " AND isvoid = 0 "
                'Sohail (15 Feb 2012) -- End

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
                'Sohail (15 Feb 2012) -- Start
                'TRA - ENHANCEMENT
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If
                'Sohail (15 Feb 2012) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_leavetype_tran", "tranheadformulaleavetypeunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If
                'Sohail (09 Nov 2013) -- End

                'Sohail (12 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                strQ = "UPDATE prtranhead_formula_leavetype_tran SET " & _
                         " isvoid = 1 " & _
                         ", voiduserunkid = @voiduserunkid " & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                       "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                       " AND isvoid = 0 "

                'Sohail (10 Jun 2014) -- Start
                'Issue - error  @voiduserunkid is not declared
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If
                'Sohail (10 Jun 2014) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_currency_tran", "tranheadformulacurrencytranunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If
                'Sohail (09 Nov 2013) -- End

                strQ = "UPDATE prtranhead_formula_currency_tran SET " & _
                         " isvoid = 1 " & _
                         ", voiduserunkid = @voiduserunkid " & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                       "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                       " AND isvoid = 0 "

                'Sohail (10 Jun 2014) -- Start
                'Issue - error  @voiduserunkid is not declared
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If
                'Sohail (10 Jun 2014) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_slab_tran", "tranheadformulaslabtranunkid", 3, 3, "tranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If
                'Sohail (09 Nov 2013) -- End

                strQ = "UPDATE prtranhead_formula_slab_tran SET " & _
                        " isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                      "WHERE tranheadunkid = @formulatranheadunkid " & _
                      " AND isvoid = 0 "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Sohail (12 Apr 2013) -- End

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_measurement_unit_tran", "tranheadformulameasurementunittranunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If
                'Sohail (09 Nov 2013) -- End

                'Sohail (29 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                strQ = "UPDATE prtranhead_formula_measurement_unit_tran SET " & _
                        " isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                      "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                      " AND isvoid = 0 "

                'Sohail (10 Jun 2014) -- Start
                'Issue - error  @voiduserunkid is not declared
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If
                'Sohail (10 Jun 2014) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_activity_unit_tran", "tranheadformulaactivityunittranunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If
                'Sohail (09 Nov 2013) -- End

                strQ = "UPDATE prtranhead_formula_activity_unit_tran SET " & _
                         " isvoid = 1 " & _
                         ", voiduserunkid = @voiduserunkid " & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                       "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                       " AND isvoid = 0 "

                'Sohail (10 Jun 2014) -- Start
                'Issue - error  @voiduserunkid is not declared
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If
                'Sohail (10 Jun 2014) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_measurement_amount_tran", "tranheadformulameasurementamounttranunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If
                'Sohail (09 Nov 2013) -- End

                strQ = "UPDATE prtranhead_formula_measurement_amount_tran SET " & _
                         " isvoid = 1 " & _
                         ", voiduserunkid = @voiduserunkid " & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                       "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                       " AND isvoid = 0 "

                'Sohail (10 Jun 2014) -- Start
                'Issue - error  @voiduserunkid is not declared
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If
                'Sohail (10 Jun 2014) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_activity_amount_tran", "tranheadformulaactivityamounttranunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If
                'Sohail (09 Nov 2013) -- End

                strQ = "UPDATE prtranhead_formula_activity_amount_tran SET " & _
                         " isvoid = 1 " & _
                         ", voiduserunkid = @voiduserunkid " & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                       "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                       " AND isvoid = 0 "

                'Sohail (10 Jun 2014) -- Start
                'Issue - error  @voiduserunkid is not declared
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If
                'Sohail (10 Jun 2014) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_shift_tran", "tranheadformulashifttranunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If
                'Sohail (09 Nov 2013) -- End

                strQ = "UPDATE prtranhead_formula_shift_tran SET " & _
                         " isvoid = 1 " & _
                         ", voiduserunkid = @voiduserunkid " & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                       "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                       " AND isvoid = 0 "

                'Sohail (10 Jun 2014) -- Start
                'Issue - error  @voiduserunkid is not declared
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If
                'Sohail (10 Jun 2014) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Sohail (29 Oct 2013) -- End

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_inexcessslab_tran", "tranheadtaxslabtranunkid", 3, 3, "tranheadunkid", "isvoid = 0") = False Then
                '    Return False
                'End If

                'strQ = "UPDATE prtranhead_inexcessslab_tran SET " & _
                '        " isvoid = 1 " & _
                '        ", voiduserunkid = @voiduserunkid " & _
                '        ", voiddatetime = @voiddatetime " & _
                '        ", voidreason = @voidreason " & _
                '      "WHERE tranheadunkid = @formulatranheadunkid " & _
                '      " AND isvoid = 0 "

                'dsList = objDataOperation.ExecQuery(strQ, "List")

                'If objDataOperation.ErrorMessage <> "" Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_slab_tran", "tranheadslabtranunkid", 3, 3, "tranheadunkid", "isvoid = 0") = False Then
                '    Return False
                'End If

                'strQ = "UPDATE prtranhead_slab_tran SET " & _
                '        " isvoid = 1 " & _
                '        ", voiduserunkid = @voiduserunkid " & _
                '        ", voiddatetime = @voiddatetime " & _
                '        ", voidreason = @voidreason " & _
                '      "WHERE tranheadunkid = @formulatranheadunkid " & _
                '      " AND isvoid = 0 "

                'dsList = objDataOperation.ExecQuery(strQ, "List")

                'If objDataOperation.ErrorMessage <> "" Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                'Hemant (20 June 2018) -- Start

                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_loanscheme_tran", "tranheadformulaloanschemeunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If

                strQ = "UPDATE prtranhead_formula_loanscheme_tran SET " & _
                         " isvoid = 1 " & _
                         ", voiduserunkid = @voiduserunkid " & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                       "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                       " AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If


                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_cumulative_tran", "tranheadformulacumulativetranunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If

                'Hemant (20 June 2018) -- End 

                strQ = "UPDATE prtranhead_formula_cumulative_tran SET " & _
                        " isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                      "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                      " AND isvoid = 0 "

                'Sohail (10 Jun 2014) -- Start
                'Issue - error  @voiduserunkid is not declared
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime) 'Sohail (15 Feb 2012)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If
                'Sohail (10 Jun 2014) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Sohail (09 Nov 2013) -- End

                'Sohail (02 Oct 2014) -- Start
                'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_common_master_tran", "tranheadformulamastertranunkid", 3, 3, "formulatranheadunkid", "isvoid = 0") = False Then
                    Return False
                End If

                strQ = "UPDATE prtranhead_formula_common_master_tran SET " & _
                         " isvoid = 1 " & _
                         ", voiduserunkid = @voiduserunkid " & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                       "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                       " AND isvoid = 0 "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                If dtVoidDateTime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                End If

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Sohail (02 Oct 2014) -- End

                If intCalcType = enCalcType.AsComputedValue OrElse intCalcType = enCalcType.OverTimeHours OrElse intCalcType = enCalcType.ShortHours Then 'As Computed Value,OverTime Hour, Short Hours
                    If objSimpleSlab.Void(intUnkid, intVoidUserID, dtVoidDateTime, strVoidReason) = False Then 'Sohail (28 Dec 2010)
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                ElseIf intCalcType = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then 'As Computed In Access of Tax Slab Value
                    If objTaxSlab.Void(intUnkid, intVoidUserID, dtVoidDateTime, strVoidReason) = False Then 'Sohail (28 Dec 2010)
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            'Sohail (12 Oct 2011) -- Start
            'Purpose : If no child is inserted
            If Not (mintCalctype_Id = enCalcType.AsComputedValue OrElse mintCalctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse mintCalctype_Id = enCalcType.OverTimeHours OrElse mintCalctype_Id = enCalcType.ShortHours) Then
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "", "", -1, 3, 0) = False Then
                    Return False
                End If
            End If
            'Sohail (12 Oct 2011) -- End

            'Sohail (21 Nov 2011) -- Start
            If mintTrnheadtype_Id = enTranHeadType.EmployersStatutoryContributions Then
                Dim intPayableHeadUnkId As Integer = GetEmpContribPayableHeadID(mintTranheadunkid)
                If intPayableHeadUnkId > 0 Then
                    strQ = "UPDATE prtranhead_master SET " & _
                                              " isvoid = 1" & _
                                              ", voiduserunkid = @voiduserunkid" & _
                                              ", voiddatetime = @voiddatetime " & _
                                              ", voidreason = @voidreason " & _
                                          "WHERE tranheadunkid = @tranheadunkid "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayableHeadUnkId)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                    If dtVoidDateTime = Nothing Then
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    Else
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                    End If

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intPayableHeadUnkId, "", "", -1, 3, 0) = False Then
                        Return False
                    End If
                End If
            End If
            'Sohail (21 Nov 2011) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT edunkid FROM prearningdeduction_master " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND ISNULL(isvoid,0) = 0 " & _
            "UNION " & _
            "SELECT batchtransactionunkid FROM prbatch_transaction_tran " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND ISNULL(isvoid,0) = 0 " & _
            "UNION " & _
            "SELECT tranheadformulatranunkid FROM prtranhead_formula_tran " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND ISNULL(isvoid,0) = 0 " & _
            "UNION " & _
            "SELECT costcentertranunkid FROM premployee_costcenter_tran " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND ISNULL(isvoid,0) = 0 " & _
            "UNION " & _
            "SELECT exemptionunkid FROM premployee_exemption_tran " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND ISNULL(isvoid,0) = 0 " & _
            "UNION " & _
            "SELECT  tranheadunkid " & _
            "FROM    prpayrollprocess_tran " & _
            "WHERE   ISNULL(isvoid, 0) = 0 " & _
                    "AND tranheadunkid = @tranheadunkid " & _
            "UNION " & _
            "SELECT  tranheadunkid " & _
            "FROM    bdgbudget_tran " & _
            "WHERE   ISNULL(isvoid, 0) = 0 " & _
                    "AND tranheadunkid = @tranheadunkid " & _
            "UNION " & _
            "SELECT  tranheadunkid " & _
            "FROM    prbankedi_master " & _
            "WHERE   ISNULL(isvoid, 0) = 0 " & _
                    "AND tranheadunkid = @tranheadunkid " & _
            "UNION " & _
            "SELECT  tranheadunkid " & _
            "FROM    praccount_configuration " & _
            "WHERE   tranheadunkid = @tranheadunkid " & _
                    "AND transactiontype_Id = 1 " & _
                    "AND isactive = 1 " & _
            "UNION " & _
            "SELECT  tranheadunkid " & _
            "FROM    praccount_configuration_employee " & _
            "WHERE   tranheadunkid = @tranheadunkid " & _
                    "AND transactiontype_Id = 1 " & _
                    "AND isactive = 1 " & _
            "UNION " & _
            "SELECT  tranheadunkid " & _
            "FROM    praccount_configuration_costcenter " & _
            "WHERE   tranheadunkid = @tranheadunkid " & _
                    "AND transactiontype_Id = 1 " & _
                    "AND isactive = 1 " & _
            "UNION " & _
            "SELECT  benefitallocationtranunkid " & _
            "FROM    hrbenefit_allocation_tran " & _
            "WHERE   ISNULL(isvoid, 0) = 0 " & _
                    "AND tranheadunkid = @tranheadunkid " & _
            "/*UNION " & _
            "SELECT  benefitallocationtranunkid " & _
            "FROM    hrbenefit_allocation_tran " & _
            "WHERE   ISNULL(isvoid, 0) = 0 " & _
                    "AND tranheadunkid = @tranheadunkid*/ " & _
            "UNION " & _
            "SELECT  empbatchpostingtranunkid " & _
            "FROM    premployee_batchposting_tran " & _
            "WHERE   ISNULL(isvoid, 0) = 0 " & _
                    "AND tranheadunkid = @tranheadunkid " & _
            "UNION " & _
            "SELECT tranheadformulacumulativetranunkid FROM prtranhead_formula_cumulative_tran " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND ISNULL(isvoid,0) = 0 " & _
            "UNION " & _
            "SELECT tranheadunkid FROM bgbudgetformulaheadmapping_tran " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND isvoid = 0 " & _
            "UNION " & _
            "SELECT tranheadunkid FROM bgbudget_tran " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND isvoid = 0 " & _
            "UNION " & _
            "SELECT tranheadunkid FROM lnloan_scheme_master " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND isactive = 1 " & _
            "UNION " & _
            "SELECT tranheadunkid FROM practivityrate_master " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND isactive = 1 " & _
            "UNION " & _
            "SELECT tranheadunkid FROM prpayactivity_tran " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND isvoid = 0 " & _
            "UNION " & _
            "SELECT ot_tranheadunkid FROM prpayactivity_tran " & _
                "WHERE ot_tranheadunkid = @tranheadunkid " & _
                "AND isvoid = 0 " & _
            "UNION " & _
            "SELECT ph_tranheadunkid FROM prpayactivity_tran " & _
                "WHERE ph_tranheadunkid = @tranheadunkid " & _
                "AND isvoid = 0 " & _
            "UNION " & _
            "SELECT tranheadunkid FROM bgbudgetcodes_tran " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND isvoid = 0 " & _
            "UNION " & _
            "SELECT tranheadunkid FROM hremp_benefit_coverage " & _
                "WHERE tranheadunkid = @tranheadunkid " & _
                "AND isvoid = 0 " & _
            "UNION " & _
            "SELECT emptranheadunkid FROM hrmembership_master " & _
                "WHERE emptranheadunkid = @tranheadunkid " & _
                "AND isactive = 1 " & _
            "UNION " & _
            "SELECT cotranheadunkid FROM hrmembership_master " & _
                "WHERE cotranheadunkid = @tranheadunkid " & _
                "AND isactive = 1 " & _
            "UNION " & _
            "SELECT qtytranheadunkid FROM cmclaim_process_tran " & _
                "WHERE qtytranheadunkid = @tranheadunkid " & _
                "AND isvoid = 0 " & _
            "UNION " & _
            "SELECT amttranheadunkid FROM cmclaim_process_tran " & _
                "WHERE amttranheadunkid = @tranheadunkid " & _
                "AND isvoid = 0 " & _
            "UNION " & _
            "SELECT mapped_tranheadunkid FROM lnloan_scheme_master " & _
                "WHERE mapped_tranheadunkid = @tranheadunkid " & _
                "AND isactive = 1 "
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (18 Feb 2019) - [bgbudgetformulaheadmapping_tran, bgbudget_tran, lnloan_scheme_master, practivityrate_master, prpayactivity_tran, bgbudgetcodes_tran, hremp_benefit_coverage, hrmembership_master, cmclaim_process_tran]
            'Sohail (09 Nov 2013) - [prtranhead_formula_cumulative_tran]
            'Sohail (05 Dec 2012) - [hrbenefit_allocation_tran, hrbenefit_allocation_tran, premployee_batchposting_tran]
            'Sohail (12 Oct 2011) - (praccount_configuration_employee, praccount_configuration_costcenter filter added)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Public Function isUsedList(ByVal intUnkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT DISTINCT tranheadunkid, 'prearningdeduction_master' AS TableName, 'Earning and Deduction' AS [Screen Name] FROM prearningdeduction_master " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND ISNULL(isvoid,0) = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'prbatch_transaction_tran' AS TableName, 'Batch Transaction' AS [Screen Name] FROM prbatch_transaction_tran " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND ISNULL(isvoid,0) = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'prtranhead_formula_tran' AS TableName, 'Transaction Head Formula' AS [Screen Name] FROM prtranhead_formula_tran " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND ISNULL(isvoid,0) = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'premployee_costcenter_tran' AS TableName, 'Employee Cost Centre' AS [Screen Name] FROM premployee_costcenter_tran " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND ISNULL(isvoid,0) = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'premployee_exemption_tran' AS TableName, 'Employee Exemption' AS [Screen Name] FROM premployee_exemption_tran " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND ISNULL(isvoid,0) = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT  tranheadunkid, 'prpayrollprocess_tran' AS TableName, 'Payslip' AS [Screen Name] " & _
                    "FROM    prpayrollprocess_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND tranheadunkid = @tranheadunkid " & _
                    "UNION " & _
                    "SELECT DISTINCT  tranheadunkid, 'bdgbudget_tran' AS TableName, 'Budget Preparation' AS [Screen Name] " & _
                    "FROM    bdgbudget_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND tranheadunkid = @tranheadunkid " & _
                    "UNION " & _
                    "SELECT DISTINCT  tranheadunkid, 'prbankedi_master' AS TableName, 'Bank EDI' AS [Screen Name] " & _
                    "FROM    prbankedi_master " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND tranheadunkid = @tranheadunkid " & _
                    "UNION " & _
                    "SELECT DISTINCT  tranheadunkid, 'praccount_configuration' AS TableName, 'Company Account Configuration' AS [Screen Name] " & _
                    "FROM    praccount_configuration " & _
                    "WHERE   tranheadunkid = @tranheadunkid " & _
                            "AND transactiontype_Id = 1 " & _
                            "AND isactive = 1 " & _
                    "UNION " & _
                    "SELECT DISTINCT  tranheadunkid, 'praccount_configuration_employee' AS TableName, 'Employee Account Configuration' AS [Screen Name] " & _
                    "FROM    praccount_configuration_employee " & _
                    "WHERE   tranheadunkid = @tranheadunkid " & _
                            "AND transactiontype_Id = 1 " & _
                            "AND isactive = 1 " & _
                    "UNION " & _
                    "SELECT DISTINCT  tranheadunkid, 'praccount_configuration_costcenter' AS TableName, 'Cost Centre Account Configuration' AS [Screen Name] " & _
                    "FROM    praccount_configuration_costcenter " & _
                    "WHERE   tranheadunkid = @tranheadunkid " & _
                            "AND transactiontype_Id = 1 " & _
                            "AND isactive = 1 " & _
                    "UNION " & _
                    "SELECT DISTINCT  tranheadunkid, 'hrbenefit_allocation_tran' AS TableName, 'Benefit Allocation' AS [Screen Name] " & _
                    "FROM    hrbenefit_allocation_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND tranheadunkid = @tranheadunkid " & _
                    "/*UNION " & _
                    "SELECT DISTINCT  tranheadunkid, 'hrbenefit_allocation_tran' AS TableName, 'Benefit Allocation' AS [Screen Name] " & _
                    "FROM    hrbenefit_allocation_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND tranheadunkid = @tranheadunkid*/ " & _
                    "UNION " & _
                    "SELECT DISTINCT  tranheadunkid, 'premployee_batchposting_tran' AS TableName, 'Batch Posting' AS [Screen Name] " & _
                    "FROM    premployee_batchposting_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND tranheadunkid = @tranheadunkid " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'prtranhead_formula_cumulative_tran' AS TableName, 'Cumulative transaction head' AS [Screen Name] FROM prtranhead_formula_cumulative_tran " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND ISNULL(isvoid,0) = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'bgbudgetformulaheadmapping_tran' AS TableName, 'Budget Formula Head' AS [Screen Name] FROM bgbudgetformulaheadmapping_tran " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND isvoid = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'bgbudget_tran' AS TableName, 'Budget Master' AS [Screen Name] FROM bgbudget_tran " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND isvoid = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'lnloan_scheme_master' AS TableName, 'Loan Scheme Master' AS [Screen Name] FROM lnloan_scheme_master " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND isactive = 1 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'practivityrate_master' AS TableName, 'Activity Rate Master' AS [Screen Name] FROM practivityrate_master " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND isactive = 1 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'prpayactivity_tran' AS TableName, 'Pay per Activity' AS [Screen Name] FROM prpayactivity_tran " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND isvoid = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT ot_tranheadunkid, 'prpayactivity_tran' AS TableName, 'Pay per Activity -> OT' AS [Screen Name] FROM prpayactivity_tran " & _
                        "WHERE ot_tranheadunkid = @tranheadunkid " & _
                        "AND isvoid = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT ph_tranheadunkid, 'prpayactivity_tran' AS TableName, 'Pay per Activity -> PH' AS [Screen Name] FROM prpayactivity_tran " & _
                        "WHERE ph_tranheadunkid = @tranheadunkid " & _
                        "AND isvoid = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'bgbudgetcodes_tran' AS TableName, 'Budget Codes Master' AS [Screen Name] FROM bgbudgetcodes_tran " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND isvoid = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT tranheadunkid, 'hremp_benefit_coverage' AS TableName, 'Benefit Coverage' AS [Screen Name] FROM hremp_benefit_coverage " & _
                        "WHERE tranheadunkid = @tranheadunkid " & _
                        "AND isvoid = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT emptranheadunkid, 'hrmembership_master' AS TableName, 'Membership Master -> Employee Contribution' AS [Screen Name] FROM hrmembership_master " & _
                        "WHERE emptranheadunkid = @tranheadunkid " & _
                        "AND isactive = 1 " & _
                    "UNION " & _
                    "SELECT DISTINCT cotranheadunkid, 'hrmembership_master' AS TableName, 'Membership Master -> Employer Contribution' AS [Screen Name] FROM hrmembership_master " & _
                        "WHERE cotranheadunkid = @tranheadunkid " & _
                        "AND isactive = 1 " & _
                    "UNION " & _
                    "SELECT DISTINCT qtytranheadunkid, 'cmclaim_process_tran' AS TableName, 'Claim and Request -> Quantity Head Mapping' AS [Screen Name] FROM cmclaim_process_tran " & _
                        "WHERE qtytranheadunkid = @tranheadunkid " & _
                        "AND isvoid = 0 " & _
                    "UNION " & _
                    "SELECT DISTINCT amttranheadunkid, 'cmclaim_process_tran' AS TableName, 'Claim and Request -> Amount Head Mapping' AS [Screen Name] FROM cmclaim_process_tran " & _
                        "WHERE amttranheadunkid = @tranheadunkid " & _
                        "AND isvoid = 0 "
            'Sohail (18 Feb 2019) - [bgbudgetformulaheadmapping_tran, bgbudget_tran, lnloan_scheme_master, practivityrate_master, prpayactivity_tran, bgbudgetcodes_tran, hremp_benefit_coverage, hrmembership_master, cmclaim_process_tran]
            'Sohail (09 Nov 2013) - [prtranhead_formula_cumulative_tran]
            'Sohail (05 Dec 2012) - [hrbenefit_allocation_tran, hrbenefit_allocation_tran, premployee_batchposting_tran]
            'Sohail (12 Oct 2011) - (praccount_configuration_employee, praccount_configuration_costcenter filter added)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (18 Feb 2019) -- End

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByRef intId As Integer = 0, _
                            Optional ByVal blnExcludeMembershipHeads As Boolean = False) As Boolean 'Sohail (06 Jan 2012)--Start
        '                   'Sohail (29 Oct 2012) - [blnExcludeMembershipHeads]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            intId = 0 'Sohail (02 Aug 2017)

            strQ = "SELECT " & _
                "  tranheadunkid " & _
                ", trnheadcode " & _
                ", trnheadname " & _
                ", trnheadtype_id " & _
                ", typeof_id " & _
                ", calctype_id " & _
                ", computeon_id " & _
                ", formula " & _
                ", formulaid " & _
                ", prtranhead_master.isappearonpayslip " & _
                ", isrecurrent " & _
                ", userunkid " & _
                ", isvoid " & _
                ", voiduserunkid " & _
                ", voiddatetime " & _
                ", voidreason " & _
                ", trnheadname1 " & _
                ", trnheadname2 " & _
                ", istaxable " & _
                ", istaxrelief " & _
                ", iscumulative " & _
                ", reftranheadid " & _
                ", isnoncashbenefit " & _
                ", ismonetary " & _
                ", prtranhead_master.isactive " & _
                ", prtranhead_master.isperformance " & _
                " FROM prtranhead_master "
            'Sohail (09 Nov 2013) - [isactive]
            'Sohail (18 Jun 2013) - [ismonetary]
            'Sohail (18 May 2013) - [isnoncashbenefit]
            'S.SANDEEP [12-JUL-2018] -- START {Ref#223|#ARUTI-} (isperformance) -- END


            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If blnExcludeMembershipHeads = True Then
                strQ += "LEFT JOIN hrmembership_master AS EmpContrib ON EmpContrib.emptranheadunkid = prtranhead_master.tranheadunkid AND EmpContrib.isactive = 1 " & _
                        "LEFT JOIN hrmembership_master AS CoContrib ON CoContrib.cotranheadunkid = prtranhead_master.tranheadunkid AND CoContrib.isactive = 1 "
            End If
            'Sohail (29 Oct 2012) -- End

            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'strQ += " WHERE 1 = 1 " 'Sohail (19 Nov 2010) Changes : istaxrelief field added., 'Sohail (21 Nov 2011) - [reftranheadid]
            '" WHERE ISNULL(isvoid,0) = 0 " 'Sohail (15 Feb 2012) - [active / inactive head concept]
            strQ &= " WHERE ISNULL(isvoid, 0) = 0 " 'Sohail (09 Nov 2013)
            'Sohail (09 Nov 2013) -- End

            If strCode.Trim <> "" Then
                strQ += " AND trnheadcode = @code"
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If
            If strName.Trim <> "" Then
                strQ += " AND trnheadname = @name"
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If
            If intUnkid > 0 Then
                strQ &= " AND tranheadunkid <> @tranheadunkid"
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If blnExcludeMembershipHeads = True Then
                strQ += " AND EmpContrib.membershipunkid IS NULL " & _
                        " AND CoContrib.membershipunkid IS NULL "
            End If
            'Sohail (29 Oct 2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (06 Jan 2012)--Start
            'purpose: To get transactionunkid from code for import of ED
            If dsList.Tables(0).Rows.Count > 0 Then
                intId = CInt(dsList.Tables(0).Rows(0).Item("tranheadunkid"))
            End If
            'Sohail (06 Jan 2012)--End
            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    'Sohail (17 Sep 2014) -- Start
    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
    Public Function isExistActivityHead(ByVal intCalcTypeID As Integer, _
                                        ByVal intActivityUnkId As Integer, _
                                        Optional ByRef intHeadId As Integer = 0, _
                                        Optional ByVal xDataOp As clsDataOperation = Nothing _
                                        ) As Boolean
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  tranheadunkid " & _
                    "FROM    prtranhead_master " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND activityunkid = @activityunkid " & _
                            "AND calctype_id = @calctype_id "

            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalcTypeID)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intHeadId = CInt(dsList.Tables(0).Rows(0).Item("tranheadunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistActivityHead; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function
    'Sohail (17 Sep 2014) -- End

    'Sohail (22 Jul 2010) -- Start

    ''' <summary>
    ''' Modify By : Suhail
    ''' Get TranUnkid, Code, Name List of Transaction Heads
    ''' </summary>
    ''' <param name="strList"></param>
    ''' <param name="mblnAddSelect"></param>
    ''' <param name="intTranHeadTypeID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <param name="intPerformanceHeadSetting">0 = All Head, 1 = Only Performance Head ,2 = All Head Except Peformance Head</param>
    Public Function getComboList(ByVal strDatabaseName As String, _
                                 Optional ByVal strList As String = "List", _
                                 Optional ByVal mblnAddSelect As Boolean = False, _
                                 Optional ByVal intTranHeadTypeID As Integer = 0, _
                                 Optional ByVal intCalcTypeID As Integer = 0, _
                                 Optional ByVal intTypeOfID As Integer = -1, _
                                 Optional ByVal blnAddEmpContribPayable As Boolean = False, _
                                 Optional ByVal blnExcludeMembershipHeads As Boolean = False, _
                                 Optional ByVal strFilter As String = "", _
                                 Optional ByVal blnAddRoundingAdjustment As Boolean = False, _
                                 Optional ByVal blnAddPPAHeads As Boolean = False, _
                                 Optional ByVal blnAddNetPayRoundingAdjustment As Boolean = False, _
                                 Optional ByVal intPerformanceHeadSetting As Integer = 2, _
                                 Optional ByVal blnIncludeInactiveHeads As Boolean = False, _
                                 Optional ByVal xDataOp As clsDataOperation = Nothing _
                                 ) As DataSet
        'Sohail (13 Jan 2022) - [xDataOp]
        'Sohail (05 Dec 2019) - [blnIncludeInactiveHeads]
        'Sohail (18 Apr 2016) - [blnAddNetPayRoundingAdjustment]
        'Sohail (17 Sep 2014) - [blnAddPPAHeads]
        'Sohail (21 Mar 2014) - [blnAddRoundingAdjustment]
        'Sohail (09 Jan 2014) - [strDatabaseName]
        'Sohail (10 Dec 2013) - [strFilter]
        'Sohail (02 Aug 2011) - blnAddEmpContribPayable
        'Public Function getComboList(Optional ByVal strList As String = "List", Optional ByVal mblnAddSelect As Boolean = False, Optional ByVal intTranHeadTypeID As Integer = 0, Optional ByVal intCalcTypeID As Integer = 0) As DataSet
        'Sohail (22 Jul 2010) -- End
        'Sohail (29 Oct 2012) - [blnExcludeMembershipHeads]
        'S.SANDEEP [12-JUL-2018] -- START {Ref#223|#ARUTI-} (intPerformanceHeadSetting) -- END

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet

        Try
            'Sohail (14 Jan 2022) -- Start
            'objDataOperation = New clsDataOperation
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Sohail (14 Jan 2022) -- End

            'Sohail (09 Jan 2014) -- Start
            'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If strDatabaseName.Trim = "" Then strDatabaseName = FinancialYear._Object._DatabaseName
            'Sohail (21 Aug 2015) -- End
            'Sohail (09 Jan 2014) -- End

            If mblnAddSelect = True Then
                'Sohail (11 Sep 2010) -- Start
                'Changes : typeid, calctypeid, typeofod added
                'strQ = "SELECT 0 AS tranheadunkid , ' ' AS code, ' ' + @Select AS name" & _
                '        " UNION "
                strQ = "SELECT 0 AS tranheadunkid , ' ' AS code, ' ' + @Select AS name, 0 AS trnheadtype_id, 0 AS calctype_id, 0 AS typeof_id " & _
               " UNION "
                'Sohail (11 Sep 2010) -- End

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            End If


            'Sohail (11 Sep 2010) -- Start
            'strQ += " SELECT tranheadunkid, trnheadcode as code, trnheadname as name, trnheadtype_id, calctype_id, typeof_id " & _
            ' " FROM prtranhead_master " & _
            ' " WHERE isvoid = 0"
            strQ += " SELECT tranheadunkid, trnheadcode as code, trnheadname as name, trnheadtype_id, calctype_id, typeof_id " & _
             " FROM " & strDatabaseName & "..prtranhead_master "

            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If blnExcludeMembershipHeads = True Then
                strQ += "LEFT JOIN " & strDatabaseName & "..hrmembership_master AS EmpContrib ON EmpContrib.emptranheadunkid = prtranhead_master.tranheadunkid AND EmpContrib.isactive = 1 " & _
                        "LEFT JOIN " & strDatabaseName & "..hrmembership_master AS CoContrib ON CoContrib.cotranheadunkid = prtranhead_master.tranheadunkid AND CoContrib.isactive = 1 "
            End If
            'Sohail (29 Oct 2012) -- End

            strQ += " WHERE ISNULL(isvoid,0) = 0"
            'Sohail (11 Sep 2010) -- End

            'Sohail (05 Dec 2019) -- Start
            'NMB UAT Enhancement # TC010 : Allow to exempt inactive heads on employee exemption screen.
            'strQ &= " AND ISNULL(prtranhead_master.isactive, 1) = 1 " 'Sohail (09 Nov 2013)
            If blnIncludeInactiveHeads = False Then
                strQ &= " AND ISNULL(prtranhead_master.isactive, 1) = 1 "
            End If
            'Sohail (05 Dec 2019) -- End

            If intTranHeadTypeID > 0 Then
                strQ += " AND trnheadtype_id = @trnheadtypeid "
                objDataOperation.AddParameter("@trnheadtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadTypeID)
            End If

            If intCalcTypeID > 0 Then
                strQ += " AND calctype_id = @calctypeid "
                objDataOperation.AddParameter("@calctypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalcTypeID)
            End If

            'Sohail (22 Jul 2010) -- Start
            If intTypeOfID >= 0 Then
                strQ += " AND typeof_id = @typeof_id "
                objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTypeOfID)
            End If
            'Sohail (22 Jul 2010) -- End

            'Sohail (02 Aug 2011) -- Start
            If blnAddEmpContribPayable = False Then
                strQ += " AND calctype_id <> @calctype_id_payable "
                objDataOperation.AddParameter("@calctype_id_payable", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE)
            End If
            'Sohail (02 Aug 2011) -- End

            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If blnExcludeMembershipHeads = True Then
                strQ += " AND EmpContrib.membershipunkid IS NULL " & _
                        " AND CoContrib.membershipunkid IS NULL "
            End If
            'Sohail (29 Oct 2012) -- End

            'Sohail (21 Mar 2014) -- Start
            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            If blnAddRoundingAdjustment = False Then
                strQ += " AND calctype_id <> @calctype_id_adjustment "
                objDataOperation.AddParameter("@calctype_id_adjustment", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.ROUNDING_ADJUSTMENT)
            End If
            'Sohail (21 Mar 2014) -- End

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            If blnAddNetPayRoundingAdjustment = False Then
                strQ += " AND calctype_id <> @calctype_id_netpayadjustment "
                objDataOperation.AddParameter("@calctype_id_netpayadjustment", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.NET_PAY_ROUNDING_ADJUSTMENT)
            End If
            'Sohail (18 Apr 2016) -- End

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            If blnAddPPAHeads = False Then
                strQ &= " AND typeof_id <> @PPA "
                objDataOperation.AddParameter("@PPA", SqlDbType.Int, eZeeDataType.INT_SIZE, enTypeOf.PAY_PER_ACTIVITY)
            End If
            'Sohail (17 Sep 2014) -- End

            'Sohail (10 Dec 2013) -- Start
            'Enhancement
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Sohail (10 Dec 2013) -- End


            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            Select Case intPerformanceHeadSetting
                Case 1
                    strQ &= " AND prtranhead_master.isperformance = 1 "
                Case 2
                    strQ &= " AND prtranhead_master.tranheadunkid NOT IN (SELECT tranheadunkid FROM " & strDatabaseName & "..prtranhead_master WHERE ISNULL(isvoid, 0) = 0 AND prtranhead_master.isperformance = 1 ) "
            End Select
            'S.SANDEEP [12-JUL-2018] -- END

            'Sohail (03 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            strQ &= " ORDER BY name "
            'Sohail (03 Aug 2013) -- End

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            'Sohail (14 Jan 2022) -- Start
            'If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (14 Jan 2022) -- End

            exForce = Nothing
        End Try
    End Function

    Public Function UpdateOtherTables(ByVal strDatabaseName As String, ByVal dtCurrentDateAndTime As Date, ByVal intUserId As Integer, ByVal xDataOp As clsDataOperation) As Boolean
        'Sohail (28 Jan 2019) - [xDataOp]
        'Sohail (14 Nov 2017) - [intUserId]
        'Sohail (21 Aug 2015) - [strDatabaseName, dtCurrentDateAndTime]

        'Sohail (12 Oct 2011) -- Start
        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'Dim objDataOperation As clsDataOperation
        'Sohail (28 Jan 2019) -- End
        Dim objED As clsEarningDeduction
        'Sohail (12 Oct 2011) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "UPDATE  prearningdeduction_master " & _
                        "SET formula = @formula " & _
                        ", formulaid = @formulaid " & _
                    "WHERE tranheadunkid = @tranheadunkid " & _
                    "AND ISNULL(isvoid,0) = 0 "

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormula.ToString)
            objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormulaid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            strQ = "SELECT edunkid FROM prearningdeduction_master WHERE ISNULL(isvoid,0) = 0  AND tranheadunkid = " & mintTranheadunkid & " "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dsRow As DataRow In dsList.Tables(0).Rows
                objED = New clsEarningDeduction
                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                objED._DataOperation = objDataOperation
                'Sohail (28 Jan 2019) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objED._Edunkid = CInt(dsRow.Item("edunkid").ToString)
                objED._Edunkid(objDataOperation, strDatabaseName) = CInt(dsRow.Item("edunkid").ToString)
                'Sohail (21 Aug 2015) -- End

                'Sohail (14 Nov 2017) -- Start
                'Issue - 70.1 - Previous transaction user id was passed to current period transaction.
                objED._Userunkid = intUserId
                'Sohail (14 Nov 2017) -- End

                If objED.InsertAuditTrailForEarningDeduction(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation.ReleaseTransaction(True)
            'Sohail (28 Jan 2019) -- End
            'Sohail (12 Oct 2011) -- End

            Return True
        Catch ex As Exception
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation.ReleaseTransaction(False)
            'Sohail (28 Jan 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function



    'Sandeep [ 29 NOV 2010 ] -- Start

    'S.SANDEEP [ 08 June 2011 ] -- START
    'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    'Public Function GetHeadListTypeWise(ByVal intHeadTypeId As Integer, ByVal intTypeOfId As Integer, Optional ByVal StrList As String = "List") As DataSet
    ''' <summary>
    ''' Modify By : Suhail
    ''' Get TranUnkid, Code, Name List of Transaction Heads
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <param name="intPerformanceHeadSetting">0 = All Head, 1 = Only Performance Head ,2 = All Head Except Peformance Head</param>
    Public Function GetHeadListTypeWise(ByVal intHeadTypeId As Integer, Optional ByVal intTypeOfId As Integer = -1, Optional ByVal StrList As String = "List" _
                                        , Optional ByVal blnAddEmpContribPayable As Boolean = False _
                                        , Optional ByVal blnAddRoundingAdjustment As Boolean = False _
                                        , Optional ByVal blnAddPPAHeads As Boolean = False _
                                        , Optional ByVal blnAddNetPayRoundingAdjustment As Boolean = False _
                                        , Optional ByVal intPerformanceHeadSetting As Integer = 2 _
                                        ) As DataSet
        'Sohail (18 Apr 2016) - [blnAddNetPayRoundingAdjustment]
        'Sohail (17 Sep 2014) - [blnAddPPAHeads]
        'Sohail (21 Mar 2014) - [blnAddRoundingAdjustment]
        'Sohail (02 Aug 2011) - blnAddEmpContribPayable
        'S.SANDEEP [ 08 June 2011 ] -- START
        'S.SANDEEP [12-JUL-2018] -- START {Ref#223|#ARUTI-} (blnOnlyPerformanceHead) -- END

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            Dim objMaster As New clsMasterData
            Dim ds As DataSet = objMaster.getComboListForHeadType("HeadType")
            Dim dicHeadType As Dictionary(Of Integer, String) = (From p In ds.Tables("HeadType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListTypeOf("TypeOf", , True)
            Dim dicTypeOf As Dictionary(Of Integer, String) = (From p In ds.Tables("TypeOf") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListCalcType("CalcType", , True, , True)
            Dim dicCalcType As Dictionary(Of Integer, String) = (From p In ds.Tables("CalcType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (18 Apr 2016) -- End

            'S.SANDEEP [ 08 June 2011 ] -- START
            'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
            'StrQ = "SELECT " & _
            '        "	 tranheadunkid AS Id " & _
            '        "	,trnheadname AS NAME " & _
            '        "	,CASE WHEN trnheadtype_id = 1 THEN @EarningforEmployee " & _
            '        "			  WHEN trnheadtype_id = 2 THEN @DeductionFromEmployee " & _
            '        "			  WHEN trnheadtype_id = 3 THEN @EmployeesStatutoryDeductions " & _
            '        "			  WHEN trnheadtype_id = 4 THEN @EmployersStatutoryContributions " & _
            '        "			  WHEN trnheadtype_id = 5 THEN @Information " & _
            '        "	END AS HeadType " & _
            '        "	,CASE WHEN typeof_id = 1 THEN @Salary " & _
            '        "		      WHEN typeof_id = 2 THEN @Allowance " & _
            '        "		      WHEN typeof_id = 3 THEN @Bonus " & _
            '        "		      WHEN typeof_id = 4 THEN @Commission " & _
            '        "		      WHEN typeof_id = 5 THEN @Benefit " & _
            '        "		      WHEN typeof_id = 6 THEN @OtherEarnings " & _
            '        "		      WHEN typeof_id = 7 THEN @LeaveDeduction " & _
            '        "		      WHEN typeof_id = 8 THEN @OtherDeductionsEmp " & _
            '        "		      WHEN typeof_id = 9 THEN @Taxes " & _
            '        "		      WHEN typeof_id = 10 THEN @TEmployeeStatutoryContributions " & _
            '        "		      WHEN typeof_id = 11 THEN @OtherDeductionsSatutary " & _
            '        "		      WHEN typeof_id = 12 THEN @TEmployersStatutoryContributions " & _
            '        "		      WHEN typeof_id = 13 THEN @TInformation " & _
            '        "	END AS TypeOf " & _
            '        "FROM prtranhead_master " & _
            '        "WHERE ISNULL(prtranhead_master.isvoid,0) = 0 "

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'StrQ = "SELECT " & _
            '        "	 tranheadunkid AS Id " & _
            '        "	,trnheadname AS NAME " & _
            '        "	,CASE WHEN trnheadtype_id = 1 THEN @EarningforEmployee " & _
            '        "			  WHEN trnheadtype_id = 2 THEN @DeductionFromEmployee " & _
            '        "			  WHEN trnheadtype_id = 3 THEN @EmployeesStatutoryDeductions " & _
            '        "			  WHEN trnheadtype_id = 4 THEN @EmployersStatutoryContributions " & _
            '        "			  WHEN trnheadtype_id = 5 THEN @Information " & _
            '        "	END AS HeadType "
            StrQ = "SELECT " & _
                    "	 tranheadunkid AS Id " & _
                    "	,trnheadcode AS Code " & _
                    "	,trnheadname AS NAME "
            'Sohail (14 Apr 2018) - [Code]

            StrQ &= ", CASE trnheadtype_id "
            For Each pair In dicHeadType
                StrQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            StrQ &= " END AS HeadType "
            'Sohail (18 Apr 2016) -- End

            If intTypeOfId > 0 Then

                'Sohail (18 Apr 2016) -- Start
                'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                'StrQ &= "	,CASE WHEN typeof_id = 1 THEN @Salary " & _
                '    "		      WHEN typeof_id = 2 THEN @Allowance " & _
                '    "		      WHEN typeof_id = 3 THEN @Bonus " & _
                '    "		      WHEN typeof_id = 4 THEN @Commission " & _
                '    "		      WHEN typeof_id = 5 THEN @Benefit " & _
                '    "		      WHEN typeof_id = 6 THEN @OtherEarnings " & _
                '    "		      WHEN typeof_id = 7 THEN @LeaveDeduction " & _
                '    "		      WHEN typeof_id = 8 THEN @OtherDeductionsEmp " & _
                '    "		      WHEN typeof_id = 9 THEN @Taxes " & _
                '    "		      WHEN typeof_id = 10 THEN @TEmployeeStatutoryContributions " & _
                '    "		      WHEN typeof_id = 11 THEN @OtherDeductionsSatutary " & _
                '    "		      WHEN typeof_id = 12 THEN @TEmployersStatutoryContributions " & _
                '    "		      WHEN typeof_id = 13 THEN @TInformation " & _
                '    "		      WHEN typeof_id = " & enTypeOf.PAY_PER_ACTIVITY & " THEN @PAY_PER_ACTIVITY " & _
                '        "	END AS TypeOf "
                StrQ &= ", CASE typeof_id "
                For Each pair In dicTypeOf
                    StrQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                StrQ &= " END AS TypeOf "
                'Sohail (18 Apr 2016) -- End
            End If

            StrQ &= ", trnheadtype_id AS trnheadtype_id " & _
                    ", typeof_id AS typeof_id " & _
                    "FROM prtranhead_master " & _
                    "WHERE ISNULL(prtranhead_master.isvoid,0) = 0 AND ISNULL(isactive, 1) = 1 "
            ''Sohail (09 Nov 2013) - [isactive]
            'S.SANDEEP [ 08 June 2011 ] -- START

            If intHeadTypeId > 0 Then
                StrQ &= "AND prtranhead_master.trnheadtype_id = @HeadId "
                objDataOperation.AddParameter("@HeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, intHeadTypeId)
            End If

            If intTypeOfId > 0 Then
                StrQ &= "AND prtranhead_master.typeof_id = @TypeId "
                objDataOperation.AddParameter("@TypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTypeOfId)
            End If

            'Sohail (02 Aug 2011) -- Start
            If blnAddEmpContribPayable = False Then
                StrQ += " AND calctype_id <> @calctype_id_payable "
                objDataOperation.AddParameter("@calctype_id_payable", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE)
            End If
            'Sohail (02 Aug 2011) -- End

            'Sohail (21 Mar 2014) -- Start
            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            If blnAddRoundingAdjustment = False Then
                StrQ += " AND calctype_id <> @calctype_id_adjustment "
                objDataOperation.AddParameter("@calctype_id_adjustment", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.ROUNDING_ADJUSTMENT)
            End If
            'Sohail (21 Mar 2014) -- End

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            If blnAddNetPayRoundingAdjustment = False Then
                StrQ += " AND calctype_id <> @calctype_id_netpayadjustment "
                objDataOperation.AddParameter("@calctype_id_netpayadjustment", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.NET_PAY_ROUNDING_ADJUSTMENT)
            End If
            'Sohail (18 Apr 2016) -- End

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            If blnAddPPAHeads = False Then
                StrQ &= " AND typeof_id <> @PPA "
                objDataOperation.AddParameter("@PPA", SqlDbType.Int, eZeeDataType.INT_SIZE, enTypeOf.PAY_PER_ACTIVITY)
            End If
            'Sohail (17 Sep 2014) -- End

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            Select Case intPerformanceHeadSetting
                Case 1
                    StrQ &= " AND prtranhead_master.isperformance = 1 "
                Case 2
                    StrQ &= " AND prtranhead_master.tranheadunkid NOT IN (SELECT tranheadunkid FROM prtranhead_master WHERE ISNULL(isvoid, 0) = 0 AND prtranhead_master.isperformance = 1 ) "
            End Select
            'S.SANDEEP [12-JUL-2018] -- END

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'objDataOperation.AddParameter("@EarningforEmployee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 2, "Earning for Employee"))
            'objDataOperation.AddParameter("@DeductionFromEmployee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 3, "Deduction From Employee"))
            'objDataOperation.AddParameter("@EmployeesStatutoryDeductions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 4, "Employees Statutory Deductions"))
            'objDataOperation.AddParameter("@EmployersStatutoryContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 5, "Employers Statutory Contributions"))
            'objDataOperation.AddParameter("@Information", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 6, "Informational"))
            'Sohail (18 Apr 2016) -- End

            'S.SANDEEP [ 08 June 2011 ] -- START
            'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
            'objDataOperation.AddParameter("@Salary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 7, "Salary"))
            'objDataOperation.AddParameter("@Allowance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 8, "Allowance"))
            'objDataOperation.AddParameter("@Bonus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 9, "Bonus"))
            'objDataOperation.AddParameter("@Commission", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 10, "Commission"))
            'objDataOperation.AddParameter("@OtherEarnings", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 11, "Other Earnings"))
            'objDataOperation.AddParameter("@Taxes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 12, "Taxes"))
            'objDataOperation.AddParameter("@OtherDeductionsEmp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 12, "Other Deductions"))
            'objDataOperation.AddParameter("@TEmployersStatutoryContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 13, "Employers Contributions"))
            'objDataOperation.AddParameter("@OtherDeductionsSatutary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 14, "Other Deductions"))
            'objDataOperation.AddParameter("@TInformation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 82, "Informational"))
            'objDataOperation.AddParameter("@TEmployeeStatutoryContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 116, "Employee Statutory Contributions"))
            'objDataOperation.AddParameter("@LeaveDeduction", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 122, "Leave Deduction"))
            'objDataOperation.AddParameter("@Benefit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 123, "Benefit"))

            If intTypeOfId > 0 Then
                'Sohail (18 Apr 2016) -- Start
                'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                'objDataOperation.AddParameter("@Salary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 8, "Salary"))
                'objDataOperation.AddParameter("@Allowance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 9, "Allowance"))
                'objDataOperation.AddParameter("@Bonus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 10, "Bonus"))
                'objDataOperation.AddParameter("@Commission", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 11, "Commission"))
                'objDataOperation.AddParameter("@OtherEarnings", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 12, "Other Earnings"))
                'objDataOperation.AddParameter("@Taxes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 13, "Taxes"))
                'objDataOperation.AddParameter("@OtherDeductionsEmp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 14, "Other Deductions"))
                'objDataOperation.AddParameter("@TEmployersStatutoryContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 15, "Employers Contributions"))
                'objDataOperation.AddParameter("@OtherDeductionsSatutary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 16, "Other Deductions"))
                'objDataOperation.AddParameter("@TInformation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 6, "Informational"))
                'objDataOperation.AddParameter("@TEmployeeStatutoryContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 17, "Employee Statutory Contributions"))
                'objDataOperation.AddParameter("@LeaveDeduction", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 18, "Leave Deduction"))
                'objDataOperation.AddParameter("@Benefit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 19, "Benefit"))
                'objDataOperation.AddParameter("@PAY_PER_ACTIVITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 554, "PAY PER ACTIVITY")) 'Sohail (17 Sep 2014)
                'Sohail (18 Apr 2016) -- End
            End If
            'S.SANDEEP [ 08 June 2011 ] -- START

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetHeadListTypeWise", mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sandeep [ 29 NOV 2010 ] -- End 

    'Sohail (02 Aug 2011) -- Start
    Public Function GetTranHeadIDUsedInJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
            
            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_employee" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_costcenter" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.TRANSACTION_HEAD)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
                'Sohail (21 Nov 2011) -- Start
            Else
                strIDs = "-999"
                'Sohail (21 Nov 2011) -- End
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTranHeadIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    Public Function GetTranHeadIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration_costcenter"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_costcenter"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.TRANSACTION_HEAD)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTranHeadIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    Public Function IsTranHeadIDUsedInJV(ByVal enAccConfigType As enAccountConfigType, ByVal enJVTranType As enJVTransactionType, ByVal intTranHeadID As Integer, ByVal dtAsOnDate As Date) As Boolean
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
            'Sohail (25 Jul 2020) -- End

            If intTranHeadID > 0 Then
                strFilter &= "AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadID)
            End If

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( " 'Sohail (25 Jul 2020)

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_employee" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_costcenter" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTranType)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("IDList").Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTranHeadIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try

    End Function

    Public Function IsTranHeadIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType, ByVal enJVTranType As enJVTransactionType, ByVal dtAsOnDate As Date, ByVal intTranHeadID As Integer, Optional ByVal strTranHeadIDs As String = "") As Boolean
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        'Sohail (31 Jul 2017) - [strTranHeadIDs]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
            'Sohail (25 Jul 2020) -- End

            If intTranHeadID > 0 Then
                strFilter &= "AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadID)
            End If

            'Sohail (31 Jul 2017) -- Start
            'Enhancement - 69.1 - Searchable list and searchable drop down on Company, Employee and Cost Center account configuration.
            If strTranHeadIDs.Trim.Length > 0 Then
                strFilter &= "AND tranheadunkid IN (" & strTranHeadIDs.Trim & ") "
            End If
            'Sohail (31 Jul 2017) -- End

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( " 'Sohail (25 Jul 2020)

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration_costcenter"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_costcenter"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTranType)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("IDList").Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTranHeadIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try

    End Function
    'Sohail (02 Aug 2011) -- End

    'Sohail (13 Sep 2011) -- Start
    Public Function GetSalaryHeadsIDs(ByVal strDatabaseName As String, Optional ByVal blnAddSelect As Boolean = False) As String
        'Sohail (21 Aug 2015) - [strDatabaseName]
        Dim dsList As DataSet
        Dim strIDs As String = ""
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = getComboList("SalaryHeads", blnAddSelect, , , enTypeOf.Salary)
            dsList = getComboList(strDatabaseName, "SalaryHeads", blnAddSelect, , , enTypeOf.Salary)
            'Sohail (21 Aug 2015) -- End
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'For Each dsRow As DataRow In dsList.Tables("SalaryHeads").Rows
            '    strIDs += ", " & dsRow.Item("tranheadunkid").ToString
            'Next
            'If strIDs.Length > 0 Then strIDs = strIDs.Substring(2)
            Dim allHeads As List(Of String) = (From p In dsList.Tables("SalaryHeads") Select (p.Item("tranheadunkid").ToString)).ToList
            strIDs = String.Join(",", allHeads.ToArray)
            'Sohail (09 Nov 2013) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSalaryHeadsIDs; Module Name: " & mstrModuleName)
        End Try
        Return strIDs
    End Function
    'Sohail (13 Sep 2011) -- End

    'Sohail (21 Nov 2011) -- Start
    Public Function GetEmpContribPayableHeadID(ByVal intUnkid As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intHeadUnkId As Integer = -1
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
                "  tranheadunkid " & _
                " FROM prtranhead_master " & _
                " WHERE ISNULL(isvoid,0) = 0 " & _
                " AND reftranheadid = @reftranheadid"

            objDataOperation.AddParameter("@reftranheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intHeadUnkId = CInt(dsList.Tables(0).Rows(0).Item("tranheadunkid").ToString)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isEmpContribPayableHead_Exist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
        Return intHeadUnkId
    End Function
    'Sohail (21 Nov 2011) -- End

    'Sohail (21 Mar 2014) -- Start
    'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
    Public Function GetRoundingAdjustmentHeadID() As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intHeadUnkId As Integer = -1
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                "  tranheadunkid " & _
                " FROM prtranhead_master " & _
                " WHERE ISNULL(isvoid,0) = 0 " & _
                " AND calctype_id = @calctype_id"

            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.ROUNDING_ADJUSTMENT)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intHeadUnkId = CInt(dsList.Tables(0).Rows(0).Item("tranheadunkid").ToString)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRoundingAdjustmentHeadID; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intHeadUnkId
    End Function
    'Sohail (21 Mar 2014) -- End

    'Sohail (18 Apr 2016) -- Start
    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
    Public Function GetNetPayRoundingAdjustmentHeadID(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intHeadUnkId As Integer = -1
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT " & _
                "  tranheadunkid " & _
                " FROM prtranhead_master " & _
                " WHERE ISNULL(isvoid,0) = 0 " & _
                " AND calctype_id = @calctype_id"

            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, enCalcType.NET_PAY_ROUNDING_ADJUSTMENT)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intHeadUnkId = CInt(dsList.Tables(0).Rows(0).Item("tranheadunkid").ToString)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNetPayRoundingAdjustmentHeadID; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return intHeadUnkId
    End Function
    'Sohail (18 Apr 2016) -- End

    'Sohail (28 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function IsTranHeadUsedInAnyFormula(ByVal intTranHeadUnkID As Integer, ByVal dtPeriodEndDate As Date, Optional ByVal strListName As String = "List") As DataSet
        'Sohail (27 Sep 2019) - [dtPeriodEndDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # : System should not allow user to map allocation with benefit while payroll is already processed for employees in the same allocation.
            'strQ = "SELECT DISTINCT prtranhead_master.tranheadunkid, prtranhead_master.trnheadcode, prtranhead_master.trnheadname " & _
            '        "FROM prtranhead_formula_tran " & _
            '        "INNER JOIN prtranhead_master " & _
            '        "ON prtranhead_formula_tran.formulatranheadunkid = prtranhead_master.tranheadunkid " & _
            '        "WHERE prtranhead_formula_tran.tranheadunkid = @tranheadunkid " & _
            '        "AND ISNULL(prtranhead_formula_tran.isvoid,0) = 0 " 'Sohail (15 Feb 2012) - [isvoid]
            ''Sohail (17 Sep 2019) - [DISTINCT prtranhead_formula_tran.tranheadunkid]=[DISTINCT prtranhead_master.tranheadunkid]            
            strQ = "SELECT  prtranhead_master.tranheadunkid  " & _
                        ", prtranhead_master.trnheadcode " & _
                        ", prtranhead_master.trnheadname " & _
                    "FROM    ( SELECT   DISTINCT prtranhead_formula_tran.formulatranheadunkid  " & _
                                    ", prtranhead_formula_tran.tranheadunkid " & _
                                    ", DENSE_RANK() OVER ( PARTITION BY prtranhead_formula_tran.formulatranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                    "FROM prtranhead_formula_tran " & _
                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_tran.periodunkid " & _
                                    "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                "WHERE     prtranhead_formula_tran.isvoid = 0 " & _
                                    "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ &= "   ) AS A " & _
                    "JOIN prtranhead_master ON A.formulatranheadunkid = prtranhead_master.tranheadunkid " & _
                    "WHERE   A.ROWNO = 1 " & _
                    "AND prtranhead_master.isvoid = 0 " & _
                    "AND A.tranheadunkid = @tranheadunkid "
            'Sohail (27 Sep 2019) -- End

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate)) 'Sohail (27 Sep 2019)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTranHeadUsedInAnyFormula; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function
    'Sohail (28 Jan 2012) -- End

    'Sohail (15 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    'Sohail (09 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    'Public Function MakeActive(ByVal intUnkid As Integer, ByVal intUserID As Integer) As Boolean

    '    Dim objDataOperation As clsDataOperation

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim intCalcType As Integer
    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    Try

    '        Me._Tranheadunkid = intUnkid
    '        intCalcType = Me._Calctype_Id

    '        strQ = "UPDATE prtranhead_master SET " & _
    '            " isvoid = 0 " & _
    '            ", voiduserunkid = -1 " & _
    '            ", userunkid = @userunkid" & _
    '            ", voiddatetime = @voiddatetime " & _
    '            ", voidreason = @voidreason " & _
    '        "WHERE tranheadunkid = @tranheadunkid "

    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If intCalcType = enCalcType.AsComputedValue OrElse intCalcType = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse intCalcType = enCalcType.OverTimeHours OrElse intCalcType = enCalcType.ShortHours Then

    '            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "prtranhead_formula_tran", "tranheadformulatranunkid", 2, 2, "formulatranheadunkid") = False Then
    '                Return False
    '            End If

    '            strQ = "UPDATE prtranhead_formula_tran SET " & _
    '                      " isvoid = 0 " & _
    '                      ", voiduserunkid = -1 " & _
    '                      ", voiddatetime = @voiddatetime " & _
    '                      ", voidreason = @voidreason " & _
    '                  "WHERE formulatranheadunkid = @formulatranheadunkid "

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
    '            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)

    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If intCalcType = enCalcType.AsComputedValue OrElse intCalcType = enCalcType.OverTimeHours OrElse intCalcType = enCalcType.ShortHours Then 'As Computed Value,OverTime Hour, Short Hours
    '                If objSimpleSlab.MakeActive(intUnkid, intUserID) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            ElseIf intCalcType = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then 'As Computed In Access of Tax Slab Value
    '                If objTaxSlab.MakeActive(intUnkid, intUserID) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If
    '        End If

    '        'Purpose : If no child is inserted
    '        If Not (mintCalctype_Id = enCalcType.AsComputedValue OrElse mintCalctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse mintCalctype_Id = enCalcType.OverTimeHours OrElse mintCalctype_Id = enCalcType.ShortHours) Then
    '            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "", "", -1, 2, 0) = False Then
    '                Return False
    '            End If
    '        End If

    '        If mintTrnheadtype_Id = enTranHeadType.EmployersStatutoryContributions Then
    '            Dim intPayableHeadUnkId As Integer = GetEmpContribPayableHeadID(mintTranheadunkid)
    '            If intPayableHeadUnkId > 0 Then
    '                strQ = "UPDATE prtranhead_master SET " & _
    '                                          " isvoid = 0 " & _
    '                                          ", voiduserunkid = -1 " & _
    '                                          ", userunkid = @userunkid" & _
    '                                          ", voiddatetime = @voiddatetime " & _
    '                                          ", voidreason = @voidreason " & _
    '                                      "WHERE tranheadunkid = @tranheadunkid "

    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayableHeadUnkId)
    '                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID.ToString)
    '                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
    '                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)

    '                Call objDataOperation.ExecNonQuery(strQ)

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intPayableHeadUnkId, "", "", -1, 2, 0) = False Then
    '                    Return False
    '                End If
    '            End If
    '        End If

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: MakeActive; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    Public Function MakeActiveInactive(ByVal blnMakeActive As Boolean, ByVal intUnkid As Integer, ByVal intUserID As Integer _
                                       , ByVal dtVoidDateTime As Date _
                                       , ByVal strVoidReason As String) As Boolean
        If blnMakeActive = False Then
            If isUsed(intUnkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 3, "This Transaction Head is already in use.")
                Return False
            End If
        End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            strQ = "UPDATE prtranhead_master SET " & _
        " isactive = @isactive " & _
        ", voiduserunkid = @voiduserunkid " & _
                                      ", userunkid = @userunkid" & _
                                      ", voiddatetime = @voiddatetime " & _
                                      ", voidreason = @voidreason " & _
                                  "WHERE tranheadunkid = @tranheadunkid "

            If blnMakeActive = True Then
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            Else
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID.ToString)
            End If

            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            End If
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "", "", -1, 2, 0) = False Then
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: MakeActiveInactive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (09 Nov 2013) -- End

    'Sohail (15 Feb 2012) -- End

    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Public Function UpdateIsDefault(ByVal blnIsDefault As Boolean, ByVal intUnkid As Integer _
                                    , ByVal intUserID As Integer _
                                    , ByVal dtCurrDateTime As Date _
                                    ) As Boolean


        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            strQ = "UPDATE prtranhead_master SET " & _
                            " isdefault = @isdefault " & _
                            ", userunkid = @userunkid " & _
                    "WHERE tranheadunkid = @tranheadunkid " & _
                            "AND isvoid =  0 " & _
                            "AND isdefault <>  @isdefault " & _
                            "AND typeof_id <> " & enTypeOf.Salary & " "

            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsDefault)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID.ToString)


            Dim intRows As Integer = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If intRows > 0 Then
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intUnkid, "", "", -1, 2, 0) = False Then
                    Return False
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: MakeActiveInactive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (18 Feb 2019) -- End


    'Gajanan [1-July-2020] -- Start
    'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
    Public Sub SendMailTo_User(ByVal strNotifyNonDisclosureDecFinalSavedUserIDs As String _
                                       , ByVal intCompanyUnkId As Integer _
                                       , ByVal xFormName As String _
                                       , ByVal xUserid As Integer _
                                       , ByVal xOprationType As clsEmployeeDataApproval.enOperationType _
                                       , ByVal xTransectionHeadname As String _
                                       , ByVal xIp As String _
                                       , ByVal xHostName As String _
                                       , ByVal iLoginTypeId As enLogin_Mode _
                                       , Optional ByVal iLoginEmployeeId As Integer = -1 _
                                       , Optional ByVal iUserId As Integer = -1 _
                                       , Optional ByVal isFromImport As Boolean = False _
                                       , Optional ByVal dtTransectionHead As DataTable = Nothing _
                                       , Optional ByVal mblnDisplayImportData As Boolean = False _
                                       , Optional ByVal xActiveInactive As enTranHeadActiveInActive = enTranHeadActiveInActive.ACTIVE _
                                       , Optional ByVal isActiveInactive As Boolean = False _
                                       )

        Dim strNofificationArrayIDs() As String
        Dim strMessage As New StringBuilder
        Dim strHeadList As New StringBuilder
        Dim objUser As New clsUserAddEdit
        Dim objMail As New clsSendMail
        Dim strUserContent As String = ""

        Try

            strNofificationArrayIDs = strNotifyNonDisclosureDecFinalSavedUserIDs.Split(",")



            If mblnDisplayImportData AndAlso IsNothing(dtTransectionHead) = False AndAlso dtTransectionHead.Rows.Count > 0 Then
                strMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px;'>")
                strMessage.Append(Language.getMessage(mstrModuleName, 17, "Transection head list as below:") & " </span></p>")

                strHeadList.Append("<TABLE border = '1' style='width:90%;' >")
                strHeadList.Append("<TR bgcolor= 'SteelBlue'>")
                strHeadList.Append("<TD align = 'LEFT'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#ffffff; '>" & Language.getMessage(mstrModuleName, 18, "Transection Head") & "</span></TD>")
                strHeadList.Append("</TR>")
                For Each drow As DataRow In dtTransectionHead.Rows
                    strHeadList.Append("<TR>")
                    strHeadList.Append("<TD align = 'LEFT' ><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & drow.Item("trnheadname") & "</span></TD>")
                    strHeadList.Append("</TR>")
                Next
                strHeadList.Append("</TABLE>")
            End If


            For Each strNofificationID As String In strNofificationArrayIDs
                objUser = New clsUserAddEdit

                If strNofificationID.Trim = "" Then Continue For

                strMessage = New StringBuilder

                objUser._Userunkid = CInt(strNofificationID)
                If objUser._Email.Trim = "" Then Continue For


                objMail._Subject = Language.getMessage(mstrModuleName, 8, "Transection Head Add/Edit Notification")

                strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")


                If isFromImport Then
                    strUserContent = Language.getMessage(mstrModuleName, 13, "Dear #username#," & _
                                         vbCrLf & vbCrLf & _
                                         "Please note that some new payroll transaction head(s) been imported by #auditusername# from #machinename# IP address #ipaddress# on #date# at #time#." & _
                                         vbCrLf & vbCrLf)

                    If mblnDisplayImportData Then
                        strUserContent &= strHeadList.ToString()
                    End If

                Else
                    strUserContent = Language.getMessage(mstrModuleName, 9, "Dear #username#," & _
                                                         vbCrLf & vbCrLf & _
                                                         "Please note that Payroll transaction head #transactionheadname# has been #opration# by #auditusername# from #machinename# IP address #ipaddress# on #date# at #time#.")

                End If


                strUserContent &= vbCrLf & vbCrLf
                strUserContent &= Language.getMessage(mstrModuleName, 16, "Regards.")

                strUserContent = strUserContent.Replace("#username#", "<B>" & objUser._Firstname & " " & objUser._Lastname & "</B>")
                strUserContent = strUserContent.Replace("#transactionheadname#", "<B>" & xTransectionHeadname & "</B>")


                If isActiveInactive Then

                    Select Case xActiveInactive
                        Case enTranHeadActiveInActive.ACTIVE
                            strUserContent = strUserContent.Replace("#opration#", "<B>" & Language.getMessage(mstrModuleName, 14, "Activated") & "</B>")
                        Case enTranHeadActiveInActive.INACTIVE
                            strUserContent = strUserContent.Replace("#opration#", "<B>" & Language.getMessage(mstrModuleName, 15, "Inactivated") & "</B>")
                    End Select
                End If

                Select Case xOprationType
                    Case clsEmployeeDataApproval.enOperationType.ADDED
                        strUserContent = strUserContent.Replace("#opration#", "<B>" & Language.getMessage(mstrModuleName, 10, "Added") & "</B>")
                    Case clsEmployeeDataApproval.enOperationType.EDITED
                        strUserContent = strUserContent.Replace("#opration#", "<B>" & Language.getMessage(mstrModuleName, 11, "Edited") & "</B>")
                    Case clsEmployeeDataApproval.enOperationType.DELETED
                        strUserContent = strUserContent.Replace("#opration#", "<B>" & Language.getMessage(mstrModuleName, 12, "Deleted") & "</B>")
                End Select

                Dim objAuditUser As New clsUserAddEdit
                objAuditUser._Userunkid = Convert.ToInt32(xUserid)
                objMail._ToEmail = objUser._Email

                strUserContent = strUserContent.Replace("#auditusername#", "<B>" & objAuditUser._Firstname & " " & objAuditUser._Lastname & "</B>")
                strUserContent = strUserContent.Replace("#machinename#", "<B>" & xHostName & "</B>")
                strUserContent = strUserContent.Replace("#ipaddress#", "<B>" & xIp & "</B>")
                strUserContent = strUserContent.Replace("#date#", "<B>" & DateTime.Now.Date.ToShortDateString() & "</B>")
                strUserContent = strUserContent.Replace("#time#", "<B>" & DateTime.Now.ToShortTimeString() & "</B>")
                strUserContent = strUserContent.Replace(vbCrLf, "<BR>")

                strMessage.Append(strUserContent)

                strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                strMessage.Append("</BODY></HTML>")

                objMail._Message = strMessage.ToString

                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If xFormName.Trim.Length > 0 Then
                    objMail._Form_Name = xFormName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId

                objMail._SenderAddress = IIf(objAuditUser._Email = "", objAuditUser._Firstname & " " & objAuditUser._Lastname, objAuditUser._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                objMail.SendMail(intCompanyUnkId)

                objAuditUser = Nothing

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendMailTo_User", mstrModuleName)
        Finally
            objUser = Nothing
            objMail = Nothing
        End Try
    End Sub
    'Gajanan [1-July-2020] -- End



#Region " Message List "
    '1, "This Transaction Head Name is already defined. Please define new Transaction Head name."
    '2, "This Transaction Head Code is already defined. Please define new Transaction Head code."
    '3, "This Transaction Head is already in used."
    '4, "Select"
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 37, "Compute On Specified Formula")
            Language.setMessage(mstrModuleName, 1, "This Transaction Head Name is already defined. Please define new Transaction Head Name.")
            Language.setMessage(mstrModuleName, 2, "This Transaction Head Code is already defined. Please define new Transaction Head Code.")
            Language.setMessage(mstrModuleName, 3, "This Transaction Head is already in use.")
            Language.setMessage(mstrModuleName, 4, "Select")

            Language.setMessage(mstrModuleName, 8, "Transection Head Add/Edit Notification")
            Language.setMessage(mstrModuleName, 9, "Dear #username#," & _
                                                         vbCrLf & vbCrLf & _
                                                         "Please note that Payroll transaction head #transactionheadname# has been #opration# by #auditusername# from #machinename# IP address #ipaddress# on #date# at #time#.")
            Language.setMessage(mstrModuleName, 10, "Added")
            Language.setMessage(mstrModuleName, 11, "Edited")
            Language.setMessage(mstrModuleName, 12, "Deleted")
            Language.setMessage(mstrModuleName, 13, "Dear #username#," & _
                                         vbCrLf & vbCrLf & _
                                         "Please note that some new payroll transaction head(s) been imported by #auditusername# from #machinename# IP address #ipaddress# on #date# at #time#." & _
                                         vbCrLf & vbCrLf)

            Language.setMessage(mstrModuleName, 14, "Activated")
            Language.setMessage(mstrModuleName, 15, "Inactivated")
            Language.setMessage(mstrModuleName, 16, "Regards.")
            Language.setMessage(mstrModuleName, 17, "Transection head list as below:")


        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class