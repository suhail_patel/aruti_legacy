﻿'************************************************************************************************************************************
'Class Name : clsTranheadInexcessslabTran.vb
'Purpose    :
'Date       :11/07/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsTranheadInexcessslabTran
   

#Region " Private variables "
    Private Shared Readonly mstrModuleName As String = "clsTranheadInexcessslabTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintTranheadunkid As Integer

    Private mintTranheadtaxslabtranunkid As Integer
    Private mdecAmountupto As Decimal 'Sohail (11 May 2011)
    Private mdecFixedamount As Decimal 'Sohail (11 May 2011)
    Private mdblRatepayable As Double
    Private mdecInexessof As Decimal 'Sohail (11 May 2011)
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mdtTaxSlab As DataTable

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTaxSlab = New DataTable("TaxSlab")
        Dim dCol As New DataColumn

        Try
            dCol = New DataColumn("tranheadtaxslabtranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("tranheadunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("amountupto")
            dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("fixedamount")
            dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("ratepayable")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("inexcessof")
            dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTaxSlab.Columns.Add(dCol)

            'Sohail (21 Nov 2011) -- Start
            dCol = New DataColumn("periodunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("period_name")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTaxSlab.Columns.Add(dCol)

            dCol = New DataColumn("end_date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTaxSlab.Columns.Add(dCol)
            'Sohail (21 Nov 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "

    Public Property _Datasource() As DataTable
        Get
            Return mdtTaxSlab
        End Get
        Set(ByVal value As DataTable)
            mdtTaxSlab = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadtaxslabtranunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadtaxslabtranunkid() As Integer
        Get
            Return mintTranheadtaxslabtranunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadtaxslabtranunkid = value
            'Call GetData()
        End Set
    End Property



    ''' <summary>
    ''' Purpose: Get or Set amountupto
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Amountupto() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecAmountupto
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecAmountupto = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fixedamount
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Fixedamount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecFixedamount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecFixedamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ratepayable
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Ratepayable() As Double
        Get
            Return mdblRatepayable
        End Get
        Set(ByVal value As Double)
            mdblRatepayable = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set inexcessof
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Inexessof() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecInexessof
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecInexessof = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Sohail (28 Jan 2019) -- Start
    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
    Private xDataOpr As clsDataOperation = Nothing
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'Sohail (28 Jan 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drSlab As DataRow

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            'Sohail (21 Nov 2011) -- Start
            'strQ = "SELECT " & _
            '  "  tranheadtaxslabtranunkid " & _
            '  ", tranheadunkid " & _
            '  ", amountupto " & _
            '  ", fixedamount " & _
            '  ", ratepayable " & _
            '  ", inexcessof " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            '  ", '' As AUD " & _
            ' "FROM prtranhead_inexcessslab_tran " & _
            ' "WHERE tranheadunkid = @tranheadunkid "  'Sohail (16 Oct 2010)
            strQ = "SELECT  prtranhead_inexcessslab_tran.tranheadtaxslabtranunkid " & _
                  ", prtranhead_inexcessslab_tran.tranheadunkid " & _
                  ", prtranhead_inexcessslab_tran.amountupto " & _
                  ", prtranhead_inexcessslab_tran.fixedamount " & _
                  ", prtranhead_inexcessslab_tran.ratepayable " & _
                  ", prtranhead_inexcessslab_tran.inexcessof " & _
                  ", prtranhead_inexcessslab_tran.userunkid " & _
                  ", prtranhead_inexcessslab_tran.isvoid " & _
                  ", prtranhead_inexcessslab_tran.voiduserunkid " & _
                  ", prtranhead_inexcessslab_tran.voiddatetime " & _
                  ", prtranhead_inexcessslab_tran.voidreason " & _
                  ", prtranhead_inexcessslab_tran.periodunkid " & _
                  ", cfcommon_period_tran.period_name " & _
                  ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
                  ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
                  ", '' AS AUD " & _
             "FROM prtranhead_inexcessslab_tran " & _
            "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            "WHERE   ISNULL(prtranhead_inexcessslab_tran.isvoid, 0) = 0 " & _
            "AND     prtranhead_inexcessslab_tran.tranheadunkid = @tranheadunkid "
            'Sohail (21 Nov 2011) -- End

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTaxSlab.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drSlab = mdtTaxSlab.NewRow()
                drSlab.Item("tranheadtaxslabtranunkid") = CInt(dtRow.Item("tranheadtaxslabtranunkid"))
                drSlab.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                drSlab.Item("amountupto") = CDec(dtRow.Item("amountupto")) 'Sohail (11 May 2011)
                drSlab.Item("fixedamount") = CDec(dtRow.Item("fixedamount")) 'Sohail (11 May 2011)
                drSlab.Item("ratepayable") = CDec(dtRow.Item("ratepayable"))
                drSlab.Item("inexcessof") = CDec(dtRow.Item("inexcessof")) 'Sohail (11 May 2011)
                drSlab.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drSlab.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drSlab.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drSlab.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drSlab.Item("voidreason") = dtRow.Item("voidreason")
                drSlab.Item("AUD") = dtRow.Item("AUD").ToString
                'Sohail (21 Nov 2011) -- Start
                drSlab.Item("periodunkid") = dtRow.Item("periodunkid").ToString
                drSlab.Item("period_name") = dtRow.Item("period_name").ToString
                drSlab.Item("end_date") = dtRow.Item("end_date").ToString
                'Sohail (21 Nov 2011) -- End
                mdtTaxSlab.Rows.Add(drSlab)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Sub


    Public Function InserUpdateDeleteTaxSlab() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dslist As DataSet

        Try
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = New clsDataOperation
            If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOpr
            End If
            objDataOperation.ClearParameters()
            'Sohail (28 Jan 2019) -- End

            For i = 0 To mdtTaxSlab.Rows.Count - 1
                With mdtTaxSlab.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO prtranhead_inexcessslab_tran ( " & _
                                    "  tranheadunkid " & _
                                    ", amountupto " & _
                                    ", fixedamount " & _
                                    ", ratepayable " & _
                                    ", inexcessof " & _
                                    ", userunkid " & _
                                    ", isvoid " & _
                                    ", voiduserunkid " & _
                                    ", voiddatetime" & _
                                    ", voidreason " & _
                                    ", periodunkid " & _
                                ") VALUES (" & _
                                    "  @tranheadunkid " & _
                                    ", @amountupto " & _
                                    ", @fixedamount " & _
                                    ", @ratepayable " & _
                                    ", @inexcessof " & _
                                    ", @userunkid " & _
                                    ", @isvoid " & _
                                    ", @voiduserunkid " & _
                                    ", @voiddatetime" & _
                                    ", @voidreason " & _
                                    ", @periodunkid " & _
                                "); SELECT @@identity" 'Sohail (21 Nov 2011) - [periodunkid]

                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
                                objDataOperation.AddParameter("@amountupto", SqlDbType.Decimal, eZeeDataType.MONEY_SIZE, .Item("amountupto").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@fixedamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("fixedamount").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@ratepayable", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("ratepayable").ToString)
                                objDataOperation.AddParameter("@inexcessof", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("inexcessof").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString) 'Sohail (21 Nov 2011)

                                'Sohail (12 Oct 2011) -- Start
                                'objDataOperation.ExecNonQuery(strQ)
                                dsList = objDataOperation.ExecQuery(strQ, "List")
                                'Sohail (12 Oct 2011) -- End

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (12 Oct 2011) -- Start
                                mintTranheadtaxslabtranunkid = dslist.Tables(0).Rows(0)(0)
                                If .Item("tranheadunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_inexcessslab_tran", "tranheadtaxslabtranunkid", mintTranheadtaxslabtranunkid, 2, 1) = False Then
                                        Return False
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_inexcessslab_tran", "tranheadtaxslabtranunkid", mintTranheadtaxslabtranunkid, 1, 1) = False Then
                                        Return False
                                    End If
                                End If
                                'Sohail (12 Oct 2011) -- End
                            Case "U"

                                strQ = "UPDATE prtranhead_inexcessslab_tran SET " & _
                                    "  tranheadunkid = @tranheadunkid" & _
                                    ", amountupto = @amountupto" & _
                                    ", fixedamount = @fixedamount" & _
                                    ", ratepayable = @ratepayable" & _
                                    ", inexcessof = @inexcessof" & _
                                    ", userunkid = @userunkid" & _
                                    ", isvoid = @isvoid" & _
                                    ", voiduserunkid = @voiduserunkid" & _
                                    ", voiddatetime = @voiddatetime " & _
                                    ", voidreason = @voidreason " & _
                                    ", periodunkid = @periodunkid " & _
                                "WHERE tranheadtaxslabtranunkid = @tranheadtaxslabtranunkid " 'Sohail (21 Nov 2011) - [periodunkid]

                                objDataOperation.AddParameter("@tranheadtaxslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadtaxslabtranunkid").ToString)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
                                objDataOperation.AddParameter("@amountupto", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amountupto").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@fixedamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("fixedamount").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@ratepayable", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("ratepayable").ToString)
                                objDataOperation.AddParameter("@inexcessof", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("inexcessof").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString) 'Sohail (21 Nov 2011)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (12 Oct 2011) -- Start
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", .Item("tranheadunkid").ToString, "prtranhead_inexcessslab_tran", "tranheadtaxslabtranunkid", .Item("tranheadtaxslabtranunkid").ToString, 2, 2) = False Then
                                    Return False
                                End If
                                'Sohail (12 Oct 2011) -- End
                            Case "D"

                                'Sohail (12 Oct 2011) -- Start
                                If .Item("tranheadtaxslabtranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", .Item("tranheadunkid").ToString, "prtranhead_inexcessslab_tran", "tranheadtaxslabtranunkid", .Item("tranheadtaxslabtranunkid").ToString, 2, 3) = False Then
                                        Return False
                                    End If

                                strQ = "DELETE FROM prtranhead_inexcessslab_tran " & _
                                        "WHERE tranheadtaxslabtranunkid = @tranheadtaxslabtranunkid "

                                objDataOperation.AddParameter("@tranheadtaxslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadtaxslabtranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                End If
                                'Sohail (12 Oct 2011) -- End

                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InserUpdateDeleteTaxSlab", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try

    End Function


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Database Table (prtranhead_inexcessslab_tran) </purpose>
    Public Function Void(ByVal intTranHeadUnkid As Integer, _
                         ByVal intVoidUserID As Integer, _
                         ByVal dtVoidDateTime As DateTime, _
                         ByVal strVoidReason As String) As Boolean

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try

            'Sohail (12 Oct 2011) -- Start
            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intTranHeadUnkid, "prtranhead_inexcessslab_tran", "tranheadtaxslabtranunkid", 3, 3) = False Then
                Return False
            End If
            'Sohail (12 Oct 2011) -- End

            'strQ = "DELETE FROM prtranhead_inexcessslab_tran " & _
            '"WHERE tranheadtaxslabtranunkid = @tranheadtaxslabtranunkid "
            strQ = "UPDATE prtranhead_inexcessslab_tran SET " & _
             " isvoid = 1 " & _
             ", voiduserunkid = @voiduserunkid" & _
             ", voiddatetime = @voiddatetime " & _
             ", voidreason = @voidreason " & _
           "WHERE tranheadunkid = @tranheadunkid "

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtVoidDateTime) & " " & Format(mdtVoiddatetime, "HH:mm:ss"))
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function


    'Sohail (21 Nov 2011) -- Start
    Public Function GetCurrentTaxSlab(ByVal intTranHeadUnkId As Integer, ByVal dtPeriodEndDate As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataTable
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtTable As DataTable = Nothing

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'strQ = "SELECT  prtranhead_inexcessslab_tran.tranheadtaxslabtranunkid " & _
            '      ", prtranhead_inexcessslab_tran.tranheadunkid " & _
            '      ", prtranhead_inexcessslab_tran.amountupto " & _
            '      ", prtranhead_inexcessslab_tran.fixedamount " & _
            '      ", prtranhead_inexcessslab_tran.ratepayable " & _
            '      ", prtranhead_inexcessslab_tran.inexcessof " & _
            '      ", prtranhead_inexcessslab_tran.userunkid " & _
            '      ", prtranhead_inexcessslab_tran.isvoid " & _
            '      ", prtranhead_inexcessslab_tran.voiduserunkid " & _
            '      ", prtranhead_inexcessslab_tran.voiddatetime " & _
            '      ", prtranhead_inexcessslab_tran.voidreason " & _
            '      ", prtranhead_inexcessslab_tran.periodunkid " & _
            '      ", cfcommon_period_tran.period_name " & _
            '      ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
            '      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
            '      ", '' AS AUD " & _
            '"FROM    prtranhead_inexcessslab_tran " & _
            '"LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '"WHERE   ISNULL(prtranhead_inexcessslab_tran.isvoid, 0) = 0 " & _
            '"AND     prtranhead_inexcessslab_tran.tranheadunkid = @tranheadunkid " & _
            '"AND cfcommon_period_tran.end_date = ( SELECT    MAX(end_date) " & _
            '                                      "FROM      prtranhead_inexcessslab_tran " & _
            '                                                "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                      "WHERE     ISNULL(prtranhead_inexcessslab_tran.isvoid, 0) = 0 " & _
            '                                                "AND prtranhead_inexcessslab_tran.tranheadunkid = @tranheadunkid " & _
            '                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date " & _
            '                                    ") "
            strQ = "SELECT  tranheadtaxslabtranunkid  " & _
                          ", tranheadunkid " & _
                          ", amountupto " & _
                          ", fixedamount " & _
                          ", ratepayable " & _
                          ", inexcessof " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason " & _
                          ", periodunkid " & _
                          ", period_name " & _
                          ", start_date " & _
                          ", end_date " & _
                          ", AUD " & _
                    "FROM    ( SELECT    prtranhead_inexcessslab_tran.tranheadtaxslabtranunkid  " & _
                  ", prtranhead_inexcessslab_tran.tranheadunkid " & _
                  ", prtranhead_inexcessslab_tran.amountupto " & _
                  ", prtranhead_inexcessslab_tran.fixedamount " & _
                  ", prtranhead_inexcessslab_tran.ratepayable " & _
                  ", prtranhead_inexcessslab_tran.inexcessof " & _
                  ", prtranhead_inexcessslab_tran.userunkid " & _
                  ", prtranhead_inexcessslab_tran.isvoid " & _
                  ", prtranhead_inexcessslab_tran.voiduserunkid " & _
                  ", prtranhead_inexcessslab_tran.voiddatetime " & _
                  ", prtranhead_inexcessslab_tran.voidreason " & _
                  ", prtranhead_inexcessslab_tran.periodunkid " & _
                  ", cfcommon_period_tran.period_name " & _
                  ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
                  ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
                  ", '' AS AUD " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY prtranhead_inexcessslab_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
            "FROM    prtranhead_inexcessslab_tran " & _
            "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                              "WHERE     prtranhead_inexcessslab_tran.isvoid = 0 " & _
                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date "

            If intTranHeadUnkId > 0 Then
                strQ &= "AND prtranhead_inexcessslab_tran.tranheadunkid = @tranheadunkid "
            End If

            strQ &= "       ) AS A " & _
                    "WHERE   A.ROWNO = 1 "
            'Sohail (26 Aug 2016) -- End

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = New DataView(dsList.Tables("List")).ToTable

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentTaxSlab; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dtTable
    End Function

    Public Function GetSlabPeriodList(ByVal intModuleRefid As Integer, ByVal intYearunkid As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal intStatusID As Integer = 0 _
                                      , Optional ByVal intTranHeadUnkId As Integer = 0) As DataSet
        '                            'Sohail (18 May 2013) - [intTranHeadUnkId]

        Dim dsList As New DataSet
        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'Dim objDataOperation As New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as periodunkid, ' ' +  @name  as name, '19000101' AS start_date, '19000101' AS end_date UNION "
            End If

            strQ &= "SELECT  DISTINCT prtranhead_inexcessslab_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_name as name " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                    "FROM    prtranhead_inexcessslab_tran " & _
                            "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND cfcommon_period_tran.modulerefid = @modulerefid "

            If intYearunkid > 0 Then
                strQ &= "AND cfcommon_period_tran.yearunkid = @YearId "
                objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            End If

            If intStatusID > 0 Then
                strQ &= "AND cfcommon_period_tran.statusid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            If intTranHeadUnkId > 0 Then
                strQ &= "AND prtranhead_inexcessslab_tran.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId)
            End If
            'Sohail (18 May 2013) -- End

            strQ &= "ORDER BY end_date "

            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))


            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "GetSlabPeriodList", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function
    'Sohail (21 Nov 2011) -- End

    'Sohail (15 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function MakeActive(ByVal intTranHeadUnkid As Integer, _
                        ByVal intUserID As Integer _
                        ) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try

            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intTranHeadUnkid, "prtranhead_inexcessslab_tran", "tranheadtaxslabtranunkid", 2, 2) = False Then
                Return False
            End If

            strQ = "UPDATE prtranhead_inexcessslab_tran SET " & _
             " isvoid = 0 " & _
             ", voiduserunkid = -1 " & _
             ", userunkid = @userunkid" & _
             ", voiddatetime = @voiddatetime " & _
             ", voidreason = @voidreason " & _
           "WHERE tranheadunkid = @tranheadunkid "

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: MakeActive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function
    'Sohail (15 Feb 2012) -- End

    ' Pinkal (22-Dec-2010) -- Start

    Public Function GetList(Optional ByVal strTableName As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadtaxslabtranunkid " & _
              ", tranheadunkid " & _
              ", amountupto " & _
              ", fixedamount " & _
              ", ratepayable " & _
              ", inexcessof " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' As AUD " & _
              ", periodunkid " & _
             "FROM prtranhead_inexcessslab_tran " 'Sohail (21 Nov 2011) - [periodunkid]

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
        Return dsList
    End Function

    ' Pinkal (22-Dec-2010) -- End

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadtaxslabtranunkid " & _
    '          ", tranheadunkid " & _
    '          ", amountupto " & _
    '          ", fixedamount " & _
    '          ", ratepayable " & _
    '          ", inexcessof " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '         "FROM prtranhead_inexcessslab_tran "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE isactive = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (prtranhead_inexcessslab_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)
    '        objDataOperation.AddParameter("@amountupto", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblamountupto.ToString)
    '        objDataOperation.AddParameter("@fixedamount", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblfixedamount.ToString)
    '        objDataOperation.AddParameter("@ratepayable", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblratepayable.ToString)
    '        objDataOperation.AddParameter("@inexcessof", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblinexessof.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)

    '        StrQ = "INSERT INTO prtranhead_inexcessslab_tran ( " & _
    '          "  tranheadunkid " & _
    '          ", amountupto " & _
    '          ", fixedamount " & _
    '          ", ratepayable " & _
    '          ", inexcessof " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime" & _
    '        ") VALUES (" & _
    '          "  @tranheadunkid " & _
    '          ", @amountupto " & _
    '          ", @fixedamount " & _
    '          ", @ratepayable " & _
    '          ", @inexcessof " & _
    '          ", @userunkid " & _
    '          ", @isvoid " & _
    '          ", @voiduserunkid " & _
    '          ", @voiddatetime" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintTranheadtaxslabTranUnkId = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prtranhead_inexcessslab_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintTranheadtaxslabtranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tranheadtaxslabtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadtaxslabtranunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)
    '        objDataOperation.AddParameter("@amountupto", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblamountupto.ToString)
    '        objDataOperation.AddParameter("@fixedamount", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblfixedamount.ToString)
    '        objDataOperation.AddParameter("@ratepayable", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblratepayable.ToString)
    '        objDataOperation.AddParameter("@inexcessof", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblinexessof.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)

    '        StrQ = "UPDATE prtranhead_inexcessslab_tran SET " & _
    '          "  tranheadunkid = @tranheadunkid" & _
    '          ", amountupto = @amountupto" & _
    '          ", fixedamount = @fixedamount" & _
    '          ", ratepayable = @ratepayable" & _
    '          ", inexcessof = @inexcessof" & _
    '          ", userunkid = @userunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime " & _
    '        "WHERE tranheadtaxslabtranunkid = @tranheadtaxslabtranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@tranheadtaxslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadtaxslabtranunkid " & _
    '          ", tranheadunkid " & _
    '          ", amountupto " & _
    '          ", fixedamount " & _
    '          ", ratepayable " & _
    '          ", inexcessof " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '         "FROM prtranhead_inexcessslab_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND tranheadtaxslabtranunkid <> @tranheadtaxslabtranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@tranheadtaxslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class