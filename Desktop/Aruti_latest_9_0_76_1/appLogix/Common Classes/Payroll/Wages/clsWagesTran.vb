﻿'************************************************************************************************************************************
'Class Name : clsWagesTran.vb
'Purpose    :
'Date       :02/07/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsWagesTran

#Region " Private variables "
    Private Shared ReadOnly mstrModuleName As String = "clsWagesTran"
    Private objDataOperation As clsDataOperation
    Private mstrMessage As String = ""
    Private mdtTran As DataTable
    Private mintWagesUnkID As Integer = -1

    'Sohail (11 Nov 2010) -- Start
    Private mintWagestranunkid As Integer
    'Private mintWagesunkid As Integer
    Private mintGradeunkid As Integer
    Private mintGradelevelunkid As Integer
    Private mdecSalary As Decimal 'Sohail (11 May 2011)
    Private mdecIncrement As Decimal 'Sohail (11 May 2011)
    Private mdecMaximum As Decimal 'Sohail (11 May 2011)
    Private mdecMidpoint As Decimal 'Sohail (31 Mar 2012)
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrvoidreason As String
    'Sohail (11 Nov 2010) -- End
    Private mintPeriodunkid As Integer 'Sohail (27 Apr 2016)

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END


#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("WagesTran")
        Dim dCol As New DataColumn
        Try
            dCol = New DataColumn("wagestranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("wagesunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("gradeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("gradelevelunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("salary")
            dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
            mdtTran.Columns.Add(dCol)

            mdtTran.Columns.Add("midpoint", System.Type.GetType("System.Decimal")).DefaultValue = 0 'Sohail (31 Mar 2012)

            dCol = New DataColumn("increment")
            dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("maximum")
            dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("GUID")
            'dCol.DataType = System.Type.GetType("System.String")
            'mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("AUD")
            'dCol.DataType = System.Type.GetType("System.Char")
            'dCol.AllowDBNull = True
            'dCol.DefaultValue = DBNull.Value
            'mdtTran.Columns.Add(dCol)

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            mdtTran.Columns.Add("gradename", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("levelname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            'Sohail (27 Apr 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "
    Public Property _WageUnkID() As Integer
        Get
            Return mintWagesUnkID
        End Get
        Set(ByVal value As Integer)
            mintWagesUnkID = value
            Call GetWagesTran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property


    'Sohail (11 Nov 2010) -- Start
    ''' <summary>
    ''' Purpose: Get or Set wagestranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Wagestranunkid() As Integer
        Get
            Return mintWagestranunkid
        End Get
        Set(ByVal value As Integer)
            mintWagestranunkid = Value
            Call GetData()
        End Set
    End Property

    'Sohail (11 Nov 2010) -- End


    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region


    'Sohail (11 Nov 2010) -- Start
    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
           
            strQ = "SELECT " & _
                      "  wagestranunkid " & _
                      ", wagesunkid " & _
                      ", gradeunkid " & _
                      ", gradelevelunkid " & _
                      ", salary " & _
                      ", midpoint " & _
                      ", increment " & _
                      ", maximum " & _
                      ", isvoid " & _
                      ", ISNULL(voiduserunkid, -1) AS voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", periodunkid " & _
             "FROM prwages_tran " & _
             "WHERE wagestranunkid = @wagestranunkid "
            'Sohail (27 Apr 2016) - [periodunkid]
            'Sohail (31 Mar 2012) - [midpoint]
            'Sohail (21 Aug 2015) - [ISNULL(voiduserunkid, -1) AS]

            objDataOperation.AddParameter("@wagestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWagestranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintWagestranunkid = CInt(dtRow.Item("wagestranunkid"))
                mintWagesUnkID = CInt(dtRow.Item("wagesunkid"))
                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintGradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                mdecSalary = CDec(dtRow.Item("salary")) 'Sohail (11 May 2011)
                mdecIncrement = CDec(dtRow.Item("increment")) 'Sohail (11 May 2011)
                mdecMaximum = CDec(dtRow.Item("maximum")) 'Sohail (11 May 2011)
                mdecMidpoint = CDec(dtRow.Item("midpoint")) 'Sohail (31 Mar 2012)
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If dtRow.Item("voiduserunkid").ToString = Nothing Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'mintVoiduserunkid = User._Object._Userunkid
                    mintVoiduserunkid = -1
                    'Sohail (21 Aug 2015) -- End
                Else
                    mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                End If
                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime").ToString
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                'Sohail (27 Apr 2016) -- Start
                'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                mintPeriodunkid = Int(dtRow.Item("periodunkid").ToString)
                'Sohail (27 Apr 2016) -- End
                Exit For
            Next

            
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Sohail (11 Nov 2010) -- End

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetWagesTran()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtRow As DataRow

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  wagestranunkid " & _
              ", wagesunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", salary " & _
              ", midpoint " & _
              ", increment " & _
              ", maximum " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", periodunkid " & _
             "FROM prwages_tran " & _
             "WHERE wagesunkid = @wagesunkid AND isvoid = 0"
            'Sohail (27 Apr 2016) - [periodunkid]
            'Sohail (31 Mar 2012) - [midpoint]

            objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWagesUnkID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            If dsList.Tables.Count > 1 Then
                dsList.Tables(0).Merge(dsList.Tables(1))
            End If
            'Sohail (27 Apr 2016) -- End

            mdtTran.Clear()

            For Each dsRow As DataRow In dsList.Tables(0).Rows
                dtRow = mdtTran.NewRow
                With dtRow
                    .Item("wagestranunkid") = CInt(dsRow.Item("wagestranunkid").ToString)
                    .Item("wagesunkid") = CInt(dsRow.Item("wagesunkid").ToString)
                    .Item("gradeunkid") = CInt(dsRow.Item("gradeunkid").ToString)
                    .Item("gradelevelunkid") = CInt(dsRow.Item("gradelevelunkid").ToString)
                    .Item("salary") = CDec(dsRow.Item("salary").ToString) 'Sohail (11 May 2011)
                    .Item("increment") = CDec(dsRow.Item("increment").ToString) 'Sohail (11 May 2011)
                    .Item("maximum") = CDec(dsRow.Item("maximum").ToString) 'Sohail (11 May 2011)
                    .Item("midpoint") = CDec(dsRow.Item("midpoint").ToString) 'Sohail (31 Mar 2012) 
                    .Item("isvoid") = CBool(dsRow.Item("isvoid"))
                    If dsRow.Item("voiduserunkid").ToString = Nothing Then
                        .Item("voiduserunkid") = DBNull.Value
                    Else
                        .Item("voiduserunkid") = CInt(dsRow.Item("voiduserunkid").ToString)
                    End If

                    If dsRow.Item("voiddatetime").ToString = Nothing Then
                        .Item("voiddatetime") = DBNull.Value
                    Else
                        .Item("voiddatetime") = dsRow.Item("voiddatetime").ToString
                    End If
                    .Item("voidreason") = dsRow.Item("voidreason").ToString

                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    .Item("periodunkid") = dsRow.Item("periodunkid").ToString
                    'Sohail (27 Apr 2016) -- End

                    mdtTran.Rows.Add(dtRow)
                End With
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function InserUpdateDeleteWagesTran(ByVal dtTran As DataTable, ByVal intWagesUnkID As Integer, ByVal intUserunkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [intUserunkid, dtCurrentDateAndTime]

        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet 'Sohail (11 Nov 2010)
        Try
            objDataOperation = New clsDataOperation

            For i = 0 To dtTran.Rows.Count - 1
                With dtTran.Rows(i)

                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    'If isExist(CInt(.Item("wagesunkid").ToString) _
                    '           , CInt(.Item("gradeunkid").ToString) _
                    '           , CInt(.Item("gradelevelunkid").ToString)) = False Then
                    Dim intExistWagesTranUnkId As Integer = 0
                    If isExist(intWagesUnkID _
                               , CInt(.Item("gradeunkid").ToString) _
                               , CInt(.Item("gradelevelunkid").ToString) _
                               , CInt(.Item("periodunkid").ToString) _
                               , intExistWagesTranUnkId _
                               ) = False Then
                        'Sohail (27 Apr 2016) -- End

                        objDataOperation.ClearParameters()

                        strQ = "INSERT INTO prwages_tran ( " & _
                            "  wagesunkid " & _
                            ", gradeunkid " & _
                            ", gradelevelunkid " & _
                            ", salary " & _
                            ", increment " & _
                            ", maximum " & _
                            ", midpoint " & _
                            ", isvoid " & _
                            ", voiduserunkid " & _
                            ", voiddatetime" & _
                            ", voidreason " & _
                            ", periodunkid " & _
                        ") VALUES (" & _
                            "  @wagesunkid " & _
                            ", @gradeunkid " & _
                            ", @gradelevelunkid " & _
                            ", @salary " & _
                            ", @increment " & _
                            ", @maximum " & _
                            ", @midpoint " & _
                            ", @isvoid " & _
                            ", @voiduserunkid " & _
                            ", @voiddatetime" & _
                            ", @voidreason " & _
                            ", @periodunkid " & _
                        "); SELECT @@identity"
                        'Sohail (27 Apr 2016) - [periodunkid]
                        'Sohail (31 Mar 2012) - [midpoint]

                        objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intWagesUnkID.ToString)
                        objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("gradeunkid").ToString)
                        objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("gradelevelunkid").ToString)
                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        'objDataOperation.AddParameter("@salary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("salary").ToString) 'Sohail (11 May 2011)
                        objDataOperation.AddParameter("@salary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("minimum").ToString)
                        'Sohail (27 Apr 2016) -- End
                        objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("increment").ToString) 'Sohail (11 May 2011)
                        objDataOperation.AddParameter("@maximum", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("maximum").ToString) 'Sohail (11 May 2011)
                        objDataOperation.AddParameter("@midpoint", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("midpoint").ToString) 'Sohail (31 Mar 2012)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                        If .Item("voiduserunkid").ToString = Nothing Then
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
                        Else
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                        End If

                        If .Item("voiddatetime").ToString = Nothing Then
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        Else
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                        End If
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString) 'Sohail (27 Apr 2016)

                        'Sohail (11 Nov 2010) -- Start
                        'objDataOperation.ExecNonQuery(strQ)
                        dsList = objDataOperation.ExecQuery(strQ, "List")
                        'Sohail (11 Nov 2010) -- End

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Sohail (11 Nov 2010) -- Start
                        mintWagestranunkid = dsList.Tables(0).Rows(0).Item(0)

                        Me._Wagestranunkid = mintWagestranunkid

                        'Anjan (11 Jun 2011)-Start
                        'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
                        'Call InsertAuditTrailForWagesTran(objDataOperation, 1)
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If InsertAuditTrailForWagesTran(objDataOperation, 1) = False Then
                        If InsertAuditTrailForWagesTran(objDataOperation, 1, intUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Anjan (11 Jun 2011)-End


                        'Sohail (11 Nov 2010) -- End

                    Else

                        objDataOperation.ClearParameters()

                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        mintWagestranunkid = intExistWagesTranUnkId
                        'Sohail (27 Apr 2016) -- End

                        strQ = "UPDATE prwages_tran SET " & _
                            "  wagesunkid = @wagesunkid" & _
                            ", gradeunkid = @gradeunkid" & _
                            ", gradelevelunkid = @gradelevelunkid" & _
                            ", salary = @salary" & _
                            ", increment = @increment" & _
                            ", maximum = @maximum" & _
                            ", midpoint = @midpoint" & _
                            ", isvoid = @isvoid" & _
                            ", voiduserunkid = @voiduserunkid" & _
                            ", voiddatetime = @voiddatetime " & _
                            ", voidreason = @voidreason " & _
                            ", periodunkid = @periodunkid " & _
                        "WHERE wagestranunkid = @wagestranunkid "
                        'Sohail (27 Apr 2016) - [periodunkid]
                        'Sohail (31 Mar 2012) - [midpoint]

                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        'objDataOperation.AddParameter("@wagestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("wagestranunkid").ToString)
                        'objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("wagesunkid").ToString)
                        objDataOperation.AddParameter("@wagestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWagestranunkid)
                        objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intWagesUnkID)
                        'Sohail (27 Apr 2016) -- End
                        objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("gradeunkid").ToString)
                        objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("gradelevelunkid").ToString)
                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        'objDataOperation.AddParameter("@salary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("salary").ToString) 'Sohail (11 May 2011)
                        objDataOperation.AddParameter("@salary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("minimum").ToString)
                        'Sohail (27 Apr 2016) -- End
                        objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("increment").ToString) 'Sohail (11 May 2011)
                        objDataOperation.AddParameter("@maximum", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("maximum").ToString) 'Sohail (11 May 2011)
                        objDataOperation.AddParameter("@midpoint", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("midpoint").ToString) 'Sohail (31 Mar 2012)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                        If .Item("voiduserunkid").ToString = Nothing Then
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
                        Else
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                        End If
                        If .Item("voiddatetime").ToString = Nothing Then
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        Else
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                        End If
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString) 'Sohail (27 Apr 2016)

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Sohail (11 Nov 2010) -- Start
                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        'Me._Wagestranunkid = CInt(.Item("wagestranunkid").ToString)
                        Me._Wagestranunkid = mintWagestranunkid
                        'Sohail (27 Apr 2016) -- End

                        'Anjan (11 Jun 2011)-Start
                        'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
                        'Call InsertAuditTrailForWagesTran(objDataOperation, 2)
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If InsertAuditTrailForWagesTran(objDataOperation, 2) = False Then
                        If InsertAuditTrailForWagesTran(objDataOperation, 2, intUserunkid, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Anjan (11 Jun 2011)-End
                        'Sohail (11 Nov 2010) -- End

                    End If

                End With
            Next

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InserUpdateDeleteBatchTran", mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Purpose    : Void All Data Used For Particular Batch
    ''' Modify By : Suhail
    ''' </summary>
    ''' <param name="intWagesUnkID"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function Void(ByVal intWagesUnkID As Integer, _
                         ByVal intVoidUserID As Integer, _
                         ByVal dtVoidDateTime As DateTime, _
                         ByVal strVoidReason As String) As Boolean

        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            objDataOperation = New clsDataOperation

            strQ = "UPDATE prwages_tran SET " & _
                " isvoid = 1" & _
                ", voiduserunkid = @voiduserunkid" & _
                ", voiddatetime = @voiddatetime " & _
                ", voidreason = @voidreason " & _
            "WHERE wagestranunkid = @wagestranunkid "

            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtVoidDateTime) & " " & Format(dtVoidDateTime, "HH:mm:ss"))
            End If
            objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intWagesUnkID.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Void", mstrModuleName)
            Return False
        Finally
            'If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try
    End Function

    'Nilay (06-Jun-2016) -- Start
    'Enhancement : Import option in Wages Table for KBC
    Public Function isExistByPeriodByLevel(ByVal intPeriodId As Integer, ByVal intGradeLevelId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                       "  wagestranunkid " & _
                       ", wagesunkid " & _
                       ", gradeunkid " & _
                       ", gradelevelunkid " & _
                       ", salary " & _
                       ", increment " & _
                       ", maximum " & _
                       ", midpoint " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                   "FROM prwages_tran " & _
                   "WHERE  ISNULL(isvoid,0) = 0 " & _
                     " AND periodunkid = @periodunkid " & _
                     " AND gradelevelunkid = @gradelevelunkid "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeLevelId)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistByPeriodByLevel; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
    'Nilay (06-Jun-2016) -- End


    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intWagesUnkID As Integer _
                            , ByVal intGradeUnkID As Integer _
                            , ByVal intGradeLevelUnkID As Integer _
                            , ByVal intPeriodUnkId As Integer _
                            , Optional ByRef intWagesTranUnkId As Integer = 0 _
                            ) As Boolean
        'Sohail (27 Apr 2016) - [intPeriodUnkId, intWagesTranUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                "  wagestranunkid " & _
                ", wagesunkid " & _
                ", gradeunkid " & _
                ", gradelevelunkid " & _
                ", salary " & _
                ", increment " & _
                ", maximum " & _
                ", midpoint " & _
                ", isvoid " & _
                ", voiduserunkid " & _
                ", voiddatetime " & _
                ", voidreason " & _
            "FROM prwages_tran " & _
            "WHERE wagesunkid = @wagesunkid " & _
            "AND gradeunkid = @gradeunkid " & _
            "AND gradelevelunkid = @gradelevelunkid AND ISNULL(isvoid,0) = 0" & _
            " AND periodunkid = @periodunkid "
            'Sohail (16 Oct 2010), 'Sohail (31 Mar 2012) - [midpoint]

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intWagesUnkID)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeUnkID)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeLevelUnkID)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId) 'Sohail (27 Apr 2016)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            If dsList.Tables(0).Rows.Count > 0 Then
                intWagesTranUnkId = CInt(dsList.Tables(0).Rows(0).Item("wagestranunkid"))
            Else
                intWagesTranUnkId = 0
            End If
            'Sohail (27 Apr 2016) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function


    Public Function getScaleInfo(ByVal intGradeUnkID As Integer _
                                 , ByVal intLevelUnkID As Integer _
                                 , ByVal dtPeriodEnd As Date _
                                 , Optional ByVal strListName As String = "List" _
                                 , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                 ) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        'Sohail (27 Apr 2016) - [dtPeriodEnd]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'strQ = "SELECT " & _
            '    "  wagestranunkid " & _
            '    ", wagesunkid " & _
            '    ", gradeunkid " & _
            '    ", gradelevelunkid " & _
            '    ", salary " & _
            '    ", increment " & _
            '    ", maximum " & _
            '    ", isvoid " & _
            '    ", voiduserunkid " & _
            '    ", voiddatetime " & _
            '    ", voidreason " & _
            '    ", midpoint " & _
            '"FROM prwages_tran " & _
            '"WHERE gradeunkid = @gradeunkid " & _
            '"AND gradelevelunkid = @gradelevelunkid AND ISNULL(isvoid,0) = 0" 'Sohail (16 Oct 2010), 'Sohail (31 Mar 2012) - [midpoint]
            strQ = "SELECT  * " & _
                    "FROM    ( SELECT    wagestranunkid  " & _
                ", wagesunkid " & _
                ", gradeunkid " & _
                ", gradelevelunkid " & _
                ", salary " & _
                ", increment " & _
                ", maximum " & _
                ", isvoid " & _
                ", voiduserunkid " & _
                ", voiddatetime " & _
                ", voidreason " & _
                ", midpoint " & _
                                      ", prwages_tran.periodunkid " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY prwages_tran.gradeunkid ORDER BY cfcommon_period_tran.end_date DESC, prwages_tran.wagestranunkid DESC ) AS ROWNO " & _
            "FROM prwages_tran " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prwages_tran.periodunkid " & _
                              "WHERE     isvoid = 0 " & _
                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                        "AND gradelevelunkid = @gradelevelunkid " & _
                                        "AND gradeunkid = @gradeunkid " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS A " & _
                    "WHERE   A.ROWNO = 1 "
            'Sohail (21 Jul 2016) -- Start
            'Issue - 63.1 - Scales are not coming for current period if current date is less than period end date
            '[AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "'] = [AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "']
            'Sohail (21 Jul 2016) -- End
            'Sohail (27 Apr 2016) -- End

            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeUnkID)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getScaleInfo", mstrModuleName)
        Finally
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function

Public Function GetSalary(ByVal intGrpId As Integer, _
                              ByVal intGradeId As Integer, _
                              ByVal intLevelId As Integer, _
                              ByVal dtPeriodEnd As Date, _
                              ByRef decSalary As Decimal _
                              ) As Decimal
        'Sohail (27 Apr 2016) - [dtPeriodEnd]
        'Sohail (11 May 2011)

        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As New DataSet

        objDataOperation = New clsDataOperation
        Try

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'strQ = "SELECT " & _
            '            " ISNULL(prwages_tran.salary,0) AS Salary " & _
            '       "FROM prwages_tran,prwages_master " & _
            '       "WHERE prwages_tran.wagesunkid = prwages_master.wagesunkid " & _
            '       "AND prwages_master.gradegroupunkid = @gradegroupunkid " & _
            '       "AND prwages_tran.gradeunkid = @gradeunkid " & _
            '       "AND prwages_tran.gradelevelunkid=@gradelevelunkid " & _
            '       "AND ISNULL(prwages_tran.isvoid,0) = 0 "
            'Sohail (12 May 2017) -- Start
            'KBC Issue - 66.1 - Latest period scale was not coming on employee master due to PARTITION BY prwages_master.wagesunkid used.
            'strQ = "SELECT   * " & _
            '       "FROM     ( SELECT   ISNULL(prwages_tran.salary, 0) AS Salary " & _
            '                          ", DENSE_RANK() OVER ( PARTITION BY prwages_master.wagesunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
            '                  "FROM      prwages_master " & _
            '                            "LEFT JOIN prwages_tran ON prwages_master.wagesunkid = prwages_tran.wagesunkid " & _
            '                                                      "AND prwages_master.periodunkid = prwages_tran.periodunkid " & _
            '                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prwages_master.periodunkid " & _
            '                  "WHERE     prwages_master.isvoid = 0 " & _
            '                            "AND prwages_tran.isvoid = 0 " & _
            '                            "AND cfcommon_period_tran.isactive = 1 " & _
            '                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            '       "AND prwages_master.gradegroupunkid = @gradegroupunkid " & _
            '       "AND prwages_tran.gradeunkid = @gradeunkid " & _
            '       "AND prwages_tran.gradelevelunkid=@gradelevelunkid " & _
            '                            "AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                ") AS A " & _
            '        "WHERE    A.ROWNO = 1 "
            strQ = "SELECT   * " & _
                   "FROM     ( SELECT   ISNULL(prwages_tran.salary, 0) AS Salary " & _
                                      ", DENSE_RANK() OVER ( ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                              "FROM      prwages_master " & _
                                        "LEFT JOIN prwages_tran ON prwages_master.wagesunkid = prwages_tran.wagesunkid " & _
                                                                  "AND prwages_master.periodunkid = prwages_tran.periodunkid " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prwages_master.periodunkid " & _
                              "WHERE     prwages_master.isvoid = 0 " & _
                                        "AND prwages_tran.isvoid = 0 " & _
                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                   "AND prwages_master.gradegroupunkid = @gradegroupunkid " & _
                   "AND prwages_tran.gradeunkid = @gradeunkid " & _
                   "AND prwages_tran.gradelevelunkid=@gradelevelunkid " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS A " & _
                    "WHERE    A.ROWNO = 1 "
            'Sohail (12 May 2017) -- End
            'Sohail (21 Jul 2016) -- Start
            'Issue - 63.1 - Scales are not coming for current period if current date is less than period end date
            '[AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' ] = [AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' ]
            'Sohail (21 Jul 2016) -- End
            'Sohail (27 Apr 2016) -- End

            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrpId.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeId.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                decSalary = dsList.Tables(0).Rows(0)("Salary")
            Else
                decSalary = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSalary", mstrModuleName)
        End Try
    End Function

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Public Function GetNextGradeLevelPriority(ByVal intCurrentGradeId As Integer, ByVal intCurrentPriority As Integer, Optional ByVal blnApplyCurrPriorityNotZero As Boolean = True, Optional ByRef intNewLevelId As Integer = 0, Optional ByRef strNewLevelName As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        intNewLevelId = 0
        strNewLevelName = ""

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  * " & _
                    "FROM    ( SELECT    hrgradelevel_master.gradelevelunkid  " & _
                                      ", hrgradelevel_master.gradeunkid " & _
                                      ", hrgrade_master.gradegroupunkid " & _
                                      ", hrgradelevel_master.code " & _
                                      ", hrgradelevel_master.name " & _
                                      ", hrgradelevel_master.priority " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY hrgradelevel_master.gradeunkid ORDER BY hrgradelevel_master.priority ASC ) AS ROWNO " & _
                              "FROM      hrgradelevel_master " & _
                                        "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = hrgradelevel_master.gradeunkid " & _
                              "WHERE     hrgradelevel_master.isactive = 1 " & _
                                        "AND hrgrade_master.isactive = 1 " & _
                                        "AND hrgradelevel_master.gradeunkid = @gradeunkid " & _
                                        "AND hrgradelevel_master.priority > @priority "

            If blnApplyCurrPriorityNotZero = True Then
                strQ &= "                AND " & intCurrentPriority & " > 0 "
            End If

            strQ &= "        ) AS A " & _
                    "WHERE   A.ROWNO = 1 "

            objDataOperation.AddParameter("@gradeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentGradeId)
            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intNewLevelId = CInt(dsList.Tables(0).Rows(0).Item("gradelevelunkid"))
                strNewLevelName = dsList.Tables(0).Rows(0).Item("name").ToString
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNextGradeLevelPriority", mstrModuleName)
        End Try
        Return dsList
    End Function

    Public Function GetNextGradePriority(ByVal intCurrentGradeGroupId As Integer, ByVal intCurrentPriority As Integer, Optional ByVal blnApplyCurrPriorityNotZero As Boolean = True, Optional ByRef intNewGradeId As Integer = 0, Optional ByRef strNewGradeName As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        intNewGradeId = 0
        strNewGradeName = ""

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  * " & _
                    "FROM    ( SELECT    hrgrade_master.gradeunkid  " & _
                                      ", hrgrade_master.gradegroupunkid " & _
                                      ", hrgrade_master.code " & _
                                      ", hrgrade_master.name " & _
                                      ", hrgrade_master.priority " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY hrgrade_master.gradegroupunkid ORDER BY hrgrade_master.priority ASC ) AS ROWNO " & _
                              "FROM      hrgrade_master " & _
                              "WHERE     hrgrade_master.isactive = 1 " & _
                                        "AND hrgrade_master.gradegroupunkid = @gradegroupunkid " & _
                                        "AND hrgrade_master.priority > @priority "

            If blnApplyCurrPriorityNotZero = True Then
                strQ &= "                AND " & intCurrentPriority & " > 0 "
            End If

            strQ &= "        ) AS A " & _
                    "WHERE   A.ROWNO = 1 "

            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentGradeGroupId)
            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intNewGradeId = CInt(dsList.Tables(0).Rows(0).Item("gradeunkid"))
                strNewGradeName = dsList.Tables(0).Rows(0).Item("name").ToString
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNextGradePriority", mstrModuleName)
        End Try
        Return dsList
    End Function

    Public Function GetNextGradeGroupPriority(ByVal intCurrentPriority As Integer, Optional ByRef intNewGradeGroupId As Integer = 0, Optional ByRef strNewGradeGroupName As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        intNewGradeGroupId = 0
        strNewGradeGroupName = ""

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT TOP 1 * " & _
                    "FROM    hrgradegroup_master " & _
                    "WHERE   hrgradegroup_master.isactive = 1 " & _
                            "AND hrgradegroup_master.priority > @priority " & _
                            "AND " & intCurrentPriority & " > 0 " & _
                    "ORDER BY hrgradegroup_master.priority ASC "

            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intNewGradeGroupId = CInt(dsList.Tables(0).Rows(0).Item("gradegroupunkid"))
                strNewGradeGroupName = dsList.Tables(0).Rows(0).Item("name").ToString
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNextGradeGroupPriority", mstrModuleName)
        End Try
        Return dsList
    End Function
    'Sohail (27 Apr 2016) -- End

    'Sohail (11 Nov 2010) -- Start

    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForWagesTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForWagesTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal intUserunkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [intUserunkid, dtCurrentDateAndTime]

        'Anjan (11 Jun 2011)-End
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atprwages_tran ( " & _
            '              "  wagestranunkid " & _
            '              ", wagesunkid " & _
            '              ", gradeunkid " & _
            '              ", gradelevelunkid " & _
            '              ", salary " & _
            '              ", increment " & _
            '              ", maximum " & _
            '              ", midpoint " & _
            '              ", audittype " & _
            '              ", audituserunkid " & _
            '              ", auditdatetime " & _
            '              ", ip " & _
            '              ", machine_name" & _
            '") VALUES (" & _
            '              "  @wagestranunkid " & _
            '              ", @wagesunkid " & _
            '              ", @gradeunkid " & _
            '              ", @gradelevelunkid " & _
            '              ", @salary " & _
            '              ", @increment " & _
            '              ", @maximum " & _
            '              ", @midpoint " & _
            '              ", @audittype " & _
            '              ", @audituserunkid " & _
            '              ", @auditdatetime " & _
            '              ", @ip " & _
            '              ", @machine_name" & _
            '"); SELECT @@identity" 'Sohail (31 Mar 2012) - [midpoint]

            strQ = "INSERT INTO atprwages_tran ( " & _
                          "  wagestranunkid " & _
                          ", wagesunkid " & _
                          ", gradeunkid " & _
                          ", gradelevelunkid " & _
                          ", salary " & _
                          ", increment " & _
                          ", maximum " & _
                          ", midpoint " & _
                          ", periodunkid " & _
                          ", audittype " & _
                          ", audituserunkid " & _
                          ", auditdatetime " & _
                          ", ip " & _
                          ", machine_name" & _
                       ", form_name " & _
                       ", module_name1 " & _
                       ", module_name2 " & _
                       ", module_name3 " & _
                       ", module_name4 " & _
                       ", module_name5 " & _
                       ", isweb " & _
            ") VALUES (" & _
                          "  @wagestranunkid " & _
                          ", @wagesunkid " & _
                          ", @gradeunkid " & _
                          ", @gradelevelunkid " & _
                          ", @salary " & _
                          ", @increment " & _
                          ", @maximum " & _
                          ", @midpoint " & _
                          ", @periodunkid " & _
                          ", @audittype " & _
                          ", @audituserunkid " & _
                          ", @auditdatetime " & _
                          ", @ip " & _
                          ", @machine_name" & _
                       ", @form_name " & _
                       ", @module_name1 " & _
                       ", @module_name2 " & _
                       ", @module_name3 " & _
                       ", @module_name4 " & _
                       ", @module_name5 " & _
                       ", @isweb " & _
            "); SELECT @@identity" 'Sohail (31 Mar 2012) - [midpoint]

            'S.SANDEEP [ 19 JULY 2012 ] -- END


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@wagestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWagestranunkid.ToString)
            objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWagesUnkID.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@salary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSalary.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@maximum", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaximum.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@midpoint", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMidpoint.ToString) 'Sohail (31 Mar 2012)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid) 'Sohail (27 Apr 2016)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            'S.SANDEEP [ 19 JULY 2012 ] -- END


            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForWagesTran", mstrModuleName)
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End
        Finally
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Nov 2010) -- End

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  wagestranunkid " & _
    '          ", wagesunkid " & _
    '          ", gradeunkid " & _
    '          ", gradelevelunkid " & _
    '          ", salary " & _
    '          ", increment " & _
    '          ", maximum " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '         "FROM prwages_tran "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE isactive = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (prwages_tran) </purpose>
    'Public Function Insert() As Boolean
    '    If isExist(mstrName) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@wagesunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintwagesunkid.ToString)
    '        objDataOperation.AddParameter("@gradeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgradeunkid.ToString)
    '        objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgradelevelunkid.ToString)
    '        objDataOperation.AddParameter("@salary", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblsalary.ToString)
    '        objDataOperation.AddParameter("@increment", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblincrement.ToString)
    '        objDataOperation.AddParameter("@maximum", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblmaximum.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)

    '        StrQ = "INSERT INTO prwages_tran ( " & _
    '          "  wagesunkid " & _
    '          ", gradeunkid " & _
    '          ", gradelevelunkid " & _
    '          ", salary " & _
    '          ", increment " & _
    '          ", maximum " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime" & _
    '        ") VALUES (" & _
    '          "  @wagesunkid " & _
    '          ", @gradeunkid " & _
    '          ", @gradelevelunkid " & _
    '          ", @salary " & _
    '          ", @increment " & _
    '          ", @maximum " & _
    '          ", @isvoid " & _
    '          ", @voiduserunkid " & _
    '          ", @voiddatetime" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintWagesTranUnkId = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prwages_tran) </purpose>
    'Public Function Update() As Boolean
    '    If isExist(mstrName, mintWagestranunkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@wagestranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintwagestranunkid.ToString)
    '        objDataOperation.AddParameter("@wagesunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintwagesunkid.ToString)
    '        objDataOperation.AddParameter("@gradeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgradeunkid.ToString)
    '        objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgradelevelunkid.ToString)
    '        objDataOperation.AddParameter("@salary", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblsalary.ToString)
    '        objDataOperation.AddParameter("@increment", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblincrement.ToString)
    '        objDataOperation.AddParameter("@maximum", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblmaximum.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)

    '        StrQ = "UPDATE prwages_tran SET " & _
    '          "  wagesunkid = @wagesunkid" & _
    '          ", gradeunkid = @gradeunkid" & _
    '          ", gradelevelunkid = @gradelevelunkid" & _
    '          ", salary = @salary" & _
    '          ", increment = @increment" & _
    '          ", maximum = @maximum" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime " & _
    '        "WHERE wagestranunkid = @wagestranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prwages_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM prwages_tran " & _
    '        "WHERE wagestranunkid = @wagestranunkid "

    '        objDataOperation.AddParameter("@wagestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@wagestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

   

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class