﻿'************************************************************************************************************************************
'Class Name : clsWages.vb
'Purpose    :
'Date       :02/07/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsWages
    Private Shared ReadOnly mstrModuleName As String = "clsWages"
    Dim objDataOperation As clsDataOperation
    Dim objWagesTran As New clsWagesTran
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintWagesunkid As Integer
    Private mintGradegroupunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintPeriodunkid As Integer 'Sohail (27 Apr 2016)

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set wagesunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Wagesunkid() As Integer
        Get
            Return mintWagesunkid
        End Get
        Set(ByVal value As Integer)
            mintWagesunkid = Value
            'Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradegroupunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Gradegroupunkid() As Integer
        Get
            Return mintGradegroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGradegroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END


    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property
    'Sohail (27 Apr 2016) -- End


#End Region

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  wagesunkid " & _
              ", gradegroupunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", periodunkid " & _
             "FROM prwages_master " & _
             "WHERE wagesunkid = @wagesunkid " 'Sohail (16 Oct 2010)
            'Sohail (27 Apr 2016) - [periodunkid]

            objDataOperation.AddParameter("@wagesunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintWagesUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintWagesunkid = CInt(dtRow.Item("wagesunkid").ToString)
                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid").ToString)
                mintUserunkid = CInt(dtRow.Item("userunkid").ToString)
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If dtRow.Item("voiduserunkid").ToString = Nothing Then
                    mintVoiduserunkid = Nothing
                Else
                    mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid").ToString)
                End If

                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime").ToString
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (27 Apr 2016) -- Start
                'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                mintPeriodunkid = Int(dtRow.Item("periodunkid").ToString)
                'Sohail (27 Apr 2016) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal intGradeGroupUnkID As Integer, Optional ByVal strTableName As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                "  wagesunkid " & _
                ", gradegroupunkid " & _
                ", userunkid " & _
                ", isvoid " & _
                ", voiduserunkid " & _
                ", voiddatetime " & _
                ", voidreason " & _
                ", periodunkid " & _
            "FROM prwages_master " & _
            "WHERE ISNULL(isvoid,0) = 0" & _
            "AND gradegroupunkid = @gradegroupunkid " 'Sohail (16 Oct 2010)
            'Sohail (27 Apr 2016) - [periodunkid]

            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeGroupUnkID)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    Public Function InsertUpdateDelete(ByVal dtTran As DataTable, ByVal dtCurrentDateAndTime As Date, ByVal intPeriodUnkId As Integer) As Boolean
        'Sohail (27 Apr 2016) - [intPeriodUnkId]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'If isExist(mintGradegroupunkid) = False Then
            Dim arrGGroup() As String = (From p In dtTran Select (p.Item("gradegroupunkid").ToString)).Distinct().ToArray
            Dim intExistWagesUnkId As Integer = 0

            For Each strGGroup In arrGGroup
                mintGradegroupunkid = strGGroup

                If isExist(mintGradegroupunkid, intPeriodUnkId, intExistWagesUnkId) = False Then
                    'Sohail (27 Apr 2016) -- End

                objDataOperation.ClearParameters()

                strQ = "INSERT INTO prwages_master ( " & _
                    "  gradegroupunkid " & _
                    ", userunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voiddatetime" & _
                    ", voidreason " & _
                        ", periodunkid " & _
                ") VALUES (" & _
                    "  @gradegroupunkid " & _
                    ", @userunkid " & _
                    ", @isvoid " & _
                    ", @voiduserunkid " & _
                    ", @voiddatetime" & _
                    ", @voidreason " & _
                        ", @periodunkid " & _
                    "); SELECT @@identity"
                    'Sohail (27 Apr 2016) - [periodunkid]


                objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                If mdtVoiddatetime = Nothing Then

                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                End If
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    'Nilay (06-Jun-2016) -- Start
                    'Enhancement : Import option in Wages Table for KBC
                    'objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid) 'Sohail (27 Apr 2016)
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
                    'Nilay (06-Jun-2016) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintWagesunkid = dsList.Tables(0).Rows(0).Item(0)

                'Sohail (11 Nov 2010) -- Start

                'Anjan (11 Jun 2011)-Start
                'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
                'Call InsertAuditTrailForWagesMaster(objDataOperation, 1)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If InsertAuditTrailForWagesMaster(objDataOperation, 1) = False Then
                If InsertAuditTrailForWagesMaster(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'Anjan (11 Jun 2011)-End
                'Sohail (11 Nov 2010) -- End

                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    'If dtTran.Rows.Count > 0 Then
                    Dim dtWTran As DataTable = (From p In dtTran Where (CInt(p.Item("gradegroupunkid")) = mintGradegroupunkid) Select (p)).ToList.CopyToDataTable

                    If dtWTran.Rows.Count > 0 Then
                        'Sohail (27 Apr 2016) -- End
                    'objWagesTran._WageUnkID = mintWagesunkid
                    'objWagesTran._DataList = dtTran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objWagesTran.InserUpdateDeleteWagesTran(dtTran, mintWagesunkid) Then

                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        'If objWagesTran.InserUpdateDeleteWagesTran(dtTran, mintWagesunkid, mintUserunkid, dtCurrentDateAndTime) Then
                        If objWagesTran.InserUpdateDeleteWagesTran(dtWTran, mintWagesunkid, mintUserunkid, dtCurrentDateAndTime) Then
                            'Sohail (27 Apr 2016) -- End
                        'Sohail (21 Aug 2015) -- End
                            'objDataOperation.ReleaseTransaction(True) 'Sohail (27 Apr 2016)
                    Else
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                End If

            Else

                'Sohail (22 Jul 2010) -- Start
                'objDataOperation = New clsDataOperation
                'objDataOperation.BindTransaction()
                'Sohail (22 Jul 2010) -- End
                objDataOperation.ClearParameters()

                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    mintWagesunkid = intExistWagesUnkId
                    'Sohail (27 Apr 2016) -- End

                strQ = "UPDATE prwages_master SET " & _
                    "  gradegroupunkid = @gradegroupunkid" & _
                    ", userunkid = @userunkid" & _
                    ", isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime " & _
                    ", voidreason = @voidreason " & _
                        ", periodunkid = @periodunkid " & _
                "WHERE wagesunkid = @wagesunkid "

                objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWagesunkid.ToString)
                objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                If mdtVoiddatetime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                End If
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    'Nilay (06-Jun-2016) -- Start
                    'Enhancement : Import option in Wages Table for KBC
                    'objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid) 'Sohail (27 Apr 2016)
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
                    'Nilay (06-Jun-2016) -- End

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (11 Nov 2010) -- Start
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call InsertAuditTrailForWagesMaster(objDataOperation, 2)
                If InsertAuditTrailForWagesMaster(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'Sohail (21 Aug 2015) -- End
                'Sohail (11 Nov 2010) -- End

                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    'If dtTran.Rows.Count > 0 Then
                    Dim dtWTran As DataTable = (From p In dtTran Where (CInt(p.Item("gradegroupunkid")) = mintGradegroupunkid) Select (p)).ToList.CopyToDataTable

                    If dtWTran.Rows.Count > 0 Then
                        'Sohail (27 Apr 2016) -- End
                    'objWagesTran._WageUnkID = mintWagesunkid
                    'objWagesTran._DataList = dtTran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objWagesTran.InserUpdateDeleteWagesTran(dtTran, mintWagesunkid) Then
                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        'If objWagesTran.InserUpdateDeleteWagesTran(dtTran, mintWagesunkid, mintUserunkid, dtCurrentDateAndTime) Then
                        If objWagesTran.InserUpdateDeleteWagesTran(dtWTran, mintWagesunkid, mintUserunkid, dtCurrentDateAndTime) Then
                            'Sohail (27 Apr 2016) -- End
                        'Sohail (21 Aug 2015) -- End
                            'objDataOperation.ReleaseTransaction(True) 'Sohail (27 Apr 2016)
                    Else
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

            End If

            Next 'Sohail (27 Apr 2016)
            

            '            objDataOperation.ReleaseTransaction(True)
            objDataOperation.ReleaseTransaction(True)   'Sohail (27 Apr 2016)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Database Table (prwages_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        '<TODO uncomment when isused method is done
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            'strQ = "DELETE FROM prwages_master " & _
            '"WHERE wagesunkid = @wagesunkid "
            strQ = "UPDATE prwages_master SET " & _
                ", userunkid = @userunkid" & _
                ", isvoid = 1" & _
                ", voiduserunkid = @voiduserunkid" & _
                ", voiddatetime = @voiddatetime " & _
                ", voidreason = @voidreason " & _
            "WHERE wagesunkid = @wagesunkid "

            objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objWagesTran.Void(intUnkid, mintVoiduserunkid, dtVoidDateTime, strVoidReason) = True Then
                objDataOperation.ReleaseTransaction(True)
            Else
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (prwages_master) </purpose>
    'Public Function Insert(Optional ByVal dtTran As DataTable = Nothing) As Boolean
    '    If isExist(mstrModuleName, mstrModuleName) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    Try
    '        objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        strQ = "INSERT INTO prwages_master ( " & _
    '          "  gradegroupunkid " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime" & _
    '        ") VALUES (" & _
    '          "  @gradegroupunkid " & _
    '          ", @userunkid " & _
    '          ", @isvoid " & _
    '          ", @voiduserunkid " & _
    '          ", @voiddatetime" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintWagesunkid = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prwages_master) </purpose>
    'Public Function Update() As Boolean
    '    If isExist(mstrModuleName, mstrModuleName, mintWagesunkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@wagesunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintwagesunkid.ToString)
    '        objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgradegroupunkid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        strQ = "UPDATE prwages_master SET " & _
    '          "  gradegroupunkid = @gradegroupunkid" & _
    '          ", userunkid = @userunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime " & _
    '        "WHERE wagesunkid = @wagesunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            '<TODO make query>
            strQ = "<Query>"

            objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intGradeGroupUnkid As Integer _
                            , ByVal intPeriodUnkId As Integer _
                            , Optional ByRef intWagesUnkId As Integer = 0 _
                            ) As Boolean
        'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        'Sohail (27 Apr 2016) - [intPeriodUnkId, intWagesUnkId]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  wagesunkid " & _
              ", gradegroupunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prwages_master " & _
             "WHERE gradegroupunkid = @gradegroupunkid AND ISNULL(isvoid,0) = 0 " & _
             " AND periodunkid = @periodunkid "
            'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeGroupUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId) 'Sohail (27 Apr 2016)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            If dsList.Tables(0).Rows.Count > 0 Then
                intWagesUnkId = CInt(dsList.Tables(0).Rows(0).Item("wagesunkid"))
            Else
                intWagesUnkId = 0
            End If
            'Sohail (27 Apr 2016) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (11 Nov 2010) -- Start

    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForWagesMaster(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForWagesMaster(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
        'Anjan (11 Jun 2011)-End

        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atprwages_master ( " & _
            '           "  wagesunkid " & _
            '           ", gradegroupunkid " & _
            '           ", audittype " & _
            '           ", audituserunkid " & _
            '           ", auditdatetime " & _
            '           ", ip " & _
            '           ", machine_name" & _
            ' ") VALUES (" & _
            '           "  @wagesunkid " & _
            '           ", @gradegroupunkid " & _
            '           ", @audittype " & _
            '           ", @audituserunkid " & _
            '           ", @auditdatetime " & _
            '           ", @ip " & _
            '           ", @machine_name" & _
            ' "); SELECT @@identity"

            strQ = "INSERT INTO atprwages_master ( " & _
                      "  wagesunkid " & _
                      ", gradegroupunkid " & _
                      ", periodunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name" & _
                   ", form_name " & _
                   ", module_name1 " & _
                   ", module_name2 " & _
                   ", module_name3 " & _
                   ", module_name4 " & _
                   ", module_name5 " & _
                   ", isweb " & _
            ") VALUES (" & _
                      "  @wagesunkid " & _
                      ", @gradegroupunkid " & _
                      ", @periodunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @machine_name" & _
                   ", @form_name " & _
                   ", @module_name1 " & _
                   ", @module_name2 " & _
                   ", @module_name3 " & _
                   ", @module_name4 " & _
                   ", @module_name5 " & _
                   ", @isweb " & _
            "); SELECT @@identity"

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@wagesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWagesunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid) 'Sohail (27 Apr 2016)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForWagesMaster", mstrModuleName)
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End
        Finally
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Nov 2010) -- End
    
    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Public Function GetWagesList(ByVal dtPeriodEnd As Date _
                                 , ByVal intGradeGroupUnkID As Integer _
                                 , ByVal intGradeUnkID As Integer _
                                 , Optional ByVal strTableName As String = "List" _
                                 ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strF1 As String = ""
        Dim strF2 As String = ""

        objDataOperation = New clsDataOperation

        Try

            If dtPeriodEnd <> Nothing Then
                'Sohail (21 Jul 2016) -- Start
                'Issue - 63.1 - Scales are not coming for current period if current date is less than period end date
                'strF1 &= " AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "
                strF1 &= " AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "
                'Sohail (21 Jul 2016) -- End
            End If

            If intGradeGroupUnkID > 0 Then
                strF1 &= " AND prwages_master.gradegroupunkid = @gradegroupunkid "
                strF2 &= " AND hrgradegroup_master.gradegroupunkid = @gradegroupunkid "
                objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeGroupUnkID)
            End If

            If intGradeUnkID > 0 Then
                strF1 &= " AND prwages_tran.gradeunkid = @gradeunkid "
                strF2 &= " AND hrgrade_master.gradeunkid = @gradeunkid "
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeUnkID)
            End If

            strQ = "WITH    cte " & _
                              "AS ( SELECT   * " & _
                                   "FROM     ( SELECT    prwages_tran.wagestranunkid  " & _
                                                      ", prwages_master.wagesunkid " & _
                                                      ", prwages_master.gradegroupunkid " & _
                                                      ", prwages_tran.gradeunkid " & _
                                                      ", prwages_tran.gradelevelunkid " & _
                                                      ", hrgradegroup_master.code AS GradeGroupCode " & _
                                                      ", hrgradegroup_master.name AS GradeGroupName " & _
                                                      ", hrgrade_master.code AS GradeCode " & _
                                                      ", hrgrade_master.name AS GradeName " & _
                                                      ", hrgradelevel_master.code AS GradeLevelCode " & _
                                                      ", hrgradelevel_master.name AS GradeLevelName " & _
                                                      ", prwages_tran.salary AS Minimum " & _
                                                      ", prwages_tran.Midpoint " & _
                                                      ", prwages_tran.Increment " & _
                                                      ", prwages_tran.Maximum " & _
                                                      ", prwages_master.periodunkid " & _
                                                      ", cfcommon_period_tran.period_code " & _
                                                      ", cfcommon_period_tran.period_name " & _
                                                      ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                                                      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                                                      ", cfcommon_period_tran.statusid " & _
                                                      ", prwages_tran.isvoid " & _
                                                      ", prwages_tran.voiduserunkid " & _
                                                      ", prwages_tran.voiddatetime " & _
                                                      ", prwages_tran.voidreason " & _
                                                      ", DENSE_RANK() OVER ( PARTITION BY prwages_master.gradegroupunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                              "FROM      prwages_master " & _
                                                        "LEFT JOIN prwages_tran ON prwages_master.wagesunkid = prwages_tran.wagesunkid " & _
                                                                                  "AND prwages_master.periodunkid = prwages_tran.periodunkid " & _
                                                        "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = prwages_master.gradegroupunkid " & _
                                                        "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prwages_tran.gradeunkid " & _
                                                        "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = prwages_tran.gradelevelunkid " & _
                                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prwages_master.periodunkid " & _
                                              "WHERE     prwages_master.isvoid = 0 " & _
                                                        "AND prwages_tran.isvoid = 0 " & _
                                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                                        "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                                        strF1 & _
                                            ") AS A " & _
                                    "WHERE    A.ROWNO = 1 " & _
                         ") " & _
                "SELECT  * " & _
                "FROM    cte " & _
                "UNION ALL " & _
                "SELECT  -1 AS wagestranunkid  " & _
                      ", -1 AS wagesunkid " & _
                      ", hrgradegroup_master.gradegroupunkid " & _
                      ", hrgrade_master.gradeunkid " & _
                      ", hrgradelevel_master.gradelevelunkid " & _
                      ", hrgradegroup_master.code AS GradeGroupCode " & _
                      ", hrgradegroup_master.name AS GradeGroupName " & _
                      ", hrgrade_master.code AS GradeCode " & _
                      ", hrgrade_master.name AS GradeName " & _
                      ", hrgradelevel_master.code AS GradeLevelCode " & _
                      ", hrgradelevel_master.name AS GradeLevelName " & _
                      ", 0.00 AS salary " & _
                      ", 0.00 AS midpoint " & _
                      ", 0.00 AS increment " & _
                      ", 0.00 AS maximum " & _
                      ", A.periodunkid " & _
                      ", A.period_code " & _
                      ", A.period_name " & _
                      ", CONVERT(CHAR(8), A.start_date, 112) AS start_date " & _
                      ", CONVERT(CHAR(8), A.end_date, 112) AS end_date " & _
                      ", A.statusid " & _
                      ", CAST(0 AS BIT) AS isvoid " & _
                      ", -1 AS voiduserunkid " & _
                      ", CAST(NULL AS DATETIME) AS voiddatetime " & _
                      ", '' AS voidreason " & _
                      ", 1 AS ROWNO " & _
                "FROM    hrgradegroup_master " & _
                        "LEFT JOIN hrgrade_master ON hrgrade_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                        "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradeunkid = hrgrade_master.gradeunkid " & _
                        "LEFT JOIN cte ON cte.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                                         "AND cte.gradeunkid = hrgrade_master.gradeunkid " & _
                                         "AND cte.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                        "LEFT JOIN ( SELECT TOP 1 " & _
                                            "* " & _
                                    "FROM    cte " & _
                                  ") AS A ON 1 = 1 " & _
                "WHERE   hrgradegroup_master.isactive = 1 " & _
                        "AND hrgrade_master.isactive = 1 " & _
                        "AND hrgradelevel_master.isactive = 1 " & _
                        strF2 & _
                        "AND cte.wagestranunkid IS NULL "

            strQ &= " ORDER BY GradeGroupName, GradeName, GradeLevelName "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetWagesList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (27 Apr 2016) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class