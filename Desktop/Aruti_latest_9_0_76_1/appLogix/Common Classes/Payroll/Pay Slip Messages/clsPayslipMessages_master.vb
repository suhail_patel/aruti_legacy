﻿'************************************************************************************************************************************
'Class Name : clsPayslipMessages_master.vb
'Purpose    :
'Date       :06/09/2010
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsPayslipMessages_master
    Private Shared ReadOnly mstrModuleName As String = "clsPayslipMessages_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMessageunkid As Integer
    Private mintAnalysisref_Id As Integer
    Private mintAnalysistranunkid As Integer
    Private mstrMsg1 As String = String.Empty
    Private mstrMsg2 As String = String.Empty
    Private mstrMsg3 As String = String.Empty
    Private mstrMsg4 As String = String.Empty
    Private mblnIsglobalmessage As Boolean
    Private mintUserunkid As Integer
    Private mblnIsactive As Boolean = True
    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    Private mintPeriodunkid As Integer = 0
    'Nilay (28-Apr-2016) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set messageunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Messageunkid() As Integer
        Get
            Return mintMessageunkid
        End Get
        Set(ByVal value As Integer)
            mintMessageunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysisref_id
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Analysisref_Id() As Integer
        Get
            Return mintAnalysisref_Id
        End Get
        Set(ByVal value As Integer)
            mintAnalysisref_Id = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysistranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Analysistranunkid() As Integer
        Get
            Return mintAnalysistranunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysistranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set msg1
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Msg1() As String
        Get
            Return mstrMsg1
        End Get
        Set(ByVal value As String)
            mstrMsg1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set msg2
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Msg2() As String
        Get
            Return mstrMsg2
        End Get
        Set(ByVal value As String)
            mstrMsg2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set msg3
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Msg3() As String
        Get
            Return mstrMsg3
        End Get
        Set(ByVal value As String)
            mstrMsg3 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set msg4
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Msg4() As String
        Get
            Return mstrMsg4
        End Get
        Set(ByVal value As String)
            mstrMsg4 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isglobalmessage
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isglobalmessage() As Boolean
        Get
            Return mblnIsglobalmessage
        End Get
        Set(ByVal value As Boolean)
            mblnIsglobalmessage = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property
    'Nilay (01-Apr-2016) -- End

#End Region

    Public Enum enPayslipMsgType
        PAYSLIP_ANALYSIS_MESSAGE = 1
        PAYSLIP_GLOBAL_MESSAGE = 2
    End Enum

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  messageunkid " & _
              ", analysisref_id " & _
              ", analysistranunkid " & _
              ", msg1 " & _
              ", msg2 " & _
              ", msg3 " & _
              ", msg4 " & _
              ", isglobalmessage " & _
              ", userunkid " & _
              ", isactive " & _
              ", periodunkid " & _
             "FROM prpayslipmessages_master " & _
             "WHERE messageunkid = @messageunkid "
            'Nilay (28-Apr-2016) -- [periodunkid]

            objDataOperation.AddParameter("@messageunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintMessageUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintmessageunkid = CInt(dtRow.Item("messageunkid"))
                mintanalysisref_id = CInt(dtRow.Item("analysisref_id"))
                mintanalysistranunkid = CInt(dtRow.Item("analysistranunkid"))
                mstrmsg1 = dtRow.Item("msg1").ToString
                mstrmsg2 = dtRow.Item("msg2").ToString
                mstrmsg3 = dtRow.Item("msg3").ToString
                mstrmsg4 = dtRow.Item("msg4").ToString
                mblnisglobalmessage = CBool(dtRow.Item("isglobalmessage"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'Nilay (28-Apr-2016) -- Start
                'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                'Nilay (28-Apr-2016) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            ByVal blnIsGlobaMsg As Boolean, _
                            Optional ByVal intAnalysisRefID As Integer = 0, _
                            Optional ByVal intEmpID As Integer = 0, _
                            Optional ByVal intGradeID As Integer = 0, _
                            Optional ByVal intCostCenterID As Integer = 0, _
                            Optional ByVal intPayPintID As Integer = 0, _
                            Optional ByVal intDepartmentID As Integer = 0, _
                            Optional ByVal intSectiontID As Integer = 0, _
                            Optional ByVal intJobID As Integer = 0, _
                            Optional ByVal intClassID As Integer = 0, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal intPeriodId As Integer = 0) As DataSet
        'Sohail (28 Apr 2016) - [intPeriodId]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            'Gajanan [1-July-2020] -- Start

            strQ = "SELECT " & _
                              "employeeunkid " & _
                            ",firstname " & _
                            ",surname " & _
                            "INTO #tmp_employee " & _
                         "FROM hremployee_master "


            If xIncludeIn_ActiveEmployee = False AndAlso blnIsGlobaMsg = False Then
                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If
            End If


            If blnApplyUserAccessFilter = True Then
                If blnIsGlobaMsg = False AndAlso xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            strQ &= "WHERE 1 = 1 "

            If xIncludeIn_ActiveEmployee = False AndAlso blnIsGlobaMsg = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If blnApplyUserAccessFilter = True Then
                If blnIsGlobaMsg = False AndAlso xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If
            'Gajanan [1-July-2020] -- End

            'Sohail (28 Apr 2016) -- Start
            'Enhancement - 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that if you print payslip for last month and you had a new payslip message this month, it does not pick message of current month but last month
            strQ &= "SELECT  A.* " & _
                    "FROM    ( "
            'Sohail (28 Apr 2016) -- End

            strQ &= "SELECT  prpayslipmessages_master.messageunkid " & _
                          ", prpayslipmessages_master.analysisref_id " & _
                          ", Analysis = CASE analysisref_id " & _
                                          "WHEN 1  THEN @employee " & _
                                          "WHEN 2  THEN @grade " & _
                                          "WHEN 5  THEN @costcenter " & _
                                          "WHEN 6  THEN @paypoint " & _
                                          "WHEN 7  THEN @department " & _
                                          "WHEN 8  THEN @section " & _
                                          "WHEN 9  THEN @job " & _
                                          "WHEN 10 THEN @class " & _
                                          "ELSE         @undefined " & _
                                        "END " & _
                          ", prpayslipmessages_master.analysistranunkid " & _
                          ", AnalysisName = CASE analysisref_id " & _
                                             "WHEN 1 THEN ISNULL(#tmp_employee.firstname + ' ' + #tmp_employee.surname,'') " & _
                                             "WHEN 2 THEN ISNULL(hrgrade_master.name,'') " & _
                                             "WHEN 5 THEN ISNULL(prcostcenter_master.costcentername,'') " & _
                                             "WHEN 6 THEN ISNULL(prpaypoint_master.paypointname,'')  " & _
                                             "WHEN 7 THEN ISNULL(hrdepartment_master.name,'')  " & _
                                             "WHEN 8 THEN ISNULL(hrsection_master.name,'')  " & _
                                             "WHEN 9 THEN ISNULL(hrjob_master.job_name,'')  " & _
                                             "WHEN 10 THEN ISNULL(hrclasses_master.name,'')  " & _
                                             "ELSE 'Undefined' " & _
                                           "END " & _
                          ", prpayslipmessages_master.msg1 " & _
                          ", prpayslipmessages_master.msg2 " & _
                          ", prpayslipmessages_master.msg3 " & _
                          ", prpayslipmessages_master.msg4 " & _
                          ", prpayslipmessages_master.isglobalmessage " & _
                          ", prpayslipmessages_master.userunkid " & _
                          ", prpayslipmessages_master.isactive " & _
                          ", prpayslipmessages_master.periodunkid " & _
                          ", cfcommon_period_tran.period_code " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                          ", cfcommon_period_tran.statusid " & _
                          ", DENSE_RANK() OVER ( PARTITION BY prpayslipmessages_master.analysisref_id, prpayslipmessages_master.analysistranunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                    "FROM    prpayslipmessages_master " & _
                            "LEFT JOIN #tmp_employee ON #tmp_employee.employeeunkid = prpayslipmessages_master.analysistranunkid AND prpayslipmessages_master.analysisref_id = 1 " & _
                            "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prpayslipmessages_master.analysistranunkid " & _
                            "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = prpayslipmessages_master.analysistranunkid " & _
                            "LEFT JOIN prpaypoint_master ON prpaypoint_master.paypointunkid = prpayslipmessages_master.analysistranunkid " & _
                            "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = prpayslipmessages_master.analysistranunkid " & _
                            "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = prpayslipmessages_master.analysistranunkid " & _
                            "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = prpayslipmessages_master.analysistranunkid " & _
                            "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = prpayslipmessages_master.analysistranunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prpayslipmessages_master.periodunkid "
            'Sohail (28 Apr 2016) -- Start
            'Enhancement - 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that if you print payslip for last month and you had a new payslip message this month, it does not pick message of current month but last month
            '[periodunkid, period_code, period_name, start_date, end_date, statusid, ROWNO], [LEFT JOIN cfcommon_period_tran]
            'Sohail (28 Apr 2016) -- End


            'Gajanan [1-July-2020] -- Start


            'If xIncludeIn_ActiveEmployee = False AndAlso blnIsGlobaMsg = False Then
            '    If xDateJoinQry.Trim.Length > 0 Then
            '        strQ &= xDateJoinQry
            '    End If
            'End If


            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            '    If blnIsGlobaMsg = False AndAlso xUACQry.Trim.Length > 0 Then
            '        strQ &= xUACQry
            '    End If
            'End If
            'If blnApplyUserAccessFilter = True Then
            '    If blnIsGlobaMsg = False AndAlso xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END
            'Gajanan [1-July-2020] -- End

            strQ &= "WHERE prpayslipmessages_master.isglobalmessage = @isglobalmessage "

            'Sohail (28 Apr 2016) -- Start
            'Enhancement - 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that if you print payslip for last month and you had a new payslip message this month, it does not pick message of current month but last month
            strQ &= "AND cfcommon_period_tran.isactive = 1 " & _
                    "AND cfcommon_period_tran.modulerefid = 1 "

            If intPeriodId > 0 Then
                strQ &= "AND cfcommon_period_tran.end_date <= '" & eZeeDate.convertDate(xPeriodEnd) & "' "
            End If
            'Sohail (28 Apr 2016) -- End

            If blnOnlyActive Then
                strQ &= " AND prpayslipmessages_master.isactive = 1 "
            End If



            If intAnalysisRefID > 0 Then
                strQ &= " AND prpayslipmessages_master.analysisref_id = @analysisref_id "
                objDataOperation.AddParameter("@analysisref_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisRefID.ToString)
            End If
            If intEmpID > 0 Then
                strQ &= " AND #tmp_employee.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID.ToString)
            End If
            If intGradeID > 0 Then
                strQ &= " AND hrgrade_master.gradeunkid = @gradeunkid "
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeID.ToString)
            End If
            If intCostCenterID > 0 Then
                strQ &= " AND prcostcenter_master.costcenterunkid = @costcenterunkid "
                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostCenterID.ToString)
            End If
            If intPayPintID > 0 Then
                strQ &= " AND prpaypoint_master.paypointunkid = @paypointunkid "
                objDataOperation.AddParameter("@paypointunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayPintID.ToString)
            End If
            If intDepartmentID > 0 Then
                strQ &= " AND hrdepartment_master.departmentunkid = @departmentunkid "
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentID.ToString)
            End If
            If intSectiontID > 0 Then
                strQ &= " AND hrsection_master.sectionunkid = @sectionunkid "
                objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectiontID.ToString)
            End If
            If intJobID > 0 Then
                strQ &= " AND hrjob_master.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID.ToString)
            End If
            If intClassID > 0 Then
                strQ &= " AND hrclasses_master.classesunkid = @classesunkid "
                objDataOperation.AddParameter("@classesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassID.ToString)
            End If

            'Sohail (28 Apr 2016) -- Start
            'Enhancement - 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that if you print payslip for last month and you had a new payslip message this month, it does not pick message of current month but last month
            'strQ &= " ORDER BY analysisref_id "
            strQ &= ") AS A " & _
                    "WHERE   1 = 1 " & _
                    " AND a.AnalysisName <> '' "


            If intPeriodId > 0 Then
                strQ &= " AND A.ROWNO = 1 "
            End If

            strQ &= " ORDER BY A.analysisref_id "

            'Gajanan [1-July-2020] -- Start
            strQ &= " DROP TABLE #tmp_employee "
            'Gajanan [1-July-2020] -- End

            'Sohail (28 Apr 2016) -- End

            objDataOperation.AddParameter("@isglobalmessage", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsGlobaMsg.ToString)
            objDataOperation.AddParameter("@employee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Employee"))
            objDataOperation.AddParameter("@grade", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Grade"))
            objDataOperation.AddParameter("@costcenter", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Cost Center"))
            objDataOperation.AddParameter("@paypoint", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pay Point"))
            objDataOperation.AddParameter("@department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Department in Department Group"))
            objDataOperation.AddParameter("@section", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Section in Department"))
            objDataOperation.AddParameter("@job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Job in Job Group"))
            objDataOperation.AddParameter("@class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Class in Class Group"))
            objDataOperation.AddParameter("@undefined", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Undefined"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, ByVal blnIsGlobaMsg As Boolean, Optional ByVal intAnalysisRefID As Integer = 0, _
    '                        Optional ByVal intEmpID As Integer = 0, _
    '                        Optional ByVal intGradeID As Integer = 0, _
    '                        Optional ByVal intCostCenterID As Integer = 0, _
    '                        Optional ByVal intPayPintID As Integer = 0, _
    '                        Optional ByVal intDepartmentID As Integer = 0, _
    '                        Optional ByVal intSectiontID As Integer = 0, _
    '                        Optional ByVal intJobID As Integer = 0, _
    '                        Optional ByVal intClassID As Integer = 0, _
    '                        Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        strQ = "SELECT  prpayslipmessages_master.messageunkid " & _
    '                      ", prpayslipmessages_master.analysisref_id " & _
    '                      ", Analysis = CASE analysisref_id " & _
    '                                      "WHEN 1  THEN @employee " & _
    '                                      "WHEN 2  THEN @grade " & _
    '                                      "WHEN 5  THEN @costcenter " & _
    '                                      "WHEN 6  THEN @paypoint " & _
    '                                      "WHEN 7  THEN @department " & _
    '                                      "WHEN 8  THEN @section " & _
    '                                      "WHEN 9  THEN @job " & _
    '                                      "WHEN 10 THEN @class " & _
    '                                      "ELSE         @undefined " & _
    '                                    "END " & _
    '                      ", prpayslipmessages_master.analysistranunkid " & _
    '                      ", AnalysisName = CASE analysisref_id " & _
    '                                         "WHEN 1 THEN ISNULL(hremployee_master.firstname + ' ' + hremployee_master.surname,'') " & _
    '                                         "WHEN 2 THEN ISNULL(hrgrade_master.name,'') " & _
    '                                         "WHEN 5 THEN ISNULL(prcostcenter_master.costcentername,'') " & _
    '                                         "WHEN 6 THEN ISNULL(prpaypoint_master.paypointname,'')  " & _
    '                                         "WHEN 7 THEN ISNULL(hrdepartment_master.name,'')  " & _
    '                                         "WHEN 8 THEN ISNULL(hrsection_master.name,'')  " & _
    '                                         "WHEN 9 THEN ISNULL(hrjob_master.job_name,'')  " & _
    '                                         "WHEN 10 THEN ISNULL(hrclasses_master.name,'')  " & _
    '                                         "ELSE 'Undefined' " & _
    '                                       "END " & _
    '                      ", prpayslipmessages_master.msg1 " & _
    '                      ", prpayslipmessages_master.msg2 " & _
    '                      ", prpayslipmessages_master.msg3 " & _
    '                      ", prpayslipmessages_master.msg4 " & _
    '                      ", prpayslipmessages_master.isglobalmessage " & _
    '                      ", prpayslipmessages_master.userunkid " & _
    '                      ", prpayslipmessages_master.isactive " & _
    '                "FROM    prpayslipmessages_master " & _
    '                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayslipmessages_master.analysistranunkid " & _
    '                        "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prpayslipmessages_master.analysistranunkid " & _
    '                        "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = prpayslipmessages_master.analysistranunkid " & _
    '                        "LEFT JOIN prpaypoint_master ON prpaypoint_master.paypointunkid = prpayslipmessages_master.analysistranunkid " & _
    '                        "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = prpayslipmessages_master.analysistranunkid " & _
    '                        "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = prpayslipmessages_master.analysistranunkid " & _
    '                        "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = prpayslipmessages_master.analysistranunkid " & _
    '                        "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = prpayslipmessages_master.analysistranunkid " & _
    '                "WHERE prpayslipmessages_master.isglobalmessage = @isglobalmessage "

    '        If blnOnlyActive Then
    '            strQ &= " AND prpayslipmessages_master.isactive = 1 "
    '        End If

    '        'Anjan (09 Aug 2011)-Start
    '        'Issue : For including setting of acitve and inactive employee.
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False AndAlso blnIsGlobaMsg = False Then
    '            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'Anjan (09 Aug 2011)-End 


    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.
    '        If blnIsGlobaMsg = False AndAlso UserAccessLevel._AccessLevel.Length > 0 Then
    '            'S.SANDEEP [ 04 FEB 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "

    '            'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '            'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '            'Select Case ConfigParameter._Object._UserAccessModeSetting
    '            '    Case enAllocation.BRANCH
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.DEPARTMENT_GROUP
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.DEPARTMENT
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.SECTION_GROUP
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.SECTION
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.UNIT_GROUP
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.UNIT
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.TEAM
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.JOB_GROUP
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            '    Case enAllocation.JOBS
    '            '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '        End If
    '            'End Select
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '            'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '            'S.SANDEEP [ 04 FEB 2012 ] -- END
    '        End If
    '        'Sohail (24 Jun 2011) -- End

    '        If intAnalysisRefID > 0 Then
    '            strQ &= " AND prpayslipmessages_master.analysisref_id = @analysisref_id "
    '            objDataOperation.AddParameter("@analysisref_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisRefID.ToString)
    '        End If
    '        If intEmpID > 0 Then
    '            strQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID.ToString)
    '        End If
    '        If intGradeID > 0 Then
    '            strQ &= " AND hrgrade_master.gradeunkid = @gradeunkid "
    '            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeID.ToString)
    '        End If
    '        If intCostCenterID > 0 Then
    '            strQ &= " AND prcostcenter_master.costcenterunkid = @costcenterunkid "
    '            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostCenterID.ToString)
    '        End If
    '        If intPayPintID > 0 Then
    '            strQ &= " AND prpaypoint_master.paypointunkid = @paypointunkid "
    '            objDataOperation.AddParameter("@paypointunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayPintID.ToString)
    '        End If
    '        If intDepartmentID > 0 Then
    '            strQ &= " AND hrdepartment_master.departmentunkid = @departmentunkid "
    '            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentID.ToString)
    '        End If
    '        If intSectiontID > 0 Then
    '            strQ &= " AND hrsection_master.sectionunkid = @sectionunkid "
    '            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectiontID.ToString)
    '        End If
    '        If intJobID > 0 Then
    '            strQ &= " AND hrjob_master.jobunkid = @jobunkid "
    '            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID.ToString)
    '        End If
    '        If intClassID > 0 Then
    '            strQ &= " AND hrclasses_master.classesunkid = @classesunkid "
    '            objDataOperation.AddParameter("@classesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassID.ToString)
    '        End If
    '        strQ &= " ORDER BY analysisref_id "

    '        objDataOperation.AddParameter("@isglobalmessage", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsGlobaMsg.ToString)
    '        objDataOperation.AddParameter("@employee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Employee"))
    '        objDataOperation.AddParameter("@grade", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Grade"))
    '        objDataOperation.AddParameter("@costcenter", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Cost Center"))
    '        objDataOperation.AddParameter("@paypoint", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pay Point"))
    '        objDataOperation.AddParameter("@department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Department in Department Group"))
    '        objDataOperation.AddParameter("@section", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Section in Department"))
    '        objDataOperation.AddParameter("@job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Job in Job Group"))
    '        objDataOperation.AddParameter("@class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Class in Class Group"))
    '        objDataOperation.AddParameter("@undefined", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Undefined"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpayslipmessages_master) </purpose>
    Public Function Insert(Optional ByVal SkipIsExist As Boolean = False, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        'Gajanan [1-July-2020] -- Add [SkipIsExist,xDataOpr]

        'Nilay (28-Apr-2016) -- Start
        'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
        'If isExist(enPayslipMsgType.PAYSLIP_ANALYSIS_MESSAGE, mintAnalysisref_Id, mintAnalysistranunkid) Then
        'Gajanan [1-July-2020] -- Start
        If SkipIsExist = False Then
            'Gajanan [1-July-2020] -- End
        If isExist(enPayslipMsgType.PAYSLIP_ANALYSIS_MESSAGE, mintAnalysisref_Id, mintAnalysistranunkid, mintPeriodunkid) Then
            'Nilay (28-Apr-2016) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Payslip Messsage already defined for the selected item.")
            Return False
        End If
        End If

        'Gajanan [1-July-2020] -- Start
        'Dim objDataOperation As clsDataOperation 'Sohail (06 Aug 2012)
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [1-July-2020] -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [1-July-2020] -- Start
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction() 'Sohail (06 Aug 2012)
        'Gajanan [1-July-2020] -- End


        Try
            objDataOperation.AddParameter("@analysisref_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisref_Id.ToString)
            objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysistranunkid.ToString)
            objDataOperation.AddParameter("@msg1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMsg1.ToString)
            objDataOperation.AddParameter("@msg2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMsg2.ToString)
            objDataOperation.AddParameter("@msg3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMsg3.ToString)
            objDataOperation.AddParameter("@msg4", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMsg4.ToString)
            objDataOperation.AddParameter("@isglobalmessage", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsglobalmessage.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            'Nilay (28-Apr-2016) -- End

            strQ = "INSERT INTO prpayslipmessages_master ( " & _
              "  analysisref_id " & _
              ", analysistranunkid " & _
              ", msg1 " & _
              ", msg2 " & _
              ", msg3 " & _
              ", msg4 " & _
              ", isglobalmessage " & _
              ", userunkid " & _
              ", isactive" & _
              ", periodunkid " & _
            ") VALUES (" & _
              "  @analysisref_id " & _
              ", @analysistranunkid " & _
              ", @msg1 " & _
              ", @msg2 " & _
              ", @msg3 " & _
              ", @msg4 " & _
              ", @isglobalmessage " & _
              ", @userunkid " & _
              ", @isactive" & _
              ", @periodunkid " & _
            "); SELECT @@identity"
            'Nilay (28-Apr-2016) -- [periodunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMessageunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (06 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'Return True
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "prpayslipmessages_master", "messageunkid", mintMessageunkid) = False Then
                'Gajanan [1-July-2020] -- Start
                'objDataOperation.ReleaseTransaction(False)
                If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
                End If
                'Gajanan [1-July-2020] -- End
                Return False
            Else

                'Gajanan [1-July-2020] -- Start
                'objDataOperation.ReleaseTransaction(True)
                If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
                End If
                'Gajanan [1-July-2020] -- End
            Return True
            End If
            'Sohail (06 Aug 2012) -- End

        Catch ex As Exception
            'Gajanan [1-July-2020] -- Start
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'Gajanan [1-July-2020] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [1-July-2020] -- Start
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [1-July-2020] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpayslipmessages_master) </purpose>
    Public Function Update(Optional ByVal SkipIsExist As Boolean = False, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [1-July-2020] -- Add [SkipIsExist,xDataOpr]

        'Nilay (28-Apr-2016) -- Start
        'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
        'If isExist(enPayslipMsgType.PAYSLIP_ANALYSIS_MESSAGE, mintAnalysisref_Id, mintAnalysistranunkid, mintMessageunkid) Then
        'Gajanan [1-July-2020] -- Start
        If SkipIsExist = False Then
            'Gajanan [1-July-2020] -- End
        If isExist(enPayslipMsgType.PAYSLIP_ANALYSIS_MESSAGE, mintAnalysisref_Id, mintAnalysistranunkid, mintPeriodunkid, mintMessageunkid) Then
            'Nilay (28-Apr-2016) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Payslip Messsage already defined for the selected item.")
            Return False
        End If
        End If

        'Gajanan [1-July-2020] -- Start
        'Dim objDataOperation As clsDataOperation 'Sohail (06 Aug 2012)
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [1-July-2020] -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan [1-July-2020] -- Start
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction() 'Sohail (06 Aug 2012)
        'Gajanan [1-July-2020] -- End

        Try
            objDataOperation.AddParameter("@messageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMessageunkid.ToString)
            objDataOperation.AddParameter("@analysisref_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisref_Id.ToString)
            objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysistranunkid.ToString)
            objDataOperation.AddParameter("@msg1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMsg1.ToString)
            objDataOperation.AddParameter("@msg2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMsg2.ToString)
            objDataOperation.AddParameter("@msg3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMsg3.ToString)
            objDataOperation.AddParameter("@msg4", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMsg4.ToString)
            objDataOperation.AddParameter("@isglobalmessage", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsglobalmessage.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            'Nilay (28-Apr-2016) -- End

            strQ = "UPDATE prpayslipmessages_master SET " & _
              "  analysisref_id = @analysisref_id" & _
              ", analysistranunkid = @analysistranunkid" & _
              ", msg1 = @msg1" & _
              ", msg2 = @msg2" & _
              ", msg3 = @msg3" & _
              ", msg4 = @msg4" & _
              ", isglobalmessage = @isglobalmessage" & _
              ", userunkid = @userunkid" & _
              ", isactive = @isactive " & _
              ", periodunkid = @periodunkid " & _
            "WHERE messageunkid = @messageunkid "
            'Nilay (28-Apr-2016) -- [periodunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (06 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'Return True
            Dim blnToInsert As Boolean
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "prpayslipmessages_master", mintMessageunkid, "messageunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 2, "prpayslipmessages_master", "messageunkid", mintMessageunkid) = False Then
                'Gajanan [1-July-2020] -- Start
                'objDataOperation.ReleaseTransaction(False)
                If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
                End If
                'Gajanan [1-July-2020] -- End
                Return False

                Return False
            Else

                'Gajanan [1-July-2020] -- Start
                'objDataOperation.ReleaseTransaction(True)
                If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
                End If
                'Gajanan [1-July-2020] -- End
            Return True
            End If
            'Sohail (06 Aug 2012) -- End

        Catch ex As Exception
            'Gajanan [1-July-2020] -- Start
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'Gajanan [1-July-2020] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [1-July-2020] -- Start
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [1-July-2020] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prpayslipmessages_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (28 Dec 2010) -- Start
            'strQ = "DELETE FROM prpayslipmessages_master " & _
            '"WHERE messageunkid = @messageunkid "
            strQ = "UPDATE prpayslipmessages_master SET " & _
              " isactive = 0 " & _
            "WHERE messageunkid = @messageunkid "
            'Sohail (28 Dec 2010) -- End

            objDataOperation.AddParameter("@messageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (06 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "prpayslipmessages_master", "messageunkid", intUnkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If
            'Sohail (06 Aug 2012) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@messageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' Nilay (28-Apr-2016) -- Start
    ''' Enhancement : 60.1 - #77 (KBC and Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    ''' Public Function isExist(ByVal enPayslipMsg As enPayslipMsgType, ByVal intAnalysisRefID As Integer, ByVal intAnalysisTranUnkID As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal enPayslipMsg As enPayslipMsgType, _
                            ByVal intAnalysisRefID As Integer, _
                            ByVal intAnalysisTranUnkID As Integer, _
                            ByVal intPeriodId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing _
                            ) As Boolean
        'Nilay (28-Apr-2016) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan [1-July-2020] -- Start
        'objDataOperation = New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
        objDataOperation = New clsDataOperation
        End If
        'Gajanan [1-July-2020] -- End


        Try

            strQ = "SELECT  prpayslipmessages_master.messageunkid " & _
                          ", prpayslipmessages_master.analysisref_id " & _
                          ", prpayslipmessages_master.analysistranunkid " & _
                          ", prpayslipmessages_master.msg1 " & _
                          ", prpayslipmessages_master.msg2 " & _
                          ", prpayslipmessages_master.msg3 " & _
                          ", prpayslipmessages_master.msg4 " & _
                          ", prpayslipmessages_master.isglobalmessage " & _
                          ", prpayslipmessages_master.userunkid " & _
                          ", prpayslipmessages_master.isactive " & _
                          ", prpayslipmessages_master.periodunkid " & _
                    "FROM    prpayslipmessages_master " & _
                    "WHERE   prpayslipmessages_master.isglobalmessage = @isglobalmessage " & _
                    "AND isactive = 1 " 'Sohail (28 Dec 2010)
            'Nilay (28-Apr-2016) -- [periodunkid]

            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            'If enPayslipMsg = enPayslipMsgType.PAYSLIP_ANALYSIS_MESSAGE Then
            '    strQ &= "AND prpayslipmessages_master.analysisref_id = @analysisref_id "
            '    strQ &= "AND prpayslipmessages_master.analysistranunkid = @analysistranunkid "

            '    If intUnkid > 0 Then
            '        strQ &= "AND prpayslipmessages_master.messageunkid <> @messageunkid "
            '        objDataOperation.AddParameter("@messageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            '    End If
            '    objDataOperation.AddParameter("@isglobalmessage", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            '    objDataOperation.AddParameter("@analysisref_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisRefID)
            '    objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisTranUnkID)
            'Else
            '    objDataOperation.AddParameter("@isglobalmessage", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            'End If

            strQ &= " AND prpayslipmessages_master.periodunkid = @periodunkid "

            If enPayslipMsg = enPayslipMsgType.PAYSLIP_ANALYSIS_MESSAGE Then
                strQ &= "AND prpayslipmessages_master.analysisref_id = @analysisref_id "
                strQ &= "AND prpayslipmessages_master.analysistranunkid = @analysistranunkid "

                If intUnkid > 0 Then
                    strQ &= "AND prpayslipmessages_master.messageunkid <> @messageunkid "
                    objDataOperation.AddParameter("@messageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                End If
                objDataOperation.AddParameter("@analysisref_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisRefID)
                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisTranUnkID)
            End If
            objDataOperation.AddParameter("@isglobalmessage", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsglobalmessage)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)
            'Nilay (28-Apr-2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [1-July-2020] -- Start
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [1-July-2020] -- End
        End Try
    End Function


    'Gajanan [1-July-2020] -- Start
    Public Function InsertAll(ByVal EmpIds As String, ByVal IsOverwriteData As Boolean) As Boolean

        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [1-July-2020] -- Start
        Dim blnFlag As Boolean = False
        'Gajanan [1-July-2020] -- End

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            For Each id As String In EmpIds.Split(CChar(","))
                _Analysistranunkid = CInt(id)
                If isExist(enPayslipMsgType.PAYSLIP_ANALYSIS_MESSAGE, mintAnalysisref_Id, CInt(id), mintPeriodunkid, , objDataOperation) Then

                    If IsOverwriteData Then
                        mintMessageunkid = GetMsgUnkid(enPayslipMsgType.PAYSLIP_ANALYSIS_MESSAGE, mintAnalysisref_Id, CInt(id), mintPeriodunkid, , objDataOperation)
                        blnFlag = Update(True, objDataOperation)
                    Else
                        Continue For
                    End If
                Else
                    blnFlag = Insert(True, objDataOperation)
                End If


                If blnFlag = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Next

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetMsgUnkid(ByVal enPayslipMsg As enPayslipMsgType, _
                            ByVal intAnalysisRefID As Integer, _
                            ByVal intAnalysisTranUnkID As Integer, _
                            ByVal intPeriodId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT  prpayslipmessages_master.messageunkid " & _
                          ", prpayslipmessages_master.analysisref_id " & _
                          ", prpayslipmessages_master.analysistranunkid " & _
                          ", prpayslipmessages_master.msg1 " & _
                          ", prpayslipmessages_master.msg2 " & _
                          ", prpayslipmessages_master.msg3 " & _
                          ", prpayslipmessages_master.msg4 " & _
                          ", prpayslipmessages_master.isglobalmessage " & _
                          ", prpayslipmessages_master.userunkid " & _
                          ", prpayslipmessages_master.isactive " & _
                          ", prpayslipmessages_master.periodunkid " & _
                    "FROM    prpayslipmessages_master " & _
                    "WHERE   prpayslipmessages_master.isglobalmessage = @isglobalmessage " & _
                    "AND isactive = 1 "

            strQ &= " AND prpayslipmessages_master.periodunkid = @periodunkid "

            If enPayslipMsg = enPayslipMsgType.PAYSLIP_ANALYSIS_MESSAGE Then
                strQ &= "AND prpayslipmessages_master.analysisref_id = @analysisref_id "
                strQ &= "AND prpayslipmessages_master.analysistranunkid = @analysistranunkid "

                If intUnkid > 0 Then
                    strQ &= "AND prpayslipmessages_master.messageunkid <> @messageunkid "
                    objDataOperation.AddParameter("@messageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                End If
                objDataOperation.AddParameter("@analysisref_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisRefID)
                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisTranUnkID)
            End If
            objDataOperation.AddParameter("@isglobalmessage", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsglobalmessage)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("messageunkid").ToString())
            Else
                Return -1
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMsgUnkid; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [1-July-2020] -- End


#Region " Message "
    '1, "Sorry, Payslip Messsage already defined for the selected item."
    '2, "Employee"
    '3, "Grade"
    '4, "Cost Center"
    '5, "Pay Point"
    '6, "Department in Department Group"
    '7, "Section in Department"
    '8, "Job in Job Group"
    '9, "Class in Class Group"
    '10, "Undefined"

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Payslip Messsage already defined for the selected item.")
			Language.setMessage(mstrModuleName, 2, "Employee")
			Language.setMessage(mstrModuleName, 3, "Grade")
			Language.setMessage(mstrModuleName, 4, "Cost Center")
			Language.setMessage(mstrModuleName, 5, "Pay Point")
			Language.setMessage(mstrModuleName, 6, "Department in Department Group")
			Language.setMessage(mstrModuleName, 7, "Section in Department")
			Language.setMessage(mstrModuleName, 8, "Job in Job Group")
			Language.setMessage(mstrModuleName, 9, "Class in Class Group")
			Language.setMessage(mstrModuleName, 10, "Undefined")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class