﻿'************************************************************************************************************************************
'Class Name : clsSalaryIncrement.vb
'Purpose    :
'Date       :07/07/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

#End Region


''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsSalaryIncrement

#Region " Private variables "

    Private Shared ReadOnly mstrModuleName As String = "clsSalaryIncrement"
    Dim objDataOperation As clsDataOperation
    Private objESalaryHistory As New clsempsalary_history_tran
    Dim mstrMessage As String = ""
    Private mintSalaryincrementtranunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtIncrementdate As Date
    Private mdecCurrentscale As Decimal
    Private mdecIncrement As Decimal
    Private mdecNewscale As Decimal
    Private mintGradegroupunkid As Integer
    Private mintGradeunkid As Integer
    Private mintGradelevelunkid As Integer
    Private mblnIsgradechange As Boolean
    Private mblnIsfromemployee As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrReason_Id As Integer
    Private mintIncrement_mode As Integer
    Private mdblPercentage As Double
    Private mdtTran As DataTable
    Private mstrWebFormName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
    Private mintLogEmployeeUnkid As Integer = -1
    Private mblnIsapproved As Boolean
    Private mintApproveruserunkid As Integer = -1
    Private mblnIsCopyPrevoiusSLAB As Boolean = False
    Private mblnIsOverwritePrevoiusSLAB As Boolean = False
    Private mintInfoSalHeadUnkid As Integer = -1
    Private mintSuccessCnt As Integer = 0
    Private mdtPSoft_SyncDateTime As Date
    Private mintRehiretranunkid As Integer = 0
    Private xDataOpr As clsDataOperation
    Private mintChangetypeid As Integer = enSalaryChangeType.SIMPLE_SALARY_CHANGE
    Private mdtPromotion_date As DateTime = Nothing
    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Private mdtActualdate As Date
    Private mdecArrears_amount As Decimal
    Private mintArrears_countryid As Integer
    Private mintNoofinstallment As Integer
    Private mdecEmi_amount As Decimal
    Private mintApproval_statusunkid As Integer
    Private mintFinalapproverunkid As Integer
    Private mintArrears_statusunkid As Integer
    'Sohail (21 Jan 2020) -- End

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set salaryincrementtranunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Salaryincrementtranunkid() As Integer
        Get
            Return mintSalaryincrementtranunkid
        End Get
        Set(ByVal value As Integer)
            mintSalaryincrementtranunkid = value
            Call GetData()

        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set incrementdate
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Incrementdate() As Date
        Get
            Return mdtIncrementdate
        End Get
        Set(ByVal value As Date)
            mdtIncrementdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currentscale
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Currentscale() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecCurrentscale
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecCurrentscale = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set increment
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Increment() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecIncrement
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecIncrement = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set newscale
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Newscale() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecNewscale
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecNewscale = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradegroupunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Gradegroupunkid() As Integer
        Get
            Return mintGradegroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGradegroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradelevelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Gradelevelunkid() As Integer
        Get
            Return mintGradelevelunkid
        End Get
        Set(ByVal value As Integer)
            mintGradelevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isgradechange
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isgradechange() As Boolean
        Get
            Return mblnIsgradechange
        End Get
        Set(ByVal value As Boolean)
            mblnIsgradechange = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose Get or Set isfromemployee
    ''' Modify By Sohail
    ''' </summary>
    Public Property _Isfromemployee() As Boolean
        Get
            Return mblnIsfromemployee
        End Get
        Set(ByVal value As Boolean)
            mblnIsfromemployee = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _Reason_Id() As Integer
        Get
            Return mstrReason_Id
        End Get
        Set(ByVal value As Integer)
            mstrReason_Id = value
        End Set
    End Property
    
    Public Property _Increment_Mode() As Integer
        Get
            Return mintIncrement_mode
        End Get
        Set(ByVal value As Integer)
            mintIncrement_mode = value
        End Set
    End Property

    Public Property _Percentage() As Double
        Get
            Return mdblPercentage
        End Get
        Set(ByVal value As Double)
            mdblPercentage = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    
    ''' <summary>
    ''' Purpose: Get or Set isapproved
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveruserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Approveruserunkid() As Integer
        Get
            Return mintApproveruserunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveruserunkid = value
        End Set
    End Property
    
    Public WriteOnly Property _IsCopyPrevoiusSLAB() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCopyPrevoiusSLAB = value
        End Set
    End Property

    Public WriteOnly Property _IsOverwritePrevoiusSLAB() As Boolean
        Set(ByVal value As Boolean)
            mblnIsOverwritePrevoiusSLAB = value
        End Set
    End Property

    Public WriteOnly Property _InfoSalHeadUnkid() As Integer
        Set(ByVal value As Integer)
            mintInfoSalHeadUnkid = value
        End Set
    End Property
    
    Public ReadOnly Property _SuccessCnt() As Integer
        Get
            Return mintSuccessCnt
        End Get
    End Property
    
    Public Property _PSoft_SyncDateTime() As Date
        Get
            Return mdtPSoft_SyncDateTime
        End Get
        Set(ByVal value As Date)
            mdtPSoft_SyncDateTime = value
        End Set
    End Property
    
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Public Property _Changetypeid() As Integer
        Get
            Return mintChangetypeid
        End Get
        Set(ByVal value As Integer)
            mintChangetypeid = value
        End Set
    End Property

    Public Property _Promotion_date() As DateTime
        Get
            Return mdtPromotion_date
        End Get
        Set(ByVal value As DateTime)
            mdtPromotion_date = value
        End Set
    End Property

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Public Property _ActualDate() As DateTime
        Get
            Return mdtActualdate
        End Get
        Set(ByVal value As DateTime)
            mdtActualdate = value
        End Set
    End Property

    Public Property _Arrears_Amount() As Decimal
        Get
            Return mdecArrears_amount
        End Get
        Set(ByVal value As Decimal)
            mdecArrears_amount = value
        End Set
    End Property

    Public Property _Arrears_Countryid() As Integer
        Get
            Return mintArrears_countryid
        End Get
        Set(ByVal value As Integer)
            mintArrears_countryid = value
        End Set
    End Property

    Public Property _NoOfInstallment() As Integer
        Get
            Return mintNoofinstallment
        End Get
        Set(ByVal value As Integer)
            mintNoofinstallment = value
        End Set
    End Property

    Public Property _EMI_Amount() As Decimal
        Get
            Return mdecEmi_amount
        End Get
        Set(ByVal value As Decimal)
            mdecEmi_amount = value
        End Set
    End Property

    Public Property _Approval_StatusUnkid() As Integer
        Get
            Return mintApproval_statusunkid
        End Get
        Set(ByVal value As Integer)
            mintApproval_statusunkid = value
        End Set
    End Property

    Public Property _FinalApproverUnkid() As Integer
        Get
            Return mintFinalapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintFinalapproverunkid = value
        End Set
    End Property

    Public Property _Arrears_StatusUnkid() As Integer
        Get
            Return mintArrears_statusunkid
        End Get
        Set(ByVal value As Integer)
            mintArrears_statusunkid = value
        End Set
    End Property
    'Sohail (21 Jan 2020) -- End

#End Region

#Region " Constructor "
    Public Sub New()

    End Sub

    Public Sub New(ByVal blnGlobalIncrement As Boolean)
        mdtTran = New DataTable("Increment")
        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("currentscale")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("increment")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("newscale")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("gradegroupunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("gradeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("gradelevelunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            mdtTran.Columns.Add("increment_mode", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("percentage", System.Type.GetType("System.Double")).DefaultValue = 0

            dCol = New DataColumn("isapproved")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("approveruserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            mdtTran.Columns.Add("isgradechange", System.Type.GetType("System.Boolean")).DefaultValue = False

            mdtTran.Columns.Add("rehiretranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0

            'mdtTran.Columns.Add("changetypeid", System.Type.GetType("System.Int32")).DefaultValue = enSalaryChangeType.SIMPLE_SALARY_CHANGE
            mdtTran.Columns.Add("promotion_date", GetType(DateTime)).DefaultValue = Nothing

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            mdtTran.Columns.Add("actualdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTran.Columns.Add("arrears_amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTran.Columns.Add("arrears_countryid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("noofinstallment", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("emi_amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTran.Columns.Add("approval_statusunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("finalapproverunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("arrears_statusunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            'Sohail (21 Jan 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New(GlobalIncrement)", mstrModuleName)
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'objDataOperation = New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END


        Try            
            strQ = "SELECT " & _
              "  salaryincrementtranunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", incrementdate " & _
              ", currentscale " & _
              ", increment " & _
              ", newscale " & _
              ", gradegroupunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", isgradechange " & _
              ", isfromemployee " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", reason_id " & _
              ", increment_mode " & _
              ", percentage " & _
              ", ISNULL(isapproved, 0) AS isapproved " & _
              ", ISNULL(approveruserunkid, 0) AS approveruserunkid " & _
              ", prsalaryincrement_tran.psoft_syncdatetime " & _
              ", ISNULL(rehiretranunkid,0) AS rehiretranunkid " & _
              ", ISNULL(changetypeid," & enSalaryChangeType.SIMPLE_SALARY_CHANGE & ") AS changetypeid " & _
              ", promotion_date " & _
              ", prsalaryincrement_tran.actualdate " & _
              ", ISNULL(prsalaryincrement_tran.arrears_amount, 0) AS arrears_amount " & _
              ", ISNULL(prsalaryincrement_tran.arrears_countryid, 0) AS arrears_countryid " & _
              ", ISNULL(prsalaryincrement_tran.noofinstallment, 0) AS noofinstallment " & _
              ", ISNULL(prsalaryincrement_tran.emi_amount, 0) AS emi_amount " & _
              ", ISNULL(prsalaryincrement_tran.approval_statusunkid, 0) AS approval_statusunkid " & _
              ", ISNULL(prsalaryincrement_tran.finalapproverunkid, 0) AS finalapproverunkid " & _
              ", ISNULL(prsalaryincrement_tran.arrears_statusunkid, 0) AS arrears_statusunkid " & _
             "FROM prsalaryincrement_tran " & _
             "WHERE salaryincrementtranunkid = @salaryincrementtranunkid "
            'Sohail (21 Jan 2020) - [actualdate, arrears_amount, arrears_countryid, noofinstallment, emi_amount, approval_statusunkid, finalapproverunkid, arrears_statusunkid]

            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintSalaryincrementtranunkid = CInt(dtRow.Item("salaryincrementtranunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtIncrementdate = dtRow.Item("incrementdate")
                mdecCurrentscale = CDec(dtRow.Item("currentscale"))
                mdecIncrement = CDec(dtRow.Item("increment"))
                mdecNewscale = CDec(dtRow.Item("newscale"))
                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintGradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                mblnIsgradechange = CBool(dtRow.Item("isgradechange"))
                mblnIsfromemployee = CBool(dtRow.Item("isfromemployee"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mstrReason_Id = CInt(dtRow.Item("reason_id"))
                mintIncrement_mode = CInt(dtRow.Item("increment_mode"))
                mdblPercentage = CDbl(dtRow.Item("percentage"))
                mblnIsapproved = CBool(dtRow.Item("isapproved"))
                mintApproveruserunkid = CInt(dtRow.Item("approveruserunkid"))
                If dtRow.Item("psoft_syncdatetime").ToString = Nothing Then
                    mdtPSoft_SyncDateTime = Nothing
                Else
                    mdtPSoft_SyncDateTime = dtRow.Item("psoft_syncdatetime")
                End If
                mintRehiretranunkid = CInt(dtRow.Item("rehiretranunkid"))
                mintChangetypeid = CInt(dtRow.Item("changetypeid"))
                If IsDBNull(dtRow.Item("promotion_date")) Then
                    mdtPromotion_date = Nothing
                Else
                    mdtPromotion_date = dtRow.Item("promotion_date")
                End If
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                If IsDBNull(dtRow.Item("actualdate")) Then
                    mdtActualdate = Nothing
                Else
                    mdtActualdate = dtRow.Item("actualdate")
                End If
                mdecArrears_amount = CDec(dtRow.Item("arrears_amount"))
                mintArrears_countryid = CInt(dtRow.Item("arrears_countryid"))
                mintNoofinstallment = CInt(dtRow.Item("noofinstallment"))
                mdecEmi_amount = CDec(dtRow.Item("emi_amount"))
                mintApproval_statusunkid = CInt(dtRow.Item("approval_statusunkid"))
                mintFinalapproverunkid = CInt(dtRow.Item("finalapproverunkid"))
                mintArrears_statusunkid = CInt(dtRow.Item("arrears_statusunkid"))
                'Sohail (21 Jan 2020) -- End

                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END

        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkID As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal blnApplyUserAccessFilter As Boolean _
                            , ByVal strTableName As String _
                            , ByVal objDataOp As clsDataOperation _
                            , Optional ByVal strEmpUnkIDs As String = "" _
                            , Optional ByVal intPeriodUnkID As Integer = 0 _
                            , Optional ByVal intYearUnkID As Integer = 0 _
                           , Optional ByVal strOrderBy As String = "" _
                            , Optional ByVal mstrAdvanceFilter As String = "" _
                            , Optional ByVal intSalaryChangeApprovalStatus As Integer = enSalaryChangeApprovalStatus.All _
                            , Optional ByVal blnAddGrouping As Boolean = False _
                            , Optional ByVal blnApplyDateFilter As Boolean = True _
                            ) As DataSet
        'Sohail (12 Oct 2021) - [blnApplyDateFilter]
        'Sohail (13 Jul 2019) - [blnAddGrouping]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        If blnApplyDateFilter = True Then 'Sohail (12 Oct 2021)
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        End If 'Sohail (12 Oct 2021)
        If blnApplyUserAccessFilter = True Then 'Sohail (12 Oct 2021)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkID, xUserModeSetting)
        End If 'Sohail (12 Oct 2021)
        If mstrAdvanceFilter.Trim.Length > 0 Then 'Sohail (12 Oct 2021)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        End If 'Sohail (12 Oct 2021)

        'Sohail (21 Jan 2020) -- Start
        'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
        'objDataOperation = New clsDataOperation
        'Sohail (21 Jan 2020) -- End
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
            'Sohail (21 Jan 2020) -- End
        End If
        objDataOperation.ClearParameters() 'Sohail (21 Jan 2020)

        Try
            strQ = "SELECT " & _
              "  prsalaryincrement_tran.salaryincrementtranunkid " & _
              ", prsalaryincrement_tran.periodunkid " & _
              ", hremployee_master.employeecode " & _
              ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
              ", cfcommon_period_tran.period_name " & _
              ", prsalaryincrement_tran.employeeunkid " & _
              ", CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) AS incrementdate " & _
              ", prsalaryincrement_tran.currentscale " & _
              ", prsalaryincrement_tran.increment " & _
              ", prsalaryincrement_tran.newscale " & _
              ", prsalaryincrement_tran.gradegroupunkid " & _
              ", ISNULL(hrgradegroup_master.name, '') AS GradeGroup  " & _
              ", prsalaryincrement_tran.gradeunkid " & _
              ", ISNULL(hrgrade_master.name, '') AS Grade " & _
              ", prsalaryincrement_tran.gradelevelunkid " & _
              ", ISNULL(hrgradelevel_master.name, '') AS GradeLevel " & _
              ", prsalaryincrement_tran.isgradechange " & _
              ", prsalaryincrement_tran.isfromemployee " & _
              ", prsalaryincrement_tran.userunkid " & _
              ", prsalaryincrement_tran.isvoid " & _
              ", prsalaryincrement_tran.voiduserunkid " & _
              ", prsalaryincrement_tran.voiddatetime " & _
              ", prsalaryincrement_tran.voidreason " & _
              ", prsalaryincrement_tran.reason_id " & _
              ", ISNULL(prsalaryincrement_tran.increment_mode, 0) AS increment_mode " & _
              ", ISNULL(prsalaryincrement_tran.percentage, 0) AS percentage " & _
              ", ISNULL(prsalaryincrement_tran.isapproved, 0) AS isapproved " & _
              ", ISNULL(prsalaryincrement_tran.approveruserunkid, 0) AS approveruserunkid " & _
              ", ISNULL(ETRF.sectiongroupunkid,0) AS sectiongroupunkid " & _
              ", ISNULL(ETRF.unitgroupunkid,0) AS unitgroupunkid " & _
              ", ISNULL(ETRF.teamunkid,0) AS teamunkid " & _
              ", ISNULL(ETRF.stationunkid,0) AS stationunkid " & _
              ", ISNULL(ETRF.deptgroupunkid,0) AS deptgroupunkid " & _
              ", ISNULL(ETRF.departmentunkid,0) AS departmentunkid " & _
              ", ISNULL(ETRF.sectionunkid,0) AS sectionunkid " & _
              ", ISNULL(ETRF.unitunkid,0) AS unitunkid " & _
              ", ISNULL(ERECAT.jobunkid,0) AS jobunkid " & _
              ", ISNULL(ETRF.classgroupunkid,0) AS classgroupunkid " & _
              ", ISNULL(ETRF.classunkid,0) AS classunkid " & _
              ", ISNULL(ERECAT.jobgroupunkid,0) AS jobgroupunkid " & _
              ", prsalaryincrement_tran.psoft_syncdatetime " & _
              ", ISNULL(rehiretranunkid,0) AS rehiretranunkid " & _
              ", CASE WHEN changetypeid = '" & enSalaryChangeType.SIMPLE_SALARY_CHANGE & "' THEN @SSC " & _
              "       WHEN changetypeid = '" & enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION & "' THEN @SCP " & _
              "  END AS changetype " & _
              ", prsalaryincrement_tran.changetypeid " & _
              ", prsalaryincrement_tran.promotion_date " & _
              ", CONVERT(CHAR(8), prsalaryincrement_tran.actualdate, 112) AS actualdate " & _
              ", ISNULL(prsalaryincrement_tran.arrears_amount, 0) AS arrears_amount " & _
              ", ISNULL(prsalaryincrement_tran.arrears_countryid, 0) AS arrears_countryid " & _
              ", ISNULL(prsalaryincrement_tran.noofinstallment, 0) AS noofinstallment " & _
              ", ISNULL(prsalaryincrement_tran.emi_amount, 0) AS emi_amount " & _
              ", ISNULL(prsalaryincrement_tran.approval_statusunkid, 0) AS approval_statusunkid " & _
              ", ISNULL(prsalaryincrement_tran.finalapproverunkid, 0) AS finalapproverunkid " & _
              ", ISNULL(prsalaryincrement_tran.arrears_statusunkid, 0) AS arrears_statusunkid " & _
              ", CAST(0 AS BIT) AS IsGrp " & _
              ", CAST(0 AS BIT) AS IsChecked " & _
              "FROM prsalaryincrement_tran " & _
              " LEFT JOIN cfcommon_period_tran ON prsalaryincrement_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
              " LEFT JOIN hremployee_master ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " & _
              " LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = prsalaryincrement_tran.gradegroupunkid " & _
              " LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prsalaryincrement_tran.gradeunkid " & _
              " LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = prsalaryincrement_tran.gradelevelunkid " & _
              " LEFT JOIN " & _
              "( " & _
              "    SELECT " & _
              "         Trf.TrfEmpId " & _
              "        ,ISNULL(Trf.stationunkid,0) AS stationunkid " & _
              "        ,ISNULL(Trf.deptgroupunkid,0) AS deptgroupunkid " & _
              "        ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
              "        ,ISNULL(Trf.sectiongroupunkid,0) AS sectiongroupunkid " & _
              "        ,ISNULL(Trf.sectionunkid,0) AS sectionunkid " & _
              "        ,ISNULL(Trf.unitgroupunkid,0) AS unitgroupunkid " & _
              "        ,ISNULL(Trf.unitunkid,0) AS unitunkid " & _
              "        ,ISNULL(Trf.teamunkid,0) AS teamunkid " & _
              "        ,ISNULL(Trf.classgroupunkid,0) AS classgroupunkid " & _
              "        ,ISNULL(Trf.classunkid,0) AS classunkid " & _
              "        ,ISNULL(Trf.EfDt,'') AS EfDt " & _
              "        ,Trf.ETT_REASON " & _
              "    FROM " & _
              "   ( " & _
              "        SELECT " & _
              "             ETT.employeeunkid AS TrfEmpId " & _
              "            ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
              "            ,ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
              "            ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
              "            ,ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
              "            ,ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
              "            ,ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
              "            ,ISNULL(ETT.unitunkid,0) AS unitunkid " & _
              "            ,ISNULL(ETT.teamunkid,0) AS teamunkid " & _
              "            ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
              "            ,ISNULL(ETT.classunkid,0) AS classunkid " & _
              "            ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
              "            ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
              "            ,CASE WHEN ETT.rehiretranunkid > 0 THEN RTT.name ELSE TTC.name END AS ETT_REASON " & _
              "        FROM hremployee_transfer_tran AS ETT " & _
              "           LEFT JOIN cfcommon_master AS RTT ON RTT.masterunkid = ETT.changereasonunkid AND RTT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
              "           LEFT JOIN cfcommon_master AS TTC ON TTC.masterunkid = ETT.changereasonunkid AND TTC.mastertype = '" & clsCommon_Master.enCommonMaster.TRANSFERS & "' " & _
              "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
              "    ) AS Trf WHERE Trf.Rno = 1 " & _
              ") AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.EfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
              "LEFT JOIN " & _
              "( " & _
              "    SELECT " & _
              "         Cat.CatEmpId " & _
              "        ,Cat.jobgroupunkid " & _
              "        ,Cat.jobunkid " & _
              "        ,Cat.CEfDt " & _
              "        ,Cat.RECAT_REASON " & _
              "    FROM " & _
              "    ( " & _
              "        SELECT " & _
              "             ECT.employeeunkid AS CatEmpId " & _
              "            ,ECT.jobgroupunkid " & _
              "            ,ECT.jobunkid " & _
              "            ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
              "            ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
              "            ,CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
              "        FROM hremployee_categorization_tran AS ECT " & _
              "        	 LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
              "        	 LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
              "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
              "    ) AS Cat WHERE Cat.Rno = 1 " & _
              ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) "
            'Sohail (13 Feb 2020) - [employeecode]
            'Sohail (21 Jan 2020) - [actualdate, arrears_amount, arrears_countryid, noofinstallment, emi_amount, approval_statusunkid, finalapproverunkid, arrears_statusunkid, IsGrp, IsChecked]

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE ISNULL(prsalaryincrement_tran.isvoid,0) = 0 "

            If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
                strQ &= "AND prsalaryincrement_tran.employeeunkid IN (" & strEmpUnkIDs & ") "
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If intYearUnkID > 0 Then
                strQ &= "AND cfcommon_period_tran.yearunkid = @yearunkid "
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkID.ToString)
            End If

            If intPeriodUnkID > 0 Then
                strQ &= "AND prsalaryincrement_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID.ToString)
            End If

            If intSalaryChangeApprovalStatus = enSalaryChangeApprovalStatus.Approved Then
                strQ &= " AND prsalaryincrement_tran.isapproved = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, intSalaryChangeApprovalStatus)
            ElseIf intSalaryChangeApprovalStatus = enSalaryChangeApprovalStatus.Pending Then
                strQ &= " AND prsalaryincrement_tran.isapproved = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            End If

            If mstrAdvanceFilter.Trim <> "" Then
                strQ &= " AND " & mstrAdvanceFilter
            End If

            If strOrderBy.Trim.Length > 0 Then
                strQ &= " ORDER BY " & strOrderBy
            End If

            objDataOperation.AddParameter("@SSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 717, "Simple Salary Change"))
            objDataOperation.AddParameter("@SCP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 718, "Salary Change With Promotion"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            If blnAddGrouping = True Then

                'Sohail (13 Feb 2020) -- Start
                'NMB Enhancement # : show employee code on salary change list.
                'Dim dt As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "employeename", "employeeunkid")
                Dim dt As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "employeename", "employeecode", "employeeunkid")
                'Sohail (13 Feb 2020) -- End
                Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dtCol = New DataColumn("IsGrp", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = True
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dt.Merge(dsList.Tables(0), False)
                'Sohail (13 Feb 2020) -- Start
                'NMB Enhancement # : show employee code on salary change list.
                'dt.DefaultView.Sort = "employeename, employeeunkid, IsGrp DESC"
                dt.DefaultView.Sort = "employeename, employeecode, employeeunkid, IsGrp DESC"
                'Sohail (13 Feb 2020) -- End

                dsList.Tables.Clear()
                dsList.Tables.Add(dt.DefaultView.ToTable)
            End If
            'Sohail (13 Jul 2019) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'objDataOperation = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (21 Jan 2020) -- End
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <param name="blnUpdateEmployeeScale">For Any Salary Change Please pass TRUE</param>
    ''' <param name="blnCalculateIncrementAmt">To update Current scale and Increment from the difference of current transaction NewScale and previous transaction NewScale </param>
    ''' <param name="blnUpdateLatestGrade">To update Grade from Previous transaction</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function Insert(ByVal strDatabaseName As String _
                           , ByVal xUserUnkid As Integer _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal blnAllowToApproveEarningDeduction As Boolean _
                           , ByVal dtCurrentDateAndTime As Date _
                           , Optional ByVal blnUpdateEmployeeScale As Boolean = True _
                           , Optional ByVal blnCalculateIncrementAmt As Boolean = False _
                           , Optional ByVal blnUpdateLatestGrade As Boolean = False _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal strFilerString As String = "" _
                           ) As Boolean
        
        If isExistOnSameDate(mdtIncrementdate, mintEmployeeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry! Increment for the employee on the selected date is already done.")
            Return False
        End If

        'Sohail (21 Jan 2020) -- Start
        'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
        If isExistOnSameDate(mdtActualdate, mintEmployeeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry! Increment for the employee on the selected actual date is already done.")
            Return False
        End If
        'Sohail (21 Jan 2020) -- End

        If mdtPromotion_date <> Nothing Then
            If isExistOnSameDate(mdtPromotion_date, mintEmployeeunkid, , True) Then
                mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry! Promotion for the employee on the selected date is already done.")
                Return False
            End If
        End If

        'Sohail (18 Dec 2018) -- Start
        'Internal Issue - Don't allow to rehire if salary increment date is greater that rehirement date is already exist in 76.1.
        If IsFutureIncrementExist(xDataOpr, mintEmployeeunkid, mdtIncrementdate) = True Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry! Increment date should be greater than last increment date.")
            Return False
        End If
        'Sohail (18 Dec 2018) -- End

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objWagesMaster As New clsWages
        Dim objWages As New clsWagesTran

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END
        Try
            'HYATT Enhancement - OUTBOUND file integration with Aruti.
            If blnCalculateIncrementAmt = True Then

                'S.SANDEEP [19 OCT 2016] -- START
                'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                'Dim ds As DataSet = objMaster.Get_Current_Scale("Scale", mintEmployeeunkid, mdtIncrementdate.Date)
                Dim ds As DataSet = objMaster.Get_Current_Scale("Scale", mintEmployeeunkid, mdtIncrementdate.Date, objDataOperation)
                'S.SANDEEP [19 OCT 2016] -- END

                If ds.Tables("Scale").Rows.Count > 0 Then
                    mdecCurrentscale = Format(ds.Tables("Scale").Rows(0).Item("newscale"), "#0.00")
                    mdecIncrement = mdecNewscale - mdecCurrentscale
                    If mblnIsgradechange = False AndAlso blnUpdateLatestGrade = True Then
                        mintGradegroupunkid = CInt(ds.Tables("Scale").Rows(0).Item("gradegroupunkid").ToString)
                        mintGradeunkid = CInt(ds.Tables("Scale").Rows(0).Item("gradeunkid").ToString)
                        mintGradelevelunkid = CInt(ds.Tables("Scale").Rows(0).Item("gradelevelunkid").ToString)
                    End If
                End If
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
            objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentscale.ToString)
            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString)
            objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewscale.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrReason_Id.ToString)
            objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_mode.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            'HYATT Enhancement - OUTBOUND file integration with Aruti.
            If mdtPSoft_SyncDateTime = Nothing Then
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
            End If
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
            objDataOperation.AddParameter("@changetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangetypeid)
            If mdtPromotion_date <> Nothing Then
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPromotion_date)
            Else
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            If mdtActualdate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualdate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@arrears_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecArrears_amount)
            objDataOperation.AddParameter("@arrears_countryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintArrears_countryid)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_amount)
            objDataOperation.AddParameter("@approval_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproval_statusunkid)
            objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalapproverunkid)
            objDataOperation.AddParameter("@arrears_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintArrears_statusunkid)
            'Sohail (21 Jan 2020) -- End

            strQ = "INSERT INTO prsalaryincrement_tran ( " & _
              "  periodunkid " & _
              ", employeeunkid " & _
              ", incrementdate " & _
              ", currentscale " & _
              ", increment " & _
              ", newscale " & _
              ", gradegroupunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", isgradechange " & _
              ", isfromemployee " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime" & _
              ", voidreason " & _
              ", reason_id " & _
              ", increment_mode " & _
              ", percentage " & _
              ", isapproved " & _
              ", approveruserunkid " & _
              ", psoft_syncdatetime " & _
              ", rehiretranunkid " & _
              ", changetypeid " & _
              ", promotion_date " & _
              ", actualdate " & _
              ", arrears_amount " & _
              ", arrears_countryid " & _
              ", noofinstallment " & _
              ", emi_amount " & _
              ", approval_statusunkid " & _
              ", finalapproverunkid " & _
              ", arrears_statusunkid " & _
            ") VALUES (" & _
              "  @periodunkid " & _
              ", @employeeunkid " & _
              ", @incrementdate " & _
              ", @currentscale " & _
              ", @increment " & _
              ", @newscale " & _
              ", @gradegroupunkid " & _
              ", @gradeunkid " & _
              ", @gradelevelunkid " & _
              ", @isgradechange " & _
              ", @isfromemployee " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime" & _
              ", @voidreason " & _
              ", @reason_id " & _
              ", @increment_mode " & _
              ", @percentage " & _
              ", @isapproved " & _
              ", @approveruserunkid " & _
              ", @psoft_syncdatetime " & _
              ", @rehiretranunkid " & _
              ", @changetypeid " & _
              ", @promotion_date " & _
              ", @actualdate " & _
              ", @arrears_amount " & _
              ", @arrears_countryid " & _
              ", @noofinstallment " & _
              ", @emi_amount " & _
              ", @approval_statusunkid " & _
              ", @finalapproverunkid " & _
              ", @arrears_statusunkid " & _
            "); SELECT @@identity"
            'Sohail (21 Jan 2020) - [actualdate, arrears_amount, arrears_countryid, noofinstallment, emi_amount, approval_statusunkid, finalapproverunkid, arrears_statusunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSalaryincrementtranunkid = dsList.Tables(0).Rows(0).Item(0)

            If blnUpdateEmployeeScale = True Then
                'Sohail (14 Nov 2018) -- Start
                'Zuri Issue - Salary change that was approved but it didnt reflect (for flat rate salary head and on approvng salaruy change) in payroll in 75.1.
                'If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString) = False Then
                '    objDataOperation.ReleaseTransaction(False)
                '    Return False
                'End If
                Dim objTHead As New clsTransactionHead
                'Sohail (23 Dec 2019) -- Start
                'NMB Issue # : Users not able to login in self service when doing any transaction on earning and deduction screen.
                'objEmployee._Employeeunkid(mdtIncrementdate) = mintEmployeeunkid
                objEmployee._Employeeunkid(mdtIncrementdate, objDataOperation) = mintEmployeeunkid
                'Sohail (23 Dec 2019) -- End
                objTHead._Tranheadunkid(strDatabaseName) = objEmployee._Tranhedunkid
                If objTHead._Trnheadtype_Id = CInt(enTranHeadType.Informational) OrElse objTHead._Calctype_Id = CInt(enCalcType.FlatRate_Others) Then
                    If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, mintEmployeeunkid, objEmployee._Tranhedunkid, mintPeriodunkid, mdecNewscale, blnApplyUserAccessFilter, strFilerString) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
                'Sohail (14 Nov 2018) -- End               
            End If

            If InsertAuditTrailForSalaryIncrement(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If mblnIsapproved = True Then
                With objESalaryHistory
                    ._Approveruserunkid = mintApproveruserunkid
                    ._Changetypeid = mintChangetypeid
                    ._Currentscale = mdecCurrentscale
                    ._Employeeunkid = mintEmployeeunkid
                    ._Gradegroupunkid = mintGradegroupunkid
                    ._Gradelevelunkid = mintGradelevelunkid
                    ._Gradeunkid = mintGradeunkid
                    ._Increment = mdecIncrement
                    ._Increment_Mode = mintIncrement_mode
                    ._Isapproved = mblnIsapproved
                    ._Incrementdate = mdtIncrementdate
                    ._Isfromemployee = mblnIsfromemployee
                    ._Isgradechange = mblnIsgradechange
                    ._Isvoid = mblnIsvoid
                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                    ._Newscale = mdecNewscale
                    ._objDataOperation = objDataOperation
                    ._Percentage = mdblPercentage
                    ._Periodunkid = mintPeriodunkid
                    ._Promotion_Date = mdtPromotion_date
                    ._Psoft_Syncdatetime = mdtPSoft_SyncDateTime
                    ._Reason_Id = mstrReason_Id
                    ._Rehiretranunkid = mintRehiretranunkid
                    ._Salaryincrementtranunkid = mintSalaryincrementtranunkid
                    ._Userunkid = mintUserunkid
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason
                    ._Voiduserunkid = mintVoiduserunkid
                    ._WebFormName = mstrWebFormName
                    ._WebHostName = mstrWebhostName
                    ._WebIP = mstrWebIP
                End With
                If objESalaryHistory.Insert(dtCurrentDateAndTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [19 OCT 2016] -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP [19 OCT 2016] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END
            objEmployee = Nothing
            objMaster = Nothing
            objWagesMaster = Nothing
            objWages = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prsalaryincrement_tran) </purpose>
    Public Function Update(ByVal strDatabaseName As String _
                           , ByVal xUserUnkid As Integer _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal blnAllowToApproveEarningDeduction As Boolean _
                           , ByVal dtCurrentDateAndTime As Date _
                           , Optional ByVal blnUpdateEmployeeScale As Boolean = True _
                           , Optional ByVal blnGlobal As Boolean = False _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal strFilerString As String = "" _
                           , Optional ByRef blnIsTableDataUpdated As Boolean = False _
                           ) As Boolean
        'Sohail (29 Sep 2021) - [blnIsTableDataUpdated]

        blnIsTableDataUpdated = False 'Sohail (29 Sep 2021)

        If isExistOnSameDate(mdtIncrementdate, mintEmployeeunkid, mintSalaryincrementtranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry! Increment for the employee on the selected date is already done.")
            Return False
        End If

        'Sohail (21 Jan 2020) -- Start
        'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
        If isExistOnSameDate(mdtActualdate, mintEmployeeunkid, mintSalaryincrementtranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry! Increment for the employee on the selected actual date is already done.")
            Return False
        End If
        'Sohail (21 Jan 2020) -- End

        If mdtPromotion_date <> Nothing Then
            If isExistOnSameDate(mdtPromotion_date, mintEmployeeunkid, mintSalaryincrementtranunkid, True) Then
                mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry! Promotion for the employee on the selected date is already done.")
                Return False
            End If
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objWagesMaster As New clsWages
        Dim objWages As New clsWagesTran

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
            'S.SANDEEP [04 JUL 2016] -- START
            objDataOperation.ClearParameters()
            'S.SANDEEP [04 JUL 2016] -- END
        End If

        If blnGlobal = False Then
            If xDataOpr Is Nothing Then
                objDataOperation.BindTransaction()
            End If
        End If

        Try
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
            objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentscale.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewscale.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrReason_Id.ToString)
            objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_mode.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            'HYATT Enhancement - OUTBOUND file integration with Aruti.
            If mdtPSoft_SyncDateTime = Nothing Then
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
            End If
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
            objDataOperation.AddParameter("@changetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangetypeid)
            If mdtPromotion_date <> Nothing Then
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPromotion_date)
            Else
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            If mdtActualdate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualdate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@arrears_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecArrears_amount)
            objDataOperation.AddParameter("@arrears_countryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintArrears_countryid)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_amount)
            objDataOperation.AddParameter("@approval_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproval_statusunkid)
            objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalapproverunkid)
            objDataOperation.AddParameter("@arrears_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintArrears_statusunkid)
            'Sohail (21 Jan 2020) -- End

            strQ = "UPDATE prsalaryincrement_tran SET " & _
              "  periodunkid = @periodunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", incrementdate = @incrementdate" & _
              ", currentscale = @currentscale" & _
              ", increment = @increment" & _
              ", newscale = @newscale" & _
              ", gradegroupunkid = @gradegroupunkid" & _
              ", gradeunkid = @gradeunkid" & _
              ", gradelevelunkid = @gradelevelunkid" & _
              ", isgradechange = @isgradechange" & _
              ", isfromemployee = @isfromemployee " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
              ", reason_id = @reason_id " & _
              ", increment_mode = @increment_mode " & _
              ", percentage = @percentage " & _
              ", isapproved = @isapproved " & _
              ", approveruserunkid = @approveruserunkid " & _
              ", psoft_syncdatetime = @psoft_syncdatetime " & _
              ", rehiretranunkid = @rehiretranunkid " & _
              ", changetypeid = @changetypeid " & _
              ", promotion_date = @promotion_date " & _
              ", actualdate = @actualdate " & _
              ", arrears_amount = @arrears_amount " & _
              ", arrears_countryid = @arrears_countryid " & _
              ", noofinstallment = @noofinstallment " & _
              ", emi_amount = @emi_amount " & _
              ", approval_statusunkid = @approval_statusunkid " & _
              ", finalapproverunkid = @finalapproverunkid " & _
              ", arrears_statusunkid = @arrears_statusunkid " & _
            "WHERE salaryincrementtranunkid = @salaryincrementtranunkid "
            'Sohail (21 Jan 2020) - [actualdate, arrears_amount, arrears_countryid, noofinstallment, emi_amount, approval_statusunkid, finalapproverunkid, arrears_statusunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If blnUpdateEmployeeScale = True Then
                'Sohail (14 Nov 2018) -- Start
                'Zuri Issue - Salary change that was approved but it didnt reflect (for flat rate salary head and on approvng salaruy change) in payroll in 75.1.
                'If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString) = False Then
                '    If xDataOpr Is Nothing Then
                '        objDataOperation.ReleaseTransaction(False)
                '    End If
                '    Return False
                'End If
                Dim objTHead As New clsTransactionHead
                'Sohail (23 Dec 2019) -- Start
                'NMB Issue # : Users not able to login in self service when doing any transaction on earning and deduction screen.
                'objEmployee._Employeeunkid(mdtIncrementdate) = mintEmployeeunkid
                objEmployee._Employeeunkid(mdtIncrementdate, objDataOperation) = mintEmployeeunkid
                'Sohail (23 Dec 2019) -- End
                objTHead._xDataOperation = objDataOperation 'Sohail (29 Sep 2021)
                objTHead._Tranheadunkid(strDatabaseName) = objEmployee._Tranhedunkid
                If objTHead._Trnheadtype_Id = CInt(enTranHeadType.Informational) OrElse objTHead._Calctype_Id = CInt(enCalcType.FlatRate_Others) Then
                    'Sohail (06 Dec 2019) -- Start
                    'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
                    'If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, mintEmployeeunkid, objEmployee._Tranhedunkid, mintPeriodunkid, mdecNewscale, blnApplyUserAccessFilter, strFilerString) = False Then
                    Dim intPeriodID As Integer = mintPeriodunkid
                    If mintPeriodunkid <= 0 Then
                        intPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, mdtIncrementdate, xYearUnkid, enStatusType.OPEN, True, False, objDataOperation, False, True)
                    End If
                    If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, mintEmployeeunkid, objEmployee._Tranhedunkid, intPeriodID, mdecNewscale, blnApplyUserAccessFilter, strFilerString) = False Then
                        'Sohail (06 Dec 2019) -- End
                    If xDataOpr Is Nothing Then
                        objDataOperation.ReleaseTransaction(False)
                    End If
                    Return False
                End If
            End If
                'Sohail (14 Nov 2018) -- End
            End If

            If IsTableDataUpdate(mintSalaryincrementtranunkid, objDataOperation) = True Then 'Sohail (29 Sep 2021)
            If InsertAuditTrailForSalaryIncrement(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                Return False
            End If
                'Sohail (29 Sep 2021) -- Start
                'NMB Enhancement : OLD - 484 : Notification when salary change is edited/deleted.
                blnIsTableDataUpdated = True
            End If
            'Sohail (29 Sep 2021) -- End

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing 'Sohail (29 Sep 2021)
            objEmployee = Nothing
            objMaster = Nothing
            objWagesMaster = Nothing
            objWages = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prsalaryincrement_tran) </purpose>
    Public Function Void(ByVal strDatabaseName As String _
                         , ByVal xYearUnkid As Integer _
                         , ByVal xCompanyUnkid As Integer _
                         , ByVal xPeriodStart As DateTime _
                         , ByVal xPeriodEnd As DateTime _
                         , ByVal xUserModeSetting As String _
                         , ByVal xOnlyApproved As Boolean _
                         , ByVal xIncludeIn_ActiveEmployee As Boolean _
                         , ByVal intUnkid As Integer _
                         , ByVal intVoidUserID As Integer _
                         , ByVal dtVoidDateTime As DateTime _
                         , ByVal strVoidReason As String _
                         , ByVal blnAllowToApproveEarningDeduction As Boolean _
                         , ByVal dtCurrentDateAndTime As Date _
                         , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                         , Optional ByVal strFilerString As String = "" _
                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                         ) As Boolean 'S.SANDEEP [01 JUN 2016] -- START -- END

        'Sohail (21 Aug 2015) - [strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]
        '<TODO umcomment when isused methods is complete
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objWagesMaster As New clsWages
        Dim objWages As New clsWagesTran
        'S.SANDEEP [22 SEP 2016] -- START
        Dim objSalHistory As New clsempsalary_history_tran
        'S.SANDEEP [22 SEP 2016] -- END



        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            'strQ = "DELETE FROM prsalaryincrement_tran " & _
            '"WHERE salaryincrementtranunkid = @salaryincrementtranunkid "
            strQ = "UPDATE prsalaryincrement_tran SET " & _
              " isvoid = 1" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
            "WHERE salaryincrementtranunkid = @salaryincrementtranunkid "

            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            End If

            Me._Salaryincrementtranunkid = intUnkid

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ' -----> NO NEED TO UPDATE EMPLOYEE COZ - DATA WILL BE DISPLAYED FROM SALARY INCREMENT ON EMPLOYEE

            'objEmployee._Employeeunkid = mintEmployeeunkid

            'dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, DateTime.Today)
            'If dsList.Tables("Salary").Rows.Count > 0 Then
            '    objEmployee._Scale = Format(dsList.Tables("Salary").Rows(0).Item("newscale"), "#0.00")
            '    objEmployee._Gradegroupunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradegroupunkid").ToString)
            '    objEmployee._Gradeunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradeunkid").ToString)
            '    objEmployee._Gradelevelunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString)
            'Else
            '    dsList = objWages.getScaleInfo(objEmployee._Gradeunkid, objEmployee._Gradelevelunkid, "Scale")
            '    objWagesMaster._Wagesunkid = CInt(dsList.Tables("Scale").Rows(0).Item("wagesunkid").ToString)
            '    If dsList.Tables("Scale").Rows.Count > 0 Then
            '        objEmployee._Scale = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), "#0.00")
            '        objEmployee._Gradegroupunkid = objWagesMaster._Gradegroupunkid
            '        objEmployee._Gradeunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradeunkid").ToString)
            '        objEmployee._Gradelevelunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradelevelunkid").ToString)
            '    Else
            '        objEmployee._Scale = "0.00"
            '    End If
            'End If

            'objEmployee.Update(, , True)

            ''Sohail (12 Oct 2011) -- Start
            'If objEmployee._Message <> "" Then
            '    objDataOperation.ReleaseTransaction(False)
            '    exForce = New Exception(objEmployee._Message)
            '    Throw exForce
            'End If
            ''Sohail (12 Oct 2011) -- End

            'S.SANDEEP [04 JUN 2015] -- END



            'S.SANDEEP [ 04 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            ''Sohail (11 Nov 2010) -- Start
            'Me._Salaryincrementtranunkid = intUnkid
            'Call InsertAuditTrailForSalaryIncrement(objDataOperation, 3)
            ''Sohail (11 Nov 2010) -- End

            'S.SANDEEP [21-JUN-2017] -- START
            objEmployee._DataOperation = objDataOperation
            'S.SANDEEP [21-JUN-2017] -- END
            'Sohail (23 Dec 2019) -- Start
            'NMB Issue # : Users not able to login in self service when doing any transaction on earning and deduction screen.
            'objEmployee._Employeeunkid(xPeriodEnd) = mintEmployeeunkid
            objEmployee._Employeeunkid(xPeriodEnd, objDataOperation) = mintEmployeeunkid
            'Sohail (23 Dec 2019) -- End
            Me._Salaryincrementtranunkid = intUnkid

            Dim ObjTHead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start

            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'ObjTHead._Tranheadunkid = objEmployee._Tranhedunkid

            'S.SANDEEP [21-JUN-2017] -- START
            ObjTHead._xDataOperation = objDataOperation
            'S.SANDEEP [21-JUN-2017] -- END
            ObjTHead._Tranheadunkid(strDatabaseName) = objEmployee._Tranhedunkid
            'Sohail (21 Aug 2015) -- End
            If ObjTHead._Trnheadtype_Id = enTranHeadType.Informational Then
                Dim objED As New clsEarningDeduction
                'S.SANDEEP [21-JUN-2017] -- START
                'dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, mdtIncrementdate.Date)
                objED._DataOperation = objDataOperation
                dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, mdtIncrementdate.Date, objDataOperation)
                'S.SANDEEP [21-JUN-2017] -- End
                If dsList.Tables("Salary").Rows.Count > 0 Then
                    'Sohail (09 Nov 2013) -- Start
                    'TRA - ENHANCEMENT
                    'If objED.InsertAllByEmployeeList(mintEmployeeunkid, _
                    '                                 objEmployee._Tranhedunkid, _
                    '                                 0, _
                    '                                 0, _
                    '                                 0, _
                    '                                 dsList.Tables("Salary").Rows(0).Item("newscale"), _
                    '                                 True, _
                    '                                 False, _
                    '                                 mintPeriodunkid, _
                    '                                 "", _
                    '                                 objDataOperation, _
                    '                                 False, _
                    '                                 False, _
                    '                                 False) = False Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objED.InsertAllByEmployeeList(mintEmployeeunkid, _
                    '                                 objEmployee._Tranhedunkid, _
                    '                                 0, _
                    '                                 0, _
                    '                                 0, _
                    '                                 dsList.Tables("Salary").Rows(0).Item("newscale"), _
                    '                                 True, _
                    '                                 False, _
                    '                                 mintPeriodunkid, _
                    '                                 "", _
                    '                                 Nothing, _
                    '                                 Nothing, _
                    '                                 objDataOperation, _
                    '                                 False, _
                    '                                 False, _
                    '                                 False) = False Then
                    If objED.InsertAllByEmployeeList(strDatabaseName, intVoidUserID, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, _
                                                     objEmployee._Tranhedunkid, _
                                                     0, _
                                                     0, _
                                                     0, _
                                                     dsList.Tables("Salary").Rows(0).Item("newscale"), _
                                                     True, _
                                                     False, _
                                                     mintPeriodunkid, _
                                                     "", _
                                                     Nothing, _
                                                     Nothing, _
                                                     blnAllowToApproveEarningDeduction, _
                                                      objDataOperation, dtCurrentDateAndTime, _
                                                     False, _
                                                     False, _
                                                     False, blnApplyUserAccessFilter, strFilerString) = False Then
                        'Sohail (21 Aug 2015) -- End
                        'Sohail (09 Nov 2013) -- End

                        'S.SANDEEP [21-JUN-2017] -- START
                        'objDataOperation.ReleaseTransaction(False)
                        If objDataOperation Is Nothing Then
                        objDataOperation.ReleaseTransaction(False)
                        End If
                        'S.SANDEEP [21-JUN-2017] -- END
                        Return True
                    End If
                End If
            End If
            ObjTHead = Nothing

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call InsertAuditTrailForSalaryIncrement(objDataOperation, 3)
            If InsertAuditTrailForSalaryIncrement(objDataOperation, 3, dtCurrentDateAndTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [ 04 SEP 2012 ] -- END


            'S.SANDEEP [22 SEP 2016] -- START
            Dim dsHistoryData As New DataSet
            objDataOperation.ClearParameters()
            dsHistoryData = clsCommonATLog.GetChildList(objDataOperation, "prempsalary_history_tran", "salaryincrementtranunkid", intUnkid)
            If dsHistoryData.Tables(0).Rows.Count > 0 Then
                For Each xrow As DataRow In dsHistoryData.Tables(0).Rows
                    objDataOperation.ClearParameters()

                    'S.SANDEEP [25 OCT 2016] -- START
                    objSalHistory._Isvoid = True
                    objSalHistory._Voiddatetime = dtVoidDateTime
                    objSalHistory._Voidreason = strVoidReason
                    objSalHistory._Voiduserunkid = intVoidUserID
                    'S.SANDEEP [25 OCT 2016] -- END

                    If objSalHistory.Delete(xrow.Item("empsalaryhistorytranunkid"), dtCurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Next
            End If
            'S.SANDEEP [22 SEP 2016] -- END


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objEmployee = Nothing
            objMaster = Nothing
            objWagesMaster = Nothing
            objWages = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            '<TODO make query>
            strQ = "<Query>"

            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intPeriodUnkID As Integer, ByVal intEmployeeUnkID As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (09 Oct 2010) -- Start
            'Changes : isautogenerated is renamed with isfromemployee
            'strQ = "SELECT " & _
            '  "  salaryincrementtranunkid " & _
            '  ", periodunkid " & _
            '  ", employeeunkid " & _
            '  ", incrementdate " & _
            '  ", currentscale " & _
            '  ", increment " & _
            '  ", newscale " & _
            '  ", gradegroupunkid " & _
            '  ", gradeunkid " & _
            '  ", gradelevelunkid " & _
            '  ", isgradechange " & _
            '  ", isautogenerated " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            ' "FROM prsalaryincrement_tran " & _
            ' "WHERE periodunkid = @periodunkid " & _
            ' "AND employeeunkid = @employeeunkid " & _
            ' "AND isvoid = 0"
            strQ = "SELECT " & _
              "  salaryincrementtranunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", incrementdate " & _
              ", currentscale " & _
              ", increment " & _
              ", newscale " & _
              ", gradegroupunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", isgradechange " & _
              ", isfromemployee " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", reason_id " & _
              ", increment_mode " & _
              ", percentage " & _
              ", isapproved " & _
              ", approveruserunkid " & _
             "FROM prsalaryincrement_tran " & _
             "WHERE periodunkid = @periodunkid " & _
             "AND employeeunkid = @employeeunkid " & _
             "AND ISNULL(isvoid,0) = 0" 'Sohail (16 Oct 2010), 'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]
            'Sohail (24 Sep 2012) - [isapproved, approveruserunkid]
            'Sohail (09 Oct 2010) -- End
            If intUnkid > 0 Then
                strQ &= " AND salaryincrementtranunkid <> @salaryincrementtranunkid"
                objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intPeriodUnkID)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmployeeUnkID)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistOnSameDate(ByVal dtIncrementDate As DateTime, ByVal intEmployeeUnkID As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal blnCheckSamePromotionDate As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [14 APR 2015] -- START
        'objDataOperation = New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [14 APR 2015] -- END

        Try

            strQ = "SELECT " & _
              "  salaryincrementtranunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", incrementdate " & _
              ", currentscale " & _
              ", increment " & _
              ", newscale " & _
              ", gradegroupunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", isgradechange " & _
              ", isfromemployee " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", reason_id " & _
              ", increment_mode " & _
              ", percentage " & _
              ", isapproved " & _
              ", approveruserunkid " & _
             "FROM prsalaryincrement_tran " & _
             "WHERE ISNULL(isvoid,0) = 0 " & _
             "AND employeeunkid = @employeeunkid "

            If blnCheckSamePromotionDate Then
                strQ &= " AND CONVERT(CHAR(8), promotion_date, 112) = @promotion_date "
                objDataOperation.AddParameter("@promotion_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtIncrementDate))
            Else
                strQ &= " AND CONVERT(CHAR(8), incrementdate, 112) = @incrementdate "
                objDataOperation.AddParameter("@incrementdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtIncrementDate))
            End If



            If intUnkid > 0 Then
                strQ &= " AND salaryincrementtranunkid <> @salaryincrementtranunkid"
                objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmployeeUnkID)



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistOnSameDate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [14 APR 2015] -- START
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
            'S.SANDEEP [14 APR 2015] -- END
        End Try
    End Function

    Public Function getLastIncrement(ByVal strListName As String, ByVal intEmployeeUnkID As Integer, Optional ByVal blnIsForValidation As Boolean = False) As DataSet
        '                           'Sohail (07 Sep 2013) - [blnIsForValidation]
        Dim dsList As DataSet = Nothing
        'Dim dtIncrement As Date = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (09 Oct 2010) -- Start
            'Changes : isautogenerated is renamed with isfromemployee
            'strQ = "SELECT TOP 1 " & _
            '    "  salaryincrementtranunkid " & _
            '    ", periodunkid " & _
            '    ", employeeunkid " & _
            '    ", CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) AS incrementdate " & _
            '    ", currentscale " & _
            '    ", increment " & _
            '    ", newscale " & _
            '    ", gradegroupunkid " & _
            '    ", gradeunkid " & _
            '    ", gradelevelunkid " & _
            '    ", isgradechange " & _
            '    ", isautogenerated " & _
            '    ", userunkid " & _
            '    ", isvoid " & _
            '    ", voiduserunkid " & _
            '    ", voiddatetime " & _
            '    ", voidreason " & _
            '"FROM prsalaryincrement_tran " & _
            '"WHERE employeeunkid = @employeeunkid " & _
            '"AND isvoid = 0" & _
            '"ORDER BY incrementdate DESC"
            strQ = "SELECT TOP 1 " & _
                "  salaryincrementtranunkid " & _
                ", periodunkid " & _
                ", employeeunkid " & _
                ", CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) AS incrementdate " & _
                ", currentscale " & _
                ", increment " & _
                ", newscale " & _
                ", prsalaryincrement_tran.gradegroupunkid " & _
                ", prsalaryincrement_tran.gradeunkid " & _
                ", prsalaryincrement_tran.gradelevelunkid " & _
                ", isgradechange " & _
                ", isfromemployee " & _
                ", userunkid " & _
                ", isvoid " & _
                ", voiduserunkid " & _
                ", voiddatetime " & _
                ", voidreason " & _
                ", reason_id " & _
                ", increment_mode " & _
                ", percentage " & _
                ", prsalaryincrement_tran.isapproved " & _
                ", prsalaryincrement_tran.actualdate " & _
                ", ISNULL(prsalaryincrement_tran.arrears_amount, 0) AS arrears_amount " & _
                ", ISNULL(prsalaryincrement_tran.arrears_countryid, 0) AS arrears_countryid " & _
                ", ISNULL(prsalaryincrement_tran.noofinstallment, 0) AS noofinstallment " & _
                ", ISNULL(prsalaryincrement_tran.emi_amount, 0) AS emi_amount " & _
                ", ISNULL(prsalaryincrement_tran.approval_statusunkid, 0) AS approval_statusunkid " & _
                ", ISNULL(prsalaryincrement_tran.finalapproverunkid, 0) AS finalapproverunkid " & _
                ", ISNULL(prsalaryincrement_tran.arrears_statusunkid, 0) AS arrears_statusunkid "
            'Sohail (21 Jan 2020) - [actualdate, arrears_amount, arrears_countryid, noofinstallment, emi_amount, approval_statusunkid, finalapproverunkid, arrears_statusunkid]
            'Sohail (07 Sep 2013) - [isapproved]

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            strQ &= ", hrgradegroup_master.name AS GradeGroupName " & _
                    ", hrgrade_master.name AS GradeName " & _
                    ", hrgradelevel_master.name AS GradeLevelName " & _
                    ", prsalaryincrement_tran.psoft_syncdatetime " & _
                    ", ISNULL(hrgradegroup_master.priority, 0) AS GradeGroupPriority " & _
                    ", ISNULL(hrgrade_master.priority, 0) AS GradePriority " & _
                    ", ISNULL(hrgradelevel_master.priority, 0) AS GradeLevelPriority "
            'Sohail (27 Apr 2016) - [GradeGroupPriority, GradePriority, GradeLevelPriority]
            'Sohail (27 Nov 2014) - [psoft_syncdatetime]
            'Sohail (20 Jun 2012) -- End

            strQ &= "FROM prsalaryincrement_tran "

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            strQ &= "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = prsalaryincrement_tran.gradegroupunkid " & _
                    "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prsalaryincrement_tran.gradeunkid " & _
                    "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = prsalaryincrement_tran.gradelevelunkid "
            'Sohail (20 Jun 2012) -- End

            strQ &= "WHERE employeeunkid = @employeeunkid " & _
            "AND ISNULL(isvoid,0) = 0 "

            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            '"AND prsalaryincrement_tran.isapproved = @isapproved " 
            If blnIsForValidation = False Then
                strQ &= "AND prsalaryincrement_tran.isapproved = @isapproved "
            End If
            'Sohail (07 Sep 2013) -- End


            strQ &= "ORDER BY incrementdate DESC" 'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]
            'Sohail (24 Sep 2012) - [isapproved]
            'Sohail (09 Oct 2010) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, enSalaryChangeApprovalStatus.Approved) 'Sohail (24 Sep 2012)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If dsList.Tables(strListName).Rows.Count > 0 Then
            '    dtIncrement = CDate(dsList.Tables(strListName).Rows(0).Item("incrementdate").ToString)
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getLastIncrementDate; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Sohail (18 Dec 2018) -- Start
    'Internal Issue - Don't allow to rehire if salary increment date is greater that rehirement date is already exist in 76.1.
    Public Function IsFutureIncrementExist(ByVal xDataOp As clsDataOperation, ByVal intEmployeeUnkID As Integer, ByVal dtCurrIncrementDate As Date) As Boolean
        '                           'Sohail (07 Sep 2013) - [blnIsForValidation]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            
            strQ = "SELECT TOP 1 " & _
                        "  CONVERT(CHAR(8), prsalaryincrement_tran.incrementdate, 112) AS incrementdate " & _
                    "FROM prsalaryincrement_tran " & _
                        "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = prsalaryincrement_tran.gradegroupunkid " & _
                        "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prsalaryincrement_tran.gradeunkid " & _
                        "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = prsalaryincrement_tran.gradelevelunkid " & _
                    "WHERE  prsalaryincrement_tran.isvoid = 0 " & _
                        "AND employeeunkid = @employeeunkid " & _
                        "AND CONVERT(CHAR(8), prsalaryincrement_tran.incrementdate, 112) >= @currincrementdate "


            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
            objDataOperation.AddParameter("@currincrementdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtCurrIncrementDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsFutureIncrementExist; Module Name: " & mstrModuleName)
            Return False
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (18 Dec 2018) -- End

    ''' <summary>
    ''' Get Employee List whose increment is done after the given Date
    ''' </summary>
    ''' <param name="dtIncrementDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetEmployeeList(ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As DateTime _
                                    , ByVal xPeriodEnd As DateTime _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , ByVal strTable As String _
                                    , ByVal dtIncrementDate As Date _
                                    , ByVal blnApplyUserAccessFilter As Boolean _
                                    , ByVal strFilerString As String _
                                    , Optional ByVal intSalaryChangeApprovalStatus As Integer = enSalaryChangeApprovalStatus.All _
                                    , Optional ByVal intIncrementBy As Integer = 0 _
                                    ) As DataSet
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFilerString]
        '                           'Sohail (24 Sep 2012) - [intSalaryChangeApprovalStatus]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        'Sohail (21 Aug 2015) -- End

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                          "  prsalaryincrement_tran.salaryincrementtranunkid " & _
                          ", prsalaryincrement_tran.periodunkid " & _
                          ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", prsalaryincrement_tran.employeeunkid " & _
                          ", CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) AS incrementdate " & _
                          ", prsalaryincrement_tran.currentscale " & _
                          ", prsalaryincrement_tran.increment " & _
                          ", prsalaryincrement_tran.newscale " & _
                          ", prsalaryincrement_tran.gradegroupunkid " & _
                          ", prsalaryincrement_tran.gradeunkid " & _
                          ", prsalaryincrement_tran.gradelevelunkid " & _
                          ", prsalaryincrement_tran.isgradechange " & _
                          ", prsalaryincrement_tran.isfromemployee " & _
                          ", prsalaryincrement_tran.userunkid " & _
                          ", prsalaryincrement_tran.isvoid " & _
                          ", prsalaryincrement_tran.voiduserunkid " & _
                          ", prsalaryincrement_tran.voiddatetime " & _
                          ", prsalaryincrement_tran.voidreason " & _
                          ", prsalaryincrement_tran.reason_id " & _
                          ", prsalaryincrement_tran.increment_mode " & _
                          ", prsalaryincrement_tran.percentage " & _
                          ", prsalaryincrement_tran.psoft_syncdatetime " & _
                 "FROM prsalaryincrement_tran " & _
                         "LEFT JOIN hremployee_master ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " & _
                         "LEFT JOIN cfcommon_period_tran ON prsalaryincrement_tran.periodunkid = cfcommon_period_tran.periodunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "WHERE prsalaryincrement_tran.isvoid = 0 " & _
                "AND ( CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) >= @IncrementDate "

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            If intIncrementBy = enSalaryIncrementBy.Auto_Increment Then
                strQ &= " OR ( MONTH(prsalaryincrement_tran.incrementdate) = MONTH(@IncrementDate) AND YEAR(prsalaryincrement_tran.incrementdate) = YEAR(@IncrementDate) ) "
            End If

            strQ &= " ) "
            'Sohail (27 Apr 2016) -- End
            'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]
            'Sohail (27 Nov 2014) - [psoft_syncdatetime]

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If
            'Sohail (21 Aug 2015) -- End

            'Sohail (24 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            If intSalaryChangeApprovalStatus = enSalaryChangeApprovalStatus.Approved Then
                strQ &= " AND prsalaryincrement_tran.isapproved = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, intSalaryChangeApprovalStatus)
            ElseIf intSalaryChangeApprovalStatus = enSalaryChangeApprovalStatus.Pending Then
                strQ &= " AND prsalaryincrement_tran.isapproved = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            End If
            'Sohail (24 Sep 2012) -- End

            objDataOperation.AddParameter("@IncrementDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtIncrementDate))

            'Anjan (09 Aug 2011)-Start
            'Issue : For including setting of acitve and inactive employee.
            'Sohail (31 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'ISSUE : Employee do not come in list if it is terminated after AS ON EMPLOYEE DATE : SO Increment may be done twice in one period
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    'Sohail (06 Jan 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    'Sohail (06 Jan 2012) -- End
            'End If
            'Sohail (31 Mar 2012) -- End
            'Anjan (09 Aug 2011)-End

            'Sohail (24 Jun 2011) -- Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select
            'strQ &= UserAccessLevel._AccessLevelFilterString 'Sohail (21 Aug 2015)
            'S.SANDEEP [ 01 JUNE 2012 ] -- END


            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Sohail (24 Jun 2011) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTable)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Public Function GetPendingEmployeeList(ByVal xDatabaseName As String _
                                            , ByVal xUserUnkid As Integer _
                                            , ByVal xYearUnkid As Integer _
                                            , ByVal xCompanyUnkid As Integer _
                                            , ByVal xPeriodStart As DateTime _
                                            , ByVal xPeriodEnd As DateTime _
                                            , ByVal xUserModeSetting As String _
                                            , ByVal xOnlyApproved As Boolean _
                                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                            , ByVal strTable As String _
                                            , ByVal dtIncrementDate As Date _
                                            , ByVal blnApplyUserAccessFilter As Boolean _
                                            , ByVal strFilerString As String _
                                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        If blnApplyUserAccessFilter = True Then 'Sohail (12 Oct 2021)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        End If 'Sohail (12 Oct 2021)
        If strFilerString.Trim.Length > 0 Then 'Sohail (12 Oct 2021)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        End If 'Sohail (12 Oct 2021)

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  A.empsalaryanniversaryunkid  " & _
                          ", A.employeeunkid " & _
                          ", CONVERT(CHAR(8), A.effectivedate, 112) AS effectivedate " & _
                          ", A.anniversarymonth " & _
                          ", hremployee_master.employeecode " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
                    "FROM    ( SELECT    hremployee_salary_anniversary_tran.*  " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY hremployee_salary_anniversary_tran.employeeunkid ORDER BY hremployee_salary_anniversary_tran.effectivedate DESC, hremployee_salary_anniversary_tran.empsalaryanniversaryunkid DESC ) AS ROWNO " & _
                              "FROM      hremployee_salary_anniversary_tran " & _
                              "WHERE     hremployee_salary_anniversary_tran.isvoid = 0 " & _
                                        "AND CONVERT(CHAR(8), hremployee_salary_anniversary_tran.effectivedate, 112) <= @periodenddate " & _
                            ") AS A " & _
                            "LEFT JOIN prsalaryincrement_tran ON A.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
                                                                "AND A.anniversarymonth = MONTH(prsalaryincrement_tran.incrementdate) " & _
                                                                "AND YEAR(prsalaryincrement_tran.incrementdate) = YEAR(@periodenddate) " & _
                                                                "AND prsalaryincrement_tran.isvoid = 0 " & _
                            "LEFT JOIN hremployee_master ON A.employeeunkid = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE   a.ROWNO = 1 " & _
                            "AND A.anniversarymonth = MONTH(@periodenddate) " & _
                            "AND prsalaryincrement_tran.salaryincrementtranunkid IS NULL "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If


            'Pinkal (17-Sep-2018) -- Start
            'Bug -  0002497: Unable to close payroll period
            If xDateFilterQry.Trim.Length > 0 Then
                strQ &= xDateFilterQry
            End If
            'Pinkal (17-Sep-2018) -- End

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If


            objDataOperation.AddParameter("@periodenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtIncrementDate))

            dsList = objDataOperation.ExecQuery(strQ, strTable)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPendingEmployeeList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (27 Apr 2016) -- End

    Public Function InsertAll(ByVal strDatabaseName As String _
                              , ByVal xUserUnkid As Integer _
                              , ByVal xYearUnkid As Integer _
                              , ByVal xCompanyUnkid As Integer _
                              , ByVal xPeriodStart As DateTime _
                              , ByVal xPeriodEnd As DateTime _
                              , ByVal xUserModeSetting As String _
                              , ByVal xOnlyApproved As Boolean _
                              , ByVal xIncludeIn_ActiveEmployee As Boolean _
                              , ByVal dtTable As DataTable _
                              , ByVal blnAllowToApproveEarningDeduction As Boolean _
                              , ByVal dtCurrentDateAndTime As Date _
                              , Optional ByRef dicAppraisal As Dictionary(Of String, ArrayList) = Nothing _
                              , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                              , Optional ByVal strFilerString As String = "" _
                              ) As Boolean
        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objWagesMaster As New clsWages
        Dim objWages As New clsWagesTran
        Dim decMaxScale As Decimal = 0
        Dim arrList As ArrayList
        Dim objTHead As New clsTransactionHead
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            For Each dtRow As DataRow In dtTable.Rows
                objDataOperation.ClearParameters()

                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdecCurrentscale = CDec(dtRow.Item("currentscale"))
                mdecIncrement = CDec(dtRow.Item("increment"))
                mdecNewscale = CDec(dtRow.Item("newscale"))
                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintGradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                mintIncrement_mode = CInt(dtRow.Item("increment_mode"))
                mdblPercentage = CDbl(dtRow.Item("percentage"))
                mblnIsgradechange = CBool(dtRow.Item("isgradechange"))
                If IsDBNull(dtRow.Item("promotion_date")) Then
                    mdtPromotion_date = Nothing
                Else
                    mdtPromotion_date = dtRow.Item("promotion_date")
                End If
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                mdecArrears_amount = CDec(dtRow.Item("arrears_amount"))
                mdecEmi_amount = CDec(dtRow.Item("emi_amount"))
                mintNoofinstallment = CInt(dtRow.Item("noofinstallment"))
                mintArrears_countryid = CInt(dtRow.Item("arrears_countryid"))
                mintApproval_statusunkid = CInt(dtRow.Item("approval_statusunkid"))
                mintFinalapproverunkid = CInt(dtRow.Item("finalapproverunkid"))
                mintArrears_statusunkid = CInt(dtRow.Item("arrears_statusunkid"))
                'Sohail (21 Jan 2020) -- End

                'Issue - Allow to Give Zero Increment if Salary Change is for Grade Change. Earlier increment was not gettng done if grade is changed with same Scale.
                If mblnIsgradechange = True OrElse mdecIncrement <> 0 Then
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                    objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
                    objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentscale.ToString)
                    objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString)
                    objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewscale.ToString)
                    objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
                    objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
                    objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
                    objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
                    objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    If mdtVoiddatetime = Nothing Then
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    Else
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    End If
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrReason_Id.ToString)
                    objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_mode.ToString)
                    objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
                    objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
                    objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
                    'HYATT Enhancement - OUTBOUND file integration with Aruti.
                    If mdtPSoft_SyncDateTime = Nothing Then
                        objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    Else
                        objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
                    End If
                    objDataOperation.AddParameter("@changetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangetypeid)
                    If mdtPromotion_date <> Nothing Then
                        objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPromotion_date)
                    Else
                        objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    End If
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    If mdtActualdate <> Nothing Then
                        objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualdate)
                    Else
                        objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    End If
                    objDataOperation.AddParameter("@arrears_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecArrears_amount)
                    objDataOperation.AddParameter("@arrears_countryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintArrears_countryid)
                    objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment)
                    objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_amount)
                    objDataOperation.AddParameter("@approval_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproval_statusunkid)
                    objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalapproverunkid)
                    objDataOperation.AddParameter("@arrears_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintArrears_statusunkid)
                    'Sohail (21 Jan 2020) -- End

                    strQ = "INSERT INTO prsalaryincrement_tran ( " & _
                                  "  periodunkid " & _
                                  ", employeeunkid " & _
                                  ", incrementdate " & _
                                  ", currentscale " & _
                                  ", increment " & _
                                  ", newscale " & _
                                  ", gradegroupunkid " & _
                                  ", gradeunkid " & _
                                  ", gradelevelunkid " & _
                                  ", isgradechange " & _
                                  ", isfromemployee " & _
                                  ", userunkid " & _
                                  ", isvoid " & _
                                  ", voiduserunkid " & _
                                  ", voiddatetime" & _
                                  ", voidreason " & _
                                  ", reason_id " & _
                                  ", increment_mode " & _
                                  ", percentage " & _
                                  ", isapproved " & _
                                  ", approveruserunkid " & _
                                  ", psoft_syncdatetime " & _
                                  ", changetypeid " & _
                                  ", promotion_date " & _
                                  ", actualdate " & _
                                  ", arrears_amount " & _
                                  ", arrears_countryid " & _
                                  ", noofinstallment " & _
                                  ", emi_amount " & _
                                  ", approval_statusunkid " & _
                                  ", finalapproverunkid " & _
                                  ", arrears_statusunkid " & _
                    ") VALUES (" & _
                              "  @periodunkid " & _
                              ", @employeeunkid " & _
                              ", @incrementdate " & _
                              ", @currentscale " & _
                              ", @increment " & _
                              ", @newscale " & _
                              ", @gradegroupunkid " & _
                              ", @gradeunkid " & _
                              ", @gradelevelunkid " & _
                              ", @isgradechange " & _
                              ", @isfromemployee " & _
                              ", @userunkid " & _
                              ", @isvoid " & _
                              ", @voiduserunkid " & _
                              ", @voiddatetime" & _
                              ", @voidreason " & _
                              ", @reason_id " & _
                              ", @increment_mode " & _
                              ", @percentage " & _
                              ", @isapproved " & _
                              ", @approveruserunkid " & _
                              ", @psoft_syncdatetime " & _
                              ", @changetypeid " & _
                              ", @promotion_date " & _
                              ", @actualdate " & _
                              ", @arrears_amount " & _
                              ", @arrears_countryid " & _
                              ", @noofinstallment " & _
                              ", @emi_amount " & _
                              ", @approval_statusunkid " & _
                              ", @finalapproverunkid " & _
                              ", @arrears_statusunkid " & _
                    "); SELECT @@identity"

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintSalaryincrementtranunkid = dsList.Tables(0).Rows(0).Item(0)

                    'Sohail (04 Jun 2018) -- Start
                    'FDRC Issue - Support Issue Id # 2296 : Salary change on importation for information Salary transaction heads doesn't reflect in payroll in 72.1.
                    'Sohail (23 Dec 2019) -- Start
                    'NMB Issue # : Users not able to login in self service when doing any transaction on earning and deduction screen.
                    'objEmployee._Employeeunkid(mdtIncrementdate) = mintEmployeeunkid
                    objEmployee._Employeeunkid(mdtIncrementdate, objDataOperation) = mintEmployeeunkid
                    'Sohail (23 Dec 2019) -- End
                    'Sohail (04 Jun 2018) -- End

                    objTHead._Tranheadunkid(strDatabaseName) = objEmployee._Tranhedunkid
                    'Sohail (04 Jun 2018) -- Start
                    'FDRC Issue - Support Issue Id # 2296 : Salary change on importation for information Salary transaction heads doesn't reflect in payroll in 72.1.
                    'If objTHead._Trnheadtype_Id = enTranHeadType.Informational Then
                    If objTHead._Trnheadtype_Id = CInt(enTranHeadType.Informational) OrElse objTHead._Calctype_Id = CInt(enCalcType.FlatRate_Others) Then
                        'Sohail (04 Jun 2018) -- End
                        'Sohail (14 Nov 2018) -- Start
                        'Zuri Issue - Salary change that was approved but it didnt reflect (for flat rate salary head and on approvng salaruy change) in payroll in 75.1.
                        'mintInfoSalHeadUnkid = objEmployee._Tranhedunkid
                        'If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString) = False Then
                        '    objDataOperation.ReleaseTransaction(False)
                        '    Return False
                        'End If
                        'mintInfoSalHeadUnkid = -1
                        If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, mintEmployeeunkid, objEmployee._Tranhedunkid, mintPeriodunkid, mdecNewscale, blnApplyUserAccessFilter, strFilerString) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        'Sohail (14 Nov 2018) -- End
                    End If

                    If InsertAuditTrailForSalaryIncrement(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    If mblnIsapproved = True Then
                        With objESalaryHistory
                            ._Approveruserunkid = mintApproveruserunkid
                            ._Changetypeid = mintChangetypeid
                            ._Currentscale = mdecCurrentscale
                            ._Employeeunkid = mintEmployeeunkid
                            ._Gradegroupunkid = mintGradegroupunkid
                            ._Gradelevelunkid = mintGradelevelunkid
                            ._Gradeunkid = mintGradeunkid
                            ._Increment = mdecIncrement
                            ._Increment_Mode = mintIncrement_mode
                            ._Isapproved = mblnIsapproved
                            ._Incrementdate = mdtIncrementdate
                            ._Isfromemployee = mblnIsfromemployee
                            ._Isgradechange = mblnIsgradechange
                            ._Isvoid = mblnIsvoid
                            ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                            ._Newscale = mdecNewscale
                            ._objDataOperation = objDataOperation
                            ._Percentage = mdblPercentage
                            ._Periodunkid = mintPeriodunkid
                            ._Promotion_Date = mdtPromotion_date
                            ._Psoft_Syncdatetime = mdtPSoft_SyncDateTime
                            ._Reason_Id = mstrReason_Id
                            ._Rehiretranunkid = mintRehiretranunkid
                            ._Salaryincrementtranunkid = mintSalaryincrementtranunkid
                            ._Userunkid = mintUserunkid
                            ._Voiddatetime = mdtVoiddatetime
                            ._Voidreason = mstrVoidreason
                            ._Voiduserunkid = mintVoiduserunkid
                            ._WebFormName = mstrWebFormName
                            ._WebHostName = mstrWebhostName
                            ._WebIP = mstrWebIP
                        End With
                        If objESalaryHistory.Insert(dtCurrentDateAndTime) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If

                    If dicAppraisal IsNot Nothing AndAlso dicAppraisal.ContainsKey(mintEmployeeunkid.ToString) = True Then
                        arrList = dicAppraisal.Item(mintEmployeeunkid.ToString)
                        arrList(1) = mintSalaryincrementtranunkid
                        dicAppraisal.Item(mintEmployeeunkid.ToString) = arrList
                    End If
                End If
                mintSuccessCnt = mintSuccessCnt + 1
            Next

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            objEmployee = Nothing
            objMaster = Nothing
            objWagesMaster = Nothing
            objWages = Nothing
        End Try
    End Function

    Public Function InsertAuditTrailForSalaryIncrement(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [25 OCT 2016] -- START
        'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
        'objDataOperation = New clsDataOperation
        'S.SANDEEP [25 OCT 2016] -- END
        Try
            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

            strQ = "INSERT INTO atprsalaryincrement_tran ( " & _
                          "  salaryincrementtranunkid " & _
                          ", periodunkid " & _
                          ", employeeunkid " & _
                          ", incrementdate " & _
                          ", currentscale " & _
                          ", increment " & _
                          ", newscale " & _
                          ", gradegroupunkid " & _
                          ", gradeunkid " & _
                          ", gradelevelunkid " & _
                          ", isgradechange " & _
                          ", isfromemployee " & _
                          ", audittype " & _
                          ", audituserunkid " & _
                          ", auditdatetime " & _
                          ", ip " & _
                          ", machine_name" & _
                          ", reason_id " & _
                          ", increment_mode " & _
                          ", percentage " & _
                          ", form_name " & _
                          ", module_name1 " & _
                          ", module_name2 " & _
                          ", module_name3 " & _
                          ", module_name4 " & _
                          ", module_name5 " & _
                          ", isweb " & _
                          ", isapproved " & _
                          ", approveruserunkid " & _
                          ", psoft_syncdatetime " & _
                          ", rehiretranunkid " & _
                          ", changetypeid " & _
                          ", promotion_date " & _
                          ", actualdate " & _
                          ", arrears_amount " & _
                          ", arrears_countryid " & _
                          ", noofinstallment " & _
                          ", emi_amount " & _
                          ", approval_statusunkid " & _
                          ", finalapproverunkid " & _
                          ", arrears_statusunkid " & _
                ") VALUES (" & _
                          "  @salaryincrementtranunkid " & _
                          ", @periodunkid " & _
                          ", @employeeunkid " & _
                          ", @incrementdate " & _
                          ", @currentscale " & _
                          ", @increment " & _
                          ", @newscale " & _
                          ", @gradegroupunkid " & _
                          ", @gradeunkid " & _
                          ", @gradelevelunkid " & _
                          ", @isgradechange " & _
                          ", @isfromemployee " & _
                          ", @audittype " & _
                          ", @audituserunkid " & _
                          ", @auditdatetime " & _
                          ", @ip " & _
                          ", @machine_name" & _
                          ", @reason_id " & _
                          ", @increment_mode " & _
                          ", @percentage " & _
                          ", @form_name " & _
                          ", @module_name1 " & _
                          ", @module_name2 " & _
                          ", @module_name3 " & _
                          ", @module_name4 " & _
                          ", @module_name5 " & _
                          ", @isweb " & _
                          ", @isapproved " & _
                          ", @approveruserunkid " & _
                          ", @psoft_syncdatetime " & _
                          ", @rehiretranunkid " & _
                          ", @changetypeid " & _
                          ", @promotion_date " & _
                          ", @actualdate " & _
                          ", @arrears_amount " & _
                          ", @arrears_countryid " & _
                          ", @noofinstallment " & _
                          ", @emi_amount " & _
                          ", @approval_statusunkid " & _
                          ", @finalapproverunkid " & _
                          ", @arrears_statusunkid " & _
                "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
            objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentscale.ToString)
            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString)
            objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewscale.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrReason_Id.ToString)
            objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_mode.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            'HYATT Enhancement - OUTBOUND file integration with Aruti.
            If mdtPSoft_SyncDateTime = Nothing Then
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
            End If
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
            objDataOperation.AddParameter("@changetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangetypeid)
            If mdtPromotion_date <> Nothing Then
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPromotion_date)
            Else
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            If mdtActualdate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualdate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@arrears_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecArrears_amount)
            objDataOperation.AddParameter("@arrears_countryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintArrears_countryid)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_amount)
            objDataOperation.AddParameter("@approval_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproval_statusunkid)
            objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalapproverunkid)
            objDataOperation.AddParameter("@arrears_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintArrears_statusunkid)
            'Sohail (21 Jan 2020) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForSalaryIncrement", mstrModuleName)
            Return False
        Finally
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            'objDataOperation = Nothing
            'S.SANDEEP [25 OCT 2016] -- END
        End Try
    End Function

    Public Function isUsedInAppraisal(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal intAppraisalMode As Integer, ByVal intSalaryIncrememntTranUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  finalemployeeunkid " & _
                    "FROM    hrapps_finalemployee " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND operationmodeid = @operationmodeid " & _
                            "AND employeeunkid = @employeeunkid " & _
                            "AND periodunkid = @periodunkid " & _
                            "AND salaryincrementtranunkid = @salaryincrementtranunkid "

            objDataOperation.AddParameter("@operationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAppraisalMode)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSalaryIncrememntTranUnkId) 'Sohail (29 Dec 2012)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsedInAppraisal; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isUsedInTrainingEnrollment(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal intTrainingAwardMode As Integer, ByVal intSalaryIncrememntTranUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  trainingenrolltranunkid " & _
                    "FROM    hrtraining_enrollment_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND operationmodeid = @operationmodeid " & _
                            "AND employeeunkid = @employeeunkid " & _
                            "AND periodunkid = @periodunkid " & _
                            "AND salaryincrementtranunkid = @salaryincrementtranunkid "

            objDataOperation.AddParameter("@operationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingAwardMode)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSalaryIncrememntTranUnkId) 'Sohail (29 Dec 2012)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsedInTrainingEnrollment; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetImportFileFormat(ByVal strTable As String) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT '' AS [employeecode], 0.00 as [increment amount/percentage/newscale], '' AS gradegroup, '' AS grade, '' AS gradelevel, '' AS promotion_date WHERE 1 = 2 "

            dsList = objDataOperation.ExecQuery(strQ, strTable)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetImportFileFormat; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Private Function UpdateED_Detail(ByVal strDatabaseName As String _
                                     , ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer _
                                     , ByVal xCompanyUnkid As Integer _
                                     , ByVal xPeriodStart As DateTime _
                                     , ByVal xPeriodEnd As DateTime _
                                     , ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean _
                                     , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                     , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                     , ByVal objDataOperation As clsDataOperation _
                                     , ByVal dtCurrentDateAndTime As Date _
                                     , ByVal intEmployeeUnkId As Integer _
                                     , ByVal intHeadUnkId As Integer _
                                     , ByVal intPeriodUnkId As Integer _
                                     , ByVal decNewScale As Decimal _
                                     , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                     , Optional ByVal strFilerString As String = "" _
                                     ) As Boolean
        'Sohail (14 Nov 2018) - [intEmployeeUnkId, intHeadUnkId, intPeriodUnkId, decNewScale]
        Dim ObjED As New clsEarningDeduction
        Try
            'Sohail (14 Nov 2018) -- Start
            'Zuri Issue - Salary change that was approved but it didnt reflect (for flat rate salary head and on approvng salaruy change) in payroll in 75.1.
            'If mintInfoSalHeadUnkid > 0 Then
            '    If ObjED.InsertAllByEmployeeList(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, _
            '                                     mintInfoSalHeadUnkid, _
            '                                     0, _
            '                                     0, _
            '                                     0, _
            '                                     mdecNewscale, _
            '                                     True, _
            '                                     False, _
            '                                     mintPeriodunkid, _
            '                                     "", _
            '                                     Nothing, _
            '                                     Nothing, _
            '                                     blnAllowToApproveEarningDeduction, _
            '                                     objDataOperation, dtCurrentDateAndTime, _
            '                                     True, _
            '                                     mblnIsCopyPrevoiusSLAB, _
            '                                     mblnIsOverwritePrevoiusSLAB, blnApplyUserAccessFilter, strFilerString) = False Then
            '        Return False
            '    End If
            'End If
            If intHeadUnkId > 0 Then
                'Gajanan (23 July 2020) -- blnSkipInsertIfExist: True
                If ObjED.InsertAllByEmployeeList(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, intEmployeeUnkId, _
                                                 intHeadUnkId, _
                                                 0, _
                                                 0, _
                                                 0, _
                                                 decNewScale, _
                                                 True, _
                                                 False, _
                                                 intPeriodUnkId, _
                                                 "", _
                                                 Nothing, _
                                                 Nothing, _
                                                 blnAllowToApproveEarningDeduction, _
                                                 objDataOperation, dtCurrentDateAndTime, _
                                                 True, _
                                                 mblnIsCopyPrevoiusSLAB, _
                                                 mblnIsOverwritePrevoiusSLAB, blnApplyUserAccessFilter, strFilerString, Nothing, , , , True) = False Then
                    'Gajanan (23 July 2020) -- Start
                    mstrMessage = ObjED._Message
                    'Gajanan (23 July 2020) -- End
                    Return False
                End If
            End If
            'Sohail (14 Nov 2018) -- End
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateED_Detail; Module Name: " & mstrModuleName)
        Finally
            ObjED = Nothing
        End Try
    End Function

    'Sohail (29 Sep 2021) -- Start
    'NMB Enhancement : OLD - 484 : Notification when salary change is edited/deleted.
    Public Function IsTableDataUpdate(ByVal intUnkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strFields As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strFields = " periodunkid,employeeunkid,incrementdate,currentscale,increment,newscale,gradegroupunkid,gradeunkid,gradelevelunkid,isgradechange,reason_id,increment_mode,percentage,isapproved,changetypeid,promotion_date,actualdate,approval_statusunkid "

            strQ = "WITH CTE AS ( " & _
                            "SELECT TOP 1 " & strFields & " " & _
                            "FROM atprsalaryincrement_tran " & _
                            "WHERE atprsalaryincrement_tran.salaryincrementtranunkid = @salaryincrementtranunkid " & _
                            "AND atprsalaryincrement_tran.audittype <> 3 " & _
                            "ORDER BY atprsalaryincrement_tran.atsalaryincrementtranunkid DESC " & _
                        ") " & _
                    " " & _
                    "SELECT " & strFields & " " & _
                    "FROM prsalaryincrement_tran " & _
                    "WHERE prsalaryincrement_tran.salaryincrementtranunkid = @salaryincrementtranunkid " & _
                    "EXCEPT " & _
                    "SELECT * FROM cte "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (29 Sep 2021) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry! Increment for the employee on the selected date is already done.")
            Language.setMessage(mstrModuleName, 2, "WEB")
			Language.setMessage(mstrModuleName, 3, "Sorry! Promotion for the employee on the selected date is already done.")
			Language.setMessage(mstrModuleName, 4, "Sorry! Increment date should be greater than last increment date.")
			Language.setMessage(mstrModuleName, 5, "Sorry! Increment for the employee on the selected actual date is already done.")
			Language.setMessage("clsMasterData", 717, "Simple Salary Change")
			Language.setMessage("clsMasterData", 718, "Salary Change With Promotion")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'*************************************** S.SANDEEP [09 MAY 2016] - TRANSACTION SCREEN FOR PROMOTION ************************************|
'
'
'
'
'
'''' <summary>
'''' Purpose: 
'''' Developer: Suhail
'''' </summary>
'Public Class clsSalaryIncrement

'#Region " Private variables "
'    Private Shared ReadOnly mstrModuleName As String = "clsSalaryIncrement"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'    Private mintSalaryincrementtranunkid As Integer
'    Private mintPeriodunkid As Integer
'    Private mintEmployeeunkid As Integer
'    Private mdtIncrementdate As Date
'    Private mdecCurrentscale As Decimal 'Sohail (11 May 2011)
'    Private mdecIncrement As Decimal 'Sohail (11 May 2011)
'    Private mdecNewscale As Decimal 'Sohail (11 May 2011)
'    Private mintGradegroupunkid As Integer
'    Private mintGradeunkid As Integer
'    Private mintGradelevelunkid As Integer
'    Private mblnIsgradechange As Boolean
'    'Sohail (09 Oct 2010) -- Start
'    'Private mblnIsautogenerated As Boolean
'    Private mblnIsfromemployee As Boolean
'    'Sohail (09 Oct 2010) -- End
'    Private mintUserunkid As Integer
'    Private mblnIsvoid As Boolean
'    Private mintVoiduserunkid As Integer
'    Private mdtVoiddatetime As Date
'    Private mstrVoidreason As String = String.Empty
'    'Sohail (12 Oct 2011) -- Start
'    'Sohail (08 Nov 2011) -- Start
'    'Private mstrRemark As String = ""
'    Private mstrReason_Id As Integer
'    'Sohail (08 Nov 2011) -- End
'    'Sohail (31 Mar 2012) -- Start
'    'TRA - ENHANCEMENT
'    Private mintIncrement_mode As Integer
'    Private mdblPercentage As Double
'    'Sohail (31 Mar 2012) -- End
'    Private mdtTran As DataTable
'    'Sohail (12 Oct 2011) -- End    

'    'S.SANDEEP [ 19 JULY 2012 ] -- START
'    'Enhancement : TRA Changes
'    Private mstrWebFormName As String = String.Empty
'    'S.SANDEEP [ 19 JULY 2012 ] -- END
'    'Sohail (24 Dec 2013) -- Start
'    'Enhancement - Send link in salary change notification
'    Private mstrWebIP As String = ""
'    Private mstrWebhostName As String = ""
'    'Sohail (24 Dec 2013) -- End

'    'S.SANDEEP [ 13 AUG 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mintLogEmployeeUnkid As Integer = -1
'    'S.SANDEEP [ 13 AUG 2012 ] -- END
'    'Sohail (24 Sep 2012) -- Start
'    'TRA - ENHANCEMENT
'    Private mblnIsapproved As Boolean
'    Private mintApproveruserunkid As Integer = -1
'    'Sohail (24 Sep 2012) -- End

'    'S.SANDEEP [ 04 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'    Private mblnIsCopyPrevoiusSLAB As Boolean = False
'    Private mblnIsOverwritePrevoiusSLAB As Boolean = False
'    Private mintInfoSalHeadUnkid As Integer = -1
'    'S.SANDEEP [ 04 SEP 2012 ] -- END

'    'S.SANDEEP [ 18 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mintSuccessCnt As Integer = 0
'    'S.SANDEEP [ 18 SEP 2012 ] -- END

'    'Sohail (27 Nov 2014) -- Start
'    'HYATT Enhancement - OUTBOUND file integration with Aruti.
'    Private mdtPSoft_SyncDateTime As Date
'    'Sohail (27 Nov 2014) -- End

'    'S.SANDEEP [14 APR 2015] -- START
'    Private mintRehiretranunkid As Integer = 0
'    Private xDataOpr As clsDataOperation
'    'S.SANDEEP [14 APR 2015] -- END

'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set salaryincrementtranunkid
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Salaryincrementtranunkid() As Integer
'        Get
'            Return mintSalaryincrementtranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintSalaryincrementtranunkid = value
'            Call GetData()

'        End Set
'    End Property


'    ''' <summary>
'    ''' Purpose: Get or Set periodunkid
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Periodunkid() As Integer
'        Get
'            Return mintPeriodunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintPeriodunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set employeeunkid
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Employeeunkid() As Integer
'        Get
'            Return mintEmployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set incrementdate
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Incrementdate() As Date
'        Get
'            Return mdtIncrementdate
'        End Get
'        Set(ByVal value As Date)
'            mdtIncrementdate = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set currentscale
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Currentscale() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecCurrentscale
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecCurrentscale = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set increment
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Increment() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecIncrement
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecIncrement = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set newscale
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Newscale() As Decimal 'Sohail (11 May 2011)
'        Get
'            Return mdecNewscale
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
'            mdecNewscale = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set gradegroupunkid
'    ''' Modify By: Sohail
'    ''' </summary>
'    Public Property _Gradegroupunkid() As Integer
'        Get
'            Return mintGradegroupunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintGradegroupunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set gradeunkid
'    ''' Modify By: Sohail
'    ''' </summary>
'    Public Property _Gradeunkid() As Integer
'        Get
'            Return mintGradeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintGradeunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set gradelevelunkid
'    ''' Modify By: Sohail
'    ''' </summary>
'    Public Property _Gradelevelunkid() As Integer
'        Get
'            Return mintGradelevelunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintGradelevelunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isgradechange
'    ''' Modify By: Sohail
'    ''' </summary>
'    Public Property _Isgradechange() As Boolean
'        Get
'            Return mblnIsgradechange
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsgradechange = value
'        End Set
'    End Property

'    'Sohail (09 Oct 2010) -- Start
'    '''' <summary>
'    '''' Purpose: Get or Set isautogenerated
'    '''' Modify By: Krishna
'    '''' </summary>
'    'Public Property _Isautogenerated() As Boolean
'    '    Get
'    '        Return mblnIsautogenerated
'    '    End Get
'    '    Set(ByVal value As Boolean)
'    '        mblnIsautogenerated = Value
'    '    End Set
'    'End Property

'    ''' <summary>
'    ''' Purpose Get or Set isfromemployee
'    ''' Modify By Sohail
'    ''' </summary>
'    Public Property _Isfromemployee() As Boolean
'        Get
'            Return mblnIsfromemployee
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsfromemployee = value
'        End Set
'    End Property
'    'Sohail (09 Oct 2010) -- End

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Suhail
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = value
'        End Set
'    End Property

'    'Sohail (12 Oct 2011) -- Start
'    'Sohail (08 Nov 2011) -- Start
'    Public Property _Reason_Id() As Integer
'        Get
'            Return mstrReason_Id
'        End Get
'        Set(ByVal value As Integer)
'            mstrReason_Id = value
'        End Set
'    End Property
'    'Sohail (08 Nov 2011) -- End

'    'Sohail (31 Mar 2012) -- Start
'    'TRA - ENHANCEMENT
'    Public Property _Increment_Mode() As Integer
'        Get
'            Return mintIncrement_mode
'        End Get
'        Set(ByVal value As Integer)
'            mintIncrement_mode = value
'        End Set
'    End Property

'    Public Property _Percentage() As Double
'        Get
'            Return mdblPercentage
'        End Get
'        Set(ByVal value As Double)
'            mdblPercentage = value
'        End Set
'    End Property
'    'Sohail (31 Mar 2012) -- End

'    Public Property _DataTable() As DataTable
'        Get
'            Return mdtTran
'        End Get
'        Set(ByVal value As DataTable)
'            mdtTran = value
'        End Set
'    End Property
'    'Sohail (12 Oct 2011) -- End

'    'S.SANDEEP [ 19 JULY 2012 ] -- START
'    'Enhancement : TRA Changes

'    Public Property _WebFormName() As String
'        Get
'            Return mstrWebFormName
'        End Get
'        Set(ByVal value As String)
'            mstrWebFormName = value
'        End Set
'    End Property

'    'S.SANDEEP [ 19 JULY 2012 ] -- END

'    'Sohail (24 Dec 2013) -- Start
'    'Enhancement - Send link in salary change notification
'    Public WriteOnly Property _WebIP() As String
'        Set(ByVal value As String)
'            mstrWebIP = value
'        End Set
'    End Property

'    Public WriteOnly Property _WebHostName() As String
'        Set(ByVal value As String)
'            mstrWebhostName = value
'        End Set
'    End Property
'    'Sohail (24 Dec 2013) -- End

'    'S.SANDEEP [ 13 AUG 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
'        Set(ByVal value As Integer)
'            mintLogEmployeeUnkid = value
'        End Set
'    End Property
'    'S.SANDEEP [ 13 AUG 2012 ] -- END

'    'Sohail (24 Sep 2012) -- Start
'    'TRA - ENHANCEMENT
'    ''' <summary>
'    ''' Purpose: Get or Set isapproved
'    ''' Modify By: Sohail
'    ''' </summary>
'    Public Property _Isapproved() As Boolean
'        Get
'            Return mblnIsapproved
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsapproved = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set approveruserunkid
'    ''' Modify By: Sohail
'    ''' </summary>
'    Public Property _Approveruserunkid() As Integer
'        Get
'            Return mintApproveruserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintApproveruserunkid = value
'        End Set
'    End Property
'    'Sohail (24 Sep 2012) -- End

'    'S.SANDEEP [ 04 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'    Public WriteOnly Property _IsCopyPrevoiusSLAB() As Boolean
'        Set(ByVal value As Boolean)
'            mblnIsCopyPrevoiusSLAB = value
'        End Set
'    End Property

'    Public WriteOnly Property _IsOverwritePrevoiusSLAB() As Boolean
'        Set(ByVal value As Boolean)
'            mblnIsOverwritePrevoiusSLAB = value
'        End Set
'    End Property

'    Public WriteOnly Property _InfoSalHeadUnkid() As Integer
'        Set(ByVal value As Integer)
'            mintInfoSalHeadUnkid = value
'        End Set
'    End Property
'    'S.SANDEEP [ 04 SEP 2012 ] -- END

'    'S.SANDEEP [ 18 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public ReadOnly Property _SuccessCnt() As Integer
'        Get
'            Return mintSuccessCnt
'        End Get
'    End Property
'    'S.SANDEEP [ 18 SEP 2012 ] -- END

'    'Sohail (27 Nov 2014) -- Start
'    'HYATT Enhancement - OUTBOUND file integration with Aruti.
'    Public Property _PSoft_SyncDateTime() As Date
'        Get
'            Return mdtPSoft_SyncDateTime
'        End Get
'        Set(ByVal value As Date)
'            mdtPSoft_SyncDateTime = value
'        End Set
'    End Property
'    'Sohail (27 Nov 2014) -- End

'    'S.SANDEEP [14 APR 2015] -- START
'    Public Property _Rehiretranunkid() As Integer
'        Get
'            Return mintRehiretranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintRehiretranunkid = value
'        End Set
'    End Property

'    Public WriteOnly Property _DataOperation() As clsDataOperation
'        Set(ByVal value As clsDataOperation)
'            xDataOpr = value
'        End Set
'    End Property
'    'S.SANDEEP [14 APR 2015] -- END

'#End Region

'    'Sohail (12 Oct 2011) -- Start
'#Region " Constructor "
'    Public Sub New()

'    End Sub

'    Public Sub New(ByVal blnGlobalIncrement As Boolean)
'        mdtTran = New DataTable("Increment")
'        Dim dCol As DataColumn
'        Try

'            dCol = New DataColumn("employeeunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("currentscale")
'            dCol.DataType = System.Type.GetType("System.Decimal")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("increment")
'            dCol.DataType = System.Type.GetType("System.Decimal")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("newscale")
'            dCol.DataType = System.Type.GetType("System.Decimal")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("gradegroupunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("gradeunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("gradelevelunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            mdtTran.Columns.Add("increment_mode", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("percentage", System.Type.GetType("System.Double")).DefaultValue = 0
'            'Sohail (31 Mar 2012) -- End

'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            dCol = New DataColumn("isapproved")
'            dCol.DataType = System.Type.GetType("System.Boolean")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("approveruserunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)
'            'Sohail (24 Sep 2012) -- End

'            mdtTran.Columns.Add("isgradechange", System.Type.GetType("System.Boolean")).DefaultValue = False 'Sohail (10 Nov 2014)

'            'S.SANDEEP [14 APR 2015] -- START
'            mdtTran.Columns.Add("rehiretranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            'S.SANDEEP [14 APR 2015] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "New(GlobalIncrement)", mstrModuleName)
'        End Try
'    End Sub
'#End Region
'    'Sohail (12 Oct 2011) -- End

'    ''' <summary>
'    ''' Modify By: Suhail
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            'Sohail (09 Oct 2010) -- Start
'            'Changes : isautogenerated is renamed with isfromemployee
'            'strQ = "SELECT " & _
'            '  "  salaryincrementtranunkid " & _
'            '  ", periodunkid " & _
'            '  ", employeeunkid " & _
'            '  ", incrementdate " & _
'            '  ", currentscale " & _
'            '  ", increment " & _
'            '  ", newscale " & _
'            '  ", gradegroupunkid " & _
'            '  ", gradeunkid " & _
'            '  ", gradelevelunkid " & _
'            '  ", isgradechange " & _
'            '  ", isautogenerated " & _
'            '  ", userunkid " & _
'            '  ", isvoid " & _
'            '  ", voiduserunkid " & _
'            '  ", voiddatetime " & _
'            '  ", voidreason " & _
'            ' "FROM prsalaryincrement_tran " & _
'            ' "WHERE salaryincrementtranunkid = @salaryincrementtranunkid " & _
'            ' "AND isvoid = 0 "
'            strQ = "SELECT " & _
'              "  salaryincrementtranunkid " & _
'              ", periodunkid " & _
'              ", employeeunkid " & _
'              ", incrementdate " & _
'              ", currentscale " & _
'              ", increment " & _
'              ", newscale " & _
'              ", gradegroupunkid " & _
'              ", gradeunkid " & _
'              ", gradelevelunkid " & _
'              ", isgradechange " & _
'                         ", isfromemployee " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'              ", reason_id " & _
'              ", increment_mode " & _
'              ", percentage " & _
'              ", ISNULL(isapproved, 0) AS isapproved " & _
'              ", ISNULL(approveruserunkid, 0) AS approveruserunkid " & _
'              ", prsalaryincrement_tran.psoft_syncdatetime " & _
'              ", ISNULL(rehiretranunkid,0) AS rehiretranunkid " & _
'             "FROM prsalaryincrement_tran " & _
'             "WHERE salaryincrementtranunkid = @salaryincrementtranunkid " 'Sohail (16 Oct 2010), 'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]
'            'Sohail (27 Nov 2014) - [psoft_syncdatetime]
'            'Sohail (24 Sep 2012) - [isapproved, approveruserunkid]
'            'Sohail (09 Oct 2010) -- End
'            'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END
'            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintSalaryincrementtranunkid = CInt(dtRow.Item("salaryincrementtranunkid"))
'                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
'                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
'                mdtIncrementdate = dtRow.Item("incrementdate")
'                mdecCurrentscale = CDec(dtRow.Item("currentscale")) 'Sohail (11 May 2011)
'                mdecIncrement = CDec(dtRow.Item("increment")) 'Sohail (11 May 2011)
'                mdecNewscale = CDec(dtRow.Item("newscale")) 'Sohail (11 May 2011)
'                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
'                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
'                mintGradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
'                mblnIsgradechange = CBool(dtRow.Item("isgradechange"))
'                'Sohail (09 Oct 2010) -- Start
'                'mblnIsautogenerated = CBool(dtRow.Item("isautogenerated"))
'                mblnIsfromemployee = CBool(dtRow.Item("isfromemployee"))
'                'Sohail (09 Oct 2010) -- End
'                mintUserunkid = CInt(dtRow.Item("userunkid"))
'                mblnIsvoid = CBool(dtRow.Item("isvoid"))
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                If dtRow.Item("voiddatetime").ToString = Nothing Then
'                    mdtVoiddatetime = Nothing
'                Else
'                    mdtVoiddatetime = dtRow.Item("voiddatetime")
'                End If
'                mstrVoidreason = dtRow.Item("voidreason").ToString
'                'Sohail (08 Nov 2011) -- Start
'                'mstrRemark = dtRow.Item("remark").ToString 'Sohail (12 Oct 2011)
'                mstrReason_Id = CInt(dtRow.Item("reason_id"))
'                'Sohail (08 Nov 2011) -- End
'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                mintIncrement_mode = CInt(dtRow.Item("increment_mode"))
'                mdblPercentage = CDbl(dtRow.Item("percentage"))
'                'Sohail (31 Mar 2012) -- End

'                'Sohail (24 Sep 2012) -- Start
'                'TRA - ENHANCEMENT
'                mblnIsapproved = CBool(dtRow.Item("isapproved"))
'                mintApproveruserunkid = CInt(dtRow.Item("approveruserunkid"))
'                'Sohail (24 Sep 2012) -- End

'                'Sohail (27 Nov 2014) -- Start
'                'HYATT Enhancement - OUTBOUND file integration with Aruti.
'                If dtRow.Item("psoft_syncdatetime").ToString = Nothing Then
'                    mdtPSoft_SyncDateTime = Nothing
'                Else
'                    mdtPSoft_SyncDateTime = dtRow.Item("psoft_syncdatetime")
'                End If
'                'Sohail (27 Nov 2014) -- End

'                'S.SANDEEP [14 APR 2015] -- START
'                mintRehiretranunkid = CInt(dtRow.Item("rehiretranunkid"))
'                'S.SANDEEP [14 APR 2015] -- END

'                Exit For
'            Next

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Suhail
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal xDatabaseName As String _
'                            , ByVal xUserUnkid As Integer _
'                            , ByVal xYearUnkID As Integer _
'                            , ByVal xCompanyUnkid As Integer _
'                            , ByVal xPeriodStart As DateTime _
'                            , ByVal xPeriodEnd As DateTime _
'                            , ByVal xUserModeSetting As String _
'                            , ByVal xOnlyApproved As Boolean _
'                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
'                            , ByVal blnApplyUserAccessFilter As Boolean _
'                            , ByVal strTableName As String _
'                            , ByVal objDataOp As clsDataOperation _
'                            , Optional ByVal strEmpUnkIDs As String = "" _
'                            , Optional ByVal intPeriodUnkID As Integer = 0 _
'                            , Optional ByVal intYearUnkID As Integer = 0 _
'                           , Optional ByVal strOrderBy As String = "" _
'                            , Optional ByVal mstrAdvanceFilter As String = "" _
'                            , Optional ByVal intSalaryChangeApprovalStatus As Integer = enSalaryChangeApprovalStatus.All) As DataSet
'        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, objDataOp]
'        '                   'Sohail (30 Mar 2013) - [(Optional ByVal intEmployeeUnkID As Integer = 0) is replaced with (Optional ByVal strEmpUnkIDs As String = "")]
'        '                   'Sohail (24 Sep 2012) - [intSalaryChangeApprovalStatus]

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'Sohail (21 Aug 2015) -- Start
'        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
'        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
'        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
'        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkID, xUserModeSetting)
'        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
'        'Sohail (21 Aug 2015) -- End

'        'Sohail (23 Apr 2012) -- Start
'        'TRA - ENHANCEMENT
'        'objDataOperation = New clsDataOperation

'        'Sohail (21 Aug 2015) -- Start
'        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'        'If objDataOp Is Nothing Then
'        '    objDataOperation = New clsDataOperation
'        'End If
'        'objDataOperation.ClearParameters()
'        objDataOperation = New clsDataOperation
'        If objDataOp IsNot Nothing Then
'            objDataOperation = objDataOp
'            objDataOperation.ClearParameters()
'        Else
'            'objDataOperation.BindTransaction()
'        End If
'        'Sohail (21 Aug 2015) -- End
'        'Sohail (23 Apr 2012) -- End

'        Try
'            'Sohail (09 Oct 2010) -- Start
'            'Changes : isautogenerated is renamed with isfromemployee
'            'strQ = "SELECT " & _
'            '  "  prsalaryincrement_tran.salaryincrementtranunkid " & _
'            '  ", prsalaryincrement_tran.periodunkid " & _
'            '  ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
'            '  ", cfcommon_period_tran.period_name " & _
'            '  ", prsalaryincrement_tran.employeeunkid " & _
'            '  ", CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) AS incrementdate " & _
'            '  ", prsalaryincrement_tran.currentscale " & _
'            '  ", prsalaryincrement_tran.increment " & _
'            '  ", prsalaryincrement_tran.newscale " & _
'            '  ", prsalaryincrement_tran.gradegroupunkid " & _
'            '  ", prsalaryincrement_tran.gradeunkid " & _
'            '  ", prsalaryincrement_tran.gradelevelunkid " & _
'            '  ", prsalaryincrement_tran.isgradechange " & _
'            '  ", prsalaryincrement_tran.isautogenerated " & _
'            '  ", prsalaryincrement_tran.userunkid " & _
'            '  ", prsalaryincrement_tran.isvoid " & _
'            '  ", prsalaryincrement_tran.voiduserunkid " & _
'            '  ", prsalaryincrement_tran.voiddatetime " & _
'            '  ", prsalaryincrement_tran.voidreason " & _
'            ' "FROM prsalaryincrement_tran " & _
'            ' "LEFT JOIN cfcommon_period_tran ON prsalaryincrement_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
'            ' "LEFT JOIN hremployee_master ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            ' "WHERE prsalaryincrement_tran.isvoid = 0 "
'            strQ = "SELECT " & _
'              "  prsalaryincrement_tran.salaryincrementtranunkid " & _
'              ", prsalaryincrement_tran.periodunkid " & _
'              ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
'              ", cfcommon_period_tran.period_name " & _
'              ", prsalaryincrement_tran.employeeunkid " & _
'              ", CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) AS incrementdate " & _
'              ", prsalaryincrement_tran.currentscale " & _
'              ", prsalaryincrement_tran.increment " & _
'              ", prsalaryincrement_tran.newscale " & _
'              ", prsalaryincrement_tran.gradegroupunkid " & _
'              ", ISNULL(hrgradegroup_master.name, '') AS GradeGroup  " & _
'              ", prsalaryincrement_tran.gradeunkid " & _
'              ", ISNULL(hrgrade_master.name, '') AS Grade " & _
'              ", prsalaryincrement_tran.gradelevelunkid " & _
'              ", ISNULL(hrgradelevel_master.name, '') AS GradeLevel " & _
'              ", prsalaryincrement_tran.isgradechange " & _
'                          ", prsalaryincrement_tran.isfromemployee " & _
'              ", prsalaryincrement_tran.userunkid " & _
'              ", prsalaryincrement_tran.isvoid " & _
'              ", prsalaryincrement_tran.voiduserunkid " & _
'              ", prsalaryincrement_tran.voiddatetime " & _
'              ", prsalaryincrement_tran.voidreason " & _
'              ", prsalaryincrement_tran.reason_id " & _
'              ", ISNULL(prsalaryincrement_tran.increment_mode, 0) AS increment_mode " & _
'              ", ISNULL(prsalaryincrement_tran.percentage, 0) AS percentage " & _
'              ", ISNULL(prsalaryincrement_tran.isapproved, 0) AS isapproved " & _
'              ", ISNULL(prsalaryincrement_tran.approveruserunkid, 0) AS approveruserunkid "
'            ' 'Sohail (24 Sep 2012) - [isapproved, approveruserunkid]

'            'Anjan (21 Nov 2011)-Start
'            'ENHANCEMENT : TRA COMMENTS
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'strQ &= ",ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
'            '         ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
'            '         ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
'            '         ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
'            '         ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
'            '         ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
'            '         ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
'            '         ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
'            '         ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
'            '         ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
'            '         ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
'            '         ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
'            '         ", prsalaryincrement_tran.psoft_syncdatetime "
'            strQ &= ", ISNULL(ETRF.sectiongroupunkid,0) AS sectiongroupunkid " & _
'                    ", ISNULL(ETRF.unitgroupunkid,0) AS unitgroupunkid " & _
'                    ", ISNULL(ETRF.teamunkid,0) AS teamunkid " & _
'                    ", ISNULL(ETRF.stationunkid,0) AS stationunkid " & _
'                    ", ISNULL(ETRF.deptgroupunkid,0) AS deptgroupunkid " & _
'                    ", ISNULL(ETRF.departmentunkid,0) AS departmentunkid " & _
'                    ", ISNULL(ETRF.sectionunkid,0) AS sectionunkid " & _
'                    ", ISNULL(ETRF.unitunkid,0) AS unitunkid " & _
'                    ", ISNULL(ERECAT.jobunkid,0) AS jobunkid " & _
'                    ", ISNULL(ETRF.classgroupunkid,0) AS classgroupunkid " & _
'                    ", ISNULL(ETRF.classunkid,0) AS classunkid " & _
'                    ", ISNULL(ERECAT.jobgroupunkid,0) AS jobgroupunkid " & _
'                     ", prsalaryincrement_tran.psoft_syncdatetime "
'            'Sohail (21 Aug 2015) -- End
'            '        'Sohail (27 Nov 2014) - [psoft_syncdatetime]
'            'Anjan (21 Nov 2011)-End 

'            'S.SANDEEP [14 APR 2015] -- START
'            strQ &= ", ISNULL(rehiretranunkid,0) AS rehiretranunkid "
'            'S.SANDEEP [14 APR 2015] -- END

'            strQ &= "FROM prsalaryincrement_tran " & _
'             "LEFT JOIN cfcommon_period_tran ON prsalaryincrement_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
'             "LEFT JOIN hremployee_master ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " & _
'             "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = prsalaryincrement_tran.gradegroupunkid " & _
'             "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prsalaryincrement_tran.gradeunkid " & _
'             "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = prsalaryincrement_tran.gradelevelunkid " & _
'             "LEFT JOIN " & _
'                    "( " & _
'                    "    SELECT " & _
'                    "         Trf.TrfEmpId " & _
'                    "        ,ISNULL(Trf.stationunkid,0) AS stationunkid " & _
'                    "        ,ISNULL(Trf.deptgroupunkid,0) AS deptgroupunkid " & _
'                    "        ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
'                    "        ,ISNULL(Trf.sectiongroupunkid,0) AS sectiongroupunkid " & _
'                    "        ,ISNULL(Trf.sectionunkid,0) AS sectionunkid " & _
'                    "        ,ISNULL(Trf.unitgroupunkid,0) AS unitgroupunkid " & _
'                    "        ,ISNULL(Trf.unitunkid,0) AS unitunkid " & _
'                    "        ,ISNULL(Trf.teamunkid,0) AS teamunkid " & _
'                    "        ,ISNULL(Trf.classgroupunkid,0) AS classgroupunkid " & _
'                    "        ,ISNULL(Trf.classunkid,0) AS classunkid " & _
'                    "        ,ISNULL(Trf.EfDt,'') AS EfDt " & _
'                    "        ,Trf.ETT_REASON " & _
'                    "    FROM " & _
'                    "   ( " & _
'                    "        SELECT " & _
'                    "             ETT.employeeunkid AS TrfEmpId " & _
'                    "            ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
'                    "            ,ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
'                    "            ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
'                    "            ,ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
'                    "            ,ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
'                    "            ,ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
'                    "            ,ISNULL(ETT.unitunkid,0) AS unitunkid " & _
'                    "            ,ISNULL(ETT.teamunkid,0) AS teamunkid " & _
'                    "            ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
'                    "            ,ISNULL(ETT.classunkid,0) AS classunkid " & _
'                    "            ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
'                    "            ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
'                    "            ,CASE WHEN ETT.rehiretranunkid > 0 THEN RTT.name ELSE TTC.name END AS ETT_REASON " & _
'                    "        FROM hremployee_transfer_tran AS ETT " & _
'                    "           LEFT JOIN cfcommon_master AS RTT ON RTT.masterunkid = ETT.changereasonunkid AND RTT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
'                    "           LEFT JOIN cfcommon_master AS TTC ON TTC.masterunkid = ETT.changereasonunkid AND TTC.mastertype = '" & clsCommon_Master.enCommonMaster.TRANSFERS & "' " & _
'                    "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
'                    "    ) AS Trf WHERE Trf.Rno = 1 " & _
'                    ") AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.EfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "    SELECT " & _
'                    "         Cat.CatEmpId " & _
'                    "        ,Cat.jobgroupunkid " & _
'                    "        ,Cat.jobunkid " & _
'                    "        ,Cat.CEfDt " & _
'                    "        ,Cat.RECAT_REASON " & _
'                    "    FROM " & _
'                    "    ( " & _
'                    "        SELECT " & _
'                    "             ECT.employeeunkid AS CatEmpId " & _
'                    "            ,ECT.jobgroupunkid " & _
'                    "            ,ECT.jobunkid " & _
'                    "            ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
'                    "            ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
'                    "            ,CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
'                    "        FROM hremployee_categorization_tran AS ECT " & _
'                    "        	 LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
'                    "        	 LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
'                    "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
'                    "    ) AS Cat WHERE Cat.Rno = 1 " & _
'                    ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) "

'            If xDateJoinQry.Trim.Length > 0 Then
'                strQ &= xDateJoinQry
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                strQ &= xUACQry
'            End If

'            If xAdvanceJoinQry.Trim.Length > 0 Then
'                strQ &= xAdvanceJoinQry
'            End If

'            strQ &= "WHERE ISNULL(prsalaryincrement_tran.isvoid,0) = 0 "
'            'Sohail (10 Nov 2014) - [LEFT JOIN hrgradegroup_master, hrgrade_master, hrgradelevel_master]
'            'Sohail (16 Oct 2010), 'Sohail (08 Nov 2011) - [reason_id]
'            'Sohail (09 Oct 2010) -- End

'            'Anjan (09 Aug 2011)-Start
'            'Issue : For including setting of acitve and inactive employee.
'            'Sohail (23 Apr 2012) -- Start
'            'TRA - ENHANCEMENT
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'If strIncludeInactiveEmployee = "" Then strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
'            'If CBool(strIncludeInactiveEmployee) = False Then
'            '    'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'            '    'Sohail (23 Apr 2012) -- End
'            '    '    strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
'            '    'Sohail (06 Jan 2012) -- Start
'            '    'TRA - ENHANCEMENT
'            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

'            '    'Sohail (23 Apr 2012) -- Start
'            '    'TRA - ENHANCEMENT
'            '    'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            '    'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
'            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
'            '    'Sohail (23 Apr 2012) -- End
'            '    'Sohail (06 Jan 2012) -- End
'            'End If
'            'Sohail (21 Aug 2015) -- End
'            'Anjan (09 Aug 2011)-End 


'            'Sohail (30 Mar 2013) -- Start
'            'TRA - ENHANCEMENT
'            'If intEmployeeUnkID > 0 Then
'            '    strQ &= "AND prsalaryincrement_tran.employeeunkid = @employeeunkid "
'            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID.ToString)
'            'End If
'            If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
'                strQ &= "AND prsalaryincrement_tran.employeeunkid IN (" & strEmpUnkIDs & ") "
'            End If
'            'Sohail (30 Mar 2013) -- End

'            'Sohail (24 Jun 2011) -- Start
'            'Issue : According to privilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If
'            'Sohail (23 Apr 2012) -- Start
'            'TRA - ENHANCEMENT
'            'Select Case ConfigParameter._Object._UserAccessModeSetting
'            '    Case enAllocation.BRANCH
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.TEAM
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOB_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOBS
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            'End Select
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'If strUserAccessLevelFilterString = "" Then
'            '    strQ &= UserAccessLevel._AccessLevelFilterString
'            'Else
'            '    strQ &= strUserAccessLevelFilterString
'            'End If
'            'Sohail (21 Aug 2015) -- End
'            'Sohail (23 Apr 2012) -- End
'            'S.SANDEEP [ 04 FEB 2012 ] -- END

'            'Sohail (24 Jun 2011) -- End

'            'Sohail (08 Mar 2016) -- Start
'            'Issue - User access on On Salary change, All approvers are receiving Notifications even if employee with salary change not in his/her user access. #51 (KBC & Kenya Project Comments List.xls)
'            If blnApplyUserAccessFilter = True Then
'                If xUACFiltrQry.Trim.Length > 0 Then
'                    strQ &= " AND " & xUACFiltrQry
'                End If
'            End If

'            If xIncludeIn_ActiveEmployee = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    strQ &= xDateFilterQry
'                End If
'            End If

'            'Sohail (08 Mar 2016) -- End

'            If intYearUnkID > 0 Then
'                strQ &= "AND cfcommon_period_tran.yearunkid = @yearunkid "
'                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkID.ToString)
'            End If

'            If intPeriodUnkID > 0 Then
'                strQ &= "AND prsalaryincrement_tran.periodunkid = @periodunkid "
'                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID.ToString)
'            End If

'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            If intSalaryChangeApprovalStatus = enSalaryChangeApprovalStatus.Approved Then
'                strQ &= " AND prsalaryincrement_tran.isapproved = @isapproved "
'                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, intSalaryChangeApprovalStatus)
'            ElseIf intSalaryChangeApprovalStatus = enSalaryChangeApprovalStatus.Pending Then
'                strQ &= " AND prsalaryincrement_tran.isapproved = @isapproved "
'                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
'            End If
'            'Sohail (24 Sep 2012) -- End

'            'Sohail (30 Dec 2013) -- Start
'            'Enhancement - Send link in salary change notification
'            If mstrAdvanceFilter.Trim <> "" Then
'                strQ &= " AND " & mstrAdvanceFilter
'            End If
'            'Sohail (30 Dec 2013) -- End

'            If strOrderBy.Trim.Length > 0 Then
'                strQ &= " ORDER BY " & strOrderBy
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'If objDataOp Is Nothing Then
'            '    objDataOperation.ReleaseTransaction(True)
'            'End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Suhail
'    ''' </summary>
'    ''' <param name="blnUpdateEmployeeScale">For Any Salary Change Please pass TRUE</param>
'    ''' <param name="blnCalculateIncrementAmt">To update Current scale and Increment from the difference of current transaction NewScale and previous transaction NewScale </param>
'    ''' <param name="blnUpdateLatestGrade">To update Grade from Previous transaction</param>
'    ''' <returns>Boolean</returns>
'    ''' <remarks></remarks>
'    Public Function Insert(ByVal strDatabaseName As String _
'                           , ByVal xUserUnkid As Integer _
'                           , ByVal xYearUnkid As Integer _
'                           , ByVal xCompanyUnkid As Integer _
'                           , ByVal xPeriodStart As DateTime _
'                           , ByVal xPeriodEnd As DateTime _
'                           , ByVal xUserModeSetting As String _
'                           , ByVal xOnlyApproved As Boolean _
'                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
'                           , ByVal blnAllowToApproveEarningDeduction As Boolean _
'                           , ByVal dtCurrentDateAndTime As Date _
'                           , Optional ByVal blnUpdateEmployeeScale As Boolean = True _
'                           , Optional ByVal blnCalculateIncrementAmt As Boolean = False _
'                           , Optional ByVal blnUpdateLatestGrade As Boolean = False _
'                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
'                           , Optional ByVal strFilerString As String = "" _
'                           ) As Boolean
'        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]
'        'Sohail (27 Nov 2014) - [blnCalculateIncrementAmt, blnUpdateLatestGrade]
'        'Sohail (29 Dec 2012) -- Start
'        'TRA - ENHANCEMENT - Code Commented to allow to give increment more than one time in one period (Issue# 131)
'        'If isExist(mintPeriodunkid, mintEmployeeunkid) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Increment for the employee in this period is already done.")
'        '    Return False
'        'End If
'        If isExistOnSameDate(mdtIncrementdate, mintEmployeeunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry! Increment for the employee on the selected date is already done.")
'            Return False
'        End If
'        'Sohail (29 Dec 2012) -- End

'        Dim objDataOperation As clsDataOperation

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objEmployee As New clsEmployee_Master
'        Dim objMaster As New clsMasterData
'        Dim objWagesMaster As New clsWages
'        Dim objWages As New clsWagesTran

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()

'        Try

'            'Sohail (27 Nov 2014) -- Start
'            'HYATT Enhancement - OUTBOUND file integration with Aruti.
'            If blnCalculateIncrementAmt = True Then
'                Dim ds As DataSet = objMaster.Get_Current_Scale("Scale", mintEmployeeunkid, mdtIncrementdate.Date)
'                If ds.Tables("Scale").Rows.Count > 0 Then
'                    mdecCurrentscale = Format(ds.Tables("Scale").Rows(0).Item("newscale"), "#0.00")
'                    mdecIncrement = mdecNewscale - mdecCurrentscale
'                    If mblnIsgradechange = False AndAlso blnUpdateLatestGrade = True Then
'                        mintGradegroupunkid = CInt(ds.Tables("Scale").Rows(0).Item("gradegroupunkid").ToString)
'                        mintGradeunkid = CInt(ds.Tables("Scale").Rows(0).Item("gradeunkid").ToString)
'                        mintGradelevelunkid = CInt(ds.Tables("Scale").Rows(0).Item("gradelevelunkid").ToString)
'                    End If
'                End If
'            End If
'            'Sohail (27 Nov 2014) -- End

'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
'            objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentscale.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewscale.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
'            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
'            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
'            objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
'            'Sohail (09 Oct 2010) -- Start
'            'objDataOperation.AddParameter("@isautogenerated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsautogenerated.ToString)
'            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
'            'Sohail (09 Oct 2010) -- End
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
'            'Sohail (08 Nov 2011) -- Start
'            'objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString) 'Sohail (12 Oct 2011)
'            objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrReason_Id.ToString)
'            'Sohail (08 Nov 2011) -- End
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_mode.ToString)
'            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
'            'Sohail (31 Mar 2012) -- End
'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
'            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
'            'Sohail (24 Sep 2012) -- End
'            'Sohail (27 Nov 2014) -- Start
'            'HYATT Enhancement - OUTBOUND file integration with Aruti.
'            If mdtPSoft_SyncDateTime = Nothing Then
'                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
'            End If
'            'Sohail (27 Nov 2014) -- End

'            'S.SANDEEP [14 APR 2015] -- START
'            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
'            'S.SANDEEP [14 APR 2015] -- END

'            'Sohail (09 Oct 2010) -- Start
'            'Changes : isautogenerated is renamed with isfromemployee
'            'strQ = "INSERT INTO prsalaryincrement_tran ( " & _
'            '  "  periodunkid " & _
'            '  ", employeeunkid " & _
'            '  ", incrementdate " & _
'            '  ", currentscale " & _
'            '  ", increment " & _
'            '  ", newscale " & _
'            '  ", gradegroupunkid " & _
'            '  ", gradeunkid " & _
'            '  ", gradelevelunkid " & _
'            '  ", isgradechange " & _
'            '  ", isautogenerated " & _
'            '  ", userunkid " & _
'            '  ", isvoid " & _
'            '  ", voiduserunkid " & _
'            '  ", voiddatetime" & _
'            '  ", voidreason " & _
'            '") VALUES (" & _
'            '  "  @periodunkid " & _
'            '  ", @employeeunkid " & _
'            '  ", @incrementdate " & _
'            '  ", @currentscale " & _
'            '  ", @increment " & _
'            '  ", @newscale " & _
'            '  ", @gradegroupunkid " & _
'            '  ", @gradeunkid " & _
'            '  ", @gradelevelunkid " & _
'            '  ", @isgradechange " & _
'            '  ", @isautogenerated " & _
'            '  ", @userunkid " & _
'            '  ", @isvoid " & _
'            '  ", @voiduserunkid " & _
'            '  ", @voiddatetime" & _
'            '  ", @voidreason " & _
'            '"); SELECT @@identity"
'            strQ = "INSERT INTO prsalaryincrement_tran ( " & _
'              "  periodunkid " & _
'              ", employeeunkid " & _
'              ", incrementdate " & _
'              ", currentscale " & _
'              ", increment " & _
'              ", newscale " & _
'              ", gradegroupunkid " & _
'              ", gradeunkid " & _
'              ", gradelevelunkid " & _
'              ", isgradechange " & _
'                          ", isfromemployee " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime" & _
'              ", voidreason " & _
'              ", reason_id " & _
'              ", increment_mode " & _
'              ", percentage " & _
'              ", isapproved " & _
'              ", approveruserunkid " & _
'              ", psoft_syncdatetime " & _
'              ", rehiretranunkid " & _
'            ") VALUES (" & _
'              "  @periodunkid " & _
'              ", @employeeunkid " & _
'              ", @incrementdate " & _
'              ", @currentscale " & _
'              ", @increment " & _
'              ", @newscale " & _
'              ", @gradegroupunkid " & _
'              ", @gradeunkid " & _
'              ", @gradelevelunkid " & _
'              ", @isgradechange " & _
'                      ", @isfromemployee " & _
'              ", @userunkid " & _
'              ", @isvoid " & _
'              ", @voiduserunkid " & _
'              ", @voiddatetime" & _
'              ", @voidreason " & _
'              ", @reason_id " & _
'              ", @increment_mode " & _
'              ", @percentage " & _
'              ", @isapproved " & _
'              ", @approveruserunkid " & _
'              ", @psoft_syncdatetime " & _
'              ", @rehiretranunkid " & _
'            "); SELECT @@identity" 'Sohail (31 Mar 2012) - [increment_mode,percentage]
'            'Sohail (27 Nov 2014) - [psoft_syncdatetime]
'            'Sohail (24 Sep 2012) - [isapproved, approveruserunkid]
'            'Sohail (09 Oct 2010) -- End
'            'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                objDataOperation.ReleaseTransaction(False)
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintSalaryincrementtranunkid = dsList.Tables(0).Rows(0).Item(0)

'            If blnUpdateEmployeeScale = True Then

'                'S.SANDEEP [04 JUN 2015] -- START
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                ' -----> NO NEED TO UPDATE EMPLOYEE COZ - DATA WILL BE DISPLAYED FROM SALARY INCREMENT ON EMPLOYEE

'                'objEmployee._Employeeunkid = mintEmployeeunkid

'                ''Sohail (27 Oct 2014) -- Start
'                ''dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, DateTime.Today)
'                'dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, mdtIncrementdate.Date)
'                ''Sohail (27 Oct 2014) -- End
'                'If dsList.Tables("Salary").Rows.Count > 0 Then
'                '    objEmployee._Scale = Format(dsList.Tables("Salary").Rows(0).Item("newscale"), "#0.00")
'                '    objEmployee._Gradegroupunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradegroupunkid").ToString)
'                '    objEmployee._Gradeunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradeunkid").ToString)
'                '    objEmployee._Gradelevelunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString)
'                'Else
'                '    dsList = objWages.getScaleInfo(objEmployee._Gradeunkid, objEmployee._Gradelevelunkid, "Scale")
'                '    objWagesMaster._Wagesunkid = CInt(dsList.Tables("Scale").Rows(0).Item("wagesunkid").ToString)
'                '    If dsList.Tables("Scale").Rows.Count > 0 Then
'                '        objEmployee._Scale = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), "#0.00")
'                '        objEmployee._Gradegroupunkid = objWagesMaster._Gradegroupunkid
'                '        objEmployee._Gradeunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradeunkid").ToString)
'                '        objEmployee._Gradelevelunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradelevelunkid").ToString)
'                '    Else
'                '        objEmployee._Scale = "0.00"
'                '    End If
'                'End If

'                'objEmployee.Update(, , True)

'                ''Sohail (12 Oct 2011) -- Start
'                'If objEmployee._Message <> "" Then
'                '    objDataOperation.ReleaseTransaction(False)
'                '    exForce = New Exception(objEmployee._Message)
'                '    Throw exForce
'                'End If
'                ''Sohail (12 Oct 2011) -- End

'                'S.SANDEEP [04 JUN 2015] -- END


'                'S.SANDEEP [ 04 SEP 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'If UpdateED_Detail(objDataOperation) = False Then
'                If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString) = False Then
'                    'Sohail (21 Aug 2015) -- End
'                    objDataOperation.ReleaseTransaction(False)
'                    Return False
'                End If
'                'S.SANDEEP [ 04 SEP 2012 ] -- END

'                'Sohail (11 Nov 2010) -- Start
'                'Anjan (11 Jun 2011)-Start
'                'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'                'Call InsertAuditTrailForSalaryIncrement(objDataOperation, 1)
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'If InsertAuditTrailForSalaryIncrement(objDataOperation, 1) = False Then
'                If InsertAuditTrailForSalaryIncrement(objDataOperation, 1, dtCurrentDateAndTime) = False Then
'                    'Sohail (21 Aug 2015) -- End
'                    objDataOperation.ReleaseTransaction(False)
'                    Return False
'                End If
'                'Anjan (11 Jun 2011)-End

'                'Sohail (11 Nov 2010) -- End
'            End If


'            objDataOperation.ReleaseTransaction(True)
'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'            objEmployee = Nothing
'            objMaster = Nothing
'            objWagesMaster = Nothing
'            objWages = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Suhail
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (prsalaryincrement_tran) </purpose>
'    Public Function Update(ByVal strDatabaseName As String _
'                           , ByVal xUserUnkid As Integer _
'                           , ByVal xYearUnkid As Integer _
'                           , ByVal xCompanyUnkid As Integer _
'                           , ByVal xPeriodStart As DateTime _
'                           , ByVal xPeriodEnd As DateTime _
'                           , ByVal xUserModeSetting As String _
'                           , ByVal xOnlyApproved As Boolean _
'                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
'                           , ByVal blnAllowToApproveEarningDeduction As Boolean _
'                           , ByVal dtCurrentDateAndTime As Date _
'                           , Optional ByVal blnUpdateEmployeeScale As Boolean = True _
'                           , Optional ByVal blnGlobal As Boolean = False _
'                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
'                           , Optional ByVal strFilerString As String = "" _
'                           ) As Boolean
'        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFilerString]
'        'Sohail (20 Nov 2013)  - [blnGlobal]
'        'Sohail (29 Dec 2012) -- Start
'        'TRA - ENHANCEMENT - Code Commented to allow to give increment more than one time in one period (Issue# 131)
'        'If isExist(mintPeriodunkid, mintEmployeeunkid, mintSalaryincrementtranunkid) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Increment for the employee in this period is already done.")
'        '    Return False
'        'End If
'        If isExistOnSameDate(mdtIncrementdate, mintEmployeeunkid, mintSalaryincrementtranunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry! Increment for the employee on the selected date is already done.")
'            Return False
'        End If
'        'Sohail (29 Dec 2012) -- End

'        'Dim objDataOperation As clsDataOperation 'Sohail (20 Nov 2013) 

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objEmployee As New clsEmployee_Master
'        Dim objMaster As New clsMasterData
'        Dim objWagesMaster As New clsWages
'        Dim objWages As New clsWagesTran

'        'S.SANDEEP [14 APR 2015] -- START
'        'objDataOperation = New clsDataOperation
'        If xDataOpr Is Nothing Then
'            objDataOperation = New clsDataOperation
'        Else
'            objDataOperation = xDataOpr
'        End If
'        'S.SANDEEP [14 APR 2015] -- END


'        'Sohail (20 Nov 2013) -- Start
'        'TRA - ENHANCEMENT
'        'objDataOperation.BindTransaction()
'        If blnGlobal = False Then
'            'S.SANDEEP [14 APR 2015] -- START
'            'objDataOperation.BindTransaction()
'            If xDataOpr Is Nothing Then
'                objDataOperation.BindTransaction()
'            End If
'            'S.SANDEEP [14 APR 2015] -- END
'        End If
'        'Sohail (20 Nov 2013) -- End

'        Try
'            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranunkid.ToString)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
'            objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentscale.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewscale.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
'            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
'            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
'            objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
'            'Sohail (09 Oct 2010) -- Start
'            'objDataOperation.AddParameter("@isautogenerated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsautogenerated.ToString)
'            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
'            'Sohail (09 Oct 2010) -- End
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
'            'Sohail (08 Nov 2011) -- Start
'            'objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString) 'Sohail (12 Oct 2011)
'            objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrReason_Id.ToString)
'            'Sohail (08 Nov 2011) -- End
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_mode.ToString)
'            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
'            'Sohail (31 Mar 2012) -- End
'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
'            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
'            'Sohail (24 Sep 2012) -- End
'            'Sohail (27 Nov 2014) -- Start
'            'HYATT Enhancement - OUTBOUND file integration with Aruti.
'            If mdtPSoft_SyncDateTime = Nothing Then
'                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
'            End If
'            'Sohail (27 Nov 2014) -- End

'            'S.SANDEEP [14 APR 2015] -- START
'            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
'            'S.SANDEEP [14 APR 2015] -- END


'            'Sohail (09 Oct 2010) -- Start
'            'Changes : isautogenerated is renamed with isfromemployee
'            'strQ = "UPDATE prsalaryincrement_tran SET " & _
'            '  "  periodunkid = @periodunkid" & _
'            '  ", employeeunkid = @employeeunkid" & _
'            '  ", incrementdate = @incrementdate" & _
'            '  ", currentscale = @currentscale" & _
'            '  ", increment = @increment" & _
'            '  ", newscale = @newscale" & _
'            '  ", gradegroupunkid = @gradegroupunkid" & _
'            '  ", gradeunkid = @gradeunkid" & _
'            '  ", gradelevelunkid = @gradelevelunkid" & _
'            '  ", isgradechange = @isgradechange" & _
'            '  ", isautogenerated = @isautogenerated" & _
'            '  ", userunkid = @userunkid" & _
'            '  ", isvoid = @isvoid" & _
'            '  ", voiduserunkid = @voiduserunkid" & _
'            '  ", voiddatetime = @voiddatetime " & _
'            '  ", voidreason = @voidreason " & _
'            '"WHERE salaryincrementtranunkid = @salaryincrementtranunkid "
'            strQ = "UPDATE prsalaryincrement_tran SET " & _
'              "  periodunkid = @periodunkid" & _
'              ", employeeunkid = @employeeunkid" & _
'              ", incrementdate = @incrementdate" & _
'              ", currentscale = @currentscale" & _
'              ", increment = @increment" & _
'              ", newscale = @newscale" & _
'              ", gradegroupunkid = @gradegroupunkid" & _
'              ", gradeunkid = @gradeunkid" & _
'              ", gradelevelunkid = @gradelevelunkid" & _
'              ", isgradechange = @isgradechange" & _
'              ", isfromemployee = @isfromemployee " & _
'              ", userunkid = @userunkid" & _
'              ", isvoid = @isvoid" & _
'              ", voiduserunkid = @voiduserunkid" & _
'              ", voiddatetime = @voiddatetime " & _
'              ", voidreason = @voidreason " & _
'              ", reason_id = @reason_id " & _
'              ", increment_mode = @increment_mode " & _
'              ", percentage = @percentage " & _
'              ", isapproved = @isapproved " & _
'              ", approveruserunkid = @approveruserunkid " & _
'              ", psoft_syncdatetime = @psoft_syncdatetime " & _
'              ", rehiretranunkid = @rehiretranunkid " & _
'            "WHERE salaryincrementtranunkid = @salaryincrementtranunkid " 'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]
'            'Sohail (27 Nov 2014) - [psoft_syncdatetime]
'            'Sohail (24 Sep 2012) - [isapproved, approveruserunkid]
'            'Sohail (09 Oct 2010) -- End
'            'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                objDataOperation.ReleaseTransaction(False)
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'objEmployee._Employeeunkid = mintEmployeeunkid 'Sohail (23 Apr 2012)

'            If blnUpdateEmployeeScale = True Then 'Sohail (11 Sep 2010)

'                'S.SANDEEP [04 JUN 2015] -- START
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                ' -----> NO NEED TO UPDATE EMPLOYEE COZ - DATA WILL BE DISPLAYED FROM SALARY INCREMENT ON EMPLOYEE

'                'objEmployee._Employeeunkid = mintEmployeeunkid 'Sohail (23 Apr 2012)

'                ''Sohail (27 Oct 2014) -- Start
'                ''dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, DateTime.Today)
'                'dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, mdtIncrementdate.Date)
'                ''Sohail (27 Oct 2014) -- End
'                'If dsList.Tables("Salary").Rows.Count > 0 Then
'                '    objEmployee._Scale = Format(dsList.Tables("Salary").Rows(0).Item("newscale"), "#0.00")
'                '    objEmployee._Gradegroupunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradegroupunkid").ToString)
'                '    objEmployee._Gradeunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradeunkid").ToString)
'                '    objEmployee._Gradelevelunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString)
'                'Else
'                '    dsList = objWages.getScaleInfo(objEmployee._Gradeunkid, objEmployee._Gradelevelunkid, "Scale")
'                '    objWagesMaster._Wagesunkid = CInt(dsList.Tables("Scale").Rows(0).Item("wagesunkid").ToString)
'                '    If dsList.Tables("Scale").Rows.Count > 0 Then
'                '        objEmployee._Scale = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), "#0.00")
'                '        objEmployee._Gradegroupunkid = objWagesMaster._Gradegroupunkid
'                '        objEmployee._Gradeunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradeunkid").ToString)
'                '        objEmployee._Gradelevelunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradelevelunkid").ToString)
'                '    Else
'                '        objEmployee._Scale = "0.00"
'                '    End If
'                'End If

'                ''Sohail (23 Apr 2012) -- Start
'                ''TRA - ENHANCEMENT
'                ''objEmployee.Update(, , True)
'                'objEmployee.Update(, , True, mintUserunkid)
'                ''Sohail (23 Apr 2012) -- End

'                ''Sohail (12 Oct 2011) -- Start
'                'If objEmployee._Message <> "" Then
'                '    'S.SANDEEP [14 APR 2015] -- START
'                '    'objDataOperation.ReleaseTransaction(False)
'                '    If xDataOpr Is Nothing Then
'                '        objDataOperation.ReleaseTransaction(False)
'                '    End If
'                '    'S.SANDEEP [14 APR 2015] -- END
'                '    exForce = New Exception(objEmployee._Message)
'                '    Throw exForce
'                'End If
'                'S.SANDEEP [04 JUN 2015] -- END


'                'Sohail (12 Oct 2011) -- End
'            End If 'Sohail (11 Sep 2010)

'            'Sohail (11 Nov 2010) -- Start

'            'S.SANDEEP [ 04 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'If UpdateED_Detail(objDataOperation) = False Then
'            If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString) = False Then
'                'Sohail (21 Aug 2015) -- End
'                'S.SANDEEP [14 APR 2015] -- START
'                'objDataOperation.ReleaseTransaction(False)
'                If xDataOpr Is Nothing Then
'                    objDataOperation.ReleaseTransaction(False)
'                End If
'                'S.SANDEEP [14 APR 2015] -- END
'                Return False
'            End If
'            'S.SANDEEP [ 04 SEP 2012 ] -- END


'            'Anjan (11 Jun 2011)-Start
'            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'            'Call InsertAuditTrailForSalaryIncrement(objDataOperation, 2)

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'If InsertAuditTrailForSalaryIncrement(objDataOperation, 2) = False Then
'            If InsertAuditTrailForSalaryIncrement(objDataOperation, 2, dtCurrentDateAndTime) = False Then
'                'Sohail (21 Aug 2015) -- End
'                'S.SANDEEP [14 APR 2015] -- START
'                'objDataOperation.ReleaseTransaction(False)
'                If xDataOpr Is Nothing Then
'                    objDataOperation.ReleaseTransaction(False)
'                End If
'                'S.SANDEEP [14 APR 2015] -- END
'                Return False
'            End If
'            'Anjan (11 Jun 2011)-End
'            'Sohail (11 Nov 2010) -- End

'            'S.SANDEEP [14 APR 2015] -- START
'            'objDataOperation.ReleaseTransaction(True)
'            If xDataOpr Is Nothing Then
'                objDataOperation.ReleaseTransaction(True)
'            End If
'            'S.SANDEEP [14 APR 2015] -- END
'            Return True
'        Catch ex As Exception
'            'S.SANDEEP [14 APR 2015] -- START
'            If xDataOpr Is Nothing Then
'                objDataOperation.ReleaseTransaction(False)
'            End If
'            'S.SANDEEP [14 APR 2015] -- END
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'            objEmployee = Nothing
'            objMaster = Nothing
'            objWagesMaster = Nothing
'            objWages = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Suhail
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (prsalaryincrement_tran) </purpose>
'    Public Function Void(ByVal strDatabaseName As String _
'                         , ByVal xYearUnkid As Integer _
'                         , ByVal xCompanyUnkid As Integer _
'                         , ByVal xPeriodStart As DateTime _
'                         , ByVal xPeriodEnd As DateTime _
'                         , ByVal xUserModeSetting As String _
'                         , ByVal xOnlyApproved As Boolean _
'                         , ByVal xIncludeIn_ActiveEmployee As Boolean _
'                         , ByVal intUnkid As Integer _
'                         , ByVal intVoidUserID As Integer _
'                         , ByVal dtVoidDateTime As DateTime _
'                         , ByVal strVoidReason As String _
'                         , ByVal blnAllowToApproveEarningDeduction As Boolean _
'                         , ByVal dtCurrentDateAndTime As Date _
'                         , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
'                         , Optional ByVal strFilerString As String = "" _
'                         ) As Boolean
'        'Sohail (21 Aug 2015) - [strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]
'        '<TODO umcomment when isused methods is complete
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim objDataOperation As clsDataOperation

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objEmployee As New clsEmployee_Master
'        Dim objMaster As New clsMasterData
'        Dim objWagesMaster As New clsWages
'        Dim objWages As New clsWagesTran

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()

'        Try
'            'strQ = "DELETE FROM prsalaryincrement_tran " & _
'            '"WHERE salaryincrementtranunkid = @salaryincrementtranunkid "
'            strQ = "UPDATE prsalaryincrement_tran SET " & _
'              " isvoid = 1" & _
'              ", voiduserunkid = @voiduserunkid" & _
'              ", voiddatetime = @voiddatetime " & _
'              ", voidreason = @voidreason " & _
'            "WHERE salaryincrementtranunkid = @salaryincrementtranunkid "

'            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
'            If dtVoidDateTime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
'            End If

'            Me._Salaryincrementtranunkid = intUnkid

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                objDataOperation.ReleaseTransaction(False)
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            ' -----> NO NEED TO UPDATE EMPLOYEE COZ - DATA WILL BE DISPLAYED FROM SALARY INCREMENT ON EMPLOYEE

'            'objEmployee._Employeeunkid = mintEmployeeunkid

'            'dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, DateTime.Today)
'            'If dsList.Tables("Salary").Rows.Count > 0 Then
'            '    objEmployee._Scale = Format(dsList.Tables("Salary").Rows(0).Item("newscale"), "#0.00")
'            '    objEmployee._Gradegroupunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradegroupunkid").ToString)
'            '    objEmployee._Gradeunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradeunkid").ToString)
'            '    objEmployee._Gradelevelunkid = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString)
'            'Else
'            '    dsList = objWages.getScaleInfo(objEmployee._Gradeunkid, objEmployee._Gradelevelunkid, "Scale")
'            '    objWagesMaster._Wagesunkid = CInt(dsList.Tables("Scale").Rows(0).Item("wagesunkid").ToString)
'            '    If dsList.Tables("Scale").Rows.Count > 0 Then
'            '        objEmployee._Scale = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), "#0.00")
'            '        objEmployee._Gradegroupunkid = objWagesMaster._Gradegroupunkid
'            '        objEmployee._Gradeunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradeunkid").ToString)
'            '        objEmployee._Gradelevelunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradelevelunkid").ToString)
'            '    Else
'            '        objEmployee._Scale = "0.00"
'            '    End If
'            'End If

'            'objEmployee.Update(, , True)

'            ''Sohail (12 Oct 2011) -- Start
'            'If objEmployee._Message <> "" Then
'            '    objDataOperation.ReleaseTransaction(False)
'            '    exForce = New Exception(objEmployee._Message)
'            '    Throw exForce
'            'End If
'            ''Sohail (12 Oct 2011) -- End

'            'S.SANDEEP [04 JUN 2015] -- END



'            'S.SANDEEP [ 04 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            ''Sohail (11 Nov 2010) -- Start
'            'Me._Salaryincrementtranunkid = intUnkid
'            'Call InsertAuditTrailForSalaryIncrement(objDataOperation, 3)
'            ''Sohail (11 Nov 2010) -- End
'            objEmployee._Employeeunkid(xPeriodEnd) = mintEmployeeunkid
'            Me._Salaryincrementtranunkid = intUnkid

'            Dim ObjTHead As New clsTransactionHead
'            'Sohail (21 Aug 2015) -- Start

'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'ObjTHead._Tranheadunkid = objEmployee._Tranhedunkid
'            ObjTHead._Tranheadunkid(strDatabaseName) = objEmployee._Tranhedunkid
'            'Sohail (21 Aug 2015) -- End
'            If ObjTHead._Trnheadtype_Id = enTranHeadType.Informational Then
'                Dim objED As New clsEarningDeduction
'                dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, mdtIncrementdate.Date)
'                If dsList.Tables("Salary").Rows.Count > 0 Then
'                    'Sohail (09 Nov 2013) -- Start
'                    'TRA - ENHANCEMENT
'                    'If objED.InsertAllByEmployeeList(mintEmployeeunkid, _
'                    '                                 objEmployee._Tranhedunkid, _
'                    '                                 0, _
'                    '                                 0, _
'                    '                                 0, _
'                    '                                 dsList.Tables("Salary").Rows(0).Item("newscale"), _
'                    '                                 True, _
'                    '                                 False, _
'                    '                                 mintPeriodunkid, _
'                    '                                 "", _
'                    '                                 objDataOperation, _
'                    '                                 False, _
'                    '                                 False, _
'                    '                                 False) = False Then
'                    'Sohail (21 Aug 2015) -- Start
'                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                    'If objED.InsertAllByEmployeeList(mintEmployeeunkid, _
'                    '                                 objEmployee._Tranhedunkid, _
'                    '                                 0, _
'                    '                                 0, _
'                    '                                 0, _
'                    '                                 dsList.Tables("Salary").Rows(0).Item("newscale"), _
'                    '                                 True, _
'                    '                                 False, _
'                    '                                 mintPeriodunkid, _
'                    '                                 "", _
'                    '                                 Nothing, _
'                    '                                 Nothing, _
'                    '                                 objDataOperation, _
'                    '                                 False, _
'                    '                                 False, _
'                    '                                 False) = False Then
'                    If objED.InsertAllByEmployeeList(strDatabaseName, intVoidUserID, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, _
'                                                     objEmployee._Tranhedunkid, _
'                                                     0, _
'                                                     0, _
'                                                     0, _
'                                                     dsList.Tables("Salary").Rows(0).Item("newscale"), _
'                                                     True, _
'                                                     False, _
'                                                     mintPeriodunkid, _
'                                                     "", _
'                                                     Nothing, _
'                                                     Nothing, _
'                                                     blnAllowToApproveEarningDeduction, _
'                                                      objDataOperation, dtCurrentDateAndTime, _
'                                                     False, _
'                                                     False, _
'                                                     False, blnApplyUserAccessFilter, strFilerString) = False Then
'                        'Sohail (21 Aug 2015) -- End
'                        'Sohail (09 Nov 2013) -- End
'                        objDataOperation.ReleaseTransaction(False)
'                        Return True
'                    End If
'                End If
'            End If
'            ObjTHead = Nothing

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'Call InsertAuditTrailForSalaryIncrement(objDataOperation, 3)
'            If InsertAuditTrailForSalaryIncrement(objDataOperation, 3, dtCurrentDateAndTime) = False Then
'                objDataOperation.ReleaseTransaction(False)
'                Return False
'            End If
'            'Sohail (21 Aug 2015) -- End
'            'S.SANDEEP [ 04 SEP 2012 ] -- END




'            objDataOperation.ReleaseTransaction(True)
'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'            objEmployee = Nothing
'            objMaster = Nothing
'            objWagesMaster = Nothing
'            objWages = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Suhail
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            '<TODO make query>
'            strQ = "<Query>"

'            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Suhail
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intPeriodUnkID As Integer, ByVal intEmployeeUnkID As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            'Sohail (09 Oct 2010) -- Start
'            'Changes : isautogenerated is renamed with isfromemployee
'            'strQ = "SELECT " & _
'            '  "  salaryincrementtranunkid " & _
'            '  ", periodunkid " & _
'            '  ", employeeunkid " & _
'            '  ", incrementdate " & _
'            '  ", currentscale " & _
'            '  ", increment " & _
'            '  ", newscale " & _
'            '  ", gradegroupunkid " & _
'            '  ", gradeunkid " & _
'            '  ", gradelevelunkid " & _
'            '  ", isgradechange " & _
'            '  ", isautogenerated " & _
'            '  ", userunkid " & _
'            '  ", isvoid " & _
'            '  ", voiduserunkid " & _
'            '  ", voiddatetime " & _
'            '  ", voidreason " & _
'            ' "FROM prsalaryincrement_tran " & _
'            ' "WHERE periodunkid = @periodunkid " & _
'            ' "AND employeeunkid = @employeeunkid " & _
'            ' "AND isvoid = 0"
'            strQ = "SELECT " & _
'              "  salaryincrementtranunkid " & _
'              ", periodunkid " & _
'              ", employeeunkid " & _
'              ", incrementdate " & _
'              ", currentscale " & _
'              ", increment " & _
'              ", newscale " & _
'              ", gradegroupunkid " & _
'              ", gradeunkid " & _
'              ", gradelevelunkid " & _
'              ", isgradechange " & _
'              ", isfromemployee " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'              ", reason_id " & _
'              ", increment_mode " & _
'              ", percentage " & _
'              ", isapproved " & _
'              ", approveruserunkid " & _
'             "FROM prsalaryincrement_tran " & _
'             "WHERE periodunkid = @periodunkid " & _
'             "AND employeeunkid = @employeeunkid " & _
'             "AND ISNULL(isvoid,0) = 0" 'Sohail (16 Oct 2010), 'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]
'            'Sohail (24 Sep 2012) - [isapproved, approveruserunkid]
'            'Sohail (09 Oct 2010) -- End
'            If intUnkid > 0 Then
'                strQ &= " AND salaryincrementtranunkid <> @salaryincrementtranunkid"
'                objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            End If

'            objDataOperation.AddParameter("@periodunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intPeriodUnkID)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmployeeUnkID)


'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    'Sohail (29 Dec 2012) -- Start
'    'TRA - ENHANCEMENT - Code Commented to allow to give increment more than one time in one period (Issue# 131)
'    Public Function isExistOnSameDate(ByVal dtIncrementDate As DateTime, ByVal intEmployeeUnkID As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'S.SANDEEP [14 APR 2015] -- START
'        'objDataOperation = New clsDataOperation
'        If xDataOpr IsNot Nothing Then
'            objDataOperation = xDataOpr
'        Else
'            objDataOperation = New clsDataOperation
'        End If
'        objDataOperation.ClearParameters()
'        'S.SANDEEP [14 APR 2015] -- END

'        Try

'            strQ = "SELECT " & _
'              "  salaryincrementtranunkid " & _
'              ", periodunkid " & _
'              ", employeeunkid " & _
'              ", incrementdate " & _
'              ", currentscale " & _
'              ", increment " & _
'              ", newscale " & _
'              ", gradegroupunkid " & _
'              ", gradeunkid " & _
'              ", gradelevelunkid " & _
'              ", isgradechange " & _
'              ", isfromemployee " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'              ", reason_id " & _
'              ", increment_mode " & _
'              ", percentage " & _
'              ", isapproved " & _
'              ", approveruserunkid " & _
'             "FROM prsalaryincrement_tran " & _
'             "WHERE ISNULL(isvoid,0) = 0 " & _
'             "AND employeeunkid = @employeeunkid " & _
'             "AND CONVERT(CHAR(8), incrementdate, 112) = @incrementdate "

'            If intUnkid > 0 Then
'                strQ &= " AND salaryincrementtranunkid <> @salaryincrementtranunkid"
'                objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            End If

'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmployeeUnkID)
'            objDataOperation.AddParameter("@incrementdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtIncrementDate))


'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExistOnSameDate; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'S.SANDEEP [14 APR 2015] -- START
'            'objDataOperation = Nothing
'            If xDataOpr Is Nothing Then
'                objDataOperation = Nothing
'            End If
'            'S.SANDEEP [14 APR 2015] -- END
'        End Try
'    End Function
'    'Sohail (29 Dec 2012) -- End

'    Public Function getLastIncrement(ByVal strListName As String, ByVal intEmployeeUnkID As Integer, Optional ByVal blnIsForValidation As Boolean = False) As DataSet
'        '                           'Sohail (07 Sep 2013) - [blnIsForValidation]
'        Dim dsList As DataSet = Nothing
'        'Dim dtIncrement As Date = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            'Sohail (09 Oct 2010) -- Start
'            'Changes : isautogenerated is renamed with isfromemployee
'            'strQ = "SELECT TOP 1 " & _
'            '    "  salaryincrementtranunkid " & _
'            '    ", periodunkid " & _
'            '    ", employeeunkid " & _
'            '    ", CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) AS incrementdate " & _
'            '    ", currentscale " & _
'            '    ", increment " & _
'            '    ", newscale " & _
'            '    ", gradegroupunkid " & _
'            '    ", gradeunkid " & _
'            '    ", gradelevelunkid " & _
'            '    ", isgradechange " & _
'            '    ", isautogenerated " & _
'            '    ", userunkid " & _
'            '    ", isvoid " & _
'            '    ", voiduserunkid " & _
'            '    ", voiddatetime " & _
'            '    ", voidreason " & _
'            '"FROM prsalaryincrement_tran " & _
'            '"WHERE employeeunkid = @employeeunkid " & _
'            '"AND isvoid = 0" & _
'            '"ORDER BY incrementdate DESC"
'            strQ = "SELECT TOP 1 " & _
'                "  salaryincrementtranunkid " & _
'                ", periodunkid " & _
'                ", employeeunkid " & _
'                ", CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) AS incrementdate " & _
'                ", currentscale " & _
'                ", increment " & _
'                ", newscale " & _
'                ", prsalaryincrement_tran.gradegroupunkid " & _
'                ", prsalaryincrement_tran.gradeunkid " & _
'                ", prsalaryincrement_tran.gradelevelunkid " & _
'                ", isgradechange " & _
'                ", isfromemployee " & _
'                ", userunkid " & _
'                ", isvoid " & _
'                ", voiduserunkid " & _
'                ", voiddatetime " & _
'                ", voidreason " & _
'                ", reason_id " & _
'                ", increment_mode " & _
'                ", percentage " & _
'                ", prsalaryincrement_tran.isapproved "
'            'Sohail (07 Sep 2013) - [isapproved]

'            'Sohail (20 Jun 2012) -- Start
'            'TRA - ENHANCEMENT
'            strQ &= ", hrgradegroup_master.name AS GradeGroupName " & _
'                    ", hrgrade_master.name AS GradeName " & _
'                    ", hrgradelevel_master.name AS GradeLevelName " & _
'                    ", prsalaryincrement_tran.psoft_syncdatetime "
'            'Sohail (27 Nov 2014) - [psoft_syncdatetime]
'            'Sohail (20 Jun 2012) -- End

'            strQ &= "FROM prsalaryincrement_tran "

'            'Sohail (20 Jun 2012) -- Start
'            'TRA - ENHANCEMENT
'            strQ &= "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = prsalaryincrement_tran.gradegroupunkid " & _
'                    "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prsalaryincrement_tran.gradeunkid " & _
'                    "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = prsalaryincrement_tran.gradelevelunkid "
'            'Sohail (20 Jun 2012) -- End

'            strQ &= "WHERE employeeunkid = @employeeunkid " & _
'            "AND ISNULL(isvoid,0) = 0 "

'            'Sohail (07 Sep 2013) -- Start
'            'TRA - ENHANCEMENT
'            '"AND prsalaryincrement_tran.isapproved = @isapproved " 
'            If blnIsForValidation = False Then
'                strQ &= "AND prsalaryincrement_tran.isapproved = @isapproved "
'            End If
'            'Sohail (07 Sep 2013) -- End


'            strQ &= "ORDER BY incrementdate DESC" 'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]
'            'Sohail (24 Sep 2012) - [isapproved]
'            'Sohail (09 Oct 2010) -- End

'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
'            objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, enSalaryChangeApprovalStatus.Approved) 'Sohail (24 Sep 2012)

'            dsList = objDataOperation.ExecQuery(strQ, strListName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'If dsList.Tables(strListName).Rows.Count > 0 Then
'            '    dtIncrement = CDate(dsList.Tables(strListName).Rows(0).Item("incrementdate").ToString)
'            'End If
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: getLastIncrementDate; Module Name: " & mstrModuleName)
'        End Try
'        Return dsList
'    End Function

'    'Sohail (09 Oct 2010) -- Start
'    ''' <summary>
'    ''' Get Employee List whose increment is done after the given Date
'    ''' </summary>
'    ''' <param name="dtIncrementDate"></param>
'    ''' <returns></returns>
'    ''' <remarks></remarks>
'    Public Function GetEmployeeList(ByVal xDatabaseName As String _
'                                    , ByVal xUserUnkid As Integer _
'                                    , ByVal xYearUnkid As Integer _
'                                    , ByVal xCompanyUnkid As Integer _
'                                    , ByVal xPeriodStart As DateTime _
'                                    , ByVal xPeriodEnd As DateTime _
'                                    , ByVal xUserModeSetting As String _
'                                    , ByVal xOnlyApproved As Boolean _
'                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
'                                    , ByVal strTable As String _
'                                    , ByVal dtIncrementDate As Date _
'                                    , ByVal blnApplyUserAccessFilter As Boolean _
'                                    , ByVal strFilerString As String _
'                                    , Optional ByVal intSalaryChangeApprovalStatus As Integer = enSalaryChangeApprovalStatus.All _
'                                    ) As DataSet
'        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFilerString]
'        '                           'Sohail (24 Sep 2012) - [intSalaryChangeApprovalStatus]

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'Sohail (21 Aug 2015) -- Start
'        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
'        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
'        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
'        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
'        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
'        'Sohail (21 Aug 2015) -- End

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                          "  prsalaryincrement_tran.salaryincrementtranunkid " & _
'                          ", prsalaryincrement_tran.periodunkid " & _
'                          ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
'                          ", cfcommon_period_tran.period_name " & _
'                          ", prsalaryincrement_tran.employeeunkid " & _
'                          ", CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) AS incrementdate " & _
'                          ", prsalaryincrement_tran.currentscale " & _
'                          ", prsalaryincrement_tran.increment " & _
'                          ", prsalaryincrement_tran.newscale " & _
'                          ", prsalaryincrement_tran.gradegroupunkid " & _
'                          ", prsalaryincrement_tran.gradeunkid " & _
'                          ", prsalaryincrement_tran.gradelevelunkid " & _
'                          ", prsalaryincrement_tran.isgradechange " & _
'                          ", prsalaryincrement_tran.isfromemployee " & _
'                          ", prsalaryincrement_tran.userunkid " & _
'                          ", prsalaryincrement_tran.isvoid " & _
'                          ", prsalaryincrement_tran.voiduserunkid " & _
'                          ", prsalaryincrement_tran.voiddatetime " & _
'                          ", prsalaryincrement_tran.voidreason " & _
'                          ", prsalaryincrement_tran.reason_id " & _
'                          ", prsalaryincrement_tran.increment_mode " & _
'                          ", prsalaryincrement_tran.percentage " & _
'                          ", prsalaryincrement_tran.psoft_syncdatetime " & _
'                 "FROM prsalaryincrement_tran " & _
'                         "LEFT JOIN hremployee_master ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " & _
'                         "LEFT JOIN cfcommon_period_tran ON prsalaryincrement_tran.periodunkid = cfcommon_period_tran.periodunkid "

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            If xDateJoinQry.Trim.Length > 0 Then
'                strQ &= xDateJoinQry
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                strQ &= xUACQry
'            End If

'            If xAdvanceJoinQry.Trim.Length > 0 Then
'                strQ &= xAdvanceJoinQry
'            End If
'            'Sohail (21 Aug 2015) -- End

'            strQ &= "WHERE ISNULL(prsalaryincrement_tran.isvoid,0) = 0 " & _
'                "AND CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) >= @IncrementDate " 'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]
'            'Sohail (27 Nov 2014) - [psoft_syncdatetime]

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            If blnApplyUserAccessFilter = True Then
'                If xUACFiltrQry.Trim.Length > 0 Then
'                    strQ &= " AND " & xUACFiltrQry
'                End If
'            End If

'            'If xIncludeIn_ActiveEmployee = False Then
'            '    If xDateFilterQry.Trim.Length > 0 Then
'            '        strQ &= xDateFilterQry
'            '    End If
'            'End If

'            If strFilerString.Trim.Length > 0 Then
'                strQ &= " AND " & strFilerString
'            End If
'            'Sohail (21 Aug 2015) -- End

'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            If intSalaryChangeApprovalStatus = enSalaryChangeApprovalStatus.Approved Then
'                strQ &= " AND prsalaryincrement_tran.isapproved = @isapproved "
'                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, intSalaryChangeApprovalStatus)
'            ElseIf intSalaryChangeApprovalStatus = enSalaryChangeApprovalStatus.Pending Then
'                strQ &= " AND prsalaryincrement_tran.isapproved = @isapproved "
'                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
'            End If
'            'Sohail (24 Sep 2012) -- End

'            objDataOperation.AddParameter("@IncrementDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtIncrementDate))

'            'Anjan (09 Aug 2011)-Start
'            'Issue : For including setting of acitve and inactive employee.
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            'ISSUE : Employee do not come in list if it is terminated after AS ON EMPLOYEE DATE : SO Increment may be done twice in one period
'            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'            '    'Sohail (06 Jan 2012) -- Start
'            '    'TRA - ENHANCEMENT
'            '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
'            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

'            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            '    'Sohail (06 Jan 2012) -- End
'            'End If
'            'Sohail (31 Mar 2012) -- End
'            'Anjan (09 Aug 2011)-End

'            'Sohail (24 Jun 2011) -- Start
'            'Issue : According to privilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If

'            'S.SANDEEP [ 01 JUNE 2012 ] -- START
'            'ENHANCEMENT : TRA DISCIPLINE CHANGES
'            'Select Case ConfigParameter._Object._UserAccessModeSetting
'            '    Case enAllocation.BRANCH
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.TEAM
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOB_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOBS
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            'End Select
'            'strQ &= UserAccessLevel._AccessLevelFilterString 'Sohail (21 Aug 2015)
'            'S.SANDEEP [ 01 JUNE 2012 ] -- END


'            'S.SANDEEP [ 04 FEB 2012 ] -- END

'            'Sohail (24 Jun 2011) -- End

'            dsList = objDataOperation.ExecQuery(strQ, strTable)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    'Sohail (12 Oct 2011) -- Start
'    'Public Function InsertAll(ByVal strEmployeeList As String, ByVal dtPeriodStartDate As Date) As Boolean
'    '    'If isExist(mintPeriodunkid, mintEmployeeunkid) Then
'    '    '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Increment for the employee in this period is already done.")
'    '    '    Return False
'    '    'End If

'    '    Dim objDataOperation As clsDataOperation

'    '    Dim dsList As DataSet = Nothing
'    '    Dim strQ As String = ""
'    '    Dim exForce As Exception
'    '    Dim objEmployee As New clsEmployee_Master
'    '    Dim objMaster As New clsMasterData
'    '    Dim objWagesMaster As New clsWages
'    '    Dim objWages As New clsWagesTran
'    '    Dim decMaxScale As Decimal = 0 'Sohail (11 May 2011)
'    '    Dim arrEmp() As String

'    '    objDataOperation = New clsDataOperation
'    '    objDataOperation.BindTransaction()

'    '    Try
'    '        arrEmp = strEmployeeList.Split(",")

'    '        For i = 0 To arrEmp.Length - 1
'    '            objDataOperation.ClearParameters()

'    '            mintEmployeeunkid = CInt(arrEmp(i).ToString)
'    '            dsList = objMaster.Get_Current_Scale("CurrScale", mintEmployeeunkid, dtPeriodStartDate)
'    '            If dsList.Tables("CurrScale").Rows.Count > 0 Then
'    '                With dsList.Tables("CurrScale").Rows(0)
'    '                    mdecCurrentscale = CDec(.Item("newscale").ToString) 'Sohail (11 May 2011)
'    '                    mintGradegroupunkid = CInt(.Item("gradegroupunkid").ToString)
'    '                    mintGradeunkid = CInt(.Item("gradeunkid").ToString)
'    '                    mintGradelevelunkid = CInt(.Item("gradelevelunkid").ToString)

'    '                    dsList = objWages.getScaleInfo(mintGradeunkid, mintGradelevelunkid, "Scale")
'    '                    If dsList.Tables("Scale").Rows.Count > 0 Then
'    '                        decMaxScale = CDec(dsList.Tables("Scale").Rows(0).Item("Maximum").ToString) 'Sohail (11 May 2011)
'    '                        mdecIncrement = CDec(dsList.Tables("Scale").Rows(0).Item("increment").ToString) 'Sohail (11 May 2011)
'    '                    End If
'    '                    If decMaxScale < (mdecCurrentscale + mdecIncrement) Then
'    '                        mdecIncrement = decMaxScale - mdecCurrentscale
'    '                    End If
'    '                End With

'    '                If mdecIncrement > 0 Then
'    '                    mdecNewscale = mdecCurrentscale + mdecIncrement

'    '                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'    '                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'    '                    objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
'    '                    objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentscale.ToString) 'Sohail (11 May 2011)
'    '                    objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString) 'Sohail (11 May 2011)
'    '                    objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewscale.ToString) 'Sohail (11 May 2011)
'    '                    objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
'    '                    objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
'    '                    objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
'    '                    objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
'    '                    objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
'    '                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'    '                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'    '                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'    '                    If mdtVoiddatetime = Nothing Then
'    '                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'    '                    Else
'    '                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'    '                    End If
'    '                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'    '                    strQ = "INSERT INTO prsalaryincrement_tran ( " & _
'    '                                  "  periodunkid " & _
'    '                                  ", employeeunkid " & _
'    '                                  ", incrementdate " & _
'    '                                  ", currentscale " & _
'    '                                  ", increment " & _
'    '                                  ", newscale " & _
'    '                                  ", gradegroupunkid " & _
'    '                                  ", gradeunkid " & _
'    '                                  ", gradelevelunkid " & _
'    '                                  ", isgradechange " & _
'    '                                  ", isfromemployee " & _
'    '                                  ", userunkid " & _
'    '                                  ", isvoid " & _
'    '                                  ", voiduserunkid " & _
'    '                                  ", voiddatetime" & _
'    '                                  ", voidreason " & _
'    '                    ") VALUES (" & _
'    '                              "  @periodunkid " & _
'    '                              ", @employeeunkid " & _
'    '                              ", @incrementdate " & _
'    '                              ", @currentscale " & _
'    '                              ", @increment " & _
'    '                              ", @newscale " & _
'    '                              ", @gradegroupunkid " & _
'    '                              ", @gradeunkid " & _
'    '                              ", @gradelevelunkid " & _
'    '                              ", @isgradechange " & _
'    '                              ", @isfromemployee " & _
'    '                              ", @userunkid " & _
'    '                              ", @isvoid " & _
'    '                              ", @voiduserunkid " & _
'    '                              ", @voiddatetime" & _
'    '                              ", @voidreason " & _
'    '                    "); SELECT @@identity"

'    '                    dsList = objDataOperation.ExecQuery(strQ, "List")

'    '                    If objDataOperation.ErrorMessage <> "" Then
'    '                        objDataOperation.ReleaseTransaction(False)
'    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '                        Throw exForce
'    '                    End If

'    '                    mintSalaryincrementtranunkid = dsList.Tables(0).Rows(0).Item(0)

'    '                    '*** Update Scale in Employee Master ***
'    '                    objEmployee._Employeeunkid = mintEmployeeunkid
'    '                    'dsList = objMaster.Get_Current_Scale("Salary", mintEmployeeunkid, DateTime.Today)
'    '                    'If dsList.Tables("Salary").Rows.Count > 0 Then
'    '                    objEmployee._Scale = mdecNewscale ' Format(dsList.Tables("Salary").Rows(0).Item("newscale"), "#0.00")
'    '                    objEmployee._Gradegroupunkid = mintGradegroupunkid 'CInt(dsList.Tables("Salary").Rows(0).Item("gradegroupunkid").ToString)
'    '                    objEmployee._Gradeunkid = mintGradeunkid 'CInt(dsList.Tables("Salary").Rows(0).Item("gradeunkid").ToString)
'    '                    objEmployee._Gradelevelunkid = mintGradelevelunkid 'CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString)
'    '                    'Else
'    '                    '    dsList = objWages.getScaleInfo(objEmployee._Gradeunkid, objEmployee._Gradelevelunkid, "Scale")
'    '                    '    objWagesMaster._Wagesunkid = CInt(dsList.Tables("Scale").Rows(0).Item("wagesunkid").ToString)
'    '                    '    If dsList.Tables("Scale").Rows.Count > 0 Then
'    '                    '        objEmployee._Scale = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), "#0.00")
'    '                    '        objEmployee._Gradegroupunkid = objWagesMaster._Gradegroupunkid
'    '                    '        objEmployee._Gradeunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradeunkid").ToString)
'    '                    '        objEmployee._Gradelevelunkid = CInt(dsList.Tables("Scale").Rows(0).Item("gradelevelunkid").ToString)
'    '                    '    Else
'    '                    '        objEmployee._Scale = "0.00"
'    '                    '    End If
'    '                    'End If

'    '                    objEmployee.Update(, , True)

'    '                    'Sohail (11 Nov 2010) -- Start

'    '                    'Anjan (11 Jun 2011)-Start
'    '                    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'    '                    'Call InsertAuditTrailForSalaryIncrement(objDataOperation, 1)
'    '                    If InsertAuditTrailForSalaryIncrement(objDataOperation, 1) = False Then
'    '                        objDataOperation.ReleaseTransaction(False)
'    '                        Return False
'    '                    End If
'    '                    'Anjan (11 Jun 2011)-End

'    '                    'Sohail (11 Nov 2010) -- End

'    '                End If

'    '            End If
'    '        Next

'    '        objDataOperation.ReleaseTransaction(True)
'    '        Return True
'    '    Catch ex As Exception
'    '        objDataOperation.ReleaseTransaction(False)
'    '        Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
'    '        Return False
'    '    Finally
'    '        exForce = Nothing
'    '        If dsList IsNot Nothing Then dsList.Dispose()
'    '        objDataOperation = Nothing
'    '        objEmployee = Nothing
'    '        objMaster = Nothing
'    '        objWagesMaster = Nothing
'    '        objWages = Nothing
'    '    End Try
'    'End Function

'    Public Function InsertAll(ByVal strDatabaseName As String _
'                              , ByVal xUserUnkid As Integer _
'                              , ByVal xYearUnkid As Integer _
'                              , ByVal xCompanyUnkid As Integer _
'                              , ByVal xPeriodStart As DateTime _
'                              , ByVal xPeriodEnd As DateTime _
'                              , ByVal xUserModeSetting As String _
'                              , ByVal xOnlyApproved As Boolean _
'                              , ByVal xIncludeIn_ActiveEmployee As Boolean _
'                              , ByVal dtTable As DataTable _
'                              , ByVal blnAllowToApproveEarningDeduction As Boolean _
'                              , ByVal dtCurrentDateAndTime As Date _
'                              , Optional ByRef dicAppraisal As Dictionary(Of String, ArrayList) = Nothing _
'                              , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
'                              , Optional ByVal strFilerString As String = "" _
'                              ) As Boolean
'        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]
'        'Sohail (29 Dec 2012) - [dicAppraisal]
'        'If isExist(mintPeriodunkid, mintEmployeeunkid) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Increment for the employee in this period is already done.")
'        '    Return False
'        'End If

'        Dim objDataOperation As clsDataOperation

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objEmployee As New clsEmployee_Master
'        Dim objMaster As New clsMasterData
'        Dim objWagesMaster As New clsWages
'        Dim objWages As New clsWagesTran
'        Dim decMaxScale As Decimal = 0
'        Dim arrList As ArrayList 'Sohail (29 Dec 2012)

'        'S.SANDEEP [ 04 SEP 2012 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        Dim objTHead As New clsTransactionHead
'        'S.SANDEEP [ 04 SEP 2012 ] -- END


'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()

'        Try

'            For Each dtRow As DataRow In dtTable.Rows
'                objDataOperation.ClearParameters()

'                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
'                mdecCurrentscale = CDec(dtRow.Item("currentscale"))
'                mdecIncrement = CDec(dtRow.Item("increment"))
'                mdecNewscale = CDec(dtRow.Item("newscale"))
'                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
'                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
'                mintGradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                mintIncrement_mode = CInt(dtRow.Item("increment_mode"))
'                mdblPercentage = CDbl(dtRow.Item("percentage"))
'                'Sohail (31 Mar 2012) -- End
'                mblnIsgradechange = CBool(dtRow.Item("isgradechange")) 'Sohail (10 Nov 2014)

'                'Sohail (09 Jun 2015) -- Start
'                'Issue - Allow to Give Zero Increment if Salary Change is for Grade Change. Earlier increment was not gettng done if grade is changed with same Scale.
'                'If mdecIncrement <> 0 Then
'                If mblnIsgradechange = True OrElse mdecIncrement <> 0 Then
'                    'Sohail (09 Jun 2015) -- End
'                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'                    objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
'                    objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentscale.ToString)
'                    objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString)
'                    objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewscale.ToString)
'                    objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
'                    objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
'                    objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
'                    objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
'                    objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
'                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'                    If mdtVoiddatetime = Nothing Then
'                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                    Else
'                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'                    End If
'                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
'                    'Sohail (08 Nov 2011) -- Start
'                    'objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'                    objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrReason_Id.ToString)
'                    'Sohail (08 Nov 2011) -- End
'                    'Sohail (31 Mar 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_mode.ToString)
'                    objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
'                    'Sohail (31 Mar 2012) -- End
'                    'Sohail (24 Sep 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
'                    objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
'                    'Sohail (24 Sep 2012) -- End
'                    'Sohail (27 Nov 2014) -- Start
'                    'HYATT Enhancement - OUTBOUND file integration with Aruti.
'                    If mdtPSoft_SyncDateTime = Nothing Then
'                        objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                    Else
'                        objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
'                    End If
'                    'Sohail (27 Nov 2014) -- End

'                    strQ = "INSERT INTO prsalaryincrement_tran ( " & _
'                                  "  periodunkid " & _
'                                  ", employeeunkid " & _
'                                  ", incrementdate " & _
'                                  ", currentscale " & _
'                                  ", increment " & _
'                                  ", newscale " & _
'                                  ", gradegroupunkid " & _
'                                  ", gradeunkid " & _
'                                  ", gradelevelunkid " & _
'                                  ", isgradechange " & _
'                                  ", isfromemployee " & _
'                                  ", userunkid " & _
'                                  ", isvoid " & _
'                                  ", voiduserunkid " & _
'                                  ", voiddatetime" & _
'                                  ", voidreason " & _
'                              ", reason_id " & _
'                              ", increment_mode " & _
'                              ", percentage " & _
'                              ", isapproved " & _
'                              ", approveruserunkid " & _
'                          ", psoft_syncdatetime " & _
'                    ") VALUES (" & _
'                              "  @periodunkid " & _
'                              ", @employeeunkid " & _
'                              ", @incrementdate " & _
'                              ", @currentscale " & _
'                              ", @increment " & _
'                              ", @newscale " & _
'                              ", @gradegroupunkid " & _
'                              ", @gradeunkid " & _
'                              ", @gradelevelunkid " & _
'                              ", @isgradechange " & _
'                              ", @isfromemployee " & _
'                              ", @userunkid " & _
'                              ", @isvoid " & _
'                              ", @voiduserunkid " & _
'                              ", @voiddatetime" & _
'                              ", @voidreason " & _
'                          ", @reason_id " & _
'                          ", @increment_mode " & _
'                          ", @percentage " & _
'                              ", @isapproved " & _
'                              ", @approveruserunkid " & _
'                          ", @psoft_syncdatetime " & _
'                    "); SELECT @@identity"
'                    'Sohail (27 Nov 2014) - [psoft_syncdatetime]
'                    'Sohail (24 Sep 2012) - [isapproved, approveruserunkid]

'                    dsList = objDataOperation.ExecQuery(strQ, "List")

'                    If objDataOperation.ErrorMessage <> "" Then
'                        objDataOperation.ReleaseTransaction(False)
'                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    mintSalaryincrementtranunkid = dsList.Tables(0).Rows(0).Item(0)

'                    '*** Update Scale in Employee Master ***
'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    ' -----> NO NEED TO UPDATE EMPLOYEE COZ - DATA WILL BE DISPLAYED FROM SALARY INCREMENT ON EMPLOYEE

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'objEmployee._Employeeunkid = mintEmployeeunkid
'                    objEmployee._Employeeunkid(xPeriodEnd) = mintEmployeeunkid
'                    'S.SANDEEP [04 JUN 2015] -- END



'                    'objEmployee._Scale = mdecNewscale
'                    'objEmployee._Gradegroupunkid = mintGradegroupunkid
'                    'objEmployee._Gradeunkid = mintGradeunkid
'                    'objEmployee._Gradelevelunkid = mintGradelevelunkid

'                    'objEmployee.Update(, , True)

'                    'If objEmployee._Message <> "" Then
'                    '    objDataOperation.ReleaseTransaction(False)
'                    '    exForce = New Exception(objEmployee._Message)
'                    '    Throw exForce
'                    'End If

'                    'S.SANDEEP [04 JUN 2015] -- END

'                    'S.SANDEEP [ 04 SEP 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'                    'Sohail (21 Aug 2015) -- Start
'                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                    'objTHead._Tranheadunkid = objEmployee._Tranhedunkid
'                    objTHead._Tranheadunkid(strDatabaseName) = objEmployee._Tranhedunkid
'                    'Sohail (21 Aug 2015) -- End
'                    If objTHead._Trnheadtype_Id = enTranHeadType.Informational Then
'                        mintInfoSalHeadUnkid = objEmployee._Tranhedunkid
'                        'Sohail (21 Aug 2015) -- Start
'                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                        'If UpdateED_Detail(objDataOperation) = False Then
'                        If UpdateED_Detail(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString) = False Then
'                            'Sohail (21 Aug 2015) -- End
'                            objDataOperation.ReleaseTransaction(False)
'                            Return False
'                        End If
'                        mintInfoSalHeadUnkid = -1
'                    End If
'                    'S.SANDEEP [ 04 SEP 2012 ] -- END

'                    'Sohail (21 Aug 2015) -- Start
'                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                    'If InsertAuditTrailForSalaryIncrement(objDataOperation, 1) = False Then
'                    If InsertAuditTrailForSalaryIncrement(objDataOperation, 1, dtCurrentDateAndTime) = False Then
'                        'Sohail (21 Aug 2015) -- End
'                        objDataOperation.ReleaseTransaction(False)
'                        Return False
'                    End If

'                    'Sohail (29 Dec 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    If dicAppraisal IsNot Nothing AndAlso dicAppraisal.ContainsKey(mintEmployeeunkid.ToString) = True Then
'                        arrList = dicAppraisal.Item(mintEmployeeunkid.ToString)
'                        arrList(1) = mintSalaryincrementtranunkid
'                        dicAppraisal.Item(mintEmployeeunkid.ToString) = arrList
'                    End If
'                    'Sohail (29 Dec 2012) -- End
'                End If
'                'S.SANDEEP [ 18 SEP 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES (For Notification From Import)
'                mintSuccessCnt = mintSuccessCnt + 1
'                'S.SANDEEP [ 18 SEP 2012 ] -- END
'            Next

'            objDataOperation.ReleaseTransaction(True)
'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'            objEmployee = Nothing
'            objMaster = Nothing
'            objWagesMaster = Nothing
'            objWages = Nothing
'        End Try
'    End Function
'    'Sohail (12 Oct 2011) -- End

'    'Sohail (09 Oct 2010) -- End

'    'Sohail (11 Nov 2010) -- Start
'    'Anjan (11 Jun 2011)-Start
'    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'    'Public Sub InsertAuditTrailForSalaryIncrement(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
'    Public Function InsertAuditTrailForSalaryIncrement(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
'        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
'        'Anjan (11 Jun 2011)-End
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            'Sohail (24 Dec 2013) -- Start
'            'Enhancement - Send link in salary change notification
'            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
'            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()
'            'Sohail (24 Dec 2013) -- End

'            'S.SANDEEP [ 19 JULY 2012 ] -- START
'            'Enhancement : TRA Changes

'            'strQ = "INSERT INTO atprsalaryincrement_tran ( " & _
'            '              "  salaryincrementtranunkid " & _
'            '              ", periodunkid " & _
'            '              ", employeeunkid " & _
'            '              ", incrementdate " & _
'            '              ", currentscale " & _
'            '              ", increment " & _
'            '              ", newscale " & _
'            '              ", gradegroupunkid " & _
'            '              ", gradeunkid " & _
'            '              ", gradelevelunkid " & _
'            '              ", isgradechange " & _
'            '              ", isfromemployee " & _
'            '              ", audittype " & _
'            '              ", audituserunkid " & _
'            '              ", auditdatetime " & _
'            '              ", ip " & _
'            '              ", machine_name" & _
'            '              ", reason_id " & _
'            '              ", increment_mode " & _
'            '              ", percentage " & _
'            '    ") VALUES (" & _
'            '              "  @salaryincrementtranunkid " & _
'            '              ", @periodunkid " & _
'            '              ", @employeeunkid " & _
'            '              ", @incrementdate " & _
'            '              ", @currentscale " & _
'            '              ", @increment " & _
'            '              ", @newscale " & _
'            '              ", @gradegroupunkid " & _
'            '              ", @gradeunkid " & _
'            '              ", @gradelevelunkid " & _
'            '              ", @isgradechange " & _
'            '              ", @isfromemployee " & _
'            '              ", @audittype " & _
'            '              ", @audituserunkid " & _
'            '              ", @auditdatetime " & _
'            '              ", @ip " & _
'            '              ", @machine_name" & _
'            '              ", @reason_id " & _
'            '              ", @increment_mode " & _
'            '              ", @percentage " & _
'            '    "); SELECT @@identity" 'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]

'            strQ = "INSERT INTO atprsalaryincrement_tran ( " & _
'                          "  salaryincrementtranunkid " & _
'                          ", periodunkid " & _
'                          ", employeeunkid " & _
'                          ", incrementdate " & _
'                          ", currentscale " & _
'                          ", increment " & _
'                          ", newscale " & _
'                          ", gradegroupunkid " & _
'                          ", gradeunkid " & _
'                          ", gradelevelunkid " & _
'                          ", isgradechange " & _
'                          ", isfromemployee " & _
'                          ", audittype " & _
'                          ", audituserunkid " & _
'                          ", auditdatetime " & _
'                          ", ip " & _
'                          ", machine_name" & _
'                          ", reason_id " & _
'                          ", increment_mode " & _
'                          ", percentage " & _
'                       ", form_name " & _
'                       ", module_name1 " & _
'                       ", module_name2 " & _
'                       ", module_name3 " & _
'                       ", module_name4 " & _
'                       ", module_name5 " & _
'                       ", isweb " & _
'                        ", isapproved " & _
'                        ", approveruserunkid " & _
'                        ", psoft_syncdatetime " & _
'                        ", rehiretranunkid " & _
'                ") VALUES (" & _
'                          "  @salaryincrementtranunkid " & _
'                          ", @periodunkid " & _
'                          ", @employeeunkid " & _
'                          ", @incrementdate " & _
'                          ", @currentscale " & _
'                          ", @increment " & _
'                          ", @newscale " & _
'                          ", @gradegroupunkid " & _
'                          ", @gradeunkid " & _
'                          ", @gradelevelunkid " & _
'                          ", @isgradechange " & _
'                          ", @isfromemployee " & _
'                          ", @audittype " & _
'                          ", @audituserunkid " & _
'                          ", @auditdatetime " & _
'                          ", @ip " & _
'                          ", @machine_name" & _
'                          ", @reason_id " & _
'                          ", @increment_mode " & _
'                          ", @percentage " & _
'                       ", @form_name " & _
'                       ", @module_name1 " & _
'                       ", @module_name2 " & _
'                       ", @module_name3 " & _
'                       ", @module_name4 " & _
'                       ", @module_name5 " & _
'                       ", @isweb " & _
'                        ", @isapproved " & _
'                        ", @approveruserunkid " & _
'                        ", @psoft_syncdatetime " & _
'                        ", @rehiretranunkid " & _
'                "); SELECT @@identity" 'Sohail (08 Nov 2011) - [reason_id], 'Sohail (31 Mar 2012) - [increment_mode,percentage]
'            'Sohail (27 Nov 2014) - [psoft_syncdatetime]
'            'Sohail (24 Sep 2012) - [isapproved, approveruserunkid]
'            'S.SANDEEP [ 19 JULY 2012 ] -- END
'            'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END


'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranunkid.ToString)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
'            objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentscale.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrement.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewscale.ToString) 'Sohail (11 May 2011)
'            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
'            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
'            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
'            objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
'            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
'            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
'            'Sohail (23 Apr 2012) -- Start
'            'TRA - ENHANCEMENT
'            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
'            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
'            'Sohail (23 Apr 2012) -- End
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
'            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
'            'Sohail (21 Aug 2015) -- End
'            'Sohail (24 Dec 2013) -- Start
'            'Enhancement - Send link in salary change notification
'            'objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
'            'objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
'            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
'            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
'            'Sohail (24 Dec 2013) -- End

'            'Sohail (08 Nov 2011) -- Start
'            'objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString) 'Sohail (12 Oct 2011)
'            objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrReason_Id.ToString)
'            'Sohail (08 Nov 2011) -- End
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_mode.ToString)
'            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
'            'Sohail (31 Mar 2012) -- End
'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
'            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
'            'Sohail (24 Sep 2012) -- End
'            'Sohail (27 Nov 2014) -- Start
'            'HYATT Enhancement - OUTBOUND file integration with Aruti.
'            If mdtPSoft_SyncDateTime = Nothing Then
'                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
'            End If
'            'Sohail (27 Nov 2014) -- End

'            'S.SANDEEP [ 19 JULY 2012 ] -- START
'            'Enhancement : TRA Changes

'            If mstrWebFormName.Trim.Length <= 0 Then
'                'S.SANDEEP [ 11 AUG 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'Dim frm As Form
'                'For Each frm In Application.OpenForms
'                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
'                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
'                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
'                '    End If
'                'Next
'                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
'                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
'                'S.SANDEEP [ 11 AUG 2012 ] -- END
'            Else
'                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
'                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
'                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
'            End If
'            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
'            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
'            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
'            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
'            'S.SANDEEP [ 19 JULY 2012 ] -- END

'            'S.SANDEEP [14 APR 2015] -- START
'            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
'            'S.SANDEEP [14 APR 2015] -- END

'            objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'Anjan (11 Jun 2011)-Start
'            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'            Return True
'            'Anjan (11 Jun 2011)-End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForSalaryIncrement", mstrModuleName)
'            'Anjan (11 Jun 2011)-Start
'            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
'            Return False
'            'Anjan (11 Jun 2011)-End
'        Finally
'            objDataOperation = Nothing
'        End Try
'    End Function
'    'Sohail (11 Nov 2010) -- End

'    'Sohail (10 Feb 2012) -- Start
'    'TRA - ENHANCEMENT
'    Public Function isUsedInAppraisal(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal intAppraisalMode As Integer, ByVal intSalaryIncrememntTranUnkId As Integer) As Boolean
'        'Sohail (29 Dec 2012) - [intSalaryIncrememntTranUnkId]

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            strQ = "SELECT  finalemployeeunkid " & _
'                    "FROM    hrapps_finalemployee " & _
'                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
'                            "AND operationmodeid = @operationmodeid " & _
'                            "AND employeeunkid = @employeeunkid " & _
'                            "AND periodunkid = @periodunkid " & _
'                            "AND salaryincrementtranunkid = @salaryincrementtranunkid "
'            '               'Sohail (29 Dec 2012) - [salaryincrementtranunkid]

'            objDataOperation.AddParameter("@operationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAppraisalMode)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
'            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSalaryIncrememntTranUnkId) 'Sohail (29 Dec 2012)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsedInAppraisal; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function
'    'Sohail (10 Feb 2012) -- End

'    'Sohail (24 Feb 2012) -- Start
'    'TRA - ENHANCEMENT
'    Public Function isUsedInTrainingEnrollment(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal intTrainingAwardMode As Integer, ByVal intSalaryIncrememntTranUnkId As Integer) As Boolean
'        'Sohail (29 Dec 2012) - [intSalaryIncrememntTranUnkId]

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            strQ = "SELECT  trainingenrolltranunkid " & _
'                    "FROM    hrtraining_enrollment_tran " & _
'                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
'                            "AND operationmodeid = @operationmodeid " & _
'                            "AND employeeunkid = @employeeunkid " & _
'                            "AND periodunkid = @periodunkid " & _
'                            "AND salaryincrementtranunkid = @salaryincrementtranunkid "
'            '               'Sohail (29 Dec 2012) - [salaryincrementtranunkid]

'            objDataOperation.AddParameter("@operationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingAwardMode)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
'            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSalaryIncrememntTranUnkId) 'Sohail (29 Dec 2012)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsedInTrainingEnrollment; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function
'    'Sohail (24 Feb 2012) -- End

'    'Sohail (22 Aug 2012) -- Start
'    'TRA - ENHANCEMENT
'    Public Function GetImportFileFormat(ByVal strTable As String) As DataSet

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT '' AS [employeecode], 0.00 as [increment amount/percentage/newscale], '' AS gradegroup, '' AS grade, '' AS gradelevel WHERE 1 = 2 "

'            dsList = objDataOperation.ExecQuery(strQ, strTable)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetImportFileFormat; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function
'    'Sohail (22 Aug 2012) -- End


'    'S.SANDEEP [ 04 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'    Private Function UpdateED_Detail(ByVal strDatabaseName As String _
'                                     , ByVal xUserUnkid As Integer _
'                                     , ByVal xYearUnkid As Integer _
'                                     , ByVal xCompanyUnkid As Integer _
'                                     , ByVal xPeriodStart As DateTime _
'                                     , ByVal xPeriodEnd As DateTime _
'                                     , ByVal xUserModeSetting As String _
'                                     , ByVal xOnlyApproved As Boolean _
'                                     , ByVal xIncludeIn_ActiveEmployee As Boolean _
'                                     , ByVal blnAllowToApproveEarningDeduction As Boolean _
'                                     , ByVal objDataOperation As clsDataOperation _
'                                     , ByVal dtCurrentDateAndTime As Date _
'                                     , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
'                                     , Optional ByVal strFilerString As String = "" _
'                                     ) As Boolean
'        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]
'        Dim ObjED As New clsEarningDeduction
'        Try
'            If mintInfoSalHeadUnkid > 0 Then
'                'Sohail (09 Nov 2013) -- Start
'                'TRA - ENHANCEMENT
'                'If ObjED.InsertAllByEmployeeList(mintEmployeeunkid, _
'                '                                 mintInfoSalHeadUnkid, _
'                '                                 0, _
'                '                                 0, _
'                '                                 0, _
'                '                                 mdecNewscale, _
'                '                                 True, _
'                '                                 False, _
'                '                                 mintPeriodunkid, _
'                '                                 "", _
'                '                                 objDataOperation, _
'                '                                 True, _
'                '                                 mblnIsCopyPrevoiusSLAB, _
'                '                                 mblnIsOverwritePrevoiusSLAB) = False Then
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'If ObjED.InsertAllByEmployeeList(mintEmployeeunkid, _
'                '                                 mintInfoSalHeadUnkid, _
'                '                                 0, _
'                '                                 0, _
'                '                                 0, _
'                '                                 mdecNewscale, _
'                '                                 True, _
'                '                                 False, _
'                '                                 mintPeriodunkid, _
'                '                                 "", _
'                '                                 Nothing, _
'                '                                 Nothing, _
'                '                                 objDataOperation, _
'                '                                 True, _
'                '                                 mblnIsCopyPrevoiusSLAB, _
'                '                                 mblnIsOverwritePrevoiusSLAB) = False Then
'                If ObjED.InsertAllByEmployeeList(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, _
'                                                 mintInfoSalHeadUnkid, _
'                                                 0, _
'                                                 0, _
'                                                 0, _
'                                                 mdecNewscale, _
'                                                 True, _
'                                                 False, _
'                                                 mintPeriodunkid, _
'                                                 "", _
'                                                 Nothing, _
'                                                 Nothing, _
'                                                 blnAllowToApproveEarningDeduction, _
'                                                 objDataOperation, dtCurrentDateAndTime, _
'                                                 True, _
'                                                 mblnIsCopyPrevoiusSLAB, _
'                                                 mblnIsOverwritePrevoiusSLAB, blnApplyUserAccessFilter, strFilerString) = False Then
'                    'Sohail (21 Aug 2015) -- End
'                    'Sohail (09 Nov 2013) -- End

'                    Return False
'                End If
'            End If
'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: UpdateED_Detail; Module Name: " & mstrModuleName)
'        Finally
'            ObjED = Nothing
'        End Try
'    End Function
'    'S.SANDEEP [ 04 SEP 2012 ] -- END


'#Region " Message List "
'    '1, "Increment for the employee in this period is already done."
'#End Region

'End Class