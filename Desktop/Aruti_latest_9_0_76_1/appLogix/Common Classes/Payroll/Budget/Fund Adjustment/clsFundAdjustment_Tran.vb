﻿'************************************************************************************************************************************
'Class Name : clsFundAdjustment_Tran.vb
'Purpose    :
'Date       : 23/05/2016
'Written By : Nilay
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Nilay
''' </summary>
Public Class clsFundAdjustment_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsFundAdjustment_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintFundAdjustmentunkid As Integer
    Private mintFundSourceunkid As Integer = -1
    Private mintFundProjectCodeunkid As Integer
    Private mdtTransactionDate As DateTime
    Private mdecCurrentBalance As Decimal = 0
    Private mdecIncrDecrAmount As Decimal = 0
    Private mstrRemark As String = String.Empty
    Private mdecNewBalance As Decimal = 0
    Private mintUserunkid As Integer = -1
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer = -1
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mintLoginEmployeeunkid As Integer = -1
    Private mintFundActivityAdjustmentunkid As Integer 'Sohail (22 Nov 2016)
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Nilay
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundadjustmentunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _FundAdjustmentunkid() As Integer
        Get
            Return mintFundAdjustmentunkid
        End Get
        Set(ByVal value As Integer)
            mintFundAdjustmentunkid = value
            Call GetData()
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set fundsourceunkid
    '''' Modify By: Nilay
    '''' NOT IN USE
    '''' </summary>
    'Public Property _FundSourceunkid() As Integer
    '    Get
    '        Return mintFundSourceunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintFundSourceunkid = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set fundprojectcodeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundProjectCodeunkid() As Integer
        Get
            Return mintFundProjectCodeunkid
        End Get
        Set(ByVal value As Integer)
            mintFundProjectCodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _TransactionDate() As Date
        Get
            Return mdtTransactionDate
        End Get
        Set(ByVal value As Date)
            mdtTransactionDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currentbalance
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _CurrentBalance() As Decimal
        Get
            Return mdecCurrentBalance
        End Get
        Set(ByVal value As Decimal)
            mdecCurrentBalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set incrdecramount
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _IncrDecrAmount() As Decimal
        Get
            Return mdecIncrDecrAmount
        End Get
        Set(ByVal value As Decimal)
            mdecIncrDecrAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set newbalance
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _NewBalance() As Decimal
        Get
            Return mdecNewBalance
        End Get
        Set(ByVal value As Decimal)
            mdecNewBalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    '''Sohail (22 Nov 2016) -- Start
    '''Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
    ''' <summary>
    ''' Purpose: Get or Set fundactivityadjustmentunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Fundactivityadjustmentunkid() As Integer
        Get
            Return mintFundActivityAdjustmentunkid
        End Get
        Set(ByVal value As Integer)
            mintFundActivityAdjustmentunkid = value
            Call GetData()
        End Set
    End Property
    'Sohail (22 Nov 2016) -- End

#End Region

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  fundadjustmentunkid " & _
                      ", fundsourceunkid " & _
                      ", fundprojectcodeunkid " & _
                      ", transactiondate " & _
                      ", currentbalance " & _
                      ", incrdecramount " & _
                      ", remark " & _
                      ", newbalance " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", ISNULL(fundactivityadjustmentunkid, 0) AS fundactivityadjustmentunkid " & _
                   "FROM bgfundadjustment_tran " & _
                   "WHERE fundadjustmentunkid = @fundadjustmentunkid "
            'Sohail (22 Nov 2016) - [fundactivityadjustmentunkid]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundAdjustmentunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintFundAdjustmentunkid = CInt(dtRow.Item("fundadjustmentunkid"))
                mintFundSourceunkid = CInt(dtRow.Item("fundsourceunkid"))
                mintFundProjectCodeunkid = CInt(dtRow.Item("fundprojectcodeunkid"))
                If IsDBNull(dtRow.Item("transactiondate")) Then
                    mdtTransactionDate = Nothing
                Else
                    mdtTransactionDate = dtRow.Item("transactiondate")
                End If
                mdecCurrentBalance = dtRow.Item("currentbalance")
                mdecIncrDecrAmount = dtRow.Item("incrdecramount")
                mstrRemark = dtRow.Item("remark").ToString
                mdecNewBalance = dtRow.Item("newbalance")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintFundActivityAdjustmentunkid = CInt(dtRow.Item("fundactivityadjustmentunkid")) 'Sohail (22 Nov 2016)
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  bgfundadjustment_tran.fundadjustmentunkid " & _
                      ", bgfundadjustment_tran.fundsourceunkid " & _
                      ", bgfundadjustment_tran.fundprojectcodeunkid " & _
                      ", ISNULL(bgfundprojectcode_master.fundprojectname,'') AS fundprojectname " & _
                      ", bgfundadjustment_tran.transactiondate " & _
                      ", ISNULL(bgfundadjustment_tran.currentbalance,0) AS currentbalance " & _
                      ", ISNULL(bgfundadjustment_tran.incrdecramount,0) AS incrdecramount " & _
                      ", ISNULL(bgfundadjustment_tran.newbalance,0) AS newbalance " & _
                      ", ISNULL(bgfundadjustment_tran.remark, '') AS remark " & _
                      ", bgfundadjustment_tran.userunkid " & _
                      ", bgfundadjustment_tran.isvoid " & _
                      ", bgfundadjustment_tran.voiduserunkid " & _
                      ", bgfundadjustment_tran.voiddatetime " & _
                      ", bgfundadjustment_tran.voidreason " & _
                      ", ISNULL(bgfundadjustment_tran.fundactivityadjustmentunkid, 0) AS fundactivityadjustmentunkid " & _
                  "FROM bgfundadjustment_tran " & _
                      "  LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid = bgfundadjustment_tran.fundprojectcodeunkid " & _
                  "WHERE bgfundadjustment_tran.isvoid = 0 " & _
                  "AND bgfundprojectcode_master.isvoid = 0 "
            'Sohail (22 Nov 2016) - [fundactivityadjustmentunkid]

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            strQ &= " ORDER BY fundprojectname, transactiondate DESC, fundadjustmentunkid DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLastCurrentBalance(ByVal strTableName As String, ByVal intFundProjectCodeunkid As Integer, _
                                          Optional ByVal objDataOp As clsDataOperation = Nothing) As DataSet
        Dim dsList As New DataSet
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
            Else
            objDataOperation = New clsDataOperation
            End If

            strQ = " SELECT TOP 1 " & _
                        "   fundadjustmentunkid " & _
                        ",  fundsourceunkid " & _
                        ",  fundprojectcodeunkid " & _
                        ",  transactiondate " & _
                        ",  currentbalance " & _
                        ",  incrdecramount " & _
                        ",  remark " & _
                        ",  newbalance " & _
                        ", ISNULL(bgfundadjustment_tran.fundactivityadjustmentunkid, 0) AS fundactivityadjustmentunkid " & _
                   " FROM bgfundadjustment_tran " & _
                   " WHERE isvoid = 0 AND fundprojectcodeunkid = @fundprojectcodeunkid "
            'Sohail (22 Nov 2016) - [fundactivityadjustmentunkid]

            strQ &= " ORDER BY transactiondate DESC, fundadjustmentunkid DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundProjectCodeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLastCurrentBalance", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgfundadjustment_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateTime As DateTime, ByVal objDataOp As clsDataOperation) As Boolean
        'Sohail (22 Nov 2016) - [objDataOp]
        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        'If isExist(mdtTransactionDate, mintFundProjectCodeunkid) Then
        If isExist() Then
            'Sohail (22 Nov 2016) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.")
            Return False
        End If

        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        If isLastTransaction(mintFundProjectCodeunkid, mdtTransactionDate) = False Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.") & " " & mdtTransactionDate.ToShortDateString
            Return False
        End If
        'Sohail (22 Nov 2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (22 Nov 2016) -- End

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundSourceunkid.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactionDate <> Nothing, mdtTransactionDate, DBNull.Value))
            objDataOperation.AddParameter("@currentbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecCurrentBalance.ToString)
            objDataOperation.AddParameter("@incrdecramount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecIncrDecrAmount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@newbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecNewBalance.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundActivityAdjustmentunkid.ToString) 'Sohail (22 Nov 2016)

            strQ = "INSERT INTO bgfundadjustment_tran ( " & _
              "  fundsourceunkid " & _
              ", fundprojectcodeunkid " & _
              ", transactiondate " & _
              ", currentbalance " & _
              ", incrdecramount " & _
              ", remark " & _
              ", newbalance " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", fundactivityadjustmentunkid" & _
            ") VALUES (" & _
              "  @fundsourceunkid " & _
              ", @fundprojectcodeunkid " & _
              ", @transactiondate " & _
              ", @currentbalance " & _
              ", @incrdecramount " & _
              ", @remark " & _
              ", @newbalance " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @fundactivityadjustmentunkid" & _
            "); SELECT @@identity"
            'Sohail (22 Nov 2016) - [fundactivityadjustmentunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFundAdjustmentunkid = dsList.Tables(0).Rows(0).Item(0)
            Call GetData(objDataOperation)

            If InsertATFundAdjustmentTran(objDataOperation, enAuditType.ADD, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objFundProjectCode As New clsFundProjectCode
            objFundProjectCode._FundProjectCodeunkid = mintFundProjectCodeunkid
            objFundProjectCode._CurrentCeilingBalance = mdecNewBalance
            If objFundProjectCode.Update(dtCurrentDateTime, objDataOperation) = False Then
                If objFundProjectCode._Message <> "" Then
                    exForce = New Exception(objFundProjectCode._Message)
                    Throw exForce
                End If
            End If

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            'objDataOperation.ReleaseTransaction(True)
            If objDataOp Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Sohail (22 Nov 2016) -- End
            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then
            objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgfundadjustment_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateTime As DateTime, ByVal objDataOp As clsDataOperation) As Boolean
        'If isExist(mstrName, mintFundadjustmentunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        If isLastTransaction(mintFundProjectCodeunkid, mdtTransactionDate, mintFundAdjustmentunkid) = False Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.") & " " & mdtTransactionDate.ToShortDateString
            Return False
        End If
        'Sohail (22 Nov 2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (22 Nov 2016) -- End

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundAdjustmentunkid.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundSourceunkid.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactionDate <> Nothing, mdtTransactionDate, DBNull.Value))
            objDataOperation.AddParameter("@currentbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecCurrentBalance.ToString)
            objDataOperation.AddParameter("@incrdecramount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecIncrDecrAmount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@newbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecNewBalance.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundActivityAdjustmentunkid.ToString) 'Sohail (22 Nov 2016)

            strQ = "UPDATE bgfundadjustment_tran SET " & _
                      "  fundsourceunkid = @fundsourceunkid" & _
                      ", fundprojectcodeunkid = @fundprojectcodeunkid" & _
                      ", transactiondate = @transactiondate" & _
                      ", currentbalance = @currentbalance" & _
                      ", incrdecramount = @incrdecramount" & _
                      ", remark = @remark" & _
                      ", newbalance = @newbalance" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      ", fundactivityadjustmentunkid = @fundactivityadjustmentunkid " & _
                   "WHERE fundadjustmentunkid = @fundadjustmentunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdateExists("atbgfundadjustment_tran", "bgfundadjustment_tran", mintFundAdjustmentunkid, _
                                                      "fundadjustmentunkid", 2, objDataOperation) = True Then

                If InsertATFundAdjustmentTran(objDataOperation, enAuditType.EDIT, dtCurrentDateTime) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            Dim objFundProjectCode As New clsFundProjectCode
            objFundProjectCode._FundProjectCodeunkid = mintFundProjectCodeunkid
            objFundProjectCode._CurrentCeilingBalance = mdecNewBalance
            If objFundProjectCode.Update(dtCurrentDateTime, objDataOperation) = False Then
                If objFundProjectCode._Message <> "" Then
                    exForce = New Exception(objFundProjectCode._Message)
                    Throw exForce
                End If
            End If

            If objDataOp Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Sohail (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <param name="dtCurrentDateTime"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertFromActivityAdjustment(ByVal dtCurrentDateTime As DateTime) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        Try
            strQ = "SELECT  bgfundactivityadjustment_tran.fundactivityadjustmentunkid  " & _
                          ", bgfundactivityadjustment_tran.fundactivityunkid " & _
                          ", bgfundactivity_tran.fundprojectcodeunkid " & _
                          ", bgfundactivityadjustment_tran.transactiondate " & _
                          ", bgfundactivityadjustment_tran.incrdecramount " & _
                          ", bgfundactivityadjustment_tran.remark " & _
                          ", bgfundactivityadjustment_tran.userunkid " & _
                          ", bgfundactivityadjustment_tran.isvoid " & _
                          ", bgfundactivityadjustment_tran.voiduserunkid " & _
                          ", bgfundactivityadjustment_tran.voiddatetime " & _
                          ", bgfundactivityadjustment_tran.voidreason " & _
                    "FROM    bgfundactivityadjustment_tran " & _
                            "LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = bgfundactivityadjustment_tran.fundactivityunkid " & _
                    "WHERE   bgfundactivityadjustment_tran.isvoid = 0 " & _
                            "AND bgfundactivity_tran.isvoid = 0 " & _
                            "AND bgfundactivityadjustment_tran.fundactivityadjustmentunkid NOT IN ( " & _
                            "SELECT  fundactivityadjustmentunkid " & _
                            "FROM    bgfundadjustment_tran " & _
                                    "WHERE   bgfundadjustment_tran.isvoid = 0 ) " & _
                            "AND fundactivityadjustmentunkid IS NOT NULL  " & _
                            "AND bgfundactivity_tran.fundprojectcodeunkid > 0 " & _
                            "ORDER BY bgfundactivity_tran.fundprojectcodeunkid, bgfundactivityadjustment_tran.fundactivityadjustmentunkid "
            'Sohail (23 May 2017) - [AND bgfundactivity_tran.fundprojectcodeunkid > 0 ]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objProjectCode As clsFundProjectCode
            For Each dsRow As DataRow In dsList.Tables("List").Rows

                objProjectCode = New clsFundProjectCode
                objProjectCode._FundProjectCodeunkid = CInt(dsRow.Item("fundprojectcodeunkid"))

                mintFundSourceunkid = -1
                mintFundProjectCodeunkid = CInt(dsRow.Item("fundprojectcodeunkid"))
                mdtTransactionDate = DateAndTime.Now
                mdecCurrentBalance = objProjectCode._CurrentCeilingBalance
                mdecIncrDecrAmount = CDec(dsRow.Item("incrdecramount")) * -1
                mdecNewBalance = objProjectCode._CurrentCeilingBalance + (CDec(dsRow.Item("incrdecramount")) * -1)
                mstrRemark = dsRow.Item("remark")
                mintUserunkid = dsRow.Item("userunkid")
                mblnIsvoid = dsRow.Item("isvoid")
                mintVoiduserunkid = dsRow.Item("voiduserunkid")
                If IsDBNull(dsRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dsRow.Item("voiddatetime")
                End If
                mstrVoidreason = dsRow.Item("voidreason")
                mintFundActivityAdjustmentunkid = dsRow.Item("fundactivityadjustmentunkid")

                If Insert(dtCurrentDateTime, Nothing) = False Then
                    Return False
                End If
            Next

            'objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            'objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <param name="intFundProjectCodeunkid"></param>
    ''' <param name="dtTransactionDate"></param>
    ''' <param name="intFundAdjustmentUnkid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function isLastTransaction(ByVal intFundProjectCodeunkid As Integer, ByRef dtTransactionDate As Date, Optional ByVal intFundAdjustmentUnkid As Integer = 0) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT  bgfundadjustment_tran.fundadjustmentunkid  " & _
                          ", bgfundadjustment_tran.fundactivityadjustmentunkid " & _
                          ", bgfundadjustment_tran.transactiondate " & _
                    "FROM    bgfundadjustment_tran " & _
                    "WHERE   bgfundadjustment_tran.isvoid = 0 "

            If intFundProjectCodeunkid > 0 Then
                strQ &= " AND bgfundadjustment_tran.fundprojectcodeunkid = @fundprojectcodeunkid "
                objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundProjectCodeunkid)
            End If

            If dtTransactionDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), bgfundadjustment_tran.transactiondate, 112) > @transactiondate "
                objDataOperation.AddParameter("@transactiondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTransactionDate).ToString)
            End If

            If intFundAdjustmentUnkid > 0 Then 'For Edit Delete mode
                strQ &= " AND bgfundadjustment_tran.fundadjustmentunkid > @fundadjustmentunkid "
                objDataOperation.AddParameter("@fundadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundAdjustmentUnkid)
            End If

            strQ &= " ORDER BY bgfundadjustment_tran.transactiondate DESC "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                dtTransactionDate = dsList.Tables(0).Rows(0).Item("transactiondate")
            End If

            Return dsList.Tables(0).Rows.Count <= 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isLastTransaction; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetFundAdjustmentUnkId(ByVal intFundActivityAdjustmentUnkid As Integer) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intFundAdjustmentunkid As Integer = 0

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT  bgfundadjustment_tran.fundadjustmentunkid " & _
                    "FROM    bgfundadjustment_tran " & _
                    "WHERE   bgfundadjustment_tran.isvoid = 0 " & _
                            "AND bgfundadjustment_tran.fundactivityadjustmentunkid = @fundactivityadjustmentunkid "

            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundActivityAdjustmentUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intFundAdjustmentunkid = dsList.Tables(0).Rows(0).Item("fundadjustmentunkid")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isLastTransaction; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intFundAdjustmentunkid
    End Function
    'Sohail (22 Nov 2016) -- End

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgfundadjustment_tran) </purpose>
    Public Function Delete(ByVal intFundAdjustmentunkid As Integer, ByVal dtCurrentDateTime As DateTime, ByVal objDataOp As clsDataOperation) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (22 Nov 2016) -- End

        Try
            strQ = "UPDATE bgfundadjustment_tran SET " & _
                        "  isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                   "WHERE fundadjustmentunkid = @fundadjustmentunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundAdjustmentunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFundAdjustmentunkid = intFundAdjustmentunkid
            Call GetData(objDataOperation)

            If InsertATFundAdjustmentTran(objDataOperation, enAuditType.DELETE, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objFundProjectCode As New clsFundProjectCode

            dsList = GetLastCurrentBalance("List", mintFundProjectCodeunkid, objDataOperation)
            objFundProjectCode._FundProjectCodeunkid = mintFundProjectCodeunkid
            If dsList.Tables("List").Rows.Count > 0 Then
                objFundProjectCode._CurrentCeilingBalance = CDec(dsList.Tables("List").Rows(0).Item("newbalance"))
            Else
                objFundProjectCode._CurrentCeilingBalance = 0
            End If
            If objFundProjectCode.Update(dtCurrentDateTime, objDataOperation) = False Then
                If objFundProjectCode._Message <> "" Then
                    exForce = New Exception(objFundProjectCode._Message)
                    Throw exForce
                End If
                'Sohail (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
                If objDataOp Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                Return False
                'Sohail (22 Nov 2016) -- End
            End If

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            If mintFundActivityAdjustmentunkid > 0 Then
                Dim objFundActAdj As New clsFundActivityAdjustment_Tran
                objFundActAdj._Fundactivityadjustmentunkid = mintFundActivityAdjustmentunkid
                If objFundActAdj._Isvoid = False Then
                    objFundActAdj._Isvoid = True
                    objFundActAdj._Voiduserunkid = mintUserunkid
                    objFundActAdj._Voiddatetime = dtCurrentDateTime
                    objFundActAdj._Voidreason = mstrVoidreason
                    If objFundActAdj.Delete(mintFundActivityAdjustmentunkid, dtCurrentDateTime, objDataOperation) = False Then
                        If objFundProjectCode._Message <> "" Then
                            exForce = New Exception(objFundProjectCode._Message)
                            Throw exForce
                        End If
                        If objDataOp Is Nothing Then
                            objDataOperation.ReleaseTransaction(False)
                        End If
                        Return False
                    End If
            End If
            End If
            'Sohail (22 Nov 2016) -- End

            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@fundadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal intFundAdjustmentunkid As Integer = -1) As Boolean
        'Sohail (22 Nov 2016) - Public Function isExist(ByVal dtTransactionDate As Date, ByVal intFundProjectCodeunkid As Integer, Optional ByVal intFundAdjustmentunkid As Integer = -1) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  fundadjustmentunkid " & _
                      ", fundsourceunkid " & _
                      ", fundprojectcodeunkid " & _
                      ", transactiondate " & _
                      ", currentbalance " & _
                      ", incrdecramount " & _
                      ", remark " & _
                      ", newbalance " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                   "FROM bgfundadjustment_tran " & _
                   "WHERE isvoid = 0  "
            'Sohail (22 Nov 2016) - removed [AND CONVERT(CHAR(8),transactiondate,112) >= @transactiondate AND fundprojectcodeunkid = @fundprojectcodeunkid ]

            If intFundAdjustmentunkid > 0 Then
                strQ &= " AND fundadjustmentunkid <> @fundadjustmentunkid "
                'Sohail (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            Else
                strQ &= " AND 1 = 2 "
                'Sohail (22 Nov 2016) -- End
            End If

            objDataOperation.ClearParameters()
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            'objDataOperation.AddParameter("@transactiondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTransactionDate).ToString)
            'objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundProjectCodeunkid)
            'Sohail (22 Nov 2016) -- End
            objDataOperation.AddParameter("@fundadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundAdjustmentunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertATFundAdjustmentTran(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType, _
                                               ByVal dtCurrentDateTime As DateTime) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO atbgfundadjustment_tran ( " & _
                       "  fundadjustmentunkid " & _
                       ", fundsourceunkid " & _
                       ", fundprojectcodeunkid " & _
                       ", transactiondate " & _
                       ", currentbalance " & _
                       ", incrdecramount " & _
                       ", remark " & _
                       ", newbalance " & _
                       ", fundactivityadjustmentunkid " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name " & _
                       ", form_name " & _
                       ", module_name1 " & _
                       ", module_name2 " & _
                       ", module_name3 " & _
                       ", module_name4 " & _
                       ", module_name5 " & _
                       ", isweb" & _
                       ", loginemployeeunkid " & _
                   ") VALUES (" & _
                       "  @fundadjustmentunkid " & _
                       ", @fundsourceunkid " & _
                       ", @fundprojectcodeunkid " & _
                       ", @transactiondate " & _
                       ", @currentbalance " & _
                       ", @incrdecramount " & _
                       ", @remark " & _
                       ", @newbalance " & _
                       ", @fundactivityadjustmentunkid " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @form_name " & _
                       ", @module_name1 " & _
                       ", @module_name2 " & _
                       ", @module_name3 " & _
                       ", @module_name4 " & _
                       ", @module_name5 " & _
                       ", @isweb" & _
                       ", @loginemployeeunkid " & _
                   ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundAdjustmentunkid.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundSourceunkid.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactionDate <> Nothing, mdtTransactionDate.ToString, DBNull.Value))
            objDataOperation.AddParameter("@currentbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecCurrentBalance.ToString)
            objDataOperation.AddParameter("@incrdecramount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecIncrDecrAmount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@newbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecNewBalance.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundActivityAdjustmentunkid.ToString) 'Sohail (22 Nov 2016)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATFundAdjustmentTran , Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
        Return True
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")
			Language.setMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class