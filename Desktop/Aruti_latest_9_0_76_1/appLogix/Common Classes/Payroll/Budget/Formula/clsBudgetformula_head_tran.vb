﻿'************************************************************************************************************************************
'Class Name : clsBudgetformula_head_tran.vb
'Purpose    :
'Date       :21/06/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System


''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudgetformula_head_tran
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetformula_head_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetformulaheadtranunkid As Integer
    Private mintBudgetformulaunkid As Integer
    Private mdtEffective_Date As Date
    Private mintTranheadunkid As Integer
    Private mintDefaultValueId As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtFormula_Tranhead As DataTable = Nothing
    Private mstrWebFormName As String = ""
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetformulaheadtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetformulaheadtranunkid(ByVal objDataOperation As clsDataOperation) As Integer
        Get
            Return mintBudgetformulaheadtranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetformulaheadtranunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetformulaunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetformulaunkid() As Integer
        Get
            Return mintBudgetformulaunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetformulaunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effective_date
    ''' Modify By: sohail
    ''' </summary>
    Public Property _Effective_Date() As Date
        Get
            Return mdtEffective_Date
        End Get
        Set(ByVal value As Date)
            mdtEffective_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set defaultvalue_id
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Defaultvalue_id() As Integer
        Get
            Return mintDefaultValueId
        End Get
        Set(ByVal value As Integer)
            mintDefaultValueId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Formula_Tranhead() As DataTable
        Get
            Return mdtFormula_Tranhead
        End Get
        Set(ByVal value As DataTable)
            mdtFormula_Tranhead = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: sohail
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: sohail
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: sohail
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  budgetformulaheadtranunkid " & _
                      ", budgetformulaunkid " & _
                      ", Convert(char(8),effectivedate,112) AS effectivedate " & _
                      ", tranheadunkid " & _
                      ", defaultvalue_id " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM bgbudgetformula_head_tran " & _
                     "WHERE budgetformulaheadtranunkid = @budgetformulaheadtranunkid "



            objDataOperation.AddParameter("@budgetformulaheadtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaheadtranunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBudgetformulaheadtranunkid = CInt(dtRow.Item("budgetformulaheadtranunkid"))
                mintBudgetformulaunkid = CInt(dtRow.Item("budgetformulaunkid"))
                If IsDBNull(dtRow.Item("effectivedate")) = False Then mdtEffective_Date = eZeeDate.convertDate(dtRow.Item("effectivedate").ToString())
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintDefaultValueId = CInt(dtRow.Item("defaultvalue_id"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intFormulaUnkId As Integer = 0, Optional ByVal dtEffectiveDate As Date = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT  * " & _
                    "FROM    ( SELECT    bgbudgetformula_head_tran.budgetformulaheadtranunkid  " & _
                                      ", bgbudgetformula_head_tran.budgetformulaunkid " & _
                                      ", bgbudgetformula_head_tran.tranheadunkid " & _
                                      ", CONVERT(CHAR(8), bgbudgetformula_head_tran.effectivedate, 112) AS effectivedate " & _
                                      ", defaultvalue_id AS defaultid " & _
                                      ", '' AS [default] " & _
                                      ", bgbudgetformula_head_tran.userunkid " & _
                                      ", bgbudgetformula_head_tran.isvoid " & _
                                      ", bgbudgetformula_head_tran.voiduserunkid " & _
                                      ", bgbudgetformula_head_tran.voiddatetime " & _
                                      ", bgbudgetformula_head_tran.voidreason " & _
                                      ", '' AS AUD " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY bgbudgetformula_head_tran.budgetformulaunkid ORDER BY bgbudgetformula_head_tran.effectivedate DESC, bgbudgetformula_head_tran.budgetformulaheadtranunkid DESC ) AS ROWNO " & _
                     " FROM bgbudgetformula_head_tran " & _
                              "WHERE     isvoid = 0 "

            If intFormulaUnkId > 0 Then
                strQ &= " AND budgetformulaunkid = @budgetformulaunkid "
                objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormulaUnkId)
            End If

            If dtEffectiveDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), bgbudgetformula_head_tran.effectivedate, 112) <= @effectivedate "
                objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEffectiveDate))
            End If

            strQ &= "        ) AS A " & _
                    "WHERE   1 = 1 "

            If dtEffectiveDate <> Nothing Then
                strQ &= " AND A.ROWNO = 1 "
            End If

            strQ &= " ORDER by CONVERT(CHAR(8), A.effectivedate, 112) DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetformula_head_tran) </purpose>
    Public Function InsertDelete_TranHead(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            If mdtFormula_Tranhead Is Nothing OrElse mdtFormula_Tranhead.Rows.Count <= 0 Then Return True

            For i = 0 To mdtFormula_Tranhead.Rows.Count - 1
                With mdtFormula_Tranhead.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                        mdtEffective_Date = eZeeDate.convertDate(.Item("effectivedate").ToString()).Date
                        mintTranheadunkid = CInt(.Item("tranheadunkid").ToString)
                        mintDefaultValueId = CInt(.Item("defaultid").ToString)


                        Select Case .Item("AUD")

                            Case "A"

                                objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid.ToString)
                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(.Item("effectivedate").ToString()).Date)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("tranheadunkid").ToString))
                                objDataOperation.AddParameter("@defaultvalue_id", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("defaultid").ToString))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                strQ = "INSERT INTO bgbudgetformula_head_tran ( " & _
                                          "  budgetformulaunkid " & _
                                          ", effectivedate " & _
                                          ", tranheadunkid " & _
                                          ", defaultvalue_id  " & _
                                          ", userunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                          ", voiddatetime " & _
                                          ", voidreason" & _
                                        ") VALUES (" & _
                                          "  @budgetformulaunkid " & _
                                          ", @effectivedate " & _
                                          ", @tranheadunkid " & _
                                          ", @defaultvalue_id  " & _
                                          ", @userunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          ", @voiddatetime " & _
                                          ", @voidreason" & _
                                        "); SELECT @@identity"

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                _Budgetformulaheadtranunkid(objDataOperation) = dsList.Tables(0).Rows(0).Item(0)

                                If ATInsertBudgetFormula_tranhead(objDataOperation, 1) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "U"


                                strQ = " UPDATE bgbudgetformula_head_tran SET " & _
                                          " effectivedate = @effectivedate" & _
                                          ",tranheadunkid = @tranheadunkid " & _
                                          ", defaultvalue_id = @defaultvalue_id " & _
                                         " WHERE budgetformulaunkid = @budgetformulaunkid "

                                objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid.ToString)
                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(.Item("effectivedate").ToString()).Date)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("tranheadunkid").ToString))
                                objDataOperation.AddParameter("@defaultvalue_id", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("defaultid").ToString))
                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                _Budgetformulaheadtranunkid(objDataOperation) = CInt(.Item("budgetformulaheadtranunkid"))

                                If ATInsertBudgetFormula_tranhead(objDataOperation, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "D"

                                strQ = "UPDATE bgbudgetformula_head_tran SET " & _
                                         "  isvoid = 1" & _
                                         ", voiduserunkid = @voiduserunkid" & _
                                         ", voiddatetime = GetDate()" & _
                                         ", voidreason = @voidreason " & _
                                         "WHERE budgetformulaunkid = @budgetformulaunkid "

                                objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If ATInsertBudgetFormula_tranhead(objDataOperation, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select

                    End If

                End With

            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete_TranHead; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@budgetformulaheadtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudgetformula_master) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "SELECT ISNULL(budgetformulaheadtranunkid,0) AS budgetformulaheadtranunkid  FROM bgbudgetformula_head_tran WHERE isvoid = 0 AND budgetformulaunkid = @budgetformulaunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            strQ = "UPDATE bgbudgetformula_head_tran SET " & _
                       "  isvoid = 1" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = GetDate()" & _
                       ", voidreason = @voidreason " & _
                       "WHERE budgetformulaunkid = @budgetformulaunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    _Budgetformulaheadtranunkid(objDataOperation) = CInt(dsList.Tables(0).Rows(i)("budgetformulaheadtranunkid"))
                    If ATInsertBudgetFormula_tranhead(objDataOperation, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  budgetformulaheadtranunkid " & _
              ", budgetformulaunkid " & _
              ", tranheadunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetformula_head_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND budgetformulaheadtranunkid <> @budgetformulaheadtranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@budgetformulaheadtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function ATInsertBudgetFormula_tranhead(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atbgbudgetformula_head_tran ( " & _
                     " budgetformulaunkid " & _
                     ", effectivedate " & _
                     ", tranheadunkid " & _
                     ", defaultvalue_id " & _
                     ", AuditType " & _
                     ", audituserunkid " & _
                     ", auditdatetime " & _
                     ", ip " & _
                     ", machine_name " & _
                     ", form_name " & _
                     ", module_name1 " & _
                     ", module_name2 " & _
                     ", module_name3 " & _
                     ", module_name4 " & _
                     ", module_name5 " & _
                     ", isweb " & _
               ") VALUES (" & _
                    "  @budgetformulaunkid " & _
                    ", @effectivedate " & _
                    ", @tranheadunkid " & _
                    ", @defaultvalue_id " & _
                    ", @AuditType " & _
                    ", @audituserunkid " & _
                    ", Getdate() " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @form_name " & _
                    ", @module_name1 " & _
                    ", @module_name2 " & _
                    ", @module_name3 " & _
                    ", @module_name4 " & _
                    ", @module_name5 " & _
                    ", @isweb " & _
               "); "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date.Date)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@defaultvalue_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDefaultValueId.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertBudgetFormula_tranhead; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class