﻿'************************************************************************************************************************************
'Class Name : clsBudget_Master.vb
'Purpose    :
'Date       :28/07/2010
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudget_Master
    Private Shared ReadOnly mstrModuleName As String = "clsBudget_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetunkid As Integer
    Private mintPayyearunkid As Integer
    Private mintPayperiodunkid As Integer
    Private mintAllocationbyunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetunkid() As Integer
        Get
            Return mintBudgetunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set payyearunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Payyearunkid() As Integer
        Get
            Return mintPayyearunkid
        End Get
        Set(ByVal value As Integer)
            mintPayyearunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set payperiodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Payperiodunkid() As Integer
        Get
            Return mintPayperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintPayperiodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationbyunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Allocationbyunkid() As Integer
        Get
            Return mintAllocationbyunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationbyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END


#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetunkid " & _
              ", payyearunkid " & _
              ", payperiodunkid " & _
              ", allocationbyunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bdgbudget_master " & _
             "WHERE budgetunkid = @budgetunkid " 'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBudgetUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbudgetunkid = CInt(dtRow.Item("budgetunkid"))
                mintpayyearunkid = CInt(dtRow.Item("payyearunkid"))
                mintpayperiodunkid = CInt(dtRow.Item("payperiodunkid"))
                mintallocationbyunkid = CInt(dtRow.Item("allocationbyunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtvoiddatetime = dtRow.Item("voiddatetime")
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal intModuleRefid As Integer, ByVal intAllocationByInkID As Integer) As DataSet 'Sohail (28 Dec 2010)
        ' Public Function GetList(ByVal strTableName As String, ByVal intModuleRefid As Integer) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  bdgbudget_master.budgetunkid " & _
                      ", bdgbudget_master.payyearunkid " & _
                      ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name as year " & _
                      ", bdgbudget_master.payperiodunkid " & _
                      ", cfcommon_period_tran.period_name " & _
                      ", convert(char(8),cfcommon_period_tran.start_date,112) as start_date " & _
                      ", convert(char(8),cfcommon_period_tran.end_date,112) as end_date " & _
                      ", bdgbudget_master.allocationbyunkid " & _
                      ", bdgbudget_master.userunkid " & _
                      ", bdgbudget_master.isvoid " & _
                      ", bdgbudget_master.voiduserunkid " & _
                      ", bdgbudget_master.voiddatetime " & _
                      ", bdgbudget_master.voidreason " & _
                "FROM bdgbudget_master " & _
                "LEFT JOIN cfcommon_period_tran ON bdgbudget_master.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON bdgbudget_master.payyearunkid = cffinancial_year_tran.yearunkid " & _
                "WHERE ISNULL(bdgbudget_master.isvoid,0) = 0 " & _
                "AND cfcommon_period_tran.modulerefid = @modulerefid " & _
                "AND bdgbudget_master.allocationbyunkid = @allocationbyunkid " 'Sohail (28 Dec 2010)


            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            objDataOperation.AddParameter("@allocationbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationByInkID) 'Sohail (28 Dec 2010)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetBudgetID(ByVal intPeriodID As Integer, ByVal intAllocationID As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  budgetunkid " & _
              ", payyearunkid " & _
              ", payperiodunkid " & _
              ", allocationbyunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bdgbudget_master " & _
             "WHERE payperiodunkid = @payperiodunkid " & _
             "AND allocationbyunkid = @allocationbyunkid " & _
             "AND ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID.ToString)
            objDataOperation.AddParameter("@allocationbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0).Item("budgetunkid").ToString)
            Else
                Return 0
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBudgetID; Module Name: " & mstrModuleName)
            Return 0
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertData(ByVal intPayYearID As Integer, ByVal intPeriodID As Integer, ByVal intAllocationID As Integer, ByVal dtTable As DataTable, ByVal intUserunkid As Integer) As Boolean
        'Sohail (21 Aug 2015) - [intUserunkid]
        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objBudget_Tran As New clsBudget_Tran

        Dim intBudgetID As Integer = 0
        Dim blnFlag As Boolean = False

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            intBudgetID = GetBudgetID(intPeriodID, intAllocationID)

            If intBudgetID > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = Void(intBudgetID, User._Object._Userunkid, Now, "") 'Sohail (14 Oct 2010)
                blnFlag = Void(intBudgetID, intUserunkid, Now, "")
                'Sohail (21 Aug 2015) -- End
                If blnFlag = False Then
                    Return False
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objBudget_Tran.Void(intBudgetID, User._Object._Userunkid, Now, "") 'Sohail (16 Oct 2010)
                blnFlag = objBudget_Tran.Void(intBudgetID, intUserunkid, Now, "")
                'Sohail (21 Aug 2015) -- End
                If blnFlag = False Then
                    Return False
                End If
            End If

            mintPayyearunkid = intPayYearID
            mintPayperiodunkid = intPeriodID
            mintAllocationbyunkid = intAllocationID
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'mintUserunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
            mintUserunkid = intUserunkid
            'Sohail (21 Aug 2015) -- End

            mintBudgetunkid = -1

            blnFlag = Insert()
            If blnFlag = False Then
                Return False
            End If

            If mintBudgetunkid > 0 Then
                blnFlag = objBudget_Tran.Insert(mintBudgetunkid, intPeriodID, dtTable)
            Else
                blnFlag = False
            End If

            If blnFlag = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertData; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            objBudget_Tran = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bdgbudget_master) </purpose>
    Private Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@payyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayyearunkid.ToString)
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayperiodunkid.ToString)
            objDataOperation.AddParameter("@allocationbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bdgbudget_master ( " & _
              "  payyearunkid " & _
              ", payperiodunkid " & _
              ", allocationbyunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @payyearunkid " & _
              ", @payperiodunkid " & _
              ", @allocationbyunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (11 Nov 2010) -- Start


            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForBudgetMaster(objDataOperation, 1)
            If InsertAuditTrailForBudgetMaster(objDataOperation, 1) = False Then
                Return False
            End If
            'Anjan (11 Jun 2011)-End 

            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Database Table (bdgbudget_master) </purpose>
    Public Function Void(ByVal intBudgetID As Integer, _
                         ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'strQ = "DELETE FROM bdgbudget_master " & _
            '"WHERE budgetunkid = @budgetunkid "
            strQ = "UPDATE bdgbudget_master SET " & _
                         " isvoid = 1" & _
                         ", voiduserunkid = @voiduserunkid" & _
                         ", voiddatetime = @voiddatetime" & _
                         ", voidreason = @voidreason " & _
                "WHERE budgetunkid = @budgetunkid " & _
                "AND ISNULL(isvoid,0) = 0" 'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetID.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (11 Nov 2010) -- Start
            Me._Budgetunkid = intBudgetID

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForBudgetMaster(objDataOperation, 3)
            If InsertAuditTrailForBudgetMaster(objDataOperation, 3) = False Then
                Return False
            End If
            'Anjan (11 Jun 2011)-End 


            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (11 Nov 2010) -- Start

    'Anjan (15 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForBudgetMaster(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForBudgetMaster(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        'Anjan (15 Jun 2011)-End 

        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atbdgbudget_master ( " & _
            '      "  budgetunkid " & _
            '      ", payyearunkid " & _
            '      ", payperiodunkid " & _
            '      ", allocationbyunkid " & _
            '      ", audittype " & _
            '      ", audituserunkid " & _
            '      ", auditdatetime " & _
            '      ", ip " & _
            '      ", machine_name" & _
            '") VALUES (" & _
            '      "  @budgetunkid " & _
            '      ", @payyearunkid " & _
            '      ", @payperiodunkid " & _
            '      ", @allocationbyunkid " & _
            '      ", @audittype " & _
            '      ", @audituserunkid " & _
            '      ", @auditdatetime " & _
            '      ", @ip " & _
            '      ", @machine_name" & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO atbdgbudget_master ( " & _
                  "  budgetunkid " & _
                  ", payyearunkid " & _
                  ", payperiodunkid " & _
                  ", allocationbyunkid " & _
                  ", audittype " & _
                  ", audituserunkid " & _
                  ", auditdatetime " & _
                  ", ip " & _
                  ", machine_name" & _
               ", form_name " & _
               ", module_name1 " & _
               ", module_name2 " & _
               ", module_name3 " & _
               ", module_name4 " & _
               ", module_name5 " & _
               ", isweb " & _
            ") VALUES (" & _
                  "  @budgetunkid " & _
                  ", @payyearunkid " & _
                  ", @payperiodunkid " & _
                  ", @allocationbyunkid " & _
                  ", @audittype " & _
                  ", @audituserunkid " & _
                  ", @auditdatetime " & _
                  ", @ip " & _
                  ", @machine_name" & _
               ", @form_name " & _
               ", @module_name1 " & _
               ", @module_name2 " & _
               ", @module_name3 " & _
               ", @module_name4 " & _
               ", @module_name5 " & _
               ", @isweb " & _
            "); SELECT @@identity"

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@payyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayyearunkid.ToString)
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayperiodunkid.ToString)
            objDataOperation.AddParameter("@allocationbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForBudgetMaster", mstrModuleName)
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End 

        Finally
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Nov 2010) -- End



    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (bdgbudget_master) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintBudgetunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@budgetunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbudgetunkid.ToString)
    '        objDataOperation.AddParameter("@payyearunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpayyearunkid.ToString)
    '        objDataOperation.AddParameter("@payperiodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpayperiodunkid.ToString)
    '        objDataOperation.AddParameter("@allocationbyunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintallocationbyunkid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

    '        StrQ = "UPDATE bdgbudget_master SET " & _
    '          "  payyearunkid = @payyearunkid" & _
    '          ", payperiodunkid = @payperiodunkid" & _
    '          ", allocationbyunkid = @allocationbyunkid" & _
    '          ", userunkid = @userunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason " & _
    '        "WHERE budgetunkid = @budgetunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  budgetunkid " & _
    '          ", payyearunkid " & _
    '          ", payperiodunkid " & _
    '          ", allocationbyunkid " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM bdgbudget_master " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND budgetunkid <> @budgetunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class