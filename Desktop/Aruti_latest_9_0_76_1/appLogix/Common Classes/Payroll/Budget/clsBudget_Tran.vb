﻿'************************************************************************************************************************************
'Class Name : clsBudget_Tran.vb
'Purpose    :
'Date       :28/07/2010
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudget_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsBudget_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgettranunkid As Integer
    Private mintBudgetunkid As Integer
    Private mintAllocationbyunkid As Integer
    Private mintTranheadunkid As Integer
    Private mdecAmount As Decimal 'Sohail (11 May 2011)
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgettranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgettranunkid() As Integer
        Get
            Return mintBudgettranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgettranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetunkid() As Integer
        Get
            Return mintBudgetunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationbyunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Allocationbyunkid() As Integer
        Get
            Return mintAllocationbyunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationbyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  budgettranunkid " & _
              ", budgetunkid " & _
              ", allocationbyunkid " & _
              ", tranheadunkid " & _
              ", amount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bdgbudget_tran " & _
             "WHERE budgettranunkid = @budgettranunkid "

            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBudgetTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbudgettranunkid = CInt(dtRow.Item("budgettranunkid"))
                mintbudgetunkid = CInt(dtRow.Item("budgetunkid"))
                mintallocationbyunkid = CInt(dtRow.Item("allocationbyunkid"))
                minttranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mdecAmount = CDec(dtRow.Item("amount")) 'Sohail (11 May 2011)
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtvoiddatetime = dtRow.Item("voiddatetime")
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal intBudgetID As Integer = 0, _
                            Optional ByVal intAllocationByID As Integer = -1, _
                            Optional ByVal intTranHeadID As Integer = 0) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgettranunkid " & _
              ", budgetunkid " & _
              ", allocationbyunkid " & _
              ", tranheadunkid " & _
              ", amount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bdgbudget_tran " & _
             "WHERE ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010)

            If intBudgetID > 0 Then
                strQ &= "AND budgetunkid = @budgetunkid "
                objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetID.ToString)
            End If
            If intAllocationByID >= 0 Then
                strQ &= "AND allocationbyunkid = @allocationbyunkid "
                objDataOperation.AddParameter("@allocationbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationByID.ToString)
            End If
            If intTranHeadID > 0 Then
                strQ &= "AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadID.ToString)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function GetDataGridList(ByVal intAllocationID As Integer, ByVal strColumn As String, ByVal strTranHeadList As String, ByVal strTableName As String) As DataSet
    Public Function GetDataGridList(ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As DateTime _
                                    , ByVal xPeriodEnd As DateTime _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , ByVal intAllocationID As Integer _
                                    , ByVal strColumn As String _
                                    , ByVal strTranHeadList As String _
                                    , ByVal strTableName As String _
                                    , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                    , Optional ByVal strFilerString As String = "" _
                                    ) As DataSet
        'Sohail (21 Aug 2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        'Sohail (21 Aug 2015) -- End

        objDataOperation = New clsDataOperation


        Try
            Select Case intAllocationID
                Case 5 'COST CENTER
                    'Sohail (11 Sep 2010) -- Start
                    'Changes : Get costcenter from employeemaster If Costcenter is not defined for any employee transactionhead on employee costcenter 
                    'strQ = "SELECT  ISNULL(premployee_costcenter_tran.costcenterunkid, 0) AS colhAllocationID " & _
                    '        ", ISNULL(prcostcenter_master.costcentername, 'Undefined') AS colhAllocation " & _
                    '        ", prtranhead_master.tranheadunkid AS colhTranHeadID " & _
                    '        ", prtranhead_master.trnheadname AS colhTranHead " & _
                    '        ", '' AS colhTranHeadType " & _
                    '        strColumn & " " & _
                    '"FROM    prtranhead_master " & _
                    '        "LEFT JOIN premployee_costcenter_tran ON premployee_costcenter_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    '        "LEFT JOIN prcostcenter_master ON premployee_costcenter_tran.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                    '"WHERE   prtranhead_master.isvoid = 0 " & _
                    '        "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                    '        "AND ( premployee_costcenter_tran.isvoid IS NULL " & _
                    '              "OR premployee_costcenter_tran.isvoid = 0 " & _
                    '            ") " & _
                    '"GROUP BY premployee_costcenter_tran.costcenterunkid " & _
                    '        ", prcostcenter_master.costcentername " & _
                    '        ", prtranhead_master.tranheadunkid " & _
                    '        ", prtranhead_master.trnheadname "
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'strQ = "SELECT " & _
                    '        "DISTINCT " & _
                    '              "  ISNULL(TableSummery.CostCenterID, 0) AS colhAllocationID " & _
                    '              ", ISNULL(TableSummery.CostCentername, 'Undefined') AS colhAllocation " & _
                    '              ", TableSummery.tranheadunkid AS colhTranHeadID " & _
                    '              ", TableSummery.trnheadname AS colhTranHead " & _
                    '        ", '' AS colhTranHeadType " & _
                    '        strColumn & " " & _
                    '        "FROM    ( SELECT    prearningdeduction_master.employeeunkid " & _
                    '                          ", prearningdeduction_master.tranheadunkid " & _
                    '                          ", prtranhead_master.trnheadname " & _
                    '                          ", ISNULL(premployee_costcenter_tran.costcenterunkid, " & _
                    '                                   "TableEmployee.costcenterunkid) AS CostCenterID " & _
                    '                          ", ISNULL(prcostcenter_master.costcentername, " & _
                    '                                   "TableEmployee.EmpCostCenter) AS CostCenterName " & _
                    '                  "FROM      prearningdeduction_master " & _
                    '                            "LEFT JOIN premployee_costcenter_tran ON prearningdeduction_master.employeeunkid = premployee_costcenter_tran.employeeunkid " & _
                    '                                                                      "AND prearningdeduction_master.tranheadunkid = premployee_costcenter_tran.tranheadunkid " & _
                    '                            "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = premployee_costcenter_tran.costcenterunkid " & _
                    '                            "LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    '                            "LEFT JOIN ( SELECT  hremployee_master.employeeunkid " & _
                    '                                              ", hremployee_master.costcenterunkid " & _
                    '                                              ", ISNULL(prcostcenter_master.costcentername, " & _
                    '                                                       "'Undefined') AS EmpCostCenter " & _
                    '                                        "FROM    hremployee_master " & _
                    '                                                "LEFT JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid "
                    strQ = "SELECT " & _
                            "DISTINCT " & _
                                  "  ISNULL(TableSummery.CostCenterID, 0) AS colhAllocationID " & _
                                  ", ISNULL(TableSummery.CostCentername, 'Undefined') AS colhAllocation " & _
                                  ", TableSummery.tranheadunkid AS colhTranHeadID " & _
                                  ", TableSummery.trnheadname AS colhTranHead " & _
                            ", '' AS colhTranHeadType " & _
                            strColumn & " " & _
                            "FROM    ( SELECT    prearningdeduction_master.employeeunkid " & _
                                              ", prearningdeduction_master.tranheadunkid " & _
                                              ", prtranhead_master.trnheadname " & _
                                              ", ISNULL(premployee_costcenter_tran.costcenterunkid, " & _
                                                       "TableEmployee.costcenterunkid) AS CostCenterID " & _
                                              ", ISNULL(prcostcenter_master.costcentername, " & _
                                                       "TableEmployee.EmpCostCenter) AS CostCenterName " & _
                                      "FROM      prearningdeduction_master " & _
                                                "LEFT JOIN premployee_costcenter_tran ON prearningdeduction_master.employeeunkid = premployee_costcenter_tran.employeeunkid " & _
                                                                                          "AND prearningdeduction_master.tranheadunkid = premployee_costcenter_tran.tranheadunkid " & _
                                                "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = premployee_costcenter_tran.costcenterunkid " & _
                                                "LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "LEFT JOIN ( SELECT  hremployee_master.employeeunkid " & _
                                                                  ", hremployee_master.costcenterunkid " & _
                                                                  ", ISNULL(prcostcenter_master.costcentername, " & _
                                                                           "'Undefined') AS EmpCostCenter " & _
                                                            "FROM    hremployee_master " & _
                                                                    "LEFT JOIN ( " & _
                                                                    "   SELECT " & _
                                                                    "		 CCT.CCTEmpId " & _
                                                                    "		,CCT.costcenterunkid " & _
                                                                    "		,CCT.CTEfDt " & _
                                                                    "		,CC_REASON " & _
                                                                    "   FROM " & _
                                                                    "   ( " & _
                                                                    "       SELECT " & _
                                                                    "            CCT.employeeunkid AS CCTEmpId " & _
                                                                    "           ,CCT.cctranheadvalueid AS costcenterunkid " & _
                                                                    "           ,CONVERT(CHAR(8),CCT.effectivedate,112) AS CTEfDt " & _
                                                                    "           ,ROW_NUMBER()OVER(PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC) AS Rno " & _
                                                                    "			,CASE WHEN CCT.rehiretranunkid > 0 THEN ISNULL(RCS.name,'') ELSE ISNULL(CTC.name,'') END AS CC_REASON " & _
                                                                    "       FROM hremployee_cctranhead_tran AS CCT " & _
                                                                    "			LEFT JOIN cfcommon_master AS RCS ON RCS.masterunkid = CCT.changereasonunkid AND RCS.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                                                    "			LEFT JOIN cfcommon_master AS CTC ON CTC.masterunkid = CCT.changereasonunkid AND CTC.mastertype = '" & clsCommon_Master.enCommonMaster.COST_CENTER & "' " & _
                                                                    "		WHERE CCT.isvoid = 0 AND CCT.istransactionhead = 0 AND CONVERT(CHAR(8),CCT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                                                    "   )CCT WHERE CCT.Rno = 1 " & _
                                                                    ")AS ECCT ON ECCT.CCTEmpId = hremployee_master.employeeunkid AND ECCT.CTEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                                                                    "LEFT JOIN prcostcenter_master ON ECCT.costcenterunkid = prcostcenter_master.costcenterunkid AND prcostcenter_master.isactive = 1 "
                    'Sohail (21 Aug 2015) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACQry.Trim.Length > 0 Then
                    '    strQ &= xUACQry
                    'End If
                    If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END



                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If
                    'Sohail (21 Aug 2015) -- End

                    strQ &= "                               WHERE   hremployee_master.isactive = 1 "

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    If blnApplyUserAccessFilter = True Then
                        If xUACFiltrQry.Trim.Length > 0 Then
                            strQ &= " AND " & xUACFiltrQry
                        End If
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry
                        End If
                    End If

                    If strFilerString.Trim.Length > 0 Then
                        strQ &= " AND " & strFilerString
                    End If
                    'Sohail (21 Aug 2015) -- End

                    strQ &= "           ) AS TableEmployee ON prearningdeduction_master.employeeunkid = TableEmployee.employeeunkid " & _
                                      "WHERE     ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
                            "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                    ") AS TableSummery " 'Sohail (16 Oct 2010)
                    'Sohail (11 Sep 2010) -- End
                Case 11 'STATION
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'strQ = "SELECT  ISNULL(hrstation_master.stationunkid, 0) AS colhAllocationID " & _
                    '        ", ISNULL(hrstation_master.name, 'Undefined') AS colhAllocation " & _
                    '        ", prtranhead_master.tranheadunkid AS colhTranHeadID " & _
                    '        ", prtranhead_master.trnheadname AS colhTranHead " & _
                    '        ", '' AS colhTranHeadType " & _
                    '        strColumn & " " & _
                    '"FROM    prtranhead_master " & _
                    '        "LEFT JOIN prearningdeduction_master ON prtranhead_master.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                    '        "LEFT JOIN hremployee_master ON prearningdeduction_master.employeeunkid = hremployee_master.employeeunkid " & _
                    '        "LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid "
                    strQ = "SELECT  ISNULL(hrstation_master.stationunkid, 0) AS colhAllocationID " & _
                            ", ISNULL(hrstation_master.name, 'Undefined') AS colhAllocation " & _
                            ", prtranhead_master.tranheadunkid AS colhTranHeadID " & _
                            ", prtranhead_master.trnheadname AS colhTranHead " & _
                            ", '' AS colhTranHeadType " & _
                            strColumn & " " & _
                    "FROM    prtranhead_master " & _
                            "LEFT JOIN prearningdeduction_master ON prtranhead_master.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                            "LEFT JOIN hremployee_master ON prearningdeduction_master.employeeunkid = hremployee_master.employeeunkid " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "	SELECT " & _
                            "        Trf.TrfEmpId " & _
                            "       ,ISNULL(Trf.stationunkid,0) AS stationunkid " & _
                            "       ,ISNULL(Trf.deptgroupunkid,0) AS deptgroupunkid " & _
                            "       ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
                            "       ,ISNULL(Trf.sectiongroupunkid,0) AS sectiongroupunkid " & _
                            "       ,ISNULL(Trf.sectionunkid,0) AS sectionunkid " & _
                            "       ,ISNULL(Trf.unitgroupunkid,0) AS unitgroupunkid " & _
                            "       ,ISNULL(Trf.unitunkid,0) AS unitunkid " & _
                            "       ,ISNULL(Trf.teamunkid,0) AS teamunkid " & _
                            "       ,ISNULL(Trf.classgroupunkid,0) AS classgroupunkid " & _
                            "       ,ISNULL(Trf.classunkid,0) AS classunkid " & _
                            "       ,ISNULL(Trf.EfDt,'') AS EfDt " & _
                            "       ,Trf.ETT_REASON " & _
                            "   FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            ETT.employeeunkid AS TrfEmpId " & _
                            "           ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
                            "           ,ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
                            "           ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                            "           ,ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                            "           ,ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
                            "           ,ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                            "           ,ISNULL(ETT.unitunkid,0) AS unitunkid " & _
                            "           ,ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                            "           ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
                            "           ,ISNULL(ETT.classunkid,0) AS classunkid " & _
                            "           ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                            "           ,CASE WHEN ETT.rehiretranunkid > 0 THEN RTT.name ELSE TTC.name END AS ETT_REASON " & _
                            "       FROM hremployee_transfer_tran AS ETT " & _
                            "           LEFT JOIN cfcommon_master AS RTT ON RTT.masterunkid = ETT.changereasonunkid AND RTT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                            "           LEFT JOIN cfcommon_master AS TTC ON TTC.masterunkid = ETT.changereasonunkid AND TTC.mastertype = '" & clsCommon_Master.enCommonMaster.TRANSFERS & "' " & _
                            "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            "   ) AS Trf WHERE Trf.Rno = 1 " & _
                            ") AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.EfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                            "LEFT JOIN hrstation_master ON ETRF.stationunkid = hrstation_master.stationunkid "
                    'Sohail (21 Aug 2015) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If


                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACQry.Trim.Length > 0 Then
                    '    strQ &= xUACQry
                    'End If
                    If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If
                    'Sohail (21 Aug 2015) -- End

                    strQ &= "WHERE   ISNULL(prtranhead_master.isvoid,0) = 0 " & _
                            "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                            "AND ( prearningdeduction_master.isvoid IS NULL " & _
                                  "OR ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
                                ")" & _
                            "AND ( hrstation_master.isactive IS NULL " & _
                                  "OR hrstation_master.isactive = 1 " & _
                                ") "

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    If blnApplyUserAccessFilter = True Then
                        If xUACFiltrQry.Trim.Length > 0 Then
                            strQ &= " AND " & xUACFiltrQry
                        End If
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry
                        End If
                    End If

                    If strFilerString.Trim.Length > 0 Then
                        strQ &= " AND " & strFilerString
                    End If
                    'Sohail (21 Aug 2015) -- End

                    strQ &= "GROUP BY hrstation_master.stationunkid " & _
                            ", hrstation_master.name " & _
                            ", prtranhead_master.tranheadunkid " & _
                            ", prtranhead_master.trnheadname " 'Sohail (16 Oct 2010)

            End Select


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataGridList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bdgbudget_tran) </purpose>
    Public Function Insert(ByVal intBudgetID As Integer, ByVal intPerodID As Integer, ByVal dtTable As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            mintBudgetunkid = intBudgetID

            For Each dtRow As DataRow In dtTable.Rows

                objDataOperation.ClearParameters()

                If CBool(dtRow.Item("objisgroup").ToString) = False Then

                    mintAllocationbyunkid = CInt(dtRow.Item("colhAllocationID").ToString)
                    mintTranheadunkid = CInt(dtRow.Item("colhTranHeadID").ToString)
                    mdecAmount = CDec(dtRow.Item("colh" & intPerodID.ToString).ToString) 'Sohail (11 May 2011)

                    objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
                    objDataOperation.AddParameter("@allocationbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyunkid.ToString)
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString) 'Sohail (16 Oct 2010)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    If mdtVoiddatetime = Nothing Then
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    Else
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    End If
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                    strQ = "INSERT INTO bdgbudget_tran ( " & _
                      "  budgetunkid " & _
                      ", allocationbyunkid " & _
                      ", tranheadunkid " & _
                      ", amount " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @budgetunkid " & _
                      ", @allocationbyunkid " & _
                      ", @tranheadunkid " & _
                      ", @amount " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintBudgettranunkid = dsList.Tables(0).Rows(0).Item(0)

                    'Sohail (11 Nov 2010) -- Start

                    'Anjan (11 Jun 2011)-Start
                    'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
                    'Call InsertAuditTrailForBudgetTran(objDataOperation, 1)
                    If InsertAuditTrailForBudgetTran(objDataOperation, 1) = False Then
                        Return False
                    End If
                    'Anjan (11 Jun 2011)-End 


                    'Sohail (11 Nov 2010) -- End
                End If
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Database Table (bdgbudget_tran) </purpose>
    Public Function Void(ByVal intBudgetID As Integer, _
                         ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception



        Try
            'Sohail (11 Nov 2010) -- Start
            dsList = GetList("List", intBudgetID)
            'Sohail (11 Nov 2010) -- End

            objDataOperation = New clsDataOperation

            'strQ = "DELETE FROM bdgbudget_tran " & _
            '"WHERE budgettranunkid = @budgettranunkid "
            strQ = "UPDATE bdgbudget_tran SET " & _
                  " isvoid = 1" & _
                  ", voiduserunkid = @voiduserunkid" & _
                  ", voiddatetime = @voiddatetime" & _
                  ", voidreason = @voidreason " & _
            "WHERE budgetunkid = @budgetunkid " & _
            "AND ISNULL(isvoid,0) = 0" 'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetID.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (11 Nov 2010) -- Start
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                Me._Budgettranunkid = CInt(dtRow.Item("budgettranunkid").ToString)

                'Anjan (11 Jun 2011)-Start
                'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
                'Call InsertAuditTrailForBudgetTran(objDataOperation, 3)
                If InsertAuditTrailForBudgetTran(objDataOperation, 3) = False Then
                    Return False
                End If
                'Anjan (11 Jun 2011)-End 
            Next
            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Sohail (11 Nov 2010) -- Start

    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForBudgetTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForBudgetTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        'Anjan (11 Jun 2011)-End 


        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atbdgbudget_tran ( " & _
            '      "  budgettranunkid " & _
            '      ", budgetunkid " & _
            '      ", allocationbyunkid " & _
            '      ", tranheadunkid " & _
            '      ", amount " & _
            '      ", audittype " & _
            '      ", audituserunkid " & _
            '      ", auditdatetime " & _
            '      ", ip " & _
            '      ", machine_name" & _
            '") VALUES (" & _
            '      "  @budgettranunkid " & _
            '      ", @budgetunkid " & _
            '      ", @allocationbyunkid " & _
            '      ", @tranheadunkid " & _
            '      ", @amount " & _
            '      ", @audittype " & _
            '      ", @audituserunkid " & _
            '      ", @auditdatetime " & _
            '      ", @ip " & _
            '      ", @machine_name" & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO atbdgbudget_tran ( " & _
                  "  budgettranunkid " & _
                  ", budgetunkid " & _
                  ", allocationbyunkid " & _
                  ", tranheadunkid " & _
                  ", amount " & _
                  ", audittype " & _
                  ", audituserunkid " & _
                  ", auditdatetime " & _
                  ", ip " & _
                  ", machine_name" & _
                ", form_name " & _
                ", module_name1 " & _
                ", module_name2 " & _
                ", module_name3 " & _
                ", module_name4 " & _
                ", module_name5 " & _
                ", isweb " & _
            ") VALUES (" & _
                  "  @budgettranunkid " & _
                  ", @budgetunkid " & _
                  ", @allocationbyunkid " & _
                  ", @tranheadunkid " & _
                  ", @amount " & _
                  ", @audittype " & _
                  ", @audituserunkid " & _
                  ", @auditdatetime " & _
                  ", @ip " & _
                  ", @machine_name" & _
                ", @form_name " & _
                ", @module_name1 " & _
                ", @module_name2 " & _
                ", @module_name3 " & _
                ", @module_name4 " & _
                ", @module_name5 " & _
                ", @isweb " & _
            "); SELECT @@identity"

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@allocationbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForBudgetTran", mstrModuleName)

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End 

        Finally
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Nov 2010) -- End




    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (bdgbudget_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintBudgettranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@budgettranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbudgettranunkid.ToString)
    '        objDataOperation.AddParameter("@budgetunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbudgetunkid.ToString)
    '        objDataOperation.AddParameter("@allocationbyunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintallocationbyunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)
    '        objDataOperation.AddParameter("@amount", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblamount.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

    '        StrQ = "UPDATE bdgbudget_tran SET " & _
    '          "  budgetunkid = @budgetunkid" & _
    '          ", allocationbyunkid = @allocationbyunkid" & _
    '          ", tranheadunkid = @tranheadunkid" & _
    '          ", amount = @amount" & _
    '          ", userunkid = @userunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason " & _
    '        "WHERE budgettranunkid = @budgettranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  budgettranunkid " & _
    '          ", budgetunkid " & _
    '          ", allocationbyunkid " & _
    '          ", tranheadunkid " & _
    '          ", amount " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM bdgbudget_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND budgettranunkid <> @budgettranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class