﻿'************************************************************************************************************************************
'Class Name : clsBudgetcodes_master.vb
'Purpose    :
'Date       :25/07/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.ComponentModel

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudgetcodes_master
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetcodes_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetcodesunkid As Integer
    Private mintBudgetunkid As Integer
    Private mintPeriodUnkId As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrWebFormName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebHost As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetcodesunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetcodesunkid(Optional ByVal xDataOper As clsDataOperation = Nothing) As Integer
        Get
            Return mintBudgetcodesunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetcodesunkid = value
            Call GetData(xDataOper)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetunkid() As Integer
        Get
            Return mintBudgetunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _PeriodUnkId() As Integer
        Get
            Return mintPeriodUnkId
        End Get
        Set(ByVal value As Integer)
            mintPeriodUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _WebFormName() As String
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHost() As String
        Set(ByVal value As String)
            mstrWebHost = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  budgetcodesunkid " & _
              ", budgetunkid " & _
              ", periodunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetcodes_master " & _
             "WHERE budgetcodesunkid = @budgetcodesunkid "

            objDataOperation.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodesunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBudgetcodesunkid = CInt(dtRow.Item("budgetcodesunkid"))
                mintBudgetunkid = CInt(dtRow.Item("budgetunkid"))
                mintPeriodUnkId = CInt(dtRow.Item("periodunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal intPeriodId As Integer = 0 _
                            , Optional ByVal strFilter As String = "" _
                            ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim objMaster As New clsMasterData
            Dim ds As DataSet = objMaster.getComboListForBudgetViewBy("ViewBy")
            Dim dicViewBy As Dictionary(Of Integer, String) = (From p In ds.Tables("ViewBy") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.GetReportAllocation("Allocation")
            Dim dicAllocation As Dictionary(Of Integer, String) = (From p In ds.Tables("Allocation") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListForBudgetPresentation("Presentation")
            Dim dicPresentation As Dictionary(Of Integer, String) = (From p In ds.Tables("Presentation") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListForBudgetWhoToInclude("WhoToInclude")
            Dim dicWhoToInclude As Dictionary(Of Integer, String) = (From p In ds.Tables("WhoToInclude") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListForBudgetSalaryLevel("SalaryLevel", , True)
            Dim dicSalaryLevel As Dictionary(Of Integer, String) = (From p In ds.Tables("SalaryLevel") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getApprovalStatus("Status", True, True, True, True, True, True)
            Dim dicStatus As Dictionary(Of Integer, String) = (From p In ds.Tables("Status") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            strQ = "SELECT  bgbudgetcodes_master.budgetcodesunkid  " & _
                          ", bgbudget_master.budgetunkid " & _
                          ", bgbudget_master.budget_code " & _
                          ", bgbudget_master.budget_name " & _
                          ", bgbudgetcodes_master.periodunkid " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", cfcommon_period_tran.statusid " & _
                          ", bgbudget_master.payyearunkid " & _
                          ", bgbudget_master.payyearname AS payyear " & _
                          ", bgbudget_master.previousperiodyearunkid " & _
                          ", bgbudget_master.viewbyid " & _
                          ", bgbudget_master.allocationbyid " & _
                          ", bgbudget_master.presentationmodeid " & _
                          ", bgbudget_master.whotoincludeid " & _
                          ", bgbudget_master.salarylevelid " & _
                          ", bgbudget_master.budget_date " & _
                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS strbudget_date " & _
                          ", bgbudget_master.isdefault " & _
                          ", CASE bgbudget_master.isdefault WHEN 0 THEN 'No' ELSE 'Yes' END AS isdefaultYesNo " & _
                          ", ISNULL(bgbudget_master.budget_statusunkid, 1) AS budget_statusunkid " & _
                          ", bgbudget_master.userunkid " & _
                          ", bgbudget_master.isvoid " & _
                          ", bgbudget_master.voiduserunkid " & _
                          ", bgbudget_master.voiddatetime " & _
                          ", bgbudget_master.voidreason " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date "
            'Sohail (13 Oct 2017) - [start_date, end_date]

            strQ &= ", CASE viewbyid "
            For Each pair In dicViewBy
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS ViewBy "

            strQ &= ", CASE allocationbyid "
            For Each pair In dicAllocation
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " WHEN 0  THEN 'Employee' "
            strQ &= " END AS AllocationBy "

            strQ &= ", CASE presentationmodeid "
            For Each pair In dicPresentation
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS PresentationMode "

            strQ &= ", CASE whotoincludeid "
            For Each pair In dicWhoToInclude
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS WhoToInclude "

            strQ &= ", CASE salarylevelid "
            For Each pair In dicSalaryLevel
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS SalaryLevel "

            strQ &= ", CASE ISNULL(bgbudget_master.budget_statusunkid, 1) "
            For Each pair In dicStatus
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS budget_status "

            strQ &= " FROM    bgbudgetcodes_master " & _
                            "LEFT JOIN bgbudget_master ON bgbudgetcodes_master.budgetunkid  = bgbudget_master.budgetunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON bgbudgetcodes_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                            "LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.yearunkid = bgbudget_master.payyearunkid " & _
                    "WHERE   bgbudgetcodes_master.isvoid = 0 " & _
                            "AND bgbudget_master.isvoid = 0 " & _
                            "AND cfcommon_period_tran.isactive = 1 " & _
                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "

            If intPeriodId > 0 Then
                strQ &= "AND cfcommon_period_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetDataGridList(ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As DateTime _
                                    , ByVal xPeriodEnd As DateTime _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , ByVal intAllocationID As Integer _
                                    , ByVal mstrAnalysis_Fields As String _
                                    , ByVal mstrAnalysis_Join As String _
                                    , ByVal mstrAnalysis_TableName As String _
                                    , ByVal mstrAnalysis_OrderBy As String _
                                    , ByVal strPeriodList As String _
                                    , ByVal strTranHeadList As String _
                                    , ByVal intBudgetUnkId As Integer _
                                    , ByVal strPreviousPeriodDBName As String _
                                    , ByVal intBudgetForID As Integer _
                                    , ByVal intPresentationID As Integer _
                                    , ByVal intWhoToIncludeID As Integer _
                                    , ByVal strTableName As String _
                                    , ByVal intPeriodUnkId As Integer _
                                    , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                    , Optional ByVal strFilerString As String = "" _
                                    , Optional ByRef dsAllHeads As DataSet = Nothing _
                                    , Optional ByVal blnForBudgetCodes As Boolean = False _
                                    , Optional ByVal mstrAnalysis_CodeField As String = "" _
                                    ) As DataSet
        'Sohail (01 Mar 2017) - [mstrAnalysis_CodeField]

        Dim dsList As DataSet = Nothing
        Dim dsAll As DataSet = Nothing
        Dim strQ As String = ""
        Dim strQ1 As String = ""
        Dim exForce As Exception
        Dim strFundQry As String = ""
        Dim strFundFld As String = ""
        Dim strFundFldIsNULL As String = ""
        Dim strJobQry As String = ""
        Dim strManPwerPlanQry As String = ""
        Dim strEmpJobField As String = ""
        Dim strEmpJobCreate As String = ""
        Dim strEmpJobTableFld As String = ""
        Dim strEmpJobJoin As String = ""
        Dim strEmpEmployeeJoin As String = ""
        Dim strActivityQry As String = ""
        Dim strEmpAnalysisFld As String
        Dim strAnalysis_OrderBy As String
        Dim strAnalysis_Join As String
        'Sohail (01 Mar 2017) -- Start
        'Enhancement - 65.1 - Export and Import option on Budget Codes.
        Dim strcteFund As String = ""
        Dim strcteActivity As String = ""
        'Sohail (01 Mar 2017) -- End

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)

            If intBudgetForID = enBudgetViewBy.Employee Then
                strEmpJobField = ", ISNULL(ERECAT.jobunkid,ISNULL(hremployee_master.jobunkid,0)) As empjobunkid " & _
                                 ", ISNULL(hrjob_master.job_name, ISNULL(EMPJOB.job_name,'')) AS EmpJobTitle "

                strEmpJobCreate = ", empjobunkid INT NULL " & _
                                  ", EmpJobTitle NVARCHAR(MAX) NULL "

                strEmpJobTableFld = ", empjobunkid " & _
                                    ", EmpJobTitle "

                strEmpEmployeeJoin = "LEFT JOIN hremployee_master ON #ce.allocationtranunkid = hremployee_master.employeeunkid "
                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'strEmpJobJoin &= "       LEFT JOIN " & _
                '                        "( " & _
                '                        "   SELECT " & _
                '                        "     Cat.CatEmpId " & _
                '                        "    ,Cat.jobgroupunkid " & _
                '                        "    ,Cat.jobunkid " & _
                '                        "    ,Cat.CEfDt " & _
                '                        "    ,Cat.RECAT_REASON " & _
                '                        "   FROM " & _
                '                        "   ( " & _
                '                        "       SELECT " & _
                '                        "            ECT.employeeunkid AS CatEmpId " & _
                '                        "           ,ECT.jobgroupunkid " & _
                '                        "           ,ECT.jobunkid " & _
                '                        "           ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                '                        "           ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                '                        "           ,CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                '                        "       FROM hremployee_categorization_tran AS ECT " & _
                '                        "           LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                '                        "           LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                '                        "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                '                        "   ) AS Cat WHERE Cat.Rno = 1 " & _
                '                        ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                '                        "LEFT JOIN hrjob_master ON ERECAT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                '                        "LEFT JOIN hrjob_master EMPJOB ON hremployee_master.jobunkid = EMPJOB.jobunkid AND EMPJOB.isactive = 1 "
                strEmpJobJoin &= "       LEFT JOIN #cteCAT AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                                        "LEFT JOIN hrjob_master ON ERECAT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                                        "LEFT JOIN hrjob_master EMPJOB ON hremployee_master.jobunkid = EMPJOB.jobunkid AND EMPJOB.isactive = 1 "
                'Sohail (01 Mar 2017) -- End
            End If

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            'strQ = "DECLARE @cols NVARCHAR(MAX) " & _
            '       "DECLARE @cols2 NVARCHAR(MAX) " & _
            '       "DECLARE @qry NVARCHAR(MAX) " & _
            '       "SELECT  @cols = STUFF((SELECT   ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectname) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
            '                               "FROM     bgfundprojectcode_master " & _
            '                               "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
            '                "FOR           XML PATH('')  " & _
            '                                ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
            '        "SELECT  @cols2 = STUFF((SELECT  ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
            '                                "FROM    bgfundprojectcode_master " & _
            '                                "WHERE   bgfundprojectcode_master.isvoid = 0 " & _
            '                "FOR            XML PATH('')  " & _
            '                                 ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
            '        " " & _
            '        "SET @qry = 'SELECT budgettranunkid, ' + @cols + N' FROM (SELECT ISNULL(bgbudgetcodesfundsource_tran.budgettranunkid, bgbudgetfundsource_tran.budgettranunkid) AS budgettranunkid, bgfundprojectcode_master.fundprojectcodeunkid, ISNULL(bgbudgetcodesfundsource_tran.percentage, bgbudgetfundsource_tran.percentage) AS percentage FROM bgbudgetfundsource_tran " & _
            '            "LEFT JOIN bgfundprojectcode_master ON bgbudgetfundsource_tran.fundprojectcodeunkid = bgfundprojectcode_master.fundprojectcodeunkid " & _
            '            "LEFT JOIN bgbudgetcodesfundsource_tran ON bgbudgetfundsource_tran.budgettranunkid = bgbudgetcodesfundsource_tran.budgettranunkid " & _
            '                    "AND bgbudgetcodesfundsource_tran.fundprojectcodeunkid = bgbudgetfundsource_tran.fundprojectcodeunkid " & _
            '                    "AND bgbudgetcodesfundsource_tran.isvoid = 0 " & _
            '                    "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid " & _
            '            "WHERE bgbudgetfundsource_tran.isvoid = 0 " & _
            '            "AND bgfundprojectcode_master.isvoid = 0 " & _
            '            ") AS z " & _
            '            "PIVOT (SUM( percentage) FOR fundprojectcodeunkid IN (' + @cols2 + ') ) AS A' "
            strQ = "DECLARE @cols NVARCHAR(MAX) " & _
                   "DECLARE @cols2 NVARCHAR(MAX) " & _
                   "DECLARE @qry NVARCHAR(MAX) " & _
                   "SELECT  @cols = STUFF((SELECT   ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectname) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                           "FROM     bgfundprojectcode_master " & _
                                           "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
                            "FOR           XML PATH('')  " & _
                                            ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                    "SELECT  @cols2 = STUFF((SELECT  ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                            "FROM    bgfundprojectcode_master " & _
                                            "WHERE   bgfundprojectcode_master.isvoid = 0 " & _
                            "FOR            XML PATH('')  " & _
                                             ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                    " " & _
                   "SET @qry = 'SELECT budgettranunkid, ISNULL(budgetcodestranunkid, 0) AS budgetcodestranunkid, ' + @cols + N' INTO #cte# FROM (SELECT ISNULL(bgbudgetcodesfundsource_tran.budgettranunkid, bgbudgetfundsource_tran.budgettranunkid) AS budgettranunkid, bgbudgetcodesfundsource_tran.budgetcodestranunkid, bgfundprojectcode_master.fundprojectcodeunkid, ISNULL(bgbudgetcodesfundsource_tran.percentage, bgbudgetfundsource_tran.percentage) AS percentage FROM bgbudgetfundsource_tran " & _
                        "LEFT JOIN bgfundprojectcode_master ON bgbudgetfundsource_tran.fundprojectcodeunkid = bgfundprojectcode_master.fundprojectcodeunkid " & _
                        "LEFT JOIN bgbudgetcodesfundsource_tran ON bgbudgetfundsource_tran.budgettranunkid = bgbudgetcodesfundsource_tran.budgettranunkid " & _
                                "AND bgbudgetcodesfundsource_tran.fundprojectcodeunkid = bgbudgetfundsource_tran.fundprojectcodeunkid " & _
                                "AND bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                                "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid " & _
                        "WHERE bgbudgetfundsource_tran.isvoid = 0 " & _
                        "AND bgfundprojectcode_master.isvoid = 0 " & _
                        "UNION ALL " & _
                        "SELECT  bgbudgetcodesfundsource_tran.budgettranunkid  " & _
                              ", bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                              ", bgbudgetcodesfundsource_tran.fundprojectcodeunkid " & _
                              ", bgbudgetcodesfundsource_tran.percentage " & _
                        "FROM    bgbudgetcodesfundsource_tran " & _
                                "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgetcodestranunkid = bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                                                                "AND bgbudgetcodes_tran.periodunkid = bgbudgetcodesfundsource_tran.periodunkid " & _
                        "WHERE   bgbudgetcodes_tran.isvoid = 0 " & _
                                "AND bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                                "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid " & _
                                "AND bgbudgetcodesfundsource_tran.budgettranunkid = 0 " & _
                        ") AS z " & _
                        "PIVOT (SUM( percentage) FOR fundprojectcodeunkid IN (' + @cols2 + ') ) AS A' "
            'Sohail (03 May 2017) - [budgetcodestranunkid, UNION ALL]
            'Sohail (01 Mar 2017) -- End

            strQ &= "SELECT @qry "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 AndAlso IsDBNull(dsList.Tables(0).Rows(0).Item(0)) = False Then
                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'strFundQry = " LEFT JOIN ( " & dsList.Tables(0).Rows(0).Item(0).ToString & " ) AS FundProjectCode ON FundProjectCode.budgettranunkid = ISNULL(bgbudgetcodes_tran.budgettranunkid, bgbudget_tran.budgettranunkid) "
                'strActivityQry = " LEFT JOIN ( " & dsList.Tables(0).Rows(0).Item(0).ToString.Replace("SUM( percentage)", "MAX( fundactivityunkid)").Replace("percentage", "fundactivityunkid") & " ) AS FundActivity ON FundActivity.budgettranunkid = ISNULL(bgbudgetcodes_tran.budgettranunkid, bgbudget_tran.budgettranunkid) "
                strFundQry = " LEFT JOIN  #cteFUND AS FundProjectCode ON FundProjectCode.budgettranunkid = ISNULL(bgbudgetcodes_tran.budgettranunkid, bgbudget_tran.budgettranunkid) "
                strActivityQry = " LEFT JOIN #cteACTIVITY AS FundActivity ON FundActivity.budgettranunkid = ISNULL(bgbudgetcodes_tran.budgettranunkid, bgbudget_tran.budgettranunkid) "

                strcteFund = dsList.Tables(0).Rows(0).Item(0).ToString
                strcteActivity = dsList.Tables(0).Rows(0).Item(0).ToString.Replace("SUM( percentage)", "MAX( fundactivityunkid)").Replace("percentage", "fundactivityunkid")
                'Sohail (01 Mar 2017) -- End

                strQ = "DECLARE @colsisnull NVARCHAR(MAX) " & _
                       "DECLARE @cols NVARCHAR(MAX) " & _
                       "DECLARE @codecols NVARCHAR(MAX) " & _
                        " " & _
                        "SELECT  @colsisnull = STUFF((SELECT   ', ISNULL(FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ISNULL(FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectname) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                               "FROM     bgfundprojectcode_master " & _
                                               "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
                                "FOR           XML PATH('')  " & _
                                                ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                        "SELECT  @cols = STUFF((SELECT   ', FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                               "FROM     bgfundprojectcode_master " & _
                                               "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
                                "FOR           XML PATH('')  " & _
                                                ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                        " " & _
                        "SELECT  @colsisnull UNION ALL SELECT  @cols "

                dsList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    strFundFldIsNULL = "," & dsList.Tables(0).Rows(0).Item(0).ToString & " "
                    strFundFld = "," & dsList.Tables(0).Rows(1).Item(0).ToString & " "
                End If
            End If

            If intBudgetForID = enBudgetViewBy.Employee AndAlso intWhoToIncludeID = enBudgetWhoToInclude.AllAsPerManPowerPlan Then

                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Planned employees not coming for those jobs which is not assigned single employee.
                'strQ = "SELECT  A.jobunkid  " & _
                '              ", JOB_NAME " & _
                '              ", PLANNED " & _
                '              ", AVAILABLE " & _
                '              ", ISNULL(PLANNED, 0) - ISNULL(AVAILABLE, 0) AS VARIATION " & _
                '        "FROM    ( SELECT    hrjob_master.job_name AS JOB_NAME " & _
                '                          ", total_position AS PLANNED " & _
                '                          ", COUNT(hremployee_master.employeeunkid) AS AVAILABLE " & _
                '                          ", hrjob_master.jobunkid " & _
                '                  "FROM      hrjob_master " & _
                '                            "LEFT JOIN ( SELECT  jobunkid  " & _
                '                                              ", employeeunkid " & _
                '                                              ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
                '                                        "FROM    hremployee_categorization_tran " & _
                '                                        "WHERE   isvoid = 0 " & _
                '                                                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                '                                      ") AS Jobs ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                '                                                   "AND Jobs.rno = 1 " & _
                '                            "LEFT JOIN hremployee_master ON Jobs.employeeunkid = hremployee_master.employeeunkid "
                'Sohail (06 May 2017) -- Start
                'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
                'strQ = "SELECT  A.jobunkid  " & _
                '              ", JOB_CODE " & _
                '              ", JOB_NAME " & _
                '              ", PLANNED " & _
                '              ", AVAILABLE " & _
                '              ", ISNULL(PLANNED, 0) - ISNULL(AVAILABLE, 0) AS VARIATION " & _
                '        "FROM    ( SELECT    hrjob_master.job_name AS JOB_NAME " & _
                '                          ", hrjob_master.job_code AS JOB_CODE " & _
                '                          ", total_position AS PLANNED " & _
                '                          ", ISNULL(AA.AVAILABLE, 0) AS AVAILABLE " & _
                '                          ", hrjob_master.jobunkid " & _
                '                  "FROM      hrjob_master " & _
                '                  "LEFT JOIN  ( SELECT     COUNT(hremployee_master.employeeunkid) AS AVAILABLE " & _
                '                          ", hrjob_master.jobunkid " & _
                '                  "FROM      hrjob_master " & _
                '                            "LEFT JOIN ( SELECT  jobunkid  " & _
                '                                              ", employeeunkid " & _
                '                                              ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
                '                                        "FROM    hremployee_categorization_tran " & _
                '                                        "WHERE   isvoid = 0 " & _
                '                                                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                '                                      ") AS Jobs ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                '                                                   "AND Jobs.rno = 1 " & _
                '                            "LEFT JOIN hremployee_master ON Jobs.employeeunkid = hremployee_master.employeeunkid "
                strQ = "SELECT  A.jobunkid  " & _
                              ", JOB_CODE " & _
                              ", JOB_NAME " & _
                              ", PLANNED " & _
                              ", AVAILABLE " & _
                              ", ISNULL(PLANNED, 0) - ISNULL(AVAILABLE, 0) AS VARIATION " & _
                        "FROM    ( SELECT    hrjob_master.job_name AS JOB_NAME " & _
                                          ", hrjob_master.job_code AS JOB_CODE " & _
                                          ", total_position AS PLANNED " & _
                                          ", ISNULL(AA.AVAILABLE, 0) AS AVAILABLE " & _
                                          ", hrjob_master.jobunkid " & _
                                  "FROM      hrjob_master " & _
                                  "LEFT JOIN  ( SELECT  SUM(B.AVAILABLE) AS AVAILABLE  " & _
                                                    ", B.jobunkid " & _
                                              "FROM    ( SELECT    COUNT(hremployee_master.employeeunkid) AS AVAILABLE " & _
                                          ", hrjob_master.jobunkid " & _
                                  "FROM      hrjob_master " & _
                                            "LEFT JOIN ( SELECT  jobunkid  " & _
                                                              ", employeeunkid " & _
                                                              ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
                                                        "FROM    hremployee_categorization_tran " & _
                                                        "WHERE   isvoid = 0 " & _
                                                                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                                      ") AS Jobs ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                                                                   "AND Jobs.rno = 1 " & _
                                            "LEFT JOIN hremployee_master ON Jobs.employeeunkid = hremployee_master.employeeunkid "
                'Sohail (06 May 2017) -- End
                'Sohail (01 Mar 2017) -- End

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If

                strQ &= " WHERE hrjob_master.isactive = 1 "

                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry & " "
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry & " "
                    End If
                End If

                strQ &= "       GROUP BY  hrjob_master.job_name " & _
                                          ", total_position " & _
                                          ", hrjob_master.jobunkid "

                'Sohail (06 May 2017) -- Start
                'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
                If intBudgetUnkId > 0 Then

                    'strQ &= "  UNION ALL " & _
                    '          "SELECT    COUNT(DISTINCT bgbudget_tran.allocationtranunkid) AS AWAILABLE  " & _
                    '                  ", bgbudget_tran.jobunkid " & _
                    '          "FROM      bgbudget_tran " & _
                    '          "WHERE     bgbudget_tran.isvoid = 0 " & _
                    '                    "AND bgbudget_tran.budgetunkid = @budgetunkid " & _
                    '                    "AND bgbudget_tran.allocationtranunkid <= 0 " & _
                    '                    "AND bgbudget_tran.jobunkid > 0 " & _
                    '          "GROUP BY  bgbudget_tran.jobunkid "
                    strQ &= "  UNION ALL " & _
                              "SELECT    COUNT(DISTINCT bgbudgetcodes_tran.allocationtranunkid) AS AWAILABLE  " & _
                                      ", bgbudgetcodes_tran.jobunkid " & _
                              "FROM      bgbudgetcodes_tran " & _
                              "WHERE     bgbudgetcodes_tran.isvoid = 0 " & _
                                        "AND bgbudgetcodes_tran.budgetunkid = @budgetunkid " & _
                                        "AND bgbudgetcodes_tran.periodunkid = @periodunkid " & _
                                        "AND bgbudgetcodes_tran.allocationtranunkid <= 0 " & _
                                        "AND bgbudgetcodes_tran.jobunkid > 0 " & _
                              "GROUP BY  bgbudgetcodes_tran.jobunkid "

                End If
                'Sohail (06 May 2017) -- End

                strQ &= "   ) B " & _
                       "GROUP BY B.jobunkid " & _
                                    ") AS AA ON AA.jobunkid = hrjob_master.jobunkid " & _
                                ") AS A " & _
                        "WHERE   1 = 1 " & _
                                "AND ISNULL(PLANNED, 0) - ISNULL(AVAILABLE, 0) > 0 "
                'Sohail (01 Mar 2017) - [") AS AA ON AA.jobunkid = hrjob_master.jobunkid "]

                Dim ds As DataSet = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dsRow As DataRow In ds.Tables(0).Rows
                        strJobQry &= " UNION ALL SELECT " & dsRow.Item("jobunkid").ToString & " AS JobUnkId, '" & dsRow.Item("JOB_NAME").ToString.Replace("'", "''") & "' AS Job, '" & dsRow.Item("JOB_CODE").ToString.Replace("'", "''") & "' AS Job_Code, " & dsRow.Item("VARIATION").ToString & " AS Cnt "
                    Next
                    strManPwerPlanQry = ";WITH ct AS ( " & _
                                        "SELECT  JobUnkId " & _
                                              ", Job " & _
                                              ", Job_Code " & _
                                              ", Cnt " & _
                                        "FROM (" & _
                                        strJobQry.Substring(10) & _
                                        ") AS A " & _
                                        "UNION ALL " & _
                                        "SELECT  ct.JobUnkId " & _
                                              ", ct.Job " & _
                                              ", ct.Job_Code " & _
                                              ", (ct.Cnt - 1) AS Cnt " & _
                                        "FROM ct " & _
                                        "JOIN (" & _
                                        strJobQry.Substring(10) & _
                                        ") AS A " & _
                                            "ON ct.JobUnkId = A.JobUnkId " & _
                                        "WHERE ct.Cnt > 1) "
                    'Sohail (01 Mar 2017) - [Job_Code]

                End If
            End If


            strQ = "CREATE TABLE #bgt ( " & _
                              "  budgetunkid INT NULL " & _
                              ", budget_code NVARCHAR(MAX) NULL " & _
                              ", budget_name NVARCHAR(MAX) NULL " & _
                              ", payyearunkid INT NULL " & _
                              ", viewbyid INT NULL " & _
                              ", allocationbyid INT NULL " & _
                              ", presentationmodeid INT NULL " & _
                              ", whotoincludeid INT NULL " & _
                              ", salarylevelid INT NULL " & _
                              ", budget_date NVARCHAR(MAX) NULL " & _
                              ", isdefault BIT NULL " & _
                              ", allocationtranunkid INT NULL " & _
                              ", tranheadunkid INT NULL " & _
                              ", trnheadname NVARCHAR(MAX) NULL " & _
                              ", IsSalary BIT NULL " & _
                              ", budgettranunkid INT NULL " & _
                              ", budgetcodestranunkid INT NULL " & _
                              ", jobunkid INT NULL " & _
                              ", amount DECIMAL(36, 6) NULL " & _
                              ", budgetamount DECIMAL(36, 6) NULL " & _
                              ", salaryamount DECIMAL(36, 6) NULL " & _
                              ", budgetsalaryamount DECIMAL(36, 6) NULL " & _
                              ", otherpayrollamount DECIMAL(36, 6) NULL " & _
                              ", budgetotherpayrollamount DECIMAL(36, 6) NULL " & _
                              ", pramount DECIMAL(36, 6) NULL " & _
                              ", IsChecked BIT NULL " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_").Replace("]", "] DECIMAL(36, 2) NULL ") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_").Replace("]", "] INT NULL DEFAULT 0 ") & _
                              ", Id INT NULL " & _
                              ", GName NVARCHAR(MAX) NULL " & _
                              ", GCode VARCHAR(MAX) " & _
                              strEmpJobCreate & _
                   " ); "
            'Sohail (01 Mar 2017) - [GCode]

            strQ &= "CREATE TABLE #cteEmp " & _
                    "( " & _
                      "  employeeunkid INT " & _
                      ", firstname VARCHAR(MAX) " & _
                      ", othername VARCHAR(MAX) " & _
                      ", surname VARCHAR(MAX) " & _
                      ", Id INT " & _
                      ", GName VARCHAR(MAX) " & _
                      ", GCode VARCHAR(MAX) " & _
                    ") " & _
                    "INSERT INTO #cteEmp " & _
                    "SELECT hremployee_master.employeeunkid, hremployee_master.firstname, hremployee_master.othername, hremployee_master.surname "
            'Sohail (01 Mar 2017) - [GCode]

            strQ &= mstrAnalysis_Fields
            strQ &= mstrAnalysis_CodeField 'Sohail (01 Mar 2017) 

            strQ &= "FROM hremployee_master "

            If intBudgetForID <> enBudgetViewBy.Employee Then
                strQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If intBudgetForID = enBudgetViewBy.Employee Then
                'Sohail (03 May 2017) -- Start
                'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
                'strQ &= mstrAnalysis_Join
                If intWhoToIncludeID = enBudgetWhoToInclude.ActiveEmployee Then

                Else
                strQ &= mstrAnalysis_Join
            End If
                'Sohail (03 May 2017) -- End
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            strQ &= " ; "

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            'strEmpAnalysisFld = ", #cteEmp.Id AS Id, #cteEmp.GName AS GName "
            strEmpAnalysisFld = ", #cteEmp.Id AS Id, #cteEmp.GName AS GName, #cteEmp.GCode "
            'Sohail (01 Mar 2017) -- End
            'If intBudgetForID = enBudgetViewBy.Employee Then
            strAnalysis_Join = mstrAnalysis_Join
            mstrAnalysis_Join = " AND 1 = 1 "
            strAnalysis_OrderBy = mstrAnalysis_OrderBy
            mstrAnalysis_OrderBy = " #cteEmp.Id "
            'End If

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            If strFundQry.Trim <> "" Then
                strQ &= " " & strcteFund.Replace("#cte#", "#cteFUND") & "; "
            End If

            If strActivityQry.Trim <> "" Then
                strQ &= " " & strcteActivity.Replace("#cte#", "#cteACTIVITY") & "; "
            End If

            strQ &= " SELECT  prtranhead_master.tranheadunkid  " & _
                          ", prtranhead_master.trnheadname " & _
                          ", SUM(prpayrollprocess_tran.amount) AS pramount " & _
                          strEmpAnalysisFld & _
                          "INTO #ctePayroll " & _
                    "FROM    " & strPreviousPeriodDBName & "..prpayrollprocess_tran " & _
                            "LEFT JOIN " & strPreviousPeriodDBName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "/*LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid*/ " & _
                            "LEFT JOIN #cteEmp ON #cteEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            mstrAnalysis_Join & _
                    "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtranhead_master.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid IN (" & strPeriodList & ") " & _
                            "AND prpayrollprocess_tran.tranheadunkid IN (" & strTranHeadList & ") " & _
                            "AND #cteEmp.employeeunkid IS NOT NULL " & _
                    "GROUP BY " & strEmpAnalysisFld.Substring(2).Replace("AS Id", "").Replace("AS GName", "").Replace("AS GCode", "") & " " & _
                              ", prtranhead_master.tranheadunkid " & _
                              ", prtranhead_master.trnheadname "
            'Sohail (01 Mar 2017) - [.Replace("AS GCode", "")]

            If intBudgetForID = enBudgetViewBy.Employee Then

                strQ &= " SELECT     Cat.CatEmpId " & _
                                  ", Cat.jobgroupunkid " & _
                                  ", Cat.jobunkid " & _
                                  ", Cat.CEfDt " & _
                                  ", Cat.RECAT_REASON " & _
                                  " INTO #cteCAT " & _
                         "FROM  ( SELECT   ECT.employeeunkid AS CatEmpId " & _
                                        ", ECT.jobgroupunkid " & _
                                        ", ECT.jobunkid " & _
                                        ", CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                                        ", ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                                        ", CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                                 "FROM     hremployee_categorization_tran AS ECT " & _
                                          "LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                          "LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                                 "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                 ") AS Cat WHERE Cat.Rno = 1 "

            End If

            strQ &= " ; "
            'Sohail (01 Mar 2017) -- End

            strQ &= "/*WITH    cte " & _
                              "AS (*/ SELECT   ISNULL(bgbudgetcodes_master.budgetunkid, bgbudget_master.budgetunkid) AS budgetunkid  " & _
                                          ", bgbudget_master.budget_code " & _
                                          ", bgbudget_master.budget_name " & _
                                          ", bgbudget_master.payyearunkid " & _
                                          ", bgbudget_master.viewbyid " & _
                                          ", bgbudget_master.allocationbyid " & _
                                          ", bgbudget_master.presentationmodeid " & _
                                          ", bgbudget_master.whotoincludeid " & _
                                          ", bgbudget_master.salarylevelid " & _
                                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS budget_date " & _
                                          ", bgbudget_master.isdefault " & _
                                          ", bgbudget_tran.allocationtranunkid " & _
                                          ", bgbudget_tran.tranheadunkid " & _
                                          ", prtranhead_master.trnheadname " & _
                                          ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgettranunkid, bgbudget_tran.budgettranunkid) AS budgettranunkid " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetcodestranunkid, -1) AS budgetcodestranunkid " & _
                                          ", bgbudget_tran.jobunkid " & _
                                          ", bgbudget_tran.amount " & _
                                          ", CASE bgbudget_master.presentationmodeid WHEN 1 THEN ISNULL(bgbudgetcodes_tran.budgetamount, bgbudget_tran.budgetamount) ELSE CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN ISNULL(bgbudgetcodes_tran.budgetsalaryamount, bgbudget_tran.budgetsalaryamount) ELSE ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, bgbudget_tran.budgetotherpayrollamount) END END AS budgetamount " & _
                                          ", bgbudget_tran.salaryamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetsalaryamount, bgbudget_tran.budgetsalaryamount) AS budgetsalaryamount " & _
                                          ", bgbudget_tran.otherpayrollamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, bgbudget_tran.budgetotherpayrollamount) AS budgetotherpayrollamount " & _
                                          ", ISNULL(payroll.pramount, 0) AS pramount " & _
                                          ", CAST(0 AS BIT) AS IsChecked " & _
                                          strFundFldIsNULL.Replace("AS [", "AS [|_") & _
                                          strFundFldIsNULL.Replace("FundProjectCode.", "FundActivity.").Replace("AS [", "AS [||_") & _
                                          mstrAnalysis_Fields & _
                                          mstrAnalysis_CodeField & _
                                   "INTO #cte FROM     bgbudget_master " & _
                                            "LEFT JOIN bgbudgetcodes_master ON bgbudget_master.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                                    "AND bgbudgetcodes_master.isvoid = 0 " & _
                                                    "AND bgbudgetcodes_master.periodunkid = @periodunkid " & _
                                            "LEFT JOIN bgbudget_tran ON bgbudget_master.budgetunkid = bgbudget_tran.budgetunkid " & _
                                            "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgettranunkid = bgbudget_tran.budgettranunkid " & _
                                                    "AND bgbudgetcodes_tran.isvoid = 0 " & _
                                                    "AND bgbudgetcodes_tran.periodunkid = @periodunkid " & _
                                            "LEFT JOIN prtranhead_master ON bgbudget_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                            "LEFT JOIN " & mstrAnalysis_TableName & " ON bgbudget_tran.allocationtranunkid = " & strAnalysis_OrderBy & " " & _
                                            strFundQry & _
                                            strActivityQry & _
                                            "/*LEFT JOIN ( SELECT  prtranhead_master.tranheadunkid  " & _
                                                              ", prtranhead_master.trnheadname " & _
                                                              ", SUM(prpayrollprocess_tran.amount) AS pramount " & _
                                                              strEmpAnalysisFld & _
                                                        "FROM    " & strPreviousPeriodDBName & "..prpayrollprocess_tran " & _
                                                                "LEFT JOIN " & strPreviousPeriodDBName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "/*LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid*/ " & _
                                                                "LEFT JOIN #cteEmp ON #cteEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                mstrAnalysis_Join & _
                                                        "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                                "AND prtranhead_master.isvoid = 0 " & _
                                                                "AND prtnaleave_tran.payperiodunkid IN (" & strPeriodList & ") " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid IN (" & strTranHeadList & ") " & _
                                                                "AND #cteEmp.employeeunkid IS NOT NULL " & _
                                                        "GROUP BY " & strEmpAnalysisFld.Substring(2).Replace("AS Id", "").Replace("AS GName", "").Replace("AS GCode", "") & " " & _
                                                                  ", prtranhead_master.tranheadunkid " & _
                                                                  ", prtranhead_master.trnheadname " & _
                                                    ") AS payroll ON payroll.tranheadunkid = bgbudget_tran.tranheadunkid*/ " & _
                                            "LEFT JOIN #ctePayroll AS payroll ON payroll.tranheadunkid = bgbudget_tran.tranheadunkid " & _
                                                        "AND bgbudget_tran.allocationtranunkid = payroll.id " & _
                                            "LEFT JOIN #cteEmp ON #cteEmp.id = " & strAnalysis_OrderBy & " " & _
                                            " " & _
                                   "WHERE    bgbudget_master.isvoid = 0 " & _
                                            "AND bgbudget_tran.isvoid = 0 " & _
                                            "AND prtranhead_master.isvoid = 0 " & _
                                            "/*AND payroll.id IS NOT NULL*/ " & _
                                            "AND #cteEmp.employeeunkid IS NOT NULL " & _
                                            "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                            "AND bgbudget_tran.jobunkid <= 0 " & _
                                            "AND bgbudget_master.budgetunkid = @budgetunkid " & _
                                   "UNION ALL " & _
                                   "SELECT   ISNULL(bgbudgetcodes_master.budgetunkid, bgbudget_master.budgetunkid) AS budgetunkid  " & _
                                          ", bgbudget_master.budget_code " & _
                                          ", bgbudget_master.budget_name " & _
                                          ", bgbudget_master.payyearunkid " & _
                                          ", bgbudget_master.viewbyid " & _
                                          ", bgbudget_master.allocationbyid " & _
                                          ", bgbudget_master.presentationmodeid " & _
                                          ", bgbudget_master.whotoincludeid " & _
                                          ", bgbudget_master.salarylevelid " & _
                                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS budget_date " & _
                                          ", bgbudget_master.isdefault " & _
                                          ", bgbudgetcodes_tran.allocationtranunkid " & _
                                          ", bgbudgetcodes_tran.tranheadunkid " & _
                                          ", prtranhead_master.trnheadname " & _
                                          ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgettranunkid, 0) AS budgettranunkid " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetcodestranunkid, -1) AS budgetcodestranunkid " & _
                                          ", bgbudgetcodes_tran.jobunkid " & _
                                          ", 0 AS amount " & _
                                          ", CASE bgbudget_master.presentationmodeid WHEN 1 THEN ISNULL(bgbudgetcodes_tran.budgetamount, 0) ELSE CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN ISNULL(bgbudgetcodes_tran.budgetsalaryamount, 0) ELSE ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, 0) END END AS budgetamount " & _
                                          ", 0 AS salaryamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetsalaryamount, 0) AS budgetsalaryamount " & _
                                          ", 0 AS otherpayrollamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, 0) AS budgetotherpayrollamount " & _
                                          ", ISNULL(payroll.pramount, 0) AS pramount " & _
                                          ", CAST(0 AS BIT) AS IsChecked " & _
                                          strFundFldIsNULL.Replace("AS [", "AS [|_") & _
                                          strFundFldIsNULL.Replace("FundProjectCode.", "FundActivity.").Replace("AS [", "AS [||_") & _
                                          mstrAnalysis_Fields & _
                                          mstrAnalysis_CodeField & _
                                   "FROM     bgbudget_master " & _
                                            "LEFT JOIN bgbudgetcodes_master ON bgbudget_master.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                                    "AND bgbudgetcodes_master.isvoid = 0 " & _
                                                    "AND bgbudgetcodes_master.periodunkid = @periodunkid " & _
                                            "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_master.budgetunkid = bgbudgetcodes_tran.budgetunkid " & _
                                                    "AND bgbudgetcodes_tran.periodunkid = bgbudgetcodes_master.periodunkid " & _
                                                    "AND bgbudgetcodes_tran.isvoid = 0 " & _
                                                    "AND bgbudgetcodes_tran.periodunkid = @periodunkid " & _
                                            "LEFT JOIN prtranhead_master ON bgbudgetcodes_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                            "LEFT JOIN " & mstrAnalysis_TableName & " ON bgbudgetcodes_tran.allocationtranunkid = " & strAnalysis_OrderBy & " " & _
                                            "LEFT JOIN #cteFUND AS FundProjectCode ON FundProjectCode.budgetcodestranunkid = bgbudgetcodes_tran.budgetcodestranunkid " & _
                                            "LEFT JOIN #cteACTIVITY AS FundActivity ON FundActivity.budgetcodestranunkid = bgbudgetcodes_tran.budgetcodestranunkid " & _
                                            "LEFT JOIN #ctePayroll AS payroll ON payroll.tranheadunkid = bgbudgetcodes_tran.tranheadunkid " & _
                                                        "AND bgbudgetcodes_tran.allocationtranunkid = payroll.id " & _
                                            "LEFT JOIN #cteEmp ON #cteEmp.id = " & strAnalysis_OrderBy & " " & _
                                            " " & _
                                   "WHERE    bgbudget_master.isvoid = 0 " & _
                                            "AND bgbudgetcodes_tran.isvoid = 0 " & _
                                            "AND prtranhead_master.isvoid = 0 " & _
                                            "AND #cteEmp.employeeunkid IS NOT NULL " & _
                                            "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                            "AND bgbudgetcodes_tran.jobunkid <= 0 " & _
                                            "AND bgbudget_master.budgetunkid = @budgetunkid " & _
                                            "AND bgbudgetcodes_tran.budgettranunkid = 0 " & _
                                   "UNION ALL " & _
                                   "/*  Sohail (06 May 2017) - [MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget] " & _
                                   "SELECT   ISNULL(bgbudgetcodes_master.budgetunkid, bgbudget_master.budgetunkid) AS budgetunkid  " & _
                                          ", bgbudget_master.budget_code " & _
                                          ", bgbudget_master.budget_name " & _
                                          ", bgbudget_master.payyearunkid " & _
                                          ", bgbudget_master.viewbyid " & _
                                          ", bgbudget_master.allocationbyid " & _
                                          ", bgbudget_master.presentationmodeid " & _
                                          ", bgbudget_master.whotoincludeid " & _
                                          ", bgbudget_master.salarylevelid " & _
                                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS budget_date " & _
                                          ", bgbudget_master.isdefault " & _
                                          ", bgbudget_tran.allocationtranunkid " & _
                                          ", bgbudget_tran.tranheadunkid " & _
                                          ", prtranhead_master.trnheadname " & _
                                          ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgettranunkid, bgbudget_tran.budgettranunkid) AS budgettranunkid " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetcodestranunkid, -1) AS budgetcodestranunkid " & _
                                          ", bgbudget_tran.jobunkid " & _
                                          ", bgbudget_tran.amount " & _
                                          ", CASE bgbudget_master.presentationmodeid WHEN 1 THEN ISNULL(bgbudgetcodes_tran.budgetamount, bgbudget_tran.budgetamount) ELSE CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN ISNULL(bgbudgetcodes_tran.budgetsalaryamount, bgbudget_tran.budgetsalaryamount) ELSE ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, bgbudget_tran.budgetotherpayrollamount) END END AS budgetamount " & _
                                          ", bgbudget_tran.salaryamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetsalaryamount, bgbudget_tran.budgetsalaryamount) AS budgetsalaryamount " & _
                                          ", bgbudget_tran.otherpayrollamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, bgbudget_tran.budgetotherpayrollamount) AS budgetotherpayrollamount " & _
                                          ", 0 AS pramount " & _
                                          ", CAST(0 AS BIT) AS IsChecked " & _
                                          strFundFldIsNULL.Replace("AS [", "AS [|_") & _
                                          strFundFldIsNULL.Replace("FundProjectCode.", "FundActivity.").Replace("AS [", "AS [||_") & _
                                          ", bgbudget_tran.jobunkid AS Id " & _
                                          ", hrjob_master.job_name + ' ' + CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GName " & _
                                          ", hrjob_master.job_code + ' ' + CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GCode " & _
                                   "FROM     bgbudget_master " & _
                                            "LEFT JOIN bgbudgetcodes_master ON bgbudget_master.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                                    "AND bgbudgetcodes_master.isvoid = 0 " & _
                                                    "AND bgbudgetcodes_master.periodunkid = @periodunkid " & _
                                            "LEFT JOIN bgbudget_tran ON bgbudget_master.budgetunkid = bgbudget_tran.budgetunkid " & _
                                            "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgettranunkid = bgbudget_tran.budgettranunkid " & _
                                                    "AND bgbudgetcodes_tran.isvoid = 0 " & _
                                                    "AND bgbudgetcodes_tran.periodunkid = @periodunkid " & _
                                            "LEFT JOIN prtranhead_master ON bgbudget_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                            "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = bgbudget_tran.jobunkid " & _
                                            strFundQry & _
                                            strActivityQry & _
                                   "WHERE    bgbudget_master.isvoid = 0 " & _
                                            "AND bgbudget_tran.isvoid = 0 " & _
                                            "AND prtranhead_master.isvoid = 0 " & _
                                            "AND bgbudget_tran.jobunkid > 0 " & _
                                            "AND bgbudget_tran.tranheadunkid IN (" & strTranHeadList & ") " & _
                                            "AND bgbudget_master.budgetunkid = @budgetunkid */ " & _
                                   "SELECT   ISNULL(bgbudgetcodes_master.budgetunkid, bgbudget_master.budgetunkid) AS budgetunkid  " & _
                                          ", bgbudget_master.budget_code " & _
                                          ", bgbudget_master.budget_name " & _
                                          ", bgbudget_master.payyearunkid " & _
                                          ", bgbudget_master.viewbyid " & _
                                          ", bgbudget_master.allocationbyid " & _
                                          ", bgbudget_master.presentationmodeid " & _
                                          ", bgbudget_master.whotoincludeid " & _
                                          ", bgbudget_master.salarylevelid " & _
                                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS budget_date " & _
                                          ", bgbudget_master.isdefault " & _
                                          ", bgbudgetcodes_tran.allocationtranunkid " & _
                                          ", bgbudgetcodes_tran.tranheadunkid " & _
                                          ", prtranhead_master.trnheadname " & _
                                          ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgettranunkid, 0) AS budgettranunkid " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetcodestranunkid, -1) AS budgetcodestranunkid " & _
                                          ", bgbudgetcodes_tran.jobunkid " & _
                                          ", 0 AS amount " & _
                                          ", CASE bgbudget_master.presentationmodeid WHEN 1 THEN ISNULL(bgbudgetcodes_tran.budgetamount, 0) ELSE CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN ISNULL(bgbudgetcodes_tran.budgetsalaryamount, 0) ELSE ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, 0) END END AS budgetamount " & _
                                          ", 0 AS salaryamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetsalaryamount, 0) AS budgetsalaryamount " & _
                                          ", 0 AS otherpayrollamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, 0) AS budgetotherpayrollamount " & _
                                          ", 0 AS pramount " & _
                                          ", CAST(0 AS BIT) AS IsChecked " & _
                                          strFundFldIsNULL.Replace("AS [", "AS [|_") & _
                                          strFundFldIsNULL.Replace("FundProjectCode.", "FundActivity.").Replace("AS [", "AS [||_") & _
                                          ", bgbudgetcodes_tran.jobunkid AS Id " & _
                                          ", hrjob_master.job_name + ' ' + CAST(bgbudgetcodes_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GName " & _
                                          ", hrjob_master.job_code + ' ' + CAST(bgbudgetcodes_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GCode " & _
                                   "FROM     bgbudget_master " & _
                                            "LEFT JOIN bgbudgetcodes_master ON bgbudget_master.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                                    "AND bgbudgetcodes_master.isvoid = 0 " & _
                                                    "AND bgbudgetcodes_master.periodunkid = @periodunkid " & _
                                            "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                                    "AND bgbudgetcodes_tran.periodunkid = bgbudgetcodes_master.periodunkid " & _
                                                    "AND bgbudgetcodes_tran.isvoid = 0 " & _
                                                    "AND bgbudgetcodes_tran.periodunkid = @periodunkid " & _
                                            "LEFT JOIN prtranhead_master ON bgbudgetcodes_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                            "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = bgbudgetcodes_tran.jobunkid " & _
                                            "LEFT JOIN #cteFUND AS FundProjectCode ON FundProjectCode.budgetcodestranunkid = bgbudgetcodes_tran.budgetcodestranunkid " & _
                                            "LEFT JOIN #cteACTIVITY AS FundActivity ON FundActivity.budgetcodestranunkid = bgbudgetcodes_tran.budgetcodestranunkid " & _
                                   "WHERE    bgbudget_master.isvoid = 0 " & _
                                            "AND bgbudgetcodes_tran.isvoid = 0 " & _
                                            "AND prtranhead_master.isvoid = 0 " & _
                                            "AND bgbudgetcodes_tran.jobunkid > 0 " & _
                                            "AND bgbudgetcodes_tran.tranheadunkid IN (" & strTranHeadList & ") " & _
                                            "AND bgbudget_master.budgetunkid = @budgetunkid " & _
                                 "/*) " & _
                        ", ce " & _
                          "AS (*/ SELECT   * " & _
                               "INTO #ce FROM     #cte " & _
                                "/*UNION ALL " & _
                                "SELECT  @budgetunkid AS budgetunkid  " & _
                                      ", '' AS budget_code " & _
                                      ", '' AS budget_name " & _
                                      ", 0 AS payyearunkid " & _
                                      ", 0 AS viewbyid " & _
                                      ", 0 AS allocationbyid " & _
                                      ", 0 AS presentationmodeid " & _
                                      ", 0 AS whotoincludeid " & _
                                      ", 0 AS salarylevelid " & _
                                      ", CONVERT(CHAR(8), GETDATE(), 112) AS budget_date " & _
                                      ", CAST(0 AS BIT) AS isdefault " & _
                                      ", " & mstrAnalysis_OrderBy & " AS allocationtranunkid " & _
                                      ", prtranhead_master.tranheadunkid " & _
                                      ", prtranhead_master.trnheadname " & _
                                      ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                      ", 0 AS budgettranunkid " & _
                                      ", 0 AS budgetcodestranunkid " & _
                                      ", 0 AS jobunkid " & _
                                      ", 0 AS amount " & _
                                      ", 0 AS budgetamount " & _
                                      ", 0 AS salaryamount " & _
                                      ", 0 AS budgetsalaryamount " & _
                                      ", 0 AS otherpayrollamount " & _
                                      ", 0 AS budgetotherpayrollamount " & _
                                      ", SUM(prpayrollprocess_tran.amount) AS pramount " & _
                                      ", CAST(0 AS BIT) AS IsChecked " & _
                                      strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace("[", "[|_") & _
                                      strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace("[", "[||_") & _
                                      strEmpAnalysisFld & _
                                "FROM    " & strPreviousPeriodDBName & "..prpayrollprocess_tran " & _
                                        "LEFT JOIN " & strPreviousPeriodDBName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                        "/*LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid*/ " & _
                                        "LEFT JOIN #cteEmp ON #cteEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                        mstrAnalysis_Join & _
                                        "LEFT JOIN #cte ON #cte.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "AND #cte.Id = " & mstrAnalysis_OrderBy & " "
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]
            'Sohail (13 Apr 2017) - ["AND payroll.id IS NOT NULL "] [show only those employees which user has access]
            'Sohail (01 Mar 2017) - [mstrAnalysis_CodeField]

            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If

            strQ &= "            WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prtranhead_master.isvoid = 0 " & _
                                        "AND #cteEmp.employeeunkid IS NOT NULL " & _
                                        mstrAnalysis_Join.Substring(mstrAnalysis_Join.LastIndexOf("AND"), mstrAnalysis_Join.Length - mstrAnalysis_Join.LastIndexOf("AND")) & _
                                        "AND prtnaleave_tran.payperiodunkid IN (" & strPeriodList & ") " & _
                                        "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                        "AND #cte.budgetunkid IS NULL "

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    strQ &= " AND " & xUACFiltrQry & " "
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry & " "
            '    End If
            'End If

            strQ &= "            GROUP BY prtranhead_master.tranheadunkid " & _
                                        ", prtranhead_master.trnheadname " & _
                                        ", prtranhead_master.typeof_id " & _
                                        ", prtranhead_master.isbasicsalaryasotherearning " & _
                                        strEmpAnalysisFld.Replace("AS Id", "").Replace("AS GName", "").Replace("AS GCode", "").Replace("hremployee_master", "#cteEmp") & _
                        ")*/ "
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]

            If strJobQry.Trim <> "" Then
                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'strQ &= ", " & strManPwerPlanQry
                strQ &= " " & strManPwerPlanQry
                strQ &= " SELECT * into #ct FROM ct "
                'Sohail (01 Mar 2017) -- End
            End If

            strQ &= " INSERT INTO #bgt ( " & _
                              "  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", tranheadunkid " & _
                              ", trnheadname " & _
                              ", IsSalary " & _
                              ", budgettranunkid " & _
                              ", budgetcodestranunkid " & _
                              ", jobunkid " & _
                              ", amount " & _
                              ", budgetamount " & _
                              ", salaryamount " & _
                              ", budgetsalaryamount " & _
                              ", otherpayrollamount " & _
                              ", budgetotherpayrollamount " & _
                              ", pramount " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                    " ) " & _
                        "SELECT  #ce.* " & _
                         strEmpJobField.Replace(",0", ",ISNULL(#ce.Id, 0)").Replace(",''", ",SUBSTRING(RTRIM(LTRIM(#ce.GName)), 0, LEN(RTRIM(LTRIM(#ce.GName))) - CHARINDEX(' ', REVERSE(RTRIM(LTRIM(#ce.GName)))) + 1)") & _
                        "FROM    #ce " & _
                        strEmpEmployeeJoin & _
                        strEmpJobJoin & _
                        "UNION ALL " & _
                        "SELECT  @budgetunkid AS budgetunkid  " & _
                              ", '' AS budget_code " & _
                              ", '' AS budget_name " & _
                              ", 0 AS payyearunkid " & _
                              ", 0 AS viewbyid " & _
                              ", 0 AS allocationbyid " & _
                              ", 0 AS presentationmodeid " & _
                              ", 0 AS whotoincludeid " & _
                              ", 0 AS salarylevelid " & _
                              ", CONVERT(CHAR(8), GETDATE(), 112) AS budget_date " & _
                              ", CAST(0 AS BIT) AS isdefault " & _
                              ", " & strAnalysis_OrderBy & " AS allocationtranunkid " & _
                              ", prtranhead_master.tranheadunkid " & _
                              ", prtranhead_master.trnheadname " & _
                              ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                              ", 0 AS budgettranunkid " & _
                              ", 0 AS budgetcodestranunkid " & _
                              ", 0 AS jobunkid " & _
                              ", 0 AS amount " & _
                              ", 0 AS budgetamount " & _
                              ", 0 AS salaryamount " & _
                              ", 0 AS budgetsalaryamount " & _
                              ", 0 AS otherpayrollamount " & _
                              ", 0 AS budgetotherpayrollamount " & _
                              ", 0 AS pramount " & _
                              ", CAST(0 AS BIT) AS IsChecked " & _
                              strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              mstrAnalysis_Fields & _
                              mstrAnalysis_CodeField & _
                              strEmpJobField & _
                        "FROM    " & mstrAnalysis_TableName & " " & _
                                "LEFT JOIN prtranhead_master ON 1 = 1 " & _
                                "LEFT JOIN #ce ON #ce.id = " & strAnalysis_OrderBy & " " & _
                                                "AND #ce.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "AND #ce.jobunkid <= 0 " & _
                                strEmpJobJoin
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]
            'Sohail (01 Mar 2017) - [mstrAnalysis_CodeField]

            'If strEmpJobJoin.Trim <> "" Then 'Sohail (19 Apr 2017)

                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'If xDateJoinQry.Trim.Length > 0 Then
                '    strQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    strQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    strQ &= xAdvanceJoinQry
                'End If
            'Sohail (19 Apr 2017) -- Start
            'MST Enhancement - 66.1 - applying user access filter on payroll budget.
            'strQ &= "LEFT JOIN #cteEmp ON #cteEmp.employeeunkid = " & strAnalysis_OrderBy & " "
            strQ &= "LEFT JOIN #cteEmp ON #cteEmp.id = " & strAnalysis_OrderBy & " "
            'Sohail (19 Apr 2017) -- End
                'Sohail (01 Mar 2017) -- End

            'End If 'Sohail (19 Apr 2017)

            strQ &= "    WHERE   1 = 1 "

            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            'strAnalysis_Join.Substring(strAnalysis_Join.LastIndexOf("AND"), strAnalysis_Join.Length - strAnalysis_Join.LastIndexOf("AND")) & _
            If intBudgetForID = enBudgetViewBy.Employee AndAlso intWhoToIncludeID = enBudgetWhoToInclude.ActiveEmployee Then

            Else
                strQ &= strAnalysis_Join.Substring(strAnalysis_Join.LastIndexOf("AND"), strAnalysis_Join.Length - strAnalysis_Join.LastIndexOf("AND"))
            End If
            'Sohail (03 May 2017) -- End

            strQ &= "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                "AND ( #ce.tranheadunkid IS NULL OR #ce.id IS NULL ) "

            'If strEmpJobJoin.Trim <> "" Then 'Sohail (19 Apr 2017)

                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'If xUACFiltrQry.Trim.Length > 0 Then
                '    strQ &= " AND " & xUACFiltrQry & " "
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        strQ &= xDateFilterQry & " "
                '    End If
                'End If
                strQ &= "AND #cteEmp.employeeunkid IS NOT NULL "
                'Sohail (01 Mar 2017) -- End

            'End If 'Sohail (19 Apr 2017)

            If strJobQry.Trim <> "" Then

                strQ &= "UNION ALL " & _
                        "SELECT  @budgetunkid AS budgetunkid  " & _
                              ", '' AS budget_code " & _
                              ", '' AS budget_name " & _
                              ", 0 AS payyearunkid " & _
                              ", 0 AS viewbyid " & _
                              ", 0 AS allocationbyid " & _
                              ", 0 AS presentationmodeid " & _
                              ", 0 AS whotoincludeid " & _
                              ", 0 AS salarylevelid " & _
                              ", CONVERT(CHAR(8), GETDATE(), 112) AS budget_date " & _
                              ", CAST(0 AS BIT) AS isdefault " & _
                              ", Cnt * -1 AS allocationtranunkid " & _
                              ", prtranhead_master.tranheadunkid " & _
                              ", prtranhead_master.trnheadname " & _
                              ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                              ", 0 AS budgettranunkid " & _
                              ", 0 AS budgetcodestranunkid " & _
                              ", #ct.JobUnkId AS jobunkid " & _
                              ", 0 AS amount " & _
                              ", 0 AS budgetamount " & _
                              ", 0 AS salaryamount " & _
                              ", 0 AS budgetsalaryamount " & _
                              ", 0 AS otherpayrollamount " & _
                              ", 0 AS budgetotherpayrollamount " & _
                              ", 0 AS pramount " & _
                              ", CAST(0 AS BIT) AS IsChecked " & _
                              strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", #ct.JobUnkId AS Id " & _
                              ", #ct.Job + ' ' + CAST(Cnt AS NVARCHAR(MAX)) AS GName " & _
                              ", #ct.Job_Code + ' ' + CAST(Cnt AS NVARCHAR(MAX)) AS GCode "
                'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]

                If strEmpJobField.Trim <> "" Then
                    strQ &= ", #ct.JobUnkId AS empjobunkid " & _
                              ", #ct.Job AS EmpJobTitle "
                End If

                strQ &= "FROM    #ct " & _
                                "LEFT JOIN prtranhead_master ON 1 = 1 " & _
                                "LEFT JOIN #ce ON #ce.id = #ct.JobUnkId " & _
                                                "AND #ce.allocationtranunkid = #ct.Cnt * -1 " & _
                                                "AND #ce.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "AND #ce.jobunkid > 0 " & _
                        "WHERE   1 = 1 " & _
                                "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                "AND ( #ce.tranheadunkid IS NULL OR #ce.id IS NULL OR #ce.allocationtranunkid IS NULL ) "
            End If

            Dim arr() As String = Nothing
            If strFundFld.Trim <> "" Then
                arr = strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_").Substring(1).Split(",")

                If strFundFld.Trim <> "" Then
                    For Each strFund As String In arr
                        strQ &= "UPDATE  #bgt SET "

                        strQ &= " " & strFund.Replace("[|_", "[||_") & " = A.fundactivityunkid "

                        strQ &= "FROM    ( SELECT    bgfundactivity_tran.fundactivityunkid  " & _
                                         ", bgfundactivity_tran.fundprojectcodeunkid " & _
                                 "FROM      bgfundactivity_tran " & _
                                 "WHERE     bgfundactivity_tran.isvoid = 0 " & _
                                           "AND bgfundactivity_tran.fundprojectcodeunkid IN ( " & _
                                           "SELECT  bgfundactivity_tran.fundprojectcodeunkid " & _
                                           "FROM    bgfundactivity_tran " & _
                                           "WHERE   bgfundactivity_tran.isvoid = 0 " & _
                                           "AND bgfundactivity_tran.fundprojectcodeunkid = " & CInt(strFund.Replace("[|_", "").Replace("]", "")) & " " & _
                                           "GROUP BY bgfundactivity_tran.fundprojectcodeunkid " & _
                                           "HAVING  COUNT(bgfundactivity_tran.fundactivityunkid) = 1 ) " & _
                               ") AS A " & _
                       "/*WHERE   A.fundprojectcodeunkid = bgbudgetfundsource_tran.fundprojectcodeunkid*/ ; "
                    Next
                End If


            End If

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            'If intBudgetForID = enBudgetViewBy.Employee AndAlso intWhoToIncludeID = enBudgetWhoToInclude.AllAsPerManPowerPlan Then 'Sohail (03 May 2017)

                strQ &= "UPDATE  #bgt " & _
                        "SET     budget_code = AA.budget_code  " & _
                              ", budget_name = AA.budget_name " & _
                              ", payyearunkid = AA.payyearunkid " & _
                              ", viewbyid = AA.viewbyid " & _
                              ", allocationbyid = AA.allocationbyid " & _
                              ", presentationmodeid = AA.presentationmodeid " & _
                              ", whotoincludeid = AA.whotoincludeid " & _
                              ", salarylevelid = AA.salarylevelid " & _
                              ", budget_date = AA.budget_date " & _
                              ", isdefault = AA.isdefault " & _
                        "FROM    ( SELECT TOP 1 " & _
                                            "* " & _
                                  "FROM      #bgt " & _
                                  "WHERE     LTRIM(RTRIM(#bgt.budget_code)) <> '' " & _
                                ") AS AA " & _
                        "WHERE   LTRIM(RTRIM(#bgt.budget_code)) = ''; "

            'End If 'Sohail (03 May 2017)
            'Sohail (01 Mar 2017) -- End

            strQ1 = strQ

            strQ &= "SELECT  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", tranheadunkid " & _
                              ", trnheadname " & _
                              ", IsSalary " & _
                              ", budgettranunkid " & _
                              ", budgetcodestranunkid " & _
                              ", #bgt.jobunkid " & _
                              ", amount " & _
                              ", budgetamount " & _
                              ", salaryamount " & _
                              ", budgetsalaryamount " & _
                              ", otherpayrollamount " & _
                              ", budgetotherpayrollamount " & _
                              ", pramount " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                              ", DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO " & _
                              ", '-' AS Collapse " & _
                        "FROM #bgt "
            'Sohail (01 Nov 2017) - [GCode in DENSE_RANK]
            'Sohail (01 Mar 2017) - [GCode]

            strQ &= " ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode, trnheadname "
            'Sohail (01 Nov 2017) - [GCode in ORDER BY]

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            'dsAll = objDataOperation.ExecQuery(strQ1, strTableName)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'Sohail (01 Mar 2017) -- End

            If blnForBudgetCodes = True Then
                strQ &= "SELECT  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", #bgt.jobunkid " & _
                              ", CASE presentationmodeid WHEN " & enBudgetPresentation.TransactionWise & " THEN SUM(budgetamount) ELSE MAX(#bgt.budgetsalaryamount) + MAX(#bgt.budgetotherpayrollamount) END AS budgetamount " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                              ", DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO " & _
                              ", '-' AS Collapse " & _
                        "FROM #bgt " & _
                        "GROUP BY budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", #bgt.jobunkid " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld
                    'Sohail (01 Nov 2017) - [GCode in DENSE_RANK]

                strQ &= " ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode "
                    'Sohail (01 Nov 2017) - [GCode in ORDER BY]

            End If

            If intPresentationID = enBudgetPresentation.TransactionWise Then

                If blnForBudgetCodes = False Then
                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    'strQ = strQ1
                    'Sohail (01 Mar 2017) -- End
                End If


                'strQ &= " ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, trnheadname "

            Else

                'Dim arr() As String = Nothing
                'If strFundFld.Trim <> "" Then
                '    arr = strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_").Substring(1).Split(",")
                'End If

                strQ &= "UPDATE  #bgt " & _
                        "SET     budgetunkid = A.budgetunkid  " & _
                              ", budget_code = A.budget_code " & _
                              ", budget_name = A.budget_name " & _
                              ", payyearunkid = A.payyearunkid " & _
                              ", viewbyid = A.viewbyid " & _
                              ", allocationbyid = A.allocationbyid " & _
                              ", presentationmodeid = A.presentationmodeid " & _
                              ", whotoincludeid = A.whotoincludeid " & _
                              ", salarylevelid = A.salarylevelid " & _
                              ", budget_date = A.budget_date " & _
                              ", isdefault = A.isdefault " & _
                              ", budgetamount = A.budgetamount " & _
                              ", budgetsalaryamount = A.budgetsalaryamount " & _
                              ", budgetotherpayrollamount = A.budgetotherpayrollamount "

                If strFundFld.Trim <> "" Then
                    For Each strFund As String In arr
                        strQ &= ", " & strFund & " = A." & strFund & " "
                        strQ &= ", " & strFund.Replace("[|_", "[||_") & " = A." & strFund.Replace("[|_", "[||_") & " "
                    Next
                End If

                strQ &= "FROM    ( SELECT    budgetunkid  " & _
                                          ", budget_code " & _
                                          ", budget_name " & _
                                          ", payyearunkid " & _
                                          ", viewbyid " & _
                                          ", allocationbyid " & _
                                          ", presentationmodeid " & _
                                          ", whotoincludeid " & _
                                          ", salarylevelid " & _
                                          ", budget_date " & _
                                          ", isdefault " & _
                                          ", budgetamount " & _
                                          ", budgetsalaryamount " & _
                                          ", budgetotherpayrollamount " & _
                                          ", isSalary " & _
                                          ", Id " & _
                                          ", GName " & _
                                          ", GCode " & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                                  "FROM      #bgt " & _
                                  "WHERE     #bgt.budgettranunkid > 0 " & _
                                  "GROUP BY  Id  " & _
                                          ", Gname " & _
                                          ", GCode " & _
                                          ", budgetunkid " & _
                                          ", budget_code " & _
                                          ", budget_name " & _
                                          ", payyearunkid " & _
                                          ", viewbyid " & _
                                          ", allocationbyid " & _
                                          ", presentationmodeid " & _
                                          ", whotoincludeid " & _
                                          ", salarylevelid " & _
                                          ", budget_date " & _
                                          ", isdefault " & _
                                          ", budgetamount " & _
                                          ", budgetsalaryamount " & _
                                          ", budgetotherpayrollamount " & _
                                          ", isSalary " & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                                ") AS A " & _
                        "WHERE   #bgt.budgettranunkid <= 0 " & _
                                "AND A.isSalary = #bgt.isSalary " & _
                                "AND A.id = #bgt.Id " & _
                                "AND A.GName = #bgt.GName  " & _
                                "AND A.GCode = #bgt.GCode ; "

                strQ &= "SELECT  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", allocationtranunkid " & _
                              "/*, budgettranunkid " & _
                              ", budgetamount*/ " & _
                              ", budgetsalaryamount " & _
                              ", budgetotherpayrollamount " & _
                              ", ISNULL([1], 0) AS [_|1] " & _
                              ", ISNULL([2], 0) AS [_|2] " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                              ", DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO " & _
                              ", '-' AS Collapse " & _
                        "FROM ( SELECT  budgetunkid " & _
                                     ", budget_code " & _
                                     ", budget_name " & _
                                     ", payyearunkid " & _
                                     ", viewbyid " & _
                                     ", allocationbyid " & _
                                     ", presentationmodeid " & _
                                     ", whotoincludeid " & _
                                     ", salarylevelid " & _
                                     ", allocationtranunkid " & _
                                     ", CASE IsSalary " & _
                                          "WHEN 1 THEN 1 " & _
                                          "ELSE 2 " & _
                                        "END AS head  " & _
                                     "/*, budgettranunkid " & _
                                     ", amount " & _
                                     ", budgetamount " & _
                                     ", salaryamount*/ " & _
                                     ", budgetsalaryamount " & _
                                     ", otherpayrollamount " & _
                                     ", budgetotherpayrollamount " & _
                                     ", pramount " & _
                                     ", IsChecked " & _
                                     strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                                     strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                                     ", Id " & _
                                     ", GName " & _
                                     ", GCode " & _
                                     strEmpJobTableFld & _
                                     ", DENSE_RANK() OVER (ORDER BY gname, GCode ) AS ROWNO " & _
                                     ", '-' AS Collapse " & _
                                "FROM ( " & _
                                        "SELECT  * " & _
                                        "FROM    #bgt "
            'Sohail (01 Nov 2017) - [GCode in DENSE_RANK]

                strQ &= ") AS A " & _
                        ") Z PIVOT ( SUM(pramount) FOR head IN ( [1], [2] ) ) AS AA " & _
                        "ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode "
            'Sohail (01 Nov 2017) - [GCode in ORDER BY]

            End If

            strQ &= " DROP TABLE #bgt " & _
                    " DROP TABLE #cteEmp " & _
                    " DROP TABLE #cteFUND " & _
                    " DROP TABLE #cteACTIVITY " & _
                    " DROP TABLE #ctePayroll " & _
                    " DROP TABLE #cte " & _
                    " DROP TABLE #ce "
            'Sohail (01 Mar 2017) - [DROP TABLE #cteFUND, DROP TABLE #cteACTIVITY, DROP TABLE #ctePayroll, DROP TABLE #cte, DROP TABLE #ce, DROP TABLE #ct]


            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            If intBudgetForID = enBudgetViewBy.Employee Then
                strQ &= " DROP TABLE #cteCAT "
            End If

            If strJobQry.Trim <> "" Then
                strQ &= " DROP TABLE #ct "
            End If
            'Sohail (01 Mar 2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            'dsAllHeads = dsAll
            dsAllHeads = New DataSet
            dsAllHeads.Tables.Add(dsList.Tables(0).Copy)
            If dsList.Tables.Count = 2 Then
                dsList.Tables.RemoveAt(0)
                dsList.Tables(0).TableName = strTableName
            End If
            'Sohail (01 Mar 2017) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataGridList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
    Public Function GetBudgetAllocation(ByVal xDatabaseName As String _
                                        , ByVal xUserUnkid As Integer _
                                        , ByVal xYearUnkid As Integer _
                                        , ByVal xCompanyUnkid As Integer _
                                        , ByVal xPeriodStart As DateTime _
                                        , ByVal xPeriodEnd As DateTime _
                                        , ByVal xUserModeSetting As String _
                                        , ByVal xOnlyApproved As Boolean _
                                        , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                        , ByVal mstrAnalysis_Fields As String _
                                        , ByVal mstrAnalysis_Join As String _
                                        , ByVal mstrAnalysis_TableName As String _
                                        , ByVal mstrAnalysis_OrderBy As String _
                                        , ByVal strBudgetcodesUnkIDs As String _
                                        , ByVal intBudgetForID As Integer _
                                        , ByVal intPresentationID As Integer _
                                        , ByVal intWhoToIncludeID As Integer _
                                        , ByVal strTableName As String _
                                        , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                        , Optional ByVal strFilerString As String = "" _
                                        , Optional ByRef dsAllHeads As DataSet = Nothing _
                                        , Optional ByVal blnForBudgetCodes As Boolean = False _
                                        , Optional ByVal mstrAnalysis_CodeField As String = "" _
                                        ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim dsAll As DataSet = Nothing
        Dim strQ As String = ""
        Dim strQ1 As String = ""
        Dim exForce As Exception
        Dim strFundQry As String = ""
        Dim strFundFld As String = ""
        Dim strFundFldIsNULL As String = ""
        Dim strJobQry As String = ""
        Dim strManPwerPlanQry As String = ""
        Dim strEmpJobField As String = ""
        Dim strEmpJobCreate As String = ""
        Dim strEmpJobTableFld As String = ""
        Dim strEmpJobJoin As String = ""
        Dim strEmpEmployeeJoin As String = ""
        Dim strActivityQry As String = ""
        Dim strEmpAnalysisFld As String
        Dim strAnalysis_OrderBy As String
        Dim strAnalysis_Join As String
        Dim strcteFund As String = ""
        Dim strcteActivity As String = ""

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        objDataOperation = New clsDataOperation

        Try

            If intBudgetForID = enBudgetViewBy.Employee Then
                strEmpJobField = ", ISNULL(ERECAT.jobunkid,ISNULL(hremployee_master.jobunkid,0)) As empjobunkid " & _
                                 ", ISNULL(hrjob_master.job_name, ISNULL(EMPJOB.job_name,'')) AS EmpJobTitle "

                strEmpJobCreate = ", empjobunkid INT NULL " & _
                                  ", EmpJobTitle NVARCHAR(MAX) NULL "

                strEmpJobTableFld = ", empjobunkid " & _
                                    ", EmpJobTitle "

                strEmpEmployeeJoin = "LEFT JOIN hremployee_master ON #ce.allocationtranunkid = hremployee_master.employeeunkid "

                strEmpJobJoin &= "       LEFT JOIN #cteCAT AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                                        "LEFT JOIN hrjob_master ON ERECAT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                                        "LEFT JOIN hrjob_master EMPJOB ON hremployee_master.jobunkid = EMPJOB.jobunkid AND EMPJOB.isactive = 1 "

            End If

            strQ = "DECLARE @cols NVARCHAR(MAX) " & _
                   "DECLARE @cols2 NVARCHAR(MAX) " & _
                   "DECLARE @qry NVARCHAR(MAX) " & _
                   "SELECT  @cols = STUFF((SELECT   ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectname) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                           "FROM     bgfundprojectcode_master " & _
                                           "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
                            "FOR           XML PATH('')  " & _
                                            ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                    "SELECT  @cols2 = STUFF((SELECT  ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                            "FROM    bgfundprojectcode_master " & _
                                            "WHERE   bgfundprojectcode_master.isvoid = 0 " & _
                            "FOR            XML PATH('')  " & _
                                             ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                    " " & _
                   "SET @qry = 'SELECT budgettranunkid, ISNULL(budgetcodestranunkid, 0) AS budgetcodestranunkid, ' + @cols + N' INTO #cte# FROM (SELECT ISNULL(bgbudgetcodesfundsource_tran.budgettranunkid, 0) AS budgettranunkid, bgbudgetcodesfundsource_tran.budgetcodestranunkid, bgfundprojectcode_master.fundprojectcodeunkid, ISNULL(bgbudgetcodesfundsource_tran.percentage, 0) AS percentage FROM bgbudgetcodesfundsource_tran " & _
                        "LEFT JOIN bgfundprojectcode_master ON bgbudgetcodesfundsource_tran.fundprojectcodeunkid = bgfundprojectcode_master.fundprojectcodeunkid " & _
                        "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgetcodestranunkid = bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                        "WHERE bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                            "AND bgfundprojectcode_master.isvoid = 0 " & _
                            "AND bgbudgetcodes_tran.isvoid = 0 " & _
                            "AND bgbudgetcodes_tran.budgetcodesunkid IN (" & strBudgetcodesUnkIDs & ") " & _
                        ") AS z " & _
                        "PIVOT (SUM( percentage) FOR fundprojectcodeunkid IN (' + @cols2 + ') ) AS A' "

            strQ &= "SELECT @qry "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 AndAlso IsDBNull(dsList.Tables(0).Rows(0).Item(0)) = False Then
                strFundQry = " LEFT JOIN  #cteFUND AS FundProjectCode ON FundProjectCode.budgetcodestranunkid = ISNULL(bgbudgetcodes_tran.budgetcodestranunkid, 0) "
                strActivityQry = " LEFT JOIN #cteACTIVITY AS FundActivity ON FundActivity.budgetcodestranunkid = ISNULL(bgbudgetcodes_tran.budgetcodestranunkid, 0) "

                strcteFund = dsList.Tables(0).Rows(0).Item(0).ToString
                strcteActivity = dsList.Tables(0).Rows(0).Item(0).ToString.Replace("SUM( percentage)", "MAX( fundactivityunkid)").Replace("percentage", "fundactivityunkid")

                strQ = "DECLARE @colsisnull NVARCHAR(MAX) " & _
                       "DECLARE @cols NVARCHAR(MAX) " & _
                       "DECLARE @codecols NVARCHAR(MAX) " & _
                        " " & _
                        "SELECT  @colsisnull = STUFF((SELECT   ', ISNULL(FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ISNULL(FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectname) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                               "FROM     bgfundprojectcode_master " & _
                                               "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
                                "FOR           XML PATH('')  " & _
                                                ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                        "SELECT  @cols = STUFF((SELECT   ', FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                               "FROM     bgfundprojectcode_master " & _
                                               "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
                                "FOR           XML PATH('')  " & _
                                                ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                        " " & _
                        "SELECT  @colsisnull UNION ALL SELECT  @cols "

                dsList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    strFundFldIsNULL = "," & dsList.Tables(0).Rows(0).Item(0).ToString & " "
                    strFundFld = "," & dsList.Tables(0).Rows(1).Item(0).ToString & " "
                End If
            End If


            strQ = "CREATE TABLE #bgt ( " & _
                              "  budgetunkid INT NULL " & _
                              ", budget_code NVARCHAR(MAX) NULL " & _
                              ", budget_name NVARCHAR(MAX) NULL " & _
                              ", payyearunkid INT NULL " & _
                              ", viewbyid INT NULL " & _
                              ", allocationbyid INT NULL " & _
                              ", presentationmodeid INT NULL " & _
                              ", whotoincludeid INT NULL " & _
                              ", salarylevelid INT NULL " & _
                              ", budget_date NVARCHAR(MAX) NULL " & _
                              ", isdefault BIT NULL " & _
                              ", allocationtranunkid INT NULL " & _
                              ", tranheadunkid INT NULL " & _
                              ", trnheadname NVARCHAR(MAX) NULL " & _
                              ", IsSalary BIT NULL " & _
                              ", budgettranunkid INT NULL " & _
                              ", budgetcodestranunkid INT NULL " & _
                              ", periodunkid INT NULL " & _
                              ", jobunkid INT NULL " & _
                              ", amount DECIMAL(36, 6) NULL " & _
                              ", budgetamount DECIMAL(36, 6) NULL " & _
                              ", salaryamount DECIMAL(36, 6) NULL " & _
                              ", budgetsalaryamount DECIMAL(36, 6) NULL " & _
                              ", otherpayrollamount DECIMAL(36, 6) NULL " & _
                              ", budgetotherpayrollamount DECIMAL(36, 6) NULL " & _
                              ", pramount DECIMAL(36, 6) NULL " & _
                              ", IsChecked BIT NULL " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_").Replace("]", "] DECIMAL(36, 2) NULL ") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_").Replace("]", "] INT NULL DEFAULT 0 ") & _
                              ", Id INT NULL " & _
                              ", GName NVARCHAR(MAX) NULL " & _
                              ", GCode VARCHAR(MAX) " & _
                              strEmpJobCreate & _
                   " ); "

            strQ &= "CREATE TABLE #cteEmp " & _
                    "( " & _
                      "  employeeunkid INT " & _
                      ", firstname VARCHAR(MAX) " & _
                      ", othername VARCHAR(MAX) " & _
                      ", surname VARCHAR(MAX) " & _
                      ", Id INT " & _
                      ", GName VARCHAR(MAX) " & _
                      ", GCode VARCHAR(MAX) " & _
                    ") " & _
                    "INSERT INTO #cteEmp " & _
                    "SELECT hremployee_master.employeeunkid, hremployee_master.firstname, hremployee_master.othername, hremployee_master.surname "

            strQ &= mstrAnalysis_Fields
            strQ &= mstrAnalysis_CodeField

            strQ &= "FROM hremployee_master "

            If intBudgetForID <> enBudgetViewBy.Employee Then
                strQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If intBudgetForID = enBudgetViewBy.Employee Then
                If intWhoToIncludeID = enBudgetWhoToInclude.ActiveEmployee Then

                Else
                    strQ &= mstrAnalysis_Join
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            strQ &= " ; "

            strEmpAnalysisFld = ", #cteEmp.Id AS Id, #cteEmp.GName AS GName, #cteEmp.GCode "
            strAnalysis_Join = mstrAnalysis_Join
            mstrAnalysis_Join = " AND 1 = 1 "
            strAnalysis_OrderBy = mstrAnalysis_OrderBy
            mstrAnalysis_OrderBy = " #cteEmp.Id "

            If strFundQry.Trim <> "" Then
                strQ &= " " & strcteFund.Replace("#cte#", "#cteFUND") & "; "
            End If

            If strActivityQry.Trim <> "" Then
                strQ &= " " & strcteActivity.Replace("#cte#", "#cteACTIVITY") & "; "
            End If



            If intBudgetForID = enBudgetViewBy.Employee Then

                strQ &= " SELECT     Cat.CatEmpId " & _
                                  ", Cat.jobgroupunkid " & _
                                  ", Cat.jobunkid " & _
                                  ", Cat.CEfDt " & _
                                  ", Cat.RECAT_REASON " & _
                                  " INTO #cteCAT " & _
                         "FROM  ( SELECT   ECT.employeeunkid AS CatEmpId " & _
                                        ", ECT.jobgroupunkid " & _
                                        ", ECT.jobunkid " & _
                                        ", CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                                        ", ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                                        ", CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                                 "FROM     hremployee_categorization_tran AS ECT " & _
                                          "LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                          "LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                                 "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                 ") AS Cat WHERE Cat.Rno = 1 "

            End If

            strQ &= " ; "

            strQ &= " SELECT   ISNULL(bgbudgetcodes_master.budgetunkid, bgbudget_master.budgetunkid) AS budgetunkid  " & _
                                          ", bgbudget_master.budget_code " & _
                                          ", bgbudget_master.budget_name " & _
                                          ", bgbudget_master.payyearunkid " & _
                                          ", bgbudget_master.viewbyid " & _
                                          ", bgbudget_master.allocationbyid " & _
                                          ", bgbudget_master.presentationmodeid " & _
                                          ", bgbudget_master.whotoincludeid " & _
                                          ", bgbudget_master.salarylevelid " & _
                                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS budget_date " & _
                                          ", bgbudget_master.isdefault " & _
                                          ", bgbudgetcodes_tran.allocationtranunkid " & _
                                          ", bgbudgetcodes_tran.tranheadunkid " & _
                                          ", prtranhead_master.trnheadname " & _
                                          ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgettranunkid, 0) AS budgettranunkid " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetcodestranunkid, -1) AS budgetcodestranunkid " & _
                                          ", ISNULL(bgbudgetcodes_master.periodunkid, 0) AS periodunkid " & _
                                          ", bgbudgetcodes_tran.jobunkid " & _
                                          ", bgbudgetcodes_tran.amount " & _
                                          ", CASE bgbudget_master.presentationmodeid WHEN 1 THEN ISNULL(bgbudgetcodes_tran.budgetamount, 0) ELSE CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN ISNULL(bgbudgetcodes_tran.budgetsalaryamount, 0) ELSE ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, 0) END END AS budgetamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetsalaryamount, 0) AS budgetsalaryamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, 0) AS budgetotherpayrollamount " & _
                                          ", CAST(0 AS BIT) AS IsChecked " & _
                                          strFundFldIsNULL.Replace("AS [", "AS [|_") & _
                                          strFundFldIsNULL.Replace("FundProjectCode.", "FundActivity.").Replace("AS [", "AS [||_") & _
                                          mstrAnalysis_Fields & _
                                          mstrAnalysis_CodeField & _
                                   "INTO #cte FROM     bgbudget_master " & _
                                            "LEFT JOIN bgbudgetcodes_master ON bgbudget_master.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                            "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                            "LEFT JOIN prtranhead_master ON bgbudgetcodes_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                            "LEFT JOIN " & mstrAnalysis_TableName & " ON bgbudgetcodes_tran.allocationtranunkid = " & strAnalysis_OrderBy & " " & _
                                            strFundQry & _
                                            strActivityQry & _
                                            "LEFT JOIN #cteEmp ON #cteEmp.id = " & strAnalysis_OrderBy & " " & _
                                   "WHERE    bgbudget_master.isvoid = 0 " & _
                                            "AND prtranhead_master.isvoid = 0 " & _
                                            "AND bgbudgetcodes_master.isvoid = 0 " & _
                                            "AND bgbudgetcodes_master.budgetcodesunkid IN (" & strBudgetcodesUnkIDs & ") " & _
                                            "AND bgbudgetcodes_tran.isvoid = 0 " & _
                                            "AND bgbudgetcodes_tran.budgetcodesunkid IN (" & strBudgetcodesUnkIDs & ") " & _
                                            "AND #cteEmp.employeeunkid IS NOT NULL " & _
                                            "AND bgbudgetcodes_tran.allocationtranunkid > 0 " & _
                                   "UNION ALL " & _
                                   "SELECT   ISNULL(bgbudgetcodes_master.budgetunkid, bgbudget_master.budgetunkid) AS budgetunkid  " & _
                                          ", bgbudget_master.budget_code " & _
                                          ", bgbudget_master.budget_name " & _
                                          ", bgbudget_master.payyearunkid " & _
                                          ", bgbudget_master.viewbyid " & _
                                          ", bgbudget_master.allocationbyid " & _
                                          ", bgbudget_master.presentationmodeid " & _
                                          ", bgbudget_master.whotoincludeid " & _
                                          ", bgbudget_master.salarylevelid " & _
                                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS budget_date " & _
                                          ", bgbudget_master.isdefault " & _
                                          ", bgbudgetcodes_tran.allocationtranunkid " & _
                                          ", bgbudgetcodes_tran.tranheadunkid " & _
                                          ", prtranhead_master.trnheadname " & _
                                          ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgettranunkid, 0) AS budgettranunkid " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetcodestranunkid, -1) AS budgetcodestranunkid " & _
                                          ", ISNULL(bgbudgetcodes_master.periodunkid, 0) AS periodunkid " & _
                                          ", bgbudgetcodes_tran.jobunkid " & _
                                          ", bgbudgetcodes_tran.amount " & _
                                          ", CASE bgbudget_master.presentationmodeid WHEN 1 THEN ISNULL(bgbudgetcodes_tran.budgetamount, 0) ELSE CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN ISNULL(bgbudgetcodes_tran.budgetsalaryamount, 0) ELSE ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, 0) END END AS budgetamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetsalaryamount, 0) AS budgetsalaryamount " & _
                                          ", ISNULL(bgbudgetcodes_tran.budgetotherpayrollamount, 0) AS budgetotherpayrollamount " & _
                                          ", CAST(0 AS BIT) AS IsChecked " & _
                                          strFundFldIsNULL.Replace("AS [", "AS [|_") & _
                                          strFundFldIsNULL.Replace("FundProjectCode.", "FundActivity.").Replace("AS [", "AS [||_") & _
                                          ", bgbudgetcodes_tran.jobunkid AS Id " & _
                                          ", hrjob_master.job_name + ' ' + CAST(bgbudgetcodes_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GName " & _
                                          ", hrjob_master.job_code + ' ' + CAST(bgbudgetcodes_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GCode " & _
                                   "FROM    bgbudget_master " & _
                                            "LEFT JOIN bgbudgetcodes_master ON bgbudget_master.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                            "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                            "LEFT JOIN prtranhead_master ON bgbudgetcodes_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                            "LEFT JOIN " & mstrAnalysis_TableName & " ON bgbudgetcodes_tran.allocationtranunkid = " & strAnalysis_OrderBy & " " & _
                                            strFundQry & _
                                            strActivityQry & _
                                            "LEFT JOIN #cteEmp ON #cteEmp.id = " & strAnalysis_OrderBy & " " & _
                                            "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = bgbudgetcodes_tran.jobunkid " & _
                                   "WHERE    bgbudget_master.isvoid = 0 " & _
                                            "AND prtranhead_master.isvoid = 0 " & _
                                            "AND bgbudgetcodes_master.isvoid = 0 " & _
                                            "AND bgbudgetcodes_master.budgetcodesunkid IN (" & strBudgetcodesUnkIDs & ") " & _
                                            "AND bgbudgetcodes_tran.isvoid = 0 " & _
                                            "AND bgbudgetcodes_tran.budgetcodesunkid IN (" & strBudgetcodesUnkIDs & ") " & _
                                            "AND bgbudgetcodes_tran.allocationtranunkid < 0 " & _
                                 "SELECT   * " & _
                                    "INTO #ce FROM     #cte "
            'Sohail (08 Nov 2017) - [CASE WHEN bgbudgetcodes_tran.allocationtranunkid > 0 THEN hremployee_master.employeeunkid ELSE bgbudgetcodes_tran.jobunkid END AS Id] = [bgbudgetcodes_tran.jobunkid AS Id]
            '                       [CASE WHEN bgbudgetcodes_tran.allocationtranunkid > 0 THEN hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname ELSE hrjob_master.job_name + ' ' + CAST(bgbudgetcodes_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) END AS GName] = [hrjob_master.job_name + ' ' + CAST(bgbudgetcodes_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GName]
            '                       [CASE WHEN bgbudgetcodes_tran.allocationtranunkid > 0 THEN hremployee_master.employeecode ELSE hrjob_master.job_code + ' ' + CAST(bgbudgetcodes_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) END AS GCode] = [hrjob_master.job_code + ' ' + CAST(bgbudgetcodes_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GCode]

            If strJobQry.Trim <> "" Then
                strQ &= " " & strManPwerPlanQry
                strQ &= " SELECT * into #ct FROM ct "
            End If

            strQ &= " INSERT INTO #bgt ( " & _
                              "  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", tranheadunkid " & _
                              ", trnheadname " & _
                              ", IsSalary " & _
                              ", budgettranunkid " & _
                              ", budgetcodestranunkid " & _
                              ", periodunkid " & _
                              ", jobunkid " & _
                              ", amount " & _
                              ", budgetamount " & _
                              ", budgetsalaryamount " & _
                              ", budgetotherpayrollamount " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                    " ) " & _
                        "SELECT  #ce.* " & _
                         strEmpJobField.Replace(",0", ",ISNULL(#ce.Id, 0)").Replace(",''", ",SUBSTRING(RTRIM(LTRIM(#ce.GName)), 0, LEN(RTRIM(LTRIM(#ce.GName))) - CHARINDEX(' ', REVERSE(RTRIM(LTRIM(#ce.GName)))) + 1)") & _
                        "FROM    #ce " & _
                        strEmpEmployeeJoin & _
                        strEmpJobJoin



            Dim arr() As String = Nothing
            If strFundFld.Trim <> "" Then
                arr = strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_").Substring(1).Split(",")

                If strFundFld.Trim <> "" Then
                    For Each strFund As String In arr
                        strQ &= "UPDATE  #bgt SET "

                        strQ &= " " & strFund.Replace("[|_", "[||_") & " = A.fundactivityunkid "

                        strQ &= "FROM    ( SELECT    bgfundactivity_tran.fundactivityunkid  " & _
                                         ", bgfundactivity_tran.fundprojectcodeunkid " & _
                                 "FROM      bgfundactivity_tran " & _
                                 "WHERE     bgfundactivity_tran.isvoid = 0 " & _
                                           "AND bgfundactivity_tran.fundprojectcodeunkid IN ( " & _
                                           "SELECT  bgfundactivity_tran.fundprojectcodeunkid " & _
                                           "FROM    bgfundactivity_tran " & _
                                           "WHERE   bgfundactivity_tran.isvoid = 0 " & _
                                           "AND bgfundactivity_tran.fundprojectcodeunkid = " & CInt(strFund.Replace("[|_", "").Replace("]", "")) & " " & _
                                           "GROUP BY bgfundactivity_tran.fundprojectcodeunkid " & _
                                           "HAVING  COUNT(bgfundactivity_tran.fundactivityunkid) = 1 ) " & _
                               ") AS A " & _
                       "/*WHERE   A.fundprojectcodeunkid = bgbudgetfundsource_tran.fundprojectcodeunkid*/ ; "
                    Next
                End If


            End If

            strQ &= "UPDATE  #bgt " & _
                    "SET     budget_code = AA.budget_code  " & _
                          ", budget_name = AA.budget_name " & _
                          ", payyearunkid = AA.payyearunkid " & _
                          ", viewbyid = AA.viewbyid " & _
                          ", allocationbyid = AA.allocationbyid " & _
                          ", presentationmodeid = AA.presentationmodeid " & _
                          ", whotoincludeid = AA.whotoincludeid " & _
                          ", salarylevelid = AA.salarylevelid " & _
                          ", budget_date = AA.budget_date " & _
                          ", isdefault = AA.isdefault " & _
                    "FROM    ( SELECT TOP 1 " & _
                                        "* " & _
                              "FROM      #bgt " & _
                              "WHERE     LTRIM(RTRIM(#bgt.budget_code)) <> '' " & _
                            ") AS AA " & _
                    "WHERE   LTRIM(RTRIM(#bgt.budget_code)) = ''; "


            strQ1 = strQ

            strQ &= "SELECT  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", tranheadunkid " & _
                              ", trnheadname " & _
                              ", IsSalary " & _
                              ", budgettranunkid " & _
                              ", budgetcodestranunkid " & _
                              ", periodunkid " & _
                              ", #bgt.jobunkid " & _
                              ", amount " & _
                              ", budgetamount " & _
                              ", budgetsalaryamount " & _
                              ", budgetotherpayrollamount " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                              ", DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO " & _
                              ", '-' AS Collapse " & _
                        "FROM #bgt "
            'Sohail (01 Nov 2017) - [GCode in DENSE_RANK]

            strQ &= " ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode, trnheadname "
            'Sohail (01 Nov 2017) - [GCode in ORDER BY]

            If blnForBudgetCodes = True Then
                strQ &= "SELECT  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", periodunkid " & _
                              ", #bgt.jobunkid " & _
                              ", CASE presentationmodeid WHEN " & enBudgetPresentation.TransactionWise & " THEN SUM(budgetamount) ELSE MAX(#bgt.budgetsalaryamount) + MAX(#bgt.budgetotherpayrollamount) END AS budgetamount " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                              ", DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO " & _
                              ", '-' AS Collapse " & _
                        "FROM #bgt " & _
                        "GROUP BY budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", periodunkid " & _
                              ", #bgt.jobunkid " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld
            'Sohail (01 Nov 2017) - [GCode in DENSE_RANK]

                strQ &= " ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode "
            'Sohail (01 Nov 2017) - [GCode in ORDER BY]

            End If

            If intPresentationID = enBudgetPresentation.TransactionWise Then

                If blnForBudgetCodes = False Then

                End If

            Else


                strQ &= "UPDATE  #bgt " & _
                        "SET     budgetunkid = A.budgetunkid  " & _
                              ", budget_code = A.budget_code " & _
                              ", budget_name = A.budget_name " & _
                              ", payyearunkid = A.payyearunkid " & _
                              ", viewbyid = A.viewbyid " & _
                              ", allocationbyid = A.allocationbyid " & _
                              ", presentationmodeid = A.presentationmodeid " & _
                              ", whotoincludeid = A.whotoincludeid " & _
                              ", salarylevelid = A.salarylevelid " & _
                              ", budget_date = A.budget_date " & _
                              ", isdefault = A.isdefault " & _
                              ", budgetamount = A.budgetamount " & _
                              ", budgetsalaryamount = A.budgetsalaryamount " & _
                              ", budgetotherpayrollamount = A.budgetotherpayrollamount "

                If strFundFld.Trim <> "" Then
                    For Each strFund As String In arr
                        strQ &= ", " & strFund & " = A." & strFund & " "
                        strQ &= ", " & strFund.Replace("[|_", "[||_") & " = A." & strFund.Replace("[|_", "[||_") & " "
                    Next
                End If

                strQ &= "FROM    ( SELECT    budgetunkid  " & _
                                          ", budget_code " & _
                                          ", budget_name " & _
                                          ", payyearunkid " & _
                                          ", viewbyid " & _
                                          ", allocationbyid " & _
                                          ", presentationmodeid " & _
                                          ", whotoincludeid " & _
                                          ", salarylevelid " & _
                                          ", budget_date " & _
                                          ", isdefault " & _
                                          ", budgetamount " & _
                                          ", budgetsalaryamount " & _
                                          ", budgetotherpayrollamount " & _
                                          ", isSalary " & _
                                          ", Id " & _
                                          ", GName " & _
                                          ", GCode " & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                                  "FROM      #bgt " & _
                                  "WHERE     #bgt.budgettranunkid > 0 " & _
                                  "GROUP BY  Id  " & _
                                          ", Gname " & _
                                          ", GCode " & _
                                          ", budgetunkid " & _
                                          ", budget_code " & _
                                          ", budget_name " & _
                                          ", payyearunkid " & _
                                          ", viewbyid " & _
                                          ", allocationbyid " & _
                                          ", presentationmodeid " & _
                                          ", whotoincludeid " & _
                                          ", salarylevelid " & _
                                          ", budget_date " & _
                                          ", isdefault " & _
                                          ", budgetamount " & _
                                          ", budgetsalaryamount " & _
                                          ", budgetotherpayrollamount " & _
                                          ", isSalary " & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                                ") AS A " & _
                        "WHERE   #bgt.budgettranunkid <= 0 " & _
                                "AND A.isSalary = #bgt.isSalary " & _
                                "AND A.id = #bgt.Id " & _
                                "AND A.GName = #bgt.GName  " & _
                                "AND A.GCode = #bgt.GCode ; "

                strQ &= "SELECT  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", allocationtranunkid " & _
                              "/*, budgettranunkid " & _
                              ", budgetamount*/ " & _
                              ", budgetsalaryamount " & _
                              ", budgetotherpayrollamount " & _
                              ", ISNULL([1], 0) AS [_|1] " & _
                              ", ISNULL([2], 0) AS [_|2] " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                              ", DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO " & _
                              ", '-' AS Collapse " & _
                        "FROM ( SELECT  budgetunkid " & _
                                     ", budget_code " & _
                                     ", budget_name " & _
                                     ", payyearunkid " & _
                                     ", viewbyid " & _
                                     ", allocationbyid " & _
                                     ", presentationmodeid " & _
                                     ", whotoincludeid " & _
                                     ", salarylevelid " & _
                                     ", allocationtranunkid " & _
                                     ", CASE IsSalary " & _
                                          "WHEN 1 THEN 1 " & _
                                          "ELSE 2 " & _
                                        "END AS head  " & _
                                     "/*, budgettranunkid " & _
                                     ", amount " & _
                                     ", budgetamount " & _
                                     ", salaryamount*/ " & _
                                     ", budgetsalaryamount " & _
                                     ", otherpayrollamount " & _
                                     ", budgetotherpayrollamount " & _
                                     ", pramount " & _
                                     ", IsChecked " & _
                                     strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                                     strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                                     ", Id " & _
                                     ", GName " & _
                                     ", GCode " & _
                                     strEmpJobTableFld & _
                                     ", DENSE_RANK() OVER (ORDER BY gname, GCode ) AS ROWNO " & _
                                     ", '-' AS Collapse " & _
                                "FROM ( " & _
                                        "SELECT  * " & _
                                        "FROM    #bgt "
            'Sohail (01 Nov 2017) - [GCode in DENSE_RANK]

                strQ &= ") AS A " & _
                        ") Z PIVOT ( SUM(pramount) FOR head IN ( [1], [2] ) ) AS AA " & _
                        "ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode, Periodunkid "
            'Sohail (01 Nov 2017) - [GCode in ORDER BY]

            End If

            strQ &= " DROP TABLE #bgt " & _
                    " DROP TABLE #cteEmp " & _
                    " DROP TABLE #cteFUND " & _
                    " DROP TABLE #cteACTIVITY " & _
                    " DROP TABLE #cte " & _
                    " DROP TABLE #ce "

            If intBudgetForID = enBudgetViewBy.Employee Then
                strQ &= " DROP TABLE #cteCAT "
            End If

            If strJobQry.Trim <> "" Then
                strQ &= " DROP TABLE #ct "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsAllHeads = New DataSet
            dsAllHeads.Tables.Add(dsList.Tables(0).Copy)
            If dsList.Tables.Count = 2 Then
                dsList.Tables.RemoveAt(0)
                dsList.Tables(0).TableName = strTableName
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBudgetAllocation; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (13 Oct 2017) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetcodes_master) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mintBudgetunkid, mintPeriodUnkId) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Budget Codes for selected period already exists.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgbudgetcodes_master ( " & _
              "  budgetunkid " & _
              ", periodunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @budgetunkid " & _
              ", @periodunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetcodesunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditBudgetCodeMaster(objDataOperation, enAuditType.ADD, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function InsertAll(ByVal dtBudgetTable As DataTable, ByVal dtHeadTable As DataTable, ByVal strPeriodIDs As String, ByVal dtCurrentDateAndTime As Date, Optional ByVal bw As BackgroundWorker = Nothing) As Boolean
        'Sohail (01 May 2021) - [bw]
        Dim objDataOperation As clsDataOperation
        Dim objBudgetCodesTran As New clsBudgetcodes_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim intBudgetID As Integer = 0
        Dim blnFlag As Boolean = False

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Dim intProgress As Integer = 0 'Sohail (01 May 2021)
        Try


            If Insert(dtCurrentDateAndTime, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objBudgetCodesTran._Budgetunkid = mintBudgetunkid
            objBudgetCodesTran._PeriodUnkId = mintPeriodUnkId
            objBudgetCodesTran._Userunkid = mintUserunkid
            objBudgetCodesTran._Isvoid = mblnIsvoid
            objBudgetCodesTran._Voiduserunkid = mintVoiduserunkid
            objBudgetCodesTran._Voiddatetime = mdtVoiddatetime
            objBudgetCodesTran._Voidreason = mstrVoidreason
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            objBudgetCodesTran._Budgetcodesunkid = mintBudgetcodesunkid
            'Sohail (13 Oct 2017) -- End
            If objBudgetCodesTran.InsertAll(objDataOperation, dtBudgetTable, dtCurrentDateAndTime, bw, intProgress) = False Then
                'Sohail (01 May 2021) - [bw, intProgress]
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'objBudgetPeriod._Budgetunkid = mintBudgetunkid
            'objBudgetPeriod._Userunkid = mintUserunkid
            'objBudgetPeriod._Isvoid = mblnIsvoid
            'objBudgetPeriod._Voiduserunkid = mintVoiduserunkid
            'objBudgetPeriod._Voiddatetime = mdtVoiddatetime
            'objBudgetPeriod._Voidreason = mstrVoidreason
            'If objBudgetPeriod.InsertAll(objDataOperation, strPeriodIDs) = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'End If

            'objBudgetHeadMapping._Budgetunkid = mintBudgetunkid
            'objBudgetHeadMapping._Userunkid = mintUserunkid
            'objBudgetHeadMapping._Isvoid = mblnIsvoid
            'objBudgetHeadMapping._Voiduserunkid = mintVoiduserunkid
            'objBudgetHeadMapping._Voiddatetime = mdtVoiddatetime
            'objBudgetHeadMapping._Voidreason = mstrVoidreason
            'If objBudgetHeadMapping.InsertAll(objDataOperation, dtHeadTable) = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudgetcodes_master) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mintBudgetunkid, mintPeriodUnkId, mintBudgetcodesunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Budget Codes for selected period already exists.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodesunkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetcodes_master SET " & _
              "  budgetunkid = @budgetunkid" & _
              ", periodunkid = @periodunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE budgetcodesunkid = @budgetcodesunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditBudgetCodeMaster(objDataOperation, enAuditType.EDIT, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function UpdateAll(ByVal dtBudgetTable As DataTable, ByVal dtHeadTable As DataTable, ByVal strPeriodIDs As String, ByVal dtCurrentDateTime As Date, ByVal strBudgetCodesTranUnkIDs As String) As Boolean
        'Sohail (03 May 2017) - [strBudgetTranUnkIDs = strBudgetCodesTranUnkIDs]
        'Sohail (19 Apr 2017) - [strBudgetTranUnkIDs]

        Dim objDataOperation As clsDataOperation
        Dim objBudgetCodesTran As New clsBudgetcodes_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objBudgetCodesFundSource As New clsBudgetCodesfundsource_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim intBudgetID As Integer = 0
        Dim blnFlag As Boolean = False

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If Update(dtCurrentDateTime, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (19 Apr 2017) -- Start
            'MST Enhancement - 66.1 - applying user access filter on payroll budget.
            'If objBudgetCodesFundSource.VoidAllByBudgetUnkId(mintBudgetunkid, mintPeriodUnkId, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            'If objBudgetCodesFundSource.VoidAllByBudgetTranUnkId(strBudgetTranUnkIDs, mintPeriodUnkId, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
            If objBudgetCodesFundSource.VoidAllByBudgetCodesTranUnkId(strBudgetCodesTranUnkIDs, mintPeriodUnkId, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
                'Sohail (03 May 2017) -- End
                'Sohail (19 Apr 2017) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (19 Apr 2017) -- Start
            'MST Enhancement - 66.1 - applying user access filter on payroll budget.
            'If objBudgetCodesTran.VoidAllByBudgetUnkId(mintBudgetunkid, mintPeriodUnkId, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            'If objBudgetCodesTran.VoidAllByBudgetTranUnkId(strBudgetTranUnkIDs, mintPeriodUnkId, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
            If objBudgetCodesTran.VoidAllByBudgetCodesTranUnkId(strBudgetCodesTranUnkIDs, mintPeriodUnkId, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
                'Sohail (03 May 2017) -- End
                'Sohail (19 Apr 2017) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            objBudgetCodesTran._Budgetunkid = mintBudgetunkid
            objBudgetCodesTran._PeriodUnkId = mintPeriodUnkId
            objBudgetCodesTran._Userunkid = mintUserunkid
            objBudgetCodesTran._Isvoid = mblnIsvoid
            objBudgetCodesTran._Voiduserunkid = mintVoiduserunkid
            objBudgetCodesTran._Voiddatetime = mdtVoiddatetime
            objBudgetCodesTran._Voidreason = mstrVoidreason
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            objBudgetCodesTran._Budgetcodesunkid = mintBudgetcodesunkid
            'Sohail (13 Oct 2017) -- End
            If objBudgetCodesTran.InsertAll(objDataOperation, dtBudgetTable, dtCurrentDateTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudgetcodes_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objDataOperation As clsDataOperation

        Dim objBudgetCodesTran As New clsBudgetcodes_Tran
        Dim objBudgetCodesFundSource As New clsBudgetCodesfundsource_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetcodes_master SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                "WHERE isvoid = 0 " & _
                        "AND budgetcodesunkid = @budgetcodesunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._Budgetcodesunkid(objDataOperation) = intUnkid

            If InsertAuditBudgetCodeMaster(objDataOperation, enAuditType.DELETE, dtCurrentDateAndTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objBudgetCodesFundSource.VoidAllByBudgetUnkId(mintBudgetunkid, mintPeriodUnkId, mintUserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objBudgetCodesTran.VoidAllByBudgetUnkId(mintBudgetunkid, mintPeriodUnkId, mintUserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intBudgetUnkId As Integer, ByVal intPeriodUnkId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetcodesunkid " & _
              ", budgetunkid " & _
              ", periodunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetcodes_master " & _
             "WHERE isvoid = 0 " & _
             "AND budgetunkid = @budgetunkid " & _
             "AND periodunkid = @periodunkid "

            If intUnkid > 0 Then
                strQ &= " AND budgetcodesunkid <> @budgetcodesunkid"
            End If

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            objDataOperation.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (01 Mar 2017) -- Start
    'Enhancement - 65.1 - Export and Import option on Budget Codes.
    Public Function isExists(ByVal strFilter As String, Optional ByRef intrefBudgetCodesUnkid As Integer = 0, Optional ByRef intrefBudgetUnkid As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        intrefBudgetCodesUnkid = 0
        intrefBudgetUnkid = 0

        Try
            strQ = "SELECT  bgbudgetcodes_master.budgetcodesunkid " & _
                          ", bgbudgetcodes_master.budgetunkid " & _
                          ", bgbudgetcodes_master.periodunkid " & _
                          ", bgbudgetcodes_master.userunkid " & _
                          ", bgbudgetcodes_master.isvoid " & _
                          ", bgbudgetcodes_master.voiduserunkid " & _
                          ", bgbudgetcodes_master.voiddatetime " & _
                          ", bgbudgetcodes_master.voidreason " & _
                    "FROM bgbudgetcodes_master " & _
                            "LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                    "WHERE bgbudgetcodes_master.isvoid = 0 " & _
                            "AND bgbudget_master.isvoid = 0 "

            If strFilter.Trim <> "" Then
                strQ &= strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intrefBudgetCodesUnkid = CInt(dsList.Tables(0).Rows(0).Item("budgetcodesunkid"))
                intrefBudgetUnkid = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExists; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (01 Mar 2017) -- End

    'Sohail (01 May 2021) -- Start
    'IHI Issue : : Project codes are missing for some employees on editong budget codes.
    Public Function Save(ByVal dtBudgetTable As DataTable, ByVal dtCurrentDateTime As Date, ByVal strBudgetCodesTranUnkIDs As String, Optional ByVal bw As BackgroundWorker = Nothing) As Boolean
        'Sohail (03 May 2017) - [strBudgetTranUnkIDs = strBudgetCodesTranUnkIDs, bw]
        'Sohail (19 Apr 2017) - [strBudgetTranUnkIDs]

        Dim objDataOperation As clsDataOperation
        Dim objBudgetCodesTran As New clsBudgetcodes_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objBudgetCodesFundSource As New clsBudgetCodesfundsource_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim intBudgetID As Integer = 0
        Dim blnFlag As Boolean = False

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Dim intProgress As Integer = 0
        Try

            If Update(dtCurrentDateTime, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If


            'If objBudgetCodesFundSource.VoidAllByBudgetCodesTranUnkId(strBudgetCodesTranUnkIDs, mintPeriodUnkId, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'End If

            'If objBudgetCodesTran.VoidAllByBudgetCodesTranUnkId(strBudgetCodesTranUnkIDs, mintPeriodUnkId, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'End If
            Dim dsFundSource As DataSet = objBudgetCodesFundSource.GetList("List", " AND budgetcodestranunkid IN (" & strBudgetCodesTranUnkIDs & ") ", objDataOperation)
            Dim dsATFundSource As DataSet = objBudgetCodesFundSource.GetLastATRecords("List", " AND budgetcodestranunkid IN (" & strBudgetCodesTranUnkIDs & ") ", objDataOperation)
            objBudgetCodesTran._Budgetunkid = mintBudgetunkid
            objBudgetCodesTran._PeriodUnkId = mintPeriodUnkId
            objBudgetCodesTran._Userunkid = mintUserunkid
            objBudgetCodesTran._Isvoid = mblnIsvoid
            objBudgetCodesTran._Voiduserunkid = mintVoiduserunkid
            objBudgetCodesTran._Voiddatetime = mdtVoiddatetime
            objBudgetCodesTran._Voidreason = mstrVoidreason
            objBudgetCodesTran._Budgetcodesunkid = mintBudgetcodesunkid
            If objBudgetCodesTran.Save(objDataOperation, dtBudgetTable, dtCurrentDateTime, dsFundSource, dsATFundSource, bw, intProgress) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (01 May 2021) -- End

    Public Function GetPreviuosPeriodBudgetCodes(ByVal strTableName As String _
                                                , ByVal intPeriodId As Integer _
                                                ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT TOP 1 " & _
                            "bgbudgetcodes_master.* " & _
                    "FROM    bgbudgetcodes_master " & _
                            "LEFT JOIN cfcommon_period_tran ON bgbudgetcodes_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "WHERE   bgbudgetcodes_master.isvoid = 0 " & _
                            "AND cfcommon_period_tran.isactive = 1 " & _
                            "AND cfcommon_period_tran.modulerefid = 1 " & _
                            "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) < ( SELECT " & _
                                                                                  "CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) " & _
                                                                                  "FROM " & _
                                                                                  "cfcommon_period_tran " & _
                                                                                  "WHERE " & _
                                                                                  "periodunkid = @periodunkid " & _
                                                                                  ") " & _
                            "ORDER BY cfcommon_period_tran.end_date DESC "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function InsertAuditBudgetCodeMaster(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType, ByVal dtCurrentDateAndTime As Date) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = "INSERT INTO  atbgbudgetcodes_master ( " & _
                                "  budgetcodesunkid " & _
                                ", budgetunkid " & _
                                ", periodunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                ", module_name1 " & _
                                ", module_name2 " & _
                                ", module_name3 " & _
                                ", module_name4 " & _
                                ", module_name5 " & _
                                ", isweb " & _
                ") VALUES (" & _
                                "  @budgetcodesunkid " & _
                                ", @budgetunkid " & _
                                ", @periodunkid " & _
                                ", @audittype " & _
                                ", @audituserunkid " & _
                                ", @auditdatetime " & _
                                ", @ip " & _
                                ", @machine_name " & _
                                ", @form_name " & _
                                ", @module_name1 " & _
                                ", @module_name2 " & _
                                ", @module_name3 " & _
                                ", @module_name4 " & _
                                ", @module_name5 " & _
                                ", @isweb " & _
            "); SELECT @@identity"

            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodesunkid.ToString)
            xDataOpr.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            xDataOpr.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)

            If mstrWebIP.ToString().Trim.Length <= 0 Then
                mstrWebIP = getIP()
            End If
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)

            If mstrWebHost.ToString().Length <= 0 Then
                mstrWebHost = getHostName()
            End If
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHost)

            If mstrWebFormName.Trim.Length <= 0 Then
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            xDataOpr.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            xDataOpr.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            xDataOpr.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            xDataOpr.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditBudgetCodeMaster; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    Public Function GetComboListPeriod(ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = True, Optional ByVal intBudgetUnkId As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If blnAddSelect = True Then
                strQ = "SELECT 0 AS periodunkid, '' AS Code, @Select AS Name, '19000101' AS start_date, '19000101' AS end_date UNION "
            End If

            strQ &= "SELECT DISTINCT " & _
                                "bgbudgetcodes_master.periodunkid  " & _
                              ", cfcommon_period_tran.period_code AS code " & _
                              ", cfcommon_period_tran.period_name AS name " & _
                              ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                              ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                        "FROM    bgbudgetcodes_master " & _
                                "LEFT JOIN cfcommon_period_tran ON bgbudgetcodes_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE   bgbudgetcodes_master.isvoid = 0 " & _
                                "AND cfcommon_period_tran.isactive = 1 "


            If intBudgetUnkId > 0 Then
                strQ &= "AND bgbudgetcodes_master.budgetunkid = @budgetunkid "
                objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, " Select"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboListPeriod; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function



    'Pinkal (21-Oct-2016) -- Start
    'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeActivityPercentage(ByVal xPeriodId As Integer, ByVal xDefaultBudgetId As Integer, ByVal xActivityId As Integer, Optional ByVal xEmployeeID As Integer = 0, Optional ByVal strEmpIDs As String = "", Optional ByVal blnOnlyHavingPercentage As Integer = -1, Optional ByVal strBudgetCodesUnkIDs As String = "", Optional ByVal colSQLFilter As List(Of clsSQLFilterCollection) = Nothing) As DataSet
        'Sohail (13 Oct 2017) - [strBudgetCodesUnkIDs, colSQLFilter]
        'Sohail (23 May 2017) - [strEmpIDs, blnOnlyHavingPercentage]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            'strQ = " SELECT DISTINCT " & _
            '          " bgbudgetcodesfundsource_tran.periodunkid " & _
            '          ",bgbudgetcodesfundsource_tran.fundactivityunkid " & _
            '          ",bgbudgetcodesfundsource_tran.fundprojectcodeunkid " & _
            '          ",bgbudget_tran.allocationtranunkid AS Employeeunkid " & _
            '          ",bgbudgetcodesfundsource_tran.percentage " & _
            '          " FROM bgbudgetcodesfundsource_tran " & _
            '          " JOIN bgbudget_tran ON bgbudget_tran.budgettranunkid = bgbudgetcodesfundsource_tran.budgettranunkid AND bgbudget_tran.isvoid = 0 " & _
            '          " JOIN bgbudget_master ON bgbudget_master.budgetunkid = bgbudget_tran.budgetunkid AND bgbudget_master.isvoid = 0 " & _
            '          " WHERE bgbudgetcodesfundsource_tran.isvoid = 0 And bgbudget_master.budgetunkid = @budgetunkid AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid " & _
            '          " AND bgbudgetcodesfundsource_tran.fundactivityunkid = @fundactivityunkid "
            strQ = " SELECT DISTINCT " & _
                      " bgbudgetcodesfundsource_tran.periodunkid " & _
                      ",bgbudgetcodesfundsource_tran.fundactivityunkid " & _
                      ",bgbudgetcodesfundsource_tran.fundprojectcodeunkid " & _
                      ",bgbudgetcodes_tran.allocationtranunkid AS Employeeunkid " & _
                      ",bgbudgetcodesfundsource_tran.percentage " & _
                      " FROM bgbudgetcodesfundsource_tran " & _
                      " JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgetcodestranunkid = bgbudgetcodesfundsource_tran.budgetcodestranunkid AND bgbudgetcodes_tran.isvoid = 0 " & _
                      " JOIN bgbudget_master ON bgbudget_master.budgetunkid = bgbudgetcodes_tran.budgetunkid AND bgbudget_master.isvoid = 0 " & _
                      " WHERE bgbudgetcodesfundsource_tran.isvoid = 0 "
            'Sohail (13 Oct 2017) - [Removed - And bgbudget_master.budgetunkid = @budgetunkid AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid]
            'Sohail (03 May 2017) -- End

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            If xPeriodId > 0 Then
                strQ &= " AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid "
            End If

            If xDefaultBudgetId > 0 Then
                strQ &= " AND bgbudget_master.budgetunkid = @budgetunkid "
            End If

            If strBudgetCodesUnkIDs.Trim.Length > 0 Then
                strQ &= " AND bgbudgetcodes_tran.budgetcodesunkid IN (" & strBudgetCodesUnkIDs & ") "
            End If

            If colSQLFilter IsNot Nothing Then
                For Each item As clsSQLFilterCollection In colSQLFilter
                    strQ += " " & item._Query & " "
                    If item._ParameterName.ToString().Trim.Length > 0 Then
                        objDataOperation.AddParameter(item._ParameterName, item._SQLDbType, item._eZeeDataType, item._ParameterValue)
                    End If
                Next
            End If
            'Sohail (13 Oct 2017) -- End

            If xEmployeeID > 0 Then
                'Sohail (03 May 2017) -- Start
                'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
                'strQ &= " AND bgbudget_tran.allocationtranunkid = @employeeunkid "
                strQ &= " AND bgbudgetcodes_tran.allocationtranunkid = @employeeunkid "
                'Sohail (03 May 2017) -- End
            End If

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If xActivityId > 0 Then
                strQ &= " AND bgbudgetcodesfundsource_tran.fundactivityunkid = @fundactivityunkid "
            End If

            If strEmpIDs.Trim <> "" Then
                strQ &= " AND bgbudgetcodes_tran.allocationtranunkid IN (" & strEmpIDs & ") "
            End If

            If blnOnlyHavingPercentage = 0 Then
                strQ &= " AND bgbudgetcodesfundsource_tran.percentage <= 0 "
            ElseIf blnOnlyHavingPercentage = 1 Then
                strQ &= " AND bgbudgetcodesfundsource_tran.percentage > 0 "
            End If
            'Sohail (23 May 2017) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDefaultBudgetId)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xActivityId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeID)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeActivityPercentage; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Pinkal (21-Oct-2016) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Budget Codes for selected period already exists.")
            Language.setMessage(mstrModuleName, 2, "WEB")
            Language.setMessage(mstrModuleName, 3, " Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class