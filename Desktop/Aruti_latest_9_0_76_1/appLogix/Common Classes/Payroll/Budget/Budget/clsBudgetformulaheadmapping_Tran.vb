﻿'************************************************************************************************************************************
'Class Name : clsBudgetformulaheadmapping_Tran.vb
'Purpose    :
'Date       :07/06/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudgetformulaheadmapping_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetformulaheadmapping_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetformulaheadmappingtranunkid As Integer
    Private mintBudgetunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintMappedformulaheadunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrWebFormName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebHost As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetformulaheadmappingtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetformulaheadmappingtranunkid() As Integer
        Get
            Return mintBudgetformulaheadmappingtranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetformulaheadmappingtranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetunkid() As Integer
        Get
            Return mintBudgetunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappedformulaheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Mappedformulaheadunkid() As Integer
        Get
            Return mintMappedformulaheadunkid
        End Get
        Set(ByVal value As Integer)
            mintMappedformulaheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Public WriteOnly Property _WebFormName() As String
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHost() As String
        Set(ByVal value As String)
            mstrWebHost = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  budgetformulaheadmappingtranunkid " & _
              ", budgetunkid " & _
              ", tranheadunkid " & _
              ", mappedformulaheadunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetformulaheadmapping_tran " & _
             "WHERE budgetformulaheadmappingtranunkid = @budgetformulaheadmappingtranunkid "

            objDataOperation.AddParameter("@budgetformulaheadmappingtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBudgetformulaheadmappingTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbudgetformulaheadmappingtranunkid = CInt(dtRow.Item("budgetformulaheadmappingtranunkid"))
                mintbudgetunkid = CInt(dtRow.Item("budgetunkid"))
                minttranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintmappedformulaheadunkid = CInt(dtRow.Item("mappedformulaheadunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetformulaheadmappingtranunkid " & _
              ", budgetunkid " & _
              ", tranheadunkid " & _
              ", mappedformulaheadunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetformulaheadmapping_tran " & _
             "WHERE isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetHeadMapping(ByVal intBudgetUnkId As Integer) As Dictionary(Of Integer, Integer)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim dicMapping As New Dictionary(Of Integer, Integer)
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  tranheadunkid " & _
              ", mappedformulaheadunkid " & _
             "FROM bgbudgetformulaheadmapping_tran " & _
             "WHERE isvoid = 0 " & _
             "AND budgetunkid = @budgetunkid "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dicMapping = (From p In dsList.Tables(0) Select New With {.headid = CInt(p.Item("tranheadunkid")), .mappedheadid = CInt(p.Item("mappedformulaheadunkid"))}).ToDictionary(Function(x) x.headid, Function(y) y.mappedheadid)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHeadMapping; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dicMapping
    End Function

    Public Function GetListByBudgetUnkId(ByVal strTableName As String _
                                         , ByVal intBudgetUnkId As Integer _
                                         , ByVal dtEffectiveDate As Date _
                                         , Optional ByVal strFilter As String = "" _
                                         , Optional ByVal blnIncludeUnMappedHeads As Boolean = False _
                                         ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim objMaster As New clsMasterData
            Dim dsHeadType As DataSet = objMaster.getComboListForHeadType("HeadType")
            Dim dicHeadType As Dictionary(Of Integer, String) = (From p In dsHeadType.Tables("HeadType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            strQ = "WITH    cte " & _
                              "AS ( SELECT   budgetformulaheadmappingtranunkid " & _
                                            ", budgetunkid " & _
                                            ", budget_code " & _
                                            ", budget_name " & _
                                            ", tranheadunkid " & _
                                            ", HeadCode " & _
                                            ", HeadName " & _
                                            ", trnheadtype_id " & _
                                            ", mappedformulaheadunkid " & _
                                            ", MappedHeadCode " & _
                                            ", MappedHeadName " & _
                                            ", formulaid " & _
                                            ", formula " & _
                                            ", IsChecked " & _
                                            ", IsSalary " & _
                                            ", ROWNO "

            strQ &= ", CASE trnheadtype_id "
            For Each pair In dicHeadType
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS trnheadtype_name "

            strQ &= "               FROM ( " & _
                                                "SELECT   bgbudgetformulaheadmapping_tran.budgetformulaheadmappingtranunkid  " & _
                                                          ", bgbudgetformulaheadmapping_tran.budgetunkid " & _
                                                          ", bgbudget_master.budget_code " & _
                                                          ", bgbudget_master.budget_name " & _
                                                          ", bgbudgetformulaheadmapping_tran.tranheadunkid " & _
                                                          ", prtranhead_master.trnheadcode AS HeadCode " & _
                                                          ", prtranhead_master.trnheadname AS HeadName " & _
                                                          ", prtranhead_master.trnheadtype_id " & _
                                                          ", bgbudgetformulaheadmapping_tran.mappedformulaheadunkid " & _
                                                          ", ISNULL(bgbudgetformula_master.Formula_Code, '') AS MappedHeadCode " & _
                                                          ", ISNULL(bgbudgetformula_master.Formula_Name, '') AS MappedHeadName " & _
                                                          ", ISNULL(bgbudgetformula_tran.formulaid, '') AS formulaid " & _
                                                          ", ISNULL(bgbudgetformula_tran.formula, '') AS formula " & _
                                                          ", CAST(1 AS BIT) AS IsChecked " & _
                                                          ", CASE WHEN prtranhead_master.typeof_id = " & CInt(enTypeOf.Salary) & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                                          ", DENSE_RANK() OVER ( PARTITION BY bgbudgetformula_master.budgetformulaunkid ORDER BY bgbudgetformula_tran.effective_date DESC, bgbudgetformula_tran.budgetformulatranunkid DESC ) AS ROWNO " & _
                                                   "FROM     bgbudgetformulaheadmapping_tran " & _
                                                            "LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid = bgbudgetformulaheadmapping_tran.budgetunkid " & _
                                                            "LEFT JOIN prtranhead_master ON bgbudgetformulaheadmapping_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                            "LEFT JOIN bgbudgetformula_master ON bgbudgetformulaheadmapping_tran.mappedformulaheadunkid = bgbudgetformula_master.budgetformulaunkid " & _
                                                            "LEFT JOIN bgbudgetformula_tran ON bgbudgetformula_master.budgetformulaunkid = bgbudgetformula_tran.budgetformulaunkid " & _
                                                   "WHERE    bgbudgetformulaheadmapping_tran.isvoid = 0 " & _
                                                            "AND prtranhead_master.isvoid = 0 " & _
                                                            "AND ISNULL(bgbudgetformula_master.isvoid, 0) = 0 " & _
                                                            "AND ISNULL(bgbudgetformula_tran.isvoid, 0) = 0 " & _
                                                            "AND bgbudgetformulaheadmapping_tran.budgetunkid = @budgetunkid " & _
                                                            "AND CONVERT(CHAR(8), ISNULL(bgbudgetformula_tran.effective_date, @effective_date), 112) <= @effective_date "
            'Sohail (15 Sep 2017) - [CASE prtranhead_master.typeof_id WHEN " & enTypeOf.Salary & " THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary] = [CASE WHEN prtranhead_master.typeof_id = " & CInt(enTypeOf.Salary) & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary]

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "            ) AS A " & _
                                "WHERE A.ROWNO = 1 " & _
                            ") " & _
                        "SELECT  * " & _
                        "FROM    cte "

            If blnIncludeUnMappedHeads = True Then

                strQ &= "    UNION ALL " & _
                            "SELECT  -1 AS budgetformulaheadmappingtranunkid  " & _
                                  ", -1 AS budgetunkid " & _
                                  ", '' AS budget_code " & _
                                  ", '' AS budget_name " & _
                                  ", prtranhead_master.tranheadunkid " & _
                                  ", prtranhead_master.trnheadcode AS HeadCode " & _
                                  ", prtranhead_master.trnheadname AS HeadName " & _
                                  ", prtranhead_master.trnheadtype_id " & _
                                  ", 0 AS mappedformulaheadunkid " & _
                                  ", '' AS MappedHeadCode " & _
                                  ", '' AS MappedHeadName " & _
                                  ", '' AS formulaid " & _
                                  ", '' AS formula " & _
                                  ", CAST(0 AS BIT) AS IsChecked " & _
                                  ", CASE WHEN prtranhead_master.typeof_id = " & CInt(enTypeOf.Salary) & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                  ", 1 AS ROWNO "
                'Sohail (15 Sep 2017) - [CASE prtranhead_master.typeof_id WHEN " & enTypeOf.Salary & " THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary] = [CASE WHEN prtranhead_master.typeof_id = " & CInt(enTypeOf.Salary) & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary]

                strQ &= ", CASE prtranhead_master.trnheadtype_id "
                For Each pair In dicHeadType
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                strQ &= " END AS trnheadtype_name "

                strQ &= "    FROM    prtranhead_master " & _
                                    "LEFT JOIN cte ON cte.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "WHERE   prtranhead_master.isvoid = 0 " & _
                                    "AND cte.tranheadunkid IS NULL "

                If strFilter.Trim <> "" Then
                    strQ &= " AND " & strFilter
                End If
            End If

            strQ &= " ORDER BY IsChecked DESC, HeadName "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)
            objDataOperation.AddParameter("@effective_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEffectiveDate))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListByBudgetUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetformulaheadmapping_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@mappedformulaheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappedformulaheadunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgbudgetformulaheadmapping_tran ( " & _
              "  budgetunkid " & _
              ", tranheadunkid " & _
              ", mappedformulaheadunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @budgetunkid " & _
              ", @tranheadunkid " & _
              ", @mappedformulaheadunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetformulaheadmappingtranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditHeadMapping(objDataOperation, enAuditType.ADD) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function InsertAll(ByVal objDataOp As clsDataOperation, ByVal dtTable As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOp.ClearParameters()

        Try
            For Each dtRow As DataRow In dtTable.Rows

                'mintBudgetunkid = CInt(dtRow.Item("budgetunkid"))
                mintBudgetformulaheadmappingtranunkid = CInt(dtRow.Item("budgetformulaheadmappingtranunkid"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintMappedformulaheadunkid = CInt(dtRow.Item("mappedformulaheadunkid"))

                'If mintBudgetformulaheadmappingtranunkid <= 0 Then
                If Insert(objDataOp) = False Then
                    Return False
                End If
                'Else
                '    If Update(objDataOp) = False Then
                '        Return False
                '    End If
                'End If

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function
    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudgetformulaheadmapping_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintBudgetformulaheadmappingtranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@budgetformulaheadmappingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaheadmappingtranunkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@mappedformulaheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappedformulaheadunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetformulaheadmapping_tran SET " & _
              "  budgetunkid = @budgetunkid" & _
              ", tranheadunkid = @tranheadunkid" & _
              ", mappedformulaheadunkid = @mappedformulaheadunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE budgetformulaheadmappingtranunkid = @budgetformulaheadmappingtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditHeadMapping(objDataOperation, enAuditType.EDIT) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudgetformulaheadmapping_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@budgetformulaheadmappingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetformulaheadmapping_tran SET " & _
                          "  isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voidreason = @voidreason " & _
                    "WHERE budgetformulaheadmappingtranunkid = @budgetformulaheadmappingtranunkid " & _
                    "AND isvoid = 0 "


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._Budgetformulaheadmappingtranunkid = intUnkid

            If InsertAuditHeadMapping(objDataOperation, enAuditType.DELETE) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function VoidAllByBudgetUnkId(ByVal intBudgetUnkid As Integer _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            strQ = "INSERT INTO atbgbudgetformulaheadmapping_tran ( " & _
                                "  budgetformulaheadmappingtranunkid " & _
                                ", budgetunkid " & _
                                ", tranheadunkid " & _
                                ", mappedformulaheadunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                ", module_name1 " & _
                                ", module_name2 " & _
                                ", module_name3 " & _
                                ", module_name4 " & _
                                ", module_name5 " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                               "  budgetformulaheadmappingtranunkid " & _
                                ", budgetunkid " & _
                                ", tranheadunkid " & _
                                ", mappedformulaheadunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrWebIP & "' " & _
                                ", '" & mstrWebHost & "' " & _
                                ", @form_name " & _
                                ", @module_name1 " & _
                                ", @module_name2 " & _
                                ", @module_name3 " & _
                                ", @module_name4 " & _
                                ", @module_name5 " & _
                                ", @isweb " & _
                         "FROM bgbudgetformulaheadmapping_tran " & _
                         "WHERE budgetunkid = @budgetunkid " & _
                         "AND isvoid = 0 "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE bgbudgetformulaheadmapping_tran SET " & _
                           "  isvoid = @isvoid" & _
                           ", voiduserunkid = @voiduserunkid" & _
                           ", voiddatetime = @voiddatetime" & _
                           ", voidreason = @voidreason " & _
                  "WHERE budgetunkid = @budgetunkid " & _
                  "AND isvoid = 0 "


            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@budgetformulaheadmappingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  budgetformulaheadmappingtranunkid " & _
              ", budgetunkid " & _
              ", tranheadunkid " & _
              ", mappedformulaheadunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetformulaheadmapping_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND budgetformulaheadmappingtranunkid <> @budgetformulaheadmappingtranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@budgetformulaheadmappingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Private Function InsertAuditHeadMapping(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atbgbudgetformulaheadmapping_tran ( " & _
                            "  budgetformulaheadmappingtranunkid " & _
                            ", budgetunkid " & _
                            ", tranheadunkid " & _
                            ", mappedformulaheadunkid " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name " & _
                            ", form_name " & _
                            ", module_name1 " & _
                            ", module_name2 " & _
                            ", module_name3 " & _
                            ", module_name4 " & _
                            ", module_name5 " & _
                            ", isweb " & _
                    ") VALUES (" & _
                            "  @budgetformulaheadmappingtranunkid " & _
                            ", @budgetunkid " & _
                            ", @tranheadunkid " & _
                            ", @mappedformulaheadunkid " & _
                            ", @audittype " & _
                            ", @audituserunkid " & _
                            ", @auditdatetime " & _
                            ", @ip " & _
                            ", @machine_name " & _
                            ", @form_name " & _
                            ", @module_name1 " & _
                            ", @module_name2 " & _
                            ", @module_name3 " & _
                            ", @module_name4 " & _
                            ", @module_name5 " & _
                            ", @isweb " & _
                         ") "


            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@budgetformulaheadmappingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaheadmappingtranunkid.ToString)
            xDataOpr.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            xDataOpr.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            xDataOpr.AddParameter("@mappedformulaheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappedformulaheadunkid.ToString)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

            If mstrWebIP.ToString().Trim.Length <= 0 Then
                mstrWebIP = getIP()
            End If
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)

            If mstrWebHost.ToString().Length <= 0 Then
                mstrWebHost = getHostName()
            End If
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHost)

            If mstrWebFormName.Trim.Length <= 0 Then
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            xDataOpr.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            xDataOpr.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            xDataOpr.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            xDataOpr.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditHeadMapping; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class