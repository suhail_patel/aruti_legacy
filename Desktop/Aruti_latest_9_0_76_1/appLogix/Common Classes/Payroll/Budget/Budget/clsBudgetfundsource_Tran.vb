﻿'************************************************************************************************************************************
'Class Name : clsBudgetfundsource_Tran.vb
'Purpose    :
'Date       :07/06/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudgetfundsource_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetfundsource_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetfundsourcetranunkid As Integer
    Private mintBudgettranunkid As Integer
    Private mintFundsourceunkid As Integer = 0
    Private mintFundProjectCodeunkid As Integer
    Private mdecPercentage As Decimal
    Private mintFundactivityunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrWebFormName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebHost As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetfundsourcetranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetfundsourcetranunkid() As Integer
        Get
            Return mintBudgetfundsourcetranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetfundsourcetranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgettranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgettranunkid() As Integer
        Get
            Return mintBudgettranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgettranunkid = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set fundsourceunkid
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Fundsourceunkid() As Integer
    '    Get
    '        Return mintFundsourceunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintFundsourceunkid = value
    '    End Set
    'End Property
    ''' <summary>
    ''' Purpose: Get or Set fundprojectcodeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundProjectCodeunkid() As Integer
        Get
            Return mintFundProjectCodeunkid
        End Get
        Set(ByVal value As Integer)
            mintFundProjectCodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set percentage
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Percentage() As Decimal
        Get
            Return mdecPercentage
        End Get
        Set(ByVal value As Decimal)
            mdecPercentage = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundactivityunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Fundactivityunkid() As Integer
        Get
            Return mintFundactivityunkid
        End Get
        Set(ByVal value As Integer)
            mintFundactivityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _WebFormName() As String
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHost() As String
        Set(ByVal value As String)
            mstrWebHost = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetfundsourcetranunkid " & _
              ", budgettranunkid " & _
              ", fundsourceunkid " & _
              ", fundprojectcodeunkid " & _
              ", percentage " & _
              ", fundactivityunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetfundsource_tran " & _
             "WHERE budgetfundsourcetranunkid = @budgetfundsourcetranunkid "

            objDataOperation.AddParameter("@budgetfundsourcetranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBudgetfundsourceTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbudgetfundsourcetranunkid = CInt(dtRow.Item("budgetfundsourcetranunkid"))
                mintbudgettranunkid = CInt(dtRow.Item("budgettranunkid"))
                mintFundsourceunkid = CInt(dtRow.Item("fundsourceunkid"))
                mintFundProjectCodeunkid = CInt(dtRow.Item("fundprojectcodeunkid"))
                mdecPercentage = dtRow.Item("percentage")
                mintFundactivityunkid = CInt(dtRow.Item("fundactivityunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetfundsourcetranunkid " & _
              ", budgettranunkid " & _
              ", fundsourceunkid " & _
              ", fundprojectcodeunkid " & _
              ", percentage " & _
              ", fundactivityunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetfundsource_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetfundsource_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPercentage.ToString)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgbudgetfundsource_tran ( " & _
              "  budgettranunkid " & _
              ", fundsourceunkid " & _
              ", fundprojectcodeunkid " & _
              ", percentage " & _
              ", fundactivityunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @budgettranunkid " & _
              ", @fundsourceunkid " & _
              ", @fundprojectcodeunkid " & _
              ", @percentage " & _
              ", @fundactivityunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetfundsourcetranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditFundSourceTran(objDataOperation, enAuditType.ADD) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function InsertAll(ByVal dtRow As DataRow, ByVal xDataOpr As clsDataOperation) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        
        xDataOpr.ClearParameters()

        Try

            Dim cols As List(Of DataColumn) = (From p As DataColumn In dtRow.Table.Columns Where (p.ColumnName.StartsWith("|_")) Select (p)).ToList
            For Each col As DataColumn In cols
                'mintFundsourceunkid = CInt(col.ColumnName.Replace("|_", ""))
                mintFundProjectCodeunkid = CInt(col.ColumnName.Replace("|_", ""))
                mdecPercentage = CDec(dtRow.Item(col.ColumnName))
                mintFundactivityunkid = CInt(dtRow.Item(col.ColumnName.Replace("|_", "||_")))

                'If isExist(mintBudgettranunkid, mintFundsourceunkid, , mintBudgetfundsourcetranunkid) = False Then
                If Insert(xDataOpr) = False Then
                    Return False
                End If
                'Else
                '    If Update(xDataOpr) = False Then
                '        Return False
                '    End If
                'End If
                
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    'Sohail (01 Apr 2017) -- Start
    'MST Issue - 65.2 - Budget codes % were not importing for the project codes which are created after budget is approved.
    Public Function InsertAllForNewProjectCodes(ByVal xDataOpr As clsDataOperation _
                                                , ByVal intFundProjectCodeUnkid As Integer _
                                                , ByVal intAuditType As Integer _
                                                , ByVal intUserunkid As Integer _
                                                , ByVal strWebIP As String _
                                                , ByVal strWebHost As String _
                                                , ByVal strWebFormName As String _
                                                , ByVal dtCurrentDateAndTime As Date) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT  bgbudget_tran.budgettranunkid  " & _
                          ", bgfundprojectcode_master.fundprojectcodeunkid " & _
                    "INTO    #tblPCodes " & _
                    "FROM    bgbudget_tran " & _
                            "LEFT JOIN bgfundprojectcode_master ON 1 = 1 " & _
                    "WHERE   bgbudget_tran.isvoid = 0 " & _
                            "AND bgfundprojectcode_master.fundprojectcodeunkid = @fundprojectcodeunkid " & _
                    " " & _
                    ";CREATE TABLE #aa " & _
                        "( " & _
                          "budgetfundsourcetranunkid INT NULL " & _
                        ") " & _
                    " " & _
                    ";INSERT  INTO bgbudgetfundsource_tran " & _
                            "( budgettranunkid  " & _
                            ", fundsourceunkid " & _
                            ", percentage " & _
                            ", userunkid " & _
                            ", isvoid " & _
                            ", voiduserunkid " & _
                            ", voiddatetime " & _
                            ", voidreason " & _
                            ", fundactivityunkid " & _
                            ", fundprojectcodeunkid " & _
                            ") " & _
                    "OUTPUT  INSERTED.budgetfundsourcetranunkid " & _
                            "INTO #AA " & _
                            "SELECT  #tblPCodes.budgettranunkid  " & _
                                  ", 0 " & _
                                  ", 0 " & _
                                  ", 1 " & _
                                  ", 0 " & _
                                  ", -1 " & _
                                  ", NULL " & _
                                  ", '' " & _
                                  ", 0 " & _
                                  ", #tblPCodes.fundprojectcodeunkid " & _
                            "FROM    #tblPCodes " & _
                                    "LEFT JOIN bgbudgetfundsource_tran ON bgbudgetfundsource_tran.budgettranunkid = #tblPCodes.budgettranunkid " & _
                                                                         "AND bgbudgetfundsource_tran.fundprojectcodeunkid = #tblPCodes.fundprojectcodeunkid " & _
                                                                         "AND bgbudgetfundsource_tran.isvoid = 0 " & _
                            "WHERE   bgbudgetfundsource_tran.budgetfundsourcetranunkid IS NULL " & _
                    " " & _
                    ";INSERT  INTO atbgbudgetfundsource_tran " & _
                            "( budgetfundsourcetranunkid  " & _
                            ", budgettranunkid " & _
                            ", fundsourceunkid " & _
                            ", percentage " & _
                            ", fundactivityunkid " & _
                            ", fundprojectcodeunkid " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name " & _
                            ", form_name " & _
                            ", module_name1 " & _
                            ", module_name2 " & _
                            ", module_name3 " & _
                            ", module_name4 " & _
                            ", module_name5 " & _
                            ", isweb " & _
                            ") " & _
                            "SELECT  bgbudgetfundsource_tran.budgetfundsourcetranunkid  " & _
                                  ", bgbudgetfundsource_tran.budgettranunkid " & _
                                  ", bgbudgetfundsource_tran.fundsourceunkid " & _
                                  ", bgbudgetfundsource_tran.percentage " & _
                                  ", bgbudgetfundsource_tran.fundactivityunkid " & _
                                  ", bgbudgetfundsource_tran.fundprojectcodeunkid " & _
                                  ", @audittype " & _
                                  ", @audituserunkid " & _
                                  ", @auditdatetime " & _
                                  ", @ip " & _
                                  ", @machine_name " & _
                                  ", @form_name " & _
                                  ", @module_name1 " & _
                                  ", @module_name2 " & _
                                  ", @module_name3 " & _
                                  ", @module_name4 " & _
                                  ", @module_name5 " & _
                                  ", @isweb " & _
                            "FROM    bgbudgetfundsource_tran " & _
                                    "JOIN #aa ON #aa.budgetfundsourcetranunkid = bgbudgetfundsource_tran.budgetfundsourcetranunkid " & _
                    "DROP TABLE #aa " & _
                    "DROP TABLE #tblPCodes "

            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundProjectCodeUnkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)

            If strWebIP.ToString().Trim.Length <= 0 Then
                strWebIP = getIP()
            End If
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strWebIP)

            If strWebHost.ToString().Length <= 0 Then
                strWebHost = getHostName()
            End If
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strWebHost)

            If strWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, strWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            Dim intAffected As Integer = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            strQ = "SELECT  bgbudgetcodes_tran.budgetcodestranunkid  " & _
                          ", bgbudgetcodes_tran.budgettranunkid " & _
                          ", bgbudgetcodes_tran.periodunkid " & _
                          ", bgfundprojectcode_master.fundprojectcodeunkid " & _
                    "INTO    #tblPCodes " & _
                    "FROM    bgbudgetcodes_tran " & _
                            "LEFT JOIN bgfundprojectcode_master ON 1 = 1 " & _
                    "WHERE   bgbudgetcodes_tran.isvoid = 0 " & _
                            "AND bgfundprojectcode_master.fundprojectcodeunkid = @fundprojectcodeunkid " & _
                    " " & _
                    ";CREATE TABLE #aa " & _
                        "( " & _
                          "budgetcodesfundsourcetranunkid INT NULL " & _
                        ") " & _
                    " " & _
                    ";INSERT  INTO bgbudgetcodesfundsource_tran " & _
                                "( budgettranunkid  " & _
                                ", budgetcodestranunkid " & _
                                ", periodunkid " & _
                                ", percentage " & _
                                ", userunkid " & _
                                ", isvoid " & _
                                ", voiduserunkid " & _
                                ", voiddatetime " & _
                                ", voidreason " & _
                                ", fundactivityunkid " & _
                                ", fundprojectcodeunkid " & _
                                ") " & _
                    "OUTPUT  INSERTED.budgetcodesfundsourcetranunkid " & _
                            "INTO #AA " & _
                            "SELECT  #tblPCodes.budgettranunkid  " & _
                                  ", #tblPCodes.budgetcodestranunkid " & _
                                  ", #tblPCodes.periodunkid " & _
                                  ", 0 " & _
                                  ", 1 " & _
                                  ", 0 " & _
                                  ", -1 " & _
                                  ", NULL " & _
                                  ", '' " & _
                                  ", 0 " & _
                                  ", #tblPCodes.fundprojectcodeunkid " & _
                            "FROM    #tblPCodes " & _
                                    "LEFT JOIN bgbudgetcodesfundsource_tran ON bgbudgetcodesfundsource_tran.budgetcodestranunkid = #tblPCodes.budgetcodestranunkid " & _
                                                                         "AND bgbudgetcodesfundsource_tran.fundprojectcodeunkid = #tblPCodes.fundprojectcodeunkid " & _
                                                                         "AND bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                            "WHERE   bgbudgetcodesfundsource_tran.budgetcodesfundsourcetranunkid IS NULL " & _
                    " " & _
                    ";INSERT  INTO atbgbudgetcodesfundsource_tran " & _
                            "( budgetcodesfundsourcetranunkid  " & _
                            ", budgettranunkid " & _
                            ", budgetcodestranunkid " & _
                            ", periodunkid " & _
                            ", percentage " & _
                            ", fundactivityunkid " & _
                            ", fundprojectcodeunkid " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name " & _
                            ", form_name " & _
                            ", module_name1 " & _
                            ", module_name2 " & _
                            ", module_name3 " & _
                            ", module_name4 " & _
                            ", module_name5 " & _
                            ", isweb " & _
                            ") " & _
                            "SELECT  bgbudgetcodesfundsource_tran.budgetcodesfundsourcetranunkid  " & _
                                  ", bgbudgetcodesfundsource_tran.budgettranunkid " & _
                                  ", bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                                  ", bgbudgetcodesfundsource_tran.periodunkid " & _
                                  ", bgbudgetcodesfundsource_tran.percentage " & _
                                  ", bgbudgetcodesfundsource_tran.fundactivityunkid " & _
                                  ", bgbudgetcodesfundsource_tran.fundprojectcodeunkid " & _
                                  ", @audittype " & _
                                  ", @audituserunkid " & _
                                  ", @auditdatetime " & _
                                  ", @ip " & _
                                  ", @machine_name " & _
                                  ", @form_name " & _
                                  ", @module_name1 " & _
                                  ", @module_name2 " & _
                                  ", @module_name3 " & _
                                  ", @module_name4 " & _
                                  ", @module_name5 " & _
                                  ", @isweb " & _
                            "FROM    bgbudgetcodesfundsource_tran " & _
                                    "JOIN #aa ON #aa.budgetcodesfundsourcetranunkid = bgbudgetcodesfundsource_tran.budgetcodesfundsourcetranunkid " & _
                    "DROP TABLE #aa " & _
                    "DROP TABLE #tblPCodes "

            intAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (23 May 2017) -- End

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function
    'Sohail (01 Apr 2017) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudgetfundsource_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintBudgetfundsourcetranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetfundsourcetranunkid.ToString)
            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPercentage.ToString)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetfundsource_tran SET " & _
              "  budgettranunkid = @budgettranunkid" & _
              ", fundsourceunkid = @fundsourceunkid" & _
              ", fundprojectcodeunkid = @fundprojectcodeunkid" & _
              ", percentage = @percentage" & _
              ", fundactivityunkid = @fundactivityunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE budgetfundsourcetranunkid = @budgetfundsourcetranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditFundSourceTran(objDataOperation, enAuditType.EDIT) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudgetfundsource_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@budgetfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetfundsource_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason " & _
           "WHERE budgetfundsourcetranunkid = @budgetfundsourcetranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._Budgetfundsourcetranunkid = intUnkid

            If InsertAuditFundSourceTran(objDataOperation, enAuditType.DELETE) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function VoidAllByBudgetUnkId(ByVal intBudgetUnkid As Integer _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            strQ = "INSERT INTO atbgbudgetfundsource_tran ( " & _
                                "  budgetfundsourcetranunkid " & _
                                ", budgettranunkid " & _
                                ", fundsourceunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                ", module_name1 " & _
                                ", module_name2 " & _
                                ", module_name3 " & _
                                ", module_name4 " & _
                                ", module_name5 " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  budgetfundsourcetranunkid " & _
                                ", bgbudgetfundsource_tran.budgettranunkid " & _
                                ", fundsourceunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrWebIP & "' " & _
                                ", '" & mstrWebHost & "' " & _
                                ", @form_name " & _
                                ", @module_name1 " & _
                                ", @module_name2 " & _
                                ", @module_name3 " & _
                                ", @module_name4 " & _
                                ", @module_name5 " & _
                                ", @isweb " & _
                         "FROM bgbudgetfundsource_tran " & _
                         "LEFT JOIN bgbudget_tran ON bgbudget_tran.budgettranunkid = bgbudgetfundsource_tran.budgettranunkid " & _
                         "WHERE bgbudgetfundsource_tran.isvoid = 0 " & _
                         "AND bgbudget_tran.isvoid = 0 " & _
                         "AND bgbudget_tran.budgetunkid = @budgetunkid "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE bgbudgetfundsource_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                "FROM bgbudget_tran " & _
                "WHERE bgbudget_tran.budgettranunkid = bgbudgetfundsource_tran.budgettranunkid " & _
                "AND bgbudgetfundsource_tran.isvoid = 0 " & _
                "AND bgbudget_tran.isvoid = 0 " & _
                "AND budgetunkid = @budgetunkid "

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    'Sohail (19 Apr 2017) -- Start
    'MST Enhancement - 66.1 - applying user access filter on payroll budget.
    Public Function VoidAllByBudgetTranUnkId(ByVal strBudgetTranUnkIDs As String _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            strQ = "SELECT budgettranunkid INTO #cteBTran FROM bgbudget_tran WHERE budgettranunkid IN (" & strBudgetTranUnkIDs & "); "

            strQ &= "INSERT INTO atbgbudgetfundsource_tran ( " & _
                                "  budgetfundsourcetranunkid " & _
                                ", budgettranunkid " & _
                                ", fundsourceunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                ", module_name1 " & _
                                ", module_name2 " & _
                                ", module_name3 " & _
                                ", module_name4 " & _
                                ", module_name5 " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  budgetfundsourcetranunkid " & _
                                ", bgbudgetfundsource_tran.budgettranunkid " & _
                                ", fundsourceunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrWebIP & "' " & _
                                ", '" & mstrWebHost & "' " & _
                                ", @form_name " & _
                                ", @module_name1 " & _
                                ", @module_name2 " & _
                                ", @module_name3 " & _
                                ", @module_name4 " & _
                                ", @module_name5 " & _
                                ", @isweb " & _
                         "FROM bgbudgetfundsource_tran " & _
                         "LEFT JOIN bgbudget_tran ON bgbudget_tran.budgettranunkid = bgbudgetfundsource_tran.budgettranunkid " & _
                         "JOIN #cteBTran ON #cteBTran.budgettranunkid = bgbudget_tran.budgettranunkid " & _
                         "WHERE bgbudgetfundsource_tran.isvoid = 0 " & _
                         "AND bgbudget_tran.isvoid = 0 "

            strQ &= "DROP TABLE #cteBTran"

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "SELECT budgettranunkid INTO #cteBTran FROM bgbudget_tran WHERE budgettranunkid IN (" & strBudgetTranUnkIDs & "); "

            strQ &= "UPDATE bgbudgetfundsource_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                "FROM bgbudget_tran " & _
                "JOIN #cteBTran ON #cteBTran.budgettranunkid = bgbudget_tran.budgettranunkid " & _
                "WHERE bgbudget_tran.budgettranunkid = bgbudgetfundsource_tran.budgettranunkid " & _
                "AND bgbudgetfundsource_tran.isvoid = 0 " & _
                "AND bgbudget_tran.isvoid = 0 "

            strQ &= "DROP TABLE #cteBTran"

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetTranUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function
    'Sohail (19 Apr 2017) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@budgetfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intBudgettranunkid As Integer, ByVal intFundProjectCodeunkid As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByRef intBudgetfundsourcetranunkid As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        intBudgetfundsourcetranunkid = 0

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetfundsourcetranunkid " & _
             "FROM bgbudgetfundsource_tran " & _
             "WHERE isvoid = 0 " & _
             "AND budgettranunkid = @budgettranunkid " & _
             "AND fundprojectcodeunkid = @fundprojectcodeunkid "

            If intUnkid > 0 Then
                strQ &= " AND budgetfundsourcetranunkid <> @budgetfundsourcetranunkid"
            End If

            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgettranunkid)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundProjectCodeunkid)
            objDataOperation.AddParameter("@budgetfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intBudgetfundsourcetranunkid = CInt(dsList.Tables(0).Rows(0).Item("budgetfundsourcetranunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Private Function InsertAuditFundSourceTran(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atbgbudgetfundsource_tran ( " & _
                            "  budgetfundsourcetranunkid " & _
                            ", budgettranunkid " & _
                            ", fundsourceunkid " & _
                            ", fundprojectcodeunkid " & _
                            ", percentage " & _
                            ", fundactivityunkid " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name " & _
                            ", form_name " & _
                            ", module_name1 " & _
                            ", module_name2 " & _
                            ", module_name3 " & _
                            ", module_name4 " & _
                            ", module_name5 " & _
                            ", isweb " & _
                    ") VALUES (" & _
                            "  @budgetfundsourcetranunkid " & _
                            ", @budgettranunkid " & _
                            ", @fundsourceunkid " & _
                            ", @fundprojectcodeunkid " & _
                            ", @percentage " & _
                            ", @fundactivityunkid " & _
                            ", @audittype " & _
                            ", @audituserunkid " & _
                            ", @auditdatetime " & _
                            ", @ip " & _
                            ", @machine_name " & _
                            ", @form_name " & _
                            ", @module_name1 " & _
                            ", @module_name2 " & _
                            ", @module_name3 " & _
                            ", @module_name4 " & _
                            ", @module_name5 " & _
                            ", @isweb " & _
                         ") "


            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@budgetfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetfundsourcetranunkid.ToString)
            xDataOpr.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            xDataOpr.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            xDataOpr.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            xDataOpr.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPercentage.ToString)
            xDataOpr.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

            If mstrWebIP.ToString().Trim.Length <= 0 Then
                mstrWebIP = getIP()
            End If
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)

            If mstrWebHost.ToString().Length <= 0 Then
                mstrWebHost = getHostName()
            End If
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHost)

            If mstrWebFormName.Trim.Length <= 0 Then
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            xDataOpr.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            xDataOpr.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            xDataOpr.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            xDataOpr.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditFundSourceTran; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class