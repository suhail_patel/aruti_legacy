﻿'************************************************************************************************************************************
'Class Name : clsBudget_MasterNew.vb
'Purpose    :
'Date       :06/06/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudget_MasterNew
    Private Shared ReadOnly mstrModuleName As String = "clsBudget_MasterNew"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetunkid As Integer
    Private mstrBudget_Code As String = String.Empty
    Private mstrBudget_Name As String = String.Empty
    'Nilay (26-Aug-2016) -- Start
    'For Other Language ADD Name1, Name2 
    Private mstrBudget_Name1 As String = String.Empty
    Private mstrBudget_Name2 As String = String.Empty
    'Nilay (26-Aug-2016) -- END
    Private mintPayyearunkid As Integer
    Private mstrPayyearName As String = String.Empty
    Private mintPreviousPeriodyearunkid As Integer
    Private mintViewbyid As Integer
    Private mintAllocationbyid As Integer
    Private mintPresentationmodeid As Integer
    Private mintWhotoincludeid As Integer
    Private mintSalarylevelid As Integer
    Private mdtBudget_date As Date
    Private mblnIsDefault As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintBudget_statusunkid As Integer = enApprovalStatus.PENDING
    'Sohail (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - All employees or all depts must be ticked if allocation is dept to make it default budget.
    Private mintSelected_Employees As Integer
    Private mintTotal_Employees As Integer
    'Sohail (22 Nov 2016) -- End

    Private mstrWebFormName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebHost As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetunkid() As Integer
        Get
            Return mintBudgetunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budget_code
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budget_Code() As String
        Get
            Return mstrBudget_Code
        End Get
        Set(ByVal value As String)
            mstrBudget_Code = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budget_name
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budget_Name() As String
        Get
            Return mstrBudget_Name
        End Get
        Set(ByVal value As String)
            mstrBudget_Name = value
        End Set
    End Property

    'Nilay (26-Aug-2016) -- Start
    'For Other Language ADD Name1, Name2 
    ''' <summary>
    ''' Purpose: Get or Set budget_name1
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budget_Name1() As String
        Get
            Return mstrBudget_Name1
        End Get
        Set(ByVal value As String)
            mstrBudget_Name1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budget_name2
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budget_Name2() As String
        Get
            Return mstrBudget_Name2
        End Get
        Set(ByVal value As String)
            mstrBudget_Name2 = value
        End Set
    End Property
    'Nilay (26-Aug-2016) -- END


    ''' <summary>
    ''' Purpose: Get or Set payyearunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Payyearunkid() As Integer
        Get
            Return mintPayyearunkid
        End Get
        Set(ByVal value As Integer)
            mintPayyearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set PayyearName
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _PayyearName() As String
        Get
            Return mstrPayyearName
        End Get
        Set(ByVal value As String)
            mstrPayyearName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set PreviousPeriodyearunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _PreviousPeriodyearunkid() As Integer
        Get
            Return mintPreviousPeriodyearunkid
        End Get
        Set(ByVal value As Integer)
            mintPreviousPeriodyearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set viewbyid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Viewbyid() As Integer
        Get
            Return mintViewbyid
        End Get
        Set(ByVal value As Integer)
            mintViewbyid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationbyid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Allocationbyid() As Integer
        Get
            Return mintAllocationbyid
        End Get
        Set(ByVal value As Integer)
            mintAllocationbyid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set presentationmodeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Presentationmodeid() As Integer
        Get
            Return mintPresentationmodeid
        End Get
        Set(ByVal value As Integer)
            mintPresentationmodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set whotoincludeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Whotoincludeid() As Integer
        Get
            Return mintWhotoincludeid
        End Get
        Set(ByVal value As Integer)
            mintWhotoincludeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set salarylevelid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Salarylevelid() As Integer
        Get
            Return mintSalarylevelid
        End Get
        Set(ByVal value As Integer)
            mintSalarylevelid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budget_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budget_date() As Date
        Get
            Return mdtBudget_date
        End Get
        Set(ByVal value As Date)
            mdtBudget_date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdefault
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _IsDefault() As Boolean
        Get
            Return mblnIsDefault
        End Get
        Set(ByVal value As Boolean)
            mblnIsDefault = value
        End Set
    End Property

    'Sohail (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - All employees or all depts must be ticked if allocation is dept to make it default budget.
    ''' <summary>
    ''' Purpose: Get or Set total_employees
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Total_Employees() As Integer
        Get
            Return mintTotal_Employees
        End Get
        Set(ByVal value As Integer)
            mintTotal_Employees = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set selected_employees
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Selected_Employees() As Integer
        Get
            Return mintSelected_Employees
        End Get
        Set(ByVal value As Integer)
            mintSelected_Employees = value
        End Set
    End Property
    'Sohail (22 Nov 2016) -- End

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Budget_statusunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budget_statusunkid() As Integer
        Get
            Return mintBudget_statusunkid
        End Get
        Set(ByVal value As Integer)
            mintBudget_statusunkid = value
        End Set
    End Property

    Public WriteOnly Property _WebFormName() As String
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHost() As String
        Set(ByVal value As String)
            mstrWebHost = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetunkid " & _
              ", budget_code " & _
              ", budget_name " & _
              ", budget_name1 " & _
              ", budget_name2 " & _
              ", payyearunkid " & _
              ", payyearname " & _
              ", previousperiodyearunkid " & _
              ", viewbyid " & _
              ", allocationbyid " & _
              ", presentationmodeid " & _
              ", whotoincludeid " & _
              ", salarylevelid " & _
              ", budget_date " & _
              ", isdefault " & _
              ", ISNULL(bgbudget_master.budget_statusunkid, 1) AS budget_statusunkid " & _
              ", ISNULL(bgbudget_master.selected_employees, 0) AS selected_employees " & _
              ", ISNULL(bgbudget_master.total_employees, 0) AS total_employees " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudget_master " & _
             "WHERE budgetunkid = @budgetunkid "
            'Sohail (22 Nov 2016) - [selected_employees, total_employees]
            'Nilay (26-Aug-2016) -- [budget_name1,budget_name2]

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBudgetunkid = CInt(dtRow.Item("budgetunkid"))
                mstrBudget_Code = dtRow.Item("budget_code").ToString
                mstrBudget_Name = dtRow.Item("budget_name").ToString
                'Nilay (26-Aug-2016) -- Start
                'For Other Language ADD Name1, Name2 
                mstrBudget_Name1 = dtRow.Item("budget_name1").ToString
                mstrBudget_Name2 = dtRow.Item("budget_name2").ToString
                'Nilay (26-Aug-2016) -- END
                mintPayyearunkid = CInt(dtRow.Item("payyearunkid"))
                mstrPayyearName = dtRow.Item("payyearname").ToString
                mintPreviousPeriodyearunkid = CInt(dtRow.Item("previousperiodyearunkid"))
                mintViewbyid = CInt(dtRow.Item("viewbyid"))
                mintAllocationbyid = CInt(dtRow.Item("allocationbyid"))
                mintPresentationmodeid = CInt(dtRow.Item("presentationmodeid"))
                mintWhotoincludeid = CInt(dtRow.Item("whotoincludeid"))
                mintSalarylevelid = CInt(dtRow.Item("salarylevelid"))
                mdtBudget_date = dtRow.Item("budget_date")
                mblnIsDefault = CBool(dtRow.Item("isdefault"))
                mintBudget_statusunkid = CInt(dtRow.Item("budget_statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - All employees or all depts must be ticked if allocation is dept to make it default budget.
                mintSelected_Employees = CInt(dtRow.Item("selected_employees"))
                mintTotal_Employees = CInt(dtRow.Item("total_employees"))
                'Sohail (22 Nov 2016) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim objMaster As New clsMasterData
            Dim ds As DataSet = objMaster.getComboListForBudgetViewBy("ViewBy")
            Dim dicViewBy As Dictionary(Of Integer, String) = (From p In ds.Tables("ViewBy") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.GetReportAllocation("Allocation")
            Dim dicAllocation As Dictionary(Of Integer, String) = (From p In ds.Tables("Allocation") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListForBudgetPresentation("Presentation")
            Dim dicPresentation As Dictionary(Of Integer, String) = (From p In ds.Tables("Presentation") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListForBudgetWhoToInclude("WhoToInclude")
            Dim dicWhoToInclude As Dictionary(Of Integer, String) = (From p In ds.Tables("WhoToInclude") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListForBudgetSalaryLevel("SalaryLevel", , True)
            Dim dicSalaryLevel As Dictionary(Of Integer, String) = (From p In ds.Tables("SalaryLevel") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getApprovalStatus("Status", True, True, True, True, True, True)
            Dim dicStatus As Dictionary(Of Integer, String) = (From p In ds.Tables("Status") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            strQ = "SELECT  bgbudget_master.budgetunkid  " & _
                          ", bgbudget_master.budget_code " & _
                          ", bgbudget_master.budget_name " & _
                          ", bgbudget_master.budget_name1 " & _
                          ", bgbudget_master.budget_name2 " & _
                          ", bgbudget_master.payyearunkid " & _
                          ", bgbudget_master.payyearname AS payyear " & _
                          ", bgbudget_master.previousperiodyearunkid " & _
                          ", bgbudget_master.viewbyid " & _
                          ", bgbudget_master.allocationbyid " & _
                          ", bgbudget_master.presentationmodeid " & _
                          ", bgbudget_master.whotoincludeid " & _
                          ", bgbudget_master.salarylevelid " & _
                          ", bgbudget_master.budget_date " & _
                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS strbudget_date " & _
                          ", bgbudget_master.isdefault " & _
                          ", CASE bgbudget_master.isdefault WHEN 0 THEN 'No' ELSE 'Yes' END AS isdefaultYesNo " & _
                          ", ISNULL(bgbudget_master.budget_statusunkid, 1) AS budget_statusunkid " & _
                          ", ISNULL(bgbudget_master.selected_employees, 0) AS selected_employees " & _
                          ", ISNULL(bgbudget_master.total_employees, 0) AS total_employees " & _
                          ", bgbudget_master.userunkid " & _
                          ", bgbudget_master.isvoid " & _
                          ", bgbudget_master.voiduserunkid " & _
                          ", bgbudget_master.voiddatetime " & _
                          ", bgbudget_master.voidreason "
            'Sohail (22 Nov 2016) - [selected_employees, total_employees]
            'Nilay (26-Aug-2016) -- [budget_name1,budget_name2]

            strQ &= ", CASE viewbyid "
            For Each pair In dicViewBy
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS ViewBy "

            strQ &= ", CASE allocationbyid "
            For Each pair In dicAllocation
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " WHEN 0  THEN 'Employee' "
            strQ &= " END AS AllocationBy "

            strQ &= ", CASE presentationmodeid "
            For Each pair In dicPresentation
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS PresentationMode "

            strQ &= ", CASE whotoincludeid "
            For Each pair In dicWhoToInclude
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS WhoToInclude "

            strQ &= ", CASE salarylevelid "
            For Each pair In dicSalaryLevel
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS SalaryLevel "

            strQ &= ", CASE ISNULL(bgbudget_master.budget_statusunkid, 1) "
            For Each pair In dicStatus
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS budget_status "

            strQ &= " FROM    bgbudget_master " & _
                            "LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.yearunkid = bgbudget_master.payyearunkid " & _
                    "WHERE   bgbudget_master.isvoid = 0 "

            If strFilter.Trim <> "" Then
                strQ &= strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetDataGridList(ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As DateTime _
                                    , ByVal xPeriodEnd As DateTime _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , ByVal intAllocationID As Integer _
                                    , ByVal mstrAnalysis_Fields As String _
                                    , ByVal mstrAnalysis_Join As String _
                                    , ByVal mstrAnalysis_TableName As String _
                                    , ByVal mstrAnalysis_OrderBy As String _
                                    , ByVal strPeriodList As String _
                                    , ByVal strTranHeadList As String _
                                    , ByVal intBudgetUnkId As Integer _
                                    , ByVal strPreviousPeriodDBName As String _
                                    , ByVal intBudgetForID As Integer _
                                    , ByVal intPresentationID As Integer _
                                    , ByVal intWhoToIncludeID As Integer _
                                    , ByVal strTableName As String _
                                    , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                    , Optional ByVal strFilerString As String = "" _
                                    , Optional ByRef dsAllHeads As DataSet = Nothing _
                                    , Optional ByVal blnForBudgetCodes As Boolean = False _
                                    , Optional ByVal mstrAnalysis_CodeField As String = "" _
                                    ) As DataSet
        'Sohail (01 Mar 2017) - [mstrAnalysis_CodeField]

        Dim dsList As DataSet = Nothing
        Dim dsAll As DataSet = Nothing
        Dim strQ As String = ""
        Dim strQ1 As String = ""
        Dim exForce As Exception
        Dim strFundQry As String = ""
        Dim strFundFld As String = ""
        Dim strFundFldIsNULL As String = ""
        Dim strJobQry As String = ""
        Dim strManPwerPlanQry As String = ""
        Dim strEmpJobField As String = ""
        Dim strEmpJobCreate As String = ""
        Dim strEmpJobTableFld As String = ""
        Dim strEmpJobJoin As String = ""
        Dim strEmpEmployeeJoin As String = ""
        Dim strActivityQry As String = ""
        Dim strEmpAnalysisFld As String
        Dim strAnalysis_OrderBy As String
        Dim strAnalysis_Join As String
        'Sohail (01 Mar 2017) -- Start
        'Enhancement - 65.1 - Export and Import option on Budget Codes.
        Dim strcteFund As String = ""
        Dim strcteActivity As String = ""
        'Sohail (01 Mar 2017) -- End

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        objDataOperation = New clsDataOperation

        Try
            If intBudgetForID = enBudgetViewBy.Employee Then
                strEmpJobField = ", ISNULL(ERECAT.jobunkid,ISNULL(hremployee_master.jobunkid,0)) As empjobunkid " & _
                                 ", ISNULL(hrjob_master.job_name, ISNULL(EMPJOB.job_name,'')) AS EmpJobTitle "

                strEmpJobCreate = ", empjobunkid INT NULL " & _
                                  ", EmpJobTitle NVARCHAR(MAX) NULL "

                strEmpJobTableFld = ", empjobunkid " & _
                                    ", EmpJobTitle "

                strEmpEmployeeJoin = "LEFT JOIN hremployee_master ON #ce.allocationtranunkid = hremployee_master.employeeunkid "
                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'strEmpJobJoin &= "       LEFT JOIN " & _
                '                        "( " & _
                '                        "   SELECT " & _
                '                        "     Cat.CatEmpId " & _
                '                        "    ,Cat.jobgroupunkid " & _
                '                        "    ,Cat.jobunkid " & _
                '                        "    ,Cat.CEfDt " & _
                '                        "    ,Cat.RECAT_REASON " & _
                '                        "   FROM " & _
                '                        "   ( " & _
                '                        "       SELECT " & _
                '                        "            ECT.employeeunkid AS CatEmpId " & _
                '                        "           ,ECT.jobgroupunkid " & _
                '                        "           ,ECT.jobunkid " & _
                '                        "           ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                '                        "           ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                '                        "           ,CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                '                        "       FROM hremployee_categorization_tran AS ECT " & _
                '                        "           LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                '                        "           LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                '                        "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                '                        "   ) AS Cat WHERE Cat.Rno = 1 " & _
                '                        ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                '                        "LEFT JOIN hrjob_master ON ERECAT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                '                        "LEFT JOIN hrjob_master EMPJOB ON hremployee_master.jobunkid = EMPJOB.jobunkid AND EMPJOB.isactive = 1 "
                strEmpJobJoin &= "       LEFT JOIN #cteCAT AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                                        "LEFT JOIN hrjob_master ON ERECAT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                                        "LEFT JOIN hrjob_master EMPJOB ON hremployee_master.jobunkid = EMPJOB.jobunkid AND EMPJOB.isactive = 1 "
                'Sohail (01 Mar 2017) -- End
            End If

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            'strQ = "DECLARE @cols NVARCHAR(MAX) " & _
            '       "DECLARE @cols2 NVARCHAR(MAX) " & _
            '       "DECLARE @qry NVARCHAR(MAX) " & _
            '       "SELECT  @cols = STUFF((SELECT   ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectname) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
            '                               "FROM     bgfundprojectcode_master " & _
            '                               "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
            '                "FOR           XML PATH('')  " & _
            '                                ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
            '        "SELECT  @cols2 = STUFF((SELECT  ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
            '                                "FROM    bgfundprojectcode_master " & _
            '                                "WHERE   bgfundprojectcode_master.isvoid = 0 " & _
            '                "FOR            XML PATH('')  " & _
            '                                 ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
            '        " " & _
            '        "SET @qry = 'SELECT budgettranunkid, ' + @cols + N' FROM (SELECT budgettranunkid, bgfundprojectcode_master.fundprojectcodeunkid, percentage  FROM bgbudgetfundsource_tran " & _
            '            "LEFT JOIN bgfundprojectcode_master ON bgbudgetfundsource_tran.fundprojectcodeunkid = bgfundprojectcode_master.fundprojectcodeunkid " & _
            '            "WHERE bgbudgetfundsource_tran.isvoid = 0 " & _
            '            "AND bgfundprojectcode_master.isvoid = 0 " & _
            '            ") AS z " & _
            '            "PIVOT (SUM( percentage) FOR fundprojectcodeunkid IN (' + @cols2 + ') ) AS A' "
            strQ = "DECLARE @cols NVARCHAR(MAX) " & _
                   "DECLARE @cols2 NVARCHAR(MAX) " & _
                   "DECLARE @qry NVARCHAR(MAX) " & _
                   "SELECT  @cols = STUFF((SELECT   ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ISNULL(' + QUOTENAME(bgfundprojectcode_master.fundprojectname) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                           "FROM     bgfundprojectcode_master " & _
                                           "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
                            "FOR           XML PATH('')  " & _
                                            ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                    "SELECT  @cols2 = STUFF((SELECT  ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                            "FROM    bgfundprojectcode_master " & _
                                            "WHERE   bgfundprojectcode_master.isvoid = 0 " & _
                            "FOR            XML PATH('')  " & _
                                             ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                    " " & _
                    "SET @qry = 'SELECT budgettranunkid, ' + @cols + N' INTO #cte# FROM (SELECT budgettranunkid, bgfundprojectcode_master.fundprojectcodeunkid, percentage  FROM bgbudgetfundsource_tran " & _
                        "LEFT JOIN bgfundprojectcode_master ON bgbudgetfundsource_tran.fundprojectcodeunkid = bgfundprojectcode_master.fundprojectcodeunkid " & _
                        "WHERE bgbudgetfundsource_tran.isvoid = 0 " & _
                        "AND bgfundprojectcode_master.isvoid = 0 " & _
                        ") AS z " & _
                        "PIVOT (SUM( percentage) FOR fundprojectcodeunkid IN (' + @cols2 + ') ) AS A' "
            'Sohail (01 Mar 2017) -- End

            strQ &= "SELECT @qry "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 AndAlso IsDBNull(dsList.Tables(0).Rows(0).Item(0)) = False Then
                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'strFundQry = " LEFT JOIN ( " & dsList.Tables(0).Rows(0).Item(0).ToString & " ) AS FundProjectCode ON FundProjectCode.budgettranunkid = bgbudget_tran.budgettranunkid "
                'strActivityQry = " LEFT JOIN ( " & dsList.Tables(0).Rows(0).Item(0).ToString.Replace("SUM( percentage)", "MAX( fundactivityunkid)").Replace("percentage", "fundactivityunkid") & " ) AS FundActivity ON FundActivity.budgettranunkid = bgbudget_tran.budgettranunkid "
                strFundQry = " LEFT JOIN #cteFUND AS FundProjectCode ON FundProjectCode.budgettranunkid = bgbudget_tran.budgettranunkid "
                strActivityQry = " LEFT JOIN #cteACTIVITY AS FundActivity ON FundActivity.budgettranunkid = bgbudget_tran.budgettranunkid "

                strcteFund = dsList.Tables(0).Rows(0).Item(0).ToString
                strcteActivity = dsList.Tables(0).Rows(0).Item(0).ToString.Replace("SUM( percentage)", "MAX( fundactivityunkid)").Replace("percentage", "fundactivityunkid")
                'Sohail (01 Mar 2017) -- End

                strQ = "DECLARE @colsisnull NVARCHAR(MAX) " & _
                       "DECLARE @cols NVARCHAR(MAX) " & _
                       "DECLARE @codecols NVARCHAR(MAX) " & _
                        " " & _
                        "SELECT  @colsisnull = STUFF((SELECT   ', ISNULL(FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', ISNULL(FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectname) + ', 0) AS ' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                               "FROM     bgfundprojectcode_master " & _
                                               "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
                                "FOR           XML PATH('')  " & _
                                                ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                        "SELECT  @cols = STUFF((SELECT   ', FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectcodeunkid) /*, ', FundProjectCode.' + QUOTENAME(bgfundprojectcode_master.fundprojectname)*/ " & _
                                               "FROM     bgfundprojectcode_master " & _
                                               "WHERE    bgfundprojectcode_master.isvoid = 0 " & _
                                "FOR           XML PATH('')  " & _
                                                ", TYPE ).value('.', 'nvarchar(max)'), 1, 1, '') " & _
                        " " & _
                        "SELECT  @colsisnull UNION ALL SELECT  @cols "

                dsList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    strFundFldIsNULL = "," & dsList.Tables(0).Rows(0).Item(0).ToString & " "
                    strFundFld = "," & dsList.Tables(0).Rows(1).Item(0).ToString & " "
                End If
            End If

            If intBudgetForID = enBudgetViewBy.Employee AndAlso intWhoToIncludeID = enBudgetWhoToInclude.AllAsPerManPowerPlan Then

                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Planned employees not coming for those jobs which is not assigned single employee.
                'strQ = "SELECT  A.jobunkid  " & _
                '              ", JOB_NAME " & _
                '              ", PLANNED " & _
                '              ", AVAILABLE " & _
                '              ", ISNULL(PLANNED, 0) - ISNULL(AVAILABLE, 0) AS VARIATION " & _
                '        "FROM    ( SELECT    hrjob_master.job_name AS JOB_NAME " & _
                '                          ", total_position AS PLANNED " & _
                '                          ", COUNT(hremployee_master.employeeunkid) AS AVAILABLE " & _
                '                          ", hrjob_master.jobunkid " & _
                '                  "FROM      hrjob_master " & _
                '                            "LEFT JOIN ( SELECT  jobunkid  " & _
                '                                              ", employeeunkid " & _
                '                                              ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
                '                                        "FROM    hremployee_categorization_tran " & _
                '                                        "WHERE   isvoid = 0 " & _
                '                                                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                '                                      ") AS Jobs ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                '                                                   "AND Jobs.rno = 1 " & _
                '                            "LEFT JOIN hremployee_master ON Jobs.employeeunkid = hremployee_master.employeeunkid "
                'Sohail (06 May 2017) -- Start
                'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
                'strQ = "SELECT  A.jobunkid  " & _
                '              ", JOB_CODE " & _
                '              ", JOB_NAME " & _
                '              ", PLANNED " & _
                '              ", AVAILABLE " & _
                '              ", ISNULL(PLANNED, 0) - ISNULL(AVAILABLE, 0) AS VARIATION " & _
                '        "FROM    ( SELECT    hrjob_master.job_name AS JOB_NAME " & _
                '                          ", hrjob_master.job_code AS JOB_CODE " & _
                '                          ", total_position AS PLANNED " & _
                '                          ", ISNULL(AA.AVAILABLE, 0) AS AVAILABLE " & _
                '                          ", hrjob_master.jobunkid " & _
                '                  "FROM      hrjob_master " & _
                '                  "LEFT JOIN  ( SELECT     COUNT(hremployee_master.employeeunkid) AS AVAILABLE " & _
                '                          ", hrjob_master.jobunkid " & _
                '                  "FROM      hrjob_master " & _
                '                            "LEFT JOIN ( SELECT  jobunkid  " & _
                '                                              ", employeeunkid " & _
                '                                              ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
                '                                        "FROM    hremployee_categorization_tran " & _
                '                                        "WHERE   isvoid = 0 " & _
                '                                                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                '                                      ") AS Jobs ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                '                                                   "AND Jobs.rno = 1 " & _
                '                            "LEFT JOIN hremployee_master ON Jobs.employeeunkid = hremployee_master.employeeunkid "
                strQ = "SELECT  A.jobunkid  " & _
                              ", JOB_CODE " & _
                              ", JOB_NAME " & _
                              ", PLANNED " & _
                              ", AVAILABLE " & _
                              ", ISNULL(PLANNED, 0) - ISNULL(AVAILABLE, 0) AS VARIATION " & _
                        "FROM    ( SELECT    hrjob_master.job_name AS JOB_NAME " & _
                                          ", hrjob_master.job_code AS JOB_CODE " & _
                                          ", total_position AS PLANNED " & _
                                          ", ISNULL(AA.AVAILABLE, 0) AS AVAILABLE " & _
                                          ", hrjob_master.jobunkid " & _
                                  "FROM      hrjob_master " & _
                                  "LEFT JOIN ( SELECT  SUM(B.AVAILABLE) AS AVAILABLE  " & _
                                                    ", B.jobunkid " & _
                                              "FROM    ( SELECT    COUNT(hremployee_master.employeeunkid) AS AVAILABLE " & _
                                          ", hrjob_master.jobunkid " & _
                                  "FROM      hrjob_master " & _
                                            "LEFT JOIN ( SELECT  jobunkid  " & _
                                                              ", employeeunkid " & _
                                                              ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
                                                        "FROM    hremployee_categorization_tran " & _
                                                        "WHERE   isvoid = 0 " & _
                                                                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                                      ") AS Jobs ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                                                                   "AND Jobs.rno = 1 " & _
                                            "LEFT JOIN hremployee_master ON Jobs.employeeunkid = hremployee_master.employeeunkid "
                'Sohail (06 May 2017) -- End
                'Sohail (01 Mar 2017) -- End

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If

                strQ &= " WHERE hrjob_master.isactive = 1 "

                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry & " "
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry & " "
                    End If
                End If

                strQ &= "       GROUP BY  hrjob_master.job_name " & _
                                          ", total_position " & _
                                          ", hrjob_master.jobunkid "

                'Sohail (06 May 2017) -- Start
                'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
                If intBudgetUnkId > 0 Then

                    strQ &= "  UNION ALL " & _
                              "SELECT    COUNT(DISTINCT bgbudget_tran.allocationtranunkid) AS AWAILABLE  " & _
                                      ", bgbudget_tran.jobunkid " & _
                              "FROM      bgbudget_tran " & _
                              "WHERE     bgbudget_tran.isvoid = 0 " & _
                                        "AND bgbudget_tran.budgetunkid = @budgetunkid " & _
                                        "AND bgbudget_tran.allocationtranunkid <= 0 " & _
                                        "AND bgbudget_tran.jobunkid > 0 " & _
                              "GROUP BY  bgbudget_tran.jobunkid "

                End If
                'Sohail (06 May 2017) -- End

                strQ &= "   ) B " & _
                          "GROUP BY B.jobunkid " & _
                                ") AS AA ON AA.jobunkid = hrjob_master.jobunkid " & _
                                ") AS A " & _
                        "WHERE   1 = 1 " & _
                                "AND ISNULL(PLANNED, 0) - ISNULL(AVAILABLE, 0) > 0 "
                'Sohail (01 Mar 2017) - [") AS AA ON AA.jobunkid = hrjob_master.jobunkid "]

                'Sohail (06 May 2017) -- Start
                'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
                objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)
                'Sohail (06 May 2017) -- End

                Dim ds As DataSet = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dsRow As DataRow In ds.Tables(0).Rows
                        strJobQry &= " UNION ALL SELECT " & dsRow.Item("jobunkid").ToString & " AS JobUnkId, '" & dsRow.Item("JOB_NAME").ToString.Replace("'", "''") & "' AS Job, '" & dsRow.Item("JOB_CODE").ToString.Replace("'", "''") & "' AS Job_Code, " & dsRow.Item("VARIATION").ToString & " AS Cnt "
                    Next
                    strManPwerPlanQry = ";WITH ct AS ( " & _
                                        "SELECT  JobUnkId " & _
                                              ", Job " & _
                                              ", Job_Code " & _
                                              ", Cnt " & _
                                        "FROM (" & _
                                        strJobQry.Substring(10) & _
                                        ") AS A " & _
                                        "UNION ALL " & _
                                        "SELECT  ct.JobUnkId " & _
                                              ", ct.Job " & _
                                              ", ct.Job_Code " & _
                                              ", (ct.Cnt - 1) AS Cnt " & _
                                        "FROM ct " & _
                                        "JOIN (" & _
                                        strJobQry.Substring(10) & _
                                        ") AS A " & _
                                            "ON ct.JobUnkId = A.JobUnkId " & _
                                        "WHERE ct.Cnt > 1) "
                    'Sohail (01 Mar 2017) - [Job_Code]

                End If
            End If

            objDataOperation.ClearParameters() 'Sohail (06 May 2017)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)

            strQ = "CREATE TABLE #bgt ( " & _
                              "  budgetunkid INT NULL " & _
                              ", budget_code NVARCHAR(MAX) NULL " & _
                              ", budget_name NVARCHAR(MAX) NULL " & _
                              ", payyearunkid INT NULL " & _
                              ", viewbyid INT NULL " & _
                              ", allocationbyid INT NULL " & _
                              ", presentationmodeid INT NULL " & _
                              ", whotoincludeid INT NULL " & _
                              ", salarylevelid INT NULL " & _
                              ", budget_date NVARCHAR(MAX) NULL " & _
                              ", isdefault BIT NULL " & _
                              ", allocationtranunkid INT NULL " & _
                              ", tranheadunkid INT NULL " & _
                              ", trnheadcode NVARCHAR(MAX) NULL " & _
                              ", trnheadname NVARCHAR(MAX) NULL " & _
                              ", IsSalary BIT NULL " & _
                              ", budgettranunkid INT NULL " & _
                              ", jobunkid INT NULL " & _
                              ", amount DECIMAL(36, 6) NULL " & _
                              ", budgetamount DECIMAL(36, 6) NULL " & _
                              ", salaryamount DECIMAL(36, 6) NULL " & _
                              ", budgetsalaryamount DECIMAL(36, 6) NULL " & _
                              ", otherpayrollamount DECIMAL(36, 6) NULL " & _
                              ", budgetotherpayrollamount DECIMAL(36, 6) NULL " & _
                              ", pramount DECIMAL(36, 6) NULL " & _
                              ", IsChecked BIT NULL " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_").Replace("]", "] DECIMAL(36, 2) NULL ") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_").Replace("]", "] INT NULL DEFAULT 0 ") & _
                              ", Id INT NULL " & _
                              ", GName NVARCHAR(MAX) NULL " & _
                              ", GCode VARCHAR(MAX) " & _
                              strEmpJobCreate & _
                   " ); "
            'Sohail (01 Mar 2017) - [GCode]

            strQ &= "CREATE TABLE #cteEmp " & _
                    "( " & _
                      "  employeeunkid INT " & _
                      ", firstname VARCHAR(MAX) " & _
                      ", othername VARCHAR(MAX) " & _
                      ", surname VARCHAR(MAX) " & _
                      ", Id INT " & _
                      ", GName VARCHAR(MAX) " & _
                      ", GCode VARCHAR(MAX) " & _
                    ") " & _
                    "INSERT INTO #cteEmp " & _
                    "SELECT hremployee_master.employeeunkid, hremployee_master.firstname, hremployee_master.othername, hremployee_master.surname "
            'Sohail (01 Mar 2017) - [GCode]

            strQ &= mstrAnalysis_Fields
            strQ &= mstrAnalysis_CodeField 'Sohail (01 Mar 2017) 

            strQ &= "FROM hremployee_master "

            If intBudgetForID <> enBudgetViewBy.Employee Then
                strQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If intBudgetForID = enBudgetViewBy.Employee Then
                strQ &= mstrAnalysis_Join
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            strQ &= " ; "

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            'strEmpAnalysisFld = ", #cteEmp.Id AS Id, #cteEmp.GName AS GName "
            strEmpAnalysisFld = ", #cteEmp.Id AS Id, #cteEmp.GName AS GName, #cteEmp.GCode "
            'Sohail (01 Mar 2017) -- End
            'If intBudgetForID = enBudgetViewBy.Employee Then
            strAnalysis_Join = mstrAnalysis_Join
            mstrAnalysis_Join = " AND 1 = 1 "
            strAnalysis_OrderBy = mstrAnalysis_OrderBy
            mstrAnalysis_OrderBy = " #cteEmp.Id "
            'End If

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            If strFundQry.Trim <> "" Then
                strQ &= " " & strcteFund.Replace("#cte#", "#cteFUND") & "; "
            End If

            If strActivityQry.Trim <> "" Then
                strQ &= " " & strcteActivity.Replace("#cte#", "#cteACTIVITY") & "; "
            End If

            strQ &= " SELECT  prtranhead_master.tranheadunkid  " & _
                          ", prtranhead_master.trnheadcode " & _
                          ", prtranhead_master.trnheadname " & _
                          ", SUM(prpayrollprocess_tran.amount) AS pramount " & _
                          strEmpAnalysisFld & _
                          "INTO #ctePayroll " & _
                    "FROM    " & strPreviousPeriodDBName & "..prpayrollprocess_tran " & _
                            "LEFT JOIN " & strPreviousPeriodDBName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "/*LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid*/ " & _
                            "LEFT JOIN #cteEmp ON #cteEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            mstrAnalysis_Join & _
                    "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtranhead_master.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid IN (" & strPeriodList & ") " & _
                            "AND prpayrollprocess_tran.tranheadunkid IN (" & strTranHeadList & ") " & _
                            "AND #cteEmp.employeeunkid IS NOT NULL " & _
                    "GROUP BY " & strEmpAnalysisFld.Substring(2).Replace("AS Id", "").Replace("AS GName", "").Replace("AS GCode", "") & " " & _
                              ", prtranhead_master.tranheadunkid " & _
                              ", prtranhead_master.trnheadcode " & _
                              ", prtranhead_master.trnheadname "
            'Sohail (01 Mar 2017) - [.Replace("AS GCode", "")]

            If intBudgetForID = enBudgetViewBy.Employee Then

                strQ &= " SELECT     Cat.CatEmpId " & _
                                  ", Cat.jobgroupunkid " & _
                                  ", Cat.jobunkid " & _
                                  ", Cat.CEfDt " & _
                                  ", Cat.RECAT_REASON " & _
                                  " INTO #cteCAT " & _
                         "FROM  ( SELECT   ECT.employeeunkid AS CatEmpId " & _
                                        ", ECT.jobgroupunkid " & _
                                        ", ECT.jobunkid " & _
                                        ", CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                                        ", ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                                        ", CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                                 "FROM     hremployee_categorization_tran AS ECT " & _
                                          "LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                          "LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                                 "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                 ") AS Cat WHERE Cat.Rno = 1 "

            End If

            strQ &= " ; "
            'Sohail (01 Mar 2017) -- End

            strQ &= "/*WITH    cte " & _
                              "AS (*/ SELECT   bgbudget_master.budgetunkid  " & _
                                          ", bgbudget_master.budget_code " & _
                                          ", bgbudget_master.budget_name " & _
                                          ", bgbudget_master.payyearunkid " & _
                                          ", bgbudget_master.viewbyid " & _
                                          ", bgbudget_master.allocationbyid " & _
                                          ", bgbudget_master.presentationmodeid " & _
                                          ", bgbudget_master.whotoincludeid " & _
                                          ", bgbudget_master.salarylevelid " & _
                                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS budget_date " & _
                                          ", bgbudget_master.isdefault " & _
                                          ", bgbudget_tran.allocationtranunkid " & _
                                          ", bgbudget_tran.tranheadunkid " & _
                                          ", prtranhead_master.trnheadcode " & _
                                          ", prtranhead_master.trnheadname " & _
                                          ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                          ", bgbudget_tran.budgettranunkid " & _
                                          ", bgbudget_tran.jobunkid " & _
                                          ", bgbudget_tran.amount " & _
                                          ", CASE bgbudget_master.presentationmodeid WHEN 1 THEN bgbudget_tran.budgetamount ELSE CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN budgetsalaryamount ELSE budgetotherpayrollamount END END AS budgetamount " & _
                                          ", bgbudget_tran.salaryamount " & _
                                          ", bgbudget_tran.budgetsalaryamount " & _
                                          ", bgbudget_tran.otherpayrollamount " & _
                                          ", bgbudget_tran.budgetotherpayrollamount " & _
                                          ", ISNULL(payroll.pramount, 0) AS pramount " & _
                                          ", CAST(0 AS BIT) AS IsChecked " & _
                                          strFundFldIsNULL.Replace("AS [", "AS [|_") & _
                                          strFundFldIsNULL.Replace("FundProjectCode.", "FundActivity.").Replace("AS [", "AS [||_") & _
                                          mstrAnalysis_Fields & _
                                          mstrAnalysis_CodeField & _
                                   "INTO #cte FROM     bgbudget_master " & _
                                            "LEFT JOIN bgbudget_tran ON bgbudget_master.budgetunkid = bgbudget_tran.budgetunkid " & _
                                            "LEFT JOIN prtranhead_master ON bgbudget_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                            "LEFT JOIN " & mstrAnalysis_TableName & " ON bgbudget_tran.allocationtranunkid = " & strAnalysis_OrderBy & " " & _
                                            strFundQry & _
                                            strActivityQry & _
                                            "/*LEFT JOIN ( SELECT  prtranhead_master.tranheadunkid  " & _
                                                              ", prtranhead_master.trnheadcode " & _
                                                              ", prtranhead_master.trnheadname " & _
                                                              ", SUM(prpayrollprocess_tran.amount) AS pramount " & _
                                                              strEmpAnalysisFld & _
                                                        "FROM    " & strPreviousPeriodDBName & "..prpayrollprocess_tran " & _
                                                                "LEFT JOIN " & strPreviousPeriodDBName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "/*LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid*/ " & _
                                                                "LEFT JOIN #cteEmp ON #cteEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                mstrAnalysis_Join & _
                                                        "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                                "AND prtranhead_master.isvoid = 0 " & _
                                                                "AND prtnaleave_tran.payperiodunkid IN (" & strPeriodList & ") " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid IN (" & strTranHeadList & ") " & _
                                                                "AND #cteEmp.employeeunkid IS NOT NULL " & _
                                                        "GROUP BY " & strEmpAnalysisFld.Substring(2).Replace("AS Id", "").Replace("AS GName", "").Replace("AS GCode", "") & " " & _
                                                                  ", prtranhead_master.tranheadunkid " & _
                                                                  ", prtranhead_master.trnheadcode " & _
                                                                  ", prtranhead_master.trnheadname " & _
                                                    ") AS payroll ON payroll.tranheadunkid = bgbudget_tran.tranheadunkid*/ " & _
                                            "LEFT JOIN #ctePayroll AS payroll ON payroll.tranheadunkid = bgbudget_tran.tranheadunkid " & _
                                                        "AND bgbudget_tran.allocationtranunkid = payroll.id " & _
                                            "LEFT JOIN #cteEmp ON #cteEmp.id = " & strAnalysis_OrderBy & " " & _
                                            " " & _
                                   "WHERE    bgbudget_master.isvoid = 0 " & _
                                            "AND bgbudget_tran.isvoid = 0 " & _
                                            "AND prtranhead_master.isvoid = 0 " & _
                                            "/*AND payroll.id IS NOT NULL*/ " & _
                                            "AND #cteEmp.employeeunkid IS NOT NULL " & _
                                            "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                            "AND bgbudget_tran.jobunkid <= 0 " & _
                                            "AND bgbudget_master.budgetunkid = @budgetunkid " & _
                                   "UNION ALL " & _
                                   "SELECT   bgbudget_master.budgetunkid  " & _
                                          ", bgbudget_master.budget_code " & _
                                          ", bgbudget_master.budget_name " & _
                                          ", bgbudget_master.payyearunkid " & _
                                          ", bgbudget_master.viewbyid " & _
                                          ", bgbudget_master.allocationbyid " & _
                                          ", bgbudget_master.presentationmodeid " & _
                                          ", bgbudget_master.whotoincludeid " & _
                                          ", bgbudget_master.salarylevelid " & _
                                          ", CONVERT(CHAR(8), bgbudget_master.budget_date, 112) AS budget_date " & _
                                          ", bgbudget_master.isdefault " & _
                                          ", bgbudget_tran.allocationtranunkid " & _
                                          ", bgbudget_tran.tranheadunkid " & _
                                          ", prtranhead_master.trnheadcode " & _
                                          ", prtranhead_master.trnheadname " & _
                                          ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                          ", bgbudget_tran.budgettranunkid " & _
                                          ", bgbudget_tran.jobunkid " & _
                                          ", bgbudget_tran.amount " & _
                                          ", CASE bgbudget_master.presentationmodeid WHEN 1 THEN bgbudget_tran.budgetamount ELSE CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN budgetsalaryamount ELSE budgetotherpayrollamount END END AS budgetamount " & _
                                          ", bgbudget_tran.salaryamount " & _
                                          ", bgbudget_tran.budgetsalaryamount " & _
                                          ", bgbudget_tran.otherpayrollamount " & _
                                          ", bgbudget_tran.budgetotherpayrollamount " & _
                                          ", 0 AS pramount " & _
                                          ", CAST(0 AS BIT) AS IsChecked " & _
                                          strFundFldIsNULL.Replace("AS [", "AS [|_") & _
                                          strFundFldIsNULL.Replace("FundProjectCode.", "FundActivity.").Replace("AS [", "AS [||_") & _
                                          ", bgbudget_tran.jobunkid AS Id " & _
                                          ", hrjob_master.job_name + ' ' + CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GName " & _
                                          ", hrjob_master.job_code + ' ' + CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GCode " & _
                                   "FROM     bgbudget_master " & _
                                            "LEFT JOIN bgbudget_tran ON bgbudget_master.budgetunkid = bgbudget_tran.budgetunkid " & _
                                            "LEFT JOIN prtranhead_master ON bgbudget_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                            "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = bgbudget_tran.jobunkid " & _
                                            strFundQry & _
                                            strActivityQry & _
                                   "WHERE    bgbudget_master.isvoid = 0 " & _
                                            "AND bgbudget_tran.isvoid = 0 " & _
                                            "AND prtranhead_master.isvoid = 0 " & _
                                            "AND bgbudget_tran.jobunkid > 0 " & _
                                            "AND bgbudget_tran.tranheadunkid IN (" & strTranHeadList & ") " & _
                                            "AND bgbudget_master.budgetunkid = @budgetunkid " & _
                                 "/*) " & _
                        ", ce " & _
                          "AS (*/ SELECT   * " & _
                               "INTO #ce FROM     #cte " & _
                                "UNION ALL " & _
                                "SELECT  @budgetunkid AS budgetunkid  " & _
                                      ", '' AS budget_code " & _
                                      ", '' AS budget_name " & _
                                      ", 0 AS payyearunkid " & _
                                      ", 0 AS viewbyid " & _
                                      ", 0 AS allocationbyid " & _
                                      ", 0 AS presentationmodeid " & _
                                      ", 0 AS whotoincludeid " & _
                                      ", 0 AS salarylevelid " & _
                                      ", CONVERT(CHAR(8), GETDATE(), 112) AS budget_date " & _
                                      ", CAST(0 AS BIT) AS isdefault " & _
                                      ", " & mstrAnalysis_OrderBy & " AS allocationtranunkid " & _
                                      ", prtranhead_master.tranheadunkid " & _
                                      ", prtranhead_master.trnheadcode " & _
                                      ", prtranhead_master.trnheadname " & _
                                      ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                                      ", 0 AS budgettranunkid " & _
                                      ", 0 AS jobunkid " & _
                                      ", 0 AS amount " & _
                                      ", 0 AS budgetamount " & _
                                      ", 0 AS salaryamount " & _
                                      ", 0 AS budgetsalaryamount " & _
                                      ", 0 AS otherpayrollamount " & _
                                      ", 0 AS budgetotherpayrollamount " & _
                                      ", SUM(prpayrollprocess_tran.amount) AS pramount " & _
                                      ", CAST(0 AS BIT) AS IsChecked " & _
                                      strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace("[", "[|_") & _
                                      strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace("[", "[||_") & _
                                      strEmpAnalysisFld & _
                                "FROM    " & strPreviousPeriodDBName & "..prpayrollprocess_tran " & _
                                        "LEFT JOIN " & strPreviousPeriodDBName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                        "/*LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid*/ " & _
                                        "LEFT JOIN #cteEmp ON #cteEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                        mstrAnalysis_Join & _
                                        "LEFT JOIN #cte ON #cte.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "AND #cte.Id = " & mstrAnalysis_OrderBy & " "
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]
            'Sohail (13 Apr 2017) - ["AND payroll.id IS NOT NULL "] [show only those employees which user has access]
            'Sohail (01 Mar 2017) - [mstrAnalysis_CodeField]

            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If

            strQ &= "            WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prtranhead_master.isvoid = 0 " & _
                                        "AND #cteEmp.employeeunkid IS NOT NULL " & _
                                        mstrAnalysis_Join.Substring(mstrAnalysis_Join.LastIndexOf("AND"), mstrAnalysis_Join.Length - mstrAnalysis_Join.LastIndexOf("AND")) & _
                                        "AND prtnaleave_tran.payperiodunkid IN (" & strPeriodList & ") " & _
                                        "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                        "AND #cte.budgetunkid IS NULL "

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    strQ &= " AND " & xUACFiltrQry & " "
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry & " "
            '    End If
            'End If

            strQ &= "            GROUP BY prtranhead_master.tranheadunkid " & _
                                        ", prtranhead_master.trnheadcode " & _
                                        ", prtranhead_master.trnheadname " & _
                                        ", prtranhead_master.typeof_id " & _
                                        ", prtranhead_master.isbasicsalaryasotherearning " & _
                                        strEmpAnalysisFld.Replace("AS Id", "").Replace("AS GName", "").Replace("AS GCode", "").Replace("hremployee_master", "#cteEmp") & _
                        "/*)*/ "
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]

            If strJobQry.Trim <> "" Then
                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'strQ &= ", " & strManPwerPlanQry
                strQ &= " " & strManPwerPlanQry
                strQ &= " SELECT * into #ct FROM ct "
                'Sohail (01 Mar 2017) -- End
            End If

            strQ &= " INSERT INTO #bgt ( " & _
                              "  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", tranheadunkid " & _
                              ", trnheadcode " & _
                              ", trnheadname " & _
                              ", IsSalary " & _
                              ", budgettranunkid " & _
                              ", jobunkid " & _
                              ", amount " & _
                              ", budgetamount " & _
                              ", salaryamount " & _
                              ", budgetsalaryamount " & _
                              ", otherpayrollamount " & _
                              ", budgetotherpayrollamount " & _
                              ", pramount " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                    " ) " & _
                        "SELECT  #ce.* " & _
                         strEmpJobField.Replace(",0", ",ISNULL(#ce.Id, 0)").Replace(",''", ",SUBSTRING(RTRIM(LTRIM(#ce.GName)), 0, LEN(RTRIM(LTRIM(#ce.GName))) - CHARINDEX(' ', REVERSE(RTRIM(LTRIM(#ce.GName)))) + 1)") & _
                        "FROM    #ce " & _
                        strEmpEmployeeJoin & _
                        strEmpJobJoin & _
                        "UNION ALL " & _
                        "SELECT  @budgetunkid AS budgetunkid  " & _
                              ", '' AS budget_code " & _
                              ", '' AS budget_name " & _
                              ", 0 AS payyearunkid " & _
                              ", 0 AS viewbyid " & _
                              ", 0 AS allocationbyid " & _
                              ", 0 AS presentationmodeid " & _
                              ", 0 AS whotoincludeid " & _
                              ", 0 AS salarylevelid " & _
                              ", CONVERT(CHAR(8), GETDATE(), 112) AS budget_date " & _
                              ", CAST(0 AS BIT) AS isdefault " & _
                              ", " & strAnalysis_OrderBy & " AS allocationtranunkid " & _
                              ", prtranhead_master.tranheadunkid " & _
                              ", prtranhead_master.trnheadcode " & _
                              ", prtranhead_master.trnheadname " & _
                              ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                              ", 0 AS budgettranunkid " & _
                              ", 0 AS jobunkid " & _
                              ", 0 AS amount " & _
                              ", 0 AS budgetamount " & _
                              ", 0 AS salaryamount " & _
                              ", 0 AS budgetsalaryamount " & _
                              ", 0 AS otherpayrollamount " & _
                              ", 0 AS budgetotherpayrollamount " & _
                              ", 0 AS pramount " & _
                              ", CAST(0 AS BIT) AS IsChecked " & _
                              strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              mstrAnalysis_Fields & _
                              mstrAnalysis_CodeField & _
                              strEmpJobField & _
                        "FROM    " & mstrAnalysis_TableName & " " & _
                                "LEFT JOIN prtranhead_master ON 1 = 1 " & _
                                "LEFT JOIN #ce ON #ce.id = " & strAnalysis_OrderBy & " " & _
                                                "AND #ce.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "AND #ce.jobunkid <= 0 " & _
                                strEmpJobJoin
            'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]
            'Sohail (01 Mar 2017) - [mstrAnalysis_CodeField]

            'If strEmpJobJoin.Trim <> "" Then 'Sohail (19 Apr 2017)

                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'If xDateJoinQry.Trim.Length > 0 Then
                '    strQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    strQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    strQ &= xAdvanceJoinQry
                'End If
            'Sohail (19 Apr 2017) -- Start
            'MST Enhancement - 66.1 - applying user access filter on payroll budget.
            'strQ &= "LEFT JOIN #cteEmp ON #cteEmp.employeeunkid = " & strAnalysis_OrderBy & " "
            strQ &= "LEFT JOIN #cteEmp ON #cteEmp.id = " & strAnalysis_OrderBy & " "
            'Sohail (19 Apr 2017) -- End
                'Sohail (01 Mar 2017) -- End

            'End If 'Sohail (19 Apr 2017)

            strQ &= "    WHERE   1 = 1 " & _
                                strAnalysis_Join.Substring(strAnalysis_Join.LastIndexOf("AND"), strAnalysis_Join.Length - strAnalysis_Join.LastIndexOf("AND")) & _
                                "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                "AND ( #ce.tranheadunkid IS NULL OR #ce.id IS NULL ) "

            'If strEmpJobJoin.Trim <> "" Then 'Sohail (19 Apr 2017)

                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'If xUACFiltrQry.Trim.Length > 0 Then
                '    strQ &= " AND " & xUACFiltrQry & " "
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        strQ &= xDateFilterQry & " "
                '    End If
                'End If
                strQ &= "AND #cteEmp.employeeunkid IS NOT NULL "
                'Sohail (01 Mar 2017) -- End

            'End If 'Sohail (19 Apr 2017)

            If strJobQry.Trim <> "" Then

                strQ &= "UNION ALL " & _
                        "SELECT  @budgetunkid AS budgetunkid  " & _
                              ", '' AS budget_code " & _
                              ", '' AS budget_name " & _
                              ", 0 AS payyearunkid " & _
                              ", 0 AS viewbyid " & _
                              ", 0 AS allocationbyid " & _
                              ", 0 AS presentationmodeid " & _
                              ", 0 AS whotoincludeid " & _
                              ", 0 AS salarylevelid " & _
                              ", CONVERT(CHAR(8), GETDATE(), 112) AS budget_date " & _
                              ", CAST(0 AS BIT) AS isdefault " & _
                              ", Cnt * -1 AS allocationtranunkid " & _
                              ", prtranhead_master.tranheadunkid " & _
                              ", prtranhead_master.trnheadcode " & _
                              ", prtranhead_master.trnheadname " & _
                              ", CASE WHEN prtranhead_master.typeof_id = " & enTypeOf.Salary & " OR prtranhead_master.isbasicsalaryasotherearning = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsSalary " & _
                              ", 0 AS budgettranunkid " & _
                              ", #ct.JobUnkId AS jobunkid " & _
                              ", 0 AS amount " & _
                              ", 0 AS budgetamount " & _
                              ", 0 AS salaryamount " & _
                              ", 0 AS budgetsalaryamount " & _
                              ", 0 AS otherpayrollamount " & _
                              ", 0 AS budgetotherpayrollamount " & _
                              ", 0 AS pramount " & _
                              ", CAST(0 AS BIT) AS IsChecked " & _
                              strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace(",", ", 0 AS ").Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", #ct.JobUnkId AS Id " & _
                              ", #ct.Job + ' ' + CAST(Cnt AS NVARCHAR(MAX)) AS GName " & _
                              ", #ct.Job_Code + ' ' + CAST(Cnt AS NVARCHAR(MAX)) AS GCode "
                'Sohail (09 Sep 2017) - [isbasicsalaryasotherearning]

                If strEmpJobField.Trim <> "" Then
                    strQ &= ", #ct.JobUnkId AS empjobunkid " & _
                              ", #ct.Job AS EmpJobTitle "
                End If

                strQ &= "FROM    #ct " & _
                                "LEFT JOIN prtranhead_master ON 1 = 1 " & _
                                "LEFT JOIN #ce ON #ce.id = #ct.JobUnkId " & _
                                                "AND #ce.allocationtranunkid = #ct.Cnt * -1 " & _
                                                "AND #ce.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "AND #ce.jobunkid > 0 " & _
                        "WHERE   1 = 1 " & _
                                "AND prtranhead_master.tranheadunkid IN (" & strTranHeadList & ") " & _
                                "AND ( #ce.tranheadunkid IS NULL OR #ce.id IS NULL OR #ce.allocationtranunkid IS NULL ) "
            End If

            Dim arr() As String = Nothing
            If strFundFld.Trim <> "" Then
                arr = strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_").Substring(1).Split(",")

                If strFundFld.Trim <> "" Then
                    For Each strFund As String In arr
                        strQ &= "UPDATE  #bgt SET "

                        strQ &= " " & strFund.Replace("[|_", "[||_") & " = A.fundactivityunkid "

                        strQ &= "FROM    ( SELECT    bgfundactivity_tran.fundactivityunkid  " & _
                                         ", bgfundactivity_tran.fundprojectcodeunkid " & _
                                 "FROM      bgfundactivity_tran " & _
                                 "WHERE     bgfundactivity_tran.isvoid = 0 " & _
                                           "AND bgfundactivity_tran.fundprojectcodeunkid IN ( " & _
                                           "SELECT  bgfundactivity_tran.fundprojectcodeunkid " & _
                                           "FROM    bgfundactivity_tran " & _
                                           "WHERE   bgfundactivity_tran.isvoid = 0 " & _
                                           "AND bgfundactivity_tran.fundprojectcodeunkid = " & CInt(strFund.Replace("[|_", "").Replace("]", "")) & " " & _
                                           "GROUP BY bgfundactivity_tran.fundprojectcodeunkid " & _
                                           "HAVING  COUNT(bgfundactivity_tran.fundactivityunkid) = 1 ) " & _
                               ") AS A " & _
                       "/*WHERE   A.fundprojectcodeunkid = bgbudgetfundsource_tran.fundprojectcodeunkid*/ ; "
                    Next
                End If


            End If

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            If intBudgetForID = enBudgetViewBy.Employee AndAlso intWhoToIncludeID = enBudgetWhoToInclude.AllAsPerManPowerPlan Then

                strQ &= "UPDATE  #bgt " & _
                        "SET     budget_code = AA.budget_code  " & _
                              ", budget_name = AA.budget_name " & _
                              ", payyearunkid = AA.payyearunkid " & _
                              ", viewbyid = AA.viewbyid " & _
                              ", allocationbyid = AA.allocationbyid " & _
                              ", presentationmodeid = AA.presentationmodeid " & _
                              ", whotoincludeid = AA.whotoincludeid " & _
                              ", salarylevelid = AA.salarylevelid " & _
                              ", budget_date = AA.budget_date " & _
                              ", isdefault = AA.isdefault " & _
                        "FROM    ( SELECT TOP 1 " & _
                                            "* " & _
                                  "FROM      #bgt " & _
                                  "WHERE     LTRIM(RTRIM(#bgt.budget_code)) <> '' " & _
                                ") AS AA " & _
                        "WHERE   LTRIM(RTRIM(#bgt.budget_code)) = ''; "

            End If
            'Sohail (01 Mar 2017) -- End

            strQ1 = strQ

            strQ &= "SELECT  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", tranheadunkid " & _
                              ", trnheadcode " & _
                              ", trnheadname " & _
                              ", IsSalary " & _
                              ", budgettranunkid " & _
                              ", #bgt.jobunkid " & _
                              ", amount " & _
                              ", budgetamount " & _
                              ", salaryamount " & _
                              ", budgetsalaryamount " & _
                              ", otherpayrollamount " & _
                              ", budgetotherpayrollamount " & _
                              ", pramount " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                              ", DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO " & _
                              ", '-' AS Collapse " & _
                        "FROM #bgt "
            'Sohail (01 Nov 2017) - [DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName) AS ROWNO] = [DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO]
            'Sohail (01 Mar 2017) - [GCode]

            strQ &= " ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode, trnheadname "
            'Sohail (01 Nov 2017) - [GCode]

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            'dsAll = objDataOperation.ExecQuery(strQ1, strTableName)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'Sohail (01 Mar 2017) -- End

            If blnForBudgetCodes = True Then
                strQ &= "SELECT  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", #bgt.jobunkid " & _
                              ", CASE presentationmodeid WHEN " & enBudgetPresentation.TransactionWise & " THEN SUM(budgetamount) ELSE MAX(#bgt.budgetsalaryamount) + MAX(#bgt.budgetotherpayrollamount) END AS budgetamount " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                              ", DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO " & _
                              ", '-' AS Collapse " & _
                        "FROM #bgt " & _
                        "GROUP BY budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              ", #bgt.jobunkid " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld
            'Sohail (01 Nov 2017) - [DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName ) AS ROWNO] = [DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO]

                strQ &= " ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode "
            'Sohail (01 Nov 2017) - [GCode]

            End If

            If intPresentationID = enBudgetPresentation.TransactionWise Then

                If blnForBudgetCodes = False Then
                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    'strQ = strQ1
                    'Sohail (01 Mar 2017) -- End
                End If


                'strQ &= " ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, trnheadname "

            Else

                'Dim arr() As String = Nothing
                'If strFundFld.Trim <> "" Then
                '    arr = strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_").Substring(1).Split(",")
                'End If

                strQ &= "UPDATE  #bgt " & _
                        "SET     budgetunkid = A.budgetunkid  " & _
                              ", budget_code = A.budget_code " & _
                              ", budget_name = A.budget_name " & _
                              ", payyearunkid = A.payyearunkid " & _
                              ", viewbyid = A.viewbyid " & _
                              ", allocationbyid = A.allocationbyid " & _
                              ", presentationmodeid = A.presentationmodeid " & _
                              ", whotoincludeid = A.whotoincludeid " & _
                              ", salarylevelid = A.salarylevelid " & _
                              ", budget_date = A.budget_date " & _
                              ", isdefault = A.isdefault " & _
                              ", budgetamount = A.budgetamount " & _
                              ", budgetsalaryamount = A.budgetsalaryamount " & _
                              ", budgetotherpayrollamount = A.budgetotherpayrollamount "

                If strFundFld.Trim <> "" Then
                    For Each strFund As String In arr
                        strQ &= ", " & strFund & " = A." & strFund & " "
                        strQ &= ", " & strFund.Replace("[|_", "[||_") & " = A." & strFund.Replace("[|_", "[||_") & " "
                    Next
                End If

                strQ &= "FROM    ( SELECT    budgetunkid  " & _
                                          ", budget_code " & _
                                          ", budget_name " & _
                                          ", payyearunkid " & _
                                          ", viewbyid " & _
                                          ", allocationbyid " & _
                                          ", presentationmodeid " & _
                                          ", whotoincludeid " & _
                                          ", salarylevelid " & _
                                          ", budget_date " & _
                                          ", isdefault " & _
                                          ", budgetamount " & _
                                          ", budgetsalaryamount " & _
                                          ", budgetotherpayrollamount " & _
                                          ", isSalary " & _
                                          ", Id " & _
                                          ", GName " & _
                                          ", GCode " & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                                  "FROM      #bgt " & _
                                  "WHERE     #bgt.budgettranunkid > 0 " & _
                                  "GROUP BY  Id  " & _
                                          ", Gname " & _
                                          ", GCode " & _
                                          ", budgetunkid " & _
                                          ", budget_code " & _
                                          ", budget_name " & _
                                          ", payyearunkid " & _
                                          ", viewbyid " & _
                                          ", allocationbyid " & _
                                          ", presentationmodeid " & _
                                          ", whotoincludeid " & _
                                          ", salarylevelid " & _
                                          ", budget_date " & _
                                          ", isdefault " & _
                                          ", budgetamount " & _
                                          ", budgetsalaryamount " & _
                                          ", budgetotherpayrollamount " & _
                                          ", isSalary " & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                                          strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                                ") AS A " & _
                        "WHERE   #bgt.budgettranunkid <= 0 " & _
                                "AND A.isSalary = #bgt.isSalary " & _
                                "AND A.id = #bgt.Id " & _
                                "AND A.GName = #bgt.GName  " & _
                                "AND A.GCode = #bgt.GCode ; "

                strQ &= "SELECT  budgetunkid  " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", budget_date " & _
                              ", isdefault " & _
                              ", allocationtranunkid " & _
                              "/*, budgettranunkid " & _
                              ", budgetamount*/ " & _
                              ", budgetsalaryamount " & _
                              ", budgetotherpayrollamount " & _
                              ", ISNULL([1], 0) AS [_|1] " & _
                              ", ISNULL([2], 0) AS [_|2] " & _
                              ", IsChecked " & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                              strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                              ", Id " & _
                              ", GName " & _
                              ", GCode " & _
                              strEmpJobTableFld & _
                              ", DENSE_RANK() OVER (ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode ) AS ROWNO " & _
                              ", '-' AS Collapse " & _
                        "FROM ( SELECT  budgetunkid " & _
                                     ", budget_code " & _
                                     ", budget_name " & _
                                     ", payyearunkid " & _
                                     ", viewbyid " & _
                                     ", allocationbyid " & _
                                     ", presentationmodeid " & _
                                     ", whotoincludeid " & _
                                     ", salarylevelid " & _
                                     ", A.budget_date " & _
                                     ", A.isdefault " & _
                                     ", allocationtranunkid " & _
                                     ", CASE IsSalary " & _
                                          "WHEN 1 THEN 1 " & _
                                          "ELSE 2 " & _
                                        "END AS head  " & _
                                     "/*, budgettranunkid " & _
                                     ", amount " & _
                                     ", budgetamount " & _
                                     ", salaryamount*/ " & _
                                     ", budgetsalaryamount " & _
                                     ", otherpayrollamount " & _
                                     ", budgetotherpayrollamount " & _
                                     ", pramount " & _
                                     ", IsChecked " & _
                                     strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [|_") & _
                                     strFundFld.Replace("FundProjectCode.", "").Replace(" [", " [||_") & _
                                     ", Id " & _
                                     ", GName " & _
                                     ", GCode " & _
                                     strEmpJobTableFld & _
                                     ", DENSE_RANK() OVER (ORDER BY gname, GCode ) AS ROWNO " & _
                                     ", '-' AS Collapse " & _
                                "FROM ( " & _
                                        "SELECT  * " & _
                                        "FROM    #bgt "
                'Sohail (01 Nov 2017) - [GCode in DENSE_RANK()]
                'Sohail (01 Mar 2017) - [budget_date, isdefault]

                strQ &= ") AS A " & _
                        ") Z PIVOT ( SUM(pramount) FOR head IN ( [1], [2] ) ) AS AA " & _
                        "ORDER BY CASE WHEN allocationtranunkid > 0 THEN 0 ELSE 1 END, GName, GCode "
                'Sohail (01 Nov 2017) - [GCode in ORDER BY]

            End If

            strQ &= " DROP TABLE #bgt " & _
                    " DROP TABLE #cteEmp " & _
                    " DROP TABLE #cteFUND " & _
                    " DROP TABLE #cteACTIVITY " & _
                    " DROP TABLE #ctePayroll " & _
                    " DROP TABLE #cte " & _
                    " DROP TABLE #ce "
            'Sohail (01 Mar 2017) - [DROP TABLE #cteFUND, DROP TABLE #cteACTIVITY, DROP TABLE #ctePayroll, DROP TABLE #cte, DROP TABLE #ce, DROP TABLE #ct]


            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            If intBudgetForID = enBudgetViewBy.Employee Then
                strQ &= " DROP TABLE #cteCAT "
            End If

            If strJobQry.Trim <> "" Then
                strQ &= " DROP TABLE #ct "
            End If
            'Sohail (01 Mar 2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Budget Codes.
            'dsAllHeads = dsAll
            dsAllHeads = New DataSet
            dsAllHeads.Tables.Add(dsList.Tables(0).Copy)
            If dsList.Tables.Count = 2 Then
                dsList.Tables.RemoveAt(0)
                dsList.Tables(0).TableName = strTableName
            End If
            'Sohail (01 Mar 2017) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataGridList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (01 Oct 2016) -- Start
    'Enhancement - 63.1 - Don't allow to approve budget if total percentage is not 100
    Public Function GetTotalPercentage(ByVal strTableName As String, ByVal intBudgetUnkId As Integer, Optional ByVal strFilter As String = "", Optional ByVal strGroupByFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  bgbudgetfundsource_tran.budgettranunkid  " & _
                          ", SUM(percentage) AS percentage " & _
                    "FROM    bgbudgetfundsource_tran " & _
                            "LEFT JOIN bgbudget_tran ON bgbudget_tran.budgettranunkid = bgbudgetfundsource_tran.budgettranunkid " & _
                    "WHERE   bgbudgetfundsource_tran.isvoid = 0 " & _
                            "AND bgbudget_tran.isvoid = 0 " & _
                            "AND bgbudget_tran.budgetunkid = @budgetunkid "

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter & " "
            End If

            strQ &= "GROUP BY bgbudgetfundsource_tran.budgettranunkid  "

            If strGroupByFilter.Trim <> "" Then
                strQ &= " HAVING " & strGroupByFilter & " "
            End If


            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (01 Oct 2016) -- End
    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudget_master) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mstrBudget_Code, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This budget code already exists.")
            Return False
        End If
        If isExist("", mstrBudget_Name) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, This budget name already exists.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budget_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Code.ToString)
            objDataOperation.AddParameter("@budget_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Name.ToString)
            'Nilay (26-Aug-2016) -- Start
            'For Other Language ADD Name1, Name2 
            objDataOperation.AddParameter("@budget_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Name1.ToString)
            objDataOperation.AddParameter("@budget_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Name2.ToString)
            'Nilay (26-Aug-2016) -- END
            objDataOperation.AddParameter("@payyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayyearunkid.ToString)
            objDataOperation.AddParameter("@payyearname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPayyearName.ToString)
            objDataOperation.AddParameter("@previousperiodyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPreviousPeriodyearunkid.ToString)
            objDataOperation.AddParameter("@viewbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintViewbyid.ToString)
            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyid.ToString)
            objDataOperation.AddParameter("@presentationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresentationmodeid.ToString)
            objDataOperation.AddParameter("@whotoincludeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWhotoincludeid.ToString)
            objDataOperation.AddParameter("@salarylevelid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalarylevelid.ToString)
            objDataOperation.AddParameter("@budget_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBudget_date)
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDefault.ToString)
            objDataOperation.AddParameter("@budget_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudget_statusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - All employees or all depts must be ticked if allocation is dept to make it default budget.
            objDataOperation.AddParameter("@selected_employees", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelected_Employees.ToString)
            objDataOperation.AddParameter("@total_employees", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotal_Employees.ToString)
            'Sohail (22 Nov 2016) -- End

            strQ = "INSERT INTO bgbudget_master ( " & _
              "  budget_code " & _
              ", budget_name " & _
              ", budget_name1 " & _
              ", budget_name2 " & _
              ", payyearunkid " & _
              ", payyearname " & _
              ", previousperiodyearunkid " & _
              ", viewbyid " & _
              ", allocationbyid " & _
              ", presentationmodeid " & _
              ", whotoincludeid " & _
              ", salarylevelid " & _
              ", budget_date " & _
              ", isdefault " & _
              ", budget_statusunkid " & _
              ", selected_employees " & _
              ", total_employees " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @budget_code " & _
              ", @budget_name " & _
              ", @budget_name1 " & _
              ", @budget_name2 " & _
              ", @payyearunkid " & _
              ", @payyearname " & _
              ", @previousperiodyearunkid " & _
              ", @viewbyid " & _
              ", @allocationbyid " & _
              ", @presentationmodeid " & _
              ", @whotoincludeid " & _
              ", @salarylevelid " & _
              ", @budget_date " & _
              ", @isdefault " & _
              ", @budget_statusunkid " & _
              ", @selected_employees " & _
              ", @total_employees " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"
            'Sohail (22 Nov 2016) - [selected_employees, total_employees]
            'Nilay (26-Aug-2016) -- [budget_name1,budget_name2]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditBudget_Master(objDataOperation, enAuditType.ADD, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function InsertAll(ByVal dtBudgetTable As DataTable, ByVal dtHeadTable As DataTable, ByVal strPeriodIDs As String, ByVal dtCurrentDateAndTime As Date) As Boolean

        Dim objDataOperation As clsDataOperation
        Dim objBudgetTran As New clsBudget_TranNew
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim intBudgetID As Integer = 0
        Dim blnFlag As Boolean = False

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            If Insert(dtCurrentDateAndTime, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objBudgetTran._Budgetunkid = mintBudgetunkid
            objBudgetTran._Userunkid = mintUserunkid
            objBudgetTran._Isvoid = mblnIsvoid
            objBudgetTran._Voiduserunkid = mintVoiduserunkid
            objBudgetTran._Voiddatetime = mdtVoiddatetime
            objBudgetTran._Voidreason = mstrVoidreason
            If objBudgetTran.InsertAll(objDataOperation, dtBudgetTable, Nothing) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objBudgetPeriod._Budgetunkid = mintBudgetunkid
            objBudgetPeriod._Userunkid = mintUserunkid
            objBudgetPeriod._Isvoid = mblnIsvoid
            objBudgetPeriod._Voiduserunkid = mintVoiduserunkid
            objBudgetPeriod._Voiddatetime = mdtVoiddatetime
            objBudgetPeriod._Voidreason = mstrVoidreason
            If objBudgetPeriod.InsertAll(objDataOperation, strPeriodIDs) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objBudgetHeadMapping._Budgetunkid = mintBudgetunkid
            objBudgetHeadMapping._Userunkid = mintUserunkid
            objBudgetHeadMapping._Isvoid = mblnIsvoid
            objBudgetHeadMapping._Voiduserunkid = mintVoiduserunkid
            objBudgetHeadMapping._Voiddatetime = mdtVoiddatetime
            objBudgetHeadMapping._Voidreason = mstrVoidreason
            If objBudgetHeadMapping.InsertAll(objDataOperation, dtHeadTable) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudget_master) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mstrBudget_Code, "", mintBudgetunkid) Then
            mstrMessage = "Sorry, This budget code is already exist."
            Return False
        End If
        If isExist("", mstrBudget_Name, mintBudgetunkid) Then
            mstrMessage = "Sorry, This budget code is already exist."
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@budget_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Code.ToString)
            objDataOperation.AddParameter("@budget_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Name.ToString)
            'Nilay (26-Aug-2016) -- Start
            'For Other Language ADD Name1, Name2 
            objDataOperation.AddParameter("@budget_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Name1.ToString)
            objDataOperation.AddParameter("@budget_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Name2.ToString)
            'Nilay (26-Aug-2016) -- END
            objDataOperation.AddParameter("@payyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayyearunkid.ToString)
            objDataOperation.AddParameter("@payyearname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPayyearName.ToString)
            objDataOperation.AddParameter("@previousperiodyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPreviousPeriodyearunkid.ToString)
            objDataOperation.AddParameter("@viewbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintViewbyid.ToString)
            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyid.ToString)
            objDataOperation.AddParameter("@presentationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresentationmodeid.ToString)
            objDataOperation.AddParameter("@whotoincludeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWhotoincludeid.ToString)
            objDataOperation.AddParameter("@salarylevelid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalarylevelid.ToString)
            objDataOperation.AddParameter("@budget_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBudget_date)
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDefault.ToString)
            objDataOperation.AddParameter("@budget_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudget_statusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - All employees or all depts must be ticked if allocation is dept to make it default budget.
            objDataOperation.AddParameter("@selected_employees", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelected_Employees.ToString)
            objDataOperation.AddParameter("@total_employees", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotal_Employees.ToString)
            'Sohail (22 Nov 2016) -- End

            strQ = "UPDATE bgbudget_master SET " & _
              "  budget_code = @budget_code" & _
              ", budget_name = @budget_name" & _
              ", budget_name1 = @budget_name1" & _
              ", budget_name2 = @budget_name2" & _
              ", payyearunkid = @payyearunkid" & _
              ", payyearname = @payyearname" & _
              ", previousperiodyearunkid = @previousperiodyearunkid" & _
              ", viewbyid = @viewbyid" & _
              ", allocationbyid = @allocationbyid" & _
              ", presentationmodeid = @presentationmodeid" & _
              ", whotoincludeid = @whotoincludeid" & _
              ", salarylevelid = @salarylevelid" & _
              ", budget_date = @budget_date" & _
              ", isdefault = @isdefault" & _
              ", budget_statusunkid = @budget_statusunkid" & _
              ", selected_employees = @selected_employees" & _
              ", total_employees = @total_employees" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE budgetunkid = @budgetunkid "
            'Sohail (22 Nov 2016) - [selected_employees, total_employees]
            'Nilay (26-Aug-2016) -- [budget_name1,budget_name2]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditBudget_Master(objDataOperation, enAuditType.EDIT, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function UpdateAll(ByVal dtBudgetTable As DataTable, ByVal dtHeadTable As DataTable, ByVal strPeriodIDs As String, ByVal dtCurrentDateTime As Date, ByVal strBudgetTranUnkIDs As String) As Boolean
        'Sohail (19 Apr 2017) - [strBudgetTranUnkIDs]

        Dim objDataOperation As clsDataOperation
        Dim objBudgetTran As New clsBudget_TranNew
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objBudgetFundSource As New clsBudgetfundsource_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim intBudgetID As Integer = 0
        Dim blnFlag As Boolean = False

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If Update(dtCurrentDateTime, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (19 Apr 2017) -- Start
            'MST Enhancement - 66.1 - applying user access filter on payroll budget.
            'If objBudgetFundSource.VoidAllByBudgetUnkId(mintBudgetunkid, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
            If objBudgetFundSource.VoidAllByBudgetTranUnkId(strBudgetTranUnkIDs, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
                'Sohail (19 Apr 2017) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (19 Apr 2017) -- Start
            'MST Enhancement - 66.1 - applying user access filter on payroll budget.
            'If objBudgetTran.VoidAllByBudgetUnkId(mintBudgetunkid, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
            If objBudgetTran.VoidAllByBudgetTranUnkId(strBudgetTranUnkIDs, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
                'Sohail (19 Apr 2017) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            objBudgetTran._Budgetunkid = mintBudgetunkid
            objBudgetTran._Userunkid = mintUserunkid
            objBudgetTran._Isvoid = mblnIsvoid
            objBudgetTran._Voiduserunkid = mintVoiduserunkid
            objBudgetTran._Voiddatetime = mdtVoiddatetime
            objBudgetTran._Voidreason = mstrVoidreason
            If objBudgetTran.InsertAll(objDataOperation, dtBudgetTable, dtCurrentDateTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objBudgetPeriod.VoidAllByBudgetUnkId(mintBudgetunkid, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            objBudgetPeriod._Budgetunkid = mintBudgetunkid
            objBudgetPeriod._Userunkid = mintUserunkid
            objBudgetPeriod._Isvoid = mblnIsvoid
            objBudgetPeriod._Voiduserunkid = mintVoiduserunkid
            objBudgetPeriod._Voiddatetime = mdtVoiddatetime
            objBudgetPeriod._Voidreason = mstrVoidreason
            If objBudgetPeriod.InsertAll(objDataOperation, strPeriodIDs) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objBudgetHeadMapping.VoidAllByBudgetUnkId(mintBudgetunkid, mintUserunkid, dtCurrentDateTime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            objBudgetHeadMapping._Budgetunkid = mintBudgetunkid
            objBudgetHeadMapping._Userunkid = mintUserunkid
            objBudgetHeadMapping._Isvoid = mblnIsvoid
            objBudgetHeadMapping._Voiduserunkid = mintVoiduserunkid
            objBudgetHeadMapping._Voiddatetime = mdtVoiddatetime
            objBudgetHeadMapping._Voidreason = mstrVoidreason
            If objBudgetHeadMapping.InsertAll(objDataOperation, dtHeadTable) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateIsDefault(ByVal intBudgetunkid As Integer _
                                    , ByVal blnIsDefault As Boolean _
                                    , ByVal intUserunkid As Integer _
                                    , ByVal dtCurrentDateAndTime As Date _
                                    ) As Boolean

        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "SELECT  bgbudget_master.budgetunkid " & _
                    "FROM    bgbudget_master " & _
                    "WHERE   bgbudget_master.isvoid = 0 " & _
                            "AND isdefault = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dsRow As DataRow In dsList.Tables(0).Rows
                _Budgetunkid = CInt(dsRow.Item("budgetunkid"))
                mblnIsDefault = False
                mintUserunkid = intUserunkid

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid)
                objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDefault)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)

                strQ = "UPDATE bgbudget_master SET " & _
                  "  isdefault = @isdefault " & _
                  ", userunkid = @userunkid " & _
                "WHERE budgetunkid = @budgetunkid "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditBudget_Master(objDataOperation, enAuditType.EDIT, dtCurrentDateAndTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            If blnIsDefault = True Then
                _Budgetunkid = intBudgetunkid
                mblnIsDefault = blnIsDefault
                mintUserunkid = intUserunkid

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
                objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDefault)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

                strQ = "UPDATE bgbudget_master SET " & _
                  "  isdefault = @isdefault " & _
                  ", userunkid = @userunkid " & _
                "WHERE budgetunkid = @budgetunkid "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditBudget_Master(objDataOperation, enAuditType.EDIT, dtCurrentDateAndTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateIsDefault; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudget_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objDataOperation As clsDataOperation

        Dim objBudgetTran As New clsBudget_TranNew
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objBudgetFundSource As New clsBudgetfundsource_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudget_master SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                "WHERE budgetunkid = @budgetunkid " & _
                "AND isvoid = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._Budgetunkid = intUnkid

            If InsertAuditBudget_Master(objDataOperation, enAuditType.DELETE, dtCurrentDateAndTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objBudgetFundSource.VoidAllByBudgetUnkId(mintBudgetunkid, mintUserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objBudgetTran.VoidAllByBudgetUnkId(mintBudgetunkid, mintUserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objBudgetPeriod.VoidAllByBudgetUnkId(mintBudgetunkid, mintUserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objBudgetHeadMapping.VoidAllByBudgetUnkId(mintBudgetunkid, mintUserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Payroll Budget.
            strQ = "SELECT bgbudgetcodes_master.budgetunkid FROM bgbudgetcodes_master " & _
                    "WHERE bgbudgetcodes_master.isvoid = 0  " & _
                    "AND bgbudgetcodes_master.budgetunkid = @budgetunkid "
            'Sohail (01 Mar 2017) -- End

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetunkid " & _
              ", budget_code " & _
              ", budget_name " & _
              ", payyearunkid " & _
              ", viewbyid " & _
              ", allocationbyid " & _
              ", presentationmodeid " & _
              ", whotoincludeid " & _
              ", salarylevelid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudget_master " & _
             "WHERE isvoid = 0 "

            If strCode.Trim <> "" Then
                strQ &= " AND budget_code = @budget_code "
                objDataOperation.AddParameter("@budget_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.Trim <> "" Then
                strQ &= " AND budget_name = @budget_name "
                objDataOperation.AddParameter("@budget_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND budgetunkid <> @budgetunkid"
            End If

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    'Sohail (01 Mar 2017) -- Start
    'Enhancement - 65.1 - Export and Import option on Budget Codes.
    Public Function isExists(ByVal strFilter As String, Optional ByRef intrefBudgetUnkid As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        intrefBudgetUnkid = 0

        Try
            strQ = "SELECT " & _
              "  budgetunkid " & _
              ", budget_code " & _
              ", budget_name " & _
              ", payyearunkid " & _
              ", viewbyid " & _
              ", allocationbyid " & _
              ", presentationmodeid " & _
              ", whotoincludeid " & _
              ", salarylevelid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudget_master " & _
             "WHERE isvoid = 0 "

            If strFilter.Trim <> "" Then
                strQ &= strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intrefBudgetUnkid = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExists; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function
    'Sohail (01 Mar 2017) -- End

    Private Function InsertAuditBudget_Master(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType, ByVal dtCurrentDateAndTime As Date) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atbgbudget_master ( " & _
                             "  budgetunkid " & _
                             ", budget_code " & _
                             ", budget_name " & _
                             ", budget_name1 " & _
                             ", budget_name2 " & _
                             ", payyearunkid " & _
                             ", payyearname " & _
                             ", previousperiodyearunkid " & _
                             ", viewbyid " & _
                             ", allocationbyid " & _
                             ", presentationmodeid " & _
                             ", whotoincludeid " & _
                             ", salarylevelid " & _
                             ", budget_date " & _
                             ", isdefault " & _
                             ", budget_statusunkid " & _
                             ", selected_employees " & _
                             ", total_employees " & _
                             ", audittype " & _
                             ", audituserunkid " & _
                             ", auditdatetime " & _
                             ", ip " & _
                             ", machine_name " & _
                             ", form_name " & _
                             ", module_name1 " & _
                             ", module_name2 " & _
                             ", module_name3 " & _
                             ", module_name4 " & _
                             ", module_name5 " & _
                             ", isweb " & _
                         ") VALUES (" & _
                             "  @budgetunkid " & _
                             ", @budget_code " & _
                             ", @budget_name " & _
                             ", @budget_name1 " & _
                             ", @budget_name2 " & _
                             ", @payyearunkid " & _
                             ", @payyearname " & _
                             ", @previousperiodyearunkid " & _
                             ", @viewbyid " & _
                             ", @allocationbyid " & _
                             ", @presentationmodeid " & _
                             ", @whotoincludeid " & _
                             ", @salarylevelid " & _
                             ", @budget_date " & _
                             ", @isdefault " & _
                             ", @budget_statusunkid " & _
                             ", @selected_employees " & _
                             ", @total_employees " & _
                             ", @audittype " & _
                             ", @audituserunkid " & _
                             ", @auditdatetime " & _
                             ", @ip " & _
                             ", @machine_name " & _
                             ", @form_name " & _
                             ", @module_name1 " & _
                             ", @module_name2 " & _
                             ", @module_name3 " & _
                             ", @module_name4 " & _
                             ", @module_name5 " & _
                             ", @isweb " & _
                         ") "
            'Sohail (22 Nov 2016) - [selected_employees, total_employees]
            'Nilay (26-Aug-2016) -- [budget_name1,budget_name2]


            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            xDataOpr.AddParameter("@budget_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Code.ToString)
            xDataOpr.AddParameter("@budget_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Name.ToString)
            'Nilay (26-Aug-2016) -- Start
            'For Other Language ADD Name1, Name2 
            xDataOpr.AddParameter("@budget_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Name1.ToString)
            xDataOpr.AddParameter("@budget_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBudget_Name2.ToString)
            'Nilay (26-Aug-2016) -- END
            xDataOpr.AddParameter("@payyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayyearunkid.ToString)
            xDataOpr.AddParameter("@payyearname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPayyearName.ToString)
            xDataOpr.AddParameter("@previousperiodyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPreviousPeriodyearunkid.ToString)
            xDataOpr.AddParameter("@viewbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintViewbyid.ToString)
            xDataOpr.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyid.ToString)
            xDataOpr.AddParameter("@presentationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresentationmodeid.ToString)
            xDataOpr.AddParameter("@whotoincludeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWhotoincludeid.ToString)
            xDataOpr.AddParameter("@salarylevelid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalarylevelid.ToString)
            xDataOpr.AddParameter("@budget_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBudget_date)
            xDataOpr.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDefault)
            xDataOpr.AddParameter("@budget_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudget_statusunkid)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - All employees or all depts must be ticked if allocation is dept to make it default budget.
            xDataOpr.AddParameter("@selected_employees", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelected_Employees.ToString)
            xDataOpr.AddParameter("@total_employees", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotal_Employees.ToString)
            'Sohail (22 Nov 2016) -- End

            If mstrWebIP.ToString().Trim.Length <= 0 Then
                mstrWebIP = getIP()
            End If
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)

            If mstrWebHost.ToString().Length <= 0 Then
                mstrWebHost = getHostName()
            End If
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHost)

            If mstrWebFormName.Trim.Length <= 0 Then
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 4, "WEB"))
            End If
            xDataOpr.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            xDataOpr.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            xDataOpr.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            xDataOpr.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditBudget_Master; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

    Public Function GetComboList(ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = True, Optional ByVal blnOnlyDefault As Boolean = False _
                                               , Optional ByVal mstrFilter As String = "", Optional ByVal mintAllocationbyID As Integer = -1) As DataSet

        'Pinkal (21-Oct-2016) -- 'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.[Optional ByVal mstrFilter as String = ""]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If blnAddSelect = True Then
                strQ = "SELECT 0 AS budgetunkid, '' AS Code, @Select AS Name UNION "
            End If

            strQ &= "SELECT  budgetunkid, budget_code, budget_name AS Name " & _
                    "FROM bgbudget_master " & _
                    "WHERE isvoid = 0 "

            If blnOnlyDefault = True Then
                strQ &= " AND isdefault = 1 "
            End If


            If mintAllocationbyID > 0 Then
                strQ &= " AND allocationbyid = " & mintAllocationbyID
            End If

            'Pinkal (21-Oct-2016) -- Start
            'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If
            'Pinkal (21-Oct-2016) -- End


            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, " Select"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (02 Aug 2017) -- Start
    'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
    Public Function GetDefaultBudgetID() As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = 0

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  budgetunkid, budget_code, budget_name AS Name " & _
                    "FROM bgbudget_master " & _
                    "WHERE isvoid = 0 "

            strQ &= " AND isdefault = 1 "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, " Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDefaultBudgetID; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function
    'Sohail (02 Aug 2017) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, " Select")
            Language.setMessage(mstrModuleName, 2, "Sorry, This budget code already exists.")
            Language.setMessage(mstrModuleName, 3, "Sorry, This budget name already exists.")
            Language.setMessage(mstrModuleName, 4, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class