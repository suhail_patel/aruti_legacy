﻿'************************************************************************************************************************************
'Class Name : clsBatchTransaction.vb
'Purpose    :
'Date       :01/07/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsBatchTransaction
    Private Shared ReadOnly mstrModuleName As String = "clsBatchTransaction"
    Dim objDataOperation As clsDataOperation
    Dim objBatchTran As New clsBatchTransactionTran
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBatchtransactionunkid As Integer
    Private mstrBatch_Code As String = String.Empty
    Private mstrBatch_Name As String = String.Empty
    Private mstrBatch_Name1 As String = String.Empty
    Private mstrBatch_Name2 As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchtransactionunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Batchtransactionunkid() As Integer
        Get
            Return mintBatchtransactionunkid
        End Get
        Set(ByVal value As Integer)
            mintBatchtransactionunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batch_code
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Batch_Code() As String
        Get
            Return mstrBatch_Code
        End Get
        Set(ByVal value As String)
            mstrBatch_Code = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batch_name
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Batch_Name() As String
        Get
            Return mstrBatch_Name
        End Get
        Set(ByVal value As String)
            mstrBatch_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batch_name1
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Batch_Name1() As String
        Get
            Return mstrBatch_Name1
        End Get
        Set(ByVal value As String)
            mstrBatch_Name1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batch_name2
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Batch_Name2() As String
        Get
            Return mstrBatch_Name2
        End Get
        Set(ByVal value As String)
            mstrBatch_Name2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                "  batchtransactionunkid " & _
                ", batch_code " & _
                ", batch_name " & _
                ", batch_name1 " & _
                ", batch_name2 " & _
                ", userunkid " & _
                ", isvoid " & _
                ", voiduserunkid " & _
                ", voiddatetime " & _
                ", voidreason " & _
               "FROM prbatch_transaction_master " & _
               "WHERE batchtransactionunkid = @batchtransactionunkid " 'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBatchTransactionUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBatchtransactionunkid = CInt(dtRow.Item("batchtransactionunkid"))
                mstrBatch_Code = dtRow.Item("batch_code").ToString
                mstrBatch_Name = dtRow.Item("batch_name").ToString
                mstrBatch_Name1 = dtRow.Item("batch_name1").ToString
                mstrBatch_Name2 = dtRow.Item("batch_name2").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime").ToString
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                "  batchtransactionunkid " & _
                ", batch_code " & _
                ", batch_name " & _
                ", batch_name1 " & _
                ", batch_name2 " & _
                ", userunkid " & _
                ", isvoid " & _
                ", voiduserunkid " & _
                ", voiddatetime " & _
                ", voidreason " & _
               "FROM prbatch_transaction_master WHERE ISNULL(isvoid,0) = 0" 'Sohail (16 Oct 2010)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prbatch_transaction_master) </purpose>
    Public Function Insert(Optional ByVal dtTran As DataTable = Nothing) As Boolean
        If isExist(mstrBatch_Code, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Batch Code is already defined.")
            Return False
        End If
        If isExist("", mstrBatch_Name) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Batch Name is already defined.")
            Return False
        End If
        

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@batch_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatch_Code.ToString)
            objDataOperation.AddParameter("@batch_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatch_Name.ToString)
            objDataOperation.AddParameter("@batch_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatch_Name1.ToString)
            objDataOperation.AddParameter("@batch_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatch_Name2.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO prbatch_transaction_master ( " & _
              "  batch_code " & _
              ", batch_name " & _
              ", batch_name1 " & _
              ", batch_name2 " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime" & _
              ", voidreason " & _
            ") VALUES (" & _
              "  @batch_code " & _
              ", @batch_name " & _
              ", @batch_name1 " & _
              ", @batch_name2 " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime" & _
              ", @voidreason " & _
            "); SELECT @@identity"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBatchtransactionunkid = dsList.Tables(0).Rows(0).Item(0)

            If dtTran.Rows.Count > 0 Then
                objBatchTran._BatchTransactionUnkid = mintBatchtransactionunkid
                objBatchTran._DataList = dtTran
                If objBatchTran.InserUpdateDeleteBatchTran = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prbatch_transaction_master) </purpose>
    Public Function Update(Optional ByVal dtTran As DataTable = Nothing) As Boolean
        If isExist(mstrBatch_Code, "", mintBatchtransactionunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Batch Code is already defined.")
            Return False
        End If
        If isExist("", mstrBatch_Name, mintBatchtransactionunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Batch Name is already defined.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchtransactionunkid.ToString)
            objDataOperation.AddParameter("@batch_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatch_Code.ToString)
            objDataOperation.AddParameter("@batch_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatch_Name.ToString)
            objDataOperation.AddParameter("@batch_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatch_Name1.ToString)
            objDataOperation.AddParameter("@batch_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatch_Name2.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE prbatch_transaction_master SET " & _
              "  batch_code = @batch_code" & _
              ", batch_name = @batch_name" & _
              ", batch_name1 = @batch_name1" & _
              ", batch_name2 = @batch_name2" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
            "WHERE batchtransactionunkid = @batchtransactionunkid ; SELECT @@identity"

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnToInsert As Boolean = False

            If dtTran.Rows.Count > 0 Then
                'Sohail (12 Oct 2011) -- Start
                Dim dt() As DataRow = dtTran.Select("AUD=''")
                If dt.Length = dtTran.Rows.Count Then
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prbatch_transaction_master", mintBatchtransactionunkid, "batchtransactionunkid", 2) Then
                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prbatch_transaction_master", "batchtransactionunkid", mintBatchtransactionunkid, "prbatch_transaction_tran", "batchtransactiontranunkid", -1, 2, 0) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If
                Else
                objBatchTran._BatchTransactionUnkid = mintBatchtransactionunkid
                objBatchTran._DataList = dtTran
                If objBatchTran.InserUpdateDeleteBatchTran = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
                'Sohail (12 Oct 2011) -- End
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prbatch_transaction_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Batch is already in use.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            'strQ = "DELETE FROM prbatch_transaction_master " & _
            '"WHERE batchtransactionunkid = @batchtransactionunkid "

            'objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            strQ = "UPDATE prbatch_transaction_master SET " & _
              " isvoid = 1" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
            "WHERE batchtransactionunkid = @batchtransactionunkid "

            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objBatchTran.Void(intUnkid, intVoidUserID, dtVoidDateTime, strVoidReason) = True Then 'Sohail (28 Dec 2010)
                objDataOperation.ReleaseTransaction(True)
            Else
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT edunkid FROM prearningdeduction_master " & _
                " WHERE batchtransactionunkid = @batchtransactionunkid " & _
                " AND ISNULL(isvoid,0) = 0" 'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  batchtransactionunkid " & _
              ", batch_code " & _
              ", batch_name " & _
              ", batch_name1 " & _
              ", batch_name2 " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prbatch_transaction_master " & _
             " WHERE ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010)

            If strCode.Trim <> "" Then
                strQ += " AND batch_code = @code"
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If
            If strName.Trim <> "" Then
                strQ += " AND batch_name = @name"
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If
            If intUnkid > 0 Then
                strQ &= " AND batchtransactionunkid <> @batchtransactionunkid"
                objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    '''  Modify by Suhail   
    ''' </summary>
    ''' <param name="strListName"></param>
    ''' <param name="mblnFlag"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As batchtransactionunkid , @Select As  name  UNION "
            End If
            strQ &= "SELECT batchtransactionunkid, batch_name as name FROM prbatch_transaction_master WHERE ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        End Try
    End Function

#Region " Message List "
    '1, "This Batch Code is already defined."
    '2, "This Batch Name is already defined."
    '3, "This Batch is already in use."
    '4, "Select"
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Batch Code is already defined.")
			Language.setMessage(mstrModuleName, 2, "This Batch Name is already defined.")
			Language.setMessage(mstrModuleName, 3, "This Batch is already in use.")
			Language.setMessage(mstrModuleName, 4, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class