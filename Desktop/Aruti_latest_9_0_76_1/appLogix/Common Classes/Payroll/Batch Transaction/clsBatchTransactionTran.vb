﻿'************************************************************************************************************************************
'Class Name : clsBatchTransactionTran.vb
'Purpose    :
'Date       :01/07/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsBatchTransactionTran

#Region " Private Variables "
    Private mdtTran As DataTable
    Private objDataOperation As clsDataOperation
    Private Shared ReadOnly mstrModuleName As String = "clsBatchTransactionTran"
    Private mintBatchTransactionUnkid As Integer = -1
    Private mintBatchTransactionTranUnkid As Integer = -1 'Sohail (12 Oct 2011)
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("BatchTran")
        Dim dCol As New DataColumn
        Try
            dCol = New DataColumn("batchtransactiontranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("batchtransactionunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("tranheadunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "
    Public Property _BatchTransactionUnkid() As Integer
        Get
            Return mintBatchTransactionUnkid
        End Get
        Set(ByVal value As Integer)
            mintBatchTransactionUnkid = value
            Call getBatchTran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region
    
    Private Sub getBatchTran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim drawTranHeadInfo As DataRow
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            strQ = "SELECT batchtransactiontranunkid " & _
                    "     ,batchtransactionunkid " & _
                    "     ,tranheadunkid " & _
                    "     ,userunkid " & _
                    "     ,isvoid " & _
                    "     ,voiduserunkid " & _
                    "     ,voiddatetime " & _
                    "     ,voidreason " & _
                    "     ,'' As AUD " & _
                    "  FROM prbatch_transaction_tran " & _
                    "WHERE prbatch_transaction_tran.batchtransactionunkid = @batchtransactionunkid AND ISNULL(isvoid,0) = 0" 'Sohail (16 Oct 2010)

            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchTransactionUnkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    drawTranHeadInfo = mdtTran.NewRow()
                    drawTranHeadInfo.Item("batchtransactiontranunkid") = .Item("batchtransactiontranunkid")
                    drawTranHeadInfo.Item("batchtransactionunkid") = .Item("batchtransactionunkid")
                    drawTranHeadInfo.Item("tranheadunkid") = .Item("tranheadunkid")
                    drawTranHeadInfo.Item("userunkid") = .Item("userunkid")
                    drawTranHeadInfo.Item("isvoid") = .Item("isvoid")
                    drawTranHeadInfo.Item("voiduserunkid") = .Item("voiduserunkid")
                    drawTranHeadInfo.Item("voiddatetime") = .Item("voiddatetime")
                    drawTranHeadInfo.Item("voidreason") = .Item("voiddatetime")
                    drawTranHeadInfo.Item("AUD") = .Item("AUD")
                    mdtTran.Rows.Add(drawTranHeadInfo)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetWagesTran", mstrModuleName)
        End Try
    End Sub

    Public Function InserUpdateDeleteBatchTran() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet

        Try
            objDataOperation = New clsDataOperation

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO prbatch_transaction_tran ( " & _
                                    "  batchtransactionunkid " & _
                                    ", tranheadunkid " & _
                                    ", userunkid " & _
                                    ", isvoid " & _
                                    ", voiduserunkid " & _
                                    ", voiddatetime" & _
                                    ", voidreason " & _
                                  ") VALUES (" & _
                                    "  @batchtransactionunkid " & _
                                    ", @tranheadunkid " & _
                                    ", @userunkid " & _
                                    ", @isvoid " & _
                                    ", @voiduserunkid " & _
                                    ", @voiddatetime" & _
                                    ", @voidreason " & _
                                  "); SELECT @@identity"

                                objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchTransactionUnkid.ToString)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                'Sohail (12 Oct 2011) -- Start
                                'objDataOperation.ExecNonQuery(strQ)
                                dsList = objDataOperation.ExecQuery(strQ, "List")
                                'Sohail (12 Oct 2011) -- End

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (12 Oct 2011) -- Start
                                mintBatchTransactionTranUnkid = dsList.Tables(0).Rows(0)(0)
                                If .Item("batchtransactionunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prbatch_transaction_master", "batchtransactionunkid", mintBatchTransactionUnkid, "prbatch_transaction_tran", "batchtransactiontranunkid", mintBatchTransactionTranUnkid, 2, 1) = False Then
                                        Return False
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prbatch_transaction_master", "batchtransactionunkid", mintBatchTransactionUnkid, "prbatch_transaction_tran", "batchtransactiontranunkid", mintBatchTransactionTranUnkid, 1, 1) = False Then
                                        Return False
                                    End If
                                End If
                                'Sohail (12 Oct 2011) -- End
                            Case "U"

                                strQ = "UPDATE prbatch_transaction_tran SET " & _
                                    "  batchtransactionunkid = @batchtransactionunkid" & _
                                    ", tranheadunkid = @tranheadunkid" & _
                                    ", userunkid = @userunkid" & _
                                    ", isvoid = @isvoid" & _
                                    ", voiduserunkid = @voiduserunkid" & _
                                    ", voiddatetime = @voiddatetime " & _
                                    ", voidreason = @voidreason " & _
                                  "WHERE batchtransactiontranunkid = @batchtransactiontranunkid "

                                objDataOperation.AddParameter("@batchtransactiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("batchtransactiontranunkid").ToString)
                                objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("batchtransactionunkid").ToString)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, CDate(.Item("voidreason")))

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (12 Oct 2011) -- Start
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prbatch_transaction_master", "batchtransactionunkid", .Item("batchtransactionunkid").ToString, "prbatch_transaction_tran", "batchtransactiontranunkid", .Item("batchtransactiontranunkid").ToString, 2, 2) = False Then
                                    Return False
                                End If
                                'Sohail (12 Oct 2011) -- End
                            Case "D"

                                'Sohail (12 Oct 2011) -- Start
                                If .Item("batchtransactiontranunkid") > 0 Then

                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prbatch_transaction_master", "batchtransactionunkid", .Item("batchtransactionunkid").ToString, "prbatch_transaction_tran", "batchtransactiontranunkid", .Item("batchtransactiontranunkid").ToString, 2, 3) = False Then
                                        Return False
                                    End If

                                strQ = "DELETE FROM prbatch_transaction_tran " & _
                                    "WHERE batchtransactiontranunkid = @batchtransactiontranunkid "

                                objDataOperation.AddParameter("@batchtransactiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("batchtransactiontranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                End If
                                'Sohail (12 Oct 2011) -- End
                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InserUpdateDeleteBatchTran", mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Purpose    : Void All Data Used For Particular Batch
    ''' Modify By : Suhail
    ''' </summary>
    ''' <param name="intBatchUnkID"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function Void(ByVal intBatchUnkID As Integer, _
                         ByVal intVoidUserID As Integer, _
                         ByVal mdtVoidDateTime As DateTime, _
                         ByVal strVoidReason As String) As Boolean
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            objDataOperation = New clsDataOperation

            'Sohail (12 Oct 2011) -- Start
            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prbatch_transaction_master", "batchtransactionunkid", intBatchUnkID, "prbatch_transaction_tran", "batchtransactiontranunkid", 3, 3) = False Then
                Return False
            End If
            'Sohail (12 Oct 2011) -- End

            strQ = "UPDATE prbatch_transaction_tran SET " & _
                    "   isvoid=1 " & _
                    ",  voiduserunkid=@voiduserunkid " & _
                    ",  voiddatetime=@voiddatetime " & _
                    ", voidreason = @voidreason " & _
                    "WHERE batchtransactionunkid=@batchtransactionunkid "

            If mdtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtVoidDateTime) & " " & Format(mdtVoidDateTime, "HH:mm:ss"))
            End If
            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchUnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Void", mstrModuleName)
            Return False
        Finally
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    '''  Modify by Suhail   
    ''' </summary>
    ''' <param name="strListName"></param>
    ''' <param name="mblnFlag"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComboList(ByVal intBatchUnkID As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal blnExcludeMembershipHeads As Boolean = True) As DataSet
        'Sohail (13 Dec 2016) - [blnExcludeMembershipHeads]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As batchtransactiontranunkid as Id, 0 AS batchtransactionunkid, 0 AS tranheadunkid, '' AS Code, @Select As  name, 0 AS trnheadtype_id, 0 AS typeof_id, 0 AS calctype_id, 0 AS computeon_id, 0 AS formula, 0 As formulaid, 0 AS Amount " & _
                    ", 0 AS currencyid, 0 AS vendorid  " & _
                    ", 0 AS costcenterunkid " & _
                    ", '' AS medicalrefno " & _
                    ", 0 AS membershiptranunkid " & _
                    ", 0 AS disciplinefileunkid " & _
                    ", NULL AS cumulative_startdate " & _
                    ", NULL AS stop_date " & _
                    ", NULL AS edstart_date " & _
                    ", 0 AS empbatchpostingunkid " & _
                    " UNION "
                'Sohail (24 Aug 2019) - [edstart_date]
                'Sohail (24 Feb 2016)  - [empbatchpostingunkid]
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            End If

            strQ &= "SELECT " & _
                "  prbatch_transaction_tran.batchtransactiontranunkid as Id " & _
                ", prbatch_transaction_tran.batchtransactionunkid " & _
                ", prbatch_transaction_tran.tranheadunkid " & _
                ", prtranhead_master.trnheadcode as Code " & _
                ", prtranhead_master.trnheadname as Name " & _
                ", prtranhead_master.trnheadtype_id " & _
                ", prtranhead_master.typeof_id " & _
                ", prtranhead_master.calctype_id " & _
                ", prtranhead_master.computeon_id " & _
                ", prtranhead_master.formula " & _
                ", prtranhead_master.formulaid " & _
                ", 0.0000 AS Amount " & _
                ", 0 AS currencyid, 0 AS vendorid " & _
                ", 0 AS costcenterunkid " & _
                ", '' AS medicalrefno " & _
                ", 0 AS membershiptranunkid " & _
                ", 0 AS disciplinefileunkid " & _
                ", NULL AS cumulative_startdate " & _
                ", NULL AS stop_date " & _
                ", NULL AS edstart_date " & _
                ", 0 AS empbatchpostingunkid " & _
            "FROM prbatch_transaction_tran " & _
            "LEFT JOIN prtranhead_master ON prbatch_transaction_tran.tranheadunkid = prtranhead_master.tranheadunkid "
            'Sohail (24 Aug 2019) - [edstart_date]

            'Sohail (13 Dec 2016) -- Start
            'Enhancement - 64.1 - Don't allow to assign Membership heads from batch entry screen.
            If blnExcludeMembershipHeads = True Then
                strQ &= "LEFT JOIN hrmembership_master AS EmpContrib ON EmpContrib.emptranheadunkid = prtranhead_master.tranheadunkid AND EmpContrib.isactive = 1 " & _
                        "LEFT JOIN hrmembership_master AS CoContrib ON CoContrib.cotranheadunkid = prtranhead_master.tranheadunkid AND CoContrib.isactive = 1 "
            End If
            'Sohail (13 Dec 2016) -- End

            strQ &= " WHERE  prbatch_transaction_tran.isvoid = 0 " & _
                     "AND prbatch_transaction_tran.batchtransactionunkid = @batchtransactionunkid "
            'Sohail (24 Feb 2016)  - [empbatchpostingunkid]
            'Sohail (29 Nov 2013) - [cumulative_startdate, stop_date]
            'Sohail (16 Oct 2010),  'Sohail (18 Jan 2012) - [costcenterunkid, medicalrefno]

            'Sohail (13 Dec 2016) -- Start
            'Enhancement - 64.1 - Don't allow to assign Membership heads from batch entry screen.
            If blnExcludeMembershipHeads = True Then
                strQ &= " AND EmpContrib.membershipunkid IS NULL " & _
                        " AND CoContrib.membershipunkid IS NULL "
            End If
            'Sohail (13 Dec 2016) -- End

            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchUnkID)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        End Try
    End Function

#Region " Message List "
    '1, "Select"
#End Region

    '    Private Shared Readonly mstrModuleName As String = "clsBatchTransactionTran"
    '    Dim objDataOperation As clsDataOperation
    '    Dim mstrMessage As String = ""

    '#Region " Private variables "
    '    Private mintBatchtransactiontranunkid As Integer
    '    Private mintBatchtransactionunkid As Integer
    '    Private mintTranheadunkid As Integer
    '    Private mintUserunkid As Integer
    '    Private mblnIsvoid As Boolean
    '    Private mintVoiduserunkid As Integer
    '    Private mdtVoiddatetime As Date
    '#End Region

    '#Region " Properties "
    '    ''' <summary>
    '    ''' Purpose: Get Message from Class 
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    Public ReadOnly Property _Message() As String
    '        Get
    '            Return mstrMessage
    '        End Get
    '    End Property

    '    ''' <summary>
    '    ''' Purpose: Get or Set batchtransactiontranunkid
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    Public Property _Batchtransactiontranunkid() As Integer
    '        Get
    '            Return mintBatchtransactiontranunkid
    '        End Get
    '        Set(ByVal value As Integer)
    '            mintBatchtransactiontranunkid = Value
    '            Call getData()
    '        End Set
    '    End Property

    '    ''' <summary>
    '    ''' Purpose: Get or Set batchtransactionunkid
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    Public Property _Batchtransactionunkid() As Integer
    '        Get
    '            Return mintBatchtransactionunkid
    '        End Get
    '        Set(ByVal value As Integer)
    '            mintBatchtransactionunkid = Value
    '        End Set
    '    End Property

    '    ''' <summary>
    '    ''' Purpose: Get or Set tranheadunkid
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    Public Property _Tranheadunkid() As Integer
    '        Get
    '            Return mintTranheadunkid
    '        End Get
    '        Set(ByVal value As Integer)
    '            mintTranheadunkid = Value
    '        End Set
    '    End Property

    '    ''' <summary>
    '    ''' Purpose: Get or Set userunkid
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    Public Property _Userunkid() As Integer
    '        Get
    '            Return mintUserunkid
    '        End Get
    '        Set(ByVal value As Integer)
    '            mintUserunkid = Value
    '        End Set
    '    End Property

    '    ''' <summary>
    '    ''' Purpose: Get or Set isvoid
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    Public Property _Isvoid() As Boolean
    '        Get
    '            Return mblnIsvoid
    '        End Get
    '        Set(ByVal value As Boolean)
    '            mblnIsvoid = Value
    '        End Set
    '    End Property

    '    ''' <summary>
    '    ''' Purpose: Get or Set voiduserunkid
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    Public Property _Voiduserunkid() As Integer
    '        Get
    '            Return mintVoiduserunkid
    '        End Get
    '        Set(ByVal value As Integer)
    '            mintVoiduserunkid = Value
    '        End Set
    '    End Property

    '    ''' <summary>
    '    ''' Purpose: Get or Set voiddatetime
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    Public Property _Voiddatetime() As Date
    '        Get
    '            Return mdtVoiddatetime
    '        End Get
    '        Set(ByVal value As Date)
    '            mdtVoiddatetime = Value
    '        End Set
    '    End Property

    '#End Region

    '    ''' <summary>
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    ''' <purpose> Assign all Property variable </purpose>
    '    Public Sub GetData()
    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception

    '        objDataOperation = New clsDataOperation

    '        Try
    '            StrQ = "SELECT " & _
    '              "  batchtransactiontranunkid " & _
    '              ", batchtransactionunkid " & _
    '              ", tranheadunkid " & _
    '              ", userunkid " & _
    '              ", isvoid " & _
    '              ", voiduserunkid " & _
    '              ", voiddatetime " & _
    '             "FROM prbatch_transaction_tran " & _
    '             "WHERE batchtransactiontranunkid = @batchtransactiontranunkid "

    '            objDataOperation.AddParameter("@batchtransactiontranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBatchTransactionTranUnkId.ToString)

    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            For Each dtRow As DataRow In dsList.Tables(0).Rows
    '                mintbatchtransactiontranunkid = CInt(dtRow.Item("batchtransactiontranunkid"))
    '                mintbatchtransactionunkid = CInt(dtRow.Item("batchtransactionunkid"))
    '                minttranheadunkid = CInt(dtRow.Item("tranheadunkid"))
    '                mintuserunkid = CInt(dtRow.Item("userunkid"))
    '                mblnisvoid = CBool(dtRow.Item("isvoid"))
    '                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
    '                mdtvoiddatetime = dtRow.Item("voiddatetime")
    '                Exit For
    '            Next
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objDataOperation = Nothing
    '        End Try
    '    End Sub


    '    ''' <summary>
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    ''' <purpose> Assign all Property variable </purpose>
    '    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception

    '        objDataOperation = New clsDataOperation

    '        Try
    '            StrQ = "SELECT " & _
    '              "  batchtransactiontranunkid " & _
    '              ", batchtransactionunkid " & _
    '              ", tranheadunkid " & _
    '              ", userunkid " & _
    '              ", isvoid " & _
    '              ", voiduserunkid " & _
    '              ", voiddatetime " & _
    '             "FROM prbatch_transaction_tran "

    '            If blnOnlyActive Then
    '                strQ &= " WHERE isactive = 1 "
    '            End If

    '            dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objDataOperation = Nothing
    '        End Try
    '        Return dsList
    '    End Function


    '    ''' <summary>
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    ''' <returns>Boolean</returns>
    '    ''' <purpose> INSERT INTO Database Table (prbatch_transaction_tran) </purpose>
    '    Public Function Insert() As Boolean
    '        If isExist(mstrName) Then
    '            mstrMessage = "<Message>"
    '            Return False
    '        End If

    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception

    '        objDataOperation = New clsDataOperation

    '        Try
    '            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbatchtransactionunkid.ToString)
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)

    '            StrQ = "INSERT INTO prbatch_transaction_tran ( " & _
    '              "  batchtransactionunkid " & _
    '              ", tranheadunkid " & _
    '              ", userunkid " & _
    '              ", isvoid " & _
    '              ", voiduserunkid " & _
    '              ", voiddatetime" & _
    '            ") VALUES (" & _
    '              "  @batchtransactionunkid " & _
    '              ", @tranheadunkid " & _
    '              ", @userunkid " & _
    '              ", @isvoid " & _
    '              ", @voiduserunkid " & _
    '              ", @voiddatetime" & _
    '            "); SELECT @@identity"

    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            mintBatchTransactionTranUnkId = dsList.Tables(0).Rows(0).Item(0)

    '            Return True
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '            Return False
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objDataOperation = Nothing
    '        End Try
    '    End Function

    '    ''' <summary>
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    ''' <returns>Boolean</returns>
    '    ''' <purpose> Update Database Table (prbatch_transaction_tran) </purpose>
    '    Public Function Update() As Boolean
    '        If isExist(mstrName, mintBatchtransactiontranunkid) Then
    '            mstrMessage = "<Message>"
    '            Return False
    '        End If

    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception

    '        objDataOperation = New clsDataOperation

    '        Try
    '            objDataOperation.AddParameter("@batchtransactiontranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbatchtransactiontranunkid.ToString)
    '            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbatchtransactionunkid.ToString)
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)

    '            StrQ = "UPDATE prbatch_transaction_tran SET " & _
    '              "  batchtransactionunkid = @batchtransactionunkid" & _
    '              ", tranheadunkid = @tranheadunkid" & _
    '              ", userunkid = @userunkid" & _
    '              ", isvoid = @isvoid" & _
    '              ", voiduserunkid = @voiduserunkid" & _
    '              ", voiddatetime = @voiddatetime " & _
    '            "WHERE batchtransactiontranunkid = @batchtransactiontranunkid "

    '            Call objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            Return True
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '            Return False
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objDataOperation = Nothing
    '        End Try
    '    End Function

    '    ''' <summary>
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    ''' <returns>Boolean</returns>
    '    ''' <purpose> Delete Database Table (prbatch_transaction_tran) </purpose>
    '    Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '        If isUsed(intUnkid) Then
    '            mstrMessage = "<Message>"
    '            Return False
    '        End If

    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception

    '        objDataOperation = New clsDataOperation

    '        Try
    '            StrQ = "DELETE FROM prbatch_transaction_tran " & _
    '            "WHERE batchtransactiontranunkid = @batchtransactiontranunkid "

    '            objDataOperation.AddParameter("@batchtransactiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '            Call objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            Return True
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '            Return False
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objDataOperation = Nothing
    '        End Try
    '    End Function

    '    ''' <summary>
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    ''' <purpose> Assign all Property variable </purpose>
    '    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception

    '        objDataOperation = New clsDataOperation

    '        Try
    '            StrQ = "<Query>"

    '            objDataOperation.AddParameter("@batchtransactiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            Return dsList.tables(0).rows.count > 0
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objDataOperation = Nothing
    '        End Try
    '    End Function

    '    ''' <summary>
    '    ''' Modify By: Suhail
    '    ''' </summary>
    '    ''' <purpose> Assign all Property variable </purpose>
    '    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception

    '        objDataOperation = New clsDataOperation

    '        Try
    '            StrQ = "SELECT " & _
    '              "  batchtransactiontranunkid " & _
    '              ", batchtransactionunkid " & _
    '              ", tranheadunkid " & _
    '              ", userunkid " & _
    '              ", isvoid " & _
    '              ", voiduserunkid " & _
    '              ", voiddatetime " & _
    '             "FROM prbatch_transaction_tran " & _
    '             "WHERE name = @name " & _
    '             "AND code = @code "

    '            If intUnkid > 0 Then
    '                strQ &= " AND batchtransactiontranunkid <> @batchtransactiontranunkid"
    '            End If

    '            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '            objDataOperation.AddParameter("@batchtransactiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            Return dsList.tables(0).rows.count > 0
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objDataOperation = Nothing
    '        End Try
    '    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class