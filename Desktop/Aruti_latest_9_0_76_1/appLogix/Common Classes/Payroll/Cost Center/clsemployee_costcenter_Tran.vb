﻿'************************************************************************************************************************************
'Class Name : clsemployee_costcenter_Tran.vb
'Purpose    :
'Date       :23/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
Imports System.ComponentModel

'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsemployee_costcenter_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_costcenter_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintCostcentertranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintCostcenterunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sohail (13 Sep 2011) -- Start
    Private mdblPercentage As Double = 100
    Private mdtTran As DataTable
    'Sohail (13 Sep 2011) -- End
    Private mintPeriodunkid As Integer = 0 'Sohail (29 Mar 2017)
    'Sohail (07 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
    Private mintAllocationbyId As Integer = enAllocation.COST_CENTER
    'Sohail (07 Feb 2019) -- End

 'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    Private mstrDatabaseName As String = String.Empty  'Sohail (21 Jul 2012)
    'Sohail (19 Dec 2018) -- Start
    'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
    Private objDataOp As clsDataOperation = Nothing
    'Sohail (19 Dec 2018) -- End

    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private mblnIsInactive As Boolean = False
    'Gajanan [24-Aug-2020] -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcentertranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costcentertranunkid(ByVal strDatabaseName As String) As Integer
        Get
            Return mintCostcentertranunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcentertranunkid = value
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call GetData()
            Call GetData(strDatabaseName)
            'Sohail (21 Aug 2015) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call GetEmployeeCostCenter_Tran() 'Sohail (13 Sep 2011)
            'Sohail (21 Aug 2015) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcenterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Sohail (13 Sep 2011) -- Start
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
    'Sohail (13 Sep 2011) -- End

'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'Sohail (29 Mar 2017) -- Start
    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property
    'Sohail (29 Mar 2017) -- End

    'Sohail (19 Dec 2018) -- Start
    'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
    Public WriteOnly Property _objDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOp = value
        End Set
    End Property
    'Sohail (19 Dec 2018) -- End

    'Sohail (07 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
    Public Property _AllocationbyId() As Integer
        Get
            Return mintAllocationbyId
        End Get
        Set(ByVal value As Integer)
            mintAllocationbyId = value
        End Set
    End Property
    'Sohail (07 Feb 2019) -- End

    'Sohail (23 Mar 2020) -- Start
    'Internal Enhancement # : Showing progress counts on employee cost center screen.
    Private Shared mintProgressTotalCount As Integer
    Public Shared Property _ProgressTotalCount() As Integer
        Get
            Return mintProgressTotalCount
        End Get
        Set(ByVal value As Integer)
            mintProgressTotalCount = value
        End Set
    End Property

    Private Shared mintProgressCurrCount As Integer
    Public Shared Property _ProgressCurrCount() As Integer
        Get
            Return mintProgressCurrCount
        End Get
        Set(ByVal value As Integer)
            mintProgressCurrCount = value
        End Set
    End Property
    'Sohail (23 Mar 2020) -- End


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Public Property _Isinactive() As Boolean
        Get
            Return mblnIsInactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsInactive = value
        End Set
    End Property
    'Gajanan [24-Aug-2020] -- End

#End Region

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Other Properties "
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
#End Region
    'Sohail (21 Jul 2012) -- End

    'Sohail (13 Sep 2011) -- Start
#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("EmpCC")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("costcentertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("tranheadunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trnheadname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("costcenterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("costcentername")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("percentage")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            dCol = New DataColumn("periodunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("period_name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("end_date")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)
            'Sohail (29 Mar 2017) -- End

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            mdtTran.Columns.Add("allocationbyid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("allocationbyname", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (07 Feb 2019) -- End
            mdtTran.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False 'Sohail (13 Jan 2022)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
             dCol.DefaultValue = ""
             'Hemant (11 Mar 2018) -- End           
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (13 Sep 2011) -- End


    'Sohail (13 Sep 2011) -- Start
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Private Sub GetEmployeeCostCenter_Tran()
    'Dim dsList As DataSet = Nothing
    'Dim strQ As String = ""
    'Dim dRowID_Tran As DataRow
    'Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    ''Sohail (21 Jul 2012) -- Start
    ''TRA - ENHANCEMENT
    '    If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
    ''Sohail (21 Jul 2012) -- End

    '    Try

    '        strQ = "SELECT  premployee_costcenter_tran.costcentertranunkid " & _
    '                      ", premployee_costcenter_tran.employeeunkid " & _
    '                      ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                      ", premployee_costcenter_tran.tranheadunkid " & _
    '                      ", ISNULL(prtranhead_master.trnheadname,'' ) AS trnheadname " & _
    '                      ", premployee_costcenter_tran.costcenterunkid " & _
    '                      ", ISNULL(prcostcenter_master.costcentername, '') AS costcentername " & _
    '                      ", premployee_costcenter_tran.userunkid " & _
    '                      ", premployee_costcenter_tran.isvoid " & _
    '                      ", premployee_costcenter_tran.voiduserunkid " & _
    '                      ", premployee_costcenter_tran.voiddatetime " & _
    '                      ", premployee_costcenter_tran.voidreason " & _
    '                      ", premployee_costcenter_tran.percentage " & _
    '                      ",'' As AUD " & _
    '                "FROM    " & mstrDatabaseName & "..premployee_costcenter_tran " & _
    '                "LEFT JOIN " & mstrDatabaseName & "..hremployee_master ON premployee_costcenter_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "LEFT JOIN " & mstrDatabaseName & "..prtranhead_master ON premployee_costcenter_tran.tranheadunkid=prtranhead_master.tranheadunkid " & _
    '                "LEFT JOIN " & mstrDatabaseName & "..prcostcenter_master ON dbo.prcostcenter_master.costcenterunkid = dbo.premployee_costcenter_tran.costcenterunkid " & _
    '                "WHERE ISNULL(premployee_costcenter_tran.isvoid,0) = 0 " & _
    '                "AND premployee_costcenter_tran.employeeunkid = @employeeunkid "

    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    ''Sohail (06 Jan 2012) -- Start
    ''TRA - ENHANCEMENT
    ''strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    ''Sohail (06 Jan 2012) -- End
    '        End If


    ''S.SANDEEP [ 04 FEB 2012 ] -- START
    ''ENHANCEMENT : TRA CHANGES
    ''If UserAccessLevel._AccessLevel.Length > 0 Then
    ''    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''End If

    ''S.SANDEEP [ 01 JUNE 2012 ] -- START
    ''ENHANCEMENT : TRA DISCIPLINE CHANGES
    ''Select Case ConfigParameter._Object._UserAccessModeSetting
    ''    Case enAllocation.BRANCH
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''    Case enAllocation.DEPARTMENT_GROUP
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''    Case enAllocation.DEPARTMENT
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''    Case enAllocation.SECTION_GROUP
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''    Case enAllocation.SECTION
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''    Case enAllocation.UNIT_GROUP
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''    Case enAllocation.UNIT
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''    Case enAllocation.TEAM
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''    Case enAllocation.JOB_GROUP
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''    Case enAllocation.JOBS
    ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    ''            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    ''        End If
    ''End Select
    '        strQ &= UserAccessLevel._AccessLevelFilterString
    ''S.SANDEEP [ 01 JUNE 2012 ] -- END


    ''S.SANDEEP [ 04 FEB 2012 ] -- END

    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mdtTran.Clear()

    '        For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
    '            With dsList.Tables("List").Rows(i)
    '                dRowID_Tran = mdtTran.NewRow()

    '                dRowID_Tran.Item("costcentertranunkid") = .Item("costcentertranunkid")
    '                dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
    '                dRowID_Tran.Item("employeename") = .Item("EmpName")
    '                dRowID_Tran.Item("tranheadunkid") = .Item("tranheadunkid")
    '                dRowID_Tran.Item("trnheadname") = .Item("trnheadname")
    '                dRowID_Tran.Item("costcenterunkid") = .Item("costcenterunkid")
    '                dRowID_Tran.Item("costcentername") = .Item("costcentername")

    '                dRowID_Tran.Item("percentage") = .Item("percentage")
    '                dRowID_Tran.Item("AUD") = .Item("AUD")

    '                mdtTran.Rows.Add(dRowID_Tran)
    '            End With
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeCostCenter_Tran; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Sub
    Public Sub GetEmployeeCostCenter_Tran(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal intEmployeeUnkid As Integer _
                            , ByVal intTranHeadUnkId As Integer _
                            , ByVal strTableName As String _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strFilerString As String = "" _
                            , Optional ByVal strEmployeeUnkidList As String = "" _
                            , Optional ByVal strAdvanceFiler As String = "" _
                            )
        'Sohail (07 Feb 2019) - [strAdvanceFiler]
        'Hemant (26 Dec 2018) -- [strEmployeeUnkidList]
        'Sohail (29 Mar 2017) - [intTranHeadUnkId]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        'Dim dRowID_Tran As DataRow 'Sohail (23 Mar 2020)
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        objDataOperation = New clsDataOperation


        Try

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            Dim dsAllocation As DataSet = (New clsMasterData).GetEAllocation_Notification("List", , , True)
            Dim dicAllocation As Dictionary(Of Integer, String) = (From p In dsAllocation.Tables(0).AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)
            Dim dicAllocationName As Dictionary(Of Integer, String) = (From p In dsAllocation.Tables(0).AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("AllocationName").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            strQ = "SELECT  hremployee_master.employeeunkid " & _
                            ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
                            ", hremployee_master.employeecode " & _
                   "INTO    #tblEmp " & _
                   "FROM    " & xDatabaseName & "..hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strAdvanceFiler.Trim.Length > 0 Then
                strQ &= strAdvanceFiler
            End If

            If strEmployeeUnkidList.Trim <> "" AndAlso strEmployeeUnkidList.Length > 0 Then
                strQ &= " AND hremployee_master.employeeunkid IN (" & strEmployeeUnkidList.Trim & ") "
            Else
                strQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkid.ToString)
            End If

            'Sohail (07 Feb 2019) -- End

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'strQ = "SELECT  premployee_costcenter_tran.costcentertranunkid " & _
            '              ", premployee_costcenter_tran.employeeunkid " & _
            '              ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '              ", premployee_costcenter_tran.tranheadunkid " & _
            '              ", ISNULL(prtranhead_master.trnheadname,'' ) AS trnheadname " & _
            '              ", premployee_costcenter_tran.costcenterunkid " & _
            '              ", ISNULL(prcostcenter_master.costcentername, '') AS costcentername " & _
            '              ", premployee_costcenter_tran.userunkid " & _
            '              ", premployee_costcenter_tran.isvoid " & _
            '              ", premployee_costcenter_tran.voiduserunkid " & _
            '              ", premployee_costcenter_tran.voiddatetime " & _
            '              ", premployee_costcenter_tran.voidreason " & _
            '              ", premployee_costcenter_tran.percentage " & _
            '              ",'' As AUD " & _
            '        "FROM    " & xDatabaseName & "..premployee_costcenter_tran " & _
            '        "LEFT JOIN " & xDatabaseName & "..hremployee_master ON premployee_costcenter_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN " & xDatabaseName & "..prtranhead_master ON premployee_costcenter_tran.tranheadunkid=prtranhead_master.tranheadunkid " & _
            '        "LEFT JOIN " & xDatabaseName & "..prcostcenter_master ON dbo.prcostcenter_master.costcenterunkid = dbo.premployee_costcenter_tran.costcenterunkid " & _
            strQ &= "SELECT  * " & _
                    "FROM    ( SELECT    premployee_costcenter_tran.costcentertranunkid  " & _
                          ", premployee_costcenter_tran.employeeunkid " & _
                                        ", #tblEmp.EmpName  " & _
                          ", premployee_costcenter_tran.tranheadunkid " & _
                                      ", ISNULL(prtranhead_master.trnheadname, '') AS trnheadname " & _
                          ", premployee_costcenter_tran.costcenterunkid " & _
                          ", ISNULL(prcostcenter_master.costcentername, '') AS costcentername " & _
                                        ", ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") AS allocationbyid " & _
                          ", premployee_costcenter_tran.userunkid " & _
                          ", premployee_costcenter_tran.isvoid " & _
                          ", premployee_costcenter_tran.voiduserunkid " & _
                          ", premployee_costcenter_tran.voiddatetime " & _
                          ", premployee_costcenter_tran.voidreason " & _
                          ", premployee_costcenter_tran.percentage " & _
                                      ", ISNULL(premployee_costcenter_tran.periodunkid, 0) AS periodunkid " & _
                                      ", cfcommon_period_tran.period_name " & _
                                      ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                                      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                                      ", '' AS AUD " & _
                                        ", '' AS GUID " & _
                                        ", ISNULL(premployee_costcenter_tran.isinactive, 0) AS isinactive " & _
                                        ", CAST(0 AS BIT) AS ischecked " & _
                                        ", DENSE_RANK() OVER ( PARTITION  BY #tblEmp.EmpName, premployee_costcenter_tran.employeeunkid, prtranhead_master.trnheadname, premployee_costcenter_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO "
            'Sohail (13 Jan 2022) - [ischecked]
            'Sohail (25 Jul 2020) - [isinactive]
            'Sohail (23 Mar 2020) - [GUID]
            'Sohail (07 Feb 2019) - [NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.]

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            strQ &= ", CASE ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") "
            For Each pair In dicAllocation
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS AllocationByName "

            strQ &= ", CASE ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") "
            For Each pair In dicAllocationName
                If pair.Key <= 0 Then
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Else
                    strQ &= " WHEN " & pair.Key & "  THEN " & pair.Value & " "
                End If
            Next
            strQ &= " END AS AllocationName "
            'Sohail (07 Feb 2019) -- End

            strQ &= "       FROM    " & xDatabaseName & "..premployee_costcenter_tran " & _
                                    "JOIN #tblEmp ON premployee_costcenter_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                        "LEFT JOIN " & xDatabaseName & "..prtranhead_master ON premployee_costcenter_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                    "LEFT JOIN " & xDatabaseName & "..prcostcenter_master ON dbo.prcostcenter_master.costcenterunkid = dbo.premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.COST_CENTER) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..cfcommon_period_tran ON premployee_costcenter_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrdepartment_master ON hrdepartment_master.departmentunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.DEPARTMENT) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrjob_master ON hrjob_master.jobunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.JOBS) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrstation_master ON hrstation_master.stationunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.BRANCH) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.DEPARTMENT_GROUP) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrsection_master ON hrsection_master.sectionunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.SECTION) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrunit_master ON hrunit_master.unitunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.UNIT) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.JOB_GROUP) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrclassgroup_master ON hrclassgroup_master.classgroupunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.CLASS_GROUP) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrclasses_master ON hrclasses_master.classesunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.CLASSES) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrteam_master ON hrteam_master.teamunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.TEAM) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrunitgroup_master ON premployee_costcenter_tran.costcenterunkid = hrunitgroup_master.unitgroupunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.UNIT_GROUP) & " " & _
                                    "LEFT JOIN " & xDatabaseName & "..hrsectiongroup_master ON premployee_costcenter_tran.costcenterunkid = hrsectiongroup_master.sectiongroupunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.SECTION_GROUP) & " "
            'Sohail (07 Feb 2019) - [NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.]
            'Sohail (29 Mar 2017) -- End

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            '    If xUACQry.Trim.Length > 0 Then
            '        strQ &= xUACQry
            '    End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            'Sohail (07 Feb 2019) -- End

            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'strQ &= "WHERE premployee_costcenter_tran.isvoid = 0 " & _
            '            "AND premployee_costcenter_tran.employeeunkid = @employeeunkid " & _
            '            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            '            "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
            strQ &= "WHERE premployee_costcenter_tran.isvoid = 0 " & _
                        "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                        "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            'If strEmployeeUnkidList.Trim <> "" AndAlso strEmployeeUnkidList.Length > 0 Then
            '    strQ &= " AND premployee_costcenter_tran.employeeunkid IN (" & strEmployeeUnkidList.Trim & ") "
            'Else
            '    strQ &= " AND premployee_costcenter_tran.employeeunkid = @employeeunkid "
            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkid.ToString)
            'End If
            'Sohail (07 Feb 2019) -- End
            'Hemant (26 Dec 2018) -- End            

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            If intTranHeadUnkId > 0 Then
                strQ &= " AND premployee_costcenter_tran.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId)
            End If
            'Sohail (29 Mar 2017) -- End

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If
            'Sohail (07 Feb 2019) -- End

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            strQ &= "           ) AS A " & _
                                "WHERE   A.ROWNO = 1 " & _
                                "AND A.isinactive = 0 "
            'Sohail (25 Jul 2020) - [isinactive]

            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            'Sohail (29 Mar 2017) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkid.ToString)
            'Hemant (26 Dec 2018) -- End
            'Sohail (21 Aug 2015) -- End

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            strQ &= " DROP TABLE #tblEmp "
            'Sohail (07 Feb 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            'Sohail (23 Mar 2020) -- Start
            'Internal Enhancement # : Showing progress counts on employee cost center screen.
            'For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
            '    With dsList.Tables("List").Rows(i)
            '        dRowID_Tran = mdtTran.NewRow()

            '        dRowID_Tran.Item("costcentertranunkid") = .Item("costcentertranunkid")
            '        dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
            '        dRowID_Tran.Item("employeename") = .Item("EmpName")
            '        dRowID_Tran.Item("tranheadunkid") = .Item("tranheadunkid")
            '        dRowID_Tran.Item("trnheadname") = .Item("trnheadname")
            '        dRowID_Tran.Item("costcenterunkid") = .Item("costcenterunkid")
            '        'Sohail (07 Feb 2019) -- Start
            '        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            '        'dRowID_Tran.Item("costcentername") = .Item("costcentername")
            '        dRowID_Tran.Item("costcentername") = .Item("AllocationName")
            '        dRowID_Tran.Item("allocationbyid") = .Item("allocationbyid")
            '        dRowID_Tran.Item("allocationbyname") = .Item("AllocationByName")
            '        'Sohail (07 Feb 2019) -- End

            '        dRowID_Tran.Item("percentage") = .Item("percentage")

            '        'Sohail (29 Mar 2017) -- Start
            '        'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            '        dRowID_Tran.Item("periodunkid") = .Item("periodunkid")
            '        dRowID_Tran.Item("period_name") = .Item("period_name").ToString
            '        dRowID_Tran.Item("end_date") = .Item("end_date").ToString
            '        'Sohail (29 Mar 2017) -- End

            '        dRowID_Tran.Item("AUD") = .Item("AUD")

            '        mdtTran.Rows.Add(dRowID_Tran)
            '    End With
            'Next
            mdtTran = dsList.Tables(0)

            Dim intColIdx As Integer = 0

            'Sohail (13 Jan 2022) -- Start
            'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
            mdtTran.Columns("IsChecked").ColumnName = "IsChecked"
            mdtTran.Columns("IsChecked").SetOrdinal(intColIdx)
            intColIdx += 1
            'Sohail (13 Jan 2022) -- End

            mdtTran.Columns("costcentertranunkid").ColumnName = "costcentertranunkid"
            mdtTran.Columns("costcentertranunkid").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("employeeunkid").ColumnName = "employeeunkid"
            mdtTran.Columns("employeeunkid").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("EmpName").ColumnName = "employeename"
            mdtTran.Columns("employeename").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("tranheadunkid").ColumnName = "tranheadunkid"
            mdtTran.Columns("tranheadunkid").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("trnheadname").ColumnName = "trnheadname"
            mdtTran.Columns("trnheadname").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("costcenterunkid").ColumnName = "costcenterunkid"
            mdtTran.Columns("costcenterunkid").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("costcentername").ColumnName = "costcentername1"
            mdtTran.Columns("AllocationName").ColumnName = "costcentername"
            mdtTran.Columns("costcentername").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("allocationbyid").ColumnName = "allocationbyid"
            mdtTran.Columns("allocationbyid").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("AllocationByName").ColumnName = "allocationbyname"
            mdtTran.Columns("allocationbyname").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("percentage").ColumnName = "percentage"
            mdtTran.Columns("percentage").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("periodunkid").ColumnName = "periodunkid"
            mdtTran.Columns("periodunkid").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("period_name").ColumnName = "period_name"
            mdtTran.Columns("period_name").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("end_date").ColumnName = "end_date"
            mdtTran.Columns("end_date").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("AUD").ColumnName = "AUD"
            mdtTran.Columns("AUD").SetOrdinal(intColIdx)
            intColIdx += 1

            mdtTran.Columns("GUID").ColumnName = "GUID"
            mdtTran.Columns("GUID").SetOrdinal(intColIdx)
            intColIdx += 1


            For i = intColIdx To mdtTran.Columns.Count - 1
                mdtTran.Columns.RemoveAt(intColIdx)
            Next
            'Sohail (23 Mar 2020) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeCostCenter_Tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End
    'Sohail (13 Sep 2011) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal strDatabaseName As String)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (19 Dec 2018) -- Start
        'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
        'objDataOperation = New clsDataOperation
        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (19 Dec 2018) -- End

        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try
            strQ = "SELECT " & _
              "  costcentertranunkid " & _
              ", employeeunkid " & _
              ", tranheadunkid " & _
              ", costcenterunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(percentage,0) AS percentage " & _
              ", ISNULL(periodunkid, 1) AS periodunkid " & _
              ", ISNULL(allocationbyid, " & enAllocation.COST_CENTER & ") AS allocationbyid " & _
              ", ISNULL(premployee_costcenter_tran.isinactive, 0) AS isinactive " & _
             "FROM " & mstrDatabaseName & "..premployee_costcenter_tran " & _
             "WHERE costcentertranunkid = @costcentertranunkid "
            'Sohail (07 Feb 2019) - [allocationbyid]
            'Sohail (29 Mar 2017) - [periodunkid]
            'Sohail (13 Sep 2011)-(percentage)

            objDataOperation.AddParameter("@costcentertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcentertranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCostcentertranunkid = CInt(dtRow.Item("costcentertranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintCostcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If mdtVoiddatetime <> Nothing Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdblPercentage = CDbl(dtRow.Item("percentage")) 'Sohail (13 Sep 2011)
                mintPeriodunkid = CInt(dtRow.Item("periodunkid")) 'Sohail (29 Mar 2017)
                mintAllocationbyId = CInt(dtRow.Item("allocationbyid")) 'Sohail (07 Feb 2019)

                'Gajanan [24-Aug-2020] -- Start
                'NMB Enhancement : Allow to set account configuration mapping 
                'inactive for closed period to allow to map head on other account 
                'configuration screen from new period
                mblnIsInactive = CBool(dtRow.Item("isinactive"))
                'Gajanan [24-Aug-2020] -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (19 Dec 2018) -- Start
            'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
            'objDataOperation = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (19 Dec 2018) -- End
        End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intEmployeeUnkID As Integer = 0, Optional ByVal intTranHeadUnkID As Integer = 0) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  costcentertranunkid " & _
    '          ", premployee_costcenter_tran.employeeunkid " & _
    '          ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '          ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
    '          ", premployee_costcenter_tran.tranheadunkid " & _
    '          ", prtranhead_master.trnheadname " & _
    '          ", premployee_costcenter_tran.costcenterunkid " & _
    '          ", prcostcenter_master.costcentername " & _
    '          ", premployee_costcenter_tran.userunkid " & _
    '          ", premployee_costcenter_tran.isvoid " & _
    '          ", premployee_costcenter_tran.voiduserunkid " & _
    '          ", premployee_costcenter_tran.voiddatetime " & _
    '          ", premployee_costcenter_tran.voidreason " & _
    '          ", ISNULL(premployee_costcenter_tran.percentage,0) AS percentage " & _
    '         " FROM premployee_costcenter_tran " & _
    '         " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = premployee_costcenter_tran.employeeunkid" & _
    '         " LEFT JOIN prcostcenter_master on prcostcenter_master.costcenterunkid = premployee_costcenter_tran.costcenterunkid" & _
    '         " LEFT JOIN prtranhead_master on prtranhead_master.tranheadunkid = premployee_costcenter_tran.tranheadunkid " & _
    '         " WHERE 1=1 " 'Sohail (13 Sep 2011)-(percentage), 'Sohail (28 Jan 2012) - [employeecode]

    '        If blnOnlyActive Then
    '            strQ &= " AND ISNULL(premployee_costcenter_tran.isvoid,0) = 0 " 'Sohail (16 Oct 2010)
    '        End If

    '        'Anjan (09 Aug 2011)-Start
    '        'Issue : For including setting of acitve and inactive employee.
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'Anjan (09 Aug 2011)-End 

    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select
    '        strQ &= UserAccessLevel._AccessLevelFilterString
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Sohail (24 Jun 2011) -- End

    '        'Sohail (07 Aug 2010) -- Start
    '        If intEmployeeUnkID > 0 Then
    '            strQ &= " AND premployee_costcenter_tran.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
    '        End If
    '        If intTranHeadUnkID > 0 Then
    '            strQ &= " AND premployee_costcenter_tran.tranheadunkid = @tranheadunkid "
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
    '        End If
    '        'Sohail (07 Aug 2010) -- End

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal strTableName As String _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal intEmployeeUnkID As Integer = 0 _
                            , Optional ByVal intTranHeadUnkID As Integer = 0 _
                            , Optional ByVal strFilerString As String = "" _
                            , Optional ByVal dtPeriodEndDate As Date = Nothing _
                            , Optional ByVal strSortField As String = "" _
                            , Optional ByVal strAdvanceFiler As String = "" _
                            , Optional ByVal mblnAddGrouping As Boolean = False _
                            , Optional ByVal mintOnlyInActive As Integer = enTranHeadActiveInActive.ACTIVE _
                            , Optional ByVal blnApplyDateFilter As Boolean = True _
                            ) As DataSet
        'Sohail (12 Oct 2021) - [blnApplyDateFilter]
        'Hemant (11 Mar 2018) -- [mblnAddGrouping]
        'Sohail (07 Feb 2019) - [strAdvanceFiler]
        'Sohail (29 Mar 2017) - [dtPeriodEndDate, strSortField]
        'Gajanan [24-Aug-2020] -- [mintOnlyInActive]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (29 Mar 2017) -- Start
        'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
        If dtPeriodEndDate = Nothing Then
            Throw New Exception(Language.getMessage(mstrModuleName, 3, "Period information is mandatory in employee cost center."))
            Exit Function
        End If
        'Sohail (29 Mar 2017) -- End

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        If blnApplyDateFilter = True Then 'Sohail (12 Oct 2021)
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        End If 'Sohail (12 Oct 2021)
        If blnApplyUserAccessFilter = True Then 'Sohail (12 Oct 2021)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        End If 'Sohail (12 Oct 2021)
        If strAdvanceFiler.Trim.Length > 0 Then 'Sohail (12 Oct 2021)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        End If 'Sohail (12 Oct 2021)

        objDataOperation = New clsDataOperation

        Try
            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            Dim dsAllocation As DataSet = (New clsMasterData).GetEAllocation_Notification("List", , , True)
            Dim dicAllocation As Dictionary(Of Integer, String) = (From p In dsAllocation.Tables(0).AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)
            Dim dicAllocationName As Dictionary(Of Integer, String) = (From p In dsAllocation.Tables(0).AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("AllocationName").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            strQ = "SELECT  hremployee_master.employeeunkid " & _
                            ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename  " & _
                            ", hremployee_master.employeecode " & _
                   "INTO    #tblEmp " & _
                   "FROM    hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strAdvanceFiler.Trim.Length > 0 Then
                strQ &= strAdvanceFiler
            End If

            If intEmployeeUnkID > 0 Then
                strQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
            End If
            'Sohail (07 Feb 2019) -- End

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'strQ = "SELECT " & _
            '  "  costcentertranunkid " & _
            '  ", premployee_costcenter_tran.employeeunkid " & _
            '  ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
            '  ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
            '  ", premployee_costcenter_tran.tranheadunkid " & _
            '  ", prtranhead_master.trnheadname " & _
            '  ", premployee_costcenter_tran.costcenterunkid " & _
            '  ", prcostcenter_master.costcentername " & _
            '  ", premployee_costcenter_tran.userunkid " & _
            '  ", premployee_costcenter_tran.isvoid " & _
            '  ", premployee_costcenter_tran.voiduserunkid " & _
            '  ", premployee_costcenter_tran.voiddatetime " & _
            '  ", premployee_costcenter_tran.voidreason " & _
            '  ", ISNULL(premployee_costcenter_tran.percentage,0) AS percentage " & _
            '  ", prcostcenter_master.costcentercode " & _
            ' " FROM premployee_costcenter_tran " & _
            ' " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = premployee_costcenter_tran.employeeunkid" & _
            ' " LEFT JOIN prcostcenter_master on prcostcenter_master.costcenterunkid = premployee_costcenter_tran.costcenterunkid" & _
            ' " LEFT JOIN prtranhead_master on prtranhead_master.tranheadunkid = premployee_costcenter_tran.tranheadunkid "
            'Sohail (02 Sep 2016) - [costcentercode]
            strQ &= "SELECT  A.costcentertranunkid  " & _
                          ", A.employeeunkid " & _
                          ", A.employeecode " & _
                          ", A.employeename " & _
                          ", A.tranheadunkid " & _
                          ", A.trnheadname " & _
                          ", A.costcenterunkid " & _
                          ", A.costcentercode " & _
                          ", A.costcentername " & _
                          ", A.percentage " & _
                          ", A.periodunkid " & _
                          ", A.period_name " & _
                          ", A.start_date " & _
                          ", A.end_date " & _
                          ", A.statusid " & _
                          ", A.allocationbyid " & _
                          ", A.AllocationByName " & _
                          ", A.AllocationName " & _
                          ", A.isinactive " & _
                          ", CAST(0 AS BIT) AS IsGrp " & _
                          ", CAST(0 AS BIT) AS IsChecked " & _
                    "FROM    ( SELECT    costcentertranunkid  " & _
              ", premployee_costcenter_tran.employeeunkid " & _
                                        ", #tblEmp.employeecode " & _
                                        ", #tblEmp.employeename  " & _
              ", premployee_costcenter_tran.tranheadunkid " & _
              ", prtranhead_master.trnheadname " & _
              ", premployee_costcenter_tran.costcenterunkid " & _
              ", prcostcenter_master.costcentername " & _
                                        ", ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") AS allocationbyid " & _
              ", premployee_costcenter_tran.userunkid " & _
              ", premployee_costcenter_tran.isvoid " & _
                                        ", ISNULL(premployee_costcenter_tran.isinactive, 0) AS isinactive " & _
              ", premployee_costcenter_tran.voiduserunkid " & _
              ", premployee_costcenter_tran.voiddatetime " & _
              ", premployee_costcenter_tran.voidreason " & _
                                      ", ISNULL(premployee_costcenter_tran.percentage, 0) AS percentage " & _
              ", prcostcenter_master.costcentercode " & _
                                      ", ISNULL(premployee_costcenter_tran.periodunkid, 1) AS periodunkid " & _
                                      ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                                      ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112), '19000101') AS start_date " & _
                                      ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112), '19000101') AS end_date " & _
                                      ", ISNULL(cfcommon_period_tran.statusid, 2) AS statusid " & _
                                        ", DENSE_RANK() OVER ( PARTITION  BY #tblEmp.employeename, premployee_costcenter_tran.employeeunkid, prtranhead_master.trnheadname, premployee_costcenter_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO "
            'Sohail (13 Jan 2022) - [IsChecked]
               'Hemant (11 Mar 2018) -- [IsGrp]
            'Sohail (07 Feb 2019) - [NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.]

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            strQ &= ", CASE ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") "
            For Each pair In dicAllocation
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS AllocationByName "

            strQ &= ", CASE ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") "
            For Each pair In dicAllocationName
                If pair.Key <= 0 Then
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Else
                    strQ &= " WHEN " & pair.Key & "  THEN " & pair.Value & " "
                End If
            Next
            strQ &= " END AS AllocationName "
            'Sohail (07 Feb 2019) -- End

            strQ &= "           FROM      premployee_costcenter_tran " & _
                                        "JOIN #tblEmp ON #tblEmp.employeeunkid = premployee_costcenter_tran.employeeunkid " & _
                                        "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.COST_CENTER) & " " & _
                                        "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = premployee_costcenter_tran.tranheadunkid " & _
                                        "LEFT JOIN cfcommon_period_tran ON premployee_costcenter_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                        "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.DEPARTMENT) & " " & _
                                        "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.JOBS) & " " & _
                                        "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.BRANCH) & " " & _
                                        "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.DEPARTMENT_GROUP) & " " & _
                                        "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.SECTION) & " " & _
                                        "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.UNIT) & " " & _
                                        "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.JOB_GROUP) & " " & _
                                        "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.CLASS_GROUP) & " " & _
                                        "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.CLASSES) & " " & _
                                        "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.TEAM) & " " & _
                                        "LEFT JOIN hrunitgroup_master ON premployee_costcenter_tran.costcenterunkid = hrunitgroup_master.unitgroupunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.UNIT_GROUP) & " " & _
                                        "LEFT JOIN hrsectiongroup_master ON premployee_costcenter_tran.costcenterunkid = hrsectiongroup_master.sectiongroupunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.SECTION_GROUP) & " "
            'Sohail (07 Feb 2019) - [NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.]
            'Sohail (29 Mar 2017) -- End

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            '    If xUACQry.Trim.Length > 0 Then
            '        strQ &= xUACQry
            '    End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            'Sohail (07 Feb 2019) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'strQ &= " WHERE 1=1 "
            strQ &= " WHERE premployee_costcenter_tran.isvoid = 0 "
            'Sohail (21 Aug 2015) -- End

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            'If intEmployeeUnkID > 0 Then
            '    strQ &= " AND premployee_costcenter_tran.employeeunkid = @employeeunkid "
            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
            'End If
            'Sohail (07 Feb 2019) -- End

            If intTranHeadUnkID > 0 Then
                strQ &= " AND premployee_costcenter_tran.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
            End If

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            If dtPeriodEndDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            End If

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If
            'Sohail (07 Feb 2019) -- End

            strQ &= " ) AS A "

            strQ &= "WHERE   1 = 1 "

            If dtPeriodEndDate <> Nothing Then
                strQ &= " AND   ROWNO = 1 "
            End If

            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            If mintOnlyInActive = enTranHeadActiveInActive.INACTIVE Then
                strQ &= "AND A.isinactive = 1 "
            Else
                strQ &= "AND A.isinactive = 0 "
            End If
            'Gajanan [24-Aug-2020] -- End

            If strSortField.Trim.Length > 0 Then
                strQ += " ORDER BY " & strSortField
            End If
            'Sohail (29 Mar 2017) -- End

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            strQ &= " DROP TABLE #tblEmp "
            'Sohail (07 Feb 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            Dim dtFinal As DataTable

            If mblnAddGrouping = True Then
                dsList.Tables(0).Columns.Add("CostCenterColumn", System.Type.GetType("System.String")).Expression = "AllocationByName + ' : ' + AllocationName "
                If strSortField.Contains("AllocationBy") = True Then
                    dsList.Tables(0).Columns.Add("GroupName", System.Type.GetType("System.String")).Expression = "AllocationByName + ' : ' + AllocationName "
                    Dim lstRow = (From p In dsList.Tables(0) Select New With {Key .AllocationByName = p.Item("AllocationByName").ToString, Key .AllocationName = p.Item("AllocationName").ToString}).ToList.Distinct

                    For Each itm In lstRow
                        Dim dr As DataRow = dsList.Tables(0).NewRow

                        dr.Item("AllocationByName") = itm.AllocationByName
                        dr.Item("AllocationName") = itm.AllocationName
                        dr.Item("IsGrp") = True
                        'Sohail (13 Jan 2022) -- Start
                        'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
                        dr.Item("IsChecked") = False
                        'Sohail (13 Jan 2022) -- End

                        dsList.Tables(0).Rows.Add(dr)
                    Next
                    dsList.Tables(0).AcceptChanges()


                    dtFinal = New DataView(dsList.Tables(0), "", "GroupName, IsGrp DESC", DataViewRowState.CurrentRows).ToTable

                Else
                    dsList.Tables(0).Columns.Add("GroupName", System.Type.GetType("System.String")).Expression = "employeename + ' : [' + employeecode + ']'"
                    Dim lstRow = (From p In dsList.Tables(0) Select New With {Key .EmployeeName = p.Item("employeename").ToString, Key .EmployeeCode = p.Item("employeecode").ToString, Key .EmployeeUnkId = p.Item("employeeunkid").ToString}).ToList.Distinct

                    For Each itm In lstRow
                        Dim dr As DataRow = dsList.Tables(0).NewRow

                        dr.Item("employeename") = itm.EmployeeName
                        dr.Item("employeecode") = itm.EmployeeCode
                        dr.Item("IsGrp") = True
                        'Sohail (13 Jan 2022) -- Start
                        'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
                        dr.Item("employeeunkid") = itm.EmployeeUnkId
                        dr.Item("IsChecked") = False
                        'Sohail (13 Jan 2022) -- End

                        dsList.Tables(0).Rows.Add(dr)
                    Next
                    dsList.Tables(0).AcceptChanges()
                    dtFinal = New DataView(dsList.Tables(0), "", "GroupName, IsGrp DESC", DataViewRowState.CurrentRows).ToTable
                End If

                dsList.Tables.Clear()
                dsList.Tables.Add(dtFinal)
            End If
            'Hemant (11 Mar 2018) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (21 Aug 2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (premployee_costcenter_tran) </purpose>
    Public Function Insert(ByVal strDatabaseName As String, ByVal objDataOpr As clsDataOperation, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName, dtCurrentDateAndTime]

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If isExist(mintEmployeeunkid, mintTranheadunkid) Then
        'Sohail (29 Mar 2017) -- Start
        'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
        'If isExist(strDatabaseName, mintEmployeeunkid, mintTranheadunkid) Then
        'Sohail (07 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
        'If isExist(strDatabaseName, mintEmployeeunkid, mintTranheadunkid, mintCostcenterunkid, mintPeriodunkid, , objDataOpr) Then
        If isExist(strDatabaseName, mintEmployeeunkid, mintTranheadunkid, mintAllocationbyId, mintCostcenterunkid, mintPeriodunkid, , objDataOpr) Then
            'Sohail (07 Feb 2019) -- End
            'Sohail (29 Mar 2017) -- End
            'Sohail (21 Aug 2015) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This CostCenter is already defined for this employee. Please define new CostCenter.")
            Return False
        End If

        'Dim objDataOperation As clsDataOperation 'Sohail (11 Nov 2010), 'Sohail (03 Dec 2013)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Sohail (03 Dec 2013) -- Start
        'Enhancement - TBC
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction() 'Sohail (11 Nov 2010)
        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            'objDataOperation.ClearParameters() 'Sohail (29 Mar 2017)
        End If
        'Sohail (03 Dec 2013) -- End
        objDataOperation.ClearParameters() 'Sohail (29 Mar 2017)

        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString) 'Sohail (13 Sep 2011)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString) 'Sohail (29 Mar 2017)
            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyId.ToString) 'Sohail (07 Feb 2019)
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInactive.ToString)

            strQ = "INSERT INTO " & mstrDatabaseName & "..premployee_costcenter_tran ( " & _
              "  employeeunkid " & _
              ", tranheadunkid " & _
              ", costcenterunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", percentage" & _
              ", periodunkid " & _
              ", allocationbyid " & _
              ", isinactive " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @tranheadunkid " & _
              ", @costcenterunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @percentage" & _
              ", @periodunkid " & _
              ", @allocationbyid " & _
              ", @isinactive " & _
            "); SELECT @@identity"
            'Sohail (07 Feb 2019) - [allocationbyid]
            'Sohail (29 Mar 2017) - [periodunkid]
            'Sohail (13 Sep 2011)-(percentage)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCostcentertranunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (11 Nov 2010) -- Start
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForEmpCostCenter(objDataOperation, 1)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForEmpCostCenter(objDataOperation, 1) = False Then
            If InsertAuditTrailForEmpCostCenter(strDatabaseName, objDataOperation, 1, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'Sohail (03 Dec 2013) -- Start
            'Enhancement - TBC
            'objDataOperation.ReleaseTransaction(True)
            If objDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Sohail (03 Dec 2013) -- End

            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False) 'Sohail (11 Nov 2010)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (premployee_costcenter_tran) </purpose>
    Public Function Update(ByVal strDatabaseName As String, ByVal objDataOpr As clsDataOperation, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName, dtCurrentDateAndTime]

        'If isExist(mintEmployeeunkid, mintTranheadunkid, mintCostcenterunkid, mintCostcentertranunkid) Then
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If isExist(mintEmployeeunkid, mintTranheadunkid, mintCostcentertranunkid) Then
        'Sohail (29 Mar 2017) -- Start
        'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
        'If isExist(strDatabaseName, mintEmployeeunkid, mintTranheadunkid, mintCostcentertranunkid) Then
        'Sohail (07 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
        'If isExist(strDatabaseName, mintEmployeeunkid, mintTranheadunkid, mintCostcenterunkid, mintPeriodunkid, mintCostcentertranunkid, objDataOpr) Then
        If isExist(strDatabaseName, mintEmployeeunkid, mintTranheadunkid, mintAllocationbyId, mintCostcenterunkid, mintPeriodunkid, mintCostcentertranunkid, objDataOpr) Then
            'Sohail (07 Feb 2019) -- End
            'Sohail (29 Mar 2017) -- End
            'Sohail (21 Aug 2015) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This CostCenter is already defined for this employee. Please define new CostCenter.")
            Return False
        End If

        'Dim objDataOperation As clsDataOperation 'Sohail (11 Nov 2010), 'Sohail (03 Dec 2013)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 Dec 2013) -- Start
        'Enhancement - TBC
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction() 'Sohail (11 Nov 2010)
        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            'objDataOperation.ClearParameters() 'Sohail (29 Mar 2017)
        End If
        'Sohail (03 Dec 2013) -- End
        objDataOperation.ClearParameters() 'Sohail (29 Mar 2017)

        Try
            objDataOperation.AddParameter("@costcentertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcentertranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString) 'Sohail (13 Sep 2011)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString) 'Sohail (29 Mar 2017)
            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyId.ToString) 'Sohail (07 Feb 2019)

            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInactive.ToString)
            'Gajanan [24-Aug-2020] -- End

            strQ = "UPDATE premployee_costcenter_tran SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", tranheadunkid = @tranheadunkid" & _
              ", costcenterunkid = @costcenterunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", percentage = @percentage " & _
              ", periodunkid = @periodunkid " & _
              ", allocationbyid = @allocationbyid " & _
              ", isinactive = @isinactive " & _
            "WHERE costcentertranunkid = @costcentertranunkid "
            'Sohail (07 Feb 2019) - [allocationbyid]
            'Sohail (29 Mar 2017) - [periodunkid]
            'Sohail (13 Sep 2011)-(percentage)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (11 Nov 2010) -- Start

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForEmpCostCenter(objDataOperation, 2)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForEmpCostCenter(objDataOperation, 2) = False Then
            If IsTableDataUpdate(mintCostcentertranunkid, objDataOperation) = True Then 'Sohail (21 Jan 2022)
            If InsertAuditTrailForEmpCostCenter(strDatabaseName, objDataOperation, 2, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            End If 'Sohail (21 Jan 2022)
            'Anjan (11 Jun 2011)-End

            'Sohail (03 Dec 2013) -- Start
            'Enhancement - TBC
            'objDataOperation.ReleaseTransaction(True)
            If objDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Sohail (03 Dec 2013) -- End
            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False) 'Sohail (11 Nov 2010)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Sohail (21 Jan 2022) -- Start
    'Enhancement :  : AT Log data optimization on employee cost center.
    Public Function IsTableDataUpdate(ByVal intUnkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strFields As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strFields = " employeeunkid,tranheadunkid,costcenterunkid,percentage,periodunkid,allocationbyid,isinactive "

            strQ = "WITH CTE AS ( " & _
                            "SELECT TOP 1 " & strFields & " " & _
                            "FROM atpremployee_costcenter_tran " & _
                            "WHERE atpremployee_costcenter_tran.costcentertranunkid = @costcentertranunkid " & _
                            "AND atpremployee_costcenter_tran.audittype <> 3 " & _
                            "ORDER BY atpremployee_costcenter_tran.atcostcentertranunkid DESC " & _
                        ") " & _
                    " " & _
                    "SELECT " & strFields & " " & _
                    "FROM premployee_costcenter_tran " & _
                    "WHERE premployee_costcenter_tran.costcentertranunkid = @costcentertranunkid " & _
                    "EXCEPT " & _
                    "SELECT * FROM cte "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@costcentertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (21 Jan 2022) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (premployee_costcenter_tran) </purpose>
    Public Function Delete(ByVal strDatabaseName As String, ByVal intUnkid As Integer, ByVal dtVoiddatetime As DateTime, ByVal xDataOp As clsDataOperation) As Boolean
		'Hemant (26 Dec 2018) -- [xDataOp]        
        'Sohail (21 Aug 2015) - [strDatabaseName]
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

		'Hemant (26 Dec 2018) -- Start
        'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
        'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
		'Dim objDataOperation As clsDataOperation 'Sohail (11 Nov 2010)
		'Hemant (26 Dec 2018) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

		'Hemant (26 Dec 2018) -- Start
        'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
        'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction() 'Sohail (11 Nov 2010)
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
		'Hemant (26 Dec 2018) -- End

        Try

            'Sandeep [ 16 Oct 2010 ] -- Start
            'strQ = "Update premployee_costcenter_tran set isvoid = 1,voiddatetime = getdate(),voidreason = @voidreason " & _
            '"WHERE costcentertranunkid = @costcentertranunkid "

            strQ = "Update premployee_costcenter_tran set isvoid = 1,voiddatetime = @voiddate,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                      "WHERE costcentertranunkid = @costcentertranunkid "

            'Sohail (27 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@voiddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime)
            'Sohail (27 Jul 2012) -- End
            'Hemant (20 June 2018) -- Start
            '  objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Hemant (20 June 2018) -- End
            'Sandeep [ 16 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@costcentertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (19 Dec 2018) -- Start
            'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
            Me._objDataOp = objDataOperation
            'Sohail (19 Dec 2018) -- End

            'Sohail (11 Nov 2010) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Me._Costcentertranunkid = intUnkid
            Me._Costcentertranunkid(strDatabaseName) = intUnkid
            'Sohail (21 Aug 2015) -- End

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForEmpCostCenter(objDataOperation, 3)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForEmpCostCenter(objDataOperation, 3) = False Then
            If InsertAuditTrailForEmpCostCenter(strDatabaseName, objDataOperation, 3, dtVoiddatetime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End
			'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
			'Hemant (26 Dec 2018) -- End
            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False) 'Sohail (11 Nov 2010)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
			'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
			'Hemant (26 Dec 2018) -- End
        End Try
    End Function

    'Sohail (24 Jan 2022) -- Start
    'Enhancement :  : Allow to void multiple cost centers at a time.
    Public Function VoideAll(ByVal strDatabaseName As String _
                             , ByVal xDataOp As clsDataOperation _
                             , ByVal strUnkIDs As String _
                             , ByVal intUserUnkId As Integer _
                             , ByVal strVoidReason As String _
                             , ByVal dtCurrentDateAndTime As Date _
                             , ByVal strIP As String _
                             , ByVal strHostName As String _
                             , ByVal strFormName As String _
                             , ByVal blnIsweb As Boolean _
                             ) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "INSERT INTO " & strDatabaseName & "..atpremployee_costcenter_tran ( " & _
                  "  costcentertranunkid " & _
                  ", employeeunkid " & _
                  ", tranheadunkid " & _
                  ", costcenterunkid " & _
                  ", periodunkid " & _
                  ", allocationbyid " & _
                  ", percentage " & _
                  ", isinactive " & _
                  ", audittype " & _
                  ", audituserunkid " & _
                  ", auditdatetime " & _
                  ", ip " & _
                  ", machine_name" & _
                  ", form_name " & _
                  ", module_name1 " & _
                  ", module_name2 " & _
                  ", module_name3 " & _
                  ", module_name4 " & _
                  ", module_name5 " & _
                  ", isweb " & _
            ") " & _
            " SELECT " & _
                  "  premployee_costcenter_tran.costcentertranunkid " & _
                  ", premployee_costcenter_tran.employeeunkid " & _
                  ", premployee_costcenter_tran.tranheadunkid " & _
                  ", premployee_costcenter_tran.costcenterunkid " & _
                  ", premployee_costcenter_tran.periodunkid " & _
                  ", premployee_costcenter_tran.allocationbyid " & _
                  ", premployee_costcenter_tran.percentage " & _
                  ", premployee_costcenter_tran.isinactive " & _
                  ", @audittype " & _
                  ", @audituserunkid " & _
                  ", @auditdatetime " & _
                  ", @ip " & _
                  ", @machine_name" & _
                  ", @form_name " & _
                  ", '' " & _
                  ", '' " & _
                  ", '' " & _
                  ", '' " & _
                  ", '' " & _
                  ", @isweb " & _
            "FROM     premployee_costcenter_tran " & _
            "WHERE    premployee_costcenter_tran.isvoid = 0 " & _
                    " AND premployee_costcenter_tran.costcentertranunkid IN (" & strUnkIDs & ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 3)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, strFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsweb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE premployee_costcenter_tran SET " & _
                        "  isvoid = 1 " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                        ", voiduserunkid = @voiduserunkid " & _
                     "WHERE isvoid = 0 AND costcentertranunkid IN (" & strUnkIDs & ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoideAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (24 Jan 2022) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (19 Nov 2010) -- Start
            'strQ = "<Query>"
            'strQ = "SELECT  DISTINCT premployee_costcenter_tran.costcenterunkid " & _
            '        "FROM    premployee_costcenter_tran " & _
            '                "INNER JOIN prpayrollprocess_tran ON premployee_costcenter_tran.costcenterunkid = prpayrollprocess_tran.costcenterunkid " & _
            '        "WHERE   ISNULL(premployee_costcenter_tran.isvoid, 0) = 0 " & _
            '                "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '                "AND costcentertranunkid = @costcentertranunkid "
            ''Sohail (19 Nov 2010) -- End

            'objDataOperation.AddParameter("@costcentertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strDatabaseName As String, ByVal intEmployeeunkid As Integer, ByVal intTranheadunkid As Integer, ByVal intAllocationById As Integer, ByVal intCostcenterunkid As Integer, ByVal intPeriodID As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        'Sohail (07 Feb 2019) - [intAllocationById]
        'Sohail (29 Mar 2017) - [intCostcenterunkid, intPeriodID, objDataOpr]
        'Public Function isExist(ByVal intEmployeeunkid As Integer, ByVal intTranheadunkid As Integer, ByVal intCostcenterunkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (29 Mar 2017) -- Start
        'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
        'objDataOperation = New clsDataOperation
        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (29 Mar 2017) -- End


        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try
            'Sohail (30 Aug 2010) -- Start
            'Changes:costcenterunkid filter removed
            'strQ = "SELECT " & _
            '  "  costcentertranunkid " & _
            '  ", employeeunkid " & _
            '  ", tranheadunkid " & _
            '  ", costcenterunkid " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            ' " FROM premployee_costcenter_tran " & _
            ' " WHERE employeeunkid = @employeeunkid " & _
            ' " AND tranheadunkid = @tranheadunkid " & _
            ' " AND costcenterunkid=@costcenterunkid AND isvoid = 0 "
            'Sohail (13 Sep 2011) -- Start
            'strQ = "SELECT " & _
            '  "  costcentertranunkid " & _
            '  ", employeeunkid " & _
            '  ", tranheadunkid " & _
            '  ", costcenterunkid " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            ' " FROM premployee_costcenter_tran " & _
            ' " WHERE employeeunkid = @employeeunkid " & _
            ' " AND tranheadunkid = @tranheadunkid " & _
            ' " AND ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010)
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'strQ = "SELECT  costcentertranunkid " & _
            '  ", employeeunkid " & _
            '  ", tranheadunkid " & _
            '  ", costcenterunkid " & _
            '              ", SUM(percentage) AS percentage " & _
            ' " FROM " & mstrDatabaseName & "..premployee_costcenter_tran " & _
            ' " WHERE employeeunkid = @employeeunkid " & _
            ' " AND tranheadunkid = @tranheadunkid " & _
            '                "AND ISNULL(isvoid, 0) = 0 " & _
            '"GROUP BY costcentertranunkid, employeeunkid, tranheadunkid, costcenterunkid " & _
            '        "HAVING SUM(percentage)>= 100 "
            ''Sohail (13 Sep 2011) -- End
            ''Sohail (30 Aug 2010) -- End
            strQ = "SELECT  costcentertranunkid " & _
              ", employeeunkid " & _
              ", tranheadunkid " & _
              ", costcenterunkid " & _
              ", allocationbyid " & _
             " FROM " & mstrDatabaseName & "..premployee_costcenter_tran " & _
                 " WHERE isvoid = 0 " & _
                         "AND employeeunkid = @employeeunkid " & _
                         "AND tranheadunkid = @tranheadunkid " & _
                         "AND costcenterunkid = @costcenterunkid " & _
                         "AND periodunkid = @periodunkid " & _
                         "AND allocationbyid = @allocationbyid "
            'Sohail (07 Feb 2019) - [allocationbyid]

            If intUnkid > 0 Then
                strQ &= " AND costcentertranunkid <> @costcentertranunkid"
            End If
            'Sohail (29 Mar 2017) -- End



            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranheadunkid)
            'Sohail (30 Aug 2010) -- Start
            'objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostcenterunkid)
            'Sohail (30 Aug 2010) -- End
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostcenterunkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
            'Sohail (29 Mar 2017) -- End
            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationById)
            'Sohail (07 Feb 2019) -- End
            objDataOperation.AddParameter("@costcentertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function



    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Public Function InactiveAll(ByVal strDatabaseName As String, _
                                ByVal strUnkIDs As String, _
                                ByVal dtCurrentDateAndTime As Date, _
                                ByVal xDataOp As clsDataOperation, _
                                ByVal intPeriodid As Integer) As Boolean
        'If isInactive(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, this account is already inactive.")
        '    Return False
        'End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "INSERT INTO premployee_costcenter_tran (  " & _
            "employeeunkid " & _
            ",tranheadunkid " & _
            ",costcenterunkid " & _
            ",userunkid " & _
            ",isvoid " & _
            ",voiduserunkid " & _
            ",voiddatetime " & _
            ",voidreason " & _
            ",percentage " & _
            ",periodunkid " & _
            ",allocationbyid " & _
            ",isInactive " & _
            ") SELECT " & _
            " employeeunkid " & _
            ",tranheadunkid " & _
            ",costcenterunkid " & _
            ",userunkid " & _
            ",@isvoid " & _
            ",voiduserunkid " & _
            ",voiddatetime " & _
            ",voidreason " & _
            ",percentage " & _
            ",@periodunkid " & _
            ",allocationbyid " & _
            ",@isInactive " & _
            " FROM premployee_costcenter_tran " & _
            "WHERE costcentertranunkid = @costcentertranunkid " & _
            "; SELECT @@identity"

            For Each id As String In strUnkIDs.Split(",")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@costcentertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(id))

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim newId As Integer
                newId = dsList.Tables(0).Rows(0).Item(0)
                dsList = Nothing


                Me._objDataOp = objDataOperation
                Me._Costcentertranunkid(strDatabaseName) = newId

                If InsertAuditTrailForEmpCostCenter(strDatabaseName, objDataOperation, 1, dtCurrentDateAndTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InactiveAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isInactive(ByVal strUnkIDs As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT * FROM ( " & _
              "SELECT  costcentertranunkid " & _
              ", ISNULL(premployee_costcenter_tran.isinactive, 0) AS isinactive " & _
              "FROM premployee_costcenter_tran " & _
              "WHERE isvoid = 0 and isinactive = 1 and costcentertranunkid IN ( " & strUnkIDs & " ) "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isInactive; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [24-Aug-2020] -- End


    'Sohail (11 Nov 2010) -- Start

    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForEmpCostCenter(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForEmpCostCenter(ByVal strDatabaseName As String, ByVal xDataOp As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Hemant (26 Dec 2018)-- [objDataOperation = xDataOp]
        'Sohail (21 Aug 2015) - [strDatabaseName, dtCurrentDateAndTime]
        'Anjan (11 Jun 2011)-End

        Dim strQ As String = ""
        Dim exForce As Exception

		'Hemant (26 Dec 2018) -- Start
        'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
        'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
        'objDataOperation = New clsDataOperation
		'Hemant (26 Dec 2018) -- End

        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try

            strQ = "INSERT INTO " & mstrDatabaseName & "..atpremployee_costcenter_tran ( " & _
                  "  costcentertranunkid " & _
                  ", employeeunkid " & _
                  ", tranheadunkid " & _
                  ", costcenterunkid " & _
                  ", audittype " & _
                  ", audituserunkid " & _
                  ", auditdatetime " & _
                  ", ip " & _
                  ", machine_name" & _
                  ", percentage " & _
                  ", form_name " & _
                  ", module_name1 " & _
                  ", module_name2 " & _
                  ", module_name3 " & _
                  ", module_name4 " & _
                  ", module_name5 " & _
                  ", isweb " & _
                  ", periodunkid " & _
                  ", allocationbyid " & _
                  ", isinactive " & _
            ") VALUES (" & _
                  "  @costcentertranunkid " & _
                  ", @employeeunkid " & _
                  ", @tranheadunkid " & _
                  ", @costcenterunkid " & _
                  ", @audittype " & _
                  ", @audituserunkid " & _
                  ", @auditdatetime " & _
                  ", @ip " & _
                  ", @machine_name" & _
                  ", @percentage " & _
                       ", @form_name " & _
                       ", @module_name1 " & _
                       ", @module_name2 " & _
                       ", @module_name3 " & _
                       ", @module_name4 " & _
                       ", @module_name5 " & _
                       ", @isweb " & _
                       ", @periodunkid " & _
                       ", @allocationbyid " & _
                  ", @isinactive " & _
            "); SELECT @@identity"
            'Sohail (07 Feb 2019) - [allocationbyid]
            'Sohail (29 Mar 2017) - [periodunkid]
            'Sohail (13 Sep 2011)-(percentage)
            'Gajanan [24-Aug-2020] -- [isinactive]

            xDataOp.ClearParameters()
            xDataOp.AddParameter("@costcentertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcentertranunkid.ToString)
            xDataOp.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            xDataOp.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            xDataOp.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            xDataOp.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            xDataOp.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (23 Apr 2012) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            xDataOp.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            xDataOp.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            xDataOp.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            xDataOp.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString) 'Sohail (13 Sep 2011)
            xDataOp.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString) 'Sohail (29 Mar 2017)
            xDataOp.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyId.ToString) 'Sohail (07 Feb 2019)

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                xDataOp.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                xDataOp.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                xDataOp.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                xDataOp.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                xDataOp.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                xDataOp.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            xDataOp.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            xDataOp.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            xDataOp.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            xDataOp.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            xDataOp.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInactive)
            'Gajanan [24-Aug-2020] -- End

            xDataOp.ExecNonQuery(strQ)

            If xDataOp.ErrorMessage <> "" Then
                exForce = New Exception(xDataOp.ErrorNumber & ": " & xDataOp.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End

        Catch ex As Exception
			'Hemant (26 Dec 2018) -- Start
        	'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
        	'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'DisplayError.Show("-1", ex.Message, "InsertAuditTrailForEmpCostCenter", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForEmpCostCenter; Module Name: " & mstrModuleName)
			'Hemant (26 Dec 2018) -- End
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End
        Finally
			'Hemant (26 Dec 2018) -- Start
        	'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
        	'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'If objDataOperation Is Nothing Then objDataOperation = Nothing
			'Hemant (26 Dec 2018) -- End
        End Try
    End Function
    'Sohail (11 Nov 2010) -- End

 'Sohail (13 Sep 2011) -- Start
    Public Function InsertUpdateDelete_EmpCostCenter(ByVal strDatabaseName As String, ByVal dtCurrentDateAndTime As Date, ByVal xDataOp As clsDataOperation, ByVal bw As BackgroundWorker) As Boolean
        'Sohail (23 Mar 2020) - [bw]
        'Hemant (26 Dec 2018) -- [xDataOp]
        'Sohail (21 Aug 2015) - [strDatabaseName, dtCurrentDateAndTime]
        'Dim objDataOperation As clsDataOperation

        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()
            If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()
            'Hemant (26 Dec 2018) -- End

            'Sohail (23 Mar 2020) -- Start
            'Internal Enhancement # : Showing progress counts on employee cost center screen.
            If bw IsNot Nothing Then
                bw.ReportProgress(0)
            End If
            clsemployee_costcenter_Tran._ProgressTotalCount = mdtTran.Rows.Count
            clsemployee_costcenter_Tran._ProgressCurrCount = 0 'For web
            Dim intCount As Integer = 0
            'Sohail (23 Mar 2020) -- End

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        mintCostcentertranunkid = CInt(.Item("costcentertranunkid").ToString)
                        mintEmployeeunkid = CInt(.Item("employeeunkid").ToString)
                        mintTranheadunkid = CInt(.Item("tranheadunkid").ToString)
                        mintCostcenterunkid = CInt(.Item("costcenterunkid").ToString)
                        mdblPercentage = CDec(.Item("percentage").ToString)
                        mintPeriodunkid = CInt(.Item("periodunkid").ToString) 'Sohail (29 Mar 2017)
                        'Sohail (07 Feb 2019) -- Start
                        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                        mintAllocationbyId = CInt(.Item("allocationbyid").ToString)
                        'Sohail (07 Feb 2019) -- End
                        'Hemant (26 Dec 2018) -- Start
                        'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
                        'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
                        mblnIsvoid = False
                        mintVoiduserunkid = -1
                        mdtVoiddatetime = Nothing
                        mstrVoidreason = ""
                        'Hemant (26 Dec 2018) -- End
                        'Sohail (25 Jul 2020) -- Start
                        'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                        mblnIsInactive = False
                        'Sohail (25 Jul 2020) -- End

                        Select Case .Item("AUD")
                            Case "A"

                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If Insert() = False Then
                                If Insert(strDatabaseName, objDataOperation, dtCurrentDateAndTime) = False Then
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If

                            Case "U"

                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If Update() = False Then
                                If Update(strDatabaseName, objDataOperation, dtCurrentDateAndTime) = False Then
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If

                            Case "D"

                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If Delete(mintCostcentertranunkid, ConfigParameter._Object._CurrentDateAndTime) = False Then

                                'Hemant (26 Dec 2018) -- Start
                                'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
                                'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
                                'If Delete(strDatabaseName, mintCostcentertranunkid, dtCurrentDateAndTime) = False Then
                                If Delete(strDatabaseName, mintCostcentertranunkid, dtCurrentDateAndTime, objDataOperation) = False Then
                                    'Hemant (26 Dec 2018) -- End
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If
                        End Select
                    End If
                End With

                'Sohail (23 Mar 2020) -- Start
                'Internal Enhancement # : Showing progress counts on employee cost center screen.
                intCount = intCount + 1
                If bw IsNot Nothing Then
                    bw.ReportProgress(intCount)
                    If bw.CancellationPending = True Then
                        Exit For
                    End If
                End If
                clsemployee_costcenter_Tran._ProgressCurrCount = intCount
                'Sohail (23 Mar 2020) -- End

            Next

            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (26 Dec 2018) -- End

            Return True

        Catch ex As Exception
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            objDataOperation.ReleaseTransaction(False)
            'Hemant (26 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_EmpCostCenter; Module Name: " & mstrModuleName)
            exForce = Nothing
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'objDataOperation = Nothing
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (26 Dec 2018) -- End
        End Try
    End Function
    'Sohail (13 Sep 2011) -- End

    'Sohail (08 Nov 2011) -- Start
    Public Function GetUnkID(ByVal intEmployeeunkid As Integer, ByVal intTranheadunkid As Integer, ByVal dtPeriodEnd As Date, ByVal xDataOp As clsDataOperation, ByRef intRecorCount As Integer, ByRef intAllocationById As Integer) As Integer
        'Sohail (07 Feb 2019) - [intAllocationById]
        'Sohail (19 Dec 2018) - [xDataOp, intRecorCount]
        'Sohail (29 Mar 2017) - [intPeriodUnkID, dtPeriodEnd]
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim strIDs As String = ""
        Dim exForce As Exception

        'Sohail (19 Dec 2018) -- Start
        'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (19 Dec 2018) -- End

        Try

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'strQ = "SELECT  costcentertranunkid " & _
            ' " FROM premployee_costcenter_tran " & _
            ' " WHERE employeeunkid = @employeeunkid " & _
            ' " AND tranheadunkid = @tranheadunkid " & _
            ' "AND isvoid = 0 "
            If dtPeriodEnd = Nothing Then
                Throw New Exception(Language.getMessage(mstrModuleName, 3, "Period information is mandatory in employee cost center."))
                Return -1
                Exit Function
            End If

            strQ = "SELECT  A.costcentertranunkid " & _
                            ", A.allocationbyid " & _
                    "FROM    ( SELECT    costcentertranunkid  " & _
                                      ", premployee_costcenter_tran.allocationbyid " & _
                                      ", ISNULL(premployee_costcenter_tran.isinactive, 0) AS isinactive " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY premployee_costcenter_tran.employeeunkid, premployee_costcenter_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                              "FROM      premployee_costcenter_tran " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = premployee_costcenter_tran.periodunkid " & _
                              "WHERE     isvoid = 0 " & _
                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.modulerefid = 1 " & _
                                        "AND employeeunkid = @employeeunkid " & _
                                        "AND tranheadunkid = @tranheadunkid " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                            ") AS A " & _
                            "WHERE   A.ROWNO = 1 " & _
                            "AND A.isinactive = 0 "
            'Sohail (25 Jul 2020) - [isinactive]
            'Sohail (07 Feb 2019) - [allocationbyid]
            'Sohail (29 Mar 2017) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranheadunkid)
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd)) 'Sohail (16 May 2017)


            dtTable = objDataOperation.ExecQuery(strQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (19 Dec 2018) -- Start
            'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
            intRecorCount = dtTable.Rows.Count
            'Sohail (19 Dec 2018) -- End

            If dtTable.Rows.Count > 0 Then
                'Sohail (07 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                intAllocationById = CInt(dtTable.Rows(0).Item("allocationbyid"))
                'Sohail (07 Feb 2019) -- End
                Return dtTable.Rows(0).Item("costcentertranunkid")
            Else
                'Sohail (07 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                intAllocationById = enAllocation.COST_CENTER
                'Sohail (07 Feb 2019) -- End
                Return -1
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUnkID; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dtTable IsNot Nothing Then dtTable.Dispose()
            'Sohail (19 Dec 2018) -- Start
            'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
            'objDataOperation = Nothing
            If dtPeriodEnd = Nothing Then objDataOperation = Nothing
            'Sohail (19 Dec 2018) -- End
        End Try
    End Function

    'Sohail (08 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetCostCenerID_Percentage(ByVal intEmployeeunkid As Integer, ByVal intTranheadunkid As Integer, ByVal intPeriodUnkID As Integer) As DataTable
        'Sohail (29 Mar 2017) - [intPeriodUnkID]
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim strIDs As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  costcenterunkid " & _
                        ", percentage " & _
                    " FROM premployee_costcenter_tran " & _
                    " WHERE employeeunkid = @employeeunkid " & _
                        " AND tranheadunkid = @tranheadunkid " & _
                        " AND periodunkid = @periodunkid " & _
                        " AND isvoid = 0 "
            'Sohail (29 Mar 2017) - [periodunkid]

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranheadunkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID) 'Sohail (29 Mar 2017)

            dtTable = objDataOperation.ExecQuery(strQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCostCenerID_Percentage; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dtTable IsNot Nothing Then dtTable.Dispose()
            objDataOperation = Nothing
        End Try
        Return dtTable
    End Function
    'Sohail (08 Jun 2012) -- End

    'Sohail (08 Nov 2011) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This CostCenter is already defined for this employee. Please define new CostCenter.")
			Language.setMessage(mstrModuleName, 2, "WEB")
			Language.setMessage(mstrModuleName, 3, "Period information is mandatory in employee cost center.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Some of this account is already inactive.")

		Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class