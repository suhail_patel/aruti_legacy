﻿'************************************************************************************************************************************
'Class Name : clscostcenter_master.vb
'Purpose    :  All Payroll Group Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :01/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clscostcenter_master
    Private Shared ReadOnly mstrModuleName As String = "clscostcenter_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintCostcenterunkid As Integer
    Private mstrCostcentercode As String = String.Empty
    Private mstrCostcentername As String = String.Empty
    Private mintCostcentergroupmasterunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrCostcentername1 As String = String.Empty
    Private mstrCostcentername2 As String = String.Empty
    'S.SANDEEP [09-AUG-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#292}
    Private mstrCustomcode As String = String.Empty
    'S.SANDEEP [09-AUG-2018] -- END
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcenterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcentercode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costcentercode() As String
        Get
            Return mstrCostcentercode
        End Get
        Set(ByVal value As String)
            mstrCostcentercode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcentername
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costcentername() As String
        Get
            Return mstrCostcentername
        End Get
        Set(ByVal value As String)
            mstrCostcentername = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcentergroupmasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costcentergroupmasterunkid() As Integer
        Get
            Return mintCostcentergroupmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcentergroupmasterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcentername1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costcentername1() As String
        Get
            Return mstrCostcentername1
        End Get
        Set(ByVal value As String)
            mstrCostcentername1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcentername2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costcentername2() As String
        Get
            Return mstrCostcentername2
        End Get
        Set(ByVal value As String)
            mstrCostcentername2 = Value
        End Set
    End Property

    'S.SANDEEP [09-AUG-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#292}
    Public Property _Customcode() As String
        Get
            Return mstrCustomcode
        End Get
        Set(ByVal value As String)
            mstrCustomcode = value
        End Set
    End Property
    'S.SANDEEP [09-AUG-2018] -- END

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  costcenterunkid " & _
              ", costcentercode " & _
              ", costcentername " & _
              ", costcentergroupmasterunkid " & _
              ", description " & _
              ", isactive " & _
              ", costcentername1 " & _
              ", costcentername2 " & _
              ", ISNULL(customcode,'') AS customcode " & _
             "FROM prcostcenter_master " & _
             "WHERE costcenterunkid = @costcenterunkid "
            'S.SANDEEP [09-AUG-2018] -- START {Ref#292} [customcode] -- END

            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintCostcenterUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintcostcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                mstrcostcentercode = dtRow.Item("costcentercode").ToString
                mstrcostcentername = dtRow.Item("costcentername").ToString
                mintcostcentergroupmasterunkid = CInt(dtRow.Item("costcentergroupmasterunkid"))
                mstrdescription = dtRow.Item("description").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstrcostcentername1 = dtRow.Item("costcentername1").ToString
                mstrCostcentername2 = dtRow.Item("costcentername2").ToString
                'S.SANDEEP [09-AUG-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#292}
                mstrCustomcode = dtRow("customcode").ToString()
                'S.SANDEEP [09-AUG-2018] -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal strFilter As String = "") As DataSet
        'Sohail (03 Jan 2020) - [strFilter]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (19 Nov 2010) -- Start
            'Issue : now get prpayrollgroup_master table from 'hrmsConfiguration' database.
            'strQ = "SELECT " & _
            '  "  costcenterunkid " & _
            '  ", costcentercode " & _
            '  ", costcentername " & _
            '  ", costcentergroupmasterunkid " & _
            '  ", prpayrollgroup_master.groupname as costcentergroup " & _
            '  ", prcostcenter_master.description " & _
            '  ", prcostcenter_master.isactive " & _
            '  ", costcentername1 " & _
            '  ", costcentername2 " & _
            ' "FROM prcostcenter_master " & _
            ' " LEFT JOIN prpayrollgroup_master on prpayrollgroup_master.groupmasterunkid = prcostcenter_master.costcentergroupmasterunkid "
            strQ = "SELECT  prcostcenter_master.costcenterunkid " & _
                          ", prcostcenter_master.costcentercode " & _
                          ", prcostcenter_master.costcentername " & _
                          ", prcostcenter_master.costcentergroupmasterunkid " & _
                          ", hrmsConfiguration..cfpayrollgroup_master.groupname AS costcentergroup " & _
              ", prcostcenter_master.description " & _
              ", prcostcenter_master.isactive " & _
                          ", prcostcenter_master.costcentername1 " & _
                          ", prcostcenter_master.costcentername2 " & _
                   ", ISNULL(prcostcenter_master.customcode,'') AS customcode " & _
             "FROM prcostcenter_master " & _
                            "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid = prcostcenter_master.costcentergroupmasterunkid "
            'Sohail (19 Nov 2010) -- End
            'S.SANDEEP [09-AUG-2018] -- START {Ref#292} [customcode] -- END

            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            strQ &= " WHERE 1 = 1 "
            'Sohail (03 Jan 2020) -- End

            If blnOnlyActive Then
                'Sohail (03 Jan 2020) -- Start
                'NMB Enhancement # : Search functionality on cost center list screen.
                'strQ &= " WHERE prcostcenter_master.isactive = 1 "
                strQ &= " AND prcostcenter_master.isactive = 1 "
                'Sohail (03 Jan 2020) -- End
            End If

            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If
            'Sohail (03 Jan 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prcostcenter_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCostcentercode, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This CostCenter Code is already defined. Please define new CostCenter Code.")
            Return False
        ElseIf isExist("", mstrCostcentername) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This CostCenter Name is already defined. Please define new CostCenter Name.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@costcentercode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcostcentercode.ToString)
            objDataOperation.AddParameter("@costcentername", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcostcentername.ToString)
            objDataOperation.AddParameter("@costcentergroupmasterunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcostcentergroupmasterunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@costcentername1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcostcentername1.ToString)
            objDataOperation.AddParameter("@costcentername2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCostcentername2.ToString)
            'S.SANDEEP [09-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#292}
            objDataOperation.AddParameter("@customcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCustomcode.ToString)
            'S.SANDEEP [09-AUG-2018] -- END

            strQ = "INSERT INTO prcostcenter_master ( " & _
              "  costcentercode " & _
              ", costcentername " & _
              ", costcentergroupmasterunkid " & _
              ", description " & _
              ", isactive " & _
              ", costcentername1 " & _
              ", costcentername2" & _
              ", customcode " & _
            ") VALUES (" & _
              "  @costcentercode " & _
              ", @costcentername " & _
              ", @costcentergroupmasterunkid " & _
              ", @description " & _
              ", @isactive " & _
              ", @costcentername1 " & _
              ", @costcentername2" & _
              ", @customcode " & _
            "); SELECT @@identity"
            'S.SANDEEP [09-AUG-2018] -- START {Ref#292} [customcode] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCostcenterUnkId = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (12 Oct 2011) -- Start
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "prcostcenter_master", "costcenterunkid", mintCostcenterunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If
            'Sohail (12 Oct 2011) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prcostcenter_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCostcentercode, "", mintCostcenterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This CostCenter Code is already defined. Please define new CostCenter Code.")
            Return False
        ElseIf isExist("", mstrCostcentername, mintCostcenterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This CostCenter Name is already defined. Please define new CostCenter Name.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@costcentercode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCostcentercode.ToString)
            objDataOperation.AddParameter("@costcentername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCostcentername.ToString)
            objDataOperation.AddParameter("@costcentergroupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcentergroupmasterunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@costcentername1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCostcentername1.ToString)
            objDataOperation.AddParameter("@costcentername2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCostcentername2.ToString)
            'S.SANDEEP [09-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#292}
            objDataOperation.AddParameter("@customcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCustomcode.ToString)
            'S.SANDEEP [09-AUG-2018] -- END

            strQ = "UPDATE prcostcenter_master SET " & _
              "  costcentercode = @costcentercode" & _
              ", costcentername = @costcentername" & _
              ", costcentergroupmasterunkid = @costcentergroupmasterunkid" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", costcentername1 = @costcentername1" & _
              ", costcentername2 = @costcentername2 " & _
              ", customcode = @customcode " & _
            "WHERE costcenterunkid = @costcenterunkid "
            'S.SANDEEP [09-AUG-2018] -- START {Ref#292} [customcode] -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            Dim blnToInsert As Boolean
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "prcostcenter_master", mintCostcenterunkid, "costcenterunkid", 2) Then
                blnToInsert = True
            End If

            If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 2, "prcostcenter_master", "costcenterunkid", mintCostcenterunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If
            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prcostcenter_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected CostCenter. Reason: This CostCenter is in use.")
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'Sohail (28 Dec 2010) -- Start
            'strQ = "DELETE FROM prcostcenter_master " & _
            '"WHERE costcenterunkid = @costcenterunkid "
            strQ = "UPDATE prcostcenter_master SET " & _
                  " isactive = 0" & _
            "WHERE costcenterunkid = @costcenterunkid "
            'Sohail (28 Dec 2010) -- End

            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "prcostcenter_master", "costcenterunkid", intUnkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If
            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, ByVal xPeriodEnd As Date) As Boolean
        'Sohail (17 Mar 2016) - [xPeriodEnd]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (19 Nov 2010) -- Start
            'strQ = "<Query>"
            'Sohail (18 Mar 2016) -- Start
            'Enhancement - Allocation changes in 58.1.
            'strQ = "SELECT  costcenterunkid " & _
            '       "FROM    premployee_costcenter_tran " & _
            '       "WHERE   ISNULL(isvoid, 0) = 0 " & _
            '               "AND costcenterunkid = @costcenterunkid " & _
            '       "UNION " & _
            '       "SELECT   costcenterunkid " & _
            '        "FROM    prpayrollprocess_tran " & _
            '        "WHERE   ISNULL(isvoid, 0) = 0 " & _
            '                "AND costcenterunkid = @costcenterunkid " & _
            '       "UNION " & _
            '       "SELECT   costcenterunkid " & _
            '        "FROM    hremployee_master " & _
            '        "WHERE   isactive = 1 " & _
            '                "AND costcenterunkid = @costcenterunkid " 'Sohail (28 Dec 2010)
            strQ = "SELECT  costcenterunkid " & _
                   "FROM    premployee_costcenter_tran " & _
                   "WHERE   ISNULL(isvoid, 0) = 0 " & _
                           "AND costcenterunkid = @costcenterunkid " & _
                   "UNION " & _
                   "SELECT   costcenterunkid " & _
                    "FROM    prpayrollprocess_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND costcenterunkid = @costcenterunkid " & _
                   "UNION " & _
                   "SELECT   C.costcenterunkid " & _
                    "FROM    hremployee_master " & _
                    "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         cctranheadvalueid AS costcenterunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_cctranhead_tran " & _
                        "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "WHERE   isactive = 1 " & _
                            "AND C.costcenterunkid = @costcenterunkid "
            'Sohail (18 Mar 2016) -- End
            'Sohail (19 Nov 2010) -- End
            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            strQ &= "UNION " & _
                   "SELECT   default_costcenterunkid " & _
                    "FROM    prtranhead_master " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND default_costcenterunkid = @costcenterunkid " & _
                    "UNION " & _
                   "SELECT   default_costcenterunkid " & _
                    "FROM    cmexpense_master " & _
                    "WHERE   ISNULL(isactive, 0) = 1 " & _
                            "AND default_costcenterunkid = @costcenterunkid "
            'Hemant (06 Jul 2020) -- End


            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  costcenterunkid " & _
              ", costcentercode " & _
              ", costcentername " & _
              ", costcentergroupmasterunkid " & _
              ", description " & _
              ", isactive " & _
              ", costcentername1 " & _
              ", costcentername2 " & _
              ", customcode " & _
             "FROM prcostcenter_master " & _
             "WHERE 1=1"
            'S.SANDEEP [09-AUG-2018] -- START {Ref#292} [customcode] -- END

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= " AND costcentercode = @costcentercode "
            End If

            If strName.Length > 0 Then
                strQ &= " AND costcentername = @costcentername "
            End If

            If intUnkid > 0 Then
                strQ &= " AND costcenterunkid <> @costcenterunkid"
            End If

            objDataOperation.AddParameter("@costcentercode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@costcentername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify by : Suhail
    ''' </summary>
    ''' <param name="strList"></param>
    ''' <param name="mblnFlag"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComboList(Optional ByVal strList As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal strDatabaseName As String = "", Optional ByVal strFilter As String = "") As DataSet
        'Sohail (03 Jan 2020) - [strFilter]
        'Sohail (14 Mar 2019) - [strDatabaseName]
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        'Sohail (14 Mar 2019) -- Start
        'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
        Dim strDB As String = ""
        'Sohail (14 Mar 2019) -- End

        Try
            objDataOperation = New clsDataOperation

            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            If strDatabaseName.Trim <> "" Then
                strDB = strDatabaseName & ".."
            End If
            'Sohail (14 Mar 2019) -- End

            If mblnFlag = True Then
                strQ = "SELECT 0 AS costcenterunkid , ' ' AS costcentercode, ' ' + @Select AS costcentername UNION "
            End If

            strQ &= " SELECT costcenterunkid, costcentercode, costcentername FROM " & strDB & "prcostcenter_master WHERE isactive =1"
            'Sohail (14 Mar 2019) - [strDB]

            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If
            'Sohail (03 Jan 2020) -- End

            strQ &= " ORDER BY costcentername " 'Sohail (10 Feb 2022)

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComborList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    'S.SANDEEP [ 22 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Shared Function GetCSV_CC(ByVal StrGrpId As String) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim StrString As String = String.Empty
        Try
            Dim objDataOperation As New clsDataOperation

            'Sohail (04 Jan 2014) -- Start
            'Issue : Conversion error from DBNULL to String when there is no cost center for given cost center group id.
            'StrQ = "SELECT STUFF((SELECT ',' + CAST(s.costcenterunkid AS NVARCHAR(50)) FROM prcostcenter_master s WHERE s.costcentergroupmasterunkid IN (" & StrGrpId & ")ORDER BY s.costcenterunkid FOR XML PATH('')),1,1,'') AS CSV"
            StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(s.costcenterunkid AS NVARCHAR(50)) FROM prcostcenter_master s WHERE s.costcentergroupmasterunkid IN (" & StrGrpId & ") ORDER BY s.costcenterunkid FOR XML PATH('')),1,1,''), '') AS CSV"
            'Sohail (04 Jan 2014) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                StrString = dsList.Tables(0).Rows(0).Item("CSV")
            End If
            Return StrString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCC_StringIds", mstrModuleName)
            Return ""
        End Try
    End Function
    'S.SANDEEP [ 22 MAR 2013 ] -- END

    'Pinkal (10-Mar-2011) -- Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetCostCenterUnkId(ByVal mstrName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " costcenterunkid " & _
                      "  FROM prcostcenter_master " & _
                      " WHERE costcentername = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("costcenterunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCostCenterUnkId", mstrModuleName)
        End Try
        Return -1
    End Function

    'Pinkal (10-Mar-2011) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This CostCenter Code is already defined. Please define new CostCenter Code.")
			Language.setMessage(mstrModuleName, 2, "This CostCenter Name is already defined. Please define new CostCenter Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class