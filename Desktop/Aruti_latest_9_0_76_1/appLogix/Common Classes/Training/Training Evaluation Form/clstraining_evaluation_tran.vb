﻿'************************************************************************************************************************************
'Class Name : clstraining_evaluation_tran.vb
'Purpose    :
'Date       : 29-July-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System.Threading
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clstraining_evaluation_tran
    Private Shared ReadOnly mstrModuleName As String = "clstraining_evaluation_tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintTrainingEvaluationTranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintTrainingRequestunkid As Integer
    Private mintCourseMasterunkId As Integer
    Private mintCategoryId As Integer
    Private mintQuestionnaireId As Integer
    Private mintSubQuestionId As Integer
    Private mintFeedbackModeId As Integer
    Private mintAnswerTypeId As Integer
    Private mstrAnswer As String = String.Empty
    Private mblnIsAskForJustification As Boolean
    Private mstrJustificationAnswer As String = String.Empty
    Private mintUserunkid As Integer
    Private mintLoginEmployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidLoginEmployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mdtTran As DataTable
    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Private mblnIsForLineManagerFeedback As Boolean
    Dim objEmailList As New List(Of clsEmailCollection)
    Private objThread As Thread
    'Hemant (20 Aug 2021) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DataTable
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingevaluationtranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingEvaluationTranunkid() As Integer
        Get
            Return mintTrainingEvaluationTranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingEvaluationTranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingRequestunkid() As Integer
        Get
            Return mintTrainingRequestunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingRequestunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coursemasterunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CourseMasterunkId() As Integer
        Get
            Return mintCourseMasterunkId
        End Get
        Set(ByVal value As Integer)
            mintCourseMasterunkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CategoryId() As Integer
        Get
            Return mintCategoryId
        End Get
        Set(ByVal value As Integer)
            mintCategoryId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set questionnaireid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _QuestionnaireId() As Integer
        Get
            Return mintQuestionnaireId
        End Get
        Set(ByVal value As Integer)
            mintQuestionnaireId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set subquestionid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _SubQuestionId() As Integer
        Get
            Return mintSubQuestionId
        End Get
        Set(ByVal value As Integer)
            mintSubQuestionId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set feedbackmodeid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FeedbackModeId() As Integer
        Get
            Return mintFeedbackModeId
        End Get
        Set(ByVal value As Integer)
            mintFeedbackModeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set answertypeid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _AnswerTypeId() As Integer
        Get
            Return mintAnswerTypeId
        End Get
        Set(ByVal value As Integer)
            mintAnswerTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set answer
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Answer() As String
        Get
            Return mstrAnswer
        End Get
        Set(ByVal value As String)
            mstrAnswer = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isaskforjustification
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsAskForJustification() As Boolean
        Get
            Return mblnIsAskForJustification
        End Get
        Set(ByVal value As Boolean)
            mblnIsAskForJustification = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set justification_answer
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _JustificationAnswer() As String
        Get
            Return mstrJustificationAnswer
        End Get
        Set(ByVal value As String)
            mstrJustificationAnswer = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Public Property _IsForLineManagerFeedback() As Boolean
        Get
            Return mblnIsForLineManagerFeedback
        End Get
        Set(ByVal value As Boolean)
            mblnIsForLineManagerFeedback = value
        End Set
    End Property
    'Hemant (20 Aug 2021) -- End

#End Region

#Region " Constuctor "
    Public Sub New()
        mdtTran = New DataTable()
        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("categoryid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("questionnaireunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("subquestionid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("feedbackmodeid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("answertypeid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("answer")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isaskforjustification")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("justification_answer")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            dCol = New DataColumn("isforlinemanager")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)
            'Hemant (20 Aug 2021) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "  trainingevaluationtranunkid " & _
                       ", employeeunkid " & _
                       ", trainingrequestunkid " & _
                       ", coursemasterunkid " & _
                       ", categoryid " & _
                       ", questionnaireid " & _
                       ", subquestionid " & _
                       ", feedbackmodeid " & _
                       ", answertypeid " & _
                       ", answer " & _
                       ", isaskforjustification " & _
                       ", justification_answer " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", ISNULL(isforlinemanager,0) AS isforlinemanager " & _
             "FROM trtraining_evaluation_tran " & _
             "WHERE trainingevaluationtranunkid = @trainingevaluationtranunkid "

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            objDataOperation.AddParameter("@trainingevaluationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingEvaluationTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTrainingEvaluationTranunkid = CInt(dtRow.Item("trainingevaluationtranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintTrainingRequestunkid = CInt(dtRow.Item("trainingrequestunkid"))
                mintCourseMasterunkId = CInt(dtRow.Item("coursemasterunkid"))
                mintCategoryId = CInt(dtRow.Item("categoryid"))
                mintQuestionnaireId = CInt(dtRow.Item("questionnaireid"))
                mintSubQuestionId = CInt(dtRow.Item("subquestionid"))
                mintFeedbackModeId = CInt(dtRow.Item("feedbackmodeid"))
                mintAnswerTypeId = CInt(dtRow.Item("answertypeid"))
                mstrAnswer = dtRow.Item("answer").ToString
                mblnIsAskForJustification = CBool(dtRow.Item("isaskforjustification"))
                mstrJustificationAnswer = dtRow.Item("justification_answer")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginEmployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidLoginEmployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                mblnIsForLineManagerFeedback = CBool(dtRow.Item("isforlinemanager"))
                'Hemant (20 Aug 2021) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>    ''' 
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal intEmployeeunkid As Integer = -1, _
                            Optional ByVal intTrainingRequestunkid As Integer = -1, _
                            Optional ByVal intFeedBackModeId As Integer = -1, _
                            Optional ByVal blnExcludeLineManagerFeedbackQuestions As Boolean = False, _
                            Optional ByVal blnForLineManagerFeedback As Boolean = False _
                            ) As DataSet
        'Hemant (20 Aug 2021) -- [blnExcludeLineManagerFeedbackQuestions,blnForLineManagerFeedback]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "  trainingevaluationtranunkid " & _
                       ", employeeunkid " & _
                       ", trainingrequestunkid " & _
                       ", coursemasterunkid " & _
                       ", categoryid " & _
                       ", questionnaireid " & _
                       ", subquestionid " & _
                       ", feedbackmodeid " & _
                       ", answertypeid " & _
                       ", answer " & _
                       ", isaskforjustification " & _
                       ", justification_answer " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", ISNULL(isforlinemanager,0) AS isforlinemanager " & _
             " FROM trtraining_evaluation_tran " & _
             " WHERE trtraining_evaluation_tran.isvoid = 0 "

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            objDataOperation.ClearParameters()

            If intEmployeeunkid > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            End If

            If intTrainingRequestunkid > 0 Then
                strQ &= " AND trainingrequestunkid = @trainingrequestunkid "
                objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingRequestunkid)
            End If

            If intFeedBackModeId > 0 Then
                strQ &= " AND feedbackmodeid = @feedbackmodeid "
                objDataOperation.AddParameter("@feedbackmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFeedBackModeId)
            End If

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            If blnExcludeLineManagerFeedbackQuestions = True Then
                strQ &= " and isforlinemanager <> 1 "
            End If

            If blnForLineManagerFeedback = True Then
                strQ &= " and isforlinemanager = 1 "
            End If
            'Hemant (20 Aug 2021) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function SaveEvaluationTran(Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                       , Optional ByVal clsTrainingRequestMaster As clstraining_request_master = Nothing) As Boolean
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try

            For Each dtRow As DataRow In mdtTran.Rows

                mintCategoryId = CInt(dtRow.Item("categoryid"))
                mintQuestionnaireId = CInt(dtRow.Item("questionnaireunkid"))
                mintSubQuestionId = CInt(dtRow.Item("subquestionid"))
                mintFeedbackModeId = CInt(dtRow.Item("feedbackmodeid"))
                mintAnswerTypeId = CInt(dtRow.Item("answertypeid"))
                mstrAnswer = CStr(dtRow.Item("answer"))
                mblnIsAskForJustification = CBool(dtRow.Item("isaskforjustification"))
                mstrJustificationAnswer = CStr(dtRow.Item("justification_answer"))
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                mblnIsForLineManagerFeedback = CBool(dtRow.Item("isforlinemanager"))
                'Hemant (20 Aug 2021) -- End


                mintTrainingEvaluationTranunkid = isExist(mintEmployeeunkid, mintTrainingRequestunkid, mintFeedbackModeId, mintQuestionnaireId, mintSubQuestionId, -1, objDataOperation)
                If mintTrainingEvaluationTranunkid > 0 Then
                    If Update(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Next

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            If clsTrainingRequestMaster IsNot Nothing AndAlso clsTrainingRequestMaster._TrainingRequestunkid > 0 Then
                If clsTrainingRequestMaster.Update(objDataOperation) = False Then
                    Return False
                End If
            End If
            'Hemant (20 Aug 2021) -- End

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtraining_evaluation_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mintEmployeeunkid, mintTrainingRequestunkid, mintFeedbackModeId, mintQuestionnaireId, mintSubQuestionId, -1, xDataOpr) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Training Evaluation is already defined. Please define new Training Evaluation.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkId.ToString)
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryId.ToString)
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireId.ToString)
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubQuestionId.ToString)
            objDataOperation.AddParameter("@feedbackmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackModeId.ToString)
            objDataOperation.AddParameter("@answertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswerTypeId.ToString)
            objDataOperation.AddParameter("@answer", SqlDbType.NVarChar, mstrAnswer.Trim.Length, mstrAnswer.ToString)
            objDataOperation.AddParameter("@isaskforjustification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAskForJustification.ToString)
            objDataOperation.AddParameter("@justification_answer", SqlDbType.NVarChar, mstrJustificationAnswer.Trim.Length, mstrJustificationAnswer.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isforlinemanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForLineManagerFeedback.ToString)
            'Hemant (20 Aug 2021) -- End

            strQ = "INSERT INTO trtraining_evaluation_tran ( " & _
                       "  employeeunkid " & _
                       ", trainingrequestunkid " & _
                       ", coursemasterunkid " & _
                       ", categoryid " & _
                       ", questionnaireid " & _
                       ", subquestionid " & _
                       ", feedbackmodeid " & _
                       ", answertypeid " & _
                       ", answer " & _
                       ", isaskforjustification " & _
                       ", justification_answer " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", isforlinemanager " & _
                    ") VALUES (" & _
                       "  @employeeunkid " & _
                       ", @trainingrequestunkid " & _
                       ", @coursemasterunkid " & _
                       ", @categoryid " & _
                       ", @questionnaireid " & _
                       ", @subquestionid " & _
                       ", @feedbackmodeid " & _
                       ", @answertypeid " & _
                       ", @answer " & _
                       ", @isaskforjustification " & _
                       ", @justification_answer " & _
                       ", @userunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidloginemployeeunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @isforlinemanager " & _
                    "); SELECT @@identity"

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingEvaluationTranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtraining_evaluation_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mintEmployeeunkid, mintTrainingRequestunkid, mintFeedbackModeId, mintQuestionnaireId, mintSubQuestionId, mintTrainingEvaluationTranunkid, xDataOpr) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Training Evaluation is already defined. Please define new Training Evaluation.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingevaluationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingEvaluationTranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkId.ToString)
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryId.ToString)
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireId.ToString)
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubQuestionId.ToString)
            objDataOperation.AddParameter("@feedbackmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackModeId.ToString)
            objDataOperation.AddParameter("@answertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswerTypeId.ToString)
            objDataOperation.AddParameter("@answer", SqlDbType.NVarChar, mstrAnswer.Trim.Length, mstrAnswer.ToString)
            objDataOperation.AddParameter("@isaskforjustification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAskForJustification.ToString)
            objDataOperation.AddParameter("@justification_answer", SqlDbType.NVarChar, mstrJustificationAnswer.Trim.Length, mstrJustificationAnswer.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isforlinemanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForLineManagerFeedback.ToString)
            'Hemant (20 Aug 2021) -- End

            strQ = "UPDATE trtraining_evaluation_tran SET " & _
                        "  employeeunkid = @employeeunkid " & _
                        ", trainingrequestunkid = @trainingrequestunkid " & _
                        ", coursemasterunkid = @coursemasterunkid " & _
                        ", categoryid = @categoryid " & _
                        ", questionnaireid = @questionnaireid " & _
                        ", subquestionid = @subquestionid " & _
                        ", feedbackmodeid = @feedbackmodeid " & _
                        ", answertypeid = @answertypeid " & _
                        ", answer = @answer " & _
                        ", isaskforjustification = @isaskforjustification " & _
                        ", justification_answer = @justification_answer " & _
                        ", userunkid = @userunkid " & _
                        ", loginemployeeunkid = @loginemployeeunkid " & _
                        ", isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                        ", isforlinemanager = @isforlinemanager " & _
                    "WHERE trainingevaluationtranunkid = @trainingevaluationtranunkid "

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintTrainingEvaluationTranunkid, objDataOperation) = False Then
                If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtraining_evaluation_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE trtraining_evaluation_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                   "WHERE trainingevaluationtranunkid = @trainingevaluationtranunkid "

            objDataOperation.AddParameter("@trainingevaluationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            _TrainingEvaluationTranunkid = intUnkid

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@trainingevaluationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iEmployeeId As Integer, _
                            ByVal intTrainingRequestUnkId As Integer, _
                            ByVal intFeedBackModeId As Integer, _
                            Optional ByVal intQuestionnaireId As Integer = -1, _
                            Optional ByVal intSubQuestionId As Integer = -1, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intRetUnkId As Integer = 0
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                       "  trainingevaluationtranunkid " & _
                       ", employeeunkid " & _
                       ", trainingrequestunkid " & _
                       ", coursemasterunkid " & _
                       ", categoryid " & _
                       ", questionnaireid " & _
                       ", subquestionid " & _
                       ", feedbackmodeid " & _
                       ", answertypeid " & _
                       ", answer " & _
                       ", isaskforjustification " & _
                       ", justification_answer " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", ISNULL(isforlinemanager,0) AS isforlinemanager " & _
                    "FROM trtraining_evaluation_tran " & _
                    "WHERE isvoid = 0 " & _
                    " AND trainingrequestunkid = @trainingrequestunkid " & _
                    " AND employeeunkid  = @employeeunkid " & _
                    " AND feedbackmodeid  = @feedbackmodeid "

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            If intQuestionnaireId > 0 Then
                strQ &= " AND questionnaireid = @questionnaireid "
                objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionnaireId)
            End If

            If intSubQuestionId > 0 Then
                strQ &= " AND subquestionid = @subquestionid "
                objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSubQuestionId)
            Else
                strQ &= " AND subquestionid <= 0 "
            End If

            If intUnkid > 0 Then
                strQ &= " AND trainingevaluationtranunkid <> @trainingevaluationtranunkid"
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingRequestUnkId)
            objDataOperation.AddParameter("@feedbackmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFeedBackModeId)
            objDataOperation.AddParameter("@trainingevaluationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRetUnkId = CInt(dsList.Tables(0).Rows(0).Item("trainingevaluationtranunkid"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRetUnkId
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO attrtraining_evaluation_tran ( " & _
                       "  tranguid " & _
                       ", trainingevaluationtranunkid " & _
                       ", employeeunkid " & _
                       ", trainingrequestunkid " & _
                       ", coursemasterunkid " & _
                       ", categoryid " & _
                       ", questionnaireid " & _
                       ", subquestionid " & _
                       ", feedbackmodeid " & _
                       ", answertypeid " & _
                       ", answer " & _
                       ", isaskforjustification " & _
                       ", justification_answer " & _
                       ", audituserunkid " & _
                       ", loginemployeeunkid " & _
                       ", audittype " & _
                       ", auditdatetime " & _
                       ", isweb " & _
                       ", ip " & _
                       ", hostname " & _
                       ", form_name " & _
                       ", isforlinemanager " & _
                  ") VALUES (" & _
                       "  LOWER(NEWID()) " & _
                       ", @trainingevaluationtranunkid " & _
                       ", @employeeunkid " & _
                       ", @trainingrequestunkid " & _
                       ", @coursemasterunkid " & _
                       ", @categoryid " & _
                       ", @questionnaireid " & _
                       ", @subquestionid " & _
                       ", @feedbackmodeid " & _
                       ", @answertypeid " & _
                       ", @answer " & _
                       ", @isaskforjustification " & _
                       ", @justification_answer " & _
                       ", @audituserunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @audittype " & _
                       ", GETDATE() " & _
                       ", @isweb " & _
                       ", @ip " & _
                       ", @hostname " & _
                       ", @form_name " & _
                       ", @isforlinemanager " & _
                  ")"

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingevaluationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingEvaluationTranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkId.ToString)
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryId.ToString)
            objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireId.ToString)
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubQuestionId.ToString)
            objDataOperation.AddParameter("@feedbackmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackModeId.ToString)
            objDataOperation.AddParameter("@answertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswerTypeId.ToString)
            objDataOperation.AddParameter("@answer", SqlDbType.NVarChar, mstrAnswer.Trim.Length, mstrAnswer.ToString)
            objDataOperation.AddParameter("@isaskforjustification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAskForJustification.ToString)
            objDataOperation.AddParameter("@justification_answer", SqlDbType.NVarChar, mstrJustificationAnswer.Trim.Length, mstrJustificationAnswer.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isforlinemanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForLineManagerFeedback.ToString)
            'Hemant (20 Aug 2021) -- End

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strQ = "SELECT TOP 1 * FROM attrtraining_evaluation_tran WHERE trainingevaluationtranunkid = @trainingevaluationtranunkid AND audittype <> 3 ORDER BY auditdatetime DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingevaluationtranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("trainingevaluationtranunkid").ToString() = mintTrainingEvaluationTranunkid AndAlso dr("employeeunkid").ToString() = mintEmployeeunkid _
                    AndAlso dr("trainingrequestunkid").ToString() = mintTrainingRequestunkid AndAlso dr("coursemasterunkid").ToString() = mintCourseMasterunkId _
                    AndAlso dr("categoryid").ToString() = mintCategoryId AndAlso dr("questionnaireid").ToString() = mintQuestionnaireId _
                    AndAlso dr("subquestionid").ToString() = mintSubQuestionId AndAlso dr("feedbackmodeid").ToString() = mintFeedbackModeId _
                    AndAlso dr("answertypeid").ToString() = mintAnswerTypeId AndAlso dr("answer").ToString() = mstrAnswer _
                    AndAlso CBool(dr("isAskforJustification")) = mblnIsAskForJustification AndAlso dr("justification_answer").ToString() = mstrJustificationAnswer _
                    AndAlso CBool(dr("isforlinemanager")) = mblnIsForLineManagerFeedback Then
                    'Hemant (20 Aug 2021) -- [isforlinemanager]

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Send ReportingTo Notification </purpose>
    ''' 
    Public Sub Send_Notification_ReportingTo(ByVal iEmployeeId As Integer, _
                                             ByVal dtEmployeeAsOnDate As Date, _
                                             ByVal intCompanyUnkId As Integer, _
                                             ByVal intCourseMasterunkid As Integer, _
                                             ByVal intTrainingRequestId As Integer, _
                                             ByVal strArutiSelfServiceURL As String, _
                                             Optional ByVal iLoginTypeId As Integer = 0, _
                                             Optional ByVal iLoginEmployeeId As Integer = 0, _
                                             Optional ByVal iUserId As Integer = 0, _
                                             Optional ByVal blnIsSendMail As Boolean = True)
        Dim objEmp As New clsEmployee_Master
        Dim objReportTo As New clsReportingToEmployee
        Dim objCommon As New clsCommon_Master
        Dim strLink As String = String.Empty
        Dim strToEmail As String = ""
        Dim strReportingToName As String = ""
        Dim intReportingToEmpId As Integer = -1
        Dim mintReportingToUserID As Integer = -1
        Dim strMessage As String = ""
        Dim strTrainingName As String = ""
        Try

            Dim objMail As New clsSendMail
            Dim strSubject As String = ""

            objEmp._Employeeunkid(dtEmployeeAsOnDate) = iEmployeeId

            objReportTo._EmployeeUnkid(dtEmployeeAsOnDate) = iEmployeeId

            Dim dt As DataTable = objReportTo._RDataTable
            Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
            If DefaultReportList.Count > 0 Then
                Dim objUserAddEdit As New clsUserAddEdit
                intReportingToEmpId = DefaultReportList(0).Item("reporttoemployeeunkid").ToString
                strToEmail = DefaultReportList(0).Item("ReportingToEmail").ToString
                strReportingToName = DefaultReportList(0).Item("ename").ToString

                mintReportingToUserID = objUserAddEdit.Return_UserId(intReportingToEmpId, intCompanyUnkId)
                objUserAddEdit = Nothing
            End If

            objCommon._Masterunkid = intCourseMasterunkid
            strTrainingName = objCommon._Name

            If strToEmail.Trim.Length <= 0 Then Exit Sub

            strSubject = Language.getMessage(mstrModuleName, 2, "Line Manager Training Feedback Notification")

            strMessage = "<HTML> <BODY>"
            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            strMessage &= Language.getMessage(mstrModuleName, 1, "Dear") & " " & info1.ToTitleCase(strReportingToName) & ", <BR><BR>"

            strMessage &= Language.getMessage(mstrModuleName, 3, "Please note that #FirstName# #Surname# has submitted his impact evaluation form for #TrainingName#.")

            strLink = strArutiSelfServiceURL & "/Training/Training_Evalution/wPg_Training_Evalution_Form.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                                        intTrainingRequestId & "|" & _
                                                                                                                                        intCompanyUnkId & "|" & _
                                                                                                                                        iEmployeeId & "|" & _
                                                                                                                                        CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & "|" & _
                                                                                                                                        mintReportingToUserID & "|" & _
                                                                                                                                        CStr(True)))

            strMessage &= "<BR></BR><BR></BR>" & _
                          Language.getMessage(mstrModuleName, 4, "Please click on the following link to provide line manager comments.") & _
                         "<BR></BR><a href='" & strLink & "'>" _
                          & strLink & "</a>"

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"

            strMessage &= "</BODY></HTML>"

            Dim strEmailContent As String
            strEmailContent = strMessage.ToString()
            strEmailContent = strEmailContent.Replace("#FirstName#", "<B>" & objEmp._Firstname & "</B>")
            strEmailContent = strEmailContent.Replace("#Surname#", "<B>" & objEmp._Surname & "</B>")
            strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & strTrainingName & "</B>")

            objMail._Subject = strSubject
            objMail._Message = strEmailContent
            objMail._ToEmail = strToEmail
            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrFormName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            objMail._SenderAddress = objEmp._Email
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT

            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                       mstrClientIP, mstrHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                       IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

            objEmailList.Add(objEmailColl)

            objUser = Nothing

            If blnIsSendMail = True AndAlso objEmailList.Count > 0 Then
                If HttpContext.Current Is Nothing Then
                    objThread = New Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = intCompanyUnkId
                    objThread.Start(arr)
                Else
                    Call Send_Notification(intCompanyUnkId)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_ReportingTo", mstrModuleName)
        Finally
            objEmp = Nothing
            objReportTo = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Private Sub Send_Notification(ByVal intCompanyUnkid As Object)
        Try
            If objEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In objEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyUnkid Is Integer Then
                        intCUnkId = intCompanyUnkid
                    Else
                        intCUnkId = CInt(intCompanyUnkid(0))
                    End If
                    If objSendMail.SendMail(intCUnkId).ToString.Length > 0 Then
                        Continue For
                    End If
                Next
                objEmailList.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'Hemant (20 Aug 2021) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Dear")
			Language.setMessage(mstrModuleName, 2, "Line Manager Training Feedback Notification")
			Language.setMessage(mstrModuleName, 3, "Please note that #FirstName# #Surname# has submitted his impact evaluation form for #TrainingName#.")
			Language.setMessage(mstrModuleName, 4, "Please click on the following link to provide line manager comments.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
