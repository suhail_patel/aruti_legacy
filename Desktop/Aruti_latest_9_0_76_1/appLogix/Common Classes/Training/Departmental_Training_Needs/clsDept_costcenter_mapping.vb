﻿'************************************************************************************************************************************
'Class Name : clsDept_costcenter_mapping.vb
'Purpose    :
'Date       :16-02-2022
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsDept_costcenter_mapping

    Private Const mstrModuleName = "clsDept_costcenter_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintUnkid As Integer
    Private mintAllocationid As Integer
    Private mintAllocationtranunkid As Integer
    Private mintCostcenterunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIp As String = ""
    Private mstrHostName As String = ""
    Private mblnIsweb As Boolean
#End Region

#Region " public variables "
    Public pintUnkid As Integer
    Public pintAllocationid As Integer
    Public pintAllocationtranunkid As Integer
    Public pintCostcenterunkid As Integer
    Public pintUserunkid As Integer
    Public pblnIsvoid As Boolean
    Public pintVoiduserunkid As Integer
    Public pdtVoiddatetime As Date
    Public pstrVoidreason As String = String.Empty
    Public pstrFormName As String = ""
    Public pstrClientIp As String = ""
    Public pstrHostName As String = ""
    Public pblnIsweb As Boolean
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Unkid() As Integer
        Get
            Return mintUnkid
        End Get
        Set(ByVal value As Integer)
            mintUnkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Allocationid() As Integer
        Get
            Return mintAllocationid
        End Get
        Set(ByVal value As Integer)
            mintAllocationid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Allocationtranunkid() As Integer
        Get
            Return mintAllocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcenterunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Sohail
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Sohail
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Sohail
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Sohail
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    Private lstCCMapping As List(Of clsDept_costcenter_mapping)
    Public WriteOnly Property _lstCCMapping() As List(Of clsDept_costcenter_mapping)
        Set(ByVal value As List(Of clsDept_costcenter_mapping))
            lstCCMapping = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  unkid " & _
              ", allocationid " & _
              ", allocationtranunkid " & _
              ", costcenterunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM trdept_costcenter_mapping " & _
             "WHERE unkid = @unkid "

            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintUnkid = CInt(dtRow.Item("unkid"))
                mintAllocationid = CInt(dtRow.Item("allocationid"))
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                mintCostcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal intCurrentAllocationId As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal blnIncludePendingDepartments As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strTable As String = ""
        Dim strAllocationName As String = ""
        Dim strUnkIdField As String = ""
        Dim strCodeField As String = ""
        Dim strNameField As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            Select Case intCurrentAllocationId

                Case enAllocation.BRANCH

                    strAllocationName = Language.getMessage("clsMasterData", 430, "Branch")
                    strTable = "hrstation_master"
                    strUnkIdField = "stationunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.DEPARTMENT_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 429, "Department Group")
                    strTable = "hrdepartment_group_master"
                    strUnkIdField = "deptgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.DEPARTMENT

                    strAllocationName = Language.getMessage("clsMasterData", 428, "Department")
                    strTable = "hrdepartment_master"
                    strUnkIdField = "departmentunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.SECTION_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 427, "Section Group")
                    strTable = "hrsectiongroup_master"
                    strUnkIdField = "sectiongroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.SECTION

                    strAllocationName = Language.getMessage("clsMasterData", 426, "Section")
                    strTable = "hrsection_master"
                    strUnkIdField = "sectionunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.UNIT_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 425, "Unit Group")
                    strTable = "hrunitgroup_master"
                    strUnkIdField = "unitgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.UNIT

                    strAllocationName = Language.getMessage("clsMasterData", 424, "Unit")
                    strTable = "hrunit_master"
                    strUnkIdField = "unitunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.TEAM

                    strAllocationName = Language.getMessage("clsMasterData", 423, "Team")
                    strTable = "hrteam_master"
                    strUnkIdField = "teamunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.JOB_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 422, "Job Group")
                    strTable = "hrjobgroup_master"
                    strUnkIdField = "jobgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.JOBS

                    strAllocationName = Language.getMessage("clsMasterData", 421, "Jobs")
                    strTable = "hrjob_master"
                    strUnkIdField = "jobunkid"
                    strCodeField = "job_code"
                    strNameField = "job_name"

                Case enAllocation.CLASS_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 420, "Class Group")
                    strTable = "hrclassgroup_master"
                    strUnkIdField = "classgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.CLASSES

                    strAllocationName = Language.getMessage("clsMasterData", 419, "Classes")
                    strTable = "hrclasses_master"
                    strUnkIdField = "classesunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.COST_CENTER

                    strAllocationName = Language.getMessage("clsMasterData", 586, "Cost Center")
                    strTable = "prcostcenter_master"
                    strUnkIdField = "costcenterunkid"
                    strCodeField = "customcode"
                    strNameField = "costcentername"

            End Select

            strQ = "SELECT " & _
                      " trdept_costcenter_mapping.allocationid " & _
                      ", '" & strAllocationName & "' AS allocationname " & _
                      ", trdept_costcenter_mapping.costcenterunkid " & _
                      ", ISNULL(CC.customcode, '') AS CCCode " & _
                      ", ISNULL(CC.costcentername, '') AS CCName " & _
                      ", trdept_costcenter_mapping.allocationtranunkid " & _
                      ", CASE trdept_costcenter_mapping.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(" & strTable & "." & strCodeField & ",'') END AS allocationtrancode " & _
                      ", CASE trdept_costcenter_mapping.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(" & strTable & "." & strNameField & ",'') END AS allocationtranname "


            strQ &= " FROM trdept_costcenter_mapping " & _
                    " LEFT JOIN " & strTable & " ON " & strTable & "." & strUnkIdField & " = trdept_costcenter_mapping.allocationtranunkid " & _
                    " LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = trdept_costcenter_mapping.costcenterunkid "

            strQ &= " WHERE trdept_costcenter_mapping.isvoid = 0 " & _
                    "AND trdept_costcenter_mapping.allocationid = @allocationid "

            If blnIncludePendingDepartments = True Then


                strQ &= " UNION "

                strQ &= " SELECT " & _
                            " " & intCurrentAllocationId & " AS allocationid " & _
                            ", '" & strAllocationName & "' AS allocationname " & _
                            ", 0 AS costcenterunkid " & _
                            ", '' AS CCCode " & _
                            ", '' AS CCName " & _
                            ", " & strTable & "." & strUnkIdField & " AS allocationtranid " & _
                            ", ISNULL(" & strTable & "." & strCodeField & ", '') AS allocationtrancode " & _
                            ", ISNULL(" & strTable & "." & strNameField & ", '') AS allocationtranname "

                strQ &= " FROM " & strTable & " "

                strQ &= " WHERE  " & _
                        " " & strTable & ".isactive = 1 AND " & strTable & "." & strUnkIdField & " NOT IN "


                strQ &= " ( " & _
                            "      SELECT allocationtranunkid " & _
                            "      FROM trdept_costcenter_mapping " & _
                            "      WHERE isvoid = 0 And allocationid = @allocationid " & _
                            " ) "



            End If

            strQ &= " ORDER BY allocationtranname "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrentAllocationId)
            objDataOperation.AddParameter("@Company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, " " & Language.getMessage("frmDepartmentalTrainingNeedsList", 82, "Company")) 'Sohail (06 May 2021)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trdept_costcenter_mapping) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try




            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO trdept_costcenter_mapping ( " & _
              "  allocationid " & _
              ", allocationtranunkid " & _
              ", costcenterunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @allocationid " & _
              ", @allocationtranunkid " & _
              ", @costcenterunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintUnkid = dsList.Tables(0).Rows(0).Item(0)

            If Insert_AtTranLog(enAuditType.ADD, xDataOp) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
                If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_CostCenter") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_CostCenter")
                If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData")
            End If

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trdept_costcenter_mapping) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnkid.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE trdept_costcenter_mapping SET " & _
              "  allocationid = @allocationid" & _
              ", allocationtranunkid = @allocationtranunkid" & _
              ", costcenterunkid = @costcenterunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE unkid = @unkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintUnkid, objDataOperation) = True Then
                If Insert_AtTranLog(enAuditType.EDIT, xDataOp) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
                If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_CostCenter") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_CostCenter")
                If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData")
            End If

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trdept_costcenter_mapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM trdept_costcenter_mapping " & _
            "WHERE unkid = @unkid "

            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xAllocationId As Integer, ByVal xAllocationTranId As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If

        Try
            strQ = "SELECT " & _
              "  unkid " & _
              ", allocationid " & _
              ", allocationtranunkid " & _
              ", costcenterunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM trdept_costcenter_mapping " & _
             " WHERE isvoid = 0 AND allocationid = @allocationid AND allocationtranunkid = @allocationtranunkid "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationId)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationTranId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintUnkid = CInt(dsList.Tables(0).Rows(0)("unkid"))
            Else
                mintUnkid = 0
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Save() As Boolean
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet = Nothing

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()

            If lstCCMapping IsNot Nothing AndAlso lstCCMapping.Count > 0 Then

                For Each objCCMapping In lstCCMapping
                    With objCCMapping
                        mintAllocationid = .pintAllocationid
                        mintAllocationtranunkid = .pintAllocationtranunkid
                        mintCostcenterunkid = .pintCostcenterunkid

                        mintUserunkid = .pintUserunkid
                        mblnIsweb = .pblnIsweb
                        mblnIsvoid = .pblnIsvoid
                        mintVoiduserunkid = .pintVoiduserunkid
                        mdtVoiddatetime = .pdtVoiddatetime
                        mstrVoidreason = .pstrVoidreason

                        mstrClientIp = .pstrClientIp
                        mstrHostName = .pstrHostName
                        mstrFormName = .pstrFormName


                        isExist(mintAllocationid, mintAllocationtranunkid, objDataOperation)

                        If mintUnkid <= 0 Then

                            If Insert(objDataOperation) = False Then
                                If objDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                Exit For
                            End If

                        Else
                            If Update(objDataOperation) = False Then
                                If objDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                Exit For
                            End If
                        End If

                    End With
                Next

            End If
            objDataOperation.ReleaseTransaction(True)
            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_CostCenter") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_CostCenter")
            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData")
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Save; Module Name: " & mstrModuleName)
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Insert_AtTranLog(ByVal xAuditType As Integer, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO attrdept_costcenter_mapping ( " & _
                      "  unkid " & _
                      ", allocationid " & _
                      ", allocationtranunkid " & _
                      ", costcenterunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", form_name " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", isweb" & _
                    ") VALUES (" & _
                      "  @unkid " & _
                      ", @allocationid " & _
                      ", @allocationtranunkid " & _
                      ", @costcenterunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @form_name " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @isweb" & _
                    "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnkid.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)

            If mstrClientIp.Trim.Length > 0 Then
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIp)
            Else
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrHostName.Length > 0 Then
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            Else
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function IsTableDataUpdate(ByVal intUnkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strFields As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strFields = " allocationid,allocationtranunkid,costcenterunkid "

            strQ = "WITH CTE AS ( " & _
                            "SELECT TOP 1 " & strFields & " " & _
                            "FROM attrdept_costcenter_mapping " & _
                            "WHERE attrdept_costcenter_mapping.unkid = @unkid " & _
                            "AND attrdept_costcenter_mapping.audittype <> 3 " & _
                            "ORDER BY attrdept_costcenter_mapping.atunkid DESC " & _
                        ") " & _
                    " " & _
                    "SELECT " & strFields & " " & _
                    "FROM trdept_costcenter_mapping " & _
                    "WHERE trdept_costcenter_mapping.unkid = @unkid " & _
                    "EXCEPT " & _
                    "SELECT * FROM cte "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class
