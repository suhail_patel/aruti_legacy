﻿'************************************************************************************************************************************
'Class Name : clshrtnafeedback_master.vb
'Purpose    :
'Date       :23/02/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clshrtnafeedback_master
    Private Shared ReadOnly mstrModuleName As String = "clshrtnafeedback_master"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintFeedbackmasterunkid As Integer
    Private mintTrainingschduleunkid As Integer
    Private mintCourseunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mblnIscomplete As Boolean
    Private mdtFeedbackdate As Date

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set feedbackmasterunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Feedbackmasterunkid() As Integer
        Get
            Return mintFeedbackmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintFeedbackmasterunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingschedulingunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Trainingschduleunkid() As Integer
        Get
            Return mintTrainingschduleunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingschduleunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set courseunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Courseunkid() As Integer
        Get
            Return mintCourseunkid
        End Get
        Set(ByVal value As Integer)
            mintCourseunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscomplete
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iscomplete() As Boolean
        Get
            Return mblnIscomplete
        End Get
        Set(ByVal value As Boolean)
            mblnIscomplete = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set feedbackdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Feedbackdate() As Date
        Get
            Return mdtFeedbackdate
        End Get
        Set(ByVal value As Date)
            mdtFeedbackdate = Value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  feedbackmasterunkid " & _
              ", trainingschedulingunkid " & _
              ", courseunkid " & _
              ", employeeunkid " & _
              ", feedbackdate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", iscomplete " & _
             "FROM hrtnafeedback_master " & _
             "WHERE feedbackmasterunkid = @feedbackmasterunkid "

            objDataOperation.AddParameter("@feedbackmasterunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintFeedbackmasterUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintfeedbackmasterunkid = CInt(dtRow.Item("feedbackmasterunkid"))
                mintTrainingschduleunkid = CInt(dtRow.Item("trainingschedulingunkid"))
                mintcourseunkid = CInt(dtRow.Item("courseunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIscomplete = CBool(dtRow.Item("iscomplete"))
                mdtFeedbackdate = dtRow.Item("feedbackdate")
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "hrtnafeedback_master.feedbackmasterunkid " & _
                    ",hrtnafeedback_master.trainingschedulingunkid " & _
                    ",hrtnafeedback_master.courseunkid " & _
                    ",hrtnafeedback_master.employeeunkid " & _
                    ",CONVERT(CHAR(8),hrtnafeedback_master.feedbackdate,112) As feedbackdate " & _
                    ",hrtnafeedback_master.userunkid " & _
                    ",hrtnafeedback_master.isvoid " & _
                    ",hrtnafeedback_master.voiduserunkid " & _
                    ",hrtnafeedback_master.voiddatetime " & _
                    ",hrtnafeedback_master.voidreason " & _
                    ",hrtnafeedback_master.iscomplete " & _
                    ",ISNULL(cfcommon_master.name,'') AS TrainingAttended " & _
                    ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                    ",ISNULL(hremployee_master.employeecode,'') AS ECode " & _
                    ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) AS SDate " & _
                    ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) AS EDate " & _
                   "FROM hrtnafeedback_master " & _
                    "JOIN hremployee_master ON hrtnafeedback_master.employeeunkid = hremployee_master.employeeunkid " & _
                    "JOIN hrtraining_scheduling ON hrtnafeedback_master.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                    "JOIN cfcommon_master ON cfcommon_master.masterunkid =  hrtraining_scheduling.course_title " & _
                    " AND cfcommon_master.masterunkid = hrtnafeedback_master.courseunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "' "

            If blnOnlyActive Then
                strQ &= " WHERE hrtnafeedback_master.isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dRow.Item("TrainingAttended") = dRow.Item("TrainingAttended") & " ( " & eZeeDate.convertDate(dRow.Item("SDate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("EDate").ToString).ToShortDateString & " ) "
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtnafeedback_master) </purpose>
    Public Function Insert(ByVal dTable As DataTable) As Boolean
        If isExist(mintTrainingschduleunkid, mintEmployeeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Feedback of this employee for this particular course group is already present. You can edit it or define new feedback of another course.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingschduleunkid.ToString)
            objDataOperation.AddParameter("@courseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscomplete.ToString)
            objDataOperation.AddParameter("@feedbackdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFeedbackdate)

            strQ = "INSERT INTO hrtnafeedback_master ( " & _
                      "  trainingschedulingunkid " & _
                      ", courseunkid " & _
                      ", employeeunkid " & _
                      ", feedbackdate " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", iscomplete" & _
                    ") VALUES (" & _
                      "  @trainingschedulingunkid " & _
                      ", @courseunkid " & _
                      ", @employeeunkid " & _
                      ", @feedbackdate " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
                      ", @iscomplete" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFeedbackmasterunkid = dsList.Tables(0).Rows(0).Item(0)

            If dTable IsNot Nothing Then
                Dim objFeedbackTran As New clshrtnafeedback_tran
                If dTable.Rows.Count > 0 Then
                    objFeedbackTran._Feedbackmasterunkid = mintFeedbackmasterunkid
                    objFeedbackTran._DataTable = dTable
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    If objFeedbackTran.InsertUpdateDeleteFeedback(objDataOperation, mintUserunkid) = False Then
                        'If objFeedbackTran.InsertUpdateDeleteFeedback(objDataOperation) = False Then
                        'Sohail (23 Apr 2012) -- End
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtnafeedback_master) </purpose>
    Public Function Update(ByVal dTable As DataTable) As Boolean
        If isExist(mintTrainingschduleunkid, mintEmployeeunkid, mintFeedbackmasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Feedback of this employee for this particular course group is already present. You can edit it or define new feedback of another course.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@feedbackmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackmasterunkid.ToString)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingschduleunkid.ToString)
            objDataOperation.AddParameter("@courseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscomplete.ToString)
            objDataOperation.AddParameter("@feedbackdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFeedbackdate)

            strQ = "UPDATE hrtnafeedback_master SET " & _
              "  trainingschedulingunkid = @trainingschedulingunkid" & _
              ", courseunkid = @courseunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", feedbackdate = @feedbackdate" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", iscomplete = @iscomplete " & _
            "WHERE feedbackmasterunkid = @feedbackmasterunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dTable IsNot Nothing Then
                Dim objFeedbackTran As New clshrtnafeedback_tran
                If dTable.Rows.Count > 0 Then
                    objFeedbackTran._Feedbackmasterunkid = mintFeedbackmasterunkid
                    objFeedbackTran._DataTable = dTable
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    If objFeedbackTran.InsertUpdateDeleteFeedback(objDataOperation, mintUserunkid) = False Then
                        'If objFeedbackTran.InsertUpdateDeleteFeedback(objDataOperation) = False Then
                        'Sohail (23 Apr 2012) -- End
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtnafeedback_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrtnafeedback_master SET " & _
                   " isvoid = @isvoid " & _
                   ",voiduserunkid = @voiduserunkid " & _
                   ",voiddatetime = @voiddatetime " & _
                   ",voidreason = @voidreason " & _
                   " WHERE feedbackmasterunkid = @feedbackmasterunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@feedbackmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = clsCommonATLog.GetChildList(objDataOperation, "hrtnafeedback_tran", "feedbackmasterunkid", intUnkid)

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                    strQ = "UPDATE hrtnafeedback_tran SET " & _
                           " isvoid = @isvoid" & _
                           ",voiduserunkid = @voiduserunkid" & _
                           ",voiddatetime = @voiddatetime" & _
                           ",voidreason = @voidreason " & _
                           "WHERE feedbacktranunkid = '" & dRow.Item("feedbacktranunkid") & "'"

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", intUnkid, "hrtnafeedback_tran", "feedbacktranunkid", dRow.Item("feedbacktranunkid"), 3, 3, , mintVoiduserunkid) = False Then
                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", intUnkid, "hrtnafeedback_tran", "feedbacktranunkid", dRow.Item("feedbacktranunkid"), 3, 3) = False Then
                        'Sohail (23 Apr 2012) -- End
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@feedbackmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intSchdulingUnkid As Integer, ByVal intEmployeeUnkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                  "  feedbackmasterunkid " & _
                  ", trainingschedulingunkid " & _
                  ", courseunkid " & _
                  ", employeeunkid " & _
                  ", userunkid " & _
                  ", isvoid " & _
                  ", voiduserunkid " & _
                  ", voiddatetime " & _
                  ", voidreason " & _
                  ", iscomplete " & _
                 "FROM hrtnafeedback_master " & _
                 "WHERE trainingschedulingunkid = @trainingschedulingunkid AND employeeunkid = @employeeunkid " & _
                 "AND ISNULL(isvoid,0) = 0 " 'Sohail (23 Apr 2012) - [ISNULL(isvoid,0) = 0]

            If intUnkid > 0 Then
                strQ &= " AND feedbackmasterunkid <> @feedbackmasterunkid"
            End If

            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.NVarChar, eZeeDataType.INT_SIZE, intSchdulingUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.INT_SIZE, intEmployeeUnkid)
            objDataOperation.AddParameter("@feedbackmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetSubItemCount(Optional ByVal intFeedbackGrpId As Integer = -1) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataoperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                   "     hrtnafdbk_item_master.fdbkitemunkid AS Id " & _
                   "    ,COUNT(hrtnafdbk_subitem_master.fdbksubitemunkid) AS Cnt " & _
                   "FROM hrtnafdbk_item_master " & _
                   "    LEFT JOIN hrtnafdbk_subitem_master ON hrtnafdbk_item_master.fdbkitemunkid = hrtnafdbk_subitem_master.fdbkitemunkid AND hrtnafdbk_subitem_master.isactive = 1 " & _
                   "WHERE hrtnafdbk_item_master.isactive = 1 AND hrtnafdbk_item_master.fromimpact = 0 "
            If intFeedbackGrpId > 0 Then
                StrQ &= " AND fdbkgroupunkid = '" & intFeedbackGrpId & "'  "
            End If
            StrQ &= " GROUP BY hrtnafdbk_item_master.fdbkitemunkid "

            dsList = objDataoperation.ExecQuery(StrQ, "List")

            If objDataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSubItemCount; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetItems_BasedGroup(Optional ByVal intGrpId As Integer = -1) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iCnt As Integer = -1
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT fdbkitemunkid FROM hrtnafdbk_item_master WHERE isactive = 1 AND hrtnafdbk_item_master.fromimpact = 0 "
            If intGrpId > 0 Then
                StrQ &= " AND fdbkgroupunkid = '" & intGrpId & "' "
            End If

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return iCnt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetItems_BasedGroup; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [06-MAR-2017] -- START
    'ISSUE/ENHANCEMENT : Training Module Notification
    Public Sub SendNotification(ByVal intTrainingSchedulingId As Integer, _
                               ByVal intEmployeeId As Integer, _
                               ByVal mdtEmpAsOnDate As DateTime, _
                               ByVal xLoginMod As enLogin_Mode, _
                               ByVal xUserId As Integer, _
                               ByVal strCSVSelectedUserIds As String, ByVal xLogEmpId As Integer, ByVal strFormName As String, ByVal intCompanyUnkId As Integer)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Dim strMessage As New System.Text.StringBuilder
        Dim objEmp As New clsEmployee_Master : Dim objTrSchedule As New clsTraining_Scheduling : Dim objCMaster As New clsCommon_Master : Dim objUsr As New clsUserAddEdit
        Try
            objEmp._Employeeunkid(mdtEmpAsOnDate) = intEmployeeId
            objTrSchedule._Trainingschedulingunkid = intTrainingSchedulingId
            objCMaster._Masterunkid = objTrSchedule._Course_Title
            Dim dsLst As New DataSet
            dsLst = objUsr.GetList("List", "hrmsConfiguration..cfuser_master.userunkid IN (" & strCSVSelectedUserIds & ")")
            If dsLst.Tables("List").Rows.Count > 0 Then
                For Each row As DataRow In dsLst.Tables("List").Rows
                    If row.Item("email").ToString.Trim.Length > 0 Then
                        Dim strName As String = row.Item("firstname") & " " & row.Item("lastname")
                        strMessage.Append("<HTML> <BODY>")

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language


                        'strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & IIf(strName.Trim.Length > 0, strName, row.Item("username")) & "</B>, <BR><BR>")
                        strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & getTitleCase(IIf(strName.Trim.Length > 0, strName, row.Item("username"))) & "</B>, <BR><BR>")
                        'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 101, "This is to inform you that, I have done with level I evaluation for the training attended with details below :") & "<BR><BR>")
                        strMessage.Append(Language.getMessage(mstrModuleName, 101, "This is to inform you that, I have done with level I evaluation for the training attended with details below :") & "<BR><BR>")
                        'Gajanan [27-Mar-2019] -- End
                        strMessage.Append("<TABLE border = '1' WIDTH = '60%'>")
                        strMessage.Append("<TR WIDTH = '90%' bgcolor= 'SteelBlue'>")
                        strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 102, "Course Title") & "</span></b></TD>")
                        strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 103, "Duration") & "</span></b></TD>")
                        strMessage.Append("</TR>")
                        strMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                        strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & objCMaster._Name & "</span></TD>")
                        strMessage.Append("<TD align = 'LEFT' WIDTH = '60%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage(mstrModuleName, 106, "Date :") & " " & objTrSchedule._Start_Date.ToShortDateString & " - " & objTrSchedule._End_Date.ToShortDateString & "<BR> " & Language.getMessage(mstrModuleName, 107, "Time :") & " " & " (" & Format(objTrSchedule._Start_Time, "HH:MM") & " - " & Format(objTrSchedule._End_Time, "HH:MM") & ") </span></TD>")
                        strMessage.Append("</TR>" & vbCrLf)
                        strMessage.Append("</TABLE>")
                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                        strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                        'Gajanan [27-Mar-2019] -- End

                        strMessage.Append("</span></p>")
                        strMessage.Append("</BODY></HTML>")
                    End If

                    If strMessage.ToString.Trim.Length > 0 Then
                        Dim objSendMail As New clsSendMail
                        objSendMail._ToEmail = row.Item("email").ToString.Trim
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 105, "Notification of Level I Evaluation")
                        objSendMail._Message = strMessage.ToString
                        objSendMail._Form_Name = strFormName
                        objSendMail._LogEmployeeUnkid = xLogEmpId
                        objSendMail._OperationModeId = xLoginMod
                        objSendMail._UserUnkid = xUserId
                        objSendMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                        Try
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(intCompanyUnkId)
                            'Sohail (30 Nov 2017) -- End
                        Catch ex As Exception
                        End Try
                        objSendMail = Nothing
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotification; Module Name: " & mstrModuleName)
        Finally
            objEmp = Nothing : objTrSchedule = Nothing : objCMaster = Nothing : objUsr = Nothing
        End Try
    End Sub
    'S.SANDEEP [06-MAR-2017] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Feedback of this employee for this particular course group is already present. You can edit it or define new feedback of another course.")
            Language.setMessage(mstrModuleName, 100, "Dear")
            Language.setMessage(mstrModuleName, 101, "This is to inform you that, I have done with level I evaluation for the training attended with details below :")
            Language.setMessage(mstrModuleName, 102, "Course Title")
            Language.setMessage(mstrModuleName, 103, "Duration")
            Language.setMessage(mstrModuleName, 105, "Notification of Level I Evaluation")
            Language.setMessage(mstrModuleName, 106, "Date :")
            Language.setMessage(mstrModuleName, 107, "Time :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
