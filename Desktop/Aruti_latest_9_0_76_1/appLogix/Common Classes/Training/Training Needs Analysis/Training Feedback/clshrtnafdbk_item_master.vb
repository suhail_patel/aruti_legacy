﻿'************************************************************************************************************************************
'Class Name : clshrtnafdbk_item_master.vb
'Purpose    :
'Date       :23/02/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clshrtnafdbk_item_master
    Private Shared ReadOnly mstrModuleName As String = "clshrtnafdbk_item_master"
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintFdbkitemunkid As Integer
    Private mintFdbkgroupunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mintResultgroupunkid As Integer
    Private mblnFromImpact As Boolean = False
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fdbkitemunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fdbkitemunkid() As Integer
        Get
            Return mintFdbkitemunkid
        End Get
        Set(ByVal value As Integer)
            mintFdbkitemunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fdbkgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fdbkgroupunkid() As Integer
        Get
            Return mintFdbkgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintFdbkgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Resultgroupunkid() As Integer
        Get
            Return mintResultgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintResultgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FromImpact
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _FromImpact() As Boolean
        Get
            Return mblnFromImpact
        End Get
        Set(ByVal value As Boolean)
            mblnFromImpact = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  fdbkitemunkid " & _
              ", fdbkgroupunkid " & _
              ", resultgroupunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
              ", fromimpact " & _
             "FROM hrtnafdbk_item_master " & _
             "WHERE fdbkitemunkid = @fdbkitemunkid "

            objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFdbkitemunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintFdbkitemunkid = CInt(dtRow.Item("fdbkitemunkid"))
                mintFdbkgroupunkid = CInt(dtRow.Item("fdbkgroupunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mintResultgroupunkid = CInt(dtRow.Item("resultgroupunkid"))
                mblnFromImpact = CBool(dtRow.Item("fromimpact"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnFromImpact As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  hrtnafdbk_item_master.fdbkitemunkid " & _
              ", hrtnafdbk_item_master.fdbkgroupunkid " & _
              ", hrtnafdbk_item_master.resultgroupunkid " & _
              ", cfcommon_master.name as resultgroup " & _
              ", hrtnafdbk_item_master.code " & _
              ", hrtnafdbk_item_master.name " & _
              ", hrtnafdbk_item_master.description " & _
              ", hrtnafdbk_item_master.isactive " & _
              ", hrtnafdbk_item_master.name1 " & _
              ", hrtnafdbk_item_master.name2 " & _
              ", ISNULL(hrtnafdbk_group_master.name,'') AS groupname " & _
              ", fromimpact " & _
             "FROM hrtnafdbk_item_master " & _
             " LEFT JOIN hrtnafdbk_group_master ON hrtnafdbk_group_master.fdbkgroupunkid = hrtnafdbk_item_master.fdbkgroupunkid " & _
             " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid =  hrtnafdbk_item_master.resultgroupunkid AND mastertype= " & clsCommon_Master.enCommonMaster.RESULT_GROUP & _
             " WHERE 1= 1"

            If blnOnlyActive Then
                strQ &= " AND hrtnafdbk_item_master.isactive = 1 "
            End If

            If blnFromImpact Then
                strQ &= " AND hrtnafdbk_item_master.fromimpact = 1 "
            Else
                strQ &= " AND hrtnafdbk_item_master.fromimpact = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtnafdbk_item_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode, , mintFdbkgroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This feedback item code already exists. Please define new item code.")
            Return False
        End If

        If isExist(, mstrName, mintFdbkgroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This feedback item name already exists. Please define new item name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@fdbkgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFdbkgroupunkid.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@fromimpact", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnFromImpact.ToString)

            strQ = "INSERT INTO hrtnafdbk_item_master ( " & _
              "  fdbkgroupunkid " & _
              ", resultgroupunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2" & _
              ", fromimpact " & _
            ") VALUES (" & _
              "  @fdbkgroupunkid " & _
              ", @resultgroupunkid " & _
              ", @code " & _
              ", @name " & _
              ", @description " & _
              ", @isactive " & _
              ", @name1 " & _
              ", @name2" & _
              ", @fromimpact " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFdbkitemunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrtnafdbk_item_master", "fdbkitemunkid", mintFdbkitemunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtnafdbk_item_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, , mintFdbkgroupunkid, mintFdbkitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This feedback item code already exists. Please define new item code.")
            Return False
        End If

        If isExist(, mstrName, mintFdbkgroupunkid, mintFdbkitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This feedback item name already exists. Please define new item name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFdbkitemunkid.ToString)
            objDataOperation.AddParameter("@fdbkgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFdbkgroupunkid.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@fromimpact", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnFromImpact.ToString)

            strQ = "UPDATE hrtnafdbk_item_master SET " & _
                      "  fdbkgroupunkid = @fdbkgroupunkid" & _
                      ", resultgroupunkid = @resultgroupunkid" & _
                      ", code = @code" & _
                      ", name = @name" & _
                      ", description = @description" & _
                      ", isactive = @isactive" & _
                      ", name1 = @name1" & _
                      ", name2 = @name2 " & _
                      ", fromimpact = @fromimpact " & _
                   "WHERE fdbkitemunkid = @fdbkitemunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrtnafdbk_item_master", mintFdbkitemunkid, "fdbkitemunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrtnafdbk_item_master", "fdbkitemunkid", mintFdbkitemunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtnafdbk_item_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this feedback item. Reason : This feedback item is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrtnafdbk_item_master SET " & _
                   " isactive = 0 " & _
                   "WHERE fdbkitemunkid = @fdbkitemunkid "

            objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrtnafdbk_item_master", "fdbkitemunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "    'SELECT * FROM '+TABLE_NAME +' WHERE ' +COLUMN_NAME +' = ' AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='fdbkitemunkid' AND TABLE_NAME <> 'hrtnafdbk_item_master' "

            objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                strQ = dRow.Item("TableName") & "'" & intUnkid & "'"
                If objDataOperation.RecordCount(strQ) > 0 Then blnFlag = True

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            mstrMessage = ""
            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intGroupId As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  fdbkitemunkid " & _
              ", fdbkgroupunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
              ", fromimpact " & _
             "FROM hrtnafdbk_item_master " & _
             "WHERE isactive = 1 "


            If strName.Trim.Length > 0 Then
                strQ &= "AND name = @name "
            End If

            If strCode.Trim.Length > 0 Then
                strQ &= "AND code = @code "
            End If

            If intGroupId > 0 Then
                strQ &= " AND fdbkgroupunkid = @fdbkgroupunkid "
            End If

            If intUnkid > 0 Then
                strQ &= " AND fdbkitemunkid <> @fdbkitemunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@fdbkgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGroupId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal intGroupUnkid As Integer = -1, Optional ByVal blnFromImpact As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As fdbkitemunkid , @ItemName As  name  UNION "
            End If
            strQ &= "SELECT fdbkitemunkid,name FROM hrtnafdbk_item_master WHERE isactive = 1 "

            If intGroupUnkid > 0 Then
                strQ &= " AND fdbkgroupunkid = '" & intGroupUnkid & "' "
            End If

            If blnFromImpact Then
                strQ &= " AND hrtnafdbk_item_master.fromimpact = 1 "
            Else
                strQ &= " AND hrtnafdbk_item_master.fromimpact = 0 "
            End If

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetFeedbackItemUnkid(ByVal StrName As String, ByVal intGroupId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "  fdbkitemunkid " & _
                   "FROM hrtnafdbk_item_master " & _
                   "WHERE isactive = 1 AND name = @name AND fdbkgroupunkid = '" & intGroupId & "' "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("fdbkitemunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFeedbackItemUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This feedback item code already exists. Please define new item code.")
            Language.setMessage(mstrModuleName, 2, "This feedback item name already exists. Please define new item name.")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this feedback item. Reason : This feedback item is already linked with some transaction.")
            Language.setMessage(mstrModuleName, 4, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
