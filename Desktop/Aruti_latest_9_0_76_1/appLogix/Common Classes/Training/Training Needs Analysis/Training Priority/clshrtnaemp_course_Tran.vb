﻿'************************************************************************************************************************************
'Class Name : clstnaemp_course_Tran.vb
'Purpose    :
'Date       :15/02/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clshrtnaemp_course_Tran
    Private Const mstrModuleName = "clshrtnaemp_course_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCourseemptranunkid As Integer
    Private mintCoursepriotranunkid As Integer
    Private mintPrioritymasterunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set courseemptranunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Courseemptranunkid() As Integer
        Get
            Return mintCourseemptranunkid
        End Get
        Set(ByVal value As Integer)
            mintCourseemptranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coursepriotranunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Coursepriotranunkid() As Integer
        Get
            Return mintCoursepriotranunkid
        End Get
        Set(ByVal value As Integer)
            mintCoursepriotranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set prioritymasterunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Prioritymasterunkid() As Integer
        Get
            Return mintPrioritymasterunkid
        End Get
        Set(ByVal value As Integer)
            mintPrioritymasterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

#End Region


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtnaemp_course_tran) </purpose>
    Public Function InsertUpdate_Delete(ByVal objDataOperation As clsDataOperation, ByVal mdtData() As DataRow) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If mdtData Is Nothing Then Return True

            For i = 0 To mdtData.Length - 1

                With mdtData(i)
                    If Not IsDBNull(.Item("AUD")) Then

                        Select Case .Item("AUD")

                            Case "A"
A:                              objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@coursepriotranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoursepriotranunkid.ToString)
                                objDataOperation.AddParameter("@courseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("courseunkid").ToString())
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString())
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                strQ = "INSERT INTO hrtnaemp_course_tran ( " & _
                                  "  coursepriotranunkid " & _
                                  ", courseunkid " & _
                                  ", employeeunkid " & _
                                  ", userunkid " & _
                                  ", isvoid " & _
                                  ", voiduserunkid " & _
                                  ", voiddatetime " & _
                                  ", voidreason" & _
                                ") VALUES (" & _
                                  "  @coursepriotranunkid " & _
                                  ", @courseunkid " & _
                                  ", @employeeunkid " & _
                                  ", @userunkid " & _
                                  ", @isvoid " & _
                                  ", @voiduserunkid " & _
                                  ", @voiddatetime " & _
                                  ", @voidreason" & _
                                "); SELECT @@identity"

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintCourseemptranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("courseemptranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", .Item("prioritymasterunkid"), "hrtnaemp_course_tran", "courseemptranunkid", mintCourseemptranunkid, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", mintPrioritymasterunkid, "hrtnaemp_course_tran", "courseemptranunkid", mintCourseemptranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "D"

                                objDataOperation.AddParameter("@coursepriotranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("coursepriotranunkid").ToString)
                                objDataOperation.AddParameter("@courseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("courseunkid").ToString())
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString())
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                If .Item("voidreason").ToString.Trim.Length > 0 Then
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                Else
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                End If

                                strQ = " UPDATE hrtnaemp_course_tran SET " & _
                                          " isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voiddatetime = @voiddatetime" & _
                                          ", voidreason = @voidreason " & _
                                         " WHERE courseunkid = @courseunkid AND coursepriotranunkid = @coursepriotranunkid AND employeeunkid = @employeeunkid "

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", mintPrioritymasterunkid, "hrtnaemp_course_tran", "courseemptranunkid", .Item("courseemptranunkid").ToString, 2, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            Case Else
                                If IsExist(.Item("coursepriotranunkid"), .Item("employeeunkid"), objDataOperation) = False Then
                                    GoTo A
                                End If
                        End Select

                    End If

                End With

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function IsExist(ByVal intCourseTranUnkid As Integer, ByVal intEmpUnkid As Integer, ByVal objDataoperation As clsDataOperation) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = ""
        Dim iCnt As Integer = -1
        Try
            StrQ = "SELECT * FROM hrtnaemp_course_tran WHERE coursepriotranunkid ='" & intCourseTranUnkid & "' AND employeeunkid = '" & intEmpUnkid & "' "

            iCnt = objDataoperation.RecordCount(StrQ)

            If objDataoperation.ErrorMessage <> "" Then
                Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
            End If

            If iCnt > 0 Then
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : IsExist; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function GetEmployeeBasedOnCourse(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal intCourseUnkid As Integer, _
                            Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = "SELECT DISTINCT " & _
                       "     hremployee_master.employeecode AS ECode " & _
                       "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master .surname,'') AS EName " & _
                       "    ,hremployee_master.employeeunkid AS EmpId " & _
                       "FROM hrtnaemp_course_tran " & _
                       "    JOIN hremployee_master ON hrtnaemp_course_tran.employeeunkid = hremployee_master.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "WHERE isvoid = 0 AND courseunkid = '" & intCourseUnkid & "' "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                dsList = objDo.ExecQuery(StrQ, StrList)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeBasedOnCourse; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    ''S.SANDEEP [ 24 FEB 2012 ] -- START
    ''ENHANCEMENT : TRA CHANGES [<TRAINING>]
    'Public Function GetEmployeeBasedOnCourse(ByVal intCourseUnkid As Integer, Optional ByVal StrList As String = "List") As DataSet
    '    Dim StrQ As String = ""
    '    Dim objDataOperation As New clsDataOperation
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        StrQ = "SELECT DISTINCT " & _
    '               "     hremployee_master.employeecode AS ECode " & _
    '               "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master .surname,'') AS EName " & _
    '               "    ,hremployee_master.employeeunkid AS EmpId " & _
    '               "FROM hrtnaemp_course_tran " & _
    '               "    JOIN hremployee_master ON hrtnaemp_course_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '               "WHERE isvoid = 0 AND courseunkid = '" & intCourseUnkid & "' "

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select
    '        StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END



    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, StrList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure : GetEmployeeBasedOnCourse; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function
    ''S.SANDEEP [ 24 FEB 2012 ] -- END 

    'S.SANDEEP [04 JUN 2015] -- END

End Class