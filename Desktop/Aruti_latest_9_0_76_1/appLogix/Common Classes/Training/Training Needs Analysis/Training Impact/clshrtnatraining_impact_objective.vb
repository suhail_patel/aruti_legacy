﻿'************************************************************************************************************************************
'Class Name : clshrtnatraining_impact_objective.vb
'Purpose    :
'Date       :27/02/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clshrtnatraining_impact_objective
    Private Const mstrModuleName = "clshrtnatraining_impact_objective"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintObjectiveunkid As Integer
    Private mintImpactunkid As Integer
    Private mstrTraining_Objective As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private dsObjectiveList As DataSet
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set objectiveunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Objectiveunkid() As Integer
        Get
            Return mintObjectiveunkid
        End Get
        Set(ByVal value As Integer)
            mintObjectiveunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set impactunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Impactunkid() As Integer
        Get
            Return mintImpactunkid
        End Get
        Set(ByVal value As Integer)
            mintImpactunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtObjectiveList
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ObjectiveList() As DataSet
        Get
            Return dsObjectiveList
        End Get
        Set(ByVal value As DataSet)
            dsObjectiveList = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New()

        dsObjectiveList = New DataSet
        dsObjectiveList.Tables.Add("Objective")
        dsObjectiveList.Tables(0).Columns.Add("Objectiveunkid", Type.GetType("System.Int32"))
        dsObjectiveList.Tables(0).Columns.Add("Impactunkid", Type.GetType("System.Int32"))
        dsObjectiveList.Tables(0).Columns.Add("Training_Objective", Type.GetType("System.String"))
        dsObjectiveList.Tables(0).Columns.Add("isvoid", Type.GetType("System.Boolean"))
        dsObjectiveList.Tables(0).Columns.Add("voiduserunkid", Type.GetType("System.Int32"))
        dsObjectiveList.Tables(0).Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
        dsObjectiveList.Tables(0).Columns.Add("voidreason", Type.GetType("System.String"))
        dsObjectiveList.Tables(0).Columns.Add("AUD", Type.GetType("System.String"))
        dsObjectiveList.Tables(0).Columns.Add("GUID", Type.GetType("System.String"))

    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intObjectiveId As Integer = -1, Optional ByVal intImpactId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  objectiveunkid " & _
              ", impactunkid " & _
              ", training_objective " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AUD " & _
              ", '' GUID " & _
              " FROM hrtnatraining_impact_objective " & _
              " WHERE 1=1"

            If blnOnlyActive Then
                strQ &= " AND isvoid = 0 "
            End If

            If intObjectiveId > 0 Then
                strQ &= " AND objectiveunkid = @objectiveunkid "
                objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intObjectiveId)
            End If

            If intImpactId > 0 Then
                strQ &= " AND impactunkid = @impactunkid "
                objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intImpactId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtnatraining_impact_objective) </purpose>
    Public Function InsertUpdateDelete_Objective(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If dsObjectiveList Is Nothing Then Return True

            For i = 0 To dsObjectiveList.Tables(0).Rows.Count - 1
                With dsObjectiveList.Tables(0).Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

Insert:
                                objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImpactunkid.ToString)
                                objDataOperation.AddParameter("@training_objective", SqlDbType.NVarChar, 4000, .Item("training_objective").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                strQ = "INSERT INTO hrtnatraining_impact_objective ( " & _
                                            "  impactunkid " & _
                                            ", training_objective " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                          ") VALUES (" & _
                                            "  @impactunkid " & _
                                            ", @training_objective " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                          "); SELECT @@identity"

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintObjectiveunkid = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("objectiveunkid") > 0 Then
                                    'Sohail (23 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", .Item("impactunkid"), "hrtnatraining_impact_objective", "objectiveunkid", .Item("objectiveunkid"), 2, 1, , mintUserunkid) = False Then
                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", .Item("impactunkid"), "hrtnatraining_impact_objective", "objectiveunkid", .Item("objectiveunkid"), 2, 1) = False Then
                                        'Sohail (23 Apr 2012) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    'Sohail (23 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", mintImpactunkid, "hrtnatraining_impact_objective", "objectiveunkid", mintObjectiveunkid, 1, 1, , mintUserunkid) = False Then
                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", mintImpactunkid, "hrtnatraining_impact_objective", "objectiveunkid", mintObjectiveunkid, 1, 1) = False Then
                                        'Sohail (23 Apr 2012) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"

                                If .Item("GUID").ToString() <> "" Then
                                    GoTo Insert
                                End If

                                strQ = "UPDATE hrtnatraining_impact_objective SET " & _
                                         "  impactunkid = @impactunkid" & _
                                         ", training_objective = @training_objective" & _
                                         ", userunkid = @userunkid" & _
                                         ", isvoid = @isvoid" & _
                                         ", voiduserunkid = @voiduserunkid" & _
                                         ", voiddatetime = @voiddatetime" & _
                                         ", voidreason = @voidreason " & _
                                       "WHERE objectiveunkid = @objectiveunkid AND impactunkid = @impactunkid"


                                objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("objectiveunkid").ToString)
                                objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("impactunkid").ToString)
                                objDataOperation.AddParameter("@training_objective", SqlDbType.NVarChar, 4000, .Item("training_objective").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (23 Apr 2012) -- Start
                                'TRA - ENHANCEMENT
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", .Item("impactunkid"), "hrtnatraining_impact_objective", "objectiveunkid", .Item("objectiveunkid"), 2, 2, , mintUserunkid) = False Then
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", .Item("impactunkid"), "hrtnatraining_impact_objective", "objectiveunkid", .Item("objectiveunkid"), 2, 2) = False Then
                                    'Sohail (23 Apr 2012) -- End
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                strQ = "UPDATE hrtnatraining_impact_objective SET " & _
                                             " isvoid = @isvoid" & _
                                             ", voiduserunkid = @voiduserunkid" & _
                                             ", voiddatetime = @voiddatetime" & _
                                             ", voidreason = @voidreason " & _
                                           "WHERE objectiveunkid = @objectiveunkid AND impactunkid = @impactunkid"


                                objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("objectiveunkid").ToString)
                                objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("impactunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IIf(.Item("isvoid") Is DBNull.Value, False, .Item("isvoid").ToString()))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(.Item("voiduserunkid") Is DBNull.Value, -1, .Item("voiduserunkid").ToString))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("voiddatetime") Is Nothing, DBNull.Value, .Item("voiddatetime")))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(.Item("voidreason") Is DBNull.Value, "", .Item("voidreason").ToString))
                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (23 Apr 2012) -- Start
                                'TRA - ENHANCEMENT
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", .Item("impactunkid"), "hrtnatraining_impact_objective", "objectiveunkid", .Item("objectiveunkid"), 2, 3, , mintUserunkid) = False Then
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", .Item("impactunkid"), "hrtnatraining_impact_objective", "objectiveunkid", .Item("objectiveunkid"), 2, 3) = False Then
                                    'Sohail (23 Apr 2012) -- End
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select

                    End If

                End With

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_Objective; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

End Class