﻿'************************************************************************************************************************************
'Class Name : clsTraining_Scheduling.vb
'Purpose    :
'Date       :31/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsTraining_Scheduling
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Scheduling"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objResourcesTran As New clsTraining_Resources_Tran
    Dim objTrainresTran As New clsTraining_Trainers_Tran
    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Dim objTrainingAlloc As New clsTraining_Allocation_Tran
    'S.SANDEEP [08-FEB-2017] -- END

#Region " Private variables "
    Private mintTrainingschedulingunkid As Integer

    'Anjan (10 Feb 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    'Private mstrCourse_Title As String = String.Empty
    Private mstrCourse_Title As String = 0
    'Anjan (10 Feb 2012)-End 


    Private mintYearunkid As Integer
    Private mdtStart_Date As Date
    Private mdtEnd_Date As Date
    Private mdtStart_Time As Date
    Private mdtEnd_Time As Date
    Private mintQualificationgroupunkid As Integer
    Private mintQualificationunkid As Integer
    Private mintTraninginstituteunkid As Integer
    Private mstrTraning_Venue As String = String.Empty
    Private mstrContact_Person As String = String.Empty
    Private mstrContact_No As String = String.Empty
    Private mstrEligibility_Criteria As String = String.Empty
    Private mintResultgroupunkid As Integer
    Private mintPostion_No As Integer
    'S.SANDEEP [ 18 May 2011 ] -- START
    'ISSUE : DOUBLE TO DECIMAL
    'Private mdblFees As Double
    'Private mdblMisc As Double
    'Private mdblTravel As Double
    'Private mdblAccomodation As Double
    'Private mdblAllowance As Double
    'Private mdblMeals As Double
    'Private mdblMaterial As Double
    'Private mdblExam As Double

    Private mdecFees As Decimal
    Private mdecMisc As Decimal
    Private mdecTravel As Decimal
    Private mdecAccomodation As Decimal
    Private mdecAllowance As Decimal
    Private mdecMeals As Decimal
    Private mdecMaterial As Decimal
    Private mdecExam As Decimal
    'S.SANDEEP [ 18 May 2011 ] -- END

    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mblnIscancel As Boolean
    Private mintCancellationuserunkid As Integer
    Private mstrCancellationreason As String = String.Empty
    Private mdtCancellationdate As Date
    Private mstrTraining_Remark As String = String.Empty

    'S.SANDEEP [ 24 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES [<TRAINING>]
    Private msinActual_Fees As Decimal
    Private msinActual_Misc As Decimal
    Private msinActual_Travel As Decimal
    Private msinActual_Accomodation As Decimal
    Private msinActual_Allowance As Decimal
    Private msinActual_Meals As Decimal
    Private msinActual_Material As Decimal
    Private msinActual_Exam As Decimal
    Private mstrFundingids As String = String.Empty
    'S.SANDEEP [ 24 FEB 2012 ] -- END 

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Private mintTrainingAllocationTypeId As Integer = 0
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingschedulingunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Trainingschedulingunkid() As Integer
        Get
            Return mintTrainingschedulingunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingschedulingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set course_title
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Course_Title() As String
        Get
            Return mstrCourse_Title
        End Get
        Set(ByVal value As String)
            mstrCourse_Title = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Start_Date() As Date
        Get
            Return mdtStart_Date
        End Get
        Set(ByVal value As Date)
            mdtStart_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _End_Date() As Date
        Get
            Return mdtEnd_Date
        End Get
        Set(ByVal value As Date)
            mdtEnd_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_time
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Start_Time() As Date
        Get
            Return mdtStart_Time
        End Get
        Set(ByVal value As Date)
            mdtStart_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_time
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _End_Time() As Date
        Get
            Return mdtEnd_Time
        End Get
        Set(ByVal value As Date)
            mdtEnd_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Qualificationgroupunkid() As Integer
        Get
            Return mintQualificationgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Qualificationunkid() As Integer
        Get
            Return mintQualificationunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set traininginstituteunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Traninginstituteunkid() As Integer
        Get
            Return mintTraninginstituteunkid
        End Get
        Set(ByVal value As Integer)
            mintTraninginstituteunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set training_venue
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Traning_Venue() As String
        Get
            Return mstrTraning_Venue
        End Get
        Set(ByVal value As String)
            mstrTraning_Venue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contact_person
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Contact_Person() As String
        Get
            Return mstrContact_Person
        End Get
        Set(ByVal value As String)
            mstrContact_Person = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contact_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Contact_No() As String
        Get
            Return mstrContact_No
        End Get
        Set(ByVal value As String)
            mstrContact_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set eligibility_criteria
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Eligibility_Criteria() As String
        Get
            Return mstrEligibility_Criteria
        End Get
        Set(ByVal value As String)
            mstrEligibility_Criteria = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Resultgroupunkid() As Integer
        Get
            Return mintResultgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintResultgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set position_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Postion_No() As Integer
        Get
            Return mintPostion_No
        End Get
        Set(ByVal value As Integer)
            mintPostion_No = value
        End Set
    End Property

    'S.SANDEEP [ 18 May 2011 ] -- START
    'ISSUE : DOUBLE TO DECIMAL
    '''' <summary>
    '''' Purpose: Get or Set fees
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    'Public Property _Fees() As Double
    '    Get
    '        Return mdblFees
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblFees = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set misc
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    'Public Property _Misc() As Double
    '    Get
    '        Return mdblMisc
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblMisc = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set travel
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    'Public Property _Travel() As Double
    '    Get
    '        Return mdblTravel
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblTravel = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set accomodation
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    'Public Property _Accomodation() As Double
    '    Get
    '        Return mdblAccomodation
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblAccomodation = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set allowance
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    'Public Property _Allowance() As Double
    '    Get
    '        Return mdblAllowance
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblAllowance = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set meals
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    'Public Property _Meals() As Double
    '    Get
    '        Return mdblMeals
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblMeals = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set material
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    'Public Property _Material() As Double
    '    Get
    '        Return mdblMaterial
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblMaterial = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set exam
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    'Public Property _Exam() As Double
    '    Get
    '        Return mdblExam
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblExam = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set fees
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fees() As Decimal
        Get
            Return mdecFees
        End Get
        Set(ByVal value As Decimal)
            mdecFees = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set misc
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Misc() As Decimal
        Get
            Return mdecMisc
        End Get
        Set(ByVal value As Decimal)
            mdecMisc = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set travel
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Travel() As Decimal
        Get
            Return mdecTravel
        End Get
        Set(ByVal value As Decimal)
            mdecTravel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accomodation
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Accomodation() As Decimal
        Get
            Return mdecAccomodation
        End Get
        Set(ByVal value As Decimal)
            mdecAccomodation = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allowance
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Allowance() As Decimal
        Get
            Return mdecAllowance
        End Get
        Set(ByVal value As Decimal)
            mdecAllowance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set meals
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Meals() As Decimal
        Get
            Return mdecMeals
        End Get
        Set(ByVal value As Decimal)
            mdecMeals = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set material
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Material() As Decimal
        Get
            Return mdecMaterial
        End Get
        Set(ByVal value As Decimal)
            mdecMaterial = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exam
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Exam() As Decimal
        Get
            Return mdecExam
        End Get
        Set(ByVal value As Decimal)
            mdecExam = value
        End Set
    End Property
    'S.SANDEEP [ 18 May 2011 ] -- END


    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscancel
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iscancel() As Boolean
        Get
            Return mblnIscancel
        End Get
        Set(ByVal value As Boolean)
            mblnIscancel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancellationuserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Cancellationuserunkid() As Integer
        Get
            Return mintCancellationuserunkid
        End Get
        Set(ByVal value As Integer)
            mintCancellationuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancellationreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Cancellationreason() As String
        Get
            Return mstrCancellationreason
        End Get
        Set(ByVal value As String)
            mstrCancellationreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancellationdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Cancellationdate() As Date
        Get
            Return mdtCancellationdate
        End Get
        Set(ByVal value As Date)
            mdtCancellationdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set training_remark
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Training_Remark() As String
        Get
            Return mstrTraining_Remark
        End Get
        Set(ByVal value As String)
            mstrTraining_Remark = value
        End Set
    End Property

    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set actual_fees
    ''' Modify By: Sandeep J. sharma
    ''' </summary>
    Public Property _Actual_Fees() As Decimal
        Get
            Return msinActual_Fees
        End Get
        Set(ByVal value As Decimal)
            msinActual_Fees = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actual_misc
    ''' Modify By: Sandeep J. sharma
    ''' </summary>
    Public Property _Actual_Misc() As Decimal
        Get
            Return msinActual_Misc
        End Get
        Set(ByVal value As Decimal)
            msinActual_Misc = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actual_travel
    ''' Modify By: Sandeep J. sharma
    ''' </summary>
    Public Property _Actual_Travel() As Decimal
        Get
            Return msinActual_Travel
        End Get
        Set(ByVal value As Decimal)
            msinActual_Travel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actual_accomodation
    ''' Modify By: Sandeep J. sharma
    ''' </summary>
    Public Property _Actual_Accomodation() As Decimal
        Get
            Return msinActual_Accomodation
        End Get
        Set(ByVal value As Decimal)
            msinActual_Accomodation = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actual_allowance
    ''' Modify By: Sandeep J. sharma
    ''' </summary>
    Public Property _Actual_Allowance() As Decimal
        Get
            Return msinActual_Allowance
        End Get
        Set(ByVal value As Decimal)
            msinActual_Allowance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actual_meals
    ''' Modify By: Sandeep J. sharma
    ''' </summary>
    Public Property _Actual_Meals() As Decimal
        Get
            Return msinActual_Meals
        End Get
        Set(ByVal value As Decimal)
            msinActual_Meals = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actual_material
    ''' Modify By: Sandeep J. sharma
    ''' </summary>
    Public Property _Actual_Material() As Decimal
        Get
            Return msinActual_Material
        End Get
        Set(ByVal value As Decimal)
            msinActual_Material = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actual_exam
    ''' Modify By: Sandeep J. sharma
    ''' </summary>
    Public Property _Actual_Exam() As Decimal
        Get
            Return msinActual_Exam
        End Get
        Set(ByVal value As Decimal)
            msinActual_Exam = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundingids
    ''' Modify By: Sandeep J. sharma
    ''' </summary>
    Public Property _Fundingids() As String
        Get
            Return mstrFundingids
        End Get
        Set(ByVal value As String)
            mstrFundingids = value
        End Set
    End Property
    'S.SANDEEP [ 18 FEB 2012 ] -- END

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Public Property _TrainingAllocationTypeId() As Integer
        Get
            Return mintTrainingAllocationTypeId
        End Get
        Set(ByVal value As Integer)
            mintTrainingAllocationTypeId = value
        End Set
    End Property
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  trainingschedulingunkid " & _
                      ", course_title " & _
                      ", yearunkid " & _
                      ", start_date " & _
                      ", end_date " & _
                      ", start_time " & _
                      ", end_time " & _
                      ", qualificationgroupunkid " & _
                      ", qualificationunkid " & _
                      ", traininginstituteunkid " & _
                      ", training_venue " & _
                      ", contact_person " & _
                      ", contact_no " & _
                      ", eligibility_criteria " & _
                      ", resultgroupunkid " & _
                      ", position_no " & _
                      ", fees " & _
                      ", misc " & _
                      ", travel " & _
                      ", accomodation " & _
                      ", allowance " & _
                      ", meals " & _
                      ", material " & _
                      ", exam " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", iscancel " & _
                      ", cancellationuserunkid " & _
                      ", cancellationreason " & _
                      ", cancellationdate " & _
                      ", training_remark " & _
                      ", actual_fees " & _
                      ", actual_misc " & _
                      ", actual_travel " & _
                      ", actual_accomodation " & _
                      ", actual_allowance " & _
                      ", actual_meals " & _
                      ", actual_material " & _
                      ", actual_exam " & _
                      ", fundingids " & _
                      ", ISNULL(allocationtypeunkid,0) AS allocationtypeunkid " & _
                     "FROM hrtraining_scheduling " & _
                     "WHERE trainingschedulingunkid = @trainingschedulingunkid "
            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            'allocationtypeunkid ---- ADDED
            'S.SANDEEP [08-FEB-2017] -- END

            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingschedulingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTrainingschedulingunkid = CInt(dtRow.Item("trainingschedulingunkid"))
                mstrCourse_Title = dtRow.Item("course_title").ToString
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mdtStart_Date = dtRow.Item("start_date")
                mdtEnd_Date = dtRow.Item("end_date")
                mdtStart_Time = dtRow.Item("start_time")
                mdtEnd_Time = dtRow.Item("end_time")
                mintQualificationgroupunkid = CInt(dtRow.Item("qualificationgroupunkid"))
                mintQualificationunkid = CInt(dtRow.Item("qualificationunkid"))
                mintTraninginstituteunkid = CInt(dtRow.Item("traininginstituteunkid"))
                mstrTraning_Venue = dtRow.Item("training_venue").ToString
                mstrContact_Person = dtRow.Item("contact_person").ToString
                mstrContact_No = dtRow.Item("contact_no").ToString
                mstrEligibility_Criteria = dtRow.Item("eligibility_criteria").ToString
                mstrTraining_Remark = dtRow.Item("training_remark").ToString
                mintResultgroupunkid = CInt(dtRow.Item("resultgroupunkid"))
                mintPostion_No = CInt(dtRow.Item("position_no"))

                'S.SANDEEP [ 18 May 2011 ] -- START
                'ISSUE : DOUBLE TO DECIMAL
                'mdblFees = CDbl(dtRow.Item("fees"))
                'mdblMisc = CDbl(dtRow.Item("misc"))
                'mdblTravel = CDbl(dtRow.Item("travel"))
                'mdblAccomodation = CDbl(dtRow.Item("accomodation"))
                'mdblAllowance = CDbl(dtRow.Item("allowance"))
                'mdblMeals = CDbl(dtRow.Item("meals"))
                'mdblMaterial = CDbl(dtRow.Item("material"))
                'mdblExam = CDbl(dtRow.Item("exam"))

                mdecFees = CDec(dtRow.Item("fees"))
                mdecMisc = CDec(dtRow.Item("misc"))
                mdecTravel = CDec(dtRow.Item("travel"))
                mdecAccomodation = CDec(dtRow.Item("accomodation"))
                mdecAllowance = CDec(dtRow.Item("allowance"))
                mdecMeals = CDec(dtRow.Item("meals"))
                mdecMaterial = CDec(dtRow.Item("material"))
                mdecExam = CDec(dtRow.Item("exam"))
                'S.SANDEEP [ 18 May 2011 ] -- END 

                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIscancel = CBool(dtRow.Item("iscancel"))
                mintCancellationuserunkid = CInt(dtRow.Item("cancellationuserunkid"))
                mstrCancellationreason = dtRow.Item("cancellationreason").ToString
                If IsDBNull(dtRow.Item("cancellationdate")) Then
                    mdtCancellationdate = Nothing
                Else
                    mdtCancellationdate = dtRow.Item("cancellationdate")
                End If

                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                msinActual_Fees = dtRow.Item("actual_fees")
                msinActual_Misc = dtRow.Item("actual_misc")
                msinActual_Travel = dtRow.Item("actual_travel")
                msinActual_Accomodation = dtRow.Item("actual_accomodation")
                msinActual_Allowance = dtRow.Item("actual_allowance")
                msinActual_Meals = dtRow.Item("actual_meals")
                msinActual_Material = dtRow.Item("actual_material")
                msinActual_Exam = dtRow.Item("actual_exam")
                mstrFundingids = dtRow.Item("fundingids").ToString
                'S.SANDEEP [ 18 FEB 2012 ] -- END
                'S.SANDEEP [08-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                mintTrainingAllocationTypeId = dtRow.Item("allocationtypeunkid")
                'S.SANDEEP [08-FEB-2017] -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'strQ = "SELECT " & _
            '         " hrtraining_scheduling.trainingschedulingunkid " & _
            '         ",hrtraining_scheduling.course_title " & _
            '         ",hrtraining_scheduling.yearunkid " & _
            '         ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) AS StDate " & _
            '         ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) AS EdDate " & _
            '         ",CONVERT(CHAR(8),hrtraining_scheduling.start_time,108) AS StTime " & _
            '         ",CONVERT(CHAR(8),hrtraining_scheduling.end_time,108)AS EdTime " & _
            '         ",hrtraining_scheduling.qualificationgroupunkid " & _
            '         ",hrtraining_scheduling.qualificationunkid " & _
            '         ",hrtraining_scheduling.traininginstituteunkid " & _
            '         ",hrtraining_scheduling.training_venue " & _
            '         ",hrtraining_scheduling.contact_person " & _
            '         ",hrtraining_scheduling.contact_no " & _
            '         ",hrtraining_scheduling.eligibility_criteria " & _
            '         ",hrtraining_scheduling.training_remark " & _
            '         ",hrtraining_scheduling.resultgroupunkid " & _
            '         ",hrtraining_scheduling.position_no " & _
            '         ",hrtraining_scheduling.fees " & _
            '         ",hrtraining_scheduling.misc " & _
            '         ",hrtraining_scheduling.travel " & _
            '         ",hrtraining_scheduling.accomodation " & _
            '         ",hrtraining_scheduling.allowance " & _
            '         ",hrtraining_scheduling.meals " & _
            '         ",hrtraining_scheduling.material " & _
            '         ",hrtraining_scheduling.exam " & _
            '         ",hrtraining_scheduling.userunkid " & _
            '         ",hrtraining_scheduling.isvoid " & _
            '         ",hrtraining_scheduling.voiddatetime " & _
            '         ",hrtraining_scheduling.voiduserunkid " & _
            '         ",hrtraining_scheduling.voidreason " & _
            '         ",hrtraining_scheduling.iscancel " & _
            '         ",hrtraining_scheduling.cancellationuserunkid " & _
            '         ",hrtraining_scheduling.cancellationreason " & _
            '         ",hrtraining_scheduling.cancellationdate " & _
            '         ",ISNULL(cfcommon_master.name,'') AS Course " & _
            '         ",ISNULL(hrinstitute_master.institute_name,'') AS Institute " & _
            '         ",ISNULL(fees,0)+ISNULL(misc,0)+ISNULL(travel,0)+ISNULL(accomodation,0)+ " & _
            '          "ISNULL(allowance,0)+ISNULL(meals,0)+ISNULL(material,0) AS TotalCost " & _
            '    "FROM hrtraining_scheduling " & _
            '         "LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid " & _
            '         "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.qualificationgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP

            strQ = "SELECT " & _
                     " hrtraining_scheduling.trainingschedulingunkid "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '",hrtraining_scheduling.course_title " & _
            strQ &= ",cm.name as course_title "
            'Anjan (10 Feb 2012)-End 



            strQ &= ",hrtraining_scheduling.yearunkid " & _
                     ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) AS StDate " & _
                     ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) AS EdDate " & _
                     ",CONVERT(CHAR(8),hrtraining_scheduling.start_time,108) AS StTime " & _
                     ",CONVERT(CHAR(8),hrtraining_scheduling.end_time,108)AS EdTime " & _
                     ",hrtraining_scheduling.qualificationgroupunkid " & _
                     ",hrtraining_scheduling.qualificationunkid " & _
                     ",hrtraining_scheduling.traininginstituteunkid " & _
                     ",hrtraining_scheduling.training_venue " & _
                     ",hrtraining_scheduling.contact_person " & _
                     ",hrtraining_scheduling.contact_no " & _
                     ",hrtraining_scheduling.eligibility_criteria " & _
                     ",hrtraining_scheduling.training_remark " & _
                     ",hrtraining_scheduling.resultgroupunkid " & _
                     ",hrtraining_scheduling.position_no " & _
                     ",hrtraining_scheduling.fees " & _
                     ",hrtraining_scheduling.misc " & _
                     ",hrtraining_scheduling.travel " & _
                     ",hrtraining_scheduling.accomodation " & _
                     ",hrtraining_scheduling.allowance " & _
                     ",hrtraining_scheduling.meals " & _
                     ",hrtraining_scheduling.material " & _
                     ",hrtraining_scheduling.exam " & _
                     ",hrtraining_scheduling.userunkid " & _
                     ",hrtraining_scheduling.isvoid " & _
                     ",hrtraining_scheduling.voiddatetime " & _
                     ",hrtraining_scheduling.voiduserunkid " & _
                     ",hrtraining_scheduling.voidreason " & _
                     ",hrtraining_scheduling.iscancel " & _
                     ",hrtraining_scheduling.cancellationuserunkid " & _
                     ",hrtraining_scheduling.cancellationreason " & _
                     ",hrtraining_scheduling.cancellationdate " & _
                     ",hrtraining_scheduling.actual_fees " & _
                     ",hrtraining_scheduling.actual_misc " & _
                     ",hrtraining_scheduling.actual_travel " & _
                     ",hrtraining_scheduling.actual_accomodation " & _
                     ",hrtraining_scheduling.actual_allowance " & _
                     ",hrtraining_scheduling.actual_meals " & _
                     ",hrtraining_scheduling.actual_material " & _
                     ",hrtraining_scheduling.actual_exam " & _
                     ",hrtraining_scheduling.fundingids " & _
                     ",ISNULL(cfcommon_master.name,'') AS Course " & _
                     ",ISNULL(CM.masterunkid,0) AS CourseId " & _
                     ",ISNULL(hrinstitute_master.institute_name,'') AS Institute " & _
                     ",ISNULL(fees,0)+ISNULL(misc,0)+ISNULL(travel,0)+ISNULL(accomodation,0)+ " & _
                      "ISNULL(allowance,0)+ISNULL(meals,0)+ISNULL(material,0)+ISNULL(exam, 0) AS TotalCost " & _
                     ",hrtraining_scheduling.allocationtypeunkid " & _
                     ",'' AS allocation_used " & _
                     ",'' AS allocationids " & _
                "FROM hrtraining_scheduling " & _
                     "LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid " & _
                     "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.qualificationgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP
            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            '",hrtraining_scheduling.allocationtypeunkid " & _ --------- ADDED
            '",'' AS allocation_used " & _ -------------- ADDED
            '",'' AS allocationids " & _ -------- ADDED
            'S.SANDEEP [08-FEB-2017] -- END


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= "LEFT JOIN cfcommon_master as CM ON CM.masterunkid = hrtraining_scheduling.course_title AND CM.mastertype = " & enCommonMaster.TRAINING_COURSEMASTER
            'Anjan (10 Feb 2012)-End 

            'S.SANDEEP [ 18 May 2011 ] -- END 

            dsList = objDataOperation.ExecQuery(strQ, strTableName)


            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            Dim objTrainingAlloc As New clsTraining_Allocation_Tran
            Dim iOwnerGroupName As String = String.Empty
            For Each dRow As DataRow In dsList.Tables(0).Rows
                iOwnerGroupName = ""
                Select Case CInt(dRow.Item("allocationtypeunkid"))
                    Case enAllocation.BRANCH
                        iOwnerGroupName = Language.getMessage("clsMasterData", 430, "Branch")
                    Case enAllocation.DEPARTMENT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 429, "Department Group")
                    Case enAllocation.DEPARTMENT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 428, "Department")
                    Case enAllocation.SECTION_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 427, "Section Group")
                    Case enAllocation.SECTION
                        iOwnerGroupName = Language.getMessage("clsMasterData", 426, "Section")
                    Case enAllocation.UNIT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 425, "Unit Group")
                    Case enAllocation.UNIT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 424, "Unit")
                    Case enAllocation.TEAM
                        iOwnerGroupName = Language.getMessage("clsMasterData", 423, "Team")
                    Case enAllocation.JOB_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 422, "Job Group")
                    Case enAllocation.JOBS
                        iOwnerGroupName = Language.getMessage("clsMasterData", 421, "Jobs")
                    Case enAllocation.CLASS_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 420, "Class Group")
                    Case enAllocation.CLASSES
                        iOwnerGroupName = Language.getMessage("clsMasterData", 419, "Classes")
                    Case enAllocation.COST_CENTER
                        iOwnerGroupName = Language.getMessage("clsMasterData", 586, "Cost Center")
                End Select

                dRow.Item("allocation_used") = IIf(iOwnerGroupName.Trim.Length > 0, iOwnerGroupName & " -> ", "") & objTrainingAlloc.GetCSV_AllocationName(dRow.Item("allocationtypeunkid"), dRow.Item("trainingschedulingunkid"))
                dRow.Item("allocationids") = objTrainingAlloc.GetCSV_AllocationIds(dRow.Item("allocationtypeunkid"), dRow.Item("trainingschedulingunkid"))
                dRow.Item("allocation_used") = dRow.Item("allocation_used").ToString.Replace("&lt;", "<")
                dRow.Item("allocation_used") = dRow.Item("allocation_used").ToString.Replace("&gt;", ">")
                dRow.Item("allocation_used") = dRow.Item("allocation_used").ToString.Replace("&amp;", "&")
            Next
            'S.SANDEEP [08-FEB-2017] -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_scheduling) </purpose>
    Public Function Insert(Optional ByVal dtResources As DataTable = Nothing, Optional ByVal dtTrainers As DataTable = Nothing, Optional ByVal dtAllcation As DataTable = Nothing) As Boolean 'S.SANDEEP [08-FEB-2017] -- START {dtAllcation}-- END
        If isExist(CInt(mstrCourse_Title), mdtStart_Date.Date, mdtEnd_Date.Date, mintTraninginstituteunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Course Title is already defined. Please define new Course Title.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@course_title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCourse_Title.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            objDataOperation.AddParameter("@start_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Time)
            objDataOperation.AddParameter("@end_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Time)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationgroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
            objDataOperation.AddParameter("@traininginstituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTraninginstituteunkid.ToString)
            objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTraning_Venue.ToString)
            objDataOperation.AddParameter("@contact_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_Person.ToString)
            objDataOperation.AddParameter("@contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_No.ToString)
            objDataOperation.AddParameter("@eligibility_criteria", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEligibility_Criteria.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)
            objDataOperation.AddParameter("@training_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTraining_Remark.ToString)
            objDataOperation.AddParameter("@position_no", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostion_No.ToString)

            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'objDataOperation.AddParameter("@fees", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblFees.ToString)
            'objDataOperation.AddParameter("@misc", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblMisc.ToString)
            'objDataOperation.AddParameter("@travel", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblTravel.ToString)
            'objDataOperation.AddParameter("@accomodation", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblAccomodation.ToString)
            'objDataOperation.AddParameter("@allowance", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblAllowance.ToString)
            'objDataOperation.AddParameter("@meals", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblMeals.ToString)
            'objDataOperation.AddParameter("@material", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblMaterial.ToString)
            'objDataOperation.AddParameter("@exam", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblExam.ToString)

            objDataOperation.AddParameter("@fees", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFees.ToString)
            objDataOperation.AddParameter("@misc", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMisc.ToString)
            objDataOperation.AddParameter("@travel", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTravel.ToString)
            objDataOperation.AddParameter("@accomodation", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAccomodation.ToString)
            objDataOperation.AddParameter("@allowance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAllowance.ToString)
            objDataOperation.AddParameter("@meals", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMeals.ToString)
            objDataOperation.AddParameter("@material", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaterial.ToString)
            objDataOperation.AddParameter("@exam", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExam.ToString)
            'S.SANDEEP [ 18 May 2011 ] -- END 

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancellationuserunkid.ToString)
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancellationreason.ToString)
            If mdtCancellationdate = Nothing Then
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancellationdate)
            End If

            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@actual_fees", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Fees.ToString)
            objDataOperation.AddParameter("@actual_misc", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Misc.ToString)
            objDataOperation.AddParameter("@actual_travel", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Travel.ToString)
            objDataOperation.AddParameter("@actual_accomodation", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Accomodation.ToString)
            objDataOperation.AddParameter("@actual_allowance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Allowance.ToString)
            objDataOperation.AddParameter("@actual_meals", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Meals.ToString)
            objDataOperation.AddParameter("@actual_material", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Material.ToString)
            objDataOperation.AddParameter("@actual_exam", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Exam.ToString)
            objDataOperation.AddParameter("@fundingids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundingids.ToString)
            'S.SANDEEP [ 18 FEB 2012 ] -- END
            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            objDataOperation.AddParameter("@allocationtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingAllocationTypeId)
            'S.SANDEEP [08-FEB-2017] -- END


            strQ = "INSERT INTO hrtraining_scheduling ( " & _
                      "  course_title " & _
                      ", yearunkid " & _
                      ", start_date " & _
                      ", end_date " & _
                      ", start_time " & _
                      ", end_time " & _
                      ", qualificationgroupunkid " & _
                      ", qualificationunkid " & _
                      ", traininginstituteunkid " & _
                      ", training_venue " & _
                      ", contact_person " & _
                      ", contact_no " & _
                      ", eligibility_criteria " & _
                      ", resultgroupunkid " & _
                      ", training_remark " & _
                      ", position_no " & _
                      ", fees " & _
                      ", misc " & _
                      ", travel " & _
                      ", accomodation " & _
                      ", allowance " & _
                      ", meals " & _
                      ", material " & _
                      ", exam " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", iscancel " & _
                      ", cancellationuserunkid " & _
                      ", cancellationreason " & _
                      ", cancellationdate" & _
                      ", actual_fees " & _
                      ", actual_misc " & _
                      ", actual_travel " & _
                      ", actual_accomodation " & _
                      ", actual_allowance " & _
                      ", actual_meals " & _
                      ", actual_material " & _
                      ", actual_exam " & _
                      ", fundingids" & _
                      ", allocationtypeunkid " & _
                    ") VALUES (" & _
                      "  @course_title " & _
                      ", @yearunkid " & _
                      ", @start_date " & _
                      ", @end_date " & _
                      ", @start_time " & _
                      ", @end_time " & _
                      ", @qualificationgroupunkid " & _
                      ", @qualificationunkid " & _
                      ", @traininginstituteunkid " & _
                      ", @training_venue " & _
                      ", @contact_person " & _
                      ", @contact_no " & _
                      ", @eligibility_criteria " & _
                      ", @resultgroupunkid " & _
                      ", @training_remark " & _
                      ", @position_no " & _
                      ", @fees " & _
                      ", @misc " & _
                      ", @travel " & _
                      ", @accomodation " & _
                      ", @allowance " & _
                      ", @meals " & _
                      ", @material " & _
                      ", @exam " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason " & _
                      ", @iscancel " & _
                      ", @cancellationuserunkid " & _
                      ", @cancellationreason " & _
                      ", @cancellationdate" & _
                      ", @actual_fees " & _
                      ", @actual_misc " & _
                      ", @actual_travel " & _
                      ", @actual_accomodation " & _
                      ", @actual_allowance " & _
                      ", @actual_meals " & _
                      ", @actual_material " & _
                      ", @actual_exam " & _
                      ", @fundingids" & _
                      ", @allocationtypeunkid " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingschedulingunkid = dsList.Tables(0).Rows(0).Item(0)

            Dim blnFlag As Boolean = False


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'If dtResources IsNot Nothing Then
            '    objResourcesTran._Training_SchedulingUnkid = mintTrainingschedulingunkid
            '    objResourcesTran._DataTable = dtResources
            '    blnFlag = objResourcesTran.InsertUpdateDelete_Resources
            '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            'End If
            'If dtTrainers IsNot Nothing Then
            '    blnFlag = False
            '    objTrainresTran._Training_SchedulingUnkid = mintTrainingschedulingunkid
            '    objTrainresTran._DataTable = dtTrainers
            '    blnFlag = objTrainresTran.InsertUpdateDelete_Trainers
            '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            'End If

            Dim blnToInsert As Boolean = False

            If dtResources IsNot Nothing Then
                If dtResources.Rows.Count > 0 Then
                    objResourcesTran._Training_SchedulingUnkid = mintTrainingschedulingunkid
                    objResourcesTran._DataTable = dtResources
                    If objResourcesTran.InsertUpdateDelete_Resources = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrtraining_scheduling", mintTrainingschedulingunkid, "trainingschedulingunkid", 2) Then
                        blnToInsert = True
                    End If
                End If
            End If

            If dtTrainers IsNot Nothing Then
                If dtTrainers.Rows.Count > 0 Then
                    objTrainresTran._Training_SchedulingUnkid = mintTrainingschedulingunkid
                    objTrainresTran._DataTable = dtTrainers
                    If objTrainresTran.InsertUpdateDelete_Trainers() = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    blnToInsert = False
                Else
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrtraining_scheduling", mintTrainingschedulingunkid, "trainingschedulingunkid", 2) Then
                        blnToInsert = True
                    End If
                End If
            End If

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            If dtAllcation IsNot Nothing Then
                If dtAllcation.Rows.Count > 0 Then
                    objTrainingAlloc._ObjDataOperation = objDataOperation
                    objTrainingAlloc._Training_SchedulingUnkid = mintTrainingschedulingunkid
                    objTrainingAlloc._DataTable = dtAllcation
                    If objTrainingAlloc.InsertUpdateDelete_Allocations() = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    blnToInsert = False
                Else
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrtraining_scheduling", mintTrainingschedulingunkid, "trainingschedulingunkid", 2) Then
                        blnToInsert = True
                    End If
                End If
            End If
            'S.SANDEEP [08-FEB-2017] -- END

            If blnToInsert = True Then
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", mintTrainingschedulingunkid, "hrtraining_resources_tran", "resourcestranunkid", -1, 1, 0) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_scheduling) </purpose>
    Public Function Update(Optional ByVal dtResources As DataTable = Nothing, Optional ByVal dtTrainers As DataTable = Nothing, Optional ByVal dtAllcation As DataTable = Nothing) As Boolean 'S.SANDEEP [08-FEB-2017] -- START {dtAllcation}-- END
        If isExist(CInt(mstrCourse_Title), mdtStart_Date.Date, mdtEnd_Date.Date, mintTraninginstituteunkid, mintTrainingschedulingunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Course Title is already defined. Please define new Course Title.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingschedulingunkid.ToString)
            objDataOperation.AddParameter("@course_title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCourse_Title.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            objDataOperation.AddParameter("@start_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Time)
            objDataOperation.AddParameter("@end_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Time)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationgroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
            objDataOperation.AddParameter("@traininginstituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTraninginstituteunkid.ToString)
            objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTraning_Venue.ToString)
            objDataOperation.AddParameter("@contact_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_Person.ToString)
            objDataOperation.AddParameter("@contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_No.ToString)
            objDataOperation.AddParameter("@eligibility_criteria", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEligibility_Criteria.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)
            objDataOperation.AddParameter("@training_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTraining_Remark.ToString)
            objDataOperation.AddParameter("@position_no", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostion_No.ToString)

            'S.SANDEEP [ 18 May 2011 ] -- START
            'ISSUE : DOUBLE TO DECIMAL
            'objDataOperation.AddParameter("@fees", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblFees.ToString)
            'objDataOperation.AddParameter("@misc", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblMisc.ToString)
            'objDataOperation.AddParameter("@travel", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblTravel.ToString)
            'objDataOperation.AddParameter("@accomodation", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblAccomodation.ToString)
            'objDataOperation.AddParameter("@allowance", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblAllowance.ToString)
            'objDataOperation.AddParameter("@meals", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblMeals.ToString)
            'objDataOperation.AddParameter("@material", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblMaterial.ToString)
            'objDataOperation.AddParameter("@exam", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblExam.ToString)

            objDataOperation.AddParameter("@fees", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFees.ToString)
            objDataOperation.AddParameter("@misc", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMisc.ToString)
            objDataOperation.AddParameter("@travel", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTravel.ToString)
            objDataOperation.AddParameter("@accomodation", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAccomodation.ToString)
            objDataOperation.AddParameter("@allowance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAllowance.ToString)
            objDataOperation.AddParameter("@meals", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMeals.ToString)
            objDataOperation.AddParameter("@material", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaterial.ToString)
            objDataOperation.AddParameter("@exam", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExam.ToString)
            'S.SANDEEP [ 18 May 2011 ] -- END

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancellationuserunkid.ToString)
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancellationreason.ToString)
            If mdtCancellationdate = Nothing Then
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancellationdate)
            End If

            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@actual_fees", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Fees.ToString)
            objDataOperation.AddParameter("@actual_misc", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Misc.ToString)
            objDataOperation.AddParameter("@actual_travel", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Travel.ToString)
            objDataOperation.AddParameter("@actual_accomodation", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Accomodation.ToString)
            objDataOperation.AddParameter("@actual_allowance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Allowance.ToString)
            objDataOperation.AddParameter("@actual_meals", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Meals.ToString)
            objDataOperation.AddParameter("@actual_material", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Material.ToString)
            objDataOperation.AddParameter("@actual_exam", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinActual_Exam.ToString)
            objDataOperation.AddParameter("@fundingids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundingids.ToString)
            'S.SANDEEP [ 18 FEB 2012 ] -- END

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            objDataOperation.AddParameter("@allocationtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingAllocationTypeId)
            'S.SANDEEP [08-FEB-2017] -- END

            strQ = "UPDATE hrtraining_scheduling SET " & _
                      "  course_title = @course_title" & _
                      ", yearunkid = @yearunkid" & _
                      ", start_date = @start_date" & _
                      ", end_date = @end_date" & _
                      ", start_time = @start_time" & _
                      ", end_time = @end_time" & _
                      ", qualificationgroupunkid = @qualificationgroupunkid" & _
                      ", qualificationunkid = @qualificationunkid" & _
                      ", traininginstituteunkid = @traininginstituteunkid" & _
                      ", training_venue = @training_venue" & _
                      ", contact_person = @contact_person" & _
                      ", contact_no = @contact_no" & _
                      ", eligibility_criteria = @eligibility_criteria" & _
                      ", resultgroupunkid = @resultgroupunkid" & _
                      ", training_remark = @training_remark" & _
                      ", position_no = @position_no" & _
                      ", fees = @fees" & _
                      ", misc = @misc" & _
                      ", travel = @travel" & _
                      ", accomodation = @accomodation" & _
                      ", allowance = @allowance" & _
                      ", meals = @meals" & _
                      ", material = @material" & _
                      ", exam = @exam" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason" & _
                      ", iscancel = @iscancel" & _
                      ", cancellationuserunkid = @cancellationuserunkid" & _
                      ", cancellationreason = @cancellationreason" & _
                      ", cancellationdate = @cancellationdate " & _
                      ", actual_fees = @actual_fees" & _
                      ", actual_misc = @actual_misc" & _
                      ", actual_travel = @actual_travel" & _
                      ", actual_accomodation = @actual_accomodation" & _
                      ", actual_allowance = @actual_allowance" & _
                      ", actual_meals = @actual_meals" & _
                      ", actual_material = @actual_material" & _
                      ", actual_exam = @actual_exam" & _
                      ", fundingids = @fundingids " & _
                      ", allocationtypeunkid = @allocationtypeunkid " & _
                    "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean = False


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'If dtResources IsNot Nothing Then
            '    objResourcesTran._Training_SchedulingUnkid = mintTrainingschedulingunkid
            '    objResourcesTran._DataTable = dtResources
            '    blnFlag = objResourcesTran.InsertUpdateDelete_Resources
            '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            'End If

            'If dtTrainers IsNot Nothing Then
            '    blnFlag = False
            '    objTrainresTran._Training_SchedulingUnkid = mintTrainingschedulingunkid
            '    objTrainresTran._DataTable = dtTrainers
            '    blnFlag = objTrainresTran.InsertUpdateDelete_Trainers
            '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            'End If

            Dim blnToInsert As Boolean = False

            If dtResources IsNot Nothing Then
                Dim dt() As DataRow = dtResources.Select("AUD = ''")
                If dt.Length = dtResources.Rows.Count Then
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrtraining_scheduling", mintTrainingschedulingunkid, "trainingschedulingunkid", 2) Then
                        blnToInsert = True
                    End If
                Else
                    objResourcesTran._Training_SchedulingUnkid = mintTrainingschedulingunkid
                    objResourcesTran._DataTable = dtResources
                    If objResourcesTran.InsertUpdateDelete_Resources() = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    blnToInsert = False
                End If
            End If

            If dtTrainers IsNot Nothing Then
                Dim dt() As DataRow = dtTrainers.Select("AUD=''")
                If dt.Length = dtTrainers.Rows.Count Then
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrtraining_scheduling", mintTrainingschedulingunkid, "trainingschedulingunkid", 2) Then
                        blnToInsert = True
                    End If
                Else
                    objTrainresTran._Training_SchedulingUnkid = mintTrainingschedulingunkid
                    objTrainresTran._DataTable = dtTrainers
                    If objTrainresTran.InsertUpdateDelete_Trainers = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    blnToInsert = False
                End If
            End If

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            If dtAllcation IsNot Nothing Then
                If dtAllcation.Rows.Count > 0 Then
                    objTrainingAlloc._ObjDataOperation = objDataOperation
                    objTrainingAlloc._Training_SchedulingUnkid = mintTrainingschedulingunkid
                    objTrainingAlloc._DataTable = dtAllcation
                    If objTrainingAlloc.InsertUpdateDelete_Allocations() = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    blnToInsert = False
                Else
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrtraining_scheduling", mintTrainingschedulingunkid, "trainingschedulingunkid", 2) Then
                        blnToInsert = True
                    End If
                End If
            End If
            'S.SANDEEP [08-FEB-2017] -- END

            If blnToInsert = True Then
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", mintTrainingschedulingunkid, "hrtraining_trainers_tran", "trainerstranunkid", -1, 2, 0) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_scheduling) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrtraining_scheduling SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                    "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'Dim blnFlag As Boolean = False
            '''''''''''''''''' Voiding All Resources
            'objResourcesTran._Training_SchedulingUnkid = intUnkid
            'blnFlag = objResourcesTran.Void_All(mblnIsvoid, mdtVoiddatetime, mstrVoidreason, mintVoiduserunkid)
            'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            '''''''''''''''''' Voiding All Trainers
            'objTrainresTran._Training_SchedulingUnkid = intUnkid
            'blnFlag = False
            'blnFlag = objTrainresTran.Void_All(mblnIsvoid, mdtVoiddatetime, mstrVoidreason, mintVoiduserunkid)
            'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            ''''''''''''''''' Voiding All Resources
            Dim dsRTran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_resources_tran", "trainingschedulingunkid", intUnkid)
            For Each dRow As DataRow In dsRTran.Tables(0).Rows
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", intUnkid, "hrtraining_resources_tran", "resourcestranunkid", dRow.Item("resourcestranunkid"), 3, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            objResourcesTran._Training_SchedulingUnkid = intUnkid
            If objResourcesTran.Void_All(mblnIsvoid, mdtVoiddatetime, mstrVoidreason, mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            dsRTran.Dispose()

            ''''''''''''''''' Voiding All Trainers
            Dim dsTTran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_trainers_tran", "trainingschedulingunkid", intUnkid)
            For Each dRow As DataRow In dsTTran.Tables(0).Rows
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", intUnkid, "hrtraining_trainers_tran", "trainerstranunkid", dRow.Item("trainerstranunkid"), 3, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            objTrainresTran._Training_SchedulingUnkid = intUnkid
            If objTrainresTran.Void_All(mblnIsvoid, mdtVoiddatetime, mstrVoidreason, mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            dsTTran.Dispose()
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            ''''''''''''''''' Voiding All Allocation
            Dim dsATran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_allocation_tran", "trainingschedulingunkid", intUnkid)
            For Each dRow As DataRow In dsTTran.Tables(0).Rows
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", intUnkid, "hrtraining_allocation_tran", "allocationtranunkid", dRow.Item("allocationtranunkid"), 3, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            objTrainingAlloc._Training_SchedulingUnkid = intUnkid
            If objTrainingAlloc.Void_All(mblnIsvoid, mdtVoiddatetime, mstrVoidreason, mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            dsATran.Dispose()
            'S.SANDEEP [08-FEB-2017] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "TABLE_NAME AS TableName  " & _
                    ",COLUMN_NAME " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('trainingschedulingunkid') "

            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "hrtraining_scheduling" Then Continue For

                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @trainingschedulingunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If

            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    '''' Public Function isExist(ByVal strTital As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intCourseMasterId As Integer, ByVal StDate As DateTime, ByVal EdDate As DateTime, ByVal intInstituteId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  trainingschedulingunkid " & _
                      ", course_title " & _
                      ", yearunkid " & _
                      ", start_date " & _
                      ", end_date " & _
                      ", start_time " & _
                      ", end_time " & _
                      ", qualificationgroupunkid " & _
                      ", qualificationunkid " & _
                      ", traininginstituteunkid " & _
                      ", training_venue " & _
                      ", contact_person " & _
                      ", contact_no " & _
                      ", eligibility_criteria " & _
                      ", resultgroupunkid " & _
                      ", training_remark " & _
                      ", position_no " & _
                      ", fees " & _
                      ", misc " & _
                      ", travel " & _
                      ", accomodation " & _
                      ", allowance " & _
                      ", meals " & _
                      ", material " & _
                      ", exam " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", iscancel " & _
                      ", cancellationuserunkid " & _
                      ", cancellationreason " & _
                      ", cancellationdate " & _
                    "FROM hrtraining_scheduling " & _
                    "WHERE isvoid = 0 AND iscancel = 0 " & _
                    " AND ISNULL(CAST((CASE WHEN ISNUMERIC(course_title) <> 0 THEN course_title ELSE 0 END)AS Int),0) = @CourseMasterId " & _
                    " AND CONVERT(CHAR(8),start_date,112) = @StDate " & _
                    " AND CONVERT(CHAR(8),end_date,112) = @EdDate "

            If intInstituteId > 0 Then
                strQ &= " AND traininginstituteunkid = @InstituteId "
            End If

            If intUnkid > 0 Then
                strQ &= " AND trainingschedulingunkid <> @trainingschedulingunkid"
            End If

            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@CourseMasterId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseMasterId)
            objDataOperation.AddParameter("@StDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(StDate).ToString)
            objDataOperation.AddParameter("@EdDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(EdDate).ToString)
            objDataOperation.AddParameter("@InstituteId", SqlDbType.Int, eZeeDataType.INT_SIZE, intInstituteId)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_scheduling) </purpose>
    Public Function Cancel(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Dim iCnt As Integer = 0
        Dim blnFlag As Boolean = False
        Try
            mstrMessage = ""

            strQ = "SELECT * FROM hrtraining_enrollment_tran WHERE trainingschedulingunkid = '" & intUnkid & "'"

            iCnt = objDataOperation.RecordCount(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt <= 0 Then
                strQ = "UPDATE hrtraining_scheduling SET " & _
                          "  iscancel = @iscancel" & _
                          ", cancellationuserunkid = @cancellationuserunkid" & _
                          ", cancellationreason = @cancellationreason" & _
                          ", cancellationdate = @cancellationdate " & _
                        "WHERE trainingschedulingunkid = @trainingschedulingunkid "

                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
                objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancellationuserunkid.ToString)
                objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancellationreason.ToString)
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancellationdate)
                objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'Dim blnFlag As Boolean = False
                '''''''''''''''''' Voiding All Resources
                'objResourcesTran._Training_SchedulingUnkid = intUnkid
                'blnFlag = objResourcesTran.Cancel_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid)
                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                '''''''''''''''''' Voiding All Trainers
                'objTrainresTran._Training_SchedulingUnkid = intUnkid
                'blnFlag = False
                'blnFlag = objTrainresTran.Cancel_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid)
                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                ''''''''''''''''' CANCELLING All Resources
                Dim dsRTran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_resources_tran", "trainingschedulingunkid", intUnkid)
                For Each dRow As DataRow In dsRTran.Tables(0).Rows
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", intUnkid, "hrtraining_resources_tran", "resourcestranunkid", dRow.Item("resourcestranunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
                objResourcesTran._Training_SchedulingUnkid = intUnkid
                If objResourcesTran.Cancel_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                dsRTran.Dispose()

                ''''''''''''''''' CANCELLING All Trainers
                Dim dsTTran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_trainers_tran", "trainingschedulingunkid", intUnkid)
                For Each dRow As DataRow In dsTTran.Tables(0).Rows
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", intUnkid, "hrtraining_trainers_tran", "trainerstranunkid", dRow.Item("trainerstranunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                objTrainresTran._Training_SchedulingUnkid = intUnkid
                If objTrainresTran.Cancel_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                'S.SANDEEP [08-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                ''''''''''''''''' Cancel All Allocation
                Dim dsATran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_allocation_tran", "trainingschedulingunkid", intUnkid)
                For Each dRow As DataRow In dsTTran.Tables(0).Rows
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", intUnkid, "hrtraining_allocation_tran", "allocationtranunkid", dRow.Item("allocationtranunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
                objTrainingAlloc._Training_SchedulingUnkid = intUnkid
                If objTrainingAlloc.Cancel_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                dsATran.Dispose()
                'S.SANDEEP [08-FEB-2017] -- END

                blnFlag = True
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry you cannot cancel this training as some of the employees are already enrolled for this training.")
                blnFlag = False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Cancel; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_scheduling) </purpose>
    Public Function MakeActive(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrtraining_scheduling SET " & _
                      "  iscancel = @iscancel" & _
                      ", cancellationuserunkid = @cancellationuserunkid" & _
                      ", cancellationreason = @cancellationreason" & _
                      ", cancellationdate = @cancellationdate " & _
                    "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancellationuserunkid.ToString)
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancellationreason.ToString)
            If mdtCancellationdate = Nothing Then
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancellationdate)
            End If
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'Dim blnFlag As Boolean = False
            '''''''''''''''''' Voiding All Resources
            'objResourcesTran._Training_SchedulingUnkid = intUnkid
            'blnFlag = objResourcesTran.MakeActive_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid)
            'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            '''''''''''''''''' Voiding All Trainers
            'objTrainresTran._Training_SchedulingUnkid = intUnkid
            'blnFlag = False
            'blnFlag = objTrainresTran.MakeActive_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid)
            'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            ''''''''''''''''' MAKING ACTIVE All Resources
            Dim dsRTran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_resources_tran", "trainingschedulingunkid", intUnkid)
            For Each dRow As DataRow In dsRTran.Tables(0).Rows
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", intUnkid, "hrtraining_resources_tran", "resourcestranunkid", dRow.Item("resourcestranunkid"), 2, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            dsRTran.Dispose()

            objResourcesTran._Training_SchedulingUnkid = intUnkid
            If objResourcesTran.MakeActive_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            dsRTran.Dispose()

            ''''''''''''''''' MAKING ACTIVE All Trainers
            Dim dsTTran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_trainers_tran", "trainingschedulingunkid", intUnkid)
            For Each dRow As DataRow In dsTTran.Tables(0).Rows
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", intUnkid, "hrtraining_trainers_tran", "trainerstranunkid", dRow.Item("trainerstranunkid"), 2, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            objTrainresTran._Training_SchedulingUnkid = intUnkid
            If objTrainresTran.MakeActive_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            dsTTran.Dispose()
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            ''''''''''''''''' Cancel All Allocation
            Dim dsATran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_allocation_tran", "trainingschedulingunkid", intUnkid)
            For Each dRow As DataRow In dsTTran.Tables(0).Rows
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", intUnkid, "hrtraining_allocation_tran", "allocationtranunkid", dRow.Item("allocationtranunkid"), 2, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            objTrainingAlloc._Training_SchedulingUnkid = intUnkid
            If objTrainingAlloc.MakeActive_All(mblnIscancel, mdtCancellationdate, mstrCancellationreason, mintCancellationuserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            dsATran.Dispose()
            'S.SANDEEP [08-FEB-2017] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: MakeActive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Dataset</returns>
    ''' <purpose>Get List For Combo</purpose>
    '''Sandeep [ 23 Oct 2010 ] -- Start
    '''Issue : Virtual List Changes
    '''Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal bnlFlag As Boolean = False) As DataSet
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal bnlFlag As Boolean = False, Optional ByVal intYearId As Integer = -1, Optional ByVal blnConcateDates As Boolean = False) As DataSet
        'Sandeep [ 23 Oct 2010 ] -- End 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            If bnlFlag = True Then
                strQ = "SELECT 0 As Id ,@Select As Name,0 as coursetypeid,0 as courseunkid, null as start_date, null as end_date,null as start_time , null as end_time ,0 as position_no ,'' AS eligibility_criteria  UNION "
            End If

            strQ &= "SELECT " & _
                    "  trainingschedulingunkid As Id " & _
                    " ,ISNULL(cfcommon_master.name,'') As Name " & _
                    " ,ISNULL(cfcommon_master.coursetypeid,0) as coursetypeid " & _
                    " ,cfcommon_master.masterunkid AS courseunkid " & _
                    " ,start_date,end_date,start_time,end_time " & _
                    " ,position_no AS position_no " & _
                    " ,eligibility_criteria as eligibility_criteria " & _
                    "FROM hrtraining_scheduling " & _
                    " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title " & _
                    "WHERE ISNULL(isvoid,0) = 0 AND ISNULL(iscancel,0) = 0 "

            If intYearId > 0 Then
                strQ &= " AND yearunkid = @YearId "
                objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If blnConcateDates = True Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If dRow.Item("Id") <= 0 Then Continue For
                    dRow.Item("Name") = dRow.Item("Name").ToString & " ( " & CDate(dRow.Item("start_date")).ToShortDateString & " - " & CDate(dRow.Item("end_date")).ToShortDateString & " ) "
                Next
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "; Procedure Name: getComboList; Module Name:", mstrModuleName)
            Return Nothing
        End Try
    End Function


    'S.SANDEEP [ 01 OCT 2014 ] -- START
    Public Function GetTrainingSchecduleId(ByVal xCourseMasterId As Integer, ByVal xStDate As Date, ByVal xEdDate As Date, ByVal xInstituteId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim xTrainingScheduleTranId As Integer = -1
        Try
            StrQ = "SELECT " & _
                   " @TranId = trainingschedulingunkid " & _
                   "FROM hrtraining_scheduling " & _
                   "WHERE isvoid = 0 AND iscancel = 0 " & _
                   " AND ISNULL(CAST((CASE WHEN ISNUMERIC(course_title) <> 0 THEN course_title ELSE 0 END)AS Int),0) = @CourseMasterId " & _
                   " AND CONVERT(CHAR(8),start_date,112) = @StDate " & _
                   " AND CONVERT(CHAR(8),end_date,112) = @EdDate AND traininginstituteunkid = @InstituteId "

            objDataOperation.AddParameter("@CourseMasterId", SqlDbType.Int, eZeeDataType.INT_SIZE, xCourseMasterId)
            objDataOperation.AddParameter("@StDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(xStDate).ToString)
            objDataOperation.AddParameter("@EdDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(xEdDate).ToString)
            objDataOperation.AddParameter("@InstituteId", SqlDbType.Int, eZeeDataType.INT_SIZE, xInstituteId)
            objDataOperation.AddParameter("@TranId", SqlDbType.Int, eZeeDataType.INT_SIZE, xTrainingScheduleTranId, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xTrainingScheduleTranId = objDataOperation.GetParameterValue("@TranId")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTrainingSchecduleId", mstrModuleName)
        Finally
        End Try
        Return xTrainingScheduleTranId
    End Function
    'S.SANDEEP [ 01 OCT 2014 ] -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage("clsMasterData", 422, "Job Group")
            Language.setMessage("clsMasterData", 423, "Team")
            Language.setMessage("clsMasterData", 424, "Unit")
            Language.setMessage("clsMasterData", 425, "Unit Group")
            Language.setMessage("clsMasterData", 426, "Section")
            Language.setMessage("clsMasterData", 427, "Section Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage("clsMasterData", 429, "Department Group")
            Language.setMessage("clsMasterData", 430, "Branch")
            Language.setMessage("clsMasterData", 586, "Cost Center")
            Language.setMessage(mstrModuleName, 1, "Course Title is already defined. Please define new Course Title.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "Sorry you cannot cancel this training as some of the employees are already enrolled for this training.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class