﻿'************************************************************************************************************************************
'Class Name : clsTraining_Trainers_Tran.vb
'Purpose    :
'Date       :02/08/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsTraining_Trainers_Tran

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Trainers_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintTrainingSchdulingUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mintTrainerUnkid As Integer
    Private mstrOther_Name As String = ""
    Private mstrOther_Company As String = ""
    Private mstrOther_Contact As String = ""
    Private mstrOther_Department As String = ""
    Private mintEmployeeUnkid As Integer = -1
    Private mintTrainerLevelId As Integer = -1
#End Region

#Region " Properties "
    Public Property _Training_SchedulingUnkid() As Integer
        Get
            Return mintTrainingSchdulingUnkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingSchdulingUnkid = value
            Get_Trainers_Tran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _TrainerUnkid() As Integer
        Get
            Return mintTrainerUnkid
        End Get
        Set(ByVal value As Integer)
            mintTrainerUnkid = value
            Call Get_Trainers_Data()
        End Set
    End Property

    Public Property _Other_Name() As String
        Get
            Return mstrOther_Name
        End Get
        Set(ByVal value As String)
            mstrOther_Name = value
        End Set
    End Property

    Public Property _Other_Company() As String
        Get
            Return mstrOther_Company
        End Get
        Set(ByVal value As String)
            mstrOther_Company = value
        End Set
    End Property

    Public Property _Other_Contact() As String
        Get
            Return mstrOther_Contact
        End Get
        Set(ByVal value As String)
            mstrOther_Contact = value
        End Set
    End Property

    Public Property _Other_Department() As String
        Get
            Return mstrOther_Department
        End Get
        Set(ByVal value As String)
            mstrOther_Department = value
        End Set
    End Property

    Public Property _EmployeeId() As Integer
        Get
            Return mintEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public Property _TrainerLevelId() As Integer
        Get
            Return mintTrainerLevelId
        End Get
        Set(ByVal value As Integer)
            mintTrainerLevelId = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("Training_Trainers")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("trainerstranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingschedulingunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("other_name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("other_position")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("other_company")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("other_contactno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("iscancel")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancellationuserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancellationreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancellationdate")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainer_level_id")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Function "
    Private Sub Get_Trainers_Tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            strQ = "SELECT " & _
                    "  trainerstranunkid " & _
                    ", trainingschedulingunkid " & _
                    ", employeeunkid " & _
                    ", other_name " & _
                    ", other_position " & _
                    ", other_company " & _
                    ", other_contactno " & _
                    ", iscancel " & _
                    ", cancellationuserunkid " & _
                    ", cancellationreason " & _
                    ", cancellationdate " & _
                    ", isvoid " & _
                    ", voiddatetime " & _
                    ", voiduserunkid " & _
                    ", voidreason " & _
                    ", '' As AUD " & _
                    ", trainer_level_id " & _
                   "FROM hrtraining_trainers_tran " & _
                   "WHERE trainingschedulingunkid = @trainingschedulingunkid " & _
                   "AND ISNULL(isvoid,0) = 0 " & _
                   "AND ISNULL(iscancel,0) = 0 "

            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow()

                    dRowID_Tran.Item("trainerstranunkid") = .Item("trainerstranunkid")
                    dRowID_Tran.Item("trainingschedulingunkid") = .Item("trainingschedulingunkid")
                    dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
                    dRowID_Tran.Item("other_name") = .Item("other_name")
                    dRowID_Tran.Item("other_position") = .Item("other_position")
                    dRowID_Tran.Item("other_company") = .Item("other_company")
                    dRowID_Tran.Item("other_contactno") = .Item("other_contactno")
                    dRowID_Tran.Item("iscancel") = .Item("iscancel")
                    dRowID_Tran.Item("cancellationuserunkid") = .Item("cancellationuserunkid")
                    dRowID_Tran.Item("cancellationreason") = .Item("cancellationreason")

                    If IsDBNull(.Item("cancellationdate")) Then
                        dRowID_Tran.Item("cancellationdate") = DBNull.Value
                    Else
                        dRowID_Tran.Item("cancellationdate") = .Item("cancellationdate")
                    End If

                    dRowID_Tran.Item("isvoid") = .Item("isvoid")

                    If IsDBNull(.Item("voiddatetime")) Then
                        dRowID_Tran.Item("voiddatetime") = DBNull.Value
                    Else
                        dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    End If
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid")
                    dRowID_Tran.Item("voidreason") = .Item("voidreason")
                    dRowID_Tran.Item("AUD") = .Item("AUD")
                    dRowID_Tran.Item("trainer_level_id") = .Item("trainer_level_id")

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Trainers_Tran", mstrModuleName)
        End Try
    End Sub

    Public Function InsertUpdateDelete_Trainers() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrtraining_trainers_tran ( " & _
                                            "  trainingschedulingunkid " & _
                                            ", employeeunkid " & _
                                            ", other_name " & _
                                            ", other_position " & _
                                            ", other_company " & _
                                            ", other_contactno " & _
                                            ", iscancel " & _
                                            ", cancellationuserunkid " & _
                                            ", cancellationreason " & _
                                            ", cancellationdate " & _
                                            ", isvoid " & _
                                            ", voiddatetime " & _
                                            ", voiduserunkid " & _
                                            ", voidreason" & _
                                            ", trainer_level_id" & _
                                       ") VALUES (" & _
                                            "  @trainingschedulingunkid " & _
                                            ", @employeeunkid " & _
                                            ", @other_name " & _
                                            ", @other_position " & _
                                            ", @other_company " & _
                                            ", @other_contactno " & _
                                            ", @iscancel " & _
                                            ", @cancellationuserunkid " & _
                                            ", @cancellationreason " & _
                                            ", @cancellationdate " & _
                                            ", @isvoid " & _
                                            ", @voiddatetime " & _
                                            ", @voiduserunkid " & _
                                            ", @voidreason" & _
                                            ", @trainer_level_id " & _
                                       "); SELECT @@identity"

                                objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@other_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_name").ToString)
                                objDataOperation.AddParameter("@other_position", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_position").ToString)
                                objDataOperation.AddParameter("@other_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_company").ToString)
                                objDataOperation.AddParameter("@other_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_contactno").ToString)
                                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iscancel").ToString)
                                objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("cancellationuserunkid").ToString)
                                objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("cancellationreason").ToString)
                                If IsDBNull(.Item("cancellationdate")) Then
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("cancellationdate").ToString)
                                End If
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@trainer_level_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainer_level_id").ToString)

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                mintTrainerUnkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("trainingschedulingunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", .Item("trainingschedulingunkid"), "hrtraining_trainers_tran", "trainerstranunkid", mintTrainerUnkid, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", mintTrainingSchdulingUnkid, "hrtraining_trainers_tran", "trainerstranunkid", mintTrainerUnkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "U"
                                strQ = "UPDATE hrtraining_trainers_tran SET " & _
                                        "  trainingschedulingunkid = @trainingschedulingunkid" & _
                                        ", employeeunkid = @employeeunkid" & _
                                        ", other_name = @other_name" & _
                                        ", other_position = @other_position" & _
                                        ", other_company = @other_company" & _
                                        ", other_contactno = @other_contactno" & _
                                        ", iscancel = @iscancel" & _
                                        ", cancellationuserunkid = @cancellationuserunkid" & _
                                        ", cancellationreason = @cancellationreason" & _
                                        ", cancellationdate = @cancellationdate" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voidreason = @voidreason " & _
                                        ", trainer_level_id = @trainer_level_id " & _
                                      "WHERE trainerstranunkid = @trainerstranunkid "

                                objDataOperation.AddParameter("@trainerstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainerstranunkid").ToString)
                                objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingschedulingunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@other_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_name").ToString)
                                objDataOperation.AddParameter("@other_position", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_position").ToString)
                                objDataOperation.AddParameter("@other_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_company").ToString)
                                objDataOperation.AddParameter("@other_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_contactno").ToString)
                                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iscancel").ToString)
                                objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("cancellationuserunkid").ToString)
                                objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("cancellationreason").ToString)
                                If IsDBNull(.Item("cancellationdate")) Then
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("cancellationdate").ToString)
                                End If
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@trainer_level_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainer_level_id").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", .Item("trainingschedulingunkid"), "hrtraining_trainers_tran", "trainerstranunkid", .Item("trainerstranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "D"

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If .Item("trainerstranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", .Item("trainingschedulingunkid"), "hrtraining_trainers_tran", "trainerstranunkid", .Item("trainerstranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                strQ = "DELETE FROM hrtraining_trainers_tran " & _
                                       "WHERE trainerstranunkid = @trainerstranunkid "

                                objDataOperation.AddParameter("@trainerstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainerstranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_Trainers", mstrModuleName)
        End Try
    End Function

    Public Function Cancel_All(ByVal isCancel As Boolean, _
                           ByVal dtCancelDate As DateTime, _
                           ByVal strCancelRemark As String, _
                           ByVal intCancelUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_trainers_tran SET " & _
                      "  iscancel = @iscancel" & _
                      ", cancellationuserunkid = @cancellationuserunkid" & _
                      ", cancellationreason = @cancellationreason" & _
                      ", cancellationdate = @cancellationdate " & _
                   "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isCancel.ToString)
            objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCancelDate)
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strCancelRemark.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCancelUserId)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Cancel_All", mstrModuleName)
        End Try
    End Function

    Public Function Void_All(ByVal isVoid As Boolean, _
                             ByVal dtVoidDate As DateTime, _
                             ByVal strVoidRemark As String, _
                             ByVal intVoidUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_trainers_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voidreason = @voidreason " & _
                    "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isVoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidRemark.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Void_All", mstrModuleName)
        End Try
    End Function

    Public Function MakeActive_All(ByVal isCancel As Boolean, _
                                   ByVal dtCancelDate As DateTime, _
                                   ByVal strCancelRemark As String, _
                                   ByVal intCancelUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_trainers_tran SET " & _
                      "  iscancel = @iscancel" & _
                      ", cancellationuserunkid = @cancellationuserunkid" & _
                      ", cancellationreason = @cancellationreason" & _
                      ", cancellationdate = @cancellationdate " & _
                   "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isCancel.ToString)
            If dtCancelDate = Nothing Then
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCancelDate)
            End If
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strCancelRemark.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCancelUserId)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Cancel_All", mstrModuleName)
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function getComboList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            Optional ByVal intScheduleId As Integer = -1, _
                            Optional ByVal intEnrollId As Integer = -1, _
                            Optional ByVal blnFlag As Boolean = False, _
                            Optional ByVal strList As String = "", _
                            Optional ByVal isFinalAnalysis As Boolean = False, _
                            Optional ByVal isAllTrainer As Boolean = False) As DataSet
        Dim StrQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intCnt As Integer = -1
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                objDo.AddParameter("@ScheduleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intScheduleId)

                If intScheduleId > 0 Then
                    StrQ = "SELECT " & _
                           "   hrtraining_trainers_tran.trainerstranunkid AS Id " & _
                           "FROM hrtraining_trainers_tran " & _
                           "WHERE ISNULL(hrtraining_trainers_tran.isvoid,0) = 0 " & _
                           " AND ISNULL(hrtraining_trainers_tran.iscancel,0) = 0 " & _
                           " AND hrtraining_trainers_tran.trainingschedulingunkid = @ScheduleId "

                    intCnt = objDo.RecordCount(StrQ)
                End If

                If blnFlag = True Then
                    StrQ = "SELECT 0 AS Id,@Select AS NAME , 0 AS EmpId , 0 AS LevelId UNION "
                End If

                StrQ &= "SELECT " & _
                        "    hrtraining_trainers_tran.trainerstranunkid AS Id " & _
                        "   ,CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN hrtraining_trainers_tran.other_name " & _
                        "		  WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername ,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                        "    END AS NAME " & _
                        "   ,hrtraining_trainers_tran.employeeunkid AS EmpId " & _
                        "   ,hrtraining_trainers_tran.trainer_level_id AS LevelId " & _
                        "FROM hrtraining_trainers_tran " & _
                        "   JOIN hrtraining_scheduling ON hrtraining_trainers_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                        "   JOIN hrtraining_enrollment_tran ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_enrollment_tran.trainingschedulingunkid " & _
                        "	LEFT JOIN hremployee_master ON hrtraining_trainers_tran.employeeunkid = hremployee_master.employeeunkid AND hrtraining_trainers_tran.employeeunkid <> -1 AND hremployee_master.isapproved = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                StrQ &= " WHERE ISNULL(hrtraining_trainers_tran.isvoid,0) = 0 AND ISNULL(hrtraining_trainers_tran.iscancel,0) = 0 "

                If intScheduleId > 0 Then
                    StrQ &= "AND hrtraining_scheduling.trainingschedulingunkid = @ScheduleId "
                End If

                If intEnrollId > 0 Then
                    StrQ &= "AND hrtraining_enrollment_tran.trainingenrolltranunkid = @EnrollId "
                End If

                If intCnt > 1 Then
                    If isAllTrainer = False Then
                        If isFinalAnalysis = False Then
                            StrQ &= "AND hrtraining_trainers_tran.trainer_level_id NOT IN (SELECT MAX(hrtraining_trainers_tran.trainer_level_id) FROM hrtraining_trainers_tran WHERE hrtraining_trainers_tran.trainingschedulingunkid = @ScheduleId AND ISNULL(hrtraining_trainers_tran.isvoid, 0) = 0 AND ISNULL    (hrtraining_trainers_tran.iscancel, 0) = 0 ) "
                        Else
                            StrQ &= "AND hrtraining_trainers_tran.trainer_level_id IN (SELECT MAX(hrtraining_trainers_tran.trainer_level_id) FROM hrtraining_trainers_tran WHERE  hrtraining_trainers_tran.trainingschedulingunkid = @ScheduleId AND ISNULL(hrtraining_trainers_tran.isvoid, 0) = 0 AND ISNULL    (hrtraining_trainers_tran.iscancel, 0) = 0 ) "
                        End If
                    End If
                End If

                objDo.AddParameter("@EnrollId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEnrollId)
                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

                dsList = objDo.ExecQuery(StrQ, strList)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                StrQ &= "ORDER BY Id "
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function getComboList(Optional ByVal intScheduleId As Integer = -1, _
    '                             Optional ByVal intEnrollId As Integer = -1, _
    '                             Optional ByVal blnFlag As Boolean = False, _
    '                             Optional ByVal strList As String = "", _
    '                             Optional ByVal isFinalAnalysis As Boolean = False, _
    '                             Optional ByVal isAllTrainer As Boolean = False) As DataSet
    '    Dim strQ As String = ""
    '    Dim strErrorMessage As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    objDataOperation = New clsDataOperation

    '    'Sandeep [ 02 Oct 2010 ] -- Start
    '    Dim intCnt As Integer = -1
    '    'Sandeep [ 02 Oct 2010 ] -- End 

    '    Try

    '        'Sandeep [ 02 Oct 2010 ] -- Start
    '        objDataOperation.AddParameter("@ScheduleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intScheduleId)

    '        If intScheduleId > 0 Then
    '            strQ = "SELECT hrtraining_trainers_tran.trainerstranunkid AS Id FROM hrtraining_trainers_tran WHERE " & _
    '                       " ISNULL(hrtraining_trainers_tran.isvoid,0) = 0 " & _
    '                       " AND ISNULL(hrtraining_trainers_tran.iscancel,0) = 0 " & _
    '                       "AND hrtraining_trainers_tran.trainingschedulingunkid = @ScheduleId "

    '            intCnt = objDataOperation.RecordCount(strQ)

    '        End If
    '        'Sandeep [ 02 Oct 2010 ] -- End 

    '        If blnFlag = True Then
    '            strQ = "SELECT 0 AS Id,@Select AS NAME , 0 AS EmpId , 0 AS LevelId UNION "
    '        End If

    '        'S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'strQ &= "SELECT " & _
    '        '            " hrtraining_trainers_tran.trainerstranunkid AS Id " & _
    '        '            ",CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN hrtraining_trainers_tran.other_name " & _
    '        '                  "WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ " & _
    '        '                                                                                     "ISNULL(hremployee_master.othername ,'')+' '+ " & _
    '        '                                                                                     "ISNULL(hremployee_master.surname,'') " & _
    '        '             "END AS NAME " & _
    '        '            ",hrtraining_trainers_tran.employeeunkid AS EmpId " & _
    '        '            ",hrtraining_trainers_tran.trainer_level_id AS LevelId " & _
    '        '            "FROM hrtraining_trainers_tran " & _
    '        '                 "JOIN hrtraining_scheduling ON hrtraining_trainers_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '        '                 "JOIN hrtraining_enrollment_tran ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_enrollment_tran.trainingschedulingunkid " & _
    '        '                 "LEFT JOIN hremployee_master ON hrtraining_trainers_tran.employeeunkid = hremployee_master.employeeunkid AND hrtraining_trainers_tran.employeeunkid <> -1 AND ISNULL(hremployee_master.isactive,0) = 1 " & _
    '        '            "WHERE ISNULL(hrtraining_trainers_tran.isvoid,0) = 0 " & _
    '        '                 "AND ISNULL(hrtraining_trainers_tran.iscancel,0) = 0 "

    '        strQ &= "SELECT " & _
    '                    " hrtraining_trainers_tran.trainerstranunkid AS Id " & _
    '                    ",CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN hrtraining_trainers_tran.other_name " & _
    '                "		  WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername ,'')+' '+ISNULL(hremployee_master.surname,'') " & _
    '                     "END AS NAME " & _
    '                    ",hrtraining_trainers_tran.employeeunkid AS EmpId " & _
    '                    ",hrtraining_trainers_tran.trainer_level_id AS LevelId " & _
    '                    "FROM hrtraining_trainers_tran " & _
    '                         "JOIN hrtraining_scheduling ON hrtraining_trainers_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '                         "JOIN hrtraining_enrollment_tran ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_enrollment_tran.trainingschedulingunkid " & _
    '                "	LEFT JOIN hremployee_master ON hrtraining_trainers_tran.employeeunkid = hremployee_master.employeeunkid AND hrtraining_trainers_tran.employeeunkid <> -1 "
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        strQ &= " WHERE ISNULL(hrtraining_trainers_tran.isvoid,0) = 0 AND ISNULL(hrtraining_trainers_tran.iscancel,0) = 0 "
    '        'S.SANDEEP [ 29 JUNE 2011 ] -- END 


    '        If intScheduleId > 0 Then
    '            strQ &= "AND hrtraining_scheduling.trainingschedulingunkid = @ScheduleId "
    '        End If

    '        If intEnrollId > 0 Then
    '            strQ &= "AND hrtraining_enrollment_tran.trainingenrolltranunkid = @EnrollId "
    '        End If

    '        'Sandeep [ 02 Oct 2010 ] -- Start
    '        'If isAllTrainer = False Then
    '        '    If isFinalAnalysis = False Then
    '        '        strQ &= "AND hrtraining_trainers_tran.trainer_level_id NOT IN (SELECT MAX(hrtraining_trainers_tran.trainer_level_id) FROM hrtraining_trainers_tran WHERE hrtraining_trainers_tran.trainingschedulingunkid = @ScheduleId AND ISNULL(hrtraining_trainers_tran.isvoid, 0) = 0 AND ISNULL    (hrtraining_trainers_tran.iscancel, 0) = 0 ) "
    '        '    Else
    '        '        strQ &= "AND hrtraining_trainers_tran.trainer_level_id IN (SELECT MAX(hrtraining_trainers_tran.trainer_level_id) FROM hrtraining_trainers_tran WHERE  hrtraining_trainers_tran.trainingschedulingunkid = @ScheduleId AND ISNULL(hrtraining_trainers_tran.isvoid, 0) = 0 AND ISNULL    (hrtraining_trainers_tran.iscancel, 0) = 0 ) "
    '        '    End If
    '        'End If
    '        If intCnt > 1 Then
    '            If isAllTrainer = False Then
    '                If isFinalAnalysis = False Then
    '                    strQ &= "AND hrtraining_trainers_tran.trainer_level_id NOT IN (SELECT MAX(hrtraining_trainers_tran.trainer_level_id) FROM hrtraining_trainers_tran WHERE hrtraining_trainers_tran.trainingschedulingunkid = @ScheduleId AND ISNULL(hrtraining_trainers_tran.isvoid, 0) = 0 AND ISNULL    (hrtraining_trainers_tran.iscancel, 0) = 0 ) "
    '                Else
    '                    strQ &= "AND hrtraining_trainers_tran.trainer_level_id IN (SELECT MAX(hrtraining_trainers_tran.trainer_level_id) FROM hrtraining_trainers_tran WHERE  hrtraining_trainers_tran.trainingschedulingunkid = @ScheduleId AND ISNULL(hrtraining_trainers_tran.isvoid, 0) = 0 AND ISNULL    (hrtraining_trainers_tran.iscancel, 0) = 0 ) "
    '                End If
    '            End If
    '        End If
    '        'Sandeep [ 02 Oct 2010 ] -- End 
    '        strQ &= "ORDER BY Id "


    '        'Sandeep [ 09 Oct 2010 ] -- Start
    '        'Issues Reported by Vimal
    '        'objDataOperation.AddParameter("@ScheduleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intScheduleId)
    '        'Sandeep [ 09 Oct 2010 ] -- End 
    '        objDataOperation.AddParameter("@EnrollId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEnrollId)
    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

    '        dsList = objDataOperation.ExecQuery(strQ, strList)
    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END

    Private Sub Get_Trainers_Data()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            strQ = "SELECT " & _
                      "  trainerstranunkid " & _
                      ", trainingschedulingunkid " & _
                      ", employeeunkid " & _
                      ", other_name " & _
                      ", other_position " & _
                      ", other_company " & _
                      ", other_contactno " & _
                      ", iscancel " & _
                      ", cancellationuserunkid " & _
                      ", cancellationreason " & _
                      ", cancellationdate " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", trainer_level_id " & _
                    "FROM hrtraining_trainers_tran " & _
                    "WHERE trainerstranunkid = @trainerstranunkid " & _
                    "AND ISNULL(isvoid,0) = 0 " & _
                    "AND ISNULL(iscancel,0) = 0 "

            objDataOperation.AddParameter("@trainerstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainerUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("List").Rows

                mstrOther_Company = dtRow.Item("other_company")
                mstrOther_Contact = dtRow.Item("other_contactno")
                mstrOther_Name = dtRow.Item("other_name")
                mstrOther_Department = dtRow.Item("other_position")
                mintTrainerUnkid = dtRow.Item("trainerstranunkid")
                mintEmployeeUnkid = dtRow.Item("employeeunkid")
                mintTrainerLevelId = dtRow.Item("trainer_level_id")

                Exit For
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Trainers_Data", mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
