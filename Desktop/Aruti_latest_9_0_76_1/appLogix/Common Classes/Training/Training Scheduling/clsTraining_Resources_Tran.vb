﻿'************************************************************************************************************************************
'Class Name : clsTraining_Resources_Tran.vb
'Purpose    :
'Date       :01/08/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsTraining_Resources_Tran

#Region " Private Variables "
    Private Const mstrModuleName As String = "clsTraining_Resources_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintTrainingSchdulingUnkid As Integer = -1
    Private mdtTran As DataTable
    'S.SANDEEP [ 12 OCT 2011 ] -- START
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    Private mintResourcesTranId As Integer = 0
    'S.SANDEEP [ 12 OCT 2011 ] -- END 
#End Region

#Region " Properties "
    Public Property _Training_SchedulingUnkid() As Integer
        Get
            Return mintTrainingSchdulingUnkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingSchdulingUnkid = value
            Get_Resources_Tran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
#End Region

#Region " Contructor "
    Public Sub New()
        mdtTran = New DataTable("Training_Resources")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("resourcestranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingschedulingunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("resourceunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("resources_remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("iscancel")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancellationuserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancellationreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancellationdate")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Medhods "
    Private Sub Get_Resources_Tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            strQ = "SELECT " & _
                     " resourcestranunkid " & _
                     ",trainingschedulingunkid " & _
                     ",resourceunkid " & _
                     ",resources_remark " & _
                     ",iscancel " & _
                     ",cancellationuserunkid " & _
                     ",cancellationreason " & _
                     ",cancellationdate " & _
                     ",isvoid " & _
                     ",voiddatetime " & _
                     ",voiduserunkid " & _
                     ",voidreason " & _
                     ",'' AS AUD " & _
                   "FROM hrtraining_resources_tran " & _
                   "WHERE hrtraining_resources_tran.trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow()

                    dRowID_Tran.Item("resourcestranunkid") = .Item("resourcestranunkid")
                    dRowID_Tran.Item("trainingschedulingunkid") = .Item("trainingschedulingunkid")
                    dRowID_Tran.Item("resourceunkid") = .Item("resourceunkid")
                    dRowID_Tran.Item("resources_remark") = .Item("resources_remark")
                    dRowID_Tran.Item("iscancel") = .Item("iscancel")
                    dRowID_Tran.Item("cancellationuserunkid") = .Item("cancellationuserunkid")
                    dRowID_Tran.Item("cancellationreason") = .Item("cancellationreason")

                    If IsDBNull(.Item("cancellationdate")) Then
                        dRowID_Tran.Item("cancellationdate") = DBNull.Value
                    Else
                        dRowID_Tran.Item("cancellationdate") = .Item("cancellationdate")
                    End If

                    dRowID_Tran.Item("isvoid") = .Item("isvoid")

                    If IsDBNull(.Item("voiddatetime")) Then
                        dRowID_Tran.Item("voiddatetime") = DBNull.Value
                    Else
                        dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    End If
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid")
                    dRowID_Tran.Item("voidreason") = .Item("voidreason")
                    dRowID_Tran.Item("AUD") = .Item("AUD")

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Resources_Tran", mstrModuleName)
        End Try
    End Sub

    Public Function InsertUpdateDelete_Resources() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrtraining_resources_tran ( " & _
                                            "  trainingschedulingunkid " & _
                                            ", resourceunkid " & _
                                            ", resources_remark " & _
                                            ", iscancel " & _
                                            ", cancellationuserunkid " & _
                                            ", cancellationreason " & _
                                            ", cancellationdate " & _
                                            ", isvoid " & _
                                            ", voiddatetime " & _
                                            ", voiduserunkid " & _
                                            ", voidreason" & _
                                      ") VALUES (" & _
                                            "  @trainingschedulingunkid " & _
                                            ", @resourceunkid " & _
                                            ", @resources_remark " & _
                                            ", @iscancel " & _
                                            ", @cancellationuserunkid " & _
                                            ", @cancellationreason " & _
                                            ", @cancellationdate " & _
                                            ", @isvoid " & _
                                            ", @voiddatetime " & _
                                            ", @voiduserunkid " & _
                                            ", @voidreason" & _
                                      "); SELECT @@identity"

                                objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)
                                objDataOperation.AddParameter("@resourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resourceunkid").ToString)
                                objDataOperation.AddParameter("@resources_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("resources_remark").ToString)
                                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iscancel").ToString)
                                objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("cancellationuserunkid").ToString)
                                objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("cancellationreason").ToString)
                                If IsDBNull(.Item("cancellationdate")) Then
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("cancellationdate").ToString)
                                End If
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)


                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                mintResourcesTranId = dsList.Tables(0).Rows(0)(0)

                                If .Item("trainingschedulingunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", .Item("trainingschedulingunkid"), "hrtraining_resources_tran", "resourcestranunkid", mintResourcesTranId, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", mintTrainingSchdulingUnkid, "hrtraining_resources_tran", "resourcestranunkid", mintResourcesTranId, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "U"
                                strQ = "UPDATE hrtraining_resources_tran SET " & _
                                        "  trainingschedulingunkid = @trainingschedulingunkid" & _
                                        ", resourceunkid = @resourceunkid" & _
                                        ", resources_remark = @resources_remark" & _
                                        ", iscancel = @iscancel" & _
                                        ", cancellationuserunkid = @cancellationuserunkid" & _
                                        ", cancellationreason = @cancellationreason" & _
                                        ", cancellationdate = @cancellationdate" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE resourcestranunkid = @resourcestranunkid "

                                objDataOperation.AddParameter("@resourcestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resourcestranunkid").ToString)
                                objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingschedulingunkid").ToString)
                                objDataOperation.AddParameter("@resourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resourceunkid").ToString)
                                objDataOperation.AddParameter("@resources_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("resources_remark").ToString)
                                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iscancel").ToString)
                                objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("cancellationuserunkid").ToString)
                                objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("cancellationreason").ToString)
                                If IsDBNull(.Item("cancellationdate")) Then
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("cancellationdate").ToString)
                                End If
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", .Item("trainingschedulingunkid"), "hrtraining_resources_tran", "resourcestranunkid", .Item("resourcestranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "D"

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If .Item("resourcestranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", .Item("trainingschedulingunkid"), "hrtraining_resources_tran", "resourcestranunkid", .Item("resourcestranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                strQ = "DELETE FROM hrtraining_resources_tran " & _
                                        "WHERE resourcestranunkid = @resourcestranunkid "

                                objDataOperation.AddParameter("@resourcestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resourcestranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_Resources", mstrModuleName)
        End Try
    End Function

    Public Function Cancel_All(ByVal isCancel As Boolean, _
                               ByVal dtCancelDate As DateTime, _
                               ByVal strCancelRemark As String, _
                               ByVal intCancelUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_resources_tran SET " & _
                      "  iscancel = @iscancel" & _
                      ", cancellationuserunkid = @cancellationuserunkid" & _
                      ", cancellationreason = @cancellationreason" & _
                      ", cancellationdate = @cancellationdate " & _
                   "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isCancel.ToString)
            objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCancelDate)
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strCancelRemark.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCancelUserId)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Cancel_All", mstrModuleName)
        End Try
    End Function

    Public Function Void_All(ByVal isVoid As Boolean, _
                             ByVal dtVoidDate As DateTime, _
                             ByVal strVoidRemark As String, _
                             ByVal intVoidUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_resources_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voidreason = @voidreason " & _
                    "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isVoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidRemark.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Void_All", mstrModuleName)
        End Try
    End Function

    Public Function MakeActive_All(ByVal isCancel As Boolean, _
                                   ByVal dtCancelDate As DateTime, _
                                   ByVal strCancelRemark As String, _
                                   ByVal intCancelUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_resources_tran SET " & _
                      "  iscancel = @iscancel" & _
                      ", cancellationuserunkid = @cancellationuserunkid" & _
                      ", cancellationreason = @cancellationreason" & _
                      ", cancellationdate = @cancellationdate " & _
                   "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isCancel.ToString)
            If dtCancelDate = Nothing Then
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCancelDate)
            End If
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strCancelRemark.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCancelUserId)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Cancel_All", mstrModuleName)
        End Try
    End Function
#End Region

End Class
