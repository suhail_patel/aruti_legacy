﻿'************************************************************************************************************************************
'Class Name :clsTraining_Allocation_Tran.vb
'Purpose    :
'Date       :08-Feb-2017
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsTraining_Allocation_Tran

#Region " Private Variables "

    Private Const mstrModuleName As String = "clsTraining_Allocation_Tran"
    Private mstrMessage As String = ""
    Private mintTrainingSchdulingUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mintAllocationTranId As Integer = 0
    Private mObjDataOperation As clsDataOperation

#End Region

#Region " Properties "

    Public Property _Training_SchedulingUnkid() As Integer
        Get
            Return mintTrainingSchdulingUnkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingSchdulingUnkid = value
            Get_Allocation_Tran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public WriteOnly Property _ObjDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            mObjDataOperation = value
        End Set
    End Property

#End Region

#Region " Contructor "

    Public Sub New()
        mdtTran = New DataTable("allocation")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("allocationtranunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingschedulingunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("allocationunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = GetType(System.DateTime)
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("iscancel")
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancellationuserunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancellationreason")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancellationdate")
            dCol.DataType = GetType(System.DateTime)
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Medhods "

    Private Sub Get_Allocation_Tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As clsDataOperation
            If mObjDataOperation IsNot Nothing Then
                objDataOperation = mObjDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                   "     allocationtranunkid " & _
                   "    ,trainingschedulingunkid " & _
                   "    ,allocationunkid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voiddatetime " & _
                   "    ,voidreason " & _
                   "    ,iscancel " & _
                   "    ,cancellationuserunkid " & _
                   "    ,cancellationreason " & _
                   "    ,cancellationdate " & _
                   "FROM hrtraining_allocation_tran " & _
                   "WHERE hrtraining_allocation_tran.trainingschedulingunkid = @trainingschedulingunkid " & _
                   " AND isvoid = 0 AND iscancel = 0 "

            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                mdtTran.ImportRow(dsList.Tables(0).Rows(i))
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Allocation_Tran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_Allocations() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        Try
            If mObjDataOperation IsNot Nothing Then
                objDataOperation = mObjDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrtraining_allocation_tran " & _
                                            "(trainingschedulingunkid " & _
                                            ",allocationunkid " & _
                                            ",isvoid " & _
                                            ",voiduserunkid " & _
                                            ",voiddatetime " & _
                                            ",voidreason " & _
                                            ",iscancel " & _
                                            ",cancellationuserunkid " & _
                                            ",cancellationreason " & _
                                            ",cancellationdate) " & _
                                        "VALUES " & _
                                            "(@trainingschedulingunkid " & _
                                            ",@allocationunkid " & _
                                            ",@isvoid " & _
                                            ",@voiduserunkid " & _
                                            ",@voiddatetime " & _
                                            ",@voidreason " & _
                                            ",@iscancel " & _
                                            ",@cancellationuserunkid " & _
                                            ",@cancellationreason " & _
                                            ",@cancellationdate " & _
                                            " ); SELECT @@identity "

                                objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid)
                                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allocationunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@iscancel", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("iscancel"))
                                objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("cancellationuserunkid"))
                                objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("cancellationreason"))
                                If IsDBNull(.Item("cancellationdate")) = False Then
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("cancellationdate"))
                                Else
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintAllocationTranId = dsList.Tables(0).Rows(0)(0)

                                If .Item("trainingschedulingunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", .Item("trainingschedulingunkid"), "hrtraining_allocation_tran", "allocationtranunkid", mintAllocationTranId, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", mintTrainingSchdulingUnkid, "hrtraining_allocation_tran", "allocationtranunkid", mintAllocationTranId, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"
                                strQ = "UPDATE hrtraining_allocation_tran " & _
                                       "SET  trainingschedulingunkid = @trainingschedulingunkid " & _
                                            ",allocationunkid = @allocationunkid " & _
                                            ",isvoid = @isvoid " & _
                                            ",voiduserunkid = @voiduserunkid " & _
                                            ",voiddatetime = @voiddatetime " & _
                                            ",voidreason = @voidreason " & _
                                            ",iscancel = @iscancel " & _
                                            ",cancellationuserunkid = @cancellationuserunkid " & _
                                            ",cancellationreason = @cancellationreason " & _
                                            ",cancellationdate = @cancellationdate " & _
                                       "WHERE allocationtranunkid = @allocationtranunkid "

                                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allocationtranunkid"))
                                objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingschedulingunkid"))
                                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allocationunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@iscancel", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("iscancel"))
                                objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("cancellationuserunkid"))
                                objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("cancellationreason"))
                                If IsDBNull(.Item("cancellationdate")) = False Then
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("cancellationdate"))
                                Else
                                    objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", .Item("trainingschedulingunkid"), "hrtraining_allocation_tran", "allocationtranunkid", .Item("allocationtranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"
                                If .Item("allocationtranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_scheduling", "trainingschedulingunkid", .Item("trainingschedulingunkid"), "hrtraining_allocation_tran", "allocationtranunkid", .Item("allocationtranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                strQ = "UPDATE hrtraining_allocation_tran " & _
                                       "SET " & _
                                       " isvoid = @isvoid " & _
                                       ",voiduserunkid = @voiduserunkid " & _
                                       ",voiddatetime = @voiddatetime " & _
                                       ",voidreason = @voidreason " & _
                                       "WHERE allocationtranunkid = @allocationtranunkid "

                                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allocationtranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_Allocations; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Cancel_All(ByVal isCancel As Boolean, _
                           ByVal dtCancelDate As DateTime, _
                           ByVal strCancelRemark As String, _
                           ByVal intCancelUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_allocation_tran SET " & _
                      "  iscancel = @iscancel" & _
                      ", cancellationuserunkid = @cancellationuserunkid" & _
                      ", cancellationreason = @cancellationreason" & _
                      ", cancellationdate = @cancellationdate " & _
                   "WHERE trainingschedulingunkid = @trainingschedulingunkid "

            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isCancel.ToString)
            objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCancelDate)
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strCancelRemark.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCancelUserId)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Cancel_All", mstrModuleName)
        End Try
    End Function

    Public Function Void_All(ByVal isVoid As Boolean, _
                             ByVal dtVoidDate As DateTime, _
                             ByVal strVoidRemark As String, _
                             ByVal intVoidUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_allocation_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voidreason = @voidreason " & _
                    "WHERE allocationtranunkid = @allocationtranunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isVoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidRemark.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Void_All", mstrModuleName)
        End Try
    End Function

    Public Function MakeActive_All(ByVal isCancel As Boolean, _
                                   ByVal dtCancelDate As DateTime, _
                                   ByVal strCancelRemark As String, _
                                   ByVal intCancelUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_allocation_tran SET " & _
                      "  iscancel = @iscancel" & _
                      ", cancellationuserunkid = @cancellationuserunkid" & _
                      ", cancellationreason = @cancellationreason" & _
                      ", cancellationdate = @cancellationdate " & _
                   "WHERE allocationtranunkid = @allocationtranunkid "

            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isCancel.ToString)
            If dtCancelDate = Nothing Then
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cancellationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCancelDate)
            End If
            objDataOperation.AddParameter("@cancellationreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strCancelRemark.ToString)
            objDataOperation.AddParameter("@cancellationuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCancelUserId)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingSchdulingUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Cancel_All", mstrModuleName)
        End Try
    End Function

    Public Function GetCSV_AllocationName(ByVal iAllocationRefId As Integer, ByVal iTrainingScheduleId As Integer, Optional ByVal strAllocationIds As String = "") As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iOwners As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim dsList As New DataSet
        Try
            If iAllocationRefId <= 0 Then Return ""

            Select Case iAllocationRefId
                Case enAllocation.BRANCH
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrstation_master ON hrstation_master.stationunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.DEPARTMENT_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.DEPARTMENT
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.SECTION_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.SECTION
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrsection_master ON hrsection_master.sectionunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.UNIT_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.UNIT
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrunit_master ON hrunit_master.unitunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.TEAM
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrteam_master ON hrteam_master.teamunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.JOB_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.JOBS
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(job_name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrjob_master ON hrjob_master.jobunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.CLASS_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.CLASSES
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrclasses_master ON hrclasses_master.classesunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "

                Case enAllocation.COST_CENTER
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(costcentername AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "' "
            End Select

            If strAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND allocationunkid IN (" & strAllocationIds & ") "
            End If
            StrQ &= " AND hrtraining_allocation_tran.isvoid = 0 AND hrtraining_allocation_tran.iscancel = 0 FOR XML PATH('')),1,1,''),'') AS CSV "

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iOwners = dsList.Tables(0).Rows(0).Item("CSV")
            End If

            Return iOwners

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCSV_AllocationName; Module Name: " & mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    Public Function GetCSV_AllocationIds(ByVal iAllocationRefId As Integer, ByVal iTrainingScheduleId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iOwners As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim dsList As New DataSet
        Try
            If iAllocationRefId <= 0 Then Return ""

            Select Case iAllocationRefId
                Case enAllocation.BRANCH
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(stationunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrstation_master ON hrstation_master.stationunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.DEPARTMENT_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(deptgroupunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.DEPARTMENT
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(departmentunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.SECTION_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(sectiongroupunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.SECTION
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(sectionunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrsection_master ON hrsection_master.sectionunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.UNIT_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(unitgroupunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.UNIT
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(unitunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrunit_master ON hrunit_master.unitunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.TEAM
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(teamunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrteam_master ON hrteam_master.teamunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.JOB_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(jobgroupunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.JOBS
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(jobunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrjob_master ON hrjob_master.jobunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.CLASS_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(classgroupunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                            " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrtraining_allocation_tran.allocationunkid " & _
                            "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.CLASSES
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(classesunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN hrclasses_master ON hrclasses_master.classesunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.COST_CENTER
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(costcenterunkid AS NVARCHAR(MAX)) FROM hrtraining_allocation_tran " & _
                           " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = hrtraining_allocation_tran.allocationunkid " & _
                           "WHERE trainingschedulingunkid = '" & iTrainingScheduleId & "'  FOR XML PATH('')),1,1,''),'') AS CSV "
            End Select

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iOwners = dsList.Tables(0).Rows(0).Item("CSV")
            End If

            Return iOwners

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCSV_AllocationName; Module Name: " & mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

#End Region

End Class
