﻿'************************************************************************************************************************************
'Class Name : clsTraining_Analysis_Master.vb
'Purpose    :
'Date       :05/08/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsTraining_Analysis_Master
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Analysis_Master"
    Dim objDataOperation As clsDataOperation
    Private objAnalysisTran As New clsTraining_Analysis_Tran
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAnalysisunkid As Integer
    Private mintTrainingenrolltranunkid As Integer
    'Sandeep [ 02 Oct 2010 ] -- Start
    'Private mdtAnalysis_Date As Date
    'Sandeep [ 02 Oct 2010 ] -- End 
    Private mstrTrainee_Remark As String = String.Empty
    Private mintUserinkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoidatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mblnIscomplete As Boolean
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysisunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Analysisunkid() As Integer
        Get
            Return mintAnalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingenrolltranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Trainingenrolltranunkid() As Integer
        Get
            Return mintTrainingenrolltranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingenrolltranunkid = Value
        End Set
    End Property

    'Sandeep [ 02 Oct 2010 ] -- Start
    '''' <summary>
    '''' Purpose: Get or Set analysis_date
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    'Public Property _Analysis_Date() As Date
    '    Get
    '        Return mdtAnalysis_Date
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtAnalysis_Date = Value
    '    End Set
    'End Property
    'Sandeep [ 02 Oct 2010 ] -- End 

    ''' <summary>
    ''' Purpose: Get or Set trainee_remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Trainee_Remark() As String
        Get
            Return mstrTrainee_Remark
        End Get
        Set(ByVal value As String)
            mstrTrainee_Remark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userinkid() As Integer
        Get
            Return mintUserinkid
        End Get
        Set(ByVal value As Integer)
            mintUserinkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidatetime() As Date
        Get
            Return mdtVoidatetime
        End Get
        Set(ByVal value As Date)
            mdtVoidatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscomplete
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iscomplete() As Boolean
        Get
            Return mblnIscomplete
        End Get
        Set(ByVal value As Boolean)
            mblnIscomplete = Value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sandeep [ 02 Oct 2010 ] -- Start
            'strQ = "SELECT " & _
            '                    "  analysisunkid " & _
            '                    ", trainingenrolltranunkid " & _
            '                    ", analysis_date " & _
            '                    ", trainee_remark " & _
            '                    ", userunkid " & _
            '                    ", isvoid " & _
            '                    ", voiddatetime " & _
            '                    ", voiduserunkid " & _
            '                    ", voidreason " & _
            '                    ", iscomplete " & _
            '                "FROM hrtraining_analysis_master " & _
            '                "WHERE analysisunkid = @analysisunkid "
            strQ = "SELECT " & _
                           "  analysisunkid " & _
                           ", trainingenrolltranunkid " & _
                           ", trainee_remark " & _
                           ", userunkid " & _
                           ", isvoid " & _
                           ", voiddatetime " & _
                           ", voiduserunkid " & _
                           ", voidreason " & _
                           ", iscomplete " & _
                        "FROM hrtraining_analysis_master " & _
                        "WHERE analysisunkid = @analysisunkid "
            'Sandeep [ 02 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAnalysisunkid = CInt(dtRow.Item("analysisunkid"))
                mintTrainingenrolltranunkid = CInt(dtRow.Item("trainingenrolltranunkid"))
                'Sandeep [ 02 Oct 2010 ] -- Start
                'mdtAnalysis_Date = dtRow.Item("analysis_date")
                'Sandeep [ 02 Oct 2010 ] -- End 
                mstrTrainee_Remark = dtRow.Item("trainee_remark").ToString
                mintUserinkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoidatetime = Nothing
                Else
                    mdtVoidatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIscomplete = CBool(dtRow.Item("iscomplete"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal strFilterString As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     hrtraining_analysis_master.analysisunkid " & _
                       "    ,hrtraining_analysis_master.trainingenrolltranunkid " & _
                       "    ,CONVERT(CHAR(8),hrtraining_analysis_tran.analysis_date,112) AS Date " & _
                       "    ,hrtraining_analysis_master.trainee_remark " & _
                       "    ,hrtraining_analysis_master.userunkid " & _
                       "    ,hrtraining_analysis_master.isvoid " & _
                       "    ,hrtraining_analysis_master.voiddatetime " & _
                       "    ,hrtraining_analysis_master.voiduserunkid " & _
                       "    ,hrtraining_analysis_master.voidreason " & _
                       "    ,hrtraining_analysis_master.iscomplete " & _
                       "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       "    ,ISNULL(hrtraining_scheduling.course_title,'0') AS Course " & _
                       "    ,hrtraining_scheduling.trainingschedulingunkid AS CourseId " & _
                       "    ,hremployee_master.employeeunkid AS EmpId " & _
                       "    ,CASE WHEN hrtraining_enrollment_tran.status_id  = 3 THEN 1 ELSE 0 END AS StatusId " & _
                       "    ,hrtraining_analysis_tran.reviewer_remark AS Remark " & _
                       "    ,hrtraining_analysis_tran.resultunkid " & _
                       "    ,CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN hrtraining_trainers_tran.other_name WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(Trainer.firstname,'')+' '+ ISNULL(Trainer.othername,'')+' '+ ISNULL(Trainer.surname,'') END AS Trainer " & _
                       "    ,hrresult_master.resultname AS Result " & _
                       "    ,hrtraining_analysis_tran.trainerstranunkid " & _
                       "    ,hrtraining_analysis_tran.analysistranunkid " & _
                       "    ,hrtraining_trainers_tran.trainer_level_id AS LevelId " & _
                       "FROM hrtraining_analysis_tran " & _
                       "    LEFT JOIN hrtraining_analysis_master ON hrtraining_analysis_master.analysisunkid = hrtraining_analysis_tran.analysisunkid " & _
                       "    LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                       "    LEFT JOIN hrtraining_enrollment_tran ON hrtraining_analysis_master.trainingenrolltranunkid = hrtraining_enrollment_tran.trainingenrolltranunkid " & _
                       "    LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "    LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                       "    LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
                       "    LEFT JOIN hremployee_master AS Trainer ON hrtraining_trainers_tran.employeeunkid = Trainer.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE ISNULL(hrtraining_analysis_master.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0 " & _
                        "AND ISNULL(hrtraining_analysis_tran.isvoid,0) = 0 "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If strFilterString.Trim.Length > 0 Then
                    StrQ &= "AND " & strFilterString
                End If

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, Optional ByVal intEnrollId As Integer = -1) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        'Sandeep [ 02 Oct 2010 ] -- Start
    '        'strQ = "SELECT " & _
    '        '                     " hrtraining_analysis_master.analysisunkid " & _
    '        '                     ",hrtraining_analysis_master.trainingenrolltranunkid " & _
    '        '                     ",CONVERT(CHAR(8),hrtraining_analysis_master.analysis_date,112) AS Date " & _
    '        '                     ",hrtraining_analysis_master.trainee_remark " & _
    '        '                     ",hrtraining_analysis_master.userunkid " & _
    '        '                     ",hrtraining_analysis_master.isvoid " & _
    '        '                     ",hrtraining_analysis_master.voiddatetime " & _
    '        '                     ",hrtraining_analysis_master.voiduserunkid " & _
    '        '                     ",hrtraining_analysis_master.voidreason " & _
    '        '                     ",hrtraining_analysis_master.iscomplete " & _
    '        '                     ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '                     ",ISNULL(hrtraining_scheduling.course_title,'') AS Course " & _
    '        '                     ",hrtraining_scheduling.trainingschedulingunkid AS CourseId " & _
    '        '                     ",hremployee_master.employeeunkid AS EmpId " & _
    '        '                     ",CASE WHEN hrtraining_enrollment_tran.status_id  = 3 THEN 1 ELSE 0 END AS StatusId " & _
    '        '                     ",hrtraining_analysis_tran.reviewer_remark AS Remark " & _
    '        '                     ",hrtraining_analysis_tran.resultunkid " & _
    '        '                     ",CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN hrtraining_trainers_tran.other_name " & _
    '        '                             "WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(Trainer.firstname,'')+' '+ " & _
    '        '                                                                                               "ISNULL(Trainer.othername,'')+' '+ " & _
    '        '                                                                                               "ISNULL(Trainer.surname,'') " & _
    '        '                      "END AS Trainer " & _
    '        '                      ",hrresult_master.resultname AS Result " & _
    '        '                      ",hrtraining_analysis_tran.trainerstranunkid " & _
    '        '                      ",hrtraining_analysis_tran.analysistranunkid " & _
    '        '                "FROM hrtraining_analysis_tran " & _
    '        '                     "LEFT JOIN hrtraining_analysis_master ON hrtraining_analysis_master.analysisunkid = hrtraining_analysis_tran.analysisunkid " & _
    '        '                     "LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '                     "LEFT JOIN hrtraining_enrollment_tran ON hrtraining_analysis_master.trainingenrolltranunkid = hrtraining_enrollment_tran.trainingenrolltranunkid " & _
    '        '                     "LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                     "LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '        '                     "LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
    '        '                     "LEFT JOIN hremployee_master AS Trainer ON hrtraining_trainers_tran.employeeunkid = Trainer.employeeunkid " & _
    '        '                "WHERE ISNULL(hrtraining_analysis_master.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0 "

    '        'Sandeep [ 16 Oct 2010 ] -- Start
    '        'strQ = "SELECT " & _
    '        '        " hrtraining_analysis_master.analysisunkid " & _
    '        '        ",hrtraining_analysis_master.trainingenrolltranunkid " & _
    '        '        ",CONVERT(CHAR(8),hrtraining_analysis_tran.analysis_date,112) AS Date " & _
    '        '        ",hrtraining_analysis_master.trainee_remark " & _
    '        '        ",hrtraining_analysis_master.userunkid " & _
    '        '        ",hrtraining_analysis_master.isvoid " & _
    '        '        ",hrtraining_analysis_master.voiddatetime " & _
    '        '        ",hrtraining_analysis_master.voiduserunkid " & _
    '        '        ",hrtraining_analysis_master.voidreason " & _
    '        '        ",hrtraining_analysis_master.iscomplete " & _
    '        '        ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '        ",ISNULL(hrtraining_scheduling.course_title,'') AS Course " & _
    '        '        ",hrtraining_scheduling.trainingschedulingunkid AS CourseId " & _
    '        '        ",hremployee_master.employeeunkid AS EmpId " & _
    '        '        ",CASE WHEN hrtraining_enrollment_tran.status_id  = 3 THEN 1 ELSE 0 END AS StatusId " & _
    '        '        ",hrtraining_analysis_tran.reviewer_remark AS Remark " & _
    '        '        ",hrtraining_analysis_tran.resultunkid " & _
    '        '        ",CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN hrtraining_trainers_tran.other_name " & _
    '        '        "          WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(Trainer.firstname,'')+' '+ " & _
    '        '                                                                           "ISNULL(Trainer.othername,'')+' '+ " & _
    '        '                                                                           "ISNULL(Trainer.surname,'') " & _
    '        '        " END AS Trainer " & _
    '        '        ",hrresult_master.resultname AS Result " & _
    '        '        ",hrtraining_analysis_tran.trainerstranunkid " & _
    '        '        ",hrtraining_analysis_tran.analysistranunkid " & _
    '        '    "FROM hrtraining_analysis_tran " & _
    '        '        "LEFT JOIN hrtraining_analysis_master ON hrtraining_analysis_master.analysisunkid = hrtraining_analysis_tran.analysisunkid " & _
    '        '        "LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '        "LEFT JOIN hrtraining_enrollment_tran ON hrtraining_analysis_master.trainingenrolltranunkid = hrtraining_enrollment_tran.trainingenrolltranunkid " & _
    '        '        "LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '        '        "LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
    '        '        "LEFT JOIN hremployee_master AS Trainer ON hrtraining_trainers_tran.employeeunkid = Trainer.employeeunkid " & _
    '        '    "WHERE ISNULL(hrtraining_analysis_master.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0 "


    '        'S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'strQ = "SELECT " & _
    '        '        " hrtraining_analysis_master.analysisunkid " & _
    '        '        ",hrtraining_analysis_master.trainingenrolltranunkid " & _
    '        '        ",CONVERT(CHAR(8),hrtraining_analysis_tran.analysis_date,112) AS Date " & _
    '        '        ",hrtraining_analysis_master.trainee_remark " & _
    '        '        ",hrtraining_analysis_master.userunkid " & _
    '        '        ",hrtraining_analysis_master.isvoid " & _
    '        '        ",hrtraining_analysis_master.voiddatetime " & _
    '        '        ",hrtraining_analysis_master.voiduserunkid " & _
    '        '        ",hrtraining_analysis_master.voidreason " & _
    '        '        ",hrtraining_analysis_master.iscomplete " & _
    '        '        ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '        ",ISNULL(hrtraining_scheduling.course_title,'') AS Course " & _
    '        '        ",hrtraining_scheduling.trainingschedulingunkid AS CourseId " & _
    '        '        ",hremployee_master.employeeunkid AS EmpId " & _
    '        '        ",CASE WHEN hrtraining_enrollment_tran.status_id  = 3 THEN 1 ELSE 0 END AS StatusId " & _
    '        '        ",hrtraining_analysis_tran.reviewer_remark AS Remark " & _
    '        '        ",hrtraining_analysis_tran.resultunkid " & _
    '        '        ",CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN hrtraining_trainers_tran.other_name " & _
    '        '        "          WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(Trainer.firstname,'')+' '+ " & _
    '        '                                                                           "ISNULL(Trainer.othername,'')+' '+ " & _
    '        '                                                                           "ISNULL(Trainer.surname,'') " & _
    '        '        " END AS Trainer " & _
    '        '        ",hrresult_master.resultname AS Result " & _
    '        '        ",hrtraining_analysis_tran.trainerstranunkid " & _
    '        '        ",hrtraining_analysis_tran.analysistranunkid " & _
    '        '        ",hrtraining_trainers_tran.trainer_level_id AS LevelId " & _
    '        '    "FROM hrtraining_analysis_tran " & _
    '        '        "LEFT JOIN hrtraining_analysis_master ON hrtraining_analysis_master.analysisunkid = hrtraining_analysis_tran.analysisunkid " & _
    '        '        "LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '        "LEFT JOIN hrtraining_enrollment_tran ON hrtraining_analysis_master.trainingenrolltranunkid = hrtraining_enrollment_tran.trainingenrolltranunkid " & _
    '        '        "LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '        '        "LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
    '        '        "LEFT JOIN hremployee_master AS Trainer ON hrtraining_trainers_tran.employeeunkid = Trainer.employeeunkid " & _
    '        '    "WHERE ISNULL(hrtraining_analysis_master.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0 " & _
    '        '    "AND ISNULL(hrtraining_analysis_tran.isvoid,0) = 0 "

    '        strQ = "SELECT " & _
    '                " hrtraining_analysis_master.analysisunkid " & _
    '                ",hrtraining_analysis_master.trainingenrolltranunkid " & _
    '                ",CONVERT(CHAR(8),hrtraining_analysis_tran.analysis_date,112) AS Date " & _
    '                ",hrtraining_analysis_master.trainee_remark " & _
    '                ",hrtraining_analysis_master.userunkid " & _
    '                ",hrtraining_analysis_master.isvoid " & _
    '                ",hrtraining_analysis_master.voiddatetime " & _
    '                ",hrtraining_analysis_master.voiduserunkid " & _
    '                ",hrtraining_analysis_master.voidreason " & _
    '                ",hrtraining_analysis_master.iscomplete " & _
    '                ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                ",ISNULL(hrtraining_scheduling.course_title,'') AS Course " & _
    '                ",hrtraining_scheduling.trainingschedulingunkid AS CourseId " & _
    '                ",hremployee_master.employeeunkid AS EmpId " & _
    '                ",CASE WHEN hrtraining_enrollment_tran.status_id  = 3 THEN 1 ELSE 0 END AS StatusId " & _
    '                ",hrtraining_analysis_tran.reviewer_remark AS Remark " & _
    '                ",hrtraining_analysis_tran.resultunkid " & _
    '                ",CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN hrtraining_trainers_tran.other_name " & _
    '                "          WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(Trainer.firstname,'')+' '+ " & _
    '                                                                                   "ISNULL(Trainer.othername,'')+' '+ " & _
    '                                                                                   "ISNULL(Trainer.surname,'') " & _
    '                " END AS Trainer " & _
    '                ",hrresult_master.resultname AS Result " & _
    '                ",hrtraining_analysis_tran.trainerstranunkid " & _
    '                ",hrtraining_analysis_tran.analysistranunkid " & _
    '                ",hrtraining_trainers_tran.trainer_level_id AS LevelId " & _
    '            "FROM hrtraining_analysis_tran " & _
    '                "LEFT JOIN hrtraining_analysis_master ON hrtraining_analysis_master.analysisunkid = hrtraining_analysis_tran.analysisunkid " & _
    '                "LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '                "LEFT JOIN hrtraining_enrollment_tran ON hrtraining_analysis_master.trainingenrolltranunkid = hrtraining_enrollment_tran.trainingenrolltranunkid " & _
    '                "LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid "
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        strQ &= "LEFT JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '                "LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
    '                "LEFT JOIN hremployee_master AS Trainer ON hrtraining_trainers_tran.employeeunkid = Trainer.employeeunkid " & _
    '            "WHERE ISNULL(hrtraining_analysis_master.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 AND ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0 " & _
    '            "AND ISNULL(hrtraining_analysis_tran.isvoid,0) = 0 "
    '        'S.SANDEEP [ 29 JUNE 2011 ] -- END 



    '        'Sandeep [ 16 Oct 2010 ] -- End 

    '        'Sandeep [ 02 Oct 2010 ] -- End 

    '        If intEnrollId > 0 Then
    '            strQ &= "AND hrtraining_analysis_master.trainingenrolltranunkid = @EnrollId "
    '            objDataOperation.AddParameter("@EnrollId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEnrollId)
    '        End If


    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        'End If

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        '        End If
    '        'End Select
    '        strQ &= UserAccessLevel._AccessLevelFilterString
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        'S.SANDEEP [ 04 FEB 2012 ] -- END


    '        'Anjan (24 Jun 2011)-End 


    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_analysis_master) </purpose>
    Public Function Insert(Optional ByVal dtAnalysisTran As DataTable = Nothing, Optional ByVal isFinalAnalysis As Boolean = False) As Boolean
        If isFinalAnalysis = False Then
            If isExist(mintTrainingenrolltranunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Analysis for the seleted trainee is already present.Please select different trainee.")
                Return False
            End If
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@trainingenrolltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingenrolltranunkid.ToString)
            'Sandeep [ 02 Oct 2010 ] -- Start
            'objDataOperation.AddParameter("@analysis_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAnalysis_Date)
            'Sandeep [ 02 Oct 2010 ] -- End 
            objDataOperation.AddParameter("@trainee_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrainee_Remark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserinkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoidatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscomplete.ToString)

            'Sandeep [ 02 Oct 2010 ] -- Start
            'strQ = "INSERT INTO hrtraining_analysis_master ( " & _
            '                      "  trainingenrolltranunkid " & _
            '                      ", analysis_date " & _
            '                      ", trainee_remark " & _
            '                      ", userunkid " & _
            '                      ", isvoid " & _
            '                      ", voiddatetime " & _
            '                      ", voiduserunkid " & _
            '                      ", voidreason" & _
            '                      ", iscomplete" & _
            '                    ") VALUES (" & _
            '                      "  @trainingenrolltranunkid " & _
            '                      ", @analysis_date " & _
            '                      ", @trainee_remark " & _
            '                      ", @userunkid " & _
            '                      ", @isvoid " & _
            '                      ", @voiddatetime " & _
            '                      ", @voiduserunkid " & _
            '                      ", @voidreason" & _
            '                      ", @iscomplete" & _
            '                    "); SELECT @@identity"
            strQ = "INSERT INTO hrtraining_analysis_master ( " & _
                              "  trainingenrolltranunkid " & _
                              ", trainee_remark " & _
                              ", userunkid " & _
                              ", isvoid " & _
                              ", voiddatetime " & _
                              ", voiduserunkid " & _
                              ", voidreason" & _
                              ", iscomplete" & _
                         ") VALUES (" & _
                              "  @trainingenrolltranunkid " & _
                              ", @trainee_remark " & _
                              ", @userunkid " & _
                              ", @isvoid " & _
                              ", @voiddatetime " & _
                              ", @voiduserunkid " & _
                              ", @voidreason" & _
                              ", @iscomplete" & _
                        "); SELECT @@identity"
            'Sandeep [ 02 Oct 2010 ] -- End 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAnalysisunkid = dsList.Tables(0).Rows(0).Item(0)

            Dim blnFlag As Boolean = False

            If dtAnalysisTran IsNot Nothing Then
                objAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                objAnalysisTran._DataTable = dtAnalysisTran
                blnFlag = objAnalysisTran.InsertUpdateDelete_AnalysisTran()
                If blnFlag = False Then objDataOperation.ReleaseTransaction(True)
            End If

            If isFinalAnalysis = True Then
                strQ = "UPDATE hrtraining_enrollment_tran SET " & _
                        " status_id = 3 " & _
                       "WHERE hrtraining_enrollment_tran.trainingenrolltranunkid = " & mintTrainingenrolltranunkid

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrtraining_enrollment_tran", "trainingenrolltranunkid", mintTrainingenrolltranunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_analysis_master) </purpose>
    Public Function Update(Optional ByVal dtAnalysisTran As DataTable = Nothing, Optional ByVal isFinalAnalysis As Boolean = False) As Boolean
        If isFinalAnalysis = False Then
            If isExist(mintTrainingenrolltranunkid, mintAnalysisunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Analysis for the seleted trainee is already present.Please select different trainee.")
                Return False
            End If
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            objDataOperation.AddParameter("@trainingenrolltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingenrolltranunkid.ToString)
            'Sandeep [ 02 Oct 2010 ] -- Start
            'objDataOperation.AddParameter("@analysis_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAnalysis_Date)
            'Sandeep [ 02 Oct 2010 ] -- End 
            objDataOperation.AddParameter("@trainee_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrainee_Remark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserinkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoidatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscomplete.ToString)

            'Sandeep [ 02 Oct 2010 ] -- Start
            'strQ = "UPDATE hrtraining_analysis_master SET " & _
            '                      "  trainingenrolltranunkid = @trainingenrolltranunkid" & _
            '                      ", analysis_date = @analysis_date" & _
            '                      ", trainee_remark = @trainee_remark" & _
            '                      ", userunkid = @userunkid" & _
            '                      ", isvoid = @isvoid" & _
            '                      ", voiddatetime = @voiddatetime" & _
            '                      ", voiduserunkid = @voiduserunkid" & _
            '                      ", voidreason = @voidreason " & _
            '                      ", iscomplete = @iscomplete " & _
            '                    "WHERE analysisunkid = @analysisunkid "
            strQ = "UPDATE hrtraining_analysis_master SET " & _
                            "  trainingenrolltranunkid = @trainingenrolltranunkid" & _
                            ", trainee_remark = @trainee_remark" & _
                            ", userunkid = @userunkid" & _
                            ", isvoid = @isvoid" & _
                            ", voiddatetime = @voiddatetime" & _
                            ", voiduserunkid = @voiduserunkid" & _
                            ", voidreason = @voidreason " & _
                            ", iscomplete = @iscomplete " & _
                       "WHERE analysisunkid = @analysisunkid "
            'Sandeep [ 02 Oct 2010 ] -- End 

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean = False

            If dtAnalysisTran IsNot Nothing Then
                objAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                objAnalysisTran._DataTable = dtAnalysisTran
                blnFlag = objAnalysisTran.InsertUpdateDelete_AnalysisTran()
                If blnFlag = False Then objDataOperation.ReleaseTransaction(True)
            End If

            If isFinalAnalysis = True Then
                strQ = "UPDATE hrtraining_enrollment_tran SET " & _
                        " status_id = 3 " & _
                           "WHERE hrtraining_enrollment_tran.trainingenrolltranunkid = " & mintTrainingenrolltranunkid

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrtraining_enrollment_tran", "trainingenrolltranunkid", mintTrainingenrolltranunkid) Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_analysis_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrtraining_analysis_master SET " & _
                      " isvoid = @isvoid" & _
                      ",voiddatetime = @voiddatetime" & _
                      ",voiduserunkid = @voiduserunkid" & _
                      ",voidreason = @voidreason " & _
                    "WHERE trainingenrolltranunkid = @trainingenrolltranunkid "

            objDataOperation.AddParameter("@trainingenrolltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT analysisunkid FROM hrtraining_analysis_master WHERE trainingenrolltranunkid = @trainingenrolltranunkid "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean = False
            ''''''''''''''''' Voiding All Data

            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            'objAnalysisTran._AnalysisUnkid = CInt(dsList.Tables(0).Rows(0)(0))
            'blnFlag = objAnalysisTran.Void_All(mblnIsvoid, mdtVoidatetime, mstrVoidreason, mintVoiduserunkid)
            'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    objAnalysisTran._AnalysisUnkid = CInt(dsList.Tables(0).Rows(0)(0))
            '    blnFlag = objAnalysisTran.Void_All(mblnIsvoid, mdtVoidatetime, mstrVoidreason, mintVoiduserunkid)
            '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            'End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dTran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrtraining_analysis_tran", "analysisunkid", dsList.Tables(0).Rows(0)(0))
                If dTran.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In dTran.Tables(0).Rows
                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_analysis_master", "analysisunkid", intUnkid, "hrtraining_analysis_tran", "analysistranunkid", dRow.Item("analysistranunkid"), 3, 3) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If

                objAnalysisTran._AnalysisUnkid = CInt(dsList.Tables(0).Rows(0)(0))
                blnFlag = objAnalysisTran.Void_All(mblnIsvoid, mdtVoidatetime, mstrVoidreason, mintVoiduserunkid)
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            'Sandeep [ 09 Oct 2010 ] -- End 
            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEnrollId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sandeep [ 02 Oct 2010 ] -- Start
            'strQ = "SELECT " & _
            '                    "  analysisunkid " & _
            '                    ", trainingenrolltranunkid " & _
            '                    ", analysis_date " & _
            '                    ", trainee_remark " & _
            '                    ", userunkid " & _
            '                    ", isvoid " & _
            '                    ", voiddatetime " & _
            '                    ", voiduserunkid " & _
            '                    ", voidreason " & _
            '                    ", iscomplete " & _
            '                   "FROM hrtraining_analysis_master " & _
            '                   "WHERE trainingenrolltranunkid = @EnrollId "
            strQ = "SELECT " & _
                            "  analysisunkid " & _
                            ", trainingenrolltranunkid " & _
                            ", trainee_remark " & _
                            ", userunkid " & _
                            ", isvoid " & _
                            ", voiddatetime " & _
                            ", voiduserunkid " & _
                            ", voidreason " & _
                            ", iscomplete " & _
                       "FROM hrtraining_analysis_master " & _
                       "WHERE trainingenrolltranunkid = @EnrollId " & _
                       " AND isvoid = 0 "
            'Sandeep [ 02 Oct 2010 ] -- End 

                objDataOperation.AddParameter("@EnrollId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEnrollId)

            If intUnkid > 0 Then
                strQ &= " AND analysisunkid <> @analysisunkid"
            End If

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Get_Analysis_Data(Optional ByVal strListName As String = "List", Optional ByVal intEnrollId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            'S.SANDEEP [ 27 FEB 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''Sandeep [ 02 Oct 2010 ] -- Start
            ''strQ = "SELECT " & _
            ''        "	 CONVERT(CHAR(8),hrtraining_analysis_master.analysis_date,112) AS Date " & _
            ''        "	,CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN ISNULL(hrtraining_trainers_tran.other_name,'') " & _
            ''        "		  WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ " & _
            ''        "															   ISNULL(hremployee_master.othername,'')+' '+ " & _
            ''        "															   ISNULL(hremployee_master.surname,'') " & _
            ''        "	 END AS Reviewer " & _
            ''        "	,hrresult_master.resultname AS Score " & _
            ''        "	,hrtraining_analysis_tran.reviewer_remark " & _
            ''        "	,hrtraining_analysis_tran.analysistranunkid " & _
            ''        "	,hrtraining_analysis_master.analysisunkid " & _
            ''        "	,hrresult_master.resultunkid " & _
            ''        "FROM hrtraining_analysis_tran " & _
            ''        "	LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
            ''        "	LEFT JOIN hremployee_master ON hrtraining_trainers_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''        "	LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            ''        "	LEFT JOIN hrtraining_analysis_master ON hrtraining_analysis_tran.analysisunkid = hrtraining_analysis_master.analysisunkid " & _
            ''        "WHERE ISNULL(hrtraining_analysis_tran.isvoid,0)=0 " & _
            ''        "	AND ISNULL(hrtraining_analysis_master.isvoid,0)=0 "

            ''S.SANDEEP [ 29 JUNE 2011 ] -- START
            ''ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            ''strQ = "SELECT " & _
            ''           "	 CONVERT(CHAR(8),hrtraining_analysis_tran.analysis_date,112) AS Date " & _
            ''           "	,CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN ISNULL(hrtraining_trainers_tran.other_name,'') " & _
            ''           "		  WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ " & _
            ''           "															   ISNULL(hremployee_master.othername,'')+' '+ " & _
            ''           "															   ISNULL(hremployee_master.surname,'') " & _
            ''           "	 END AS Reviewer " & _
            ''           "	,hrresult_master.resultname AS Score " & _
            ''           "	,hrtraining_analysis_tran.reviewer_remark " & _
            ''           "	,hrtraining_analysis_tran.analysistranunkid " & _
            ''           "	,hrtraining_analysis_master.analysisunkid " & _
            ''           "	,hrresult_master.resultunkid " & _
            ''           "FROM hrtraining_analysis_tran " & _
            ''           "	LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
            ''           "	LEFT JOIN hremployee_master ON hrtraining_trainers_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''           "	LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            ''           "	LEFT JOIN hrtraining_analysis_master ON hrtraining_analysis_tran.analysisunkid = hrtraining_analysis_master.analysisunkid " & _
            ''           "WHERE ISNULL(hrtraining_analysis_tran.isvoid,0)=0 " & _
            ''           "	AND ISNULL(hrtraining_analysis_master.isvoid,0)=0 "

            'strQ = "SELECT " & _
            '           "	 CONVERT(CHAR(8),hrtraining_analysis_tran.analysis_date,112) AS Date " & _
            '           "	,CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN ISNULL(hrtraining_trainers_tran.other_name,'') " & _
            '           "		  WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ " & _
            '           "															   ISNULL(hremployee_master.othername,'')+' '+ " & _
            '           "															   ISNULL(hremployee_master.surname,'') " & _
            '           "	 END AS Reviewer " & _
            '           "	,hrresult_master.resultname AS Score " & _
            '           "	,hrtraining_analysis_tran.reviewer_remark " & _
            '           "	,hrtraining_analysis_tran.analysistranunkid " & _
            '           "	,hrtraining_analysis_master.analysisunkid " & _
            '           "	,hrresult_master.resultunkid " & _
            '           "FROM hrtraining_analysis_tran " & _
            '           "	LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
            '       "	LEFT JOIN hremployee_master ON hrtraining_trainers_tran.employeeunkid = hremployee_master.employeeunkid "
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    'Sohail (06 Jan 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    'Sohail (06 Jan 2012) -- End
            'End If
            'strQ &= "	LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '           "	LEFT JOIN hrtraining_analysis_master ON hrtraining_analysis_tran.analysisunkid = hrtraining_analysis_master.analysisunkid " & _
            '           "WHERE ISNULL(hrtraining_analysis_tran.isvoid,0)=0 " & _
            '           "	AND ISNULL(hrtraining_analysis_master.isvoid,0)=0 "
            ''S.SANDEEP [ 29 JUNE 2011 ] -- END 


            ''Sandeep [ 02 Oct 2010 ] -- End 
            'If intEnrollId > 0 Then
            '    strQ &= "AND hrtraining_analysis_master.trainingenrolltranunkid = " & intEnrollId
            'End If

            
            ''Anjan (24 Jun 2011)-Start
            ''Issue : According to privilege that lower level user should not see superior level employees.

            ''S.SANDEEP [ 04 FEB 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            ''End If
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            'End If
            'End Select
            ''S.SANDEEP [ 04 FEB 2012 ] -- END

            ''Anjan (24 Jun 2011)-End 


            ''Sandeep [ 02 Oct 2010 ] -- Start
            ''strQ &= " ORDER BY CONVERT(CHAR(8),hrtraining_analysis_master.analysis_date,112) "
            'strQ &= " ORDER BY CONVERT(CHAR(8),hrtraining_analysis_tran.analysis_date,112) "
            ''Sandeep [ 02 Oct 2010 ] -- End

            strQ &= "SELECT " & _
                    " hrtraining_analysis_master.analysisunkid " & _
                    ",resultunkid " & _
                    ",gpa_value " & _
                    "FROM hrtraining_analysis_tran " & _
                    "   JOIN hrtraining_analysis_master ON hrtraining_analysis_tran.analysisunkid = hrtraining_analysis_master.analysisunkid " & _
                    "WHERE hrtraining_analysis_master.isvoid = 0 AND hrtraining_analysis_tran.isvoid = 0 " & _
                    " AND trainingenrolltranunkid = '" & intEnrollId & "' "
            'S.SANDEEP [ 27 FEB 2012 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Analysis_Data", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function IsAlreadyQualified(ByVal intQualifiyId As Integer, ByVal intEmpId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                    "	employeeunkid " & _
                    "  ,qualificationunkid " & _
                   "FROM hremp_qualification_tran " & _
                   "WHERE ISNULL(isvoid,0) = 0 " & _
                   "AND employeeunkid = @EmpId AND qualificationunkid = @QufId "

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@QufId", SqlDbType.Int, eZeeDataType.INT_SIZE, intQualifiyId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsAlreadyQualified", mstrModuleName)
            Return True
        End Try
    End Function

    'S.SANDEEP [19-MAY-2017] -- START
    Public Sub SendNotification(ByVal intTrainingSchedulingId As Integer, _
                                ByVal intEmployeeId As Integer, _
                                ByVal mdtEmpAsOnDate As DateTime, _
                                ByVal strRemark As String, ByVal xLoginMod As enLogin_Mode, ByVal xUserId As Integer, ByVal xSenderName As String, ByVal intCompanyUnkId As Integer)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Dim strMessage As New System.Text.StringBuilder
        Dim objEmp As New clsEmployee_Master : Dim objTrSchedule As New clsTraining_Scheduling : Dim objCMaster As New clsCommon_Master
        Try
            objEmp._Employeeunkid(mdtEmpAsOnDate) = intEmployeeId
            objTrSchedule._Trainingschedulingunkid = intTrainingSchedulingId
            objCMaster._Masterunkid = objTrSchedule._Course_Title

            strMessage.Append("<HTML> <BODY>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>, <BR><BR>")
            strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & getTitleCase(objEmp._Firstname & " " & objEmp._Surname) & "</B>, <BR><BR>")
            'Gajanan [27-Mar-2019] -- End

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 101, "This is to inform you that, you have completed your training. Please find the details below :") & "<BR><BR>")
            strMessage.Append(Language.getMessage(mstrModuleName, 101, "This is to inform you that, you have completed your training. Please find the details below :") & "<BR><BR>")
            'Gajanan [27-Mar-2019] -- End
            strMessage.Append("<TABLE border = '1'>")
            strMessage.Append("<TR bgcolor= 'SteelBlue'>")
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 102, "Course Title") & "</span></b></TD>")
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 103, "Duration") & "</span></b></TD>")
            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 104, "Remark") & "</span></b></TD>")
            strMessage.Append("</TR>")
            strMessage.Append("<TR>" & vbCrLf)
            strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & objCMaster._Name & "</span></TD>")
            strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & objTrSchedule._Start_Date.ToShortDateString & " - " & objTrSchedule._End_Date.ToShortDateString & "<BR> </span></TD>")
            strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & strRemark & "</span></TD>")
            strMessage.Append("</TR>" & vbCrLf)
            strMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End

            strMessage.Append("</span></p>")
            strMessage.Append("</BODY></HTML>")

            If strMessage.ToString.Trim.Length > 0 Then
                Dim objSendMail As New clsSendMail
                objSendMail._ToEmail = objEmp._Email
                objSendMail._Subject = Language.getMessage(mstrModuleName, 105, "Notification of Training Completion")
                objSendMail._Message = strMessage.ToString
                objSendMail._Form_Name = ""
                objSendMail._LogEmployeeUnkid = 0
                objSendMail._OperationModeId = xLoginMod
                objSendMail._UserUnkid = xUserId
                objSendMail._SenderAddress = xSenderName
                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                Try
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objSendMail.SendMail()
                    objSendMail.SendMail(intCompanyUnkId)
                    'Sohail (30 Nov 2017) -- End
                Catch ex As Exception
                End Try
                objSendMail = Nothing
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [19-MAY-2017] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Analysis for the seleted trainee is already present.Please select different trainee.")
			Language.setMessage(mstrModuleName, 100, "Dear")
			Language.setMessage(mstrModuleName, 101, "This is to inform you that, you have completed your training. Please find the details below :")
			Language.setMessage(mstrModuleName, 102, "Course Title")
			Language.setMessage(mstrModuleName, 103, "Duration")
			Language.setMessage(mstrModuleName, 104, "Remark")
			Language.setMessage(mstrModuleName, 105, "Notification of Training Completion")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class