﻿'************************************************************************************************************************************
'Class Name : clseval_subquestion_master.vb
'Purpose    :
'Date       :08-07-2021
'Written By :Gajanan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Gajanan
''' </summary>
Public Class clseval_subquestion_master
    Private Shared ReadOnly mstrModuleName As String = "clseval_subquestion_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSubquestionid As Integer
    Private mintCategoryid As Integer
    Private mintQuestionid As Integer
    Private mstrSubquestion As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set subquestionid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Subquestionid() As Integer
        Get
            Return mintSubquestionid
        End Get
        Set(ByVal value As Integer)
            mintSubquestionid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Categoryid() As Integer
        Get
            Return mintCategoryid
        End Get
        Set(ByVal value As Integer)
            mintCategoryid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set questionid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Questionid() As Integer
        Get
            Return mintQuestionid
        End Get
        Set(ByVal value As Integer)
            mintQuestionid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set subquestion
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Subquestion() As String
        Get
            Return mstrSubquestion
        End Get
        Set(ByVal value As String)
            mstrSubquestion = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property


    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  subquestionid " & _
              ", categoryid " & _
              ", questionid " & _
              ", subquestion " & _
              ", description " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM treval_subquestion_master " & _
             "WHERE subquestionid = @subquestionid "

            objDataOperation.AddParameter("@subquestionid", SqlDbType.int, eZeeDataType.INT_SIZE, mintSubquestionid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintsubquestionid = CInt(dtRow.Item("subquestionid"))
                mintcategoryid = CInt(dtRow.Item("categoryid"))
                mintquestionid = CInt(dtRow.Item("questionid"))
                mstrsubquestion = dtRow.Item("subquestion").ToString
                mstrdescription = dtRow.Item("description").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intQuestionid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   " subquestionid " & _
                   ",treval_subquestion_master.categoryid " & _
                   ",treval_subquestion_master.questionid " & _
                   ",subquestion " & _
                   ",treval_subquestion_master.description " & _
                   ",treval_subquestion_master.isvoid " & _
                   ",ISNULL(treval_group_master.categoryname,'') as Category " & _
                   ",ISNULL(treval_question_master.question,'') as Question " & _
                   ",ISNULL(treval_question_master.answertype,0) as AnsType " & _
                   ",ISNULL(treval_question_master.isAskforJustification, 0) AS isAskforJustification " & _
                   ",ISNULL(treval_question_master.isforlinemanager, 0) AS isforlinemanager " & _
                "FROM treval_subquestion_master " & _
                "LEFT JOIN treval_group_master " & _
                     "ON treval_group_master.categoryid = treval_subquestion_master.categoryid " & _
                "LEFT JOIN treval_question_master " & _
                     "ON treval_question_master.questionnaireId = treval_subquestion_master.questionid WHERE 1=1 "

            'Hemant (20 Aug 2021) -- [isforlinemanager]
            If blnOnlyActive Then
                strQ &= " and treval_subquestion_master.isvoid = 0 "
            End If

            If intQuestionid > 0 Then
                strQ &= " and treval_subquestion_master.questionid = @questionid "
                objDataOperation.AddParameter("@questionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (treval_subquestion_master) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByRef intNewUnkId As Integer = 0) As Boolean
        If isExist(mstrSubquestion, -1, xDataOpr) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This sub-question is already defined. Please define new sub-question.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryid.ToString)
            objDataOperation.AddParameter("@questionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionid.ToString)
            objDataOperation.AddParameter("@subquestion", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrSubquestion.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "INSERT INTO treval_subquestion_master ( " & _
              "  categoryid " & _
              ", questionid " & _
              ", subquestion " & _
              ", description " & _
              ", isvoid" & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
            ") VALUES (" & _
              "  @categoryid " & _
              ", @questionid " & _
              ", @subquestion " & _
              ", @description " & _
              ", @isvoid" & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSubquestionid = dsList.Tables(0).Rows(0).Item(0)

            intNewUnkId = mintSubquestionid

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintSubquestionid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (treval_subquestion_master) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mstrSubquestion, mintSubquestionid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This sub-question is already defined. Please define new sub-question.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubquestionid.ToString)
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryid.ToString)
            objDataOperation.AddParameter("@questionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionid.ToString)
            objDataOperation.AddParameter("@subquestion", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrSubquestion.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            strQ = "UPDATE treval_subquestion_master SET " & _
              "  categoryid = @categoryid" & _
              ", questionid = @questionid" & _
              ", subquestion = @subquestion" & _
              ", description = @description" & _
              ", isvoid = @isvoid " & _
            "WHERE subquestionid = @subquestionid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintSubquestionid, objDataOperation) = False Then
                If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintSubquestionid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (treval_subquestion_master) </purpose>
    Public Function Delete(ByVal intQuestionnaireId As Integer, ByVal intSubQuestionId As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objEval As New clseval_answer_master
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            With objEval
                ._Isvoid = mblnIsvoid
                ._Voiduserunkid = mintVoiduserunkid
                ._Voiddatetime = mdtVoiddatetime
                ._Voidreason = mstrVoidreason
                ._IsWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
                ._ClientIP = mstrClientIP
                ._FormName = mstrFormName
                ._HostName = mstrHostName
                .VoidAllByMasterUnkID(intQuestionnaireId, 3, objDataOperation, intSubQuestionId)
            End With

            strQ = "UPDATE treval_subquestion_master SET " & _
                  " isvoid = @isvoid " & _
                  ", voiduserunkid = @voiduserunkid" & _
                  ", voiddatetime = @voiddatetime" & _
                  ", voidreason = @voidreason " & _
                "WHERE subquestionid = @subquestionid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSubQuestionId)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, intSubQuestionId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            Dim Tables() As String = {"trtraining_evaluation_tran"}
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each value As String In Tables
                Select Case value
                    Case "trtraining_evaluation_tran"
                        strQ = "SELECT subquestionid FROM " & value & " WHERE isvoid = 0 AND subquestionid = @subquestionid  "

                    Case Else
                End Select

                If strQ.Trim.Length > 0 Then

                    dsList = objDataOperation.ExecQuery(strQ, "Used")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables("Used").Rows.Count > 0 Then
                        Return True
                        Exit For
                    End If
                End If
            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  subquestionid " & _
              ", categoryid " & _
              ", questionid " & _
              ", subquestion " & _
             "FROM treval_subquestion_master " & _
             "WHERE  isvoid = 0 AND subquestion = @subquestion "

            If intUnkid > 0 Then
                strQ &= " AND subquestionid <> @subquestionid"
            End If

            objDataOperation.AddParameter("@subquestion", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType, ByVal intUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Try

            StrQ = "INSERT INTO attreval_subquestion_master ( " & _
                    "  tranguid " & _
                    ", subquestionid  " & _
                    ", categoryid  " & _
                    ", questionid  " & _
                    ", subquestion  " & _
                    ", description  " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", form_name " & _
                    ", ip " & _
                    ", hostname " & _
                    ", isweb" & _
                  ") select " & _
                    "  LOWER(NEWID()) " & _
                    ", subquestionid  " & _
                    ", categoryid  " & _
                    ", questionid  " & _
                    ", subquestion  " & _
                    ", description  " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", GETDATE() " & _
                    ", @form_name " & _
                    ", @ip " & _
                    ", @hostname " & _
                    ", @isweb" & _
                    "  From treval_subquestion_master  WHERE 1=1 AND treval_subquestion_master.subquestionid  = @subquestionid  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@subquestionid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strQ = "SELECT TOP 1 * FROM attreval_subquestion_master WHERE subquestionid = @subquestionid AND audittype <> 3 ORDER BY auditdatetime DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@subquestionid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("subquestionid").ToString() = mintSubquestionid AndAlso dr("categoryid").ToString() = mintCategoryid _
                    AndAlso dr("questionid").ToString() = mintQuestionid AndAlso dr("subquestion").ToString() = mstrSubquestion _
                    AndAlso dr("description").ToString() = mstrDescription _
                    Then

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, This sub-question is already defined. Please define new sub-question.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class