﻿'************************************************************************************************************************************
'Class Name : clsTraining_Approval_Matrix
'Purpose    :
'Date       : 02-Feb-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsTraining_Approval_Matrix
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Approval_Matrix"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintTrainingApprovalMatrixunkid As Integer
    Private mintLevelunkid As Integer
    Private mintCalendarunkid As Integer
    Private mdecCostAmountFrom As Decimal
    Private mdecCostAmountTo As Decimal
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
#End Region

#Region " Public variables "

    Public pintTrainingApprovalMatrixunkid As Integer
    Public pintLevelunkid As Integer
    Public pintCalendarunkid As Integer
    Public pdecCostAmountFrom As Decimal
    Public pdecCostAmountTo As Decimal
    Public pblnIsvoid As Boolean
    Public pintVoiduserunkid As Integer
    Public pdtVoiddatetime As Date
    Public pstrVoidreason As String = String.Empty
    Public pstrHostName As String = ""
    Public pstrClientIP As String = ""
    Public pintAuditUserId As Integer = 0
    Public pblnIsWeb As Boolean = False
    Public pstrFormName As String = ""
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Private mDataOp As clsDataOperation = Nothing
    Public Property _DataOp() As clsDataOperation
        Get
            Return mDataOp
        End Get
        Set(ByVal value As clsDataOperation)
            mDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingapprovalmatrixunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingApprovalMatrixunkid() As Integer
        Get
            Return mintTrainingApprovalMatrixunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingApprovalMatrixunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calendarunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Calendarunkid() As Integer
        Get
            Return mintCalendarunkid
        End Get
        Set(ByVal value As Integer)
            mintCalendarunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costamountfrom
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CostAmountFrom() As Decimal
        Get
            Return mdecCostAmountFrom
        End Get
        Set(ByVal value As Decimal)
            mdecCostAmountFrom = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costamountto
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CostAmountTo() As Decimal
        Get
            Return mdecCostAmountTo
        End Get
        Set(ByVal value As Decimal)
            mdecCostAmountTo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mDataOp IsNot Nothing Then
            objDataOperation = mDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  trainingapprovalmatrixunkid " & _
                      ", levelunkid " & _
                      ", calendarunkid " & _
                      ", costamountfrom " & _
                      ", costamountto " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                 "FROM trtraining_Approval_Matrix " & _
                 "WHERE trainingapprovalmatrixunkid = @trainingapprovalmatrixunkid "

            objDataOperation.AddParameter("@trainingapprovalmatrixunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingApprovalMatrixunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTrainingApprovalMatrixunkid = CInt(dtRow.Item("trainingapprovalmatrixunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintCalendarunkid = CInt(dtRow.Item("calendarunkid"))
                mdecCostAmountFrom = CDec(dtRow.Item("costamountfrom"))
                mdecCostAmountTo = CDec(dtRow.Item("costamountto"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal intCalendarId As Integer = 0 _
                            , Optional ByVal strFilter As String = "" _
                            , Optional ByVal strOrderBy As String = "" _
                            , Optional ByVal intStatusID As Integer = 0 _
                            ) As DataSet
        'Hemant (03 Dec 2021) -- [intStatusID]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  trtraining_Approval_Matrix.trainingapprovalmatrixunkid " & _
                          ", trtraining_Approval_Matrix.levelunkid " & _
                          ", hrtraining_approverlevel_master.levelcode " & _
                          ", hrtraining_approverlevel_master.levelname AS approver_level " & _
                          ", hrtraining_approverlevel_master.levelname1 " & _
                          ", hrtraining_approverlevel_master.levelname2 " & _
                          ", hrtraining_approverlevel_master.priority " & _
                          ", trtraining_Approval_Matrix.calendarunkid " & _
                          ", ISNULL(trtraining_calendar_master.calendar_name, '') AS calendar_name" & _
                          ", trtraining_Approval_Matrix.costamountfrom " & _
                          ", trtraining_Approval_Matrix.costamountto " & _
                          ", trtraining_Approval_Matrix.isvoid " & _
                          ", trtraining_Approval_Matrix.voiduserunkid " & _
                          ", trtraining_Approval_Matrix.voiddatetime " & _
                          ", trtraining_Approval_Matrix.voidreason " & _
                    "FROM    trtraining_Approval_Matrix " & _
                          " LEFT JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trtraining_Approval_Matrix.calendarunkid " & _
                          " LEFT JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = trtraining_Approval_Matrix.levelunkid " & _
                    "WHERE   ISNULL(trtraining_Approval_Matrix.isvoid, 0) = 0 " & _
                            "AND hrtraining_approverlevel_master.isactive = 1 "

            If intCalendarId > 0 Then
                strQ &= " AND trtraining_Approval_Matrix.calendarunkid = @calendarunkid "
                objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalendarId)
            End If

            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            If intStatusID > 0 Then
                strQ &= "AND statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If
            'Hemant (03 Dec 2021) -- End

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            If strOrderBy.Trim <> "" Then
                strQ &= " ORDER BY " & strOrderBy
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtraining_Approval_Matrix) </purpose>
    Public Function Insert(Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mintCalendarunkid, mintLevelunkid, , objDataOp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver is already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        End If


        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@costamountfrom", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCostAmountFrom.ToString)
            objDataOperation.AddParameter("@costamountto", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCostAmountTo.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO trtraining_Approval_Matrix ( " & _
                      "  levelunkid " & _
                      ", calendarunkid " & _
                      ", costamountfrom " & _
                      ", costamountto " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                  ") VALUES (" & _
                      "  @levelunkid " & _
                      ", @calendarunkid " & _
                      ", @costamountfrom " & _
                      ", @costamountto " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingApprovalMatrixunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True

        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtraining_Approval_Matrix) </purpose>
    Public Function Update() As Boolean
        If isExist(mintCalendarunkid, mintLevelunkid, mintTrainingApprovalMatrixunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver is already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@trainingapprovalmatrixunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingApprovalMatrixunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@costamountfrom", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCostAmountFrom.ToString)
            objDataOperation.AddParameter("@costamountto", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCostAmountTo.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE trtraining_Approval_Matrix SET " & _
                      "  levelunkid = @levelunkid " & _
                      ", calendarunkid = @calendarunkid " & _
                      ", costamountfrom = @costamountfrom " & _
                      ", costamountto = @costamountto " & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE trainingapprovalmatrixunkid = @trainingapprovalmatrixunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtraining_Approval_Matrix) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If
            objDataOperation.ClearParameters()

        Try
            mDataOp = objDataOperation
            _TrainingApprovalMatrixunkid = CInt(intUnkid)
            mblnIsvoid = pblnIsvoid
            mintVoiduserunkid = pintVoiduserunkid
            mdtVoiddatetime = pdtVoiddatetime
            mstrVoidreason = pstrVoidreason
            mintAuditUserId = pintAuditUserId
            mstrFormName = pstrFormName
            mstrClientIP = pstrClientIP
            mstrHostName = pstrHostName
            mblnIsWeb = pblnIsWeb

            strQ = "UPDATE trtraining_Approval_Matrix SET " & _
                       "  isvoid = @isvoid" & _
                       " ,voiduserunkid = @voiduserunkid" & _
                       " ,voiddatetime = @voiddatetime" & _
                       " ,voidreason = @voidreason " & _
                   "WHERE trainingapprovalmatrixunkid = @trainingapprovalmatrixunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@trainingapprovalmatrixunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            If objDataOp Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intApproverUserUnkID As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = " "

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intCalendarId As Integer, ByVal intLevelId As Integer, Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                       "  trainingapprovalmatrixunkid " & _
                       " ,levelunkid " & _
                       " ,calendarunkid " & _
                       " ,costamountfrom  " & _
                       " ,costamountto " & _
                       " ,isvoid " & _
                       " ,voiduserunkid " & _
                       " ,voiddatetime " & _
                       " ,voidreason " & _
                   "FROM trtraining_Approval_Matrix " & _
                   "WHERE ISNULL(isvoid, 0) = 0 " & _
                       " AND calendarunkid = @calendarunkid " & _
                       " AND levelunkid = @levelunkid "



            If intUnkid > 0 Then
                strQ &= " AND trainingapprovalmatrixunkid <> @trainingapprovalmatrixunkid"
            End If

            objDataOperation.AddParameter("@calendarunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCalendarId)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intLevelId)
            objDataOperation.AddParameter("@trainingapprovalmatrixunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO attrtraining_Approval_Matrix ( " & _
                        "  tranguid " & _
                        ", trainingapprovalmatrixunkid " & _
                        ", levelunkid " & _
                        ", calendarunkid " & _
                        ", costamountfrom " & _
                        ", costamountto " & _
                        ", audittypeid " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", formname " & _
                        ", ip " & _
                        ", host " & _
                        ", isweb" & _
                   ") VALUES (" & _
                        "  LOWER(NEWID()) " & _
                        ", @trainingapprovalmatrixunkid " & _
                        ", @levelunkid " & _
                        ", @calendarunkid " & _
                        ", @costamountfrom " & _
                        ", @costamountto " & _
                        ", @audittypeid " & _
                        ", @audituserunkid " & _
                        ", GETDATE() " & _
                        ", @formname " & _
                        ", @ip " & _
                        ", @host " & _
                        ", @isweb" & _
                    ")"

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@trainingapprovalmatrixunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingApprovalMatrixunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@costamountfrom", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCostAmountFrom.ToString)
            objDataOperation.AddParameter("@costamountto", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCostAmountTo.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtraining_Approval_Matrix) </purpose>
    Public Function DeleteByCalendarUnkid(ByVal intCalendarid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        End If

        Try
            strQ = "INSERT INTO attrtraining_Approval_Matrix ( " & _
                       "  tranguid " & _
                       ", trainingapprovalmatrixunkid " & _
                       ", levelunkid " & _
                       ", calendarunkid " & _
                       ", costamountfrom " & _
                       ", costamountto " & _
                       ", audittypeid " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", formname " & _
                       ", ip " & _
                       ", host " & _
                       ", isweb ) " & _
                  " SELECT " & _
                       "  LOWER(NEWID()) " & _
                       ", trainingapprovalmatrixunkid " & _
                       ", levelunkid " & _
                       ", calendarunkid " & _
                       ", costamountfrom " & _
                       ", costamountto " & _
                       ", 3 " & _
                       ", @audituserunkid " & _
                       ", GETDATE() " & _
                       ", @formname " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @isweb" & _
                   " From trtraining_Approval_Matrix " & _
                    "WHERE isvoid = 0 " & _
               "AND calendarunkid = @calendarunkid "


            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalendarid.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE trtraining_Approval_Matrix SET " & _
                       "  isvoid = @isvoid" & _
                       " ,voiduserunkid = @voiduserunkid" & _
                       " ,voiddatetime = @voiddatetime" & _
                       " ,voidreason = @voidreason " & _
                   "WHERE calendarunkid = @calendarunkid "

            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalendarid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteByCalendarUnkid; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            If objDataOp Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function getLevelByCostAmount(ByVal decCostAmount As Decimal) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Dim intPeriodID As Integer = 0
        Try

            strQ = "SELECT TOP 1 " & _
                      "  trainingapprovalmatrixunkid " & _
                      ", levelunkid " & _
                      ", calendarunkid " & _
                      ", costamountfrom " & _
                      ", costamountto " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                    "FROM trtraining_approval_matrix " & _
                    "WHERE isvoid = 0 "

            If decCostAmount >= 0 Then
                strQ &= "AND costamountfrom <= @costamountfrom "
                objDataOperation.AddParameter("@costamountfrom", SqlDbType.Int, eZeeDataType.INT_SIZE, decCostAmount)
            End If

            strQ &= " ORDER BY enddate DESC "


            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getLevelByCostAmount", mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
        End Try
    End Function
End Class
