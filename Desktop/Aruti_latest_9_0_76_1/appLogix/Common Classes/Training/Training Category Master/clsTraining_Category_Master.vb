﻿'************************************************************************************************************************************
'Class Name : clsTraining_Category_Master
'Purpose    :
'Date       : 01-Feb-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsTraining_Category_Master
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Category_Master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation


    Public Enum enTrainingCategoryDefaultId
        Custom = 0
        Business_Strategy = 1
        Market_Environment = 2
        Talent_PDP = 3
        Successor_PDP = 4
        Individual_Development_Plan = 5
    End Enum

#Region " Private variables "
    Private mintCategoryunkid As Integer
    Private mstrCategoryCode As String = String.Empty
    Private mstrCategoryName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mintPriorityUnkid As Integer = -1
    Private mintCategoryDefaultypeid As Integer = 0
    Private mblnIsactive As Boolean = True
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Categoryunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintCategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintCategoryunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categorycode
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CategoryCode() As String
        Get
            Return mstrCategoryCode
        End Get
        Set(ByVal value As String)
            mstrCategoryCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryname
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CategoryName() As String
        Get
            Return mstrCategoryName
        End Get
        Set(ByVal value As String)
            mstrCategoryName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priorityunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Priorityunkid() As Integer
        Get
            Return mintPriorityUnkid
        End Get
        Set(ByVal value As Integer)
            mintPriorityUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priorityunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CategoryDefaultypeid() As Integer
        Get
            Return mintCategoryDefaultypeid
        End Get
        Set(ByVal value As Integer)
            mintCategoryDefaultypeid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If objDoOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
              "  categoryunkid " & _
              ", categorycode " & _
              ", categoryname " & _
              ", description " & _
              ", trpriorityunkid " & _
                      ", categorydefaultypeid " & _
              ", isactive " & _
                      " FROM trtrainingcategory_master " & _
                      " WHERE categoryunkid = @categoryunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCategoryunkid = CInt(dtRow.Item("categoryunkid"))
                mstrCategoryCode = dtRow.Item("categorycode").ToString
                mstrCategoryName = dtRow.Item("categoryname").ToString
                mstrDescription = dtRow.Item("description").ToString
                mintPriorityUnkid = CInt(dtRow.Item("trpriorityunkid").ToString)
                mintCategoryDefaultypeid = CInt(dtRow.Item("categorydefaultypeid").ToString)
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  trtrainingcategory_master.categoryunkid " & _
                      ", trtrainingcategory_master.categorycode " & _
                      ", trtrainingcategory_master.categoryname " & _
                      ", trtrainingcategory_master.description " & _
                      ", trtrainingcategory_master.trpriorityunkid " & _
                      ", trtrainingcategory_master.categorydefaultypeid " & _
              ", trtrainingcategory_master.isactive " & _
                      ", trtrainingpriority_master.trpriority_name " & _
                      ", trtrainingpriority_master.trpriority " & _
             " FROM trtrainingcategory_master " & _
                     " LEFT JOIN trtrainingpriority_master ON trtrainingpriority_master.trpriorityunkid = trtrainingcategory_master.trpriorityunkid " & _
             " WHERE trtrainingcategory_master.isactive = 1 "

            objDataOperation.ClearParameters()
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtrainingcategory_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCategoryName, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categorycode", SqlDbType.NVarChar, mstrCategoryCode.Trim.Length, mstrCategoryCode.ToString)
            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, mstrCategoryName.Trim.Length, mstrCategoryName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Trim.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@trpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriorityUnkid.ToString)
            objDataOperation.AddParameter("@categorydefaultypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryDefaultypeid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO trtrainingcategory_master ( " & _
              " categorycode " & _
              ", categoryname " & _
              ", description " & _
              ", trpriorityunkid " & _
                      ", categorydefaultypeid " & _
              ", isactive" & _
            ") VALUES (" & _
              " @categorycode " & _
              ", @categoryname " & _
              ", @description " & _
              ", @trpriorityunkid " & _
                      ", @categorydefaultypeid " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCategoryunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtrainingcategory_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCategoryName, mintCategoryunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@categorycode", SqlDbType.NVarChar, mstrCategoryCode.Trim.Length, mstrCategoryCode.ToString)
            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, mstrCategoryName.Trim.Length, mstrCategoryName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Trim.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@trpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriorityUnkid.ToString)
            objDataOperation.AddParameter("@categorydefaultypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryDefaultypeid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = " UPDATE  trtrainingcategory_master SET " & _
            " categorycode = @categorycode" & _
              ", categoryname = @categoryname" & _
              ", description = @description" & _
              ", trpriorityunkid = @trpriorityunkid" & _
                      ", categorydefaultypeid = @categorydefaultypeid " & _
              ", isactive = @isactive " & _
                      " WHERE categoryunkid = @categoryunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtrainingcategory_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot delete this Category. Reason : This Category is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _Categoryunkid = intUnkid
        mblnIsactive = False

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            strQ = "UPDATE trtrainingcategory_master SET isactive = 0 WHERE categoryunkid = @categoryunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "select isnull(trainingcategoryunkid,0) FROM trdepartmentaltrainingneed_master WHERE trainingcategoryunkid = @categoryunkid AND isvoid = 0 "

            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCategory As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                    "  categoryunkid " & _
                    ", categorycode " & _
                    ", categoryname " & _
                    ", description " & _
                    ", trpriorityunkid " & _
                      ", categorydefaultypeid " & _
                    ", isactive " & _
                      " FROM trtrainingcategory_master " & _
                      " WHERE isactive = 1 " & _
                    " AND categoryname = @categoryname "

            If intUnkid > 0 Then
                strQ &= " AND categoryunkid <> @categoryunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, strCategory.Trim.Length, strCategory)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strTableName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal xDataOper As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If


        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as categoryunkid, ' ' +  @Select  as categoryname UNION "
            End If

            strQ &= "SELECT " & _
              "  categoryunkid " & _
              ", categoryname " & _
                        " FROM trtrainingcategory_master " & _
                        " WHERE trtrainingcategory_master.isactive = 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOper Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO attrtrainingcategory_master ( " & _
                    "  tranguid " & _
                    ", categoryunkid " & _
                    ", categorycode " & _
                    ", categoryname " & _
                    ", description " & _
                    ", trpriorityunkid " & _
                    ", categorydefaultypeid " & _
                    ", audittypeid " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    "  LOWER(NEWID()) " & _
                    ", @categoryunkid " & _
                    ", @categorycode " & _
                    ", @categoryname " & _
                    ", @description " & _
                    ", @trpriorityunkid " & _
                    ", @categorydefaultypeid " & _
                    ", @audittypeid " & _
                    ", @audituserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@categorycode", SqlDbType.NVarChar, mstrCategoryCode.Trim.Length, mstrCategoryCode.ToString)
            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, mstrCategoryName.Trim.Length, mstrCategoryName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Trim.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@trpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriorityUnkid.ToString)
            objDataOperation.AddParameter("@categorydefaultypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryDefaultypeid.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)

            If mstrClientIP.Trim.Length > 0 Then
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            Else
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrHostName.Length > 0 Then
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            Else
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
			Language.setMessage(mstrModuleName, 2, "Sorry, you cannot delete this Category. Reason : This Category is already linked with some transaction.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
