﻿'************************************************************************************************************************************
'Class Name : clsTraining_need_form_cost_tran.vb
'Purpose    :
'Date       :28-11-2019
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsTraining_need_form_cost_tran
    Private Const mstrModuleName = "clsTraining_need_form_cost_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private xDataOp As clsDataOperation
    Private mintTrainingneedformcosttranunkid As Integer
    Private mintTrainingneedformtranunkid As Integer
    Private mintTrainingcostitemunkid As Integer
    Private mdecQuantity As Decimal
    Private mdecUnitprice As Decimal
    Private mdecAmount As Decimal
    Private mintCountryunkid As Integer
    Private mintBase_Countryunkid As Integer
    Private mdecBase_Amount As Decimal
    Private mintExchangerateunkid As Integer
    Private mdecExchange_Rate As Decimal
    Private mstrDescription As String = String.Empty
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingneedformcosttranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trainingneedformcosttranunkid() As Integer
        Get
            Return mintTrainingneedformcosttranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingneedformcosttranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingneedformtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trainingneedformtranunkid() As Integer
        Get
            Return mintTrainingneedformtranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingneedformtranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingcostitemunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trainingcostitemunkid() As Integer
        Get
            Return mintTrainingcostitemunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingcostitemunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set quantity
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Quantity() As Decimal
        Get
            Return mdecQuantity
        End Get
        Set(ByVal value As Decimal)
            mdecQuantity = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitprice
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Unitprice() As Decimal
        Get
            Return mdecUnitprice
        End Get
        Set(ByVal value As Decimal)
            mdecUnitprice = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set base_countryunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Base_Countryunkid() As Integer
        Get
            Return mintBase_Countryunkid
        End Get
        Set(ByVal value As Integer)
            mintBase_Countryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set base_amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Base_Amount() As Decimal
        Get
            Return mdecBase_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecBase_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchangerateunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Exchangerateunkid() As Integer
        Get
            Return mintExchangerateunkid
        End Get
        Set(ByVal value As Integer)
            mintExchangerateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchange_rate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Exchange_Rate() As Decimal
        Get
            Return mdecExchange_Rate
        End Get
        Set(ByVal value As Decimal)
            mdecExchange_Rate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property





    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "SELECT " & _
              "  trainingneedformcosttranunkid " & _
              ", trainingneedformtranunkid " & _
              ", trainingcostitemunkid " & _
              ", quantity " & _
              ", unitprice " & _
              ", amount " & _
              ", countryunkid " & _
              ", base_countryunkid " & _
              ", base_amount " & _
              ", exchangerateunkid " & _
              ", exchange_rate " & _
              ", description " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtraining_need_form_cost_tran " & _
             "WHERE trainingneedformcosttranunkid = @trainingneedformcosttranunkid "

            objDataOperation.AddParameter("@trainingneedformcosttranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintTrainingneedformcostTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                minttrainingneedformcosttranunkid = CInt(dtRow.Item("trainingneedformcosttranunkid"))
                minttrainingneedformtranunkid = CInt(dtRow.Item("trainingneedformtranunkid"))
                minttrainingcostitemunkid = CInt(dtRow.Item("trainingcostitemunkid"))
                mdecQuantity = dtRow.Item("quantity")
                mdecUnitprice = dtRow.Item("unitprice")
                mdecAmount = dtRow.Item("amount")
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                mintbase_countryunkid = CInt(dtRow.Item("base_countryunkid"))
                mdecBase_Amount = dtRow.Item("base_amount")
                mintexchangerateunkid = CInt(dtRow.Item("exchangerateunkid"))
                mdecExchange_Rate = dtRow.Item("exchange_rate")
                mstrdescription = dtRow.Item("description").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnisweb = CBool(dtRow.Item("isweb"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintvoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intTrainingneedformtranunkid As Integer = 0, Optional ByVal intTrainingneedformunkid As Integer = 0, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            Dim objExRate As New clsExchangeRate
            Dim dsExRate As DataSet = objExRate.GetList("Currency", True, False, 0, 0, False, Nothing, True, objDataOperation)

            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
              "  hrtraining_need_form_cost_tran.trainingneedformcosttranunkid " & _
              ", hrtraining_need_form_cost_tran.trainingneedformtranunkid " & _
              ", hrtraining_need_form_cost_tran.trainingcostitemunkid " & _
              ", ISNULL(cfcommon_master.name, '') AS trainingcostitemname " & _
              ", hrtraining_need_form_cost_tran.quantity " & _
              ", hrtraining_need_form_cost_tran.unitprice " & _
              ", hrtraining_need_form_cost_tran.amount " & _
              ", hrtraining_need_form_cost_tran.countryunkid " & _
              ", hrtraining_need_form_cost_tran.base_countryunkid " & _
              ", hrtraining_need_form_cost_tran.base_amount " & _
              ", hrtraining_need_form_cost_tran.exchangerateunkid " & _
              ", hrtraining_need_form_cost_tran.exchange_rate " & _
              ", hrtraining_need_form_cost_tran.description " & _
              ", hrtraining_need_form_cost_tran.userunkid " & _
              ", hrtraining_need_form_cost_tran.loginemployeeunkid " & _
              ", hrtraining_need_form_cost_tran.isweb " & _
              ", hrtraining_need_form_cost_tran.isvoid " & _
              ", hrtraining_need_form_cost_tran.voiduserunkid " & _
              ", hrtraining_need_form_cost_tran.voidloginemployeeunkid " & _
              ", hrtraining_need_form_cost_tran.voiddatetime " & _
              ", hrtraining_need_form_cost_tran.voidreason " & _
              ", '' AS AUD "

            strQ &= ", CASE hrtraining_need_form_cost_tran.countryunkid "
            For Each dsRow As DataRow In dsExRate.Tables(0).Rows
                strQ &= " WHEN " & CInt(dsRow.Item("countryunkid")) & "  THEN '" & dsRow.Item("currency_sign") & "' "
            Next
            strQ &= " END AS currency_sign "

            strQ &= "FROM hrtraining_need_form_cost_tran " & _
                " LEFT JOIN hrtraining_need_form_tran ON hrtraining_need_form_tran.trainingneedformtranunkid = hrtraining_need_form_cost_tran.trainingneedformtranunkid " & _
                " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_need_form_cost_tran.trainingcostitemunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_REQUEST_COST_ITEM & " " & _
             "WHERE hrtraining_need_form_cost_tran.isvoid = 0 "

            If intTrainingneedformtranunkid > 0 Then
                strQ &= " AND hrtraining_need_form_cost_tran.trainingneedformtranunkid = @trainingneedformtranunkid "
                objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingneedformtranunkid)
            End If

            If intTrainingneedformunkid > 0 Then
                strQ &= " AND hrtraining_need_form_tran.trainingneedformunkid = @trainingneedformunkid "
                objDataOperation.AddParameter("@trainingneedformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingneedformunkid)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_need_form_cost_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttrainingneedformtranunkid.ToString)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttrainingcostitemunkid.ToString)
            objDataOperation.AddParameter("@quantity", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecQuantity.ToString)
            objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecUnitprice.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbase_countryunkid.ToString)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBase_Amount.ToString)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintexchangerateunkid.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_Rate.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            StrQ = "INSERT INTO hrtraining_need_form_cost_tran ( " & _
              "  trainingneedformtranunkid " & _
              ", trainingcostitemunkid " & _
              ", quantity " & _
              ", unitprice " & _
              ", amount " & _
              ", countryunkid " & _
              ", base_countryunkid " & _
              ", base_amount " & _
              ", exchangerateunkid " & _
              ", exchange_rate " & _
              ", description " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @trainingneedformtranunkid " & _
              ", @trainingcostitemunkid " & _
              ", @quantity " & _
              ", @unitprice " & _
              ", @amount " & _
              ", @countryunkid " & _
              ", @base_countryunkid " & _
              ", @base_amount " & _
              ", @exchangerateunkid " & _
              ", @exchange_rate " & _
              ", @description " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingneedformcostTranUnkId = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAll(ByVal dtTable As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            For Each dtRow As DataRow In dtTable.Rows

                If Not (dtRow.Item("AUD").ToString = "A" OrElse dtRow.Item("AUD").ToString = "D") Then Continue For

                mintTrainingcostitemunkid = CInt(dtRow.Item("trainingcostitemunkid"))
                mdecQuantity = CDec(dtRow.Item("quantity"))
                mdecUnitprice = CDec(dtRow.Item("unitprice"))
                mdecAmount = CDec(dtRow.Item("amount"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintBase_Countryunkid = CInt(dtRow.Item("base_countryunkid"))
                mdecBase_Amount = CDec(dtRow.Item("base_amount"))
                mintExchangerateunkid = CInt(dtRow.Item("exchangerateunkid"))
                mdecExchange_Rate = CDec(dtRow.Item("exchange_rate"))
                mstrDescription = dtRow.Item("description").ToString

                Me._xDataOp = objDataOperation

                If dtRow.Item("AUD").ToString = "A" Then
                    mblnIsvoid = False
                    mintVoiduserunkid = -1
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = ""
                    mintVoidloginemployeeunkid = -1

                    If Insert() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                ElseIf dtRow.Item("AUD").ToString = "D" Then
                    mblnIsvoid = True
                    mintVoiduserunkid = mintUserunkid
                    mdtVoiddatetime = mdtAuditDate
                    mstrVoidreason = Language.getMessage(mstrModuleName, 1, "Allocation Removed")
                    mintVoidloginemployeeunkid = mintLoginemployeeunkid
                    Dim intOldUserUnkId As Integer = mintUserunkid
                    Dim intOldLoginemployeeUnkId As Integer = mintLoginemployeeunkid

                    If Delete() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    mintUserunkid = intOldUserUnkId
                    mintLoginemployeeunkid = intOldLoginemployeeUnkId
                End If
            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_need_form_cost_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintTrainingneedformcosttranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingneedformcosttranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttrainingneedformcosttranunkid.ToString)
            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttrainingneedformtranunkid.ToString)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttrainingcostitemunkid.ToString)
            objDataOperation.AddParameter("@quantity", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecQuantity.ToString)
            objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecUnitprice.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbase_countryunkid.ToString)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBase_Amount.ToString)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintexchangerateunkid.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_Rate.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            StrQ = "UPDATE hrtraining_need_form_cost_tran SET " & _
              "  trainingneedformtranunkid = @trainingneedformtranunkid" & _
              ", trainingcostitemunkid = @trainingcostitemunkid" & _
              ", quantity = @quantity" & _
              ", unitprice = @unitprice" & _
              ", amount = @amount" & _
              ", countryunkid = @countryunkid" & _
              ", base_countryunkid = @base_countryunkid" & _
              ", base_amount = @base_amount" & _
              ", exchangerateunkid = @exchangerateunkid" & _
              ", exchange_rate = @exchange_rate" & _
              ", description = @description" & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE trainingneedformcosttranunkid = @trainingneedformcosttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_need_form_cost_tran) </purpose>
    Public Function Delete() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnxDataOp As Boolean = False

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
            blnxDataOp = True
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hrtraining_need_form_cost_tran SET " & _
                     " isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid " & _
                     ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason " & _
                   "WHERE trainingneedformcosttranunkid = @trainingneedformcosttranunkid "

            objDataOperation.AddParameter("@trainingneedformcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformcosttranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._Trainingneedformcosttranunkid = mintTrainingneedformcosttranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If blnxDataOp = False Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If blnxDataOp = False Then objDataOperation = Nothing
        End Try
    End Function

    Public Function DeleteByTrainingNeedFormTranUnkId() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnxDataOp As Boolean = False

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
            blnxDataOp = True
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                 " hrtraining_need_form_cost_tran.trainingneedformcosttranunkid " & _
            "FROM hrtraining_need_form_cost_tran " & _
            "WHERE hrtraining_need_form_cost_tran.isvoid = 0 " & _
            "AND hrtraining_need_form_cost_tran.trainingneedformtranunkid = @trainingneedformtranunkid "

            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformtranunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dsRow As DataRow In dsList.Tables(0).Rows

                mintTrainingneedformcosttranunkid = CInt(dsRow.Item("trainingneedformcosttranunkid"))

                Me._xDataOp = objDataOperation
                If Delete() = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Next

            If blnxDataOp = False Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteByTrainingNeedFormTranUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If blnxDataOp = False Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@trainingneedformcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  trainingneedformcosttranunkid " & _
              ", trainingneedformtranunkid " & _
              ", trainingcostitemunkid " & _
              ", quantity " & _
              ", unitprice " & _
              ", amount " & _
              ", countryunkid " & _
              ", base_countryunkid " & _
              ", base_amount " & _
              ", exchangerateunkid " & _
              ", exchange_rate " & _
              ", description " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtraining_need_form_cost_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND trainingneedformcosttranunkid <> @trainingneedformcosttranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@trainingneedformcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistItem(ByVal intTrainingneedformTranunkid As Integer, ByVal intTrainingCostItemUnkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  trainingneedformcosttranunkid " & _
              ", trainingneedformtranunkid " & _
              ", trainingcostitemunkid " & _
              ", quantity " & _
              ", unitprice " & _
              ", amount " & _
              ", countryunkid " & _
              ", base_countryunkid " & _
              ", base_amount " & _
              ", exchangerateunkid " & _
              ", exchange_rate " & _
              ", description " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtraining_need_form_cost_tran " & _
             "WHERE isvoid = 0 " & _
             "AND trainingneedformtranunkid = @trainingneedformtranunkid " & _
             "AND trainingcostitemunkid = @trainingcostitemunkid "

            If intUnkid > 0 Then
                strQ &= " AND trainingneedformcosttranunkid <> @trainingneedformcosttranunkid"
                objDataOperation.AddParameter("@trainingneedformcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingneedformTranunkid)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingCostItemUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistItem; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrail(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@trainingneedformcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformcosttranunkid.ToString)
            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformtranunkid.ToString)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcostitemunkid.ToString)
            objDataOperation.AddParameter("@quantity", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecQuantity.ToString)
            objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecUnitprice.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBase_Countryunkid.ToString)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBase_Amount.ToString)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExchangerateunkid.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_Rate.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)

            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)

            strQ = "INSERT INTO athrtraining_need_form_cost_tran ( " & _
                            "  trainingneedformcosttranunkid " & _
                            ", trainingneedformtranunkid " & _
                            ", trainingcostitemunkid " & _
                            ", quantity " & _
                            ", unitprice " & _
                            ", amount " & _
                            ", countryunkid " & _
                            ", base_countryunkid " & _
                            ", base_amount " & _
                            ", exchangerateunkid " & _
                            ", exchange_rate " & _
                            ", description " & _
                            ", audituserunkid " & _
                            ", loginemployeeunkid " & _
                            ", audittype " & _
                            ", auditdatetime " & _
                            ", isweb " & _
                            ", ip " & _
                            ", host " & _
                            ", form_name " & _
                  ") VALUES (" & _
                            "  @trainingneedformcosttranunkid " & _
                            ", @trainingneedformtranunkid " & _
                            ", @trainingcostitemunkid " & _
                            ", @quantity " & _
                            ", @unitprice " & _
                            ", @amount " & _
                            ", @countryunkid " & _
                            ", @base_countryunkid " & _
                            ", @base_amount " & _
                            ", @exchangerateunkid " & _
                            ", @exchange_rate " & _
                            ", @description " & _
                            ", @audituserunkid " & _
                            ", @loginemployeeunkid " & _
                            ", @audittype " & _
                            ", @auditdatetime " & _
                            ", @isweb " & _
                            ", @ip " & _
                            ", @host " & _
                            ", @form_name " & _
                  "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class