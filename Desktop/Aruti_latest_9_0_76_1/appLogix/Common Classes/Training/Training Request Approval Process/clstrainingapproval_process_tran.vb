﻿
'************************************************************************************************************************************
'Class Name : clstrainingapproval_process_tran.vb
'Purpose    :
'Date       :03-Mar-2021
'Written By :Hemant Morker
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant Morker
''' </summary>
Public Class clstrainingapproval_process_tran
    Private Const mstrModuleName As String = "clstraining_requisition_approval_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintPendingTrainingTranunkid As Integer
    Private mintTrainingRequestunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintApproverTranunkid As Integer
    Private mintPriority As Integer
    Private mdtApprovaldate As Date
    Private mdecTotalCostAmount As Decimal
    Private mdecApprovedAmount As Decimal
    Private mintStatusunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mintVisibleId As Integer = -1
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = String.Empty
    Private mstrClientIP As String = String.Empty
    Private mstrHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False
    Private mintMapuserunkid As Integer
    Private mintCompletedStatusunkid As Integer
    Private mstrCompletedRemark As String = String.Empty
    Private mintCompletedVisibleId As Integer = -1
    'Hemant (09 Feb 2022) -- Start            
    'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
    Private mdecApprovedAmountEmp As Decimal
    'Hemant (09 Feb 2022) -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    '' <summary>
    '' Purpose: Get or Set PendingTrainingTranunkid
    '' Modify By: Hemant
    '' </summary>
    Public Property _PendingTrainingTranunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer

        Get
            Return mintPendingTrainingTranunkid
        End Get
        Set(ByVal value As Integer)
            mintPendingTrainingTranunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TrainingRequestunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingRequestunkid() As Integer
        Get
            Return mintTrainingRequestunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingRequestunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovertranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Priority
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TotalCostAmount
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TotalCostAmount() As Decimal
        Get
            Return mdecTotalCostAmount
        End Get
        Set(ByVal value As Decimal)
            mdecTotalCostAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApprovedAmount
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ApprovedAmount() As Decimal
        Get
            Return mdecApprovedAmount
        End Get
        Set(ByVal value As Decimal)
            mdecApprovedAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visibleid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VisibleId() As Integer
        Get
            Return mintVisibleId
        End Get
        Set(ByVal value As Integer)
            mintVisibleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set completed_vstatusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CompletedStatusunkid() As Integer
        Get
            Return mintCompletedStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintCompletedStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set completed_vremark
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CompletedRemark() As String
        Get
            Return mstrCompletedRemark
        End Get
        Set(ByVal value As String)
            mstrCompletedRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set completed_visibleid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CompletedVisibleId() As Integer
        Get
            Return mintCompletedVisibleId
        End Get
        Set(ByVal value As Integer)
            mintCompletedVisibleId = value
        End Set
    End Property

    'Hemant (09 Feb 2022) -- Start            
    'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
    Public Property _ApprovedAmountEmp() As Decimal
        Get
            Return mdecApprovedAmountEmp
        End Get
        Set(ByVal value As Decimal)
            mdecApprovedAmountEmp = value
        End Set
    End Property
    'Hemant (09 Feb 2022) -- End


#End Region

    '' <summary>
    '' Modify By: Hemant
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  pendingtrainingtranunkid " & _
                      ", trainingrequestunkid " & _
                      ", employeeunkid " & _
                      ", approvertranunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", totalcostamount " & _
                      ", approvedamount " & _
                      ", statusunkid " & _
                      ", visibleid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", mapuserunkid " & _
                      ", completed_statusunkid " & _
                      ", completed_visibleid " & _
                      ", completed_remark " & _
                     "FROM trtrainingapproval_process_tran " & _
                     "WHERE pendingtrainingtranunkid = @pendingtrainingtranunkid "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@pendingtrainingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingTrainingTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPendingTrainingTranunkid = CInt(dtRow.Item("pendingtrainingtranunkid"))
                mintTrainingRequestunkid = CInt(dtRow.Item("trainingrequestunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintApproverTranunkid = CInt(dtRow.Item("approvertranunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mdtApprovaldate = CDate(dtRow.Item("approvaldate"))
                mdecTotalCostAmount = CDec(dtRow.Item("totalcostamount"))
                mdecApprovedAmount = CDec(dtRow.Item("approvedamount"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintVisibleId = CInt(dtRow.Item("visibleid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mintCompletedStatusunkid = CInt(dtRow.Item("completed_statusunkid"))
                mintCompletedVisibleId = CInt(dtRow.Item("completed_visibleid"))
                mstrCompletedRemark = dtRow.Item("completed_remark").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    '' <summary>
    '' Modify By: Hemant
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal mstrFilter As String = "", _
                            Optional ByVal strAdvanceFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName, "Emp")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName, "Emp")

            strQ = "SELECT " & _
                           " -1 AS pendingtrainingtranunkid " & _
                           ", trtraining_request_master.trainingrequestunkid " & _
                           ", NULL AS application_date " & _
                           ", trtrainingapproval_process_tran.employeeunkid " & _
                           ", ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.othername, '') + ' ' + ISNULL(Emp.surname, '') AS Employee " & _
                           ", '' AS Training " & _
                           ", -1 AS Approverunkid " & _
                           ", -1 AS levelunkid " & _
                           ", -1 AS priority " & _
                           ", -1 AS mapuserunkid " & _
                           ", '' AS ApproverName " & _
                           ",  NULL AS total_training_cost " & _
                           ",  NULL AS approved_amount " & _
                           ", -1 AS training_statusunkid " & _
                           ", -1 AS statusunkid " & _
                           ", '' AS status " & _
                           ", '' AS remark " & _
                           ", -1 AS stationunkid " & _
                           ", -1 AS deptgroupunkid " & _
                           ", -1 AS departmentunkid " & _
                           ", -1 AS sectiongroupunkid " & _
                           ", -1 AS sectionunkid " & _
                           ", -1 AS unitgroupunkid " & _
                           ", -1 AS unitunkid " & _
                           ", -1 AS teamunkid " & _
                           ", -1 AS classgroupunkid " & _
                           ", -1 AS classunkid " & _
                           ", -1 AS jobgroupunkid " & _
                           ", -1 AS jobunkid " & _
                           ", -1 AS gradegroupunkid " & _
                           ", -1 AS gradeunkid " & _
                           ", -1 AS gradelevelunkid " & _
                           ", 0 AS isvoid " & _
                           ", NULL AS voiddatetime " & _
                           ", '' AS voidreason " & _
                           ", -1 AS voiduserunkid " & _
                           ", 0 AS visibleid " & _
                           ", 1 AS isgrp " & _
                           ", -1 AS completed_statusunkid " & _
                           ", '' AS completed_status " & _
                           ", '' AS completed_remark " & _
                           ", 0 AS completed_visibleid " & _
                           ", 0 as iscompleted_submit_approval " & _
                        " FROM trtraining_request_master " & _
                        " LEFT JOIN trtrainingapproval_process_tran ON trtraining_request_master.trainingrequestunkid = trtrainingapproval_process_tran.trainingrequestunkid " & _
                                " AND trtraining_request_master.isvoid = 0 " & _
                        " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = trtrainingapproval_process_tran.employeeunkid " & _
                        " LEFT JOIN hrtraining_approver_master ON hrtraining_approver_master.mappingunkid = trtrainingapproval_process_tran.approvertranunkid " & _
                        " LEFT JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = hrtraining_approver_master.levelunkid "

            strQ &= " WHERE trtraining_request_master.isvoid = 0 "

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " UNION " & _
                       " SELECT " & _
                               "trtrainingapproval_process_tran.pendingtrainingtranunkid " & _
                               ", trtraining_request_master.trainingrequestunkid " & _
                               ", trtraining_request_master.application_date " & _
                               ", trtrainingapproval_process_tran.employeeunkid " & _
                               ", ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.othername, '') + ' ' + ISNULL(Emp.surname, '') AS Employee " & _
                               ", ISNULL(cfcommon_master.name,'') AS Training " & _
                               ", trtrainingapproval_process_tran.approvertranunkid AS Approverunkid " & _
                               ", hrtraining_approverlevel_master.levelunkid " & _
                               ", trtrainingapproval_process_tran.priority " & _
                               ", trtrainingapproval_process_tran.mapuserunkid " & _
                               ", ISNULL(hrmsConfiguration..cfuser_master.username,'') AS ApproverName " & _
                               ", ISNULL(trtraining_request_master.totaltrainingcost, 0.00) AS total_training_cost " & _
                               ", ISNULL(trtrainingapproval_process_tran.approvedamount, 0.00) AS approved_amount " & _
                               ", trtraining_request_master.statusunkid as training_statusunkid " & _
                               ", trtrainingapproval_process_tran.statusunkid " & _
                               ", CASE " & _
                                    "WHEN trtrainingapproval_process_tran.statusunkid = 1 THEN @Pending " & _
                                    "WHEN trtrainingapproval_process_tran.statusunkid = 2 THEN @Approved " & _
                                    "WHEN trtrainingapproval_process_tran.statusunkid = 3 THEN @Reject " & _
                                    "WHEN trtrainingapproval_process_tran.statusunkid = 4 THEN @Assigned " & _
                                "END AS status " & _
                               ", trtrainingapproval_process_tran.remark " & _
                               ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                               ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                               ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                               ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                               ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                               ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                               ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                               ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                               ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                               ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
                               ", ISNULL(ECT.jobgroupunkid, 0) AS jobgroupunkid " & _
                               ", ISNULL(ECT.jobunkid, 0) AS jobunkid " & _
                               ", ISNULL(GRD.gradegroupunkid, 0) AS gradegroupunkid " & _
                               ", ISNULL(GRD.gradeunkid, 0) AS gradeunkid " & _
                               ", ISNULL(GRD.gradelevelunkid, 0) AS gradelevelunkid " & _
                               ", trtrainingapproval_process_tran.isvoid " & _
                               ", trtrainingapproval_process_tran.voiddatetime " & _
                               ", trtrainingapproval_process_tran.voidreason " & _
                               ", trtrainingapproval_process_tran.voiduserunkid " & _
                               ", trtrainingapproval_process_tran.visibleid " & _
                               ", 0 AS isgrp " & _
                               ", trtrainingapproval_process_tran.completed_statusunkid " & _
                               ", CASE " & _
                                    "WHEN trtrainingapproval_process_tran.completed_statusunkid = 1 THEN @Pending " & _
                                    "WHEN trtrainingapproval_process_tran.completed_statusunkid = 2 THEN @Approved " & _
                                    "WHEN trtrainingapproval_process_tran.completed_statusunkid = 3 THEN @Reject " & _
                                    "WHEN trtrainingapproval_process_tran.completed_statusunkid = 4 THEN @Assigned " & _
                                "END AS completedstatus " & _
                                ", trtrainingapproval_process_tran.completed_remark " & _
                                ", trtrainingapproval_process_tran.completed_visibleid " & _
                                ", trtraining_request_master.iscompleted_submit_approval as iscompleted_submit_approval " & _
                        " FROM trtrainingapproval_process_tran " & _
                                " LEFT JOIN trtraining_request_master ON trtraining_request_master.trainingrequestunkid = trtrainingapproval_process_tran.trainingrequestunkid " & _
                                        " AND trtraining_request_master.isvoid = 0 " & _
                                " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = trtrainingapproval_process_tran.employeeunkid " & _
                                " LEFT JOIN hrtraining_approver_master ON hrtraining_approver_master.mappingunkid = trtrainingapproval_process_tran.approvertranunkid " & _
                                " LEFT JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = hrtraining_approver_master.levelunkid " & _
                                " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = trtrainingapproval_process_tran.mapuserunkid " & _
                                " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = trtraining_request_master.coursemasterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        stationunkid " & _
                            "       ,deptgroupunkid " & _
                            "       ,departmentunkid " & _
                            "       ,sectiongroupunkid " & _
                            "       ,sectionunkid " & _
                            "       ,unitgroupunkid " & _
                            "       ,unitunkid " & _
                            "       ,teamunkid " & _
                            "       ,classgroupunkid " & _
                            "       ,classunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ETT ON ETT.employeeunkid = Emp.employeeunkid AND ETT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobgroupunkid " & _
                            "       ,jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ECT ON ECT.employeeunkid = Emp.employeeunkid AND ECT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        gradegroupunkid " & _
                            "       ,gradeunkid " & _
                            "       ,gradelevelunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS GRD ON GRD.employeeunkid = Emp.employeeunkid AND GRD.rno = 1 "

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            StrQCondition &= " WHERE   trtrainingapproval_process_tran.isvoid = 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrFilter
            End If

            If strAdvanceFilter.Trim.Length > 0 Then

                StrQCondition &= " AND " & strAdvanceFilter.Replace("ADF", "Emp").Replace("hremployee_master", "Emp")

            End If

            strQ &= StrQCondition
            strQ &= StrQDtFilters

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 102, "Rejected"))
            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 103, "Assigned"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = StrFinalQurey
                StrQDtFilters = ""

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = hrtraining_approver_master.approverempunkid ")

                    strQ &= StrQCondition
                Else
                    
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') END ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = hrtraining_approver_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master AS App ON App.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    strQ &= StrQCondition

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry
                        End If
                    End If

                    If strAdvanceFilter.Trim.Length > 0 Then
                        strQ &= " AND " & strAdvanceFilter.Replace("ADF", "Emp")
                    End If

                End If


                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 100, "Pending"))
                objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 101, "Approved"))
                objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 102, "Rejected"))
                objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 103, "Assigned"))

                dsExtList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(0).Copy)
                Else
                    Dim dtExtList As DataTable = New DataView(dsExtList.Tables("List"), "isgrp = 0", "", DataViewRowState.CurrentRows).ToTable
                    dsExtList.Tables.RemoveAt(0)
                    dsExtList.Tables.Add(dtExtList.Copy)
                    dsList.Tables(0).Merge(dsExtList.Tables(0), True)

                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "trainingrequestunkid DESC, priority ,isgrp DESC", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtrainingapproval_process_tran) </purpose>
    Public Function Insert(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@totalcostamount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecTotalCostAmount.ToString)
            objDataOperation.AddParameter("@approvedamount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecApprovedAmount.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@completed_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedStatusunkid.ToString)
            objDataOperation.AddParameter("@completed_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompletedRemark.ToString)
            objDataOperation.AddParameter("@completed_visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedVisibleId.ToString)

            strQ = "INSERT INTO trtrainingapproval_process_tran ( " & _
                      "  trainingrequestunkid " & _
                      ", employeeunkid " & _
                      ", approvertranunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", totalcostamount " & _
                      ", approvedamount " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", visibleid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                      ", mapuserunkid" & _
                      ", completed_statusunkid " & _
                      ", completed_remark " & _
                      ", completed_visibleid " & _
                    ") VALUES (" & _
                      "  @trainingrequestunkid " & _
                      ", @employeeunkid " & _
                      ", @approvertranunkid " & _
                      ", @priority " & _
                      ", @approvaldate " & _
                      ", @totalcostamount " & _
                      ", @approvedamount " & _
                      ", @statusunkid " & _
                      ", @remark " & _
                      ", @visibleid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                      ", @mapuserunkid" & _
                      ", @completed_statusunkid " & _
                      ", @completed_remark " & _
                      ", @completed_visibleid " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPendingTrainingTranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForTrainingApproval(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtrainingapproval_process_tran) </purpose>
    Public Function Update(ByVal xDatabaseName As String, _
                           ByVal xPeriodStart As DateTime, _
                           ByVal intTrainingId As Integer, _
                           Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        'Hemant (16 Nov 2021) -- [xDatabaseName,xPeriodStart]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mintStatusunkid = enTrainingRequestStatus.PENDING Then Return True

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingtrainingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingTrainingTranunkid.ToString)
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@totalcostamount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecTotalCostAmount.ToString)
            objDataOperation.AddParameter("@approvedamount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecApprovedAmount.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@completed_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedStatusunkid.ToString)
            objDataOperation.AddParameter("@completed_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompletedRemark.ToString)
            objDataOperation.AddParameter("@completed_visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedVisibleId.ToString)

            strQ = "UPDATE trtrainingapproval_process_tran SET " & _
                      "  trainingrequestunkid = @trainingrequestunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", approvertranunkid = @approvertranunkid" & _
                      ", priority = @priority " & _
                      ", approvaldate = @approvaldate" & _
                      ", totalcostamount = @totalcostamount" & _
                      ", approvedamount = @approvedamount" & _
                      ", statusunkid = @statusunkid" & _
                      ", remark = @remark" & _
                      ", visibleid = @visibleid " & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      ", mapuserunkid = @mapuserunkid " & _
                      ", completed_statusunkid = @completed_statusunkid" & _
                      ", completed_remark = @completed_remark" & _
                      ", completed_visibleid = @completed_visibleid " & _
                    "WHERE pendingtrainingtranunkid = @pendingtrainingtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForTrainingApproval(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT pendingtrainingtranunkid,trainingrequestunkid,approvertranunkid, priority,visibleid FROM trtrainingapproval_process_tran WHERE isvoid = 0 AND trainingrequestunkid = @trainingrequestunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                strQ = " UPDATE trtrainingapproval_process_tran set " & _
                          " visibleid = " & mintStatusunkid & _
                          " WHERE  trainingrequestunkid = @trainingrequestunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid  AND isvoid = 0   "


                Dim dtVisibility As DataTable = New DataView(dsApprover.Tables(0), "priority = " & mintPriority, "", DataViewRowState.CurrentRows).ToTable

                For i As Integer = 0 To dtVisibility.Rows.Count - 1
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("trainingrequestunkid").ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approvertranunkid").ToString)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

                Dim intMinPriority As Integer = IIf(IsDBNull(dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority)), -1, dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority))

                dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

                If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                    If mintStatusunkid = enTrainingRequestStatus.APPROVED Then
                        strQ = " UPDATE trtrainingapproval_process_tran set " & _
                                  " visibleid = 1 " & _
                                  " WHERE  trainingrequestunkid = @trainingrequestunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid AND isvoid = 0   "

                    ElseIf mintStatusunkid = enTrainingRequestStatus.REJECTED Then

                        strQ = " UPDATE trtrainingapproval_process_tran set " & _
                                  " visibleid = -1 " & _
                                  " WHERE  trainingrequestunkid = @trainingrequestunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid AND isvoid = 0   "
                    End If

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("trainingrequestunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approvertranunkid").ToString)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If

            End If

            If mintStatusunkid = enTrainingRequestStatus.APPROVED Then

                strQ = " Update trtrainingapproval_process_tran SET " & _
                          "  totalcostamount = @totalcostamount " & _
                          ", approvedamount = @approvedamount " & _
                          "  WHERE isvoid = 0 AND trainingrequestunkid = @trainingrequestunkid and employeeunkid = @employeeunkid AND priority >= @priority "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@totalcostamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalCostAmount)
                objDataOperation.AddParameter("@approvedamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedAmount)
                objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            If UpdateTrainingRequestStatus(xDatabaseName, xPeriodStart, intTrainingId, objDataOperation) = False Then
                'Hemant (16 Nov 2021) -- [xDatabaseName,xPeriodStart]
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function UpdateTrainingRequestStatus(ByVal xDatabaseName As String, _
                                                ByVal xPeriodStart As DateTime, _
                                                ByVal intTrainingId As Integer, _
                                                ByVal objDataOperation As clsDataOperation) As Boolean

        'Hemant (16 Nov 2021) -- [xDatabaseName,xPeriodStart]
        Dim exForce As Exception
        Try
            If mintStatusunkid <> enTrainingRequestStatus.REJECTED AndAlso mintStatusunkid <> enTrainingRequestStatus.CANCELLED Then
                Dim objLoanApprover As New clsLoanApprover_master

                Dim dtApprover As DataTable = GetTrainingApprovalData(xDatabaseName, _
                                                                      xPeriodStart, _
                                                                      xPeriodStart, _
                                                                      False, _
                                                                      mintEmployeeunkid, _
                                                                  intTrainingId, _
                                                                  "  trtraining_request_master.trainingrequestunkid = " & mintTrainingRequestunkid & " AND hrtraining_approverlevel_master.priority > " & mintPriority, _
                                                                  objDataOperation)
                'Hemant (16 Nov 2021) -- [xDatabaseName,xPeriodStart,xPeriodStart,False]

                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count <= 0 Then
                    Dim objTrainingRequest As New clstraining_request_master
                    objTrainingRequest._TrainingRequestunkid = mintTrainingRequestunkid
                    objTrainingRequest._Approvertranunkid = mintApproverTranunkid
                    objTrainingRequest._ApprovedCost = mdecApprovedAmount
                    objTrainingRequest._Statusunkid = mintStatusunkid
                    objTrainingRequest._ClientIP = mstrClientIP
                    objTrainingRequest._FormName = mstrFormName
                    objTrainingRequest._HostName = mstrHostName
                    objTrainingRequest._IsWeb = mblnIsWeb
                    'Hemant (09 Feb 2022) -- Start            
                    'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
                    objTrainingRequest._ApprovedAmountEmp = mdecApprovedAmountEmp
                    'Hemant (09 Feb 2022) -- End
                    If objTrainingRequest.Update(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objTrainingRequest = Nothing
                End If
                dtApprover.Rows.Clear()
                dtApprover = Nothing

            ElseIf mintStatusunkid = enTrainingRequestStatus.REJECTED Then
                Dim objTrainingRequest As New clstraining_request_master
                objTrainingRequest._TrainingRequestunkid = mintTrainingRequestunkid
                objTrainingRequest._Approvertranunkid = mintApproverTranunkid
                objTrainingRequest._Statusunkid = mintStatusunkid
                objTrainingRequest._ClientIP = mstrClientIP
                objTrainingRequest._FormName = mstrFormName
                objTrainingRequest._HostName = mstrHostName
                objTrainingRequest._IsWeb = mblnIsWeb
                If objTrainingRequest.Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objTrainingRequest = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoanApplicationStatus; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetTrainingApprovalData(ByVal xDatabaseName As String, _
                                            ByVal xPeriodStart As DateTime, _
                                            ByVal xPeriodEnd As DateTime, _
                                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                            ByVal intEmployeeID As Integer, _
                                            Optional ByVal intTrainingId As Integer = -1, _
                                            Optional ByVal mstrFilter As String = "", _
                                            Optional ByVal objDataOp As clsDataOperation = Nothing) As DataTable

        'Hemant (16 Nov 2021) -- [xDatabaseName,xPeriodStart,xPeriodEnd,xIncludeIn_ActiveEmployee]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
            Else
                objDataOperation = New clsDataOperation
            End If

            'Hemant (16 Nov 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-520(Finca Uganda) - Training Notifications are being sent Even to Inactive.
            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Hemant (16 Nov 2021) -- End

            objDataOperation.ClearParameters()

            strQ = " SELECT " & _
                      "  trtraining_request_master.trainingrequestunkid " & _
                      ", trtrainingapproval_process_tran.pendingtrainingtranunkid " & _
                      ", trtrainingapproval_process_tran.employeeunkid " & _
                      ", #EMP_CODE# AS employeecode " & _
                      ", #EMP_NAME# AS employeename " & _
                      ", trtrainingapproval_process_tran.approvertranunkid " & _
                      ", hrtraining_approverlevel_master.levelunkid " & _
                      ", hrtraining_approverlevel_master.levelname " & _
                      ", hrtraining_approverlevel_master.priority " & _
                       ", trtrainingapproval_process_tran.mapuserunkid " & _
                      ", trtrainingapproval_process_tran.approvedamount " & _
                      " FROM trtraining_request_master " & _
                      " JOIN trtrainingapproval_process_tran ON trtrainingapproval_process_tran.trainingrequestunkid = trtraining_request_master.trainingrequestunkid " & _
                      " JOIN hrtraining_approver_master ON trtrainingapproval_process_tran.approvertranunkid = hrtraining_approver_master.mappingunkid " & _
                      "     AND hrtraining_approver_master.isvoid = 0 AND hrtraining_approver_master.isactive = 1 " & _
                      " #EMPLOYEE_JOIN# " & _
                      " JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = hrtraining_approver_master.levelunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master on cfuser_master.userunkid = trtrainingapproval_process_tran.mapuserunkid " & _
                      " LEFT JOIN hremployee_master UserEmp ON UserEmp.employeeunkid = hrmsConfiguration..cfuser_master.employeeunkid "

            'Hemant (16 Nov 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-520(Finca Uganda) - Training Notifications are being sent Even to Inactive.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "UserEmp")
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry.Replace("hremployee_master", "UserEmp")
            End If
            'Hemant (16 Nov 2021) -- End

            strQFinal = strQ

            strQCondition = " WHERE trtrainingapproval_process_tran.isvoid = 0  AND trtrainingapproval_process_tran.employeeunkid = " & intEmployeeID

            If mstrFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & mstrFilter.Trim
            End If

            strQ &= strQCondition

            'Hemant (16 Nov 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-520(Finca Uganda) - Training Notifications are being sent Even to Inactive.
            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry.Replace("hremployee_master", "UserEmp")
                End If
            End If
            'Hemant (16 Nov 2021) -- End

            strQ = strQ.Replace("#EMP_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#EMP_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = trtrainingapproval_process_tran.employeeunkid AND hremployee_master.isapproved = 1 ")


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority,employeename", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)
            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
                Return dsList.Tables("List")
            Else
                Return Nothing
            End If
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "GetTrainingApprovalData", mstrMessage)
            Return Nothing
        Finally
            exForce = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function IsPendingTrainingRequest(ByVal intTrainingRequestunkid As Integer, Optional ByVal blnGetApplicationStatus As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = " SELECT " & _
                   "     trainingrequestunkid " & _
                   "    ,statusunkid " & _
                   " FROM trtrainingapproval_process_tran " & _
                   " WHERE isvoid = 0 " & _
                   "    AND trainingrequestunkid = @trainingrequestunkid "


            If blnGetApplicationStatus = True Then
                strQ &= "    AND statusunkid = 1 AND visibleid < 2 "
            Else
                strQ &= "    AND statusunkid <> 1 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingRequestunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPendingLoanApplication; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApprovalTranList(ByVal xDatabaseName As String, _
                                        ByVal xUserUnkid As Integer, _
                                        ByVal xYearUnkid As Integer, _
                                        ByVal xCompanyUnkid As Integer, _
                                        ByVal xPeriodStart As DateTime, _
                                        ByVal xPeriodEnd As DateTime, _
                                        ByVal xUserModeSetting As String, _
                                        ByVal xOnlyApproved As Boolean, _
                                        ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                        ByVal strTableName As String, _
                                        Optional ByVal intEmployeeID As Integer = 0, _
                                        Optional ByVal intTrainingRequestId As Integer = 0, _
                                        Optional ByVal mstrFilter As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT " & _
                       "  trtraining_request_master.trainingrequestunkid " & _
                       ", trtrainingapproval_process_tran.pendingtrainingtranunkid " & _
                       ", trtraining_request_master.application_date " & _
                       ", ISNULL(emp.firstname, '') + ' ' + ISNULL(emp.othername, '') + ' ' + ISNULL(emp.surname, '') AS Employee " & _
                       ", trtrainingapproval_process_tran.isvoid " & _
                       ", trtrainingapproval_process_tran.remark " & _
                       ", hrtraining_approverlevel_master.levelunkid " & _
                       ", hrtraining_approverlevel_master.levelname " & _
                       ", hrtraining_approverlevel_master.priority " & _
                       ", trtrainingapproval_process_tran.totalcostamount " & _
                       ", trtrainingapproval_process_tran.approvedamount " & _
                       ", CASE " & _
                            " WHEN trtrainingapproval_process_tran.statusunkid = " & enTrainingRequestStatus.PENDING & " THEN @Pending " & _
                            " WHEN trtrainingapproval_process_tran.statusunkid = " & enTrainingRequestStatus.APPROVED & " THEN @Approved " & _
                            " WHEN trtrainingapproval_process_tran.statusunkid = " & enTrainingRequestStatus.REJECTED & " THEN @Reject " & _
                         " END AS status " & _
                       ", trtrainingapproval_process_tran.employeeunkid " & _
                       ", trtrainingapproval_process_tran.approvertranunkid " & _
                       ", trtrainingapproval_process_tran.userunkid " & _
                       ", trtrainingapproval_process_tran.statusunkid " & _
                       ", ISNULL(trtraining_request_master.statusunkid, 1) AS 'training_statusunkid' " & _
                       ", trtrainingapproval_process_tran.isvoid " & _
                       ", trtrainingapproval_process_tran.remark " & _
                       ", trtrainingapproval_process_tran.mapuserunkid " & _
                       ", CASE " & _
                            " WHEN trtrainingapproval_process_tran.completed_statusunkid = " & enTrainingRequestStatus.PENDING & " THEN @Pending " & _
                            " WHEN trtrainingapproval_process_tran.completed_statusunkid = " & enTrainingRequestStatus.APPROVED & " THEN @Approved " & _
                            " WHEN trtrainingapproval_process_tran.completed_statusunkid = " & enTrainingRequestStatus.REJECTED & " THEN @Reject " & _
                       " END AS status " & _
                       ", trtrainingapproval_process_tran.completed_statusunkid " & _
                       ", trtrainingapproval_process_tran.completed_remark " & _
                     " FROM trtrainingapproval_process_tran " & _
                     " LEFT JOIN trtraining_request_master    ON trtraining_request_master.trainingrequestunkid = trtrainingapproval_process_tran.trainingrequestunkid AND trtraining_request_master.isvoid = 0 " & _
                     " LEFT JOIN cfcommon_master ON cfcommon_master.mastertype = trtraining_request_master.coursemasterunkid " & _
                     " LEFT JOIN hremployee_master emp    ON trtrainingapproval_process_tran.employeeunkid = emp.employeeunkid " & _
                     " LEFT JOIN hrtraining_approver_master ON hrtraining_approver_master.mappingunkid = trtrainingapproval_process_tran.approvertranunkid " & _
                     " LEFT JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = hrtraining_approver_master.levelunkid " & _
                        " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        stationunkid " & _
                            "       ,deptgroupunkid " & _
                            "       ,departmentunkid " & _
                            "       ,sectiongroupunkid " & _
                            "       ,sectionunkid " & _
                            "       ,unitgroupunkid " & _
                            "       ,unitunkid " & _
                            "       ,teamunkid " & _
                            "       ,classgroupunkid " & _
                            "       ,classunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ETT ON ETT.employeeunkid = emp.employeeunkid AND ETT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobgroupunkid " & _
                            "       ,jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ECT ON ECT.employeeunkid = emp.employeeunkid AND ECT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        gradegroupunkid " & _
                            "       ,gradeunkid " & _
                            "       ,gradelevelunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS GRD ON GRD.employeeunkid = emp.employeeunkid AND GRD.rno = 1 "

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "emp")
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry.Replace("hremployee_master", "emp")
            End If

            StrQCondition &= " WHERE trtrainingapproval_process_tran.isvoid = 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry.Replace("hremployee_master", "emp") & " "
                End If
            End If

            If intEmployeeID > 0 Then
                StrQCondition &= " AND trtrainingapproval_process_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            End If

            If intTrainingRequestId > 0 Then
                StrQCondition &= " AND trtrainingapproval_process_tran.trainingrequestunkid = @trainingrequestunkid "
                objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingRequestId)
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrFilter
            End If

            strQ &= StrQCondition
            strQ &= StrQDtFilters

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstraining_requisition_approval_master", 102, "Rejected"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "application_date, priority desc", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApprovalTranList", mstrMessage)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtrainingapproval_process_tran) </purpose>
    Public Function InsertAuditTrailForTrainingApproval(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingtrainingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingTrainingTranunkid.ToString)
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@totalcostamount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecTotalCostAmount.ToString)
            objDataOperation.AddParameter("@approvedamount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecApprovedAmount.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrClientIP.Trim.Length <= 0, getIP, mstrClientIP))
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrHostName.Trim.Length <= 0, getHostName, mstrHostName))
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@completed_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedStatusunkid.ToString)
            objDataOperation.AddParameter("@completed_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompletedRemark.ToString)
            objDataOperation.AddParameter("@completed_visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedVisibleId.ToString)

            strQ = "INSERT INTO attrtrainingapproval_process_tran ( " & _
                      "  tranguid " & _
                      ", pendingtrainingtranunkid " & _
                      ", trainingrequestunkid " & _
                      ", employeeunkid " & _
                      ", approvertranunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", totalcostamount " & _
                      ", approvedamount " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", visibleid " & _
                      ", audittypeid " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", host" & _
                      ", formname " & _
                      ", isweb " & _
                      ", mapuserunkid " & _
                      ", completed_statusunkid " & _
                      ", completed_remark " & _
                      ", completed_visibleid " & _
                    ") VALUES (" & _
                      "  LOWER(NEWID()) " & _
                      ", @pendingtrainingtranunkid " & _
                      ", @trainingrequestunkid " & _
                      ", @employeeunkid " & _
                      ", @approvertranunkid " & _
                      ", @priority " & _
                      ", @approvaldate " & _
                      ", @totalcostamount " & _
                      ", @approvedamount " & _
                      ", @statusunkid " & _
                      ", @remark " & _
                      ", @visibleid " & _
                      ", @audittypeid " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @host" & _
                      ", @formname " & _
                      ", @isweb " & _
                      ", @mapuserunkid " & _
                      ", @completed_statusunkid " & _
                      ", @completed_remark " & _
                      ", @completed_visibleid " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForTrainingApproval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
End Class
