﻿'************************************************************************************************************************************
'Class Name : clsScan_Attach_Documents.vb
'Purpose    :
'Date       :20/09/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Security.AccessControl
Imports System.IO

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsScan_Attach_Documents
    Private Shared ReadOnly mstrModuleName As String = "clsScan_Attach_Documents"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim mstrNewattachdocumentids As String = ""
    Dim mstrDeleteattachdocumentids As String = ""


#Region " ENUM "

    Public Enum enfile_Operation
        COPY = 1
        MOVE = 2
        DELETE = 3
    End Enum

#End Region

#Region " Private Variables "

    Private mdtTran As DataTable
    Private dsList As New DataSet
    Private mintScanattachTranUnkId As Integer = -1
    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    Private mintEmployeeunkid As Integer = -1
    'Nilay (03-Dec-2015) -- End
    'Sohail (04 Jul 2019) -- Start
    'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
    Private xDataOp As clsDataOperation
    'Sohail (04 Jul 2019) -- End

#End Region

#Region " Properties "

    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property
    'Nilay (03-Dec-2015) -- End

    Public Property _Datatable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public ReadOnly Property _Newattachdocumentids() As String
        Get
            Return mstrNewattachdocumentids
        End Get
    End Property

    Public ReadOnly Property _Deleteattachdocumentids() As String
        Get
            Return mstrDeleteattachdocumentids
        End Get
    End Property

    'Sohail (04 Jul 2019) -- Start
    'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property
    'Sohail (04 Jul 2019) -- End

#End Region

#Region " Constructor "

    Public Sub New(Optional ByVal blnIsTable As Boolean = True)
        If blnIsTable = True Then
            mdtTran = New DataTable("Documents")
            mdtTran.Columns.Add("scanattachtranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("documentunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("filename", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("scanattachrefid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("modulerefid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("transactionunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("attached_date", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("orgfilepath", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("valuename", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("doctype", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("destfilepath", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("code", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("names", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("document", System.Type.GetType("System.String")).DefaultValue = ""

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            mdtTran.Columns.Add("fileuniquename", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("filepath", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("filesize", System.Type.GetType("System.Int32")).DefaultValue = -1
            'SHANI (16 JUL 2015) -- End

            'S.SANDEEP [26 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            mdtTran.Columns.Add("form_name", GetType(String)).DefaultValue = ""
            'S.SANDEEP [26 JUL 2016] -- END


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            mdtTran.Columns.Add("fileextension", GetType(String)).DefaultValue = ""
            mdtTran.Columns.Add("Documentype", GetType(String)).DefaultValue = ""
            mdtTran.Columns.Add("DocBase64Value", GetType(String)).DefaultValue = ""
            'Pinkal (20-Nov-2018) -- End

            'S.SANDEEP |25-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            mdtTran.Columns.Add("file_data", GetType(System.Byte())).DefaultValue = DBNull.Value
            'S.SANDEEP |25-JAN-2019| -- END

            'S.SANDEEP |26-APR-2019| -- START
            mdtTran.Columns.Add("PGUID", GetType(System.String)).DefaultValue = ""
            mdtTran.Columns.Add("isinapproval", GetType(System.Boolean)).DefaultValue = False
            'S.SANDEEP |26-APR-2019| -- END

        End If
    End Sub

#End Region

#Region " Public Functions "

    Public Function InsertUpdateDelete_Documents(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        mstrMessage = "" 'Sohail (29 Jan 2013)

        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        mstrNewattachdocumentids = ""
        mstrDeleteattachdocumentids = ""

        'Gajanan [22-Feb-2019] -- End

        Try

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            'If objDOperation Is Nothing Then
            '    Dim objDataOperation As New clsDataOperation
            '    objDataOperation = New clsDataOperation
            '    objDataOperation.BindTransaction()
            'Else
            '    objDataOperation = objDOperation
            'End If
            Dim objDataOperation As clsDataOperation
            If objDOperation Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = objDOperation
            End If

            'SHANI (20 JUN 2015) -- End 

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                StrQ = "INSERT INTO hrdocuments_tran ( " & _
                                            "  documentunkid " & _
              ", employeeunkid " & _
                                            ", filename " & _
              ", scanattachrefid " & _
                                            ", modulerefid " & _
                                            ", userunkid " & _
                                            ", transactionunkid " & _
                                            ", attached_date" & _
                                            ", fileuniquename" & _
                                            ", filepath" & _
                                            ", filesize" & _
                                            ", form_name " & _
                                            ", isactive " & _
                                            ", file_data " & _
                                            ", isinapproval " & _
                                        ") VALUES (" & _
                                            "  @documentunkid " & _
                                            ", @employeeunkid " & _
                                            ", @filename " & _
                                            ", @scanattachrefid " & _
                                            ", @modulerefid " & _
                                            ", @userunkid " & _
                                            ", @transactionunkid " & _
                                            ", @attached_date" & _
                                            ", @fileuniquename " & _
                                            ", @filepath" & _
                                            ", @filesize" & _
                                            ", @form_name " & _
                                            ", @isactive " & _
                                            ", @file_data " & _
                                            ", @isinapproval " & _
                                        "); SELECT @@identity" 'S.SANDEEP [26 JUL 2016] -- START {form_name}-- END
                                'S.SANDEEP |25-JAN-2019| -- START {Ref#2540|ARUTI-} [file_data] -- END
                                'S.SANDEEP |26-APR-2019| -- START {isinapproval} -- END

                                'SHANI (16 JUL 2015)--{fileuniquename,filepath,filesize}

                                objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("documentunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filename").ToString)
                                objDataOperation.AddParameter("@scanattachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("scanattachrefid").ToString)
                                objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("modulerefid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@transactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("transactionunkid").ToString)
                                objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("attached_date"))

                                'SHANI (16 JUL 2015) -- Start
                                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                                'Upload Image folder to access those attachemts from Server and 
                                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                                objDataOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("fileuniquename").ToString)
                                objDataOperation.AddParameter("@filepath", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("filepath").ToString)
                                objDataOperation.AddParameter("@filesize", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("filesize")).ToString)
                                'SHANI (16 JUL 2015) -- End 

                                'S.SANDEEP [26 JUL 2016] -- START
                                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, .Item("form_name").ToString)
                                'S.SANDEEP [26 JUL 2016] -- END


                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, 1)


                                'Gajanan [22-Feb-2019] -- End

                                'S.SANDEEP |25-JAN-2019| -- START
                                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                                If IsDBNull(.Item("file_data")) = False Then
                                    objDataOperation.AddParameter("@file_data", SqlDbType.VarBinary, DirectCast(.Item("file_data"), Byte()).Length, .Item("file_data"))
                                Else
                                    objDataOperation.AddParameter("@file_data", SqlDbType.VarBinary, 0, DBNull.Value)
                                End If
                                'S.SANDEEP |25-JAN-2019| -- END

                                'S.SANDEEP |26-APR-2019| -- START
                                If IsDBNull(.Item("isinapproval")) Then
                                    .Item("isinapproval") = False
                                End If
                                objDataOperation.AddParameter("@isinapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isinapproval")))
                                'S.SANDEEP |26-APR-2019| -- END

                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintScanattachTranUnkId = dsList.Tables(0).Rows(0).Item(0)

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                If mstrNewattachdocumentids.Length > 0 Then
                                    mstrNewattachdocumentids &= "," + mintScanattachTranUnkId.ToString()
                                Else
                                    mstrNewattachdocumentids = mintScanattachTranUnkId.ToString()
                                End If
                                'Gajanan [22-Feb-2019] -- End

                                'Pinkal (20-Nov-2018) -- Start
                                'Enhancement - Working on P2P Integration for NMB.
                                .Item("scanattachtranunkid") = mintScanattachTranUnkId
                                'Pinkal (20-Nov-2018) -- End


                                'SHANI (16 JUL 2015) -- Start
                                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                                'Upload Image folder to access those attachemts from Server and 
                                'all client machines if ArutiSelfService is installed otherwise save then on Document path


                                'If DoFileOperation(.Item("orgfilepath"), .Item("destfilepath").ToString, enfile_Operation.COPY) = False Then

                                '    'Anjan [ 05 Feb 2013 ] -- Start
                                '    'ENHANCEMENT : TRA CHANGES
                                '    'exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    'Throw exForce

                                '    'Pinkal (07-May-2015) -- Start
                                '    'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
                                '    If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                '    'Pinkal (07-May-2015) -- End

                                '    Return False
                                '    'Anjan [ 05 Feb 2013 ] -- End


                                'End If
                                'SHANI (16 JUL 2015) -- End
                            Case "U"
                                StrQ = "UPDATE hrdocuments_tran SET " & _
                                        "  documentunkid = @documentunkid" & _
                                        ", employeeunkid = @employeeunkid" & _
                                        ", filename = @filename" & _
                                        ", scanattachrefid = @scanattachrefid" & _
                                        ", modulerefid = @modulerefid" & _
                                        ", userunkid = @userunkid " & _
                                        ", transactionunkid = @transactionunkid" & _
                                        ", attached_date = @attached_date " & _
                                        ", fileuniquename = @fileuniquename " & _
                                        ", filepath = @filepath " & _
                                        ", filesize = @filesize " & _
                                        ", form_name = @form_name " & _
                                        ", file_data = @file_data " & _
                                        ", isinapproval = @isinapproval " & _
                                      "WHERE scanattachtranunkid = @scanattachtranunkid "
                                'S.SANDEEP |25-JAN-2019| -- START {Ref#2540|ARUTI-} [file_data] -- END
                                'SHANI (16 JUL 2015)--{fileuniquename,filepath}
                                'S.SANDEEP |26-APR-2019| -- START {isinapproval} -- END


                                objDataOperation.AddParameter("@scanattachtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("scanattachtranunkid").ToString)
                                objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("documentunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filename").ToString)
                                objDataOperation.AddParameter("@scanattachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("scanattachrefid").ToString)
                                objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("modulerefid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@transactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("transactionunkid").ToString)
                                objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("attached_date"))
                                'SHANI (16 JUL 2015) -- Start
                                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                                'Upload Image folder to access those attachemts from Server and 
                                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                                objDataOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("fileuniquename").ToString)
                                objDataOperation.AddParameter("@filepath", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("filepath").ToString)
                                objDataOperation.AddParameter("@filesize", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("filesize").ToString)
                                'SHANI (16 JUL 2015) -- End 

                                'S.SANDEEP [26 JUL 2016] -- START
                                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, .Item("form_name"))
                                'S.SANDEEP [26 JUL 2016] -- END

                                'S.SANDEEP |25-JAN-2019| -- START
                                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                                If IsDBNull(.Item("file_data")) = False Then
                                    objDataOperation.AddParameter("@file_data", SqlDbType.VarBinary, DirectCast(.Item("file_data"), Byte()).Length, .Item("file_data"))
                                Else
                                    objDataOperation.AddParameter("@file_data", SqlDbType.VarBinary, 0, DBNull.Value)
                                End If
                                'S.SANDEEP |25-JAN-2019| -- END

                                'S.SANDEEP |26-APR-2019| -- START
                                objDataOperation.AddParameter("@isinapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isinapproval")))
                                'S.SANDEEP |26-APR-2019| -- END

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'SHANI (16 JUL 2015) -- Start
                                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                                'Upload Image folder to access those attachemts from Server and 
                                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                                'If DoFileOperation(.Item("orgfilepath"), .Item("destfilepath").ToString, enfile_Operation.MOVE) = False Then
                                '    'Anjan [ 05 Feb 2013 ] -- Start
                                '    'ENHANCEMENT : TRA CHANGES
                                '    'exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    'Throw exForce

                                '    'Pinkal (07-May-2015) -- Start
                                '    'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
                                '    If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                '    'Pinkal (07-May-2015) -- End
                                '    Return False
                                '    'Anjan [ 05 Feb 2013 ] -- End
                                'End If
                                'SHANI (16 JUL 2015) -- End
                            Case "D"

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                'StrQ = "DELETE FROM hrdocuments_tran " & _
                                '       " WHERE scanattachtranunkid = @scanattachtranunkid "

                                StrQ = "Update hrdocuments_tran SET isactive = 0" & _
                                       " WHERE scanattachtranunkid = @scanattachtranunkid "
                                'Gajanan [22-Feb-2019] -- End

                                objDataOperation.AddParameter("@scanattachtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("scanattachtranunkid").ToString)


                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                                If CInt(.Item("scanattachtranunkid")) > 0 Then
                                    If mstrDeleteattachdocumentids.Length > 0 Then
                                        mstrDeleteattachdocumentids &= "," + .Item("scanattachtranunkid").ToString
                                    Else
                                        mstrDeleteattachdocumentids = .Item("scanattachtranunkid").ToString
                                    End If
                                End If

                                'Gajanan [22-Feb-2019] -- End


                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'SHANI (16 JUL 2015) -- Start
                                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                                'Upload Image folder to access those attachemts from Server and 
                                'all client machines if ArutiSelfService is installed otherwise save then on Document path

                                'If DoFileOperation(.Item("orgfilepath"), .Item("destfilepath").ToString, enfile_Operation.DELETE) = False Then
                                '    'Anjan [ 05 Feb 2013 ] -- Start
                                '    'ENHANCEMENT : TRA CHANGES
                                '    'exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    'Throw exForce

                                '    'Pinkal (07-May-2015) -- Start
                                '    'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
                                '    If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                '    'Pinkal (07-May-2015) -- End

                                '    Return False
                                '    'Anjan [ 05 Feb 2013 ] -- End
                                'End If

                                'SHANI (16 JUL 2015) -- End

                        End Select
                    End If
                End With
            Next


            'Pinkal (07-May-2015) -- Start
            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Pinkal (07-May-2015) -- End

            Return True

        Catch ex As Exception

            'Pinkal (07-May-2015) -- Start
            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Pinkal (07-May-2015) -- End
            'exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_Documents; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Delete(ByVal strUnkids As String, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            If strUnkids.Trim.Length > 0 Then

                If objDOperation Is Nothing Then
                    objDataOperation = New clsDataOperation
                    objDataOperation.BindTransaction()
                Else
                    objDataOperation = objDOperation
                End If

                For Each StrId As String In strUnkids.Split(",")
                    If StrId.Trim.Length > 0 Then

                        objDataOperation.ClearParameters()

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : New Screen Training Need Form.
                        'StrQ = "DELETE FROM hrdocuments_tran WHERE scanattachtranunkid = @scanattachtranunkid "
                        StrQ = "Update hrdocuments_tran SET isactive = 0" & _
                                       " WHERE scanattachtranunkid = @scanattachtranunkid "
                        'Sohail (14 Nov 2019) -- End

                        objDataOperation.AddParameter("@scanattachtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, StrId.ToString)

                        Call objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If
                Next

                If objDOperation Is Nothing Then
                    objDataOperation.ReleaseTransaction(True)
                End If
            End If

            Return True

        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            Throw exForce
        Finally
        End Try
    End Function

    'Pinkal (07-May-2015) -- End

    Public Function GetDocType(ByVal intCompanyId As Integer, ByVal blnAddApprovalFilter As Boolean, Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsCombo As New DataSet
        Try
            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
            'objDataOperation = New clsDataOperation
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Sohail (04 Jul 2019) -- End

            'S.SANDEEP |26-APR-2019| -- START
            Dim strFilter As String = String.Empty
            If blnAddApprovalFilter Then
                Dim strKeyValue As String = String.Empty
                Dim objConfig As New clsConfigOptions
                strKeyValue = objConfig.GetKeyValue(intCompanyId, "SkipApprovalOnEmpData")
                objConfig = Nothing
                If strKeyValue IsNot Nothing AndAlso strKeyValue.Trim.Length > 0 Then
                    Dim oFile As List(Of Integer) = strKeyValue.Split(",").Select(Function(x) CInt(x)).ToList()
                    If oFile.IndexOf(CInt(enScreenName.frmDependantsAndBeneficiariesList)) <= -1 Then
                        strFilter &= "," & CInt(enScanAttactRefId.DEPENDANTS).ToString()
                    End If
                    If oFile.IndexOf(CInt(enScreenName.frmQualificationsList)) <= -1 Then
                        strFilter &= "," & CInt(enScanAttactRefId.QUALIFICATIONS).ToString()
                    End If
                    If oFile.IndexOf(CInt(enScreenName.frmJobHistory_ExperienceList)) <= -1 Then
                        strFilter &= "," & CInt(enScanAttactRefId.JOBHISTORYS).ToString()
                    End If
                    If oFile.IndexOf(CInt(enScreenName.frmIdentityInfoList)) <= -1 Then
                        strFilter &= "," & CInt(enScanAttactRefId.IDENTITYS).ToString()
                    End If
                    If oFile.IndexOf(CInt(enScreenName.frmAddressList)) <= -1 Then
                        strFilter &= "," & CInt(enScanAttactRefId.PERSONAL_ADDRESS_TYPE).ToString() & _
                                     "," & CInt(enScanAttactRefId.DOMICILE_ADDRESS_TYPE).ToString() & _
                                     "," & CInt(enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE).ToString()
                    End If
                    If oFile.IndexOf(CInt(enScreenName.frmEmergencyAddressList)) <= -1 Then
                        strFilter &= "," & CInt(enScanAttactRefId.EMERGENCY_CONTACT1).ToString() & _
                                     "," & CInt(enScanAttactRefId.EMERGENCY_CONTACT2).ToString() & _
                                     "," & CInt(enScanAttactRefId.EMERGENCY_CONTACT3).ToString()
                    End If
                    If oFile.IndexOf(CInt(enScreenName.frmMembershipInfoList)) <= -1 Then
                        strFilter &= "," & CInt(enScanAttactRefId.MEMBERSHIPS).ToString()
                    End If
                    If oFile.IndexOf(CInt(enScreenName.frmBirthinfo)) <= -1 Then
                        strFilter &= "," & CInt(enScanAttactRefId.EMPLOYEE_BIRTHINFO).ToString()
                    End If
                    If oFile.IndexOf(CInt(enScreenName.frmOtherinfo)) <= -1 Then
                        strFilter &= "," & CInt(enScanAttactRefId.EMPLYOEE_OTHERLDETAILS).ToString()
                    End If
                End If
            End If
            'S.SANDEEP |26-APR-2019| -- END

            StrQ = "SELECT Id, Name FROM ( "
            StrQ &= " SELECT 0 AS Id,  @Select AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.IDENTITYS & " AS Id, @IDENTITYS AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.MEMBERSHIPS & " AS Id, @MEMBERSHIPS AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.QUALIFICATIONS & " AS Id, @QUALIFICATIONS AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.JOBHISTORYS & " AS Id, @JOBHISTORYS AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.LEAVEFORMS & " AS Id, @LEAVEFORMS AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.DISCIPLINES & " AS Id, @DISCIPLINES AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.CURRICULAM_VITAE & " AS Id, @CURRICULAM_VITAE AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.OTHERS & " AS Id, @OTHERS AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.TRAININGS & " AS Id, @TRAININGS AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.ASSET_DECLARATION & " AS Id, @ASSET_DECLARATION AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.DEPENDANTS & " AS Id, @DEPENDANTS AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.CLAIM_REQUEST & " AS Id, @CLAIM_REQUEST AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.ASSESSMENT & " AS Id, @ASSESSMENT AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.PERSONAL_ADDRESS_TYPE & " AS Id, @PERSONAL_ADDRESS_TYPE AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.DOMICILE_ADDRESS_TYPE & " AS Id, @DOMICILE_ADDRESS_TYPE AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE & " AS Id, @RECRUITMENT_ADDRESS_TYPE AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.EMERGENCY_CONTACT1 & " AS Id, @EMERGENCY_CONTACT1 AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.EMERGENCY_CONTACT2 & " AS Id, @EMERGENCY_CONTACT2 AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.EMERGENCY_CONTACT3 & " AS Id, @EMERGENCY_CONTACT3 AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.EMPLOYEE_BIRTHINFO & " AS Id, @EMPLOYEE_BIRTHINFO AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.EMPLYOEE_OTHERLDETAILS & " AS Id, @EMPLYOEE_OTHERLDETAILS AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.COVER_LETTER & " AS Id, @COVER_LETTER AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.STAFF_REQUISITION & " AS Id, @STAFF_REQUISITION AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.CLAIM_RETIREMENT & " AS Id, @CLAIM_RETIREMENT AS Name " & _
                    " UNION SELECT " & enScanAttactRefId.TRAINING_NEED_FORM & " AS Id, @TRAINING_NEED_FORM AS Name "
            'Sohail (14 Nov 2019) -- [TRAINING_NEED_FORM]
            'Pinkal (11-Sep-2019) -- Enhancement NMB - Working On Claim Retirement for NMB.[" UNION SELECT " & enScanAttactRefId.CLAIM_RETIREMENT & " AS Id, @CLAIM_RETIREMENT AS Name "]
            'Gajanan [02-SEP-2019] -- [STAFF_REQUISITION]

            'Sohail (04 Jul 2019) - [COVER_LETTER]

            StrQ &= ") AS A WHERE 1 = 1 "

            'S.SANDEEP |26-APR-2019| -- START
            If blnAddApprovalFilter Then
                If strFilter.Trim.Length > 0 Then
                    strFilter = Mid(strFilter, 2)
                    StrQ &= " AND Id NOT IN (" & strFilter & ")"
                End If
            End If
            'S.SANDEEP |26-APR-2019| -- END

            'S.SANDEEP |26-APR-2019| -- START {PERSONAL_ADDRESS_TYPE,DOMICILE_ADDRESS_TYPE,RECRUITMENT_ADDRESS_TYPE,EMERGENCY_CONTACT1,EMERGENCY_CONTACT2,EMERGENCY_CONTACT3} -- END
            'Shani (20-Aug-2016)--Add-->[CLAIM_REQUEST]-->'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew

            'Sohail (07 Mar 2012) - [ASSET_DECLARATION] -- SHANI (20 JUN 2015) - [DEPENDANTS]

            objDataOperation.AddParameter("@IDENTITYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Identities"))
            objDataOperation.AddParameter("@MEMBERSHIPS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Memberships"))
            objDataOperation.AddParameter("@QUALIFICATIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Qualifications"))
            objDataOperation.AddParameter("@JOBHISTORYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Job History"))
            objDataOperation.AddParameter("@LEAVEFORMS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Leave Forms"))
            objDataOperation.AddParameter("@DISCIPLINES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Discipline"))
            objDataOperation.AddParameter("@CURRICULAM_VITAE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Curriculum Vitae"))
            objDataOperation.AddParameter("@OTHERS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Others"))
            objDataOperation.AddParameter("@TRAININGS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Trainings"))
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Select"))
            objDataOperation.AddParameter("@ASSET_DECLARATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Asset Declaration")) 'Sohail (07 Mar 2012)
            objDataOperation.AddParameter("@Dependants", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Dependants")) 'Shani (20 JUN 2015)

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            objDataOperation.AddParameter("@CLAIM_REQUEST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Claim Request"))
            'Shani (20-Aug-2016) -- End

            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 136
            objDataOperation.AddParameter("@ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Assessment"))
            'S.SANDEEP [29-NOV-2017] -- END


            'S.SANDEEP |26-APR-2019| -- START
            objDataOperation.AddParameter("@PERSONAL_ADDRESS_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Personal Address"))
            objDataOperation.AddParameter("@DOMICILE_ADDRESS_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Domicile Address"))
            objDataOperation.AddParameter("@RECRUITMENT_ADDRESS_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Recruitment Address"))
            objDataOperation.AddParameter("@EMERGENCY_CONTACT1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Emergency Contact1"))
            objDataOperation.AddParameter("@EMERGENCY_CONTACT2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Emergency Contact2"))
            objDataOperation.AddParameter("@EMERGENCY_CONTACT3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Emergency Contact3"))
            objDataOperation.AddParameter("@EMPLOYEE_BIRTHINFO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Employee BirthInfo"))
            objDataOperation.AddParameter("@EMPLYOEE_OTHERLDETAILS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Employee Other Info"))
            'S.SANDEEP |26-APR-2019| -- END
            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
            objDataOperation.AddParameter("@COVER_LETTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Cover Letter"))
            'Sohail (04 Jul 2019) -- End

            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            objDataOperation.AddParameter("@STAFF_REQUISITION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Staff Requisition"))
            'Gajanan [02-SEP-2019] -- End


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            objDataOperation.AddParameter("@CLAIM_RETIREMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Claim Retirement"))
            'Pinkal (11-Sep-2019) -- End
            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : New Screen Training Need Form.
            objDataOperation.AddParameter("@TRAINING_NEED_FORM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Training Need Form"))
            'Sohail (14 Nov 2019) -- End

            If StrList.Trim.Length <= 0 Then StrList = "List"

            dsCombo = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsCombo

        Catch ex As Exception
            Throw ex
        Finally
            dsCombo.Dispose()
        End Try
    End Function

    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Public Function GetDocFolderName(Optional ByVal StrList As String = "List", Optional ByVal intScanAttactRefId As Integer = 0) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsCombo As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT * FROM ( " & _
                        " SELECT 0 AS Id,  @Select AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.IDENTITYS & " AS Id, @IDENTITYS AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.MEMBERSHIPS & " AS Id, @MEMBERSHIPS AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.QUALIFICATIONS & " AS Id, @QUALIFICATIONS AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.JOBHISTORYS & " AS Id, @JOBHISTORYS AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.LEAVEFORMS & " AS Id, @LEAVEFORMS AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.DISCIPLINES & " AS Id, @DISCIPLINES AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.CURRICULAM_VITAE & " AS Id, @CURRICULAM_VITAE AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.OTHERS & " AS Id, @OTHERS AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.TRAININGS & " AS Id, @TRAININGS AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.ASSET_DECLARATION & " AS Id, @ASSET_DECLARATION AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.DEPENDANTS & " AS Id, @DEPENDANTS AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.CLAIM_REQUEST & " AS Id, @CLAIM_REQUEST AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.ASSESSMENT & " AS Id, @ASSESSMENT AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.PERSONAL_ADDRESS_TYPE & " AS Id, @PERSONAL_ADDRESS_TYPE AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.DOMICILE_ADDRESS_TYPE & " AS Id, @DOMICILE_ADDRESS_TYPE AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE & " AS Id, @RECRUITMENT_ADDRESS_TYPE AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.EMERGENCY_CONTACT1 & " AS Id, @EMERGENCY_CONTACT1 AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.EMERGENCY_CONTACT2 & " AS Id, @EMERGENCY_CONTACT2 AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.EMERGENCY_CONTACT3 & " AS Id, @EMERGENCY_CONTACT3 AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.EMPLOYEE_BIRTHINFO & " AS Id, @EMPLOYEE_BIRTHINFO AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.EMPLYOEE_OTHERLDETAILS & " AS Id, @EMPLYOEE_OTHERLDETAILS AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.COVER_LETTER & " AS Id, @COVER_LETTER AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.STAFF_REQUISITION & " AS Id, @STAFF_REQUISITION AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.CLAIM_RETIREMENT & " AS Id, @CLAIM_RETIREMENT AS Name " & _
                        " UNION SELECT " & enScanAttactRefId.TRAINING_NEED_FORM & " AS Id, @TRAINING_NEED_FORM AS Name " & _
                        " ) AS A "
            'Pinkal (11-Sep-2019) -- 'Enhancement NMB - Working On Claim Retirement for NMB.[" UNION SELECT " & enScanAttactRefId.CLAIM_RETIREMENT & " AS Id, @COVER_LETTER AS Name " & _]
            'Gajanan [02-SEP-2019] -- [STAFF_REQUISITION]
            'Sohail (04 Jul 2019) - [COVER_LETTER]

            'S.SANDEEP |26-APR-2019| -- START {PERSONAL_ADDRESS_TYPE,DOMICILE_ADDRESS_TYPE,RECRUITMENT_ADDRESS_TYPE,EMERGENCY_CONTACT1,EMERGENCY_CONTACT2,EMERGENCY_CONTACT3} -- END

            'Shani (20-Aug-2016) --Add-->[" UNION SELECT " & enScanAttactRefId.CLAIM_REQUEST & " AS Id, @CLAIM_REQUEST AS Name "]


            If intScanAttactRefId > 0 Then
                StrQ &= " WHERE A.Id = " & intScanAttactRefId & " "
            End If
            objDataOperation.AddParameter("@IDENTITYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Identities")
            objDataOperation.AddParameter("@MEMBERSHIPS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Memberships")
            objDataOperation.AddParameter("@QUALIFICATIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Qualifications")
            objDataOperation.AddParameter("@JOBHISTORYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Job History")
            objDataOperation.AddParameter("@LEAVEFORMS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Leave Forms")
            objDataOperation.AddParameter("@DISCIPLINES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Discipline")
            objDataOperation.AddParameter("@CURRICULAM_VITAE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Curriculum Vitae")
            objDataOperation.AddParameter("@OTHERS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Others")
            objDataOperation.AddParameter("@TRAININGS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Trainings")
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")
            objDataOperation.AddParameter("@ASSET_DECLARATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Asset Declaration")
            objDataOperation.AddParameter("@Dependants", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Dependants")

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            objDataOperation.AddParameter("@CLAIM_REQUEST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "ClaimRequest")
            'Shani (20-Aug-2016) -- End

            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 136
            objDataOperation.AddParameter("@ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Assessment"))
            'S.SANDEEP [29-NOV-2017] -- END

            'S.SANDEEP |26-APR-2019| -- START
            objDataOperation.AddParameter("@PERSONAL_ADDRESS_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Personal Address"))
            objDataOperation.AddParameter("@DOMICILE_ADDRESS_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Domicile Address"))
            objDataOperation.AddParameter("@RECRUITMENT_ADDRESS_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Recruitment Address"))
            objDataOperation.AddParameter("@EMERGENCY_CONTACT1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Emergency Contact1"))
            objDataOperation.AddParameter("@EMERGENCY_CONTACT2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Emergency Contact2"))
            objDataOperation.AddParameter("@EMERGENCY_CONTACT3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Emergency Contact3"))
            objDataOperation.AddParameter("@EMPLOYEE_BIRTHINFO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Employee BirthInfo"))
            objDataOperation.AddParameter("@EMPLYOEE_OTHERLDETAILS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Employee Other Info"))
            'S.SANDEEP |26-APR-2019| -- END
            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
            objDataOperation.AddParameter("@COVER_LETTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Cover Letter"))
            objDataOperation.AddParameter("@STAFF_REQUISITION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Staff Requisition")) 'Gajanan [02-SEP-2019]
            'Sohail (04 Jul 2019) -- End
            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            objDataOperation.AddParameter("@CLAIM_RETIREMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Claim Retirement"))
            'Pinkal (11-Sep-2019) -- End
            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : New Screen Training Need Form.
            objDataOperation.AddParameter("@TRAINING_NEED_FORM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Training Need Form"))
            'Sohail (14 Nov 2019) -- End

            If StrList.Trim.Length <= 0 Then StrList = "List"

            dsCombo = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsCombo

        Catch ex As Exception
            Throw ex
        Finally
            dsCombo.Dispose()
        End Try
    End Function
    'SHANI (16 JUL 2015) -- End 

    Public Function IsExist(ByVal intModuleRefid As Integer, _
                            ByVal intScanRefId As Integer, _
                            ByVal mstrFile_Name As String, _
                            ByVal intEmpId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal inttransactionUnkid As Integer = -1) As Boolean

        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = -1 : Dim blnFlag As Boolean = False : Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            'StrQ &= "SELECT * FROM hrdocuments_tran " & _
            '        " WHERE modulerefid = @ModuleRefid AND scanattachrefid = @ScanRefId AND [filename] = @File_Name AND employeeunkid = @EmpId and isactive =1"
            StrQ &= "SELECT * FROM hrdocuments_tran " & _
                    " WHERE modulerefid = @ModuleRefid AND scanattachrefid = @ScanRefId AND [filename] = @File_Name  and isactive =1"

            If intEmpId > 0 Then
                StrQ &= " AND employeeunkid = @EmpId "
            End If
            'Gajanan [02-SEP-2019] -- End

            objDataOperation.AddParameter("@ModuleRefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            objDataOperation.AddParameter("@ScanRefId", SqlDbType.Int, eZeeDataType.INT_SIZE, intScanRefId)
            objDataOperation.AddParameter("@File_Name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFile_Name)
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

            If intUnkid > 0 Then
                StrQ &= " AND scanattachtranunkid <> @Unkid "
                objDataOperation.AddParameter("@Unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            If inttransactionUnkid > 0 Then
                StrQ &= " AND transactionunkid = @transactionUnkid "
                objDataOperation.AddParameter("@transactionUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, inttransactionUnkid)
            End If
            'Gajanan [02-SEP-2019] -- End

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function GetList(Optional ByVal strList As String = "List", Optional ByVal StrEditUnkids As String = "" _
    '                            , Optional ByVal strDocumentPath As String = "") As DataSet 'Sohail (23 Apr 2012) - [strDocumentPath]

    'S.SANDEEP |13-OCT-2021| -- START
    'Public Function GetList(ByVal strDocument_Path As String, _
    '                        ByVal strList As String, _
    '                        ByVal StrEditUnkids As String, _
    '                        Optional ByVal intEmployeeId As Integer = -1, _
    '                        Optional ByVal blnIncludeDocInApproval As Boolean = False, _
    '                        Optional ByVal xDataOperation As clsDataOperation = Nothing, _
    '                        Optional ByVal dtAsonDate As Date = Nothing, _
    '                        Optional ByVal strFilter As String = "", _
    '                        Optional ByVal blnOnlyStructure As Boolean = False, _
    '                        Optional ByVal intVacancyId As Integer = -1, _
    '                        Optional ByVal intScanAttachRefId As Integer = 0 _
    '                        ) As DataSet 'S.SANDEEP |26-APR-2019| -- START {blnIncludeDocInApproval,xDataOperation} -- END
    '    'Sohail (03 Sep 2021) - [intScanAttachRefId]
    '    'Hemant (29 Dec 2020) -- [intVacancyId]
    '    'S.SANDEEP |29-SEP-2020| -- START {blnOnlyStructure} -- END
    '    'Sohail (18 May 2019) - [dtAsonDate]
    '    'Nilay (03-Dec-2015) -- [Optional ByVal intEmployeeId As Integer = -1]

    '    'Shani(24-Aug-2015) -- End
    '    Dim StrQ As String = String.Empty
    '    Dim ds As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        'Sohail (04 Jul 2019) -- Start
    '        'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
    '        _xDataOp = xDataOperation
    '        Dim dsDocType As DataSet = GetDocType(0, False, "List")
    '        Dim dicDocType As Dictionary(Of Integer, String) = (From p In dsDocType.Tables("List") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
    '        'Sohail (04 Jul 2019) -- End

    '        'S.SANDEEP |26-APR-2019| -- START
    '        'objDataOperation = New clsDataOperation
    '        If xDataOperation Is Nothing Then
    '            objDataOperation = New clsDataOperation
    '        Else
    '            objDataOperation = xDataOperation
    '        End If
    '        objDataOperation.ClearParameters()
    '        'S.SANDEEP |26-APR-2019| -- END

    '        'Sohail (18 May 2019) -- Start
    '        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    '        StrQ = "SELECT * " & _
    '                   "INTO #TableDepn " & _
    '                   "FROM " & _
    '                   "( " & _
    '                       "SELECT hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid " & _
    '                            ", hrdependant_beneficiaries_status_tran.isactive " & _
    '                            ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
    '                       "FROM hrdependant_beneficiaries_status_tran "

    '        If intEmployeeId > 0 Then
    '            StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
    '        End If

    '        StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

    '        'S.SANDEEP |29-SEP-2020| -- START
    '        If blnOnlyStructure Then
    '            StrQ &= " AND 1 = 2 "
    '        End If
    '        'S.SANDEEP |29-SEP-2020| -- END

    '        If intEmployeeId > 0 Then
    '            StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = " & intEmployeeId & " "
    '        End If

    '        If dtAsonDate <> Nothing Then
    '            StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
    '            objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsonDate))
    '        Else
    '            'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
    '        End If

    '        StrQ &= ") AS A " & _
    '                "WHERE 1 = 1 " & _
    '                " AND A.ROWNO = 1 "
    '        'Sohail (18 May 2019) -- End

    '        'Sohail (04 Jul 2019) -- Start
    '        'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
    '        'StrQ &= "SELECT " & _
    '        '             " CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(hremployee_master.employeecode,'') " & _
    '        '                    "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(applicant_code,'') END AS CODE " & _
    '        '             ",CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL (hremployee_master.surname,'') " & _
    '        '                    "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.othername,'')+' '+ISNULL(rcapplicant_master.surname,'')  END AS ENAME " & _
    '        '             ",ISNULL(cfcommon_master.name,'') AS DOC_NAME " & _
    '        '             ",CASE WHEN scanattachrefid  = " & enScanAttactRefId.IDENTITYS & " THEN @IDENTITYS " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.MEMBERSHIPS & " THEN @MEMBERSHIPS " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.QUALIFICATIONS & " THEN @QUALIFICATIONS " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.JOBHISTORYS & " THEN @JOBHISTORYS " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " THEN @LEAVEFORMS " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.DISCIPLINES & " THEN @DISCIPLINES " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.CURRICULAM_VITAE & " THEN @CURRICULAM_VITAE " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.OTHERS & " THEN @OTHERS " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.TRAININGS & " THEN @TRAININGS " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " THEN @ASSET_DECLARATION " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.DEPENDANTS & " THEN @Dependants " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " THEN @CLAIM_REQUEST " & _
    '        '                    "WHEN scanattachrefid = " & enScanAttactRefId.ASSESSMENT & " THEN @ASSESSMENT " & _
    '        '             " END AS DOC_TYPE " & _
    '        '             ",[filename] AS ATTACHED_FILE " & _
    '        '             ",CASE WHEN hrdocuments_tran.scanattachrefid   = " & enScanAttactRefId.IDENTITYS & " THEN ISNULL(IDTYPE.NAME,'') " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.MEMBERSHIPS & " THEN ISNULL(hrmembership_master.membershipname,'') " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.QUALIFICATIONS & " THEN CASE WHEN hrdocuments_tran.modulerefid = 1 THEN ISNULL(hrqualification_master.qualificationname,'') " & _
    '        '                                                                         "WHEN  hrdocuments_tran.modulerefid = 2 THEN ISNULL(rcQualif.qualificationname,'') END " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.JOBHISTORYS & " THEN CASE WHEN hrdocuments_tran.modulerefid = 1 THEN ISNULL(hremp_experience_tran.company,'') " & _
    '        '                                                                         "WHEN  hrdocuments_tran.modulerefid = 2 THEN ISNULL(rcjobhistory.companyname,'') END " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.LEAVEFORMS & " THEN CASE WHEN ISNULL(lvcancelform.cancelformno,'') <> '' THEN ISNULL(lvcancelform.cancelformno,'') ELSE  ISNULL(lvleaveform.formno, '') END " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.DISCIPLINES & " THEN CASE WHEN ISNULL(hrdiscipline_analysis.incident_caused,'') = '' THEN " & _
    '        '                                                                                                "ISNULL (hraction_reason_master.reason_action,'') " & _
    '        '                                                                                      "ELSE  hrdiscipline_analysis.incident_caused END " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CURRICULAM_VITAE & " THEN @CURRICULAM_VITAE " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.OTHERS & " THEN @OTHERS " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.TRAININGS & " THEN ISNULL(hrtraining_scheduling.course_title,'') " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.ASSET_DECLARATION & " THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL (hremployee_master.surname,'') " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.DEPENDANTS & " THEN ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL (hrdependants_beneficiaries_tran.last_name,'') " & _
    '        '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CLAIM_REQUEST & " THEN ISNULL(cmclaim_request_master.claimrequestno,'') " & _
    '        '             " END AS AVALUE " & _
    '        '             ",CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN hremployee_master.employeeunkid " & _
    '        '                    "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN rcapplicant_master.applicantunkid END AS UNKID " & _
    '        '             ",CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN 0 " & _
    '        '                    "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN 1 END AS IsApplicant " & _
    '        '             ",scanattachrefid AS DOCTYPEID " & _
    '        '             ",ISNULL(hrdocuments_tran.modulerefid,-1) AS modulerefid " & _
    '        '             ",ISNULL(hrdocuments_tran.scanattachrefid,-1) AS scanattachrefid " & _
    '        '             ",ISNULL(hrdocuments_tran.scanattachtranunkid,-1) AS scanattachtranunkid " & _
    '        '             ",ISNULL(hrdocuments_tran.transactionunkid,-1) AS transactionunkid " & _
    '        '             ",ISNULL(hrdocuments_tran.userunkid,1) AS userunkid " & _
    '        '             ",ISNULL(hrdocuments_tran.attached_date,'19000101') AS attached_date " & _
    '        '             ",ISNULL(hrdocuments_tran.documentunkid,0) AS documentunkid " & _
    '        '             ",ISNULL(hrdocuments_tran.fileuniquename,'') AS fileuniquename " & _
    '        '             ",ISNULL(hrdocuments_tran.filepath,'') AS filepath " & _
    '        '             ",ISNULL(hrdocuments_tran.filesize,-1) AS filesize "
    '        StrQ &= "SELECT "
    '        'Hemant (29 Dec 2020) -- Start
    '        'Enhancement # AH-2017 : PACT TZ-Assist to download CV in bulk by particular Vacancy 
    '        StrQ &= " DISTINCT "
    '        'Hemant (29 Dec 2020) -- End
    '        StrQ &= " CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(hremployee_master.employeecode,'') " & _
    '                            "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(applicant_code,'') END AS CODE " & _
    '                     ",CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL (hremployee_master.surname,'') " & _
    '                            "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.othername,'')+' '+ISNULL(rcapplicant_master.surname,'')  END AS ENAME " & _
    '                     ",ISNULL(cfcommon_master.name,'') AS DOC_NAME " & _
    '                     ",[filename] AS ATTACHED_FILE " & _
    '                     ",CASE WHEN hrdocuments_tran.scanattachrefid   = " & enScanAttactRefId.IDENTITYS & " THEN ISNULL(IDTYPE.NAME,'') " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.MEMBERSHIPS & " THEN ISNULL(hrmembership_master.membershipname,'') " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.QUALIFICATIONS & " THEN CASE WHEN hrdocuments_tran.modulerefid = 1 THEN ISNULL(hrqualification_master.qualificationname,'') " & _
    '                                                                                 "WHEN  hrdocuments_tran.modulerefid = 2 THEN ISNULL(rcQualif.qualificationname,'') END " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.JOBHISTORYS & " THEN CASE WHEN hrdocuments_tran.modulerefid = 1 THEN ISNULL(hremp_experience_tran.company,'') " & _
    '                                                                                 "WHEN  hrdocuments_tran.modulerefid = 2 THEN ISNULL(rcjobhistory.companyname,'') END " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.LEAVEFORMS & " THEN CASE WHEN ISNULL(lvcancelform.cancelformno,'') <> '' THEN ISNULL(lvcancelform.cancelformno,'') ELSE  ISNULL(lvleaveform.formno, '') END " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.DISCIPLINES & " THEN CASE WHEN ISNULL(hrdiscipline_analysis.incident_caused,'') = '' THEN " & _
    '                                                                                                        "ISNULL (hraction_reason_master.reason_action,'') " & _
    '                                                                                              "ELSE  hrdiscipline_analysis.incident_caused END " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CURRICULAM_VITAE & " THEN @CURRICULAM_VITAE " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.OTHERS & " THEN @OTHERS " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.TRAININGS & " THEN ISNULL(hrtraining_scheduling.course_title,'') " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.ASSET_DECLARATION & " THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL (hremployee_master.surname,'') " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.DEPENDANTS & " THEN ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL (hrdependants_beneficiaries_tran.last_name,'') " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CLAIM_REQUEST & " THEN ISNULL(cmclaim_request_master.claimrequestno,'') " & _
    '                    "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.COVER_LETTER & " THEN @COVER_LETTER " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.STAFF_REQUISITION & " THEN @STAFF_REQUISITION " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CLAIM_RETIREMENT & " THEN @CLAIM_RETIREMENT " & _
    '                            "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.TRAINING_NEED_FORM & " THEN @TRAINING_NEED_FORM " & _
    '                     " END AS AVALUE " & _
    '                     ",CASE WHEN modulerefid = " & enImg_Email_RefId.Staff_Requisition & " THEN 0 WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN hremployee_master.employeeunkid " & _
    '                            "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN rcapplicant_master.applicantunkid END AS UNKID " & _
    '                     ",CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN 0 " & _
    '                            "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN 1 END AS IsApplicant " & _
    '                     ",scanattachrefid AS DOCTYPEID " & _
    '                     ",ISNULL(hrdocuments_tran.modulerefid,-1) AS modulerefid " & _
    '                     ",ISNULL(hrdocuments_tran.scanattachrefid,-1) AS scanattachrefid " & _
    '                     ",ISNULL(hrdocuments_tran.scanattachtranunkid,-1) AS scanattachtranunkid " & _
    '                     ",ISNULL(hrdocuments_tran.transactionunkid,-1) AS transactionunkid " & _
    '                     ",ISNULL(hrdocuments_tran.userunkid,1) AS userunkid " & _
    '                     ",ISNULL(hrdocuments_tran.attached_date,'19000101') AS attached_date " & _
    '                     ",ISNULL(hrdocuments_tran.documentunkid,0) AS documentunkid " & _
    '                     ",ISNULL(hrdocuments_tran.fileuniquename,'') AS fileuniquename " & _
    '                     ",ISNULL(hrdocuments_tran.filepath,'') AS filepath " & _
    '                     ",ISNULL(hrdocuments_tran.filesize,-1) AS filesize "
    '        'Sohail (14 Nov 2019) -- [TRAINING_NEED_FORM]
    '        'Gajanan [02-SEP-2019] -- [enImg_Email_RefId.Staff_Requisition]
    '        'Sohail (04 Jul 2019) -- End


    '        'Pinkal (11-Sep-2019) -- Enhancement NMB - Working On Claim Retirement for NMB.[ '"WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CLAIM_RETIREMENT & " THEN @CLAIM_RETIREMENT " & _]


    '        'S.SANDEEP [26 JUL 2016] -- START
    '        'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    '        StrQ &= ",ISNULL(hrdocuments_tran.form_name,'') AS form_name  "
    '        'S.SANDEEP [26 JUL 2016] -- END


    '        'Pinkal (20-Nov-2018) -- Start
    '        'Enhancement - Working on P2P Integration for NMB.
    '        StrQ &= ", '' AS fileextension " & _
    '                        ", '' AS Documentype " & _
    '                        ", '' AS DocBase64Value "
    '        'Pinkal (20-Nov-2018) -- End

    '        'S.SANDEEP |25-JAN-2019| -- START
    '        'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
    '        StrQ &= ",hrdocuments_tran.file_data "
    '        'S.SANDEEP |25-JAN-2019| -- END

    '        'Sohail (26 Mar 2012) - [ASSET_DECLARATION]
    '        'SHANI (20 JUN 2015) -- [Dependants]
    '        'SHANI (16 JUL 2015) -- [fileuniquename,filepath,filesize]

    '        'Nilay (03-Dec-2015) -- Start
    '        'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    '        'If StrEditUnkids.Trim.Length > 0 Then
    '        '    StrQ &= ",'' AS AUD "
    '        'End If
    '        'If StrEditUnkids.Trim.Length > 0 OrElse intEmployeeId > 0 Then
    '        '    StrQ &= ",'' AS AUD "
    '        'End If
    '        StrQ &= ",'' AS AUD " & _
    '                ",ISNULL(cfcommon_master.ismandatoryfornewemp,0) AS ismandatoryfornewemp "
    '        'Nilay (03-Dec-2015) -- End

    '        'S.SANDEEP |26-APR-2019| -- START
    '        StrQ &= ",isinapproval "
    '        'S.SANDEEP |26-APR-2019| -- END

    '        'Sohail (04 Jul 2019) -- Start
    '        'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
    '        StrQ &= ", CASE hrdocuments_tran.scanattachrefid "
    '        For Each pair In dicDocType
    '            StrQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
    '        Next
    '        StrQ &= " END AS DOC_TYPE "
    '        'Sohail (04 Jul 2019) -- End
    '        'Hemant (29 Dec 2020) -- Start
    '        'Enhancement # AH-2017 : PACT TZ-Assist to download CV in bulk by particular Vacancy 
    '        If intVacancyId > 0 Then
    '            StrQ &= " ,ISNULL(rcapp_vacancy_mapping.vacancyunkid,0) AS vacancyunkid "
    '        End If
    '        'Hemant (29 Dec 2020) -- End

    '        StrQ &= "FROM hrdocuments_tran " & _
    '             "LEFT JOIN cfcommon_master ON dbo.cfcommon_master.masterunkid = hrdocuments_tran.documentunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " " & _
    '             "LEFT JOIN hremployee_master ON hrdocuments_tran.employeeunkid = hremployee_master.employeeunkid AND modulerefid NOT IN(" & enImg_Email_RefId.Applicant_Module & ") " & _
    '             "LEFT JOIN rcapplicant_master ON hrdocuments_tran.employeeunkid = rcapplicant_master.applicantunkid AND modulerefid IN(" & enImg_Email_RefId.Applicant_Module & ") " & _
    '             "LEFT JOIN hremployee_idinfo_tran ON hrdocuments_tran.transactionunkid = hremployee_idinfo_tran.idtypeunkid " & _
    '                  "AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.IDENTITYS & " AND hremployee_master.employeeunkid = hremployee_idinfo_tran.employeeunkid " & _
    '             "LEFT JOIN cfcommon_master AS IDTYPE ON IDTYPE.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
    '             "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrdocuments_tran.transactionunkid " & _
    '                  "AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.MEMBERSHIPS & " AND hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
    '             "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '             "LEFT JOIN hremp_qualification_tran ON hremp_qualification_tran.qualificationunkid = hrdocuments_tran.transactionunkid " & _
    '                  "AND modulerefid IN (" & enImg_Email_RefId.Employee_Module & ") AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.QUALIFICATIONS & " AND hremployee_master.employeeunkid = hremp_qualification_tran.employeeunkid " & _
    '             "LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
    '             "LEFT JOIN rcapplicantqualification_tran ON rcapplicantqualification_tran.qualificationunkid = hrdocuments_tran.transactionunkid " & _
    '                  "AND modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.QUALIFICATIONS & " AND rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
    '             "LEFT JOIN hrqualification_master AS rcQualif ON rcapplicantqualification_tran.qualificationunkid = rcQualif.qualificationunkid " & _
    '             "LEFT JOIN hremp_experience_tran ON hremp_experience_tran.experiencetranunkid = hrdocuments_tran.transactionunkid " & _
    '                  "AND modulerefid IN (" & enImg_Email_RefId.Employee_Module & ") AND scanattachrefid = " & enScanAttactRefId.JOBHISTORYS & " " & _
    '             "LEFT JOIN rcjobhistory ON rcjobhistory.jobhistorytranunkid = hrdocuments_tran.transactionunkid " & _
    '                  "AND modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") AND scanattachrefid = " & enScanAttactRefId.JOBHISTORYS & " AND rcjobhistory.isvoid = 0 " & _
    '             "LEFT JOIN lvleaveform ON hrdocuments_tran.transactionunkid = lvleaveform.formunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " " & _
    '             "LEFT JOIN lvcancelform ON hrdocuments_tran.transactionunkid = lvcancelform.cancelunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " " & _
    '             "LEFT JOIN hrdiscipline_analysis ON hrdiscipline_analysis.disciplineanalysisunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.DISCIPLINES & " " & _
    '             "LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hrdiscipline_analysis.disciplinaryactionunkid " & _
    '             "LEFT JOIN hrtraining_scheduling ON hrtraining_scheduling.trainingschedulingunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.TRAININGS & " " & _
    '             "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.DEPENDANTS & " " & _
    '             "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
    '                "AND #TableDepn.isactive = 1 " & _
    '             "LEFT JOIN cmclaim_request_tran ON cmclaim_request_tran.crtranunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " " & _
    '             "LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_request_tran.crmasterunkid  " & _
    '             "LEFT JOIN cmclaim_retirement_tran ON cmclaim_retirement_tran.claimretirementtranunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.CLAIM_RETIREMENT & " " & _
    '             "LEFT JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmclaim_retirement_tran.claimretirementunkid  "
    '        'Hemant (29 Dec 2020) -- Start
    '        'Enhancement # AH-2017 : PACT TZ-Assist to download CV in bulk by particular Vacancy 
    '        If intVacancyId > 0 Then
    '            StrQ &= " LEFT JOIN rcapp_vacancy_mapping	ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid AND rcapp_vacancy_mapping.isvoid = 0 "
    '        End If
    '        'Hemant (29 Dec 2020) -- End

    '        'Pinkal (11-Sep-2019) -- 'Enhancement NMB - Working On Claim Retirement for NMB.[ "LEFT JOIN cmclaim_retirement_tran ON cmclaim_retirement_tran.claimretirementtranunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.CLAIM_RETIREMENT & " " & _
    '        '"LEFT JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmclaim_retirement_tran.claimretirementunkid  "]

    '        'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]
    '        'Gajanan [22-Feb-2019] -- Start
    '        'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '        '"WHERE 1 = 1 and isactive = 1" 'Sohail (26 Mar 2012) - [ASSET_DECLARATION]
    '        StrQ &= "WHERE 1 = 1 and hrdocuments_tran.isactive = 1"
    '        'Gajanan [22-Feb-2019] -- End
    '        'SHANI (20 JUN 2015) -- [Dependants]

    '        'Sohail (03 Sep 2021) -- Start
    '        'Enhancement :  : Performance issue on scan attachment list screen.
    '        If intScanAttachRefId > 0 Then
    '            StrQ &= " AND hrdocuments_tran.scanattachrefid = @scanattachrefid "
    '            objDataOperation.AddParameter("@scanattachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScanAttachRefId)
    '        End If
    '        'Sohail (03 Sep 2021) -- End

    '        'S.SANDEEP [24-Apr-2018] -- START
    '        'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
    '        '-> Added Information
    '        '-> AND rcapplicantqualification_tran.isvoid = 0
    '        '-> AND rcjobhistory.isvoid = 0
    '        'S.SANDEEP [24-Apr-2018] -- END

    '        'Pinkal (28-Nov-2017) -- Start
    '        'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
    '        '"WHEN scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " THEN @CLAIM_REQUEST " & _
    '        '"WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CLAIM_REQUEST & " THEN ISNULL(cmclaim_request_master.claimrequestno,'') " & _
    '        '"LEFT JOIN cmclaim_request_tran ON cmclaim_request_tran.crtranunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " " & _
    '        '"LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_request_tran.crmasterunkid  " & _
    '        'Pinkal (28-Nov-2017) -- End


    '        'Nilay (03-Dec-2015) -- Start
    '        'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    '        'If StrEditUnkids.Trim.Length > 0 Then
    '        '    StrQ &= " AND hrdocuments_tran.scanattachtranunkid IN(" & StrEditUnkids & ") ORDER BY scanattachrefid "
    '        'Else
    '        '    StrQ &= " ORDER BY scanattachrefid "
    '        'End If
    '        If intEmployeeId > 0 Then
    '            StrQ &= " AND hrdocuments_tran.employeeunkid = " & intEmployeeId & " "
    '            StrQ &= " AND hrdocuments_tran.modulerefid NOT IN(" & enImg_Email_RefId.Applicant_Module & ") " 'Sohail (27 Jan 2021)
    '        ElseIf StrEditUnkids.Trim.Length > 0 Then
    '            StrQ &= " AND hrdocuments_tran.scanattachtranunkid IN(" & StrEditUnkids & ") "
    '        End If

    '        'S.SANDEEP |26-APR-2019| -- START
    '        If blnIncludeDocInApproval = False Then StrQ &= " AND hrdocuments_tran.isinapproval = 0 "
    '        'S.SANDEEP |26-APR-2019| -- END

    '        'S.SANDEEP |18-JAN-2020| -- START
    '        'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    '        If strFilter.Trim.Length > 0 Then
    '            StrQ &= " AND " & strFilter
    '        End If
    '        'S.SANDEEP |18-JAN-2020| -- END

    '        'S.SANDEEP |29-SEP-2020| -- START
    '        If blnOnlyStructure Then
    '            StrQ &= " AND 1 = 2 "
    '        End If
    '        'S.SANDEEP |29-SEP-2020| -- END

    '        'Hemant (29 Dec 2020) -- Start
    '        'Enhancement # AH-2017 : PACT TZ-Assist to download CV in bulk by particular Vacancy 
    '        If intVacancyId > 0 Then
    '            StrQ &= " AND rcapp_vacancy_mapping.vacancyunkid = @vacancyunkid "
    '            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyId)
    '        End If
    '        'Hemant (29 Dec 2020) -- End

    '        StrQ &= " ORDER BY scanattachrefid "
    '        'Nilay (03-Dec-2015) -- End

    '        objDataOperation.AddParameter("@IDENTITYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Identities"))
    '        objDataOperation.AddParameter("@MEMBERSHIPS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Memberships"))
    '        objDataOperation.AddParameter("@QUALIFICATIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Qualifications"))
    '        objDataOperation.AddParameter("@JOBHISTORYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Job History"))
    '        objDataOperation.AddParameter("@LEAVEFORMS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Leave Forms"))
    '        objDataOperation.AddParameter("@DISCIPLINES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Discipline"))
    '        objDataOperation.AddParameter("@CURRICULAM_VITAE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Curriculum Vitae"))
    '        objDataOperation.AddParameter("@OTHERS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Others"))
    '        objDataOperation.AddParameter("@TRAININGS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Trainings"))
    '        objDataOperation.AddParameter("@ASSET_DECLARATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Asset Declaration")) 'Sohail (26 Mar 2012)
    '        objDataOperation.AddParameter("@DEPENDANTS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Dependants")) 'Shani (20 Jun 2015)


    '        'Pinkal (28-Nov-2017) -- Start
    '        'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
    '        objDataOperation.AddParameter("@CLAIM_REQUEST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Claim Request"))
    '        'Pinkal (28-Nov-2017) -- End

    '        'S.SANDEEP [29-NOV-2017] -- START
    '        'ISSUE/ENHANCEMENT : REF-ID # 136
    '        objDataOperation.AddParameter("@ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Assessment"))
    '        'S.SANDEEP [29-NOV-2017] -- END

    '        'Sohail (04 Jul 2019) -- Start
    '        'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
    '        objDataOperation.AddParameter("@COVER_LETTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Cover Letter"))
    '        'Sohail (04 Jul 2019) -- End

    '        'Gajanan [02-SEP-2019] -- Start      
    '        'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    '        objDataOperation.AddParameter("@STAFF_REQUISITION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Staff Requisition"))
    '        'Gajanan [02-SEP-2019] -- End

    '        'Pinkal (11-Sep-2019) -- Start
    '        'Enhancement NMB - Working On Claim Retirement for NMB.
    '        objDataOperation.AddParameter("@CLAIM_RETIREMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Claim Retirement"))
    '        'Pinkal (11-Sep-2019) -- End

    '        'Sohail (14 Nov 2019) -- Start
    '        'NMB UAT Enhancement # : New Screen Training Need Form.
    '        objDataOperation.AddParameter("@TRAINING_NEED_FORM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Training Need Form"))
    '        'Sohail (14 Nov 2019) -- End

    '        If strList.Trim.Length <= 0 Then strList = "List"

    '        'Sohail (18 May 2019) -- Start
    '        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    '        StrQ &= " DROP TABLE #TableDepn "
    '        'Sohail (18 May 2019) -- End

    '        ds = objDataOperation.ExecQuery(StrQ, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Nilay (03-Dec-2015) -- Start
    '        'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    '        'If StrEditUnkids.Trim.Length > 0 Then
    '        If StrEditUnkids.Trim.Length > 0 OrElse intEmployeeId > 0 Then
    '            'Nilay (03-Dec-2015) -- End
    '            For Each dRow As DataRow In ds.Tables(0).Rows
    '                Dim dtRow As DataRow = mdtTran.NewRow

    '                dtRow.Item("scanattachtranunkid") = dRow.Item("scanattachtranunkid")
    '                dtRow.Item("documentunkid") = dRow.Item("documentunkid")
    '                dtRow.Item("employeeunkid") = dRow.Item("UNKID")
    '                dtRow.Item("filename") = dRow.Item("ATTACHED_FILE")
    '                dtRow.Item("scanattachrefid") = dRow.Item("scanattachrefid")
    '                dtRow.Item("modulerefid") = dRow.Item("modulerefid")
    '                dtRow.Item("userunkid") = dRow.Item("userunkid")
    '                dtRow.Item("transactionunkid") = dRow.Item("transactionunkid")
    '                dtRow.Item("attached_date") = dRow.Item("attached_date")

    '                dtRow.Item("AUD") = dRow.Item("AUD")
    '                'Sohail (23 Apr 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'dtRow.Item("orgfilepath") = ConfigParameter._Object._Document_Path.ToString & "\" & dRow.Item("DOC_TYPE").ToString & "\" & dRow.Item("ATTACHED_FILE").ToString

    '                'Shani(24-Aug-2015) -- Start
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'If strDocumentPath = "" Then strDocumentPath = ConfigParameter._Object._Document_Path.ToString
    '                'dtRow.Item("orgfilepath") = strDocumentPath & "\" & dRow.Item("DOC_TYPE").ToString & "\" & dRow.Item("ATTACHED_FILE").ToString
    '                dtRow.Item("orgfilepath") = strDocument_Path & "\" & dRow.Item("DOC_TYPE").ToString & "\" & dRow.Item("ATTACHED_FILE").ToString
    '                'Shani(24-Aug-2015) -- End


    '                'Sohail (23 Apr 2012) -- End
    '                dtRow.Item("valuename") = dRow.Item("AVALUE")
    '                dtRow.Item("doctype") = dRow.Item("DOC_TYPE")
    '                dtRow.Item("code") = dRow.Item("CODE")
    '                dtRow.Item("names") = dRow.Item("ENAME")
    '                dtRow.Item("document") = dRow.Item("DOC_NAME")

    '                'SHANI (16 JUL 2015) -- Start
    '                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    '                'Upload Image folder to access those attachemts from Server and 
    '                'all client machines if ArutiSelfService is installed otherwise save then on Document path
    '                dtRow.Item("fileuniquename") = dRow.Item("fileuniquename")
    '                dtRow.Item("filepath") = dRow.Item("filepath")
    '                'SHANI (16 JUL 2015) -- End 

    '                'S.SANDEEP [26 JUL 2016] -- START
    '                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    '                dtRow.Item("form_name") = dRow.Item("form_name")
    '                'S.SANDEEP [26 JUL 2016] -- END


    '                'Pinkal (20-Nov-2018) -- Start
    '                'Enhancement - Working on P2P Integration for NMB.
    '                dtRow.Item("fileextension") = dRow.Item("fileextension")
    '                dtRow.Item("Documentype") = dRow.Item("Documentype")
    '                dtRow.Item("DocBase64Value") = dRow.Item("DocBase64Value")
    '                'Pinkal (20-Nov-2018) -- End

    '                'S.SANDEEP |25-JAN-2019| -- START
    '                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
    '                dtRow.Item("file_data") = dRow.Item("file_data")
    '                'S.SANDEEP |25-JAN-2019| -- END

    '                'S.SANDEEP |26-APR-2019| -- START
    '                dtRow.Item("isinapproval") = dRow.Item("isinapproval")
    '                'S.SANDEEP |26-APR-2019| -- END

    '                mdtTran.Rows.Add(dtRow)
    '            Next
    '        End If

    '        Return ds

    '    Catch ex As Exception
    '        Throw ex
    '        Return Nothing
    '    Finally
    '        ds.Dispose()
    '    End Try
    'End Function
    Public Function GetList(ByVal strDocument_Path As String, _
                            ByVal strList As String, _
                            ByVal StrEditUnkids As String, _
                            Optional ByVal intEmployeeId As Integer = -1, _
                            Optional ByVal blnIncludeDocInApproval As Boolean = False, _
                            Optional ByVal xDataOperation As clsDataOperation = Nothing, _
                            Optional ByVal dtAsonDate As Date = Nothing, _
                            Optional ByVal strFilter As String = "", _
                            Optional ByVal blnOnlyStructure As Boolean = False, _
                            Optional ByVal intVacancyId As Integer = -1, _
                            Optional ByVal intScanAttachRefId As Integer = 0, _
                            Optional ByVal intApplicantId As Integer = 0, _
                            Optional ByVal IncludeFileData As Boolean = False _
                            ) As DataSet

        Dim StrQ As String = String.Empty
        Dim ds As New DataSet
        Dim exForce As Exception
        Try
            _xDataOp = xDataOperation
            Dim dsDocType As DataSet = GetDocType(0, False, "List")
            Dim dicDocType As Dictionary(Of Integer, String) = (From p In dsDocType.Tables("List") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            If xDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOperation
            End If
            objDataOperation.ClearParameters()

            If intApplicantId <= 0 Then
            StrQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid " & _
                                ", hrdependant_beneficiaries_status_tran.isactive " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If intEmployeeId > 0 Then
                StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If blnOnlyStructure Then
                StrQ &= " AND 1 = 2 "
            End If

            If intEmployeeId > 0 Then
                StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = " & intEmployeeId & " "
            End If

            If dtAsonDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsonDate))
            End If

            StrQ &= ") AS A " & _
                        " WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            End If

            StrQ &= "SELECT DISTINCT "
            StrQ &= " CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(hremployee_master.employeecode,'') " & _
                                "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(applicant_code,'') END AS CODE " & _
                         ",CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL (hremployee_master.surname,'') " & _
                                "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.othername,'')+' '+ISNULL(rcapplicant_master.surname,'')  END AS ENAME " & _
                         ",ISNULL(cfcommon_master.name,'') AS DOC_NAME " & _
                         ",[filename] AS ATTACHED_FILE " & _
                         ",CASE WHEN hrdocuments_tran.scanattachrefid   = " & enScanAttactRefId.IDENTITYS & " THEN ISNULL(IDTYPE.NAME,'') " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.MEMBERSHIPS & " THEN ISNULL(hrmembership_master.membershipname,'') " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.QUALIFICATIONS & " THEN CASE WHEN hrdocuments_tran.modulerefid = 1 THEN ISNULL(hrqualification_master.qualificationname,'') " & _
                                                                                     "WHEN  hrdocuments_tran.modulerefid = 2 THEN ISNULL(rcQualif.qualificationname,'') END " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.JOBHISTORYS & " THEN CASE WHEN hrdocuments_tran.modulerefid = 1 THEN ISNULL(hremp_experience_tran.company,'') " & _
                                                                                     "WHEN  hrdocuments_tran.modulerefid = 2 THEN ISNULL(rcjobhistory.companyname,'') END " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.LEAVEFORMS & " THEN CASE WHEN ISNULL(lvcancelform.cancelformno,'') <> '' THEN ISNULL(lvcancelform.cancelformno,'') ELSE  ISNULL(lvleaveform.formno, '') END " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.DISCIPLINES & " THEN CASE WHEN ISNULL(hrdiscipline_analysis.incident_caused,'') = '' THEN " & _
                                                                                                            "ISNULL (hraction_reason_master.reason_action,'') " & _
                                                                                                  "ELSE  hrdiscipline_analysis.incident_caused END " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CURRICULAM_VITAE & " THEN @CURRICULAM_VITAE " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.OTHERS & " THEN @OTHERS " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.TRAININGS & " THEN ISNULL(hrtraining_scheduling.course_title,'') " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.ASSET_DECLARATION & " THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL (hremployee_master.surname,'') " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.DEPENDANTS & " THEN ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL (hrdependants_beneficiaries_tran.last_name,'') " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CLAIM_REQUEST & " THEN ISNULL(cmclaim_request_master.claimrequestno,'') " & _
                        "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.COVER_LETTER & " THEN @COVER_LETTER " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.STAFF_REQUISITION & " THEN @STAFF_REQUISITION " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.CLAIM_RETIREMENT & " THEN @CLAIM_RETIREMENT " & _
                                "WHEN hrdocuments_tran.scanattachrefid  = " & enScanAttactRefId.TRAINING_NEED_FORM & " THEN @TRAINING_NEED_FORM " & _
                         " END AS AVALUE " & _
                         ",CASE WHEN modulerefid = " & enImg_Email_RefId.Staff_Requisition & " THEN 0 WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN hremployee_master.employeeunkid " & _
                                "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN rcapplicant_master.applicantunkid END AS UNKID " & _
                         ",CASE WHEN modulerefid NOT IN (" & enImg_Email_RefId.Applicant_Module & ") THEN 0 " & _
                                "WHEN modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") THEN 1 END AS IsApplicant " & _
                         ",scanattachrefid AS DOCTYPEID " & _
                         ",ISNULL(hrdocuments_tran.modulerefid,-1) AS modulerefid " & _
                         ",ISNULL(hrdocuments_tran.scanattachrefid,-1) AS scanattachrefid " & _
                         ",ISNULL(hrdocuments_tran.scanattachtranunkid,-1) AS scanattachtranunkid " & _
                         ",ISNULL(hrdocuments_tran.transactionunkid,-1) AS transactionunkid " & _
                         ",ISNULL(hrdocuments_tran.userunkid,1) AS userunkid " & _
                         ",ISNULL(hrdocuments_tran.attached_date,'19000101') AS attached_date " & _
                         ",ISNULL(hrdocuments_tran.documentunkid,0) AS documentunkid " & _
                         ",ISNULL(hrdocuments_tran.fileuniquename,'') AS fileuniquename " & _
                         ",ISNULL(hrdocuments_tran.filepath,'') AS filepath " & _
                         ",ISNULL(hrdocuments_tran.filesize,-1) AS filesize "

            StrQ &= ",ISNULL(hrdocuments_tran.form_name,'') AS form_name  "

            StrQ &= ", '' AS fileextension " & _
                            ", '' AS Documentype " & _
                            ", '' AS DocBase64Value "

            If IncludeFileData = True Then StrQ &= ",hrdocuments_tran.file_data "

            StrQ &= ",'' AS AUD " & _
                    ",ISNULL(cfcommon_master.ismandatoryfornewemp,0) AS ismandatoryfornewemp "

            StrQ &= ",isinapproval "

            StrQ &= ", CASE hrdocuments_tran.scanattachrefid "

            For Each pair In dicDocType
                StrQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            StrQ &= " END AS DOC_TYPE "

            If intVacancyId > 0 Then
                StrQ &= " ,ISNULL(rcapp_vacancy_mapping.vacancyunkid,0) AS vacancyunkid "
            End If

            StrQ &= "FROM hrdocuments_tran " & _
                 "LEFT JOIN cfcommon_master ON dbo.cfcommon_master.masterunkid = hrdocuments_tran.documentunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " " & _
                 "LEFT JOIN hremployee_master ON hrdocuments_tran.employeeunkid = hremployee_master.employeeunkid AND modulerefid NOT IN(" & enImg_Email_RefId.Applicant_Module & ") " & _
                 "LEFT JOIN rcapplicant_master ON hrdocuments_tran.employeeunkid = rcapplicant_master.applicantunkid AND modulerefid IN(" & enImg_Email_RefId.Applicant_Module & ") " & _
                 "LEFT JOIN hremployee_idinfo_tran ON hrdocuments_tran.transactionunkid = hremployee_idinfo_tran.idtypeunkid " & _
                      "AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.IDENTITYS & " AND hremployee_master.employeeunkid = hremployee_idinfo_tran.employeeunkid " & _
                 "LEFT JOIN cfcommon_master AS IDTYPE ON IDTYPE.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
                 "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrdocuments_tran.transactionunkid " & _
                      "AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.MEMBERSHIPS & " AND hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                 "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                 "LEFT JOIN hremp_qualification_tran ON hremp_qualification_tran.qualificationunkid = hrdocuments_tran.transactionunkid " & _
                      "AND modulerefid IN (" & enImg_Email_RefId.Employee_Module & ") AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.QUALIFICATIONS & " AND hremployee_master.employeeunkid = hremp_qualification_tran.employeeunkid " & _
                 "LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                 "LEFT JOIN rcapplicantqualification_tran ON rcapplicantqualification_tran.qualificationunkid = hrdocuments_tran.transactionunkid " & _
                      "AND modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.QUALIFICATIONS & " AND rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
                 "LEFT JOIN hrqualification_master AS rcQualif ON rcapplicantqualification_tran.qualificationunkid = rcQualif.qualificationunkid " & _
                 "LEFT JOIN hremp_experience_tran ON hremp_experience_tran.experiencetranunkid = hrdocuments_tran.transactionunkid " & _
                      "AND modulerefid IN (" & enImg_Email_RefId.Employee_Module & ") AND scanattachrefid = " & enScanAttactRefId.JOBHISTORYS & " " & _
                 "LEFT JOIN rcjobhistory ON rcjobhistory.jobhistorytranunkid = hrdocuments_tran.transactionunkid " & _
                      "AND modulerefid IN (" & enImg_Email_RefId.Applicant_Module & ") AND scanattachrefid = " & enScanAttactRefId.JOBHISTORYS & " AND rcjobhistory.isvoid = 0 " & _
                 "LEFT JOIN lvleaveform ON hrdocuments_tran.transactionunkid = lvleaveform.formunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " " & _
                 "LEFT JOIN lvcancelform ON hrdocuments_tran.transactionunkid = lvcancelform.cancelunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " " & _
                 "LEFT JOIN hrdiscipline_analysis ON hrdiscipline_analysis.disciplineanalysisunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.DISCIPLINES & " " & _
                 "LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hrdiscipline_analysis.disciplinaryactionunkid " & _
                 "LEFT JOIN hrtraining_scheduling ON hrtraining_scheduling.trainingschedulingunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.TRAININGS & " " & _
                    "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.DEPENDANTS & " "

            If intApplicantId <= 0 Then
                StrQ &= "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid AND #TableDepn.isactive = 1 "
            End If

            StrQ &= "LEFT JOIN cmclaim_request_tran ON cmclaim_request_tran.crtranunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " " & _
                 "LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_request_tran.crmasterunkid  " & _
                 "LEFT JOIN cmclaim_retirement_tran ON cmclaim_retirement_tran.claimretirementtranunkid = hrdocuments_tran.transactionunkid AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.CLAIM_RETIREMENT & " " & _
                 "LEFT JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmclaim_retirement_tran.claimretirementunkid  "

            If intVacancyId > 0 Then
                StrQ &= " LEFT JOIN rcapp_vacancy_mapping	ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid AND rcapp_vacancy_mapping.isvoid = 0 "
            End If

            StrQ &= "WHERE 1 = 1 and hrdocuments_tran.isactive = 1"

            If intScanAttachRefId > 0 Then
                StrQ &= " AND hrdocuments_tran.scanattachrefid = @scanattachrefid "
                objDataOperation.AddParameter("@scanattachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScanAttachRefId)
            End If

            If intApplicantId > 0 Then
                StrQ &= " AND rcapplicant_master.applicantunkid  = " & intApplicantId & " "
            End If

            If intEmployeeId > 0 Then
                StrQ &= " AND hrdocuments_tran.employeeunkid = " & intEmployeeId & " "
                StrQ &= " AND hrdocuments_tran.modulerefid NOT IN(" & enImg_Email_RefId.Applicant_Module & ") " 'Sohail (27 Jan 2021)
            ElseIf StrEditUnkids.Trim.Length > 0 Then
                StrQ &= " AND hrdocuments_tran.scanattachtranunkid IN(" & StrEditUnkids & ") "
            End If

            If blnIncludeDocInApproval = False Then StrQ &= " AND hrdocuments_tran.isinapproval = 0 "

            If strFilter.Trim.Length > 0 Then
                StrQ &= " AND " & strFilter
            End If

            If blnOnlyStructure Then
                StrQ &= " AND 1 = 2 "
            End If

            If intVacancyId > 0 Then
                StrQ &= " AND rcapp_vacancy_mapping.vacancyunkid = @vacancyunkid "
                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyId)
            End If

            StrQ &= " ORDER BY scanattachrefid "

            objDataOperation.AddParameter("@IDENTITYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Identities"))
            objDataOperation.AddParameter("@MEMBERSHIPS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Memberships"))
            objDataOperation.AddParameter("@QUALIFICATIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Qualifications"))
            objDataOperation.AddParameter("@JOBHISTORYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Job History"))
            objDataOperation.AddParameter("@LEAVEFORMS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Leave Forms"))
            objDataOperation.AddParameter("@DISCIPLINES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Discipline"))
            objDataOperation.AddParameter("@CURRICULAM_VITAE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Curriculum Vitae"))
            objDataOperation.AddParameter("@OTHERS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Others"))
            objDataOperation.AddParameter("@TRAININGS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Trainings"))
            objDataOperation.AddParameter("@ASSET_DECLARATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Asset Declaration")) 'Sohail (26 Mar 2012)
            objDataOperation.AddParameter("@DEPENDANTS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Dependants")) 'Shani (20 Jun 2015)
            objDataOperation.AddParameter("@CLAIM_REQUEST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Claim Request"))
            objDataOperation.AddParameter("@ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Assessment"))
            objDataOperation.AddParameter("@COVER_LETTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Cover Letter"))
            objDataOperation.AddParameter("@STAFF_REQUISITION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Staff Requisition"))
            objDataOperation.AddParameter("@CLAIM_RETIREMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Claim Retirement"))
            objDataOperation.AddParameter("@TRAINING_NEED_FORM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Training Need Form"))

            If strList.Trim.Length <= 0 Then strList = "List"

            If intApplicantId <= 0 Then StrQ &= " DROP TABLE #TableDepn "

            ds = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If StrEditUnkids.Trim.Length > 0 OrElse intEmployeeId > 0 Then
                For Each dRow As DataRow In ds.Tables(0).Rows
                    Dim dtRow As DataRow = mdtTran.NewRow
                    dtRow.Item("scanattachtranunkid") = dRow.Item("scanattachtranunkid")
                    dtRow.Item("documentunkid") = dRow.Item("documentunkid")
                    dtRow.Item("employeeunkid") = dRow.Item("UNKID")
                    dtRow.Item("filename") = dRow.Item("ATTACHED_FILE")
                    dtRow.Item("scanattachrefid") = dRow.Item("scanattachrefid")
                    dtRow.Item("modulerefid") = dRow.Item("modulerefid")
                    dtRow.Item("userunkid") = dRow.Item("userunkid")
                    dtRow.Item("transactionunkid") = dRow.Item("transactionunkid")
                    dtRow.Item("attached_date") = dRow.Item("attached_date")

                    dtRow.Item("AUD") = dRow.Item("AUD")
                    dtRow.Item("orgfilepath") = strDocument_Path & "\" & dRow.Item("DOC_TYPE").ToString & "\" & dRow.Item("ATTACHED_FILE").ToString
                    dtRow.Item("valuename") = dRow.Item("AVALUE")
                    dtRow.Item("doctype") = dRow.Item("DOC_TYPE")
                    dtRow.Item("code") = dRow.Item("CODE")
                    dtRow.Item("names") = dRow.Item("ENAME")
                    dtRow.Item("document") = dRow.Item("DOC_NAME")

                    dtRow.Item("fileuniquename") = dRow.Item("fileuniquename")
                    dtRow.Item("filepath") = dRow.Item("filepath")
                    dtRow.Item("form_name") = dRow.Item("form_name")
                    dtRow.Item("fileextension") = dRow.Item("fileextension")
                    dtRow.Item("Documentype") = dRow.Item("Documentype")
                    dtRow.Item("DocBase64Value") = dRow.Item("DocBase64Value")
                    If IncludeFileData = True Then dtRow.Item("file_data") = dRow.Item("file_data")
                    dtRow.Item("isinapproval") = dRow.Item("isinapproval")

                    mdtTran.Rows.Add(dtRow)
                Next
            End If

            Return ds

        Catch ex As Exception
            Throw ex
            Return Nothing
        Finally
            ds.Dispose()
        End Try
    End Function
    'S.SANDEEP |13-OCT-2021| -- END

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function GetAssociatedValue(ByVal intDocumentTypeId As Integer, _
    '                                   ByVal intEmployeeId As Integer, _
    '                                   ByVal intModuleRefId As Integer, _
    '                                   Optional ByVal blnAddSelect As Boolean = False,, Optional ByVal strIncludeInactiveEmployee As String = "" _
    '                                  ,Optional ByVal strEmployeeAsOnDate As String = "" _
    '                                  ,Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet 'Sohail (23 Apr 2012) - [strIncludeInactiveEmployee,strEmployeeAsOnDate,strUserAccessLevelFilterString]

    Public Function GetAssociatedValue(ByVal xDatabaseName As String, _
                                       ByVal xUserUnkid As Integer, _
                                       ByVal xYearUnkid As Integer, _
                                       ByVal xCompanyUnkid As Integer, _
                                       ByVal xPeriodStart As DateTime, _
                                       ByVal xPeriodEnd As DateTime, _
                                       ByVal xUserModeSetting As String, _
                                       ByVal xOnlyApproved As Boolean, _
                                       ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                       ByVal intDocumentTypeId As Integer, _
                                       ByVal intEmployeeId As Integer, _
                                       ByVal intModuleRefId As Integer, _
                                       ByVal xFinancialYearName As String, _
                                       ByVal xFinancialDataBaseStartDate As DateTime, _
                                       ByVal xFinancialDataBaseEndDate As DateTime, _
                                       ByVal xTransactionScreenName As String, _
                                       Optional ByVal blnAddSelect As Boolean = False, _
                                       Optional ByVal intTransactionId As Integer = 0) As DataSet
        'S.SANDEEP [26 JUL 2016] -- START {xTransactionScreenName,intTransactionId} -- END

        'S.SANDEEP [04 JUN 2015] -- END


        Dim StrQ As String = String.Empty

        'Shani (15-Nov-2016) -- Start
        'Enhancement - 
        Dim dsAValue As DataSet = Nothing
        'Shani (15-Nov-2016) -- End


        Try
            Select Case intModuleRefId
                Case enImg_Email_RefId.Employee_Module, _
                     enImg_Email_RefId.Dependants_Beneficiaries, _
                     enImg_Email_RefId.Leave_Module, _
                     enImg_Email_RefId.Payroll_Module, _
                     enImg_Email_RefId.Training_Module, enImg_Email_RefId.Discipline_Module, enImg_Email_RefId.Claim_Request, enImg_Email_RefId.Appraisal_Module, enImg_Email_RefId.Staff_Requisition 'S.SANDEEP [29-NOV-2017] -- START {REF-ID # 136 [enImg_Email_RefId.Appraisal_Module]} -- END
                    'Gajanan [02-SEP-2019] -- [Staff_Requisition]
                    'Shani (20-Aug-2016) -- [Claim_Request]


                    Select Case intDocumentTypeId
                        Case enScanAttactRefId.IDENTITYS        '1
                            dsAValue = clsEmployee_Master.GetEmployee_Identity(intEmployeeId, True)
                        Case enScanAttactRefId.MEMBERSHIPS      '2
                            dsAValue = clsEmployee_Master.GetEmployee_Membership(intEmployeeId, True)
                        Case enScanAttactRefId.QUALIFICATIONS   '3
                            dsAValue = clsEmp_Qualification_Tran.GetEmployeeQualification(intEmployeeId, True)
                        Case enScanAttactRefId.JOBHISTORYS      '4 NEW REQ. SO LEFT FOR NOW

                        Case enScanAttactRefId.LEAVEFORMS       '5
                            dsAValue = clsleaveform.GetLeaveForms(intEmployeeId, True)
                        Case enScanAttactRefId.DISCIPLINES      '6 NEW REQ. SO LEFT FOR NOW
                            'S.SANDEEP [24 MAY 2016] -- START
                            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                            ''Dim objDFile As New clsDiscipline_File
                            ' ''Sohail (23 Apr 2012) -- Start
                            ' ''TRA - ENHANCEMENT
                            ' ''dsAValue = objDFile.GetList("List", True, intEmployeeId)
                            ''dsAValue = objDFile.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", True, intEmployeeId)
                            ' ''Sohail (23 Apr 2012) -- End
                            Select Case xTransactionScreenName.ToString.ToUpper
                                Case "FRMCHARGECOUNTRESPONSE"
                                    dsAValue = (New clsDiscipline_file_tran).GetComboListForCharge(intTransactionId, False)
                                Case "FRMDISCIPLINEFILING", "FRMDISCIPLINEFILINGLIST"
                                    If intTransactionId > 0 Then
                                        dsAValue = (New clsDiscipline_file_tran).GetComboListForCharge(intTransactionId, False)
                                    Else
                                        dsAValue = (New clsDiscipline_file_master).GetComboList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List")
                                    End If
                                    'Case "FRMCHARGECOUNTRESPONSE", "FRMDISCIPLINEFILING", "FRMDISCIPLINEFILINGLIST"
                                    '    dsAValue = (New clsDiscipline_file_tran).GetComboListForCharge(intTransactionId, False)
                                Case "FRMDISCIPLINEPROCEEDINGLIST", "FRMPROCEEDINGSAPPROVALADDEDIT", "FRMCHARGEPROCEEDINGDETAILS", "FRMAPPROVEDISAPPROVECHARGES"
                                    dsAValue = (New clsdiscipline_proceeding_master).GetComboListForCharge(intTransactionId, False)
                                    'S.SANDEEP |11-NOV-2019| -- START
                                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                                Case "FRMDISCIPLINEHEARINGADDEDIT"
                                    If intTransactionId > 0 Then
                                        dsAValue = (New clshearing_schedule_master).GetComboListForCharge(intTransactionId, False)
                                    Else
                                        dsAValue = (New clsDiscipline_file_master).GetComboList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List")
                                    End If
                                    'S.SANDEEP |11-NOV-2019| -- END
                                Case Else
                                    dsAValue = (New clsDiscipline_file_master).GetComboList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", True)
                            End Select
                            'S.SANDEEP [24 MAY 2016] -- END
                        Case enScanAttactRefId.TRAININGS        '9
                            dsAValue = clsTraining_Enrollment_Tran.GetEmployee_TrainingList(intEmployeeId, True)

                            'Sohail (07 Mar 2012) -- Start
                            'TRA - ENHANCEMENT
                        Case enScanAttactRefId.ASSET_DECLARATION '10
                            'Sohail (26 Mar 2012) -- Start
                            'TRA - ENHANCEMENT
                            'dsAValue = clsAssetdeclaration_master.GetList("AssetDeclaration", , intEmployeeId)
                            'Sohail (23 Apr 2012) -- Start
                            'TRA - ENHANCEMENT
                            'dsAValue = clsAssetdeclaration_master.GetList("AssetDeclaration", , intEmployeeId, True)
                            'Sohail (16 Apr 2015) -- Start
                            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
                            'dsAValue = clsAssetdeclaration_master.GetList("AssetDeclaration", , intEmployeeId, True, strIncludeInactiveEmployee, strEmployeeAsOnDate, strUserAccessLevelFilterString)

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'Dim dicDB As New Dictionary(Of String, String)
                            'dicDB.Add(FinancialYear._Object._DatabaseName, FinancialYear._Object._FinancialYear_Name)

                            Dim dicDB As New Dictionary(Of String, String)
                            dicDB.Add(xDatabaseName, xFinancialYearName)
                            'S.SANDEEP [04 JUN 2015] -- END


                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'dsAValue = clsAssetdeclaration_master.GetList("AssetDeclaration", dicDB, , intEmployeeId, True, strIncludeInactiveEmployee, strEmployeeAsOnDate, strUserAccessLevelFilterString)
                            Dim dicDBDate As New Dictionary(Of String, String)
                            Dim dicDBYearId As New Dictionary(Of String, Integer)

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                            'dicDBDate.Add(FinancialYear._Object._DatabaseName, (FinancialYear._Object._Database_Start_Date & "|" & FinancialYear._Object._Database_End_Date).ToString)
                            'dicDBYearId.Add(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid)

                            'Nilay (03-Dec-2015) -- Start
                            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                            'dicDBDate.Add(FinancialYear._Object._DatabaseName, (xFinancialDataBaseStartDate & "|" & xFinancialDataBaseEndDate).ToString)
                            dicDBDate.Add(FinancialYear._Object._DatabaseName, (eZeeDate.convertDate(xFinancialDataBaseStartDate) & "|" & eZeeDate.convertDate(xFinancialDataBaseEndDate)).ToString)
                            'Nilay (03-Dec-2015) -- End


                            dicDBYearId.Add(xDatabaseName, xYearUnkid)

                            'S.SANDEEP [04 JUN 2015] -- END



                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                            'dsAValue = clsAssetdeclaration_master.GetList(User._Object._Userunkid, _
                            '                                 Company._Object._Companyunkid, _
                            '                                 ConfigParameter._Object._UserAccessModeSetting, True, _
                            '                                 ConfigParameter._Object._IsIncludeInactiveEmp, _
                            '                                 "AssetDeclaration", dicDB, dicDBDate, dicDBYearId, , _
                            '                                 intEmployeeId, True, strIncludeInactiveEmployee, _
                            '                                 strEmployeeAsOnDate, strUserAccessLevelFilterString)


                            dsAValue = clsAssetdeclaration_master.GetList(xUserUnkid, _
                                                                          xCompanyUnkid, _
                                                                          xUserModeSetting, xOnlyApproved, _
                                                                          xIncludeIn_ActiveEmployee, _
                                                                          "AssetDeclaration", dicDB, dicDBDate, dicDBYearId, , _
                                                                          intEmployeeId, True, eZeeDate.convertDate(xPeriodEnd))

                            'S.SANDEEP [04 JUN 2015] -- END



                            'Shani(24-Aug-2015) -- End

                            'Sohail (16 Apr 2015) -- End
                            'Sohail (23 Apr 2012) -- End
                            'Sohail (26 Mar 2012) -- End
                            'Sohail (07 Mar 2012) -- End

                            'SHANI (20 JUN 2015) -- Start
                            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                        Case enScanAttactRefId.DEPENDANTS '11
                            Dim objDependant As New clsDependants_Beneficiary_tran
                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'dsAValue = objDependant.GetList("Dependant", , , , intEmployeeId)

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'dsAValue = objDependant.GetList(FinancialYear._Object._DatabaseName, _
                            '                                User._Object._Userunkid, _
                            '                                FinancialYear._Object._YearUnkid, _
                            '                                Company._Object._Companyunkid, _
                            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                            '                                ConfigParameter._Object._UserAccessModeSetting, True, _
                            '                                ConfigParameter._Object._IsIncludeInactiveEmp, "Dependant", , intEmployeeId)



                            'S.SANDEEP [20-JUN-2018] -- Start
                            'Enhancement - Implementing Employee Approver Flow For NMB .

                            'dsAValue = objDependant.GetList(xDatabaseName, _
                            '                                xUserUnkid, _
                            '                                xYearUnkid, _
                            '                                xCompanyUnkid, _
                            '                                xPeriodStart, _
                            '                                xPeriodEnd, _
                            '                                xUserModeSetting, True, _
                            '                                xIncludeIn_ActiveEmployee, "Dependant", False, intEmployeeId)

                            dsAValue = objDependant.GetList(xDatabaseName, _
                                                            xUserUnkid, _
                                                            xYearUnkid, _
                                                            xCompanyUnkid, _
                                                            xPeriodStart, _
                                                            xPeriodEnd, _
                                                         xUserModeSetting, xOnlyApproved, _
                                                         xIncludeIn_ActiveEmployee, "Dependant", False, intEmployeeId, "", True, , xPeriodEnd)
                            'Sohail (18 May 2019) - [xPeriodEnd]
                            'S.SANDEEP [20-JUN-2018] -- End

                            'S.SANDEEP [04 JUN 2015] -- END


                            'S.SANDEEP [04 JUN 2015] -- END

                            objDependant = Nothing
                            'SHANI (20 JUN 2015) -- End 

                            'Shani (20-Aug-2016) -- Start
                            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                        Case enScanAttactRefId.CLAIM_REQUEST '12

                            'Pinkal (28-Nov-2017) -- Start
                            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                            'dsAValue = (New clsclaim_request_master).GetClaimNoForTran(intTransactionId, False, "List")
                            dsAValue = (New clsclaim_request_master).GetClaimNoForTran(intTransactionId, True, "List")
                            'Pinkal (28-Nov-2017) -- End
                            'If intTransactionId <= 0 Then dsAValue.Tables(0).Rows.Clear()
                            'Shani (20-Aug-2016) -- End

                            'S.SANDEEP [29-NOV-2017] -- START
                            'ISSUE/ENHANCEMENT : REF-ID # 136
                        Case enScanAttactRefId.ASSESSMENT
                            Select Case xTransactionScreenName.ToString.ToUpper
                                Case "FRMUPDATEFIELDVALUE"
                                    dsAValue = (New clsassess_empupdate_tran).GetListAttachedAssociatedValue(intTransactionId, Nothing)
                            End Select
                            'S.SANDEEP [29-NOV-2017] -- END

                            'Gajanan [02-SEP-2019] -- Start      
                            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                        Case enScanAttactRefId.STAFF_REQUISITION
                            Dim StrSearch As String = String.Empty

                            StrSearch &= " rcstaffrequisition_tran.form_statusunkid = " & CInt(enApprovalStatus.PENDING) & " "

                            If intTransactionId > 0 Then
                                StrSearch &= "AND rcstaffrequisition_tran.staffrequisitiontranunkid = " & intTransactionId & " "
                            End If

                            dsAValue = (New clsStaffrequisition_Tran).GetList("List", StrSearch)
                            'Gajanan [02-SEP-2019] -- End

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : New Screen Training Need Form.
                        Case enScanAttactRefId.TRAINING_NEED_FORM
                            Dim StrSearch As String = String.Empty

                            'StrSearch &= " hrtraining_need_form_tran.form_statusunkid = " & CInt(enApprovalStatus.PENDING) & " "

                            If intTransactionId > 0 Then
                                StrSearch &= "AND hrtraining_need_form_tran.trainingneedformtranunkid = " & intTransactionId & " "
                            End If

                            dsAValue = (New clsTraining_need_form_tran).GetList("List", , StrSearch)
                            'Sohail (14 Nov 2019) -- End

                    End Select
                Case enImg_Email_RefId.Applicant_Module
                    Select Case intDocumentTypeId
                        Case enScanAttactRefId.QUALIFICATIONS   '3
                            dsAValue = clsApplicant_master.GetApplicantQualification(intEmployeeId, True)
                        Case enScanAttactRefId.JOBHISTORYS      '4
                            dsAValue = clsApplicant_master.GetApplicantJobHistory(intEmployeeId, True)
                    End Select
            End Select

            Return dsAValue

        Catch ex As Exception
            Throw ex
        Finally
            'Shani (15-Nov-2016) -- Start
            'Enhancement - 
            If dsAValue IsNot Nothing Then dsAValue.Dispose()
            'Shani (15-Nov-2016) -- End
        End Try
    End Function

    Private Function DoFileOperation(ByVal strSourcePath As String, ByVal strDestinationPath As String, ByVal eOperation As enfile_Operation) As Boolean
        Dim blnFlag As Boolean = False
        Dim strAccountInfo As String = ""
        Dim strDestinationDir As String = ""
        Try
            If strSourcePath.Trim.Length <= 0 Then Return blnFlag


            If strDestinationPath.Trim.Length > 0 Then
                strDestinationDir = strDestinationPath.Substring(0, strDestinationPath.LastIndexOf("\"))
                If System.IO.Directory.Exists(strDestinationDir) = False Then
                    System.IO.Directory.CreateDirectory(strDestinationDir)
                    strAccountInfo = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString()
                    AddDirectorySecurity(strDestinationDir, strAccountInfo, FileSystemRights.DeleteSubdirectoriesAndFiles, AccessControlType.Deny)
                End If

                Select Case eOperation
                    Case enfile_Operation.COPY
                        If System.IO.File.Exists(strDestinationPath) = False Then
                            System.IO.File.Copy(strSourcePath, strDestinationPath, True)
                            blnFlag = True
                        Else
                            'Anjan [ 05 Feb 2013 ] -- Start
                            'Issue: If same file found on current location then prompt message.
                            'Throw New Exception(Language.getMessage(mstrModuleName, 111, "The Selected file with same name already exists.Please rename the selected file from source if you would like same selected file to be attached."))
                            mstrMessage = Language.getMessage(mstrModuleName, 12, "The Selected file with same name already exists. Please rename the selected file from source if you would like to attach the selected file.")

                            'Anjan [ 05 Feb 2013 ] -- End

                        End If
                    Case enfile_Operation.MOVE
                        If strSourcePath <> strDestinationPath Then
                            If System.IO.File.Exists(strDestinationPath) = False Then
                                System.IO.File.Move(strSourcePath, strDestinationPath)
                                blnFlag = True
                            End If
                        Else
                            blnFlag = True
                        End If
                    Case enfile_Operation.DELETE
                        If System.IO.File.Exists(strDestinationPath) = False Then
                            System.IO.File.Delete(strSourcePath)
                            blnFlag = True
                        End If
                End Select
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DoFileOperation; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Shared Sub AddDirectorySecurity(ByVal FileName As String, _
                                           ByVal Account As String, _
                                           ByVal Rights As FileSystemRights, _
                                           ByVal ControlType As AccessControlType)
        Try
            Dim dInfo As New IO.DirectoryInfo(FileName)
            Dim dSecurity As DirectorySecurity = dInfo.GetAccessControl()
            dSecurity.AddAccessRule(New FileSystemAccessRule(Account, Rights, ControlType))
            dInfo.SetAccessControl(dSecurity)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddDirectorySecurity", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Shared Function IsValidImage(ByVal strFile As String) As Boolean
        Try
            Dim img As System.Drawing.Image = System.Drawing.Image.FromFile(strFile)
        Catch ex As OutOfMemoryException
            Return False
        End Try
        Return True
    End Function

    Public Function GetAttachmentTranunkIds(ByVal EmployeeId As Integer, ByVal intScanAttachRefId As Integer, ByVal intModuleRefId As Integer, Optional ByVal intTransactionId As Integer = -1, Optional ByVal mstrFile_name As String = "") As DataTable 'Shani [16 JUL 2016]--mstrFile_name
        Dim mdtTable As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            Dim strQ As String = " SELECT ISNULL(scanattachtranunkid,0) AS scanattachtranunkid FROM hrdocuments_tran " & _
                                           " WHERE employeeunkid = @employeeunkid AND scanattachrefid = @scanattachrefid AND modulerefid = @modulerefid "

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            If mstrFile_name.Trim <> "" Then
                strQ &= " AND filename = @filename"
                objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFile_name)
            End If
            'SHANI (16 JUL 2015) -- End 

            If intTransactionId > 0 Then
                strQ &= " AND transactionunkid = @transactionunkid "
                objDataOperation.AddParameter("@transactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionId)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, EmployeeId)
            objDataOperation.AddParameter("@scanattachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScanAttachRefId)
            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefId)

            Dim dsScanList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsScanList IsNot Nothing AndAlso dsScanList.Tables(0).Rows.Count > 0 Then
                mdtTable = dsScanList.Tables(0)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAttachmentTranunkIds", mstrModuleName)
        Finally
        End Try
        Return mdtTable
    End Function


    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function GetQulificationAttachment(ByVal intEmpId As Integer, ByVal intModuleRefUnkid As Integer, ByVal intTrancationUnkid As Integer) As DataTable
    ''' <summary>
    ''' Get All Document 
    ''' </summary>
    ''' <param name="intEmpId"></param>
    ''' <param name="intModuleRefUnkid"></param>
    ''' <param name="intTrancationUnkid"></param>
    ''' <param name="strDocument_Path"></param>
    ''' <param name="intFillDocumentType">0 = All, 1 = Active, 2 = Inactive</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetQulificationAttachment(ByVal intEmpId As Integer, _
                                              ByVal intModuleRefUnkid As Integer, _
                                              ByVal intTrancationUnkid As Integer, _
                                              ByVal strDocument_Path As String, _
                                              Optional ByVal intFillDocumentType As Integer = 1, _
                                              Optional ByVal strScanattachids As String = "", _
                                              Optional ByVal blnIncludeDocInApproval As Boolean = False) As DataTable 'S.SANDEEP |26-APR-2019| -- START {blnIncludeDocInApproval} -- END
        'Shani(24-Aug-2015) -- End

        Dim mdtTable As DataTable = Nothing
        Dim strQ As String = ""

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'Dim strDocumentPath As String = ""
        'Dim objfileinfo As System.IO.FileInfo
        'Shani(24-Aug-2015) -- End


        Dim exForce As Exception
        Try
            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
            'objDataOperation = New clsDataOperation
            If xDataOp IsNot Nothing Then
                _xDataOp = xDataOp
            End If
            Dim dsDocType As DataSet = GetDocType(0, False, "List")
            Dim dicDocType As Dictionary(Of Integer, String) = (From p In dsDocType.Tables("List") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            'Sohail (04 Jul 2019) -- End
            objDataOperation.ClearParameters()
            strQ = "SELECT " & _
                        " hrdocuments_tran.scanattachtranunkid " & _
                        ",ISNULL(hrdocuments_tran.employeeunkid,-1) AS employeeunkid " & _
                        ",ISNULL(hrdocuments_tran.documentunkid,-1) AS documentunkid " & _
                        "/*,CASE WHEN scanattachrefid  = " & enScanAttactRefId.IDENTITYS & " THEN @IDENTITYS " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.MEMBERSHIPS & " THEN @MEMBERSHIPS " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.QUALIFICATIONS & " THEN @QUALIFICATIONS " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.JOBHISTORYS & " THEN @JOBHISTORYS " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " THEN @LEAVEFORMS " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.DISCIPLINES & " THEN @DISCIPLINES " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.CURRICULAM_VITAE & " THEN @CURRICULAM_VITAE " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.OTHERS & " THEN @OTHERS " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.TRAININGS & " THEN @TRAININGS " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " THEN @ASSET_DECLARATION " & _
                            "WHEN scanattachrefid = " & enScanAttactRefId.DEPENDANTS & " THEN @Dependants " & _
                        " END AS DOC_TYPE*/ " & _
                        ",ISNULL(hrdocuments_tran.modulerefid,-1) AS modulerefid " & _
                        ",ISNULL(hrdocuments_tran.scanattachrefid,-1) AS scanattachrefid " & _
                        ",ISNULL(hrdocuments_tran.transactionunkid,-1) AS transactionunkid " & _
                        ",'' AS file_path " & _
                        ",ISNULL(hrdocuments_tran.filename,'') AS filename " & _
                        ",'' AS filesize " & _
                        ",ISNULL(hrdocuments_tran.attached_date,'19000101') AS attached_date " & _
                        ",'' AS localpath " & _
                        ",'' AS GUID " & _
                        ",'' AS AUD " & _
                        ",ISNULL(hrdocuments_tran.fileuniquename,'') AS fileuniquename " & _
                        ",ISNULL(hrdocuments_tran.filepath,'') AS filepath " & _
                        ",ISNULL(hrdocuments_tran.filesize,-1) AS filesize_kb " & _
                        ",ISNULL(hrdocuments_tran.userunkid,1) AS userunkid " & _
                        ",ISNULL(hrdocuments_tran.form_name,'') AS form_name " & _
                        ",hrdocuments_tran.isactive " 'Gajanan [22-Feb-2019] -- Add ISactive

            'S.SANDEEP |26-APR-2019| -- START
            strQ &= ",'' AS PGUID "
            'S.SANDEEP |26-APR-2019| -- END

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            strQ &= ", '' AS fileextension " & _
                            ", '' AS Documentype " & _
                            ", '' AS DocBase64Value "
            'Pinkal (20-Nov-2018) -- End

            'S.SANDEEP |25-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            strQ &= ",hrdocuments_tran.file_data "
            'S.SANDEEP |25-JAN-2019| -- END


            'Gajanan [27-May-2019] -- Start              
            strQ &= ",isinapproval "
            'Gajanan [27-May-2019] -- End

            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
            strQ &= ", CASE hrdocuments_tran.scanattachrefid "
            For Each pair In dicDocType
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS DOC_TYPE "
            'Sohail (04 Jul 2019) -- End

            strQ &= "FROM hrdocuments_tran " 'S.SANDEEP [26 JUL 2016] -- START {form_name} -- END

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            '"WHERE hrdocuments_tran.employeeunkid = " & intEmpId & " AND hrdocuments_tran.scanattachrefid = " & intModuleRefUnkid & " " & _
            '" AND transactionunkid = " & intTrancationUnkid

            strQ &= "WHERE 1=1 "


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            '[get active and  inactive document]
            Select Case intFillDocumentType
                Case 0  'ALL
                Case 1  'ACTIVE
                    strQ &= " AND hrdocuments_tran.isactive = 1 "
                Case 2  'INACTIVE
                    strQ &= " AND hrdocuments_tran.isactive = 0 "
            End Select
            'Gajanan [22-Feb-2019] -- End

            If intEmpId > 0 Then
                strQ &= " AND hrdocuments_tran.employeeunkid = " & intEmpId & " "
            End If
            If intModuleRefUnkid > 0 Then
                strQ &= " AND hrdocuments_tran.scanattachrefid = " & intModuleRefUnkid & " "
            End If
            If intTrancationUnkid > 0 Then
                strQ &= " AND transactionunkid = " & intTrancationUnkid & " "
            End If
            'Nilay (03-Dec-2015) -- End

            'SHANI (27 JUL 2015) -- [ISNULL Added,userunkid]


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            '[This Change is Only Used For Download(newadded,deleted) Attach Documents for approval bio data]
            If strScanattachids.Length > 0 Then
                strQ &= " AND scanattachtranunkid in (" & strScanattachids & ")"
            End If
            'Gajanan [22-Feb-2019] -- End

            'S.SANDEEP |26-APR-2019| -- START
            If blnIncludeDocInApproval = False Then strQ &= " AND hrdocuments_tran.isinapproval = 0 "
            'S.SANDEEP |26-APR-2019| -- END

            objDataOperation.AddParameter("@IDENTITYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Identities"))
            objDataOperation.AddParameter("@MEMBERSHIPS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Memberships"))
            objDataOperation.AddParameter("@QUALIFICATIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Qualifications"))
            objDataOperation.AddParameter("@JOBHISTORYS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Job History"))
            objDataOperation.AddParameter("@LEAVEFORMS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Leave Forms"))
            objDataOperation.AddParameter("@DISCIPLINES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Discipline"))
            objDataOperation.AddParameter("@CURRICULAM_VITAE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Curriculum Vitae"))
            objDataOperation.AddParameter("@OTHERS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Others"))
            objDataOperation.AddParameter("@TRAININGS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Trainings"))
            objDataOperation.AddParameter("@ASSET_DECLARATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Asset Declaration"))
            objDataOperation.AddParameter("@DEPENDANTS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Dependants"))

            Dim dsScanList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable = dsScanList.Tables(0).Clone
            If dsScanList IsNot Nothing AndAlso dsScanList.Tables(0).Rows.Count > 0 Then
                Dim dtRow As DataRow
                For Each dRow As DataRow In dsScanList.Tables(0).Rows
                    dtRow = mdtTable.NewRow
                    dtRow.Item("scanattachtranunkid") = dRow.Item("scanattachtranunkid")
                    dtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                    dtRow.Item("documentunkid") = dRow.Item("documentunkid")
                    dtRow.Item("DOC_TYPE") = dRow.Item("DOC_TYPE")
                    dtRow.Item("modulerefid") = dRow.Item("modulerefid")
                    dtRow.Item("scanattachrefid") = dRow.Item("scanattachrefid")
                    dtRow.Item("form_name") = dRow.Item("form_name")


                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If strDocumentPath = "" Then strDocumentPath = ConfigParameter._Object._Document_Path.ToString
                    'dtRow.Item("file_path") = strDocumentPath & "\" & dRow.Item("DOC_TYPE").ToString & "\" & dRow.Item("filename").ToString
                    dtRow.Item("file_path") = strDocument_Path & "\" & dRow.Item("DOC_TYPE").ToString & "\" & dRow.Item("filename").ToString
                    'Shani(24-Aug-2015) -- End

                    dtRow.Item("filename") = dRow.Item("filename")

                    'SHANI (16 JUL 2015) -- Start
                    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                    'Upload Image folder to access those attachemts from Server and 
                    'all client machines if ArutiSelfService is installed otherwise save then on Document path
                    'If System.IO.File.Exists(dtRow.Item("file_path").ToString) Then
                    '    objfileinfo = My.Computer.FileSystem.GetFileInfo(dtRow.Item("file_path").ToString)
                    '    dtRow.Item("filesize") = ConvertFileSize(objfileinfo.Length)
                    'Else
                    '    dtRow.Item("filesize") = dRow.Item("filesize")
                    'End If
                    dtRow.Item("filesize") = ConvertFileSize(CDbl(CDbl(dRow.Item("filesize_kb")) * 1024))
                    'SHANI (16 JUL 2015) -- End
                    dtRow.Item("attached_date") = dRow.Item("attached_date")
                    dtRow.Item("localpath") = dRow.Item("localpath")
                    dtRow.Item("GUID") = dRow.Item("GUID")
                    dtRow.Item("AUD") = dRow.Item("AUD")

                    'SHANI (16 JUL 2015) -- Start
                    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                    'Upload Image folder to access those attachemts from Server and 
                    'all client machines if ArutiSelfService is installed otherwise save then on Document path
                    dtRow.Item("fileuniquename") = dRow.Item("fileuniquename")
                    dtRow.Item("filepath") = dRow.Item("filepath")
                    dtRow.Item("filesize_kb") = dRow.Item("filesize_kb")
                    dtRow.Item("userunkid") = dRow.Item("userunkid")
                    dtRow.Item("transactionunkid") = dRow.Item("transactionunkid")
                    'SHANI (16 JUL 2015) -- End 

                    'S.SANDEEP [26 JUL 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    dtRow.Item("form_name") = dRow.Item("form_name")
                    'S.SANDEEP [26 JUL 2016] -- END


                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                    dtRow.Item("fileextension") = dRow.Item("fileextension")
                    dtRow.Item("Documentype") = dRow.Item("Documentype")
                    dtRow.Item("DocBase64Value") = dRow.Item("DocBase64Value")
                    'Pinkal (20-Nov-2018) -- End


                    'S.SANDEEP |25-JAN-2019| -- START
                    'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                    dtRow.Item("file_data") = dRow.Item("file_data")
                    'S.SANDEEP |25-JAN-2019| -- END

                    mdtTable.Rows.Add(dtRow)
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetQulificationAttachment", mstrModuleName)
        End Try
        Return mdtTable
    End Function

    Public Function ConvertFileSize(ByVal dblFileLength As Double) As String
        Dim strfileSize As String = "0B"
        Try
            Dim sizes As String() = {"B", "KB", "MB", "GB"}
            Dim len As Double = dblFileLength
            Dim order As Integer = 0
            While len >= 1024 AndAlso order + 1 < sizes.Length
                order += 1
                len = CDbl(len / 1024)
            End While
            strfileSize = [String].Format("{0:0.##} {1}", len, sizes(order))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ConvertFileSize", mstrModuleName)
        End Try
        Return strfileSize
    End Function

    Public Function DeleteTransacation(ByVal intEmpUnkId As Integer, ByVal intScanType As Integer, ByVal intTranunkid As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing, Optional ByVal mstrFormname As String = "") As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : New Screen Training Need Form.
            'If intEmpUnkId > 0 AndAlso intScanType > 0 AndAlso intTranunkid > 0 Then
            If (intEmpUnkId > 0 OrElse intScanType = enScanAttactRefId.STAFF_REQUISITION OrElse intScanType = enScanAttactRefId.TRAINING_NEED_FORM) _
                AndAlso intScanType > 0 AndAlso intTranunkid > 0 Then
                'Sohail (14 Nov 2019) -- End

                If objDOperation Is Nothing Then
                    objDataOperation = New clsDataOperation
                    objDataOperation.BindTransaction()
                Else
                    objDataOperation = objDOperation
                End If


                objDataOperation.ClearParameters()


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'StrQ = "DELETE FROM hrdocuments_tran WHERE employeeunkid = @employeeunkid AND scanattachrefid =@scanattachrefid AND transactionunkid = @transactionunkid "
                StrQ = "update hrdocuments_tran set isactive = 0 "

                StrQ &= " WHERE employeeunkid = @employeeunkid AND scanattachrefid =@scanattachrefid AND transactionunkid = @transactionunkid "

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkId)
                objDataOperation.AddParameter("@scanattachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScanType)
                objDataOperation.AddParameter("@transactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranunkid)

                'Gajanan [22-Feb-2019] -- End



                Call objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If objDOperation Is Nothing Then
                    objDataOperation.ReleaseTransaction(True)
                End If
            End If

            Return True

        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            Throw exForce
        Finally
        End Try
    End Function
    'SHANI (20 JUN 2015) -- End 



    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    Public Function GetCSVAttachmentids(ByVal intEmpUnkId As Integer, ByVal intScanType As Integer, ByVal intTranunkid As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing) As String
        Dim strQ As String = ""
        Dim Attachmentlist As String = ""

        Dim exForce As Exception
        Try
            If objDOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDOperation
            End If

            strQ = "SELECT " & _
                   "ISNULL(STUFF ((SELECT " & _
                               "',' + " & _
                               "CAST( ISNULL(hrdocuments_tran.scanattachtranunkid, 0)as NVARCHAR(MAX)) " & _
                          "FROM hrdocuments_tran " & _
                          "WHERE hrdocuments_tran.employeeunkid = @employeeunkid and scanattachrefid = @scanattachrefid and transactionunkid = @transactionunkid and isactive = 1 " & _
                          "FOR XML PATH ('')) " & _
                     ", 1, 1, ''), '') AS attachids "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkId)
            objDataOperation.AddParameter("@scanattachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScanType)
            objDataOperation.AddParameter("@transactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranunkid)


            Dim dsattachmentList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsNothing(dsattachmentList.Tables(0)) = False AndAlso dsattachmentList.Tables(0).Rows.Count > 0 Then
                Attachmentlist = dsattachmentList.Tables(0).Rows(0)("attachids").ToString()
            End If
            Return Attachmentlist

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAttachmentids", mstrModuleName)
        Finally
            exForce = Nothing
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try

    End Function
    'Gajanan [22-Feb-2019] -- End

    'Sohail (04 Jul 2019) -- Start
    'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
    Public Function GetValidationList(ByVal intModuleRefid As Integer _
                                     , ByVal intScanRefId As Integer _
                                     , ByVal strEmpIDs As String _
                                     ) As DataSet

        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = 0 : Dim blnFlag As Boolean = False : Dim exForce As Exception
        Dim ds As DataSet = Nothing
        Try
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
                objDataOperation = New clsDataOperation
            End If



            If intModuleRefid = enImg_Email_RefId.Applicant_Module Then
                StrQ = "SELECT rcapplicant_master.applicantunkid AS employeeunkid " & _
                        ", rcapplicant_master.applicant_code AS Code " & _
                        ", rcapplicant_master.firstname + ' ' + ISNULL(rcapplicant_master.othername, '') + ' ' + rcapplicant_master.surname AS Name "

                StrQ &= "FROM rcapplicant_master "
            Else
                StrQ = "SELECT hremployee_master.employeeunkid " & _
                        ", hremployee_master.emlpoyeecode AS Code " & _
                        ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS Name "

                StrQ &= "FROM hremployee_master "
            End If



            If intModuleRefid = enImg_Email_RefId.Applicant_Module Then
                StrQ &= " LEFT JOIN hrdocuments_tran ON rcapplicant_master.applicantunkid = hrdocuments_tran.employeeunkid " & _
                        " AND hrdocuments_tran.isactive = 1 "
            Else
                StrQ &= " LEFT JOIN hrdocuments_tran ON hremployee_master.employeeunkid = hrdocuments_tran.employeeunkid " & _
                        " AND hrdocuments_tran.isactive = 1 "
            End If

            StrQ &= " AND hrdocuments_tran.modulerefid = @modulerefid "

            If intModuleRefid = enImg_Email_RefId.Applicant_Module Then
                'StrQ &= " AND rcapplicant_master.isactive = 1 "
            Else
                StrQ &= " AND hremployee_master.isapproved = 1 "
            End If

            If intScanRefId > 0 Then
                StrQ &= " AND hrdocuments_tran.scanattachrefid = @scanattachrefid "
                objDataOperation.AddParameter("@scanattachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScanRefId)
            End If

            If strEmpIDs.Trim.Length > 0 Then
                StrQ &= " AND hrdocuments_tran.employeeunkid IN (" & strEmpIDs & ") @employeeunkid "
            End If

            StrQ &= "WHERE 1 = 1  " & _
                    "AND NOT ( isfromonline = 1 AND LTRIM(ISNULL(UserId, '')) <> '') " & _
                    "AND scanattachtranunkid IS NULL "

            If intModuleRefid = enImg_Email_RefId.Applicant_Module Then
                StrQ &= " ORDER BY rcapplicant_master.firstname + ' ' + ISNULL(rcapplicant_master.othername, '') + ' ' + rcapplicant_master.surname "
            Else
                StrQ &= " ORDER BY hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname "
            End If
            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)

            ds = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return ds

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Function
    'Sohail (04 Jul 2019) -- End

#End Region

'Public Class clsScan_Attach_Documents
'    Private Shared Readonly mstrModuleName As String = "clsScan_Attach_Documents"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintScanattachtranunkid As Integer
'    Private mintEmployeeunkid As Integer
'    Private mintScanattachrefid As Integer
'    Private mstrFilename As String = String.Empty
'    Private mintDocumentunkid As Integer
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set scanattachtranunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Scanattachtranunkid() As Integer
'        Get
'            Return mintScanattachtranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintScanattachtranunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set employeeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Employeeunkid() As Integer
'        Get
'            Return mintEmployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set scanattachrefid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Scanattachrefid() As Integer
'        Get
'            Return mintScanattachrefid
'        End Get
'        Set(ByVal value As Integer)
'            mintScanattachrefid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set filename
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Filename() As String
'        Get
'            Return mstrFilename
'        End Get
'        Set(ByVal value As String)
'            mstrFilename = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set documentunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Documentunkid() As Integer
'        Get
'            Return mintDocumentunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintDocumentunkid = Value
'        End Set
'    End Property
'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  scanattachtranunkid " & _
'              ", employeeunkid " & _
'              ", scanattachrefid " & _
'              ", filename " & _
'              ", documentunkid " & _
'             "FROM hrdocuments_tran " & _
'             "WHERE scanattachtranunkid = @scanattachtranunkid "

'            objDataOperation.AddParameter("@scanattachtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintScanattachTranUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintScanattachtranunkid = CInt(dtRow.Item("scanattachtranunkid"))
'                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
'                mintScanattachrefid = CInt(dtRow.Item("scanattachrefid"))
'                mstrFilename = dtRow.Item("filename").ToString
'                mintDocumentunkid = CInt(dtRow.Item("documentunkid"))
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, ByVal intRefId As Integer, ByVal intEmpId As Integer, Optional ByVal intDocumentId As Integer = 0) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                           "  scanattachtranunkid " & _
'                           ", employeeunkid " & _
'                           ", scanattachrefid " & _
'                           ", filename " & _
'                           ", documentunkid " & _
'                       "FROM hrdocuments_tran " & _
'                       "WHERE scanattachrefid = @scanattachrefid " & _
'                       "AND employeeunkid = @EmpId "

'            If intDocumentId > 0 Then
'                strQ &= " AND documentunkid = @documentunkid "
'                objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDocumentId)
'            End If

'            objDataOperation.AddParameter("@scanattachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRefId)
'            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hrdocuments_tran) </purpose>
'    Public Function Insert() As Boolean
'        If isExist(mintEmployeeunkid, mstrFilename) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "Document is already exist. Please attach different document.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
'            objDataOperation.AddParameter("@scanattachrefid", SqlDbType.int, eZeeDataType.INT_SIZE, mintscanattachrefid.ToString)
'            objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFilename.ToString)
'            objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDocumentunkid.ToString)

'            strQ = "INSERT INTO hrdocuments_tran ( " & _
'                          "  employeeunkid " & _
'                          ", scanattachrefid " & _
'                          ", filename " & _
'                          ", documentunkid" & _
'                        ") VALUES (" & _
'                          "  @employeeunkid " & _
'                          ", @scanattachrefid " & _
'                          ", @filename " & _
'                          ", @documentunkid" & _
'                        "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintScanattachTranUnkId = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (hrdocuments_tran) </purpose>
'    Public Function Update() As Boolean
'        If isExist(mintEmployeeunkid, mstrFilename, mintScanattachtranunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "Document is already exist. Please attach different document.")
'            Return False
'        End If


'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@scanattachtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintscanattachtranunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
'            objDataOperation.AddParameter("@scanattachrefid", SqlDbType.int, eZeeDataType.INT_SIZE, mintscanattachrefid.ToString)
'            objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFilename.ToString)
'            objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDocumentunkid.ToString)

'            strQ = "UPDATE hrdocuments_tran SET " & _
'              "  employeeunkid = @employeeunkid" & _
'              ", scanattachrefid = @scanattachrefid" & _
'              ", filename = @filename " & _
'              ", documentunkid = @documentunkid " & _
'            "WHERE scanattachtranunkid = @scanattachtranunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (hrdocuments_tran) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "DELETE FROM hrdocuments_tran " & _
'            "WHERE scanattachtranunkid = @scanattachtranunkid "

'            objDataOperation.AddParameter("@scanattachtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@scanattachtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intEmpId As Integer, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                      "  scanattachtranunkid " & _
'                      ", employeeunkid " & _
'                      ", scanattachrefid " & _
'                      ", filename " & _
'                     "FROM hrdocuments_tran " & _
'                     "WHERE filename = @filename " & _
'                     "AND employeeunkid = @employeeunkid "

'            If intUnkid > 0 Then
'                strQ &= " AND scanattachtranunkid <> @scanattachtranunkid"
'            End If

'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpId)
'            objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
'            objDataOperation.AddParameter("@scanattachtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Identities")
			Language.setMessage(mstrModuleName, 2, "Memberships")
			Language.setMessage(mstrModuleName, 3, "Qualifications")
			Language.setMessage(mstrModuleName, 4, "Job History")
			Language.setMessage(mstrModuleName, 5, "Leave Forms")
			Language.setMessage(mstrModuleName, 6, "Discipline")
			Language.setMessage(mstrModuleName, 7, "Curriculum Vitae")
			Language.setMessage(mstrModuleName, 8, "Others")
			Language.setMessage(mstrModuleName, 9, "Trainings")
			Language.setMessage(mstrModuleName, 10, "Select")
			Language.setMessage(mstrModuleName, 11, "Asset Declaration")
			Language.setMessage(mstrModuleName, 12, "The Selected file with same name already exists. Please rename the selected file from source if you would like to attach the selected file.")
			Language.setMessage(mstrModuleName, 13, "Dependants")
			Language.setMessage(mstrModuleName, 14, "Claim Request")
			Language.setMessage(mstrModuleName, 15, "Assessment")
			Language.setMessage(mstrModuleName, 16, "Personal Address")
			Language.setMessage(mstrModuleName, 17, "Domicile Address")
			Language.setMessage(mstrModuleName, 18, "Recruitment Address")
			Language.setMessage(mstrModuleName, 19, "Emergency Contact1")
			Language.setMessage(mstrModuleName, 20, "Emergency Contact2")
			Language.setMessage(mstrModuleName, 21, "Emergency Contact3")
			Language.setMessage(mstrModuleName, 22, "Employee BirthInfo")
			Language.setMessage(mstrModuleName, 23, "Employee Other Info")
			Language.setMessage(mstrModuleName, 24, "Cover Letter")
			Language.setMessage(mstrModuleName, 25, "Staff Requisition")
			Language.setMessage(mstrModuleName, 26, "Claim Retirement")
			Language.setMessage(mstrModuleName, 27, "Training Need Form")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


