﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 2

Public Class frmZipCodeList

#Region "Private Variable"

    Private objZipcodemaster As clszipcode_master
    Private ReadOnly mstrModuleName As String = "frmZipCodeList"

#End Region

#Region "Form's Event"

    Private Sub frmZipCodeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objZipcodemaster = New clszipcode_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            FillCombo()
            fillList()
            If lvZipcode.Items.Count > 0 Then lvZipcode.Items(0).Selected = True
            lvZipcode.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmZipCodeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmZipCodeList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvZipcode.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmZipCodeList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmZipCodeList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objZipcodemaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clszipcode_master.SetMessages()
            objfrm._Other_ModuleNames = "clszipcode_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Dropdown's Event"

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            FillState()
            FillCity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try
            FillCity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmZipCode_AddEdit As New frmZipcode_AddEdit
            If objfrmZipCode_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvZipcode.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Zipcode from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvZipcode.Select()
                Exit Sub
            End If
            Dim objfrmZipcode_AddEdit As New frmZipcode_AddEdit
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvZipcode.SelectedItems(0).Index
                If objfrmZipcode_AddEdit.displayDialog(CInt(lvZipcode.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call fillList()
                End If
                objfrmZipcode_AddEdit = Nothing

                lvZipcode.Items(intSelectedIndex).Selected = True
                lvZipcode.EnsureVisible(intSelectedIndex)
                lvZipcode.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmZipcode_AddEdit IsNot Nothing Then objfrmZipcode_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvZipcode.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Zipcode from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvZipcode.Select()
            Exit Sub
        End If
        If objZipcodemaster.isUsed(CInt(lvZipcode.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Zipcode. Reason: This Zipcode is in use."), enMsgBoxStyle.Information) '?2
            lvZipcode.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvZipcode.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Zipcode?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objZipcodemaster.Delete(CInt(lvZipcode.SelectedItems(0).Tag))
                lvZipcode.SelectedItems(0).Remove()

                If lvZipcode.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvZipcode.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvZipcode.Items.Count - 1
                    lvZipcode.Items(intSelectedIndex).Selected = True
                    lvZipcode.EnsureVisible(intSelectedIndex)
                ElseIf lvZipcode.Items.Count <> 0 Then
                    lvZipcode.Items(intSelectedIndex).Selected = True
                    lvZipcode.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvZipcode.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCountry.SelectedIndex = 0
            cboState.SelectedIndex = 0
            cboCity.SelectedIndex = 0
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim strSearching As String = String.Empty
        Dim dsZipcodeList As New DataSet
        Dim dtZipcode As DataTable
        Try
            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            If User._Object.Privilege._AllowToViewZipcodeList = False Then Exit Sub
            'Anjan (25 Oct 2012)-End 


            dsZipcodeList = objZipcodemaster.GetList("Zipcode")

            If CInt(cboCountry.SelectedValue) > 0 Then
                strSearching = "AND countryunkid=" & CInt(cboCountry.SelectedValue) & " AND stateunkid=" & CInt(cboState.SelectedValue) & " AND cityunkid=" & _
                CInt(cboCity.SelectedValue)
            End If

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtZipcode = New DataView(dsZipcodeList.Tables("Zipcode"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtZipcode = dsZipcodeList.Tables("Zipcode")
            End If

            Dim lvItem As ListViewItem

            lvZipcode.Items.Clear()
            For Each drRow As DataRow In dtZipcode.Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("country").ToString
                lvItem.Tag = drRow("zipcodeunkid")
                lvItem.SubItems.Add(drRow("state").ToString)
                lvItem.SubItems.Add(drRow("city").ToString)
                lvItem.SubItems.Add(drRow("zipcode_code").ToString)
                lvItem.SubItems.Add(drRow("zipcode_no").ToString)
                lvZipcode.Items.Add(lvItem)
            Next

            If lvZipcode.Items.Count > 16 Then
                colhZipcodeNo.Width = 161 - 18
            Else
                colhZipcodeNo.Width = 161
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsZipcodeList.Dispose()
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMasterdata As New clsMasterData
            Dim dsCountry As DataSet = objMasterdata.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsCountry.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillState()
        Try
            Dim objStateMaster As New clsstate_master
            Dim dsState As DataSet = Nothing
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsState = objStateMaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            Else
                dsState = objStateMaster.GetList("State", True, True)
            End If

            cboState.DisplayMember = "name"
            cboState.ValueMember = "stateunkid"
            cboState.DataSource = dsState.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillState", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCity()
        Try
            Dim objCityMaster As New clscity_master
            Dim dsCity As DataSet = Nothing
            If CInt(cboState.SelectedValue) > 0 Then
                dsCity = objCityMaster.GetList("City", True, False, CInt(cboState.SelectedValue))
            Else
                dsCity = objCityMaster.GetList("City", True, True)
            End If

            cboCity.DisplayMember = "name"
            cboCity.ValueMember = "cityunkid"
            cboCity.DataSource = dsCity.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCity", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddZipCode
            btnEdit.Enabled = User._Object.Privilege._EditZipCode
            btnDelete.Enabled = User._Object.Privilege._DeleteZipCode
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbZipcode.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbZipcode.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.colhCity.Text = Language._Object.getCaption(CStr(Me.colhCity.Tag), Me.colhCity.Text)
            Me.colhState.Text = Language._Object.getCaption(CStr(Me.colhState.Tag), Me.colhState.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCountry.Text = Language._Object.getCaption(CStr(Me.colhCountry.Tag), Me.colhCountry.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.gbZipcode.Text = Language._Object.getCaption(Me.gbZipcode.Name, Me.gbZipcode.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhZipcodeNo.Text = Language._Object.getCaption(CStr(Me.colhZipcodeNo.Tag), Me.colhZipcodeNo.Text)
            Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Zipcode from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Zipcode. Reason: This Zipcode is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Zipcode?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class