﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5

Public Class frmZipcode_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmZipcode_AddEdit"
    Private mblnCancel As Boolean = True
    Private objZipcodemaster As clszipcode_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintZipcodeMasterUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintZipcodeMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintZipcodeMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmZipcode_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objZipcodemaster = New clszipcode_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objZipcodemaster._Zipcodeunkid = mintZipcodeMasterUnkid
            End If
            FillCombo()
            GetValue()
            cboCountry.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmZipcode_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmZipcode_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmZipcode_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmZipcode_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmZipcode_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmZipcode_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objZipcodemaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clszipcode_master.SetMessages()
            objfrm._Other_ModuleNames = "clszipcode_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Dropdown's Event"

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            FillState()
            'FillCity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try
            FillCity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboCountry.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Country is compulsory information.Please Select Country."), enMsgBoxStyle.Information)
                cboCountry.Select()
                Exit Sub
            ElseIf CInt(cboState.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "State is compulsory information.Please Select State."), enMsgBoxStyle.Information)
                cboState.Select()
                Exit Sub
            ElseIf CInt(cboCity.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "City is compulsory information.Please Select City."), enMsgBoxStyle.Information)
                cboCity.Select()
                Exit Sub
                'ElseIf Trim(txtZipCode.Text) = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Zipcode cannot be blank. Zipcode is required information."), enMsgBoxStyle.Information)
                '    txtZipCode.Select()
                '    Exit Sub
            ElseIf Trim(txtZipcodeNo.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Zipcode No cannot be blank. Zipcode No is required information."), enMsgBoxStyle.Information)
                txtZipcodeNo.Select()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objZipcodemaster.Update()
            Else
                blnFlag = objZipcodemaster.Insert()
            End If

            If blnFlag = False And objZipcodemaster._Message <> "" Then
                eZeeMsgBox.Show(objZipcodemaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objZipcodemaster = Nothing
                    objZipcodemaster = New clszipcode_master
                    Call GetValue()
                    cboCountry.Select()
                Else
                    mintZipcodeMasterUnkid = objZipcodemaster._Zipcodeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddState_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddState.Click
        Try
            Dim objfrmStatemaster As New frmState_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmStatemaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmStatemaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmStatemaster)
            End If
            'Anjan (02 Sep 2011)-End 
            If objfrmStatemaster.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillState()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddState_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddCity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCity.Click
        Try
            Dim objfrmCitymaster As New frmCity_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmCitymaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCitymaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCitymaster)
            End If
            'Anjan (02 Sep 2011)-End 
            If objfrmCitymaster.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillCity()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCity_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboCountry.BackColor = GUI.ColorComp
            cboState.BackColor = GUI.ColorComp
            cboCity.BackColor = GUI.ColorComp
            txtZipCode.BackColor = GUI.ColorOptional
            txtZipcodeNo.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboCountry.SelectedValue = objZipcodemaster._Countryunkid
            cboState.SelectedValue = objZipcodemaster._Stateunkid
            cboCity.SelectedValue = objZipcodemaster._Cityunkid
            txtZipCode.Text = objZipcodemaster._Zipcode_Code
            txtZipcodeNo.Text = objZipcodemaster._Zipcode_No
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objZipcodemaster._Countryunkid = CInt(cboCountry.SelectedValue)
            objZipcodemaster._Stateunkid = CInt(cboState.SelectedValue)
            objZipcodemaster._Cityunkid = CInt(cboCity.SelectedValue)
            objZipcodemaster._Zipcode_Code = txtZipCode.Text.Trim
            objZipcodemaster._Zipcode_No = txtZipcodeNo.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMasterdata As New clsMasterData
            Dim dsCountry As DataSet = objMasterdata.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsCountry.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillState()
        Dim dsState As DataSet = Nothing
        Try
            Dim objStatemaster As New clsstate_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsState = objStatemaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            Else
                dsState = objStatemaster.GetList("State", True, True)
            End If
            cboState.ValueMember = "stateunkid"
            cboState.DisplayMember = "name"
            cboState.DataSource = dsState.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillState", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCity()
        Dim dsCity As DataSet = Nothing
        Try
            Dim objCitymaster As New clscity_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsCity = objCitymaster.GetList("City", True, False, CInt(cboState.SelectedValue))
            Else
                dsCity = objCitymaster.GetList("City", True, True)
            End If
            cboCity.ValueMember = "cityunkid"
            cboCity.DisplayMember = "name"
            cboCity.DataSource = dsCity.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCity", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            objbtnAddCity.Enabled = User._Object.Privilege._AddCity
            objbtnAddState.Enabled = User._Object.Privilege._AddState
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbZipcode.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbZipcode.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblZipcode.Text = Language._Object.getCaption(Me.lblZipcode.Name, Me.lblZipcode.Text)
			Me.gbZipcode.Text = Language._Object.getCaption(Me.gbZipcode.Name, Me.gbZipcode.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Country is compulsory information.Please Select Country.")
			Language.setMessage(mstrModuleName, 2, "State is compulsory information.Please Select State.")
			Language.setMessage(mstrModuleName, 3, "City is compulsory information.Please Select City.")
			Language.setMessage(mstrModuleName, 4, "Zipcode No cannot be blank. Zipcode No is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class