﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApprover_Swapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApprover_Swapping))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.EZeeHeading1 = New eZee.Common.eZeeHeading
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.LblCategory = New System.Windows.Forms.Label
        Me.btnSwap = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgFromEmployee = New System.Windows.Forms.DataGridView
        Me.dgcolhFromEmpcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFromEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgToEmployee = New System.Windows.Forms.DataGridView
        Me.dgcolhToEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhToEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.LblFromLevel = New System.Windows.Forms.Label
        Me.lblToApprover = New System.Windows.Forms.Label
        Me.objbtnSearchFromLevel = New eZee.Common.eZeeGradientButton
        Me.cboToLevel = New System.Windows.Forms.ComboBox
        Me.cboFromLevel = New System.Windows.Forms.ComboBox
        Me.cboToApprover = New System.Windows.Forms.ComboBox
        Me.objbtnSearchFromApprover = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchToLevel = New eZee.Common.eZeeGradientButton
        Me.lblToLevel = New System.Windows.Forms.Label
        Me.cboFromApprover = New System.Windows.Forms.ComboBox
        Me.objbtnSearchToApprover = New eZee.Common.eZeeGradientButton
        Me.lblFromApprover = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.EZeeHeading1.SuspendLayout()
        CType(Me.dgFromEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgToEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.EZeeHeading1)
        Me.Panel1.Controls.Add(Me.btnSwap)
        Me.Panel1.Controls.Add(Me.dgFromEmployee)
        Me.Panel1.Controls.Add(Me.dgToEmployee)
        Me.Panel1.Controls.Add(Me.EZeeFooter1)
        Me.Panel1.Controls.Add(Me.LblFromLevel)
        Me.Panel1.Controls.Add(Me.lblToApprover)
        Me.Panel1.Controls.Add(Me.objbtnSearchFromLevel)
        Me.Panel1.Controls.Add(Me.cboToLevel)
        Me.Panel1.Controls.Add(Me.cboFromLevel)
        Me.Panel1.Controls.Add(Me.cboToApprover)
        Me.Panel1.Controls.Add(Me.objbtnSearchFromApprover)
        Me.Panel1.Controls.Add(Me.objbtnSearchToLevel)
        Me.Panel1.Controls.Add(Me.lblToLevel)
        Me.Panel1.Controls.Add(Me.cboFromApprover)
        Me.Panel1.Controls.Add(Me.objbtnSearchToApprover)
        Me.Panel1.Controls.Add(Me.lblFromApprover)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(763, 499)
        Me.Panel1.TabIndex = 0
        '
        'EZeeHeading1
        '
        Me.EZeeHeading1.BorderColor = System.Drawing.Color.Black
        Me.EZeeHeading1.Controls.Add(Me.cboCategory)
        Me.EZeeHeading1.Controls.Add(Me.LblCategory)
        Me.EZeeHeading1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeading1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeading1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeHeading1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeading1.Name = "EZeeHeading1"
        Me.EZeeHeading1.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.EZeeHeading1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeHeading1.ShowDefaultBorderColor = True
        Me.EZeeHeading1.Size = New System.Drawing.Size(763, 25)
        Me.EZeeHeading1.TabIndex = 140
        Me.EZeeHeading1.Text = "Swap Approver"
        '
        'cboCategory
        '
        Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Location = New System.Drawing.Point(516, 2)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(212, 21)
        Me.cboCategory.TabIndex = 312
        '
        'LblCategory
        '
        Me.LblCategory.BackColor = System.Drawing.Color.Transparent
        Me.LblCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCategory.Location = New System.Drawing.Point(433, 5)
        Me.LblCategory.Name = "LblCategory"
        Me.LblCategory.Size = New System.Drawing.Size(77, 15)
        Me.LblCategory.TabIndex = 313
        Me.LblCategory.Text = "Category"
        '
        'btnSwap
        '
        Me.btnSwap.BackColor = System.Drawing.Color.White
        Me.btnSwap.BackgroundImage = CType(resources.GetObject("btnSwap.BackgroundImage"), System.Drawing.Image)
        Me.btnSwap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSwap.BorderColor = System.Drawing.Color.Empty
        Me.btnSwap.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSwap.FlatAppearance.BorderSize = 0
        Me.btnSwap.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSwap.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSwap.ForeColor = System.Drawing.Color.Black
        Me.btnSwap.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSwap.GradientForeColor = System.Drawing.Color.Black
        Me.btnSwap.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSwap.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSwap.Location = New System.Drawing.Point(334, 223)
        Me.btnSwap.Name = "btnSwap"
        Me.btnSwap.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSwap.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSwap.Size = New System.Drawing.Size(94, 67)
        Me.btnSwap.TabIndex = 139
        Me.btnSwap.Text = "Swap"
        Me.btnSwap.UseVisualStyleBackColor = True
        '
        'dgFromEmployee
        '
        Me.dgFromEmployee.AllowUserToAddRows = False
        Me.dgFromEmployee.AllowUserToDeleteRows = False
        Me.dgFromEmployee.AllowUserToResizeColumns = False
        Me.dgFromEmployee.AllowUserToResizeRows = False
        Me.dgFromEmployee.BackgroundColor = System.Drawing.Color.White
        Me.dgFromEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgFromEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhFromEmpcode, Me.dgcolhFromEmployee})
        Me.dgFromEmployee.Location = New System.Drawing.Point(8, 89)
        Me.dgFromEmployee.Name = "dgFromEmployee"
        Me.dgFromEmployee.RowHeadersVisible = False
        Me.dgFromEmployee.Size = New System.Drawing.Size(319, 348)
        Me.dgFromEmployee.TabIndex = 138
        '
        'dgcolhFromEmpcode
        '
        Me.dgcolhFromEmpcode.FillWeight = 145.3488!
        Me.dgcolhFromEmpcode.HeaderText = "Employee Code"
        Me.dgcolhFromEmpcode.Name = "dgcolhFromEmpcode"
        Me.dgcolhFromEmpcode.ReadOnly = True
        Me.dgcolhFromEmpcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhFromEmpcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhFromEmpcode.Width = 125
        '
        'dgcolhFromEmployee
        '
        Me.dgcolhFromEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhFromEmployee.FillWeight = 54.65117!
        Me.dgcolhFromEmployee.HeaderText = "Employee"
        Me.dgcolhFromEmployee.Name = "dgcolhFromEmployee"
        Me.dgcolhFromEmployee.ReadOnly = True
        Me.dgcolhFromEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhFromEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgToEmployee
        '
        Me.dgToEmployee.AllowUserToAddRows = False
        Me.dgToEmployee.AllowUserToDeleteRows = False
        Me.dgToEmployee.AllowUserToResizeColumns = False
        Me.dgToEmployee.AllowUserToResizeRows = False
        Me.dgToEmployee.BackgroundColor = System.Drawing.Color.White
        Me.dgToEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgToEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhToEmpCode, Me.dgcolhToEmployee})
        Me.dgToEmployee.Location = New System.Drawing.Point(435, 89)
        Me.dgToEmployee.Name = "dgToEmployee"
        Me.dgToEmployee.RowHeadersVisible = False
        Me.dgToEmployee.Size = New System.Drawing.Size(319, 348)
        Me.dgToEmployee.TabIndex = 137
        '
        'dgcolhToEmpCode
        '
        Me.dgcolhToEmpCode.FillWeight = 145.3488!
        Me.dgcolhToEmpCode.HeaderText = "Employee Code"
        Me.dgcolhToEmpCode.Name = "dgcolhToEmpCode"
        Me.dgcolhToEmpCode.ReadOnly = True
        Me.dgcolhToEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhToEmpCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhToEmpCode.Width = 125
        '
        'dgcolhToEmployee
        '
        Me.dgcolhToEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhToEmployee.FillWeight = 54.65117!
        Me.dgcolhToEmployee.HeaderText = "Employee"
        Me.dgcolhToEmployee.Name = "dgcolhToEmployee"
        Me.dgcolhToEmployee.ReadOnly = True
        Me.dgcolhToEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhToEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 444)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(763, 55)
        Me.EZeeFooter1.TabIndex = 136
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(553, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(96, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(655, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(96, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'LblFromLevel
        '
        Me.LblFromLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFromLevel.Location = New System.Drawing.Point(6, 64)
        Me.LblFromLevel.Name = "LblFromLevel"
        Me.LblFromLevel.Size = New System.Drawing.Size(80, 15)
        Me.LblFromLevel.TabIndex = 134
        Me.LblFromLevel.Text = "Level"
        Me.LblFromLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToApprover
        '
        Me.lblToApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToApprover.Location = New System.Drawing.Point(433, 38)
        Me.lblToApprover.Name = "lblToApprover"
        Me.lblToApprover.Size = New System.Drawing.Size(77, 15)
        Me.lblToApprover.TabIndex = 129
        Me.lblToApprover.Text = "To Approver"
        Me.lblToApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchFromLevel
        '
        Me.objbtnSearchFromLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFromLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFromLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFromLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFromLevel.BorderSelected = False
        Me.objbtnSearchFromLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFromLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchFromLevel.Image = CType(resources.GetObject("objbtnSearchFromLevel.Image"), System.Drawing.Image)
        Me.objbtnSearchFromLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFromLevel.Location = New System.Drawing.Point(305, 61)
        Me.objbtnSearchFromLevel.Name = "objbtnSearchFromLevel"
        Me.objbtnSearchFromLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFromLevel.TabIndex = 133
        '
        'cboToLevel
        '
        Me.cboToLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToLevel.DropDownWidth = 300
        Me.cboToLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToLevel.FormattingEnabled = True
        Me.cboToLevel.Location = New System.Drawing.Point(516, 62)
        Me.cboToLevel.Name = "cboToLevel"
        Me.cboToLevel.Size = New System.Drawing.Size(212, 21)
        Me.cboToLevel.TabIndex = 125
        '
        'cboFromLevel
        '
        Me.cboFromLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromLevel.DropDownWidth = 300
        Me.cboFromLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromLevel.FormattingEnabled = True
        Me.cboFromLevel.Location = New System.Drawing.Point(89, 61)
        Me.cboFromLevel.Name = "cboFromLevel"
        Me.cboFromLevel.Size = New System.Drawing.Size(212, 21)
        Me.cboFromLevel.TabIndex = 132
        '
        'cboToApprover
        '
        Me.cboToApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToApprover.DropDownWidth = 300
        Me.cboToApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToApprover.FormattingEnabled = True
        Me.cboToApprover.Location = New System.Drawing.Point(516, 35)
        Me.cboToApprover.Name = "cboToApprover"
        Me.cboToApprover.Size = New System.Drawing.Size(212, 21)
        Me.cboToApprover.TabIndex = 124
        '
        'objbtnSearchFromApprover
        '
        Me.objbtnSearchFromApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFromApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFromApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFromApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFromApprover.BorderSelected = False
        Me.objbtnSearchFromApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFromApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchFromApprover.Image = CType(resources.GetObject("objbtnSearchFromApprover.Image"), System.Drawing.Image)
        Me.objbtnSearchFromApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFromApprover.Location = New System.Drawing.Point(305, 34)
        Me.objbtnSearchFromApprover.Name = "objbtnSearchFromApprover"
        Me.objbtnSearchFromApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFromApprover.TabIndex = 131
        '
        'objbtnSearchToLevel
        '
        Me.objbtnSearchToLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchToLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchToLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchToLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchToLevel.BorderSelected = False
        Me.objbtnSearchToLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchToLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchToLevel.Image = CType(resources.GetObject("objbtnSearchToLevel.Image"), System.Drawing.Image)
        Me.objbtnSearchToLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchToLevel.Location = New System.Drawing.Point(734, 62)
        Me.objbtnSearchToLevel.Name = "objbtnSearchToLevel"
        Me.objbtnSearchToLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchToLevel.TabIndex = 126
        '
        'lblToLevel
        '
        Me.lblToLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToLevel.Location = New System.Drawing.Point(433, 65)
        Me.lblToLevel.Name = "lblToLevel"
        Me.lblToLevel.Size = New System.Drawing.Size(77, 15)
        Me.lblToLevel.TabIndex = 130
        Me.lblToLevel.Text = "Level"
        Me.lblToLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromApprover
        '
        Me.cboFromApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromApprover.DropDownWidth = 300
        Me.cboFromApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromApprover.FormattingEnabled = True
        Me.cboFromApprover.Location = New System.Drawing.Point(89, 34)
        Me.cboFromApprover.Name = "cboFromApprover"
        Me.cboFromApprover.Size = New System.Drawing.Size(212, 21)
        Me.cboFromApprover.TabIndex = 123
        '
        'objbtnSearchToApprover
        '
        Me.objbtnSearchToApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchToApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchToApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchToApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchToApprover.BorderSelected = False
        Me.objbtnSearchToApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchToApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchToApprover.Image = CType(resources.GetObject("objbtnSearchToApprover.Image"), System.Drawing.Image)
        Me.objbtnSearchToApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchToApprover.Location = New System.Drawing.Point(734, 35)
        Me.objbtnSearchToApprover.Name = "objbtnSearchToApprover"
        Me.objbtnSearchToApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchToApprover.TabIndex = 127
        '
        'lblFromApprover
        '
        Me.lblFromApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromApprover.Location = New System.Drawing.Point(6, 37)
        Me.lblFromApprover.Name = "lblFromApprover"
        Me.lblFromApprover.Size = New System.Drawing.Size(80, 15)
        Me.lblFromApprover.TabIndex = 128
        Me.lblFromApprover.Text = "From Approver"
        Me.lblFromApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmApprover_Swapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 499)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApprover_Swapping"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Swap Approver"
        Me.Panel1.ResumeLayout(False)
        Me.EZeeHeading1.ResumeLayout(False)
        CType(Me.dgFromEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgToEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LblFromLevel As System.Windows.Forms.Label
    Friend WithEvents lblToApprover As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchFromLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents cboToLevel As System.Windows.Forms.ComboBox
    Friend WithEvents cboFromLevel As System.Windows.Forms.ComboBox
    Friend WithEvents cboToApprover As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchFromApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchToLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents lblToLevel As System.Windows.Forms.Label
    Friend WithEvents cboFromApprover As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchToApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgFromEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents dgToEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents btnSwap As eZee.Common.eZeeLightButton
    Friend WithEvents dgcolhFromEmpcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFromEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhToEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhToEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EZeeHeading1 As eZee.Common.eZeeHeading
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents LblCategory As System.Windows.Forms.Label
    Friend WithEvents lblFromApprover As System.Windows.Forms.Label
End Class
