﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBankBranch_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBankBranch_AddEdit))
        Me.PnlVendor = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbBankInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchBankGroup = New eZee.Common.eZeeGradientButton
        Me.chkClearing = New System.Windows.Forms.CheckBox
        Me.txtSortCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblSortCode = New System.Windows.Forms.Label
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.cboZipcode = New System.Windows.Forms.ComboBox
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.txtname = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtcontactperson = New eZee.TextBox.AlphanumericTextBox
        Me.lblcontactperson = New System.Windows.Forms.Label
        Me.txtcontactno = New eZee.TextBox.AlphanumericTextBox
        Me.lblContactNo = New System.Windows.Forms.Label
        Me.lblPincode = New System.Windows.Forms.Label
        Me.lblstate = New System.Windows.Forms.Label
        Me.txtaddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtaddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.cboGroup = New System.Windows.Forms.ComboBox
        Me.lblGroup = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.PnlVendor.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbBankInformation.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlVendor
        '
        Me.PnlVendor.Controls.Add(Me.objFooter)
        Me.PnlVendor.Controls.Add(Me.gbBankInformation)
        Me.PnlVendor.Controls.Add(Me.eZeeHeader)
        Me.PnlVendor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlVendor.Location = New System.Drawing.Point(0, 0)
        Me.PnlVendor.Name = "PnlVendor"
        Me.PnlVendor.Size = New System.Drawing.Size(469, 377)
        Me.PnlVendor.TabIndex = 2
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 327)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(469, 50)
        Me.objFooter.TabIndex = 15
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(363, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 14
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(263, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbBankInformation
        '
        Me.gbBankInformation.BorderColor = System.Drawing.Color.Black
        Me.gbBankInformation.Checked = False
        Me.gbBankInformation.CollapseAllExceptThis = False
        Me.gbBankInformation.CollapsedHoverImage = Nothing
        Me.gbBankInformation.CollapsedNormalImage = Nothing
        Me.gbBankInformation.CollapsedPressedImage = Nothing
        Me.gbBankInformation.CollapseOnLoad = False
        Me.gbBankInformation.Controls.Add(Me.objbtnSearchBankGroup)
        Me.gbBankInformation.Controls.Add(Me.chkClearing)
        Me.gbBankInformation.Controls.Add(Me.txtSortCode)
        Me.gbBankInformation.Controls.Add(Me.lblSortCode)
        Me.gbBankInformation.Controls.Add(Me.objbtnAddGroup)
        Me.gbBankInformation.Controls.Add(Me.cboCity)
        Me.gbBankInformation.Controls.Add(Me.lblCity)
        Me.gbBankInformation.Controls.Add(Me.cboCountry)
        Me.gbBankInformation.Controls.Add(Me.lblCountry)
        Me.gbBankInformation.Controls.Add(Me.cboZipcode)
        Me.gbBankInformation.Controls.Add(Me.cboState)
        Me.gbBankInformation.Controls.Add(Me.txtname)
        Me.gbBankInformation.Controls.Add(Me.lblName)
        Me.gbBankInformation.Controls.Add(Me.txtcontactperson)
        Me.gbBankInformation.Controls.Add(Me.lblcontactperson)
        Me.gbBankInformation.Controls.Add(Me.txtcontactno)
        Me.gbBankInformation.Controls.Add(Me.lblContactNo)
        Me.gbBankInformation.Controls.Add(Me.lblPincode)
        Me.gbBankInformation.Controls.Add(Me.lblstate)
        Me.gbBankInformation.Controls.Add(Me.txtaddress2)
        Me.gbBankInformation.Controls.Add(Me.txtaddress1)
        Me.gbBankInformation.Controls.Add(Me.lblAddress)
        Me.gbBankInformation.Controls.Add(Me.txtCode)
        Me.gbBankInformation.Controls.Add(Me.lblCode)
        Me.gbBankInformation.Controls.Add(Me.cboGroup)
        Me.gbBankInformation.Controls.Add(Me.lblGroup)
        Me.gbBankInformation.ExpandedHoverImage = Nothing
        Me.gbBankInformation.ExpandedNormalImage = Nothing
        Me.gbBankInformation.ExpandedPressedImage = Nothing
        Me.gbBankInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBankInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBankInformation.HeaderHeight = 25
        Me.gbBankInformation.HeaderMessage = ""
        Me.gbBankInformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBankInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBankInformation.HeightOnCollapse = 0
        Me.gbBankInformation.LeftTextSpace = 0
        Me.gbBankInformation.Location = New System.Drawing.Point(9, 68)
        Me.gbBankInformation.Name = "gbBankInformation"
        Me.gbBankInformation.OpenHeight = 300
        Me.gbBankInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBankInformation.ShowBorder = True
        Me.gbBankInformation.ShowCheckBox = False
        Me.gbBankInformation.ShowCollapseButton = False
        Me.gbBankInformation.ShowDefaultBorderColor = True
        Me.gbBankInformation.ShowDownButton = False
        Me.gbBankInformation.ShowHeader = True
        Me.gbBankInformation.Size = New System.Drawing.Size(448, 253)
        Me.gbBankInformation.TabIndex = 1
        Me.gbBankInformation.Temp = 0
        Me.gbBankInformation.Text = "Bank Branch"
        Me.gbBankInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchBankGroup
        '
        Me.objbtnSearchBankGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankGroup.BorderSelected = False
        Me.objbtnSearchBankGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankGroup.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBankGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankGroup.Location = New System.Drawing.Point(259, 30)
        Me.objbtnSearchBankGroup.Name = "objbtnSearchBankGroup"
        Me.objbtnSearchBankGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankGroup.TabIndex = 221
        '
        'chkClearing
        '
        Me.chkClearing.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkClearing.Location = New System.Drawing.Point(313, 32)
        Me.chkClearing.Name = "chkClearing"
        Me.chkClearing.Size = New System.Drawing.Size(81, 17)
        Me.chkClearing.TabIndex = 3
        Me.chkClearing.Text = "Clearing"
        Me.chkClearing.UseVisualStyleBackColor = True
        '
        'txtSortCode
        '
        Me.txtSortCode.Flags = 0
        Me.txtSortCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSortCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSortCode.Location = New System.Drawing.Point(313, 57)
        Me.txtSortCode.Name = "txtSortCode"
        Me.txtSortCode.Size = New System.Drawing.Size(124, 21)
        Me.txtSortCode.TabIndex = 3
        '
        'lblSortCode
        '
        Me.lblSortCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSortCode.Location = New System.Drawing.Point(230, 59)
        Me.lblSortCode.Name = "lblSortCode"
        Me.lblSortCode.Size = New System.Drawing.Size(72, 16)
        Me.lblSortCode.TabIndex = 219
        Me.lblSortCode.Text = "Sort Code"
        Me.lblSortCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(232, 30)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 216
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(101, 192)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(124, 21)
        Me.cboCity.TabIndex = 9
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(9, 194)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(86, 16)
        Me.lblCity.TabIndex = 56
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(101, 165)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(124, 21)
        Me.cboCountry.TabIndex = 7
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(9, 166)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(86, 16)
        Me.lblCountry.TabIndex = 52
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboZipcode
        '
        Me.cboZipcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboZipcode.FormattingEnabled = True
        Me.cboZipcode.Location = New System.Drawing.Point(318, 192)
        Me.cboZipcode.Name = "cboZipcode"
        Me.cboZipcode.Size = New System.Drawing.Size(119, 21)
        Me.cboZipcode.TabIndex = 10
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(318, 164)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(119, 21)
        Me.cboState.TabIndex = 8
        '
        'txtname
        '
        Me.txtname.Flags = 0
        Me.txtname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtname.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtname.Location = New System.Drawing.Point(101, 84)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(336, 21)
        Me.txtname.TabIndex = 4
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(9, 85)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(86, 16)
        Me.lblName.TabIndex = 47
        Me.lblName.Text = "Branch Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtcontactperson
        '
        Me.txtcontactperson.Flags = 0
        Me.txtcontactperson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcontactperson.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtcontactperson.Location = New System.Drawing.Point(101, 219)
        Me.txtcontactperson.Name = "txtcontactperson"
        Me.txtcontactperson.Size = New System.Drawing.Size(124, 21)
        Me.txtcontactperson.TabIndex = 11
        '
        'lblcontactperson
        '
        Me.lblcontactperson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcontactperson.Location = New System.Drawing.Point(9, 222)
        Me.lblcontactperson.Name = "lblcontactperson"
        Me.lblcontactperson.Size = New System.Drawing.Size(86, 16)
        Me.lblcontactperson.TabIndex = 40
        Me.lblcontactperson.Text = "Contact Person"
        Me.lblcontactperson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtcontactno
        '
        Me.txtcontactno.Flags = 0
        Me.txtcontactno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcontactno.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtcontactno.Location = New System.Drawing.Point(318, 220)
        Me.txtcontactno.Name = "txtcontactno"
        Me.txtcontactno.Size = New System.Drawing.Size(119, 21)
        Me.txtcontactno.TabIndex = 12
        '
        'lblContactNo
        '
        Me.lblContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNo.Location = New System.Drawing.Point(229, 221)
        Me.lblContactNo.Name = "lblContactNo"
        Me.lblContactNo.Size = New System.Drawing.Size(81, 16)
        Me.lblContactNo.TabIndex = 38
        Me.lblContactNo.Text = "Contact No"
        Me.lblContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPincode
        '
        Me.lblPincode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPincode.Location = New System.Drawing.Point(229, 193)
        Me.lblPincode.Name = "lblPincode"
        Me.lblPincode.Size = New System.Drawing.Size(81, 16)
        Me.lblPincode.TabIndex = 36
        Me.lblPincode.Text = "ZIP code"
        Me.lblPincode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblstate
        '
        Me.lblstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblstate.Location = New System.Drawing.Point(229, 167)
        Me.lblstate.Name = "lblstate"
        Me.lblstate.Size = New System.Drawing.Size(81, 16)
        Me.lblstate.TabIndex = 34
        Me.lblstate.Text = "State"
        Me.lblstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtaddress2
        '
        Me.txtaddress2.Flags = 0
        Me.txtaddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtaddress2.Location = New System.Drawing.Point(101, 138)
        Me.txtaddress2.Name = "txtaddress2"
        Me.txtaddress2.Size = New System.Drawing.Size(336, 21)
        Me.txtaddress2.TabIndex = 6
        '
        'txtaddress1
        '
        Me.txtaddress1.Flags = 0
        Me.txtaddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtaddress1.Location = New System.Drawing.Point(101, 111)
        Me.txtaddress1.Name = "txtaddress1"
        Me.txtaddress1.Size = New System.Drawing.Size(336, 21)
        Me.txtaddress1.TabIndex = 5
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(9, 114)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(86, 16)
        Me.lblAddress.TabIndex = 31
        Me.lblAddress.Text = "Address"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(101, 57)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(124, 21)
        Me.txtCode.TabIndex = 2
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(9, 59)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(81, 16)
        Me.lblCode.TabIndex = 11
        Me.lblCode.Text = "Branch Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGroup
        '
        Me.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Location = New System.Drawing.Point(101, 30)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(124, 21)
        Me.cboGroup.TabIndex = 1
        '
        'lblGroup
        '
        Me.lblGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroup.Location = New System.Drawing.Point(9, 32)
        Me.lblGroup.Name = "lblGroup"
        Me.lblGroup.Size = New System.Drawing.Size(86, 16)
        Me.lblGroup.TabIndex = 1
        Me.lblGroup.Text = "Bank Group"
        Me.lblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(469, 60)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Branch Information"
        '
        'frmBankBranch_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(469, 377)
        Me.Controls.Add(Me.PnlVendor)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBankBranch_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Bank Branch"
        Me.PnlVendor.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbBankInformation.ResumeLayout(False)
        Me.gbBankInformation.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PnlVendor As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbBankInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtcontactperson As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblcontactperson As System.Windows.Forms.Label
    Friend WithEvents txtcontactno As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblContactNo As System.Windows.Forms.Label
    Friend WithEvents lblPincode As System.Windows.Forms.Label
    Friend WithEvents lblstate As System.Windows.Forms.Label
    Friend WithEvents txtaddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtaddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroup As System.Windows.Forms.Label
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents txtname As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents cboZipcode As System.Windows.Forms.ComboBox
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents txtSortCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSortCode As System.Windows.Forms.Label
    Friend WithEvents chkClearing As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchBankGroup As eZee.Common.eZeeGradientButton
End Class
