<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonImageScanner
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonImageScanner))
        Me.pnlFooter = New System.Windows.Forms.Panel
        Me.btnAccept = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMain = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.picImage = New System.Windows.Forms.PictureBox
        Me.VSTwain1 = New Vintasoft.Twain.VSTwain
        Me.pnlFooter.SuspendLayout()
        Me.gbMain.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlFooter
        '
        Me.pnlFooter.Controls.Add(Me.btnAccept)
        Me.pnlFooter.Controls.Add(Me.btnCancel)
        Me.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlFooter.Location = New System.Drawing.Point(0, 431)
        Me.pnlFooter.Name = "pnlFooter"
        Me.pnlFooter.Size = New System.Drawing.Size(558, 66)
        Me.pnlFooter.TabIndex = 0
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.BackColor = System.Drawing.Color.White
        Me.btnAccept.BackgroundImage = CType(resources.GetObject("btnAccept.BackgroundImage"), System.Drawing.Image)
        Me.btnAccept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAccept.BorderColor = System.Drawing.Color.Empty
        Me.btnAccept.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnAccept.FlatAppearance.BorderSize = 0
        Me.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAccept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.ForeColor = System.Drawing.Color.Black
        Me.btnAccept.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAccept.GradientForeColor = System.Drawing.Color.Black
        Me.btnAccept.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAccept.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAccept.Location = New System.Drawing.Point(190, 19)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAccept.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAccept.Selected = False
        Me.btnAccept.ShowDefaultBorderColor = True
        Me.btnAccept.Size = New System.Drawing.Size(90, 30)
        Me.btnAccept.TabIndex = 2
        Me.btnAccept.Text = "&Accept"
        Me.btnAccept.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(286, 19)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Selected = False
        Me.btnCancel.ShowDefaultBorderColor = True
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'gbMain
        '
        Me.gbMain.BorderColor = System.Drawing.Color.Black
        Me.gbMain.Checked = False
        Me.gbMain.CollapseAllExceptThis = False
        Me.gbMain.CollapsedHoverImage = Nothing
        Me.gbMain.CollapsedNormalImage = Nothing
        Me.gbMain.CollapsedPressedImage = Nothing
        Me.gbMain.CollapseOnLoad = False
        Me.gbMain.Controls.Add(Me.pnlMain)
        Me.gbMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbMain.ExpandedHoverImage = Nothing
        Me.gbMain.ExpandedNormalImage = Nothing
        Me.gbMain.ExpandedPressedImage = Nothing
        Me.gbMain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMain.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMain.HeaderHeight = 25
        Me.gbMain.HeightOnCollapse = 0
        Me.gbMain.LeftTextSpace = 0
        Me.gbMain.Location = New System.Drawing.Point(0, 0)
        Me.gbMain.Name = "gbMain"
        Me.gbMain.OpenHeight = 497
        Me.gbMain.Padding = New System.Windows.Forms.Padding(3)
        Me.gbMain.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMain.ShowBorder = True
        Me.gbMain.ShowCheckBox = False
        Me.gbMain.ShowCollapseButton = False
        Me.gbMain.ShowDefaultBorderColor = True
        Me.gbMain.ShowDownButton = False
        Me.gbMain.ShowHeader = True
        Me.gbMain.Size = New System.Drawing.Size(558, 497)
        Me.gbMain.TabIndex = 1
        Me.gbMain.Temp = 0
        Me.gbMain.Text = "Scan Image"
        Me.gbMain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMain
        '
        Me.pnlMain.AutoScroll = True
        Me.pnlMain.BackColor = System.Drawing.Color.White
        Me.pnlMain.Controls.Add(Me.picImage)
        Me.pnlMain.Location = New System.Drawing.Point(2, 26)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(556, 405)
        Me.pnlMain.TabIndex = 12
        '
        'picImage
        '
        Me.picImage.Location = New System.Drawing.Point(2, 3)
        Me.picImage.Name = "picImage"
        Me.picImage.Size = New System.Drawing.Size(102, 100)
        Me.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picImage.TabIndex = 0
        Me.picImage.TabStop = False
        '
        'VSTwain1
        '
        Me.VSTwain1.AllowExceptions = True
        Me.VSTwain1.AppProductName = "VintaSoftTwain.NET"
        Me.VSTwain1.AutoCleanBuffer = True
        Me.VSTwain1.DisableAfterAcquire = False
        Me.VSTwain1.FileFormat = Vintasoft.Twain.FileFormat.Bmp
        Me.VSTwain1.FileName = "c:\test.bmp"
        Me.VSTwain1.IsTwain2Compatible = False
        Me.VSTwain1.JpegQuality = 90
        Me.VSTwain1.MaxImages = 1
        Me.VSTwain1.ModalUI = False
        Me.VSTwain1.Parent = Me
        Me.VSTwain1.PdfImageCompression = Vintasoft.Twain.PdfImageCompression.[Auto]
        Me.VSTwain1.PdfMultiPage = True
        Me.VSTwain1.ShowIndicators = True
        Me.VSTwain1.ShowUI = True
        Me.VSTwain1.TiffCompression = Vintasoft.Twain.TiffCompression.[Auto]
        Me.VSTwain1.TiffMultiPage = True
        Me.VSTwain1.TransferMode = Vintasoft.Twain.TransferMode.Memory
        Me.VSTwain1.TwainDllPath = "C:\WINDOWS\twain_32.dll"
        '
        'frmCommonImageScanner
        '
        Me.AcceptButton = Me.btnAccept
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(558, 497)
        Me.Controls.Add(Me.pnlFooter)
        Me.Controls.Add(Me.gbMain)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommonImageScanner"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Scan Image"
        Me.pnlFooter.ResumeLayout(False)
        Me.gbMain.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlFooter As System.Windows.Forms.Panel
    Friend WithEvents gbMain As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents picImage As System.Windows.Forms.PictureBox
    Friend WithEvents btnAccept As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents VSTwain1 As Vintasoft.Twain.VSTwain
End Class
