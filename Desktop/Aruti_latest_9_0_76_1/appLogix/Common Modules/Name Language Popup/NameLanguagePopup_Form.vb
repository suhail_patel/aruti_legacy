Imports eZeeCommonLib

Public Class NameLanguagePopup_Form
    Private ReadOnly mstrModuleName As String = "NameLanguagePopup_Form"

    Private mblnCancel As Boolean = True
    Private mstrName As String = ""
    Private mstrName1 As String = ""
    Private mstrName2 As String = ""

    Public Sub displayDialog(ByRef Name As String, ByRef Name1 As String, ByRef Name2 As String)
        mstrName = Name

        If Name1 = "" Then Name1 = Name
        If Name2 = "" Then Name2 = Name

        mstrName1 = Name1
        mstrName2 = Name2

        Me.ShowDialog()

        If Not mblnCancel Then
            Name = mstrName
            Name1 = mstrName1
            Name2 = mstrName2
        End If
    End Sub

    'Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    'Dim objfrm As New frmLanguage
    '    Try
    '        If User._Object._RightToLeft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        Call SetMessages()

    '        objfrm.displayDialog(Me)

    '        Call SetLanguage()

    '    Catch ex As System.Exception
    '        Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '    Finally
    '        objfrm.Dispose()
    '        objfrm = Nothing
    '    End Try
    'End Sub

    Private Sub NameLanguagePopup_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Call Language.setLanguage(Me.Name)


        'Call OtherSettings()
        Call Set_Logo(Me, gApplicationType)
        txtName.Text = mstrName
        txtName1.Text = mstrName1
        txtName2.Text = mstrName2
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        mstrName = txtName.Text
        mstrName1 = txtName1.Text
        mstrName2 = txtName2.Text
        mblnCancel = False
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbNameLanguage.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbNameLanguage.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbNameLanguage.Text = Language._Object.getCaption(Me.gbNameLanguage.Name, Me.gbNameLanguage.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblName2.Text = Language._Object.getCaption(Me.lblName2.Name, Me.lblName2.Text)
            Me.lblName1.Text = Language._Object.getCaption(Me.lblName1.Name, Me.lblName1.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class