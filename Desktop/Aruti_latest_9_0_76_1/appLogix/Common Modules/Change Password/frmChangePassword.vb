﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message index = 7


Public Class frmChangePassword

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmChangePassword"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objUserPwd As clsUserAddEdit
    Private mblnIsFromConfiguration As Boolean
    'S.SANDEEP [ 09 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsPasswdExp As Boolean = False
    Private mstrUserName As String = String.Empty
    Private mintUserId As Integer = 0
    'S.SANDEEP [ 09 NOV 2012 ] -- END
#End Region

#Region "displayDialog"

    'S.SANDEEP [ 09 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function displayDialog(ByVal _IsFromConfiguration As Boolean) As Boolean
    Public Function displayDialog(ByVal _IsFromConfiguration As Boolean, _
                                  Optional ByVal blnIsPasswdExp As Boolean = False, _
                                  Optional ByVal strUserName As String = "", _
                                  Optional ByVal intUserId As Integer = 0) As Boolean
        'S.SANDEEP [ 09 NOV 2012 ] -- END
        Try
            mblnIsFromConfiguration = _IsFromConfiguration

            'S.SANDEEP [ 09 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnIsPasswdExp = blnIsPasswdExp
            mstrUserName = strUserName
            mintUserId = intUserId
            'S.SANDEEP [ 09 NOV 2012 ] -- END

            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtOldPwd.BackColor = GUI.ColorComp
            txtNewPwd.BackColor = GUI.ColorComp
            txtConfirmPwd.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'S.SANDEEP [ 09 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'lblUserName.Text = User._Object._Username
            'objUserPwd._Userunkid = User._Object._Userunkid

            If mblnIsPasswdExp = True Then
                lblUserName.Text = mstrUserName
                objUserPwd._Userunkid = mintUserId

                'Anjan / Pinkal [11 December 2014] -- Start
                'ENHANCEMENT : language was not getting set on change password screen.
                gobjLocalization._LangId = objUserPwd._Languageunkid
                'Anjan / Pinkal [11 December 2014] -- End


            Else
                'S.SANDEEP [ 26 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If lblUserName.Text = "" And mstrUserName <> "" Then
                    lblUserName.Text = mstrUserName
                    objUserPwd._Userunkid = mintUserId
                Else
                    lblUserName.Text = User._Object._Username
                    objUserPwd._Userunkid = User._Object._Userunkid
                End If
                'S.SANDEEP [ 26 NOV 2012 ] -- END
            End If
            'S.SANDEEP [ 09 NOV 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'S.SANDEEP [ 09 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mblnIsPasswdExp = True Then
                Dim objPwdOp As New clsPassowdOptions
                If objPwdOp._IsPasswordExpiredSet Then
                    If objUserPwd._Exp_Days.ToString.Trim.Length = 8 Then
                        Dim dtExDate As Date = CDate(eZeeDate.convertDate(objUserPwd._Exp_Days.ToString))
                        dtExDate = dtExDate.AddDays(objPwdOp._PasswordMaxLen)
                        objUserPwd._Exp_Days = CInt(eZeeDate.convertDate(dtExDate))
                    End If
                    'S.SANDEEP [ 11 FEB 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Else
                    objUserPwd._Exp_Days = 0
                    'S.SANDEEP [ 11 FEB 2013 ] -- END
                End If
            End If
            'S.SANDEEP [ 09 NOV 2012 ] -- END
            objUserPwd._Password = txtConfirmPwd.Text
            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            objUserPwd._OldPassword = txtOldPwd.Text
            objUserPwd._ChangedUserunkid = User._Object._Userunkid
            objUserPwd._Ip = getIP()
            objUserPwd._Hostname = getHostName()
            objUserPwd._Isweb = False
            'Hemant (25 May 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmChangePassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            objUserPwd = New clsUserAddEdit
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 26 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            lblUserName.Text = ""
            'S.SANDEEP [ 26 NOV 2012 ] -- END

            'Anjan / Pinkal [11 December 2014] -- Start
            'ENHANCEMENT : language was not getting set on change password screen.
            GetValue()
            Language.setLanguage(Me.Name)
            'Anjan / Pinkal [11 December 2014] -- End



            Call OtherSettings()
            SetColor()

            txtOldPwd.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmChangePassword_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmChangePassword_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmChangePassword_KeyDown", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnflag As Boolean = False
        Try

            'S.SANDEEP [ 05 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If txtOldPwd.Text.Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Old Password cannot be blank. Old Password is required information."), enMsgBoxStyle.Information)
            '    txtOldPwd.Select()
            '    Exit Sub
            'End If
            'S.SANDEEP [ 05 NOV 2012 ] -- END

            If txtNewPwd.Text.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "New Password cannot be blank. New Password is required information."), enMsgBoxStyle.Information)
                txtNewPwd.Select()
                Exit Sub

            ElseIf txtConfirmPwd.Text.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Confirm Password cannot be blank. Confirm Password is required information."), enMsgBoxStyle.Information)
                txtConfirmPwd.Select()
                Exit Sub
            End If

            If objUserPwd._Password <> txtOldPwd.Text Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Old Password does not match. Please enter correct password."), enMsgBoxStyle.Information)
                txtOldPwd.Select()
                Exit Sub

            ElseIf txtNewPwd.Text <> txtConfirmPwd.Text Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "New Password does not match with confirm password."), enMsgBoxStyle.Information)
                txtConfirmPwd.Select()
                Exit Sub

            End If

            'S.SANDEEP [ 05 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If User._Object.PWDOptions._IsPasswordLenghtSet = True Then
            '    If txtNewPwd.TextLength < User._Object.PWDOptions._PasswordLength Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Password length should be greater than equal to " & User._Object.PWDOptions._PasswordLength & " ."), enMsgBoxStyle.Information)
            '        txtNewPwd.Select()
            '        Exit Sub
            '    End If
            'End If

            Dim objPswd As New clsPassowdOptions
            If objPswd._IsPasswordLenghtSet Then
                If txtNewPwd.Text.Trim.Length < objPswd._PasswordLength Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Password length cannot be less than ") & objPswd._PasswordLength & " " & Language.getMessage(mstrModuleName, 8, "character(s)"), enMsgBoxStyle.Information)
                    txtNewPwd.Focus()
                    Exit Sub
                End If
            End If

            Dim mRegx As System.Text.RegularExpressions.Regex
            If txtNewPwd.Text.Trim.Length > 0 Then
                If objPswd._IsPasswdPolicySet Then
                    If objPswd._IsUpperCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
                        If mRegx.IsMatch(txtNewPwd.Text.Trim) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Password must contain atleast one upper case character."), enMsgBoxStyle.Information)
                            txtNewPwd.Focus()
                            Exit Sub
                        End If
                    End If
                    If objPswd._IsLowerCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
                        If mRegx.IsMatch(txtNewPwd.Text.Trim) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Password must contain atleast one lower case character."), enMsgBoxStyle.Information)
                            txtNewPwd.Focus()
                            Exit Sub
                        End If
                    End If
                    If objPswd._IsNumeric_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
                        If mRegx.IsMatch(txtNewPwd.Text.Trim) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Password must contain atleast one numeric digit."), enMsgBoxStyle.Information)
                            txtNewPwd.Focus()
                            Exit Sub
                        End If
                    End If
                    If objPswd._IsSpecalChars_Mandatory Then
                        'S.SANDEEP [ 07 MAY 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'mRegx = New System.Text.RegularExpressions.Regex("^.*[\[\]^$.|?*+()\\~`!@#%&\-_+={}'&quot;&lt;&gt;:;,\ ].*$")
                        mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
                        'S.SANDEEP [ 07 MAY 2013 ] -- END
                        If mRegx.IsMatch(txtNewPwd.Text.Trim) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Password must contain atleast one special character."), enMsgBoxStyle.Information)
                            txtNewPwd.Focus()
                    Exit Sub
                End If
            End If
                End If
            End If
            'S.SANDEEP [ 05 NOV 2012 ] -- END

            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            If objPswd._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION AndAlso objPswd._IsPasswordHistory AndAlso _
               (objPswd._PasswdLastUsedNumber > 0 OrElse objPswd._PasswdLastUsedDays > 0) Then
                Dim objPasswordHistory As New clsPasswordHistory
                Dim intTopCount As Integer
                Dim strFilter As String = String.Empty
                Dim strOrderBy As String = String.Empty
                If objPswd._PasswdLastUsedNumber > 0 Then
                    intTopCount = objPswd._PasswdLastUsedNumber
                End If

                If objPswd._PasswdLastUsedDays > 0 Then
                    Dim dtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
                    Dim dtStartDate As Date = dtEndDate.AddDays(-1 * objPswd._PasswdLastUsedDays)
                    strFilter &= "AND Convert(char(8),transactiondate,112) >= '" & eZeeDate.convertDate(dtStartDate) & "' AND Convert(char(8),transactiondate,112) <= '" & eZeeDate.convertDate(dtEndDate) & "' "
                End If

                strOrderBy = " order by transactiondate desc "
                Dim dsList As DataSet = objPasswordHistory.GetFilterDataList("List", User._Object._Userunkid, -1, -1, intTopCount, strFilter, strOrderBy)
                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim drPassword() As DataRow = dsList.Tables(0).Select("new_password = '" & clsSecurity.Encrypt(txtNewPwd.Text, "ezee").ToString & "' ")
                    If drPassword.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Entered New Password is already Used. Please Enter another Password!!!"), enMsgBoxStyle.Information)
                        txtNewPwd.Focus()
                        Exit Sub
                    End If
                End If
                objPasswordHistory = Nothing
            End If
            'Hemant (25 May 2021) -- End


            SetValue()
            blnflag = objUserPwd.Update(True)

            If blnflag Then
                'S.SANDEEP [ 21 MAY 2012 ] -- START
                'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                'modGlobal.SetUserTracingInfo(mblnIsFromConfiguration, enUserMode.Password)
                modGlobal.SetUserTracingInfo(mblnIsFromConfiguration, enUserMode.Password, enLogin_Mode.DESKTOP)
                'S.SANDEEP [ 21 MAY 2012 ] -- END
            End If

            'S.SANDEEP [22-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : {#0001898}
            If blnflag Then
                objUserPwd.UpdateEmployeePassword(User._Object._Userunkid, txtConfirmPwd.Text)
            End If
            'S.SANDEEP [22-Jan-2018] -- END
            


            If blnflag = False And objUserPwd._Message <> "" Then
                eZeeMsgBox.Show(objUserPwd._Message, enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password changed successfully."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End
   
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbPasswordinfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPasswordinfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblOldPwd.Text = Language._Object.getCaption(Me.lblOldPwd.Name, Me.lblOldPwd.Text)
			Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
			Me.lblConfirmPwd.Text = Language._Object.getCaption(Me.lblConfirmPwd.Name, Me.lblConfirmPwd.Text)
			Me.lblNewPwd.Text = Language._Object.getCaption(Me.lblNewPwd.Name, Me.lblNewPwd.Text)
			Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)
			Me.gbPasswordinfo.Text = Language._Object.getCaption(Me.gbPasswordinfo.Name, Me.gbPasswordinfo.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "New Password cannot be blank. New Password is required information.")
			Language.setMessage(mstrModuleName, 3, "Confirm Password cannot be blank. Confirm Password is required information.")
			Language.setMessage(mstrModuleName, 4, "Old Password does not match. Please enter correct password.")
			Language.setMessage(mstrModuleName, 5, "New Password does not match with confirm password.")
			Language.setMessage(mstrModuleName, 6, "Password changed successfully.")
			Language.setMessage(mstrModuleName, 7, "Password length cannot be less than")
			Language.setMessage(mstrModuleName, 8, "character(s)")
			Language.setMessage(mstrModuleName, 9, "Password must contain atleast one upper case character.")
			Language.setMessage(mstrModuleName, 10, "Password must contain atleast one lower case character.")
			Language.setMessage(mstrModuleName, 11, "Password must contain atleast one numeric digit.")
			Language.setMessage(mstrModuleName, 12, "Password must contain atleast one special character.")
			Language.setMessage(mstrModuleName, 13, "Sorry, Entered New Password is already Used. Please Enter another Password!!!")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class

