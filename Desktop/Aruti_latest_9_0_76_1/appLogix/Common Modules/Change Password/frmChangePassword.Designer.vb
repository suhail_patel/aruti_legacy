﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChangePassword
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChangePassword))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbPasswordinfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblUser = New System.Windows.Forms.Label
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.txtConfirmPwd = New eZee.TextBox.AlphanumericTextBox
        Me.txtOldPwd = New eZee.TextBox.AlphanumericTextBox
        Me.txtNewPwd = New eZee.TextBox.AlphanumericTextBox
        Me.lblOldPwd = New System.Windows.Forms.Label
        Me.lblUserName = New System.Windows.Forms.Label
        Me.lblNewPwd = New System.Windows.Forms.Label
        Me.lblConfirmPwd = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbPasswordinfo.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbPasswordinfo)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(340, 214)
        Me.pnlMain.TabIndex = 0
        '
        'gbPasswordinfo
        '
        Me.gbPasswordinfo.BorderColor = System.Drawing.Color.Black
        Me.gbPasswordinfo.Checked = False
        Me.gbPasswordinfo.CollapseAllExceptThis = False
        Me.gbPasswordinfo.CollapsedHoverImage = Nothing
        Me.gbPasswordinfo.CollapsedNormalImage = Nothing
        Me.gbPasswordinfo.CollapsedPressedImage = Nothing
        Me.gbPasswordinfo.CollapseOnLoad = False
        Me.gbPasswordinfo.Controls.Add(Me.lblUser)
        Me.gbPasswordinfo.Controls.Add(Me.EZeeLine1)
        Me.gbPasswordinfo.Controls.Add(Me.txtConfirmPwd)
        Me.gbPasswordinfo.Controls.Add(Me.txtOldPwd)
        Me.gbPasswordinfo.Controls.Add(Me.txtNewPwd)
        Me.gbPasswordinfo.Controls.Add(Me.lblOldPwd)
        Me.gbPasswordinfo.Controls.Add(Me.lblUserName)
        Me.gbPasswordinfo.Controls.Add(Me.lblNewPwd)
        Me.gbPasswordinfo.Controls.Add(Me.lblConfirmPwd)
        Me.gbPasswordinfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbPasswordinfo.ExpandedHoverImage = Nothing
        Me.gbPasswordinfo.ExpandedNormalImage = Nothing
        Me.gbPasswordinfo.ExpandedPressedImage = Nothing
        Me.gbPasswordinfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPasswordinfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPasswordinfo.HeaderHeight = 25
        Me.gbPasswordinfo.HeaderMessage = ""
        Me.gbPasswordinfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPasswordinfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPasswordinfo.HeightOnCollapse = 0
        Me.gbPasswordinfo.LeftTextSpace = 0
        Me.gbPasswordinfo.Location = New System.Drawing.Point(0, 0)
        Me.gbPasswordinfo.Name = "gbPasswordinfo"
        Me.gbPasswordinfo.OpenHeight = 300
        Me.gbPasswordinfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPasswordinfo.ShowBorder = True
        Me.gbPasswordinfo.ShowCheckBox = False
        Me.gbPasswordinfo.ShowCollapseButton = False
        Me.gbPasswordinfo.ShowDefaultBorderColor = True
        Me.gbPasswordinfo.ShowDownButton = False
        Me.gbPasswordinfo.ShowHeader = True
        Me.gbPasswordinfo.Size = New System.Drawing.Size(340, 159)
        Me.gbPasswordinfo.TabIndex = 9
        Me.gbPasswordinfo.Temp = 0
        Me.gbPasswordinfo.Text = "Password Information"
        Me.gbPasswordinfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(8, 36)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(118, 17)
        Me.lblUser.TabIndex = 1
        Me.lblUser.Text = "User"
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(11, 56)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(319, 13)
        Me.EZeeLine1.TabIndex = 9
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtConfirmPwd
        '
        Me.txtConfirmPwd.Flags = 0
        Me.txtConfirmPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConfirmPwd.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(93), Global.Microsoft.VisualBasic.ChrW(91)}
        Me.txtConfirmPwd.Location = New System.Drawing.Point(150, 131)
        Me.txtConfirmPwd.Name = "txtConfirmPwd"
        Me.txtConfirmPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtConfirmPwd.Size = New System.Drawing.Size(180, 21)
        Me.txtConfirmPwd.TabIndex = 3
        '
        'txtOldPwd
        '
        Me.txtOldPwd.Flags = 0
        Me.txtOldPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOldPwd.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtOldPwd.Location = New System.Drawing.Point(150, 77)
        Me.txtOldPwd.Name = "txtOldPwd"
        Me.txtOldPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtOldPwd.Size = New System.Drawing.Size(180, 21)
        Me.txtOldPwd.TabIndex = 1
        '
        'txtNewPwd
        '
        Me.txtNewPwd.Flags = 0
        Me.txtNewPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewPwd.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(93), Global.Microsoft.VisualBasic.ChrW(91)}
        Me.txtNewPwd.Location = New System.Drawing.Point(150, 104)
        Me.txtNewPwd.Name = "txtNewPwd"
        Me.txtNewPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtNewPwd.Size = New System.Drawing.Size(180, 21)
        Me.txtNewPwd.TabIndex = 2
        '
        'lblOldPwd
        '
        Me.lblOldPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOldPwd.Location = New System.Drawing.Point(8, 79)
        Me.lblOldPwd.Name = "lblOldPwd"
        Me.lblOldPwd.Size = New System.Drawing.Size(118, 17)
        Me.lblOldPwd.TabIndex = 2
        Me.lblOldPwd.Text = "Old Password"
        '
        'lblUserName
        '
        Me.lblUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.Location = New System.Drawing.Point(150, 36)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(180, 17)
        Me.lblUserName.TabIndex = 5
        Me.lblUserName.Text = "User"
        '
        'lblNewPwd
        '
        Me.lblNewPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewPwd.Location = New System.Drawing.Point(8, 106)
        Me.lblNewPwd.Name = "lblNewPwd"
        Me.lblNewPwd.Size = New System.Drawing.Size(118, 17)
        Me.lblNewPwd.TabIndex = 3
        Me.lblNewPwd.Text = "New Password"
        '
        'lblConfirmPwd
        '
        Me.lblConfirmPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConfirmPwd.Location = New System.Drawing.Point(8, 133)
        Me.lblConfirmPwd.Name = "lblConfirmPwd"
        Me.lblConfirmPwd.Size = New System.Drawing.Size(118, 17)
        Me.lblConfirmPwd.TabIndex = 4
        Me.lblConfirmPwd.Text = "Confirm Password"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 159)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(340, 55)
        Me.EZeeFooter1.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(240, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(144, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmChangePassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(340, 214)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChangePassword"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Change Password"
        Me.pnlMain.ResumeLayout(False)
        Me.gbPasswordinfo.ResumeLayout(False)
        Me.gbPasswordinfo.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents lblOldPwd As System.Windows.Forms.Label
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents lblConfirmPwd As System.Windows.Forms.Label
    Friend WithEvents lblNewPwd As System.Windows.Forms.Label
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents txtConfirmPwd As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtNewPwd As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOldPwd As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents gbPasswordinfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
End Class
