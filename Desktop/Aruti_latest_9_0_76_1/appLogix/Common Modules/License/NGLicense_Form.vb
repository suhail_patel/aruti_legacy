Option Strict On

Imports eZeeCommonLib

Public Class frmNGLicense
    Private mobjLic As ArutiLic
    Private mdsList As New DataSet
    Dim objAppSettings As New clsApplicationSettings
    Private ReadOnly mstrModuleName As String = "frmNGLicense"

#Region " Form "

    Private Sub frmNGLicense_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control And e.KeyCode = Windows.Forms.Keys.C Then
            Call CopyToClipBoard(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmLicense_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmLicense_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLicense_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
           
            'Me.Icon = ecore32.core.FDCIcon
            'Me.Text = ecore32.core.FrontDesk & " " & Me.Text
           
            'Dim objHotel As New clsHotel(True)
            'txtPropertyName.Text = objHotel._Hotel_Name
            'objHotel = Nothing
            'txtPropertyName.Text = "eZeeAruti"

            Dim objGroupMaster As New clsGroup_Master(True)

            txtPropertyName.Text = objGroupMaster._Groupname

            Dim blnIsLic As Boolean = False
            
            
            mobjLic = New ArutiLic(blnIsLic)
            
            txtSessionID.Text = mobjLic.SessionId
            txtMachineID.Text = mobjLic.MachineId

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmLicense_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub btnActivate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActivate.Click
        Try
            If txtLicSerialNo.Enabled And txtLicSerialNo.Text <> "" Then
                Cursor.Current = Cursors.WaitCursor

                Dim strHotelName As String = ""
                'Dim objHotel As New clsHotel(True)
                'strHotelName = objHotel._Hotel_Name
                strHotelName = txtPropertyName.Text

                mobjLic.LicenseKey = txtLicSerialNo.Text.Trim
                mobjLic.RoomKey = CStr(IIf(txtNoofRoomKey.Text.Trim.Equals(""), 0, txtNoofRoomKey.Text.Trim))
                mobjLic.NoOfCompany = CStr(0)
                mobjLic.HotelName = strHotelName

                If mobjLic.GetEventCode() = 1 Then

                    Dim strRooms As Integer = mobjLic.ValidateRoomKey(txtNoofRoomKey.Text)
                    Dim strEmp As String = strRooms.ToString().Substring(strRooms.ToString().Length - 6, 6)

                    'Sohail (04 Jun 2013) -- Start
                    'TRA - ENHANCEMENT
                    'Company._Object._Total_Active_Employee_AsOnFromDate = Nothing
                    'Company._Object._Total_Active_Employee_AsOnToDate = Nothing
                    Company._Object._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    'Sohail (04 Jun 2013) -- End
                    Dim intcnt As Integer = Company._Object._Total_Active_Employee_ForAllCompany
                    If intcnt > CInt(strEmp) Then
                        eZeeMsgBox.Show("Sorry, total employee " & intcnt & " exceeds the limit of license purchased for " & CInt(strEmp) & " employees.")
                        Exit Sub
                    End If
                End If

                Cursor.Current = Cursors.Default

                If mobjLic.licenseIsValid() Then
                    eZeeMsgBox.Show("Registered Successfully.")
                Else
                    eZeeMsgBox.Show("Invalid License Serial No.")
                End If
            Else
                eZeeMsgBox.Show("Please enter License Serial No and press Activate.")
            End If

            Me.Close()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnActivate_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    '#Region " Othter Controls "
    '    Private Sub lnkEmail_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEmail.LinkClicked
    '        Try
    '            Dim strMessage As String = ""
    '            Dim objSendMail As New clsSendMail
    '            Dim strToMail As String = ""
    '            Select Case ecore32.core.iCore
    '                Case ecore32.enCore.EZEE
    '                    strToMail = "support@ezeefrontdesk.com"
    '                Case ecore32.enCore.IZZIE
    '                    strToMail = "support@Ibratro.com"
    '                Case ecore32.enCore.BLUECHAMBER
    '                    strToMail = "support@bluechamber.com"
    '                Case ecore32.enCore.FD360
    '                    strToMail = "support@angsystems.com"
    '                Case ecore32.enCore.BLUEPARROT
    '                    strToMail = "sales@blueparrot-software.co.uk"
    '            End Select
    '            objSendMail._ToEmail = strToMail
    '            objSendMail._Subject = "License Request"
    '            objSendMail._Message = "<table> " & _
    '                                    "<TR> <TD>Property Name : </TD> <TD>" & txtPropertyName.Text & "</TD></TR> " & _
    '                                    "<TR> <TD>Session ID : </TD> <TD>" & txtSessionID.Text & "</TD></TR> " & _
    '                                    "<TR> <TD>Machine ID : </TD> <TD>" & txtMachineID.Text & "</TD></TR> " & _
    '                                    "<TR> <TD>No of Rooms : </TD> <TD>" & txtNoofRooms.Text & "</TD></TR> " & _
    '                                    "</table>"
    '            strMessage = objSendMail.SendMail()
    '            If strMessage <> "" Then
    '                eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
    '            Else
    '                eZeeMsgBox.Show("Your request for license has been sent to our Sales Department. " & vbCrLf & "You will receive response from us as soon as possible. " & vbCrLf & "Thank you.", enMsgBoxStyle.Information)
    '            End If
    '        Catch ex As Exception
    '            Call DisplayError.Show("-1", ex.Message, "lnkEmail_LinkClicked", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub lnkCopyToClipBoard_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyToClipBoard.LinkClicked
    '        Try
    '            Call CopyToClipBoard()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lnkCopyToClipBoard_LinkClicked", mstrModuleName)
    '        End Try
    '    End Sub

    Private Sub CopyToClipBoard(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyToClipBoard.LinkClicked
        Try
            Dim strCopyString As String = ""
            strCopyString = "Property Name : " & txtPropertyName.Text & " " & vbCrLf & _
                            "Session ID : " & txtSessionID.Text & " " & vbCrLf & _
                            "Machine ID : " & txtMachineID.Text & " "
            My.Computer.Clipboard.SetText(strCopyString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CopyToClipBoard", mstrModuleName)
        End Try
    End Sub
    '#End Region

    '    Private Sub OtherSettings()
    '        Try
    '            Me.SuspendLayout()
    '            Select Case eZeeApplication._AppType
    '                Case enApplication.eZeePOS
    '                    Me.Icon = ecore32.core.BurrpIcon
    '                Case enApplication.eZeePOSConfigartion
    '                    Me.Icon = ecore32.core.BackOfficeIcon
    '                Case enApplication.eZeeFD
    '                    Me.Icon = ecore32.core.FDIcon
    '                Case Else
    '                    Me.Icon = ecore32.core.FDCIcon
    '            End Select
    '            Me.BackColor = GUI.FormColor

    '            Me.gbLicInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
    '            Me.gbLicInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

    '            Me.gbRegistration.GradientColor = GUI._eZeeContainerHeaderBackColor
    '            Me.gbRegistration.ForeColor = GUI._eZeeContainerHeaderForeColor


    '            Me.btnActivate.GradientBackColor = GUI._ButttonBackColor
    '            Me.btnActivate.GradientForeColor = GUI._ButttonFontColor

    '            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
    '            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

    '            Me.btnExtend.GradientBackColor = GUI._ButttonBackColor
    '            Me.btnExtend.GradientForeColor = GUI._ButttonFontColor


    '            Me.ResumeLayout()
    '        Catch Ex As Exception
    '            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
    '        End Try
    '    End Sub

 
End Class