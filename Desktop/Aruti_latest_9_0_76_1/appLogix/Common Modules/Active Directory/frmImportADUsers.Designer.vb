﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportADUsers
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportADUsers))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtSearchUser = New eZee.TextBox.AlphanumericTextBox
        Me.pnlAdUser = New System.Windows.Forms.Panel
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.dgAdUsers = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhUserName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhUname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objColhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.mnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuImportManagerUser = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportEmpUser = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.pnlAdUser.SuspendLayout()
        CType(Me.dgAdUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.mnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(384, 402)
        Me.pnlMain.TabIndex = 0
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtSearchUser)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.pnlAdUser)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearch)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(6, 7)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(371, 336)
        Me.EZeeCollapsibleContainer1.TabIndex = 68
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Import Active Directory User(s)"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSearchUser
        '
        Me.txtSearchUser.Flags = 0
        Me.txtSearchUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchUser.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchUser.Location = New System.Drawing.Point(3, 28)
        Me.txtSearchUser.Name = "txtSearchUser"
        Me.txtSearchUser.Size = New System.Drawing.Size(366, 21)
        Me.txtSearchUser.TabIndex = 125
        '
        'pnlAdUser
        '
        Me.pnlAdUser.Controls.Add(Me.objChkAll)
        Me.pnlAdUser.Controls.Add(Me.dgAdUsers)
        Me.pnlAdUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlAdUser.Location = New System.Drawing.Point(2, 51)
        Me.pnlAdUser.Name = "pnlAdUser"
        Me.pnlAdUser.Size = New System.Drawing.Size(367, 281)
        Me.pnlAdUser.TabIndex = 69
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.BackColor = System.Drawing.Color.Transparent
        Me.objChkAll.Location = New System.Drawing.Point(6, 5)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 2
        Me.objChkAll.UseVisualStyleBackColor = False
        '
        'dgAdUsers
        '
        Me.dgAdUsers.AllowUserToAddRows = False
        Me.dgAdUsers.AllowUserToDeleteRows = False
        Me.dgAdUsers.AllowUserToResizeColumns = False
        Me.dgAdUsers.AllowUserToResizeRows = False
        Me.dgAdUsers.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgAdUsers.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgAdUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgAdUsers.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.objdgcolhCheck, Me.colhUserName, Me.objcolhUname, Me.objColhIsGroup, Me.objdgcolhGrpId})
        Me.dgAdUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgAdUsers.Location = New System.Drawing.Point(0, 0)
        Me.dgAdUsers.Name = "dgAdUsers"
        Me.dgAdUsers.RowHeadersVisible = False
        Me.dgAdUsers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgAdUsers.Size = New System.Drawing.Size(367, 281)
        Me.dgAdUsers.TabIndex = 1
        '
        'objdgcolhCollapse
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.NullValue = Nothing
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gray
        Me.objdgcolhCollapse.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollapse.Visible = False
        Me.objdgcolhCollapse.Width = 25
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'colhUserName
        '
        Me.colhUserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhUserName.HeaderText = "User Name"
        Me.colhUserName.Name = "colhUserName"
        Me.colhUserName.ReadOnly = True
        Me.colhUserName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhUserName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhUname
        '
        Me.objcolhUname.HeaderText = "Uname"
        Me.objcolhUname.Name = "objcolhUname"
        Me.objcolhUname.ReadOnly = True
        Me.objcolhUname.Visible = False
        '
        'objColhIsGroup
        '
        Me.objColhIsGroup.HeaderText = "IsGroup"
        Me.objColhIsGroup.Name = "objColhIsGroup"
        Me.objColhIsGroup.ReadOnly = True
        Me.objColhIsGroup.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "GroupId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.ReadOnly = True
        Me.objdgcolhGrpId.Visible = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(346, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(23, 24)
        Me.objbtnSearch.TabIndex = 67
        Me.objbtnSearch.TabStop = False
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnOperation)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 347)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(384, 55)
        Me.EZeeFooter1.TabIndex = 0
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.ContextMenuStrip = Me.mnuOperation
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(178, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(110, 30)
        Me.btnOperation.SplitButtonMenu = Me.mnuOperation
        Me.btnOperation.TabIndex = 5
        Me.btnOperation.Text = "&Operation"
        '
        'mnuOperation
        '
        Me.mnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImportManagerUser, Me.mnuImportEmpUser})
        Me.mnuOperation.Name = "mnuOperation"
        Me.mnuOperation.Size = New System.Drawing.Size(221, 48)
        '
        'mnuImportManagerUser
        '
        Me.mnuImportManagerUser.Name = "mnuImportManagerUser"
        Me.mnuImportManagerUser.Size = New System.Drawing.Size(220, 22)
        Me.mnuImportManagerUser.Text = "Import User(s) As Manager"
        '
        'mnuImportEmpUser
        '
        Me.mnuImportEmpUser.Name = "mnuImportEmpUser"
        Me.mnuImportEmpUser.Size = New System.Drawing.Size(220, 22)
        Me.mnuImportEmpUser.Text = "Import User(s) As Employee"
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(294, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(82, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmImportADUsers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 402)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportADUsers"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Import Acive Directory User(s)"
        Me.pnlMain.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        Me.pnlAdUser.ResumeLayout(False)
        Me.pnlAdUser.PerformLayout()
        CType(Me.dgAdUsers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.mnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgAdUsers As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdUser As System.Windows.Forms.Panel
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuImportManagerUser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportEmpUser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtSearchUser As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhUserName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhUname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objColhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
End Class
