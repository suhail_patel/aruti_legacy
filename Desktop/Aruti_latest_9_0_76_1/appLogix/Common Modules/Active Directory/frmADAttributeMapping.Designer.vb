﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmADAttributeMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmADAttributeMapping))
        Me.LblLoginUserName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnMapHierarchy = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboLoginUserName = New System.Windows.Forms.ComboBox
        Me.LblPassword = New System.Windows.Forms.Label
        Me.cboPassword = New System.Windows.Forms.ComboBox
        Me.LblFirstName = New System.Windows.Forms.Label
        Me.cboFirstName = New System.Windows.Forms.ComboBox
        Me.LblMiddleName = New System.Windows.Forms.Label
        Me.cboMiddleName = New System.Windows.Forms.ComboBox
        Me.LblLastname = New System.Windows.Forms.Label
        Me.cboLastName = New System.Windows.Forms.ComboBox
        Me.LblDisplayName = New System.Windows.Forms.Label
        Me.cboDisplayName = New System.Windows.Forms.ComboBox
        Me.LblJobTitle = New System.Windows.Forms.Label
        Me.cboJobtitle = New System.Windows.Forms.ComboBox
        Me.LblDepartment = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.LblEmailAddress = New System.Windows.Forms.Label
        Me.cboEmailAddress = New System.Windows.Forms.ComboBox
        Me.LblTelephone = New System.Windows.Forms.Label
        Me.cboTelephone = New System.Windows.Forms.ComboBox
        Me.LblCity = New System.Windows.Forms.Label
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.LblState = New System.Windows.Forms.Label
        Me.cboRegion = New System.Windows.Forms.ComboBox
        Me.LblCountry = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.cboDescription = New System.Windows.Forms.ComboBox
        Me.LblDescription = New System.Windows.Forms.Label
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.nudHierarchyLevel = New System.Windows.Forms.NumericUpDown
        Me.LblHierarchyLevel = New System.Windows.Forms.Label
        Me.cboEntryPoint = New System.Windows.Forms.ComboBox
        Me.LblConsiderMainParent = New System.Windows.Forms.Label
        Me.cboOffice = New System.Windows.Forms.ComboBox
        Me.LblOffice = New System.Windows.Forms.Label
        Me.cboPOBoxNo = New System.Windows.Forms.ComboBox
        Me.LblPOBox = New System.Windows.Forms.Label
        Me.cboStreet = New System.Windows.Forms.ComboBox
        Me.LblStreet = New System.Windows.Forms.Label
        Me.cboMobile = New System.Windows.Forms.ComboBox
        Me.LblMobile = New System.Windows.Forms.Label
        Me.txtOUSearch = New eZee.TextBox.AlphanumericTextBox
        Me.lnPaymentRoundingOption = New eZee.Common.eZeeLine
        Me.dgOUMapping = New System.Windows.Forms.DataGridView
        Me.dgcolhOUName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAllocation = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.objdgcolhOUPath = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LblTransferAllocation = New System.Windows.Forms.Label
        Me.cboTransferAllocation = New System.Windows.Forms.ComboBox
        Me.objFooter.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        CType(Me.nudHierarchyLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgOUMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LblLoginUserName
        '
        Me.LblLoginUserName.Location = New System.Drawing.Point(11, 37)
        Me.LblLoginUserName.Name = "LblLoginUserName"
        Me.LblLoginUserName.Size = New System.Drawing.Size(100, 18)
        Me.LblLoginUserName.TabIndex = 0
        Me.LblLoginUserName.Text = "Login User Name"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnMapHierarchy)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 585)
        Me.objFooter.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(754, 59)
        Me.objFooter.TabIndex = 7
        '
        'btnMapHierarchy
        '
        Me.btnMapHierarchy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMapHierarchy.BackColor = System.Drawing.Color.White
        Me.btnMapHierarchy.BackgroundImage = CType(resources.GetObject("btnMapHierarchy.BackgroundImage"), System.Drawing.Image)
        Me.btnMapHierarchy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMapHierarchy.BorderColor = System.Drawing.Color.Empty
        Me.btnMapHierarchy.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnMapHierarchy.FlatAppearance.BorderSize = 0
        Me.btnMapHierarchy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMapHierarchy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMapHierarchy.ForeColor = System.Drawing.Color.Black
        Me.btnMapHierarchy.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMapHierarchy.GradientForeColor = System.Drawing.Color.Black
        Me.btnMapHierarchy.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMapHierarchy.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMapHierarchy.Location = New System.Drawing.Point(11, 16)
        Me.btnMapHierarchy.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnMapHierarchy.Name = "btnMapHierarchy"
        Me.btnMapHierarchy.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMapHierarchy.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMapHierarchy.Size = New System.Drawing.Size(105, 30)
        Me.btnMapHierarchy.TabIndex = 131
        Me.btnMapHierarchy.Text = "Map Hierarchy"
        Me.btnMapHierarchy.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(542, 15)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 130
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(645, 15)
        Me.btnClose.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 129
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cboLoginUserName
        '
        Me.cboLoginUserName.BackColor = System.Drawing.Color.White
        Me.cboLoginUserName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboLoginUserName.Enabled = False
        Me.cboLoginUserName.FormattingEnabled = True
        Me.cboLoginUserName.Location = New System.Drawing.Point(118, 36)
        Me.cboLoginUserName.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboLoginUserName.Name = "cboLoginUserName"
        Me.cboLoginUserName.Size = New System.Drawing.Size(232, 21)
        Me.cboLoginUserName.TabIndex = 8
        '
        'LblPassword
        '
        Me.LblPassword.Location = New System.Drawing.Point(11, 63)
        Me.LblPassword.Name = "LblPassword"
        Me.LblPassword.Size = New System.Drawing.Size(100, 18)
        Me.LblPassword.TabIndex = 9
        Me.LblPassword.Text = "Password"
        '
        'cboPassword
        '
        Me.cboPassword.BackColor = System.Drawing.Color.White
        Me.cboPassword.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboPassword.Enabled = False
        Me.cboPassword.FormattingEnabled = True
        Me.cboPassword.Location = New System.Drawing.Point(118, 62)
        Me.cboPassword.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboPassword.Name = "cboPassword"
        Me.cboPassword.Size = New System.Drawing.Size(232, 21)
        Me.cboPassword.TabIndex = 10
        '
        'LblFirstName
        '
        Me.LblFirstName.Location = New System.Drawing.Point(11, 89)
        Me.LblFirstName.Name = "LblFirstName"
        Me.LblFirstName.Size = New System.Drawing.Size(100, 18)
        Me.LblFirstName.TabIndex = 11
        Me.LblFirstName.Text = "FirstName"
        '
        'cboFirstName
        '
        Me.cboFirstName.BackColor = System.Drawing.Color.White
        Me.cboFirstName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboFirstName.Enabled = False
        Me.cboFirstName.FormattingEnabled = True
        Me.cboFirstName.Location = New System.Drawing.Point(118, 88)
        Me.cboFirstName.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboFirstName.Name = "cboFirstName"
        Me.cboFirstName.Size = New System.Drawing.Size(232, 21)
        Me.cboFirstName.TabIndex = 12
        '
        'LblMiddleName
        '
        Me.LblMiddleName.Location = New System.Drawing.Point(11, 115)
        Me.LblMiddleName.Name = "LblMiddleName"
        Me.LblMiddleName.Size = New System.Drawing.Size(100, 18)
        Me.LblMiddleName.TabIndex = 13
        Me.LblMiddleName.Text = "MiddleName"
        '
        'cboMiddleName
        '
        Me.cboMiddleName.BackColor = System.Drawing.Color.White
        Me.cboMiddleName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboMiddleName.Enabled = False
        Me.cboMiddleName.FormattingEnabled = True
        Me.cboMiddleName.Location = New System.Drawing.Point(118, 114)
        Me.cboMiddleName.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboMiddleName.Name = "cboMiddleName"
        Me.cboMiddleName.Size = New System.Drawing.Size(232, 21)
        Me.cboMiddleName.TabIndex = 14
        '
        'LblLastname
        '
        Me.LblLastname.Location = New System.Drawing.Point(11, 141)
        Me.LblLastname.Name = "LblLastname"
        Me.LblLastname.Size = New System.Drawing.Size(100, 18)
        Me.LblLastname.TabIndex = 15
        Me.LblLastname.Text = "LastName"
        '
        'cboLastName
        '
        Me.cboLastName.BackColor = System.Drawing.Color.White
        Me.cboLastName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboLastName.Enabled = False
        Me.cboLastName.FormattingEnabled = True
        Me.cboLastName.Location = New System.Drawing.Point(118, 140)
        Me.cboLastName.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboLastName.Name = "cboLastName"
        Me.cboLastName.Size = New System.Drawing.Size(232, 21)
        Me.cboLastName.TabIndex = 16
        '
        'LblDisplayName
        '
        Me.LblDisplayName.Location = New System.Drawing.Point(11, 167)
        Me.LblDisplayName.Name = "LblDisplayName"
        Me.LblDisplayName.Size = New System.Drawing.Size(100, 18)
        Me.LblDisplayName.TabIndex = 17
        Me.LblDisplayName.Text = "Display Name"
        '
        'cboDisplayName
        '
        Me.cboDisplayName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplayName.FormattingEnabled = True
        Me.cboDisplayName.Location = New System.Drawing.Point(118, 166)
        Me.cboDisplayName.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboDisplayName.Name = "cboDisplayName"
        Me.cboDisplayName.Size = New System.Drawing.Size(232, 21)
        Me.cboDisplayName.TabIndex = 18
        '
        'LblJobTitle
        '
        Me.LblJobTitle.Location = New System.Drawing.Point(11, 193)
        Me.LblJobTitle.Name = "LblJobTitle"
        Me.LblJobTitle.Size = New System.Drawing.Size(100, 18)
        Me.LblJobTitle.TabIndex = 19
        Me.LblJobTitle.Text = "Job Title"
        '
        'cboJobtitle
        '
        Me.cboJobtitle.BackColor = System.Drawing.Color.White
        Me.cboJobtitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboJobtitle.Enabled = False
        Me.cboJobtitle.FormattingEnabled = True
        Me.cboJobtitle.Location = New System.Drawing.Point(118, 192)
        Me.cboJobtitle.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboJobtitle.Name = "cboJobtitle"
        Me.cboJobtitle.Size = New System.Drawing.Size(232, 21)
        Me.cboJobtitle.TabIndex = 20
        '
        'LblDepartment
        '
        Me.LblDepartment.Location = New System.Drawing.Point(11, 219)
        Me.LblDepartment.Name = "LblDepartment"
        Me.LblDepartment.Size = New System.Drawing.Size(100, 18)
        Me.LblDepartment.TabIndex = 21
        Me.LblDepartment.Text = "Department"
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(118, 218)
        Me.cboDepartment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(232, 21)
        Me.cboDepartment.TabIndex = 22
        '
        'LblEmailAddress
        '
        Me.LblEmailAddress.Location = New System.Drawing.Point(11, 245)
        Me.LblEmailAddress.Name = "LblEmailAddress"
        Me.LblEmailAddress.Size = New System.Drawing.Size(100, 18)
        Me.LblEmailAddress.TabIndex = 23
        Me.LblEmailAddress.Text = "Email Address"
        '
        'cboEmailAddress
        '
        Me.cboEmailAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmailAddress.FormattingEnabled = True
        Me.cboEmailAddress.Location = New System.Drawing.Point(118, 244)
        Me.cboEmailAddress.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboEmailAddress.Name = "cboEmailAddress"
        Me.cboEmailAddress.Size = New System.Drawing.Size(232, 21)
        Me.cboEmailAddress.TabIndex = 24
        '
        'LblTelephone
        '
        Me.LblTelephone.Location = New System.Drawing.Point(11, 271)
        Me.LblTelephone.Name = "LblTelephone"
        Me.LblTelephone.Size = New System.Drawing.Size(100, 18)
        Me.LblTelephone.TabIndex = 25
        Me.LblTelephone.Text = "Telephone"
        '
        'cboTelephone
        '
        Me.cboTelephone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTelephone.FormattingEnabled = True
        Me.cboTelephone.Location = New System.Drawing.Point(118, 270)
        Me.cboTelephone.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboTelephone.Name = "cboTelephone"
        Me.cboTelephone.Size = New System.Drawing.Size(232, 21)
        Me.cboTelephone.TabIndex = 26
        '
        'LblCity
        '
        Me.LblCity.Location = New System.Drawing.Point(11, 401)
        Me.LblCity.Name = "LblCity"
        Me.LblCity.Size = New System.Drawing.Size(100, 18)
        Me.LblCity.TabIndex = 27
        Me.LblCity.Text = "City/Town"
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(118, 400)
        Me.cboCity.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(232, 21)
        Me.cboCity.TabIndex = 28
        '
        'LblState
        '
        Me.LblState.Location = New System.Drawing.Point(11, 427)
        Me.LblState.Name = "LblState"
        Me.LblState.Size = New System.Drawing.Size(100, 18)
        Me.LblState.TabIndex = 29
        Me.LblState.Text = "State"
        '
        'cboRegion
        '
        Me.cboRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRegion.FormattingEnabled = True
        Me.cboRegion.Location = New System.Drawing.Point(118, 426)
        Me.cboRegion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboRegion.Name = "cboRegion"
        Me.cboRegion.Size = New System.Drawing.Size(232, 21)
        Me.cboRegion.TabIndex = 30
        '
        'LblCountry
        '
        Me.LblCountry.Location = New System.Drawing.Point(11, 453)
        Me.LblCountry.Name = "LblCountry"
        Me.LblCountry.Size = New System.Drawing.Size(100, 18)
        Me.LblCountry.TabIndex = 31
        Me.LblCountry.Text = "Country"
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(118, 452)
        Me.cboCountry.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(232, 21)
        Me.cboCountry.TabIndex = 32
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.cboTransferAllocation)
        Me.pnlMain.Controls.Add(Me.LblTransferAllocation)
        Me.pnlMain.Controls.Add(Me.cboDescription)
        Me.pnlMain.Controls.Add(Me.LblDescription)
        Me.pnlMain.Controls.Add(Me.cboCompany)
        Me.pnlMain.Controls.Add(Me.lblCompany)
        Me.pnlMain.Controls.Add(Me.nudHierarchyLevel)
        Me.pnlMain.Controls.Add(Me.LblHierarchyLevel)
        Me.pnlMain.Controls.Add(Me.cboEntryPoint)
        Me.pnlMain.Controls.Add(Me.LblConsiderMainParent)
        Me.pnlMain.Controls.Add(Me.cboOffice)
        Me.pnlMain.Controls.Add(Me.LblOffice)
        Me.pnlMain.Controls.Add(Me.cboPOBoxNo)
        Me.pnlMain.Controls.Add(Me.LblPOBox)
        Me.pnlMain.Controls.Add(Me.cboStreet)
        Me.pnlMain.Controls.Add(Me.LblStreet)
        Me.pnlMain.Controls.Add(Me.cboMobile)
        Me.pnlMain.Controls.Add(Me.LblMobile)
        Me.pnlMain.Controls.Add(Me.txtOUSearch)
        Me.pnlMain.Controls.Add(Me.lnPaymentRoundingOption)
        Me.pnlMain.Controls.Add(Me.dgOUMapping)
        Me.pnlMain.Controls.Add(Me.cboCountry)
        Me.pnlMain.Controls.Add(Me.LblCountry)
        Me.pnlMain.Controls.Add(Me.cboRegion)
        Me.pnlMain.Controls.Add(Me.LblState)
        Me.pnlMain.Controls.Add(Me.cboCity)
        Me.pnlMain.Controls.Add(Me.LblCity)
        Me.pnlMain.Controls.Add(Me.cboTelephone)
        Me.pnlMain.Controls.Add(Me.LblTelephone)
        Me.pnlMain.Controls.Add(Me.cboEmailAddress)
        Me.pnlMain.Controls.Add(Me.LblEmailAddress)
        Me.pnlMain.Controls.Add(Me.cboDepartment)
        Me.pnlMain.Controls.Add(Me.LblDepartment)
        Me.pnlMain.Controls.Add(Me.cboJobtitle)
        Me.pnlMain.Controls.Add(Me.LblJobTitle)
        Me.pnlMain.Controls.Add(Me.cboDisplayName)
        Me.pnlMain.Controls.Add(Me.LblDisplayName)
        Me.pnlMain.Controls.Add(Me.cboLastName)
        Me.pnlMain.Controls.Add(Me.LblLastname)
        Me.pnlMain.Controls.Add(Me.cboMiddleName)
        Me.pnlMain.Controls.Add(Me.LblMiddleName)
        Me.pnlMain.Controls.Add(Me.cboFirstName)
        Me.pnlMain.Controls.Add(Me.LblFirstName)
        Me.pnlMain.Controls.Add(Me.cboPassword)
        Me.pnlMain.Controls.Add(Me.LblPassword)
        Me.pnlMain.Controls.Add(Me.cboLoginUserName)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.LblLoginUserName)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(754, 644)
        Me.pnlMain.TabIndex = 0
        '
        'cboDescription
        '
        Me.cboDescription.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDescription.FormattingEnabled = True
        Me.cboDescription.Location = New System.Drawing.Point(117, 478)
        Me.cboDescription.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboDescription.Name = "cboDescription"
        Me.cboDescription.Size = New System.Drawing.Size(232, 21)
        Me.cboDescription.TabIndex = 148
        '
        'LblDescription
        '
        Me.LblDescription.Location = New System.Drawing.Point(11, 479)
        Me.LblDescription.Name = "LblDescription"
        Me.LblDescription.Size = New System.Drawing.Size(100, 18)
        Me.LblDescription.TabIndex = 147
        Me.LblDescription.Text = "Description"
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.DropDownWidth = 250
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(118, 9)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(232, 21)
        Me.cboCompany.TabIndex = 146
        '
        'lblCompany
        '
        Me.lblCompany.BackColor = System.Drawing.Color.Transparent
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(11, 10)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(100, 18)
        Me.lblCompany.TabIndex = 145
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudHierarchyLevel
        '
        Me.nudHierarchyLevel.Location = New System.Drawing.Point(118, 556)
        Me.nudHierarchyLevel.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudHierarchyLevel.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudHierarchyLevel.Name = "nudHierarchyLevel"
        Me.nudHierarchyLevel.Size = New System.Drawing.Size(58, 20)
        Me.nudHierarchyLevel.TabIndex = 144
        Me.nudHierarchyLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudHierarchyLevel.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'LblHierarchyLevel
        '
        Me.LblHierarchyLevel.Location = New System.Drawing.Point(11, 558)
        Me.LblHierarchyLevel.Name = "LblHierarchyLevel"
        Me.LblHierarchyLevel.Size = New System.Drawing.Size(100, 18)
        Me.LblHierarchyLevel.TabIndex = 143
        Me.LblHierarchyLevel.Text = "Hierarchy Level"
        '
        'cboEntryPoint
        '
        Me.cboEntryPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEntryPoint.FormattingEnabled = True
        Me.cboEntryPoint.Location = New System.Drawing.Point(118, 530)
        Me.cboEntryPoint.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboEntryPoint.Name = "cboEntryPoint"
        Me.cboEntryPoint.Size = New System.Drawing.Size(232, 21)
        Me.cboEntryPoint.TabIndex = 142
        '
        'LblConsiderMainParent
        '
        Me.LblConsiderMainParent.Location = New System.Drawing.Point(11, 533)
        Me.LblConsiderMainParent.Name = "LblConsiderMainParent"
        Me.LblConsiderMainParent.Size = New System.Drawing.Size(100, 18)
        Me.LblConsiderMainParent.TabIndex = 141
        Me.LblConsiderMainParent.Text = "Entry Point"
        '
        'cboOffice
        '
        Me.cboOffice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboOffice.Enabled = False
        Me.cboOffice.FormattingEnabled = True
        Me.cboOffice.Location = New System.Drawing.Point(118, 322)
        Me.cboOffice.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboOffice.Name = "cboOffice"
        Me.cboOffice.Size = New System.Drawing.Size(232, 21)
        Me.cboOffice.TabIndex = 140
        '
        'LblOffice
        '
        Me.LblOffice.Location = New System.Drawing.Point(11, 323)
        Me.LblOffice.Name = "LblOffice"
        Me.LblOffice.Size = New System.Drawing.Size(100, 18)
        Me.LblOffice.TabIndex = 139
        Me.LblOffice.Text = "Office"
        '
        'cboPOBoxNo
        '
        Me.cboPOBoxNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPOBoxNo.FormattingEnabled = True
        Me.cboPOBoxNo.Location = New System.Drawing.Point(118, 374)
        Me.cboPOBoxNo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboPOBoxNo.Name = "cboPOBoxNo"
        Me.cboPOBoxNo.Size = New System.Drawing.Size(232, 21)
        Me.cboPOBoxNo.TabIndex = 138
        '
        'LblPOBox
        '
        Me.LblPOBox.Location = New System.Drawing.Point(11, 375)
        Me.LblPOBox.Name = "LblPOBox"
        Me.LblPOBox.Size = New System.Drawing.Size(100, 18)
        Me.LblPOBox.TabIndex = 137
        Me.LblPOBox.Text = "P.O Box"
        '
        'cboStreet
        '
        Me.cboStreet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStreet.FormattingEnabled = True
        Me.cboStreet.Location = New System.Drawing.Point(118, 348)
        Me.cboStreet.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboStreet.Name = "cboStreet"
        Me.cboStreet.Size = New System.Drawing.Size(232, 21)
        Me.cboStreet.TabIndex = 136
        '
        'LblStreet
        '
        Me.LblStreet.Location = New System.Drawing.Point(11, 349)
        Me.LblStreet.Name = "LblStreet"
        Me.LblStreet.Size = New System.Drawing.Size(100, 18)
        Me.LblStreet.TabIndex = 135
        Me.LblStreet.Text = "Street"
        '
        'cboMobile
        '
        Me.cboMobile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMobile.FormattingEnabled = True
        Me.cboMobile.Location = New System.Drawing.Point(118, 296)
        Me.cboMobile.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboMobile.Name = "cboMobile"
        Me.cboMobile.Size = New System.Drawing.Size(232, 21)
        Me.cboMobile.TabIndex = 134
        '
        'LblMobile
        '
        Me.LblMobile.Location = New System.Drawing.Point(11, 297)
        Me.LblMobile.Name = "LblMobile"
        Me.LblMobile.Size = New System.Drawing.Size(100, 18)
        Me.LblMobile.TabIndex = 133
        Me.LblMobile.Text = "Mobile"
        '
        'txtOUSearch
        '
        Me.txtOUSearch.Flags = 0
        Me.txtOUSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtOUSearch.Location = New System.Drawing.Point(359, 24)
        Me.txtOUSearch.Name = "txtOUSearch"
        Me.txtOUSearch.Size = New System.Drawing.Size(387, 20)
        Me.txtOUSearch.TabIndex = 132
        '
        'lnPaymentRoundingOption
        '
        Me.lnPaymentRoundingOption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnPaymentRoundingOption.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnPaymentRoundingOption.Location = New System.Drawing.Point(358, 8)
        Me.lnPaymentRoundingOption.Name = "lnPaymentRoundingOption"
        Me.lnPaymentRoundingOption.Size = New System.Drawing.Size(390, 14)
        Me.lnPaymentRoundingOption.TabIndex = 131
        Me.lnPaymentRoundingOption.Text = "Organization Unit Mapping With Allocations"
        Me.lnPaymentRoundingOption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgOUMapping
        '
        Me.dgOUMapping.AllowUserToAddRows = False
        Me.dgOUMapping.AllowUserToDeleteRows = False
        Me.dgOUMapping.AllowUserToResizeRows = False
        Me.dgOUMapping.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgOUMapping.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgOUMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgOUMapping.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhOUName, Me.dgcolhAllocation, Me.objdgcolhOUPath})
        Me.dgOUMapping.Location = New System.Drawing.Point(360, 47)
        Me.dgOUMapping.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dgOUMapping.Name = "dgOUMapping"
        Me.dgOUMapping.RowHeadersVisible = False
        Me.dgOUMapping.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgOUMapping.Size = New System.Drawing.Size(387, 532)
        Me.dgOUMapping.TabIndex = 33
        '
        'dgcolhOUName
        '
        Me.dgcolhOUName.Frozen = True
        Me.dgcolhOUName.HeaderText = "Organization Unit (O.U)"
        Me.dgcolhOUName.Name = "dgcolhOUName"
        Me.dgcolhOUName.ReadOnly = True
        Me.dgcolhOUName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOUName.Width = 185
        '
        'dgcolhAllocation
        '
        Me.dgcolhAllocation.HeaderText = "Allocations"
        Me.dgcolhAllocation.Name = "dgcolhAllocation"
        Me.dgcolhAllocation.Width = 180
        '
        'objdgcolhOUPath
        '
        Me.objdgcolhOUPath.HeaderText = "Path"
        Me.objdgcolhOUPath.Name = "objdgcolhOUPath"
        Me.objdgcolhOUPath.ReadOnly = True
        Me.objdgcolhOUPath.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhOUPath.Width = 500
        '
        'LblTransferAllocation
        '
        Me.LblTransferAllocation.Location = New System.Drawing.Point(11, 505)
        Me.LblTransferAllocation.Name = "LblTransferAllocation"
        Me.LblTransferAllocation.Size = New System.Drawing.Size(100, 18)
        Me.LblTransferAllocation.TabIndex = 149
        Me.LblTransferAllocation.Text = "Transfer Allocation"
        '
        'cboTransferAllocation
        '
        Me.cboTransferAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransferAllocation.FormattingEnabled = True
        Me.cboTransferAllocation.Location = New System.Drawing.Point(117, 504)
        Me.cboTransferAllocation.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboTransferAllocation.Name = "cboTransferAllocation"
        Me.cboTransferAllocation.Size = New System.Drawing.Size(232, 21)
        Me.cboTransferAllocation.TabIndex = 150
        '
        'frmADAttributeMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 644)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmADAttributeMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Active Directory Attribute Mapping"
        Me.objFooter.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.nudHierarchyLevel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgOUMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblLoginUserName As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboLoginUserName As System.Windows.Forms.ComboBox
    Friend WithEvents LblPassword As System.Windows.Forms.Label
    Friend WithEvents cboPassword As System.Windows.Forms.ComboBox
    Friend WithEvents LblFirstName As System.Windows.Forms.Label
    Friend WithEvents cboFirstName As System.Windows.Forms.ComboBox
    Friend WithEvents LblMiddleName As System.Windows.Forms.Label
    Friend WithEvents cboMiddleName As System.Windows.Forms.ComboBox
    Friend WithEvents LblLastname As System.Windows.Forms.Label
    Friend WithEvents cboLastName As System.Windows.Forms.ComboBox
    Friend WithEvents LblDisplayName As System.Windows.Forms.Label
    Friend WithEvents cboDisplayName As System.Windows.Forms.ComboBox
    Friend WithEvents LblJobTitle As System.Windows.Forms.Label
    Friend WithEvents cboJobtitle As System.Windows.Forms.ComboBox
    Friend WithEvents LblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents LblEmailAddress As System.Windows.Forms.Label
    Friend WithEvents cboEmailAddress As System.Windows.Forms.ComboBox
    Friend WithEvents LblTelephone As System.Windows.Forms.Label
    Friend WithEvents cboTelephone As System.Windows.Forms.ComboBox
    Friend WithEvents LblCity As System.Windows.Forms.Label
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents LblState As System.Windows.Forms.Label
    Friend WithEvents cboRegion As System.Windows.Forms.ComboBox
    Friend WithEvents LblCountry As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents dgOUMapping As System.Windows.Forms.DataGridView
    Friend WithEvents lnPaymentRoundingOption As eZee.Common.eZeeLine
    Friend WithEvents txtOUSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboMobile As System.Windows.Forms.ComboBox
    Friend WithEvents LblMobile As System.Windows.Forms.Label
    Friend WithEvents cboPOBoxNo As System.Windows.Forms.ComboBox
    Friend WithEvents LblPOBox As System.Windows.Forms.Label
    Friend WithEvents cboStreet As System.Windows.Forms.ComboBox
    Friend WithEvents LblStreet As System.Windows.Forms.Label
    Friend WithEvents cboOffice As System.Windows.Forms.ComboBox
    Friend WithEvents LblOffice As System.Windows.Forms.Label
    Friend WithEvents LblConsiderMainParent As System.Windows.Forms.Label
    Friend WithEvents cboEntryPoint As System.Windows.Forms.ComboBox
    Friend WithEvents LblHierarchyLevel As System.Windows.Forms.Label
    Friend WithEvents nudHierarchyLevel As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnMapHierarchy As eZee.Common.eZeeLightButton
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents LblDescription As System.Windows.Forms.Label
    Friend WithEvents cboDescription As System.Windows.Forms.ComboBox
    Friend WithEvents dgcolhOUName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAllocation As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objdgcolhOUPath As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboTransferAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents LblTransferAllocation As System.Windows.Forms.Label
End Class
