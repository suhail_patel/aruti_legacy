﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 4

Public Class frmCity_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmCity_AddEdit"
    Private mblnCancel As Boolean = True
    Private objCityMaster As clscity_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintCityMasterUnkid As Integer = -1


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintCityMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintCityMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmCity_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCityMaster = New clscity_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            setColor()
            If menAction = enAction.EDIT_ONE Then
                objCityMaster._Cityunkid = mintCityMasterUnkid
            End If
            FillCombo()
            GetValue()
            cboCountry.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCity_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCity_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCity_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCity_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCity_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCity_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCityMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clscity_master.SetMessages()
            objfrm._Other_ModuleNames = "clscity_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboCountry.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Country is compulsory information.Please Select Country."), enMsgBoxStyle.Information)
                cboCountry.Focus()
                Exit Sub
            ElseIf CInt(cboState.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "State is compulsory information.Please Select State."), enMsgBoxStyle.Information)
                cboState.Focus()
                Exit Sub
                'ElseIf Trim(txtCityCode.Text) = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "City Code cannot be blank. City Code is required information."), enMsgBoxStyle.Information)
                '    txtCityCode.Focus()
                '    Exit Sub
            ElseIf Trim(txtCityName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "City Name cannot be blank. City Name is required information."), enMsgBoxStyle.Information)
                txtCityName.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objCityMaster.Update()
            Else
                blnFlag = objCityMaster.Insert()
            End If

            If blnFlag = False And objCityMaster._Message <> "" Then
                eZeeMsgBox.Show(objCityMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objCityMaster = Nothing
                    objCityMaster = New clscity_master
                    Call GetValue()
                    cboCountry.Select()
                Else
                    mintCityMasterUnkid = objCityMaster._Cityunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddState_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddState.Click
        Try
            Dim objFrmStatemaster As New frmState_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrmStatemaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmStatemaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmStatemaster)
            End If
            'Anjan (02 Sep 2011)-End 

            If objFrmStatemaster.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillState()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddState_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'Anjan (02 Sep 2011)-End 
            Call objFrmLangPopup.displayDialog(txtCityName.Text, objCityMaster._Name1, objCityMaster._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


#End Region

#Region "Dropdown Event"

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            FillState()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboCountry.BackColor = GUI.ColorComp
            cboState.BackColor = GUI.ColorComp
            txtCityCode.BackColor = GUI.ColorOptional
            txtCityName.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCityCode.Text = objCityMaster._Code
            txtCityName.Text = objCityMaster._Name
            cboCountry.SelectedValue = objCityMaster._Countryunkid
            cboState.SelectedValue = objCityMaster._Stateunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objCityMaster._Code = txtCityCode.Text.Trim
            objCityMaster._Name = txtCityName.Text.Trim
            objCityMaster._Countryunkid = CInt(cboCountry.SelectedValue)
            objCityMaster._Stateunkid = CInt(cboState.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMasterdata As New clsMasterData
            Dim dsCountry As DataSet = objMasterdata.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsCountry.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillState()
        Dim dsState As DataSet = Nothing
        Try
            Dim objStatemaster As New clsstate_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsState = objStatemaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            Else
                dsState = objStatemaster.GetList("State", True, True)
            End If
            cboState.ValueMember = "stateunkid"
            cboState.DisplayMember = "name"
            cboState.DataSource = dsState.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillState", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            objbtnAddState.Enabled = User._Object.Privilege._AddState

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbCity.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCity.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbCity.Text = Language._Object.getCaption(Me.gbCity.Name, Me.gbCity.Text)
			Me.lblCityName.Text = Language._Object.getCaption(Me.lblCityName.Name, Me.lblCityName.Text)
			Me.lblCityCode.Text = Language._Object.getCaption(Me.lblCityCode.Name, Me.lblCityCode.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Country is compulsory information.Please Select Country.")
			Language.setMessage(mstrModuleName, 2, "State is compulsory information.Please Select State.")
			Language.setMessage(mstrModuleName, 3, "City Name cannot be blank. City Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class