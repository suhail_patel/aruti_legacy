﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReminder_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReminder_AddEdit))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbReminderDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.gbUser = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtSearchUser = New eZee.TextBox.AlphanumericTextBox
        Me.pnlUserList = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvUserList = New System.Windows.Forms.DataGridView
        Me.objbtnAddType = New eZee.Common.eZeeGradientButton
        Me.lvUserList = New System.Windows.Forms.ListView
        Me.colhDeskClerk = New System.Windows.Forms.ColumnHeader
        Me.lblDeskClerk = New System.Windows.Forms.Label
        Me.objlblOption = New System.Windows.Forms.Label
        Me.nudRecurr = New System.Windows.Forms.NumericUpDown
        Me.dtRecurringDate = New System.Windows.Forms.DateTimePicker
        Me.chkStopRecurring = New System.Windows.Forms.CheckBox
        Me.cboInterval = New System.Windows.Forms.ComboBox
        Me.lblInterval = New System.Windows.Forms.Label
        Me.objlblRecurring = New System.Windows.Forms.Label
        Me.lblIntervatnadRecurr = New System.Windows.Forms.Label
        Me.txtMessage = New eZee.TextBox.AlphanumericTextBox
        Me.lblMessage = New System.Windows.Forms.Label
        Me.cboPriority = New System.Windows.Forms.ComboBox
        Me.lblPriority = New System.Windows.Forms.Label
        Me.cboType = New System.Windows.Forms.ComboBox
        Me.lblType = New System.Windows.Forms.Label
        Me.dtpStartTime = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhUserUnkId = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhUserName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFirstName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLastName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbReminderDetails.SuspendLayout()
        Me.gbUser.SuspendLayout()
        Me.pnlUserList.SuspendLayout()
        CType(Me.dgvUserList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudRecurr, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbReminderDetails
        '
        Me.gbReminderDetails.BorderColor = System.Drawing.Color.Black
        Me.gbReminderDetails.Checked = False
        Me.gbReminderDetails.CollapseAllExceptThis = False
        Me.gbReminderDetails.CollapsedHoverImage = Nothing
        Me.gbReminderDetails.CollapsedNormalImage = Nothing
        Me.gbReminderDetails.CollapsedPressedImage = Nothing
        Me.gbReminderDetails.CollapseOnLoad = False
        Me.gbReminderDetails.Controls.Add(Me.gbUser)
        Me.gbReminderDetails.Controls.Add(Me.objbtnAddType)
        Me.gbReminderDetails.Controls.Add(Me.lvUserList)
        Me.gbReminderDetails.Controls.Add(Me.lblDeskClerk)
        Me.gbReminderDetails.Controls.Add(Me.objlblOption)
        Me.gbReminderDetails.Controls.Add(Me.nudRecurr)
        Me.gbReminderDetails.Controls.Add(Me.dtRecurringDate)
        Me.gbReminderDetails.Controls.Add(Me.chkStopRecurring)
        Me.gbReminderDetails.Controls.Add(Me.cboInterval)
        Me.gbReminderDetails.Controls.Add(Me.lblInterval)
        Me.gbReminderDetails.Controls.Add(Me.objlblRecurring)
        Me.gbReminderDetails.Controls.Add(Me.lblIntervatnadRecurr)
        Me.gbReminderDetails.Controls.Add(Me.txtMessage)
        Me.gbReminderDetails.Controls.Add(Me.lblMessage)
        Me.gbReminderDetails.Controls.Add(Me.cboPriority)
        Me.gbReminderDetails.Controls.Add(Me.lblPriority)
        Me.gbReminderDetails.Controls.Add(Me.cboType)
        Me.gbReminderDetails.Controls.Add(Me.lblType)
        Me.gbReminderDetails.Controls.Add(Me.dtpStartTime)
        Me.gbReminderDetails.Controls.Add(Me.dtpStartDate)
        Me.gbReminderDetails.Controls.Add(Me.lblStartDate)
        Me.gbReminderDetails.Controls.Add(Me.objLine1)
        Me.gbReminderDetails.Controls.Add(Me.txtName)
        Me.gbReminderDetails.Controls.Add(Me.lblName)
        Me.gbReminderDetails.ExpandedHoverImage = Nothing
        Me.gbReminderDetails.ExpandedNormalImage = Nothing
        Me.gbReminderDetails.ExpandedPressedImage = Nothing
        Me.gbReminderDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbReminderDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbReminderDetails.HeaderHeight = 25
        Me.gbReminderDetails.HeaderMessage = ""
        Me.gbReminderDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbReminderDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbReminderDetails.HeightOnCollapse = 0
        Me.gbReminderDetails.LeftTextSpace = 0
        Me.gbReminderDetails.Location = New System.Drawing.Point(13, 13)
        Me.gbReminderDetails.Name = "gbReminderDetails"
        Me.gbReminderDetails.OpenHeight = 300
        Me.gbReminderDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbReminderDetails.ShowBorder = True
        Me.gbReminderDetails.ShowCheckBox = False
        Me.gbReminderDetails.ShowCollapseButton = False
        Me.gbReminderDetails.ShowDefaultBorderColor = True
        Me.gbReminderDetails.ShowDownButton = False
        Me.gbReminderDetails.ShowHeader = True
        Me.gbReminderDetails.Size = New System.Drawing.Size(681, 377)
        Me.gbReminderDetails.TabIndex = 60
        Me.gbReminderDetails.Temp = 0
        Me.gbReminderDetails.Text = "Reminder Details"
        Me.gbReminderDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbUser
        '
        Me.gbUser.BackColor = System.Drawing.Color.Transparent
        Me.gbUser.BorderColor = System.Drawing.Color.Black
        Me.gbUser.Checked = False
        Me.gbUser.CollapseAllExceptThis = False
        Me.gbUser.CollapsedHoverImage = Nothing
        Me.gbUser.CollapsedNormalImage = Nothing
        Me.gbUser.CollapsedPressedImage = Nothing
        Me.gbUser.CollapseOnLoad = False
        Me.gbUser.Controls.Add(Me.txtSearchUser)
        Me.gbUser.Controls.Add(Me.pnlUserList)
        Me.gbUser.ExpandedHoverImage = Nothing
        Me.gbUser.ExpandedNormalImage = Nothing
        Me.gbUser.ExpandedPressedImage = Nothing
        Me.gbUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUser.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUser.HeaderHeight = 28
        Me.gbUser.HeaderMessage = ""
        Me.gbUser.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbUser.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUser.HeightOnCollapse = 0
        Me.gbUser.LeftTextSpace = 0
        Me.gbUser.Location = New System.Drawing.Point(334, 226)
        Me.gbUser.Name = "gbUser"
        Me.gbUser.OpenHeight = 125
        Me.gbUser.Padding = New System.Windows.Forms.Padding(3)
        Me.gbUser.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUser.ShowBorder = True
        Me.gbUser.ShowCheckBox = False
        Me.gbUser.ShowCollapseButton = False
        Me.gbUser.ShowDefaultBorderColor = True
        Me.gbUser.ShowDownButton = False
        Me.gbUser.ShowHeader = True
        Me.gbUser.Size = New System.Drawing.Size(330, 146)
        Me.gbUser.TabIndex = 220
        Me.gbUser.Temp = 0
        Me.gbUser.Text = "Users   "
        Me.gbUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSearchUser
        '
        Me.txtSearchUser.Flags = 0
        Me.txtSearchUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchUser.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearchUser.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchUser.Name = "txtSearchUser"
        Me.txtSearchUser.Size = New System.Drawing.Size(269, 21)
        Me.txtSearchUser.TabIndex = 222
        '
        'pnlUserList
        '
        Me.pnlUserList.Controls.Add(Me.chkSelectAll)
        Me.pnlUserList.Controls.Add(Me.dgvUserList)
        Me.pnlUserList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlUserList.Location = New System.Drawing.Point(3, 26)
        Me.pnlUserList.Name = "pnlUserList"
        Me.pnlUserList.Size = New System.Drawing.Size(325, 116)
        Me.pnlUserList.TabIndex = 220
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelectAll.Location = New System.Drawing.Point(7, 4)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 17)
        Me.chkSelectAll.TabIndex = 220
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvUserList
        '
        Me.dgvUserList.AllowUserToAddRows = False
        Me.dgvUserList.AllowUserToDeleteRows = False
        Me.dgvUserList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUserList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhCheck, Me.colhUserUnkId, Me.colhUserName, Me.colhFirstName, Me.colhLastName})
        Me.dgvUserList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvUserList.Location = New System.Drawing.Point(0, 0)
        Me.dgvUserList.Name = "dgvUserList"
        Me.dgvUserList.RowHeadersVisible = False
        Me.dgvUserList.Size = New System.Drawing.Size(325, 116)
        Me.dgvUserList.TabIndex = 219
        '
        'objbtnAddType
        '
        Me.objbtnAddType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddType.BorderSelected = False
        Me.objbtnAddType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddType.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Add
        Me.objbtnAddType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddType.Location = New System.Drawing.Point(492, 79)
        Me.objbtnAddType.Name = "objbtnAddType"
        Me.objbtnAddType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddType.TabIndex = 215
        '
        'lvUserList
        '
        Me.lvUserList.CheckBoxes = True
        Me.lvUserList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhDeskClerk})
        Me.lvUserList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvUserList.FullRowSelect = True
        Me.lvUserList.GridLines = True
        Me.lvUserList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvUserList.Location = New System.Drawing.Point(252, 312)
        Me.lvUserList.MultiSelect = False
        Me.lvUserList.Name = "lvUserList"
        Me.lvUserList.Size = New System.Drawing.Size(37, 56)
        Me.lvUserList.TabIndex = 114
        Me.lvUserList.UseCompatibleStateImageBehavior = False
        Me.lvUserList.View = System.Windows.Forms.View.Details
        Me.lvUserList.Visible = False
        '
        'colhDeskClerk
        '
        Me.colhDeskClerk.Tag = "colhDeskClerk"
        Me.colhDeskClerk.Text = "Desk Clerk"
        Me.colhDeskClerk.Width = 246
        '
        'lblDeskClerk
        '
        Me.lblDeskClerk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeskClerk.Location = New System.Drawing.Point(196, 225)
        Me.lblDeskClerk.Name = "lblDeskClerk"
        Me.lblDeskClerk.Size = New System.Drawing.Size(73, 15)
        Me.lblDeskClerk.TabIndex = 113
        Me.lblDeskClerk.Text = "Users"
        Me.lblDeskClerk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblDeskClerk.Visible = False
        '
        'objlblOption
        '
        Me.objlblOption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOption.Location = New System.Drawing.Point(227, 257)
        Me.objlblOption.Name = "objlblOption"
        Me.objlblOption.Size = New System.Drawing.Size(42, 13)
        Me.objlblOption.TabIndex = 107
        Me.objlblOption.Text = "XX"
        Me.objlblOption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.objlblOption.Visible = False
        '
        'nudRecurr
        '
        Me.nudRecurr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudRecurr.Location = New System.Drawing.Point(275, 253)
        Me.nudRecurr.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudRecurr.Name = "nudRecurr"
        Me.nudRecurr.Size = New System.Drawing.Size(53, 21)
        Me.nudRecurr.TabIndex = 8
        Me.nudRecurr.Value = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudRecurr.Visible = False
        '
        'dtRecurringDate
        '
        Me.dtRecurringDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtRecurringDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtRecurringDate.Location = New System.Drawing.Point(163, 336)
        Me.dtRecurringDate.Name = "dtRecurringDate"
        Me.dtRecurringDate.Size = New System.Drawing.Size(83, 21)
        Me.dtRecurringDate.TabIndex = 10
        '
        'chkStopRecurring
        '
        Me.chkStopRecurring.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkStopRecurring.Location = New System.Drawing.Point(25, 338)
        Me.chkStopRecurring.Name = "chkStopRecurring"
        Me.chkStopRecurring.Size = New System.Drawing.Size(132, 17)
        Me.chkStopRecurring.TabIndex = 9
        Me.chkStopRecurring.Text = "Stop recurring after"
        Me.chkStopRecurring.UseVisualStyleBackColor = True
        '
        'cboInterval
        '
        Me.cboInterval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterval.FormattingEnabled = True
        Me.cboInterval.Items.AddRange(New Object() {"Select Interval", "Daily", "Weekly", "Monthly", "Quarter", "Yearly"})
        Me.cboInterval.Location = New System.Drawing.Point(89, 253)
        Me.cboInterval.Name = "cboInterval"
        Me.cboInterval.Size = New System.Drawing.Size(132, 21)
        Me.cboInterval.TabIndex = 7
        '
        'lblInterval
        '
        Me.lblInterval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterval.Location = New System.Drawing.Point(22, 256)
        Me.lblInterval.Name = "lblInterval"
        Me.lblInterval.Size = New System.Drawing.Size(61, 13)
        Me.lblInterval.TabIndex = 105
        Me.lblInterval.Text = "Interval"
        Me.lblInterval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblRecurring
        '
        Me.objlblRecurring.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblRecurring.Location = New System.Drawing.Point(22, 279)
        Me.objlblRecurring.Name = "objlblRecurring"
        Me.objlblRecurring.Size = New System.Drawing.Size(306, 51)
        Me.objlblRecurring.TabIndex = 109
        Me.objlblRecurring.Text = "#Message"
        '
        'lblIntervatnadRecurr
        '
        Me.lblIntervatnadRecurr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntervatnadRecurr.Location = New System.Drawing.Point(9, 226)
        Me.lblIntervatnadRecurr.Name = "lblIntervatnadRecurr"
        Me.lblIntervatnadRecurr.Size = New System.Drawing.Size(161, 15)
        Me.lblIntervatnadRecurr.TabIndex = 104
        Me.lblIntervatnadRecurr.Text = "Interval and Recurrence"
        '
        'txtMessage
        '
        Me.txtMessage.Flags = 0
        Me.txtMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMessage.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMessage.Location = New System.Drawing.Point(11, 127)
        Me.txtMessage.Multiline = True
        Me.txtMessage.Name = "txtMessage"
        Me.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMessage.Size = New System.Drawing.Size(657, 90)
        Me.txtMessage.TabIndex = 6
        Me.txtMessage.TabStop = False
        '
        'lblMessage
        '
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(9, 108)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(85, 16)
        Me.lblMessage.TabIndex = 102
        Me.lblMessage.Text = "Message"
        Me.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPriority
        '
        Me.cboPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPriority.FormattingEnabled = True
        Me.cboPriority.Items.AddRange(New Object() {"Select Priority", "Low", "Medium", "High"})
        Me.cboPriority.Location = New System.Drawing.Point(575, 79)
        Me.cboPriority.Name = "cboPriority"
        Me.cboPriority.Size = New System.Drawing.Size(92, 21)
        Me.cboPriority.TabIndex = 5
        '
        'lblPriority
        '
        Me.lblPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPriority.Location = New System.Drawing.Point(519, 81)
        Me.lblPriority.Name = "lblPriority"
        Me.lblPriority.Size = New System.Drawing.Size(51, 16)
        Me.lblPriority.TabIndex = 100
        Me.lblPriority.Text = "Priority"
        Me.lblPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboType
        '
        Me.cboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboType.FormattingEnabled = True
        Me.cboType.Location = New System.Drawing.Point(339, 79)
        Me.cboType.Name = "cboType"
        Me.cboType.Size = New System.Drawing.Size(147, 21)
        Me.cboType.TabIndex = 4
        '
        'lblType
        '
        Me.lblType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(275, 81)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(58, 16)
        Me.lblType.TabIndex = 98
        Me.lblType.Text = "Type"
        Me.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartTime
        '
        Me.dtpStartTime.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpStartTime.Location = New System.Drawing.Point(182, 79)
        Me.dtpStartTime.Name = "dtpStartTime"
        Me.dtpStartTime.ShowUpDown = True
        Me.dtpStartTime.Size = New System.Drawing.Size(87, 21)
        Me.dtpStartTime.TabIndex = 3
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(89, 79)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(87, 21)
        Me.dtpStartDate.TabIndex = 2
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(8, 81)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(75, 16)
        Me.lblStartDate.TabIndex = 95
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(11, 61)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(657, 12)
        Me.objLine1.TabIndex = 94
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(89, 34)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(579, 21)
        Me.txtName.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 36)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(75, 16)
        Me.lblName.TabIndex = 19
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 396)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(706, 55)
        Me.objFooter.TabIndex = 50
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(494, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 11
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(597, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 12
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "First Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Last Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'colhCheck
        '
        Me.colhCheck.HeaderText = ""
        Me.colhCheck.Name = "colhCheck"
        Me.colhCheck.Width = 25
        '
        'colhUserUnkId
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F0"
        Me.colhUserUnkId.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhUserUnkId.HeaderText = "ID"
        Me.colhUserUnkId.Name = "colhUserUnkId"
        Me.colhUserUnkId.ReadOnly = True
        Me.colhUserUnkId.Visible = False
        '
        'colhUserName
        '
        Me.colhUserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhUserName.HeaderText = "User Name"
        Me.colhUserName.Name = "colhUserName"
        Me.colhUserName.ReadOnly = True
        '
        'colhFirstName
        '
        Me.colhFirstName.HeaderText = "First Name"
        Me.colhFirstName.Name = "colhFirstName"
        Me.colhFirstName.ReadOnly = True
        Me.colhFirstName.Width = 90
        '
        'colhLastName
        '
        Me.colhLastName.HeaderText = "Last Name"
        Me.colhLastName.Name = "colhLastName"
        Me.colhLastName.ReadOnly = True
        Me.colhLastName.Width = 90
        '
        'frmReminder_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 451)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbReminderDetails)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReminder_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Reminder"
        Me.gbReminderDetails.ResumeLayout(False)
        Me.gbReminderDetails.PerformLayout()
        Me.gbUser.ResumeLayout(False)
        Me.gbUser.PerformLayout()
        Me.pnlUserList.ResumeLayout(False)
        CType(Me.dgvUserList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudRecurr, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbReminderDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboPriority As System.Windows.Forms.ComboBox
    Friend WithEvents lblPriority As System.Windows.Forms.Label
    Friend WithEvents cboType As System.Windows.Forms.ComboBox
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents txtMessage As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblIntervatnadRecurr As System.Windows.Forms.Label
    Friend WithEvents objlblOption As System.Windows.Forms.Label
    Friend WithEvents nudRecurr As System.Windows.Forms.NumericUpDown
    Friend WithEvents dtRecurringDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkStopRecurring As System.Windows.Forms.CheckBox
    Friend WithEvents cboInterval As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterval As System.Windows.Forms.Label
    Friend WithEvents objlblRecurring As System.Windows.Forms.Label
    Friend WithEvents lvUserList As System.Windows.Forms.ListView
    Friend WithEvents colhDeskClerk As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblDeskClerk As System.Windows.Forms.Label
    Friend WithEvents objbtnAddType As eZee.Common.eZeeGradientButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbUser As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlUserList As System.Windows.Forms.Panel
    Friend WithEvents dgvUserList As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearchUser As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents colhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhUserUnkId As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhUserName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFirstName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLastName As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
