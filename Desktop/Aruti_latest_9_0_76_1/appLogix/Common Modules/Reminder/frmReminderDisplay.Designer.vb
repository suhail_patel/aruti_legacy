<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReminderDisplay
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReminderDisplay))
        Me.btnOK = New eZee.Common.eZeeLightButton(Me.components)
        Me.cmsSoonze = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.miFiveMin = New System.Windows.Forms.ToolStripMenuItem
        Me.miTenMin = New System.Windows.Forms.ToolStripMenuItem
        Me.miTwentyMin = New System.Windows.Forms.ToolStripMenuItem
        Me.miThirtyMin = New System.Windows.Forms.ToolStripMenuItem
        Me.miOneHour = New System.Windows.Forms.ToolStripMenuItem
        Me.miTwoHour = New System.Windows.Forms.ToolStripMenuItem
        Me.miFourHours = New System.Windows.Forms.ToolStripMenuItem
        Me.miEightHours = New System.Windows.Forms.ToolStripMenuItem
        Me.miOneDay = New System.Windows.Forms.ToolStripMenuItem
        Me.miTwoDay = New System.Windows.Forms.ToolStripMenuItem
        Me.miThreeDays = New System.Windows.Forms.ToolStripMenuItem
        Me.miOneWeek = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnSnooze = New eZee.Common.eZeeSplitButton
        Me.txttitle = New System.Windows.Forms.TextBox
        Me.txtMessage = New System.Windows.Forms.TextBox
        Me.objlblDueDateTime = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.cmsSoonze.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackColor = System.Drawing.Color.White
        Me.btnOK.BackgroundImage = CType(resources.GetObject("btnOK.BackgroundImage"), System.Drawing.Image)
        Me.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOK.BorderColor = System.Drawing.Color.Empty
        Me.btnOK.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnOK.FlatAppearance.BorderSize = 0
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOK.GradientForeColor = System.Drawing.Color.Black
        Me.btnOK.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Location = New System.Drawing.Point(458, 13)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Size = New System.Drawing.Size(91, 30)
        Me.btnOK.TabIndex = 144
        Me.btnOK.Text = "&OK"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'cmsSoonze
        '
        Me.cmsSoonze.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miFiveMin, Me.miTenMin, Me.miTwentyMin, Me.miThirtyMin, Me.miOneHour, Me.miTwoHour, Me.miFourHours, Me.miEightHours, Me.miOneDay, Me.miTwoDay, Me.miThreeDays, Me.miOneWeek})
        Me.cmsSoonze.Name = "ContextMenuStrip1"
        Me.cmsSoonze.Size = New System.Drawing.Size(121, 268)
        '
        'miFiveMin
        '
        Me.miFiveMin.Name = "miFiveMin"
        Me.miFiveMin.Size = New System.Drawing.Size(120, 22)
        Me.miFiveMin.Text = "5 Minuts"
        '
        'miTenMin
        '
        Me.miTenMin.Name = "miTenMin"
        Me.miTenMin.Size = New System.Drawing.Size(120, 22)
        Me.miTenMin.Text = "10 Minuts"
        '
        'miTwentyMin
        '
        Me.miTwentyMin.Name = "miTwentyMin"
        Me.miTwentyMin.Size = New System.Drawing.Size(120, 22)
        Me.miTwentyMin.Text = "20 Minuts"
        '
        'miThirtyMin
        '
        Me.miThirtyMin.Name = "miThirtyMin"
        Me.miThirtyMin.Size = New System.Drawing.Size(120, 22)
        Me.miThirtyMin.Text = "30 Minuts"
        '
        'miOneHour
        '
        Me.miOneHour.Name = "miOneHour"
        Me.miOneHour.Size = New System.Drawing.Size(120, 22)
        Me.miOneHour.Text = "1 Hour"
        '
        'miTwoHour
        '
        Me.miTwoHour.Name = "miTwoHour"
        Me.miTwoHour.Size = New System.Drawing.Size(120, 22)
        Me.miTwoHour.Text = "2 Hours"
        '
        'miFourHours
        '
        Me.miFourHours.Name = "miFourHours"
        Me.miFourHours.Size = New System.Drawing.Size(120, 22)
        Me.miFourHours.Text = "4 Hours"
        '
        'miEightHours
        '
        Me.miEightHours.Name = "miEightHours"
        Me.miEightHours.Size = New System.Drawing.Size(120, 22)
        Me.miEightHours.Text = "8 Hours"
        '
        'miOneDay
        '
        Me.miOneDay.Name = "miOneDay"
        Me.miOneDay.Size = New System.Drawing.Size(120, 22)
        Me.miOneDay.Text = "1 Day"
        '
        'miTwoDay
        '
        Me.miTwoDay.Name = "miTwoDay"
        Me.miTwoDay.Size = New System.Drawing.Size(120, 22)
        Me.miTwoDay.Text = "2 Days"
        '
        'miThreeDays
        '
        Me.miThreeDays.Name = "miThreeDays"
        Me.miThreeDays.Size = New System.Drawing.Size(120, 22)
        Me.miThreeDays.Text = "3 Days"
        '
        'miOneWeek
        '
        Me.miOneWeek.Name = "miOneWeek"
        Me.miOneWeek.Size = New System.Drawing.Size(120, 22)
        Me.miOneWeek.Text = "1 Week"
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objefFormFooter)
        Me.pnlMain.Controls.Add(Me.txttitle)
        Me.pnlMain.Controls.Add(Me.txtMessage)
        Me.pnlMain.Controls.Add(Me.objlblDueDateTime)
        Me.pnlMain.Controls.Add(Me.lblMessage)
        Me.pnlMain.Controls.Add(Me.lblTitle)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(561, 348)
        Me.pnlMain.TabIndex = 3
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnSnooze)
        Me.objefFormFooter.Controls.Add(Me.btnOK)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 293)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(561, 55)
        Me.objefFormFooter.TabIndex = 222
        '
        'btnSnooze
        '
        Me.btnSnooze.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSnooze.BorderColor = System.Drawing.Color.Black
        Me.btnSnooze.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSnooze.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSnooze.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnSnooze.Location = New System.Drawing.Point(361, 13)
        Me.btnSnooze.Name = "btnSnooze"
        Me.btnSnooze.ShowDefaultBorderColor = True
        Me.btnSnooze.Size = New System.Drawing.Size(91, 30)
        Me.btnSnooze.SplitButtonMenu = Me.cmsSoonze
        Me.btnSnooze.TabIndex = 221
        Me.btnSnooze.Text = "&Snooze"
        '
        'txttitle
        '
        Me.txttitle.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txttitle.Location = New System.Drawing.Point(74, 43)
        Me.txttitle.Name = "txttitle"
        Me.txttitle.ReadOnly = True
        Me.txttitle.Size = New System.Drawing.Size(403, 14)
        Me.txttitle.TabIndex = 220
        Me.txttitle.Text = "xx"
        '
        'txtMessage
        '
        Me.txtMessage.Location = New System.Drawing.Point(11, 88)
        Me.txtMessage.Multiline = True
        Me.txtMessage.Name = "txtMessage"
        Me.txtMessage.ReadOnly = True
        Me.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMessage.Size = New System.Drawing.Size(539, 199)
        Me.txtMessage.TabIndex = 219
        Me.txtMessage.Text = "xx"
        '
        'objlblDueDateTime
        '
        Me.objlblDueDateTime.BackColor = System.Drawing.Color.SteelBlue
        Me.objlblDueDateTime.Dock = System.Windows.Forms.DockStyle.Top
        Me.objlblDueDateTime.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDueDateTime.Location = New System.Drawing.Point(0, 0)
        Me.objlblDueDateTime.Name = "objlblDueDateTime"
        Me.objlblDueDateTime.Size = New System.Drawing.Size(561, 28)
        Me.objlblDueDateTime.TabIndex = 145
        Me.objlblDueDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMessage
        '
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(9, 71)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(467, 14)
        Me.lblMessage.TabIndex = 6
        Me.lblMessage.Text = "Message"
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(10, 43)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(60, 14)
        Me.lblTitle.TabIndex = 1
        Me.lblTitle.Text = "Title"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmReminderDisplay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(561, 348)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReminderDisplay"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reminder"
        Me.TopMost = True
        Me.cmsSoonze.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOK As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objlblDueDateTime As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents cmsSoonze As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents miFiveMin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miTenMin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miTwentyMin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miThirtyMin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miOneHour As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miTwoHour As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miFourHours As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miEightHours As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miOneDay As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miTwoDay As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miThreeDays As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miOneWeek As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents txtMessage As System.Windows.Forms.TextBox
    Friend WithEvents txttitle As System.Windows.Forms.TextBox
    Friend WithEvents btnSnooze As eZee.Common.eZeeSplitButton
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
End Class
