﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System

#End Region

Public Class frmVoidReason_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmVoidReason_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintVoidReasonUnkid As Integer = -1
    Private objVoidReason As clsVoidReason

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintVoidReasonUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintVoidReasonUnkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub SetColor()
        txtAlias.BackColor = GUI.ColorOptional
        cboCategory.BackColor = GUI.ColorComp
        txtReason.BackColor = GUI.ColorComp
    End Sub

    Private Sub SetValue()
        Try
            objVoidReason._Alias = txtAlias.Text
            objVoidReason._Categoryunkid = CInt(cboCategory.SelectedValue)
            objVoidReason._Reason = txtReason.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAlias.Text = objVoidReason._Alias
            cboCategory.SelectedValue = objVoidReason._Categoryunkid
            txtReason.Text = objVoidReason._Reason
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Category is compulsory information. Please select Category to continue."), enMsgBoxStyle.Information)
                cboCategory.Focus()
                Return False
            End If

            If txtReason.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reason is compulsory information. Reason cannot be blank."), enMsgBoxStyle.Information)
                txtReason.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsList = objMaster.GetVoidCategoryList("List")
            With cboCategory
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList = Nothing
            objMaster = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmVoidReason_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objVoidReason = New clsVoidReason
        Try
            Call Set_Logo(Me, gApplicationType)
            SetColor()

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End


            If menAction = enAction.EDIT_ONE Then
                objVoidReason._Reasonunkid = mintVoidReasonUnkid
            End If

            FillCombo()

            GetValue()

            txtAlias.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmState_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVoidReason_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmState_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVoidReason_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmState_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVoidReason_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objVoidReason = Nothing
    End Sub

    'Pinkal (10-Jul-2014) -- Start
    'Enhancement - Language Settings

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsVoidReason.SetMessages()
            objfrm._Other_ModuleNames = "clsVoidReason"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (10-Jul-2014) -- End

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objVoidReason.Update
            Else
                blnFlag = objVoidReason.Insert
            End If

            If blnFlag = False And objVoidReason._Message <> "" Then
                eZeeMsgBox.Show(objVoidReason._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objVoidReason = Nothing
                    objVoidReason = New clsVoidReason
                    Call GetValue()
                    txtAlias.Select()
                Else
                    mintVoidReasonUnkid = objVoidReason._Reasonunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbVoidReason.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbVoidReason.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbVoidReason.Text = Language._Object.getCaption(Me.gbVoidReason.Name, Me.gbVoidReason.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblAlias.Text = Language._Object.getCaption(Me.lblAlias.Name, Me.lblAlias.Text)
            Me.lblCategory.Text = Language._Object.getCaption(Me.lblCategory.Name, Me.lblCategory.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Category is compulsory information. Please select Category to continue.")
            Language.setMessage(mstrModuleName, 2, "Reason is compulsory information. Reason cannot be blank.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class