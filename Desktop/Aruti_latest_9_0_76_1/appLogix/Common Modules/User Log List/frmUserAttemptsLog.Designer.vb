﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserAttemptsLog
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserAttemptsLog))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.spcContainer = New System.Windows.Forms.SplitContainer
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboAttemptType = New System.Windows.Forms.ComboBox
        Me.lblAttemptsType = New System.Windows.Forms.Label
        Me.btnReset = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.cboFilter = New System.Windows.Forms.ComboBox
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.btnSearch = New eZee.Common.eZeeGradientButton
        Me.gbUserLog = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlData = New System.Windows.Forms.Panel
        Me.lvUserLog = New eZee.Common.eZeeListView(Me.components)
        Me.colhUserName = New System.Windows.Forms.ColumnHeader
        Me.colhIp = New System.Windows.Forms.ColumnHeader
        Me.colhMachine = New System.Windows.Forms.ColumnHeader
        Me.colhAttempts = New System.Windows.Forms.ColumnHeader
        Me.colhAttemptType = New System.Windows.Forms.ColumnHeader
        Me.colhLoginFrom = New System.Windows.Forms.ColumnHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.colhAttempt_Date = New System.Windows.Forms.ColumnHeader
        Me.pnlMain.SuspendLayout()
        Me.spcContainer.Panel1.SuspendLayout()
        Me.spcContainer.Panel2.SuspendLayout()
        Me.spcContainer.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbUserLog.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.spcContainer)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(726, 424)
        Me.pnlMain.TabIndex = 1
        '
        'spcContainer
        '
        Me.spcContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spcContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.spcContainer.Location = New System.Drawing.Point(0, 0)
        Me.spcContainer.Margin = New System.Windows.Forms.Padding(0)
        Me.spcContainer.Name = "spcContainer"
        Me.spcContainer.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'spcContainer.Panel1
        '
        Me.spcContainer.Panel1.Controls.Add(Me.gbFilterCriteria)
        '
        'spcContainer.Panel2
        '
        Me.spcContainer.Panel2.Controls.Add(Me.gbUserLog)
        Me.spcContainer.Size = New System.Drawing.Size(726, 374)
        Me.spcContainer.SplitterDistance = 77
        Me.spcContainer.SplitterWidth = 3
        Me.spcContainer.TabIndex = 2
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboAttemptType)
        Me.gbFilterCriteria.Controls.Add(Me.lblAttemptsType)
        Me.gbFilterCriteria.Controls.Add(Me.btnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboFilter)
        Me.gbFilterCriteria.Controls.Add(Me.objlblCaption)
        Me.gbFilterCriteria.Controls.Add(Me.cboViewBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblViewBy)
        Me.gbFilterCriteria.Controls.Add(Me.btnSearch)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(726, 77)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAttemptType
        '
        Me.cboAttemptType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAttemptType.DropDownWidth = 200
        Me.cboAttemptType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAttemptType.FormattingEnabled = True
        Me.cboAttemptType.Location = New System.Drawing.Point(93, 39)
        Me.cboAttemptType.Name = "cboAttemptType"
        Me.cboAttemptType.Size = New System.Drawing.Size(140, 21)
        Me.cboAttemptType.TabIndex = 242
        '
        'lblAttemptsType
        '
        Me.lblAttemptsType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttemptsType.Location = New System.Drawing.Point(8, 41)
        Me.lblAttemptsType.Name = "lblAttemptsType"
        Me.lblAttemptsType.Size = New System.Drawing.Size(79, 16)
        Me.lblAttemptsType.TabIndex = 241
        Me.lblAttemptsType.Text = "Attempt Type"
        Me.lblAttemptsType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.Transparent
        Me.btnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.btnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.btnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnReset.BorderSelected = False
        Me.btnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnReset.Image = Global.Aruti.Data.My.Resources.Resources.reset_20
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnReset.Location = New System.Drawing.Point(701, 2)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(21, 21)
        Me.btnReset.TabIndex = 217
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Search
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(696, 39)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 239
        '
        'cboFilter
        '
        Me.cboFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilter.DropDownWidth = 200
        Me.cboFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilter.FormattingEnabled = True
        Me.cboFilter.Location = New System.Drawing.Point(536, 39)
        Me.cboFilter.Name = "cboFilter"
        Me.cboFilter.Size = New System.Drawing.Size(154, 21)
        Me.cboFilter.TabIndex = 238
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(452, 41)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(78, 16)
        Me.objlblCaption.TabIndex = 237
        Me.objlblCaption.Text = "#Caption"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.DropDownWidth = 200
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(313, 39)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(133, 21)
        Me.cboViewBy.TabIndex = 235
        '
        'lblViewBy
        '
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(239, 41)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(68, 16)
        Me.lblViewBy.TabIndex = 234
        Me.lblViewBy.Text = "View By"
        Me.lblViewBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.BackColor = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.btnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSearch.BorderSelected = False
        Me.btnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnSearch.Image = Global.Aruti.Data.My.Resources.Resources.search_20
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnSearch.Location = New System.Drawing.Point(677, 2)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(21, 21)
        Me.btnSearch.TabIndex = 218
        '
        'gbUserLog
        '
        Me.gbUserLog.BorderColor = System.Drawing.Color.Black
        Me.gbUserLog.Checked = False
        Me.gbUserLog.CollapseAllExceptThis = False
        Me.gbUserLog.CollapsedHoverImage = Nothing
        Me.gbUserLog.CollapsedNormalImage = Nothing
        Me.gbUserLog.CollapsedPressedImage = Nothing
        Me.gbUserLog.CollapseOnLoad = False
        Me.gbUserLog.Controls.Add(Me.pnlData)
        Me.gbUserLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbUserLog.ExpandedHoverImage = Nothing
        Me.gbUserLog.ExpandedNormalImage = Nothing
        Me.gbUserLog.ExpandedPressedImage = Nothing
        Me.gbUserLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUserLog.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUserLog.HeaderHeight = 25
        Me.gbUserLog.HeaderMessage = ""
        Me.gbUserLog.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbUserLog.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUserLog.HeightOnCollapse = 0
        Me.gbUserLog.LeftTextSpace = 0
        Me.gbUserLog.Location = New System.Drawing.Point(0, 0)
        Me.gbUserLog.Name = "gbUserLog"
        Me.gbUserLog.OpenHeight = 300
        Me.gbUserLog.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUserLog.ShowBorder = True
        Me.gbUserLog.ShowCheckBox = False
        Me.gbUserLog.ShowCollapseButton = False
        Me.gbUserLog.ShowDefaultBorderColor = True
        Me.gbUserLog.ShowDownButton = False
        Me.gbUserLog.ShowHeader = True
        Me.gbUserLog.Size = New System.Drawing.Size(726, 294)
        Me.gbUserLog.TabIndex = 0
        Me.gbUserLog.Temp = 0
        Me.gbUserLog.Text = "User Attempts Log"
        Me.gbUserLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlData
        '
        Me.pnlData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlData.Controls.Add(Me.lvUserLog)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(2, 26)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(722, 266)
        Me.pnlData.TabIndex = 2
        '
        'lvUserLog
        '
        Me.lvUserLog.BackColorOnChecked = False
        Me.lvUserLog.ColumnHeaders = Nothing
        Me.lvUserLog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhUserName, Me.colhIp, Me.colhMachine, Me.colhAttempts, Me.colhAttemptType, Me.colhLoginFrom, Me.colhAttempt_Date})
        Me.lvUserLog.CompulsoryColumns = ""
        Me.lvUserLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvUserLog.FullRowSelect = True
        Me.lvUserLog.GridLines = True
        Me.lvUserLog.GroupingColumn = Nothing
        Me.lvUserLog.HideSelection = False
        Me.lvUserLog.Location = New System.Drawing.Point(0, 0)
        Me.lvUserLog.MinColumnWidth = 50
        Me.lvUserLog.MultiSelect = False
        Me.lvUserLog.Name = "lvUserLog"
        Me.lvUserLog.OptionalColumns = ""
        Me.lvUserLog.ShowMoreItem = False
        Me.lvUserLog.ShowSaveItem = False
        Me.lvUserLog.ShowSelectAll = True
        Me.lvUserLog.ShowSizeAllColumnsToFit = True
        Me.lvUserLog.Size = New System.Drawing.Size(722, 266)
        Me.lvUserLog.Sortable = True
        Me.lvUserLog.TabIndex = 1
        Me.lvUserLog.UseCompatibleStateImageBehavior = False
        Me.lvUserLog.View = System.Windows.Forms.View.Details
        '
        'colhUserName
        '
        Me.colhUserName.Tag = "colhUserName"
        Me.colhUserName.Text = "User"
        Me.colhUserName.Width = 0
        '
        'colhIp
        '
        Me.colhIp.Tag = "colhIp"
        Me.colhIp.Text = "IP"
        Me.colhIp.Width = 165
        '
        'colhMachine
        '
        Me.colhMachine.Tag = "colhMachine"
        Me.colhMachine.Text = "Machine"
        Me.colhMachine.Width = 148
        '
        'colhAttempts
        '
        Me.colhAttempts.Tag = "colhAttempts"
        Me.colhAttempts.Text = "Attempts"
        '
        'colhAttemptType
        '
        Me.colhAttemptType.DisplayIndex = 5
        Me.colhAttemptType.Tag = "colhAttemptType"
        Me.colhAttemptType.Text = "Attempt Type"
        Me.colhAttemptType.Width = 115
        '
        'colhLoginFrom
        '
        Me.colhLoginFrom.DisplayIndex = 6
        Me.colhLoginFrom.Tag = "colhLoginFrom"
        Me.colhLoginFrom.Text = "Login From"
        Me.colhLoginFrom.Width = 140
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 374)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(726, 50)
        Me.EZeeFooter1.TabIndex = 0
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(502, 9)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(103, 30)
        Me.btnExport.TabIndex = 1
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(611, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(103, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'colhAttempt_Date
        '
        Me.colhAttempt_Date.DisplayIndex = 4
        Me.colhAttempt_Date.Tag = "colhAttempt_Date"
        Me.colhAttempt_Date.Text = "Attempt Date"
        Me.colhAttempt_Date.Width = 90
        '
        'frmUserAttemptsLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(726, 424)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUserAttemptsLog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User Login Attempts"
        Me.pnlMain.ResumeLayout(False)
        Me.spcContainer.Panel1.ResumeLayout(False)
        Me.spcContainer.Panel2.ResumeLayout(False)
        Me.spcContainer.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbUserLog.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents spcContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFilter As System.Windows.Forms.ComboBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents btnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents gbUserLog As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lvUserLog As eZee.Common.eZeeListView
    Friend WithEvents colhUserName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMachine As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAttempts As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoginFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboAttemptType As System.Windows.Forms.ComboBox
    Friend WithEvents lblAttemptsType As System.Windows.Forms.Label
    Friend WithEvents colhAttemptType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAttempt_Date As System.Windows.Forms.ColumnHeader
End Class
