﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayrollGroupsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayrollGroupsList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvGroupList = New eZee.Common.eZeeListView(Me.components)
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.gbGroupList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblGroupLists = New System.Windows.Forms.Label
        Me.cboGroupList = New System.Windows.Forms.ComboBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbGroupList.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lvGroupList)
        Me.pnlMainInfo.Controls.Add(Me.gbGroupList)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(752, 388)
        Me.pnlMainInfo.TabIndex = 1
        '
        'lvGroupList
        '
        Me.lvGroupList.BackColorOnChecked = True
        Me.lvGroupList.ColumnHeaders = Nothing
        Me.lvGroupList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCode, Me.colhName, Me.colhDescription})
        Me.lvGroupList.CompulsoryColumns = ""
        Me.lvGroupList.FullRowSelect = True
        Me.lvGroupList.GridLines = True
        Me.lvGroupList.GroupingColumn = Nothing
        Me.lvGroupList.HideSelection = False
        Me.lvGroupList.Location = New System.Drawing.Point(11, 133)
        Me.lvGroupList.MinColumnWidth = 50
        Me.lvGroupList.MultiSelect = False
        Me.lvGroupList.Name = "lvGroupList"
        Me.lvGroupList.OptionalColumns = ""
        Me.lvGroupList.ShowMoreItem = False
        Me.lvGroupList.ShowSaveItem = False
        Me.lvGroupList.ShowSelectAll = True
        Me.lvGroupList.ShowSizeAllColumnsToFit = True
        Me.lvGroupList.Size = New System.Drawing.Size(730, 194)
        Me.lvGroupList.Sortable = True
        Me.lvGroupList.TabIndex = 7
        Me.lvGroupList.UseCompatibleStateImageBehavior = False
        Me.lvGroupList.View = System.Windows.Forms.View.Details
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 100
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 220
        '
        'colhDescription
        '
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 405
        '
        'gbGroupList
        '
        Me.gbGroupList.BorderColor = System.Drawing.Color.Black
        Me.gbGroupList.Checked = False
        Me.gbGroupList.CollapseAllExceptThis = False
        Me.gbGroupList.CollapsedHoverImage = Nothing
        Me.gbGroupList.CollapsedNormalImage = Nothing
        Me.gbGroupList.CollapsedPressedImage = Nothing
        Me.gbGroupList.CollapseOnLoad = False
        Me.gbGroupList.Controls.Add(Me.lblGroupLists)
        Me.gbGroupList.Controls.Add(Me.cboGroupList)
        Me.gbGroupList.ExpandedHoverImage = Nothing
        Me.gbGroupList.ExpandedNormalImage = Nothing
        Me.gbGroupList.ExpandedPressedImage = Nothing
        Me.gbGroupList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGroupList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbGroupList.HeaderHeight = 25
        Me.gbGroupList.HeaderMessage = ""
        Me.gbGroupList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbGroupList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbGroupList.HeightOnCollapse = 0
        Me.gbGroupList.LeftTextSpace = 0
        Me.gbGroupList.Location = New System.Drawing.Point(11, 65)
        Me.gbGroupList.Name = "gbGroupList"
        Me.gbGroupList.OpenHeight = 63
        Me.gbGroupList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbGroupList.ShowBorder = True
        Me.gbGroupList.ShowCheckBox = False
        Me.gbGroupList.ShowCollapseButton = False
        Me.gbGroupList.ShowDefaultBorderColor = True
        Me.gbGroupList.ShowDownButton = False
        Me.gbGroupList.ShowHeader = True
        Me.gbGroupList.Size = New System.Drawing.Size(730, 63)
        Me.gbGroupList.TabIndex = 6
        Me.gbGroupList.Temp = 0
        Me.gbGroupList.Text = "Filter Criteria"
        Me.gbGroupList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGroupLists
        '
        Me.lblGroupLists.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroupLists.Location = New System.Drawing.Point(8, 36)
        Me.lblGroupLists.Name = "lblGroupLists"
        Me.lblGroupLists.Size = New System.Drawing.Size(75, 15)
        Me.lblGroupLists.TabIndex = 1
        Me.lblGroupLists.Text = "Group"
        Me.lblGroupLists.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGroupList
        '
        Me.cboGroupList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroupList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGroupList.FormattingEnabled = True
        Me.cboGroupList.Location = New System.Drawing.Point(89, 33)
        Me.cboGroupList.Name = "cboGroupList"
        Me.cboGroupList.Size = New System.Drawing.Size(177, 21)
        Me.cboGroupList.TabIndex = 2
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(752, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Groups List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 333)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(752, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(437, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 121
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(540, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(334, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(643, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmPayrollGroupsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(752, 388)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayrollGroupsList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payroll Groups List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbGroupList.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents lvGroupList As eZee.Common.eZeeListView
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbGroupList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblGroupLists As System.Windows.Forms.Label
    Friend WithEvents cboGroupList As System.Windows.Forms.ComboBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
End Class
