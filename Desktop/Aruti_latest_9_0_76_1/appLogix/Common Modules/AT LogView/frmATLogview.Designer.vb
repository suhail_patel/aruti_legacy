﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmATLogview
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmATLogview))
        Me.gbGroupList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnReset = New eZee.Common.eZeeGradientButton
        Me.btnSearch = New eZee.Common.eZeeGradientButton
        Me.lblTodate = New System.Windows.Forms.Label
        Me.dtpTodate = New System.Windows.Forms.DateTimePicker
        Me.lblEventType = New System.Windows.Forms.Label
        Me.cboEventType = New System.Windows.Forms.ComboBox
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.objbtnSearchUser = New eZee.Common.eZeeGradientButton
        Me.lblUser = New System.Windows.Forms.Label
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvAuditData = New System.Windows.Forms.DataGridView
        Me.tvModuleList = New System.Windows.Forms.TreeView
        Me.tblpnlData = New System.Windows.Forms.TableLayoutPanel
        Me.pnlTreeData = New System.Windows.Forms.Panel
        Me.pnlViewData = New System.Windows.Forms.Panel
        Me.cboChildData = New System.Windows.Forms.ComboBox
        Me.tabcData = New System.Windows.Forms.TabControl
        Me.tabpMasterDetail = New System.Windows.Forms.TabPage
        Me.tabpChildDetail = New System.Windows.Forms.TabPage
        Me.lvData = New eZee.Common.eZeeListView(Me.components)
        Me.gbGroupList.SuspendLayout()
        Me.objFooter.SuspendLayout()
        CType(Me.dgvAuditData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblpnlData.SuspendLayout()
        Me.pnlTreeData.SuspendLayout()
        Me.pnlViewData.SuspendLayout()
        Me.tabcData.SuspendLayout()
        Me.tabpMasterDetail.SuspendLayout()
        Me.tabpChildDetail.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbGroupList
        '
        Me.gbGroupList.BorderColor = System.Drawing.Color.Black
        Me.gbGroupList.Checked = False
        Me.gbGroupList.CollapseAllExceptThis = False
        Me.gbGroupList.CollapsedHoverImage = Nothing
        Me.gbGroupList.CollapsedNormalImage = Nothing
        Me.gbGroupList.CollapsedPressedImage = Nothing
        Me.gbGroupList.CollapseOnLoad = False
        Me.gbGroupList.Controls.Add(Me.btnReset)
        Me.gbGroupList.Controls.Add(Me.btnSearch)
        Me.gbGroupList.Controls.Add(Me.lblTodate)
        Me.gbGroupList.Controls.Add(Me.dtpTodate)
        Me.gbGroupList.Controls.Add(Me.lblEventType)
        Me.gbGroupList.Controls.Add(Me.cboEventType)
        Me.gbGroupList.Controls.Add(Me.dtpFromDate)
        Me.gbGroupList.Controls.Add(Me.lblFromDate)
        Me.gbGroupList.Controls.Add(Me.objbtnSearchUser)
        Me.gbGroupList.Controls.Add(Me.lblUser)
        Me.gbGroupList.Controls.Add(Me.cboUser)
        Me.gbGroupList.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbGroupList.ExpandedHoverImage = Nothing
        Me.gbGroupList.ExpandedNormalImage = Nothing
        Me.gbGroupList.ExpandedPressedImage = Nothing
        Me.gbGroupList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGroupList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbGroupList.HeaderHeight = 25
        Me.gbGroupList.HeaderMessage = ""
        Me.gbGroupList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbGroupList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbGroupList.HeightOnCollapse = 0
        Me.gbGroupList.LeftTextSpace = 0
        Me.gbGroupList.Location = New System.Drawing.Point(0, 0)
        Me.gbGroupList.Name = "gbGroupList"
        Me.gbGroupList.OpenHeight = 63
        Me.gbGroupList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbGroupList.ShowBorder = True
        Me.gbGroupList.ShowCheckBox = False
        Me.gbGroupList.ShowCollapseButton = False
        Me.gbGroupList.ShowDefaultBorderColor = True
        Me.gbGroupList.ShowDownButton = False
        Me.gbGroupList.ShowHeader = True
        Me.gbGroupList.Size = New System.Drawing.Size(706, 64)
        Me.gbGroupList.TabIndex = 7
        Me.gbGroupList.Temp = 0
        Me.gbGroupList.Text = "Filter Criteria"
        Me.gbGroupList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.Transparent
        Me.btnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.btnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.btnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnReset.BorderSelected = False
        Me.btnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnReset.Image = Global.Aruti.Data.My.Resources.Resources.reset_20
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnReset.Location = New System.Drawing.Point(681, 2)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(21, 21)
        Me.btnReset.TabIndex = 219
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.BackColor = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.btnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSearch.BorderSelected = False
        Me.btnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnSearch.Image = Global.Aruti.Data.My.Resources.Resources.search_20
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnSearch.Location = New System.Drawing.Point(657, 2)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(21, 21)
        Me.btnSearch.TabIndex = 220
        '
        'lblTodate
        '
        Me.lblTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodate.Location = New System.Drawing.Point(564, 36)
        Me.lblTodate.Name = "lblTodate"
        Me.lblTodate.Size = New System.Drawing.Size(22, 15)
        Me.lblTodate.TabIndex = 210
        Me.lblTodate.Text = "To"
        '
        'dtpTodate
        '
        Me.dtpTodate.Checked = False
        Me.dtpTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTodate.Location = New System.Drawing.Point(592, 33)
        Me.dtpTodate.Name = "dtpTodate"
        Me.dtpTodate.ShowCheckBox = True
        Me.dtpTodate.Size = New System.Drawing.Size(105, 21)
        Me.dtpTodate.TabIndex = 209
        '
        'lblEventType
        '
        Me.lblEventType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEventType.Location = New System.Drawing.Point(222, 36)
        Me.lblEventType.Name = "lblEventType"
        Me.lblEventType.Size = New System.Drawing.Size(69, 15)
        Me.lblEventType.TabIndex = 91
        Me.lblEventType.Text = "Event Type"
        Me.lblEventType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEventType
        '
        Me.cboEventType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEventType.DropDownWidth = 200
        Me.cboEventType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEventType.FormattingEnabled = True
        Me.cboEventType.Location = New System.Drawing.Point(298, 33)
        Me.cboEventType.Name = "cboEventType"
        Me.cboEventType.Size = New System.Drawing.Size(82, 21)
        Me.cboEventType.TabIndex = 92
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(453, 33)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpFromDate.TabIndex = 207
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(386, 36)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(61, 15)
        Me.lblFromDate.TabIndex = 208
        Me.lblFromDate.Text = "From Date"
        '
        'objbtnSearchUser
        '
        Me.objbtnSearchUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUser.BorderSelected = False
        Me.objbtnSearchUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUser.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUser.Location = New System.Drawing.Point(195, 33)
        Me.objbtnSearchUser.Name = "objbtnSearchUser"
        Me.objbtnSearchUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUser.TabIndex = 87
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(8, 36)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(44, 15)
        Me.lblUser.TabIndex = 1
        Me.lblUser.Text = "User"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.DropDownWidth = 200
        Me.cboUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(58, 33)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(131, 21)
        Me.cboUser.TabIndex = 2
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnExport)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1018, 55)
        Me.objFooter.TabIndex = 8
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(806, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(97, 30)
        Me.btnExport.TabIndex = 120
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(909, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvAuditData
        '
        Me.dgvAuditData.AllowUserToAddRows = False
        Me.dgvAuditData.AllowUserToDeleteRows = False
        Me.dgvAuditData.AllowUserToResizeRows = False
        Me.dgvAuditData.BackgroundColor = System.Drawing.Color.White
        Me.dgvAuditData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAuditData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvAuditData.ColumnHeadersHeight = 22
        Me.dgvAuditData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAuditData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAuditData.Location = New System.Drawing.Point(0, 0)
        Me.dgvAuditData.Name = "dgvAuditData"
        Me.dgvAuditData.ReadOnly = True
        Me.dgvAuditData.RowHeadersVisible = False
        Me.dgvAuditData.RowHeadersWidth = 5
        Me.dgvAuditData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAuditData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAuditData.Size = New System.Drawing.Size(698, 414)
        Me.dgvAuditData.TabIndex = 9
        '
        'tvModuleList
        '
        Me.tvModuleList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvModuleList.HideSelection = False
        Me.tvModuleList.Location = New System.Drawing.Point(0, 0)
        Me.tvModuleList.Name = "tvModuleList"
        Me.tvModuleList.Size = New System.Drawing.Size(300, 511)
        Me.tvModuleList.TabIndex = 10
        '
        'tblpnlData
        '
        Me.tblpnlData.ColumnCount = 2
        Me.tblpnlData.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 306.0!))
        Me.tblpnlData.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.tblpnlData.Controls.Add(Me.pnlTreeData, 0, 0)
        Me.tblpnlData.Controls.Add(Me.pnlViewData, 1, 0)
        Me.tblpnlData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblpnlData.Location = New System.Drawing.Point(0, 0)
        Me.tblpnlData.Name = "tblpnlData"
        Me.tblpnlData.RowCount = 1
        Me.tblpnlData.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpnlData.Size = New System.Drawing.Size(1018, 517)
        Me.tblpnlData.TabIndex = 11
        '
        'pnlTreeData
        '
        Me.pnlTreeData.Controls.Add(Me.tvModuleList)
        Me.pnlTreeData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTreeData.Location = New System.Drawing.Point(3, 3)
        Me.pnlTreeData.Name = "pnlTreeData"
        Me.pnlTreeData.Size = New System.Drawing.Size(300, 511)
        Me.pnlTreeData.TabIndex = 12
        '
        'pnlViewData
        '
        Me.pnlViewData.Controls.Add(Me.cboChildData)
        Me.pnlViewData.Controls.Add(Me.tabcData)
        Me.pnlViewData.Controls.Add(Me.gbGroupList)
        Me.pnlViewData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlViewData.Location = New System.Drawing.Point(309, 3)
        Me.pnlViewData.Name = "pnlViewData"
        Me.pnlViewData.Size = New System.Drawing.Size(706, 511)
        Me.pnlViewData.TabIndex = 13
        '
        'cboChildData
        '
        Me.cboChildData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChildData.FormattingEnabled = True
        Me.cboChildData.Location = New System.Drawing.Point(466, 68)
        Me.cboChildData.Name = "cboChildData"
        Me.cboChildData.Size = New System.Drawing.Size(236, 21)
        Me.cboChildData.TabIndex = 10
        '
        'tabcData
        '
        Me.tabcData.Controls.Add(Me.tabpMasterDetail)
        Me.tabcData.Controls.Add(Me.tabpChildDetail)
        Me.tabcData.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tabcData.Location = New System.Drawing.Point(0, 71)
        Me.tabcData.Margin = New System.Windows.Forms.Padding(0)
        Me.tabcData.Name = "tabcData"
        Me.tabcData.Padding = New System.Drawing.Point(0, 0)
        Me.tabcData.SelectedIndex = 0
        Me.tabcData.Size = New System.Drawing.Size(706, 440)
        Me.tabcData.TabIndex = 8
        '
        'tabpMasterDetail
        '
        Me.tabpMasterDetail.Controls.Add(Me.dgvAuditData)
        Me.tabpMasterDetail.Location = New System.Drawing.Point(4, 22)
        Me.tabpMasterDetail.Name = "tabpMasterDetail"
        Me.tabpMasterDetail.Size = New System.Drawing.Size(698, 414)
        Me.tabpMasterDetail.TabIndex = 0
        Me.tabpMasterDetail.Text = "Master Detail"
        Me.tabpMasterDetail.UseVisualStyleBackColor = True
        '
        'tabpChildDetail
        '
        Me.tabpChildDetail.Controls.Add(Me.lvData)
        Me.tabpChildDetail.Location = New System.Drawing.Point(4, 22)
        Me.tabpChildDetail.Margin = New System.Windows.Forms.Padding(0)
        Me.tabpChildDetail.Name = "tabpChildDetail"
        Me.tabpChildDetail.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpChildDetail.Size = New System.Drawing.Size(698, 414)
        Me.tabpChildDetail.TabIndex = 1
        Me.tabpChildDetail.UseVisualStyleBackColor = True
        '
        'lvData
        '
        Me.lvData.BackColorOnChecked = False
        Me.lvData.ColumnHeaders = Nothing
        Me.lvData.CompulsoryColumns = ""
        Me.lvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvData.FullRowSelect = True
        Me.lvData.GridLines = True
        Me.lvData.GroupingColumn = Nothing
        Me.lvData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvData.HideSelection = False
        Me.lvData.Location = New System.Drawing.Point(3, 3)
        Me.lvData.MinColumnWidth = 50
        Me.lvData.MultiSelect = False
        Me.lvData.Name = "lvData"
        Me.lvData.OptionalColumns = ""
        Me.lvData.ShowMoreItem = False
        Me.lvData.ShowSaveItem = False
        Me.lvData.ShowSelectAll = True
        Me.lvData.ShowSizeAllColumnsToFit = True
        Me.lvData.Size = New System.Drawing.Size(692, 408)
        Me.lvData.Sortable = True
        Me.lvData.TabIndex = 0
        Me.lvData.UseCompatibleStateImageBehavior = False
        Me.lvData.View = System.Windows.Forms.View.Details
        '
        'frmATLogview
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1018, 572)
        Me.Controls.Add(Me.tblpnlData)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmATLogview"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Application Events Log"
        Me.gbGroupList.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgvAuditData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblpnlData.ResumeLayout(False)
        Me.pnlTreeData.ResumeLayout(False)
        Me.pnlViewData.ResumeLayout(False)
        Me.tabcData.ResumeLayout(False)
        Me.tabpMasterDetail.ResumeLayout(False)
        Me.tabpChildDetail.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbGroupList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchUser As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEventType As System.Windows.Forms.Label
    Friend WithEvents cboEventType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTodate As System.Windows.Forms.Label
    Friend WithEvents dtpTodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dgvAuditData As System.Windows.Forms.DataGridView
    Friend WithEvents tvModuleList As System.Windows.Forms.TreeView
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents btnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents tblpnlData As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tabcData As System.Windows.Forms.TabControl
    Friend WithEvents tabpMasterDetail As System.Windows.Forms.TabPage
    Friend WithEvents tabpChildDetail As System.Windows.Forms.TabPage
    Friend WithEvents pnlTreeData As System.Windows.Forms.Panel
    Friend WithEvents pnlViewData As System.Windows.Forms.Panel
    Friend WithEvents cboChildData As System.Windows.Forms.ComboBox
    Friend WithEvents lvData As eZee.Common.eZeeListView
End Class
