﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmATEmail_LogView

#Region " Private Variables "

    Private mstrModuleName As String = "frmATEmail_LogView"
    Private dTable As DataTable
    Private mDicCols As Dictionary(Of Integer, String)
#End Region

#Region " Private Methods & Functions "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            With cboViewBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 100, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 105, "DeskTop"))
                Select Case gApplicationType
                    Case enArutiApplicatinType.Aruti_Payroll
                        .Items.Add(Language.getMessage(mstrModuleName, 106, "Employee Self Service"))
                        .Items.Add(Language.getMessage(mstrModuleName, 107, "Manager Self Service"))
                End Select
                .SelectedIndex = 0
            End With

            Dim objSMail As New clsSendMail
            dsList = objSMail.getComboList_AT(True, "List")
            With cboViewType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objSMail = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Grid()
        Try
            Dim objMail As New clsSendMail
            dTable = objMail.Generate_Log_List(CInt(cboViewType.SelectedValue), _
                                          dtpFromDate.Value.Date, _
                                          dtpTodate.Value.Date, _
                                          cboViewBy.SelectedIndex, _
                                          IIf(cboFilter.SelectedValue = Nothing, 0, cboFilter.SelectedValue), _
                                          txtModuleName.Text, _
                                          txtIPAddress.Text, _
                                          txtMachine.Text, _
                                          txtSubjectFilter.Text)

            If dTable IsNot Nothing Then
                dgvData.DataSource = dTable
                mDicCols = New Dictionary(Of Integer, String)
                If dgvData.ColumnCount > 0 Then
                    For iCol As Integer = 0 To dgvData.Columns.Count - 1
                        If mDicCols.ContainsKey(dgvData.Columns(iCol).Index) Then Continue For
                        mDicCols.Add(dgvData.Columns(iCol).Index, dgvData.Columns(iCol).Name)
                        If dgvData.Columns(iCol).Index <= 0 Then
                            dgvData.Columns(iCol).HeaderText = "" : dgvData.Columns(iCol).Width = 25
                            dgvData.Columns(iCol).Resizable = DataGridViewTriState.False
                            dgvData.Columns(iCol).Frozen = True
                        Else
                            dgvData.Columns(iCol).ReadOnly = True
                        End If
                        If CInt(cboViewType.SelectedValue) <> clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT Then
                            If dgvData.Columns(iCol).Name.ToUpper = "FILE ATTACHED" Then
                                dgvData.Columns(iCol).Visible = False
                            End If
                        End If

                        If dgvData.Columns(iCol).Name.ToUpper = "ISUB" Or dgvData.Columns(iCol).Name.ToUpper = "ICON" Or dgvData.Columns(iCol).Name.ToUpper = "IRG" Then
                            dgvData.Columns(iCol).Visible = False
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Store_User_Log(ByVal iSearch As Boolean) As Boolean
        Dim objViewAT As New clsView_AT_Logs
        Try
            objViewAT._Atviewmodeid = clsView_AT_Logs.enAT_View_Mode.EMAIL_ATVIEW_LOG
            objViewAT._Userunkid = User._Object._Userunkid
            objViewAT._Viewdatetime = ConfigParameter._Object._CurrentDateAndTime
            If iSearch = True Then
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.SEARCH
            Else
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.DELETE
            End If
            Dim StrXML_Value As String = ""
            StrXML_Value = "<" & Me.Name & " xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbCrLf
            If cboViewType.SelectedIndex > 0 Then
                StrXML_Value &= "<" & cboViewType.Name & ">" & CInt(cboViewType.SelectedIndex) & "</" & cboViewType.Name & ">" & vbCrLf
            End If
            For Each ctrl As Control In gbGroupList.Controls
                If TypeOf ctrl Is DateTimePicker Then
                    StrXML_Value &= "<" & ctrl.Name & ">" & eZeeDate.convertDate(CType(ctrl, DateTimePicker).Value) & "</" & ctrl.Name & ">" & vbCrLf
                ElseIf TypeOf ctrl Is ComboBox Then
                    If CType(ctrl, ComboBox).SelectedIndex >= 0 Then
                        If CType(ctrl, ComboBox).DataSource IsNot Nothing Then
                            StrXML_Value &= "<" & ctrl.Name & ">" & CInt(CType(ctrl, ComboBox).SelectedValue) & "</" & ctrl.Name & ">" & vbCrLf
                        Else
                            StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, ComboBox).SelectedIndex & "</" & ctrl.Name & ">" & vbCrLf
                        End If
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                ElseIf TypeOf ctrl Is TextBox Then
                    If CType(ctrl, TextBox).Text <> "" Then
                        StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, TextBox).Text & "</" & ctrl.Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                End If
            Next
            'S.SANDEEP [ 17 OCT 2014 ] -- START
            StrXML_Value &= "<IP>" & getIP() & "</IP>" & vbCrLf
            StrXML_Value &= "<Host>" & getHostName() & "</Host>" & vbCrLf
            'S.SANDEEP [ 17 OCT 2014 ] -- END
            StrXML_Value &= "</" & Me.Name & ">"
            objViewAT._Value_View = StrXML_Value

            If objViewAT.Insert = False Then
                eZeeMsgBox.Show(objViewAT._Message, enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Store_User_Log", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmATEmail_LogView_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call btnReset_Click(sender, e)
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteEmailNotificationAuditTrails
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmATEmail_LogView_Load", mstrModuleName)
        Finally
        End Try
    End Sub
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If cboViewType.SelectedIndex <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "View Type is mandatory information. Please select view type."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call Fill_Grid()
            If dgvData.RowCount > 0 Then
                objchkAll.Visible = True
            Else
                objwbContent.Navigate("about:blank") : objwbContent.Document.OpenNew(False)
                objwbContent.Refresh() : txtSubject.Text = ""
            End If
            Call Store_User_Log(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            If cboViewBy.SelectedIndex <= 0 Then
                If dTable IsNot Nothing Then dTable = Nothing
                dgvData.DataSource = Nothing : objchkAll.Visible = False
                RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
                objchkAll.Checked = False
                AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
                objwbContent.Navigate("about:blank") : objwbContent.Document.OpenNew(False) : objwbContent.Refresh()
                txtSubject.Text = ""
            End If
            cboViewBy.SelectedIndex = 0
            txtIPAddress.Text = ""
            txtMachine.Text = ""
            txtModuleName.Text = ""
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpTodate.Value = ConfigParameter._Object._CurrentDateAndTime
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboFilter.ValueMember
                .DisplayMember = cboFilter.DisplayMember
                Select Case gApplicationType
                    Case enArutiApplicatinType.Aruti_Payroll
                        If cboViewBy.SelectedIndex = 2 Then
                            .CodeMember = "employeecode"
                        End If

                End Select
                .DataSource = CType(cboFilter.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboFilter.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dTable IsNot Nothing Then
                Dim dtmp() As DataRow = dTable.Select("iCol=true")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please tick atleast one of the email notification in order to delete."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Alert! You are about to delete notification log between the selected date range." & vbCrLf & _
                                   "This will permanently delete the selected log and the deleted log cannot be recovered in any circumstances." & vbCrLf & _
                                   "Do you wish to continue?"), enMsgBoxStyle.Exclamation + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
                    Dim objMail As New clsSendMail
                    If objMail.Delete_Email_Log(CInt(cboViewType.SelectedValue), dtpFromDate.Value.Date, dtpTodate.Value.Date) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Email Notification Log deleted successfully."), enMsgBoxStyle.Information)
                        Call Fill_Grid()
                        Call Store_User_Log(False)
                        objwbContent.Navigate("about:blank") : objwbContent.Document.OpenNew(False) : objwbContent.Refresh()
                        txtSubject.Text = ""
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Problem in deleting notification log."), enMsgBoxStyle.Information)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Select Case cboViewBy.SelectedIndex
                Case 0  'NONE
                    objlblCaption.Enabled = False
                    cboFilter.Enabled = False
                    objbtnSearch.Enabled = False
                    cboFilter.SelectedValue = 0
                Case 1, 3  'DESKTOP,MANAGER SELF SERVICE 
                    cboFilter.DataSource = Nothing
                    objlblCaption.Enabled = True
                    cboFilter.Enabled = True
                    objbtnSearch.Enabled = True
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 200, "User")
                    Dim objFUser As New clsUserAddEdit

                    'S.SANDEEP [10 AUG 2015] -- START
                    'ENHANCEMENT : Aruti SaaS Changes
                    'Dim objOption As New clsPassowdOptions
                    'Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
                    'If drOption.Length > 0 Then
                    '    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                    '        Dim objPswd As New clsPassowdOptions
                    '        If objPswd._IsEmployeeAsUser Then
                    '            dsList = objFUser.getComboList("List", True, False, True)
                    '        Else
                    '            dsList = objFUser.getComboList("List", True, False)
                    '        End If
                    '        objPswd = Nothing
                    '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
                    '        dsList = objFUser.getComboList("List", True, True)
                    '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                    '        dsList = objFUser.getComboList("List", True, True)
                    '    End If
                    'Else
                    '    dsList = objFUser.getComboList("List", True, False)
                    'End If

                    'Nilay (01-Mar-2016) -- Start
                    'dsList = objFUser.getNewComboList("List", , True)
                    dsList = objFUser.getNewComboList("List", , True, , , , True)
                    'Nilay (01-Mar-2016) -- End

                    'S.SANDEEP [10 AUG 2015] -- END

                    objFUser = Nothing
                    With cboFilter
                        .ValueMember = "userunkid"
                        .DisplayMember = "name"
                    End With
                    cboFilter.DataSource = dsList.Tables("List")
                    cboFilter.SelectedValue = 0
                Case 2  'EMPLOYEE SELF SERVICE
                    cboFilter.DataSource = Nothing
                    objlblCaption.Enabled = True
                    cboFilter.Enabled = True
                    objbtnSearch.Enabled = True
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 201, "Employee")
                    Dim objEmp As New clsEmployee_Master
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsList = objEmp.GetEmployeeList("List", True)
                    dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, False, "List", True)
                    'S.SANDEEP [04 JUN 2015] -- END
                    With cboFilter
                        .ValueMember = "employeeunkid"
                        .DisplayMember = "employeename"
                    End With
                    cboFilter.DataSource = dsList.Tables("List")
                    cboFilter.SelectedValue = 0
            End Select
            If dTable IsNot Nothing Then dTable = Nothing
            dgvData.DataSource = Nothing : objchkAll.Visible = False
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            objchkAll.Checked = False
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            objwbContent.Navigate("about:blank") : objwbContent.Document.OpenNew(False) : objwbContent.Refresh()
            txtSubject.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvData.SelectionChanged
        Try
            If dgvData.SelectedRows.Count > 0 Then
                txtSubject.Text = dgvData.Rows(dgvData.SelectedRows(0).Index).Cells("iSub").Value.ToString()
                objwbContent.Navigate("about:blank") : objwbContent.Document.OpenNew(False)
                objwbContent.Document.Write(dgvData.Rows(dgvData.SelectedRows(0).Index).Cells("iCon").Value.ToString())
                objwbContent.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_SelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            If dTable IsNot Nothing Then
                For Each dRow As DataRow In dTable.Rows
                    dRow.Item("iCol") = CBool(objchkAll.CheckState)
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbGroupList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbGroupList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbGroupList.Text = Language._Object.getCaption(Me.gbGroupList.Name, Me.gbGroupList.Text)
            Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)
            Me.lblModuleName.Text = Language._Object.getCaption(Me.lblModuleName.Name, Me.lblModuleName.Text)
            Me.lblMachine.Text = Language._Object.getCaption(Me.lblMachine.Name, Me.lblMachine.Text)
            Me.lblIPAddress.Text = Language._Object.getCaption(Me.lblIPAddress.Name, Me.lblIPAddress.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.Name, Me.btnSearch.Text)
            Me.lblTodate.Text = Language._Object.getCaption(Me.lblTodate.Name, Me.lblTodate.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblSubjectFilter.Text = Language._Object.getCaption(Me.lblSubjectFilter.Name, Me.lblSubjectFilter.Text)
            Me.lblSubject.Text = Language._Object.getCaption(Me.lblSubject.Name, Me.lblSubject.Text)
            Me.lblViewType.Text = Language._Object.getCaption(Me.lblViewType.Name, Me.lblViewType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Please tick atleast one of the email notification in order to delete.")
            Language.setMessage(mstrModuleName, 2, "Alert! You are about to delete notification log between the selected date range." & vbCrLf & _
                                            "This will permanently delete the selected log and the deleted log cannot be recovered in any circumstances." & vbCrLf & _
                                            "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 3, "Email Notification Log deleted successfully.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Problem in deleting notification log.")
            Language.setMessage(mstrModuleName, 5, "View Type is mandatory information. Please select view type.")
            Language.setMessage(mstrModuleName, 100, "Select")
            Language.setMessage(mstrModuleName, 105, "DeskTop")
            Language.setMessage(mstrModuleName, 106, "Employee Self Service")
            Language.setMessage(mstrModuleName, 107, "Manager Self Service")
            Language.setMessage(mstrModuleName, 200, "User")
            Language.setMessage(mstrModuleName, 201, "Employee")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class