﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmCountry

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmCountry"
    Private mdtList As DataTable
    Private mstrSearchText As String = ""

#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet = Nothing
        Try
            objlblCount.Text = "( 0 / 0 )"

            dsList = objMaster.getCountryList("List", False)

            colhAlias.DataPropertyName = "Alias"
            colhCountry.DataPropertyName = "country_name"
            colhCurrency.DataPropertyName = "currency"
            objdgcolhCountryUnkId.DataPropertyName = "countryunkid"
            colhCurrency.ReadOnly = Not User._Object.Privilege._AllowToEditCurrencySign

            mdtList = dsList.Tables(0)

            dgvCountry.AutoGenerateColumns = False
            objlblCount.Text = "( 0 / " & mdtList.Rows.Count.ToString & " )"
            dgvCountry.DataSource = mdtList.DefaultView
            mdtList.DefaultView.Sort = "country_name "

            mstrSearchText = Language.getMessage(mstrModuleName, 1, "Type to Search")
            Call txtSearch_Leave(txtSearch, New System.EventArgs)


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            'btnNew.Enabled = User._Object.Privilege._AddCostCenter
            'btnEdit.Enabled = User._Object.Privilege._EditCostCenter
            'btnDelete.Enabled = User._Object.Privilege._DeleteCostCenter


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 1, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmCountry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call SetVisibility()

            Call FillList()

            dgvCountry.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCountry_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCountry_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            'If e.KeyCode = Keys.Delete And dgvCostcenterCodes.Focused = True Then
            '    Call btnDelete.PerformClick()
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCountry_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCountry_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMasterData.SetMessages()
            objfrm._Other_ModuleNames = "clsMasterData"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Textbox Events "

    Private Sub txtSearch_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.GotFocus
        Try
            With txtSearch
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.Leave
        Try
            If txtSearch.Text.Trim = "" Then
                With txtSearch
                    .ForeColor = Color.Gray
                    .Text = mstrSearchText
                    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If txtSearch.Text.Trim = mstrSearchText Then Exit Sub
            If mdtList IsNot Nothing AndAlso mdtList.Rows.Count > 0 Then
                mdtList.DefaultView.RowFilter = "country_name LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'  OR alias LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' OR currency LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' "
            Else
                mdtList.DefaultView.RowFilter = ""
            End If
            dgvCountry.Refresh()
            objlblCount.Text = "( " & dgvCountry.Rows.Count.ToString & " / " & mdtList.Rows.Count.ToString & " )"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " DataGrid Events "

    Private Sub dgvCountry_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCountry.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case colhCurrency.Index
                    SendKeys.Send("{F2}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvCountry_CellEnter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvCountry_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvCountry.EditingControlShowing
        Try
            Select Case dgvCountry.CurrentCell.ColumnIndex
                Case colhCurrency.Index
                    If e.Control IsNot Nothing Then
                        Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                        'RemoveHandler txt.KeyPress, AddressOf tb_keypress
                        'AddHandler txt.KeyPress, AddressOf tb_keypress
                    End If
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvCountry_EditingControlShowing", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvCountry_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvCountry.CellValidating
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If dgvCountry.Focused = False Then Exit Sub

            Select Case e.ColumnIndex
                Case colhCurrency.Index

                    If e.FormattedValue.ToString.Trim = "" AndAlso CBool(User._Object.Privilege._AllowToEditCurrencySign) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Currency sign is mandatory. Please enter currency sign."), enMsgBoxStyle.Information)
                        e.Cancel = True
                        Exit Sub
                    End If

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvCountry_CellValidating", mstrModuleName)
        Finally
           
        End Try
    End Sub

    Private Sub dgvCountry_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCountry.CellValueChanged
        Try
            If e.RowIndex <= -1 Then Exit Sub

            RemoveHandler dgvCountry.CellValueChanged, AddressOf dgvCountry_CellValueChanged

            Select Case e.ColumnIndex
                Case colhCurrency.Index
                    If dgvCountry.Rows(e.RowIndex).Cells(colhCurrency.Index).Value.ToString.Trim <> "" Then
                        Dim objCountry As New clsCountry
                        objCountry._CountryUnkid = CInt(dgvCountry.Rows(e.RowIndex).Cells(objdgcolhCountryUnkId.Index).Value)
                        objCountry._CurrencySign = dgvCountry.Rows(e.RowIndex).Cells(colhCurrency.Index).Value.ToString
                        If objCountry.UpdateCountryCurrency(objCountry, Nothing) = False Then
                            Throw New Exception()
                        End If
                    End If
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvCountry_CellValueChanged", mstrModuleName)
        Finally
            RemoveHandler dgvCountry.CellValueChanged, AddressOf dgvCountry_CellValueChanged
            AddHandler dgvCountry.CellValueChanged, AddressOf dgvCountry_CellValueChanged
        End Try
    End Sub

    Private Sub dgvCountry_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvCountry.DataError

    End Sub

#End Region


    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhAlias.HeaderText = Language._Object.getCaption(Me.colhAlias.Name, Me.colhAlias.HeaderText)
			Me.colhCountry.HeaderText = Language._Object.getCaption(Me.colhCountry.Name, Me.colhCountry.HeaderText)
			Me.colhCurrency.HeaderText = Language._Object.getCaption(Me.colhCurrency.Name, Me.colhCurrency.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Type to Search")
			Language.setMessage(mstrModuleName, 2, "Sorry, Currency sign is mandatory. Please enter currency sign.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class