﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCountry
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCountry))
        Me.pnlState = New System.Windows.Forms.Panel
        Me.objlblCount = New System.Windows.Forms.Label
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.dgvCountry = New System.Windows.Forms.DataGridView
        Me.objdgColhBlank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAlias = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrency = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCountryUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlState.SuspendLayout()
        CType(Me.dgvCountry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlState
        '
        Me.pnlState.Controls.Add(Me.objlblCount)
        Me.pnlState.Controls.Add(Me.txtSearch)
        Me.pnlState.Controls.Add(Me.dgvCountry)
        Me.pnlState.Controls.Add(Me.objFooter)
        Me.pnlState.Controls.Add(Me.eZeeHeader)
        Me.pnlState.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlState.Location = New System.Drawing.Point(0, 0)
        Me.pnlState.Name = "pnlState"
        Me.pnlState.Size = New System.Drawing.Size(684, 461)
        Me.pnlState.TabIndex = 1
        '
        'objlblCount
        '
        Me.objlblCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCount.Location = New System.Drawing.Point(574, 68)
        Me.objlblCount.Name = "objlblCount"
        Me.objlblCount.Size = New System.Drawing.Size(95, 16)
        Me.objlblCount.TabIndex = 163
        Me.objlblCount.Text = "( 0 / 0 )"
        Me.objlblCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearch.Location = New System.Drawing.Point(12, 66)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(556, 21)
        Me.txtSearch.TabIndex = 162
        '
        'dgvCountry
        '
        Me.dgvCountry.AllowUserToAddRows = False
        Me.dgvCountry.AllowUserToDeleteRows = False
        Me.dgvCountry.AllowUserToResizeRows = False
        Me.dgvCountry.BackgroundColor = System.Drawing.Color.White
        Me.dgvCountry.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvCountry.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgColhBlank, Me.colhAlias, Me.colhCountry, Me.colhCurrency, Me.objdgcolhCountryUnkId})
        Me.dgvCountry.Location = New System.Drawing.Point(12, 93)
        Me.dgvCountry.Name = "dgvCountry"
        Me.dgvCountry.RowHeadersVisible = False
        Me.dgvCountry.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvCountry.Size = New System.Drawing.Size(664, 307)
        Me.dgvCountry.TabIndex = 126
        '
        'objdgColhBlank
        '
        Me.objdgColhBlank.HeaderText = ""
        Me.objdgColhBlank.Name = "objdgColhBlank"
        Me.objdgColhBlank.ReadOnly = True
        Me.objdgColhBlank.Visible = False
        Me.objdgColhBlank.Width = 30
        '
        'colhAlias
        '
        Me.colhAlias.HeaderText = "Alias"
        Me.colhAlias.Name = "colhAlias"
        Me.colhAlias.ReadOnly = True
        Me.colhAlias.Width = 120
        '
        'colhCountry
        '
        Me.colhCountry.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhCountry.HeaderText = "Country"
        Me.colhCountry.Name = "colhCountry"
        Me.colhCountry.ReadOnly = True
        '
        'colhCurrency
        '
        Me.colhCurrency.HeaderText = "Currency Sign"
        Me.colhCurrency.Name = "colhCurrency"
        Me.colhCurrency.ReadOnly = True
        Me.colhCurrency.Width = 120
        '
        'objdgcolhCountryUnkId
        '
        Me.objdgcolhCountryUnkId.HeaderText = "objdgcolhCountryUnkId"
        Me.objdgcolhCountryUnkId.Name = "objdgcolhCountryUnkId"
        Me.objdgcolhCountryUnkId.ReadOnly = True
        Me.objdgcolhCountryUnkId.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 406)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(684, 55)
        Me.objFooter.TabIndex = 10
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.CausesValidation = False
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(583, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(684, 60)
        Me.eZeeHeader.TabIndex = 7
        Me.eZeeHeader.Title = "Edit Currency Sign"
        '
        'frmCountry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(684, 461)
        Me.Controls.Add(Me.pnlState)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCountry"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit Currency Sign"
        Me.pnlState.ResumeLayout(False)
        Me.pnlState.PerformLayout()
        CType(Me.dgvCountry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlState As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents dgvCountry As System.Windows.Forms.DataGridView
    Friend WithEvents objlblCount As System.Windows.Forms.Label
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objdgColhBlank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAlias As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrency As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCountryUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
