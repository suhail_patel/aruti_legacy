﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonImportData
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonImportData))
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.BtnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.BtnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMasterList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlMasterTableList = New System.Windows.Forms.Panel
        Me.lvMasterList = New System.Windows.Forms.ListView
        Me.colhMasterList = New System.Windows.Forms.ColumnHeader
        Me.colhTotalFields = New System.Windows.Forms.ColumnHeader
        Me.colhIsImport = New System.Windows.Forms.ColumnHeader
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.pnlControl = New System.Windows.Forms.Panel
        Me.EZeeFooter1.SuspendLayout()
        Me.gbMasterList.SuspendLayout()
        Me.pnlMasterTableList.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(717, 72)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 20)
        Me.btnOpenFile.TabIndex = 16
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(365, 71)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(346, 21)
        Me.txtFilePath.TabIndex = 15
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(253, 72)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(96, 20)
        Me.lblSelectfile.TabIndex = 14
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = "Description"
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(839, 60)
        Me.EZeeHeader1.TabIndex = 31
        Me.EZeeHeader1.Title = "HeaderText"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.BtnImport)
        Me.EZeeFooter1.Controls.Add(Me.BtnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 491)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(839, 55)
        Me.EZeeFooter1.TabIndex = 32
        '
        'BtnImport
        '
        Me.BtnImport.BackColor = System.Drawing.Color.White
        Me.BtnImport.BackgroundImage = CType(resources.GetObject("BtnImport.BackgroundImage"), System.Drawing.Image)
        Me.BtnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnImport.BorderColor = System.Drawing.Color.Empty
        Me.BtnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.BtnImport.FlatAppearance.BorderSize = 0
        Me.BtnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnImport.ForeColor = System.Drawing.Color.Black
        Me.BtnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BtnImport.GradientForeColor = System.Drawing.Color.Black
        Me.BtnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.BtnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.BtnImport.Location = New System.Drawing.Point(641, 13)
        Me.BtnImport.Name = "BtnImport"
        Me.BtnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.BtnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.BtnImport.Size = New System.Drawing.Size(90, 30)
        Me.BtnImport.TabIndex = 2
        Me.BtnImport.Text = "&Import"
        Me.BtnImport.UseVisualStyleBackColor = True
        '
        'BtnClose
        '
        Me.BtnClose.BackColor = System.Drawing.Color.White
        Me.BtnClose.BackgroundImage = CType(resources.GetObject("BtnClose.BackgroundImage"), System.Drawing.Image)
        Me.BtnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnClose.BorderColor = System.Drawing.Color.Empty
        Me.BtnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.BtnClose.FlatAppearance.BorderSize = 0
        Me.BtnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnClose.ForeColor = System.Drawing.Color.Black
        Me.BtnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BtnClose.GradientForeColor = System.Drawing.Color.Black
        Me.BtnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.BtnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.BtnClose.Location = New System.Drawing.Point(737, 13)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.BtnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.BtnClose.Size = New System.Drawing.Size(90, 30)
        Me.BtnClose.TabIndex = 1
        Me.BtnClose.Text = "&Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'gbMasterList
        '
        Me.gbMasterList.BorderColor = System.Drawing.Color.Black
        Me.gbMasterList.Checked = False
        Me.gbMasterList.CollapseAllExceptThis = False
        Me.gbMasterList.CollapsedHoverImage = Nothing
        Me.gbMasterList.CollapsedNormalImage = Nothing
        Me.gbMasterList.CollapsedPressedImage = Nothing
        Me.gbMasterList.CollapseOnLoad = False
        Me.gbMasterList.Controls.Add(Me.pnlMasterTableList)
        Me.gbMasterList.ExpandedHoverImage = Nothing
        Me.gbMasterList.ExpandedNormalImage = Nothing
        Me.gbMasterList.ExpandedPressedImage = Nothing
        Me.gbMasterList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMasterList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMasterList.HeaderHeight = 25
        Me.gbMasterList.HeightOnCollapse = 0
        Me.gbMasterList.LeftTextSpace = 0
        Me.gbMasterList.Location = New System.Drawing.Point(2, 59)
        Me.gbMasterList.Name = "gbMasterList"
        Me.gbMasterList.OpenHeight = 336
        Me.gbMasterList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMasterList.ShowBorder = True
        Me.gbMasterList.ShowCheckBox = False
        Me.gbMasterList.ShowCollapseButton = False
        Me.gbMasterList.ShowDefaultBorderColor = True
        Me.gbMasterList.ShowDownButton = False
        Me.gbMasterList.ShowHeader = True
        Me.gbMasterList.Size = New System.Drawing.Size(240, 431)
        Me.gbMasterList.TabIndex = 33
        Me.gbMasterList.Temp = 0
        Me.gbMasterList.Text = "Master Lists"
        Me.gbMasterList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMasterTableList
        '
        Me.pnlMasterTableList.Controls.Add(Me.lvMasterList)
        Me.pnlMasterTableList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMasterTableList.Location = New System.Drawing.Point(1, 25)
        Me.pnlMasterTableList.Name = "pnlMasterTableList"
        Me.pnlMasterTableList.Size = New System.Drawing.Size(237, 401)
        Me.pnlMasterTableList.TabIndex = 1
        '
        'lvMasterList
        '
        Me.lvMasterList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhMasterList, Me.colhTotalFields, Me.colhIsImport})
        Me.lvMasterList.FullRowSelect = True
        Me.lvMasterList.GridLines = True
        Me.lvMasterList.Location = New System.Drawing.Point(0, 0)
        Me.lvMasterList.MultiSelect = False
        Me.lvMasterList.Name = "lvMasterList"
        Me.lvMasterList.Size = New System.Drawing.Size(237, 402)
        Me.lvMasterList.TabIndex = 0
        Me.lvMasterList.UseCompatibleStateImageBehavior = False
        Me.lvMasterList.View = System.Windows.Forms.View.Details
        '
        'colhMasterList
        '
        Me.colhMasterList.Text = "Masters"
        Me.colhMasterList.Width = 232
        '
        'colhTotalFields
        '
        Me.colhTotalFields.Tag = "colhTotalFields"
        Me.colhTotalFields.Text = "Total Fields"
        Me.colhTotalFields.Width = 0
        '
        'colhIsImport
        '
        Me.colhIsImport.Tag = "colhIsImport"
        Me.colhIsImport.Text = "Import"
        Me.colhIsImport.Width = 0
        '
        'objLine1
        '
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(246, 98)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(581, 13)
        Me.objLine1.TabIndex = 34
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlControl
        '
        Me.pnlControl.AutoScroll = True
        Me.pnlControl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlControl.Location = New System.Drawing.Point(249, 114)
        Me.pnlControl.Name = "pnlControl"
        Me.pnlControl.Size = New System.Drawing.Size(578, 371)
        Me.pnlControl.TabIndex = 36
        '
        'frmCommonImportData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 546)
        Me.Controls.Add(Me.pnlControl)
        Me.Controls.Add(Me.objLine1)
        Me.Controls.Add(Me.gbMasterList)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.EZeeHeader1)
        Me.Controls.Add(Me.btnOpenFile)
        Me.Controls.Add(Me.txtFilePath)
        Me.Controls.Add(Me.lblSelectfile)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        '  Me.HelpProvider1.SetHelpNavigator(Me, System.Windows.Forms.HelpNavigator.Topic)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommonImportData"
        '    Me.HelpProvider1.SetShowHelp(Me, True)
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Common Import"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbMasterList.ResumeLayout(False)
        Me.pnlMasterTableList.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents gbMasterList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlMasterTableList As System.Windows.Forms.Panel
    Friend WithEvents lvMasterList As System.Windows.Forms.ListView
    Friend WithEvents colhMasterList As System.Windows.Forms.ColumnHeader
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents pnlControl As System.Windows.Forms.Panel
    Friend WithEvents colhTotalFields As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIsImport As System.Windows.Forms.ColumnHeader
    Friend WithEvents BtnImport As eZee.Common.eZeeLightButton
    Friend WithEvents BtnClose As eZee.Common.eZeeLightButton
End Class
