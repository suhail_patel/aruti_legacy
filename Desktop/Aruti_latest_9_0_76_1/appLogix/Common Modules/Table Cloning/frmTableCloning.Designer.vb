﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTableCloning
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTableCloning))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objlblNote = New System.Windows.Forms.Label
        Me.btnStart = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnStop = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblSelectCompany = New System.Windows.Forms.Label
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.txtDatabaseName = New System.Windows.Forms.TextBox
        Me.lblDatabase = New System.Windows.Forms.Label
        Me.gbCompanyInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.gbItemInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objChkICheck = New System.Windows.Forms.CheckBox
        Me.lvItemInformation = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhICheck = New System.Windows.Forms.ColumnHeader
        Me.objcolhcgid = New System.Windows.Forms.ColumnHeader
        Me.objcolhIGrp = New System.Windows.Forms.ColumnHeader
        Me.objcolhtablename = New System.Windows.Forms.ColumnHeader
        Me.colhItemList = New System.Windows.Forms.ColumnHeader
        Me.objcolhMtypeId = New System.Windows.Forms.ColumnHeader
        Me.gbDestinationInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objchkDCheck = New System.Windows.Forms.CheckBox
        Me.lvDestinationInfo = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhDCheck = New System.Windows.Forms.ColumnHeader
        Me.objcolhDGrp = New System.Windows.Forms.ColumnHeader
        Me.colhDestination = New System.Windows.Forms.ColumnHeader
        Me.objcolhsdate = New System.Windows.Forms.ColumnHeader
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.tslblCurrentProgress = New System.Windows.Forms.ToolStripStatusLabel
        Me.tspbCurrProgress = New System.Windows.Forms.ToolStripProgressBar
        Me.objSep1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.tslblOverallProgress = New System.Windows.Forms.ToolStripStatusLabel
        Me.tspbOverallProgress = New System.Windows.Forms.ToolStripProgressBar
        Me.objCurrProgress = New System.Windows.Forms.ToolStripStatusLabel
        Me.objOvrProgress = New System.Windows.Forms.ToolStripStatusLabel
        Me.EZeeFooter1.SuspendLayout()
        Me.gbCompanyInfo.SuspendLayout()
        Me.gbItemInformation.SuspendLayout()
        Me.gbDestinationInfo.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objlblNote)
        Me.EZeeFooter1.Controls.Add(Me.btnStart)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnStop)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 401)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(684, 50)
        Me.EZeeFooter1.TabIndex = 1
        '
        'objlblNote
        '
        Me.objlblNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblNote.ForeColor = System.Drawing.Color.DarkRed
        Me.objlblNote.Location = New System.Drawing.Point(8, 10)
        Me.objlblNote.Name = "objlblNote"
        Me.objlblNote.Size = New System.Drawing.Size(464, 30)
        Me.objlblNote.TabIndex = 3
        Me.objlblNote.Text = "Note : Existing Data will be overwritten on destination side, We recommend you to" & _
            " use newly created database(s). For Payroll && Performace Appraisal"
        '
        'btnStart
        '
        Me.btnStart.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStart.BackColor = System.Drawing.Color.White
        Me.btnStart.BackgroundImage = CType(resources.GetObject("btnStart.BackgroundImage"), System.Drawing.Image)
        Me.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnStart.BorderColor = System.Drawing.Color.Empty
        Me.btnStart.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnStart.FlatAppearance.BorderSize = 0
        Me.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStart.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStart.ForeColor = System.Drawing.Color.Black
        Me.btnStart.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnStart.GradientForeColor = System.Drawing.Color.Black
        Me.btnStart.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStart.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnStart.Location = New System.Drawing.Point(478, 11)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStart.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnStart.Size = New System.Drawing.Size(94, 30)
        Me.btnStart.TabIndex = 1
        Me.btnStart.Text = "&Start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(578, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnStop
        '
        Me.btnStop.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStop.BackColor = System.Drawing.Color.White
        Me.btnStop.BackgroundImage = CType(resources.GetObject("btnStop.BackgroundImage"), System.Drawing.Image)
        Me.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnStop.BorderColor = System.Drawing.Color.Empty
        Me.btnStop.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnStop.FlatAppearance.BorderSize = 0
        Me.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStop.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStop.ForeColor = System.Drawing.Color.Black
        Me.btnStop.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnStop.GradientForeColor = System.Drawing.Color.Black
        Me.btnStop.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStop.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnStop.Location = New System.Drawing.Point(478, 11)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStop.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnStop.Size = New System.Drawing.Size(94, 30)
        Me.btnStop.TabIndex = 2
        Me.btnStop.Text = "St&op"
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'lblSelectCompany
        '
        Me.lblSelectCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectCompany.Location = New System.Drawing.Point(12, 35)
        Me.lblSelectCompany.Name = "lblSelectCompany"
        Me.lblSelectCompany.Size = New System.Drawing.Size(88, 16)
        Me.lblSelectCompany.TabIndex = 2
        Me.lblSelectCompany.Text = "Select Company"
        Me.lblSelectCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(106, 33)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(211, 21)
        Me.cboCompany.TabIndex = 3
        '
        'txtDatabaseName
        '
        Me.txtDatabaseName.BackColor = System.Drawing.SystemColors.Info
        Me.txtDatabaseName.Enabled = False
        Me.txtDatabaseName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDatabaseName.Location = New System.Drawing.Point(106, 60)
        Me.txtDatabaseName.Name = "txtDatabaseName"
        Me.txtDatabaseName.ReadOnly = True
        Me.txtDatabaseName.Size = New System.Drawing.Size(211, 21)
        Me.txtDatabaseName.TabIndex = 60
        '
        'lblDatabase
        '
        Me.lblDatabase.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatabase.Location = New System.Drawing.Point(12, 62)
        Me.lblDatabase.Name = "lblDatabase"
        Me.lblDatabase.Size = New System.Drawing.Size(88, 16)
        Me.lblDatabase.TabIndex = 61
        Me.lblDatabase.Text = "Database"
        Me.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbCompanyInfo
        '
        Me.gbCompanyInfo.BorderColor = System.Drawing.Color.Black
        Me.gbCompanyInfo.Checked = False
        Me.gbCompanyInfo.CollapseAllExceptThis = False
        Me.gbCompanyInfo.CollapsedHoverImage = Nothing
        Me.gbCompanyInfo.CollapsedNormalImage = Nothing
        Me.gbCompanyInfo.CollapsedPressedImage = Nothing
        Me.gbCompanyInfo.CollapseOnLoad = False
        Me.gbCompanyInfo.Controls.Add(Me.cboCompany)
        Me.gbCompanyInfo.Controls.Add(Me.lblSelectCompany)
        Me.gbCompanyInfo.Controls.Add(Me.lblDatabase)
        Me.gbCompanyInfo.Controls.Add(Me.txtDatabaseName)
        Me.gbCompanyInfo.ExpandedHoverImage = Nothing
        Me.gbCompanyInfo.ExpandedNormalImage = Nothing
        Me.gbCompanyInfo.ExpandedPressedImage = Nothing
        Me.gbCompanyInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCompanyInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCompanyInfo.HeaderHeight = 25
        Me.gbCompanyInfo.HeaderMessage = ""
        Me.gbCompanyInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbCompanyInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCompanyInfo.HeightOnCollapse = 0
        Me.gbCompanyInfo.LeftTextSpace = 0
        Me.gbCompanyInfo.Location = New System.Drawing.Point(3, 3)
        Me.gbCompanyInfo.Name = "gbCompanyInfo"
        Me.gbCompanyInfo.OpenHeight = 300
        Me.gbCompanyInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCompanyInfo.ShowBorder = True
        Me.gbCompanyInfo.ShowCheckBox = False
        Me.gbCompanyInfo.ShowCollapseButton = False
        Me.gbCompanyInfo.ShowDefaultBorderColor = True
        Me.gbCompanyInfo.ShowDownButton = False
        Me.gbCompanyInfo.ShowHeader = True
        Me.gbCompanyInfo.Size = New System.Drawing.Size(329, 88)
        Me.gbCompanyInfo.TabIndex = 62
        Me.gbCompanyInfo.Temp = 0
        Me.gbCompanyInfo.Text = "Source Information"
        Me.gbCompanyInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbItemInformation
        '
        Me.gbItemInformation.BorderColor = System.Drawing.Color.Black
        Me.gbItemInformation.Checked = False
        Me.gbItemInformation.CollapseAllExceptThis = False
        Me.gbItemInformation.CollapsedHoverImage = Nothing
        Me.gbItemInformation.CollapsedNormalImage = Nothing
        Me.gbItemInformation.CollapsedPressedImage = Nothing
        Me.gbItemInformation.CollapseOnLoad = False
        Me.gbItemInformation.Controls.Add(Me.objChkICheck)
        Me.gbItemInformation.Controls.Add(Me.lvItemInformation)
        Me.gbItemInformation.ExpandedHoverImage = Nothing
        Me.gbItemInformation.ExpandedNormalImage = Nothing
        Me.gbItemInformation.ExpandedPressedImage = Nothing
        Me.gbItemInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbItemInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbItemInformation.HeaderHeight = 25
        Me.gbItemInformation.HeaderMessage = ""
        Me.gbItemInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbItemInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbItemInformation.HeightOnCollapse = 0
        Me.gbItemInformation.LeftTextSpace = 0
        Me.gbItemInformation.Location = New System.Drawing.Point(333, 3)
        Me.gbItemInformation.Name = "gbItemInformation"
        Me.gbItemInformation.OpenHeight = 300
        Me.gbItemInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbItemInformation.ShowBorder = True
        Me.gbItemInformation.ShowCheckBox = False
        Me.gbItemInformation.ShowCollapseButton = False
        Me.gbItemInformation.ShowDefaultBorderColor = True
        Me.gbItemInformation.ShowDownButton = False
        Me.gbItemInformation.ShowHeader = True
        Me.gbItemInformation.Size = New System.Drawing.Size(353, 397)
        Me.gbItemInformation.TabIndex = 63
        Me.gbItemInformation.Temp = 0
        Me.gbItemInformation.Text = "Item(s) Information"
        Me.gbItemInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objChkICheck
        '
        Me.objChkICheck.AutoSize = True
        Me.objChkICheck.Location = New System.Drawing.Point(8, 33)
        Me.objChkICheck.Name = "objChkICheck"
        Me.objChkICheck.Size = New System.Drawing.Size(15, 14)
        Me.objChkICheck.TabIndex = 9
        Me.objChkICheck.UseVisualStyleBackColor = True
        '
        'lvItemInformation
        '
        Me.lvItemInformation.BackColorOnChecked = False
        Me.lvItemInformation.CheckBoxes = True
        Me.lvItemInformation.ColumnHeaders = Nothing
        Me.lvItemInformation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhICheck, Me.objcolhcgid, Me.objcolhIGrp, Me.objcolhtablename, Me.colhItemList, Me.objcolhMtypeId})
        Me.lvItemInformation.CompulsoryColumns = ""
        Me.lvItemInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvItemInformation.FullRowSelect = True
        Me.lvItemInformation.GridLines = True
        Me.lvItemInformation.GroupingColumn = Nothing
        Me.lvItemInformation.HideSelection = False
        Me.lvItemInformation.Location = New System.Drawing.Point(2, 26)
        Me.lvItemInformation.MinColumnWidth = 50
        Me.lvItemInformation.MultiSelect = False
        Me.lvItemInformation.Name = "lvItemInformation"
        Me.lvItemInformation.OptionalColumns = ""
        Me.lvItemInformation.ShowMoreItem = False
        Me.lvItemInformation.ShowSaveItem = False
        Me.lvItemInformation.ShowSelectAll = True
        Me.lvItemInformation.ShowSizeAllColumnsToFit = True
        Me.lvItemInformation.Size = New System.Drawing.Size(349, 369)
        Me.lvItemInformation.Sortable = True
        Me.lvItemInformation.TabIndex = 8
        Me.lvItemInformation.UseCompatibleStateImageBehavior = False
        Me.lvItemInformation.View = System.Windows.Forms.View.Details
        '
        'objcolhICheck
        '
        Me.objcolhICheck.Tag = "objcolhICheck"
        Me.objcolhICheck.Text = ""
        Me.objcolhICheck.Width = 25
        '
        'objcolhcgid
        '
        Me.objcolhcgid.DisplayIndex = 4
        Me.objcolhcgid.Tag = "objcolhcgid"
        Me.objcolhcgid.Text = "objcolhcgid"
        Me.objcolhcgid.Width = 0
        '
        'objcolhIGrp
        '
        Me.objcolhIGrp.DisplayIndex = 1
        Me.objcolhIGrp.Tag = "objcolhIGrp"
        Me.objcolhIGrp.Text = "objcolhIGrp"
        Me.objcolhIGrp.Width = 0
        '
        'objcolhtablename
        '
        Me.objcolhtablename.Tag = "objcolhtablename"
        Me.objcolhtablename.Text = "objcolhtablename"
        Me.objcolhtablename.Width = 0
        '
        'colhItemList
        '
        Me.colhItemList.DisplayIndex = 2
        Me.colhItemList.Tag = "colhItemList"
        Me.colhItemList.Text = "Item(s) List"
        Me.colhItemList.Width = 310
        '
        'objcolhMtypeId
        '
        Me.objcolhMtypeId.Tag = "objcolhMtypeId"
        Me.objcolhMtypeId.Text = "objcolhMtypeId"
        Me.objcolhMtypeId.Width = 0
        '
        'gbDestinationInfo
        '
        Me.gbDestinationInfo.BorderColor = System.Drawing.Color.Black
        Me.gbDestinationInfo.Checked = False
        Me.gbDestinationInfo.CollapseAllExceptThis = False
        Me.gbDestinationInfo.CollapsedHoverImage = Nothing
        Me.gbDestinationInfo.CollapsedNormalImage = Nothing
        Me.gbDestinationInfo.CollapsedPressedImage = Nothing
        Me.gbDestinationInfo.CollapseOnLoad = False
        Me.gbDestinationInfo.Controls.Add(Me.objchkDCheck)
        Me.gbDestinationInfo.Controls.Add(Me.lvDestinationInfo)
        Me.gbDestinationInfo.ExpandedHoverImage = Nothing
        Me.gbDestinationInfo.ExpandedNormalImage = Nothing
        Me.gbDestinationInfo.ExpandedPressedImage = Nothing
        Me.gbDestinationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDestinationInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDestinationInfo.HeaderHeight = 25
        Me.gbDestinationInfo.HeaderMessage = ""
        Me.gbDestinationInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDestinationInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDestinationInfo.HeightOnCollapse = 0
        Me.gbDestinationInfo.LeftTextSpace = 0
        Me.gbDestinationInfo.Location = New System.Drawing.Point(3, 92)
        Me.gbDestinationInfo.Name = "gbDestinationInfo"
        Me.gbDestinationInfo.OpenHeight = 300
        Me.gbDestinationInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDestinationInfo.ShowBorder = True
        Me.gbDestinationInfo.ShowCheckBox = False
        Me.gbDestinationInfo.ShowCollapseButton = False
        Me.gbDestinationInfo.ShowDefaultBorderColor = True
        Me.gbDestinationInfo.ShowDownButton = False
        Me.gbDestinationInfo.ShowHeader = True
        Me.gbDestinationInfo.Size = New System.Drawing.Size(329, 308)
        Me.gbDestinationInfo.TabIndex = 64
        Me.gbDestinationInfo.Temp = 0
        Me.gbDestinationInfo.Text = "Destination Information"
        Me.gbDestinationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkDCheck
        '
        Me.objchkDCheck.AutoSize = True
        Me.objchkDCheck.Location = New System.Drawing.Point(8, 33)
        Me.objchkDCheck.Name = "objchkDCheck"
        Me.objchkDCheck.Size = New System.Drawing.Size(15, 14)
        Me.objchkDCheck.TabIndex = 8
        Me.objchkDCheck.UseVisualStyleBackColor = True
        '
        'lvDestinationInfo
        '
        Me.lvDestinationInfo.BackColorOnChecked = False
        Me.lvDestinationInfo.CheckBoxes = True
        Me.lvDestinationInfo.ColumnHeaders = Nothing
        Me.lvDestinationInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhDCheck, Me.objcolhDGrp, Me.colhDestination, Me.objcolhsdate})
        Me.lvDestinationInfo.CompulsoryColumns = ""
        Me.lvDestinationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDestinationInfo.FullRowSelect = True
        Me.lvDestinationInfo.GridLines = True
        Me.lvDestinationInfo.GroupingColumn = Nothing
        Me.lvDestinationInfo.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDestinationInfo.HideSelection = False
        Me.lvDestinationInfo.Location = New System.Drawing.Point(2, 26)
        Me.lvDestinationInfo.MinColumnWidth = 50
        Me.lvDestinationInfo.MultiSelect = False
        Me.lvDestinationInfo.Name = "lvDestinationInfo"
        Me.lvDestinationInfo.OptionalColumns = ""
        Me.lvDestinationInfo.ShowMoreItem = False
        Me.lvDestinationInfo.ShowSaveItem = False
        Me.lvDestinationInfo.ShowSelectAll = True
        Me.lvDestinationInfo.ShowSizeAllColumnsToFit = True
        Me.lvDestinationInfo.Size = New System.Drawing.Size(325, 280)
        Me.lvDestinationInfo.Sortable = True
        Me.lvDestinationInfo.TabIndex = 7
        Me.lvDestinationInfo.UseCompatibleStateImageBehavior = False
        Me.lvDestinationInfo.View = System.Windows.Forms.View.Details
        '
        'objcolhDCheck
        '
        Me.objcolhDCheck.Tag = "objcolhDCheck"
        Me.objcolhDCheck.Text = ""
        Me.objcolhDCheck.Width = 25
        '
        'objcolhDGrp
        '
        Me.objcolhDGrp.Tag = "objcolhDGrp"
        Me.objcolhDGrp.Text = "objcolhDGrp"
        Me.objcolhDGrp.Width = 0
        '
        'colhDestination
        '
        Me.colhDestination.Tag = "colhDestination"
        Me.colhDestination.Text = "Destination(s) Information"
        Me.colhDestination.Width = 290
        '
        'objcolhsdate
        '
        Me.objcolhsdate.Tag = "objcolhsdate"
        Me.objcolhsdate.Text = "sdate"
        Me.objcolhsdate.Width = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.gbCompanyInfo)
        Me.SplitContainer1.Panel1.Controls.Add(Me.EZeeFooter1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.gbDestinationInfo)
        Me.SplitContainer1.Panel1.Controls.Add(Me.gbItemInformation)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.StatusStrip1)
        Me.SplitContainer1.Size = New System.Drawing.Size(684, 478)
        Me.SplitContainer1.SplitterDistance = 451
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 65
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tslblCurrentProgress, Me.tspbCurrProgress, Me.objCurrProgress, Me.objSep1, Me.tslblOverallProgress, Me.tspbOverallProgress, Me.objOvrProgress})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 3)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(684, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 66
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tslblCurrentProgress
        '
        Me.tslblCurrentProgress.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.tslblCurrentProgress.Name = "tslblCurrentProgress"
        Me.tslblCurrentProgress.Size = New System.Drawing.Size(95, 17)
        Me.tslblCurrentProgress.Text = "Current Progress"
        Me.tslblCurrentProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'tspbCurrProgress
        '
        Me.tspbCurrProgress.Name = "tspbCurrProgress"
        Me.tspbCurrProgress.Size = New System.Drawing.Size(170, 16)
        Me.tspbCurrProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(10, 17)
        Me.objSep1.Text = "|"
        '
        'tslblOverallProgress
        '
        Me.tslblOverallProgress.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.tslblOverallProgress.Name = "tslblOverallProgress"
        Me.tslblOverallProgress.Size = New System.Drawing.Size(92, 17)
        Me.tslblOverallProgress.Text = "Overall Progress"
        Me.tslblOverallProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'tspbOverallProgress
        '
        Me.tspbOverallProgress.Name = "tspbOverallProgress"
        Me.tspbOverallProgress.Size = New System.Drawing.Size(220, 16)
        Me.tspbOverallProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'objCurrProgress
        '
        Me.objCurrProgress.AutoSize = False
        Me.objCurrProgress.ForeColor = System.Drawing.Color.Red
        Me.objCurrProgress.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objCurrProgress.Name = "objCurrProgress"
        Me.objCurrProgress.Size = New System.Drawing.Size(30, 17)
        Me.objCurrProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'objOvrProgress
        '
        Me.objOvrProgress.AutoSize = False
        Me.objOvrProgress.ForeColor = System.Drawing.Color.Red
        Me.objOvrProgress.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objOvrProgress.Name = "objOvrProgress"
        Me.objOvrProgress.Size = New System.Drawing.Size(30, 17)
        Me.objOvrProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'frmTableCloning
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 478)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTableCloning"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Clone Masters Table(s)"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbCompanyInfo.ResumeLayout(False)
        Me.gbCompanyInfo.PerformLayout()
        Me.gbItemInformation.ResumeLayout(False)
        Me.gbItemInformation.PerformLayout()
        Me.gbDestinationInfo.ResumeLayout(False)
        Me.gbDestinationInfo.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnStart As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnStop As eZee.Common.eZeeLightButton
    Friend WithEvents lblSelectCompany As System.Windows.Forms.Label
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents txtDatabaseName As System.Windows.Forms.TextBox
    Friend WithEvents lblDatabase As System.Windows.Forms.Label
    Friend WithEvents gbCompanyInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbItemInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbDestinationInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lvDestinationInfo As eZee.Common.eZeeListView
    Friend WithEvents lvItemInformation As eZee.Common.eZeeListView
    Friend WithEvents objcolhDCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDestination As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhICheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhItemList As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkICheck As System.Windows.Forms.CheckBox
    Friend WithEvents objchkDCheck As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhtablename As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhcgid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMtypeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tslblCurrentProgress As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tspbCurrProgress As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents tslblOverallProgress As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tspbOverallProgress As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objlblNote As System.Windows.Forms.Label
    Friend WithEvents objcolhsdate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objCurrProgress As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objOvrProgress As System.Windows.Forms.ToolStripStatusLabel
End Class
