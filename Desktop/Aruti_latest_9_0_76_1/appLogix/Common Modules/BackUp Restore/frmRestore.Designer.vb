<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRestore
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRestore))
        Me.lblRestorePath = New System.Windows.Forms.Label
        Me.txtRestorePath = New eZee.TextBox.AlphanumericTextBox
        Me.lvRestore = New System.Windows.Forms.ListView
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhTime = New System.Windows.Forms.ColumnHeader
        Me.colhBackupPath = New System.Windows.Forms.ColumnHeader
        Me.colhUser = New System.Windows.Forms.ColumnHeader
        Me.ofdRestore = New System.Windows.Forms.OpenFileDialog
        Me.tmrWait = New System.Windows.Forms.Timer(Me.components)
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnRestore = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnBrowse = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pbProgress = New System.Windows.Forms.ProgressBar
        Me.pnlMain.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblRestorePath
        '
        Me.lblRestorePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRestorePath.Location = New System.Drawing.Point(12, 73)
        Me.lblRestorePath.Name = "lblRestorePath"
        Me.lblRestorePath.Size = New System.Drawing.Size(81, 16)
        Me.lblRestorePath.TabIndex = 1
        Me.lblRestorePath.Text = "Restore path"
        '
        'txtRestorePath
        '
        Me.txtRestorePath.BackColor = System.Drawing.Color.White
        Me.txtRestorePath.Flags = 0
        Me.txtRestorePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRestorePath.InvalidChars = New Char(-1) {}
        Me.txtRestorePath.Location = New System.Drawing.Point(99, 71)
        Me.txtRestorePath.Name = "txtRestorePath"
        Me.txtRestorePath.ReadOnly = True
        Me.txtRestorePath.Size = New System.Drawing.Size(471, 21)
        Me.txtRestorePath.TabIndex = 2
        '
        'lvRestore
        '
        Me.lvRestore.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvRestore.BackgroundImageTiled = True
        Me.lvRestore.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhDate, Me.colhTime, Me.colhBackupPath, Me.colhUser})
        Me.lvRestore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvRestore.FullRowSelect = True
        Me.lvRestore.GridLines = True
        Me.lvRestore.HideSelection = False
        Me.lvRestore.HoverSelection = True
        Me.lvRestore.Location = New System.Drawing.Point(9, 106)
        Me.lvRestore.MultiSelect = False
        Me.lvRestore.Name = "lvRestore"
        Me.lvRestore.Size = New System.Drawing.Size(657, 235)
        Me.lvRestore.TabIndex = 4
        Me.lvRestore.UseCompatibleStateImageBehavior = False
        Me.lvRestore.View = System.Windows.Forms.View.Details
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 100
        '
        'colhTime
        '
        Me.colhTime.Tag = "colhTime"
        Me.colhTime.Text = "Time"
        Me.colhTime.Width = 111
        '
        'colhBackupPath
        '
        Me.colhBackupPath.Tag = "colhBackupPath"
        Me.colhBackupPath.Text = "Backup Path"
        Me.colhBackupPath.Width = 333
        '
        'colhUser
        '
        Me.colhUser.Tag = "colhUser"
        Me.colhUser.Text = "User"
        Me.colhUser.Width = 109
        '
        'tmrWait
        '
        Me.tmrWait.Interval = 30000
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objefFormFooter)
        Me.pnlMain.Controls.Add(Me.btnBrowse)
        Me.pnlMain.Controls.Add(Me.eZeeHeader)
        Me.pnlMain.Controls.Add(Me.txtRestorePath)
        Me.pnlMain.Controls.Add(Me.lblRestorePath)
        Me.pnlMain.Controls.Add(Me.lvRestore)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(675, 401)
        Me.pnlMain.TabIndex = 0
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnRestore)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 346)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(675, 55)
        Me.objefFormFooter.TabIndex = 6
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(576, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnRestore
        '
        Me.btnRestore.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRestore.BackColor = System.Drawing.Color.White
        Me.btnRestore.BackgroundImage = CType(resources.GetObject("btnRestore.BackgroundImage"), System.Drawing.Image)
        Me.btnRestore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRestore.BorderColor = System.Drawing.Color.Empty
        Me.btnRestore.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRestore.FlatAppearance.BorderSize = 0
        Me.btnRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRestore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRestore.ForeColor = System.Drawing.Color.Black
        Me.btnRestore.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRestore.GradientForeColor = System.Drawing.Color.Black
        Me.btnRestore.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRestore.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRestore.Location = New System.Drawing.Point(447, 13)
        Me.btnRestore.Name = "btnRestore"
        Me.btnRestore.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRestore.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRestore.Size = New System.Drawing.Size(123, 30)
        Me.btnRestore.TabIndex = 0
        Me.btnRestore.Text = "&Start Restore"
        Me.btnRestore.UseVisualStyleBackColor = False
        '
        'btnBrowse
        '
        Me.btnBrowse.BackColor = System.Drawing.Color.White
        Me.btnBrowse.BackgroundImage = CType(resources.GetObject("btnBrowse.BackgroundImage"), System.Drawing.Image)
        Me.btnBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBrowse.BorderColor = System.Drawing.Color.Empty
        Me.btnBrowse.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnBrowse.GradientForeColor = System.Drawing.Color.Black
        Me.btnBrowse.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBrowse.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Location = New System.Drawing.Point(576, 66)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBrowse.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Size = New System.Drawing.Size(90, 30)
        Me.btnBrowse.TabIndex = 3
        Me.btnBrowse.Text = "&Browse"
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.Color.White
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = "Important!! Please note if you restore database all your existing data will be ov" & _
            "erwritten. "
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(675, 60)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Database Restore"
        '
        'pbProgress
        '
        Me.pbProgress.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pbProgress.Location = New System.Drawing.Point(0, 401)
        Me.pbProgress.Name = "pbProgress"
        Me.pbProgress.Size = New System.Drawing.Size(675, 17)
        Me.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbProgress.TabIndex = 1
        '
        'frmRestore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(675, 418)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.pbProgress)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRestore"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Database Restore"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblRestorePath As System.Windows.Forms.Label
    Friend WithEvents txtRestorePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lvRestore As System.Windows.Forms.ListView
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTime As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBackupPath As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUser As System.Windows.Forms.ColumnHeader
    Friend WithEvents ofdRestore As System.Windows.Forms.OpenFileDialog
    Friend WithEvents tmrWait As System.Windows.Forms.Timer
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents btnBrowse As eZee.Common.eZeeLightButton
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnRestore As eZee.Common.eZeeLightButton
    Friend WithEvents pbProgress As System.Windows.Forms.ProgressBar
End Class
