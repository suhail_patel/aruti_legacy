﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class frmDailyBackup

#Region " Private Variables "

    Public ReadOnly mstrModuleName As String = "frmMDI"
    Private objCompany As clsCompany_Master
    Private mdtTable As DataTable
    Private iCnt As Integer = 35
    Private WithEvents objBackupDatabase As New eZeeDatabase
    Private mstrBaseDirPath As String = String.Empty

#End Region

#Region " Form's Events "

    Private Sub frmDailyBackup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objCompany = New clsCompany_Master
        Try
            Me.Text = Language.getMessage(mstrModuleName, 1, "Daily Scheduled Backup at : ") & ConfigParameter._Object._BackupTime.ToShortTimeString
            mdtTable = objCompany.Get_DataBaseList("DailyBackup", , True)
            pbStatus.Style = ProgressBarStyle.Continuous
            If mdtTable.Rows.Count > 0 Then
                bgw_DataBackup.RunWorkerAsync()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyBackup_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Backgrownd Worker Event(s) "

    Private Sub bgw_DataBackup_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgw_DataBackup.DoWork
        Try
            Dim objAppSettings As New clsApplicationSettings
            Dim mstrBasePath As String = objAppSettings._ApplicationPath & "Data\Backup"
            Dim strFinalPath As String = String.Empty

            If Not System.IO.Directory.Exists(mstrBasePath) Then
                System.IO.Directory.CreateDirectory(mstrBasePath)
            End If

            mstrBasePath = mstrBasePath & "\" & "Database_Backup_" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)

            If Not System.IO.Directory.Exists(mstrBasePath) Then
                System.IO.Directory.CreateDirectory(mstrBasePath)
            End If

            For Each dtRow As DataRow In mdtTable.Rows
                strFinalPath = ""
                If dtRow.Item("Database").ToString.ToUpper = "HRMSCONFIGURATION" Then
                    System.IO.Directory.CreateDirectory(mstrBasePath & "\" & dtRow.Item("Database").ToString)
                    strFinalPath = mstrBasePath & "\" & dtRow.Item("Database").ToString

                    eZeeDatabase.change_database(dtRow.Item("Database").ToString.ToString)
                    strFinalPath = objBackupDatabase.Backup(strFinalPath)
                    strFinalPath = ""
                ElseIf CBool(dtRow.Item("IsGrp")) = True Then
                    If Not System.IO.Directory.Exists(mstrBasePath & "\" & dtRow.Item("Database").ToString.Trim) Then
                        System.IO.Directory.CreateDirectory(mstrBasePath & "\" & dtRow.Item("Database").ToString.Trim)
                        mstrBaseDirPath = mstrBasePath & "\" & dtRow.Item("Database").ToString.Trim
                    End If
                ElseIf CBool(dtRow.Item("IsGrp")) = False Then
                    If Not System.IO.Directory.Exists(mstrBaseDirPath & "\" & dtRow.Item("Database").ToString.Trim) Then
                        System.IO.Directory.CreateDirectory(mstrBaseDirPath & "\" & dtRow.Item("Database").ToString.Trim)
                    End If
                    strFinalPath = mstrBaseDirPath & "\" & dtRow.Item("Database").ToString.Trim

                    eZeeDatabase.change_database(dtRow.Item("Database").ToString.Trim)
                    strFinalPath = objBackupDatabase.Backup(strFinalPath)
                    strFinalPath = ""
                End If
                iCnt += 5
                bgw_DataBackup.ReportProgress(iCnt)
                System.Threading.Thread.Sleep(1000)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgw_DataBackup_DoWork", mstrModuleName)
        End Try
    End Sub

    Private Sub bgw_DataBackup_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgw_DataBackup.ProgressChanged
        Try
            pbStatus.Value = e.ProgressPercentage
            Dim myString As String = pbStatus.Value & " " & Language.getMessage(mstrModuleName, 2, "% Done")
            Dim canvas As Graphics = Me.pbStatus.CreateGraphics
            canvas.DrawString(myString, New Font("Verdana", 10, FontStyle.Bold), New SolidBrush(Color.Black), 220, 5)
            canvas.Dispose()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgw_DataBackup_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub bgw_DataBackup_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_DataBackup.RunWorkerCompleted
        Try
            eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgw_DataBackup_RunWorkerCompleted", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
            Me.pbStatus.Text = Language._Object.getCaption(Me.pbStatus.Name, Me.pbStatus.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Daily Scheduled Backup at :")
			Language.setMessage(mstrModuleName, 2, "% Done")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class