<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBackUp
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBackUp))
        Me.fdbBackup = New System.Windows.Forms.FolderBrowserDialog
        Me.txtBackupPath = New eZee.TextBox.AlphanumericTextBox
        Me.lblBackUp = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnBackup = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbBackUp = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnBrowse = New eZee.Common.eZeeLightButton(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.pbBackup = New System.Windows.Forms.ToolStripProgressBar
        Me.Panel1.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.gbBackUp.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtBackupPath
        '
        Me.txtBackupPath.BackColor = System.Drawing.Color.White
        Me.txtBackupPath.Flags = 0
        Me.txtBackupPath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBackupPath.InvalidChars = New Char(-1) {}
        Me.txtBackupPath.Location = New System.Drawing.Point(101, 38)
        Me.txtBackupPath.Name = "txtBackupPath"
        Me.txtBackupPath.ReadOnly = True
        Me.txtBackupPath.Size = New System.Drawing.Size(355, 21)
        Me.txtBackupPath.TabIndex = 1
        '
        'lblBackUp
        '
        Me.lblBackUp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBackUp.Location = New System.Drawing.Point(8, 42)
        Me.lblBackUp.Name = "lblBackUp"
        Me.lblBackUp.Size = New System.Drawing.Size(87, 13)
        Me.lblBackUp.TabIndex = 0
        Me.lblBackUp.Text = "Backup path"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objefFormFooter)
        Me.Panel1.Controls.Add(Me.gbBackUp)
        Me.Panel1.Controls.Add(Me.StatusStrip1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(581, 169)
        Me.Panel1.TabIndex = 0
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnBackup)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 92)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(581, 55)
        Me.objefFormFooter.TabIndex = 2
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(482, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnBackup
        '
        Me.btnBackup.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBackup.BackColor = System.Drawing.Color.White
        Me.btnBackup.BackgroundImage = CType(resources.GetObject("btnBackup.BackgroundImage"), System.Drawing.Image)
        Me.btnBackup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBackup.BorderColor = System.Drawing.Color.Empty
        Me.btnBackup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnBackup.FlatAppearance.BorderSize = 0
        Me.btnBackup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBackup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBackup.ForeColor = System.Drawing.Color.Black
        Me.btnBackup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnBackup.GradientForeColor = System.Drawing.Color.Black
        Me.btnBackup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBackup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnBackup.Location = New System.Drawing.Point(353, 13)
        Me.btnBackup.Name = "btnBackup"
        Me.btnBackup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBackup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnBackup.Size = New System.Drawing.Size(123, 30)
        Me.btnBackup.TabIndex = 0
        Me.btnBackup.Text = "&Start Backup"
        Me.btnBackup.UseVisualStyleBackColor = False
        '
        'gbBackUp
        '
        Me.gbBackUp.BorderColor = System.Drawing.Color.Black
        Me.gbBackUp.Checked = False
        Me.gbBackUp.CollapseAllExceptThis = False
        Me.gbBackUp.CollapsedHoverImage = Nothing
        Me.gbBackUp.CollapsedNormalImage = Nothing
        Me.gbBackUp.CollapsedPressedImage = Nothing
        Me.gbBackUp.CollapseOnLoad = False
        Me.gbBackUp.Controls.Add(Me.btnBrowse)
        Me.gbBackUp.Controls.Add(Me.lblBackUp)
        Me.gbBackUp.Controls.Add(Me.txtBackupPath)
        Me.gbBackUp.ExpandedHoverImage = Nothing
        Me.gbBackUp.ExpandedNormalImage = Nothing
        Me.gbBackUp.ExpandedPressedImage = Nothing
        Me.gbBackUp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBackUp.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBackUp.HeaderHeight = 25
        Me.gbBackUp.HeightOnCollapse = 0
        Me.gbBackUp.LeftTextSpace = 0
        Me.gbBackUp.Location = New System.Drawing.Point(9, 9)
        Me.gbBackUp.Name = "gbBackUp"
        Me.gbBackUp.OpenHeight = 73
        Me.gbBackUp.Padding = New System.Windows.Forms.Padding(3)
        Me.gbBackUp.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBackUp.ShowBorder = True
        Me.gbBackUp.ShowCheckBox = False
        Me.gbBackUp.ShowCollapseButton = False
        Me.gbBackUp.ShowDefaultBorderColor = True
        Me.gbBackUp.ShowDownButton = False
        Me.gbBackUp.ShowHeader = True
        Me.gbBackUp.Size = New System.Drawing.Size(563, 73)
        Me.gbBackUp.TabIndex = 0
        Me.gbBackUp.Temp = 0
        Me.gbBackUp.Text = "Database Backup"
        Me.gbBackUp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnBrowse
        '
        Me.btnBrowse.BackColor = System.Drawing.Color.White
        Me.btnBrowse.BackgroundImage = CType(resources.GetObject("btnBrowse.BackgroundImage"), System.Drawing.Image)
        Me.btnBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBrowse.BorderColor = System.Drawing.Color.Empty
        Me.btnBrowse.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnBrowse.GradientForeColor = System.Drawing.Color.Black
        Me.btnBrowse.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBrowse.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Location = New System.Drawing.Point(462, 33)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBrowse.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Size = New System.Drawing.Size(90, 30)
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Text = "&Browse"
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.pbBackup})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 147)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(581, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'pbBackup
        '
        Me.pbBackup.Name = "pbBackup"
        Me.pbBackup.Size = New System.Drawing.Size(575, 16)
        Me.pbBackup.Visible = False
        '
        'frmBackUp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 169)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBackUp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Database Back up"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.gbBackUp.ResumeLayout(False)
        Me.gbBackUp.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents fdbBackup As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents lblBackUp As System.Windows.Forms.Label
    Friend WithEvents txtBackupPath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbBackUp As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnBrowse As eZee.Common.eZeeLightButton
    Friend WithEvents btnBackup As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents pbBackup As System.Windows.Forms.ToolStripProgressBar
End Class
