﻿Option Strict On

#Region " Improts "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Drawing.Printing
Imports System.IO
'S.SANDEEP |25-JAN-2019| -- START
'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
Imports System.IO.Packaging
'S.SANDEEP |25-JAN-2019| -- END

#End Region

Public Class frmScanAttachmentList

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmScanAttachmentList"
    Private objDocument As clsScan_Attach_Documents
    Dim img As System.Drawing.Image
    Private mstrAttachedFiles As String = String.Empty
    Private mblnIsFromMail As Boolean = False

    'Sohail (07 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintRefModuleId As Integer = -1
    Private mintEmployeeUnkId As Integer = -1
    'Sohail (07 Mar 2012) -- End
    'Sohail (02 Jun 2015) -- Start
    'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
    Private mblnFromRecruitment As Boolean = False
    'Sohail (02 Jun 2015) -- End

    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Private mblnIsSelfServiceInstall As Boolean
    Private mdsDoc As DataSet
    Private dtTable As DataTable
    'SHANI (16 JUL 2015) -- End 

    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    Private mblnIsFromEmpForm As Boolean = False
    Private mblnIsApproved As Boolean = True
    'Nilay (03-Dec-2015) -- End

#End Region

    'Sohail (07 Mar 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Display Dialog "
    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    'Public Function displayDialog(Optional ByVal intRefModuleId As Integer = 0, Optional ByVal intEmployeeID As Integer = 0, Optional ByVal blnFromRecruitment As Boolean = False) As Boolean
    Public Function displayDialog(Optional ByVal intRefModuleId As Integer = 0, Optional ByVal intEmployeeID As Integer = 0, _
                                  Optional ByVal blnFromRecruitment As Boolean = False, _
                                  Optional ByVal blnFromEmpForm As Boolean = False, _
                                  Optional ByVal blnIsApproved As Boolean = True) As Boolean
        'Nilay (03-Dec-2015) -- End

        'Sohail (02 Jun 2015) - [intModuleID]
        Try

            mintRefModuleId = intRefModuleId
            mintEmployeeUnkId = intEmployeeID
            mblnFromRecruitment = blnFromRecruitment 'Sohail (02 Jun 2015)

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            mblnIsFromEmpForm = blnFromEmpForm
            mblnIsApproved = blnIsApproved
            'Nilay (03-Dec-2015) -- End

            'mAction = eAction
            'mstrToEditIds = StrEditIds

            Me.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function
#End Region
    'Sohail (07 Mar 2012) -- End

#Region " Properties "

    'Public Property _Email_Address() As String
    '    Get
    '        Return mstrEmailAddress
    '    End Get
    '    Set(ByVal value As String)
    '        mstrEmailAddress = value
    '    End Set
    'End Property

    'Public Property _Data_Set() As DataSet
    '    Get
    '        Return dsEmailData
    '    End Get
    '    Set(ByVal value As DataSet)
    '        dsEmailData = value
    '    End Set
    'End Property

    Public Property _Attached_Files() As String
        Get
            Return mstrAttachedFiles
        End Get
        Set(ByVal value As String)
            mstrAttachedFiles = value
        End Set
    End Property

    Public Property _IsFromMail() As Boolean
        Get
            Return mblnIsFromMail
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromMail = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objEMaster As New clsEmployee_Master
        Dim objAMaster As New clsApplicant_master
        Dim objCMaster As New clsCommon_Master
        Dim dsCombo As New DataSet
        'Hemant (29 Dec 2020) -- Start
        'Enhancement # AH-2017 : PACT TZ-Assist to download CV in bulk by particular Vacancy 
        Dim objVacancy As New clsVacancy
        'Hemant (29 Dec 2020) -- End
        Try
            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsCombo = objEMaster.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombo = objEMaster.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombo = objEMaster.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If
            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                     User._Object._Userunkid, _
            '                                     FinancialYear._Object._YearUnkid, _
            '                                     Company._Object._Companyunkid, _
            '                                     FinancialYear._Object._Database_Start_Date.Date, _
            '                                     FinancialYear._Object._Database_End_Date.Date, _
            '                                     ConfigParameter._Object._UserAccessModeSetting, _
            '                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmScanOrAttachmentInfo))) Then
                    mblnIsApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .

            'If mblnIsFromEmpForm = True Then
            '    dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                     User._Object._Userunkid, _
            '                                     FinancialYear._Object._YearUnkid, _
            '                                     Company._Object._Companyunkid, _
            '                                     FinancialYear._Object._Database_Start_Date.Date, _
            '                                     FinancialYear._Object._Database_End_Date.Date, _
            '                                     ConfigParameter._Object._UserAccessModeSetting, _
            '                                     mblnIsApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "List", False, mintEmployeeUnkId)
            'Else
            '    dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                     User._Object._Userunkid, _
            '                                     FinancialYear._Object._YearUnkid, _
            '                                     Company._Object._Companyunkid, _
            '                                     FinancialYear._Object._Database_Start_Date.Date, _
            '                                     FinancialYear._Object._Database_End_Date.Date, _
            '                                     ConfigParameter._Object._UserAccessModeSetting, _
            '                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'End If


            If mblnIsFromEmpForm = True Then
                dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 FinancialYear._Object._Database_Start_Date.Date, _
                                                 FinancialYear._Object._Database_End_Date.Date, _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 mblnIsApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
                                                 False, mintEmployeeUnkId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)
            Else
                dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 FinancialYear._Object._Database_Start_Date.Date, _
                                                 FinancialYear._Object._Database_End_Date.Date, _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 mblnIsApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
                                                 True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)
            End If



            'S.SANDEEP [20-JUN-2018] -- End

            'Nilay (03-Dec-2015) -- End
            
            'Shani(24-Aug-2015) -- End
            
            'Anjan (17 Apr 2012)-End 



            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objAMaster.GetApplicantList("List", True)
            With cboApplicant
                .ValueMember = "applicantunkid"
                .DisplayMember = "applicantname"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocument
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            'S.SANDEEP |26-APR-2019| -- START
            'dsCombo = objDocument.GetDocType()
            'Sohail (30 Jul 2019) -- Start
            'Pact Issue # 0004026 - 76.1 : CV and cover letter attachments are not coming on applicant -> attachment list.
            'dsCombo = objDocument.GetDocType(Company._Object._Companyunkid, True)
            Dim dtTable As DataTable = Nothing
            If mblnFromRecruitment = False Then
            dsCombo = objDocument.GetDocType(Company._Object._Companyunkid, True)
                dtTable = New DataView(dsCombo.Tables(0)).ToTable
            Else
                dsCombo = objDocument.GetDocType(Company._Object._Companyunkid, False)
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-507 - On Applicants List-Preview Attachments, They only want to display the CV and cover letter only. Remove the qualifications as a document type.
                'dtTable = New DataView(dsCombo.Tables(0), "Id IN (" & enScanAttactRefId.QUALIFICATIONS & ", " & enScanAttactRefId.CURRICULAM_VITAE & ", " & enScanAttactRefId.COVER_LETTER & ") ", "", DataViewRowState.CurrentRows).ToTable
                dtTable = New DataView(dsCombo.Tables(0), "Id IN (" & enScanAttactRefId.CURRICULAM_VITAE & ", " & enScanAttactRefId.COVER_LETTER & ") ", "", DataViewRowState.CurrentRows).ToTable
                'Hemant (01 Nov 2021) -- End
            End If
            'Sohail (30 Jul 2019) -- End
            'S.SANDEEP |26-APR-2019| -- END
            With cboDocumentType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                'Sohail (30 Jul 2019) -- Start
                'Pact Issue # 0004026 - 76.1 : CV and cover letter attachments are not coming on applicant -> attachment list.
                '.DataSource = dsCombo.Tables(0)
                .DataSource = dtTable
                'Sohail (30 Jul 2019) -- End
            End With

            'Hemant (29 Dec 2020) -- Start
            'Enhancement # AH-2017 : PACT TZ-Assist to download CV in bulk by particular Vacancy 
            dsCombo = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , , , , True)
            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With
            'Hemant (29 Dec 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEMaster = Nothing : objAMaster = Nothing : objCMaster = Nothing : dsCombo.Dispose()
            'Hemant (29 Dec 2020) -- Start
            'Enhancement # AH-2017 : PACT TZ-Assist to download CV in bulk by particular Vacancy 
            objVacancy = Nothing
            'Hemant (29 Dec 2020) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet

        'SHANI (16 JUL 2015) -- Start
        'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
        'Upload Image folder to access those attachemts from Server and 
        'all client machines if ArutiSelfService is installed otherwise save then on Document path
        'Dim dtTable As DataTable
        dtTable = Nothing
        'SHANI (16 JUL 2015) -- End 


        Dim StrSearching As String = ""
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objDocument.GetList( "List")

            'S.SANDEEP |13-OCT-2021| -- START
            'dsList = objDocument.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), , , , , , CInt(cboVacancy.SelectedValue), CInt(cboDocumentType.SelectedValue))
            dsList = objDocument.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), , , , , , CInt(cboVacancy.SelectedValue), CInt(cboDocumentType.SelectedValue), CInt(cboApplicant.SelectedValue))
            'S.SANDEEP |13-OCT-2021| -- END

            'Sohail (03 Sep 2021) - [CInt(cboDocumentType.SelectedValue)]
            'Hemant (29 Dec 2020) -- [CInt(cboVacancy.SelectedValue)]
            'Shani(24-Aug-2015) -- End

            lvScanAttachmentList.Items.Clear()

            If radSearchByApp.Checked Then
                'Hemant (20 Nov 2019) -- Start
                'ISSUE/ENHANCEMENT(PACT) :  Ability to download attachments in bulky and not individual applications (cover letters and CVs)
                'StrSearching &= "AND UNKID = '" & CInt(cboApplicant.SelectedValue) & "' AND IsApplicant = 1 "
                If CInt(cboApplicant.SelectedValue) > 0 Then
                    StrSearching &= "AND UNKID = '" & CInt(cboApplicant.SelectedValue) & "' "
                End If
                StrSearching &= "AND IsApplicant = 1 "
                'Hemant (20 Nov 2019) -- End
            ElseIf radSearchByEmp.Checked Then
                StrSearching &= "AND UNKID = '" & CInt(cboEmployee.SelectedValue) & "' AND IsApplicant = 0 "
            End If

            If CInt(cboDocument.SelectedValue) > 0 Then
                StrSearching &= "AND documentunkid = '" & CInt(cboDocument.SelectedValue) & "'"
            End If

            If CInt(cboDocumentType.SelectedValue) > 0 Then
                StrSearching &= "AND DOCTYPEID = '" & CInt(cboDocumentType.SelectedValue) & "'"
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            dtTable.Columns.Add("temppath", Type.GetType("System.String"))
            'SHANI (16 JUL 2015) -- End 

            lvScanAttachmentList.BeginUpdate()
            For Each dtRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = ""

                lvItem.SubItems.Add(dtRow.Item("CODE").ToString)            'CODE
                lvItem.SubItems.Add(dtRow.Item("ENAME").ToString)           'NAME
                lvItem.SubItems.Add(dtRow.Item("DOC_NAME").ToString)        'DOCUMENT
                lvItem.SubItems.Add(dtRow.Item("ATTACHED_FILE").ToString)   'FILENAME
                lvItem.SubItems.Add(dtRow.Item("DOC_TYPE").ToString)        'DOCUMENTTYPE
                lvItem.SubItems.Add(dtRow.Item("UNKID").ToString)           'UNKID
                lvItem.SubItems.Add(dtRow.Item("AVALUE").ToString)          'VALUE

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                'If ConfigParameter._Object._Document_Path.ToString.LastIndexOf("\") = ConfigParameter._Object._Document_Path.ToString.Trim.Length - 1 Then
                '    lvItem.SubItems.Add(ConfigParameter._Object._Document_Path.ToString & dtRow.Item("DOC_TYPE").ToString & "\" & dtRow.Item("ATTACHED_FILE").ToString)
                'Else
                '    lvItem.SubItems.Add(ConfigParameter._Object._Document_Path.ToString & "\" & dtRow.Item("DOC_TYPE").ToString & "\" & dtRow.Item("ATTACHED_FILE").ToString)
                'End If
                lvItem.SubItems.Add(dtRow.Item("filepath").ToString)
                
                'SHANI (16 JUL 2015) -- End

                 'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                lvItem.SubItems.Add(dtRow.Item("ismandatoryfornewemp").ToString)
                'Nilay (03-Dec-2015) -- End

                lvItem.Tag = dtRow.Item("scanattachtranunkid")

                'Sohail (26 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                lvItem.SubItems(objcolhDocType.Index).Tag = CInt(dtRow.Item("scanattachrefid"))
                lvItem.SubItems(colhValue.Index).Tag = CInt(dtRow.Item("transactionunkid"))
                'Sohail (26 Mar 2012) -- End

                lvScanAttachmentList.Items.Add(lvItem)
            Next

            lvScanAttachmentList.GroupingColumn = objcolhDocType
            lvScanAttachmentList.DisplayGroups(True)
            lvScanAttachmentList.GridLines = False
            lvScanAttachmentList.EndUpdate()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path

    'S.SANDEEP |03-APR-2019| -- START
    Private Function CreateImage(ByVal xrow As DataRow, Optional ByRef strErr As String = "") As String
        'Private Function CreateImage(ByVal xrow As DataRow) As String
        'S.SANDEEP |03-APR-2019| -- END
        Dim strError As String = ""
        Dim strLocalpath As String = ""
        Try
            Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(xrow("scanattachrefid").ToString)) Select (p.Item("Name").ToString)).FirstOrDefault

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xrow("filepath").ToString, xrow("fileuniquename").ToString, mstrFolderName, strError)
            Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xrow("filepath").ToString, xrow("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)
            'Shani(24-Aug-2015) -- End

            If imagebyte IsNot Nothing Then
                strLocalpath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xrow("fileuniquename").ToString
                Dim ms As New MemoryStream(imagebyte)
                Dim fs As New FileStream(strLocalpath, FileMode.Create)
                ms.WriteTo(fs)
                ms.Close()
                fs.Close()
                fs.Dispose()
            End If

            'S.SANDEEP |03-APR-2019| -- START
            strErr = strError
            'S.SANDEEP |03-APR-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateImage :", mstrModuleName)
        End Try
        Return strLocalpath
    End Function
    'SHANI (16 JUL 2015) -- End 

    'S.SANDEEP |25-JAN-2019| -- START
    'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
    Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal)
        Try
            Using zip As Package = System.IO.Packaging.Package.Open(zipFilename, FileMode.OpenOrCreate)
                For Each item As String In fileToAdd
                    Dim destFilename As String = ".\" & Path.GetFileName(item)
                    Dim uri As Uri = PackUriHelper.CreatePartUri(New Uri(destFilename, UriKind.Relative))
                    If zip.PartExists(uri) Then
                        zip.DeletePart(uri)
                    End If
                    Dim part As PackagePart = zip.CreatePart(uri, "", compression)
                    Using fileStream As New FileStream(item, FileMode.Open, FileAccess.Read)
                        Using dest As Stream = part.GetStream()
                            CopyStream(CType(fileStream, Stream), part.GetStream())
                        End Using
                    End Using
                Next
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddFileToZip", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop
    End Sub
    'S.SANDEEP |25-JAN-2019| -- END

#End Region

#Region " Form's Events "

    Private Sub frmScanAttachmentList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmScanAttachmentList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'S.SANDEEP |25-JAN-2019| -- START
        'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
        'objDocument = New clsScan_Attach_Documents(False)
        objDocument = New clsScan_Attach_Documents()
        'S.SANDEEP |25-JAN-2019| -- END
        Try
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            If mblnIsFromEmpForm = True Then
                radSearchByEmp.Checked = True
                radSearchByEmp.Enabled = False
                btnEdit.Enabled = False
                btnDelete.Enabled = False
                objpnlEmp.Visible = True
                objpnlApplicant.Visible = False
                Call FillList()
            End If
            'Nilay (03-Dec-2015) -- End

            'Sohail (02 Jun 2015) -- Start
            'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
            If mblnFromRecruitment = True Then
                radSearchByApp.Checked = True
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-506 - Internal applicants CVs cannot be viewed on desktop (From Applicants list screen). Make search by employee option active on applicant master > Preview attachments.
                'radSearchByEmp.Enabled = False
                'Hemant (01 Nov 2021) -- End
            End If
            'Sohail (02 Jun 2015) -- End

            If radSearchByEmp.Checked = True Then
                objpnlEmp.Visible = True
                objpnlApplicant.Visible = False
            End If

            If mblnIsFromMail = True Then
                radSearchByApp.Enabled = False
            End If

            'Sohail (07 Mar 2012) -- Start
            'TRA - ENHANCEMENT

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'If mintRefModuleId > 0 Then
            '    cboDocumentType.SelectedValue = mintRefModuleId
            '    cboDocumentType.Enabled = False
            'End If
            'Sohail (30 Jul 2019) -- Start
            'Pact Issue # 0004026 - 76.1 : CV and cover letter attachments are not coming on applicant -> attachment list.
            'If mintRefModuleId > 0 AndAlso mblnIsFromEmpForm = False Then
            If mintRefModuleId > 0 AndAlso mblnIsFromEmpForm = False AndAlso mblnFromRecruitment = False Then
                'Sohail (30 Jul 2019) -- End
                cboDocumentType.SelectedValue = mintRefModuleId
                cboDocumentType.Enabled = False
            End If
            'Nilay (03-Dec-2015) -- End

            If mintEmployeeUnkId > 0 Then

                'Sohail (02 Jun 2015) -- Start
                'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
                If mblnFromRecruitment = False Then
                    'Sohail (02 Jun 2015) -- End

                cboEmployee.SelectedValue = mintEmployeeUnkId
                cboEmployee.Enabled = False
                objbtnESearch.Enabled = False
                radSearchByApp.Enabled = False

                    'Sohail (02 Jun 2015) -- Start
                    'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
                Else
                    cboApplicant.SelectedValue = mintEmployeeUnkId
                    'Hemant (20 Nov 2019) -- Start
                    'ISSUE/ENHANCEMENT(PACT) :  Ability to download attachments in bulky and not individual applications (cover letters and CVs)
                    'cboApplicant.Enabled = False
                    'objbtnASearch.Enabled = False
                    'Hemant (20 Nov 2019) -- End
                    'Hemant (01 Nov 2021) -- Start
                    'ENHANCEMENT : OLD-506 - Internal applicants CVs cannot be viewed on desktop (From Applicants list screen). Make search by employee option active on applicant master > Preview attachments.
                    'radSearchByEmp.Enabled = False
                    'Hemant (01 Nov 2021) -- End
                    Call FillList()
                End If
                'Sohail (02 Jun 2015) -- End

            End If
            'Sohail (07 Mar 2012) -- End

            lvScanAttachmentList.GridLines = False

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            mblnIsSelfServiceInstall = IsSelfServiceExist()
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            'SHANI (16 JUL 2015) -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmScanAttachmentList_Load", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsScan_Attach_Documents.SetMessages()
            objfrm._Other_ModuleNames = "clsScan_Attach_Documents"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End
#End Region

#Region " Button's Events "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try

            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            If CInt(cboDocumentType.SelectedValue) <> CInt(enScanAttactRefId.STAFF_REQUISITION) Then
                'Gajanan [02-SEP-2019] -- End
            If radSearchByApp.Checked = True Then
                    'Hemant (20 Nov 2019) -- Start
                    'ISSUE/ENHANCEMENT(PACT) :  Ability to download attachments in bulky and not individual applications (cover letters and CVs)
                    'If CInt(cboApplicant.SelectedValue) <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one applicant to fill list."), enMsgBoxStyle.Information)
                    '    Exit Sub
                    'End If
                    'Hemant (20 Nov 2019) -- End
            ElseIf radSearchByEmp.Checked = True Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select atleast one employee to fill list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            End If 'Gajanan [02-SEP-2019]

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'Sohail (02 Jun 2015) -- Start
            'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
            'radSearchByEmp.Checked = True
            'cboDocumentType.SelectedValue = 0
            If mblnFromRecruitment = True Then
                radSearchByApp.Checked = True
            Else
            radSearchByEmp.Checked = True
                cboDocumentType.SelectedValue = 0
            End If
            'Sohail (02 Jun 2015) -- End
            cboDocument.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            lvScanAttachmentList.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvScanAttachmentList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one scan/attachment entry in order to perform operatation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim strIds As String = String.Empty

            For Each lvItem As ListViewItem In lvScanAttachmentList.CheckedItems

                'Sohail (26 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                If CInt(lvItem.SubItems(objcolhDocType.Index).Tag) = enScanAttactRefId.ASSET_DECLARATION Then
                    Dim objAssetDeclare As New clsAssetdeclaration_master

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objAssetDeclare._Assetdeclarationunkid = CInt(lvItem.SubItems(colhValue.Index).Tag)
                    objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvItem.SubItems(colhValue.Index).Tag)
                    'Shani(24-Aug-2015) -- End
                    If objAssetDeclare._Isfinalsaved = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry! Asset Declaration is already Final Saved for ") & lvItem.SubItems(colhName.Index).Text & ". " & Language.getMessage(mstrModuleName, 11, "Please Uncheck the transaction."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                'Sohail (26 Mar 2012) -- End

                strIds &= "," & lvItem.Tag.ToString
            Next

            If strIds.Trim.Length > 0 Then
                strIds = Mid(strIds, 2)
                Dim frm As New frmScanOrAttachmentInfo
                If radSearchByApp.Checked = True Then
                    'Nilay (03-Dec-2015) -- Start
                    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                    'frm.displayDialog(Language.getMessage(mstrModuleName, 10, "Select Applicant"), enImg_Email_RefId.Applicant_Module, enAction.EDIT_ONE, strIds)

                    'S.SANDEEP |26-APR-2019| -- START
                    'frm.displayDialog(Language.getMessage(mstrModuleName, 10, "Select Applicant"), enImg_Email_RefId.Applicant_Module, _
                    '                  enAction.EDIT_ONE, strIds, mstrModuleName, CInt(cboApplicant.SelectedValue), , , False) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
                    frm.displayDialog(Language.getMessage(mstrModuleName, 10, "Select Applicant"), enImg_Email_RefId.Applicant_Module, _
                                      enAction.EDIT_ONE, strIds, mstrModuleName, True, CInt(cboApplicant.SelectedValue), , , False) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
                    'S.SANDEEP |26-APR-2019| -- END
                    
                    'Nilay (03-Dec-2015) -- End

                ElseIf radSearchByEmp.Checked = True Then
                    'Nilay (03-Dec-2015) -- Start
                    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                    'frm.displayDialog(Language.getMessage(mstrModuleName, 9, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.EDIT_ONE, strIds)
                    'S.SANDEEP |26-APR-2019| -- START
                    'frm.displayDialog(Language.getMessage(mstrModuleName, 9, "Select Employee"), enImg_Email_RefId.Employee_Module, _
                    '                  enAction.EDIT_ONE, strIds, mstrModuleName, CInt(cboEmployee.SelectedValue), , , True) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END

                    frm.displayDialog(Language.getMessage(mstrModuleName, 9, "Select Employee"), enImg_Email_RefId.Employee_Module, _
                                      enAction.EDIT_ONE, strIds, mstrModuleName, True, CInt(cboEmployee.SelectedValue), , , True) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
                    'S.SANDEEP |26-APR-2019| -- END

                    'Nilay (03-Dec-2015) -- End
                End If
            End If
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvScanAttachmentList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one scan/attachment entry in order to perform operatation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim strIds As String = String.Empty

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            Dim blnMessageShown As Boolean = False
            'Nilay (03-Dec-2015) -- End

            For Each litems As ListViewItem In lvScanAttachmentList.CheckedItems

                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                If CBool(litems.SubItems(objcolhIsMandatoryForNewEmp.Index).Text) = True Then
                    If blnMessageShown = False Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You are not authorized to delete mandatory documents from this list. If you press YES then only NON mandatory documents amongst the selected transactions will be deleted. Mandatory documents can be deleted from employee master add/edit screen -> Operation -> scan/attach document. Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        Else
                            blnMessageShown = True
                            Continue For
                        End If
                    Else
                        Continue For
                    End If
                End If
                'Nilay (03-Dec-2015) -- End

                'Sohail (26 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                If CInt(litems.SubItems(objcolhDocType.Index).Tag) = enScanAttactRefId.ASSET_DECLARATION Then
                    Dim objAssetDeclare As New clsAssetdeclaration_master

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objAssetDeclare._Assetdeclarationunkid = CInt(litems.SubItems(colhValue.Index).Tag)
                    objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(litems.SubItems(colhValue.Index).Tag)
                    'Shani(24-Aug-2015) -- End
                    If objAssetDeclare._Isfinalsaved = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry! Asset Declaration is already Final Saved for ") & litems.SubItems(colhName.Index).Text & ". " & Language.getMessage(mstrModuleName, 11, "Please Uncheck the transaction."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                'Sohail (26 Mar 2012) -- End

                strIds &= "," & CStr(litems.Tag)

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                Dim strError As String = ""
                Dim xRow() As DataRow = dtTable.Select("scanattachtranunkid=" & litems.Tag.ToString)
                Dim strFileName As String = xRow(0).Item("fileuniquename").ToString
                Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(xRow(0).Item("scanattachrefid").ToString)) Select (p.Item("Name").ToString)).FirstOrDefault
                If mblnIsSelfServiceInstall Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If clsFileUploadDownload.DeleteFile(xRow(0).Item("filepath").ToString, strFileName, mstrFolderName, strError) = False Then
                    'Gajanan [8-April-2019] -- Start
                    'If clsFileUploadDownload.DeleteFile(xRow(0).Item("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                    If clsFileUploadDownload.DeleteFile(xRow(0).Item("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                        'Gajanan [8-April-2019] -- End
                        'Shani(24-Aug-2015) -- End

                        eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Else
                    Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                    If File.Exists(strDocLocalPath) Then
                        File.Delete(strDocLocalPath)
                    End If
                End If
                'SHANI (16 JUL 2015) -- End 

            Next

            If strIds.Trim.Length > 0 Then
                strIds = Mid(strIds, 2)

                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                'If objDocument.Delete(strIds) = False Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Problem in deleting information."), enMsgBoxStyle.Information)
                'Else
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Checked information successfully deleted."), enMsgBoxStyle.Information)
                '    Call FillList()
                'End If
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete this attachment?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                If objDocument.Delete(strIds) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Problem in deleting information."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Checked information successfully deleted."), enMsgBoxStyle.Information)
                    Call FillList()
                End If
            End If
                'Nilay (03-Dec-2015) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnASearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnASearch.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboApplicant.ValueMember
                .DisplayMember = cboApplicant.DisplayMember
                .CodeMember = "applicant_code"
                .DataSource = CType(cboApplicant.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboApplicant.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnASearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnESearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnESearch.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnESearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEClose.Click
        Try
            Call btnClose_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If lvScanAttachmentList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one scan/attachment entry in order to perform operatation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Dim objLetterFields As New clsLetterFields
            'If radSearchByEmp.Checked = True Then
            '    Dim objEmp As New clsEmployee_Master
            '    objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            '    If objEmp._Email.Trim.Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry,Email address of selected employee is not define. Please define email address first."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    Else
            '        mstrEmailAddress = cboEmployee.Text & " " & "<" & objEmp._Email.Trim & ">"
            '        dsEmailData = objLetterFields.GetEmployeeData(CStr(cboEmployee.SelectedValue), enImg_Email_RefId.Employee_Module, "List")
            '    End If
            '    objEmp = Nothing
            'ElseIf radSearchByApp.Checked = True Then
            '    Dim objApplicant As New clsApplicant_master
            '    objApplicant._Applicantunkid = CInt(cboApplicant.SelectedValue)
            '    If objApplicant._Email.Trim.Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry,Email address of selected applicant is not define. Please define email address first."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    Else
            '        mstrEmailAddress = cboApplicant.Text & " " & "<" & objApplicant._Email.Trim & ">"
            '        dsEmailData = objLetterFields.GetEmployeeData(CStr(cboApplicant.SelectedValue), enImg_Email_RefId.Applicant_Module, "List")
            '    End If
            '    objApplicant = Nothing
            'End If

            For Each lvItem As ListViewItem In lvScanAttachmentList.CheckedItems
                mstrAttachedFiles &= "|" & lvItem.SubItems(objcolhFullPath.Index).Text.ToString
            Next

            If mstrAttachedFiles.Trim.Length > 0 Then mstrAttachedFiles = Mid(mstrAttachedFiles, 2)
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub radSearchByEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radSearchByEmp.CheckedChanged
        'Hemant (01 Nov 2021) -- Start
        'ENHANCEMENT : OLD-506 - Internal applicants CVs cannot be viewed on desktop (From Applicants list screen). Make search by employee option active on applicant master > Preview attachments.
        Dim objAMaster As New clsApplicant_master
        'Hemant (01 Nov 2021) -- End
        Try
            If radSearchByEmp.Checked = True Then
                objpnlEmp.Visible = True
                objpnlApplicant.Visible = False
                cboApplicant.SelectedValue = 0
                lvScanAttachmentList.Items.Clear()
                'Hemant (29 Dec 2020) -- Start
                'Enhancement # AH-2017 : PACT TZ-Assist to download CV in bulk by particular Vacancy 
                lblVacancy.Visible = False
                cboVacancy.Visible = False
                cboVacancy.SelectedValue = 0
                'Hemant (29 Dec 2020) -- End
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-506 - Internal applicants CVs cannot be viewed on desktop (From Applicants list screen). Make search by employee option active on applicant master > Preview attachments.
                objAMaster._Applicantunkid = mintEmployeeUnkId
                cboEmployee.SelectedValue = objAMaster._Employeeunkid
                'Hemant (01 Nov 2021) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radSearchByEmp_CheckedChanged", mstrModuleName)
        Finally
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-506 - Internal applicants CVs cannot be viewed on desktop (From Applicants list screen). Make search by employee option active on applicant master > Preview attachments.
            objAMaster = Nothing
            'Hemant (01 Nov 2021) -- End
        End Try
    End Sub

    Private Sub radSearchByApp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radSearchByApp.CheckedChanged
        Try
            If radSearchByApp.Checked = True Then
                objpnlApplicant.Visible = True
                objpnlEmp.Visible = False
                cboEmployee.SelectedValue = 0
                lvScanAttachmentList.Items.Clear()
                'Hemant (29 Dec 2020) -- Start
                'Enhancement # AH-2017 : PACT TZ-Assist to download CV in bulk by particular Vacancy 
                lblVacancy.Visible = True
                cboVacancy.Visible = True
                cboVacancy.SelectedValue = 0
                'Hemant (29 Dec 2020) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radSearchByApp_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPreview.Click
        Try
            If lvScanAttachmentList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one scan/attachment entry in order to perform operatation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim strIds As String = String.Empty

            For Each lvItem As ListViewItem In lvScanAttachmentList.CheckedItems
                strIds &= "," & lvItem.Tag.ToString
            Next

            If strIds.Trim.Length > 0 Then
                strIds = Mid(strIds, 2)
                Dim frm As New frmPreviewDocuments
                frm.displayDialog(strIds)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreview_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        Try
            If lvScanAttachmentList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one scan/attachment entry in order to perform operatation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If objPrintDialog1.ShowDialog() <> DialogResult.Cancel Then
                Dim StrFile As String = ""
                For Each lvItem As ListViewItem In lvScanAttachmentList.CheckedItems
                    'SHANI (16 JUL 2015) -- Start
                    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                    'Upload Image folder to access those attachemts from Server and 
                    'all client machines if ArutiSelfService is installed otherwise save then on Document path
                    'If ConfigParameter._Object._Document_Path.LastIndexOf("\") = ConfigParameter._Object._Document_Path.Length - 1 Then
                    '    StrFile = ConfigParameter._Object._Document_Path & lvItem.SubItems(objcolhDocType.Index).Text & "\" & lvItem.SubItems(colhFileName.Index).Text
                    'Else
                    '    StrFile = ConfigParameter._Object._Document_Path & "\" & lvItem.SubItems(objcolhDocType.Index).Text & "\" & lvItem.SubItems(colhFileName.Index).Text
                    'End If
                    If mblnIsSelfServiceInstall Then
                        Dim strError As String = ""
                        Dim xRow() As DataRow = dtTable.Select("scanattachtranunkid=" & lvItem.Tag.ToString)

                        If xRow(0).Item("temppath").ToString.Trim <> "" Then
                            If System.IO.File.Exists(xRow(0).Item("temppath").ToString.Trim) Then
                                StrFile = xRow(0).Item("temppath").ToString.Trim
                            Else
                                StrFile = CreateImage(xRow(0))
                                If StrFile <> "" Then
                                    xRow(0).Item("temppath") = StrFile
                                    xRow(0).AcceptChanges()
                                End If
                            End If
                    Else
                            StrFile = CreateImage(xRow(0))
                            If StrFile <> "" Then
                                xRow(0).Item("temppath") = StrFile
                                xRow(0).AcceptChanges()
                    End If
                        End If
                    Else
                        StrFile = lvItem.SubItems(objcolhFullPath.Index).Text
                    End If
                    'SHANI (16 JUL 2015) -- End 

                    If IO.File.Exists(StrFile) Then
                        If clsScan_Attach_Documents.IsValidImage(StrFile) Then
                            img = System.Drawing.Image.FromFile(StrFile)
                            objPrintDocument1.Print()
                        Else
                            Dim psi As New ProcessStartInfo
                            psi.UseShellExecute = True
                            psi.Verb = "print"
                            psi.WindowStyle = ProcessWindowStyle.Hidden
                            psi.Arguments = objPrintDialog1.PrinterSettings.PrinterName.ToString()
                            psi.FileName = StrFile
                            Try
                                Process.Start(psi)
                            Catch ex As Exception
                                Continue For
                            End Try

                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrint_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles objPrintDocument1.PrintPage
        'Dim sz As New SizeF(100 * img.Width / img.HorizontalResolution, _
        '            100 * img.Height / img.VerticalResolution)

        'Dim p As New PointF((e.PageBounds.Width - sz.Width) / 2, _
        '                    (e.PageBounds.Height - sz.Height) / 2)

        e.Graphics.DrawImage(img, e.MarginBounds.Left, e.MarginBounds.Top)
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvScanAttachmentList.ItemChecked, AddressOf lvScanAttachmentList_ItemChecked

            For Each lvItem As ListViewItem In lvScanAttachmentList.Items
                ''Nilay (03-Dec-2015) -- Start
                ''ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                'If CBool(lvItem.SubItems(objcolhIsMandatoryForNewEmp.Index).Text) = True Then Continue For
                ''Nilay (03-Dec-2015) -- End
                lvItem.Checked = CBool(objchkAll.CheckState)
            Next
            AddHandler lvScanAttachmentList.ItemChecked, AddressOf lvScanAttachmentList_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvScanAttachmentList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvScanAttachmentList.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged


            ''Nilay (03-Dec-2015) -- Start
            ''ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'If CBool(e.Item.SubItems(objcolhIsMandatoryForNewEmp.Index).Text) = True Then
            '    e.Item.Checked = False
            'End If
            ''Nilay (03-Dec-2015) -- End

            If lvScanAttachmentList.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvScanAttachmentList.CheckedItems.Count < lvScanAttachmentList.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvScanAttachmentList.CheckedItems.Count = lvScanAttachmentList.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvScanAttachmentList_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuSendMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSendMail_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |25-JAN-2019| -- START
    'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
    Private Sub mnuDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDownload.Click
        Try

            objDocument = New clsScan_Attach_Documents()
            If lvScanAttachmentList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one scan/attachment entry in order to perform operatation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim strIds As String = String.Empty
            strIds = String.Join(",", lvScanAttachmentList.Items.Cast(Of ListViewItem).AsEnumerable().Where(Function(x) x.Checked = True).Select(Function(x) x.Tag.ToString()).ToArray())
            'S.SANDEEP |04-SEP-2021| -- START
            'Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", strIds)
            Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", strIds, , , , , , CBool(IIf(strIds.Trim.Length <= 0, True, False)), , , , True)
            'S.SANDEEP |04-SEP-2021| -- END
            Dim mdtTran As DataTable = objDocument._Datatable.Copy
            If mdtTran.Columns.Contains("temppath") = False Then mdtTran.Columns.Add("temppath", Type.GetType("System.String"))
            'S.SANDEEP |03-APR-2019| -- START
            If mdtTran.Columns.Contains("error") = False Then mdtTran.Columns.Add("error", Type.GetType("System.String"))
            Dim strError As String = String.Empty
            'S.SANDEEP |03-APR-2019| -- END
            Dim strLocalPath As String = String.Empty
            Dim fileToAdd As New List(Of String)
            For Each xRow As DataRow In mdtTran.Rows
                If IsDBNull(xRow("file_data")) = False Then
                    strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                    Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
                    Dim fs As New FileStream(strLocalPath, FileMode.Create)
                    ms.WriteTo(fs)
                    ms.Close()
                    fs.Close()
                    fs.Dispose()
                    If strLocalPath <> "" Then
                        fileToAdd.Add(strLocalPath)
                    End If
                    'S.SANDEEP |03-APR-2019| -- START
                ElseIf IsDBNull(xRow("file_data")) = True Then
                    If mblnIsSelfServiceInstall Then
                        If xRow("temppath").ToString.Trim <> "" Then
                            If System.IO.File.Exists(xRow("temppath").ToString.Trim) Then
                                strLocalPath = xRow("temppath").ToString.Trim
                            End If
                        Else
                            strLocalPath = CreateImage(xRow, strError)
                            xRow("error") = strError
                        End If
                    Else
                        strLocalPath = xRow("filepath").ToString()
                        If IO.File.Exists(strLocalPath) = False Then
                            xRow("error") = Language.getMessage(mstrModuleName, 14, "File not found.")
                        End If
                    End If
                    If strLocalPath <> "" Then
                        fileToAdd.Add(strLocalPath)
                    End If
                    'S.SANDEEP |03-APR-2019| -- END
                End If
            Next

            'S.SANDEEP |03-APR-2019| -- START
            mdtTran.AcceptChanges()
            Dim objValid As New frmCommonValidationList
            If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("error") <> "").Count() > 0 Then
                Dim eRData As DataTable = mdtTran.DefaultView.ToTable(True, "error", "fileuniquename")
                objValid.displayDialog(False, Language.getMessage(mstrModuleName, 15, "Download Issue(s)."), eRData)
                Exit Try
            End If
            'S.SANDEEP |03-APR-2019| -- END


            If fileToAdd.Count > 0 Then
                'Hemant (20 Nov 2019) -- Start
                'ISSUE/ENHANCEMENT(PACT) :  Ability to download attachments directly and not in zip format
                'Dim sfd As New SaveFileDialog() : sfd.Filter = "Zip File (.zip)|*.zip"
                'If sfd.ShowDialog() = DialogResult.OK Then
                '    Dim fl As New System.IO.FileInfo(sfd.FileName)
                '    AddFileToZip(fl.FullName, fileToAdd, CompressionOption.Normal)
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "File(s) downloaded successfully to the selected location."), enMsgBoxStyle.Information)
                'End If
                Dim fbd As New FolderBrowserDialog()
                fbd.ShowNewFolderButton = True
                If (fbd.ShowDialog() = DialogResult.OK) Then
                    Dim fl As New System.IO.FileInfo(fbd.SelectedPath)
                    For Each item As String In fileToAdd
                        Dim destFilename As String = ".\" & Path.GetFileName(item)
                        System.IO.File.Copy(item.ToString, fl.FullName.ToString & "\" & Path.GetFileName(item))
                    Next
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "File(s) downloaded successfully to the selected location."), enMsgBoxStyle.Information)
                End If
                'Hemant (20 Nov 2019) -- End
            End If
            If fileToAdd.Count > 0 Then
                For Each fl As String In fileToAdd
                    Try
                        System.IO.File.Delete(fl)
                    Catch ex As Exception

                    End Try
                Next
            End If
            

            'Dim strIds As String = String.Empty
            'strIds = String.Join(",", lvScanAttachmentList.Items.Cast(Of ListViewItem).AsEnumerable().Where(Function(x) x.Checked = True).Select(Function(x) x.Tag.ToString()).ToArray())
            'Dim strFileList As List(Of String) = Nothing
            'Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", strIds)
            'Dim mdtTran As DataTable = objDocument._Datatable.Copy
            'If mdtTran.Columns.Contains("temppath") = False Then mdtTran.Columns.Add("temppath", Type.GetType("System.String"))
            'Dim strFilePath As String = String.Empty
            'Dim strLocalPath As String = String.Empty
            'Dim sfd As New SaveFileDialog()
            'sfd.Filter = "Zip File (.zip)|*.zip"
            'Dim buffer = New Byte(4095) {}
            'If sfd.ShowDialog() = DialogResult.OK Then
            '    Dim fl As New System.IO.FileInfo(sfd.FileName)
            '    Using stream = New ZipOutputStream(System.IO.File.Create(fl.FullName))
            '        'stream.Password = "@123@"
            '        For Each row As DataRow In mdtTran.Rows
            '            Dim entry = stream.PutNextEntry(row("filename").ToString())
            '            Using ms = New System.IO.MemoryStream(CType(row("file_data"), Byte()))
            '                Dim sourceBytes As Integer
            '                Do
            '                    sourceBytes = ms.Read(buffer, 0, buffer.Length)
            '                    stream.Write(buffer, 0, sourceBytes)
            '                Loop While sourceBytes > 0
            '            End Using
            '        Next
            '        stream.Flush()
            '        stream.Close()
            '    End Using
            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuDownload_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |25-JAN-2019| -- END


#End Region

	
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnEClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.lblDocType.Text = Language._Object.getCaption(Me.lblDocType.Name, Me.lblDocType.Text)
			Me.lblDocument.Text = Language._Object.getCaption(Me.lblDocument.Name, Me.lblDocument.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDocument.Text = Language._Object.getCaption(CStr(Me.colhDocument.Tag), Me.colhDocument.Text)
			Me.colhFileName.Text = Language._Object.getCaption(CStr(Me.colhFileName.Tag), Me.colhFileName.Text)
			Me.mnuPrint.Text = Language._Object.getCaption(Me.mnuPrint.Name, Me.mnuPrint.Text)
			Me.mnuPreview.Text = Language._Object.getCaption(Me.mnuPreview.Name, Me.mnuPreview.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.radSearchByEmp.Text = Language._Object.getCaption(Me.radSearchByEmp.Name, Me.radSearchByEmp.Text)
			Me.radSearchByApp.Text = Language._Object.getCaption(Me.radSearchByApp.Name, Me.radSearchByApp.Text)
			Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
			Me.colhValue.Text = Language._Object.getCaption(CStr(Me.colhValue.Tag), Me.colhValue.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnEClose.Text = Language._Object.getCaption(Me.btnEClose.Name, Me.btnEClose.Text)
            Me.mnuDownload.Text = Language._Object.getCaption(Me.mnuDownload.Name, Me.mnuDownload.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one applicant to fill list.")
			Language.setMessage(mstrModuleName, 2, "Please select atleast one employee to fill list.")
			Language.setMessage(mstrModuleName, 3, "Please check atleast one scan/attachment entry in order to perform operatation.")
			Language.setMessage(mstrModuleName, 4, "Problem in deleting information.")
			Language.setMessage(mstrModuleName, 5, "Checked information successfully deleted.")
			Language.setMessage(mstrModuleName, 7, "Are you sure you want to delete this attachment?")
			Language.setMessage(mstrModuleName, 8, "Sorry! Asset Declaration is already Final Saved for")
			Language.setMessage(mstrModuleName, 9, "Select Employee")
			Language.setMessage(mstrModuleName, 10, "Select Applicant")
			Language.setMessage(mstrModuleName, 11, "Please Uncheck the transaction.")
			Language.setMessage(mstrModuleName, 12, "You are not authorized to delete mandatory documents from this list. If you press YES then only NON mandatory documents amongst the selected transactions will be deleted. Mandatory documents can be deleted from employee master add/edit screen -> Operation -> scan/attach document. Do you wish to continue?")
            Language.setMessage(mstrModuleName, 13, "File(s) downloaded successfully to the selected location.")
            Language.setMessage(mstrModuleName, 14, "File not found.")
            Language.setMessage(mstrModuleName, 15, "Download Issue(s).")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class