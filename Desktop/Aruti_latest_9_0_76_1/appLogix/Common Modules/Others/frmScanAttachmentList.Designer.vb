﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScanAttachmentList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmScanAttachmentList))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDownload = New System.Windows.Forms.ToolStripMenuItem
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnESearch = New eZee.Common.eZeeGradientButton
        Me.cboDocument = New System.Windows.Forms.ComboBox
        Me.lblDocType = New System.Windows.Forms.Label
        Me.cboDocumentType = New System.Windows.Forms.ComboBox
        Me.lblDocument = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnlApplicant = New System.Windows.Forms.Panel
        Me.lblApplicant = New System.Windows.Forms.Label
        Me.cboApplicant = New System.Windows.Forms.ComboBox
        Me.objbtnASearch = New eZee.Common.eZeeGradientButton
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.radSearchByEmp = New System.Windows.Forms.RadioButton
        Me.radSearchByApp = New System.Windows.Forms.RadioButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lvScanAttachmentList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhDocument = New System.Windows.Forms.ColumnHeader
        Me.colhFileName = New System.Windows.Forms.ColumnHeader
        Me.objcolhDocType = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpId = New System.Windows.Forms.ColumnHeader
        Me.colhValue = New System.Windows.Forms.ColumnHeader
        Me.objcolhFullPath = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsMandatoryForNewEmp = New System.Windows.Forms.ColumnHeader
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.objPrintDialog1 = New System.Windows.Forms.PrintDialog
        Me.objPrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.objeFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.objFooter.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.objpnlApplicant.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objeFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(802, 58)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Scan/Attachment List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 405)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(802, 55)
        Me.objFooter.TabIndex = 130
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.ContextMenuStrip = Me.cmnuOperations
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(107, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperations
        Me.btnOperation.TabIndex = 129
        Me.btnOperation.Text = "&Operations"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPreview, Me.mnuPrint, Me.mnuDownload})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(170, 70)
        '
        'mnuPreview
        '
        Me.mnuPreview.Name = "mnuPreview"
        Me.mnuPreview.Size = New System.Drawing.Size(169, 22)
        Me.mnuPreview.Text = "P&review"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(169, 22)
        Me.mnuPrint.Text = "&Print"
        Me.mnuPrint.Visible = False
        '
        'mnuDownload
        '
        Me.mnuDownload.Name = "mnuDownload"
        Me.mnuDownload.Size = New System.Drawing.Size(169, 22)
        Me.mnuDownload.Text = "Download && View"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(487, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 128
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        Me.btnEdit.Visible = False
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(384, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 128
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        Me.btnNew.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(590, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 126
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(693, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 121
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnESearch
        '
        Me.objbtnESearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnESearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnESearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnESearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnESearch.BorderSelected = False
        Me.objbtnESearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnESearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnESearch.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Search
        Me.objbtnESearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnESearch.Location = New System.Drawing.Point(308, 1)
        Me.objbtnESearch.Name = "objbtnESearch"
        Me.objbtnESearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnESearch.TabIndex = 143
        '
        'cboDocument
        '
        Me.cboDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDocument.FormattingEnabled = True
        Me.cboDocument.Location = New System.Drawing.Point(408, 33)
        Me.cboDocument.Name = "cboDocument"
        Me.cboDocument.Size = New System.Drawing.Size(128, 21)
        Me.cboDocument.TabIndex = 142
        '
        'lblDocType
        '
        Me.lblDocType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocType.Location = New System.Drawing.Point(546, 36)
        Me.lblDocType.Name = "lblDocType"
        Me.lblDocType.Size = New System.Drawing.Size(88, 15)
        Me.lblDocType.TabIndex = 141
        Me.lblDocType.Text = "Document Type"
        Me.lblDocType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDocumentType
        '
        Me.cboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDocumentType.FormattingEnabled = True
        Me.cboDocumentType.Location = New System.Drawing.Point(640, 33)
        Me.cboDocumentType.Name = "cboDocumentType"
        Me.cboDocumentType.Size = New System.Drawing.Size(128, 21)
        Me.cboDocumentType.TabIndex = 140
        '
        'lblDocument
        '
        Me.lblDocument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocument.Location = New System.Drawing.Point(340, 36)
        Me.lblDocument.Name = "lblDocument"
        Me.lblDocument.Size = New System.Drawing.Size(62, 15)
        Me.lblDocument.TabIndex = 139
        Me.lblDocument.Text = "Document"
        Me.lblDocument.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(123, 1)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(179, 21)
        Me.cboEmployee.TabIndex = 138
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(3, 4)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(114, 15)
        Me.lblEmployee.TabIndex = 137
        Me.lblEmployee.Text = "Select Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.objpnlApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.objpnlEmp)
        Me.gbFilterCriteria.Controls.Add(Me.radSearchByEmp)
        Me.gbFilterCriteria.Controls.Add(Me.radSearchByApp)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboDocument)
        Me.gbFilterCriteria.Controls.Add(Me.lblDocType)
        Me.gbFilterCriteria.Controls.Add(Me.lblDocument)
        Me.gbFilterCriteria.Controls.Add(Me.cboDocumentType)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(778, 85)
        Me.gbFilterCriteria.TabIndex = 144
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlApplicant
        '
        Me.objpnlApplicant.Controls.Add(Me.lblApplicant)
        Me.objpnlApplicant.Controls.Add(Me.cboApplicant)
        Me.objpnlApplicant.Controls.Add(Me.objbtnASearch)
        Me.objpnlApplicant.Location = New System.Drawing.Point(6, 33)
        Me.objpnlApplicant.Name = "objpnlApplicant"
        Me.objpnlApplicant.Size = New System.Drawing.Size(330, 23)
        Me.objpnlApplicant.TabIndex = 151
        '
        'lblApplicant
        '
        Me.lblApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicant.Location = New System.Drawing.Point(3, 4)
        Me.lblApplicant.Name = "lblApplicant"
        Me.lblApplicant.Size = New System.Drawing.Size(114, 15)
        Me.lblApplicant.TabIndex = 137
        Me.lblApplicant.Text = "Select Applicant"
        Me.lblApplicant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApplicant
        '
        Me.cboApplicant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplicant.DropDownWidth = 350
        Me.cboApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplicant.FormattingEnabled = True
        Me.cboApplicant.Location = New System.Drawing.Point(123, 1)
        Me.cboApplicant.Name = "cboApplicant"
        Me.cboApplicant.Size = New System.Drawing.Size(179, 21)
        Me.cboApplicant.TabIndex = 138
        '
        'objbtnASearch
        '
        Me.objbtnASearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnASearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnASearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnASearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnASearch.BorderSelected = False
        Me.objbtnASearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnASearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnASearch.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Search
        Me.objbtnASearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnASearch.Location = New System.Drawing.Point(308, 1)
        Me.objbtnASearch.Name = "objbtnASearch"
        Me.objbtnASearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnASearch.TabIndex = 143
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.lblEmployee)
        Me.objpnlEmp.Controls.Add(Me.cboEmployee)
        Me.objpnlEmp.Controls.Add(Me.objbtnESearch)
        Me.objpnlEmp.Location = New System.Drawing.Point(6, 33)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(331, 23)
        Me.objpnlEmp.TabIndex = 150
        '
        'radSearchByEmp
        '
        Me.radSearchByEmp.BackColor = System.Drawing.Color.Transparent
        Me.radSearchByEmp.Checked = True
        Me.radSearchByEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSearchByEmp.Location = New System.Drawing.Point(427, 4)
        Me.radSearchByEmp.Name = "radSearchByEmp"
        Me.radSearchByEmp.Size = New System.Drawing.Size(145, 17)
        Me.radSearchByEmp.TabIndex = 148
        Me.radSearchByEmp.TabStop = True
        Me.radSearchByEmp.Text = "Search By Employee"
        Me.radSearchByEmp.UseVisualStyleBackColor = False
        '
        'radSearchByApp
        '
        Me.radSearchByApp.BackColor = System.Drawing.Color.Transparent
        Me.radSearchByApp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSearchByApp.Location = New System.Drawing.Point(578, 4)
        Me.radSearchByApp.Name = "radSearchByApp"
        Me.radSearchByApp.Size = New System.Drawing.Size(145, 17)
        Me.radSearchByApp.TabIndex = 148
        Me.radSearchByApp.Text = "Search By Applicant"
        Me.radSearchByApp.UseVisualStyleBackColor = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(752, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 146
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(729, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 145
        Me.objbtnSearch.TabStop = False
        '
        'lvScanAttachmentList
        '
        Me.lvScanAttachmentList.BackColorOnChecked = False
        Me.lvScanAttachmentList.CheckBoxes = True
        Me.lvScanAttachmentList.ColumnHeaders = Nothing
        Me.lvScanAttachmentList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhCode, Me.colhName, Me.colhDocument, Me.colhFileName, Me.objcolhDocType, Me.objcolhEmpId, Me.colhValue, Me.objcolhFullPath, Me.objcolhIsMandatoryForNewEmp})
        Me.lvScanAttachmentList.CompulsoryColumns = ""
        Me.lvScanAttachmentList.FullRowSelect = True
        Me.lvScanAttachmentList.GridLines = True
        Me.lvScanAttachmentList.GroupingColumn = Nothing
        Me.lvScanAttachmentList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvScanAttachmentList.HideSelection = False
        Me.lvScanAttachmentList.Location = New System.Drawing.Point(12, 155)
        Me.lvScanAttachmentList.MinColumnWidth = 50
        Me.lvScanAttachmentList.MultiSelect = False
        Me.lvScanAttachmentList.Name = "lvScanAttachmentList"
        Me.lvScanAttachmentList.OptionalColumns = ""
        Me.lvScanAttachmentList.ShowMoreItem = False
        Me.lvScanAttachmentList.ShowSaveItem = False
        Me.lvScanAttachmentList.ShowSelectAll = True
        Me.lvScanAttachmentList.ShowSizeAllColumnsToFit = True
        Me.lvScanAttachmentList.Size = New System.Drawing.Size(778, 244)
        Me.lvScanAttachmentList.Sortable = True
        Me.lvScanAttachmentList.TabIndex = 145
        Me.lvScanAttachmentList.UseCompatibleStateImageBehavior = False
        Me.lvScanAttachmentList.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 85
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Employee/Applicant"
        Me.colhName.Width = 165
        '
        'colhDocument
        '
        Me.colhDocument.Tag = "colhDocument"
        Me.colhDocument.Text = "Document"
        Me.colhDocument.Width = 160
        '
        'colhFileName
        '
        Me.colhFileName.Tag = "colhFileName"
        Me.colhFileName.Text = "Filename"
        Me.colhFileName.Width = 160
        '
        'objcolhDocType
        '
        Me.objcolhDocType.DisplayIndex = 6
        Me.objcolhDocType.Tag = "objcolhDocType"
        Me.objcolhDocType.Text = ""
        Me.objcolhDocType.Width = 0
        '
        'objcolhEmpId
        '
        Me.objcolhEmpId.DisplayIndex = 7
        Me.objcolhEmpId.Tag = "objcolhEmpId"
        Me.objcolhEmpId.Text = ""
        Me.objcolhEmpId.Width = 0
        '
        'colhValue
        '
        Me.colhValue.DisplayIndex = 5
        Me.colhValue.Tag = "colhValue"
        Me.colhValue.Text = "Associated Value"
        Me.colhValue.Width = 155
        '
        'objcolhFullPath
        '
        Me.objcolhFullPath.Tag = "objcolhFullPath"
        Me.objcolhFullPath.Text = ""
        Me.objcolhFullPath.Width = 0
        '
        'objcolhIsMandatoryForNewEmp
        '
        Me.objcolhIsMandatoryForNewEmp.Text = "objcolhIsMandatoryForNewEmp"
        Me.objcolhIsMandatoryForNewEmp.Width = 0
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(20, 159)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 146
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'objPrintDialog1
        '
        Me.objPrintDialog1.UseEXDialog = True
        '
        'objPrintDocument1
        '
        '
        'objeFooter
        '
        Me.objeFooter.BorderColor = System.Drawing.Color.Silver
        Me.objeFooter.Controls.Add(Me.btnOk)
        Me.objeFooter.Controls.Add(Me.btnEClose)
        Me.objeFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objeFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objeFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objeFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objeFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objeFooter.Location = New System.Drawing.Point(0, 350)
        Me.objeFooter.Name = "objeFooter"
        Me.objeFooter.Size = New System.Drawing.Size(802, 55)
        Me.objeFooter.TabIndex = 147
        Me.objeFooter.Visible = False
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(590, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 126
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnEClose
        '
        Me.btnEClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEClose.BackColor = System.Drawing.Color.White
        Me.btnEClose.BackgroundImage = CType(resources.GetObject("btnEClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnEClose.FlatAppearance.BorderSize = 0
        Me.btnEClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEClose.ForeColor = System.Drawing.Color.Black
        Me.btnEClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Location = New System.Drawing.Point(693, 13)
        Me.btnEClose.Name = "btnEClose"
        Me.btnEClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Size = New System.Drawing.Size(97, 30)
        Me.btnEClose.TabIndex = 121
        Me.btnEClose.Text = "&Close"
        Me.btnEClose.UseVisualStyleBackColor = True
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(9, 64)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(114, 15)
        Me.lblVacancy.TabIndex = 153
        Me.lblVacancy.Text = "Vacancy"
        Me.lblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 350
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(129, 61)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(179, 21)
        Me.cboVacancy.TabIndex = 154
        '
        'frmScanAttachmentList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(802, 460)
        Me.Controls.Add(Me.objchkAll)
        Me.Controls.Add(Me.lvScanAttachmentList)
        Me.Controls.Add(Me.objeFooter)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmScanAttachmentList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Scan Attachment List"
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.objpnlApplicant.ResumeLayout(False)
        Me.objpnlEmp.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objeFooter.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents objbtnESearch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDocument As System.Windows.Forms.ComboBox
    Friend WithEvents lblDocType As System.Windows.Forms.Label
    Friend WithEvents cboDocumentType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDocument As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lvScanAttachmentList As eZee.Common.eZeeListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDocument As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFileName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDocType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents radSearchByEmp As System.Windows.Forms.RadioButton
    Friend WithEvents radSearchByApp As System.Windows.Forms.RadioButton
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objpnlApplicant As System.Windows.Forms.Panel
    Friend WithEvents lblApplicant As System.Windows.Forms.Label
    Friend WithEvents cboApplicant As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnASearch As eZee.Common.eZeeGradientButton
    Friend WithEvents colhValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents objPrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents objPrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnEClose As eZee.Common.eZeeLightButton
    Public WithEvents objFooter As eZee.Common.eZeeFooter
    Public WithEvents objeFooter As eZee.Common.eZeeFooter
    Friend WithEvents objcolhFullPath As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsMandatoryForNewEmp As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuDownload As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
End Class
