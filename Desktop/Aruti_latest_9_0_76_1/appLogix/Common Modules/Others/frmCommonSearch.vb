
Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmCommonSearch
    Private ReadOnly mstrModuleName As String = "frmCommonSearch"

#Region " Private Variables "

    Private objDataOperation As clsDataOperation


    Private mblnCancel As Boolean = True

    Private m_DataSource As DataTable
    Private m_Dataview As DataView

    Private m_DisplayMember As String
    Private m_ValueMember As String
    Private m_CodeMember As String

    Private m_SelectedText As String = ""
    Private m_SelectedValue As String = ""
    Private m_SelectedCode As String = ""

    'S.SANDEEP [09 APR 2015] -- START
    Private m_TypedText As String = ""
    Private m_ShowTextAsSelected As Boolean = False
    'S.SANDEEP [09 APR 2015] -- END

#End Region

#Region " Form's Property "

    Public Property DataSource() As DataTable
        Get
            Return m_DataSource
        End Get
        Set(ByVal value As DataTable)
            m_DataSource = value
        End Set
    End Property

    Public WriteOnly Property DisplayMember() As String
        Set(ByVal value As String)
            m_DisplayMember = value
        End Set
    End Property

    Public WriteOnly Property ValueMember() As String
        Set(ByVal value As String)
            m_ValueMember = value
        End Set
    End Property

    Public WriteOnly Property CodeMember() As String
        Set(ByVal value As String)
            m_CodeMember = value
        End Set
    End Property

    Public Property SelectedText() As String
        Get
            Return m_SelectedText
        End Get
        Set(ByVal value As String)
            m_SelectedText = value
        End Set
    End Property

    Public Property SelectedValue() As String
        Get
            Return m_SelectedValue
        End Get
        Set(ByVal value As String)
            m_SelectedValue = value
        End Set
    End Property

    Public Property SelectedAlias() As String
        Get
            Return m_SelectedCode
        End Get
        Set(ByVal value As String)
            m_SelectedCode = value
        End Set
    End Property

    'S.SANDEEP [09 APR 2015] -- START
    Public Property TypedText() As String
        Get
            Return m_TypedText
        End Get
        Set(ByVal value As String)
            m_TypedText = value
        End Set
    End Property

    Public Property TextAsSelected() As Boolean
        Get
            Return m_ShowTextAsSelected
        End Get
        Set(ByVal value As Boolean)
            m_ShowTextAsSelected = value
        End Set
    End Property
    'S.SANDEEP [09 APR 2015] -- END

#End Region

#Region " Display Dialogue "

    Public Function DisplayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Functions & Procedures "

    Private Sub setColor()
        Try
            txtSearch.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Function fillDataGridView() As Boolean
        Try

            dgvSearch.AutoGenerateColumns = False

            'm_DataSource.Rows.RemoveAt(0)


            m_Dataview = New DataView(m_DataSource, "", "", DataViewRowState.CurrentRows)

            dgvSearch.DataSource = m_Dataview

            objcolUnkId.DataPropertyName = m_ValueMember
            objcolCode.DataPropertyName = m_CodeMember
            objcolName.DataPropertyName = m_DisplayMember

            dgvSearch.Refresh()

            Return True
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "fillDataGridView", mstrModuleName)
            Return False
        End Try
        'Change Date : 5 April, 2008 -- End

    End Function
#End Region

#Region " Form "

    Private Sub frmCommonSearch_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            m_DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCommonSearch_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCommonSearch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            Call setColor()
            Call fillDataGridView()

            'S.SANDEEP [09 APR 2015] -- START
            If m_TypedText.Trim.Length > 0 Then
                txtSearch.Text = m_TypedText
                m_TypedText = ""
            End If
            'S.SANDEEP [09 APR 2015] -- END

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCommonSearch_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [09 APR 2015] -- START
    Private Sub frmCommonSearch_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            If m_ShowTextAsSelected = False Then
                If txtSearch.SelectionLength > 0 Then
                    txtSearch.SelectionStart = 1
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommonSearch_Shown", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [09 APR 2015] -- END

    

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)
            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvSearch.Rows.Count > 0 Then
                        If dgvSearch.SelectedRows(0).Index = dgvSearch.Rows(dgvSearch.RowCount - 1).Index Then Exit Sub
                        dgvSearch.Rows(dgvSearch.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvSearch.Rows.Count > 0 Then
                        If dgvSearch.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvSearch.Rows(dgvSearch.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "txtSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim strSearch As String = ""
        Try
            'Sandeep | 04 JAN 2010 | -- Start
            If txtSearch.Text.Contains("'") Or txtSearch.Text.Contains("[") Or txtSearch.Text.Contains("]") Then Exit Sub
            'Sandeep | 04 JAN 2010 | -- END 

            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = m_DisplayMember & " LIKE '%" & txtSearch.Text & "%' "

                'S.SANDEEP [ 12 OCT 2011 ] -- START
                If m_CodeMember <> "" Then
                    strSearch &= " OR " & m_CodeMember & " LIKE '%" & txtSearch.Text & "%'"
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                
                '& _
                '                                        " OR " & m_ValueMember & " LIKE '%" & txtSearch.Text & "%' "
            End If

            'S.SANDEEP [09 APR 2015] -- START
            'm_Dataview.RowFilter = strSearch
            If m_Dataview IsNot Nothing Then
            m_Dataview.RowFilter = strSearch
            End If
            'S.SANDEEP [09 APR 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvSearch_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearch.GotFocus, btnCancel.GotFocus, btnOk.GotFocus
        txtSearch.Focus()
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

        Try

            If dgvSearch.SelectedRows.Count > 0 Then
                mblnCancel = False
                m_SelectedText = dgvSearch.SelectedRows(0).Cells(objcolName.Index).Value
                m_SelectedValue = dgvSearch.SelectedRows(0).Cells(objcolUnkId.Index).Value
                m_SelectedCode = dgvSearch.SelectedRows(0).Cells(objcolCode.Index).Value
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class