﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdvanceSearch
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdvanceSearch))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gvFilterInfo = New System.Windows.Forms.DataGridView
        Me.objdgcolhFilterID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgColhBlank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFilterName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFilterGroupName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFilterIsGrp = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhFilterGroupId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgDetail = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.flpAllocations = New System.Windows.Forms.FlowLayoutPanel
        Me.btnBranch = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDeptGrp = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDept = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSectionGroup = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSection = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUnitGroup = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUnit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnTeam = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnJobGroup = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnJob = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClassGroup = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClass = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnGradeGroup = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnGrade = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnGradeLevel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCostCenterGroup = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCostCenter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmployementType = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReligion = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnGender = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnRelationFromDpndt = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNationality = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnPayType = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnPayPoint = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnMaritalStatus = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnApply = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSkill = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.gvFilterInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpAllocations.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gvFilterInfo)
        Me.pnlMainInfo.Controls.Add(Me.pnlEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.flpAllocations)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(831, 455)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gvFilterInfo
        '
        Me.gvFilterInfo.AllowUserToAddRows = False
        Me.gvFilterInfo.AllowUserToDeleteRows = False
        Me.gvFilterInfo.AllowUserToResizeRows = False
        Me.gvFilterInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gvFilterInfo.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.gvFilterInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.gvFilterInfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.gvFilterInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.gvFilterInfo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhFilterID, Me.objdgColhBlank, Me.dgcolhFilterName, Me.objdgcolhFilterGroupName, Me.objdgcolhFilterIsGrp, Me.objdgcolhFilterGroupId})
        Me.gvFilterInfo.Location = New System.Drawing.Point(482, 3)
        Me.gvFilterInfo.Name = "gvFilterInfo"
        Me.gvFilterInfo.RowHeadersVisible = False
        Me.gvFilterInfo.RowHeadersWidth = 5
        Me.gvFilterInfo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.gvFilterInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvFilterInfo.Size = New System.Drawing.Size(346, 400)
        Me.gvFilterInfo.TabIndex = 287
        '
        'objdgcolhFilterID
        '
        Me.objdgcolhFilterID.HeaderText = "Unkid"
        Me.objdgcolhFilterID.Name = "objdgcolhFilterID"
        Me.objdgcolhFilterID.ReadOnly = True
        Me.objdgcolhFilterID.Visible = False
        '
        'objdgColhBlank
        '
        Me.objdgColhBlank.HeaderText = ""
        Me.objdgColhBlank.Name = "objdgColhBlank"
        Me.objdgColhBlank.ReadOnly = True
        Me.objdgColhBlank.Width = 30
        '
        'dgcolhFilterName
        '
        Me.dgcolhFilterName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhFilterName.HeaderText = "Filter Value"
        Me.dgcolhFilterName.Name = "dgcolhFilterName"
        Me.dgcolhFilterName.ReadOnly = True
        '
        'objdgcolhFilterGroupName
        '
        Me.objdgcolhFilterGroupName.HeaderText = "GroupName"
        Me.objdgcolhFilterGroupName.Name = "objdgcolhFilterGroupName"
        Me.objdgcolhFilterGroupName.ReadOnly = True
        '
        'objdgcolhFilterIsGrp
        '
        Me.objdgcolhFilterIsGrp.HeaderText = "objdgcolhFilterIsGrp"
        Me.objdgcolhFilterIsGrp.Name = "objdgcolhFilterIsGrp"
        Me.objdgcolhFilterIsGrp.ReadOnly = True
        '
        'objdgcolhFilterGroupId
        '
        Me.objdgcolhFilterGroupId.HeaderText = "objdgcolhFilterGroupId"
        Me.objdgcolhFilterGroupId.Name = "objdgcolhFilterGroupId"
        Me.objdgcolhFilterGroupId.ReadOnly = True
        Me.objdgcolhFilterGroupId.Visible = False
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgDetail)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearch)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(227, 3)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(249, 400)
        Me.pnlEmployeeList.TabIndex = 7
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgDetail
        '
        Me.dgDetail.AllowUserToAddRows = False
        Me.dgDetail.AllowUserToDeleteRows = False
        Me.dgDetail.AllowUserToResizeRows = False
        Me.dgDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgDetail.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgDetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhUnkid, Me.dgColhDescription})
        Me.dgDetail.Location = New System.Drawing.Point(1, 28)
        Me.dgDetail.Name = "dgDetail"
        Me.dgDetail.RowHeadersVisible = False
        Me.dgDetail.RowHeadersWidth = 5
        Me.dgDetail.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgDetail.Size = New System.Drawing.Size(246, 372)
        Me.dgDetail.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhUnkid
        '
        Me.objdgcolhUnkid.HeaderText = "Unkid"
        Me.objdgcolhUnkid.Name = "objdgcolhUnkid"
        Me.objdgcolhUnkid.ReadOnly = True
        Me.objdgcolhUnkid.Visible = False
        '
        'dgColhDescription
        '
        Me.dgColhDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhDescription.HeaderText = "Description"
        Me.dgColhDescription.Name = "dgColhDescription"
        Me.dgColhDescription.ReadOnly = True
        '
        'txtSearch
        '
        Me.txtSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearch.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(1, 1)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(245, 21)
        Me.txtSearch.TabIndex = 12
        '
        'flpAllocations
        '
        Me.flpAllocations.AutoScroll = True
        Me.flpAllocations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.flpAllocations.Controls.Add(Me.btnBranch)
        Me.flpAllocations.Controls.Add(Me.btnDeptGrp)
        Me.flpAllocations.Controls.Add(Me.btnDept)
        Me.flpAllocations.Controls.Add(Me.btnSectionGroup)
        Me.flpAllocations.Controls.Add(Me.btnSection)
        Me.flpAllocations.Controls.Add(Me.btnUnitGroup)
        Me.flpAllocations.Controls.Add(Me.btnUnit)
        Me.flpAllocations.Controls.Add(Me.btnTeam)
        Me.flpAllocations.Controls.Add(Me.btnJobGroup)
        Me.flpAllocations.Controls.Add(Me.btnJob)
        Me.flpAllocations.Controls.Add(Me.btnClassGroup)
        Me.flpAllocations.Controls.Add(Me.btnClass)
        Me.flpAllocations.Controls.Add(Me.btnGradeGroup)
        Me.flpAllocations.Controls.Add(Me.btnGrade)
        Me.flpAllocations.Controls.Add(Me.btnGradeLevel)
        Me.flpAllocations.Controls.Add(Me.btnCostCenterGroup)
        Me.flpAllocations.Controls.Add(Me.btnCostCenter)
        Me.flpAllocations.Controls.Add(Me.btnEmployementType)
        Me.flpAllocations.Controls.Add(Me.btnReligion)
        Me.flpAllocations.Controls.Add(Me.btnGender)
        Me.flpAllocations.Controls.Add(Me.btnRelationFromDpndt)
        Me.flpAllocations.Controls.Add(Me.btnNationality)
        Me.flpAllocations.Controls.Add(Me.btnPayType)
        Me.flpAllocations.Controls.Add(Me.btnPayPoint)
        Me.flpAllocations.Controls.Add(Me.btnMaritalStatus)
        Me.flpAllocations.Controls.Add(Me.btnSkill)
        Me.flpAllocations.Location = New System.Drawing.Point(2, 3)
        Me.flpAllocations.Name = "flpAllocations"
        Me.flpAllocations.Size = New System.Drawing.Size(219, 396)
        Me.flpAllocations.TabIndex = 3
        '
        'btnBranch
        '
        Me.btnBranch.BackColor = System.Drawing.Color.White
        Me.btnBranch.BackgroundImage = CType(resources.GetObject("btnBranch.BackgroundImage"), System.Drawing.Image)
        Me.btnBranch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBranch.BorderColor = System.Drawing.Color.Empty
        Me.btnBranch.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnBranch.FlatAppearance.BorderSize = 0
        Me.btnBranch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBranch.ForeColor = System.Drawing.Color.Black
        Me.btnBranch.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnBranch.GradientForeColor = System.Drawing.Color.Black
        Me.btnBranch.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBranch.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnBranch.Location = New System.Drawing.Point(3, 3)
        Me.btnBranch.Name = "btnBranch"
        Me.btnBranch.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBranch.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnBranch.Size = New System.Drawing.Size(192, 38)
        Me.btnBranch.TabIndex = 0
        Me.btnBranch.Text = "Branch"
        Me.btnBranch.UseVisualStyleBackColor = True
        '
        'btnDeptGrp
        '
        Me.btnDeptGrp.BackColor = System.Drawing.Color.White
        Me.btnDeptGrp.BackgroundImage = CType(resources.GetObject("btnDeptGrp.BackgroundImage"), System.Drawing.Image)
        Me.btnDeptGrp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeptGrp.BorderColor = System.Drawing.Color.Empty
        Me.btnDeptGrp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnDeptGrp.FlatAppearance.BorderSize = 0
        Me.btnDeptGrp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeptGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeptGrp.ForeColor = System.Drawing.Color.Black
        Me.btnDeptGrp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeptGrp.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeptGrp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeptGrp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeptGrp.Location = New System.Drawing.Point(3, 47)
        Me.btnDeptGrp.Name = "btnDeptGrp"
        Me.btnDeptGrp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeptGrp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeptGrp.Size = New System.Drawing.Size(192, 38)
        Me.btnDeptGrp.TabIndex = 1
        Me.btnDeptGrp.Text = "Department Group"
        Me.btnDeptGrp.UseVisualStyleBackColor = True
        '
        'btnDept
        '
        Me.btnDept.BackColor = System.Drawing.Color.White
        Me.btnDept.BackgroundImage = CType(resources.GetObject("btnDept.BackgroundImage"), System.Drawing.Image)
        Me.btnDept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDept.BorderColor = System.Drawing.Color.Empty
        Me.btnDept.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnDept.FlatAppearance.BorderSize = 0
        Me.btnDept.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDept.ForeColor = System.Drawing.Color.Black
        Me.btnDept.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDept.GradientForeColor = System.Drawing.Color.Black
        Me.btnDept.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDept.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDept.Location = New System.Drawing.Point(3, 91)
        Me.btnDept.Name = "btnDept"
        Me.btnDept.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDept.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDept.Size = New System.Drawing.Size(192, 38)
        Me.btnDept.TabIndex = 2
        Me.btnDept.Text = "Department"
        Me.btnDept.UseVisualStyleBackColor = True
        '
        'btnSectionGroup
        '
        Me.btnSectionGroup.BackColor = System.Drawing.Color.White
        Me.btnSectionGroup.BackgroundImage = CType(resources.GetObject("btnSectionGroup.BackgroundImage"), System.Drawing.Image)
        Me.btnSectionGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSectionGroup.BorderColor = System.Drawing.Color.Empty
        Me.btnSectionGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnSectionGroup.FlatAppearance.BorderSize = 0
        Me.btnSectionGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSectionGroup.ForeColor = System.Drawing.Color.Black
        Me.btnSectionGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSectionGroup.GradientForeColor = System.Drawing.Color.Black
        Me.btnSectionGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSectionGroup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSectionGroup.Location = New System.Drawing.Point(3, 135)
        Me.btnSectionGroup.Name = "btnSectionGroup"
        Me.btnSectionGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSectionGroup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSectionGroup.Size = New System.Drawing.Size(192, 38)
        Me.btnSectionGroup.TabIndex = 6
        Me.btnSectionGroup.Text = "Section Group"
        Me.btnSectionGroup.UseVisualStyleBackColor = True
        '
        'btnSection
        '
        Me.btnSection.BackColor = System.Drawing.Color.White
        Me.btnSection.BackgroundImage = CType(resources.GetObject("btnSection.BackgroundImage"), System.Drawing.Image)
        Me.btnSection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSection.BorderColor = System.Drawing.Color.Empty
        Me.btnSection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnSection.FlatAppearance.BorderSize = 0
        Me.btnSection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSection.ForeColor = System.Drawing.Color.Black
        Me.btnSection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSection.Location = New System.Drawing.Point(3, 179)
        Me.btnSection.Name = "btnSection"
        Me.btnSection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSection.Size = New System.Drawing.Size(192, 38)
        Me.btnSection.TabIndex = 3
        Me.btnSection.Text = "Section"
        Me.btnSection.UseVisualStyleBackColor = True
        '
        'btnUnitGroup
        '
        Me.btnUnitGroup.BackColor = System.Drawing.Color.White
        Me.btnUnitGroup.BackgroundImage = CType(resources.GetObject("btnUnitGroup.BackgroundImage"), System.Drawing.Image)
        Me.btnUnitGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnitGroup.BorderColor = System.Drawing.Color.Empty
        Me.btnUnitGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnUnitGroup.FlatAppearance.BorderSize = 0
        Me.btnUnitGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnitGroup.ForeColor = System.Drawing.Color.Black
        Me.btnUnitGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnitGroup.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnitGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnitGroup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnitGroup.Location = New System.Drawing.Point(3, 223)
        Me.btnUnitGroup.Name = "btnUnitGroup"
        Me.btnUnitGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnitGroup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnitGroup.Size = New System.Drawing.Size(192, 38)
        Me.btnUnitGroup.TabIndex = 7
        Me.btnUnitGroup.Text = "Unit Group"
        Me.btnUnitGroup.UseVisualStyleBackColor = True
        '
        'btnUnit
        '
        Me.btnUnit.BackColor = System.Drawing.Color.White
        Me.btnUnit.BackgroundImage = CType(resources.GetObject("btnUnit.BackgroundImage"), System.Drawing.Image)
        Me.btnUnit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnit.BorderColor = System.Drawing.Color.Empty
        Me.btnUnit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnUnit.FlatAppearance.BorderSize = 0
        Me.btnUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnit.ForeColor = System.Drawing.Color.Black
        Me.btnUnit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnit.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnit.Location = New System.Drawing.Point(3, 267)
        Me.btnUnit.Name = "btnUnit"
        Me.btnUnit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnit.Size = New System.Drawing.Size(192, 38)
        Me.btnUnit.TabIndex = 4
        Me.btnUnit.Text = "Unit"
        Me.btnUnit.UseVisualStyleBackColor = True
        '
        'btnTeam
        '
        Me.btnTeam.BackColor = System.Drawing.Color.White
        Me.btnTeam.BackgroundImage = CType(resources.GetObject("btnTeam.BackgroundImage"), System.Drawing.Image)
        Me.btnTeam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTeam.BorderColor = System.Drawing.Color.Empty
        Me.btnTeam.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnTeam.FlatAppearance.BorderSize = 0
        Me.btnTeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTeam.ForeColor = System.Drawing.Color.Black
        Me.btnTeam.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTeam.GradientForeColor = System.Drawing.Color.Black
        Me.btnTeam.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTeam.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTeam.Location = New System.Drawing.Point(3, 311)
        Me.btnTeam.Name = "btnTeam"
        Me.btnTeam.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTeam.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTeam.Size = New System.Drawing.Size(192, 38)
        Me.btnTeam.TabIndex = 8
        Me.btnTeam.Text = "Team"
        Me.btnTeam.UseVisualStyleBackColor = True
        '
        'btnJobGroup
        '
        Me.btnJobGroup.BackColor = System.Drawing.Color.White
        Me.btnJobGroup.BackgroundImage = CType(resources.GetObject("btnJobGroup.BackgroundImage"), System.Drawing.Image)
        Me.btnJobGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnJobGroup.BorderColor = System.Drawing.Color.Empty
        Me.btnJobGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnJobGroup.FlatAppearance.BorderSize = 0
        Me.btnJobGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJobGroup.ForeColor = System.Drawing.Color.Black
        Me.btnJobGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnJobGroup.GradientForeColor = System.Drawing.Color.Black
        Me.btnJobGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJobGroup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnJobGroup.Location = New System.Drawing.Point(3, 355)
        Me.btnJobGroup.Name = "btnJobGroup"
        Me.btnJobGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJobGroup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnJobGroup.Size = New System.Drawing.Size(192, 38)
        Me.btnJobGroup.TabIndex = 11
        Me.btnJobGroup.Text = "Job Group"
        Me.btnJobGroup.UseVisualStyleBackColor = True
        '
        'btnJob
        '
        Me.btnJob.BackColor = System.Drawing.Color.White
        Me.btnJob.BackgroundImage = CType(resources.GetObject("btnJob.BackgroundImage"), System.Drawing.Image)
        Me.btnJob.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnJob.BorderColor = System.Drawing.Color.Empty
        Me.btnJob.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnJob.FlatAppearance.BorderSize = 0
        Me.btnJob.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJob.ForeColor = System.Drawing.Color.Black
        Me.btnJob.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnJob.GradientForeColor = System.Drawing.Color.Black
        Me.btnJob.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJob.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnJob.Location = New System.Drawing.Point(3, 399)
        Me.btnJob.Name = "btnJob"
        Me.btnJob.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJob.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnJob.Size = New System.Drawing.Size(192, 38)
        Me.btnJob.TabIndex = 5
        Me.btnJob.Text = "Job"
        Me.btnJob.UseVisualStyleBackColor = True
        '
        'btnClassGroup
        '
        Me.btnClassGroup.BackColor = System.Drawing.Color.White
        Me.btnClassGroup.BackgroundImage = CType(resources.GetObject("btnClassGroup.BackgroundImage"), System.Drawing.Image)
        Me.btnClassGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClassGroup.BorderColor = System.Drawing.Color.Empty
        Me.btnClassGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnClassGroup.FlatAppearance.BorderSize = 0
        Me.btnClassGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClassGroup.ForeColor = System.Drawing.Color.Black
        Me.btnClassGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClassGroup.GradientForeColor = System.Drawing.Color.Black
        Me.btnClassGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClassGroup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClassGroup.Location = New System.Drawing.Point(3, 443)
        Me.btnClassGroup.Name = "btnClassGroup"
        Me.btnClassGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClassGroup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClassGroup.Size = New System.Drawing.Size(192, 38)
        Me.btnClassGroup.TabIndex = 9
        Me.btnClassGroup.Text = "Class Group"
        Me.btnClassGroup.UseVisualStyleBackColor = True
        '
        'btnClass
        '
        Me.btnClass.BackColor = System.Drawing.Color.White
        Me.btnClass.BackgroundImage = CType(resources.GetObject("btnClass.BackgroundImage"), System.Drawing.Image)
        Me.btnClass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClass.BorderColor = System.Drawing.Color.Empty
        Me.btnClass.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnClass.FlatAppearance.BorderSize = 0
        Me.btnClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClass.ForeColor = System.Drawing.Color.Black
        Me.btnClass.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClass.GradientForeColor = System.Drawing.Color.Black
        Me.btnClass.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClass.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClass.Location = New System.Drawing.Point(3, 487)
        Me.btnClass.Name = "btnClass"
        Me.btnClass.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClass.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClass.Size = New System.Drawing.Size(192, 38)
        Me.btnClass.TabIndex = 10
        Me.btnClass.Text = "Class"
        Me.btnClass.UseVisualStyleBackColor = True
        '
        'btnGradeGroup
        '
        Me.btnGradeGroup.BackColor = System.Drawing.Color.White
        Me.btnGradeGroup.BackgroundImage = CType(resources.GetObject("btnGradeGroup.BackgroundImage"), System.Drawing.Image)
        Me.btnGradeGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGradeGroup.BorderColor = System.Drawing.Color.Empty
        Me.btnGradeGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnGradeGroup.FlatAppearance.BorderSize = 0
        Me.btnGradeGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGradeGroup.ForeColor = System.Drawing.Color.Black
        Me.btnGradeGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGradeGroup.GradientForeColor = System.Drawing.Color.Black
        Me.btnGradeGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGradeGroup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGradeGroup.Location = New System.Drawing.Point(3, 531)
        Me.btnGradeGroup.Name = "btnGradeGroup"
        Me.btnGradeGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGradeGroup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGradeGroup.Size = New System.Drawing.Size(192, 38)
        Me.btnGradeGroup.TabIndex = 12
        Me.btnGradeGroup.Text = "Grade Group"
        Me.btnGradeGroup.UseVisualStyleBackColor = True
        '
        'btnGrade
        '
        Me.btnGrade.BackColor = System.Drawing.Color.White
        Me.btnGrade.BackgroundImage = CType(resources.GetObject("btnGrade.BackgroundImage"), System.Drawing.Image)
        Me.btnGrade.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGrade.BorderColor = System.Drawing.Color.Empty
        Me.btnGrade.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnGrade.FlatAppearance.BorderSize = 0
        Me.btnGrade.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrade.ForeColor = System.Drawing.Color.Black
        Me.btnGrade.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGrade.GradientForeColor = System.Drawing.Color.Black
        Me.btnGrade.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGrade.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGrade.Location = New System.Drawing.Point(3, 575)
        Me.btnGrade.Name = "btnGrade"
        Me.btnGrade.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGrade.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGrade.Size = New System.Drawing.Size(192, 38)
        Me.btnGrade.TabIndex = 13
        Me.btnGrade.Text = "Grade"
        Me.btnGrade.UseVisualStyleBackColor = True
        '
        'btnGradeLevel
        '
        Me.btnGradeLevel.BackColor = System.Drawing.Color.White
        Me.btnGradeLevel.BackgroundImage = CType(resources.GetObject("btnGradeLevel.BackgroundImage"), System.Drawing.Image)
        Me.btnGradeLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGradeLevel.BorderColor = System.Drawing.Color.Empty
        Me.btnGradeLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnGradeLevel.FlatAppearance.BorderSize = 0
        Me.btnGradeLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGradeLevel.ForeColor = System.Drawing.Color.Black
        Me.btnGradeLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGradeLevel.GradientForeColor = System.Drawing.Color.Black
        Me.btnGradeLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGradeLevel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGradeLevel.Location = New System.Drawing.Point(3, 619)
        Me.btnGradeLevel.Name = "btnGradeLevel"
        Me.btnGradeLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGradeLevel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGradeLevel.Size = New System.Drawing.Size(192, 38)
        Me.btnGradeLevel.TabIndex = 14
        Me.btnGradeLevel.Text = "Grade Level"
        Me.btnGradeLevel.UseVisualStyleBackColor = True
        '
        'btnCostCenterGroup
        '
        Me.btnCostCenterGroup.BackColor = System.Drawing.Color.White
        Me.btnCostCenterGroup.BackgroundImage = CType(resources.GetObject("btnCostCenterGroup.BackgroundImage"), System.Drawing.Image)
        Me.btnCostCenterGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCostCenterGroup.BorderColor = System.Drawing.Color.Empty
        Me.btnCostCenterGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnCostCenterGroup.FlatAppearance.BorderSize = 0
        Me.btnCostCenterGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCostCenterGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCostCenterGroup.ForeColor = System.Drawing.Color.Black
        Me.btnCostCenterGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCostCenterGroup.GradientForeColor = System.Drawing.Color.Black
        Me.btnCostCenterGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCostCenterGroup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCostCenterGroup.Location = New System.Drawing.Point(3, 663)
        Me.btnCostCenterGroup.Name = "btnCostCenterGroup"
        Me.btnCostCenterGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCostCenterGroup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCostCenterGroup.Size = New System.Drawing.Size(192, 38)
        Me.btnCostCenterGroup.TabIndex = 15
        Me.btnCostCenterGroup.Text = "Cost Center Group"
        Me.btnCostCenterGroup.UseVisualStyleBackColor = True
        '
        'btnCostCenter
        '
        Me.btnCostCenter.BackColor = System.Drawing.Color.White
        Me.btnCostCenter.BackgroundImage = CType(resources.GetObject("btnCostCenter.BackgroundImage"), System.Drawing.Image)
        Me.btnCostCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCostCenter.BorderColor = System.Drawing.Color.Empty
        Me.btnCostCenter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnCostCenter.FlatAppearance.BorderSize = 0
        Me.btnCostCenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCostCenter.ForeColor = System.Drawing.Color.Black
        Me.btnCostCenter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCostCenter.GradientForeColor = System.Drawing.Color.Black
        Me.btnCostCenter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCostCenter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCostCenter.Location = New System.Drawing.Point(3, 707)
        Me.btnCostCenter.Name = "btnCostCenter"
        Me.btnCostCenter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCostCenter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCostCenter.Size = New System.Drawing.Size(192, 38)
        Me.btnCostCenter.TabIndex = 16
        Me.btnCostCenter.Text = "Cost Center"
        Me.btnCostCenter.UseVisualStyleBackColor = True
        '
        'btnEmployementType
        '
        Me.btnEmployementType.BackColor = System.Drawing.Color.White
        Me.btnEmployementType.BackgroundImage = CType(resources.GetObject("btnEmployementType.BackgroundImage"), System.Drawing.Image)
        Me.btnEmployementType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmployementType.BorderColor = System.Drawing.Color.Empty
        Me.btnEmployementType.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnEmployementType.FlatAppearance.BorderSize = 0
        Me.btnEmployementType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmployementType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmployementType.ForeColor = System.Drawing.Color.Black
        Me.btnEmployementType.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmployementType.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmployementType.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployementType.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployementType.Location = New System.Drawing.Point(3, 751)
        Me.btnEmployementType.Name = "btnEmployementType"
        Me.btnEmployementType.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployementType.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployementType.Size = New System.Drawing.Size(192, 38)
        Me.btnEmployementType.TabIndex = 17
        Me.btnEmployementType.Text = "Employment Type"
        Me.btnEmployementType.UseVisualStyleBackColor = True
        '
        'btnReligion
        '
        Me.btnReligion.BackColor = System.Drawing.Color.White
        Me.btnReligion.BackgroundImage = CType(resources.GetObject("btnReligion.BackgroundImage"), System.Drawing.Image)
        Me.btnReligion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReligion.BorderColor = System.Drawing.Color.Empty
        Me.btnReligion.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnReligion.FlatAppearance.BorderSize = 0
        Me.btnReligion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReligion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReligion.ForeColor = System.Drawing.Color.Black
        Me.btnReligion.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReligion.GradientForeColor = System.Drawing.Color.Black
        Me.btnReligion.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReligion.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReligion.Location = New System.Drawing.Point(3, 795)
        Me.btnReligion.Name = "btnReligion"
        Me.btnReligion.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReligion.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReligion.Size = New System.Drawing.Size(192, 38)
        Me.btnReligion.TabIndex = 18
        Me.btnReligion.Text = "Religion"
        Me.btnReligion.UseVisualStyleBackColor = True
        '
        'btnGender
        '
        Me.btnGender.BackColor = System.Drawing.Color.White
        Me.btnGender.BackgroundImage = CType(resources.GetObject("btnGender.BackgroundImage"), System.Drawing.Image)
        Me.btnGender.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGender.BorderColor = System.Drawing.Color.Empty
        Me.btnGender.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnGender.FlatAppearance.BorderSize = 0
        Me.btnGender.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGender.ForeColor = System.Drawing.Color.Black
        Me.btnGender.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGender.GradientForeColor = System.Drawing.Color.Black
        Me.btnGender.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGender.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGender.Location = New System.Drawing.Point(3, 839)
        Me.btnGender.Name = "btnGender"
        Me.btnGender.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGender.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGender.Size = New System.Drawing.Size(192, 38)
        Me.btnGender.TabIndex = 19
        Me.btnGender.Text = "Gender"
        Me.btnGender.UseVisualStyleBackColor = True
        '
        'btnRelationFromDpndt
        '
        Me.btnRelationFromDpndt.BackColor = System.Drawing.Color.White
        Me.btnRelationFromDpndt.BackgroundImage = CType(resources.GetObject("btnRelationFromDpndt.BackgroundImage"), System.Drawing.Image)
        Me.btnRelationFromDpndt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRelationFromDpndt.BorderColor = System.Drawing.Color.Empty
        Me.btnRelationFromDpndt.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnRelationFromDpndt.FlatAppearance.BorderSize = 0
        Me.btnRelationFromDpndt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRelationFromDpndt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRelationFromDpndt.ForeColor = System.Drawing.Color.Black
        Me.btnRelationFromDpndt.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRelationFromDpndt.GradientForeColor = System.Drawing.Color.Black
        Me.btnRelationFromDpndt.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRelationFromDpndt.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRelationFromDpndt.Location = New System.Drawing.Point(3, 883)
        Me.btnRelationFromDpndt.Name = "btnRelationFromDpndt"
        Me.btnRelationFromDpndt.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRelationFromDpndt.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRelationFromDpndt.Size = New System.Drawing.Size(192, 38)
        Me.btnRelationFromDpndt.TabIndex = 24
        Me.btnRelationFromDpndt.Text = "Relation from Dependents"
        Me.btnRelationFromDpndt.UseVisualStyleBackColor = True
        '
        'btnNationality
        '
        Me.btnNationality.BackColor = System.Drawing.Color.White
        Me.btnNationality.BackgroundImage = CType(resources.GetObject("btnNationality.BackgroundImage"), System.Drawing.Image)
        Me.btnNationality.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNationality.BorderColor = System.Drawing.Color.Empty
        Me.btnNationality.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnNationality.FlatAppearance.BorderSize = 0
        Me.btnNationality.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNationality.ForeColor = System.Drawing.Color.Black
        Me.btnNationality.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNationality.GradientForeColor = System.Drawing.Color.Black
        Me.btnNationality.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNationality.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNationality.Location = New System.Drawing.Point(3, 927)
        Me.btnNationality.Name = "btnNationality"
        Me.btnNationality.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNationality.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNationality.Size = New System.Drawing.Size(192, 38)
        Me.btnNationality.TabIndex = 20
        Me.btnNationality.Text = "Nationality"
        Me.btnNationality.UseVisualStyleBackColor = True
        '
        'btnPayType
        '
        Me.btnPayType.BackColor = System.Drawing.Color.White
        Me.btnPayType.BackgroundImage = CType(resources.GetObject("btnPayType.BackgroundImage"), System.Drawing.Image)
        Me.btnPayType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPayType.BorderColor = System.Drawing.Color.Empty
        Me.btnPayType.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnPayType.FlatAppearance.BorderSize = 0
        Me.btnPayType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayType.ForeColor = System.Drawing.Color.Black
        Me.btnPayType.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPayType.GradientForeColor = System.Drawing.Color.Black
        Me.btnPayType.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPayType.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPayType.Location = New System.Drawing.Point(3, 971)
        Me.btnPayType.Name = "btnPayType"
        Me.btnPayType.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPayType.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPayType.Size = New System.Drawing.Size(192, 38)
        Me.btnPayType.TabIndex = 21
        Me.btnPayType.Text = "Pay Type"
        Me.btnPayType.UseVisualStyleBackColor = True
        '
        'btnPayPoint
        '
        Me.btnPayPoint.BackColor = System.Drawing.Color.White
        Me.btnPayPoint.BackgroundImage = CType(resources.GetObject("btnPayPoint.BackgroundImage"), System.Drawing.Image)
        Me.btnPayPoint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPayPoint.BorderColor = System.Drawing.Color.Empty
        Me.btnPayPoint.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnPayPoint.FlatAppearance.BorderSize = 0
        Me.btnPayPoint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayPoint.ForeColor = System.Drawing.Color.Black
        Me.btnPayPoint.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPayPoint.GradientForeColor = System.Drawing.Color.Black
        Me.btnPayPoint.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPayPoint.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPayPoint.Location = New System.Drawing.Point(3, 1015)
        Me.btnPayPoint.Name = "btnPayPoint"
        Me.btnPayPoint.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPayPoint.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPayPoint.Size = New System.Drawing.Size(192, 38)
        Me.btnPayPoint.TabIndex = 22
        Me.btnPayPoint.Text = "Pay Point"
        Me.btnPayPoint.UseVisualStyleBackColor = True
        '
        'btnMaritalStatus
        '
        Me.btnMaritalStatus.BackColor = System.Drawing.Color.White
        Me.btnMaritalStatus.BackgroundImage = CType(resources.GetObject("btnMaritalStatus.BackgroundImage"), System.Drawing.Image)
        Me.btnMaritalStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMaritalStatus.BorderColor = System.Drawing.Color.Empty
        Me.btnMaritalStatus.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnMaritalStatus.FlatAppearance.BorderSize = 0
        Me.btnMaritalStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMaritalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMaritalStatus.ForeColor = System.Drawing.Color.Black
        Me.btnMaritalStatus.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMaritalStatus.GradientForeColor = System.Drawing.Color.Black
        Me.btnMaritalStatus.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMaritalStatus.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMaritalStatus.Location = New System.Drawing.Point(3, 1059)
        Me.btnMaritalStatus.Name = "btnMaritalStatus"
        Me.btnMaritalStatus.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMaritalStatus.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMaritalStatus.Size = New System.Drawing.Size(192, 38)
        Me.btnMaritalStatus.TabIndex = 23
        Me.btnMaritalStatus.Text = "Marital Status"
        Me.btnMaritalStatus.UseVisualStyleBackColor = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnApply)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 405)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(831, 50)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(730, 8)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(98, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.BackColor = System.Drawing.Color.White
        Me.btnApply.BackgroundImage = CType(resources.GetObject("btnApply.BackgroundImage"), System.Drawing.Image)
        Me.btnApply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApply.BorderColor = System.Drawing.Color.Empty
        Me.btnApply.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApply.FlatAppearance.BorderSize = 0
        Me.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApply.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApply.ForeColor = System.Drawing.Color.Black
        Me.btnApply.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApply.GradientForeColor = System.Drawing.Color.Black
        Me.btnApply.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Location = New System.Drawing.Point(626, 8)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Size = New System.Drawing.Size(98, 30)
        Me.btnApply.TabIndex = 2
        Me.btnApply.Text = "&Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'btnSkill
        '
        Me.btnSkill.BackColor = System.Drawing.Color.White
        Me.btnSkill.BackgroundImage = CType(resources.GetObject("btnSkill.BackgroundImage"), System.Drawing.Image)
        Me.btnSkill.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSkill.BorderColor = System.Drawing.Color.Empty
        Me.btnSkill.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnSkill.FlatAppearance.BorderSize = 0
        Me.btnSkill.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSkill.ForeColor = System.Drawing.Color.Black
        Me.btnSkill.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSkill.GradientForeColor = System.Drawing.Color.Black
        Me.btnSkill.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSkill.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSkill.Location = New System.Drawing.Point(3, 1103)
        Me.btnSkill.Name = "btnSkill"
        Me.btnSkill.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSkill.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSkill.Size = New System.Drawing.Size(192, 38)
        Me.btnSkill.TabIndex = 25
        Me.btnSkill.Text = "Skill"
        Me.btnSkill.UseVisualStyleBackColor = True
        '
        'frmAdvanceSearch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(831, 455)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdvanceSearch"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Advance Search"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.gvFilterInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpAllocations.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnApply As eZee.Common.eZeeLightButton
    Friend WithEvents flpAllocations As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnBranch As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeptGrp As eZee.Common.eZeeLightButton
    Friend WithEvents btnDept As eZee.Common.eZeeLightButton
    Friend WithEvents btnSection As eZee.Common.eZeeLightButton
    Friend WithEvents btnUnit As eZee.Common.eZeeLightButton
    Friend WithEvents btnJob As eZee.Common.eZeeLightButton
    Friend WithEvents btnSectionGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnUnitGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnTeam As eZee.Common.eZeeLightButton
    Friend WithEvents btnClass As eZee.Common.eZeeLightButton
    Friend WithEvents btnClassGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnJobGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnGradeGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnGrade As eZee.Common.eZeeLightButton
    Friend WithEvents btnGradeLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnCostCenterGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnCostCenter As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployementType As eZee.Common.eZeeLightButton
    Friend WithEvents btnReligion As eZee.Common.eZeeLightButton
    Friend WithEvents btnGender As eZee.Common.eZeeLightButton
    Friend WithEvents btnNationality As eZee.Common.eZeeLightButton
    Friend WithEvents btnPayType As eZee.Common.eZeeLightButton
    Friend WithEvents btnPayPoint As eZee.Common.eZeeLightButton
    Friend WithEvents btnMaritalStatus As eZee.Common.eZeeLightButton
    Friend WithEvents btnRelationFromDpndt As eZee.Common.eZeeLightButton
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgDetail As System.Windows.Forms.DataGridView
    Private WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvFilterInfo As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhFilterID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgColhBlank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFilterName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFilterGroupName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFilterIsGrp As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhFilterGroupId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSkill As eZee.Common.eZeeLightButton
End Class
