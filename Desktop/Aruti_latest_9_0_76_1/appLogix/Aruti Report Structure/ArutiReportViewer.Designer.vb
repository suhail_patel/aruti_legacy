﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ArutiReportViewer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crwArutiReportViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'crwArutiReportViewer
        '
        Me.crwArutiReportViewer.ActiveViewIndex = -1
        Me.crwArutiReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crwArutiReportViewer.DisplayGroupTree = False
        Me.crwArutiReportViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crwArutiReportViewer.Location = New System.Drawing.Point(0, 0)
        Me.crwArutiReportViewer.Name = "crwArutiReportViewer"
        Me.crwArutiReportViewer.SelectionFormula = ""
        Me.crwArutiReportViewer.Size = New System.Drawing.Size(424, 266)
        Me.crwArutiReportViewer.TabIndex = 0
        Me.crwArutiReportViewer.ViewTimeSelectionFormula = ""
        '
        'ArutiReportViewer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(424, 266)
        Me.Controls.Add(Me.crwArutiReportViewer)
        Me.Name = "ArutiReportViewer"
        Me.Text = "ArutiReportViewer"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub

    'Sandeep [ 07 FEB 2011 ] -- START
    'Friend WithEvents crwArutiReportViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Public WithEvents crwArutiReportViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    'Sandeep [ 07 FEB 2011 ] -- END 

End Class
