﻿Imports eZeeCommonLib
Public Class frmBaseReportForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub


    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents objeZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objbtnReset As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnExport As eZee.Common.eZeeSplitButton
    Friend WithEvents objbtnReport As eZee.Common.eZeeSplitButton
    Friend WithEvents objbtnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objmnuReportPrint As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents objmnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objmnuPrintPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objsep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objmnuSetDefaultPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objmnuReportExport As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents objmnuExportToRichText As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objmnuExportToWord As System.Windows.Forms.ToolStripMenuItem

    Friend WithEvents objmnuExportToExcel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objmnuExportToPDF As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objmnuExportToHTML As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objsep0 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objmnuSetDefaultExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents objmnuExportToExcelExtra As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents objmnuExportToExcelDataOnly As System.Windows.Forms.ToolStripMenuItem

    Protected Friend WithEvents NavPanel As eZee.Common.eZeeFooter

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBaseReportForm))
        Me.NavPanel = New eZee.Common.eZeeFooter
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.objLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnReport = New eZee.Common.eZeeSplitButton
        Me.objmnuReportPrint = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.objmnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuPrintPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.objsep1 = New System.Windows.Forms.ToolStripSeparator
        Me.objmnuSetDefaultPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.objbtnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnExport = New eZee.Common.eZeeSplitButton
        Me.objmnuReportExport = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.objmnuExportToRichText = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuExportToWord = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuExportToExcel = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuExportToPDF = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuExportToHTML = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuExportToExcelExtra = New System.Windows.Forms.ToolStripMenuItem
        Me.objsep0 = New System.Windows.Forms.ToolStripSeparator
        Me.objmnuSetDefaultExport = New System.Windows.Forms.ToolStripMenuItem
        Me.objeZeeHeader = New eZee.Common.eZeeHeader
        Me.objmnuExportToExcelDataOnly = New System.Windows.Forms.ToolStripMenuItem
        Me.NavPanel.SuspendLayout()
        Me.objmnuReportPrint.SuspendLayout()
        Me.objmnuReportExport.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.BorderColor = System.Drawing.Color.Silver
        Me.NavPanel.Controls.Add(Me.objbtnAdvanceFilter)
        Me.NavPanel.Controls.Add(Me.objLanguage)
        Me.NavPanel.Controls.Add(Me.objbtnReset)
        Me.NavPanel.Controls.Add(Me.objbtnReport)
        Me.NavPanel.Controls.Add(Me.objbtnCancel)
        Me.NavPanel.Controls.Add(Me.objbtnExport)
        Me.NavPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.NavPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NavPanel.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.NavPanel.GradientColor1 = System.Drawing.Color.Empty
        Me.NavPanel.GradientColor2 = System.Drawing.Color.Empty
        Me.NavPanel.Location = New System.Drawing.Point(0, 397)
        Me.NavPanel.Name = "NavPanel"
        Me.NavPanel.Size = New System.Drawing.Size(655, 55)
        Me.NavPanel.TabIndex = 1
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(163, 12)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 5
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        '
        'objLanguage
        '
        Me.objLanguage.BackColor = System.Drawing.Color.White
        Me.objLanguage.BackgroundImage = CType(resources.GetObject("objLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objLanguage.FlatAppearance.BorderSize = 0
        Me.objLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLanguage.ForeColor = System.Drawing.Color.Black
        Me.objLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objLanguage.Image = Global.Aruti.Data.My.Resources.Resources.FONT_TXT
        Me.objLanguage.Location = New System.Drawing.Point(12, 12)
        Me.objLanguage.Name = "objLanguage"
        Me.objLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objLanguage.TabIndex = 4
        Me.objLanguage.UseVisualStyleBackColor = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.White
        Me.objbtnReset.BackgroundImage = CType(resources.GetObject("objbtnReset.BackgroundImage"), System.Drawing.Image)
        Me.objbtnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnReset.BorderColor = System.Drawing.Color.Empty
        Me.objbtnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnReset.FlatAppearance.BorderSize = 0
        Me.objbtnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnReset.ForeColor = System.Drawing.Color.Black
        Me.objbtnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnReset.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnReset.Location = New System.Drawing.Point(266, 12)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnReset.Size = New System.Drawing.Size(90, 30)
        Me.objbtnReset.TabIndex = 1
        Me.objbtnReset.Text = "Reset"
        Me.objbtnReset.UseVisualStyleBackColor = False
        '
        'objbtnReport
        '
        Me.objbtnReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReport.BorderColor = System.Drawing.Color.Black
        Me.objbtnReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnReport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnReport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.objbtnReport.Location = New System.Drawing.Point(458, 12)
        Me.objbtnReport.Name = "objbtnReport"
        Me.objbtnReport.ShowDefaultBorderColor = True
        Me.objbtnReport.Size = New System.Drawing.Size(90, 30)
        Me.objbtnReport.SplitButtonMenu = Me.objmnuReportPrint
        Me.objbtnReport.TabIndex = 0
        Me.objbtnReport.Text = "&Report"
        '
        'objmnuReportPrint
        '
        Me.objmnuReportPrint.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objmnuPrint, Me.objmnuPrintPreview, Me.objsep1, Me.objmnuSetDefaultPrint})
        Me.objmnuReportPrint.Name = "ContextMenuStrip1"
        Me.objmnuReportPrint.Size = New System.Drawing.Size(144, 76)
        '
        'objmnuPrint
        '
        Me.objmnuPrint.Image = Global.Aruti.Data.My.Resources.Resources.PrintSummary_16
        Me.objmnuPrint.Name = "objmnuPrint"
        Me.objmnuPrint.Size = New System.Drawing.Size(143, 22)
        Me.objmnuPrint.Text = "Print"
        '
        'objmnuPrintPreview
        '
        Me.objmnuPrintPreview.Image = Global.Aruti.Data.My.Resources.Resources.RichText_16
        Me.objmnuPrintPreview.Name = "objmnuPrintPreview"
        Me.objmnuPrintPreview.Size = New System.Drawing.Size(143, 22)
        Me.objmnuPrintPreview.Text = "Print Preview"
        '
        'objsep1
        '
        Me.objsep1.Name = "objsep1"
        Me.objsep1.Size = New System.Drawing.Size(140, 6)
        '
        'objmnuSetDefaultPrint
        '
        Me.objmnuSetDefaultPrint.Image = Global.Aruti.Data.My.Resources.Resources.Check_16
        Me.objmnuSetDefaultPrint.Name = "objmnuSetDefaultPrint"
        Me.objmnuSetDefaultPrint.Size = New System.Drawing.Size(143, 22)
        Me.objmnuSetDefaultPrint.Text = "Set Default"
        '
        'objbtnCancel
        '
        Me.objbtnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnCancel.BackColor = System.Drawing.Color.White
        Me.objbtnCancel.BackgroundImage = CType(resources.GetObject("objbtnCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbtnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbtnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnCancel.FlatAppearance.BorderSize = 0
        Me.objbtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnCancel.ForeColor = System.Drawing.Color.Black
        Me.objbtnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnCancel.Location = New System.Drawing.Point(554, 12)
        Me.objbtnCancel.Name = "objbtnCancel"
        Me.objbtnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnCancel.Size = New System.Drawing.Size(90, 30)
        Me.objbtnCancel.TabIndex = 3
        Me.objbtnCancel.Text = "&Close"
        Me.objbtnCancel.UseVisualStyleBackColor = True
        '
        'objbtnExport
        '
        Me.objbtnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnExport.BorderColor = System.Drawing.Color.Black
        Me.objbtnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnExport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.objbtnExport.Location = New System.Drawing.Point(362, 12)
        Me.objbtnExport.Name = "objbtnExport"
        Me.objbtnExport.ShowDefaultBorderColor = True
        Me.objbtnExport.Size = New System.Drawing.Size(90, 30)
        Me.objbtnExport.SplitButtonMenu = Me.objmnuReportExport
        Me.objbtnExport.TabIndex = 2
        Me.objbtnExport.Text = "&Export"
        '
        'objmnuReportExport
        '
        Me.objmnuReportExport.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objmnuExportToRichText, Me.objmnuExportToWord, Me.objmnuExportToExcel, Me.objmnuExportToPDF, Me.objmnuExportToHTML, Me.objmnuExportToExcelExtra, Me.objmnuExportToExcelDataOnly, Me.objsep0, Me.objmnuSetDefaultExport})
        Me.objmnuReportExport.Name = "ContextMenuStrip1"
        Me.objmnuReportExport.Size = New System.Drawing.Size(156, 186)
        '
        'objmnuExportToRichText
        '
        Me.objmnuExportToRichText.Image = Global.Aruti.Data.My.Resources.Resources.RichText_161
        Me.objmnuExportToRichText.Name = "objmnuExportToRichText"
        Me.objmnuExportToRichText.Size = New System.Drawing.Size(155, 22)
        Me.objmnuExportToRichText.Text = "RichText "
        '
        'objmnuExportToWord
        '
        Me.objmnuExportToWord.Image = Global.Aruti.Data.My.Resources.Resources.Word_16
        Me.objmnuExportToWord.Name = "objmnuExportToWord"
        Me.objmnuExportToWord.Size = New System.Drawing.Size(155, 22)
        Me.objmnuExportToWord.Text = "Word "
        '
        'objmnuExportToExcel
        '
        Me.objmnuExportToExcel.Image = Global.Aruti.Data.My.Resources.Resources.Excel_16
        Me.objmnuExportToExcel.Name = "objmnuExportToExcel"
        Me.objmnuExportToExcel.Size = New System.Drawing.Size(155, 22)
        Me.objmnuExportToExcel.Text = "Excel "
        '
        'objmnuExportToPDF
        '
        Me.objmnuExportToPDF.Image = Global.Aruti.Data.My.Resources.Resources.PDF_16
        Me.objmnuExportToPDF.Name = "objmnuExportToPDF"
        Me.objmnuExportToPDF.Size = New System.Drawing.Size(155, 22)
        Me.objmnuExportToPDF.Text = "PDF"
        '
        'objmnuExportToHTML
        '
        Me.objmnuExportToHTML.Image = Global.Aruti.Data.My.Resources.Resources.HTML_16
        Me.objmnuExportToHTML.Name = "objmnuExportToHTML"
        Me.objmnuExportToHTML.Size = New System.Drawing.Size(155, 22)
        Me.objmnuExportToHTML.Text = "HTML "
        '
        'objmnuExportToExcelExtra
        '
        Me.objmnuExportToExcelExtra.Image = Global.Aruti.Data.My.Resources.Resources.Excel_16
        Me.objmnuExportToExcelExtra.Name = "objmnuExportToExcelExtra"
        Me.objmnuExportToExcelExtra.Size = New System.Drawing.Size(155, 22)
        Me.objmnuExportToExcelExtra.Text = "Excel Advance"
        '
        'objsep0
        '
        Me.objsep0.Name = "objsep0"
        Me.objsep0.Size = New System.Drawing.Size(152, 6)
        '
        'objmnuSetDefaultExport
        '
        Me.objmnuSetDefaultExport.Image = Global.Aruti.Data.My.Resources.Resources.Check_161
        Me.objmnuSetDefaultExport.Name = "objmnuSetDefaultExport"
        Me.objmnuSetDefaultExport.Size = New System.Drawing.Size(155, 22)
        Me.objmnuSetDefaultExport.Text = "Set Default"
        '
        'objeZeeHeader
        '
        Me.objeZeeHeader.BackColor = System.Drawing.Color.White
        Me.objeZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.objeZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.objeZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.objeZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objeZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.objeZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.objeZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objeZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.objeZeeHeader.Icon = Nothing
        Me.objeZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.objeZeeHeader.Message = ""
        Me.objeZeeHeader.Name = "objeZeeHeader"
        Me.objeZeeHeader.Size = New System.Drawing.Size(655, 60)
        Me.objeZeeHeader.TabIndex = 0
        Me.objeZeeHeader.TabStop = False
        Me.objeZeeHeader.Title = ""
        '
        'objmnuExportToExcelDataOnly
        '
        Me.objmnuExportToExcelDataOnly.Image = Global.Aruti.Data.My.Resources.Resources.Excel_16
        Me.objmnuExportToExcelDataOnly.Name = "objmnuExportToExcelDataOnly"
        Me.objmnuExportToExcelDataOnly.Size = New System.Drawing.Size(155, 22)
        Me.objmnuExportToExcelDataOnly.Text = "Excel Data Only"
        '
        'frmBaseReportForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(655, 452)
        Me.Controls.Add(Me.objeZeeHeader)
        Me.Controls.Add(Me.NavPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmBaseReportForm"
        Me.Text = "Main Parent Form"
        Me.NavPanel.ResumeLayout(False)
        Me.objmnuReportPrint.ResumeLayout(False)
        Me.objmnuReportExport.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim objAppSetting As New clsApplicationSettings
    Public Event Report_Click As PrintButtonEventHandler
    Public Event Export_Click As PrintButtonEventHandler
    Public Event Cancel_Click As EventHandler
    Public Event Reset_Click As EventHandler
    Public Event Language_Click As EventHandler
    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Event AdvanceFilter_Click As EventHandler
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    Public Delegate Sub PrintButtonEventHandler(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs)

#Region "Properties"
    Public Property _Message() As String
        Get
            Return objeZeeHeader.Message
        End Get
        Set(ByVal value As String)
            objeZeeHeader.Message = value
        End Set
    End Property

    Public Property _Title() As String
        Get
            Return objeZeeHeader.Title
        End Get
        Set(ByVal value As String)
            objeZeeHeader.Title = value
        End Set
    End Property

    'Sohail (08 Dec 2012) -- Start
    'TRA - ENHANCEMENT - To show / hide excelextra menu on export button
    Private mblnShowExcelExtraMenu As Boolean = False
    Public Property _Show_ExcelExtra_Menu() As Boolean
        Get
            Return mblnShowExcelExtraMenu
        End Get
        Set(ByVal value As Boolean)
            mblnShowExcelExtraMenu = value
        End Set
    End Property
    'Sohail (08 Dec 2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnShowAdvanceFilter As Boolean = False
    Public Property _Show_AdvanceFilter() As Boolean
        Get
            Return mblnShowAdvanceFilter
        End Get
        Set(ByVal value As Boolean)
            mblnShowAdvanceFilter = value
            'Pinkal (26-Nov-2018) -- Start
            'Enhancement - Working on Papaye Manual Attendance Register Report.
            objbtnAdvanceFilter.Visible = mblnShowAdvanceFilter
            'Pinkal (26-Nov-2018) -- End
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'Pinkal (29-May-2018) -- Start
    'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
    Private mblnShowExcelDataOnly As Boolean = False
    Public Property _Show_ExcelDataOnly_Menu() As Boolean
        Get
            Return mblnShowExcelDataOnly
        End Get
        Set(ByVal value As Boolean)
            mblnShowExcelDataOnly = value
        End Set
    End Property
    'Pinkal (29-May-2018) -- End


#End Region

    Private Sub frmBaseReportForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Sohail (08 Dec 2012) -- Start
        'TRA - ENHANCEMENT
        objmnuExportToExcelExtra.Visible = mblnShowExcelExtraMenu
        'Sohail (08 Dec 2012) -- End

        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objbtnAdvanceFilter.Visible = mblnShowAdvanceFilter
        'S.SANDEEP [ 13 FEB 2013 ] -- END


        'Pinkal (29-May-2018) -- Start
        'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
        objmnuExportToExcelDataOnly.Visible = mblnShowExcelDataOnly
        'Pinkal (29-May-2018) -- End

    End Sub

    Private Sub objbtnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReport.Click
        RaiseEvent Report_Click(sender, New Aruti.Data.PrintButtonEventArgs(objAppSetting._SetDefaultPrintAction))
    End Sub

    Private Sub objbtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnCancel.Click
        RaiseEvent Cancel_Click(sender, e)
        Me.Close()
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        RaiseEvent Reset_Click(sender, e)
    End Sub

    Private Sub objbtnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnExport.Click
        RaiseEvent Export_Click(sender, New Aruti.Data.PrintButtonEventArgs(objAppSetting._SetDefaultExportAction))
    End Sub

    Private Sub objLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLanguage.Click
        RaiseEvent Language_Click(sender, e)
    End Sub

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        RaiseEvent AdvanceFilter_Click(sender, e)
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#Region " Print Context Menu "

    Private Sub objmnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuPrint.Click
        RaiseEvent Report_Click(sender, New Aruti.Data.PrintButtonEventArgs(Aruti.Data.enPrintAction.Print))
    End Sub

    Private Sub objmnuPrintPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuPrintPreview.Click
        RaiseEvent Report_Click(sender, New Aruti.Data.PrintButtonEventArgs(Aruti.Data.enPrintAction.Preview))
    End Sub

    Private Sub objmnuSetDefaultPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuSetDefaultPrint.Click
        Dim objfrm As New Aruti.Data.frmSetDefaultView
        Try

            Call objfrm.displayDialog()

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objmnuSetDefaultPrint_Click", Me.Name)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Export Context Menu "
    Private Sub objmnuExportToRichText_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToRichText.Click
        RaiseEvent Export_Click(sender, New Aruti.Data.PrintButtonEventArgs(Aruti.Data.enExportAction.RichText))
    End Sub

    Private Sub objmnuExportToWord_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToWord.Click
        RaiseEvent Export_Click(sender, New Aruti.Data.PrintButtonEventArgs(Aruti.Data.enExportAction.Word))
    End Sub

    Private Sub objmnuExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToExcel.Click
        RaiseEvent Export_Click(sender, New Aruti.Data.PrintButtonEventArgs(Aruti.Data.enExportAction.Excel))
    End Sub

    Private Sub objmnuExportToPDF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToPDF.Click
        RaiseEvent Export_Click(sender, New Aruti.Data.PrintButtonEventArgs(Aruti.Data.enExportAction.PDF))
    End Sub

    Private Sub objmnuExportToHTML_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToHTML.Click
        RaiseEvent Export_Click(sender, New Aruti.Data.PrintButtonEventArgs(Aruti.Data.enExportAction.HTML))
    End Sub

    Private Sub objmnuSetDefaultExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuSetDefaultExport.Click
        Dim objfrm As New Aruti.Data.frmSetDefaultExport
        Try

            Call objfrm.displayDialog()

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objmnuSetDefaultExport_Click", Me.Name)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Sohail (08 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub objmnuExportToExcelExtra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objmnuExportToExcelExtra.Click
        RaiseEvent Export_Click(sender, New Aruti.Data.PrintButtonEventArgs(Aruti.Data.enExportAction.ExcelExtra))
    End Sub
    'Sohail (08 Dec 2012) -- End


    'Pinkal (29-May-2018) -- Start
    'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
    Private Sub objmnuExportToExcelDataOnly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToExcelDataOnly.Click
        RaiseEvent Export_Click(sender, New Aruti.Data.PrintButtonEventArgs(Aruti.Data.enExportAction.ExcelDataOnly))
    End Sub
    'Pinkal (29-May-2018) -- End


#End Region

End Class