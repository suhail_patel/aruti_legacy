﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmUserDef_ReportMode

#Region " Private Variables "

    Private mstrModuleName As String = "frmUserDef_ReportMode"
    Private mblnCancel As Boolean = True
    Private mintReportId As Integer = -1
    Private mintReportTypeId As Integer = -1
    Private mintModeId As Integer = -1
    Private mstrEarningTranHeadIds As String = String.Empty
    Private mstrDeductionTranHeadIds As String = String.Empty
    Private mstrNonEarningDeductionTranHeadIds As String = String.Empty 'Sohail (09 Mar 2012)
    Private objUserDefRMode As clsUserDef_ReportMode
    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrEmployerContributionHeadIds As String = String.Empty
    Private mstrInformationHeadIds As String = String.Empty
    'Sohail (09 Mar 2012) -- End
#End Region

#Region " Properties "

    Public Property _Earning_HeadIds() As String
        Get
            Return mstrEarningTranHeadIds
        End Get
        Set(ByVal value As String)
            mstrEarningTranHeadIds = value
        End Set
    End Property

    Public Property _Deduction_HeadIds() As String
        Get
            Return mstrDeductionTranHeadIds
        End Get
        Set(ByVal value As String)
            mstrDeductionTranHeadIds = value
        End Set
    End Property

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _NonEarningDeduction_HeadIds() As String
        Get
            Return mstrNonEarningDeductionTranHeadIds
        End Get
        Set(ByVal value As String)
            mstrNonEarningDeductionTranHeadIds = value
        End Set
    End Property

    Public Property _EmployerContribution_HeadIds() As String
        Get
            Return mstrEmployerContributionHeadIds
        End Get
        Set(ByVal value As String)
            mstrEmployerContributionHeadIds = value
        End Set
    End Property

    Public Property _Information_HeadIds() As String
        Get
            Return mstrInformationHeadIds
        End Get
        Set(ByVal value As String)
            mstrInformationHeadIds = value
        End Set
    End Property
    'Sohail (09 Mar 2012) -- End

#End Region

#Region " Dispaly Dialog "

    Public Function displayDialog(ByVal intReportUnkid As Integer, ByVal intModeId As Integer, Optional ByVal intReportTypeUnkid As Integer = -1) As Boolean
        Try
            mintReportId = intReportUnkid
            mintReportTypeId = intReportTypeUnkid
            mintModeId = intModeId
            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : displayDialog ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmUserDef_ReportMode_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objUserDefRMode = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmUserDef_ReportMode_FormClosed ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmUserDef_ReportMode_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objUserDefRMode = New clsUserDef_ReportMode
        Try
            Call FillList()

            objUserDefRMode._Reportmodeid = mintModeId
            objUserDefRMode._Reporttypeid = mintReportTypeId
            objUserDefRMode._Reportunkid = mintReportId
            Call SetVisibility()
            Call GetValue()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmUserDef_ReportMode_Load ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Function "

    Private Sub GetValue()
        Try
            If objUserDefRMode._EarningTranHeadIds.Trim.Length > 0 Then
                mstrEarningTranHeadIds = objUserDefRMode._EarningTranHeadIds
                For Each strId As String In mstrEarningTranHeadIds.Trim.Split(CChar(","))
                    For Each lvItem As ListViewItem In lvEarning.Items
                        If lvItem.Tag.ToString = strId Then lvItem.Checked = True
                    Next
                Next
            End If

            If objUserDefRMode._DeductionTranHeadIds.Trim.Length > 0 Then
                mstrDeductionTranHeadIds = objUserDefRMode._DeductionTranHeadIds
                For Each strId As String In mstrDeductionTranHeadIds.Trim.Split(CChar(","))
                    For Each lvItem As ListViewItem In lvDeduction.Items
                        If lvItem.Tag.ToString = strId Then lvItem.Checked = True
                    Next
                Next
            End If

            'Sohail (09 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            If objUserDefRMode._NonEarningDeduction_HeadIds.Trim.Length > 0 Then
                mstrNonEarningDeductionTranHeadIds = objUserDefRMode._NonEarningDeduction_HeadIds
                For Each strId As String In mstrNonEarningDeductionTranHeadIds.Trim.Split(CChar(","))
                    For Each lvItem As ListViewItem In lvNonEarningDeduction.Items
                        If lvItem.Tag.ToString = strId Then lvItem.Checked = True
                    Next
                Next
            End If
            'Sohail (09 Mar 2012) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetValue ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objUserDefRMode._Reportmodeid = mintModeId
            objUserDefRMode._Reporttypeid = mintReportTypeId
            objUserDefRMode._Reportunkid = mintReportId
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetValue ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetCommaSeparatedIds()
        Try
            If lvEarning.Enabled = True Then
                mstrEarningTranHeadIds = String.Empty
                For Each lvItem As ListViewItem In lvEarning.CheckedItems
                    mstrEarningTranHeadIds &= "," & lvItem.Tag.ToString
                Next

                If mstrEarningTranHeadIds.Trim.Length > 0 Then
                    mstrEarningTranHeadIds = Mid(mstrEarningTranHeadIds, 2)
                End If
            End If

            If lvDeduction.Enabled = True Then
                mstrDeductionTranHeadIds = String.Empty
                For Each lvItem As ListViewItem In lvDeduction.CheckedItems
                    mstrDeductionTranHeadIds &= "," & lvItem.Tag.ToString
                Next

                If mstrDeductionTranHeadIds.Trim.Length > 0 Then
                    mstrDeductionTranHeadIds = Mid(mstrDeductionTranHeadIds, 2)
                End If
            End If
            
            'Sohail (09 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            If lvNonEarningDeduction.Enabled = True Then
                mstrNonEarningDeductionTranHeadIds = String.Empty
                For Each lvItem As ListViewItem In lvNonEarningDeduction.CheckedItems
                    mstrNonEarningDeductionTranHeadIds &= "," & lvItem.Tag.ToString
                Next

                If mstrNonEarningDeductionTranHeadIds.Trim.Length > 0 Then
                    mstrNonEarningDeductionTranHeadIds = Mid(mstrNonEarningDeductionTranHeadIds, 2)
                End If
            End If
            'Sohail (09 Mar 2012) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetCommaSeparatedIds ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Try
            Dim objTransHead As New clsTransactionHead

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsList = objTransHead.GetHeadListTypeWise(enTranHeadType.EarningForEmployees)
            dsList = objTransHead.GetHeadListTypeWise(enTranHeadType.EarningForEmployees, , , , , True)
            'Sohail (17 Sep 2014) -- End
            lvEarning.Items.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = ""

                lvItem.SubItems.Add(dtRow.Item("HeadType").ToString)
                lvItem.SubItems.Add(dtRow.Item("NAME").ToString)
                lvItem.SubItems.Add(dtRow.Item("trnheadtype_id").ToString)

                lvItem.Tag = dtRow.Item("Id")

                lvEarning.Items.Add(lvItem)
                lvItem = Nothing
            Next

            lvEarning.GridLines = False
            lvEarning.GroupingColumn = colhEHeadTypeName
            lvEarning.DisplayGroups(True)

            If lvEarning.Items.Count > 4 Then
                colhETranHead.Width = colhETranHead.Width - 20
            End If


            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsList = objTransHead.GetHeadListTypeWise(enTranHeadType.DeductionForEmployee)
            dsList = objTransHead.GetHeadListTypeWise(enTranHeadType.DeductionForEmployee, , , , , True)
            'Sohail (17 Sep 2014) -- End
            Dim dsList1 As New DataSet
            dsList1 = objTransHead.GetHeadListTypeWise(enTranHeadType.EmployeesStatutoryDeductions)
            dsList.Merge(dsList1, True)
            dsList1.Dispose()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = ""

                lvItem.SubItems.Add(dtRow.Item("HeadType").ToString)
                lvItem.SubItems.Add(dtRow.Item("NAME").ToString)
                lvItem.SubItems.Add(dtRow.Item("trnheadtype_id").ToString)

                lvItem.Tag = dtRow.Item("Id")

                lvDeduction.Items.Add(lvItem)
                lvItem = Nothing
            Next

            lvDeduction.GridLines = False
            lvDeduction.GroupingColumn = colhDHeadTypeName
            lvDeduction.DisplayGroups(True)

            If lvDeduction.Items.Count >= 4 Then
                colhDTransactionHead.Width = colhDTransactionHead.Width - 20
            End If

            'Sohail (09 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            dsList = objTransHead.GetHeadListTypeWise(enTranHeadType.EmployersStatutoryContributions)
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsList1 = objTransHead.GetHeadListTypeWise(enTranHeadType.Informational)
            dsList1 = objTransHead.GetHeadListTypeWise(enTranHeadType.Informational, , , , , True)
            'Sohail (17 Sep 2014) -- End
            dsList.Merge(dsList1, True)
            dsList1.Dispose()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = ""

                lvItem.SubItems.Add(dtRow.Item("HeadType").ToString)
                lvItem.SubItems.Add(dtRow.Item("NAME").ToString)
                lvItem.SubItems.Add(dtRow.Item("trnheadtype_id").ToString)

                lvItem.Tag = dtRow.Item("Id")

                lvNonEarningDeduction.Items.Add(lvItem)
                lvItem = Nothing
            Next

            lvNonEarningDeduction.GridLines = False
            lvNonEarningDeduction.GroupingColumn = colhDHeadTypeName
            lvNonEarningDeduction.DisplayGroups(True)

            If lvNonEarningDeduction.Items.Count >= 4 Then
                colhNTranHead.Width = 250 - 18
            End If
            'Sohail (09 Mar 2012) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : FillList ; Module Name : " & mstrModuleName)
        Finally
            dsList.Dispose()
            lvItem = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            Select Case CInt(mintModeId)
                Case enUserDef_RepMode.EARNINGS
                    lvDeduction.Enabled = False
                    lvEarning.Enabled = True
                    objchkDAll.Enabled = False
                    objchkEAll.Enabled = True
                    'Sohail (09 Mar 2012) -- Start
                    'TRA - ENHANCEMENT
                    'lvNonEarningDeduction.Enabled = False
                    'objchkNAll.Enabled = False
                    'Sohail (09 Mar 2012) -- End
                Case enUserDef_RepMode.DEDUCTIONS
                    lvDeduction.Enabled = True
                    lvEarning.Enabled = False
                    objchkEAll.Enabled = False
                    objchkDAll.Enabled = True
                    'Sohail (09 Mar 2012) -- Start
                    'TRA - ENHANCEMENT
                    'lvNonEarningDeduction.Enabled = False
                    'objchkNAll.Enabled = False
                    'Sohail (09 Mar 2012) -- End
                Case enUserDef_RepMode.EARNINGANDDEDUCTION
                    lvDeduction.Enabled = True
                    lvEarning.Enabled = True
                    objchkDAll.Enabled = True
                    objchkEAll.Enabled = True
                    'Sohail (09 Mar 2012) -- Start
                    'TRA - ENHANCEMENT
                    'lvNonEarningDeduction.Enabled = True
                    'objchkNAll.Enabled = True
                    'Sohail (09 Mar 2012) -- End
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetVisibility ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnClose_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            Call GetCommaSeparatedIds()
            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnOk_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Try
            Select Case CInt(mintModeId)
                Case enUserDef_RepMode.EARNINGS, enUserDef_RepMode.EARNINGANDDEDUCTION 'Sohail (09 Mar 2012)
10:                 If lvEarning.CheckedItems.Count <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one transaction head to save."), enMsgBoxStyle.Information)
                        lvEarning.Focus()
                        Exit Sub
                    End If
                Case enUserDef_RepMode.DEDUCTIONS, enUserDef_RepMode.EARNINGANDDEDUCTION 'Sohail (09 Mar 2012)
20:                 If lvDeduction.CheckedItems.Count <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one transaction head to save."), enMsgBoxStyle.Information)
                        lvDeduction.Focus()
                        Exit Sub
                    End If
                Case enUserDef_RepMode.EARNINGANDDEDUCTION
                    'Sohail (09 Mar 2012) -- Start
                    'TRA - ENHANCEMENT
                    'GoTo 10
                    'GoTo 20
                    'Sohail (09 Mar 2012) -- End
            End Select

            Call GetCommaSeparatedIds()

            Select Case CInt(mintModeId)
                Case enUserDef_RepMode.EARNINGS
                    Call SetValue()
                    objUserDefRMode._Headtypeid = enTranHeadType.EarningForEmployees
                    objUserDefRMode._EarningTranHeadIds = mstrEarningTranHeadIds

                    Dim intUnkid As Integer = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(mintModeId), enTranHeadType.EarningForEmployees)

                    objUserDefRMode._Reportmodeunkid = intUnkid

                    If intUnkid <= 0 Then
                        objUserDefRMode.Insert()
                    Else
                        objUserDefRMode.Update()
                    End If

                Case enUserDef_RepMode.DEDUCTIONS
                    Call SetValue()
                    objUserDefRMode._Headtypeid = enTranHeadType.DeductionForEmployee
                    objUserDefRMode._EarningTranHeadIds = mstrDeductionTranHeadIds

                    Dim intUnkid As Integer = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(mintModeId), enTranHeadType.DeductionForEmployee)

                    objUserDefRMode._Reportmodeunkid = intUnkid

                    If intUnkid <= 0 Then
                        objUserDefRMode.Insert()
                    Else
                        objUserDefRMode.Update()
                    End If

                Case enUserDef_RepMode.EARNINGANDDEDUCTION
                    'Sohail (09 Mar 2012) -- Start
                    'TRA - ENHANCEMENT
                    For i As Integer = 0 To 2
                        'For i As Integer = 0 To 1
                        'Sohail (09 Mar 2012) -- End

                        Call SetValue()

                        Dim intUnkid As Integer = -1

                        If i = 0 Then
                            objUserDefRMode._Headtypeid = enTranHeadType.EarningForEmployees
                            objUserDefRMode._EarningTranHeadIds = mstrEarningTranHeadIds
                            intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(mintModeId), enTranHeadType.EarningForEmployees)
                            objUserDefRMode._Reportmodeunkid = intUnkid
                            'Sohail (09 Mar 2012) -- Start
                            'TRA - ENHANCEMENT
                            'Else
                            '    objUserDefRMode._Headtypeid = enTranHeadType.DeductionForEmployee
                            '    objUserDefRMode._EarningTranHeadIds = mstrDeductionTranHeadIds
                            '    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(mintModeId), enTranHeadType.DeductionForEmployee)
                            '    objUserDefRMode._Reportmodeunkid = intUnkid
                        ElseIf i = 1 Then
                            objUserDefRMode._Headtypeid = enTranHeadType.DeductionForEmployee
                            objUserDefRMode._EarningTranHeadIds = mstrDeductionTranHeadIds
                            intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(mintModeId), enTranHeadType.DeductionForEmployee)
                            objUserDefRMode._Reportmodeunkid = intUnkid
                        Else
                            objUserDefRMode._Headtypeid = enTranHeadType.EmployersStatutoryContributions
                            objUserDefRMode._EarningTranHeadIds = mstrNonEarningDeductionTranHeadIds
                            intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(mintModeId), enTranHeadType.EmployersStatutoryContributions)
                            objUserDefRMode._Reportmodeunkid = intUnkid
                            'Sohail (09 Mar 2012) -- End
                        End If


                        If intUnkid <= 0 Then
                            objUserDefRMode.Insert()
                        Else
                            objUserDefRMode.Update()
                        End If

                    Next
            End Select
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Information Successfully Saved."), enMsgBoxStyle.Information)
            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnSaveSelection_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Contols "

    Private Sub objchkEAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEAll.CheckedChanged
        Try
            RemoveHandler lvEarning.ItemChecked, AddressOf lvEarning_ItemChecked
            For Each lvItem As ListViewItem In lvEarning.Items
                lvItem.Checked = CBool(objchkEAll.CheckState)
            Next
            AddHandler lvEarning.ItemChecked, AddressOf lvEarning_ItemChecked
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : objchkEAll_CheckedChanged ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkDAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkDAll.CheckedChanged
        Try
            RemoveHandler lvDeduction.ItemChecked, AddressOf lvDeduction_ItemChecked
            For Each lvItem As ListViewItem In lvDeduction.Items
                lvItem.Checked = CBool(objchkDAll.CheckState)
            Next
            AddHandler lvDeduction.ItemChecked, AddressOf lvDeduction_ItemChecked
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : objchkDAll_CheckedChanged ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub objchkNAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNAll.CheckedChanged
        Try
            RemoveHandler lvNonEarningDeduction.ItemChecked, AddressOf lvNonEarningDeduction_ItemChecked
            For Each lvItem As ListViewItem In lvNonEarningDeduction.Items
                lvItem.Checked = CBool(objchkNAll.CheckState)
            Next
            AddHandler lvNonEarningDeduction.ItemChecked, AddressOf lvNonEarningDeduction_ItemChecked
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : objchkNAll_CheckedChanged ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (09 Mar 2012) -- End

    Private Sub lvEarning_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEarning.ItemChecked
        Try
            RemoveHandler objchkEAll.CheckedChanged, AddressOf objchkEAll_CheckedChanged
            If lvEarning.CheckedItems.Count <= 0 Then
                objchkEAll.CheckState = CheckState.Unchecked
            ElseIf lvEarning.CheckedItems.Count < lvEarning.Items.Count Then
                objchkEAll.CheckState = CheckState.Indeterminate
            ElseIf lvEarning.CheckedItems.Count = lvEarning.Items.Count Then
                objchkEAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkEAll.CheckedChanged, AddressOf objchkEAll_CheckedChanged
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : lvEarning_ItemChecked ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvDeduction_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDeduction.ItemChecked
        Try
            RemoveHandler objchkDAll.CheckedChanged, AddressOf objchkDAll_CheckedChanged
            If lvDeduction.CheckedItems.Count <= 0 Then
                objchkDAll.CheckState = CheckState.Unchecked
            ElseIf lvDeduction.CheckedItems.Count < lvDeduction.Items.Count Then
                objchkDAll.CheckState = CheckState.Indeterminate
            ElseIf lvDeduction.CheckedItems.Count = lvDeduction.Items.Count Then
                objchkDAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkDAll.CheckedChanged, AddressOf objchkDAll_CheckedChanged
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : lvDeduction_ItemChecked ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub lvNonEarningDeduction_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvNonEarningDeduction.ItemChecked
        Try
            RemoveHandler objchkNAll.CheckedChanged, AddressOf objchkNAll_CheckedChanged
            If lvNonEarningDeduction.CheckedItems.Count <= 0 Then
                objchkNAll.CheckState = CheckState.Unchecked
            ElseIf lvNonEarningDeduction.CheckedItems.Count < lvNonEarningDeduction.Items.Count Then
                objchkNAll.CheckState = CheckState.Indeterminate
            ElseIf lvNonEarningDeduction.CheckedItems.Count = lvNonEarningDeduction.Items.Count Then
                objchkNAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkNAll.CheckedChanged, AddressOf objchkNAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvNonEarningDeduction_ItemChecked", mstrModuleName)
        End Try
    End Sub
    'Sohail (09 Mar 2012) -- End
#End Region

End Class