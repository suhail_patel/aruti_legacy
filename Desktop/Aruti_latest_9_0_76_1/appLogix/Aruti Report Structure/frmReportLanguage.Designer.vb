﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportLanguage
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportLanguage))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objspc1 = New System.Windows.Forms.SplitContainer
        Me.cboFillType = New System.Windows.Forms.ComboBox
        Me.lblChangeFor = New System.Windows.Forms.Label
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.lblSearchBy = New System.Windows.Forms.Label
        Me.cboFormFilter = New System.Windows.Forms.ComboBox
        Me.dgLanguage = New System.Windows.Forms.DataGridView
        Me.dgColLanguage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcollanguage1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolLanguage2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcollangunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.lblReportCategory = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.objspc1.Panel1.SuspendLayout()
        Me.objspc1.Panel2.SuspendLayout()
        Me.objspc1.SuspendLayout()
        CType(Me.dgLanguage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objspc1)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(823, 464)
        Me.pnlMain.TabIndex = 0
        '
        'objspc1
        '
        Me.objspc1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspc1.Location = New System.Drawing.Point(0, 0)
        Me.objspc1.Name = "objspc1"
        Me.objspc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc1.Panel1
        '
        Me.objspc1.Panel1.Controls.Add(Me.lblReportCategory)
        Me.objspc1.Panel1.Controls.Add(Me.cboCategory)
        Me.objspc1.Panel1.Controls.Add(Me.cboFillType)
        Me.objspc1.Panel1.Controls.Add(Me.lblChangeFor)
        Me.objspc1.Panel1.Controls.Add(Me.txtSearch)
        Me.objspc1.Panel1.Controls.Add(Me.lblSearchBy)
        Me.objspc1.Panel1.Controls.Add(Me.cboFormFilter)
        '
        'objspc1.Panel2
        '
        Me.objspc1.Panel2.Controls.Add(Me.dgLanguage)
        Me.objspc1.Size = New System.Drawing.Size(823, 409)
        Me.objspc1.SplitterDistance = 72
        Me.objspc1.SplitterWidth = 2
        Me.objspc1.TabIndex = 21
        '
        'cboFillType
        '
        Me.cboFillType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFillType.FormattingEnabled = True
        Me.cboFillType.Location = New System.Drawing.Point(149, 13)
        Me.cboFillType.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboFillType.Name = "cboFillType"
        Me.cboFillType.Size = New System.Drawing.Size(223, 21)
        Me.cboFillType.TabIndex = 165
        '
        'lblChangeFor
        '
        Me.lblChangeFor.Location = New System.Drawing.Point(12, 15)
        Me.lblChangeFor.Name = "lblChangeFor"
        Me.lblChangeFor.Size = New System.Drawing.Size(131, 17)
        Me.lblChangeFor.TabIndex = 164
        Me.lblChangeFor.Text = "Change Language For"
        Me.lblChangeFor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(270, 42)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(541, 21)
        Me.txtSearch.TabIndex = 163
        '
        'lblSearchBy
        '
        Me.lblSearchBy.Location = New System.Drawing.Point(12, 44)
        Me.lblSearchBy.Name = "lblSearchBy"
        Me.lblSearchBy.Size = New System.Drawing.Size(131, 17)
        Me.lblSearchBy.TabIndex = 21
        Me.lblSearchBy.Text = "Search By"
        Me.lblSearchBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFormFilter
        '
        Me.cboFormFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormFilter.FormattingEnabled = True
        Me.cboFormFilter.Location = New System.Drawing.Point(149, 42)
        Me.cboFormFilter.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboFormFilter.Name = "cboFormFilter"
        Me.cboFormFilter.Size = New System.Drawing.Size(115, 21)
        Me.cboFormFilter.TabIndex = 20
        '
        'dgLanguage
        '
        Me.dgLanguage.AllowUserToAddRows = False
        Me.dgLanguage.AllowUserToDeleteRows = False
        Me.dgLanguage.BackgroundColor = System.Drawing.Color.White
        Me.dgLanguage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgLanguage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgLanguage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgColLanguage, Me.dgcollanguage1, Me.dgcolLanguage2, Me.dgcollangunkid})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgLanguage.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgLanguage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgLanguage.Location = New System.Drawing.Point(0, 0)
        Me.dgLanguage.MultiSelect = False
        Me.dgLanguage.Name = "dgLanguage"
        Me.dgLanguage.RowHeadersVisible = False
        Me.dgLanguage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgLanguage.ShowCellErrors = False
        Me.dgLanguage.ShowRowErrors = False
        Me.dgLanguage.Size = New System.Drawing.Size(823, 335)
        Me.dgLanguage.TabIndex = 2
        '
        'dgColLanguage
        '
        Me.dgColLanguage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColLanguage.HeaderText = "Language"
        Me.dgColLanguage.Name = "dgColLanguage"
        Me.dgColLanguage.ReadOnly = True
        '
        'dgcollanguage1
        '
        Me.dgcollanguage1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcollanguage1.HeaderText = "Language1"
        Me.dgcollanguage1.Name = "dgcollanguage1"
        '
        'dgcolLanguage2
        '
        Me.dgcolLanguage2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolLanguage2.HeaderText = "Language2"
        Me.dgcolLanguage2.Name = "dgcolLanguage2"
        '
        'dgcollangunkid
        '
        Me.dgcollangunkid.HeaderText = "ID"
        Me.dgcollangunkid.Name = "dgcollangunkid"
        Me.dgcollangunkid.ReadOnly = True
        Me.dgcollangunkid.Visible = False
        Me.dgcollangunkid.Width = 5
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 409)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(823, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.CausesValidation = False
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(718, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Language"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Language1"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Language2"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        Me.DataGridViewTextBoxColumn4.Width = 5
        '
        'cboCategory
        '
        Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Location = New System.Drawing.Point(515, 13)
        Me.cboCategory.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(296, 21)
        Me.cboCategory.TabIndex = 166
        '
        'lblReportCategory
        '
        Me.lblReportCategory.Location = New System.Drawing.Point(378, 15)
        Me.lblReportCategory.Name = "lblReportCategory"
        Me.lblReportCategory.Size = New System.Drawing.Size(131, 17)
        Me.lblReportCategory.TabIndex = 167
        Me.lblReportCategory.Text = "Report Category"
        Me.lblReportCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmReportLanguage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(823, 464)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportLanguage"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Report Language"
        Me.pnlMain.ResumeLayout(False)
        Me.objspc1.Panel1.ResumeLayout(False)
        Me.objspc1.Panel1.PerformLayout()
        Me.objspc1.Panel2.ResumeLayout(False)
        Me.objspc1.ResumeLayout(False)
        CType(Me.dgLanguage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents dgLanguage As System.Windows.Forms.DataGridView
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgColLanguage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcollanguage1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolLanguage2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcollangunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboFormFilter As System.Windows.Forms.ComboBox
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents lblSearchBy As System.Windows.Forms.Label
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblChangeFor As System.Windows.Forms.Label
    Friend WithEvents cboFillType As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportCategory As System.Windows.Forms.Label
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
End Class
