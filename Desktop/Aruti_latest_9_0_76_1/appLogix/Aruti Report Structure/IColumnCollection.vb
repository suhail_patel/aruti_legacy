Imports System.Text

Public Class IColumnCollection
    Implements System.Collections.IList, ICloneable

    Dim mColList As New List(Of IColumn)

    Public Sub New()
    End Sub

    Private Sub New(ByVal members As List(Of IColumn))
        For Each e As IColumn In members
            mColList.Add(DirectCast(e.Clone(), IColumn))
        Next
    End Sub

    Public Function Clone() As Object Implements System.ICloneable.Clone
        Return New IColumnCollection(Me.mColList)
    End Function


    Public Overloads Overrides Function ToString() As String
        Dim str As New StringBuilder()

        For Each e As IColumn In mColList
            str.AppendFormat(" {0}" & vbCr & vbLf, e)
        Next

        Return str.ToString()
    End Function


    Public Sub CopyTo(ByVal array As System.Array, ByVal index As Integer) Implements System.Collections.ICollection.CopyTo
        mColList.CopyTo(array, index)
    End Sub

    Public ReadOnly Property Count() As Integer Implements System.Collections.ICollection.Count
        Get
            Return mColList.Count
        End Get
    End Property

    Public ReadOnly Property IsSynchronized() As Boolean Implements System.Collections.ICollection.IsSynchronized
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property SyncRoot() As Object Implements System.Collections.ICollection.SyncRoot
        Get
            Return Nothing
        End Get
    End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        Return mColList.GetEnumerator
    End Function

    Public Function Add(ByVal value As Object) As Integer Implements System.Collections.IList.Add
        mColList.Add(value)
    End Function

    Public Sub Clear() Implements System.Collections.IList.Clear
        mColList.Clear()
    End Sub

    Public Function Contains(ByVal value As Object) As Boolean Implements System.Collections.IList.Contains
        Return mColList.Contains(value)
    End Function

    Public Function IndexOf(ByVal value As Object) As Integer Implements System.Collections.IList.IndexOf
        Return mColList.IndexOf(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As Object) Implements System.Collections.IList.Insert
        mColList.Insert(index, value)
    End Sub

    Public ReadOnly Property IsFixedSize() As Boolean Implements System.Collections.IList.IsFixedSize
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.IList.IsReadOnly
        Get
            Return False
        End Get
    End Property

    Public Property Item(ByVal index As Integer) As Object Implements System.Collections.IList.Item
        Get
            Return mColList.Item(index)
        End Get
        Set(ByVal value As Object)
            mColList.Item(index) = value
        End Set
    End Property

    Default Public Property ColumnItem(ByVal index As Integer) As IColumn
        Get
            Return DirectCast(mColList.Item(index), IColumn)
        End Get
        Set(ByVal value As IColumn)
            mColList.Item(index) = value
        End Set
    End Property

    Public ReadOnly Property Items() As Object
        Get
            Return mColList
        End Get
    End Property

    Public Sub Remove(ByVal value As Object) Implements System.Collections.IList.Remove
        mColList.Remove(value)
    End Sub

    Public Sub RemoveAt(ByVal index As Integer) Implements System.Collections.IList.RemoveAt
        mColList.RemoveAt(index)
    End Sub

End Class
