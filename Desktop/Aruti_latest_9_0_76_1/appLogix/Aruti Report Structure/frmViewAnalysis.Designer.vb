﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewAnalysis
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewAnalysis))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objSpDetail = New System.Windows.Forms.SplitContainer
        Me.pnlAnalysis = New System.Windows.Forms.Panel
        Me.radTeam = New System.Windows.Forms.RadioButton
        Me.radUnitGrp = New System.Windows.Forms.RadioButton
        Me.radSectionGrp = New System.Windows.Forms.RadioButton
        Me.radCostCenter = New System.Windows.Forms.RadioButton
        Me.radJob = New System.Windows.Forms.RadioButton
        Me.radUnit = New System.Windows.Forms.RadioButton
        Me.radDepartment = New System.Windows.Forms.RadioButton
        Me.radSection = New System.Windows.Forms.RadioButton
        Me.radBranch = New System.Windows.Forms.RadioButton
        Me.chkSelectallList = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhReportBy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhReportById = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objSpTitle = New System.Windows.Forms.SplitContainer
        Me.lblAnalysisBy = New System.Windows.Forms.Label
        Me.lblReportBy = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSet = New eZee.Common.eZeeLightButton(Me.components)
        Me.radDepartmentGrp = New System.Windows.Forms.RadioButton
        Me.radJobGrp = New System.Windows.Forms.RadioButton
        Me.radClassGrp = New System.Windows.Forms.RadioButton
        Me.radClass = New System.Windows.Forms.RadioButton
        Me.radGrade = New System.Windows.Forms.RadioButton
        Me.radGradeGrp = New System.Windows.Forms.RadioButton
        Me.radGradeLevel = New System.Windows.Forms.RadioButton
        Me.pnlMain.SuspendLayout()
        Me.objSpDetail.Panel1.SuspendLayout()
        Me.objSpDetail.Panel2.SuspendLayout()
        Me.objSpDetail.SuspendLayout()
        Me.pnlAnalysis.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objSpTitle.Panel1.SuspendLayout()
        Me.objSpTitle.Panel2.SuspendLayout()
        Me.objSpTitle.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objSpDetail)
        Me.pnlMain.Controls.Add(Me.objSpTitle)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(537, 460)
        Me.pnlMain.TabIndex = 0
        '
        'objSpDetail
        '
        Me.objSpDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objSpDetail.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSpDetail.IsSplitterFixed = True
        Me.objSpDetail.Location = New System.Drawing.Point(0, 29)
        Me.objSpDetail.Name = "objSpDetail"
        '
        'objSpDetail.Panel1
        '
        Me.objSpDetail.Panel1.Controls.Add(Me.pnlAnalysis)
        '
        'objSpDetail.Panel2
        '
        Me.objSpDetail.Panel2.Controls.Add(Me.chkSelectallList)
        Me.objSpDetail.Panel2.Controls.Add(Me.dgvData)
        Me.objSpDetail.Size = New System.Drawing.Size(536, 378)
        Me.objSpDetail.SplitterDistance = 176
        Me.objSpDetail.SplitterWidth = 2
        Me.objSpDetail.TabIndex = 1
        '
        'pnlAnalysis
        '
        Me.pnlAnalysis.AutoScroll = True
        Me.pnlAnalysis.Controls.Add(Me.radGradeLevel)
        Me.pnlAnalysis.Controls.Add(Me.radGrade)
        Me.pnlAnalysis.Controls.Add(Me.radGradeGrp)
        Me.pnlAnalysis.Controls.Add(Me.radClass)
        Me.pnlAnalysis.Controls.Add(Me.radClassGrp)
        Me.pnlAnalysis.Controls.Add(Me.radJobGrp)
        Me.pnlAnalysis.Controls.Add(Me.radDepartmentGrp)
        Me.pnlAnalysis.Controls.Add(Me.radTeam)
        Me.pnlAnalysis.Controls.Add(Me.radUnitGrp)
        Me.pnlAnalysis.Controls.Add(Me.radSectionGrp)
        Me.pnlAnalysis.Controls.Add(Me.radCostCenter)
        Me.pnlAnalysis.Controls.Add(Me.radJob)
        Me.pnlAnalysis.Controls.Add(Me.radUnit)
        Me.pnlAnalysis.Controls.Add(Me.radDepartment)
        Me.pnlAnalysis.Controls.Add(Me.radSection)
        Me.pnlAnalysis.Controls.Add(Me.radBranch)
        Me.pnlAnalysis.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAnalysis.Location = New System.Drawing.Point(0, 0)
        Me.pnlAnalysis.Name = "pnlAnalysis"
        Me.pnlAnalysis.Size = New System.Drawing.Size(174, 376)
        Me.pnlAnalysis.TabIndex = 0
        '
        'radTeam
        '
        Me.radTeam.Location = New System.Drawing.Point(8, 167)
        Me.radTeam.Name = "radTeam"
        Me.radTeam.Size = New System.Drawing.Size(160, 17)
        Me.radTeam.TabIndex = 7
        Me.radTeam.TabStop = True
        Me.radTeam.Text = "Team"
        Me.radTeam.UseVisualStyleBackColor = True
        '
        'radUnitGrp
        '
        Me.radUnitGrp.Location = New System.Drawing.Point(8, 122)
        Me.radUnitGrp.Name = "radUnitGrp"
        Me.radUnitGrp.Size = New System.Drawing.Size(160, 17)
        Me.radUnitGrp.TabIndex = 5
        Me.radUnitGrp.TabStop = True
        Me.radUnitGrp.Text = "Unit Group"
        Me.radUnitGrp.UseVisualStyleBackColor = True
        '
        'radSectionGrp
        '
        Me.radSectionGrp.Location = New System.Drawing.Point(8, 75)
        Me.radSectionGrp.Name = "radSectionGrp"
        Me.radSectionGrp.Size = New System.Drawing.Size(160, 17)
        Me.radSectionGrp.TabIndex = 3
        Me.radSectionGrp.TabStop = True
        Me.radSectionGrp.Text = "Section Group"
        Me.radSectionGrp.UseVisualStyleBackColor = True
        '
        'radCostCenter
        '
        Me.radCostCenter.Location = New System.Drawing.Point(8, 351)
        Me.radCostCenter.Name = "radCostCenter"
        Me.radCostCenter.Size = New System.Drawing.Size(160, 17)
        Me.radCostCenter.TabIndex = 15
        Me.radCostCenter.TabStop = True
        Me.radCostCenter.Text = "Cost Center"
        Me.radCostCenter.UseVisualStyleBackColor = True
        '
        'radJob
        '
        Me.radJob.Location = New System.Drawing.Point(8, 213)
        Me.radJob.Name = "radJob"
        Me.radJob.Size = New System.Drawing.Size(160, 17)
        Me.radJob.TabIndex = 9
        Me.radJob.TabStop = True
        Me.radJob.Text = "Job"
        Me.radJob.UseVisualStyleBackColor = True
        '
        'radUnit
        '
        Me.radUnit.Location = New System.Drawing.Point(8, 145)
        Me.radUnit.Name = "radUnit"
        Me.radUnit.Size = New System.Drawing.Size(160, 17)
        Me.radUnit.TabIndex = 6
        Me.radUnit.TabStop = True
        Me.radUnit.Text = "Unit"
        Me.radUnit.UseVisualStyleBackColor = True
        '
        'radDepartment
        '
        Me.radDepartment.Location = New System.Drawing.Point(8, 52)
        Me.radDepartment.Name = "radDepartment"
        Me.radDepartment.Size = New System.Drawing.Size(160, 17)
        Me.radDepartment.TabIndex = 2
        Me.radDepartment.TabStop = True
        Me.radDepartment.Text = "Department"
        Me.radDepartment.UseVisualStyleBackColor = True
        '
        'radSection
        '
        Me.radSection.Location = New System.Drawing.Point(8, 98)
        Me.radSection.Name = "radSection"
        Me.radSection.Size = New System.Drawing.Size(160, 17)
        Me.radSection.TabIndex = 4
        Me.radSection.TabStop = True
        Me.radSection.Text = "Section"
        Me.radSection.UseVisualStyleBackColor = True
        '
        'radBranch
        '
        Me.radBranch.Location = New System.Drawing.Point(8, 6)
        Me.radBranch.Name = "radBranch"
        Me.radBranch.Size = New System.Drawing.Size(160, 17)
        Me.radBranch.TabIndex = 0
        Me.radBranch.TabStop = True
        Me.radBranch.Text = "Branch"
        Me.radBranch.UseVisualStyleBackColor = True
        '
        'chkSelectallList
        '
        Me.chkSelectallList.AutoSize = True
        Me.chkSelectallList.Location = New System.Drawing.Point(6, 4)
        Me.chkSelectallList.Name = "chkSelectallList"
        Me.chkSelectallList.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectallList.TabIndex = 2
        Me.chkSelectallList.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkSelectallList.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeight = 22
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhReportBy, Me.objdgcolhReportById})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(356, 376)
        Me.dgvData.TabIndex = 0
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhReportBy
        '
        Me.dgcolhReportBy.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhReportBy.HeaderText = "Report By"
        Me.dgcolhReportBy.Name = "dgcolhReportBy"
        Me.dgcolhReportBy.ReadOnly = True
        Me.dgcolhReportBy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhReportById
        '
        Me.objdgcolhReportById.HeaderText = "objdgcolhReportById"
        Me.objdgcolhReportById.Name = "objdgcolhReportById"
        Me.objdgcolhReportById.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhReportById.Visible = False
        '
        'objSpTitle
        '
        Me.objSpTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objSpTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.objSpTitle.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSpTitle.IsSplitterFixed = True
        Me.objSpTitle.Location = New System.Drawing.Point(0, 0)
        Me.objSpTitle.Name = "objSpTitle"
        '
        'objSpTitle.Panel1
        '
        Me.objSpTitle.Panel1.Controls.Add(Me.lblAnalysisBy)
        '
        'objSpTitle.Panel2
        '
        Me.objSpTitle.Panel2.Controls.Add(Me.lblReportBy)
        Me.objSpTitle.Size = New System.Drawing.Size(537, 28)
        Me.objSpTitle.SplitterDistance = 176
        Me.objSpTitle.SplitterWidth = 2
        Me.objSpTitle.TabIndex = 0
        '
        'lblAnalysisBy
        '
        Me.lblAnalysisBy.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnalysisBy.Location = New System.Drawing.Point(0, 0)
        Me.lblAnalysisBy.Name = "lblAnalysisBy"
        Me.lblAnalysisBy.Size = New System.Drawing.Size(174, 26)
        Me.lblAnalysisBy.TabIndex = 3
        Me.lblAnalysisBy.Text = "Analysis By"
        Me.lblAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblReportBy
        '
        Me.lblReportBy.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblReportBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportBy.Location = New System.Drawing.Point(0, 0)
        Me.lblReportBy.Name = "lblReportBy"
        Me.lblReportBy.Size = New System.Drawing.Size(357, 26)
        Me.lblReportBy.TabIndex = 5
        Me.lblReportBy.Text = "Report By"
        Me.lblReportBy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Controls.Add(Me.btnSet)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 410)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(537, 50)
        Me.objFooter.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(432, 8)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSet
        '
        Me.btnSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSet.BackColor = System.Drawing.Color.White
        Me.btnSet.BackgroundImage = CType(resources.GetObject("btnSet.BackgroundImage"), System.Drawing.Image)
        Me.btnSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSet.BorderColor = System.Drawing.Color.Empty
        Me.btnSet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSet.FlatAppearance.BorderSize = 0
        Me.btnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSet.ForeColor = System.Drawing.Color.Black
        Me.btnSet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSet.GradientForeColor = System.Drawing.Color.Black
        Me.btnSet.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Location = New System.Drawing.Point(329, 8)
        Me.btnSet.Name = "btnSet"
        Me.btnSet.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Size = New System.Drawing.Size(97, 30)
        Me.btnSet.TabIndex = 0
        Me.btnSet.Text = "&Set"
        Me.btnSet.UseVisualStyleBackColor = True
        '
        'radDepartmentGrp
        '
        Me.radDepartmentGrp.Location = New System.Drawing.Point(8, 29)
        Me.radDepartmentGrp.Name = "radDepartmentGrp"
        Me.radDepartmentGrp.Size = New System.Drawing.Size(160, 17)
        Me.radDepartmentGrp.TabIndex = 1
        Me.radDepartmentGrp.TabStop = True
        Me.radDepartmentGrp.Text = "Department Group"
        Me.radDepartmentGrp.UseVisualStyleBackColor = True
        '
        'radJobGrp
        '
        Me.radJobGrp.Location = New System.Drawing.Point(8, 190)
        Me.radJobGrp.Name = "radJobGrp"
        Me.radJobGrp.Size = New System.Drawing.Size(160, 17)
        Me.radJobGrp.TabIndex = 8
        Me.radJobGrp.TabStop = True
        Me.radJobGrp.Text = "Job Group"
        Me.radJobGrp.UseVisualStyleBackColor = True
        '
        'radClassGrp
        '
        Me.radClassGrp.Location = New System.Drawing.Point(8, 236)
        Me.radClassGrp.Name = "radClassGrp"
        Me.radClassGrp.Size = New System.Drawing.Size(160, 17)
        Me.radClassGrp.TabIndex = 10
        Me.radClassGrp.TabStop = True
        Me.radClassGrp.Text = "Class Group"
        Me.radClassGrp.UseVisualStyleBackColor = True
        '
        'radClass
        '
        Me.radClass.Location = New System.Drawing.Point(8, 259)
        Me.radClass.Name = "radClass"
        Me.radClass.Size = New System.Drawing.Size(160, 17)
        Me.radClass.TabIndex = 11
        Me.radClass.TabStop = True
        Me.radClass.Text = "Class"
        Me.radClass.UseVisualStyleBackColor = True
        '
        'radGrade
        '
        Me.radGrade.Location = New System.Drawing.Point(8, 305)
        Me.radGrade.Name = "radGrade"
        Me.radGrade.Size = New System.Drawing.Size(160, 17)
        Me.radGrade.TabIndex = 13
        Me.radGrade.TabStop = True
        Me.radGrade.Text = "Grade"
        Me.radGrade.UseVisualStyleBackColor = True
        '
        'radGradeGrp
        '
        Me.radGradeGrp.Location = New System.Drawing.Point(8, 282)
        Me.radGradeGrp.Name = "radGradeGrp"
        Me.radGradeGrp.Size = New System.Drawing.Size(160, 17)
        Me.radGradeGrp.TabIndex = 12
        Me.radGradeGrp.TabStop = True
        Me.radGradeGrp.Text = "Grade Group"
        Me.radGradeGrp.UseVisualStyleBackColor = True
        '
        'radGradeLevel
        '
        Me.radGradeLevel.Location = New System.Drawing.Point(8, 328)
        Me.radGradeLevel.Name = "radGradeLevel"
        Me.radGradeLevel.Size = New System.Drawing.Size(160, 17)
        Me.radGradeLevel.TabIndex = 14
        Me.radGradeLevel.TabStop = True
        Me.radGradeLevel.Text = "Grade Level"
        Me.radGradeLevel.UseVisualStyleBackColor = True
        '
        'frmViewAnalysis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(537, 460)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewAnalysis"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Analysis By"
        Me.pnlMain.ResumeLayout(False)
        Me.objSpDetail.Panel1.ResumeLayout(False)
        Me.objSpDetail.Panel2.ResumeLayout(False)
        Me.objSpDetail.Panel2.PerformLayout()
        Me.objSpDetail.ResumeLayout(False)
        Me.pnlAnalysis.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objSpTitle.Panel1.ResumeLayout(False)
        Me.objSpTitle.Panel2.ResumeLayout(False)
        Me.objSpTitle.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSet As eZee.Common.eZeeLightButton
    Friend WithEvents chkSelectallList As System.Windows.Forms.CheckBox
    Friend WithEvents objSpTitle As System.Windows.Forms.SplitContainer
    Friend WithEvents objSpDetail As System.Windows.Forms.SplitContainer
    Friend WithEvents lblAnalysisBy As System.Windows.Forms.Label
    Friend WithEvents lblReportBy As System.Windows.Forms.Label
    Friend WithEvents pnlAnalysis As System.Windows.Forms.Panel
    Friend WithEvents radCostCenter As System.Windows.Forms.RadioButton
    Friend WithEvents radJob As System.Windows.Forms.RadioButton
    Friend WithEvents radUnit As System.Windows.Forms.RadioButton
    Friend WithEvents radDepartment As System.Windows.Forms.RadioButton
    Friend WithEvents radSection As System.Windows.Forms.RadioButton
    Friend WithEvents radBranch As System.Windows.Forms.RadioButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhReportBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhReportById As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents radTeam As System.Windows.Forms.RadioButton
    Friend WithEvents radUnitGrp As System.Windows.Forms.RadioButton
    Friend WithEvents radSectionGrp As System.Windows.Forms.RadioButton
    Friend WithEvents radJobGrp As System.Windows.Forms.RadioButton
    Friend WithEvents radDepartmentGrp As System.Windows.Forms.RadioButton
    Friend WithEvents radGradeLevel As System.Windows.Forms.RadioButton
    Friend WithEvents radGrade As System.Windows.Forms.RadioButton
    Friend WithEvents radGradeGrp As System.Windows.Forms.RadioButton
    Friend WithEvents radClass As System.Windows.Forms.RadioButton
    Friend WithEvents radClassGrp As System.Windows.Forms.RadioButton
End Class
