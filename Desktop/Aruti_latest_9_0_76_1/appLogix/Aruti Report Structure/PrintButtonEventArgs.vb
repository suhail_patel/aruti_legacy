﻿Public Class PrintButtonEventArgs
    Inherits EventArgs

#Region "Fields"
    Private mintType As Integer = 0
#End Region

#Region "Constructor"
    Public Sub New(ByVal intType As Integer)
        mintType = intType
    End Sub
#End Region

#Region "Properties"
    Public ReadOnly Property Type() As Integer
        Get
            Return mintType
        End Get
    End Property
#End Region
End Class
