Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
Imports ExcelWriter
Imports System.Text

Public MustInherit Class IReportData
    Private ReadOnly mstrModuleName As String = "IReportData"

    Private m_strReportName As String = ""
    Private m_strReportType As String = ""
    Private m_strFilterTitle As String = ""
    Private m_strFilterQuery As String = ""
    Private m_strReportDesc As String = ""
    Private m_strCompanyName As String = ""

    Private m_strOrderByDisplay As String = ""
    Private m_strOrderByQuery As String = ""
    Private m_blnOrderByDescending As Boolean = False 'Sohail (18 Feb 2012)
    Private m_strFileNameAfterExported As String = "" 'Sohail (22 Mar 2013)


    'Pinkal (01-Dec-2016) -- Start
    'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
    Private m_strUserName As String = ""
    'Pinkal (01-Dec-2016) -- End



    Public MustOverride Sub generateReport(ByVal pintReportType As Integer, _
                          Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, _
                          Optional ByVal ExportAction As enExportAction = enExportAction.None)

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public MustOverride Sub generateReportNew(ByVal xDatabaseName As String, _
                                              ByVal xUserUnkid As Integer, _
                                              ByVal xYearUnkid As Integer, _
                                              ByVal xCompanyUnkid As Integer, _
                                              ByVal xPeriodStart As DateTime, _
                                              ByVal xPeriodEnd As DateTime, _
                                              ByVal xUserModeSetting As String, _
                                              ByVal xOnlyApproved As Boolean, _
                                              ByVal xExportReportPath As String, _
                                              ByVal xOpenReportAfterExport As Boolean, _
                                              ByVal pintReportType As Integer, _
                                              Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, _
                                              Optional ByVal ExportAction As enExportAction = enExportAction.None, _
                                              Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                              )
    'S.SANDEEP [04 JUN 2015] -- END

    Public MustOverride Sub setOrderBy(ByVal intReportType As Integer)
    Public MustOverride Sub setDefaultOrderBy(ByVal intReportType As Integer)

    Public Property _ReportName() As String
        Get
            Return m_strReportName
        End Get
        Set(ByVal value As String)
            m_strReportName = value
        End Set
    End Property

    'Public Property _ReportType() As String
    '    Get
    '        Return m_strReportType
    '    End Get
    '    Set(ByVal value As String)
    '        m_strReportType = value
    '    End Set
    'End Property

    Public Property _FilterTitle() As String
        Get
            Return m_strFilterTitle
        End Get
        Set(ByVal value As String)
            m_strFilterTitle = value
        End Set
    End Property

    Public Property _FilterQuery() As String
        Get
            Return m_strFilterQuery
        End Get
        Set(ByVal value As String)
            m_strFilterQuery = value
        End Set
    End Property

    Public ReadOnly Property _ReportDesc() As String
        Get
            Return m_strReportDesc
        End Get
    End Property
    Public ReadOnly Property _CompanyName() As String
        Get
            Return m_strCompanyName
        End Get
    End Property
    Public ReadOnly Property _PrintDate() As String
        Get
            Return ReportFunction.PrintDate
        End Get
    End Property


    'Pinkal (01-Dec-2016) -- Start
    'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].

    'Public ReadOnly Property _UserName() As String
    '    Get
    '        Return User._Object._Username
    '    End Get
    'End Property

    Public Property _UserName() As String
        Get
            If m_strUserName.Trim.Length > 0 Then
                Return m_strUserName
            Else
            Return User._Object._Username
            End If
        End Get
        Set(ByVal value As String)
            m_strUserName = value
        End Set
    End Property

    'Pinkal (01-Dec-2016) -- End


    Public Property OrderByDisplay() As String
        Get
            Return m_strOrderByDisplay
        End Get
        Set(ByVal value As String)
            m_strOrderByDisplay = value
        End Set
    End Property

    Public Property OrderByQuery() As String
        Get
            Return m_strOrderByQuery
        End Get
        Set(ByVal value As String)
            m_strOrderByQuery = value
        End Set
    End Property

    'Sohail (18 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _OrderByDescending() As Boolean
        Get
            Return m_blnOrderByDescending
        End Get
        Set(ByVal value As Boolean)
            m_blnOrderByDescending = value
        End Set
    End Property
    'Sohail (18 Feb 2012) -- End

    'Sohail (22 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _FileNameAfterExported() As String
        Get
            Return m_strFileNameAfterExported
        End Get
    End Property
    'Sohail (22 Mar 2013) -- End

#Region " Preview Text"

    Private m_strPreviewTextHeader As String = ""
    Public Property _PreviewTextHeader() As String
        Get
            Return m_strPreviewTextHeader
        End Get
        Set(ByVal value As String)
            m_strPreviewTextHeader = value
        End Set
    End Property

    Private m_strPreviewTextTitle As String = ""
    Public Property _PreviewTextTitle() As String
        Get
            Return m_strPreviewTextTitle
        End Get
        Set(ByVal value As String)
            m_strPreviewTextTitle = value
        End Set
    End Property

    Private m_strPreviewTextData As String = ""
    Public Property _PreviewTextData() As String
        Get
            Return m_strPreviewTextData
        End Get
        Set(ByVal value As String)
            m_strPreviewTextData = value
        End Set
    End Property
#End Region

    Public Sub setReportData(ByVal intReportId As Integer, ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Dim objReportAruti As clsArutiReportClass

        Try
            objReportAruti = New clsArutiReportClass
            objReportAruti._Reportunkid = intReportId
            objReportAruti.GetData()

            Select Case intLangId
                Case 0
            m_strReportName = objReportAruti._Name
                Case 1
                    m_strReportName = objReportAruti._Name1        
                Case 2
                    m_strReportName = objReportAruti._Name2
                Case Else
            End Select
            m_strReportDesc = objReportAruti._Description
            Dim objCoy As New clsCompany_Master
            objCoy._Companyunkid = intCompanyId
            m_strCompanyName = objCoy._Name
            objCoy = Nothing

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "setReportName", mstrModuleName)
        Finally
            objReportAruti = Nothing
        End Try
    End Sub

    Public Sub ReportExecute(ByVal objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass, _
                            ByVal PrintAction As enPrintAction, _
                            ByVal ExportAction As enExportAction, _
                            ByVal strExportPath As String, _
                            ByVal blnOpenExport As Boolean)

        Dim objReportViewer As ArutiReportViewer
        Dim strReportExportFileExtension As String = ""
        Dim strReportExportFile As String = ""
        Try

            If IsNothing(objRpt) Then Exit Sub
            If PrintAction = enPrintAction.None And ExportAction = enExportAction.None Then Exit Sub

            'Printing
            Select Case PrintAction
                Case enPrintAction.Preview

                    objReportViewer = New ArutiReportViewer

                    objReportViewer._Heading = _ReportName

                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    Using prd As New System.Drawing.Printing.PrintDocument
                        objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind
                    End Using

                    objReportViewer.crwArutiReportViewer.ReportSource = objRpt

                    objReportViewer.Show()

                    Exit Sub
                Case enPrintAction.Print
                    Dim prd As New System.Drawing.Printing.PrintDocument

                    objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4

                    objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize

                    'S.SANDEEP [ 20 AUG 2011 ] -- START
                    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                    'objRpt.PrintToPrinter(1, False, 0, 0)
                    objRpt.PrintToPrinter(ConfigParameter._Object._NoofPagestoPrint, False, 0, 0)
                    'S.SANDEEP [ 20 AUG 2011 ] -- END 
                    Exit Sub
            End Select


            'Exporting
            If System.IO.Directory.Exists(strExportPath) = False Then
                Dim dig As New System.Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    strExportPath = dig.SelectedPath
                Else
                    Exit Sub
                End If
            End If

            'Sandeep (27 July 2009) -- Start
            If Me._ReportName.Contains("/") = True Then
                Me._ReportName = Me._ReportName.Replace("/", "_")
            End If

            If Me._ReportName.Contains("\") Then
                Me._ReportName = Me._ReportName.Replace("\", "_")
            End If

            'Sandeep [ 17 Apr 2010 ] -- Start
            If Me._ReportName.Contains(":") Then
                Me._ReportName = Me._ReportName.Replace(":", "_")
            End If
            'Sandeep [ 17 Apr 2010 ] -- End

            'Sandeep (27 July 2009) -- End

            'Sohail (05 Aug 2020) -- Start
            'NMB Issue # : Error on acknowledgement on non disclosure screen when 2 employees click on acknowledge button at same time.
            'strReportExportFile = strExportPath & "\" & _
            '                       Me._ReportName.Replace(" ", "_") & "_" & _
            '                       Now.Date.ToString("yyyyMMdd")
            strReportExportFile = strExportPath & "\" & _
                                    Me._ReportName.Replace(" ", "_") & "_" & _
                                   Now.Date.ToString("yyyyMMdd") & Format(Now, "HHmmssfff")
            'Sohail (05 Aug 2020) -- End

            Select Case ExportAction

                'Pinkal (29-May-2018) -- Start
                'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
                Case enExportAction.Excel, enExportAction.ExcelDataOnly
                    'Pinkal (29-May-2018) -- End
                    strReportExportFileExtension = ".xls"
                Case enExportAction.HTML
                    'Sohail (22 Apr 2016) -- Start
                    'Issue - 59.1 - THE SYSTEM CANNOT FIND THE FILE SPECFIED when Open After Export option ticked and report exported in HTML.
                    'strReportExportFileExtension = ".html"
                    strReportExportFileExtension = ".htm"
                    'Sohail (22 Apr 2016) -- End
                Case enExportAction.PDF
                    strReportExportFileExtension = ".pdf"
                Case enExportAction.RichText
                    strReportExportFileExtension = ".rtf"
                Case enExportAction.Word
                    strReportExportFileExtension = ".doc"
                Case Else
                    Exit Sub
            End Select

            'Pinkal (29-May-2018) -- Start
            'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
            objRpt.ExportToDisk(IIf(ExportAction <> enExportAction.ExcelDataOnly, ExportAction, CrystalDecisions.Shared.ExportFormatType.ExcelRecord), strReportExportFile & strReportExportFileExtension)
            'Pinkal (29-May-2018) -- End

            'Sohail (29 May 2020) -- Start
            'NMB Enhancement # : After employee Acknowledges, the email sent to him will have the declaration form as an attachment.
            m_strFileNameAfterExported = strReportExportFile & strReportExportFileExtension
            'Sohail (29 May 2020) -- End

            If blnOpenExport Then
                Call Shell("explorer " & strReportExportFile & strReportExportFileExtension, vbMaximizedFocus)
            Else

            End If

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "ReportExecute", mstrModuleName)
        End Try
    End Sub

    'Sohail (08 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' ReportExecute for Export Excel Extra
    ''' </summary>
    ''' <param name="objRpt"></param>
    ''' <param name="PrintAction"></param>
    ''' <param name="ExportAction"></param>
    ''' <param name="strExportPath"></param>
    ''' <param name="blnOpenExport"></param>
    ''' <param name="objDataReader"></param>
    ''' <param name="intArrayColumnWidth">Example : Dim intArrayColumnWidth As Integer() = {100,180,100,80...}</param>
    ''' <param name="mblnShowReportHeader">Show  Report Header i.e. Company Name, Report Name, Filter Title for Export Excel Extra.</param>
    ''' <param name="mblnShowColumnHeader">Show  Column Header for Export Excel Extra.</param>
    ''' <param name="mblnShowGrandTotal">Show  Grand Total for Decimal Column for Export Excel Extra.</param>
    ''' <param name="mstrArrayGroupColumnNames">Provide Column Names for grouping</param>
    ''' <param name="mstrReportExcelTypeName">Provide Custom ReportTypeName for Export Excel Extra if ReportTypeName is other than ._ReportName.</param>
    ''' <param name="mstrReportExcelExtraTitle">Provide Report_Extra_Title for Export Excel Extra to be printed below Report_Name and with same style as Report_Name. Use | seperator for more than one line.</param>
    ''' <param name="mstrReportExcelFilterTitle">Provide Custom Report_Filter_Title for Export Excel Extra if Filter_Title is other than ._Filter_Title.</param>
    ''' <param name="mstrReportExcelSubTotalCaption">Provide Custom Report_SubTotal_Caption for Export Excel Extra if Report_SubTotal_Caption is other than "Sub Total".</param>
    ''' <param name="mstrReportExcelGrandTotalCaption">Provide Custom Report_GrandTotal_Caption for Export Excel Extra if Report_GrandTotal_Caption is other than "Grand Total".</param>
    ''' <param name="mblnReportExcelShowRecordCount">Show  Record_Count on Total Row for Export Excel Extra.</param>
    ''' <param name="marrWorksheetRowsExtraReportHeader">Provide WorksheetRowCollection to be printed before Table Column Header.</param>
    ''' <param name="marrWorksheetRowsExtraReportFooter">Provide WorksheetRowCollection to be printed after Grand Total.</param>
    ''' <param name="mdicGrandTotalExtraInfo">Provide dictionary to print extra information in any cell of Grand Total. The format is integer = cell index, object = any string or numeric value </param>
    ''' <param name="mdicSubTotalExtraInfo">Provide array dictionary to print extra information in any cell of Sub Total. The format is integer = cell index, object = any string or numeric value </param>
    ''' <param name="mblnShowSubTotal">Show Sub Total for Decimal Column for Export Excel Extra.</param>
    ''' <remarks></remarks>
    Public Sub ReportExecute(ByVal objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass, _
                            ByVal PrintAction As enPrintAction, _
                            ByVal ExportAction As enExportAction, _
                            ByVal strExportPath As String, _
                            ByVal blnOpenExport As Boolean, _
                            ByVal objDataReader As System.Data.DataTable, _
                            ByVal intArrayColumnWidth As Integer(), _
                            Optional ByVal mblnShowReportHeader As Boolean = True, _
                            Optional ByVal mblnShowColumnHeader As Boolean = True, _
                            Optional ByVal mblnShowGrandTotal As Boolean = True, _
                            Optional ByVal mstrArrayGroupColumnNames As String() = Nothing, _
                            Optional ByVal mstrReportExcelTypeName As String = "", _
                            Optional ByVal mstrReportExcelExtraTitle As String = "", _
                            Optional ByVal mstrReportExcelFilterTitle As String = "", _
                            Optional ByVal mstrReportExcelSubTotalCaption() As String = Nothing, _
                            Optional ByVal mstrReportExcelGrandTotalCaption As String = "", _
                            Optional ByVal mblnReportExcelShowRecordCount As Boolean = True, _
                            Optional ByVal marrWorksheetRowsExtraReportHeader As ArrayList = Nothing, _
                            Optional ByVal marrWorksheetRowsExtraReportFooter As ArrayList = Nothing, _
                            Optional ByVal mdicGrandTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
                            Optional ByVal mdicSubTotalExtraInfo() As Dictionary(Of Integer, Object) = Nothing, _
                            Optional ByVal mblnShowSubTotal As Boolean = True, _
                            Optional ByVal blnShowSeparateGroupingColumn As Boolean = False, _
                            Optional ByVal mstrCustomReportName As String = "", _
                            Optional ByVal mblnPrintedByInFooter As Boolean = False, _
                            Optional ByVal mblnPromptforSavedMsg As Boolean = True, _
                            Optional ByVal marrExtraCompanyDetailsReportHeader As ArrayList = Nothing, _
                            Optional ByVal marrWorksheetStyle() As WorksheetStyle = Nothing, _
                            Optional ByVal blnRegenerateROWNO As Boolean = False _
                            )
        'Sohail (09 Dec 2021) -- [blnRegenerateROWNO]
        'Sohail (27 Oct 2021) -- [marrWorksheetStyle]
        'Hemant (25 Jul 2019) -- [marrExtraCompanyDetailsReportHeader]
        'Pinkal (22-Jul-2019) -- 'Enhancement - Working on P2P Document attachment in Claim Request for NMB.[Optional ByVal mblnPromptforSavedMsg As Boolean = True]
        'Pinkal (13-Apr-2017) -- 'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To Leave Reports.[Optional ByVal mblnPrintedByInFooter As Boolean = False]
        'Pinkal (21-Mar-2017) -- 'Enhancement - Working On Budget Timesheet Report. [Optional ByVal mstrCustomReportName As String = ""]
        'Sohail (09 Apr 2015)  - [blnShowSeparateGroupingColumn]




        Dim objReportViewer As ArutiReportViewer
        Dim strReportExportFileExtension As String = ""
        Dim strReportExportFile As String = ""

        'Sohail (22 Mar 2013) -- Start
        'TRA - ENHANCEMENT
        Dim strExportFileName As String = ""
        m_strFileNameAfterExported = ""
        'Sohail (22 Mar 2013) -- End


        Try

            'If IsNothing(objRpt) Then Exit Sub
            If PrintAction = enPrintAction.None AndAlso ExportAction = enExportAction.None Then Exit Sub

            'Printing
            Select Case PrintAction
                Case enPrintAction.Preview

                    objReportViewer = New ArutiReportViewer

                    objReportViewer._Heading = _ReportName

                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    Using prd As New System.Drawing.Printing.PrintDocument
                        objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind
                    End Using

                    objReportViewer.crwArutiReportViewer.ReportSource = objRpt

                    objReportViewer.Show()

                    Exit Sub
                Case enPrintAction.Print
                    Dim prd As New System.Drawing.Printing.PrintDocument

                    objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4

                    objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize

                    objRpt.PrintToPrinter(ConfigParameter._Object._NoofPagestoPrint, False, 0, 0)
                    Exit Sub
            End Select


            'Exporting
            If System.IO.Directory.Exists(strExportPath) = False Then
                Dim dig As New System.Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    strExportPath = dig.SelectedPath
                Else
                    Exit Sub
                End If
            End If

            If Me._ReportName.Contains("/") = True Then
                Me._ReportName = Me._ReportName.Replace("/", "_")
            End If

            If Me._ReportName.Contains("\") Then
                Me._ReportName = Me._ReportName.Replace("\", "_")
            End If

            If Me._ReportName.Contains(":") Then
                Me._ReportName = Me._ReportName.Replace(":", "_")
            End If

            'Sohail (22 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'strReportExportFile = strExportPath & "\" & _
            '                        IIf(mstrReportExcelTypeName.Trim <> "", mstrReportExcelTypeName.Replace(" ", "_"), m_strReportName.Replace(" ", "_")).Replace(" ", "_") & "_" & _
            '                        Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
            strReportExportFile = strExportPath & "\"

            'Pinkal (21-Mar-2017) -- Start
            'Enhancement - Working On Budget Timesheet Report.
            If mstrCustomReportName.Trim.Length <= 0 Then
            strExportFileName = IIf(mstrReportExcelTypeName.Trim <> "", mstrReportExcelTypeName.Replace(" ", "_"), m_strReportName.Replace(" ", "_")).Replace(" ", "_") & "_" & _
                                    Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
            Else
                strExportFileName = mstrCustomReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
            End If

            'Pinkal (21-Mar-2017) -- End

            'S.SANDEEP [28-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : CCBRT, MONTHLY PAYROLL REPORT
            strExportFileName = strExportFileName.Replace("/", "_")
            strExportFileName = strExportFileName.Replace("\", "_")
            strExportFileName = strExportFileName.Replace(":", "_")
            'S.SANDEEP [28-FEB-2017] -- END


            strReportExportFile &= strExportFileName
            'Sohail (22 Mar 2013) -- End


            Select Case ExportAction
                Case enExportAction.Excel, enExportAction.ExcelDataOnly
                    strReportExportFileExtension = ".xls"
                Case enExportAction.HTML
                    'Sohail (22 Apr 2016) -- Start
                    'Issue - 59.1 - THE SYSTEM CANNOT FIND THE FILE SPECFIED when Open After Export option ticked and report exported in HTML.
                    'strReportExportFileExtension = ".html"
                    strReportExportFileExtension = ".htm"
                    'Sohail (22 Apr 2016) -- End
                Case enExportAction.PDF
                    strReportExportFileExtension = ".pdf"
                Case enExportAction.RichText
                    strReportExportFileExtension = ".rtf"
                Case enExportAction.Word
                    strReportExportFileExtension = ".doc"
                Case enExportAction.ExcelExtra
                    strReportExportFileExtension = ".xls"
                    'Hemant (24 Dec 2020) -- Start
                    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                Case enExportAction.ExcelXLSX
                    strReportExportFileExtension = ".xlsx"
                    'Hemant (24 Dec 2020) -- End
                Case Else
                    Exit Sub
            End Select


            Select Case ExportAction

                'Pinkal (29-May-2018) -- Start
                'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
                Case enExportAction.Excel, enExportAction.HTML, enExportAction.PDF, enExportAction.RichText, enExportAction.Word, enExportAction.ExcelDataOnly
                    objRpt.ExportToDisk(IIf(ExportAction <> enExportAction.ExcelDataOnly, ExportAction, CrystalDecisions.Shared.ExportFormatType.ExcelRecord), strReportExportFile & strReportExportFileExtension)
                    'Pinkal (29-May-2018) -- End

                Case enExportAction.ExcelExtra, enExportAction.ExcelXLSX
                    'Hemant (24 Dec 2020) -- [enExportAction.ExcelXLSX]
                    If objDataReader IsNot Nothing Then
                        'Sohail (02 Apr 2014) -- Start
                        'ENHANCEMENT - Multiple Worksheet in Advance Excel Report for Oman EFT. 
                        'If Export_to_Excel_Extra(strReportExportFile & strReportExportFileExtension, objDataReader, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal) = False Then
                        Dim objDataReaders(0) As DataTable
                        objDataReaders(0) = objDataReader
                        'Sohail (09 Apr 2015) -- Start
                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                        'If Export_to_Excel_Extra(strReportExportFile & strReportExportFileExtension, objDataReaders, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal) = False Then

                        'Pinkal (08-Apr-2017) -- Start
                        'Enhancement - Working on Employee Master When employee appointment date is changed,employee related dates didn't update.
                        'If Export_to_Excel_Extra(strReportExportFile & strReportExportFileExtension, objDataReaders, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal, blnShowSeparateGroupingColumn) = False Then
                        If Export_to_Excel_Extra(strReportExportFile & strReportExportFileExtension, objDataReaders, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal, blnShowSeparateGroupingColumn, mblnPrintedByInFooter, marrExtraCompanyDetailsReportHeader, marrWorksheetStyle, blnRegenerateROWNO) = False Then
                            'Sohail (09 Dec 2021) -- [blnRegenerateROWNO]
                            'Sohail (27 Oct 2021) -- [marrWorksheetStyle]
                            'Hemant (25 Jul 2019) --[marrExtraCompanyDetailsReportHeader]
                            'Pinkal (08-Apr-2017) -- End


                            'Sohail (09 Apr 2015) -- End
                            'Sohail (02 Apr 2014) -- End
                            Exit Sub
                        Else
                            'Pinkal (22-Jul-2019) -- Start
                            'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
                            If mblnPromptforSavedMsg Then eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported."), enMsgBoxStyle.Information)
                            'Pinkal (22-Jul-2019) -- End
                        End If
                    Else
                        objRpt.ExportToDisk(ExportAction, strReportExportFile & strReportExportFileExtension)
                    End If
                Case Else
                    Exit Sub
            End Select

            m_strFileNameAfterExported = strExportFileName & strReportExportFileExtension 'Sohail (22 Mar 2013)

            If blnOpenExport Then
                Process.Start(strReportExportFile & strReportExportFileExtension)
            End If

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "ReportExecute", mstrModuleName)
        End Try
    End Sub

    'Sohail (02 Apr 2014) -- Start
    'ENHANCEMENT - Multiple Worksheet in Advance Excel Report for Oman EFT.
    Public Sub ReportExecute(ByVal objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass, _
                            ByVal PrintAction As enPrintAction, _
                            ByVal ExportAction As enExportAction, _
                            ByVal strExportPath As String, _
                            ByVal blnOpenExport As Boolean, _
                            ByVal objDataReader() As System.Data.DataTable, _
                            ByVal intArrayColumnWidth As Integer(), _
                            Optional ByVal mblnShowReportHeader As Boolean = True, _
                            Optional ByVal mblnShowColumnHeader As Boolean = True, _
                            Optional ByVal mblnShowGrandTotal As Boolean = True, _
                            Optional ByVal mstrArrayGroupColumnNames As String() = Nothing, _
                            Optional ByVal mstrReportExcelTypeName As String = "", _
                            Optional ByVal mstrReportExcelExtraTitle As String = "", _
                            Optional ByVal mstrReportExcelFilterTitle As String = "", _
                            Optional ByVal mstrReportExcelSubTotalCaption() As String = Nothing, _
                            Optional ByVal mstrReportExcelGrandTotalCaption As String = "", _
                            Optional ByVal mblnReportExcelShowRecordCount As Boolean = True, _
                            Optional ByVal marrWorksheetRowsExtraReportHeader As ArrayList = Nothing, _
                            Optional ByVal marrWorksheetRowsExtraReportFooter As ArrayList = Nothing, _
                            Optional ByVal mdicGrandTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
                            Optional ByVal mdicSubTotalExtraInfo() As Dictionary(Of Integer, Object) = Nothing, _
                            Optional ByVal mblnShowSubTotal As Boolean = True, _
                            Optional ByVal mstrCustomReportName As String = "", _
                            Optional ByVal mblnPrintedByInFooter As Boolean = False, _
                            Optional ByVal mblnPromptforSavedMsg As Boolean = True, _
                            Optional ByVal marrWorksheetStyle() As WorksheetStyle = Nothing, _
                            Optional ByVal blnRegenerateROWNO As Boolean = False _
                            )
        'Sohail (09 Dec 2021) -- [blnRegenerateROWNO]
        'Sohail (27 Oct 2021) -- [marrWorksheetStyle]
        'Pinkal (22-Jul-2019) -- 'Enhancement - Working on P2P Document attachment in Claim Request for NMB.[Optional ByVal mblnPromptforSavedMsg As Boolean = True]

        'Pinkal (13-Apr-2017) -- 'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To Leave Reports.[Optional ByVal mblnPrintedByInFooter As Boolean = False]
        'Pinkal (21-Mar-2017) -- 'Enhancement - Working On Budget Timesheet Report. [Optional ByVal mstrCustomReportName As String = ""]


        Dim objReportViewer As ArutiReportViewer
        Dim strReportExportFileExtension As String = ""
        Dim strReportExportFile As String = ""

        Dim strExportFileName As String = ""
        m_strFileNameAfterExported = ""


        Try

            'If IsNothing(objRpt) Then Exit Sub
            If PrintAction = enPrintAction.None AndAlso ExportAction = enExportAction.None Then Exit Sub

            'Printing
            Select Case PrintAction
                Case enPrintAction.Preview

                    objReportViewer = New ArutiReportViewer

                    objReportViewer._Heading = _ReportName

                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    Using prd As New System.Drawing.Printing.PrintDocument
                        objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind
                    End Using

                    objReportViewer.crwArutiReportViewer.ReportSource = objRpt

                    objReportViewer.Show()

                    Exit Sub
                Case enPrintAction.Print
                    Dim prd As New System.Drawing.Printing.PrintDocument

                    objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4

                    objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize

                    objRpt.PrintToPrinter(ConfigParameter._Object._NoofPagestoPrint, False, 0, 0)
                    Exit Sub
            End Select


            'Exporting
            If System.IO.Directory.Exists(strExportPath) = False Then
                Dim dig As New System.Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    strExportPath = dig.SelectedPath
                Else
                    Exit Sub
                End If
            End If

            If Me._ReportName.Contains("/") = True Then
                Me._ReportName = Me._ReportName.Replace("/", "_")
            End If

            If Me._ReportName.Contains("\") Then
                Me._ReportName = Me._ReportName.Replace("\", "_")
            End If

            If Me._ReportName.Contains(":") Then
                Me._ReportName = Me._ReportName.Replace(":", "_")
            End If

            strReportExportFile = strExportPath & "\"

            'Pinkal (21-Mar-2017) -- Start
            'Enhancement - Working On Budget Timesheet Report.
            If mstrCustomReportName.Trim.Length <= 0 Then
            strExportFileName = IIf(mstrReportExcelTypeName.Trim <> "", mstrReportExcelTypeName.Replace(" ", "_"), m_strReportName.Replace(" ", "_")).Replace(" ", "_") & "_" & _
                                    Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
            Else
                strExportFileName = mstrCustomReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
            End If

            'Pinkal (21-Mar-2017) -- End




            strReportExportFile &= strExportFileName


            Select Case ExportAction

                'Pinkal (29-May-2018) -- Start
                'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
                Case enExportAction.Excel, enExportAction.ExcelDataOnly
                    'Pinkal (29-May-2018) -- End
                    strReportExportFileExtension = ".xls"
                Case enExportAction.HTML
                    'Sohail (22 Apr 2016) -- Start
                    'Issue - 59.1 - THE SYSTEM CANNOT FIND THE FILE SPECFIED when Open After Export option ticked and report exported in HTML.
                    'strReportExportFileExtension = ".html"
                    strReportExportFileExtension = ".htm"
                    'Sohail (22 Apr 2016) -- End
                Case enExportAction.PDF
                    strReportExportFileExtension = ".pdf"
                Case enExportAction.RichText
                    strReportExportFileExtension = ".rtf"
                Case enExportAction.Word
                    strReportExportFileExtension = ".doc"
                Case enExportAction.ExcelExtra
                    strReportExportFileExtension = ".xls"
                    'Hemant (24 Dec 2020) -- Start
                    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                Case enExportAction.ExcelXLSX
                    strReportExportFileExtension = ".xlsx"
                    'Hemant (24 Dec 2020) -- End
                Case Else
                    Exit Sub
            End Select


            Select Case ExportAction

                'Pinkal (29-May-2018) -- Start
                'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
                Case enExportAction.Excel, enExportAction.HTML, enExportAction.PDF, enExportAction.RichText, enExportAction.Word, enExportAction.ExcelDataOnly
                    objRpt.ExportToDisk(IIf(ExportAction <> enExportAction.ExcelDataOnly, ExportAction, CrystalDecisions.Shared.ExportFormatType.ExcelRecord), strReportExportFile & strReportExportFileExtension)
                    'Pinkal (29-May-2018) -- End

                Case enExportAction.ExcelExtra, enExportAction.ExcelXLSX
                    'Hemant (24 Dec 2020) -- [enExportAction.ExcelXLSX]
                    If objDataReader IsNot Nothing Then
                        'Pinkal (13-Apr-2017) -- Start
                        'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                        'If Export_to_Excel_Extra(strReportExportFile & strReportExportFileExtension, objDataReader, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal) = False Then
                        If Export_to_Excel_Extra(strReportExportFile & strReportExportFileExtension, objDataReader, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal, False, mblnPrintedByInFooter, , marrWorksheetStyle, blnRegenerateROWNO) = False Then
                            'Sohail (09 Dec 2021) -- [blnRegenerateROWNO]
                            'Sohail (27 Oct 2021) -- [marrWorksheetStyle]
                            'Pinkal (13-Apr-2017) -- End
                            Exit Sub
                        Else
                            'Pinkal (22-Jul-2019) -- Start
                            'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
                            If mblnPromptforSavedMsg Then eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported."), enMsgBoxStyle.Information)
                            'Pinkal (22-Jul-2019) -- End
                        End If
                    Else
                        objRpt.ExportToDisk(ExportAction, strReportExportFile & strReportExportFileExtension)
                    End If
                Case Else
                    Exit Sub
            End Select

            m_strFileNameAfterExported = strExportFileName & strReportExportFileExtension

            If blnOpenExport Then
                Process.Start(strReportExportFile & strReportExportFileExtension)
            End If

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "ReportExecute", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Apr 2014) -- End

    'Sohail (29 Apr 2014) -- Start
    'Enhancement - Export Excel Report with Company Logo 
    Public Sub ReportExecute(ByVal intCompanyUnkId As Integer, _
                             ByVal intReportUnkId As Integer, _
                             ByVal objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass, _
                             ByVal PrintAction As enPrintAction, _
                             ByVal ExportAction As enExportAction, _
                             ByVal strExportPath As String, _
                             ByVal blnOpenExport As Boolean, _
                             ByVal objDataReader As System.Data.DataTable, _
                             ByVal intArrayColumnWidth As Integer(), _
                             Optional ByVal mblnShowReportHeader As Boolean = True, _
                             Optional ByVal mblnShowColumnHeader As Boolean = True, _
                             Optional ByVal mblnShowGrandTotal As Boolean = True, _
                             Optional ByVal mstrArrayGroupColumnNames As String() = Nothing, _
                             Optional ByVal mstrReportExcelTypeName As String = "", _
                             Optional ByVal mstrReportExcelExtraTitle As String = "", _
                             Optional ByVal mstrReportExcelFilterTitle As String = "", _
                             Optional ByVal mstrReportExcelSubTotalCaption() As String = Nothing, _
                             Optional ByVal mstrReportExcelGrandTotalCaption As String = "", _
                             Optional ByVal mblnReportExcelShowRecordCount As Boolean = True, _
                             Optional ByVal marrWorksheetRowsExtraReportHeader As ArrayList = Nothing, _
                             Optional ByVal marrWorksheetRowsExtraReportFooter As ArrayList = Nothing, _
                             Optional ByVal mdicGrandTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
                             Optional ByVal mdicSubTotalExtraInfo() As Dictionary(Of Integer, Object) = Nothing, _
                             Optional ByVal mblnShowSubTotal As Boolean = True, _
                             Optional ByVal blnShowSeparateGroupingColumn As Boolean = False, _
                             Optional ByVal mblnSaveAsHtml As Boolean = False, _
                             Optional ByVal mstrCustomReportName As String = "", _
                             Optional ByVal mblnIsFromEmailAttachment As Boolean = False, _
                             Optional ByVal mblnPrintedByInFooter As Boolean = False, _
                             Optional ByVal mblnPromptforSavedMsg As Boolean = True, _
                             Optional ByVal marrExtraCompanyDetailsReportHeader As ArrayList = Nothing, _
                             Optional ByVal marrWorksheetStyle() As WorksheetStyle = Nothing, _
                             Optional ByVal mstrWorksheetStyle As String = "", _
                             Optional ByVal blnRegenerateROWNO As Boolean = False _
                            )
        'Sohail (09 Dec 2021) -- [blnRegenerateROWNO]
        'Sohail (27 Oct 2021) -- [marrWorksheetStyle, mstrWorksheetStyle]
        'Hemant (25 Jul 2019) -- [marrExtraCompanyDetailsReportHeader]
        'Pinkal (22-Jul-2019) --  'Enhancement - Working on P2P Document attachment in Claim Request for NMB.[Optional ByVal mblnPromptforSavedMsg As Boolean = True]
        'Pinkal (13-Apr-2017) -- 'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To Leave Reports.[Optional ByVal mblnPrintedByInFooter As Boolean = False]

        'Nilay (21 Mar 2017) -- [mblnIsFromEmailAttachment]

        'Sohail (09 Apr 2015)  - [blnShowSeparateGroupingColumn]

        'Pinkal (21-Mar-2017) -- 'Enhancement - Working On Budget Timesheet Report. [Optional ByVal mblnSaveAsHtml  As Boolean = False,   Optional ByVal mstrCustomReportName As String = ""]


        Dim objReportViewer As ArutiReportViewer
        Dim strReportExportFileExtension As String = ""
        Dim strReportExportFile As String = ""

        Dim strExportFileName As String = ""
        m_strFileNameAfterExported = ""


        Try

            'If IsNothing(objRpt) Then Exit Sub
            If PrintAction = enPrintAction.None AndAlso ExportAction = enExportAction.None Then Exit Sub

            'Printing
            Select Case PrintAction
                Case enPrintAction.Preview

                    objReportViewer = New ArutiReportViewer

                    objReportViewer._Heading = _ReportName

                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    Using prd As New System.Drawing.Printing.PrintDocument
                        objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind
                    End Using

                    objReportViewer.crwArutiReportViewer.ReportSource = objRpt

                    objReportViewer.Show()

                    Exit Sub
                Case enPrintAction.Print
                    Dim prd As New System.Drawing.Printing.PrintDocument

                    objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4

                    objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize

                    objRpt.PrintToPrinter(ConfigParameter._Object._NoofPagestoPrint, False, 0, 0)
                    Exit Sub
            End Select


            'Exporting
            If System.IO.Directory.Exists(strExportPath) = False Then
                Dim dig As New System.Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    strExportPath = dig.SelectedPath
                Else
                    Exit Sub
                End If
            End If

            If Me._ReportName.Contains("/") = True Then
                Me._ReportName = Me._ReportName.Replace("/", "_")
            End If

            If Me._ReportName.Contains("\") Then
                Me._ReportName = Me._ReportName.Replace("\", "_")
            End If

            If Me._ReportName.Contains(":") Then
                Me._ReportName = Me._ReportName.Replace(":", "_")
            End If

            strReportExportFile = strExportPath & "\"

            'Pinkal (21-Mar-2017) -- Start
            'Enhancement - Working On Budget Timesheet Report.
            If mstrCustomReportName.Trim.Length <= 0 Then
            strExportFileName = IIf(mstrReportExcelTypeName.Trim <> "", mstrReportExcelTypeName.Replace(" ", "_"), m_strReportName.Replace(" ", "_")).Replace(" ", "_") & "_" & _
                                    Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
            Else
                strExportFileName = mstrCustomReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
            End If
            'Pinkal (21-Mar-2017) -- End

            strReportExportFile &= strExportFileName


            Select Case ExportAction

                'Pinkal (29-May-2018) -- Start
                'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
                Case enExportAction.Excel, enExportAction.ExcelDataOnly
                    'Pinkal (29-May-2018) -- End
                    strReportExportFileExtension = ".xls"
                Case enExportAction.HTML
                    'Sohail (22 Apr 2016) -- Start
                    'Issue - 59.1 - THE SYSTEM CANNOT FIND THE FILE SPECFIED when Open After Export option ticked and report exported in HTML.
                    'strReportExportFileExtension = ".html"
                    strReportExportFileExtension = ".htm"
                    'Sohail (22 Apr 2016) -- End
                Case enExportAction.PDF
                    strReportExportFileExtension = ".pdf"
                Case enExportAction.RichText
                    strReportExportFileExtension = ".rtf"
                Case enExportAction.Word
                    strReportExportFileExtension = ".doc"
                Case enExportAction.ExcelExtra
                    strReportExportFileExtension = ".xls"
                Case enExportAction.ExcelHTML
                    'Pinkal (21-Mar-2017) -- Start
                    'Enhancement - Working On Budget Timesheet Report.                    
                    If mblnSaveAsHtml Then strReportExportFileExtension = ".html" Else strReportExportFileExtension = ".xls"
                    'Pinkal (21-Mar-2017) -- End
                    'Hemant (24 Dec 2020) -- Start
                    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                Case enExportAction.ExcelXLSX
                    strReportExportFileExtension = ".xlsx"
                    'Hemant (24 Dec 2020) -- End
                Case Else
                    Exit Sub
            End Select


            Select Case ExportAction

                'Pinkal (29-May-2018) -- Start
                'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
                Case enExportAction.Excel, enExportAction.HTML, enExportAction.PDF, enExportAction.RichText, enExportAction.Word, enExportAction.ExcelDataOnly
                    objRpt.ExportToDisk(IIf(ExportAction <> enExportAction.ExcelDataOnly, ExportAction, CrystalDecisions.Shared.ExportFormatType.ExcelRecord), strReportExportFile & strReportExportFileExtension)
                    'Pinkal (29-May-2018) -- End

                Case enExportAction.ExcelExtra, enExportAction.ExcelXLSX
                    'Hemant (24 Dec 2020) -- [enExportAction.ExcelXLSX]
                    If objDataReader IsNot Nothing Then
                        Dim objDataReaders(0) As DataTable
                        objDataReaders(0) = objDataReader
                        'Sohail (09 Apr 2015) -- Start
                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                        'If Export_to_Excel_Extra(strReportExportFile & strReportExportFileExtension, objDataReaders, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal) = False Then

                        'Pinkal (13-Apr-2017) -- Start
                        'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                        'If Export_to_Excel_Extra(strReportExportFile & strReportExportFileExtension, objDataReaders, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal, blnShowSeparateGroupingColumn) = False Then
                        If Export_to_Excel_Extra(strReportExportFile & strReportExportFileExtension, objDataReaders, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal, blnShowSeparateGroupingColumn, mblnPrintedByInFooter, , marrWorksheetStyle, blnRegenerateROWNO) = False Then
                            'Sohail (09 Dec 2021) -- [blnRegenerateROWNO]
                            'Sohail (27 Oct 2021) -- [marrWorksheetStyle]
                            'Pinkal (13-Apr-2017) -- End

                            'Sohail (09 Apr 2015) -- End
                            Exit Sub
                        Else
                            'Nilay (21 Mar 2017) -- Start
                            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported."), enMsgBoxStyle.Information)
                            If mblnIsFromEmailAttachment = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported."), enMsgBoxStyle.Information)
                        End If
                            'Nilay (21 Mar 2017) -- End
                        End If
                    Else
                        objRpt.ExportToDisk(ExportAction, strReportExportFile & strReportExportFileExtension)
                    End If
                Case enExportAction.ExcelHTML
                    'Sohail (09 Apr 2015) -- Start
                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                    'If Export_to_Excel_HTML(intCompanyUnkId, intReportUnkId, strReportExportFile & strReportExportFileExtension, objDataReader, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal) = False Then

                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    'If Export_to_Excel_HTML(intCompanyUnkId, intReportUnkId, strReportExportFile & strReportExportFileExtension, objDataReader, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal, blnShowSeparateGroupingColumn) = False Then
                    If Export_to_Excel_HTML(intCompanyUnkId, intReportUnkId, strReportExportFile & strReportExportFileExtension, objDataReader, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal, blnShowSeparateGroupingColumn, mblnPrintedByInFooter, marrExtraCompanyDetailsReportHeader, mstrWorksheetStyle) = False Then
                        'Sohail (27 Oct 2021) -- [mstrWorksheetStyle]
                        'Hemant (25 Jul 2019) -- [marrExtraCompanyDetailsReportHeader]
                        'Pinkal (13-Apr-2017) -- End

                        'Sohail (09 Apr 2015) -- End
                        Exit Sub
                    Else

                        'Pinkal (22-Jul-2019) -- Start
                        'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
                        If mblnIsFromEmailAttachment = False AndAlso mblnPromptforSavedMsg Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported."), enMsgBoxStyle.Information)
                    End If
                        'Pinkal (22-Jul-2019) -- End
                    End If

                Case Else
                    Exit Sub
            End Select

            m_strFileNameAfterExported = strExportFileName & strReportExportFileExtension

            If blnOpenExport Then
                Process.Start(strReportExportFile & strReportExportFileExtension)
            End If

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "ReportExecute", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Apr 2014) -- End

    'Sohail (30 Dec 2015) -- Start
    'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
    Public Sub ReportExecute(ByVal objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass, _
                            ByVal PrintAction As enPrintAction, _
                            ByVal ExportAction As enExportAction, _
                            ByVal strExportPath As String, _
                            ByVal blnOpenExport As Boolean, _
                            ByVal objDataReader As System.Data.DataTable, _
                            ByVal objDataReader1 As System.Data.DataTable, _
                            ByVal intArrayColumnWidth As Integer(), _
                            Optional ByVal mblnShowReportHeader As Boolean = True, _
                            Optional ByVal mblnShowColumnHeader As Boolean = True, _
                            Optional ByVal mblnShowGrandTotal As Boolean = True, _
                            Optional ByVal mstrArrayGroupColumnNames As String() = Nothing, _
                            Optional ByVal mstrArrayGroupColumnNamesTableB As String() = Nothing, _
                            Optional ByVal mstrReportExcelTypeName As String = "", _
                            Optional ByVal mstrReportExcelExtraTitle As String = "", _
                            Optional ByVal mstrReportExcelFilterTitle As String = "", _
                            Optional ByVal mstrReportExcelSubTotalCaption() As String = Nothing, _
                            Optional ByVal mstrReportExcelGrandTotalCaption As String = "", _
                            Optional ByVal mblnReportExcelShowRecordCount As Boolean = True, _
                            Optional ByVal marrWorksheetRowsExtraReportHeader As ArrayList = Nothing, _
                            Optional ByVal marrWorksheetRowsExtraReportFooter As ArrayList = Nothing, _
                            Optional ByVal mdicGrandTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
                            Optional ByVal mdicSubTotalExtraInfo() As Dictionary(Of Integer, Object) = Nothing, _
                            Optional ByVal mblnShowSubTotal As Boolean = True, _
                            Optional ByVal blnShowSeparateGroupingColumn As Boolean = False, _
                            Optional ByVal mstrCustomReportName As String = "", _
                            Optional ByVal mblnPrintedByInFooter As Boolean = False, _
                            Optional ByVal mblnPromptforSavedMsg As Boolean = True)


        'Pinkal (22-Jul-2019) -- Enhancement - Working on P2P Document attachment in Claim Request for NMB.[Optional ByVal mblnPromptforSavedMsg As Boolean = True]

        'Pinkal (13-Apr-2017) -- 'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To Leave Reports.[Optional ByVal mblnPrintedByInFooter As Boolean = False]
        'Pinkal (21-Mar-2017) -- 'Enhancement - Working On Budget Timesheet Report. [Optional ByVal mstrCustomReportName As String = ""]

        Dim objReportViewer As ArutiReportViewer
        Dim strReportExportFileExtension As String = ""
        Dim strReportExportFile As String = ""

        Dim strExportFileName As String = ""
        m_strFileNameAfterExported = ""


        Try

            If PrintAction = enPrintAction.None AndAlso ExportAction = enExportAction.None Then Exit Sub

            'Printing
            Select Case PrintAction
                Case enPrintAction.Preview

                    objReportViewer = New ArutiReportViewer

                    objReportViewer._Heading = _ReportName

                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    Using prd As New System.Drawing.Printing.PrintDocument
                        objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind
                    End Using

                    objReportViewer.crwArutiReportViewer.ReportSource = objRpt

                    objReportViewer.Show()

                    Exit Sub
                Case enPrintAction.Print
                    Dim prd As New System.Drawing.Printing.PrintDocument

                    objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                    'objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4

                    objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize

                    objRpt.PrintToPrinter(ConfigParameter._Object._NoofPagestoPrint, False, 0, 0)
                    Exit Sub
            End Select


            'Exporting
            If System.IO.Directory.Exists(strExportPath) = False Then
                Dim dig As New System.Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    strExportPath = dig.SelectedPath
                Else
                    Exit Sub
                End If
            End If

            If Me._ReportName.Contains("/") = True Then
                Me._ReportName = Me._ReportName.Replace("/", "_")
            End If

            If Me._ReportName.Contains("\") Then
                Me._ReportName = Me._ReportName.Replace("\", "_")
            End If

            If Me._ReportName.Contains(":") Then
                Me._ReportName = Me._ReportName.Replace(":", "_")
            End If


            strReportExportFile = strExportPath & "\"

            'Pinkal (21-Mar-2017) -- Start
            'Enhancement - Working On Budget Timesheet Report.

            If mstrCustomReportName.Trim.Length <= 0 Then
            strExportFileName = IIf(mstrReportExcelTypeName.Trim <> "", mstrReportExcelTypeName.Replace(" ", "_"), m_strReportName.Replace(" ", "_")).Replace(" ", "_") & "_" & _
                                    Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
            Else
                strExportFileName = mstrCustomReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
            End If

            'Pinkal (21-Mar-2017) -- End



            strReportExportFile &= strExportFileName


            Select Case ExportAction

                'Pinkal (29-May-2018) -- Start
                'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
                Case enExportAction.Excel, enExportAction.ExcelDataOnly
                    'Pinkal (29-May-2018) -- End
                    strReportExportFileExtension = ".xls"
                Case enExportAction.HTML
                    'Sohail (22 Apr 2016) -- Start
                    'Issue - 59.1 - THE SYSTEM CANNOT FIND THE FILE SPECFIED when Open After Export option ticked and report exported in HTML.
                    'strReportExportFileExtension = ".html"
                    strReportExportFileExtension = ".htm"
                    'Sohail (22 Apr 2016) -- End
                Case enExportAction.PDF
                    strReportExportFileExtension = ".pdf"
                Case enExportAction.RichText
                    strReportExportFileExtension = ".rtf"
                Case enExportAction.Word
                    strReportExportFileExtension = ".doc"
                Case enExportAction.ExcelExtra
                    strReportExportFileExtension = ".xls"
                    'Hemant (24 Dec 2020) -- Start
                    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                Case enExportAction.ExcelXLSX
                    strReportExportFileExtension = ".xlsx"
                    'Hemant (24 Dec 2020) -- End
                Case Else
                    Exit Sub
            End Select


            Select Case ExportAction

                'Pinkal (29-May-2018) -- Start
                'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
                Case enExportAction.Excel, enExportAction.HTML, enExportAction.PDF, enExportAction.RichText, enExportAction.Word, enExportAction.ExcelDataOnly
                    objRpt.ExportToDisk(IIf(ExportAction <> enExportAction.ExcelDataOnly, ExportAction, CrystalDecisions.Shared.ExportFormatType.ExcelRecord), strReportExportFile & strReportExportFileExtension)
                    'Pinkal (29-May-2018) -- End

                Case enExportAction.ExcelExtra, enExportAction.ExcelXLSX
                    'Hemant (24 Dec 2020) -- [enExportAction.ExcelXLSX]
                    If objDataReader IsNot Nothing Then
                        Dim objDataReaders(0) As DataTable
                        objDataReaders(0) = objDataReader
                        If Export_to_Excel_Extra_SalaryReconciliation(strReportExportFile & strReportExportFileExtension, objDataReader, objDataReader1, intArrayColumnWidth, mblnShowReportHeader, mblnShowColumnHeader, mblnShowGrandTotal, mstrArrayGroupColumnNames, mstrArrayGroupColumnNamesTableB, mstrReportExcelTypeName, mstrReportExcelExtraTitle, mstrReportExcelFilterTitle, mstrReportExcelSubTotalCaption, mstrReportExcelGrandTotalCaption, mblnReportExcelShowRecordCount, marrWorksheetRowsExtraReportHeader, marrWorksheetRowsExtraReportFooter, mdicGrandTotalExtraInfo, mdicSubTotalExtraInfo, mblnShowSubTotal, blnShowSeparateGroupingColumn) = False Then
                            Exit Sub
                        Else
                            'Pinkal (22-Jul-2019) -- Start
                            'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
                            If mblnPromptforSavedMsg Then eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported."), enMsgBoxStyle.Information)
                            'Pinkal (22-Jul-2019) -- End
                        End If
                    Else
                        objRpt.ExportToDisk(ExportAction, strReportExportFile & strReportExportFileExtension)
                    End If
                Case Else
                    Exit Sub
            End Select

            m_strFileNameAfterExported = strExportFileName & strReportExportFileExtension

            If blnOpenExport Then
                Process.Start(strReportExportFile & strReportExportFileExtension)
            End If

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "ReportExecute", mstrModuleName)
        End Try
    End Sub
    'Sohail (30 Dec 2015) -- End

#Region " Export_to_Excel_Extra b4 giving more than 2 dynamic group column "
    'Private Function Export_to_Excel_Extra(ByVal strExportPath As String, _
    '                                      ByVal objDataReader As System.Data.DataTable, _
    '                                      ByVal intArrayColumnWidth As Integer(), _
    '                                      Optional ByVal mblnShowReportHeader As Boolean = True, _
    '                                      Optional ByVal mblnShowColumnHeader As Boolean = True, _
    '                                      Optional ByVal mblnShowGrandTotal As Boolean = True, _
    '                                      Optional ByVal mstrArrayGroupColumnNames As String() = Nothing, _
    '                                      Optional ByVal mstrReportExcelTypeName As String = "", _
    '                                      Optional ByVal mstrReportExcelExtraTitle As String = "", _
    '                                      Optional ByVal mstrReportExcelFilterTitle As String = "", _
    '                                      Optional ByVal mstrReportExcelSubTotalCaption As String = "", _
    '                                      Optional ByVal mstrReportExcelGrandTotalCaption As String = "", _
    '                                      Optional ByVal mblnReportExcelShowRecordCount As Boolean = True, _
    '                                      Optional ByVal marrWorksheetRowsExtraReportHeader As ArrayList = Nothing, _
    '                                      Optional ByVal marrWorksheetRowsExtraReportFooter As ArrayList = Nothing, _
    '                                      Optional ByVal mdicGrandTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
    '                                      Optional ByVal mdicSubTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
    '                                      Optional ByVal mdicSubGroupTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
    '                                      Optional ByVal mblnShowSubTotal As Boolean = True, _
    '                                      Optional ByVal mstrReportExcelSubGroupTotalCaption As String = "") As Boolean

    '    Dim blnIsDecimalfield As Boolean = False
    '    Dim intColCount As Integer = objDataReader.Columns.Count
    '    Dim intRowCount As Integer = objDataReader.Rows.Count

    '    Try
    '        'Add a workbook
    '        Dim book As New ExcelWriter.Workbook

    '        ' Specify which Sheet should be opened and the size of window by default
    '        book.ExcelWorkbook.ActiveSheetIndex = 1
    '        book.ExcelWorkbook.WindowTopX = 100
    '        book.ExcelWorkbook.WindowTopY = 200
    '        book.ExcelWorkbook.WindowHeight = 7000
    '        book.ExcelWorkbook.WindowWidth = 8000

    '        ' Some optional properties of the Document
    '        book.Properties.Author = "Aruti"
    '        book.Properties.Title = "Excel Export"
    '        book.Properties.Created = DateTime.Now

    '        ' Add styles for header of the Workbook
    '        Dim style As WorksheetStyle = book.Styles.Add("HeaderStyle")
    '        With style
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 9
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Center
    '            .Alignment.WrapText = True
    '            .Font.Color = "White"
    '            .Interior.Color = "Blue"
    '            .Interior.Pattern = StyleInteriorPattern.DiagCross
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '        End With

    '        Dim styleFtr As WorksheetStyle = book.Styles.Add("FooterStyle")
    '        With styleFtr
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.WrapText = True
    '            .Font.Color = "White"
    '            .Interior.Color = "Blue"
    '            .Interior.Pattern = StyleInteriorPattern.DiagCross
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '        End With

    '        Dim styleFtrN As WorksheetStyle = book.Styles.Add("FooterStyleN")
    '        With styleFtrN
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Right
    '            .Alignment.WrapText = True
    '            .Font.Color = "White"
    '            .Interior.Color = "Blue"
    '            .Interior.Pattern = StyleInteriorPattern.DiagCross
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '            .NumberFormat = GUI.fmtCurrency
    '        End With

    '        Dim styleFilterTitle As WorksheetStyle = book.Styles.Add("FilterTitle") 'Without Border
    '        With styleFilterTitle
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Color = "#000000"
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .NumberFormat = "@"
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '        End With

    '        'Add styles to the workbook 
    '        Dim s8 As WorksheetStyle = book.Styles.Add("s8")
    '        With s8
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Color = "#000000"
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '            .NumberFormat = "@"
    '        End With

    '        Dim s8w As WorksheetStyle = book.Styles.Add("s8w") 'Without Border
    '        With s8w
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Color = "#000000"
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .NumberFormat = "@"
    '        End With

    '        Dim s8b As WorksheetStyle = book.Styles.Add("s8b")
    '        With s8b
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '            .NumberFormat = "@"
    '        End With

    '        Dim s8bc As WorksheetStyle = book.Styles.Add("s8bc")
    '        With s8bc
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Center
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '            .NumberFormat = "@"
    '        End With

    '        Dim s8bcf As WorksheetStyle = book.Styles.Add("s8bcf")
    '        With s8bcf
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Bold = True
    '            .Font.Color = "#000000"
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Center
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '            .Interior.Color = "#C5D9F1"
    '            .Interior.Pattern = StyleInteriorPattern.Gray0625
    '            .NumberFormat = "@"
    '        End With

    '        Dim s8bw As WorksheetStyle = book.Styles.Add("s8bw") 'Without Borders
    '        With s8bw
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .NumberFormat = "@"
    '        End With

    '        Dim n8 As WorksheetStyle = book.Styles.Add("n8")
    '        With n8
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Color = "#000000"
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Right
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '            '.NumberFormat = "General Number"
    '            .NumberFormat = GUI.fmtCurrency
    '        End With

    '        Dim n8b As WorksheetStyle = book.Styles.Add("n8b")
    '        With n8b
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 8
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Right
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '            '.NumberFormat = "General Number"
    '            .NumberFormat = GUI.fmtCurrency
    '        End With

    '        Dim s10b As WorksheetStyle = book.Styles.Add("s10b")
    '        With s10b
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 10
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '            .NumberFormat = "@"
    '        End With

    '        Dim s10bw As WorksheetStyle = book.Styles.Add("s10bw") 'Without Borders
    '        With s10bw
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 10
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .NumberFormat = "@"
    '        End With



    '        Dim s9w As WorksheetStyle = book.Styles.Add("s9w") 'Without Borders
    '        With s9w
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 9
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .NumberFormat = "@"
    '        End With

    '        Dim s9bw As WorksheetStyle = book.Styles.Add("s9bw") 'Without Borders
    '        With s9bw
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 9
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Left
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .NumberFormat = "@"
    '        End With

    '        Dim s9wc As WorksheetStyle = book.Styles.Add("s9wc") 'Without Borders Center alignment
    '        With s9wc
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 9
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Center
    '            .Alignment.Vertical = StyleVerticalAlignment.Top
    '            .Alignment.WrapText = True
    '            .NumberFormat = "@"
    '        End With

    '        Dim s12b As WorksheetStyle = book.Styles.Add("s12b")
    '        With s12b
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 12
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Center
    '            .Alignment.WrapText = True
    '            .Font.Color = "Purple"
    '            .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
    '            .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
    '        End With

    '        Dim s12bw As WorksheetStyle = book.Styles.Add("s12bw")
    '        With s12bw
    '            .Font.FontName = "Tahoma"
    '            .Font.Size = 12
    '            .Font.Bold = True
    '            .Alignment.Horizontal = StyleHorizontalAlignment.Center
    '            .Alignment.WrapText = True
    '            .Font.Color = "Purple"
    '        End With

    '        ' Add a Worksheet with some data
    '        Dim sheet As Worksheet = book.Worksheets.Add(IIf(mstrReportExcelTypeName.Trim <> "", Left(mstrReportExcelTypeName.Replace(" ", "_"), 31), Left(m_strReportName.Replace(" ", "_"), 31)))

    '        '' we can optionally set some column settings
    '        If intArrayColumnWidth IsNot Nothing Then
    '            For Each item In intArrayColumnWidth
    '                sheet.Table.Columns.Add(New WorksheetColumn(item))
    '            Next
    '        End If


    '        'Add row with some properties
    '        Dim row As WorksheetRow
    '        Dim wcHeader As WorksheetCell = Nothing

    '        If mblnShowReportHeader = True Then
    '            row = sheet.Table.Rows.Add
    '            row.AutoFitHeight = True
    '            row.Cells.Add(New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Prepared By :"), "s10bw"))
    '            row.Cells.Add(New WorksheetCell(User._Object._Username, "s10bw"))
    '            wcHeader = New WorksheetCell(m_strCompanyName, "s12bw")
    '            row.Cells.Add(wcHeader)
    '            wcHeader.MergeAcross = intColCount - 3

    '            row = sheet.Table.Rows.Add
    '            row.AutoFitHeight = True
    '            row.Cells.Add(New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Date :"), "s10bw"))
    '            row.Cells.Add(New WorksheetCell(Now.Date.ToShortDateString, "s10bw"))
    '            '*** Report Name
    '            wcHeader = New WorksheetCell(IIf(mstrReportExcelTypeName.Trim = "", m_strReportName, mstrReportExcelTypeName), "s12bw")
    '            row.Cells.Add(wcHeader)
    '            wcHeader.MergeAcross = intColCount - 3

    '            '*** Extra Title
    '            If mstrReportExcelExtraTitle.Trim <> "" Then
    '                Dim arrTitle() As String = mstrReportExcelExtraTitle.Split("|")
    '                For Each strTitle As String In arrTitle
    '                    row = sheet.Table.Rows.Add
    '                    row.AutoFitHeight = True
    '                    row.Cells.Add(New WorksheetCell("", "s10bw"))
    '                    row.Cells.Add(New WorksheetCell("", "s10bw"))
    '                    wcHeader = New WorksheetCell(strTitle, "s12bw")
    '                    row.Cells.Add(wcHeader)
    '                    wcHeader.MergeAcross = intColCount - 3
    '                Next
    '            End If

    '            '*** Filter Title
    '            row = sheet.Table.Rows.Add
    '            row.AutoFitHeight = True
    '            If (mstrReportExcelFilterTitle = "" AndAlso m_strFilterTitle = "") OrElse mstrReportExcelFilterTitle = " " Then
    '                wcHeader = New WorksheetCell("", "s8w")
    '            ElseIf mstrReportExcelFilterTitle.Trim <> "" Then
    '                wcHeader = New WorksheetCell(mstrReportExcelFilterTitle, "FilterTitle")
    '            Else
    '                wcHeader = New WorksheetCell(m_strFilterTitle, "FilterTitle")
    '            End If
    '            row.Cells.Add(wcHeader)
    '            wcHeader.MergeAcross = intColCount - 1


    '        End If

    '        '*** Extra Report Header
    '        If marrWorksheetRowsExtraReportHeader IsNot Nothing AndAlso marrWorksheetRowsExtraReportHeader.Count > 0 Then
    '            For Each wrow In marrWorksheetRowsExtraReportHeader
    '                wrow.AutoFitHeight = True
    '                sheet.Table.Rows.Add(wrow)
    '            Next
    '        Else

    '            '*** Blank Row
    '            row = sheet.Table.Rows.Add
    '            wcHeader = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcHeader)
    '            wcHeader.MergeAcross = intColCount - 1
    '        End If

    '        Dim blnGrpColumn As Boolean = False

    '        If mblnShowColumnHeader = True Then

    '            row = sheet.Table.Rows.Add
    '            row.AutoFitHeight = True

    '            'Add header text for the columns  
    '            For Each col As DataColumn In objDataReader.Columns
    '                If blnGrpColumn = False Then
    '                    wcHeader = New WorksheetCell(col.Caption, "HeaderStyle")
    '                    row.Cells.Add(wcHeader)

    '                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                        For Each strCol As String In mstrArrayGroupColumnNames
    '                            If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                    wcHeader.MergeAcross = 1
    '                                    blnGrpColumn = True
    '                                End If
    '                            End If
    '                        Next
    '                    End If
    '                Else
    '                    blnGrpColumn = False

    '                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                        For Each strCol As String In mstrArrayGroupColumnNames
    '                            If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                    wcHeader.MergeAcross = 2
    '                                    blnGrpColumn = True
    '                                End If
    '                            End If
    '                        Next
    '                    End If
    '                End If

    '            Next


    '            'Freeze the top rows
    '            sheet.Options.FreezePanes = True
    '            sheet.Options.SplitHorizontal = sheet.Table.Rows.Count
    '            sheet.Options.TopRowBottomPane = sheet.Table.Rows.Count
    '            sheet.Options.ActivePane = 2
    '        End If

    '        'Loop through each row in datatable  
    '        Dim dtrrow As DataRow
    '        Dim wc As WorksheetCell = Nothing
    '        Dim PrevGrp As String = ""
    '        Dim CurrGrp As String = ""
    '        Dim PrevGrp2 As String = ""
    '        Dim CurrGrp2 As String = ""
    '        Dim j As Integer = 0
    '        Dim k As Integer = 0
    '        Dim strGroupCells As String = ""
    '        Dim strSubGroupCells As String = ""
    '        Dim dicGroupCells As New Dictionary(Of Integer, String)
    '        Dim dicSubGroupCells As New Dictionary(Of Integer, String)
    '        For i = 0 To intRowCount - 1
    '            dtrrow = objDataReader.Rows(i)

    '            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then

    '                If objDataReader.Columns.Contains(mstrArrayGroupColumnNames(0).Trim) = True Then
    '                    CurrGrp = dtrrow(mstrArrayGroupColumnNames(0).Trim).ToString
    '                    If mstrArrayGroupColumnNames.Length > 1 Then
    '                        CurrGrp2 = dtrrow(mstrArrayGroupColumnNames(1).Trim).ToString
    '                    End If

    '                    j += 1
    '                    k += 1

    '                    '*** Group Footer
    '                    If mblnShowSubTotal = True AndAlso (PrevGrp <> CurrGrp OrElse PrevGrp2 <> CurrGrp2) AndAlso blnIsDecimalfield = True AndAlso i > 0 Then
    '                        row = sheet.Table.Rows.Add
    '                        row.AutoFitHeight = True

    '                        blnGrpColumn = False

    '                        For Each col As DataColumn In objDataReader.Columns
    '                            If blnGrpColumn = False Then

    '                                If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then

    '                                    If PrevGrp2 <> CurrGrp2 AndAlso mstrArrayGroupColumnNames.Length > 1 Then
    '                                        wc = New WorksheetCell()
    '                                        wc.StyleID = "n8b"
    '                                        wc.Data.Type = DataType.Number

    '                                        wc.Formula = "=SUM(R[-" & (j - 1) & "]C:R[-1]C)"
    '                                        row.Cells.Add(wc)

    '                                        If dicSubGroupCells.ContainsKey(col.Ordinal) = False Then
    '                                            dicSubGroupCells.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
    '                                        Else
    '                                            dicSubGroupCells.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
    '                                        End If
    '                                    Else

    '                                        wc = New WorksheetCell()
    '                                        wc.StyleID = "n8b"
    '                                        wc.Data.Type = DataType.Number
    '                                        If PrevGrp <> CurrGrp AndAlso mstrArrayGroupColumnNames.Length = 1 Then
    '                                            wc.Formula = "=SUM(R[-" & (j - 1) & "]C:R[-1]C)"

    '                                            If dicGroupCells.ContainsKey(col.Ordinal) = False Then
    '                                                dicGroupCells.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
    '                                            Else
    '                                                dicGroupCells.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
    '                                            End If

    '                                        ElseIf PrevGrp <> CurrGrp AndAlso mstrArrayGroupColumnNames.Length > 1 Then
    '                                            wc.Formula = "=" & dicGroupCells.Item(col.Ordinal)

    '                                            If dicGroupCells.ContainsKey(col.Ordinal) = False Then
    '                                                dicGroupCells.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
    '                                            Else
    '                                                dicGroupCells.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
    '                                            End If

    '                                        Else
    '                                            Continue For
    '                                        End If

    '                                        row.Cells.Add(wc)
    '                                    End If

    '                                Else
    '                                    If PrevGrp2 <> CurrGrp2 AndAlso mstrArrayGroupColumnNames.Length > 1 Then
    '                                        If col.Ordinal = 0 Then
    '                                            'wc = New WorksheetCell(IIf(mstrReportExcelSubTotalCaption = "", Language.getMessage(mstrModuleName, 10, "Sub Group Total"), mstrReportExcelSubTotalCaption), DataType.String, "s8b")
    '                                            wc = New WorksheetCell(IIf(mstrReportExcelSubGroupTotalCaption = "", Language.getMessage(mstrModuleName, 10, "Sub Group Total"), mstrReportExcelSubGroupTotalCaption), DataType.String, "s8b")
    '                                        ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = 1 Then
    '                                            wc = New WorksheetCell("( " & (j - 1).ToString & " )", DataType.String, "s8b")
    '                                        Else
    '                                            wc = New WorksheetCell("", DataType.String, "s8b")
    '                                        End If
    '                                        row.Cells.Add(wc)

    '                                    End If

    '                                    If PrevGrp <> CurrGrp AndAlso mstrArrayGroupColumnNames.Length = 1 Then
    '                                        If col.Ordinal = 0 Then
    '                                            wc = New WorksheetCell(IIf(mstrReportExcelSubTotalCaption = "", Language.getMessage(mstrModuleName, 11, "Sub Total"), mstrReportExcelSubTotalCaption), DataType.String, "s8b")
    '                                        ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = 1 Then
    '                                            wc = New WorksheetCell("( " & (j - 1).ToString & " )", DataType.String, "s8b")
    '                                        Else
    '                                            wc = New WorksheetCell("", DataType.String, "s8b")
    '                                        End If
    '                                        row.Cells.Add(wc)
    '                                    End If
    '                                End If

    '                                If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                                    For Each strCol As String In mstrArrayGroupColumnNames
    '                                        If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                            If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                                wc.MergeAcross = 1
    '                                                blnGrpColumn = True
    '                                            End If
    '                                        End If
    '                                    Next
    '                                End If

    '                            Else

    '                                blnGrpColumn = False

    '                                If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                                    For Each strCol As String In mstrArrayGroupColumnNames
    '                                        If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                            If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                                wc.MergeAcross = 2
    '                                                blnGrpColumn = True
    '                                            End If
    '                                        End If
    '                                    Next
    '                                End If
    '                            End If
    '                        Next

    '                        If PrevGrp2 <> CurrGrp2 AndAlso mstrArrayGroupColumnNames.Length > 1 Then
    '                            If mdicSubGroupTotalExtraInfo IsNot Nothing AndAlso mdicSubGroupTotalExtraInfo.Count > 0 Then
    '                                For Each pair In mdicSubGroupTotalExtraInfo
    '                                    wc = New WorksheetCell(pair.Value, "s8b")
    '                                    sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
    '                                Next
    '                            End If
    '                        End If

    '                        If PrevGrp <> CurrGrp AndAlso mstrArrayGroupColumnNames.Length = 1 Then
    '                            If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Count > 0 Then
    '                                For Each pair In mdicSubTotalExtraInfo
    '                                    wc = New WorksheetCell(pair.Value, "s8b")
    '                                    sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
    '                                Next
    '                            End If
    '                        End If


    '                        If dicSubGroupCells.Count > 0 AndAlso mstrArrayGroupColumnNames.Length > 1 Then

    '                            If PrevGrp <> CurrGrp Then
    '                                row = sheet.Table.Rows.Add
    '                                row.AutoFitHeight = True

    '                                blnGrpColumn = False

    '                                For Each col As DataColumn In objDataReader.Columns
    '                                    If blnGrpColumn = False Then


    '                                        If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then
    '                                            wc = New WorksheetCell()
    '                                            wc.StyleID = "n8b"
    '                                            wc.Data.Type = DataType.Number
    '                                            wc.Formula = "=" & dicSubGroupCells.Item(col.Ordinal)

    '                                            If dicGroupCells.ContainsKey(col.Ordinal) = False Then
    '                                                dicGroupCells.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
    '                                            Else
    '                                                dicGroupCells.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
    '                                            End If

    '                                        Else
    '                                            If col.Ordinal = 0 Then
    '                                                wc = New WorksheetCell(IIf(mstrReportExcelSubTotalCaption = "", Language.getMessage(mstrModuleName, 11, "Sub Total"), mstrReportExcelSubTotalCaption), DataType.String, "s8b")
    '                                            ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = 1 Then
    '                                                wc = New WorksheetCell("( " & (k - 1).ToString & " )", DataType.String, "s8b")

    '                                                'k = 1
    '                                            Else
    '                                                wc = New WorksheetCell("", DataType.String, "s8b")
    '                                            End If
    '                                        End If
    '                                        row.Cells.Add(wc)

    '                                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                                            For Each strCol As String In mstrArrayGroupColumnNames
    '                                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                                        wc.MergeAcross = 1
    '                                                        blnGrpColumn = True
    '                                                    End If
    '                                                End If
    '                                            Next
    '                                        End If

    '                                    Else
    '                                        blnGrpColumn = False

    '                                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                                            For Each strCol As String In mstrArrayGroupColumnNames
    '                                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                                        wc.MergeAcross = 2
    '                                                        blnGrpColumn = True
    '                                                    End If
    '                                                End If
    '                                            Next
    '                                        End If
    '                                    End If
    '                                Next

    '                                dicSubGroupCells.Clear()

    '                                If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Count > 0 Then
    '                                    For Each pair In mdicSubTotalExtraInfo
    '                                        wc = New WorksheetCell(pair.Value, "s8b")
    '                                        sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
    '                                    Next
    '                                End If
    '                            End If
    '                        End If

    '                        j = 1

    '                    End If

    '                    '*** Group Header
    '                    If PrevGrp <> CurrGrp Then
    '                        row = sheet.Table.Rows.Add
    '                        row.AutoFitHeight = True

    '                        If mstrArrayGroupColumnNames.Length = 1 Then
    '                            'Sohail (17 Jan 2013) -- Start
    '                            'TRA - ENHANCEMENT
    '                            'wcHeader = New WorksheetCell(objDataReader.Columns(mstrArrayGroupColumnNames(0).Trim).Caption & " : " & CurrGrp, "s8b")
    '                            If objDataReader.Columns(mstrArrayGroupColumnNames(0).Trim).Caption = "" Then
    '                                wcHeader = New WorksheetCell(CurrGrp, "s8b")
    '                            Else
    '                                wcHeader = New WorksheetCell(objDataReader.Columns(mstrArrayGroupColumnNames(0).Trim).Caption & " : " & CurrGrp, "s8b")
    '                            End If
    '                            'Sohail (17 Jan 2013) -- End
    '                        ElseIf mstrArrayGroupColumnNames.Length = 2 Then
    '                            'Sohail (17 Jan 2013) -- Start
    '                            'TRA - ENHANCEMENT
    '                            'wcHeader = New WorksheetCell(objDataReader.Columns(mstrArrayGroupColumnNames(0).Trim).Caption & " : " & CurrGrp, "s8bcf")
    '                            If objDataReader.Columns(mstrArrayGroupColumnNames(0).Trim).Caption = "" Then
    '                                wcHeader = New WorksheetCell(CurrGrp, "s8bcf")
    '                            Else
    '                                wcHeader = New WorksheetCell(objDataReader.Columns(mstrArrayGroupColumnNames(0).Trim).Caption & " : " & CurrGrp, "s8bcf")
    '                            End If
    '                            'Sohail (17 Jan 2013) -- End
    '                        End If

    '                        row.Cells.Add(wcHeader)
    '                        wcHeader.MergeAcross = intColCount - 1
    '                    End If

    '                    '*** Sub Group Header
    '                    If PrevGrp2 <> CurrGrp2 AndAlso mstrArrayGroupColumnNames.Length > 1 Then
    '                        row = sheet.Table.Rows.Add
    '                        row.AutoFitHeight = True

    '                        'Sohail (17 Jan 2013) -- Start
    '                        'TRA - ENHANCEMENT
    '                        'wcHeader = New WorksheetCell(objDataReader.Columns(mstrArrayGroupColumnNames(1).Trim).Caption & " : " & CurrGrp2, "s8b")
    '                        If objDataReader.Columns(mstrArrayGroupColumnNames(1).Trim).Caption = "" Then
    '                            wcHeader = New WorksheetCell(CurrGrp2, "s8b")
    '                        Else
    '                            wcHeader = New WorksheetCell(objDataReader.Columns(mstrArrayGroupColumnNames(1).Trim).Caption & " : " & CurrGrp2, "s8b")
    '                        End If
    '                        'Sohail (17 Jan 2013) -- End
    '                        row.Cells.Add(wcHeader)
    '                        wcHeader.MergeAcross = intColCount - 1
    '                    End If

    '                    If PrevGrp <> CurrGrp Then
    '                        k = 1
    '                    End If
    '                End If

    '            End If


    '            'Add row to the excel sheet
    '            row = sheet.Table.Rows.Add
    '            row.AutoFitHeight = True

    '            blnGrpColumn = False

    '            'Loop through each column
    '            For Each col As DataColumn In objDataReader.Columns
    '                If blnGrpColumn = False Then
    '                    If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then
    '                        wc = New WorksheetCell(dtrrow(col.ColumnName).ToString, DataType.Number, "n8")
    '                        blnIsDecimalfield = True
    '                    ElseIf col.DataType Is System.Type.GetType("System.Int32") OrElse col.DataType Is System.Type.GetType("System.Integer") Then
    '                        wc = New WorksheetCell(dtrrow(col.ColumnName).ToString, DataType.Number, "n8")
    '                    Else
    '                        wc = New WorksheetCell(dtrrow(col.ColumnName).ToString, DataType.String, "s8")
    '                    End If

    '                    row.Cells.Add(wc)

    '                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                        For Each strCol As String In mstrArrayGroupColumnNames
    '                            If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                    wc.MergeAcross = 1
    '                                    blnGrpColumn = True
    '                                End If
    '                            End If
    '                        Next
    '                    End If
    '                Else
    '                    blnGrpColumn = False

    '                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                        For Each strCol As String In mstrArrayGroupColumnNames
    '                            If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                    wc.MergeAcross = 2
    '                                    blnGrpColumn = True
    '                                End If
    '                            End If
    '                        Next
    '                    End If
    '                End If
    '            Next

    '            PrevGrp = CurrGrp
    '            PrevGrp2 = CurrGrp2
    '        Next

    '        '*** Group Footer for last group
    '        If mblnShowSubTotal = True AndAlso blnIsDecimalfield = True AndAlso mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso objDataReader.Columns.Contains(mstrArrayGroupColumnNames(0).Trim) = True Then
    '            row = sheet.Table.Rows.Add
    '            row.AutoFitHeight = True

    '            blnGrpColumn = False

    '            For Each col As DataColumn In objDataReader.Columns
    '                If blnGrpColumn = False Then

    '                    If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then

    '                        If mstrArrayGroupColumnNames.Length > 1 Then
    '                            wc = New WorksheetCell()
    '                            wc.StyleID = "n8b"
    '                            wc.Data.Type = DataType.Number

    '                            wc.Formula = "=SUM(R[-" & j & "]C:R[-1]C)"
    '                            row.Cells.Add(wc)

    '                            If dicSubGroupCells.ContainsKey(col.Ordinal) = False Then
    '                                dicSubGroupCells.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
    '                            Else
    '                                dicSubGroupCells.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
    '                            End If

    '                        Else

    '                            wc = New WorksheetCell()
    '                            wc.StyleID = "n8b"
    '                            wc.Data.Type = DataType.Number
    '                            If mstrArrayGroupColumnNames.Length = 1 Then
    '                                wc.Formula = "=SUM(R[-" & j & "]C:R[-1]C)"

    '                                If dicGroupCells.ContainsKey(col.Ordinal) = False Then
    '                                    dicGroupCells.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
    '                                Else
    '                                    dicGroupCells.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
    '                                End If

    '                            ElseIf mstrArrayGroupColumnNames.Length > 1 Then
    '                                wc.Formula = "=" & dicGroupCells.Item(col.Ordinal)

    '                            Else
    '                                Continue For
    '                            End If

    '                            row.Cells.Add(wc)
    '                        End If
    '                    Else
    '                        If mstrArrayGroupColumnNames.Length > 1 Then
    '                            If col.Ordinal = 0 Then
    '                                'wc = New WorksheetCell(IIf(mstrReportExcelSubTotalCaption = "", Language.getMessage(mstrModuleName, 10, "Sub Group Total"), mstrReportExcelSubTotalCaption), DataType.String, "s8b")
    '                                wc = New WorksheetCell(IIf(mstrReportExcelSubGroupTotalCaption = "", Language.getMessage(mstrModuleName, 10, "Sub Group Total"), mstrReportExcelSubGroupTotalCaption), DataType.String, "s8b")
    '                            ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = 1 Then
    '                                wc = New WorksheetCell("( " & j.ToString & " )", DataType.String, "s8b")
    '                                'k += 1
    '                            Else
    '                                wc = New WorksheetCell("", DataType.String, "s8b")
    '                            End If
    '                            row.Cells.Add(wc)
    '                        End If

    '                        If mstrArrayGroupColumnNames.Length = 1 Then
    '                            If col.Ordinal = 0 Then
    '                                wc = New WorksheetCell(IIf(mstrReportExcelSubTotalCaption = "", Language.getMessage(mstrModuleName, 11, "Sub Total"), mstrReportExcelSubTotalCaption), DataType.String, "s8b")
    '                            ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = 1 Then
    '                                wc = New WorksheetCell("( " & j.ToString & " )", DataType.String, "s8b")
    '                            Else
    '                                wc = New WorksheetCell("", DataType.String, "s8b")
    '                            End If
    '                            row.Cells.Add(wc)
    '                        End If

    '                    End If

    '                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                        For Each strCol As String In mstrArrayGroupColumnNames
    '                            If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                    wc.MergeAcross = 1
    '                                    blnGrpColumn = True
    '                                End If
    '                            End If
    '                        Next
    '                    End If

    '                Else
    '                    blnGrpColumn = False

    '                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                        For Each strCol As String In mstrArrayGroupColumnNames
    '                            If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                    wc.MergeAcross = 2
    '                                    blnGrpColumn = True
    '                                End If
    '                            End If
    '                        Next
    '                    End If
    '                End If
    '            Next

    '            If mstrArrayGroupColumnNames.Length > 1 Then
    '                If mdicSubGroupTotalExtraInfo IsNot Nothing AndAlso mdicSubGroupTotalExtraInfo.Count > 0 Then
    '                    For Each pair In mdicSubGroupTotalExtraInfo
    '                        wc = New WorksheetCell(pair.Value, "s8b")
    '                        sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
    '                    Next
    '                End If
    '            End If

    '            If mstrArrayGroupColumnNames.Length = 1 Then
    '                If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Count > 0 Then
    '                    For Each pair In mdicSubTotalExtraInfo
    '                        wc = New WorksheetCell(pair.Value, "s8b")
    '                        sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
    '                    Next
    '                End If
    '            End If


    '            If dicGroupCells.Count > 0 AndAlso mstrArrayGroupColumnNames.Length > 1 Then

    '                row = sheet.Table.Rows.Add
    '                row.AutoFitHeight = True

    '                blnGrpColumn = False

    '                For Each col As DataColumn In objDataReader.Columns
    '                    If blnGrpColumn = False Then

    '                        If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then
    '                            wc = New WorksheetCell()
    '                            wc.StyleID = "n8b"
    '                            wc.Data.Type = DataType.Number
    '                            wc.Formula = "=" & dicSubGroupCells.Item(col.Ordinal)

    '                            If dicGroupCells.ContainsKey(col.Ordinal) = False Then
    '                                dicGroupCells.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
    '                            Else
    '                                dicGroupCells.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
    '                            End If

    '                        Else
    '                            If col.Ordinal = 0 Then
    '                                wc = New WorksheetCell(IIf(mstrReportExcelSubTotalCaption = "", Language.getMessage(mstrModuleName, 11, "Sub Total"), mstrReportExcelSubTotalCaption), DataType.String, "s8b")
    '                            ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = 1 Then
    '                                wc = New WorksheetCell("( " & k.ToString & " )", DataType.String, "s8b")

    '                                'k = 1
    '                            Else
    '                                wc = New WorksheetCell("", DataType.String, "s8b")
    '                            End If
    '                        End If
    '                        row.Cells.Add(wc)

    '                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                            For Each strCol As String In mstrArrayGroupColumnNames
    '                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                        wc.MergeAcross = 1
    '                                        blnGrpColumn = True
    '                                    End If
    '                                End If
    '                            Next
    '                        End If

    '                    Else
    '                        blnGrpColumn = False

    '                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                            For Each strCol As String In mstrArrayGroupColumnNames
    '                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                        wc.MergeAcross = 2
    '                                        blnGrpColumn = True
    '                                    End If
    '                                End If
    '                            Next
    '                        End If
    '                    End If
    '                Next

    '                If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Count > 0 Then
    '                    For Each pair In mdicSubTotalExtraInfo
    '                        wc = New WorksheetCell(pair.Value, "s8b")
    '                        sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
    '                    Next
    '                End If

    '                dicSubGroupCells.Clear()
    '            End If
    '            j = 1



    '        End If

    '        '*** Grand Total
    '        If mblnShowGrandTotal = True AndAlso blnIsDecimalfield = True Then
    '            row = sheet.Table.Rows.Add
    '            row.AutoFitHeight = True

    '            blnGrpColumn = False

    '            For Each col As DataColumn In objDataReader.Columns
    '                If blnGrpColumn = False Then


    '                    If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then
    '                        wc = New WorksheetCell()
    '                        wc.StyleID = "FooterStyleN"
    '                        wc.Data.Type = DataType.Number
    '                        If dicGroupCells.Count > 0 Then
    '                            wc.Formula = "=" & dicGroupCells.Item(col.Ordinal)
    '                        Else
    '                            wc.Formula = "=SUM(R[-" & intRowCount & "]C:R[-1]C)"
    '                        End If

    '                    Else
    '                        If col.Ordinal = 0 Then
    '                            wc = New WorksheetCell(IIf(mstrReportExcelGrandTotalCaption = "", Language.getMessage(mstrModuleName, 11, "Grand Total"), mstrReportExcelGrandTotalCaption), DataType.String, "FooterStyle")
    '                        ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = 1 Then
    '                            wc = New WorksheetCell("( " & intRowCount.ToString & " )", DataType.String, "FooterStyle")
    '                        Else
    '                            wc = New WorksheetCell("", DataType.String, "FooterStyle")
    '                        End If
    '                    End If
    '                    row.Cells.Add(wc)

    '                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                        For Each strCol As String In mstrArrayGroupColumnNames
    '                            If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                    wc.MergeAcross = 1
    '                                    blnGrpColumn = True
    '                                End If
    '                            End If
    '                        Next
    '                    End If

    '                Else
    '                    blnGrpColumn = False

    '                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
    '                        For Each strCol As String In mstrArrayGroupColumnNames
    '                            If objDataReader.Columns.Contains(strCol.Trim) = True Then
    '                                If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
    '                                    wc.MergeAcross = 2
    '                                    blnGrpColumn = True
    '                                End If
    '                            End If
    '                        Next
    '                    End If
    '                End If
    '            Next

    '            If mdicGrandTotalExtraInfo IsNot Nothing AndAlso mdicGrandTotalExtraInfo.Count > 0 Then
    '                For Each pair In mdicGrandTotalExtraInfo
    '                    wc = New WorksheetCell(pair.Value, "FooterStyle")
    '                    sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
    '                Next
    '            End If
    '        End If

    '        '*** Extra Report Footer
    '        If marrWorksheetRowsExtraReportFooter IsNot Nothing AndAlso marrWorksheetRowsExtraReportFooter.Count > 0 Then
    '            For Each wrow In marrWorksheetRowsExtraReportFooter
    '                wrow.AutoFitHeight = True
    '                sheet.Table.Rows.Add(wrow)
    '            Next
    '        End If

    '        'Save the work book
    '        book.Save(strExportPath)


    '        Return True

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Export_to_Excel_Extra", mstrModuleName)
    '        Return False
    '    End Try
    'End Function
#End Region

    Private Function Export_to_Excel_Extra(ByVal strExportPath As String, _
                                           ByVal objDataReader() As System.Data.DataTable, _
                                           ByVal intArrayColumnWidth As Integer(), _
                                           Optional ByVal mblnShowReportHeader As Boolean = True, _
                                           Optional ByVal mblnShowColumnHeader As Boolean = True, _
                                           Optional ByVal mblnShowGrandTotal As Boolean = True, _
                                           Optional ByVal mstrArrayGroupColumnNames As String() = Nothing, _
                                           Optional ByVal mstrReportExcelTypeName As String = "", _
                                           Optional ByVal mstrReportExcelExtraTitle As String = "", _
                                           Optional ByVal mstrReportExcelFilterTitle As String = "", _
                                           Optional ByVal mstrReportExcelSubTotalCaption() As String = Nothing, _
                                           Optional ByVal mstrReportExcelGrandTotalCaption As String = "", _
                                           Optional ByVal mblnReportExcelShowRecordCount As Boolean = True, _
                                           Optional ByVal marrWorksheetRowsExtraReportHeader As ArrayList = Nothing, _
                                           Optional ByVal marrWorksheetRowsExtraReportFooter As ArrayList = Nothing, _
                                           Optional ByVal mdicGrandTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
                                           Optional ByVal mdicSubTotalExtraInfo() As Dictionary(Of Integer, Object) = Nothing, _
                                           Optional ByVal mblnShowSubTotal As Boolean = True, _
                                           Optional ByVal blnShowSeparateGroupingColumn As Boolean = False, _
                                           Optional ByVal mblnPrintedByInFooter As Boolean = False, _
                                           Optional ByVal marrExtraCompanyDetailsHeader As ArrayList = Nothing, _
                                           Optional ByVal marrWorksheetStyle() As WorksheetStyle = Nothing, _
                                           Optional ByVal blnRegenerateROWNO As Boolean = False _
                                           ) As Boolean
        'Sohail (09 Dec 2021) -- [blnRegenerateROWNO]
        'Sohail (27 Oct 2021) -- [marrWorksheetStyle]
        'Hemant (25 Jul 2019) -- [marrExtraCompanyDetailsHeader]
        'Sohail (09 Apr 2015)  - [blnShowSeparateGroupingColumn]

        'Pinkal (13-Apr-2017) -- 'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To Leave Reports.[Optional ByVal mblnPrintedByInFooter As Boolean = False]


        Dim blnIsDecimalfield As Boolean = False
        'Sohail (02 Apr 2014) -- Start
        'ENHANCEMENT - Multiple Worksheet in Advance Excel Report for Oman EFT.
        'Dim intColCount As Integer = objDataReader.Columns.Count
        'Dim intRowCount As Integer = objDataReader.Rows.Count
        Dim intColCount As Integer
        Dim intRowCount As Integer
        'Sohail (02 Apr 2014) -- End

        Try
            'Add a workbook
            Dim book As New ExcelWriter.Workbook

            ' Specify which Sheet should be opened and the size of window by default
            book.ExcelWorkbook.ActiveSheetIndex = 1
            book.ExcelWorkbook.WindowTopX = 100
            book.ExcelWorkbook.WindowTopY = 200
            book.ExcelWorkbook.WindowHeight = 7000
            book.ExcelWorkbook.WindowWidth = 8000

            ' Some optional properties of the Document
            book.Properties.Author = "Aruti"
            book.Properties.Title = "Excel Export"
            book.Properties.Created = DateTime.Now

            ' Add styles for header of the Workbook
            Dim style As WorksheetStyle = book.Styles.Add("HeaderStyle")
            With style
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Font.Color = "White"
                .Interior.Color = "Blue"
                'Sohail (25 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.Interior.Pattern = StyleInteriorPattern.DiagCross
                .Interior.Pattern = StyleInteriorPattern.Solid
                'Sohail (25 Jan 2013) -- End
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column..
            Dim HStyleWC As WorksheetStyle = book.Styles.Add("HeaderStyleWC")
            With HStyleWC
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With
            'Sohail (10 Jul 2019) -- End

            Dim styleFtr As WorksheetStyle = book.Styles.Add("FooterStyle")
            With styleFtr
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.WrapText = True
                .Font.Color = "White"
                .Interior.Color = "Blue"
                'Sohail (25 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.Interior.Pattern = StyleInteriorPattern.DiagCross
                .Interior.Pattern = StyleInteriorPattern.Solid
                'Sohail (25 Jan 2013) -- End
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With

            Dim styleFtrN As WorksheetStyle = book.Styles.Add("FooterStyleN")
            With styleFtrN
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.WrapText = True
                .Font.Color = "White"
                .Interior.Color = "Blue"
                'Sohail (25 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.Interior.Pattern = StyleInteriorPattern.DiagCross
                .Interior.Pattern = StyleInteriorPattern.Solid
                'Sohail (25 Jan 2013) -- End
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = GUI.fmtCurrency
            End With

            Dim FooterStyleI As WorksheetStyle = book.Styles.Add("FooterStyleI")
            With FooterStyleI
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.WrapText = True
                .Font.Color = "White"
                .Interior.Color = "Blue"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "0"
            End With

            Dim styleFilterTitle As WorksheetStyle = book.Styles.Add("FilterTitle") 'Without Border
            With styleFilterTitle
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "@"
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With

            'Add styles to the workbook 
            Dim s8 As WorksheetStyle = book.Styles.Add("s8")
            With s8
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            'Sohail (30 Dec 2016) -- Start
            'Enhancement - 64.1 - Right align style for string column.
            Dim s8r As WorksheetStyle = book.Styles.Add("s8r")
            With s8r
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With
            'Sohail (30 Dec 2016) -- End

            Dim s8w As WorksheetStyle = book.Styles.Add("s8w") 'Without Border
            With s8w
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With

            Dim s8b As WorksheetStyle = book.Styles.Add("s8b")
            With s8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8bc As WorksheetStyle = book.Styles.Add("s8bc")
            With s8bc
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8bcf As WorksheetStyle = book.Styles.Add("s8bcf")
            With s8bcf
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                'Sohail (09 Apr 2015) -- Start
                'CCK Enhancement - New report Detailed Salary Breakdown report.
                '.Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.Vertical = StyleVerticalAlignment.Center
                'Sohail (09 Apr 2015) -- End
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .Interior.Color = "#C5D9F1"
                'Sohail (25 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.Interior.Pattern = StyleInteriorPattern.Gray0625
                .Interior.Pattern = StyleInteriorPattern.Solid
                'Sohail (25 Jan 2013) -- End
                .NumberFormat = "General"
            End With

            Dim s8bw As WorksheetStyle = book.Styles.Add("s8bw") 'Without Borders
            With s8bw
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With

            'Hemant (25 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
            Dim s8bwp As WorksheetStyle = book.Styles.Add("s8bwp")
            With s8bwp
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Font.Color = "Purple"
            End With
            'Hemant (25 Jul 2019) -- End


            'Sohail (06 Apr 2015) -- Start
            'Enhancement - New statutory report HELB report for Kenya.
            Dim s8vc As WorksheetStyle = book.Styles.Add("s8vc")
            With s8vc
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Center
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With
            'Sohail (06 Apr 2015) -- End

            'Shani(22-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            Dim s8BBc As WorksheetStyle = book.Styles.Add("s8BBc")
            With s8BBc
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Font.Underline = UnderlineStyle.Single
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Center
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With
            'Shani(22-Dec-2015) -- End

            'Sohail (11 Feb 2016) -- Start
            'Enhancement - 3 Logo options on Loan Advance Saving Report for KBC.
            Dim s8BottomLine As WorksheetStyle = book.Styles.Add("s8BottomLine") 'Botton Line
            With s8BottomLine
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With
            'Sohail (11 Feb 2016) -- End

            'Sohail (23 Jan 2021) -- Start
            'Netis Gabon Enhancement : - New statutory report CFP Report.
            Dim s8ctw As WorksheetStyle = book.Styles.Add("s8ctw")
            With s8ctw
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With

            Dim s8bctw As WorksheetStyle = book.Styles.Add("s8bctw")
            With s8bctw
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With

            Dim s8c As WorksheetStyle = book.Styles.Add("s8c")
            With s8c
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8br As WorksheetStyle = book.Styles.Add("s8br")
            With s8br
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_l As WorksheetStyle = book.Styles.Add("s8_l")
            With s8_l
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8b_l As WorksheetStyle = book.Styles.Add("s8b_l")
            With s8b_l
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_r As WorksheetStyle = book.Styles.Add("s8_r")
            With s8_r
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_t As WorksheetStyle = book.Styles.Add("s8_t")
            With s8_t
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_b As WorksheetStyle = book.Styles.Add("s8_b")
            With s8_b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_lr As WorksheetStyle = book.Styles.Add("s8_lr")
            With s8_lr
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_tb As WorksheetStyle = book.Styles.Add("s8_tb")
            With s8_tb
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_ltb As WorksheetStyle = book.Styles.Add("s8_ltb")
            With s8_ltb
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_rtb As WorksheetStyle = book.Styles.Add("s8_rtb")
            With s8_rtb
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With
            'Sohail (23 Jan 2021) -- End

            'Sohail (27 Oct 2021) -- Start
            'Gabon Enhancement : OLD-474 : New statutory Rapport Trimestriel CNAMGS report for Gabon.
            Dim s8_lrt As WorksheetStyle = book.Styles.Add("s8_lrt")
            With s8_lrt
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_lrb As WorksheetStyle = book.Styles.Add("s8_lrb")
            With s8_lrb
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8_lrtb As WorksheetStyle = book.Styles.Add("s8_lrtb")
            With s8_lrtb
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With
            'Sohail (27 Oct 2021) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            Dim s8bcg As WorksheetStyle = book.Styles.Add("s8bcg")
            With s8bcg
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Interior.Color = "#D9D9D9"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8brg As WorksheetStyle = book.Styles.Add("s8brg")
            With s8brg
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Interior.Color = "#D9D9D9"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With
            'Sohail (08 Jul 2021) -- End

            Dim count8 As WorksheetStyle = book.Styles.Add("count8")
            With count8
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = False
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                '.NumberFormat = "General Number"
                .NumberFormat = "0"
            End With

            Dim count8b As WorksheetStyle = book.Styles.Add("count8b")
            With count8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                '.NumberFormat = "General Number"
                .NumberFormat = "0"
            End With

            Dim countFooterStyleN As WorksheetStyle = book.Styles.Add("countFooterStyleN")
            With countFooterStyleN
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Font.Color = "#000000"
                .Font.Color = "White"
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Interior.Color = "Blue"
                'Sohail (25 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.Interior.Pattern = StyleInteriorPattern.DiagCross
                .Interior.Pattern = StyleInteriorPattern.Solid
                'Sohail (25 Jan 2013) -- End
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "0"
            End With

            Dim n8 As WorksheetStyle = book.Styles.Add("n8")
            With n8
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                '.NumberFormat = "General Number"
                .NumberFormat = GUI.fmtCurrency
            End With

            Dim n8b As WorksheetStyle = book.Styles.Add("n8b")
            With n8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                '.NumberFormat = "General Number"
                .NumberFormat = GUI.fmtCurrency
            End With

            'Sohail (27 Oct 2021) -- Start
            'Gabon Enhancement : OLD-474 : New statutory Rapport Trimestriel CNAMGS report for Gabon.
            Dim n8bc As WorksheetStyle = book.Styles.Add("n8bc")
            With n8bc
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = GUI.fmtCurrency
            End With
            'Sohail (27 Oct 2021) -- End

            'Sohail (15 Mar 2014) -- Start
            'Enhancement - SUM function for Integer Column
            Dim i8 As WorksheetStyle = book.Styles.Add("i8")
            With i8
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = False
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "0"
            End With

            Dim i8b As WorksheetStyle = book.Styles.Add("i8b")
            With i8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "0"
            End With
            'Sohail (15 Mar 2014) -- End

            'S.SANDEEP [04-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
            Dim i8bw As WorksheetStyle = book.Styles.Add("i8bw")
            With i8bw
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "0"
            End With

            Dim i8w As WorksheetStyle = book.Styles.Add("i8w")
            With i8w
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = False
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "0"
            End With

            Dim n8w As WorksheetStyle = book.Styles.Add("n8w")
            With n8w
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = False
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = GUI.fmtCurrency
            End With

            Dim n8bw As WorksheetStyle = book.Styles.Add("n8bw")
            With n8bw
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = GUI.fmtCurrency
            End With
            'S.SANDEEP [04-May-2018] -- END

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            Dim s9b As WorksheetStyle = book.Styles.Add("s9b")
            With s9b
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s9bc As WorksheetStyle = book.Styles.Add("s9bc")
            With s9bc
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8cw As WorksheetStyle = book.Styles.Add("s8cw")
            With s8cw
                .Font.FontName = "Tahoma"
                .Font.Size = 8 'Sohail (23 Jan 2021)
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With

            Dim s9bcw As WorksheetStyle = book.Styles.Add("s9bcw")
            With s9bcw
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With
            'Nilay (15-Dec-2015) -- End

            Dim s10b As WorksheetStyle = book.Styles.Add("s10b")
            With s10b
                .Font.FontName = "Tahoma"
                .Font.Size = 10
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            Dim s10bc As WorksheetStyle = book.Styles.Add("s10bc")
            With s10bc
                .Font.FontName = "Tahoma"
                .Font.Size = 10
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s10bcg As WorksheetStyle = book.Styles.Add("s10bcg")
            With s10bcg
                .Font.FontName = "Tahoma"
                .Font.Size = 10
                .Font.Bold = True
                .Interior.Color = "#D9D9D9"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With
            'Sohail (08 Jul 2021) -- End

            Dim s10bw As WorksheetStyle = book.Styles.Add("s10bw") 'Without Borders
            With s10bw
                .Font.FontName = "Tahoma"
                .Font.Size = 10
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With



            Dim s9w As WorksheetStyle = book.Styles.Add("s9w") 'Without Borders
            With s9w
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With

            Dim s9bw As WorksheetStyle = book.Styles.Add("s9bw") 'Without Borders
            With s9bw
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With

            Dim s9wc As WorksheetStyle = book.Styles.Add("s9wc") 'Without Borders Center alignment
            With s9wc
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "General"
            End With

            Dim s12b As WorksheetStyle = book.Styles.Add("s12b")
            With s12b
                .Font.FontName = "Tahoma"
                .Font.Size = 12
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Font.Color = "Purple"
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            Dim s12bc As WorksheetStyle = book.Styles.Add("s12bc")
            With s12bc
                .Font.FontName = "Tahoma"
                .Font.Size = 12
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With
            'Sohail (08 Jul 2021) -- End

            Dim s12bw As WorksheetStyle = book.Styles.Add("s12bw")
            With s12bw
                .Font.FontName = "Tahoma"
                .Font.Size = 12
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Font.Color = "Purple"
            End With

            'Sohail (27 Oct 2021) -- Start
            'Gabon Enhancement : OLD-474 : New statutory Rapport Trimestriel CNAMGS report for Gabon.
            If marrWorksheetStyle IsNot Nothing Then
                Dim ws As WorksheetStyle = Nothing
                For Each arr As WorksheetStyle In marrWorksheetStyle
                    book.Styles.Add(arr)
                Next
            End If
            'Sohail (27 Oct 2021) -- End

            'Sohail (02 Apr 2014) -- Start
            'ENHANCEMENT - Multiple Worksheet in Advance Excel Report for Oman EFT.
            'Dim intColCount As Integer = objDataReader.Columns.Count
            'Dim intRowCount As Integer = objDataReader.Rows.Count
            'Sohail (02 Apr 2014) -- End

            'Sohail (02 Apr 2014) -- Start
            'ENHANCEMENT - Multiple Worksheet in Advance Excel Report for Oman EFT.

            ' Add a Worksheet with some data
            'Dim sheet As Worksheet = book.Worksheets.Add(IIf(mstrReportExcelTypeName.Trim <> "", Left(mstrReportExcelTypeName.Replace(" ", "_"), 31), Left(m_strReportName.Replace(" ", "_"), 31)))
            Dim sheet As Worksheet = Nothing
            'Sohail (02 Apr 2014) -- End

            For intTables As Integer = 0 To objDataReader.Length - 1 'Sohail (02 Apr 2014)

                If objDataReader.Length = 1 Then
                    sheet = book.Worksheets.Add(IIf(mstrReportExcelTypeName.Trim <> "", Left(mstrReportExcelTypeName.Replace(" ", "_"), 31), Left(m_strReportName.Replace(" ", "_"), 31)))
                Else
                    sheet = book.Worksheets.Add(Left(objDataReader(intTables).TableName, 31))
                End If

                'Sohail (09 Apr 2015) -- Start
                'CCK Enhancement - New report Detailed Salary Breakdown report.
                '*** if first columns are grouping column then set first non grouping column as first column in datatable.
                If blnShowSeparateGroupingColumn = False AndAlso mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                    If objDataReader(intTables).Columns.Contains(objDataReader(intTables).Columns(0).ColumnName.Trim) = True Then
                        Dim c As DataColumn = (From p In objDataReader(intTables).Columns.Cast(Of DataColumn)() Where (mstrArrayGroupColumnNames.Contains(p.ColumnName) = False) Select (p)).First
                        If c IsNot Nothing Then
                            objDataReader(intTables).Columns(c.ColumnName).SetOrdinal(0)
                        End If
                    End If
                End If

                Dim intExcelRowIndex As Integer = 0
                Dim intFunctionStart As Integer = 0
                'Sohail (09 Apr 2015) -- End

                intColCount = objDataReader(intTables).Columns.Count
                intRowCount = objDataReader(intTables).Rows.Count

            '' we can optionally set some column settings
            If intArrayColumnWidth IsNot Nothing Then
                For Each item In intArrayColumnWidth
                    sheet.Table.Columns.Add(New WorksheetColumn(item))
                Next
            End If


            'Add row with some properties
                Dim row As WorksheetRow = Nothing
            Dim wcHeader As WorksheetCell = Nothing

                '*** Column Header

                'Sohail (12 Feb 2018) -- Start
                'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                'intExcelRowIndex += 1 'Sohail (09 Apr 2015) 
                'row = sheet.Table.Rows.Add
                'row.AutoFitHeight = True
                'Sohail (12 Feb 2018) -- End

                If mblnShowReportHeader = True Then

                    'Sohail (12 Feb 2018) -- Start
                    'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                    intExcelRowIndex += 1
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True
                    'Sohail (12 Feb 2018) -- End

                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    If mblnPrintedByInFooter = False Then
                row.Cells.Add(New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Printed By :"), "s10bw"))
                    'Pinkal (01-Dec-2016) -- Start
                    'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
                    'row.Cells.Add(New WorksheetCell(User._Object._Username, "s10bw"))
                    If m_strUserName.Trim.Length > 0 Then
                        row.Cells.Add(New WorksheetCell(m_strUserName.Trim, "s10bw"))
                    Else
                row.Cells.Add(New WorksheetCell(User._Object._Username, "s10bw"))
                    End If
                    End If
                    'Pinkal (13-Apr-2017) -- End

                    'Pinkal (01-Dec-2016) -- End


                wcHeader = New WorksheetCell(m_strCompanyName, "s12bw")
                row.Cells.Add(wcHeader)
                    'Sohail (21 Apr 2014) -- Start
                    'Enhancement - Error when less than 3 columns
                    'wcHeader.MergeAcross = intColCount - 3
                    If intColCount > 3 Then

                        'Pinkal (13-Apr-2017) -- Start
                        'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                        If mblnPrintedByInFooter = False Then
                wcHeader.MergeAcross = intColCount - 3
                        Else
                            wcHeader.MergeAcross = intColCount - 1
                        End If
                        'Pinkal (13-Apr-2017) -- End
                    End If
                    'Sohail (21 Apr 2014) -- End

                    'Hemant (25 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
                    If marrExtraCompanyDetailsHeader IsNot Nothing AndAlso marrExtraCompanyDetailsHeader.Count > 0 Then
                        For Each wrow In marrExtraCompanyDetailsHeader
                            wrow.AutoFitHeight = True
                            sheet.Table.Rows.Add(wrow)
                            intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                        Next
                    End If
                    'Hemant (25 Jul 2019) -- End

                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True

                    If mblnPrintedByInFooter = False Then
                row.Cells.Add(New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Date :"), "s10bw"))
                row.Cells.Add(New WorksheetCell(ConfigParameter._Object._CurrentDateAndTime.ToString, "s10bw"))
                    End If
                    'Pinkal (13-Apr-2017) -- End

                    'End If 'Sohail (12 Feb 2018)


                '*** Report Name
                wcHeader = New WorksheetCell(IIf(mstrReportExcelTypeName.Trim = "", m_strReportName, mstrReportExcelTypeName), "s12bw")
                row.Cells.Add(wcHeader)
                    'Sohail (21 Apr 2014) -- Start
                    'Enhancement - Error when less than 3 columns
                    'wcHeader.MergeAcross = intColCount - 3
                    If intColCount > 3 Then
                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    If mblnPrintedByInFooter = False Then
                wcHeader.MergeAcross = intColCount - 3
                    Else
                        wcHeader.MergeAcross = intColCount - 1
                    End If
                    'Pinkal (13-Apr-2017) -- End
                    End If
                    'Sohail (21 Apr 2014) -- End

                '*** Extra Title
                If mstrReportExcelExtraTitle.Trim <> "" Then
                    Dim arrTitle() As String = mstrReportExcelExtraTitle.Split("|")
                    For Each strTitle As String In arrTitle
                            intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                    row = sheet.Table.Rows.Add
                    row.AutoFitHeight = True
                    row.Cells.Add(New WorksheetCell("", "s10bw"))
                    row.Cells.Add(New WorksheetCell("", "s10bw"))
                        wcHeader = New WorksheetCell(strTitle, "s12bw")
                    row.Cells.Add(wcHeader)
                            'Sohail (21 Apr 2014) -- Start
                            'Enhancement - Error when less than 3 columns
                            'wcHeader.MergeAcross = intColCount - 3
                            If intColCount > 3 Then
                    wcHeader.MergeAcross = intColCount - 3
                            End If
                            'Sohail (21 Apr 2014) -- End
                    Next
                End If

                '*** Filter Title
                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True
                If (mstrReportExcelFilterTitle = "" AndAlso m_strFilterTitle = "") OrElse mstrReportExcelFilterTitle = " " Then
                        'Sohail (31 Jan 2020) -- Start
                        'SNV Enhancement # 0004467 : Show bank name's on bank payment voucher report.
                        'wcHeader = New WorksheetCell("", "s8w")
                        If objDataReader.Length = 1 Then
                    wcHeader = New WorksheetCell("", "s8w")
                        Else
                            wcHeader = New WorksheetCell(objDataReader(intTables).TableName, "s8bc")
                        End If
                        'Sohail (31 Jan 2020) -- End
                ElseIf mstrReportExcelFilterTitle.Trim <> "" Then
                    wcHeader = New WorksheetCell(mstrReportExcelFilterTitle, "FilterTitle")
                Else
                    wcHeader = New WorksheetCell(m_strFilterTitle, "FilterTitle")
                End If
                row.Cells.Add(wcHeader)
                wcHeader.MergeAcross = intColCount - 1

                End If 'Sohail (12 Feb 2018)

                '*** Extra Report Header
                If marrWorksheetRowsExtraReportHeader IsNot Nothing AndAlso marrWorksheetRowsExtraReportHeader.Count > 0 Then
                    For Each wrow In marrWorksheetRowsExtraReportHeader
                        wrow.AutoFitHeight = True
                        sheet.Table.Rows.Add(wrow)
                        intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                    Next
                Else

                If mblnShowReportHeader = True Then
                    '*** Blank Row
                        intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                    row = sheet.Table.Rows.Add
                    wcHeader = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcHeader)
                    wcHeader.MergeAcross = intColCount - 1
                End If
            End If

            Dim blnGrpColumn As Boolean = False
            Dim intMergeCols As Integer = 0

            If mblnShowColumnHeader = True Then

                    'Sohail (09 Apr 2015) -- Start
                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                    intExcelRowIndex += 1
                    intFunctionStart = intExcelRowIndex
                    'Sohail (09 Apr 2015) -- End
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True

                blnGrpColumn = False


                'Add header text for the columns  
                    For Each col As DataColumn In objDataReader(intTables).Columns
                    If blnGrpColumn = False Then
                            'Sohail (10 Jul 2019) -- Start
                            'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column..
                            'wcHeader = New WorksheetCell(col.Caption, "HeaderStyle")
                            If objDataReader(0).ExtendedProperties.ContainsKey("HeaderStyle") = True Then
                                wcHeader = New WorksheetCell(col.Caption, objDataReader(0).ExtendedProperties.Item("HeaderStyle").ToString)
                            Else
                        wcHeader = New WorksheetCell(col.Caption, "HeaderStyle")
                            End If
                            'Sohail (10 Jul 2019) -- End

                        intMergeCols = 1

                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                    If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                        wcHeader.MergeAcross = intMergeCols
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next
                        End If
                        row.Cells.Add(wcHeader)
                    Else

                        blnGrpColumn = False
                        intMergeCols += 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                    If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                        wcHeader.MergeAcross = intMergeCols
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next

                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                If blnShowSeparateGroupingColumn = True Then
                                    wcHeader = New WorksheetCell(col.Caption, "HeaderStyle")
                                    row.Cells.Add(wcHeader)
                                End If
                                'Sohail (09 Apr 2015) -- End

                        End If
                    End If

                Next


                'Freeze the top rows
                sheet.Options.FreezePanes = True
                sheet.Options.SplitHorizontal = sheet.Table.Rows.Count
                sheet.Options.TopRowBottomPane = sheet.Table.Rows.Count
                sheet.Options.ActivePane = 2
            End If

            'Loop through each row in datatable  
            Dim dtRow As DataRow
            Dim wc As WorksheetCell = Nothing
            Dim j As Integer = 0
            Dim CurrGrpColumns As New ArrayList
            Dim PrevGrpColumns As New ArrayList
            Dim blnIsGrpChanged As Boolean
            Dim dicGroupCells() As Dictionary(Of Integer, String) = Nothing
            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                ReDim dicGroupCells(mstrArrayGroupColumnNames.Count - 1)
            End If

            Dim objGrpCell As New Dictionary(Of Integer, String)
            Dim objDicGrpFooter As New Dictionary(Of Integer, Object)
            Dim idxChangedGroupColumn As Integer = 0
            Dim intCountColOrdinal As Integer = 1
                Dim objDicGrpHeader As New Dictionary(Of Integer, String) 'Sohail (09 Apr 2015) 

            For i = 0 To intRowCount - 1
                    dtRow = objDataReader(intTables).Rows(i)

                If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then

                    blnIsGrpChanged = False
                    CurrGrpColumns.Clear()
                    For Each strColName As String In mstrArrayGroupColumnNames
                        CurrGrpColumns.Add(dtRow.Item(strColName).ToString)
                    Next

                    '*** Check which group column is changed
                    If PrevGrpColumns.Count > 0 Then
                        For idx As Integer = 0 To CurrGrpColumns.Count - 1
                            If CurrGrpColumns(idx) <> PrevGrpColumns(idx) Then
                                idxChangedGroupColumn = idx
                                blnIsGrpChanged = True
                                Exit For
                            End If
                        Next
                    End If

                        If objDataReader(intTables).Columns.Contains(mstrArrayGroupColumnNames(0).Trim) = True Then


                        j += 1

                        '*** Group Footer (If any group column changed)  
                        If mblnShowSubTotal = True AndAlso (blnIsGrpChanged = True) AndAlso blnIsDecimalfield = True AndAlso i > 0 Then


                            '*** Loop through last group column to first group column
                            For idx As Integer = CurrGrpColumns.Count - 1 To 0 Step -1

                                '*** If parent group column is changed, show group footer for all child group column
                                If idx >= idxChangedGroupColumn OrElse CurrGrpColumns(idx) <> PrevGrpColumns(idx) Then

                                        intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                            row = sheet.Table.Rows.Add
                            row.AutoFitHeight = True

                            blnGrpColumn = False
                                    intCountColOrdinal = 1

                                        For Each col As DataColumn In objDataReader(intTables).Columns

                                If blnGrpColumn = False Then
                                            If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                                                'Sohail (15 Mar 2014) - [Int64]
                                            wc = New WorksheetCell()
                                                'Sohail (15 Mar 2014) -- Start
                                                'Enhancement - SUM function for Integer Column.
                                                'wc.StyleID = "n8b"
                                                If col.DataType Is System.Type.GetType("System.Int64") Then
                                                    wc.StyleID = "i8b"
                                                Else
                                            wc.StyleID = "n8b"
                                                End If
                                                'Sohail (15 Mar 2014) -- End
                                            wc.Data.Type = DataType.Number

                                                If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                            wc.Formula = "=SUM(R[-" & (j - 1) & "]C:R[-1]C)"

                                                Else '*** If other than Last Column group is changed
                                                    wc.Formula = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)

                                            End If


                                                If dicGroupCells(idx) Is Nothing Then
                                                    objGrpCell = New Dictionary(Of Integer, String)
                                                Else
                                                    objGrpCell = dicGroupCells(idx)
                                                End If

                                                If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                                    objGrpCell.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
                                                Else
                                                    objGrpCell.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
                                                End If

                                                dicGroupCells(idx) = objGrpCell

                                            Else
                                                If col.Ordinal = 0 Then
                                                    If mstrReportExcelSubTotalCaption IsNot Nothing AndAlso mstrReportExcelSubTotalCaption.Length - 1 >= idx AndAlso mstrReportExcelSubTotalCaption(idx) IsNot Nothing Then
                                                        wc = New WorksheetCell(mstrReportExcelSubTotalCaption(idx).ToString, DataType.String, "s8b")
                                            Else
                                                        wc = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Sub Total"), DataType.String, "s8b")
                                            End If
                                                ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal Then
                                                    If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                                        wc = New WorksheetCell()
                                                        wc.StyleID = "count8b"
                                                        wc.Formula = "=COUNTA(R[-" & (j - 1) & "]C:R[-1]C)"

                                                    Else '*** If other than Last Column group is changed
                                                        wc = New WorksheetCell()
                                                        wc.StyleID = "count8b"
                                                        wc.Formula = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)
                                        End If

                                                    If dicGroupCells(idx) Is Nothing Then
                                                        objGrpCell = New Dictionary(Of Integer, String)
                                    Else
                                                        objGrpCell = dicGroupCells(idx)
                                            End If

                                                    If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                                        objGrpCell.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
                                                    Else
                                                        objGrpCell.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
                                        End If

                                                    dicGroupCells(idx) = objGrpCell
                                            Else
                                                wc = New WorksheetCell("", DataType.String, "s8b")
                                            End If
                                    End If

                                            blnGrpColumn = False
                                            intMergeCols = 1
                                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                        For Each strCol As String In mstrArrayGroupColumnNames
                                                        If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                                            'Sohail (18 Aug 2017) -- Start
                                                            'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                                                            'If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                                            'Sohail (28 Sep 2017) -- Start
                                                            'Issue - 70.1 - Alignment issue on sub total and grand total row in excel advance.
                                                            'If (mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                                            If (blnShowSeparateGroupingColumn = False AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (blnShowSeparateGroupingColumn = True AndAlso mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                                                'Sohail (28 Sep 2017) -- End
                                                                'Sohail (18 Aug 2017) -- End
                                                                'Sohail (09 Apr 2015) -- Start
                                                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                                'wc.MergeAcross = intMergeCols
                                                                If blnShowSeparateGroupingColumn = False Then
                                                            wc.MergeAcross = intMergeCols
                                                                Else
                                                                    If objDicGrpHeader.ContainsKey(idx) = True Then
                                                                        'Sohail (26 Jun 2015) -- Start
                                                                        'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                                                        'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                                        'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).Data.Text = sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text
                                                                        sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                                        'Sohail (26 Jun 2015) -- End
                                                                    End If
                                                                End If
                                                                'Sohail (09 Apr 2015) -- End
                                                    blnGrpColumn = True

                                                            If col.Ordinal = 0 Then
                                                                intCountColOrdinal += 1
                                                            Else
                                                                intCountColOrdinal = 1
                                                            End If
                                                End If
                                            End If
                                        Next
                                    End If
                                            row.Cells.Add(wc)
                                Else

                                    blnGrpColumn = False
                                            intMergeCols += 1
                                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                        For Each strCol As String In mstrArrayGroupColumnNames
                                                        If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                                            'Sohail (18 Aug 2017) -- Start
                                                            'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                                                            'If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                                            'Sohail (28 Sep 2017) -- Start
                                                            'Issue - 70.1 - Alignment issue on sub total and grand total row in excel advance.
                                                            'If (mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                                            If (blnShowSeparateGroupingColumn = False AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (blnShowSeparateGroupingColumn = True AndAlso mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                                                'Sohail (28 Sep 2017) -- End
                                                                'Sohail (18 Aug 2017) -- End
                                                                'Sohail (09 Apr 2015) -- Start
                                                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                                'wc.MergeAcross = intMergeCols
                                                                If blnShowSeparateGroupingColumn = False Then
                                                            wc.MergeAcross = intMergeCols
                                                                Else
                                                                    If objDicGrpHeader.ContainsKey(idx) = True Then
                                                                        'Sohail (26 Jun 2015) -- Start
                                                                        'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                                                        'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                                        'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).Data.Text = sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text
                                                                        sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                                        'Sohail (26 Jun 2015) -- End
                                                                    End If
                                                                End If
                                                                'Sohail (09 Apr 2015) -- End
                                                    blnGrpColumn = True

                                                            If intCountColOrdinal > 1 Then
                                                                intCountColOrdinal += 1
                                                            End If
                                                End If
                                            End If
                                        Next

                                                    'Sohail (09 Apr 2015) -- Start
                                                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                    If blnShowSeparateGroupingColumn = True Then
                                                        wc = New WorksheetCell(row.Cells(0).Data.Text, DataType.String, "s8b")
                                                        row.Cells.Add(wc)
                                                    End If
                                                    'Sohail (09 Apr 2015) -- End

                                    End If
                                End If

                            Next

                                        'Sohail (26 Jun 2015) -- Start
                                        'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso blnShowSeparateGroupingColumn = True Then
                                            If sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Data.Text Is Nothing AndAlso sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Formula Is Nothing Then
                                                sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Data.Text = sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Data.Text
                                                sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Formula = sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Formula
                                            End If
                                            For delidx = idx To 0 Step -1
                                                sheet.Table.Rows(intExcelRowIndex - 1).Cells.RemoveAt(0)
                                            Next
                                            sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Index = idx + 2
                                        End If
                                        'Sohail (26 Jun 2015) -- End

                                    If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Length - 1 >= idx Then
                                        objDicGrpFooter = mdicSubTotalExtraInfo(idx)
                                        If objDicGrpFooter IsNot Nothing Then
                                            For Each pair In objDicGrpFooter
                                        wc = New WorksheetCell(pair.Value, "s8b")
                                        sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
                                    Next
                                End If
                            End If

                                            End If


                                    Next

                            For sr As Integer = idxChangedGroupColumn + 1 To dicGroupCells.Count - 1
                                dicGroupCells(sr) = Nothing
                                        Next

                            j = 1

                        End If

                        '*** Group Header
                        Dim blnIsChanged As Boolean = False
                        Dim strStyle As String
                        Dim intGrpSpace As Integer
                        For idx As Integer = 0 To CurrGrpColumns.Count - 1
                            If PrevGrpColumns.Count = 0 OrElse blnIsChanged = True OrElse CurrGrpColumns(idx) <> PrevGrpColumns(idx) Then
                                    'Sohail (09 Apr 2015) -- Start
                                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                                    intExcelRowIndex += 1
                                    intFunctionStart = intExcelRowIndex
                                    If objDicGrpHeader.ContainsKey(idx) = False Then
                                        objDicGrpHeader.Add(idx, intFunctionStart)
                                    Else
                                        objDicGrpHeader.Item(idx) = intFunctionStart
                                    End If
                                    'Sohail (09 Apr 2015) -- End
                            row = sheet.Table.Rows.Add
                            row.AutoFitHeight = True
                                strStyle = "s8b"
                                intGrpSpace = idx - 1

                                If idx = 0 Then
                                    intGrpSpace = 0
                                        'Sohail (18 Aug 2017) -- Start
                                        'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                                        'If mstrArrayGroupColumnNames.Length > 1 Then
                                        If (blnShowSeparateGroupingColumn = False AndAlso mstrArrayGroupColumnNames.Length > 1) OrElse (blnShowSeparateGroupingColumn = True AndAlso mstrArrayGroupColumnNames.Length >= 1) Then
                                            'Sohail (18 Aug 2017) -- End
                                        strStyle = "s8bcf"
                                    End If
                                End If
                                    If objDataReader(intTables).Columns(mstrArrayGroupColumnNames(idx).Trim).Caption = "" Then
                                    wcHeader = New WorksheetCell(Space(4 * intGrpSpace) & CurrGrpColumns(idx).ToString, strStyle)
                                Else
                                        'Sohail (18 Aug 2017) -- Start
                                        'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                                        'wcHeader = New WorksheetCell(Space(4 * intGrpSpace) & objDataReader(intTables).Columns(mstrArrayGroupColumnNames(idx).Trim).Caption.Replace(" :", "") & " : " & CurrGrpColumns(idx).ToString, strStyle)
                                        If blnShowSeparateGroupingColumn = True Then
                                            wcHeader = New WorksheetCell(Space(4 * intGrpSpace) & CurrGrpColumns(idx).ToString, strStyle)
                                        Else
                                        wcHeader = New WorksheetCell(Space(4 * intGrpSpace) & objDataReader(intTables).Columns(mstrArrayGroupColumnNames(idx).Trim).Caption.Replace(" :", "") & " : " & CurrGrpColumns(idx).ToString, strStyle)
                            End If
                                        'Sohail (18 Aug 2017) -- End
                                    End If
                                    'Sohail (26 Jun 2015) -- Start
                                    'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                    If blnShowSeparateGroupingColumn = True AndAlso idx > 0 Then
                                        wcHeader.Index = idx + 1
                                    End If
                                    'Sohail (26 Jun 2015) -- End
                            row.Cells.Add(wcHeader)
                                    'Sohail (09 Apr 2015) -- Start
                                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                                    'wcHeader.MergeAcross = intColCount - 1
                                    If blnShowSeparateGroupingColumn = False Then
                            wcHeader.MergeAcross = intColCount - 1
                                    Else
                                        'Sohail (26 Jun 2015) -- Start
                                        'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                        'For clm As Integer = 1 To intColCount - 1
                                        '    wcHeader = New WorksheetCell("", strStyle)
                                        '    row.Cells.Add(wcHeader)
                                        'Next
                                        For clm As Integer = idx + 1 To intColCount - 1
                                            wcHeader = New WorksheetCell("", strStyle)
                                            row.Cells.Add(wcHeader)
                                        Next
                                        'Sohail (26 Jun 2015) -- End
                                    End If
                                    'Sohail (09 Apr 2015) -- End

                                blnIsChanged = True
                        End If
                        Next

                    End If

                    PrevGrpColumns = CurrGrpColumns.Clone
                End If


                'Add row to the excel sheet
                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True

                blnGrpColumn = False

                'Loop through each column
                    For Each col As DataColumn In objDataReader(intTables).Columns
                    If blnGrpColumn = False Then
                        If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then

                                'Pinkal (25-Nov-2021)-- Start
                                'Gabon Statutory Report Enhancements. 
                                If col.ExtendedProperties.ContainsKey("style") = True Then
                                    wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.Number, col.ExtendedProperties.Item("style").ToString)
                                Else
                            wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.Number, "n8")
                                End If
                                'Pinkal (25-Nov-2021)-- End

                            blnIsDecimalfield = True
                            'Sohail (15 Mar 2014) -- Start
                            'Enhancement - SUM function for Integer Column.
                            'ElseIf col.DataType Is System.Type.GetType("System.Int32") OrElse col.DataType Is System.Type.GetType("System.Integer") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                        ElseIf col.DataType Is System.Type.GetType("System.Int32") OrElse col.DataType Is System.Type.GetType("System.Integer") Then
                            wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.Number, "count8")
                            blnIsDecimalfield = True
                        ElseIf col.DataType Is System.Type.GetType("System.Int64") Then
                            wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.Number, "i8")
                            blnIsDecimalfield = True
                            'Sohail (15 Mar 2014) -- End
                        Else
                                'Sohail (30 Dec 2016) -- Start
                                'Enhancement - 64.1 - Right align style for string column.
                                'wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.String, "s8")
                                If col.ExtendedProperties.ContainsKey("style") = True Then
                                    wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.String, col.ExtendedProperties.Item("style").ToString)
                                Else
                            wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.String, "s8")
                        End If
                                'Sohail (30 Dec 2016) -- End
                            End If

                            'Sohail (24 Feb 2017) -- Start
                            'Enhancement - 65.1 - Allow to set excel style on perticular column in Advance Excel.
                            If col.ExtendedProperties.ContainsKey("style") = True Then
                                wc.StyleID = col.ExtendedProperties.Item("style").ToString
                            End If
                            'Sohail (24 Feb 2017) -- End

                            'Sohail (09 Dec 2021) -- Start
                            'Enhancement : OLD-524 : Netis Gabon - New Statutory Report ID21 DECLARATION ET TRAITEMENT DES SALAIRES for Gabon.
                            If blnRegenerateROWNO = True AndAlso col.ColumnName = "ROWNO" Then
                                wc.Data.Text = (i + 1).ToString
                            End If
                            'Sohail (09 Dec 2021) -- End

                        blnGrpColumn = False
                        intMergeCols = 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                    If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                        wc.MergeAcross = intMergeCols
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next
                        End If
                            'Sohail (26 Jun 2015) -- Start
                            'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                            'row.Cells.Add(wc)
                            If blnShowSeparateGroupingColumn = False Then
                        row.Cells.Add(wc)
                    Else
                                If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso col.Ordinal < mstrArrayGroupColumnNames.Length Then
                                    'Do nothing
                                Else
                                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso col.Ordinal = mstrArrayGroupColumnNames.Length Then
                                        wc.Index = col.Ordinal + 1
                                    End If
                                    row.Cells.Add(wc)
                                End If
                            End If
                            'Sohail (26 Jun 2015) -- End
                        Else
                        blnGrpColumn = False
                        intMergeCols += 1

                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                    If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                        wc.MergeAcross = intMergeCols
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next

                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                If blnShowSeparateGroupingColumn = True Then
                                    wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.String, "s8")
                                    'row.Cells.Add(wc) 'Sohail (26 Jun 2015)
                                End If
                                'Sohail (09 Apr 2015) -- End

                        End If
                    End If

                Next

            Next

            '*** Group Footer for last group
                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                'If mblnShowSubTotal = True AndAlso blnIsDecimalfield = True AndAlso mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso objDataReader(intTables).Columns.Contains(mstrArrayGroupColumnNames(0).Trim) = True Then
                If mblnShowSubTotal = True AndAlso (blnIsDecimalfield = True OrElse mblnReportExcelShowRecordCount = True) AndAlso mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso objDataReader(intTables).Columns.Contains(mstrArrayGroupColumnNames(0).Trim) = True Then
                'Gajanan [24-OCT-2019] -- End
                For idx As Integer = CurrGrpColumns.Count - 1 To 0 Step -1
                        intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True

                blnGrpColumn = False
                    intCountColOrdinal = 1

                        For Each col As DataColumn In objDataReader(intTables).Columns

                    If blnGrpColumn = False Then


                            If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                                'Sohail (15 Mar 2014) - [Int64]
                                wc = New WorksheetCell()
                                'Sohail (15 Mar 2014) -- Start
                                'Enhancement - SUM function for Integer Columne.
                                'wc.StyleID = "n8b"
                                If col.DataType Is System.Type.GetType("System.Int64") Then
                                    wc.StyleID = "i8b"
                                Else
                                wc.StyleID = "n8b"
                                End If
                                'Sohail (15 Mar 2014) -- End
                                wc.Data.Type = DataType.Number

                                If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                    wc.Formula = "=SUM(R[-" & (j) & "]C:R[-1]C)"

                                Else '*** If other than Last Column group is changed
                                    wc.Formula = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)

                                End If

                                If dicGroupCells(idx) Is Nothing Then
                                    objGrpCell = New Dictionary(Of Integer, String)
                            Else
                                    objGrpCell = dicGroupCells(idx)
                                    End If

                                If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                    objGrpCell.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
                                Else
                                    objGrpCell.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
                                End If

                                dicGroupCells(idx) = objGrpCell
                        Else
                                If col.Ordinal = 0 Then
                                    If mstrReportExcelSubTotalCaption IsNot Nothing AndAlso mstrReportExcelSubTotalCaption.Length - 1 >= idx AndAlso mstrReportExcelSubTotalCaption(idx) IsNot Nothing Then
                                        wc = New WorksheetCell(mstrReportExcelSubTotalCaption(idx).ToString, DataType.String, "s8b")
                                Else
                                        wc = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Sub Total"), DataType.String, "s8b")
                                End If
                                ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal Then
                                    If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                        wc = New WorksheetCell()
                                        wc.StyleID = "count8b"
                                        wc.Formula = "=COUNTA(R[-" & (j) & "]C:R[-1]C)"

                                    Else '*** If other than Last Column group is changed
                                        wc = New WorksheetCell()
                                        wc.StyleID = "count8b"
                                        wc.Formula = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)

                            End If

                                    If dicGroupCells(idx) Is Nothing Then
                                        objGrpCell = New Dictionary(Of Integer, String)
                                Else
                                        objGrpCell = dicGroupCells(idx)
                            End If

                                    If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                        objGrpCell.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
                                    Else
                                        objGrpCell.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
                        End If

                                    dicGroupCells(idx) = objGrpCell
                                Else
                                    wc = New WorksheetCell("", DataType.String, "s8b")
                                End If
                        End If

                        blnGrpColumn = False
                            intMergeCols = 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                        If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                            'Sohail (18 Aug 2017) -- Start
                                            'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                                            'If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                            'Sohail (28 Sep 2017) -- Start
                                            'Issue - 70.1 - Alignment issue on sub total and grand total row in excel advance.
                                            'If (mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                            If (blnShowSeparateGroupingColumn = False AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (blnShowSeparateGroupingColumn = True AndAlso mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                                'Sohail (28 Sep 2017) -- End
                                                'Sohail (18 Aug 2017) -- End
                                                'Sohail (09 Apr 2015) -- Start
                                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                'wc.MergeAcross = intMergeCols
                                                If blnShowSeparateGroupingColumn = False Then
                                            wc.MergeAcross = intMergeCols
                                                Else
                                                    If objDicGrpHeader.ContainsKey(idx) = True Then
                                                        'Sohail (26 Jun 2015) -- Start
                                                        'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                                        'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                        'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text = sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text
                                                        sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                    End If
                                                End If
                                                'Sohail (09 Apr 2015) -- End
                                        blnGrpColumn = True

                                If col.Ordinal = 0 Then
                                                intCountColOrdinal += 1
                                Else
                                                intCountColOrdinal = 1
                                End If
                            End If
                                    End If
                                Next

                            End If

                            row.Cells.Add(wc)
                        Else
                            blnGrpColumn = False
                            intMergeCols += 1
                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                For Each strCol As String In mstrArrayGroupColumnNames
                                        If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                            'Sohail (18 Aug 2017) -- Start
                                            'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                                            'If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                            'Sohail (28 Sep 2017) -- Start
                                            'Issue - 70.1 - Alignment issue on sub total and grand total row in excel advance.
                                            'If (mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                            If (blnShowSeparateGroupingColumn = False AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (blnShowSeparateGroupingColumn = True AndAlso mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                                'Sohail (28 Sep 2017) -- End
                                                'Sohail (18 Aug 2017) -- End
                                                'Sohail (09 Apr 2015) -- Start
                                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                'wc.MergeAcross = intMergeCols
                                                If blnShowSeparateGroupingColumn = False Then
                                            wc.MergeAcross = intMergeCols
                                                Else
                                                    If objDicGrpHeader.ContainsKey(idx) = True Then
                                                        'Sohail (26 Jun 2015) -- Start
                                                        'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                                        'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                        'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).Data.Text = sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text
                                                        sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                        'Sohail (26 Jun 2015) -- End
                                                    End If
                                                End If
                                                'Sohail (09 Apr 2015) -- End
                                            blnGrpColumn = True

                                            If intCountColOrdinal > 1 Then
                                                intCountColOrdinal += 1
                                            End If
                                        End If
                                    End If
                                Next

                                    'Sohail (09 Apr 2015) -- Start
                                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                                    If blnShowSeparateGroupingColumn = True Then
                                        wc = New WorksheetCell(row.Cells(0).Data.Text, DataType.String, "s8b")
                                        row.Cells.Add(wc)
                                    End If
                                    'Sohail (09 Apr 2015) -- End
                            End If
                        End If

                    Next


                        'Sohail (26 Jun 2015) -- Start
                        'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso blnShowSeparateGroupingColumn = True Then
                            If sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Data.Text Is Nothing AndAlso sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Formula Is Nothing Then
                                sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Data.Text = sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Data.Text
                                sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Formula = sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Formula
                            End If
                            For delidx = idx To 0 Step -1
                                sheet.Table.Rows(intExcelRowIndex - 1).Cells.RemoveAt(0)
                            Next
                            sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Index = idx + 2
                        End If
                        'Sohail (26 Jun 2015) -- End

                    If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Length - 1 >= idx Then
                        objDicGrpFooter = mdicSubTotalExtraInfo(idx)
                        If objDicGrpFooter IsNot Nothing Then
                            For Each pair In objDicGrpFooter
                            wc = New WorksheetCell(pair.Value, "s8b")
                            sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
                                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                        Next
                    End If
                End If
                Next


            End If

            '*** Grand Total
                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change    
                'If mblnShowGrandTotal = True AndAlso blnIsDecimalfield = True Then
                If mblnShowGrandTotal = True AndAlso (blnIsDecimalfield = True OrElse mblnReportExcelShowRecordCount = True) Then
                'Gajanan [24-OCT-2019] -- End
                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True

                blnGrpColumn = False
                intCountColOrdinal = 1

                    For Each col As DataColumn In objDataReader(intTables).Columns

                    If blnGrpColumn = False Then

                        If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                            'Sohail (15 Mar 2014) - [Int64]
                            wc = New WorksheetCell()
                            'Sohail (15 Mar 2014) -- Start
                            'Enhancement - Cost Center Report Branch Wise.
                            'wc.StyleID = "FooterStyleN"
                            If col.DataType Is System.Type.GetType("System.Int64") Then
                                wc.StyleID = "FooterStyleI"
                            Else
                            wc.StyleID = "FooterStyleN"
                            End If
                            'Sohail (15 Mar 2014) -- End
                            wc.Data.Type = DataType.Number

                            If dicGroupCells IsNot Nothing AndAlso dicGroupCells.Count > 0 Then
                                    'Pinkal (15-Jan-2020) -- Start
                                    'Enhancements -  Working on OT Requisistion Reports for NMB.
                                    If dicGroupCells(0) IsNot Nothing Then
                                wc.Formula = "=" & dicGroupCells(0).Item(col.Ordinal)
                                    End If
                                    'Pinkal (15-Jan-2020) -- End
                            Else
                                wc.Formula = "=SUM(R[-" & intRowCount & "]C:R[-1]C)"
                            End If

                        Else
                            If col.Ordinal = 0 Then
                                wc = New WorksheetCell(IIf(mstrReportExcelGrandTotalCaption = "", Language.getMessage(mstrModuleName, 11, "Grand Total"), mstrReportExcelGrandTotalCaption), DataType.String, "FooterStyle")
                            ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal Then
                                wc = New WorksheetCell()
                                wc.StyleID = "countFooterStyleN"

                                If dicGroupCells IsNot Nothing AndAlso dicGroupCells.Count > 0 Then

                                        'Pinkal (15-Jan-2020) -- Start
                                        'Enhancements -  Working on OT Requisistion Reports for NMB.
                                        If dicGroupCells(0) IsNot Nothing Then
                                    wc.Formula = "=" & dicGroupCells(0).Item(col.Ordinal)
                                        End If
                                        'Pinkal (15-Jan-2020) -- End

                                Else
                                    wc.Formula = "=COUNTA(R[-" & intRowCount & "]C:R[-1]C)"
                                End If
                            Else
                                wc = New WorksheetCell("", DataType.String, "FooterStyle")
                            End If
                        End If

                        blnGrpColumn = False
                        intMergeCols = 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                    If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                        'Sohail (18 Aug 2017) -- Start
                                        'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                                        'If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                        'Sohail (28 Sep 2017) -- Start
                                        'Issue - 70.1 - Alignment issue on sub total and grand total row in excel advance.
                                        'If (mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                        If (blnShowSeparateGroupingColumn = False AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (blnShowSeparateGroupingColumn = True AndAlso mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                            'Sohail (28 Sep 2017) -- End
                                            'Sohail (18 Aug 2017) -- End
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                        wc.MergeAcross = intMergeCols
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                        blnGrpColumn = True

                                        If col.Ordinal = 0 Then
                                            intCountColOrdinal += 1
                                        Else
                                            intCountColOrdinal = 1
                                        End If
                                    End If
                                End If
                            Next
                        End If

                        row.Cells.Add(wc)
                    Else

                        blnGrpColumn = False
                        intMergeCols += 1

                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                    If objDataReader(intTables).Columns.Contains(strCol.Trim) = True Then
                                        'Sohail (18 Aug 2017) -- Start
                                        'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                                        'If col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal Then
                                        'Sohail (28 Sep 2017) -- Start
                                        'Issue - 70.1 - Alignment issue on sub total and grand total row in excel advance.
                                        'If (mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                        If (blnShowSeparateGroupingColumn = False AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (blnShowSeparateGroupingColumn = True AndAlso mstrArrayGroupColumnNames.Length = 1 AndAlso col.Ordinal = objDataReader(intTables).Columns(strCol.Trim).Ordinal) OrElse (mstrArrayGroupColumnNames.Length > 1 AndAlso col.Ordinal + 1 = objDataReader(intTables).Columns(strCol.Trim).Ordinal) Then
                                            'Sohail (28 Sep 2017) -- End
                                            'Sohail (18 Aug 2017) -- End
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                        wc.MergeAcross = intMergeCols
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                        blnGrpColumn = True

                                        If intCountColOrdinal > 1 Then
                                            intCountColOrdinal += 1
                                        End If
                                    End If
                                End If
                            Next

                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                If blnShowSeparateGroupingColumn = True Then
                                    wc = New WorksheetCell("", DataType.String, "FooterStyle")
                                    row.Cells.Add(wc)
                                End If
                                'Sohail (09 Apr 2015) -- End

                        End If
                    End If
                Next

                If mdicGrandTotalExtraInfo IsNot Nothing AndAlso mdicGrandTotalExtraInfo.Count > 0 Then
                    For Each pair In mdicGrandTotalExtraInfo
                        wc = New WorksheetCell(pair.Value, "FooterStyle")
                        sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
                            intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                    Next
                End If
            End If

            '*** Extra Report Footer
            If marrWorksheetRowsExtraReportFooter IsNot Nothing AndAlso marrWorksheetRowsExtraReportFooter.Count > 0 Then
                For Each wrow In marrWorksheetRowsExtraReportFooter
                    wrow.AutoFitHeight = True
                    sheet.Table.Rows.Add(wrow)
                        intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                Next

            End If

            Next 'Sohail (02 Apr 2014)


            'Pinkal (13-Apr-2017) -- Start
            'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
            Dim frow As WorksheetRow = Nothing
            Dim wcFooter As WorksheetCell
            If mblnPrintedByInFooter Then
                frow = sheet.Table.Rows.Add
                frow = sheet.Table.Rows.Add
                frow.AutoFitHeight = True
                'wcFooter = New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Printed By :") & Space(5) & IIf(m_strUserName.Trim.Length > 0, m_strUserName, User._Object._Username), "s10bw")
                'wcFooter.MergeAcross = intColCount - 2
                'row.Cells.Add(wcFooter)

                frow.Cells.Add(New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Printed By :"), "s10bw"))
                wcFooter = New WorksheetCell(IIf(m_strUserName.Trim.Length > 0, m_strUserName, User._Object._Username), "s10bw")
                wcFooter.MergeAcross = intColCount - 2
                frow.Cells.Add(wcFooter)

                frow = sheet.Table.Rows.Add
                frow.AutoFitHeight = True
                frow.Cells.Add(New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Date :"), "s10bw"))
                wcFooter = New WorksheetCell(ConfigParameter._Object._CurrentDateAndTime.ToString(), "s10bw")
                wcFooter.MergeAcross = intColCount - 2
                frow.Cells.Add(wcFooter)
            End If
            'Pinkal (13-Apr-2017) -- End




            'Save the work book
            book.Save(strExportPath)

            Dim strText As String = IO.File.ReadAllText(strExportPath)
            If strText.IndexOf("#10;") > 0 Then
                strText = strText.Replace("#10;", "&#10;")
                IO.File.WriteAllText(strExportPath, strText)
            End If


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_to_Excel_Extra", mstrModuleName)
            Return False
        End Try
    End Function

    'Sohail (08 Dec 2012) -- End

    'Sohail (29 Apr 2014) -- Start
    'Enhancement - Export Excel Report with Logo 
    Private Function Export_to_Excel_HTML(ByVal intCompanyUnkId As Integer, _
                                          ByVal intReportUnkId As Integer, _
                                          ByVal strExportPath As String, _
                                          ByVal objDataReader As System.Data.DataTable, _
                                          ByVal intArrayColumnWidth As Integer(), _
                                          Optional ByVal mblnShowReportHeader As Boolean = True, _
                                          Optional ByVal mblnShowColumnHeader As Boolean = True, _
                                          Optional ByVal mblnShowGrandTotal As Boolean = True, _
                                          Optional ByVal mstrArrayGroupColumnNames As String() = Nothing, _
                                          Optional ByVal mstrReportExcelTypeName As String = "", _
                                          Optional ByVal mstrReportExcelExtraTitle As String = "", _
                                          Optional ByVal mstrReportExcelFilterTitle As String = "", _
                                          Optional ByVal mstrReportExcelSubTotalCaption() As String = Nothing, _
                                          Optional ByVal mstrReportExcelGrandTotalCaption As String = "", _
                                          Optional ByVal mblnReportExcelShowRecordCount As Boolean = True, _
                                          Optional ByVal marrWorksheetRowsExtraReportHeader As ArrayList = Nothing, _
                                          Optional ByVal marrWorksheetRowsExtraReportFooter As ArrayList = Nothing, _
                                          Optional ByVal mdicGrandTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
                                          Optional ByVal mdicSubTotalExtraInfo() As Dictionary(Of Integer, Object) = Nothing, _
                                          Optional ByVal mblnShowSubTotal As Boolean = True, _
                                          Optional ByVal blnShowSeparateGroupingColumn As Boolean = False, _
                                          Optional ByVal mblnPrintedByInFooter As Boolean = False, _
                                          Optional ByVal marrExtraCompanyDetailsReportHeader As ArrayList = Nothing, _
                                          Optional ByVal mstrWorksheetStyle As String = "" _
                                          ) As Boolean
        'Hemant (25 Jul 2019) -- [marrExtraCompanyDetailsReportHeader]
        'Pinkal (13-Apr-2017) -- 'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To Leave Reports.[Optional ByVal mblnPrintedByInFooter As Boolean = False]

        'Sohail (09 Apr 2015)  - [blnShowSeparateGroupingColumn]

        Dim strBuilder As New StringBuilder
        Dim blnIsDecimalfield As Boolean = False
        Dim intColCount As Integer
        Dim intRowCount As Integer

        Try


            Dim intExcelRowIndex As Integer = 0
            Dim intFunctionStart As Integer = 0

            'For intTables As Integer = 0 To objDataReader.Length - 1 

            'If objDataReader.Length = 1 Then
            'sheet = book.Worksheets.Add(IIf(mstrReportExcelTypeName.Trim <> "", Left(mstrReportExcelTypeName.Replace(" ", "_"), 31), Left(m_strReportName.Replace(" ", "_"), 31)))
            'Else
            'sheet = book.Worksheets.Add(Left(objDataReader(intTables).TableName, 31))
            'End If

            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = intCompanyUnkId
            objConfig.GetReportSettings(intReportUnkId)

            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyUnkId

            mblnPrintedByInFooter = True 'Hemant (08 Aug 2019)

            '*** HEADER PART
            strBuilder.Append(" <TITLE> " & mstrReportExcelTypeName & " </TITLE> " & vbCrLf)

            'Sohail (12 Nov 2014) -- Start
            'Enhancement - Freeze Column Header Row in HTML Spreadsheet.
            strBuilder.Append("<!--[if gte mso 9]>  <xml><x:WorksheetOptions><x:FreezePanes/><x:SplitHorizontal>##1##</x:SplitHorizontal></x:WorksheetOptions></xml>  <![endif]-->" & vbCrLf)
            'Sohail (12 Nov 2014) -- End

            'Sohail (27 Oct 2021) -- Start
            'Gabon Enhancement : OLD-474 : New statutory Rapport Trimestriel CNAMGS report for Gabon.
            strBuilder.Append("<style>")
            strBuilder.Append(".s8 {font-size:10px; font-family:Tahoma; } ")
            strBuilder.Append(".s8w {font-size:10px; border-width:0px; font-family:Tahoma; } ")
            strBuilder.Append(".s8ctw {font-size:10px; border-width:0px; font-family:Tahoma; text-align:center;} ")
            strBuilder.Append(".s8b {font-size:10px; font-weight:bold; font-family:Tahoma; } ")
            strBuilder.Append(".s8bc {font-size:10px; font-weight:bold; font-family:Tahoma; text-align:center;} ")
            strBuilder.Append(".s8_lrtb {font-size:10px; font-weight:bold; font-family:Tahoma; text-align:left; border-bottom-width:0px; } ")
            strBuilder.Append(".s8_lr {font-size:10px; font-family:Tahoma; text-align:left; border-top-width:0px; border-bottom-width:0px; } ")
            strBuilder.Append(".s8_lrb {font-size:10px; font-family:Tahoma; text-align:left; border-top-width:0px; } ")
            strBuilder.Append(".s10bc {font-size:12px; font-weight:bold; font-family:Tahoma; text-align:center;} ")
            strBuilder.Append(".s10bcw {font-size:12px; font-weight:bold; font-family:Tahoma; text-align:center; border-width:0px;} ")
            strBuilder.Append(".n8 {font-size:10px; font-family:Tahoma; mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";} ")
            strBuilder.Append(".n8b {font-size:10px; font-weight:bold; font-family:Tahoma; mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";} ")
            strBuilder.Append(".n8bc {font-size:10px; font-weight:bold; font-family:Tahoma; text-align:center; mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";} ")
            strBuilder.Append(".i8 {font-size:10px; font-family:Tahoma; } ")
            strBuilder.Append(".i8b {font-size:10px; font-weight:bold; font-family:Tahoma; } ")
            If mstrWorksheetStyle.Trim.Length > 0 Then
                strBuilder.Append(mstrWorksheetStyle & " ")
            End If
            strBuilder.Append("</style>")
            'Sohail (27 Oct 2021) -- End

            strBuilder.Append(" <BODY style='font-family:Tahoma; '> " & vbCrLf)

            'Pinkal (13-Apr-2017) -- Start
            'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
            'strBuilder.Append(" <TABLE BORDER=1 WIDTH=140% CELLSPACING =0 CELLPADDING =3 style='font-size:10px;'> " & vbCrLf)
            'Sohail (27 Oct 2021) -- Start
            'Gabon Enhancement : OLD-474 : New statutory Rapport Trimestriel CNAMGS report for Gabon.
            'strBuilder.Append(" <TABLE BORDER=1 WIDTH=100% CELLSPACING =0 CELLPADDING =3 style='font-size:10px;'> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 WIDTH=100% CELLSPACING =0 CELLPADDING =3 style='font-size:10px;font-family:Tahoma;'> " & vbCrLf)
            'Sohail (27 Oct 2021) -- End
            'Pinkal (13-Apr-2017) -- End



            'Sohail (09 Apr 2015) -- Start
            'CCK Enhancement - New report Detailed Salary Breakdown report.
            '*** if first columns are grouping column then set first non grouping column as first column in datatable.
            If blnShowSeparateGroupingColumn = False AndAlso mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                If objDataReader.Columns.Contains(objDataReader.Columns(0).ColumnName.Trim) = True Then
                    Dim c As DataColumn = (From p In objDataReader.Columns.Cast(Of DataColumn)() Where (mstrArrayGroupColumnNames.Contains(p.ColumnName) = False) Select (p)).First
                    If c IsNot Nothing Then
                        objDataReader.Columns(c.ColumnName).SetOrdinal(0)
                    End If
                End If
            End If
            'Sohail (09 Apr 2015) -- End

            intColCount = objDataReader.Columns.Count
            intRowCount = objDataReader.Rows.Count
            If mstrArrayGroupColumnNames IsNot Nothing Then
                'Sohail (09 Apr 2015) -- Start
                'CCK Enhancement - New report Detailed Salary Breakdown report.
                'intColCount -= mstrArrayGroupColumnNames.Length
                If blnShowSeparateGroupingColumn = False Then
                intColCount -= mstrArrayGroupColumnNames.Length
            End If
                'Sohail (09 Apr 2015) -- End
            End If

            ' we can optionally set some column settings
            strBuilder.Append(" <TR HEIGHT = 20> " & vbCrLf)
            For i As Integer = 0 To intColCount - 1
                If intArrayColumnWidth IsNot Nothing AndAlso intArrayColumnWidth.Length > i Then
                    strBuilder.Append(" <TD style='width:" & Format((intArrayColumnWidth(i) * 1.33333), 0) & "px;border-width: 0px;' align='left'>&nbsp;</TD> " & vbCrLf)
                Else
                    strBuilder.Append(" <TD style='width:" & 170 & "px;border-width: 0px;' align='left'>&nbsp;</TD> " & vbCrLf)
                End If
            Next
            intExcelRowIndex += 1
            strBuilder.Append(" </TR>")

            '*** Column Header
            If mblnShowReportHeader = True Then

                If objConfig._IsDisplayLogo = True Then
                    'Hemant (08 Aug 2019) -- Start
                    'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    'strBuilder.Append(" <TR HEIGHT = 100> " & vbCrLf)
                    strBuilder.Append(" <TR> " & vbCrLf)
                    'Hemant (08 Aug 2019) -- End
                    strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                    Dim objAppSett As New clsApplicationSettings
                    Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                    'Sohail (26 Dec 2015) -- Start
                    'Issue : a generic error occurred in gdi+ [Enhancement - Show Company Logo option on Payroll Report as SUm function was not working at FDRC for HTML as their french language in MS EXCEL.]
                    If IO.Directory.Exists(objAppSett._ApplicationPath & "Data\Images") = False Then
                        IO.Directory.CreateDirectory(objAppSett._ApplicationPath & "Data\Images")
                    End If
                    'Sohail (26 Dec 2015) -- End
                    objAppSett = Nothing
                    If objCompany._Image IsNot Nothing Then
                        objCompany._Image.Save(strImPath)
                    End If
                    strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                    strBuilder.Append(" </TD> " & vbCrLf)
                    strBuilder.Append(" </TR> " & vbCrLf)
                    intExcelRowIndex += 1
                End If

                'Pinkal (13-Apr-2017) -- Start
                'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                If mblnPrintedByInFooter = False Then
                strBuilder.Append(" <TR> " & vbCrLf)

                    'Pinkal (22-Apr-2017) -- Start
                    'Enhancement - Creating Timesheet Project Summary Report For PSI & AGA Khan .
                    'strBuilder.Append(" <TD width='10%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                    strBuilder.Append(" <TD width='5%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                    'Pinkal (22-Apr-2017) -- End
                strBuilder.Append(" <b>" & Language.getMessage(mstrModuleName, 37, "Printed By :") & " </B> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                    'strBuilder.Append("<b>" & User._Object._Username & "</b>" & vbCrLf)
                    strBuilder.Append("<b>" & IIf(m_strUserName.Trim.Length > 0, m_strUserName, User._Object._Username) & "</b>" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                Else
                    strBuilder.Append(" <TR> " & vbCrLf)
                End If
                'Pinkal (13-Apr-2017) -- End

                If intColCount > 3 Then
                    'Hemant (08 Aug 2019) -- Start
                    'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    'If mblnPrintedByInFooter = False Then
                    '    strBuilder.Append(" <TD colspan=" & intColCount - 2 & " align='center' style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                    'Else
                    '    strBuilder.Append(" <TD colspan=" & intColCount & " align='center' style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                    'End If
                    strBuilder.Append(" <TD width='5%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                    strBuilder.Append(" </TD> " & vbCrLf)
                    strBuilder.Append(" <TD WIDTH='10%' align='left' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                    strBuilder.Append(" </TD> " & vbCrLf)
                    strBuilder.Append(" <TD colspan=" & intColCount - 2 & " align='center' style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                    'Hemant (08 Aug 2019) -- End
                Else
                    strBuilder.Append(" <TD align='center' style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                End If
                strBuilder.Append(" <B> " & Company._Object._Name & " </B> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)

                strBuilder.Append(" </TR> " & vbCrLf)

                intExcelRowIndex += 1

                'Hemant (25 Jul 2019) -- Start
                'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
                If marrExtraCompanyDetailsReportHeader IsNot Nothing AndAlso marrExtraCompanyDetailsReportHeader.Count > 0 Then
                    For Each wrow In marrExtraCompanyDetailsReportHeader
                        strBuilder.Append(wrow)
                        intExcelRowIndex += 1
                    Next
                End If
                'Hemant (25 Jul 2019) -- End

                strBuilder.Append(" <TR valign='middle'> " & vbCrLf)

                'Pinkal (13-Apr-2017) -- Start
                'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                If mblnPrintedByInFooter = False Then

                    'Pinkal (22-Apr-2017) -- Start
                    'Enhancement - Creating Timesheet Project Summary Report For PSI & AGA Khan .
                    'strBuilder.Append(" <TD width='10%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                    strBuilder.Append(" <TD width='5%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                    'Pinkal (22-Apr-2017) -- End


                strBuilder.Append(" <B>" & Language.getMessage(mstrModuleName, 6, "Date :") & "</B> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='35' align='left' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                strBuilder.Append("<b>" & ConfigParameter._Object._CurrentDateAndTime.ToString & "</b>" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                End If
                'Pinkal (13-Apr-2017) -- End

                '*** Report Name

                'Pinkal (13-Apr-2017) -- Start
                'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                'If intColCount > 3 Then
                '    strBuilder.Append(" <TD width='60%' colspan=" & intColCount - 2 & "  align='center'  style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                'Else
                '    strBuilder.Append(" <TD width='60%'  align='center' style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                'End If
                If intColCount > 3 Then
                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    'Hemant (08 Aug 2019) -- Start
                    'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    'If mblnPrintedByInFooter = False Then
                    '    strBuilder.Append(" <TD width='50%' colspan=" & intColCount - 2 & "  align='center'  style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                    'Else
                    '    strBuilder.Append(" <TD width='50%' colspan=" & intColCount & "  align='center'  style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                    'End If
                    strBuilder.Append(" <TD width='5%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                    strBuilder.Append(" </TD> " & vbCrLf)
                    strBuilder.Append(" <TD WIDTH='10%' align='left' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                    strBuilder.Append(" </TD> " & vbCrLf)
                        strBuilder.Append(" <TD width='50%' colspan=" & intColCount - 2 & "  align='center'  style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                    'Hemant (08 Aug 2019) -- End
                    'Pinkal (13-Apr-2017) -- End
                Else
                    strBuilder.Append(" <TD width='50%'  align='center' style='font-size:15px;color:purple;border-width: 0px;'> " & vbCrLf)
                End If
                'Pinkal (13-Apr-2017) -- End



                strBuilder.Append(" <b> " & IIf(mstrReportExcelTypeName.Trim = "", m_strReportName, mstrReportExcelTypeName) & "</B> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
                intExcelRowIndex += 1

                '*** Extra Title
                If mstrReportExcelExtraTitle.Trim <> "" Then
                    Dim arrTitle() As String = mstrReportExcelExtraTitle.Split("|")
                    For Each strTitle As String In arrTitle

                        strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
                        strBuilder.Append(" <TD style='border-width: 0px;'> &nbsp;</TD> " & vbCrLf)
                        strBuilder.Append(" <TD style='border-width: 0px;'> &nbsp;</TD> " & vbCrLf)

                        If intColCount > 3 Then
                            'Hemant (25 Jul 2019) -- Start
                            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
                            'strBuilder.Append(" <TD width='60%' colspan=" & intColCount - 2 & "  align='center' style='border-width: 0px; > " & vbCrLf)
                            strBuilder.Append(" <TD width='60%' colspan=" & intColCount - 2 & "  align='center' style='font-size:15px;color:purple;border-width: 0px;' > " & vbCrLf)
                            'Hemant (25 Jul 2019) -- End

                        Else
                            'Hemant (25 Jul 2019) -- Start
                            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
                            'strBuilder.Append(" <TD width='60%'  align='center' style='border-width: 0px;' > " & vbCrLf)
                            strBuilder.Append(" <TD width='60%'  align='center' style='font-size:15px;color:purple;border-width: 0px;' > " & vbCrLf)
                            'Hemant (25 Jul 2019) -- End
                        End If

                        strBuilder.Append(" <b> " & strTitle & "</B> " & vbCrLf)
                        strBuilder.Append(" </TD> " & vbCrLf)
                        strBuilder.Append(" </TR> " & vbCrLf)
                        intExcelRowIndex += 1
                    Next
                End If

                '*** Filter Title
                strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
                If (mstrReportExcelFilterTitle = "" AndAlso m_strFilterTitle = "") OrElse mstrReportExcelFilterTitle = " " Then
                    strBuilder.Append(" <TD colspan=" & intColCount & " style='border-width: 0px;'> &nbsp;</TD> " & vbCrLf)
                ElseIf mstrReportExcelFilterTitle.Trim <> "" Then
                    strBuilder.Append(" <TD colspan=" & intColCount & " style='border-width: 0px;>" & mstrReportExcelFilterTitle & "</TD> " & vbCrLf)
                Else
                    strBuilder.Append(" <TD colspan=" & intColCount & " style='border-width: 0px;'>" & m_strFilterTitle & "</TD> " & vbCrLf)
                End If
                strBuilder.Append(" </TR>" & vbCrLf)
                intExcelRowIndex += 1
            End If

            'Hemant (08 Aug 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            If strExportPath.Contains(".xls") = True AndAlso intExcelRowIndex < 8 Then
                strBuilder.Append(" <TR HEIGHT = 20> " & vbCrLf)
                For i As Integer = 0 To intColCount - 1
                    If intArrayColumnWidth IsNot Nothing AndAlso intArrayColumnWidth.Length > i Then
                        strBuilder.Append(" <TD style='width:" & Format((intArrayColumnWidth(i) * 1.33333), 0) & "px;border-width: 0px;' align='left'>&nbsp;</TD> " & vbCrLf)
                    Else
                        strBuilder.Append(" <TD style='width:" & 170 & "px;border-width: 0px;' align='left'>&nbsp;</TD> " & vbCrLf)
                    End If
                Next
                intExcelRowIndex += 1
                strBuilder.Append(" </TR>")
                strBuilder.Append(" <TR HEIGHT = 20> " & vbCrLf)
                For i As Integer = 0 To intColCount - 1
                    If intArrayColumnWidth IsNot Nothing AndAlso intArrayColumnWidth.Length > i Then
                        strBuilder.Append(" <TD style='width:" & Format((intArrayColumnWidth(i) * 1.33333), 0) & "px;border-width: 0px;' align='left'>&nbsp;</TD> " & vbCrLf)
                    Else
                        strBuilder.Append(" <TD style='width:" & 170 & "px;border-width: 0px;' align='left'>&nbsp;</TD> " & vbCrLf)
                    End If
                Next
                intExcelRowIndex += 1
                strBuilder.Append(" </TR>")
            End If
            'Hemant (08 Aug 2019) -- End

            '*** Extra Report Header
            If marrWorksheetRowsExtraReportHeader IsNot Nothing AndAlso marrWorksheetRowsExtraReportHeader.Count > 0 Then
                For Each wrow In marrWorksheetRowsExtraReportHeader
                    strBuilder.Append(wrow)
                    intExcelRowIndex += 1
                Next
            Else

                If mblnShowReportHeader = True Then
                    '*** Blank Row
                    strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
                    strBuilder.Append(" <TD colspan=" & intColCount & " style='border-width: 0px;'> &nbsp;</TD> " & vbCrLf)
                    strBuilder.Append(" </TR>" & vbCrLf)
                    intExcelRowIndex += 1
                End If
            End If

            Dim blnGrpColumn As Boolean = False
            Dim intMergeCols As Integer = 0


            If mblnShowColumnHeader = True Then

                strBuilder.Append(" <TR valign='middle' >  " & vbCrLf)

                blnGrpColumn = False


                'Add header text for the columns  
                For Each col As DataColumn In objDataReader.Columns
                    If blnGrpColumn = False Then

                        intMergeCols = 1

                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next
                        End If

                        strBuilder.Append(" <TD colspan=" & intMergeCols & " align='center' style='background-color:blue; color:white; font-weight:bold; font-size:12px;'>" & col.Caption & "</TD> " & vbCrLf)
                    Else

                        blnGrpColumn = False
                        intMergeCols += 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next
                        End If
                        'Sohail (09 Apr 2015) -- Start
                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                        If blnShowSeparateGroupingColumn = True Then
                            strBuilder.Append(" <TD colspan=1 align='center' style='background-color:blue; color:white; font-weight:bold; font-size:12px;'>" & col.Caption & "</TD> " & vbCrLf)
                        End If
                        'Sohail (09 Apr 2015) -- End
                    End If

                Next
                strBuilder.Append(" </TR>" & vbCrLf)
                intExcelRowIndex += 1
                intFunctionStart = intExcelRowIndex + 1

            End If

            'Sohail (12 Nov 2014) -- Start
            'Enhancement - Freeze Column Header Row in HTML Spreadsheet.
            strBuilder = strBuilder.Replace("##1##", intExcelRowIndex)
            'Sohail (12 Nov 2014) -- End

            'Loop through each row in datatable  
            Dim dtRow As DataRow
            'Dim wc As WorksheetCell = Nothing
            Dim j As Integer = 0
            Dim CurrGrpColumns As New ArrayList
            Dim PrevGrpColumns As New ArrayList
            Dim blnIsGrpChanged As Boolean
            Dim dicGroupCells() As Dictionary(Of Integer, String) = Nothing
            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                ReDim dicGroupCells(mstrArrayGroupColumnNames.Count - 1)
            End If
            Dim objGrpCell As New Dictionary(Of Integer, String)
            Dim objDicGrpFooter As New Dictionary(Of Integer, Object)
            Dim idxChangedGroupColumn As Integer = 0
            Dim intCountColOrdinal As Integer = 1
            Dim objDicGrpHeader As New Dictionary(Of Integer, String) 'Sohail (09 Apr 2015) 

            For i = 0 To intRowCount - 1
                dtRow = objDataReader.Rows(i)

                If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then

                    blnIsGrpChanged = False
                    CurrGrpColumns.Clear()
                    For Each strColName As String In mstrArrayGroupColumnNames
                        CurrGrpColumns.Add(dtRow.Item(strColName).ToString)
                    Next

                    '*** Check which group column is changed
                    If PrevGrpColumns.Count > 0 Then
                        For idx As Integer = 0 To CurrGrpColumns.Count - 1
                            If CurrGrpColumns(idx) <> PrevGrpColumns(idx) Then
                                idxChangedGroupColumn = idx
                                blnIsGrpChanged = True
                                Exit For
                            End If
                        Next
                    End If

                    If objDataReader.Columns.Contains(mstrArrayGroupColumnNames(0).Trim) = True Then


                        j += 1

                        '*** Group Footer (If any group column changed)  
                        If mblnShowSubTotal = True AndAlso (blnIsGrpChanged = True) AndAlso blnIsDecimalfield = True AndAlso i > 0 Then


                            '*** Loop through last group column to first group column

                            For idx As Integer = CurrGrpColumns.Count - 1 To 0 Step -1

                                '*** If parent group column is changed, show group footer for all child group column
                                If idx >= idxChangedGroupColumn OrElse CurrGrpColumns(idx) <> PrevGrpColumns(idx) Then

                                    strBuilder.Append(" <TR valign='middle'> " & vbCrLf)

                                    blnGrpColumn = False
                                    intCountColOrdinal = 1
                                    Dim strTD As String = "" 'Sohail (09 Apr 2015) 

                                    Dim intColIdx As Integer = 0
                                    For Each col As DataColumn In objDataReader.Columns

                                        If blnGrpColumn = False Then
                                            intColIdx += 1

                                            'Dim strTD As String = "" 'Sohail (09 Apr 2015) 
                                            If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64") Then

                                                If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                                    strTD = "=SUM(" & ConvertToExcelColumnLetter(intColIdx) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(intColIdx) & intExcelRowIndex.ToString & ")"
                                                Else '*** If other than Last Column group is changed
                                                    'Sohail (09 Apr 2015) -- Start
                                                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                    'strTD = "=SUM(" & ConvertToExcelColumnLetter(intColIdx) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(intColIdx) & intExcelRowIndex.ToString & ")"
                                                    strTD = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)
                                                    'Sohail (09 Apr 2015) -- End
                                                End If


                                                If dicGroupCells(idx) Is Nothing Then
                                                    objGrpCell = New Dictionary(Of Integer, String)
                                                Else
                                                    objGrpCell = dicGroupCells(idx)
                                                End If

                                                If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                                    objGrpCell.Add(col.Ordinal, "$" & ConvertToExcelColumnLetter(intColIdx) & "$" & (intExcelRowIndex + 1).ToString)
                                                Else
                                                    objGrpCell.Item(col.Ordinal) += "+$" & ConvertToExcelColumnLetter(intColIdx) & "$" & (intExcelRowIndex + 1).ToString
                                                End If

                                                dicGroupCells(idx) = objGrpCell

                                            Else
                                                If col.Ordinal = 0 Then
                                                    If mstrReportExcelSubTotalCaption IsNot Nothing AndAlso mstrReportExcelSubTotalCaption.Length - 1 >= idx AndAlso mstrReportExcelSubTotalCaption(idx) IsNot Nothing Then
                                                        strTD = mstrReportExcelSubTotalCaption(idx).ToString
                                                    Else
                                                        strTD = Language.getMessage(mstrModuleName, 11, "Sub Total")
                                                    End If
                                                ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal Then
                                                    If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                                        strTD = "=COUNTA(" & ConvertToExcelColumnLetter(intColIdx) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(intColIdx) & intExcelRowIndex.ToString & ")"

                                                    Else '*** If other than Last Column group is changed
                                                        'Sohail (09 Apr 2015) -- Start
                                                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                        'strTD = "=SUM(" & ConvertToExcelColumnLetter(intColIdx) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(intColIdx) & intExcelRowIndex.ToString & ")"
                                                        strTD = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)
                                                        'Sohail (09 Apr 2015) -- End
                                                    End If

                                                    If dicGroupCells(idx) Is Nothing Then
                                                        objGrpCell = New Dictionary(Of Integer, String)
                                                    Else
                                                        objGrpCell = dicGroupCells(idx)
                                                    End If

                                                    If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                                        'objGrpCell.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
                                                        objGrpCell.Add(col.Ordinal, "$" & ConvertToExcelColumnLetter(intColIdx) & "$" & (intExcelRowIndex + 1).ToString)
                                                    Else
                                                        'objGrpCell.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
                                                        objGrpCell.Item(col.Ordinal) += "+$" & ConvertToExcelColumnLetter(intColIdx) & "$" & (intExcelRowIndex + 1).ToString
                                                    End If

                                                    dicGroupCells(idx) = objGrpCell
                                                Else
                                                    strTD = "&nbsp;"
                                                End If
                                            End If

                                            blnGrpColumn = False
                                            intMergeCols = 1
                                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                                For Each strCol As String In mstrArrayGroupColumnNames
                                                    If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                                        If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                                            blnGrpColumn = True

                                                            If col.Ordinal = 0 Then
                                                                intCountColOrdinal += 1
                                                            Else
                                                                intCountColOrdinal = 1
                                                            End If
                                                        End If
                                                    End If
                                                Next
                                            End If

                                            If mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal AndAlso Not (col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64")) Then
                                                'Sohail (09 Apr 2015) -- Start
                                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                'strBuilder.Append(" <TD colspan=" & intMergeCols & " align='center'><B>" & strTD & "</TD></B> " & vbCrLf)
                                                If mstrArrayGroupColumnNames.Contains(col.ColumnName) = False Then
                                                strBuilder.Append(" <TD colspan=" & intMergeCols & " align='center'><B>" & strTD & "</TD></B> " & vbCrLf)
                                            Else
                                                    If mstrArrayGroupColumnNames.Contains(col.ColumnName) = False Then
                                                        strBuilder.Append(" <TD colspan=" & intMergeCols & " align='center'><B>" & strTD & "</TD></B> " & vbCrLf)
                                                    End If
                                                End If
                                                'Sohail (09 Apr 2015) -- End
                                            Else
                                                'Sohail (09 Apr 2015) -- Start
                                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                'strBuilder.Append(" <TD colspan=" & intMergeCols & "><B>" & strTD & "</TD></B> " & vbCrLf)
                                                If blnShowSeparateGroupingColumn = False Then
                                                    strBuilder.Append(" <TD style='mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";' colspan=" & intMergeCols & "><B>" & strTD & "</TD></B> " & vbCrLf)
                                                Else
                                                    If mstrArrayGroupColumnNames.Contains(col.ColumnName) = False Then
                                                        strBuilder.Append(" <TD style='mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";' colspan=" & intMergeCols & "><B>" & strTD & "</TD></B> " & vbCrLf)
                                                    End If
                                                End If
                                                'Sohail (09 Apr 2015) -- End
                                            End If
                                        Else

                                            blnGrpColumn = False
                                            intMergeCols += 1
                                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                                For Each strCol As String In mstrArrayGroupColumnNames
                                                    If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                                        If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                                            'wc.MergeAcross = intMergeCols
                                                            blnGrpColumn = True

                                                            If intCountColOrdinal > 1 Then
                                                                intCountColOrdinal += 1
                                                            End If
                                                        End If
                                                    End If
                                                Next
                                            End If

                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            If blnShowSeparateGroupingColumn = True Then
                                                intColIdx += 1
                                                If idx = 0 Then
                                                    strBuilder.Append(" <TD >" & strTD & "</TD>" & vbCrLf)
                                                End If
                                            End If
                                            'Sohail (09 Apr 2015) -- End

                                        End If

                                    Next
                                    strBuilder.Append(" </TR>" & vbCrLf)
                                    intExcelRowIndex += 1

                                    'Sohail (09 Apr 2015) -- Start
                                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                                    For clm As Integer = 0 To intColCount - 1
                                        If objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Ordinal = clm Then
                                            strBuilder = strBuilder.Replace("##G" & idx & "##", CInt(intExcelRowIndex - objDicGrpHeader.Item(idx)) + 2)
                                        End If
                                    Next
                                    'Sohail (09 Apr 2015) -- End

                                    If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Length - 1 >= idx Then
                                        objDicGrpFooter = mdicSubTotalExtraInfo(idx)
                                        If objDicGrpFooter IsNot Nothing Then
                                            For Each pair In objDicGrpFooter
                                            Next
                                        End If
                                    End If

                                End If


                            Next

                            For sr As Integer = idxChangedGroupColumn + 1 To dicGroupCells.Count - 1
                                dicGroupCells(sr) = Nothing
                            Next

                            j = 1

                        End If

                        '*** Group Header
                        Dim blnIsChanged As Boolean = False
                        Dim strStyle As String
                        Dim intGrpSpace As Integer
                        For idx As Integer = 0 To CurrGrpColumns.Count - 1
                            If PrevGrpColumns.Count = 0 OrElse blnIsChanged = True OrElse CurrGrpColumns(idx) <> PrevGrpColumns(idx) Then
                                strStyle = "s8b"
                                intGrpSpace = idx - 1
                                strBuilder.Append(" <TR valign='middle'> " & vbCrLf)

                                If idx = 0 Then
                                    intGrpSpace = 0
                                    If mstrArrayGroupColumnNames.Length > 1 Then
                                        strStyle = "s8bcf"
                                    End If
                                End If
                                If objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Caption = "" Then
                                    'Sohail (09 Apr 2015) -- Start
                                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                                    'strBuilder.Append(" <TD colspan=" & intColCount & "><B>" & New String("&nbsp;", (4 * intGrpSpace)) & CurrGrpColumns(idx).ToString & "</B></TD> " & vbCrLf)
                                    If blnShowSeparateGroupingColumn = False Then
                                    strBuilder.Append(" <TD colspan=" & intColCount & "><B>" & New String("&nbsp;", (4 * intGrpSpace)) & CurrGrpColumns(idx).ToString & "</B></TD> " & vbCrLf)
                                Else
                                        For clm As Integer = 0 To intColCount - 1
                                            If objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Ordinal = clm Then
                                                strBuilder.Append(" <TD rowspan=##G" & idx & "## colspan=1><B>" & New String("&nbsp;", (4 * intGrpSpace)) & CurrGrpColumns(idx).ToString & "</B></TD> " & vbCrLf)
                                            Else
                                                strBuilder.Append(" <TD >&nbsp;</TD>" & vbCrLf)
                                            End If
                                        Next
                                    End If
                                    'Sohail (09 Apr 2015) -- End
                                Else
                                    If intReportUnkId = 1 Then
                                        'Sohail (09 Apr 2015) -- Start
                                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                                        'strBuilder.Append(" <TD colspan=" & intColCount & " style='font-size:18px;'><B>" & New String("&nbsp;", (4 * intGrpSpace)) & objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Caption.Replace(" :", "") & " : " & CurrGrpColumns(idx).ToString & "</B></TD> " & vbCrLf)
                                        If blnShowSeparateGroupingColumn = False Then
                                        strBuilder.Append(" <TD colspan=" & intColCount & " style='font-size:18px;'><B>" & New String("&nbsp;", (4 * intGrpSpace)) & objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Caption.Replace(" :", "") & " : " & CurrGrpColumns(idx).ToString & "</B></TD> " & vbCrLf)
                                    Else
                                            For clm As Integer = 0 To intColCount - 1
                                                If objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Ordinal = clm Then
                                                    strBuilder.Append(" <TD rowspan=##G" & idx & "## colspan=1 style='font-size:18px;'><B>" & New String("&nbsp;", (4 * intGrpSpace)) & objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Caption.Replace(" :", "") & " : " & CurrGrpColumns(idx).ToString & "</B></TD> " & vbCrLf)
                                                Else
                                                    strBuilder.Append(" <TD >&nbsp;</TD>" & vbCrLf)
                                                End If
                                            Next
                                        End If
                                        'Sohail (09 Apr 2015) -- End
                                    Else
                                        'Sohail (09 Apr 2015) -- Start
                                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                                        'strBuilder.Append(" <TD colspan=" & intColCount & "><B>" & New String("&nbsp;", (4 * intGrpSpace)) & objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Caption.Replace(" :", "") & " : " & CurrGrpColumns(idx).ToString & "</B></TD> " & vbCrLf)
                                        If blnShowSeparateGroupingColumn = False Then
                                        strBuilder.Append(" <TD colspan=" & intColCount & "><B>" & New String("&nbsp;", (4 * intGrpSpace)) & objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Caption.Replace(" :", "") & " : " & CurrGrpColumns(idx).ToString & "</B></TD> " & vbCrLf)
                                        Else
                                            For clm As Integer = 0 To intColCount - 1
                                                If objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Ordinal = clm Then
                                                    strBuilder.Append(" <TD rowspan=##G" & idx & "## colspan=1><B>" & New String("&nbsp;", (4 * intGrpSpace)) & objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Caption.Replace(" :", "") & " : " & CurrGrpColumns(idx).ToString & "</B></TD> " & vbCrLf)
                                                ElseIf objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Ordinal <= clm Then
                                                    strBuilder.Append(" <TD >&nbsp;</TD>" & vbCrLf)
                                                End If
                                            Next
                                        End If
                                        'Sohail (09 Apr 2015) -- End
                                    End If
                                End If

                                blnIsChanged = True
                                strBuilder.Append(" </TR>" & vbCrLf)
                                intExcelRowIndex += 1
                                intFunctionStart = intExcelRowIndex + 1
                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                If objDicGrpHeader.ContainsKey(idx) = False Then
                                    objDicGrpHeader.Add(idx, intFunctionStart)
                                Else
                                    objDicGrpHeader.Item(idx) = intFunctionStart
                                End If
                                'Sohail (09 Apr 2015) -- End
                            End If
                        Next

                    End If

                    PrevGrpColumns = CurrGrpColumns.Clone
                End If


                'Add row to the excel sheet
                strBuilder.Append(" <TR valign='middle'> " & vbCrLf)

                blnGrpColumn = False

                'Loop through each column
                For Each col As DataColumn In objDataReader.Columns
                    Dim strTD As String = "" 'Sohail (09 Apr 2015) 
                    If blnGrpColumn = False Then
                        If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then
                            'Sohail (09 Apr 2015) -- Start
                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                            'strBuilder.Append(" <TD  align='right' style='mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";'>" & Format(dtRow(col.ColumnName), GUI.fmtCurrency) & "</TD> " & vbCrLf)
                            'Hemant (25 Jul 2019) -- Start
                            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
                            'strTD = " <TD  align='right' style='mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";'>" & Format(dtRow(col.ColumnName), GUI.fmtCurrency) & "</TD> "

                            'Pinkal (25-Nov-2021)-- Start
                            'Gabon Statutory Report Enhancements. 
                            If col.ExtendedProperties.ContainsKey("style") = True Then
                                strTD = " <TD  align='right' style='" & col.ExtendedProperties.Item("style").ToString() & ";'>" & CDec(dtRow(col.ColumnName)).ToString(GUI.fmtCurrency, System.Globalization.CultureInfo.InvariantCulture) & "</TD> "
                            Else
                            strTD = " <TD  align='right' style='mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";'>" & CDec(dtRow(col.ColumnName)).ToString(GUI.fmtCurrency, System.Globalization.CultureInfo.InvariantCulture) & "</TD> "
                            End If
                            'Pinkal (25-Nov-2021)-- End

                            'Hemant (25 Jul 2019) -- End
                            'Sohail (09 Apr 2015) -- End
                            blnIsDecimalfield = True
                        ElseIf col.DataType Is System.Type.GetType("System.Int32") OrElse col.DataType Is System.Type.GetType("System.Integer") Then
                            'Sohail (09 Apr 2015) -- Start
                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                            'strBuilder.Append(" <TD align='left'>" & dtRow(col.ColumnName).ToString & "</TD> " & vbCrLf)
                            strTD = " <TD align='left'>" & dtRow(col.ColumnName).ToString & "</TD> "
                            'Sohail (09 Apr 2015) -- End
                            blnIsDecimalfield = True
                        ElseIf col.DataType Is System.Type.GetType("System.Int64") Then
                            'Sohail (09 Apr 2015) -- Start
                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                            'strBuilder.Append(" <TD align='right'>" & dtRow(col.ColumnName).ToString & "</TD> " & vbCrLf)
                            strTD = " <TD align='right'>" & dtRow(col.ColumnName).ToString & "</TD> "
                            'Sohail (09 Apr 2015) -- End
                            blnIsDecimalfield = True
                        Else
                            'Sohail (13 Dec 2014) -- Start
                            'Issue : string converted to numeric column if string is numeric (for numeric emp code).
                            'strBuilder.Append(" <TD align='left'>" & dtRow(col.ColumnName).ToString & "</TD> " & vbCrLf)
                            If IsNumeric(dtRow(col.ColumnName)) = True Then
                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                'strBuilder.Append(" <TD align='left' x:str> " & dtRow(col.ColumnName).ToString & "</TD> " & vbCrLf)
                                'Hemant (08 Aug 2019) -- Start
                                'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                                'strTD = " <TD align='left' x:str> " & dtRow(col.ColumnName).ToString & "</TD> "
                                strTD = " <TD align='left' x:str> &nbsp;" & dtRow(col.ColumnName).ToString & "</TD> "
                                'Hemant (08 Aug 2019) -- End

                                'Sohail (09 Apr 2015) -- End
                                'Sohail (13 Jan 2020) -- Start
                                'NMB Issue # : Grade like "4-46" is coming as "Apr-46" on custom payroll report.
                            ElseIf col.DataType Is System.Type.GetType("System.String") AndAlso IsDate(dtRow(col.ColumnName)) = True Then
                                strTD = " <TD align='left' x:str> &nbsp;" & dtRow(col.ColumnName).ToString & "</TD> "
                            ElseIf col.DataType Is System.Type.GetType("System.String") AndAlso IsDate("01-" & dtRow(col.ColumnName).ToString) = True Then
                                strTD = " <TD align='left' x:str> &nbsp;" & dtRow(col.ColumnName).ToString & "</TD> "
                                'Sohail (13 Jan 2020) -- End
                            Else
                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                'strBuilder.Append(" <TD align='left'>" & dtRow(col.ColumnName).ToString & "</TD> " & vbCrLf)
                                strTD = " <TD align='left'>" & dtRow(col.ColumnName).ToString & "</TD> "
                                'Sohail (09 Apr 2015) -- End
                        End If
                            'Sohail (13 Dec 2014) -- End

                        End If

                        'Sohail (09 Apr 2015) -- Start
                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                        If blnShowSeparateGroupingColumn = False Then
                            strBuilder.Append(strTD & vbCrLf)
                        Else
                            If mstrArrayGroupColumnNames.Contains(col.ColumnName) = False Then
                                strBuilder.Append(strTD & vbCrLf)
                            End If
                        End If
                        'Sohail (09 Apr 2015) -- End

                        blnGrpColumn = False
                        intMergeCols = 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next
                        End If
                    Else
                        blnGrpColumn = False
                        intMergeCols += 1

                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next
                        End If

                        'Sohail (09 Apr 2015) -- Start
                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                        If blnShowSeparateGroupingColumn = True Then
                            'strBuilder.Append(" <TD >" & dtRow(col.ColumnName).ToString & "</TD>" & vbCrLf)
                        End If
                        'Sohail (09 Apr 2015) -- End

                    End If

                Next
                strBuilder.Append(" </TR>" & vbCrLf)
                intExcelRowIndex += 1
            Next


            '*** Group Footer for last group
            If mblnShowSubTotal = True AndAlso blnIsDecimalfield = True AndAlso mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso objDataReader.Columns.Contains(mstrArrayGroupColumnNames(0).Trim) = True Then

                'strBuilder.Append(" <TR valign='middle'> " & vbCrLf) 'Sohail (09 Apr 2015) 


                For idx As Integer = CurrGrpColumns.Count - 1 To 0 Step -1

                    strBuilder.Append(" <TR valign='middle'> " & vbCrLf)

                    blnGrpColumn = False
                    intCountColOrdinal = 1
                    Dim strTD As String = "" 'Sohail (09 Apr 2015) 

                    Dim intColIdx As Integer = 0

                    For Each col As DataColumn In objDataReader.Columns

                        If blnGrpColumn = False Then
                            intColIdx += 1

                            'Dim strTD As String = "" 'Sohail (09 Apr 2015) 
                            If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                                If col.DataType Is System.Type.GetType("System.Int64") Then

                                Else

                                End If

                                If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                    strTD = "=SUM(" & ConvertToExcelColumnLetter(intColIdx) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(intColIdx) & intExcelRowIndex.ToString & ")"
                                Else '*** If other than Last Column group is changed
                                    'Sohail (09 Apr 2015) -- Start
                                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                                    'strTD = "=SUM(" & ConvertToExcelColumnLetter(intColIdx) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(intColIdx) & intExcelRowIndex.ToString & ")"
                                    strTD = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)
                                    'Sohail (09 Apr 2015) -- End
                                End If

                                If dicGroupCells(idx) Is Nothing Then
                                    objGrpCell = New Dictionary(Of Integer, String)
                                Else
                                    objGrpCell = dicGroupCells(idx)
                                End If

                                If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                    objGrpCell.Add(col.Ordinal, "$" & ConvertToExcelColumnLetter(intColIdx) & "$" & (intExcelRowIndex + 1).ToString)
                                Else
                                    objGrpCell.Item(col.Ordinal) += "+$" & ConvertToExcelColumnLetter(intColIdx) & "$" & (intExcelRowIndex + 1).ToString
                                End If

                                dicGroupCells(idx) = objGrpCell
                            Else
                                If col.Ordinal = 0 Then
                                    If mstrReportExcelSubTotalCaption IsNot Nothing AndAlso mstrReportExcelSubTotalCaption.Length - 1 >= idx AndAlso mstrReportExcelSubTotalCaption(idx) IsNot Nothing Then
                                        strTD = mstrReportExcelSubTotalCaption(idx).ToString
                                    Else
                                        strTD = Language.getMessage(mstrModuleName, 11, "Sub Total")
                                    End If
                                ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal Then
                                    If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                        strTD = "=COUNTA(" & ConvertToExcelColumnLetter(intColIdx) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(intColIdx) & intExcelRowIndex.ToString & ")"

                                    Else '*** If other than Last Column group is changed
                                        'Sohail (09 Apr 2015) -- Start
                                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                                        'strTD = "=SUM(" & ConvertToExcelColumnLetter(intColIdx) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(intColIdx) & intExcelRowIndex.ToString & ")"
                                        strTD = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)
                                        'Sohail (09 Apr 2015) -- End
                                    End If

                                    If dicGroupCells(idx) Is Nothing Then
                                        objGrpCell = New Dictionary(Of Integer, String)
                                    Else
                                        objGrpCell = dicGroupCells(idx)
                                    End If

                                    If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                        objGrpCell.Add(col.Ordinal, "$" & ConvertToExcelColumnLetter(intColIdx) & "$" & (intExcelRowIndex + 1).ToString)
                                    Else
                                        objGrpCell.Item(col.Ordinal) += "+$" & ConvertToExcelColumnLetter(intColIdx) & "$" & (intExcelRowIndex + 1).ToString
                                    End If

                                    dicGroupCells(idx) = objGrpCell
                                Else
                                    strTD = "&nbsp;"
                                End If
                            End If

                            blnGrpColumn = False
                            intMergeCols = 1
                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                For Each strCol As String In mstrArrayGroupColumnNames
                                    If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                            blnGrpColumn = True

                                            If col.Ordinal = 0 Then
                                                intCountColOrdinal += 1
                                            Else
                                                intCountColOrdinal = 1
                                            End If
                                        End If
                                    End If
                                Next
                            End If

                            If mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal AndAlso Not (col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64")) Then
                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                'strBuilder.Append(" <TD colspan=" & intMergeCols & " align='center'><B>" & strTD & "</TD></B> " & vbCrLf)
                                If mstrArrayGroupColumnNames.Contains(col.ColumnName) = False Then
                                strBuilder.Append(" <TD colspan=" & intMergeCols & " align='center'><B>" & strTD & "</TD></B> " & vbCrLf)
                            Else
                                    If mstrArrayGroupColumnNames.Contains(col.ColumnName) = False Then
                                        strBuilder.Append(" <TD colspan=" & intMergeCols & " align='center'><B>" & strTD & "</TD></B> " & vbCrLf)
                                    End If
                                End If
                                'Sohail (09 Apr 2015) -- End
                            Else
                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                'strBuilder.Append(" <TD colspan=" & intMergeCols & "><B>" & strTD & "</TD></B> " & vbCrLf)
                                If mstrArrayGroupColumnNames.Contains(col.ColumnName) = False Then
                                    strBuilder.Append(" <TD style='mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";' colspan=" & intMergeCols & "><B>" & strTD & "</TD></B> " & vbCrLf)
                                Else
                                    If mstrArrayGroupColumnNames.Contains(col.ColumnName) = False Then
                                        strBuilder.Append(" <TD style='mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";' colspan=" & intMergeCols & "><B>" & strTD & "</TD></B> " & vbCrLf)
                                    End If
                                End If
                                'Sohail (09 Apr 2015) -- End
                            End If
                        Else
                            blnGrpColumn = False
                            intMergeCols += 1
                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                For Each strCol As String In mstrArrayGroupColumnNames
                                    If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                            blnGrpColumn = True

                                            If intCountColOrdinal > 1 Then
                                                intCountColOrdinal += 1
                                            End If
                                        End If
                                    End If
                                Next
                            End If

                            'Sohail (09 Apr 2015) -- Start
                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                            If blnShowSeparateGroupingColumn = True Then
                                intColIdx += 1
                                If idx = 0 Then
                                    strBuilder.Append(" <TD >" & strTD & "</TD>" & vbCrLf)
                                End If
                            End If
                            'Sohail (09 Apr 2015) -- End

                        End If

                    Next


                    If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Length - 1 >= idx Then
                        objDicGrpFooter = mdicSubTotalExtraInfo(idx)
                        If objDicGrpFooter IsNot Nothing Then
                            For Each pair In objDicGrpFooter
                            Next
                        End If
                    End If

                    'Sohail (09 Apr 2015) -- Start
                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                strBuilder.Append(" </TR> " & vbCrLf)
                intExcelRowIndex += 1

                    For clm As Integer = 0 To intColCount - 1
                        If objDataReader.Columns(mstrArrayGroupColumnNames(idx).Trim).Ordinal = clm Then
                            strBuilder = strBuilder.Replace("##G" & idx & "##", CInt(intExcelRowIndex - objDicGrpHeader.Item(idx)) + 2)
                        End If
                    Next
                    'Sohail (09 Apr 2015) -- End

                Next

            End If

            '*** Grand Total
            If mblnShowGrandTotal = True AndAlso blnIsDecimalfield = True Then
                strBuilder.Append(" <TR valign='middle' >  " & vbCrLf)

                blnGrpColumn = False
                intCountColOrdinal = 1

                For Each col As DataColumn In objDataReader.Columns

                    If blnGrpColumn = False Then

                        If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                            If col.DataType Is System.Type.GetType("System.Int64") Then

                            Else

                            End If


                            If dicGroupCells IsNot Nothing AndAlso dicGroupCells.Count > 0 Then
                                'Pinkal (15-Jan-2020) -- Start
                                'Enhancements -  Working on OT Requisistion Reports for NMB.
                                If dicGroupCells(0) IsNot Nothing Then
                                strBuilder.Append(" <TD style='background-color:blue; color:white; font-weight:bold; font-size:10px;mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";'>=" & dicGroupCells(0).Item(col.Ordinal) & "</TD> " & vbCrLf)
                                End If
                                'Pinkal (15-Jan-2020) -- End
                            Else
                                strBuilder.Append(" <TD style='background-color:blue; color:white; font-weight:bold; font-size:10px;mso-number-format:" & Chr(34) & GUI.fmtCurrency & Chr(34) & ";'>=SUM(" & ConvertToExcelColumnLetter(col.Ordinal + 1) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(col.Ordinal + 1) & intExcelRowIndex.ToString & ")</TD> " & vbCrLf)
                            End If

                        Else
                            If col.Ordinal = 0 Then
                                strBuilder.Append(" <TD style='background-color:blue; color:white; font-weight:bold; font-size:10px;'>" & IIf(mstrReportExcelGrandTotalCaption = "", Language.getMessage(mstrModuleName, 11, "Grand Total"), mstrReportExcelGrandTotalCaption) & "</TD> " & vbCrLf)
                            ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal Then

                                If dicGroupCells IsNot Nothing AndAlso dicGroupCells.Count > 0 Then
                                    'Pinkal (15-Jan-2020) -- Start
                                    'Enhancements -  Working on OT Requisistion Reports for NMB.
                                    If dicGroupCells(0) IsNot Nothing Then
                                    strBuilder.Append(" <TD style='background-color:blue; color:white; font-weight:bold; font-size:10px;' align='center'>=" & dicGroupCells(0).Item(col.Ordinal) & "</TD> " & vbCrLf)
                                    End If
                                    'Pinkal (15-Jan-2020) -- End
                                Else
                                    strBuilder.Append(" <TD style='background-color:blue; color:white; font-weight:bold; font-size:10px;' align='center'>=COUNTA(" & ConvertToExcelColumnLetter(col.Ordinal + 1) & intFunctionStart.ToString & ":" & ConvertToExcelColumnLetter(col.Ordinal + 1) & intExcelRowIndex.ToString & ")</TD> " & vbCrLf)
                                End If
                            Else
                                strBuilder.Append(" <TD style='background-color:blue; color:white; font-weight:bold; font-size:10px;'>&nbsp;</TD> " & vbCrLf)
                            End If
                        End If

                        blnGrpColumn = False
                        intMergeCols = 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                        blnGrpColumn = True

                                        If col.Ordinal = 0 Then
                                            intCountColOrdinal += 1
                                        Else
                                            intCountColOrdinal = 1
                                        End If
                                    End If
                                End If
                            Next
                        End If

                    Else

                        blnGrpColumn = False
                        intMergeCols += 1

                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If objDataReader.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = objDataReader.Columns(strCol.Trim).Ordinal Then
                                        blnGrpColumn = True

                                        If intCountColOrdinal > 1 Then
                                            intCountColOrdinal += 1
                                        End If
                                    End If
                                End If
                            Next
                        End If

                        'Sohail (09 Apr 2015) -- Start
                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                        If blnShowSeparateGroupingColumn = True Then
                            strBuilder.Append(" <TD style='background-color:blue; color:white; font-weight:bold; font-size:10px;'>&nbsp;</TD>" & vbCrLf)
                        End If
                        'Sohail (09 Apr 2015) -- End

                    End If
                Next

                strBuilder.Append(" </TR> " & vbCrLf)
                intExcelRowIndex += 1

                If mdicGrandTotalExtraInfo IsNot Nothing AndAlso mdicGrandTotalExtraInfo.Count > 0 Then
                    For Each pair In mdicGrandTotalExtraInfo

                    Next
                End If
            End If

            '*** Extra Report Footer
            If marrWorksheetRowsExtraReportFooter IsNot Nothing AndAlso marrWorksheetRowsExtraReportFooter.Count > 0 Then
                For Each wrow In marrWorksheetRowsExtraReportFooter
                    strBuilder.Append(wrow)
                Next
            End If

            'Pinkal (13-Apr-2017) -- Start
            'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
            If mblnPrintedByInFooter Then

                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD width='10%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" <TD colspan=" & intColCount - 2 & " align='left' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD width='10%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" <TD colspan=" & intColCount - 2 & " align='left' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD width='10%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                strBuilder.Append(" <b>" & Language.getMessage(mstrModuleName, 37, "Printed By :") & " </B> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" <TD colspan=" & intColCount - 2 & " align='left' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                'strBuilder.Append("<b>" & User._Object._Username & "</b>" & vbCrLf)
                strBuilder.Append("<b>" & IIf(m_strUserName.Trim.Length > 0, m_strUserName, User._Object._Username) & "</b>" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD width='10%' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                strBuilder.Append(" <B>" & Language.getMessage(mstrModuleName, 6, "Date :") & "</B> " & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" <TD colspan=" & intColCount - 2 & " align='left' style='font-size:13px;border-width: 0px;'> " & vbCrLf)
                strBuilder.Append("<b>" & ConfigParameter._Object._CurrentDateAndTime.ToString & "</b>" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

            End If
            'Pinkal (13-Apr-2017) -- End


            strBuilder.Append(" </table> " & vbCrLf)
            strBuilder.Append(" </body> " & vbCrLf)

            Using outfile As New IO.StreamWriter(strExportPath)
                outfile.Write(strBuilder.ToString())
            End Using

            Dim strText As String = IO.File.ReadAllText(strExportPath)
            If strText.IndexOf("#10;") > 0 Then
                strText = strText.Replace("#10;", "&#10;")
                IO.File.WriteAllText(strExportPath, strText)
            End If


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_to_Excel_HTML", mstrModuleName)
            Return False
        End Try
    End Function

    'Sohail (30 Dec 2015) -- Start
    'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
    Private Function Export_to_Excel_Extra_SalaryReconciliation(ByVal strExportPath As String, _
                                                                ByVal dtSummary As System.Data.DataTable, _
                                                                ByVal dtDetail As System.Data.DataTable, _
                                                                ByVal intArrayColumnWidth As Integer(), _
                                                                Optional ByVal mblnShowReportHeader As Boolean = True, _
                                                                Optional ByVal mblnShowColumnHeader As Boolean = True, _
                                                                Optional ByVal mblnShowGrandTotal As Boolean = True, _
                                                                Optional ByVal mstrArrayGroupColumnNames As String() = Nothing, _
                                                                Optional ByVal mstrArrayGroupColumnNamesTableB As String() = Nothing, _
                                                                Optional ByVal mstrReportExcelTypeName As String = "", _
                                                                Optional ByVal mstrReportExcelExtraTitle As String = "", _
                                                                Optional ByVal mstrReportExcelFilterTitle As String = "", _
                                                                Optional ByVal mstrReportExcelSubTotalCaption() As String = Nothing, _
                                                                Optional ByVal mstrReportExcelGrandTotalCaption As String = "", _
                                                                Optional ByVal mblnReportExcelShowRecordCount As Boolean = True, _
                                                                Optional ByVal marrWorksheetRowsExtraReportHeader As ArrayList = Nothing, _
                                                                Optional ByVal marrWorksheetRowsExtraReportFooter As ArrayList = Nothing, _
                                                                Optional ByVal mdicGrandTotalExtraInfo As Dictionary(Of Integer, Object) = Nothing, _
                                                                Optional ByVal mdicSubTotalExtraInfo() As Dictionary(Of Integer, Object) = Nothing, _
                                                                Optional ByVal mblnShowSubTotal As Boolean = True, _
                                                                Optional ByVal blnShowSeparateGroupingColumn As Boolean = False _
                                                                ) As Boolean

        Dim blnIsDecimalfield As Boolean = False
        Dim intColCount As Integer
        Dim intRowCount As Integer
        Dim intTableBColCount As Integer
        Dim intTableBRowCount As Integer
        Dim strGTotalFormula As String = ""

        Try
            'Add a workbook
            Dim book As New ExcelWriter.Workbook

            ' Specify which Sheet should be opened and the size of window by default
            book.ExcelWorkbook.ActiveSheetIndex = 1
            book.ExcelWorkbook.WindowTopX = 100
            book.ExcelWorkbook.WindowTopY = 200
            book.ExcelWorkbook.WindowHeight = 7000
            book.ExcelWorkbook.WindowWidth = 8000

            ' Some optional properties of the Document
            book.Properties.Author = "Aruti"
            book.Properties.Title = "Excel Export"
            book.Properties.Created = DateTime.Now

            ' Add styles for header of the Workbook
            Dim style As WorksheetStyle = book.Styles.Add("HeaderStyle")
            With style
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Font.Color = "White"
                .Interior.Color = "Blue"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With

            Dim styleFtr As WorksheetStyle = book.Styles.Add("FooterStyle")
            With styleFtr
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.WrapText = True
                .Font.Color = "White"
                .Interior.Color = "Blue"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With

            Dim styleFtrN As WorksheetStyle = book.Styles.Add("FooterStyleN")
            With styleFtrN
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.WrapText = True
                .Font.Color = "White"
                .Interior.Color = "Blue"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = GUI.fmtCurrency
            End With

            Dim FooterStyleI As WorksheetStyle = book.Styles.Add("FooterStyleI")
            With FooterStyleI
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.WrapText = True
                .Font.Color = "White"
                .Interior.Color = "Blue"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "0"
            End With

            Dim styleFilterTitle As WorksheetStyle = book.Styles.Add("FilterTitle") 'Without Border
            With styleFilterTitle
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "@"
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With

            'Add styles to the workbook 
            Dim s8 As WorksheetStyle = book.Styles.Add("s8")
            With s8
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            Dim s8w As WorksheetStyle = book.Styles.Add("s8w") 'Without Border
            With s8w
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "@"
            End With

            Dim s8b As WorksheetStyle = book.Styles.Add("s8b")
            With s8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "@"
            End With

            Dim s8bc As WorksheetStyle = book.Styles.Add("s8bc")
            With s8bc
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "@"
            End With

            Dim s8bcf As WorksheetStyle = book.Styles.Add("s8bcf")
            With s8bcf
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Center
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .Interior.Color = "#C5D9F1"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .NumberFormat = "@"
            End With

            Dim s8bw As WorksheetStyle = book.Styles.Add("s8bw") 'Without Borders
            With s8bw
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "@"
            End With

            'Hemant (25 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
            Dim s8bwp As WorksheetStyle = book.Styles.Add("s8bwp")
            With s8bwp
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Font.Color = "Purple"
            End With
            'Hemant (25 Jul 2019) -- End


            Dim s8vc As WorksheetStyle = book.Styles.Add("s8vc") 'Alignment Vertical Center
            With s8vc
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Center
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With

            'Sohail (30 Dec 2015) -- Start
            'Enhancement - Custom Advance Excel for Salary Reconciliation Report for FDRC.
            Dim s8vclr As WorksheetStyle = book.Styles.Add("s8vclr") 'Alignment Vertical Center, Border Left and Right
            With s8vclr
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Center
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .NumberFormat = "General"
            End With
            'Sohail (30 Dec 2015) -- End

            Dim count8 As WorksheetStyle = book.Styles.Add("count8")
            With count8
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = False
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                '.NumberFormat = "General Number"
                .NumberFormat = "0"
            End With

            Dim count8b As WorksheetStyle = book.Styles.Add("count8b")
            With count8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                '.NumberFormat = "General Number"
                .NumberFormat = "0"
            End With

            Dim countFooterStyleN As WorksheetStyle = book.Styles.Add("countFooterStyleN")
            With countFooterStyleN
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Font.Color = "#000000"
                .Font.Color = "White"
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Interior.Color = "Blue"
                .Interior.Pattern = StyleInteriorPattern.Solid
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "0"
            End With

            Dim n8 As WorksheetStyle = book.Styles.Add("n8")
            With n8
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Color = "#000000"
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                '.NumberFormat = "General Number"
                .NumberFormat = GUI.fmtCurrency
            End With

            Dim n8b As WorksheetStyle = book.Styles.Add("n8b")
            With n8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                '.NumberFormat = "General Number"
                .NumberFormat = GUI.fmtCurrency
            End With

            Dim i8 As WorksheetStyle = book.Styles.Add("i8")
            With i8
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = False
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "0"
            End With

            Dim i8b As WorksheetStyle = book.Styles.Add("i8b")
            With i8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "0"
            End With

            'S.SANDEEP [04-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
            Dim i8bw As WorksheetStyle = book.Styles.Add("i8bw")
            With i8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "0"
            End With

            Dim i8w As WorksheetStyle = book.Styles.Add("i8w")
            With i8b
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = False
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "0"
            End With

            Dim n8w As WorksheetStyle = book.Styles.Add("n8w")
            With n8w
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = False
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = GUI.fmtCurrency
            End With

            Dim n8bw As WorksheetStyle = book.Styles.Add("n8bw")
            With n8bw
                .Font.FontName = "Tahoma"
                .Font.Size = 8
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Right
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = GUI.fmtCurrency
            End With
            'S.SANDEEP [04-May-2018] -- END

            Dim s10b As WorksheetStyle = book.Styles.Add("s10b")
            With s10b
                .Font.FontName = "Tahoma"
                .Font.Size = 10
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                .NumberFormat = "@"
            End With

            Dim s10bw As WorksheetStyle = book.Styles.Add("s10bw") 'Without Borders
            With s10bw
                .Font.FontName = "Tahoma"
                .Font.Size = 10
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "@"
            End With



            Dim s9w As WorksheetStyle = book.Styles.Add("s9w") 'Without Borders
            With s9w
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "@"
            End With

            Dim s9bw As WorksheetStyle = book.Styles.Add("s9bw") 'Without Borders
            With s9bw
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Left
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "@"
            End With

            Dim s9wc As WorksheetStyle = book.Styles.Add("s9wc") 'Without Borders Center alignment
            With s9wc
                .Font.FontName = "Tahoma"
                .Font.Size = 9
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.Vertical = StyleVerticalAlignment.Top
                .Alignment.WrapText = True
                .NumberFormat = "@"
            End With

            Dim s12b As WorksheetStyle = book.Styles.Add("s12b")
            With s12b
                .Font.FontName = "Tahoma"
                .Font.Size = 12
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Font.Color = "Purple"
                .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
            End With

            Dim s12bw As WorksheetStyle = book.Styles.Add("s12bw")
            With s12bw
                .Font.FontName = "Tahoma"
                .Font.Size = 12
                .Font.Bold = True
                .Alignment.Horizontal = StyleHorizontalAlignment.Center
                .Alignment.WrapText = True
                .Font.Color = "Purple"
            End With


            ' Add a Worksheet with some data
            Dim sheet As Worksheet = book.Worksheets.Add(IIf(mstrReportExcelTypeName.Trim <> "", Left(mstrReportExcelTypeName.Replace(" ", "_"), 31), Left(m_strReportName.Replace(" ", "_"), 31)))

            'Sohail (09 Apr 2015) -- Start
            'CCK Enhancement - New report Detailed Salary Breakdown report.
            '*** if first columns are grouping column then set first non grouping column as first column in datatable.
            If blnShowSeparateGroupingColumn = False AndAlso mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                If dtSummary.Columns.Contains(dtSummary.Columns(0).ColumnName.Trim) = True Then
                    Dim c As DataColumn = (From p In dtSummary.Columns.Cast(Of DataColumn)() Where (mstrArrayGroupColumnNames.Contains(p.ColumnName) = False) Select (p)).First
                    If c IsNot Nothing Then
                        dtSummary.Columns(c.ColumnName).SetOrdinal(0)
                    End If
                End If
            End If

            Dim intExcelRowIndex As Integer = 0
            Dim intFunctionStart As Integer = 0

            intColCount = dtSummary.Columns.Count
            intRowCount = dtSummary.Rows.Count

            intTableBColCount = dtDetail.Columns.Count + 1
            intTableBRowCount = dtDetail.Rows.Count

            '' we can optionally set some column settings
            If intArrayColumnWidth IsNot Nothing Then
                For Each item In intArrayColumnWidth
                    sheet.Table.Columns.Add(New WorksheetColumn(item))
                Next
            End If
            sheet.Table.Columns(intColCount - 1).Width = 90
            sheet.Table.Columns(intColCount).Width = 30

            'Add row with some properties
            Dim row As WorksheetRow
            Dim wcHeader As WorksheetCell = Nothing

            '*** Column Header
            If mblnShowReportHeader = True Then
                intExcelRowIndex += 1 'Sohail (09 Apr 2015) 
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True
                row.Cells.Add(New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Printed By :"), "s10bw"))
                'row.Cells.Add(New WorksheetCell(User._Object._Username, "s10bw"))
                row.Cells.Add(New WorksheetCell(IIf(m_strUserName.Trim.Length > 0, m_strUserName, User._Object._Username), "s10bw"))
                wcHeader = New WorksheetCell(m_strCompanyName, "s12bw")
                row.Cells.Add(wcHeader)
                If intColCount > 3 Then
                    wcHeader.MergeAcross = intColCount + intTableBColCount - 4
                End If

                intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True
                row.Cells.Add(New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Date :"), "s10bw"))
                'row.Cells.Add(New WorksheetCell(Now.Date.ToShortDateString, "s10bw"))
                row.Cells.Add(New WorksheetCell(ConfigParameter._Object._CurrentDateAndTime.ToString, "s10bw"))
                '*** Report Name
                wcHeader = New WorksheetCell(IIf(mstrReportExcelTypeName.Trim = "", m_strReportName, mstrReportExcelTypeName), "s12bw")
                row.Cells.Add(wcHeader)
                If intColCount > 3 Then
                    wcHeader.MergeAcross = intColCount + intTableBColCount - 4
                End If

                '*** Extra Title
                If mstrReportExcelExtraTitle.Trim <> "" Then
                    Dim arrTitle() As String = mstrReportExcelExtraTitle.Split("|")
                    For Each strTitle As String In arrTitle
                        intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                        row = sheet.Table.Rows.Add
                        row.AutoFitHeight = True
                        row.Cells.Add(New WorksheetCell("", "s10bw"))
                        row.Cells.Add(New WorksheetCell("", "s10bw"))
                        wcHeader = New WorksheetCell(strTitle, "s12bw")
                        row.Cells.Add(wcHeader)
                        If intColCount > 3 Then
                            wcHeader.MergeAcross = intColCount + intTableBColCount - 4
                        End If
                    Next
                End If

                '*** Filter Title
                intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True
                If (mstrReportExcelFilterTitle = "" AndAlso m_strFilterTitle = "") OrElse mstrReportExcelFilterTitle = " " Then
                    wcHeader = New WorksheetCell("", "s8w")
                ElseIf mstrReportExcelFilterTitle.Trim <> "" Then
                    wcHeader = New WorksheetCell(mstrReportExcelFilterTitle, "FilterTitle")
                Else
                    wcHeader = New WorksheetCell(m_strFilterTitle, "FilterTitle")
                End If
                row.Cells.Add(wcHeader)
                wcHeader.MergeAcross = intColCount + intTableBColCount - 2


            End If

            '*** Extra Report Header
            If marrWorksheetRowsExtraReportHeader IsNot Nothing AndAlso marrWorksheetRowsExtraReportHeader.Count > 0 Then
                For Each wrow In marrWorksheetRowsExtraReportHeader
                    wrow.AutoFitHeight = True
                    sheet.Table.Rows.Add(wrow)
                    intExcelRowIndex += 1
                Next
            Else

                If mblnShowReportHeader = True Then
                    '*** Blank Row
                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                    row = sheet.Table.Rows.Add
                    wcHeader = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcHeader)
                    wcHeader.MergeAcross = intColCount + intTableBColCount - 2
                End If
            End If

            Dim blnGrpColumn As Boolean = False
            Dim intMergeCols As Integer = 0

            If mblnShowColumnHeader = True Then

                intExcelRowIndex += 1
                intFunctionStart = intExcelRowIndex
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True

                blnGrpColumn = False


                'Add header text for the columns  
                For Each col As DataColumn In dtSummary.Columns
                    If blnGrpColumn = False Then
                        wcHeader = New WorksheetCell(col.Caption, "HeaderStyle")

                        intMergeCols = 1

                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                        If blnShowSeparateGroupingColumn = False Then
                                            wcHeader.MergeAcross = intMergeCols
                                        End If
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next
                        End If
                        row.Cells.Add(wcHeader)
                    Else

                        blnGrpColumn = False
                        intMergeCols += 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                        If blnShowSeparateGroupingColumn = False Then
                                            wcHeader.MergeAcross = intMergeCols
                                        End If
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next

                            If blnShowSeparateGroupingColumn = True Then
                                wcHeader = New WorksheetCell(col.Caption, "HeaderStyle")
                                row.Cells.Add(wcHeader)
                            End If

                        End If
                    End If

                Next

                'blank cell between two tables
                'wcHeader = New WorksheetCell("", "HeaderStyle")
                'row.Cells.Add(wcHeader)

                blnGrpColumn = False


                'Add header text for the columns  
                For Each col As DataColumn In dtDetail.Columns
                    If blnGrpColumn = False Then
                        wcHeader = New WorksheetCell(col.Caption, "HeaderStyle")

                        intMergeCols = 1

                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                        If blnShowSeparateGroupingColumn = False Then
                                            wcHeader.MergeAcross = intMergeCols
                                        End If
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next
                        End If
                        row.Cells.Add(wcHeader)
                    Else

                        blnGrpColumn = False
                        intMergeCols += 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                        If blnShowSeparateGroupingColumn = False Then
                                            wcHeader.MergeAcross = intMergeCols
                                        End If
                                        blnGrpColumn = True
                                    End If
                                End If
                            Next

                            If blnShowSeparateGroupingColumn = True Then
                                wcHeader = New WorksheetCell(col.Caption, "HeaderStyle")
                                row.Cells.Add(wcHeader)
                            End If

                        End If
                    End If

                Next

                'Freeze the top rows
                sheet.Options.FreezePanes = True
                sheet.Options.SplitHorizontal = sheet.Table.Rows.Count
                sheet.Options.TopRowBottomPane = sheet.Table.Rows.Count
                sheet.Options.ActivePane = 2
            End If

            'Loop through each row in datatable  
            Dim dtRow As DataRow
            Dim wc As WorksheetCell = Nothing
            Dim j As Integer = 0
            Dim CurrGrpColumns As New ArrayList
            Dim PrevGrpColumns As New ArrayList
            Dim CurrGrpColumnsTableB As New ArrayList
            Dim PrevGrpColumnsTableB As New ArrayList
            Dim blnIsGrpChanged As Boolean
            Dim dicGroupCells() As Dictionary(Of Integer, String) = Nothing
            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                ReDim dicGroupCells(mstrArrayGroupColumnNames.Count - 1)
            End If
            Dim objGrpCell As New Dictionary(Of Integer, String)
            Dim objDicGrpFooter As New Dictionary(Of Integer, Object)
            Dim idxChangedGroupColumn As Integer = 0
            Dim intCountColOrdinal As Integer = 1
            Dim objDicGrpHeader As New Dictionary(Of Integer, String) 'Sohail (09 Apr 2015) 

            CurrGrpColumnsTableB.Clear()
            'For Each strColName As String In mstrArrayGroupColumnNames
            '    CurrGrpColumns.Add(dtRow.Item(strColName).ToString)
            'Next

            For i = 0 To intRowCount - 1
                dtRow = dtSummary.Rows(i)

                If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then

                    blnIsGrpChanged = False
                    CurrGrpColumns.Clear()
                    For Each strColName As String In mstrArrayGroupColumnNames
                        CurrGrpColumns.Add(dtRow.Item(strColName).ToString)
                    Next

                    '*** Check which group column is changed
                    If PrevGrpColumns.Count > 0 Then
                        For idx As Integer = 0 To CurrGrpColumns.Count - 1
                            If CurrGrpColumns(idx) <> PrevGrpColumns(idx) Then
                                idxChangedGroupColumn = idx
                                blnIsGrpChanged = True
                                Exit For
                            End If
                        Next
                    End If

                    If dtSummary.Columns.Contains(mstrArrayGroupColumnNames(0).Trim) = True Then


                        j += 1

                        '*** Group Footer (If any group column changed)  
                        If mblnShowSubTotal = True AndAlso (blnIsGrpChanged = True) AndAlso blnIsDecimalfield = True AndAlso i > 0 Then


                            '*** Loop through last group column to first group column
                            For idx As Integer = CurrGrpColumns.Count - 1 To 0 Step -1

                                '*** If parent group column is changed, show group footer for all child group column
                                If idx >= idxChangedGroupColumn OrElse CurrGrpColumns(idx) <> PrevGrpColumns(idx) Then

                                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                                    row = sheet.Table.Rows.Add
                                    row.AutoFitHeight = True

                                    blnGrpColumn = False
                                    intCountColOrdinal = 1

                                    For Each col As DataColumn In dtSummary.Columns

                                        If blnGrpColumn = False Then
                                            If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                                                'Sohail (15 Mar 2014) - [Int64]
                                                wc = New WorksheetCell()
                                                'Sohail (15 Mar 2014) -- Start
                                                'Enhancement - SUM function for Integer Column.
                                                'wc.StyleID = "n8b"
                                                If col.DataType Is System.Type.GetType("System.Int64") Then
                                                    wc.StyleID = "i8b"
                                                Else
                                                    wc.StyleID = "n8b"
                                                End If
                                                'Sohail (15 Mar 2014) -- End
                                                wc.Data.Type = DataType.Number

                                                If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                                    'wc.Formula = "=SUM(R[-" & (j - 1) & "]C:R[-1]C)"
                                                    wc.Formula = "=R[-1]C-R[-" & (j - 1) & "]C"

                                                Else '*** If other than Last Column group is changed
                                                    wc.Formula = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)

                                                End If


                                                If dicGroupCells(idx) Is Nothing Then
                                                    objGrpCell = New Dictionary(Of Integer, String)
                                                Else
                                                    objGrpCell = dicGroupCells(idx)
                                                End If

                                                If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                                    objGrpCell.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
                                                Else
                                                    objGrpCell.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
                                                End If

                                                dicGroupCells(idx) = objGrpCell

                                            Else
                                                If col.Ordinal = 0 Then
                                                    If mstrReportExcelSubTotalCaption IsNot Nothing AndAlso mstrReportExcelSubTotalCaption.Length - 1 >= idx AndAlso mstrReportExcelSubTotalCaption(idx) IsNot Nothing Then
                                                        wc = New WorksheetCell(mstrReportExcelSubTotalCaption(idx).ToString, DataType.String, "s8b")
                                                    Else
                                                        'wc = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Sub Total"), DataType.String, "s8b")
                                                        wc = New WorksheetCell("", DataType.String, "s8b")
                                                    End If
                                                ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal Then
                                                    If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                                        wc = New WorksheetCell("Variance", "s8b")
                                                        'wc = New WorksheetCell()
                                                        'wc.StyleID = "count8b"
                                                        'wc.Formula = "=COUNTA(R[-" & (j - 1) & "]C:R[-1]C)"

                                                    Else '*** If other than Last Column group is changed
                                                        wc = New WorksheetCell()
                                                        wc.StyleID = "count8b"
                                                        wc.Formula = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)
                                                    End If

                                                    If dicGroupCells(idx) Is Nothing Then
                                                        objGrpCell = New Dictionary(Of Integer, String)
                                                    Else
                                                        objGrpCell = dicGroupCells(idx)
                                                    End If

                                                    If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                                        objGrpCell.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
                                                    Else
                                                        objGrpCell.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
                                                    End If

                                                    dicGroupCells(idx) = objGrpCell
                                                Else
                                                    wc = New WorksheetCell("", DataType.String, "s8b")
                                                End If
                                            End If

                                            blnGrpColumn = False
                                            intMergeCols = 1
                                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                                For Each strCol As String In mstrArrayGroupColumnNames
                                                    If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                                        If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                                            'Sohail (09 Apr 2015) -- Start
                                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                            'wc.MergeAcross = intMergeCols
                                                            If blnShowSeparateGroupingColumn = False Then
                                                                wc.MergeAcross = intMergeCols
                                                            Else
                                                                If objDicGrpHeader.ContainsKey(idx) = True Then
                                                                    'Sohail (26 Jun 2015) -- Start
                                                                    'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                                                    'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                                    'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).Data.Text = sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text
                                                                    sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                                    'Sohail (26 Jun 2015) -- End
                                                                End If
                                                            End If
                                                            'Sohail (09 Apr 2015) -- End
                                                            blnGrpColumn = True

                                                            If col.Ordinal = 0 Then
                                                                intCountColOrdinal += 1
                                                            Else
                                                                intCountColOrdinal = 1
                                                            End If
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            row.Cells.Add(wc)
                                        Else

                                            blnGrpColumn = False
                                            intMergeCols += 1
                                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                                For Each strCol As String In mstrArrayGroupColumnNames
                                                    If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                                        If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                                            'Sohail (09 Apr 2015) -- Start
                                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                            'wc.MergeAcross = intMergeCols
                                                            If blnShowSeparateGroupingColumn = False Then
                                                                wc.MergeAcross = intMergeCols
                                                            Else
                                                                If objDicGrpHeader.ContainsKey(idx) = True Then
                                                                    'Sohail (26 Jun 2015) -- Start
                                                                    'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                                                    'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                                    'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).Data.Text = sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text
                                                                    sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                                    'Sohail (26 Jun 2015) -- End
                                                                End If
                                                            End If
                                                            'Sohail (09 Apr 2015) -- End
                                                            blnGrpColumn = True

                                                            If intCountColOrdinal > 1 Then
                                                                intCountColOrdinal += 1
                                                            End If
                                                        End If
                                                    End If
                                                Next

                                                'Sohail (09 Apr 2015) -- Start
                                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                                If blnShowSeparateGroupingColumn = True Then
                                                    wc = New WorksheetCell(row.Cells(0).Data.Text, DataType.String, "s8b")
                                                    row.Cells.Add(wc)
                                                End If
                                                'Sohail (09 Apr 2015) -- End

                                            End If
                                        End If

                                    Next

                                    'Sohail (26 Jun 2015) -- Start
                                    'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso blnShowSeparateGroupingColumn = True Then
                                        If sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Data.Text Is Nothing AndAlso sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Formula Is Nothing Then
                                            sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Data.Text = sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Data.Text
                                            sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Formula = sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Formula
                                        End If
                                        For delidx = idx To 0 Step -1
                                            'sheet.Table.Rows(intExcelRowIndex - 1).Cells.RemoveAt(0)
                                        Next
                                        'sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Index = idx + 2
                                    End If
                                    'Sohail (26 Jun 2015) -- End

                                    If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Length - 1 >= idx Then
                                        objDicGrpFooter = mdicSubTotalExtraInfo(idx)
                                        If objDicGrpFooter IsNot Nothing Then
                                            For Each pair In objDicGrpFooter
                                                wc = New WorksheetCell(pair.Value, "s8b")
                                                sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
                                            Next
                                        End If
                                    End If

                                End If


                            Next

                            For sr As Integer = idxChangedGroupColumn + 1 To dicGroupCells.Count - 1
                                dicGroupCells(sr) = Nothing
                            Next

                            j = 1

                            'Table B
                            Dim a As List(Of DataRow) = (From p In dtDetail Where (p.Item("Column10").ToString = PrevGrpColumns(0).ToString) Select (p)).ToList
                            Dim b As List(Of String) = (From p In a Select (p.Item("Column5").ToString)).Distinct().ToList

                            If intExcelRowIndex <= (intFunctionStart + a.Count + b.Count) Then
                                For clm As Integer = intExcelRowIndex To (intFunctionStart + a.Count + b.Count)

                                    intExcelRowIndex += 1
                                    row = sheet.Table.Rows.Add
                                    row.AutoFitHeight = True

                                    'wcHeader = New WorksheetCell("", "s8w")
                                    'row.Cells.Add(wcHeader)

                                    wcHeader = New WorksheetCell("", "s8vclr")
                                    wcHeader.MergeAcross = intColCount - 1
                                    row.Cells.Add(wcHeader)
                                Next
                            Else
                                If a.Count <= 0 Then
                                    For clm As Integer = intFunctionStart To intExcelRowIndex - 1
                                        wcHeader = New WorksheetCell("", "s8w")
                                        sheet.Table.Rows(clm).Cells.Add(wcHeader)

                                        wcHeader = New WorksheetCell("", "s8vclr")
                                        wcHeader.MergeAcross = intTableBColCount - 3
                                        sheet.Table.Rows(clm).Cells.Add(wcHeader)
                                    Next
                                End If
                            End If

                            Dim strCurrGrp As String = ""
                            Dim strPrevGrp As String = ""
                            Dim dsRow As DataRow = Nothing
                            Dim strTotalFormula As String = ""
                            Dim intExcelRowIndexB As Integer = intFunctionStart - 1
                            For iB As Integer = 0 To a.Count - 1
                                dsRow = a(iB)

                                '*** Group Header
                                Dim blnIsChangedB As Boolean = False
                                Dim strStyleB As String
                                Dim intGrpSpaceB As Integer
                                strCurrGrp = dsRow.Item("Column5").ToString
                                'For idx As Integer = 0 To b.Count - 1

                                If strPrevGrp = "" OrElse strPrevGrp <> strCurrGrp Then
                                    intExcelRowIndexB += 1
                                    'intFunctionStart = intExcelRowIndexb

                                    'Blank cell between 2 tables
                                    wcHeader = New WorksheetCell("", "s8w")
                                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                    'If objDicGrpHeader.ContainsKey(idx) = False Then
                                    '    objDicGrpHeader.Add(idx, intFunctionStart)
                                    'Else
                                    '    objDicGrpHeader.Item(idx) = intFunctionStart
                                    'End If
                                    'row = sheet.Table.Rows.Add
                                    'row.AutoFitHeight = True
                                    strStyleB = "s8b"
                                    'intGrpSpaceB = idx - 1

                                    If dtDetail.Columns("Column5").Caption = "" Then
                                        wcHeader = New WorksheetCell(dsRow.Item("Column5").ToString, strStyleB)
                                    Else
                                        wcHeader = New WorksheetCell(Space(4 * intGrpSpaceB) & dtDetail.Columns("Column5").Caption.Replace(" :", "") & " : " & dsRow.Item("Column5").ToString, strStyleB)
                                    End If
                                    'If blnShowSeparateGroupingColumn = True AndAlso idx > 0 Then
                                    '    wcHeader.Index = idx + 1
                                    'End If
                                    'row.Cells.Add(wcHeader)
                                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                    If blnShowSeparateGroupingColumn = False Then
                                        wcHeader.MergeAcross = intColCount - 1
                                    Else
                                        wcHeader.MergeAcross = intTableBColCount - 3
                                        'For clm As Integer = idx + 1 To intColCount - 1
                                        '    wcHeader = New WorksheetCell("", strStyleB)
                                        '    row.Cells.Add(wcHeader)
                                        'Next

                                        'wcHeader = New WorksheetCell("", "s8w")
                                        'row.Cells.Add(wcHeader)

                                        'For clm As Integer = idx + 1 To intTableBColCount - 1
                                        '    wcHeader = New WorksheetCell("", strStyleB)
                                        '    row.Cells.Add(wcHeader)
                                        'Next
                                    End If

                                    blnIsChangedB = True
                                End If
                                'Next

                                'Blank cell between 2 tables
                                intExcelRowIndexB += 1

                                wcHeader = New WorksheetCell("", "s8w")
                                sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell("", "s8vclr")
                                sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell(dsRow.Item("Column3").ToString, "s8")
                                sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell(dsRow.Item("Column81").ToString, "n8")
                                sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                strTotalFormula += "+" & "R" & intExcelRowIndexB + 1 & "C" & DirectCast(DirectCast(DirectCast(sheet.Table.Rows(intExcelRowIndexB), ExcelWriter.WorksheetRow).Table, ExcelWriter.WorksheetTable).Columns, ExcelWriter.WorksheetColumnCollection).Count

                                strPrevGrp = strCurrGrp
                            Next
                            If strTotalFormula.Trim <> "" Then
                                strTotalFormula = strTotalFormula.Substring(1)
                            End If

                            If a.Count > 0 Then
                                intExcelRowIndexB += 1

                                wcHeader = New WorksheetCell("", "s8w")
                                sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell("Total", "s8b")
                                sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell("", "s8")
                                sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell("", "n8b")
                                wcHeader.Formula = "=" & strTotalFormula
                                sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                                strGTotalFormula += "+" & "R" & intExcelRowIndexB + 1 & "C" & DirectCast(DirectCast(DirectCast(sheet.Table.Rows(intExcelRowIndexB), ExcelWriter.WorksheetRow).Table, ExcelWriter.WorksheetTable).Columns, ExcelWriter.WorksheetColumnCollection).Count
                            End If
                        End If

                        '*** Group Header
                        Dim blnIsChanged As Boolean = False
                        Dim strStyle As String
                        Dim intGrpSpace As Integer
                        For idx As Integer = 0 To CurrGrpColumns.Count - 1
                            If PrevGrpColumns.Count = 0 OrElse blnIsChanged = True OrElse CurrGrpColumns(idx) <> PrevGrpColumns(idx) Then
                                intExcelRowIndex += 1
                                intFunctionStart = intExcelRowIndex
                                If objDicGrpHeader.ContainsKey(idx) = False Then
                                    objDicGrpHeader.Add(idx, intFunctionStart)
                                Else
                                    objDicGrpHeader.Item(idx) = intFunctionStart
                                End If
                                row = sheet.Table.Rows.Add
                                row.AutoFitHeight = True
                                strStyle = "s8b"
                                intGrpSpace = idx - 1

                                If idx = 0 Then
                                    intGrpSpace = 0
                                    If mstrArrayGroupColumnNames.Length > 1 Then
                                        strStyle = "s8bcf"
                                    End If
                                End If
                                If dtSummary.Columns(mstrArrayGroupColumnNames(idx).Trim).Caption = "" Then
                                    wcHeader = New WorksheetCell(Space(4 * intGrpSpace) & CurrGrpColumns(idx).ToString, strStyle)
                                Else
                                    wcHeader = New WorksheetCell(Space(4 * intGrpSpace) & dtSummary.Columns(mstrArrayGroupColumnNames(idx).Trim).Caption.Replace(" :", "") & " : " & CurrGrpColumns(idx).ToString, strStyle)
                                End If
                                'Sohail (26 Jun 2015) -- Start
                                'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                If blnShowSeparateGroupingColumn = True AndAlso idx > 0 Then
                                    wcHeader.Index = idx + 1
                                End If
                                'Sohail (26 Jun 2015) -- End
                                row.Cells.Add(wcHeader)
                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                'wcHeader.MergeAcross = intColCount - 1
                                If blnShowSeparateGroupingColumn = False Then
                                    wcHeader.MergeAcross = intColCount - 1
                                Else
                                    'Sohail (26 Jun 2015) -- Start
                                    'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                    'For clm As Integer = 1 To intColCount - 1
                                    '    wcHeader = New WorksheetCell("", strStyle)
                                    '    row.Cells.Add(wcHeader)
                                    'Next
                                    'For clm As Integer = idx + 1 To intColCount - 1
                                    '    wcHeader = New WorksheetCell("", strStyle)
                                    '    row.Cells.Add(wcHeader)
                                    'Next

                                    'wcHeader = New WorksheetCell("", "s8w")
                                    'row.Cells.Add(wcHeader)

                                    'For clm As Integer = idx + 1 To intTableBColCount - 1
                                    '    wcHeader = New WorksheetCell("", strStyle)
                                    '    row.Cells.Add(wcHeader)
                                    'Next
                                    wcHeader.MergeAcross = intColCount + intTableBColCount - 2
                                End If

                                blnIsChanged = True
                            End If
                        Next

                    End If


                    'Add row to the excel sheet
                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                    row = sheet.Table.Rows.Add
                    row.AutoFitHeight = True

                    blnGrpColumn = False

                    'Loop through each column
                    For Each col As DataColumn In dtSummary.Columns
                        If blnGrpColumn = False Then
                            If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then
                                wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.Number, "n8")
                                blnIsDecimalfield = True
                                'Sohail (15 Mar 2014) -- Start
                                'Enhancement - SUM function for Integer Column.
                                'ElseIf col.DataType Is System.Type.GetType("System.Int32") OrElse col.DataType Is System.Type.GetType("System.Integer") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                            ElseIf col.DataType Is System.Type.GetType("System.Int32") OrElse col.DataType Is System.Type.GetType("System.Integer") Then
                                wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.Number, "count8")
                                blnIsDecimalfield = True
                            ElseIf col.DataType Is System.Type.GetType("System.Int64") Then
                                wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.Number, "i8")
                                blnIsDecimalfield = True
                                'Sohail (15 Mar 2014) -- End
                            Else
                                wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.String, "s8")
                            End If


                            blnGrpColumn = False
                            intMergeCols = 1
                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                For Each strCol As String In mstrArrayGroupColumnNames
                                    If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                                wc.MergeAcross = intMergeCols
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                            blnGrpColumn = True
                                        Else
                                            If blnShowSeparateGroupingColumn = True AndAlso col.Ordinal = 0 Then
                                                wc.StyleID = "s8vclr"
                                                wc.Data.Text = ""
                                            End If
                                        End If
                                    End If
                                Next
                            End If
                            'Sohail (26 Jun 2015) -- Start
                            'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                            'row.Cells.Add(wc)
                            If blnShowSeparateGroupingColumn = False Then
                                row.Cells.Add(wc)
                            Else
                                If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso col.Ordinal < mstrArrayGroupColumnNames.Length Then
                                    'Do nothing
                                    row.Cells.Add(wc)
                                Else
                                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso col.Ordinal = mstrArrayGroupColumnNames.Length Then
                                        wc.Index = col.Ordinal + 1
                                    End If
                                    row.Cells.Add(wc)
                                End If
                            End If
                            'Sohail (26 Jun 2015) -- End
                        Else
                            blnGrpColumn = False
                            intMergeCols += 1

                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                For Each strCol As String In mstrArrayGroupColumnNames
                                    If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                                wc.MergeAcross = intMergeCols
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                            blnGrpColumn = True
                                        End If
                                    End If
                                Next

                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                If blnShowSeparateGroupingColumn = True Then
                                    wc = New WorksheetCell(dtRow(col.ColumnName).ToString, DataType.String, "s8")
                                    'row.Cells.Add(wc) 'Sohail (26 Jun 2015)
                                End If
                                'Sohail (09 Apr 2015) -- End

                            End If
                        End If

                    Next

                    PrevGrpColumns = CurrGrpColumns.Clone
                End If

            Next

            '*** Group Footer for last group
            If mblnShowSubTotal = True AndAlso blnIsDecimalfield = True AndAlso mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso dtSummary.Columns.Contains(mstrArrayGroupColumnNames(0).Trim) = True Then


                For idx As Integer = CurrGrpColumns.Count - 1 To 0 Step -1
                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                    row = sheet.Table.Rows.Add
                    row.AutoFitHeight = True

                    blnGrpColumn = False
                    intCountColOrdinal = 1

                    For Each col As DataColumn In dtSummary.Columns

                        If blnGrpColumn = False Then


                            If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                                'Sohail (15 Mar 2014) - [Int64]
                                wc = New WorksheetCell()
                                'Sohail (15 Mar 2014) -- Start
                                'Enhancement - SUM function for Integer Columne.
                                'wc.StyleID = "n8b"
                                If col.DataType Is System.Type.GetType("System.Int64") Then
                                    wc.StyleID = "i8b"
                                Else
                                    wc.StyleID = "n8b"
                                End If
                                'Sohail (15 Mar 2014) -- End
                                wc.Data.Type = DataType.Number

                                If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                    'wc.Formula = "=SUM(R[-" & (j) & "]C:R[-1]C)"
                                    wc.Formula = "=R[-1]C-R[-" & (j) & "]C"

                                Else '*** If other than Last Column group is changed
                                    wc.Formula = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)

                                End If

                                If dicGroupCells(idx) Is Nothing Then
                                    objGrpCell = New Dictionary(Of Integer, String)
                                Else
                                    objGrpCell = dicGroupCells(idx)
                                End If

                                If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                    objGrpCell.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
                                Else
                                    objGrpCell.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
                                End If

                                dicGroupCells(idx) = objGrpCell
                            Else
                                If col.Ordinal = 0 Then
                                    If mstrReportExcelSubTotalCaption IsNot Nothing AndAlso mstrReportExcelSubTotalCaption.Length - 1 >= idx AndAlso mstrReportExcelSubTotalCaption(idx) IsNot Nothing Then
                                        wc = New WorksheetCell(mstrReportExcelSubTotalCaption(idx).ToString, DataType.String, "s8b")
                                    Else
                                        'wc = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Sub Total"), DataType.String, "s8b")
                                        wc = New WorksheetCell("", DataType.String, "s8b")
                                    End If
                                ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal Then
                                    If idx = CurrGrpColumns.Count - 1 Then '*** If Last Column group is changed
                                        'wc = New WorksheetCell()
                                        'wc.StyleID = "count8b"
                                        'wc.Formula = "=COUNTA(R[-" & (j) & "]C:R[-1]C)"
                                        wc = New WorksheetCell("Variance", "s8b")

                                    Else '*** If other than Last Column group is changed
                                        wc = New WorksheetCell()
                                        wc.StyleID = "count8b"
                                        wc.Formula = "=" & dicGroupCells(idx + 1).Item(col.Ordinal)

                                    End If

                                    If dicGroupCells(idx) Is Nothing Then
                                        objGrpCell = New Dictionary(Of Integer, String)
                                    Else
                                        objGrpCell = dicGroupCells(idx)
                                    End If

                                    If objGrpCell.ContainsKey(col.Ordinal) = False Then
                                        objGrpCell.Add(col.Ordinal, "R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1)
                                    Else
                                        objGrpCell.Item(col.Ordinal) += "+R" & sheet.Table.Rows.Count & "C" & col.Ordinal + 1
                                    End If

                                    dicGroupCells(idx) = objGrpCell
                                Else
                                    wc = New WorksheetCell("", DataType.String, "s8b")
                                End If
                            End If

                            blnGrpColumn = False
                            intMergeCols = 1
                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                For Each strCol As String In mstrArrayGroupColumnNames
                                    If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                                wc.MergeAcross = intMergeCols
                                            Else
                                                If objDicGrpHeader.ContainsKey(idx) = True Then
                                                    'Sohail (26 Jun 2015) -- Start
                                                    'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                                    'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                    'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text = sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text
                                                    sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                End If
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                            blnGrpColumn = True

                                            If col.Ordinal = 0 Then
                                                intCountColOrdinal += 1
                                            Else
                                                intCountColOrdinal = 1
                                            End If
                                        End If
                                    End If
                                Next

                            End If

                            row.Cells.Add(wc)
                        Else
                            blnGrpColumn = False
                            intMergeCols += 1
                            If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                                For Each strCol As String In mstrArrayGroupColumnNames
                                    If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                        If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                            'Sohail (09 Apr 2015) -- Start
                                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                                            'wc.MergeAcross = intMergeCols
                                            If blnShowSeparateGroupingColumn = False Then
                                                wc.MergeAcross = intMergeCols
                                            Else
                                                If objDicGrpHeader.ContainsKey(idx) = True Then
                                                    'Sohail (26 Jun 2015) -- Start
                                                    'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                                                    'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                    'sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(objDataReader(intTables).Columns.IndexOf(mstrArrayGroupColumnNames(idx))).Data.Text = sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).Data.Text
                                                    sheet.Table.Rows(objDicGrpHeader.Item(idx) - 1).Cells(0).MergeDown = intExcelRowIndex - objDicGrpHeader.Item(idx)
                                                    'Sohail (26 Jun 2015) -- End
                                                End If
                                            End If
                                            'Sohail (09 Apr 2015) -- End
                                            blnGrpColumn = True

                                            If intCountColOrdinal > 1 Then
                                                intCountColOrdinal += 1
                                            End If
                                        End If
                                    End If
                                Next

                                'Sohail (09 Apr 2015) -- Start
                                'CCK Enhancement - New report Detailed Salary Breakdown report.
                                If blnShowSeparateGroupingColumn = True Then
                                    wc = New WorksheetCell(row.Cells(0).Data.Text, DataType.String, "s8b")
                                    row.Cells.Add(wc)
                                End If
                                'Sohail (09 Apr 2015) -- End
                            End If
                        End If

                    Next


                    'Sohail (26 Jun 2015) -- Start
                    'Issue - Row Merge issue in MS Office Excel in Detailed Salary Breakdown Report.
                    If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 AndAlso blnShowSeparateGroupingColumn = True Then
                        If sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Data.Text Is Nothing AndAlso sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Formula Is Nothing Then
                            sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Data.Text = sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Data.Text
                            sheet.Table.Rows(intExcelRowIndex - 1).Cells(idx + 1).Formula = sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Formula
                        End If
                        For delidx = idx To 0 Step -1
                            'sheet.Table.Rows(intExcelRowIndex - 1).Cells.RemoveAt(0)
                        Next
                        'sheet.Table.Rows(intExcelRowIndex - 1).Cells(0).Index = idx + 2
                    End If
                    'Sohail (26 Jun 2015) -- End

                    If mdicSubTotalExtraInfo IsNot Nothing AndAlso mdicSubTotalExtraInfo.Length - 1 >= idx Then
                        objDicGrpFooter = mdicSubTotalExtraInfo(idx)
                        If objDicGrpFooter IsNot Nothing Then
                            For Each pair In objDicGrpFooter
                                wc = New WorksheetCell(pair.Value, "s8b")
                                sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
                                intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                            Next
                        End If
                    End If
                Next

                'Table B
                Dim a As List(Of DataRow) = (From p In dtDetail Where (p.Item("Column10").ToString = PrevGrpColumns(0).ToString) Select (p)).ToList
                Dim b As List(Of String) = (From p In a Select (p.Item("Column5").ToString)).Distinct().ToList

                If intExcelRowIndex < (intFunctionStart + a.Count + b.Count) Then
                    For clm As Integer = intExcelRowIndex To (intFunctionStart + a.Count + b.Count)

                        intExcelRowIndex += 1
                        row = sheet.Table.Rows.Add
                        row.AutoFitHeight = True

                        'wcHeader = New WorksheetCell("", "s8w")
                        'row.Cells.Add(wcHeader)

                        wcHeader = New WorksheetCell("", "s8vclr")
                        wcHeader.MergeAcross = intColCount - 1
                        row.Cells.Add(wcHeader)
                    Next
                Else
                    If a.Count <= 0 Then
                        For clm As Integer = intFunctionStart To intExcelRowIndex - 1

                            If clm = intExcelRowIndex - 1 Then
                                wcHeader = New WorksheetCell("", "s8")
                                sheet.Table.Rows(clm).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell("", "s8b")
                                sheet.Table.Rows(clm).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell("", "s8")
                                sheet.Table.Rows(clm).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell("", "n8b")
                                sheet.Table.Rows(clm).Cells.Add(wcHeader)
                            Else
                                wcHeader = New WorksheetCell("", "s8w")
                                sheet.Table.Rows(clm).Cells.Add(wcHeader)

                                wcHeader = New WorksheetCell("", "s8vclr")
                                wcHeader.MergeAcross = intTableBColCount - 3
                                sheet.Table.Rows(clm).Cells.Add(wcHeader)
                            End If

                        Next
                    End If
                End If

                Dim strCurrGrp As String = ""
                Dim strPrevGrp As String = ""
                Dim dsRow As DataRow = Nothing
                Dim strTotalFormula As String = ""
                Dim intExcelRowIndexB As Integer = intFunctionStart - 1
                For iB As Integer = 0 To a.Count - 1
                    dsRow = a(iB)

                    '*** Group Header
                    Dim blnIsChangedB As Boolean = False
                    Dim strStyleB As String
                    Dim intGrpSpaceB As Integer
                    strCurrGrp = dsRow.Item("Column5").ToString
                    'For idx As Integer = 0 To b.Count - 1

                    If strPrevGrp = "" OrElse strPrevGrp <> strCurrGrp Then
                        intExcelRowIndexB += 1
                        'intFunctionStart = intExcelRowIndexb

                        'Blank cell between 2 tables
                        wcHeader = New WorksheetCell("", "s8w")
                        sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                        'If objDicGrpHeader.ContainsKey(idx) = False Then
                        '    objDicGrpHeader.Add(idx, intFunctionStart)
                        'Else
                        '    objDicGrpHeader.Item(idx) = intFunctionStart
                        'End If
                        'row = sheet.Table.Rows.Add
                        'row.AutoFitHeight = True
                        strStyleB = "s8b"
                        'intGrpSpaceB = idx - 1

                        If dtDetail.Columns("Column5").Caption = "" Then
                            wcHeader = New WorksheetCell(dsRow.Item("Column5").ToString, strStyleB)
                        Else
                            wcHeader = New WorksheetCell(Space(4 * intGrpSpaceB) & dtDetail.Columns("Column5").Caption.Replace(" :", "") & " : " & dsRow.Item("Column5").ToString, strStyleB)
                        End If
                        'If blnShowSeparateGroupingColumn = True AndAlso idx > 0 Then
                        '    wcHeader.Index = idx + 1
                        'End If
                        'row.Cells.Add(wcHeader)
                        sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                        If blnShowSeparateGroupingColumn = False Then
                            wcHeader.MergeAcross = intColCount - 1
                        Else
                            wcHeader.MergeAcross = intTableBColCount - 3
                            'For clm As Integer = idx + 1 To intColCount - 1
                            '    wcHeader = New WorksheetCell("", strStyleB)
                            '    row.Cells.Add(wcHeader)
                            'Next

                            'wcHeader = New WorksheetCell("", "s8w")
                            'row.Cells.Add(wcHeader)

                            'For clm As Integer = idx + 1 To intTableBColCount - 1
                            '    wcHeader = New WorksheetCell("", strStyleB)
                            '    row.Cells.Add(wcHeader)
                            'Next
                        End If

                        blnIsChangedB = True
                    End If
                    'Next

                    'Blank cell between 2 tables
                    intExcelRowIndexB += 1

                    wcHeader = New WorksheetCell("", "s8w")
                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                    wcHeader = New WorksheetCell("", "s8vclr")
                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                    wcHeader = New WorksheetCell(dsRow.Item("Column3").ToString, "s8")
                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                    wcHeader = New WorksheetCell(dsRow.Item("Column81").ToString, "n8")
                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                    strTotalFormula += "+" & "R" & intExcelRowIndexB + 1 & "C" & DirectCast(DirectCast(DirectCast(sheet.Table.Rows(intExcelRowIndexB), ExcelWriter.WorksheetRow).Table, ExcelWriter.WorksheetTable).Columns, ExcelWriter.WorksheetColumnCollection).Count

                    strPrevGrp = strCurrGrp
                Next
                If strTotalFormula.Trim <> "" Then
                    strTotalFormula = strTotalFormula.Substring(1)
                End If

                If a.Count > 0 Then
                    intExcelRowIndexB += 1

                    sheet.Table.Rows(intExcelRowIndexB).Cells(0).StyleID = "s8"

                    wcHeader = New WorksheetCell("", "s8")
                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                    wcHeader = New WorksheetCell("Total", "s8b")
                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                    wcHeader = New WorksheetCell("", "s8")
                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                    wcHeader = New WorksheetCell("", "n8b")
                    wcHeader.Formula = "=" & strTotalFormula
                    sheet.Table.Rows(intExcelRowIndexB).Cells.Add(wcHeader)

                    strGTotalFormula += "+" & "R" & intExcelRowIndexB + 1 & "C" & DirectCast(DirectCast(DirectCast(sheet.Table.Rows(intExcelRowIndexB), ExcelWriter.WorksheetRow).Table, ExcelWriter.WorksheetTable).Columns, ExcelWriter.WorksheetColumnCollection).Count
                End If

            End If

            '*** Grand Total
            If mblnShowGrandTotal = True AndAlso blnIsDecimalfield = True Then
                intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                row = sheet.Table.Rows.Add
                row.AutoFitHeight = True

                blnGrpColumn = False
                intCountColOrdinal = 1

                For Each col As DataColumn In dtSummary.Columns

                    If blnGrpColumn = False Then

                        If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                            'Sohail (15 Mar 2014) - [Int64]
                            wc = New WorksheetCell()
                            'Sohail (15 Mar 2014) -- Start
                            'Enhancement - Cost Center Report Branch Wise.
                            'wc.StyleID = "FooterStyleN"
                            If col.DataType Is System.Type.GetType("System.Int64") Then
                                wc.StyleID = "FooterStyleI"
                            Else
                                wc.StyleID = "FooterStyleN"
                            End If
                            'Sohail (15 Mar 2014) -- End
                            wc.Data.Type = DataType.Number

                            If dicGroupCells IsNot Nothing AndAlso dicGroupCells.Count > 0 Then

                                'Pinkal (15-Jan-2020) -- Start
                                'Enhancements -  Working on OT Requisistion Reports for NMB.
                                If dicGroupCells(0) IsNot Nothing Then
                                wc.Formula = "=" & dicGroupCells(0).Item(col.Ordinal)
                                End If
                                'Pinkal (15-Jan-2020) -- End

                            Else
                                wc.Formula = "=SUM(R[-" & intRowCount & "]C:R[-1]C)"
                            End If

                        Else
                            If col.Ordinal = 0 Then
                                wc = New WorksheetCell(IIf(mstrReportExcelGrandTotalCaption = "", Language.getMessage(mstrModuleName, 11, "Grand Total"), mstrReportExcelGrandTotalCaption), DataType.String, "FooterStyle")
                            ElseIf mblnReportExcelShowRecordCount = True AndAlso col.Ordinal = intCountColOrdinal Then
                                wc = New WorksheetCell()
                                wc.StyleID = "countFooterStyleN"

                                If dicGroupCells IsNot Nothing AndAlso dicGroupCells.Count > 0 Then
                                    'Pinkal (15-Jan-2020) -- Start
                                    'Enhancements -  Working on OT Requisistion Reports for NMB.
                                    If dicGroupCells(0) IsNot Nothing Then
                                    wc.Formula = "=" & dicGroupCells(0).Item(col.Ordinal)
                                    End If
                                    'Pinkal (15-Jan-2020) -- End
                                Else
                                    wc.Formula = "=COUNTA(R[-" & intRowCount & "]C:R[-1]C)"
                                End If
                            Else
                                wc = New WorksheetCell("", DataType.String, "FooterStyle")
                            End If
                        End If

                        blnGrpColumn = False
                        intMergeCols = 1
                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                        'Sohail (09 Apr 2015) -- Start
                                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                                        'wc.MergeAcross = intMergeCols
                                        If blnShowSeparateGroupingColumn = False Then
                                            wc.MergeAcross = intMergeCols
                                        End If
                                        'Sohail (09 Apr 2015) -- End
                                        blnGrpColumn = True

                                        If col.Ordinal = 0 Then
                                            intCountColOrdinal += 1
                                        Else
                                            intCountColOrdinal = 1
                                        End If
                                    End If
                                End If
                            Next
                        End If

                        row.Cells.Add(wc)
                    Else

                        blnGrpColumn = False
                        intMergeCols += 1

                        If mstrArrayGroupColumnNames IsNot Nothing AndAlso mstrArrayGroupColumnNames.Length > 0 Then
                            For Each strCol As String In mstrArrayGroupColumnNames
                                If dtSummary.Columns.Contains(strCol.Trim) = True Then
                                    If col.Ordinal + 1 = dtSummary.Columns(strCol.Trim).Ordinal Then
                                        'Sohail (09 Apr 2015) -- Start
                                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                                        'wc.MergeAcross = intMergeCols
                                        If blnShowSeparateGroupingColumn = False Then
                                            wc.MergeAcross = intMergeCols
                                        End If
                                        'Sohail (09 Apr 2015) -- End
                                        blnGrpColumn = True

                                        If intCountColOrdinal > 1 Then
                                            intCountColOrdinal += 1
                                        End If
                                    End If
                                End If
                            Next

                            'Sohail (09 Apr 2015) -- Start
                            'CCK Enhancement - New report Detailed Salary Breakdown report.
                            If blnShowSeparateGroupingColumn = True Then
                                wc = New WorksheetCell("", DataType.String, "FooterStyle")
                                row.Cells.Add(wc)
                            End If
                            'Sohail (09 Apr 2015) -- End

                        End If
                    End If
                Next

                If mdicGrandTotalExtraInfo IsNot Nothing AndAlso mdicGrandTotalExtraInfo.Count > 0 Then
                    For Each pair In mdicGrandTotalExtraInfo
                        wc = New WorksheetCell(pair.Value, "FooterStyle")
                        sheet.Table.Rows(sheet.Table.Rows.Count - 1).Cells.Item(pair.Key) = wc
                        intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                    Next
                End If

            row = sheet.Table.Rows.Add
            row.AutoFitHeight = True
            intExcelRowIndex += 1

            wc = New WorksheetCell("", DataType.String, "s8w")
            wc.MergeAcross = intColCount
            row.Cells.Add(wc)

            wc = New WorksheetCell("Total", DataType.String, "s8b")
            row.Cells.Add(wc)

            wc = New WorksheetCell("", DataType.String, "s8")
            row.Cells.Add(wc)

            If strGTotalFormula.Trim <> "" Then
                strGTotalFormula = strGTotalFormula.Substring(1)
            End If
            wc = New WorksheetCell("", "n8b")
            wc.Formula = "=" & strGTotalFormula
            row.Cells.Add(wc)

            '*** Extra Report Footer
            If marrWorksheetRowsExtraReportFooter IsNot Nothing AndAlso marrWorksheetRowsExtraReportFooter.Count > 0 Then
                For Each wrow In marrWorksheetRowsExtraReportFooter
                    wrow.AutoFitHeight = True
                    sheet.Table.Rows.Add(wrow)
                    intExcelRowIndex += 1 'Sohail (09 Apr 2015)
                Next
            End If

            End If


            'Save the work book
            book.Save(strExportPath)

            Dim strText As String = IO.File.ReadAllText(strExportPath)
            If strText.IndexOf("#10;") > 0 Then
                strText = strText.Replace("#10;", "&#10;")
                IO.File.WriteAllText(strExportPath, strText)
            End If


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_to_Excel_Extra_SalaryReconciliation", mstrModuleName)
            Return False
        End Try
    End Function
    'Sohail (30 Dec 2015) -- End

    Function ConvertToExcelColumnLetter(ByVal intCol As Integer) As String
        Dim strLetter As String = ""
        'Sohail (01 Oct 2014) -- Start
        'Enhancement -ColumnName issues for BA, CA, DA column.
        'Dim intAlpha As Integer
        'Dim intRemainder As Integer
        'intAlpha = Int(intCol / 27)
        'intRemainder = intCol - (intAlpha * 26)
        'If intAlpha > 0 Then
        '    strLetter = Chr(intAlpha + 64)
        'End If
        'If intRemainder > 0 Then
        '    strLetter = strLetter & Chr(intRemainder + 64)
        'End If
        If intCol > 26 Then
            strLetter = Chr(Int((intCol - 1) / 26) + 64) & Chr(((intCol - 1) Mod 26) + 65)
        Else
            strLetter = Chr(intCol + 64)
        End If
        'Sohail (01 Oct 2014) -- End

        Return strLetter
    End Function
    'Sohail (29 Apr 2014) -- End

    Public Sub OrderByExecute(ByVal iColCollection As IColumnCollection)
        Dim frm As New frmFieldSort()
        Try
            frm._ArrayList = iColCollection

            frm._OrderByQueryName = m_strOrderByQuery
            frm._OrderByDisplayName = m_strOrderByDisplay
            frm._OrderByDescending = ConfigParameter._Object._OrderByDescendingOnPayslip 'Sohail (18 Feb 2012)

            If frm.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                m_strOrderByQuery = frm._OrderByQueryName
                m_strOrderByDisplay = frm._OrderByDisplayName
            End If

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "OrderByExecute", mstrModuleName)
        Finally
            frm.Dispose()
            frm = Nothing
        End Try
    End Sub

    Private Sub TextChange(ByVal rpt As CrystalDecisions.CrystalReports.Engine.ReportClass, _
                            ByVal strObjectName As String, _
                            ByVal strValue As String)

        Dim txt As CrystalDecisions.CrystalReports.Engine.TextObject = Nothing
        Try
            txt = DirectCast(rpt.ReportDefinition.ReportObjects(strObjectName), CrystalDecisions.CrystalReports.Engine.TextObject)
            txt.Text = strValue

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "TextChange", mstrModuleName)
        Finally
            If txt IsNot Nothing Then
                txt.Dispose()
                txt = Nothing
            End If
        End Try
    End Sub

    Public Sub PreviewImage(ByRef g As System.Drawing.Graphics)
        Try
            Dim intLn As Integer = 10
            Using String_Formate As New System.Drawing.StringFormat
                String_Formate.Alignment = System.Drawing.StringAlignment.Center
                String_Formate.LineAlignment = System.Drawing.StringAlignment.Near

                g.DrawString(Me._ReportName, New System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Bold), System.Drawing.Brushes.Blue, 200, intLn, String_Formate)
                intLn += 20

                If Me._PreviewTextTitle <> "" Then
                    g.DrawString(Me._PreviewTextTitle, New System.Drawing.Font("Arial", 8), System.Drawing.Brushes.Black, 200, intLn, String_Formate)
                    intLn += 15
                End If

                If Me._PreviewTextHeader <> "" Then
                    g.DrawLine(System.Drawing.Pens.Black, 10, intLn, g.ClipBounds.Width - 20, intLn)
                    g.DrawString(Me._PreviewTextHeader, New System.Drawing.Font("Lucida Console", 8), System.Drawing.Brushes.Black, 200, intLn + 5, String_Formate)
                    g.DrawLine(System.Drawing.Pens.Black, 10, intLn + 15, g.ClipBounds.Width - 20, intLn + 15)
                    intLn += 20
                End If

                g.DrawString(Me._PreviewTextData, New System.Drawing.Font("Lucida Console", 8), System.Drawing.Brushes.Black, 200, intLn, String_Formate)
            End Using

        Catch ex As Exception

        End Try
    End Sub

    'S.SANDEEP [ 05 NOV 2014 ] -- START

    'S.SANDEEP [29 DEC 2015] -- START
    'Public Sub ReportExecute(ByVal objRDoc As CrystalDecisions.CrystalReports.Engine.ReportDocument, _
    '                            ByVal PrintAction As enPrintAction, _
    '                            ByVal ExportAction As enExportAction, _
    '                            ByVal strExportPath As String, _
    '                            ByVal blnOpenExport As Boolean)

    '    Dim objReportViewer As ArutiReportViewer
    '    Dim strReportExportFileExtension As String = ""
    '    Dim strReportExportFile As String = ""
    '    Try

    '        If IsNothing(objRDoc) Then Exit Sub
    '        If PrintAction = enPrintAction.None And ExportAction = enExportAction.None Then Exit Sub

    '        'Printing
    '        Select Case PrintAction
    '            Case enPrintAction.Preview
    '                objReportViewer = New ArutiReportViewer
    '                objReportViewer._Heading = _ReportName
    '                Using prd As New System.Drawing.Printing.PrintDocument
    '                    objRDoc.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind
    '                End Using
    '                objReportViewer.crwArutiReportViewer.ReportSource = objRDoc
    '                objReportViewer.Show()
    '                Exit Sub
    '            Case enPrintAction.Print
    '                Dim prd As New System.Drawing.Printing.PrintDocument
    '                objRDoc.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
    '                objRDoc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
    '                objRDoc.PrintToPrinter(ConfigParameter._Object._NoofPagestoPrint, False, 0, 0)
    '                Exit Sub
    '        End Select


    '        'Exporting
    '        If System.IO.Directory.Exists(strExportPath) = False Then
    '            Dim dig As New Windows.Forms.FolderBrowserDialog
    '            dig.Description = "Select Folder Where to export report."
    '            If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
    '                strExportPath = dig.SelectedPath
    '            Else
    '                Exit Sub
    '            End If
    '        End If

    '        If Me._ReportName.Contains("/") = True Then
    '            Me._ReportName = Me._ReportName.Replace("/", "_")
    '        End If

    '        If Me._ReportName.Contains("\") Then
    '            Me._ReportName = Me._ReportName.Replace("\", "_")
    '        End If

    '        If Me._ReportName.Contains(":") Then
    '            Me._ReportName = Me._ReportName.Replace(":", "_")
    '        End If

    '        strReportExportFile = strExportPath & "\" & _
    '                                Me._ReportName.Replace(" ", "_") & "_" & _
    '                                Now.Date.ToString("yyyyMMdd")

    '        Select Case ExportAction
    '            Case enExportAction.Excel
    '                strReportExportFileExtension = ".xls"
    '            Case enExportAction.HTML
    '                strReportExportFileExtension = ".html"
    '            Case enExportAction.PDF
    '                strReportExportFileExtension = ".pdf"
    '            Case enExportAction.RichText
    '                strReportExportFileExtension = ".rtf"
    '            Case enExportAction.Word
    '                strReportExportFileExtension = ".doc"
    '            Case Else
    '                Exit Sub
    '        End Select

    '        objRDoc.ExportToDisk(ExportAction, strReportExportFile & strReportExportFileExtension)

    '        If blnOpenExport Then
    '            Call Shell("explorer " & strReportExportFile & strReportExportFileExtension, vbMaximizedFocus)
    '        End If

    '    Catch ex As Exception
    '        Call DisplayError.Show(-1, ex.Message, "ReportExecute", mstrModuleName)
    '    End Try
    'End Sub
    'S.SANDEEP [29 DEC 2015] -- END

    'S.SANDEEP [ 05 NOV 2014 ] -- END

End Class
