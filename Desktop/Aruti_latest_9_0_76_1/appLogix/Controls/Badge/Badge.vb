﻿Public Class Badge

    Public Shadows Event Click(ByVal sender As Object, ByVal e As System.EventArgs)


#Region " Properties "

    Public Property Text_Number() As String
        Get
            Return lblNumber.Text
        End Get
        Set(ByVal value As String)
            lblNumber.Text = value
        End Set
    End Property

    Public Property Text_Caption() As String
        Get
            Return lblCaption.Text
        End Get
        Set(ByVal value As String)
            lblCaption.Text = value
        End Set
    End Property

    Public Property FontSize_Number() As Single
        Get
            Return lblNumber.Font.Size
        End Get
        Set(ByVal value As Single)
            lblNumber.Font = New Font(lblNumber.Font.Name, value)
        End Set
    End Property

    Public Property FontSize_Caption() As Single
        Get
            Return lblCaption.Font.Size
        End Get
        Set(ByVal value As Single)
            lblCaption.Font = New Font(lblCaption.Font.Name, value)
        End Set
    End Property

    Public Property Height_Caption() As Integer
        Get
            Return lblCaption.Size.Height
        End Get
        Set(ByVal value As Integer)
            lblCaption.Size = New Size(lblCaption.Width, value)
        End Set
    End Property

    Public Overrides Property Cursor() As System.Windows.Forms.Cursor
        Get
            Return MyBase.Cursor
        End Get
        Set(ByVal value As System.Windows.Forms.Cursor)
            MyBase.Cursor = value

            If value = Cursors.Hand Then
                lblCaption.ForeColor = Color.Blue
            Else
                lblCaption.ForeColor = Color.Black
            End If
        End Set
    End Property
#End Region

#Region " Events "

    Protected Sub Me_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnlBadge.Click, lblCaption.Click, lblNumber.Click
        Try

            RaiseEvent Click(sender, e)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

#End Region

End Class
