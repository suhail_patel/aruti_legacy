'************************************************************************************************************************************
'Class Name : frmLanguage.vb
'Purpose    : show, edit objLanguage.
'Written By : 
'Modified   : Naimish 05/06/2009
'************************************************************************************************************************************

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Imports System.Text
Imports System.Text.RegularExpressions

Imports eZeeCommonLib

Public Class frmLanguage
    Private ReadOnly mstrModuleName As String = "frmLanguage"

    ''' <summary>
    ''' a Form Control which contain control's name will fetch.
    ''' </summary>
    Private mFormControl As Control = Nothing

    Private m_strOthersModuleNames As String = ""
    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private m_strOthersFormNames As String = ""

    Private m_strFormNameFilter As String = ""
    Private m_strModuleNameFilter As String = ""
    Private m_strOtherModuleNameFilter As String = ""

    Private m_dicFormNameFilter As New Dictionary(Of Integer, String)
    Private m_dicModuleNameFilter As New Dictionary(Of Integer, String)
    Private m_dicOtherModuleNameFilter As New Dictionary(Of Integer, String)
    'Sohail (22 Nov 2018) -- End
    Private blnIsChanage As Boolean = False

    Private WithEvents objFindReplaceLanguage As New frmFindReplaceLanguage

    Public Property _Other_ModuleNames() As String
        Get
            Return m_strOthersModuleNames
        End Get
        Set(ByVal value As String)
            m_strOthersModuleNames = value
        End Set
    End Property

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Public Property _Other_FormNames() As String
        Get
            Return m_strOthersFormNames
        End Get
        Set(ByVal value As String)
            m_strOthersFormNames = value
        End Set
    End Property
    'Sohail (22 Nov 2018) -- End


#Region " Public Method "
    Public Sub displayDialog(ByRef obj As Object)
        If TypeOf obj Is Form Then
            mFormControl = CType(obj, Form)
            Me.ShowDialog()
        End If
    End Sub

    Public Sub displayDialog()
        Me.ShowDialog()
    End Sub
#End Region

#Region " Private Methods "
    Private Sub fillDatagridViews()
        Try
            'Sohail (20 Nov 2018) -- Start
            'NMB Enhancement - SMS Integration to send notification 75.1.
            cboFormFilter.Items.Clear()
            cboModuleFilter.Items.Clear()
            cboOtherModuleFilter.Items.Clear()

            m_dicFormNameFilter.Clear()
            m_dicModuleNameFilter.Clear()
            m_dicOtherModuleNameFilter.Clear()
            'Sohail (20 Nov 2018) -- End

            'Form Language
            If Not mFormControl Is Nothing Then ' Specify Forms
                Language._Object.setFullLanguage(mFormControl)

                dgLanguage.AutoGenerateColumns = False
                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                'dgLanguage.DataSource = New DataView(Language._Object._Language.Tables(0), " formname = '" & mFormControl.Name & "' ", "controlname", DataViewRowState.CurrentRows)
                Dim strForms() As String = m_strOthersFormNames.Split(",")
                m_strFormNameFilter = " formname IN ('" & mFormControl.Name & "' "
                For Each str As String In strForms
                    m_strFormNameFilter &= ", '" & str.Trim & "' "
                Next
                m_strFormNameFilter &= ") "
                'Sohail (04 Feb 2020) -- Start
                'Enhancement # : Set Alphabet order in dropdown items.
                'dgLanguage.DataSource = New DataView(Language._Object._Language.Tables(0), m_strFormNameFilter, "controlname", DataViewRowState.CurrentRows)
                dgLanguage.DataSource = New DataView(Language._Object._Language.Tables(0), m_strFormNameFilter, "formname, controlname", DataViewRowState.CurrentRows)
                'Sohail (04 Feb 2020) -- End
                Dim dtFormCol As DataTable = CType(dgLanguage.DataSource, DataView).ToTable(True, "formname")
                Dim i As Integer = 0
                For Each dtRow As DataRow In dtFormCol.Rows
                    cboFormFilter.Items.Add(Mid(dtRow.Item("formname").ToString, 4).Replace("NewMDI", "Master Form").Replace("MDI", "Master Form"))
                    m_dicFormNameFilter.Add(i, dtRow.Item("formname").ToString)
                    i += 1
                Next
                If cboFormFilter.Items.Count > 1 Then cboFormFilter.Visible = True
                'Sohail (22 Nov 2018) -- End

                dgcolApplication.DataPropertyName = "langunkid"
                dgcolformname.DataPropertyName = "formname"
                dgcolcontrolname.DataPropertyName = "controlname"
                dgColLanguage.DataPropertyName = "language"
                dgcollanguage1.DataPropertyName = "language1"
                dgcolLanguage2.DataPropertyName = "language2"
                dgcolApplication.DataPropertyName = "application"

                dgColLanguage.HeaderText = Language._Object.getMessage("Language", 1, "Default English")
                dgcollanguage1.HeaderText = Language._Object.getMessage("Language", 2, "Custom 1")
                dgcolLanguage2.HeaderText = Language._Object.getMessage("Language", 3, "Custom 2")
            Else
                tabcLanguage.TabPages.Remove(tabpCaption)
            End If

            If m_strOthersModuleNames.Trim = "" Then
                tabcLanguage.TabPages.Remove(tabpOthers)
            Else
                dgOtherMessages.Refresh()
            End If

            'Messages
            Dim dtvMsg As DataView
            If Not mFormControl Is Nothing Then
                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                'dtvMsg = New DataView(Language._Object._Messages.Tables(0), "module_name = '" & mFormControl.Name & "' ", "", DataViewRowState.CurrentRows)
                Dim strForms() As String = m_strOthersFormNames.Split(",")
                m_strModuleNameFilter = " module_name IN ('" & mFormControl.Name & "' "
                For Each str As String In strForms
                    m_strModuleNameFilter &= ", '" & str.Trim & "' "
                Next
                m_strModuleNameFilter &= ") "
                'Sohail (04 Feb 2020) -- Start
                'Enhancement # : Set Alphabet order in dropdown items.
                'dtvMsg = New DataView(Language._Object._Messages.Tables(0), m_strModuleNameFilter, "", DataViewRowState.CurrentRows)
                dtvMsg = New DataView(Language._Object._Messages.Tables(0), m_strModuleNameFilter, "module_name", DataViewRowState.CurrentRows)
                'Sohail (04 Feb 2020) -- End
                Dim dtModuleCol As DataTable = dtvMsg.ToTable(True, "module_name")
                Dim i As Integer = 0
                For Each dtRow As DataRow In dtModuleCol.Rows
                    cboModuleFilter.Items.Add(Mid(dtRow.Item("module_name").ToString, 4).Replace("NewMDI", "Master Form").Replace("MDI", "Master Form"))
                    m_dicModuleNameFilter.Add(i, dtRow.Item("module_name").ToString)
                    i += 1
                Next
                If cboModuleFilter.Items.Count > 1 Then cboModuleFilter.Visible = True
                'Sohail (22 Nov 2018) -- End

                'Naimish (09 Feb 2010) -- Start
                If dtvMsg.Count > 0 Then
                    dgMessage.AutoGenerateColumns = False
                    dgMessage.DataSource = dtvMsg

                    dgcolMsgCode.DataPropertyName = "messagecode"
                    dgcolMsgModuleName.DataPropertyName = "module_name"
                    dgcolMessage.DataPropertyName = "message"
                    dgcolMessage1.DataPropertyName = "message1"
                    dgcolMessage2.DataPropertyName = "message2"
                    dgcolMsgApplication.DataPropertyName = "application"

                    dgcolMessage.HeaderText = Language._Object.getMessage("Language", 1, "Default English")
                    dgcolMessage1.HeaderText = Language._Object.getMessage("Language", 2, "Custom 1")
                    dgcolMessage2.HeaderText = Language._Object.getMessage("Language", 3, "Custom 2")
                Else
                    tabcLanguage.TabPages.Remove(tabpMessages)
                End If
                'Naimish (09 Feb 2010) -- End
            Else
                'Naimish (09 Feb 2010) -- Start
                'dtvMsg = New DataView(Language._Object._Messages.Tables(0), "", "", DataViewRowState.CurrentRows)
                tabcLanguage.TabPages.Remove(tabpMessages)
                'Naimish (09 Feb 2010) -- End
            End If


            'Other Messages
            If m_strOthersModuleNames.Trim <> "" Then
                Dim strForms() As String = m_strOthersModuleNames.Split(",")
                'Sohail (20 Nov 2018) -- Start
                'NMB Enhancement - SMS Integration to send notification 75.1.
                'Dim strFilter As String = ""
                'For Each str As String In strForms
                '    strFilter &= "OR module_name = '" & str & "' "
                'Next
                'strFilter = strFilter.Substring(2)

                'Dim dtvOther As New DataView(Language._Object._Messages.Tables(0), strFilter, "", DataViewRowState.CurrentRows)
                m_strOtherModuleNameFilter = " module_name IN ('' "
                For Each str As String In strForms
                    m_strOtherModuleNameFilter &= ", '" & str.Trim & "' "
                Next
                m_strOtherModuleNameFilter &= ") "

                'Sohail (04 Feb 2020) -- Start
                'Enhancement # : Set Alphabet order in dropdown items.
                'Dim dtvOther As New DataView(Language._Object._Messages.Tables(0), m_strOtherModuleNameFilter, "", DataViewRowState.CurrentRows)
                Dim dtvOther As New DataView(Language._Object._Messages.Tables(0), m_strOtherModuleNameFilter, "module_name", DataViewRowState.CurrentRows)
                'Sohail (04 Feb 2020) -- End
                Dim dtOtherModuleCol As DataTable = dtvOther.ToTable(True, "module_name")
                Dim i As Integer = 0
                For Each dtRow As DataRow In dtOtherModuleCol.Rows
                    cboOtherModuleFilter.Items.Add(Mid(dtRow.Item("module_name").ToString, 4).Replace("MasterData", "System Generated Dropdown Values"))
                    m_dicOtherModuleNameFilter.Add(i, dtRow.Item("module_name").ToString)
                    i += 1
                Next
                If cboOtherModuleFilter.Items.Count > 1 Then cboOtherModuleFilter.Visible = True
                'Sohail (20 Nov 2018) -- End               

                'Naimish (09 Feb 2010) -- Start
                If dtvOther.Count > 0 Then
                    dgOtherMessages.AutoGenerateColumns = False
                    dgOtherMessages.DataSource = dtvOther

                    dgcolOtMsgID.DataPropertyName = "messagecode"
                    dgcolOtModuleName.DataPropertyName = "module_name"
                    dgcolOtLanguage.DataPropertyName = "message"
                    dgcolOtLanguage1.DataPropertyName = "message1"
                    dgcolOtLanguage2.DataPropertyName = "message2"
                    dgcolOtApplication.DataPropertyName = "application"

                    dgcolOtLanguage.HeaderText = Language.getMessage("Language", 1, "Default English")
                    dgcolOtLanguage1.HeaderText = Language.getMessage("Language", 2, "Custom 1")
                    dgcolOtLanguage2.HeaderText = Language.getMessage("Language", 3, "Custom 2")
                Else
                    tabcLanguage.TabPages.Remove(tabpOthers)
                End If
                'Naimish (09 Feb 2010) -- End
            Else
                tabcLanguage.TabPages.Remove(tabpOthers)
            End If

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "fillDatagridViews", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Forms "

    Private Sub frmLanguage_FormClosing(ByVal sender As System.Object, _
                ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing

        Try
            objFindReplaceLanguage.Close()
            mFormControl = Nothing

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmLanguage_FormClosing", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLanguage_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave.PerformClick()
            End If
            If e.Control = True And e.KeyCode = Keys.F Then
                btnFindReplace.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLanguage_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLanguage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strQ As String = ""

        Try
            Call Set_Logo(Me, gApplicationType)
            Call OtherSettings()

            Call fillDatagridViews()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmLanguage_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            dgLanguage.EndEdit()

            Call Language._Object.SaveValue()
            blnIsChanage = False
        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "btnSave_Click", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If blnIsChanage Then
                If eZeeMsgBox.Show("Do you want save changes you made?", enMsgBoxStyle.YesNo + enMsgBoxStyle.Question) = Windows.Forms.DialogResult.Yes Then
                    Call Language._Object.SaveValue()
                End If
            End If

            Me.Close()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub cboFormFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFormFilter.SelectedIndexChanged
        Try
            CType(dgLanguage.DataSource, DataView).RowFilter = m_strFormNameFilter & " AND formname = '" & m_dicFormNameFilter.Item(cboFormFilter.SelectedIndex) & "' "
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "cboFormFilter_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboModuleFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboModuleFilter.SelectedIndexChanged
        Try
            CType(dgMessage.DataSource, DataView).RowFilter = m_strModuleNameFilter & " AND module_name = '" & m_dicModuleNameFilter.Item(cboModuleFilter.SelectedIndex) & "' "
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "cboModuleFilter_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboOtherModuleFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOtherModuleFilter.SelectedIndexChanged
        Try
            CType(dgOtherMessages.DataSource, DataView).RowFilter = m_strOtherModuleNameFilter & " AND module_name = '" & m_dicOtherModuleNameFilter.Item(cboOtherModuleFilter.SelectedIndex) & "' "
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "cboOtherModuleFilter_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 Nov 2018) -- End

#End Region

#Region " Controls "
    Private Sub dtLanguage_RowChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ListChangedEventArgs)
        blnIsChanage = True
    End Sub

#End Region

#Region "For Find Replace"
    ' make the regex and match class level variables 
    ' to make happen find next 
    Private regex As Regex
    Private match As Match
    Private eSearchEvent As New SearchEvent

    Private mintMatchIndex As Integer = 0
    Private mintRowIndex As Integer = 0
    Private mintCntFind As Integer = 0

    Private Sub btnFindReplace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindReplace.Click
        objFindReplaceLanguage.TopMost = True
        objFindReplaceLanguage.Show()
    End Sub

    ''' <summary> 
    ''' This function makes and returns a RegEx object 
    ''' depending on user input 
    ''' </summary> 
    ''' <returns></returns> 
    Private Function GetRegExpression(ByVal searchTextBox As String) As Regex
        Dim result As Regex
        Dim regExString As String

        ' Get what the user entered 
        regExString = searchTextBox

        ' If regular expressions checkbox is selected, 
        ' our job is easy. Just do nothing 
        'If useRegulatExpressionCheckBox.Checked Then
        If eSearchEvent._UseWildcards Then
            ' wild cards checkbox checked 
            regExString = regExString.Replace("*", "\w*")
            ' multiple characters wildcard (*) 
            regExString = regExString.Replace("?", "\w")
            ' single character wildcard (?) 
            ' if wild cards selected, find whole words only 
            regExString = [String].Format("{0}{1}{0}", "\b", regExString)
        Else
            ' replace escape characters 
            'regExString = regex.Escape(regExString)
            regExString = RegularExpressions.Regex.Escape(regExString)
        End If

        ' Is whole word check box checked? 
        If eSearchEvent._MatchWholeWord Then
            regExString = [String].Format("{0}{1}{0}", "\b", regExString)
        End If

        ' Is match case checkbox checked? 
        If eSearchEvent._MatchCase Then
            result = New Regex(regExString)
        Else
            result = New Regex(regExString, RegexOptions.IgnoreCase)
        End If

        Return result
    End Function

    ''' <summary> 
    ''' finds the text in searchTextBox in contentTextBox 
    ''' </summary> 
    Private Function FindText(ByVal contentTextBox As String, _
                        ByVal searchTextBox As String, _
                        ByVal NextIndex As Integer) As Boolean

        If NextIndex = 0 Then
            match = regex.Match(contentTextBox)
        Else
            match = regex.Match(contentTextBox, NextIndex)
        End If

        If match.Success Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Property _CurrentText(ByVal intCnt As Integer) As String
        Get
            If tabcLanguage.SelectedTab Is tabpCaption Then
                If eSearchEvent._LookIn = 2 Then
                    Return dgLanguage.Rows(intCnt).Cells(dgcolLanguage2.Index).Value
                Else
                    Return dgLanguage.Rows(intCnt).Cells(dgcollanguage1.Index).Value
                End If
            ElseIf tabcLanguage.SelectedTab Is tabpMessages Then
                If eSearchEvent._LookIn = 2 Then
                    Return dgMessage.Rows(intCnt).Cells(dgcolLanguage2.Index).Value
                Else
                    Return dgMessage.Rows(intCnt).Cells(dgcollanguage1.Index).Value
                End If

            ElseIf tabcLanguage.SelectedTab Is tabpOthers Then
                If eSearchEvent._LookIn = 2 Then
                    Return dgOtherMessages.Rows(intCnt).Cells(dgcolLanguage2.Index).Value
                Else
                    Return dgOtherMessages.Rows(intCnt).Cells(dgcollanguage1.Index).Value
                End If
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If tabcLanguage.SelectedTab Is tabpCaption Then
                If eSearchEvent._LookIn = 2 Then
                    dgLanguage.Rows(intCnt).Cells(dgcolLanguage2.Index).Value = value
                Else
                    dgLanguage.Rows(intCnt).Cells(dgcollanguage1.Index).Value = value
                End If
            ElseIf tabcLanguage.SelectedTab Is tabpMessages Then
                If eSearchEvent._LookIn = 2 Then
                    dgMessage.Rows(intCnt).Cells(dgcolLanguage2.Index).Value = value
                Else
                    dgMessage.Rows(intCnt).Cells(dgcollanguage1.Index).Value = value
                End If

            ElseIf tabcLanguage.SelectedTab Is tabpOthers Then
                If eSearchEvent._LookIn = 2 Then
                    dgOtherMessages.Rows(intCnt).Cells(dgcolLanguage2.Index).Value = value
                Else
                    dgOtherMessages.Rows(intCnt).Cells(dgcollanguage1.Index).Value = value
                End If
            End If
        End Set
    End Property

    Private WriteOnly Property _SelectedRow(ByVal intCnt As Integer) As Boolean
        Set(ByVal value As Boolean)
            If tabcLanguage.SelectedTab Is tabpCaption Then
                If eSearchEvent._LookIn = 2 Then
                    dgLanguage.Rows(intCnt).Cells(dgcolLanguage2.Index).Selected = value
                Else
                    dgLanguage.Rows(intCnt).Cells(dgcollanguage1.Index).Selected = value
                End If
            ElseIf tabcLanguage.SelectedTab Is tabpMessages Then
                If eSearchEvent._LookIn = 2 Then
                    dgMessage.Rows(intCnt).Cells(dgcolLanguage2.Index).Selected = value
                Else
                    dgMessage.Rows(intCnt).Cells(dgcollanguage1.Index).Selected = value
                End If

            ElseIf tabcLanguage.SelectedTab Is tabpOthers Then
                If eSearchEvent._LookIn = 2 Then
                    dgOtherMessages.Rows(intCnt).Cells(dgcolLanguage2.Index).Selected = value
                Else
                    dgOtherMessages.Rows(intCnt).Cells(dgcollanguage1.Index).Selected = value
                End If
            End If
        End Set
    End Property

    Private ReadOnly Property _RowCount() As Integer
        Get
            If tabcLanguage.SelectedTab Is tabpCaption Then
                Return dgLanguage.Rows.Count
            ElseIf tabcLanguage.SelectedTab Is tabpMessages Then
                Return dgMessage.Rows.Count
            ElseIf tabcLanguage.SelectedTab Is tabpOthers Then
                Return dgOtherMessages.Rows.Count
            Else
                Return 0
            End If
        End Get
    End Property

    Private Sub objFindReplaceLanguage_FindNext(ByVal sender As Object, ByVal e As SearchEvent) Handles objFindReplaceLanguage.FindNext
        Dim intTempRowIndex As Integer = -1
        Dim intCnt As Integer
        Dim input As String = ""

        Try
            eSearchEvent = e

            regex = GetRegExpression(eSearchEvent._FindWhat)

            If mintRowIndex = _RowCount - 1 Then
                mintRowIndex = 0
            End If

            For intCnt = mintRowIndex To _RowCount - 1
                input = _CurrentText(intCnt)

                If FindText(input, e._FindWhat, mintMatchIndex) Then
                    mintMatchIndex = match.Index + 1
                    intTempRowIndex = intCnt
                    Exit For
                Else
                    mintMatchIndex = 0
                End If
            Next

            mintRowIndex = intTempRowIndex

            If mintRowIndex > -1 Then
                _SelectedRow(intCnt) = True

            Else
                MessageBox.Show([String].Format("Cannot find '{0}'. ", e._FindWhat), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                mintRowIndex = 0
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "FindString", mstrModuleName)
        End Try
    End Sub

    Private Sub objFindReplaceLanguage_Replace(ByVal sender As Object, ByVal e As SearchEvent) Handles objFindReplaceLanguage.Replace
        eSearchEvent = e

        Dim input As String = ""
        Dim replaceRegex As Regex = GetRegExpression(e._FindWhat)
        Dim replacedString As String
        Try

            If mintRowIndex > -1 Then
                input = _CurrentText(mintRowIndex)

                If mintMatchIndex <> 0 Then
                    replacedString = replaceRegex.Replace(input, e._ReplaceWith, 1, mintMatchIndex - 1)

                    If input <> replacedString Then
                        _CurrentText(mintRowIndex) = replacedString
                    End If
                End If

            End If

            Call objFindReplaceLanguage_FindNext(sender, e)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objFindReplaceLanguage_Replace", mstrModuleName)
        End Try
    End Sub

    Private Sub objFindReplaceLanguage_ReplaceAll(ByVal sender As Object, ByVal e As SearchEvent) Handles objFindReplaceLanguage.ReplaceAll
        eSearchEvent = e

        Dim input As String = ""
        Dim replaceRegex As Regex = GetRegExpression(e._FindWhat)
        Dim replacedString As String
        Dim CountReplace As Integer = 0

        Try

            For iCnt As Integer = 0 To _RowCount - 1
                input = _CurrentText(iCnt)

                replacedString = replaceRegex.Replace(input, e._ReplaceWith)

                If input <> replacedString Then
                    _CurrentText(iCnt) = replacedString
                    CountReplace += 1
                End If
            Next

            MessageBox.Show(CountReplace & " Replacements are made. ", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objFindReplaceLanguage_ReplaceAll", mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnFindReplace.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFindReplace.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.tabpCaption.Text = Language._Object.getCaption(Me.tabpCaption.Name, Me.tabpCaption.Text)
			Me.tabpMessages.Text = Language._Object.getCaption(Me.tabpMessages.Name, Me.tabpMessages.Text)
			Me.btnFindReplace.Text = Language._Object.getCaption(Me.btnFindReplace.Name, Me.btnFindReplace.Text)
			Me.tabpOthers.Text = Language._Object.getCaption(Me.tabpOthers.Name, Me.tabpOthers.Text)
			Me.dgColLanguage.HeaderText = Language._Object.getCaption(Me.dgColLanguage.Name, Me.dgColLanguage.HeaderText)
			Me.dgcollanguage1.HeaderText = Language._Object.getCaption(Me.dgcollanguage1.Name, Me.dgcollanguage1.HeaderText)
			Me.dgcolLanguage2.HeaderText = Language._Object.getCaption(Me.dgcolLanguage2.Name, Me.dgcolLanguage2.HeaderText)
			Me.dgcollangunkid.HeaderText = Language._Object.getCaption(Me.dgcollangunkid.Name, Me.dgcollangunkid.HeaderText)
			Me.dgcolformname.HeaderText = Language._Object.getCaption(Me.dgcolformname.Name, Me.dgcolformname.HeaderText)
			Me.dgcolcontrolname.HeaderText = Language._Object.getCaption(Me.dgcolcontrolname.Name, Me.dgcolcontrolname.HeaderText)
			Me.dgcolApplication.HeaderText = Language._Object.getCaption(Me.dgcolApplication.Name, Me.dgcolApplication.HeaderText)
			Me.dgcolMessage.HeaderText = Language._Object.getCaption(Me.dgcolMessage.Name, Me.dgcolMessage.HeaderText)
			Me.dgcolMessage1.HeaderText = Language._Object.getCaption(Me.dgcolMessage1.Name, Me.dgcolMessage1.HeaderText)
			Me.dgcolMessage2.HeaderText = Language._Object.getCaption(Me.dgcolMessage2.Name, Me.dgcolMessage2.HeaderText)
			Me.dgcolMsgCode.HeaderText = Language._Object.getCaption(Me.dgcolMsgCode.Name, Me.dgcolMsgCode.HeaderText)
			Me.dgcolMsgModuleName.HeaderText = Language._Object.getCaption(Me.dgcolMsgModuleName.Name, Me.dgcolMsgModuleName.HeaderText)
			Me.dgcolMsgApplication.HeaderText = Language._Object.getCaption(Me.dgcolMsgApplication.Name, Me.dgcolMsgApplication.HeaderText)
			Me.dgcolOtLanguage.HeaderText = Language._Object.getCaption(Me.dgcolOtLanguage.Name, Me.dgcolOtLanguage.HeaderText)
			Me.dgcolOtLanguage1.HeaderText = Language._Object.getCaption(Me.dgcolOtLanguage1.Name, Me.dgcolOtLanguage1.HeaderText)
			Me.dgcolOtLanguage2.HeaderText = Language._Object.getCaption(Me.dgcolOtLanguage2.Name, Me.dgcolOtLanguage2.HeaderText)
			Me.dgcolOtMsgID.HeaderText = Language._Object.getCaption(Me.dgcolOtMsgID.Name, Me.dgcolOtMsgID.HeaderText)
			Me.dgcolOtModuleName.HeaderText = Language._Object.getCaption(Me.dgcolOtModuleName.Name, Me.dgcolOtModuleName.HeaderText)
			Me.dgcolOtApplication.HeaderText = Language._Object.getCaption(Me.dgcolOtApplication.Name, Me.dgcolOtApplication.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage("Language", 1, "Default English")
			Language.setMessage("Language", 2, "Custom 1")
			Language.setMessage("Language", 3, "Custom 2")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

