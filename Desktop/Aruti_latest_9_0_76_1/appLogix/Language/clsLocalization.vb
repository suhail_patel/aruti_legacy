'************************************************************************************************************************************
'Class Name : clsLocalization.vb
'Purpose    : show, edit objLanguage.
'Written By : Jitu
'Modified   : Naimish 05/06/2009
'************************************************************************************************************************************

Imports System
Imports System.Data
Imports eZeeCommonLib
Imports System.Windows.Forms
Imports System.Drawing
Imports system.Data.SqlClient
Imports System.Web
Imports System.Web.SessionState

Public Class clsLocalization
    Private ReadOnly mstrModuleName As String = "clsLocalization"

    Private m_intApplicationType As Integer = -1
    Private m_strFormName As String = ""

    Private mdsLanguage As DataSet
    Private mdvLanguage As DataView
    Private mdaLanguage As SqlDataAdapter
    Private mcbLanguage As SqlCommandBuilder

    Private mintLangId As Integer = 0

    Private mdsMessages As DataSet
    Private mdaMessages As SqlDataAdapter
    Private mcbMessages As SqlCommandBuilder

    Public Property _Language() As DataSet
        Get
            Return mdsLanguage
        End Get
        Set(ByVal value As DataSet)
            mdsLanguage = value
        End Set
    End Property

    Public Property _Messages() As DataSet
        Get
            Return mdsMessages
        End Get
        Set(ByVal value As DataSet)
            mdsMessages = value
        End Set
    End Property


    Public Property _LangId() As Integer
        Get
            Return mintLangId
        End Get
        Set(ByVal value As Integer)
            mintLangId = value
        End Set
    End Property


    'Sohail (07 Dec 2013) -- Start
    'Enhancement - OMAN
    Dim mstrDatabaseName As String = ""
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
    'Sohail (07 Dec 2013) -- End


#Region " Constructor, Destructor"
    Public Sub New()
        Me.New(0)
    End Sub

    Public Sub New(ByVal intApplicationId As Integer)
        m_intApplicationType = intApplicationId
        Call Refresh()

    End Sub

    'Sohail (07 Dec 2013) -- Start
    'Enhancement - OMAN
    Public Sub New(ByVal intApplicationId As Integer, ByVal strDatabaseName As String)
        m_intApplicationType = intApplicationId
        mstrDatabaseName = strDatabaseName
        Call Refresh()

    End Sub
    'Sohail (07 Dec 2013) -- End

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
#End Region

    'Pinkal (09-Aug-2021)-- Start
    'NMB New UI Enhancements.
    'Public Sub Refresh(Optional ByVal RefteshOnlyMessage As Boolean = False)
    Public Sub Refresh(Optional ByVal RefteshOnlyMessage As Boolean = False, Optional ByVal xApplicationType As Integer = 0)
        'Pinkal (09-Aug-2021) -- End
        Dim strQ As String = ""
        Dim strFilter As String = ""
        Dim objDataOperation As New clsDataOperation

        Try

            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.
            If xApplicationType > 0 Then m_intApplicationType = xApplicationType
            'Pinkal (09-Aug-2021) -- End


            'Sandeep | 17 JAN 2011 | -- START
            'strFilter &= "WHERE Application = " & m_intApplicationType
            'strQ = "SELECT application,formname,controlname,language,language1,language2 FROM cfLanguage " & strFilter

            'Sohail (07 Dec 2013) -- Start
            'Enhancement - setting language for web
            'Dim strLangCondition As String = IIf(m_intApplicationType = 1, FinancialYear._Object._ConfigDatabaseName & "..cfLanguage ", FinancialYear._Object._DatabaseName & "..cfLanguage ")
            'Dim strMsgCondition As String = IIf(m_intApplicationType = 1, FinancialYear._Object._ConfigDatabaseName & "..cfmessages ", FinancialYear._Object._DatabaseName & "..cfmessages ")
            If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
            Dim strLangCondition As String = IIf(m_intApplicationType = 1, FinancialYear._Object._ConfigDatabaseName & "..cfLanguage ", mstrDatabaseName & "..cfLanguage ")
            Dim strMsgCondition As String = IIf(m_intApplicationType = 1, FinancialYear._Object._ConfigDatabaseName & "..cfmessages ", mstrDatabaseName & "..cfmessages ")
            'Sohail (07 Dec 2013) -- End


            strQ = "SELECT formname,controlname,language,language1,language2 FROM " & strLangCondition
            'Sandeep | 17 JAN 2011 | -- END 




            If Not mdsLanguage Is Nothing Then mdsLanguage = Nothing
            mdsLanguage = New DataSet
            mdaLanguage = New SqlDataAdapter
            mdaLanguage.SelectCommand = objDataOperation.SQL_Command(strQ)
            'mdaLanguage.ContinueUpdateOnError = True
            mcbLanguage = New SqlCommandBuilder(mdaLanguage)
            mdaLanguage.Fill(mdsLanguage, "Language")


            'Sandeep | 17 JAN 2011 | -- START
            'mdaLanguage.InsertCommand = objDataOperation.SQL_Command("INSERT INTO cflanguage(application,formname,controlname,language,language1,language2) SELECT @application,@formname,@controlname,@language,@language1,@language2 WHERE NOT EXISTS (SELECT * FROM cflanguage WHERE application=@application AND formname=@formname AND controlname=@controlname)")
            'mdaLanguage.InsertCommand.Parameters.Add("@application", SqlDbType.Int).SourceColumn = "application"
            mdaLanguage.InsertCommand = objDataOperation.SQL_Command("INSERT INTO " & strLangCondition & " (formname,controlname,language,language1,language2) SELECT @formname,@controlname,@language,@language1,@language2 WHERE NOT EXISTS (SELECT * FROM  " & strLangCondition & " WHERE formname=@formname AND controlname=@controlname)")
            'Sandeep | 17 JAN 2011 | -- END
            mdaLanguage.InsertCommand.Parameters.Add("@formname", SqlDbType.NVarChar).SourceColumn = "formname"
            mdaLanguage.InsertCommand.Parameters.Add("@controlname", SqlDbType.NVarChar).SourceColumn = "controlname"
            mdaLanguage.InsertCommand.Parameters.Add("@language", SqlDbType.NVarChar).SourceColumn = "language"
            mdaLanguage.InsertCommand.Parameters.Add("@language1", SqlDbType.NVarChar).SourceColumn = "language1"
            mdaLanguage.InsertCommand.Parameters.Add("@language2", SqlDbType.NVarChar).SourceColumn = "language2"


            'Sandeep | 17 JAN 2011 | -- START
            'mdaLanguage.UpdateCommand = objDataOperation.SQL_Command("UPDATE cflanguage SET language=@language,language1=@language1,language2=@language2 WHERE application=@application AND formname=@formname AND controlname=@controlname")
            'mdaLanguage.UpdateCommand.Parameters.Add("@application", SqlDbType.Int).SourceColumn = "application"
            mdaLanguage.UpdateCommand = objDataOperation.SQL_Command("UPDATE " & strLangCondition & " SET language=@language,language1=@language1,language2=@language2 WHERE formname=@formname AND controlname=@controlname")
            'Sandeep | 17 JAN 2011 | -- END 
            mdaLanguage.UpdateCommand.Parameters.Add("@formname", SqlDbType.NVarChar).SourceColumn = "formname"
            mdaLanguage.UpdateCommand.Parameters.Add("@controlname", SqlDbType.NVarChar).SourceColumn = "controlname"
            mdaLanguage.UpdateCommand.Parameters.Add("@language", SqlDbType.NVarChar).SourceColumn = "language"
            mdaLanguage.UpdateCommand.Parameters.Add("@language1", SqlDbType.NVarChar).SourceColumn = "language1"
            mdaLanguage.UpdateCommand.Parameters.Add("@language2", SqlDbType.NVarChar).SourceColumn = "language2"


            'Sandeep | 17 JAN 2011 | -- START
            'strFilter = ""
            'Select Case m_intApplicationType
            '    Case 0, 1
            '        strFilter &= "WHERE Application IN (0,1) "
            '    Case 2, 3
            '        strFilter &= "WHERE Application IN (2,3)"
            '    Case Else
            'End Select
            'strQ = "SELECT messagecode,application,module_name,message,message1,message2 from cfmessages " & strFilter

            strQ = "SELECT messagecode,module_name,message,message1,message2 from " & strMsgCondition
            'Sandeep | 17 JAN 2011 | -- END 



            If Not mdsMessages Is Nothing Then mdsMessages = Nothing
            mdsMessages = New DataSet
            mdaMessages = New SqlDataAdapter
            mdaMessages.SelectCommand = objDataOperation.SQL_Command(strQ)
            mcbMessages = New SqlCommandBuilder(mdaMessages)
            mdaMessages.Fill(mdsMessages, "Messages")


            'Sandeep | 17 JAN 2011 | -- START
            'mdaMessages.InsertCommand = objDataOperation.SQL_Command("INSERT INTO cfmessages(messagecode,application,module_name,message,message1,message2) SELECT @messagecode,@application,@module_name,@message,@message1,@message2 WHERE NOT EXISTS (SELECT * FROM cfmessages WHERE messagecode=@messagecode AND module_name=@module_name AND application=@application)")
            'mdaMessages.InsertCommand.Parameters.Add("@application", SqlDbType.Int).SourceColumn = "application"
            mdaMessages.InsertCommand = objDataOperation.SQL_Command("INSERT INTO " & strMsgCondition & " (messagecode,module_name,message,message1,message2) SELECT @messagecode,@module_name,@message,@message1,@message2 WHERE NOT EXISTS (SELECT * FROM  " & strMsgCondition & "  WHERE messagecode=@messagecode AND module_name=@module_name)")
            'Sandeep | 17 JAN 2011 | -- END 
            mdaMessages.InsertCommand.Parameters.Add("@messagecode", SqlDbType.NVarChar).SourceColumn = "messagecode"
            mdaMessages.InsertCommand.Parameters.Add("@module_name", SqlDbType.NVarChar).SourceColumn = "module_name"
            mdaMessages.InsertCommand.Parameters.Add("@message", SqlDbType.NVarChar).SourceColumn = "message"
            mdaMessages.InsertCommand.Parameters.Add("@message1", SqlDbType.NVarChar).SourceColumn = "message1"
            mdaMessages.InsertCommand.Parameters.Add("@message2", SqlDbType.NVarChar).SourceColumn = "message2"

            'Sandeep | 17 JAN 2011 | -- START
            'mdaMessages.UpdateCommand = objDataOperation.SQL_Command("UPDATE cfmessages SET message=@message,message1=@message1,message2=@message2 WHERE application=@application AND messagecode=@messagecode AND module_name=@module_name")
            'mdaMessages.UpdateCommand.Parameters.Add("@application", SqlDbType.Int).SourceColumn = "application"
            mdaMessages.UpdateCommand = objDataOperation.SQL_Command("UPDATE " & strMsgCondition & " SET message=@message,message1=@message1,message2=@message2 WHERE messagecode=@messagecode AND module_name=@module_name")
            'Sandeep | 17 JAN 2011 | -- END 
            mdaMessages.UpdateCommand.Parameters.Add("@messagecode", SqlDbType.NVarChar).SourceColumn = "messagecode"
            mdaMessages.UpdateCommand.Parameters.Add("@module_name", SqlDbType.NVarChar).SourceColumn = "module_name"
            mdaMessages.UpdateCommand.Parameters.Add("@message", SqlDbType.NVarChar).SourceColumn = "message"
            mdaMessages.UpdateCommand.Parameters.Add("@message1", SqlDbType.NVarChar).SourceColumn = "message1"
            mdaMessages.UpdateCommand.Parameters.Add("@message2", SqlDbType.NVarChar).SourceColumn = "message2"

        Catch ex As System.Exception
            'Call DisplayError.Show(-1, ex.Message, "Refresh", mstrModuleName)
            Throw New Exception(ex.Message & " " & "Refresh" & " " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Sub

    Public Sub SaveValue()
        Try
            'S.SANDEEP |20-JAN-2022| -- START
            'ISSUE : Object reference not set to an instance of an object.Proc Name : getCaption - clsLocalization
            ''mdsLanguage.AcceptChanges()
            'mdaLanguage.Update(mdsLanguage, "Language")
            'mcbLanguage.RefreshSchema()

            'mdsLanguage.AcceptChanges()

            'mdaMessages.Update(mdsMessages, "Messages")
            'mcbMessages.RefreshSchema()

            'mdsMessages.AcceptChanges()


            If mdsLanguage IsNot Nothing Then mdaLanguage.Update(mdsLanguage, "Language")
            If mdsLanguage IsNot Nothing Then mcbLanguage.RefreshSchema()

            If mdsLanguage IsNot Nothing Then mdsLanguage.AcceptChanges()

            If mdsMessages IsNot Nothing Then mdaMessages.Update(mdsMessages, "Messages")
            If mdsMessages IsNot Nothing Then mcbMessages.RefreshSchema()

            If mdsMessages IsNot Nothing Then mdsMessages.AcceptChanges()
            'S.SANDEEP |20-JAN-2022| -- END
        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "SaveValue", mstrModuleName)

        Finally

        End Try
    End Sub

#Region " For Old Style Language Filtering "
    Public Sub setLanguage(ByVal pForm As Form)
        'If User._Object._LanguageId = 0 Then Exit Sub
        'Pinkal (10-Feb-2021) -- Start
        'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
        m_strFormName = pForm.Name
        'Pinkal (10-Feb-2021) -- End

        If mintLangId = 0 Then Exit Sub

        Dim strFilter As String = ""

        Try

            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            'm_strFormName = pForm.Name
            'Pinkal (10-Feb-2021) -- End
            strFilter = " formname = '" & m_strFormName & "' "

            mdvLanguage = New DataView(mdsLanguage.Tables("Language"), strFilter, "controlname", DataViewRowState.CurrentRows)

            Call getControlsLanguage(pForm)

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "setLanguage", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub getControlsLanguage(ByVal objSource As Object)
        Try
            Dim ctlChild As Control
            Try
                Select Case True

                    'Main Form
                    Case TypeOf objSource Is Form
                        If objSource.Name.ToUpper = "FRMMDI" Then
                        Else
                            objSource.Text = getCaption(objSource.Name, objSource.Text)
                        End If

                        For Each ctlChild In objSource.Controls
                            Call getControlsLanguage(ctlChild)
                        Next

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call getControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        'Jitu (13 Aug 2009) --Start
                        'Case TypeOf objSource Is Divelements.SandRibbon.Ribbon
                        '    Dim ctlRControl As Divelements.SandRibbon.Ribbon
                        '    ctlRControl = CType(objSource, Divelements.SandRibbon.Ribbon)

                        '    Call getControlsLanguage(ctlRControl.ApplicationMenu)

                        '    For Each objTab As Divelements.SandRibbon.RibbonTab In ctlRControl.Tabs
                        '        Call getControlsLanguage(objTab)
                        '    Next

                        'Case TypeOf objSource Is Divelements.SandRibbon.RibbonTab
                        '    Dim ctlTab As Divelements.SandRibbon.RibbonTab
                        '    ctlTab = CType(objSource, Divelements.SandRibbon.RibbonTab)

                        '    Call getCaption(ctlTab.Tag, ctlTab.Text)

                        '    For Each objGroup As Divelements.SandRibbon.RibbonChunk In ctlTab.Chunks
                        '        Call getCaption(objGroup.Tag, objGroup.Text)
                        '        Call getControlsLanguage(objGroup)
                        '    Next

                        'Case TypeOf objSource Is Divelements.SandRibbon.RibbonChunk
                        '    Dim ctlTab As Divelements.SandRibbon.RibbonChunk
                        '    ctlTab = CType(objSource, Divelements.SandRibbon.RibbonChunk)

                        '    For i As Integer = 0 To ctlTab.Items.Count - 1
                        '        If TypeOf ctlTab.Items(i) Is Divelements.SandRibbon.Button Then
                        '            Call getControlsLanguage(ctlTab.Items(i))
                        '        ElseIf TypeOf ctlTab.Items(i) Is Divelements.SandRibbon.StripLayout Then
                        '            Call getControlsLanguage(ctlTab.Items(i))
                        '        End If
                        '    Next

                        'Case TypeOf objSource Is Divelements.SandRibbon.StripLayout
                        '    Dim objCtl As Divelements.SandRibbon.StripLayout
                        '    objCtl = CType(objSource, Divelements.SandRibbon.StripLayout)

                        '    For i As Integer = 0 To objCtl.Items.Count - 1
                        '        If TypeOf objCtl.Items(i) Is Divelements.SandRibbon.Button Then
                        '            Call getControlsLanguage(objCtl.Items(i))
                        '        ElseIf TypeOf objCtl.Items(i) Is Divelements.SandRibbon.MenuItem Then
                        '            Call getControlsLanguage(objCtl.Items(i))
                        '        End If
                        '    Next

                        'Case TypeOf objSource Is Divelements.SandRibbon.Button
                        '    Call getCaption(objSource.Tag, objSource.Text)

                        '    Dim objCtl As Divelements.SandRibbon.Button
                        '    objCtl = CType(objSource, Divelements.SandRibbon.Button)

                        '    If objCtl.PopupWidget IsNot Nothing Then
                        '        Call getControlsLanguage(objCtl.PopupWidget)
                        '    End If

                        'Case TypeOf objSource Is Divelements.SandRibbon.MenuItem
                        '    Call getCaption(objSource.Tag, objSource.Text)

                        '    Dim objCtl As Divelements.SandRibbon.MenuItem
                        '    objCtl = CType(objSource, Divelements.SandRibbon.MenuItem)

                        '    For i As Integer = 0 To objCtl.Items.Count - 1
                        '        If TypeOf objCtl.Items(i) Is Divelements.SandRibbon.Button Then
                        '            Call getControlsLanguage(objCtl.Items(i))
                        '        ElseIf TypeOf objCtl.Items(i) Is Divelements.SandRibbon.Menu Then
                        '            Call getControlsLanguage(objCtl.Items(i))
                        '        End If
                        '    Next
                        'Jitu (13 Aug 2009) --End

                        'NonContainer Control
                    Case TypeOf objSource Is Label, _
                         TypeOf objSource Is CheckBox, _
                         TypeOf objSource Is Button, _
                         TypeOf objSource Is RadioButton, _
                         TypeOf objSource Is eZee.Common.eZeeSplitButton

                        If TypeOf objSource Is eZee.Common.eZeeSplitButton Then
                            If CType(objSource, eZee.Common.eZeeSplitButton).SplitButtonMenu IsNot Nothing Then
                                Call getControlsLanguage(CType(objSource, eZee.Common.eZeeSplitButton).SplitButtonMenu)
                            End If
                        End If

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call getControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        objSource.Text = getCaption(objSource.Name, objSource.Text)

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call getControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        'Container Control
                    Case TypeOf objSource Is GroupBox

                        objSource.Text = getCaption(objSource.Name, objSource.Text)

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call getControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        For Each ctlChild In objSource.Controls
                            Call getControlsLanguage(ctlChild)
                        Next

                    Case TypeOf objSource Is ListView
                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call getControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        For Each objColumn As ColumnHeader In objSource.Columns
                            If objColumn.Tag <> "" Then
                                objSource.Text = getCaption(objColumn.Tag, objColumn.Text)
                            End If
                        Next

                    Case TypeOf objSource Is DataGridView
                        'Rashmi (03 Apr 2009) -- Start
                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call getControlsLanguage(objSource.ContextMenuStrip)
                        End If
                        'Rashmi (03 Apr 2009) -- End

                        For Each objColumn As DataGridViewColumn In objSource.Columns
                            If objColumn.Name <> "" Then
                                objSource.Text = getCaption(objColumn.Name, objColumn.HeaderText)
                            End If
                        Next

                        'Non Text Controls
                    Case TypeOf objSource Is Panel, _
                         TypeOf objSource Is PictureBox, _
                         TypeOf objSource Is SplitContainer, _
                         TypeOf objSource Is SplitterPanel

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call getControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        If TypeOf objSource Is eZee.Common.eZeeCollapseContainer Then
                            objSource.Text = getCaption(objSource.Name, objSource.headertext)
                        End If

                        For Each ctlChild In objSource.Controls
                            Call getControlsLanguage(ctlChild)
                        Next

                    Case TypeOf objSource Is TabControl
                        For Each objTabPage As TabPage In objSource.TabPages
                            objSource.Text = getCaption(objTabPage.Name, objTabPage.Text)
                            Call getControlsLanguage(objTabPage)
                        Next

                        'Special Contaols
                    Case TypeOf objSource Is eZee.Common.eZeeImageHeader
                        objSource.Title = getCaption(objSource.Name & "_Title", objSource.Title)
                        objSource.Message = getCaption(objSource.Name & "_Message", objSource.Message)

                    Case TypeOf objSource Is eZee.Common.eZeeCollapseContainer
                        objSource.headertext = getCaption(objSource.Name, objSource.headertext)
                        'objSource.CaptionText = getCaption(objSource.Name, objSource.CaptionText)

                        For Each ctlChild In objSource.Controls
                            Call getControlsLanguage(ctlChild)
                        Next

                    Case TypeOf objSource Is eZee.SideBar.eZeePane
                        For Each ctlChild In objSource.Controls
                            Call getControlsLanguage(ctlChild)
                        Next

                    Case TypeOf objSource Is eZee.SideBar.eZeeItem
                        objSource.Text = getCaption(objSource.Name, objSource.Text)

                    Case TypeOf objSource Is eZee.Common.eZeeWizard

                        objSource.CancelText = getCaption(objSource.Name & "_CancelText", objSource.CancelText)
                        objSource.NextText = getCaption(objSource.Name & "_NextText", objSource.NextText)
                        objSource.BackText = getCaption(objSource.Name & "_BackText", objSource.BackText)
                        objSource.FinishText = getCaption(objSource.Name & "_FinishText", objSource.FinishText)

                        For Each page As eZee.Common.eZeeWizardPage In objSource.Pages
                            Call getControlsLanguage(page)
                        Next



                    Case TypeOf objSource Is MenuStrip, _
                         TypeOf objSource Is ContextMenuStrip

                        For Each objMenuItem As ToolStripItem In objSource.Items
                            If TypeOf objSource Is ToolStripSeparator Then
                                Continue For
                            ElseIf TypeOf objMenuItem Is ToolStripMenuItem Then
                                objSource.Text = getCaption(objMenuItem.Name, objMenuItem.Text)
                                Dim objtsmi As ToolStripMenuItem
                                objtsmi = CType(objMenuItem, ToolStripMenuItem)
                                If objtsmi.DropDownItems.Count > 0 Then
                                    Call getControlsLanguage(objMenuItem)
                                End If
                            End If
                        Next

                    Case TypeOf objSource Is ToolStripMenuItem

                        For Each objMenuItem As ToolStripItem In objSource.DropDownItems
                            If TypeOf objSource Is ToolStripSeparator Then
                                Continue For
                            ElseIf TypeOf objMenuItem Is ToolStripMenuItem Then
                                objSource.Text = getCaption(objMenuItem.Name, objMenuItem.Text)
                                Dim objtsmi As ToolStripMenuItem
                                objtsmi = CType(objMenuItem, ToolStripMenuItem)
                                If objtsmi.DropDownItems.Count > 0 Then
                                    Call getControlsLanguage(objMenuItem)
                                End If
                            End If
                        Next

                        'For Each objMenuItem As ToolStripMenuItem In objSource.DropDownItems
                        '    objSource.Text = getCaption(objMenuItem.Name, objMenuItem.Text)

                        '    If objMenuItem.DropDownItems.Count > 0 Then
                        '        Call getControlsLanguage(objMenuItem)
                        '    End If
                        'Next

                    Case TypeOf objSource Is ToolStrip

                        For Each objMenuItem As ToolStripItem In objSource.Items
                            objSource.Text = getCaption(objMenuItem.Name, objMenuItem.Text)

                            'If objMenuItem.DropDownItems.Count > 0 Then
                            '    Call getControlsLanguage(objMenuItem)
                            'End If
                        Next

                    Case TypeOf objSource Is eZee.Common.eZeeTextBox, _
                        TypeOf objSource Is eZee.Common.eZeeNumericTextBox, _
                        TypeOf objSource Is eZee.Common.eZeePopUp

                        objSource.caption = getCaption(objSource.Name, objSource.caption)
                        objSource.Ok_Caption = getCaption(objSource.Name & "_Ok_Caption", objSource.Ok_Caption)
                        objSource.Cancel_Caption = getCaption(objSource.Name & "_Cancel_Caption", objSource.Cancel_Caption)

                        'Rashmi (03 Apr 2009) -- Start
                    Case TypeOf objSource Is TreeView
                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call getControlsLanguage(objSource.ContextMenuStrip)
                        End If
                        'Rashmi (03 Apr 2009) -- End
                End Select

            Catch ex As System.Exception
                Call DisplayError.Show(-1, ex.Message, "getControlsLanguage", mstrModuleName)
            Finally
                ctlChild = Nothing
            End Try
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region " For New Language Style "
    Public Sub setLanguage(ByVal FormName As String)
        'If User._Object._LanguageId = 0 Then Exit Sub
        'Pinkal (10-Feb-2021) -- Start
        'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
        m_strFormName = FormName
        'Pinkal (10-Feb-2021) -- End

        If mintLangId = 0 Then Exit Sub

        Dim strFilter As String = ""

        Try
            'S.SANDEEP |20-JAN-2022| -- START
            'ISSUE : Object reference not set to an instance of an object.Proc Name : getCaption - clsLocalization
            If mdsLanguage Is Nothing Then Exit Sub
            'S.SANDEEP |20-JAN-2022| -- END

            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            'm_strFormName = FormName
            'Pinkal (10-Feb-2021) -- End

            strFilter = " formname = '" & FormName & "' "

            mdvLanguage = New DataView(mdsLanguage.Tables("Language"), strFilter, "controlname", DataViewRowState.CurrentRows)

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "setLanguage", mstrModuleName)
        Finally

        End Try
    End Sub

    Public Sub setFullLanguage(ByVal pForm As Form)
        Dim strFilter As String = ""

        Try
            m_strFormName = pForm.Name

            'Sandeep | 17 JAN 2011 | -- START
            'strFilter = " formname = '" & pForm.Name & "' AND application = 0"
            strFilter = " formname = '" & pForm.Name & "'"
            'Sandeep | 17 JAN 2011 | -- END 

            Call setControlsLanguage(pForm)
            Call SaveValue()

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "setLanguage", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub setControlsLanguage(ByVal objSource As Object)
        Try
            Dim ctlChild As Control
            Try
                Select Case True
                    'Main Form
                    Case TypeOf objSource Is Form
                        If objSource.Name.ToUpper = "FRMNEWMDI" Or objSource.Name.ToUpper = "FRMMDI" Then

                        Else
                            Call setCaption(objSource.Name, objSource.Text)
                        End If

                        For Each ctlChild In objSource.Controls
                            Call setControlsLanguage(ctlChild)
                        Next

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        'Jitu (13 Aug 2009) --Start
                        'Case TypeOf objSource Is Divelements.SandRibbon.Ribbon
                        '    Dim ctlRControl As Divelements.SandRibbon.Ribbon
                        '    ctlRControl = CType(objSource, Divelements.SandRibbon.Ribbon)

                        '    Call setControlsLanguage(ctlRControl.ApplicationMenu)

                        '    For Each objTab As Divelements.SandRibbon.RibbonTab In ctlRControl.Tabs
                        '        Call setControlsLanguage(objTab)
                        '    Next

                        'Case TypeOf objSource Is Divelements.SandRibbon.RibbonTab
                        '    Dim ctlTab As Divelements.SandRibbon.RibbonTab
                        '    ctlTab = CType(objSource, Divelements.SandRibbon.RibbonTab)

                        '    Call setCaption(ctlTab.Tag, ctlTab.Text)

                        '    For Each objGroup As Divelements.SandRibbon.RibbonChunk In ctlTab.Chunks
                        '        Call setCaption(objGroup.Tag, objGroup.Text)
                        '        Call setControlsLanguage(objGroup)
                        '    Next

                        'Case TypeOf objSource Is Divelements.SandRibbon.RibbonChunk
                        '    Dim ctlTab As Divelements.SandRibbon.RibbonChunk
                        '    ctlTab = CType(objSource, Divelements.SandRibbon.RibbonChunk)

                        '    For i As Integer = 0 To ctlTab.Items.Count - 1
                        '        If TypeOf ctlTab.Items(i) Is Divelements.SandRibbon.Button Then
                        '            Call setControlsLanguage(ctlTab.Items(i))
                        '        ElseIf TypeOf ctlTab.Items(i) Is Divelements.SandRibbon.StripLayout Then
                        '            Call setControlsLanguage(ctlTab.Items(i))
                        '        End If
                        '    Next
                        'Case TypeOf objSource Is Divelements.SandRibbon.ToolBar
                        '    Dim ctlTab As Divelements.SandRibbon.ToolBar
                        '    ctlTab = CType(objSource, Divelements.SandRibbon.ToolBar)

                        '    For i As Integer = 0 To ctlTab.Items.Count - 1
                        '        If TypeOf ctlTab.Items(i) Is Divelements.SandRibbon.Button Then
                        '            Call setControlsLanguage(ctlTab.Items(i))
                        '        ElseIf TypeOf ctlTab.Items(i) Is Divelements.SandRibbon.StripLayout Then
                        '            Call setControlsLanguage(ctlTab.Items(i))
                        '        ElseIf TypeOf ctlTab.Items(i) Is Divelements.SandRibbon.Label Then
                        '            Call setControlsLanguage(ctlTab.Items(i))
                        '        End If
                        '    Next

                        'Case TypeOf objSource Is Divelements.SandRibbon.StripLayout
                        '    Dim objCtl As Divelements.SandRibbon.StripLayout
                        '    objCtl = CType(objSource, Divelements.SandRibbon.StripLayout)

                        '    For i As Integer = 0 To objCtl.Items.Count - 1
                        '        If TypeOf objCtl.Items(i) Is Divelements.SandRibbon.Button Then
                        '            Call setControlsLanguage(objCtl.Items(i))
                        '        ElseIf TypeOf objCtl.Items(i) Is Divelements.SandRibbon.MenuItem Then
                        '            Call setControlsLanguage(objCtl.Items(i))
                        '        ElseIf TypeOf objCtl.Items(i) Is Divelements.SandRibbon.Menu Then
                        '            Call setControlsLanguage(objCtl.Items(i))
                        '        End If
                        '    Next

                        'Case TypeOf objSource Is Divelements.SandRibbon.Button
                        '    Call setCaption(objSource.Tag, objSource.Text)

                        '    Dim objCtl As Divelements.SandRibbon.Button
                        '    objCtl = CType(objSource, Divelements.SandRibbon.Button)

                        '    If objCtl.PopupWidget IsNot Nothing Then
                        '        Call setControlsLanguage(objCtl.PopupWidget)
                        '    End If

                        'Case TypeOf objSource Is Divelements.SandRibbon.MenuItem
                        '    Call setCaption(objSource.Tag, objSource.Text)

                        '    Dim objCtl As Divelements.SandRibbon.MenuItem
                        '    objCtl = CType(objSource, Divelements.SandRibbon.MenuItem)

                        '    For i As Integer = 0 To objCtl.Items.Count - 1
                        '        If TypeOf objCtl.Items(i) Is Divelements.SandRibbon.Button Then
                        '            Call setControlsLanguage(objCtl.Items(i))
                        '        ElseIf TypeOf objCtl.Items(i) Is Divelements.SandRibbon.Menu Then
                        '            Call setControlsLanguage(objCtl.Items(i))
                        '        End If
                        '    Next
                        '    'Jitu (13 Aug 2009) --End

                        'Case TypeOf objSource Is Divelements.SandRibbon.Label
                        '    Call setCaption(objSource.Tag, objSource.Text)

                        'NonContainer Control
                    Case TypeOf objSource Is Label, _
                         TypeOf objSource Is CheckBox, _
                         TypeOf objSource Is Button, _
                         TypeOf objSource Is RadioButton, _
                         TypeOf objSource Is eZee.Common.eZeeSplitButton


                        If TypeOf objSource Is eZee.Common.eZeeSplitButton Then
                            If CType(objSource, eZee.Common.eZeeSplitButton).SplitButtonMenu IsNot Nothing Then
                                Call setControlsLanguage(CType(objSource, eZee.Common.eZeeSplitButton).SplitButtonMenu)
                            End If
                        End If

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        Call setCaption(objSource.Name, objSource.Text)

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        'Container Control
                    Case TypeOf objSource Is GroupBox, _
                        TypeOf objSource Is eZee.Common.eZeeCollapsibleContainer

                        Call setCaption(objSource.Name, objSource.Text)

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        For Each ctlChild In objSource.Controls
                            Call setControlsLanguage(ctlChild)
                        Next

                    Case TypeOf objSource Is ListView
                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        For Each objColumn As ColumnHeader In objSource.Columns
                            If objColumn.Tag <> "" Then
                                Call setCaption(objColumn.Tag, objColumn.Text)
                            End If
                        Next

                    Case TypeOf objSource Is DataGridView
                        'Rashmi (03 Apr 2009) -- Start
                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If
                        'Rashmi (03 Apr 2009) -- End

                        For Each objColumn As DataGridViewColumn In objSource.Columns
                            If objColumn.Name <> "" Then
                                Call setCaption(objColumn.Name, objColumn.HeaderText)
                            End If
                        Next

                    Case TypeOf objSource Is eZee.Common.NavigationBar
                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        If TypeOf objSource Is eZee.Common.eZeeCollapseContainer Then
                            Call setCaption(objSource.Name, objSource.headertext)
                        End If

                        For Each ctlChild In objSource.Controls
                            Call setControlsLanguage(ctlChild)
                        Next

                        'Non Text Controls
                    Case TypeOf objSource Is Panel, _
                         TypeOf objSource Is PictureBox, _
                         TypeOf objSource Is SplitContainer, _
                         TypeOf objSource Is SplitterPanel

                        If objSource.Name.ToString.ToUpper.StartsWith("XOBJ") Then Exit Sub

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        If TypeOf objSource Is eZee.Common.eZeeCollapseContainer Then
                            Call setCaption(objSource.Name, objSource.headertext)
                        End If

                        For Each ctlChild In objSource.Controls
                            Call setControlsLanguage(ctlChild)
                        Next

                    Case TypeOf objSource Is TabControl
                        For Each objTabPage As TabPage In objSource.TabPages
                            Call setCaption(objTabPage.Name, objTabPage.Text)
                            Call setControlsLanguage(objTabPage)
                        Next

                        'Special Contaols
                    Case TypeOf objSource Is eZee.Common.eZeeImageHeader
                        Call setCaption(objSource.Name & "_Title", objSource.Title)
                        Call setCaption(objSource.Name & "_Message", objSource.Message)

                    Case TypeOf objSource Is eZee.Common.eZeeHeader
                        Call setCaption(objSource.Name & "_Title", objSource.Title)
                        Call setCaption(objSource.Name & "_Message", objSource.Message)

                    Case TypeOf objSource Is eZee.Common.eZeeLine
                        Call setCaption(objSource.Name, objSource.Text)

                    Case TypeOf objSource Is eZee.Common.eZeeCollapseContainer
                        Call setCaption(objSource.Name, objSource.headertext)
                        'objSource.CaptionText = setCaption(objSource.Name, objSource.CaptionText)

                        For Each ctlChild In objSource.Controls
                            Call setControlsLanguage(ctlChild)
                        Next

                    Case TypeOf objSource Is eZee.SideBar.eZeePane
                        For Each ctlChild In objSource.Controls
                            Call setControlsLanguage(ctlChild)
                        Next

                    Case TypeOf objSource Is eZee.SideBar.eZeeItem
                        Call setCaption(objSource.Name, objSource.Text)

                    Case TypeOf objSource Is eZee.Common.eZeeWizard

                        Call setCaption(objSource.Name & "_CancelText", objSource.CancelText)
                        Call setCaption(objSource.Name & "_NextText", objSource.NextText)
                        Call setCaption(objSource.Name & "_BackText", objSource.BackText)
                        Call setCaption(objSource.Name & "_FinishText", objSource.FinishText)
                        'Anjan [09 Jan 2015] -- Start
                        'ENHANCEMENT : included save & Finish button language on wizard control.
                        Call setCaption(objSource.Name & "_SaveText", objSource.SaveText)
                        'Anjan [09 Jan 2015] -- End

                        For Each page As eZee.Common.eZeeWizardPage In objSource.Pages
                            Call setControlsLanguage(page)
                        Next

                    Case TypeOf objSource Is MenuStrip, _
                         TypeOf objSource Is ContextMenuStrip

                        'For Each objMenuItem As ToolStripMenuItem In objSource.Items
                        '    Call setCaption(objMenuItem.Name, objMenuItem.Text)
                        '    If TypeOf objSource Is ToolStripSeparator Then
                        '        Continue For
                        '    Else
                        '        If objMenuItem.DropDownItems.Count > 0 Then
                        '            Call setControlsLanguage(objMenuItem)
                        '        End If
                        '    End If
                        'Next
                        For Each objMenuItem As ToolStripItem In objSource.Items
                            If TypeOf objSource Is ToolStripSeparator Then
                                Continue For
                            ElseIf TypeOf objMenuItem Is ToolStripMenuItem Then
                                Call setCaption(objMenuItem.Name, objMenuItem.Text)
                                Dim objtsmi As ToolStripMenuItem
                                objtsmi = CType(objMenuItem, ToolStripMenuItem)
                                If objtsmi.DropDownItems.Count > 0 Then
                                    Call setControlsLanguage(objMenuItem)
                                End If
                            End If
                        Next

                    Case TypeOf objSource Is ToolStripMenuItem

                        'For Each objMenuItem As ToolStripMenuItem In objSource.DropDownItems
                        '    Call setCaption(objMenuItem.Name, objMenuItem.Text)

                        '    If objMenuItem.DropDownItems.Count > 0 Then
                        '        Call setControlsLanguage(objMenuItem)
                        '    End If
                        'Next
                        For Each objMenuItem As ToolStripItem In objSource.DropDownItems
                            If TypeOf objSource Is ToolStripSeparator Then
                                Continue For
                            ElseIf TypeOf objMenuItem Is ToolStripMenuItem Then
                                Call setCaption(objMenuItem.Name, objMenuItem.Text)
                                Dim objtsmi As ToolStripMenuItem
                                objtsmi = CType(objMenuItem, ToolStripMenuItem)
                                If objtsmi.DropDownItems.Count > 0 Then
                                    Call setControlsLanguage(objMenuItem)
                                End If
                            End If
                        Next


                    Case TypeOf objSource Is ToolStrip

                        For Each objMenuItem As ToolStripItem In objSource.Items
                            Call setCaption(objMenuItem.Name, objMenuItem.Text)
                        Next

                    Case TypeOf objSource Is eZee.Common.NavigationButton
                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If

                        Dim objBut As eZee.Common.NavigationButton
                        objBut = DirectCast(objSource, eZee.Common.NavigationButton)
                        Call setCaption(objBut.Name, objBut.Text)

                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If

                    Case TypeOf objSource Is eZee.Common.eZeeTextBox, _
                        TypeOf objSource Is eZee.Common.eZeeNumericTextBox, _
                        TypeOf objSource Is eZee.Common.eZeePopUp

                        Call setCaption(objSource.Name, objSource.caption)
                        Call setCaption(objSource.Name & "_Ok_Caption", objSource.Ok_Caption)
                        Call setCaption(objSource.Name & "_Cancel_Caption", objSource.Cancel_Caption)

                        'Rashmi (03 Apr 2009) -- Start
                    Case TypeOf objSource Is TreeView
                        If objSource.ContextMenuStrip IsNot Nothing Then
                            Call setControlsLanguage(objSource.ContextMenuStrip)
                        End If
                        'Rashmi (03 Apr 2009) -- End
                End Select

            Catch ex As System.Exception
                Call DisplayError.Show(-1, ex.Message, "setControlsLanguage", mstrModuleName)
            Finally
                ctlChild = Nothing
            End Try
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Find language caption from cfLanguage to given form or control and it's child controls.
    ''' Modify By: Naimish
    ''' </summary>
    ''' <param name="ControlName">control name which caption want to find.</param>
    ''' <param name="DefaultCaption">Given control's default caption.</param>
    ''' <returns > A String which determine a control caption.</returns>
    Public Function getCaption(ByVal ControlName As String, ByVal DefaultCaption As String) As String
        'if user._object._languageid = 0 then return defaultcaption
        If mintLangId = 0 Then Return DefaultCaption

        If ControlName Is Nothing Then Return DefaultCaption
        If ControlName.ToUpper.StartsWith("OBJ") Or ControlName.ToUpper = "FRMMDI" Then Return DefaultCaption

        Try
            'mdvLanguage.RowFilter = "controlname = '" & ControlName.ToUpper & "' "

            'S.SANDEEP |20-JAN-2022| -- START
            'ISSUE : Object reference not set to an instance of an object.Proc Name : getCaption - clsLocalization
            'Dim objDataRowView() As DataRowView = mdvLanguage.FindRows(ControlName.ToUpper) 'HRK -- Optimum Performance Boost
            If mdvLanguage Is Nothing Then Return DefaultCaption

            Dim objDataRowView() As DataRowView = mdvLanguage.FindRows(ControlName.ToUpper) 'HRK -- Optimum Performance Boost
            'S.SANDEEP |20-JAN-2022| -- END

            If objDataRowView.Length <= 0 Then Return DefaultCaption
            'Select Case User._Object._LanguageId
            Select Case mintLangId
                Case 0
                    Return objDataRowView(0).Row.Item("language").ToString
                Case 1
                    Return objDataRowView(0).Row.Item("language1").ToString
                Case 2
                    Return objDataRowView(0).Row.Item("language2").ToString
                Case Else
                    Return DefaultCaption
            End Select

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "getCaption", mstrModuleName)
            Return DefaultCaption
        End Try
    End Function

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    'Private Sub setCaption(ByVal ControlName As String, ByVal DefaultCaption As String)
    Public Sub setCaption(ByVal ControlName As String, ByVal DefaultCaption As String)
        'Sohail (22 Nov 2018) -- End
        If ControlName Is Nothing Then Exit Sub
        If ControlName.ToUpper.StartsWith("OBJ") Or ControlName.ToUpper = "FRMMDI" Then Exit Sub
        Dim strFilter As String = " formname = '" & m_strFormName & "' AND controlname = '" & ControlName & "' "
        Dim dtRows() As DataRow
        Try
            'S.SANDEEP |20-JAN-2022| -- START
            'ISSUE : Object reference not set to an instance of an object.Proc Name : getCaption - clsLocalization
            'dtRows = mdsLanguage.Tables(0).Select(strFilter)
            If mdsLanguage Is Nothing Then Exit Sub

            dtRows = mdsLanguage.Tables(0).Select(strFilter)
            'S.SANDEEP |20-JAN-2022| -- END


            If dtRows.Length > 0 Then
                'If User._Object._LanguageId = 0 Then
                If mintLangId = 0 Then
                    dtRows(0).Item("language") = DefaultCaption
                End If
            Else
                Dim DvRow As DataRow = mdsLanguage.Tables(0).NewRow

                'Sandeep | 17 JAN 2011 | -- START
                'DvRow.Item("application") = m_intApplicationType
                'Sandeep | 17 JAN 2011 | -- END 
                DvRow.Item("formname") = m_strFormName
                DvRow.Item("controlname") = ControlName
                DvRow.Item("language") = DefaultCaption
                DvRow.Item("language1") = DefaultCaption
                DvRow.Item("language2") = DefaultCaption

                DvRow.EndEdit()
                mdsLanguage.Tables(0).Rows.Add(DvRow)
            End If

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "setCaption", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Messages "

    'Sandeep | 17 JAN 2011 | -- START
    '''' <summary>
    '''' Find custom messages text from cfmessages from given code.
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <param name="MsgCode">Message Code number.</param>
    '''' <returns > A String which determine a custom message text.</returns>
    'Public Function getMessage(ByVal strModuleName As String, _
    '                                ByVal MsgCode As Integer, _
    '                                ByVal strDefaultMsg As String, _
    '                                Optional ByVal intApplicationID As Integer = -1, _
    '                                Optional ByVal intLanguageID As Integer = -1) As String
    ''' <summary>
    ''' Find custom messages text from cfmessages from given code.
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <param name="MsgCode">Message Code number.</param>
    ''' <returns > A String which determine a custom message text.</returns>
    Public Function getMessage(ByVal strModuleName As String, _
                                ByVal MsgCode As Integer, _
                                ByVal strDefaultMsg As String, _
                                Optional ByVal intLanguageID As Integer = -1) As String
        'Sandeep | 17 JAN 2011 | -- END 

        Dim strFilter As String = ""
        Dim dtRows() As DataRow
        Try
            'S.SANDEEP |20-JAN-2022| -- START
            'ISSUE : Object reference not set to an instance of an object.Proc Name : getCaption - clsLocalization
            If mdsMessages Is Nothing Then Return strDefaultMsg
            'S.SANDEEP |20-JAN-2022| -- END


            'Sandeep | 17 JAN 2011 | -- START
            'If intApplicationID < 0 Then intApplicationID = m_intApplicationType
            'Sandeep | 17 JAN 2011 | -- END 

            'Sohail (07 Dec 2013) -- Start
            'Enhancement - setting for language in web
            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Session IsNot Nothing AndAlso HttpContext.Current.Session("LangId") IsNot Nothing Then
                intLanguageID = CInt(HttpContext.Current.Session("LangId"))
            End If
            'Sohail (07 Dec 2013) -- End

            'If intLanguageID = -1 Then intLanguageID = User._Object._LanguageId
            If intLanguageID = -1 Then intLanguageID = mintLangId


            'Sandeep | 17 JAN 2011 | -- START
            'strFilter = " Application = " & intApplicationID & _
            '                     " AND module_name = '" & strModuleName & "' " & _
            '                     " AND messagecode = '" & MsgCode & "' "

            strFilter = " module_name = '" & strModuleName & "' " & _
                     " AND messagecode = '" & MsgCode & "' "
            'Sandeep | 17 JAN 2011 | -- END 


            dtRows = mdsMessages.Tables(0).Select(strFilter)

            If dtRows.Length > 0 Then
                Select Case intLanguageID
                    Case 1
                        Return dtRows(0).Item("message1").ToString & ""
                    Case 2
                        Return dtRows(0).Item("message2").ToString & ""
                    Case Else
                        Return strDefaultMsg
                End Select
            Else
                Return strDefaultMsg
            End If

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "getMessage", mstrModuleName)
            Return strDefaultMsg
        End Try
    End Function



    'Sandeep | 17 JAN 2011 | -- START
    'Public Sub setMessage(ByVal strModuleName As String, _
    '                                ByVal MsgCode As Integer, _
    '                                ByVal strDefaultMsg As String, _
    '                                Optional ByVal intApplicationID As Integer = -1, _
    '                                Optional ByVal intLanguageID As Integer = -1)

    Public Sub setMessage(ByVal strModuleName As String, _
                                ByVal MsgCode As Integer, _
                                ByVal strDefaultMsg As String, _
                                Optional ByVal intLanguageID As Integer = -1)
        'Sandeep | 17 JAN 2011 | -- END 

        Dim strFilter As String = ""
        Dim dtRows() As DataRow
        Try

            'S.SANDEEP |20-JAN-2022| -- START
            'ISSUE : Object reference not set to an instance of an object.Proc Name : getCaption - clsLocalization
            If mdsMessages Is Nothing Then Exit Sub
            'S.SANDEEP |20-JAN-2022| -- END

            'Sandeep | 17 JAN 2011 | -- START
            'If intApplicationID < 0 Then intApplicationID = m_intApplicationType
            'Sandeep | 17 JAN 2011 | -- END 
            'If intLanguageID = -1 Then intLanguageID = User._Object._LanguageId

            If intLanguageID = -1 Then intLanguageID = mintLangId


            'Sandeep | 17 JAN 2011 | -- START
            'If intApplicationID < 0 Then
            '    strFilter = " module_name = '" & strModuleName & "' " & _
            '            " AND messagecode = '" & MsgCode & "' "
            'Else
            '    strFilter = " Application = " & intApplicationID & _
            '             " AND module_name = '" & strModuleName & "' " & _
            '             " AND messagecode = '" & MsgCode & "' "
            'End If

            strFilter = " module_name = '" & strModuleName & "' " & _
                    " AND messagecode = '" & MsgCode & "' "
            'Sandeep | 17 JAN 2011 | -- END 


            dtRows = mdsMessages.Tables(0).Select(strFilter)

            If dtRows.Length > 0 Then
                dtRows(0).Item("message") = strDefaultMsg
            Else
                Dim DvRow As DataRow = mdsMessages.Tables(0).NewRow
                DvRow.Item("messagecode") = MsgCode
                'Sandeep | 17 JAN 2011 | -- START
                'DvRow.Item("application") = intApplicationID
                'Sandeep | 17 JAN 2011 | -- END 
                DvRow.Item("module_name") = strModuleName
                DvRow.Item("message") = strDefaultMsg
                DvRow.Item("message1") = strDefaultMsg
                DvRow.Item("message2") = strDefaultMsg
                mdsMessages.Tables(0).Rows.Add(DvRow)
            End If

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "setMessage", mstrModuleName)
        End Try
    End Sub
#End Region

    Public Sub ctlRightToLeftlayOut(ByVal ctlParents As Object)
        Dim i As Integer

        If ctlParents.dock = DockStyle.Right Then
            ctlParents.dock = DockStyle.Left
        ElseIf ctlParents.dock = DockStyle.Left Then
            ctlParents.dock = DockStyle.Right
        End If

        If TypeOf ctlParents Is Form Then
            For i = 0 To ctlParents.Controls.Count - 1
                If TypeOf ctlParents.Controls(i) Is Panel Or _
                        TypeOf ctlParents.Controls(i) Is GroupBox Or _
                        TypeOf ctlParents.Controls(i) Is TabPage Or _
                        TypeOf ctlParents.Controls(i) Is TabControl Or _
                        TypeOf ctlParents.Controls(i) Is UserControl Then
                    ctlRightToLeftlayOut(ctlParents.Controls(i))
                End If
            Next
        ElseIf TypeOf ctlParents Is TabControl Then
            Dim tbc As TabControl
            tbc = CType(ctlParents, TabControl)
            tbc.RightToLeft = RightToLeft.Yes
            tbc.RightToLeftLayout = True
            For Each ctl As TabPage In tbc.TabPages
                ctlRightToLeftlayOut(ctl)
            Next
        ElseIf TypeOf ctlParents Is Panel Or _
            TypeOf ctlParents Is GroupBox Or _
            TypeOf ctlParents Is TabPage Or _
            TypeOf ctlParents Is TabControl Or _
            TypeOf ctlParents Is UserControl Then

            For i = 0 To ctlParents.Controls.Count - 1
                If TypeOf ctlParents.Controls(i) Is ListView Then
                    Dim lst As ListView
                    lst = CType(ctlParents.Controls(i), ListView)
                    lst.RightToLeft = RightToLeft.Yes
                    lst.RightToLeftLayout = True
                End If
                If TypeOf ctlParents.Controls(i) Is TreeView Then
                    Dim tv As TreeView
                    tv = CType(ctlParents.Controls(i), TreeView)
                    tv.RightToLeft = RightToLeft.Yes
                    tv.RightToLeftLayout = True
                End If
                ctlParents.Controls(i).Left = ctlParents.Width - (ctlParents.Controls(i).Left + ctlParents.Controls(i).Width)
                If TypeOf ctlParents.Controls(i) Is Panel Or _
                        TypeOf ctlParents.Controls(i) Is GroupBox Or _
                        TypeOf ctlParents.Controls(i) Is TabPage Or _
                        TypeOf ctlParents.Controls(i) Is TabControl Or _
                        TypeOf ctlParents.Controls(i) Is UserControl Then
                    ctlRightToLeftlayOut(ctlParents.Controls(i))
                End If
            Next
        End If
    End Sub
    
End Class
