'************************************************************************************************************************************
'Class Name : frmLanguage.vb
'Purpose    : show, edit objLanguage.
'Written By : 
'Modified   : Naimish 05/06/2009
'************************************************************************************************************************************

'************************************************************************************************************************************
'Class Name : frmFindReplaceLanguage.vb
'Purpose    : 
'Written By : 
'Modified   : Naimish 05/06/2009
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System


Public Class frmFindReplaceLanguage

    Public Event FindNext(ByVal sender As System.Object, ByVal e As SearchEvent)
    Public Event Replace(ByVal sender As System.Object, ByVal e As SearchEvent)
    Public Event ReplaceAll(ByVal sender As System.Object, ByVal e As SearchEvent)

    Private eSearchEvent As New SearchEvent

    Private mstrModuleName As String = "frmFindReplaceLanguage"

#Region " Form "
    Private Sub frmFindReplaceLanguage_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
        Me.Hide()
    End Sub

    Private Sub frmFindReplaceLanguage_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Escape Then
            Me.Hide()
        End If
    End Sub

    Private Sub frmFindReplaceLanguage_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnFindNext.PerformClick()
        End If
    End Sub

    Private Sub frmFindReplaceLanguage_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call Set_Logo(Me, gApplicationType)

        cboLanguage.Items.Clear()

        Dim arrList As New ArrayList
        arrList.Add(New ComboBoxValue("Language1", 1))
        arrList.Add(New ComboBoxValue("Language2", 2))



        'Pinkal (10-Jul-2014) -- Start
        'Enhancement - Language Settings
        Language.setLanguage(Me.Name)
        'Pinkal (10-Jul-2014) -- End

        Call OtherSettings()

        cboLanguage.DisplayMember = "Display"
        cboLanguage.ValueMember = "Value"
        cboLanguage.DataSource = arrList
    End Sub



    'Pinkal (10-Jul-2014) -- Start
    'Enhancement - Language Settings

    Private Sub frmFindReplaceLanguage_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            Me.SendToBack()
            objfrm.btnFindReplace.Visible = False
            objfrm.displayDialog(Me)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFindReplaceLanguage_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (10-Jul-2014) -- End

#End Region

#Region " Buttons "
    Private Sub btnFindNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindNext.Click
        RaiseEvent FindNext(Me, eSearchEvent)
    End Sub

    Private Sub btnReplaceAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReplaceAll.Click
        RaiseEvent ReplaceAll(Me, eSearchEvent)
    End Sub

    Private Sub btnReplace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReplace.Click
        RaiseEvent Replace(Me, eSearchEvent)
    End Sub
#End Region

#Region " Controls "
    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        eSearchEvent._FindWhat = txtSearch.Text
    End Sub

    Private Sub txtReplace_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReplace.TextChanged
        eSearchEvent._ReplaceWith = txtReplace.Text
    End Sub

    Private Sub cboLanguage_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLanguage.SelectedValueChanged
        eSearchEvent._LookIn = cboLanguage.SelectedValue
    End Sub

    Private Sub chkMatchWholeWord_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMatchWholeWord.CheckedChanged
        eSearchEvent._MatchWholeWord = chkMatchWholeWord.Checked
    End Sub

    Private Sub chkMatchCase_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMatchCase.CheckedChanged
        eSearchEvent._MatchCase = chkMatchCase.Checked
    End Sub

    Private Sub chkUseWildCards_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUseWildCards.CheckedChanged
        eSearchEvent._UseWildcards = chkUseWildCards.Checked
    End Sub
#End Region

#Region " UI Settings "
    
#End Region







	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFindOption.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFindOption.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnFindNext.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFindNext.GradientForeColor = GUI._ButttonFontColor

			Me.btnReplace.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReplace.GradientForeColor = GUI._ButttonFontColor

			Me.btnReplaceAll.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReplaceAll.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblReplace.Text = Language._Object.getCaption(Me.lblReplace.Name, Me.lblReplace.Text)
			Me.lblSearch.Text = Language._Object.getCaption(Me.lblSearch.Name, Me.lblSearch.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.gbFindOption.Text = Language._Object.getCaption(Me.gbFindOption.Name, Me.gbFindOption.Text)
			Me.chkUseWildCards.Text = Language._Object.getCaption(Me.chkUseWildCards.Name, Me.chkUseWildCards.Text)
			Me.chkMatchCase.Text = Language._Object.getCaption(Me.chkMatchCase.Name, Me.chkMatchCase.Text)
			Me.chkMatchWholeWord.Text = Language._Object.getCaption(Me.chkMatchWholeWord.Name, Me.chkMatchWholeWord.Text)
			Me.btnFindNext.Text = Language._Object.getCaption(Me.btnFindNext.Name, Me.btnFindNext.Text)
			Me.btnReplace.Text = Language._Object.getCaption(Me.btnReplace.Name, Me.btnReplace.Text)
			Me.btnReplaceAll.Text = Language._Object.getCaption(Me.btnReplaceAll.Name, Me.btnReplaceAll.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

Public Class SearchEvent
    Inherits System.EventArgs

    Public Sub New()

    End Sub

    Public Sub New( _
        ByVal strFindWhat As String, _
        ByVal strReplaceWith As String, _
        ByVal intLookIn As Integer, _
        ByVal blnMatchWholeWord As Boolean, _
        ByVal blnMatchCase As Boolean, _
        ByVal blnUseWildcards As Boolean)

        mstrFindWhat = strFindWhat
        mstrReplaceWith = strReplaceWith
        mintLookIn = intLookIn
        mblnMatchWholeWord = blnMatchWholeWord
        mblnMatchCase = blnMatchCase
        mblnUseWildcards = blnUseWildcards

    End Sub

    Private mstrFindWhat As String = ""
    Public Property _FindWhat() As String
        Get
            Return mstrFindWhat
        End Get
        Set(ByVal value As String)
            mstrFindWhat = value
        End Set
    End Property

    Private mstrReplaceWith As String = ""
    Public Property _ReplaceWith() As String
        Get
            Return mstrReplaceWith
        End Get
        Set(ByVal value As String)
            mstrReplaceWith = value
        End Set
    End Property

    Private mintLookIn As Integer = 1
    Public Property _LookIn() As Integer
        Get
            Return mintLookIn
        End Get
        Set(ByVal value As Integer)
            mintLookIn = value
        End Set
    End Property

    Private mblnMatchWholeWord As Boolean = False
    Public Property _MatchWholeWord() As Boolean
        Get
            Return mblnMatchWholeWord
        End Get
        Set(ByVal value As Boolean)
            mblnMatchWholeWord = value
        End Set
    End Property

    Private mblnMatchCase As Boolean = False
    Public Property _MatchCase() As Boolean
        Get
            Return mblnMatchCase
        End Get
        Set(ByVal value As Boolean)
            mblnMatchCase = value
        End Set
    End Property

    Private mblnUseWildcards As Boolean = False
    Public Property _UseWildcards() As Boolean
        Get
            Return mblnUseWildcards
        End Get
        Set(ByVal value As Boolean)
            mblnUseWildcards = value
        End Set
    End Property

End Class