﻿Imports eZeeCommonLib
Imports System.Management
Imports System.Globalization
Imports System.Xml
Imports System.DirectoryServices
Imports System.Runtime.CompilerServices
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.IO.Stream
Imports System.Net
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.ComponentModel
Imports System.Net.NetworkInformation

'Sandeep | 17 JAN 2011 | -- START
'Module modGlobal
Public Module modGlobal
    'Sandeep | 17 JAN 2011 | -- END 
    Private ReadOnly mstrModuleName As String = "modGlobal-[appLogix]"
    Private Declare Function GetShortPathName Lib "kernel32" Alias "GetShortPathNameA" (ByVal longPath As String, ByVal shortPath As String, ByVal shortBufferSize As Int32) As Int32
    Public gobjConfigOptions As clsConfigOptions
    Public gobjUser As New clsUserAddEdit
    Public gobjCompany As New clsCompany_Master
    Public gobjFinancial As New clsCompany_Master
    Public mstrAccessLevelIds As String = String.Empty
    Public mstrAccessLevelFilterString As String = String.Empty 'Sohail (17 Apr 2012)
    'Sohail (25 Aug 2010) -- Start
    'Public gstrConfigDatabaseName As String = "hrmsConfiguration"
    Public gApplicationType As enArutiApplicatinType = Nothing
    'Sohail (31 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public mstrAccessLevelBranchFilterString As String = String.Empty
    Public mstrAccessLevelDepartmentGroupFilterString As String = String.Empty
    Public mstrAccessLevelDepartmentFilterString As String = String.Empty
    Public mstrAccessLevelSectionGroupFilterString As String = String.Empty
    Public mstrAccessLevelSectionFilterString As String = String.Empty
    Public mstrAccessLevelUnitGroupFilterString As String = String.Empty
    Public mstrAccessLevelUnitFilterString As String = String.Empty
    Public mstrAccessLevelTeamFilterString As String = String.Empty
    Public mstrAccessLevelJobGroupFilterString As String = String.Empty
    Public mstrAccessLevelJobFilterString As String = String.Empty
    Public mstrAccessLevelClassGroupFilterString As String = String.Empty
    Public mstrAccessLevelClassFilterString As String = String.Empty
    'Sohail (31 May 2012) -- End

    'S.SANDEEP [ 09 AUG 2011 ] -- START
    'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
    Public gobjAppSettings As New clsApplicationSettings
    Public gobjGSetting As New clsGeneralSettings
    'S.SANDEEP [ 09 AUG 2011 ] -- END 
    'Sohail (25 Aug 2010) -- End

    'Sandeep | 17 JAN 2011 | -- START
    Public gobjLocalization As clsLocalization
    'Sandeep | 17 JAN 2011 | -- END 

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    'Issue : Prevetion of Popping Message for Exit Confirmation
    Public blnIsIdealTimeSet As Boolean = False
    'S.SANDEEP [ 12 OCT 2011 ] -- END 
    Public GintTotalApplicantToImport As Integer = 0

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP |14-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PM ERROR
    'Public StrModuleName1 As String = String.Empty
    'Public StrModuleName2 As String = String.Empty
    'Public StrModuleName3 As String = String.Empty
    'Public StrModuleName4 As String = String.Empty
    'Public StrModuleName5 As String = String.Empty

    Public StrModuleName1 As String = ""
    Public StrModuleName2 As String = ""
    Public StrModuleName3 As String = ""
    Public StrModuleName4 As String = ""
    Public StrModuleName5 As String = ""
    'S.SANDEEP |14-MAR-2020| -- END
    
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 11 MAR 2014 ] -- START
    Public iEmailRegxExpression As String = "^([a-zA-Z0-9_\-\.'’’]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'S.SANDEEP [ 11 MAR 2014 ] -- END


    'Sohail (10 Mar 2014) -- Start
    'Enhancement - Send Emails in Application Backgropund.
    Public gobjEmailList As New List(Of clsEmailCollection)
    'Sohail (10 Mar 2014) -- End

    'S.SANDEEP [29 APR 2015] -- START
    Public mdtTransferAsOnDate As Date = Now.Date
    'S.SANDEEP [29 APR 2015] -- END

    Public Enum StatusType
        Open = 1
        Close = 2
    End Enum

    'Public Enum enPaymentRefId
    '    LOAN = 1
    '    ADVANCE = 2
    '    PAYSLIP = 3
    '    SAVING_WITHDRAWAL = 4
    '    SAVING_REDEMPTION = 5
    '    SAVING_REPAYMENT = 6
    'End Enum


    Public Function getIP() As String
        Dim IPHEntry As System.Net.IPHostEntry
        Dim IPAdd() As System.Net.IPAddress

        Dim localHost As String
        Dim exForce As Exception

        Try
            localHost = System.Net.Dns.GetHostName()
            IPHEntry = System.Net.Dns.GetHostEntry(localHost)
            IPAdd = IPHEntry.AddressList

            Dim IP4 = New List(Of Net.IPAddress)(Net.Dns.GetHostEntry(localHost).AddressList).Find(Function(f) f.AddressFamily = Net.Sockets.AddressFamily.InterNetwork)
            'Return IPAdd(0).ToString
            Return IP4.ToString

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getIP", "modGlobal")
            Return ""
        Finally
            exForce = Nothing
        End Try

    End Function

    Public Function getHostName() As String
        Dim localHost As String
        Try
            localHost = System.Net.Dns.GetHostName()
            Return localHost
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getHostName", "modGlobal")
            Return ""
        End Try
    End Function

    'Gajanan [27-June-2020] -- Start
    'Enhancement:Create AT-Non Disclosure Declaration Report

    Public Function GetMACAddress() As String
        Dim nics As NetworkInterface() = NetworkInterface.GetAllNetworkInterfaces()
        Dim sMacAddress As String = String.Empty

        For Each adapter As NetworkInterface In nics
            If sMacAddress = String.Empty Then
                Dim properties As IPInterfaceProperties = adapter.GetIPProperties()

                Dim regex = "(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})"
                Dim replace = "$1-$2-$3-$4-$5-$6"
                sMacAddress = New Regex(regex).Replace(adapter.GetPhysicalAddress().ToString(), regex, replace)
                Exit For
            End If
        Next

        Return sMacAddress
    End Function

    <DllImport("Ws2_32.dll")> _
 Private Function inet_addr(ByVal ip As String) As Int32
    End Function
    <DllImport("Iphlpapi.dll")> _
    Private Function SendARP(ByVal dest As Int32, ByVal host As Int32, ByRef mac As Int64, ByRef length As Int32) As Integer
    End Function
    Public Function GetClientMac(ByVal _sClientIP As String) As String
        Dim _sMacAddress As String = String.Empty
        Try
            Dim _nDescIp As Int32 = inet_addr(_sClientIP)
            Dim _nLocalHost As Int32 = inet_addr(String.Empty)
            Dim _nMacInfo As New Int64()
            Dim len As Int32 = 6
            Dim _nClientMacInfo As Integer = SendARP(_nDescIp, 0, _nMacInfo, len)
            _sMacAddress = _nMacInfo.ToString("X")
            Dim _nMacLength As Integer = _sMacAddress.Length
            If _nMacLength = 12 Then
                Dim _sbMac As New StringBuilder()
                For i As Integer = _nMacLength To 1 Step -1
                    If i Mod 2 = 0 Then
                        _sbMac.AppendFormat("{0} ", _sMacAddress.Substring(i - 2, 2))
                    End If
                Next i
                _sMacAddress = _sbMac.ToString().TrimEnd().Replace(" "c, "-"c)
            End If
        Catch
            _sMacAddress = String.Empty
        End Try
        Return _sMacAddress
    End Function



    'Gajanan [27-June-2020] -- End

    'S.SANDEEP [ 10 May 2013 ] -- START
    'ENHANCEMENT : Database Setup Changes (SMO/DMO)
    'Public Function CheckDatabase() As Boolean
    '    Dim objDatabaseConn As New eZeeCommonLib.eZeeDatabase
    '    Try
    '        Dim blnFlag As Boolean = False
    '        Dim arrDB As ArrayList = objDatabaseConn.fillDatabaseList()
    '        For i As Integer = 0 To arrDB.Count - 1
    '            If arrDB(i).ToString.ToUpper = "HRMSCONFIGURATION" Then
    '                blnFlag = True
    '                Exit For
    '            End If
    '        Next

    '        Dim objAppSettings As New clsApplicationSettings
    '        If blnFlag = False Then

    '            If Dir(objAppSettings._ApplicationPath & "SQL_DMO.exe") <> "" Then
    '                Call Shell(objAppSettings._ApplicationPath & "SQL_DMO.exe -c", AppWinStyle.NormalFocus, True)
    '                eZeeCommonLib.eZeeDatabase.change_database("HRMSCONFIGURATION")
    '            End If

    '        End If

    '        blnFlag = False
    '        arrDB = objDatabaseConn.fillDatabaseList()
    '        For i As Integer = 0 To arrDB.Count - 1
    '            If arrDB(i).ToString.ToUpper = "HRMSCONFIGURATION" Then
    '                blnFlag = True
    '                Exit For
    '            End If
    '        Next

    '        Return blnFlag
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & vbCrLf & "[CheckDatabase," & "modGlobal" & "]")
    '        Return False
    '    Finally
    '        objDatabaseConn = Nothing
    '    End Try
    'End Function

    Public Function CheckDatabase() As Boolean
        Dim objDatabaseConn As New eZeeCommonLib.eZeeDatabase
        Try
            Dim blnFlag As Boolean = False
            Dim arrDB As ArrayList = objDatabaseConn.fillDatabaseList()
            For i As Integer = 0 To arrDB.Count - 1
                If arrDB(i).ToString.ToUpper = "HRMSCONFIGURATION" Then
                    blnFlag = True
                    Exit For
                End If
            Next

            Dim objAppSettings As New clsApplicationSettings
            If blnFlag = False Then

                If Dir(objAppSettings._ApplicationPath & "Aruti_SQL.exe") <> "" Then
                    Dim StrQ As String = ""
                    Dim objDataOp As New clsDataOperation

                    objDataOp.ExecNonQuery("USE [master]")

                    'Sandeep (27 -Nov 2017) - Start
                    'Issue: Not able to create database when application server is different then database server.
                    'StrQ = "IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'NT AUTHORITY\SYSTEM') " & _
                    '       "  DROP LOGIN [NT AUTHORITY\SYSTEM]; " & _
                    '       "IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'BUILTIN\Administrators') " & _
                    '       "  DROP LOGIN [BUILTIN\Administrators]; " & _
                    '       "IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'BUILTIN\Users') " & _
                    '       "  DROP LOGIN [BUILTIN\Users] "

                    'objDataOp.ExecNonQuery(StrQ)


                    'If objDataOp.ErrorMessage <> "" Then

                    '    eZeeMsgBox.Show("Sorry, Error in creating database. Please contact Aruti Support Team.", enMsgBoxStyle.Information)
                    '    Return False
                    'End If
                    'Sandeep (27 -Nov 2017) - End

                    StrQ = "IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'hrmsConfiguration') " & _
                           " DROP DATABASE [hrmsConfiguration] " & _
                           " CREATE DATABASE [hrmsConfiguration] "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then

                        eZeeMsgBox.Show("Sorry, Error in creating database. Please contact Aruti Support Team.", enMsgBoxStyle.Information)
                        Return False
                    End If
                    Call Shell(objAppSettings._ApplicationPath & "Aruti_SQL.exe " & "/N" & " " & """", AppWinStyle.NormalFocus, True)
                End If

            End If

            blnFlag = False
            arrDB = objDatabaseConn.fillDatabaseList()
            For i As Integer = 0 To arrDB.Count - 1
                If arrDB(i).ToString.ToUpper = "HRMSCONFIGURATION" Then
                    blnFlag = True
                    Exit For
                End If
            Next

            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & vbCrLf & "[CheckDatabase," & "modGlobal" & "]")
            Return False
        Finally
            objDatabaseConn = Nothing
        End Try
    End Function
    'S.SANDEEP [ 10 May 2013 ] -- END



    Public Sub Set_Logo(ByVal frm As Form, ByVal eAppType As enArutiApplicatinType)
        Try
            Select Case eAppType
                Case enArutiApplicatinType.Aruti_Configuration
                    frm.Icon = Global.Aruti.Data.My.Resources.aruti_config

                Case enArutiApplicatinType.Aruti_Payroll
                    frm.Icon = Global.Aruti.Data.My.Resources.logo_32
                    AddHandler frm.KeyDown, AddressOf frm_keydown

                Case enArutiApplicatinType.Aruti_TimeSheet
                    frm.Icon = Global.Aruti.Data.My.Resources.aruti_timesheet

            End Select

            RemoveHandler frm.KeyDown, AddressOf frm_keydown
            AddHandler frm.KeyDown, AddressOf frm_keydown
            Call SetCustomDateFormat(frm)
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Language._Object.setFullLanguage(frm)
            'S.SANDEEP [ 19 JULY 2012 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Logo", mstrModuleName)
        End Try
    End Sub


    'Pinkal (16-Apr-2016) -- Start
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.

    Public Function GetAll(ByVal control As Control, ByVal type As Type) As IEnumerable(Of Control)
        Dim controls = control.Controls.Cast(Of Control)()
        Return controls.SelectMany(Function(ctrl) GetAll(ctrl, type)).Concat(controls).Where(Function(c) c.GetType() Is type)
    End Function

    Public Sub SetCustomDateFormat(ByVal frm As Windows.Forms.Form)
        Try
            Dim ctl = GetAll(frm, GetType(DateTimePicker))
            Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
            NewCulture.DateTimeFormat.ShortDatePattern = ConfigParameter._Object._CompanyDateFormat
            NewCulture.DateTimeFormat.DateSeparator = ConfigParameter._Object._CompanyDateSeparator
            System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture

            For Each cl In ctl
                If CType(cl, DateTimePicker).ShowUpDown = False AndAlso CType(cl, DateTimePicker).Format <> DateTimePickerFormat.Custom AndAlso CType(cl, DateTimePicker).Format <> DateTimePickerFormat.Time Then
                    CType(cl, DateTimePicker).Format = DateTimePickerFormat.Custom
                    CType(cl, DateTimePicker).CustomFormat = ConfigParameter._Object._CompanyDateFormat
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCustomDateFormat", mstrModuleName)
        End Try
    End Sub

    'Pinkal (16-Apr-2016) -- End


    Private Sub frm_keydown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.F1 And e.KeyCode <> Keys.P Then
            Dim frm As Form = sender

            If gApplicationType = enArutiApplicatinType.Aruti_Payroll Then
                Help.ShowHelp(sender, "Help.chm", HelpNavigator.Topic, CType(sender, Form).Tag)
            Else
                Help.ShowHelp(sender, "help_config.chm", HelpNavigator.Topic, CType(sender, Form).Tag)
            End If

        End If
    End Sub



    'Pinkal (11-June-2011) -- Start

    'S.SANDEEP [ 21 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    'Public Sub SetUserTracingInfo(ByVal blnConfiguration As Boolean, ByVal UserMode As enUserMode)
    '    Try
    '        Dim objUserTracing As New clsUser_tracking_Tran

    '        If blnConfiguration Then
    '            objUserTracing._Companyunkid = -1
    '            objUserTracing._Yearunkid = -1
    '        Else
    '            objUserTracing._Companyunkid = Company._Object._Companyunkid
    '            objUserTracing._Yearunkid = FinancialYear._Object._YearUnkid
    '        End If

    '        objUserTracing._Isconfiguration = blnConfiguration
    '        objUserTracing._Userunkid = User._Object._Userunkid
    '        objUserTracing._Modeid = UserMode
    '        objUserTracing._Logindate = ConfigParameter._Object._CurrentDateAndTime

    '        Dim minttrackingunkid As Integer = objUserTracing.isUserLoginExist(objUserTracing._Userunkid, objUserTracing._Isconfiguration, objUserTracing._Modeid, objUserTracing._Logindate.Date)

    '        If minttrackingunkid > 0 Then
    '            objUserTracing._Trackingunkid = minttrackingunkid
    '            objUserTracing._Logindate = ConfigParameter._Object._CurrentDateAndTime
    '            objUserTracing._Ip = getIP()
    '            objUserTracing._Machine = getHostName()
    '            objUserTracing._Logindate = ConfigParameter._Object._CurrentDateAndTime
    '            objUserTracing.Update()
    '        Else
    '            objUserTracing._Ip = getIP()
    '            objUserTracing._Machine = getHostName()
    '            objUserTracing._Logindate = ConfigParameter._Object._CurrentDateAndTime
    '            objUserTracing.Insert()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetUserTracingInfo", mstrModuleName)
    '    End Try
    'End Sub


    'Public Sub SetUserTracingInfo(ByVal blnConfiguration As Boolean, _
    '                              ByVal UserMode As enUserMode, _
    '                              ByVal eLoginMode As enLogin_Mode, _
    '                              Optional ByVal mintSessionUserId As Integer = -1, _
    '                              Optional ByVal blnLogOut As Boolean = False, _
    '                              Optional ByVal StrDataBaseName As String = "")'S.SANDEEP [ 10 JULY 2012 ] -- START -- END

    Public Sub SetUserTracingInfo(ByVal blnConfiguration As Boolean, _
                                  ByVal UserMode As enUserMode, _
                                  ByVal eLoginMode As enLogin_Mode, _
                                  Optional ByVal mintSessionUserId As Integer = -1, _
                                  Optional ByVal blnLogOut As Boolean = False, _
                                  Optional ByVal StrDataBaseName As String = "", _
                                  Optional ByVal mintSessionCompanyId As Integer = -1, _
                                  Optional ByVal mintSessionYearId As Integer = -1, _
                                  Optional ByVal mstrIpAddress As String = "", _
                                  Optional ByVal mstrHostName As String = "")
        Try
            Dim objUserTracing As New clsUser_tracking_Tran
            Dim objConfig As New clsConfigOptions
            'S.SANDEEP [ 10 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim minttrackingunkid As Integer = objUserTracing.isUserLoginExist(IIf(mintSessionUserId <= 0, User._Object._Userunkid, mintSessionUserId), getIP(), eLoginMode)
            Dim minttrackingunkid As Integer = -1
            If blnLogOut = True Then
                minttrackingunkid = objUserTracing.isUserLoginExist(IIf(mintSessionUserId <= 0, User._Object._Userunkid, mintSessionUserId), _
                                                                    IIf(mstrIpAddress.Trim.Length > 0, mstrIpAddress, getIP()), _
                                                                    eLoginMode, _
                                                                    IIf(mintSessionCompanyId <= 0, Company._Object._Companyunkid, mintSessionCompanyId), _
                                                                    IIf(mintSessionYearId <= 0, FinancialYear._Object._YearUnkid, mintSessionYearId))
            Else

                minttrackingunkid = objUserTracing.isUserLoginExist(IIf(mintSessionUserId <= 0, User._Object._Userunkid, mintSessionUserId), _
                                                                    IIf(mstrIpAddress.Trim.Length > 0, mstrIpAddress, getIP()), _
                                                                    eLoginMode, _
                                                                    IIf(mintSessionCompanyId <= 0, Company._Object._Companyunkid, mintSessionCompanyId), _
                                                                    IIf(mintSessionYearId <= 0, FinancialYear._Object._YearUnkid, mintSessionYearId), _
                                                                    objConfig._CurrentDateAndTime)

            End If
            'S.SANDEEP [ 10 JULY 2012 ] -- END

            If minttrackingunkid <= 0 Then
                If blnConfiguration Then
                    objUserTracing._Companyunkid = -1
                    objUserTracing._Yearunkid = -1
                Else
                    'S.SANDEEP [ 10 JULY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'objUserTracing._Companyunkid = Company._Object._Companyunkid
                    'objUserTracing._Yearunkid = FinancialYear._Object._YearUnkid

                    objUserTracing._Companyunkid = IIf(mintSessionCompanyId <= 0, Company._Object._Companyunkid, mintSessionCompanyId)
                    objUserTracing._Yearunkid = IIf(mintSessionYearId <= 0, FinancialYear._Object._YearUnkid, mintSessionYearId)
                    'S.SANDEEP [ 10 JULY 2012 ] -- END
                End If
                objUserTracing._Isconfiguration = blnConfiguration
                If mintSessionUserId > 0 Then
                    objUserTracing._Userunkid = mintSessionUserId
                Else
                    objUserTracing._Userunkid = User._Object._Userunkid
                End If
                objUserTracing._Login_Modeid = eLoginMode
                If StrDataBaseName = "" Then
                    objUserTracing._Database_Name = IIf(FinancialYear._Object._DatabaseName.Trim.Length <= 0, FinancialYear._Object._ConfigDatabaseName, FinancialYear._Object._DatabaseName)
                Else
                    objUserTracing._Database_Name = StrDataBaseName
                End If
                objUserTracing._Modeid = UserMode

                'S.SANDEEP |18-OCT-2021| -- START
                'objUserTracing._Logindate = ConfigParameter._Object._CurrentDateAndTime
                objUserTracing._Logindate = objConfig._CurrentDateAndTime
                'S.SANDEEP |18-OCT-2021| -- End

                'Pinkal (03-Sep-2012) -- Start
                'Enhancement : TRA Changes
                'objUserTracing._Ip = getIP()
                objUserTracing._Ip = IIf(mstrIpAddress.Trim.Length > 0, mstrIpAddress, getIP())

                ' objUserTracing._Machine = getHostName()
                objUserTracing._Machine = IIf(mstrHostName.Trim.Length > 0, mstrHostName, getHostName())

                'Pinkal (03-Sep-2012) -- End

                'S.SANDEEP |18-OCT-2021| -- START
                'objUserTracing._Logindate = ConfigParameter._Object._CurrentDateAndTime
                objUserTracing._Logindate = objConfig._CurrentDateAndTime
                'S.SANDEEP |18-OCT-2021| -- End
                objUserTracing._Logout_Date = Nothing
                objUserTracing.Insert()
            Else
                objUserTracing._Trackingunkid = minttrackingunkid
                If blnLogOut = True Then
                    'S.SANDEEP |18-OCT-2021| -- START
                    'objUserTracing._Logout_Date = ConfigParameter._Object._CurrentDateAndTime
                    objUserTracing._Logout_Date = objConfig._CurrentDateAndTime
                    'S.SANDEEP |18-OCT-2021| -- END
                Else
                    objUserTracing._Logout_Date = Nothing
                End If
                objUserTracing.Update()
            End If
            objConfig = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetUserTracingInfo", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 21 MAY 2012 ] -- END



    'Pinkal (11-June-2011) -- End

    Public Function GetMotherBoardSrNo() As String
        Dim strNo As String = ""
        Try
            'Dim List
            'Dim Msg As String = ""
            'Dim A As Object

            'On Local Error Resume Next

            'List = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_BaseBoard")
            'For Each A In List
            '    Msg = Msg & "Motherboard Serial Number: " & A.SerialNumber & vbCrLf
            'Next

            'List = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_Processor")
            'For Each A In List
            '    Msg = Msg & "Processor Unique ID: " & A.UniqueID & vbCrLf
            'Next

            'List = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_BIOS")
            'For Each A In List
            '    Msg = Msg & "BIOS Serial Number: " & A.SerialNumber & vbCrLf
            'Next

            'List = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_LogicalDisk")
            'For Each A In List
            '    Msg = Msg & "Disk Serial Number: " & A.VolumeSerialNumber & vbCrLf
            'Next

            '*** Motherboard Serial Number ***
            Dim search As New ManagementObjectSearcher("SELECT SerialNumber FROM Win32_BaseBoard") 'SELECT Product, SerialNumber FROM Win32_BaseBoard
            Dim info As ManagementObjectCollection = search.Get()



            For Each obj As ManagementObject In info
                For Each Data As PropertyData In obj.Properties
                    'Sohail (05 Apr 2018) -- Start
                    'Powersoft Issue : on Recruitment Import from Google Cloud machine : Object reference not set to an instance of an object in 71.1.
                    'strNo = Data.Value.ToString
                    strNo = Convert.ToString(Data.Value)
                    'Sohail (05 Apr 2018) -- End
                Next
            Next

            '*** Processor Unique ID ***
            'search = New ManagementObjectSearcher("SELECT UniqueID FROM Win32_Processor")
            'info = search.Get()

            'For Each obj As ManagementObject In info
            '    For Each Data As PropertyData In obj.Properties
            '        Msg &= " -> " & Data.Name & " : " & Data.Value & vbCrLf
            '    Next
            'Next
            search.Dispose()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", "")
        End Try
        Return strNo
    End Function

    'S.SANDEEP [ 28 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}


    'S.SANDEEP [20-JUL-2017] -- START
    'ISSUE/ENHANCEMENT : AUTO NUMBER ISSUE
    'Public Function Set_AutoNumber(ByVal objDataOpr As clsDataOperation, _
    '                                   ByVal intUnkid As Integer, _
    '                                   ByVal StrTableName As String, _
    '                                   ByVal StrColumnName As String, _
    '                                   ByVal StrIdentityColName As String, _
    '                                   ByVal StrKeyName As String, ByVal StrPrifix As String, Optional ByVal intCompanyId As Integer = 0) As Boolean   'Pinkal (25-APR-2012) -- Start

    '    'Public Function Set_AutoNumber(ByVal objDataOpr As clsDataOperation, _
    '    '                                   ByVal intUnkid As Integer, _
    '    '                                   ByVal StrTableName As String, _
    '    '                                   ByVal StrColumnName As String, _
    '    '                                   ByVal StrIdentityColName As String, _
    '    '                                   ByVal StrKeyName As String, ByVal StrPrifix As String) As Boolean

    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        StrQ = "DECLARE @NUM AS NVARCHAR(MAX) " & _
    '               "DECLARE @CNUM AS INT " & _
    '               "SET @NUM = '' " & _
    '               "SET @CNUM = -1 " & _
    '               "SELECT @NUM = ISNULL(MAX(key_value),1) FROM hrmsConfiguration..cfconfiguration WHERE key_name = '" & StrKeyName & "' AND companyunkid = '" & IIf(intCompanyId > 0, intCompanyId, Company._Object._Companyunkid) & "'; " & _
    '               "SET @CNUM = @NUM ; "
    '        If StrPrifix.Trim.Length > 0 Then
    '            StrQ &= " SET @CNUM = @NUM ; SET @NUM = '" & StrPrifix & "' + CAST(@NUM AS NVARCHAR(MAX)); "
    '        End If
    '        StrQ &= "UPDATE " & StrTableName & " SET " & StrColumnName & " = @NUM WHERE " & StrIdentityColName & " = " & intUnkid & "; " & _
    '                "UPDATE hrmsConfiguration..cfconfiguration  SET key_value = @CNUM + 1 WHERE key_name = '" & StrKeyName & "' AND companyunkid = '" & IIf(intCompanyId > 0, intCompanyId, Company._Object._Companyunkid) & "' "

    '        objDataOpr.ExecNonQuery(StrQ)

    '        If objDataOpr.ErrorMessage <> "" Then
    '            If objDataOpr.ErrorMessage.ToString().Contains("UQ_") Then
    '                exForce = New Exception("This number is already generated in system for some transaction(s). Please check Auto number settings in configuration.")
    '                Throw exForce
    '            Else
    '                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
    '                Throw exForce
    '            End If
    '        End If

    '        Return True

    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '    End Try
    'End Function

    Public Function Set_AutoNumber(ByVal objDataOpr As clsDataOperation, _
                                       ByVal intUnkid As Integer, _
                                       ByVal StrTableName As String, _
                                       ByVal StrColumnName As String, _
                                       ByVal StrIdentityColName As String, _
                                       ByVal StrKeyName As String, ByVal StrPrifix As String, Optional ByVal intCompanyId As Integer = 0) As Boolean   'Pinkal (25-APR-2012) -- Start

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim intValue As Integer = 0
        Try
            While intValue <= 0
                StrQ = "DECLARE @NUM AS NVARCHAR(MAX) " & _
                       "DECLARE @CNUM AS INT " & _
                       "SET @NUM = '' " & _
                       "SET @CNUM = -1 " & _
                       "SELECT @NUM = ISNULL(MAX(key_value),1) FROM hrmsConfiguration..cfconfiguration WHERE key_name = '" & StrKeyName & "' AND companyunkid = '" & IIf(intCompanyId > 0, intCompanyId, Company._Object._Companyunkid) & "'; " & _
                       "SET @CNUM = @NUM ; "
                If StrPrifix.Trim.Length > 0 Then
                    StrQ &= " SET @CNUM = @NUM ; SET @NUM = '" & StrPrifix & "' + CAST(@NUM AS NVARCHAR(MAX)); "
                End If
                StrQ &= "UPDATE " & StrTableName & " SET " & StrColumnName & " = @NUM WHERE " & StrIdentityColName & " = " & intUnkid & " AND NOT EXISTS(SELECT 1 FROM " & StrTableName & " WHERE " & StrColumnName & " = @NUM); "

                intValue = objDataOpr.ExecNonQuery(StrQ)

                StrQ = "UPDATE hrmsConfiguration..cfconfiguration  SET key_value = CAST(key_value AS BIGINT) + 1 WHERE key_name = '" & StrKeyName & "' AND companyunkid = '" & IIf(intCompanyId > 0, intCompanyId, Company._Object._Companyunkid) & "' "
                objDataOpr.ExecNonQuery(StrQ)

                If intValue > 0 Then

                    If objDataOpr.ErrorMessage <> "" Then
                        If objDataOpr.ErrorMessage.ToString().Contains("UQ_") Then
                            exForce = New Exception("This number is already generated in system for some transaction(s). Please check Auto number settings in configuration.")
                            Throw exForce
                        Else
                            exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    Return True

                End If
            End While

        Catch ex As Exception

            'Gajanan [18-NOV-2019] -- Start   
            'Throw ex
            Throw New Exception(ex.Message & "; Procedure Name: Set_AutoNumber; Module Name: " & mstrModuleName)
            'Gajanan [18-NOV-2019] -- End   
        Finally
        End Try
    End Function
    'S.SANDEEP [20-JUL-2017] -- END




    'S.SANDEEP [ 28 MARCH 2012 ] -- END

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Get_Saved_Number(ByVal objDataOpr As clsDataOperation, _
                               ByVal intUnkid As Integer, _
                               ByVal StrTableName As String, _
                               ByVal StrColumnName As String, _
                               ByVal StrIdentityColName As String, _
                               ByRef StrVoucherNo As String) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsVoc As New DataSet
        Try
            StrQ = "SELECT " & StrColumnName & " FROM " & StrTableName & " WHERE " & StrIdentityColName & " = " & intUnkid

            dsVoc = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            End If

            If dsVoc.Tables("List").Rows.Count > 0 Then
                StrVoucherNo = dsVoc.Tables("List").Rows(0)(StrColumnName)
                Return True
            Else
                StrVoucherNo = ""
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Saved_Number", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 20 APRIL 2012 ] -- END


    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Sub Blank_ModuleName()
        StrModuleName1 = "" : StrModuleName2 = ""
        StrModuleName3 = "" : StrModuleName4 = ""
        StrModuleName5 = ""
    End Sub
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'Pinkal (25-Oct-2012) -- Start
    'Enhancement : TRA Changes

    Public Function GetWeekDayNumber(ByVal weekName As String) As Integer

        Dim weekNames As New Dictionary(Of String, Integer)
        Dim dayName As String() = CultureInfo.CurrentCulture.DateTimeFormat.DayNames()
        For i As Integer = 0 To 6
            weekNames.Add(dayName(i), i)
        Next

        Return weekNames(weekName)
    End Function

    'Pinkal (25-Oct-2012) -- End

    'Sohail (28 Oct 2017) -- Start
    'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
    Public Function CountNoOfWeekDays(ByVal intDay As DayOfWeek, ByVal dtStart As Date, ByVal dtEnd As Date) As Integer
        Dim intCount As Integer = 0
        Try
            Dim ts As TimeSpan = dtEnd - dtStart 'Total duration
            intCount = CInt(Math.Floor(ts.TotalDays / 7)) 'Number of whole weeks
            Dim intRemainder As Integer = CInt(ts.TotalDays Mod 7) 'Number of remaining days
            Dim intSinceLastDay As Integer = CInt(dtEnd.DayOfWeek - intDay) 'Number of days since last [day]
            If intSinceLastDay < 0 Then intSinceLastDay += 7 'Adjust for negative days since last [day]

            'If the days in excess of an even week are greater than or equal to the number days since the last [day], then count this one, too.
            If intRemainder >= intSinceLastDay Then intCount += 1

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CountNoOfWeekDays; Module Name: " & mstrModuleName)
        End Try
        Return intCount
    End Function
    'Sohail (28 Oct 2017) -- End



    'Pinkal (27-Mar-2013) -- Start
    'Enhancement : TRA Changes

    Public Function ImageCompression(ByVal mstrImagePath As String) As Byte()
        Const bmpW = 200
        Const bmpH = 200
        Dim newWidth As Integer = bmpW
        Dim newHeight As Integer = bmpH
        Dim upBmp As Bitmap = Bitmap.FromFile(mstrImagePath)
        Dim newBmp As Bitmap = New Bitmap(newWidth, newHeight, Imaging.PixelFormat.Format24bppRgb)
        newBmp.SetResolution(72, 72)
        Dim upWidth As Integer = upBmp.Width
        Dim upHeight As Integer = upBmp.Height

        If upBmp.Width > upBmp.Height Then
            upHeight = upBmp.Width
            upWidth = upBmp.Width
        ElseIf upBmp.Width < upBmp.Height Then
            upHeight = upBmp.Height
            upWidth = upBmp.Height
        End If

        Dim newX As Integer = 0
        Dim newY As Integer = 0
        Dim reDuce As Decimal
        If upWidth > upHeight Then
            reDuce = newWidth / upWidth
            newHeight = Int(upHeight * reDuce)
            newY = Int((bmpH - newHeight) / 2)
            newX = 0
        ElseIf upWidth < upHeight Then
            reDuce = newHeight / upHeight
            newWidth = Int(upWidth * reDuce)
            newX = Int((bmpW - newWidth) / 2)
            newY = 0
        ElseIf upWidth = upHeight Then
            reDuce = newHeight / upHeight
            newWidth = Int(upWidth * reDuce)
            newX = Int((bmpW - newWidth) / 2)
            newY = Int((bmpH - newHeight) / 2)
        End If

        Dim newGraphic As Graphics = Graphics.FromImage(newBmp)
        newGraphic.Clear(Color.WhiteSmoke)
        newGraphic.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        newGraphic.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        newGraphic.DrawImage(upBmp, newX, newY, newWidth, newHeight)
        Dim ms As New System.IO.MemoryStream()
        newBmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
        Return ms.ToArray()
    End Function

    'Pinkal (27-Mar-2013) -- End

    Public Function GetShortPath(ByVal strLongPath As String) As String
        Dim intLongPathLength As Int32

        'If strLongPath.StartsWith("\\") = True Then
        '    intLongPathLength = strLongPath.Length + 1
        'Else
        '    intLongPathLength = strLongPath.Length
        'End If
        intLongPathLength = strLongPath.Length + 1

        '
        'A string with a buffer to receive the short path from the api call…
        Dim strShortPath As String = Space(intLongPathLength)
        '
        'Will hold the return value of the api call which should be the length.
        Dim returnValue As Long
        '
        'Now call the function to do the conversion…
        returnValue = GetShortPathName(strLongPath, strShortPath, strShortPath.Length)

        'strShortPath = strShortPath.ToString

        Return strShortPath.ToString

    End Function


    'FOR GET TIME
    Public Function CalculateTime(ByVal IsHours As Boolean, ByVal intSecond As Integer) As Double
        Dim calctime As Double = 0
        Dim calcMinute As Double = 0
        Dim calMinute As Double = 0
        Try
            If IsHours Then
                calcMinute = intSecond / 60
                calMinute = calcMinute Mod 60
                Dim calHour As Double = calcMinute / 60
                Return CDec(CDec(Int(calHour) + (calMinute / 100)))
            Else
                calctime = CDec(intSecond / 60)
                Return Math.Round(calctime)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CalculateTime", "modglobal")
        End Try
        Return calctime
    End Function

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 200)  Working on An option to show/review % allocation of the projects..
    Public Function CalculateSecondsFromHrs(ByVal mstrHours As String) As Integer
        Dim mintSeconds As Integer = 0
        Try
            If mstrHours.Trim.Length > 0 Then
                Dim mintHours As Integer = mstrHours.Substring(0, mstrHours.Trim.IndexOf(":"))
                Dim mintMinutes As Integer = mstrHours.Substring(mstrHours.Trim.IndexOf(":") + 1, mstrHours.Trim.Length - (mstrHours.Trim.IndexOf(":") + 1))
                mintSeconds = (mintHours * 3600) + (mintMinutes * 60)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CalculateSecondsFromHrs", "modglobal")
        End Try
        Return mintSeconds
    End Function
    'Pinkal (28-Mar-2018) -- End


    'Sohail (08 May 2015) -- Start
    'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
    'S.SANDEEP [15 NOV 2016] -- START
    ''' <summary>
    ''' Get User Access Filter string from new employee transfer table and employee categorization table.
    ''' </summary>
    ''' <param name="xJoinUACQry">In All Employee Join Use This Variable As This Will Apply The User Access To All(USE THIS BEFORE WHERE BLOCK OF EACH QUERY)</param>
    ''' <param name="xUACFilter">This was Used Before !! PLEASE DO NOT USE THIS AS IT WILL NOT RETURN ANY STRING</param>
    ''' <param name="dtAsOnDate">Default : ConfigParameter._Object._EmployeeAsOnDate</param>
    ''' <param name="blnOnlyApprovedEmployees">Default : True</param>
    ''' <param name="strDatabaseName">Default : FinancialYear._Object._DatabaseName</param>
    ''' <param name="intUserId">Default : User._Object._Userunkid</param>
    ''' <param name="intCompId">Default : Company._Object._Companyunkid</param>
    ''' <param name="intYearId">Default : FinancialYear._Object._YearUnkid</param>
    ''' <param name="strUserAccessModeSetting">Default : ConfigParameter._Object._UserAccessModeSetting</param>
    Public Sub NewAccessLevelFilterString(ByRef xJoinUACQry As String, _
                                          ByRef xUACFilter As String, _
                                          Optional ByVal dtAsOnDate As Date = Nothing, _
                                          Optional ByVal blnOnlyApprovedEmployees As Boolean = True, _
                                          Optional ByVal strDatabaseName As String = "", _
                                          Optional ByVal intUserId As Integer = 0, _
                                          Optional ByVal intCompId As Integer = 0, _
                                          Optional ByVal intYearId As Integer = 0, _
                                          Optional ByVal strUserAccessModeSetting As String = "", _
                                          Optional ByVal xHr_EmployeeTableAlias As String = "", _
                                          Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                          Optional ByVal strORQueryForUserAccess As String = "" _
                                          )
        'Sohail (01 Jan 2021)- [strORQueryForUserAccess]
        'Pinkal (17-Dec-2015) -- Start    'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.[Optional ByVal blnAddApprovalCondition As Boolean = True]

        Dim strFilterString As String = String.Empty
        Try
            If strDatabaseName.Trim = "" Then strDatabaseName = FinancialYear._Object._DatabaseName
            If dtAsOnDate = Nothing Then dtAsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            If intUserId <= 0 Then intUserId = User._Object._Userunkid
            If intCompId <= 0 Then intCompId = Company._Object._Companyunkid
            If intYearId <= 0 Then intYearId = FinancialYear._Object._YearUnkid
            If xHr_EmployeeTableAlias.Trim = "" Then xHr_EmployeeTableAlias = "hremployee_master"

            Using xDataOpr As New clsDataOperation
                Dim dsList As New DataSet
                Dim exForce As Exception
                Dim StrQ As String = String.Empty
                Dim xAccessLevel As String = String.Empty

                xDataOpr.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                xDataOpr.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
                xDataOpr.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)

                If strUserAccessModeSetting.Trim = "" Then strUserAccessModeSetting = ConfigParameter._Object._UserAccessModeSetting
                Dim arrID As String() = strUserAccessModeSetting.Split(",")
                xJoinUACQry = " JOIN ( " & _
                        "SELECT  Trf_AS.TrfEmpId " & _
                        "FROM    ( SELECT    ETT.employeeunkid AS TrfEmpId  " & _
                                          ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                                          ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                                          ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                                          ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                                          ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                                          ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                                          ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                                          ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                                          ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                                          ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
                                          ", CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                                          ", ETT.employeeunkid " & _
                                          ", ROW_NUMBER() OVER ( PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC ) AS Rno " & _
                                  "FROM      " & strDatabaseName & "..hremployee_transfer_tran AS ETT " & _
                                  "WHERE     isvoid = 0 " & _
                                            "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtAsOnDate).ToString & "' " & _
                                ") AS Trf_AS " & _
                                "JOIN ( SELECT   ECT.employeeunkid AS CatEmpId  " & _
                                              ", ECT.jobgroupunkid " & _
                                              ", ECT.jobunkid " & _
                                              ", CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                                              ", ECT.employeeunkid " & _
                                              ", ROW_NUMBER() OVER ( PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC ) AS Rno " & _
                                       "FROM     " & strDatabaseName & "..hremployee_categorization_tran AS ECT " & _
                                       "WHERE    isvoid = 0 " & _
                                                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtAsOnDate).ToString & "' " & _
                                     ") AS RUCat ON Trf_AS.employeeunkid = RUCat.employeeunkid " & _
                        "WHERE   Trf_AS.Rno = 1 " & _
                                "AND RUCat.Rno = 1 "

                For i = 0 To arrID.Length - 1

                    StrQ = "SELECT " & _
                           "    STUFF((SELECT ',' + CAST(allocationunkid AS NVARCHAR(50)) " & _
                           "FROM hrmsConfiguration..cfuseraccess_privilege_tran " & _
                           "JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid " & _
                           "WHERE userunkid = @UserId AND companyunkid = @CompId AND yearunkid = @YearId AND referenceunkid = " & arrID(i).ToString & " " & _
                           "ORDER BY hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid FOR XML PATH('')),1,1,'') AS CSV "

                    dsList = xDataOpr.ExecQuery(StrQ, "List")

                    If xDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [25-JUL-2017] -- START
                    'ISSUE/ENHANCEMENT : IF EMPLOYEE HAVING ALLOCATION VALUE AS "0" THEY WERE COMING 
                    'If dsList.Tables("List").Rows.Count > 0 Then
                    '    If dsList.Tables("List").Rows(0)("CSV").ToString.Trim.Length > 0 Then
                    '        xAccessLevel = dsList.Tables("List").Rows(0)("CSV").ToString
                    '    Else
                    '        xAccessLevel = "0"
                    '    End If
                    'Else
                    '    xAccessLevel = "0"
                    'End If

                    If dsList.Tables("List").Rows.Count > 0 Then
                        If dsList.Tables("List").Rows(0)("CSV").ToString.Trim.Length > 0 Then
                            xAccessLevel = dsList.Tables("List").Rows(0)("CSV").ToString
                        Else
                            xAccessLevel = "-999"
                        End If
                    Else
                        xAccessLevel = "-999"
                    End If
                    'S.SANDEEP [25-JUL-2017] -- END


                    'Sohail (01 Jan 2021) -- Start
                    'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                    strFilterString &= " AND "

                    If i = 0 Then
                        strFilterString &= " (( "
                    End If
                    'Sohail (01 Jan 2021) -- End

                    If CInt(arrID(i)) = enAllocation.DEPARTMENT Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.departmentunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.departmentunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    If CInt(arrID(i)) = enAllocation.CLASS_GROUP Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.classgroupunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.classgroupunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    If CInt(arrID(i)) = enAllocation.CLASSES Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.classunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.classunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    If CInt(arrID(i)) = enAllocation.JOBS Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND RUCat.jobunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " RUCat.jobunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    'S.SANDEEP [20-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}
                    If CInt(arrID(i)) = enAllocation.JOB_GROUP Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND RUCat.jobgroupunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " RUCat.jobgroupunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    If CInt(arrID(i)) = enAllocation.BRANCH Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.stationunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.stationunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    If CInt(arrID(i)) = enAllocation.DEPARTMENT_GROUP Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.deptgroupunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.deptgroupunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    If CInt(arrID(i)) = enAllocation.SECTION_GROUP Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.sectiongroupunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.sectiongroupunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    If CInt(arrID(i)) = enAllocation.SECTION Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.sectionunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.sectionunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    If CInt(arrID(i)) = enAllocation.UNIT_GROUP Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.unitgroupunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.unitgroupunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If

                    If CInt(arrID(i)) = enAllocation.UNIT Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.unitunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.unitunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End

                    End If

                    If CInt(arrID(i)) = enAllocation.TEAM Then
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                        'strFilterString &= " AND Trf_AS.teamunkid IN (" & xAccessLevel & ") "
                        strFilterString &= " Trf_AS.teamunkid IN (" & xAccessLevel & ") "
                        'Sohail (01 Jan 2021) -- End
                    End If
                    'S.SANDEEP [20-JUN-2018] -- END

                Next

                xJoinUACQry &= strFilterString

                'Sohail (01 Jan 2021) -- Start
                'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS. (to allow to get all employees of own department apart from user access departments employees when user has bot ESS and MSS access)
                xJoinUACQry &= " ) "

                If strORQueryForUserAccess.Trim <> "" Then
                    xJoinUACQry &= " OR ( " & strORQueryForUserAccess & " ) "
                End If

                xJoinUACQry &= " ) "
                'Sohail (01 Jan 2021) -- End

                If blnOnlyApprovedEmployees = False Then
                    If blnAddApprovalCondition Then
                        xJoinUACQry &= ") AS UA ON " & xHr_EmployeeTableAlias & ".employeeunkid = UA.TrfEmpId AND " & xHr_EmployeeTableAlias & ".isapproved = 0 "
                    Else
                        xJoinUACQry &= " UNION SELECT " & xHr_EmployeeTableAlias & ".employeeunkid FROM " & xHr_EmployeeTableAlias & " WITH (INDEX(PK_hremployee_master)) WHERE " & xHr_EmployeeTableAlias & ".isapproved = 0) AS UA ON " & xHr_EmployeeTableAlias & ".employeeunkid = UA.TrfEmpId "
                    End If
                Else
                    xJoinUACQry &= ") AS UA ON " & xHr_EmployeeTableAlias & ".employeeunkid = UA.TrfEmpId AND " & xHr_EmployeeTableAlias & ".isapproved = 1 "
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: NewUserAccessString; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Public Sub NewAccessLevelFilterString(ByRef xJoinUACQry As String, _
    '                                      ByRef xUACFilter As String, _
    '                                      Optional ByVal dtAsOnDate As Date = Nothing, _
    '                                      Optional ByVal blnOnlyApprovedEmployees As Boolean = True, _
    '                                      Optional ByVal strDatabaseName As String = "", _
    '                                      Optional ByVal intUserId As Integer = 0, _
    '                                      Optional ByVal intCompId As Integer = 0, _
    '                                      Optional ByVal intYearId As Integer = 0, _
    '                                      Optional ByVal strUserAccessModeSetting As String = "", _
    '                                      Optional ByVal xHr_EmployeeTableAlias As String = "", _
    '                                      Optional ByVal blnAddApprovalCondition As Boolean = True)

    '    'Pinkal (17-Dec-2015) -- Start    'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.[Optional ByVal blnAddApprovalCondition As Boolean = True]

    '    Dim strFilterString As String = String.Empty
    '    Try
    '        If strDatabaseName.Trim = "" Then strDatabaseName = FinancialYear._Object._DatabaseName
    '        If dtAsOnDate = Nothing Then dtAsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
    '        If intUserId <= 0 Then intUserId = User._Object._Userunkid
    '        If intCompId <= 0 Then intCompId = Company._Object._Companyunkid
    '        If intYearId <= 0 Then intYearId = FinancialYear._Object._YearUnkid
    '        If xHr_EmployeeTableAlias.Trim = "" Then xHr_EmployeeTableAlias = "hremployee_master"

    '        Using xDataOpr As New clsDataOperation
    '            Dim dsList As New DataSet
    '            Dim exForce As Exception
    '            Dim StrQ As String = String.Empty
    '            Dim xAccessLevel As String = String.Empty

    '            xDataOpr.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
    '            xDataOpr.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
    '            xDataOpr.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)

    '            If strUserAccessModeSetting.Trim = "" Then strUserAccessModeSetting = ConfigParameter._Object._UserAccessModeSetting
    '            Dim arrID As String() = strUserAccessModeSetting.Split(",")
    '            xJoinUACQry = ""
    '            'xJoinUACQry = "LEFT JOIN " & _
    '            '               " ( " & _
    '            '               "    SELECT " & _
    '            '               "         Trf.TrfEmpId " & _
    '            '               "        ,Trf.stationunkid " & _
    '            '               "        ,Trf.deptgroupunkid " & _
    '            '               "        ,Trf.departmentunkid " & _
    '            '               "        ,Trf.sectiongroupunkid " & _
    '            '               "        ,Trf.sectionunkid " & _
    '            '               "        ,Trf.unitgroupunkid " & _
    '            '               "        ,Trf.unitunkid " & _
    '            '               "        ,Trf.teamunkid " & _
    '            '               "        ,Trf.classgroupunkid " & _
    '            '               "        ,Trf.classunkid " & _
    '            '               "        ,Trf.EfDt " & _
    '            '               "        ,RUCat.jobgroupunkid " & _
    '            '               "        ,RUCat.jobunkid " & _
    '            '               "    FROM " & _
    '            '               "    ( " & _
    '            '               "        SELECT " & _
    '            '               "             ETT.employeeunkid AS TrfEmpId  " & _
    '            '                          ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
    '            '                          ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
    '            '                          ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
    '            '                          ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
    '            '                          ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
    '            '                          ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
    '            '                          ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
    '            '                          ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
    '            '                          ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
    '            '                          ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
    '            '                          ", CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
    '            '                          ", ETT.employeeunkid " & _
    '            '                          ", ROW_NUMBER() OVER ( PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC ) AS Rno " & _
    '            '                  "FROM      " & strDatabaseName & "..hremployee_transfer_tran AS ETT " & _
    '            '               "        WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtAsOnDate).ToString & "' " & _
    '            '                ") AS Trf " & _
    '            '               "    JOIN " & _
    '            '               "    ( " & _
    '            '               "        SELECT " & _
    '            '               "             ECT.employeeunkid AS CatEmpId  " & _
    '            '                              ", ECT.jobgroupunkid " & _
    '            '                              ", ECT.jobunkid " & _
    '            '                              ", CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
    '            '                              ", ECT.employeeunkid " & _
    '            '                              ", ROW_NUMBER() OVER ( PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC ) AS Rno " & _
    '            '                       "FROM     " & strDatabaseName & "..hremployee_categorization_tran AS ECT " & _
    '            '               "        WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtAsOnDate).ToString & "' " & _
    '            '                     ") AS RUCat ON trf.employeeunkid = RUCat.employeeunkid " & _
    '            '               "    WHERE Trf.Rno = 1 AND RUCat.Rno = 1 " & _
    '            '               ") AS UA ON UA .TrfEmpId = " & xHr_EmployeeTableAlias & ".employeeunkid "


    '            xUACFilter &= " ( " & xHr_EmployeeTableAlias & ".employeeunkid IN ( " & _
    '                    "SELECT  Trf_AS.TrfEmpId " & _
    '                    "FROM    ( SELECT    ETT.employeeunkid AS TrfEmpId  " & _
    '                                      ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
    '                                      ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
    '                                      ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
    '                                      ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
    '                                      ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
    '                                      ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
    '                                      ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
    '                                      ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
    '                                      ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
    '                                      ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
    '                                      ", CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
    '                                      ", ETT.employeeunkid " & _
    '                                      ", ROW_NUMBER() OVER ( PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC ) AS Rno " & _
    '                              "FROM      " & strDatabaseName & "..hremployee_transfer_tran AS ETT " & _
    '                              "WHERE     isvoid = 0 " & _
    '                                        "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtAsOnDate).ToString & "' " & _
    '                            ") AS Trf_AS " & _
    '                            "JOIN ( SELECT   ECT.employeeunkid AS CatEmpId  " & _
    '                                          ", ECT.jobgroupunkid " & _
    '                                          ", ECT.jobunkid " & _
    '                                          ", CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
    '                                          ", ECT.employeeunkid " & _
    '                                          ", ROW_NUMBER() OVER ( PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC ) AS Rno " & _
    '                                   "FROM     " & strDatabaseName & "..hremployee_categorization_tran AS ECT " & _
    '                                   "WHERE    isvoid = 0 " & _
    '                                            "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtAsOnDate).ToString & "' " & _
    '                                 ") AS RUCat ON Trf_AS.employeeunkid = RUCat.employeeunkid " & _
    '                    "WHERE   Trf_AS.Rno = 1 " & _
    '                            "AND RUCat.Rno = 1 "

    '            For i = 0 To arrID.Length - 1

    '                StrQ = "SELECT " & _
    '                       "    STUFF((SELECT ',' + CAST(allocationunkid AS NVARCHAR(50)) " & _
    '                       "FROM hrmsConfiguration..cfuseraccess_privilege_tran " & _
    '                       "JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid " & _
    '                       "WHERE userunkid = @UserId AND companyunkid = @CompId AND yearunkid = @YearId AND referenceunkid = " & arrID(i).ToString & " " & _
    '                       "ORDER BY hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid FOR XML PATH('')),1,1,'') AS CSV "

    '                dsList = xDataOpr.ExecQuery(StrQ, "List")

    '                If xDataOpr.ErrorMessage <> "" Then
    '                    exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                If dsList.Tables("List").Rows.Count > 0 Then
    '                    If dsList.Tables("List").Rows(0)("CSV").ToString.Trim.Length > 0 Then
    '                        xAccessLevel = dsList.Tables("List").Rows(0)("CSV").ToString
    '                    Else
    '                        xAccessLevel = "0"
    '                    End If
    '                Else
    '                    xAccessLevel = "0"
    '                End If

    '                If CInt(arrID(i)) = enAllocation.DEPARTMENT Then
    '                    strFilterString &= " AND Trf_AS.departmentunkid IN (" & xAccessLevel & ") "
    '                End If

    '                If CInt(arrID(i)) = enAllocation.CLASS_GROUP Then
    '                    strFilterString &= " AND Trf_AS.classgroupunkid IN (" & xAccessLevel & ") "
    '                End If

    '                If CInt(arrID(i)) = enAllocation.CLASSES Then
    '                    strFilterString &= " AND Trf_AS.classunkid IN (" & xAccessLevel & ") "
    '                End If

    '                If CInt(arrID(i)) = enAllocation.JOBS Then
    '                    strFilterString &= " AND RUCat.jobunkid IN (" & xAccessLevel & ") "
    '                End If

    '            Next

    '            xUACFilter &= strFilterString

    '            'Shani(24-Aug-2015) -- Start [SANDEEP DONE CHANGES AS UNAPPROVERD EMPLOYEE WAS NOT COMING]
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'ISSUE : WHILE IMPORTING EMPLOYEE, TRANSACTIONS WILL NOT INSERT IN TRANSFER,DATES,COSTCENTER 
    '            '        TABLE, DUE TO IT'S NOT APPROVED BY DEFAULT -- SO IT WAS NOT COMING IN EMPLOYEE LIST IN PENDING STATUS.
    '            '        CONDITION MODIFIED AS PENDING EMPLOYEE ONLY.


    '            'Pinkal (17-Dec-2015) -- Start
    '            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
    'If blnOnlyApprovedEmployees = False Then
    '                If blnAddApprovalCondition Then
    '                    xUACFilter &= ") AND " & xHr_EmployeeTableAlias & ".isapproved = 0) "
    '                Else
    '                    'S.SANDEEP [25 Jan 2016] -- START
    '                    'xUACFilter &= ")) "

    '                    'S.SANDEEP [30 SEP 2016] -- START
    '                    'ISSUE : 'OR' CONDITION TAKING LONG TIME TO EXECUTE
    '                    'xUACFilter &= ") OR " & xHr_EmployeeTableAlias & ".isapproved = 0) "
    '                    xUACFilter &= " UNION SELECT " & xHr_EmployeeTableAlias & ".employeeunkid FROM " & xHr_EmployeeTableAlias & " WHERE " & xHr_EmployeeTableAlias & ".isapproved = 0)) "
    '                    'S.SANDEEP [30 SEP 2016] -- END

    '                    'S.SANDEEP [25 Jan 2016] -- END
    '                End If
    'Else
    '                xUACFilter &= ") AND " & xHr_EmployeeTableAlias & ".isapproved = 1) "
    'End If
    '            'Pinkal (17-Dec-2015) -- End

    '            'If blnOnlyApprovedEmployees = False Then
    '            '    '<TODO NEED TO OPTIMIZE THE CONDTION NOT WORKING CORRECTLY>
    '            '    'xUACFilter &= " OR " & xHr_EmployeeTableAlias & ".isapproved = 0 )) "
    '            '    xUACFilter &= " OR " & xHr_EmployeeTableAlias & ".isapproved = 0 )) "
    '            'Else
    '            '    xUACFilter &= " AND " & xHr_EmployeeTableAlias & ".isapproved = 1 )) "
    '            'End If


    '            'Shani(24-Aug-2015) -- End


    '        End Using
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: NewUserAccessString; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'S.SANDEEP [15 NOV 2016] -- END


    'Sohail (08 May 2015) -- End

    'S.SANDEEP [12 MAY 2015] -- START
    Public Sub GetDatesFilterString(ByRef xJoinDateQry As String, _
                                    ByRef xDateFilter As String, _
                                    Optional ByVal xStartDate As Date = Nothing, _
                                    Optional ByVal xEndDate As Date = Nothing, _
                                    Optional ByVal xIncludeReinstementDate As Boolean = True, _
                                    Optional ByVal xIncludeExcludeTermEmp_PayProcess As Boolean = False, _
                                    Optional ByVal xStrDatabaseName As String = "", _
                                    Optional ByVal xHrEmployeeTableAlias As String = "")
        Try
            If xStrDatabaseName.Trim = "" Then xStrDatabaseName = FinancialYear._Object._DatabaseName
            If xStartDate = Nothing Then xStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            If xEndDate = Nothing Then xEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            If xHrEmployeeTableAlias.Trim = "" Then xHrEmployeeTableAlias = "hremployee_master"
            Dim xNextDate = DateAdd(DateInterval.Day, 1, xEndDate)

            xJoinDateQry = " LEFT JOIN " & _
                            "( " & _
                                 "SELECT " & _
                                      " T.EmpId " & _
                                      ",T.EOC " & _
                                      ",T.LEAVING " & _
                                      ",T.isexclude_payroll AS IsExPayroll " & _
                                 "FROM " & _
                                 "( " & _
                                      "SELECT " & _
                                           "employeeunkid AS EmpId " & _
                                           ",date1 AS EOC " & _
                                           ",date2 AS LEAVING " & _
                                           ",effectivedate " & _
                                           ",isexclude_payroll " & _
                                           ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                      "FROM " & xStrDatabaseName & "..hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_TERMINATION & "') AND isvoid = 0 " & _
                                      " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEndDate).ToString & "' " & _
                                 ") AS T WHERE T.xNo = 1 " & _
                            ") AS TRM ON TRM.EmpId = " & xHrEmployeeTableAlias & ".employeeunkid " & _
                            "LEFT JOIN " & _
                            "( " & _
                                 "SELECT " & _
                                      " R.EmpId " & _
                                      ",R.RETIRE " & _
                                      ",R.isexclude_payroll AS IsExPayroll " & _
                                 "FROM " & _
                                 "( " & _
                                      "SELECT " & _
                                           " employeeunkid AS EmpId " & _
                                           ",date1 AS RETIRE " & _
                                           ",effectivedate " & _
                                           ",isexclude_payroll " & _
                                           ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                      "FROM " & xStrDatabaseName & "..hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') AND isvoid = 0 " & _
                                      " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEndDate).ToString & "' " & _
                                 ") AS R WHERE R.xNo = 1 " & _
                            ") AS RET ON RET.EmpId = " & xHrEmployeeTableAlias & ".employeeunkid " & _
                            "LEFT JOIN " & _
                            "( " & _
                                 "SELECT " & _
                                       "RH.EmpId " & _
                                      ",RH.REHIRE " & _
                                 "FROM " & _
                                 "( " & _
                                      "SELECT " & _
                                            "employeeunkid AS EmpId " & _
                                           ",reinstatment_date AS REHIRE " & _
                                           ",effectivedate " & _
                                           ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                      "FROM " & xStrDatabaseName & "..hremployee_rehire_tran WHERE isvoid = 0 " & _
                                      " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEndDate).ToString & "' " & _
                                 ") AS RH WHERE RH.xNo = 1 " & _
                            ") AS HIRE ON HIRE.EmpId = " & xHrEmployeeTableAlias & ".employeeunkid "
            'Sohail (10 Feb 2021) - [R.isexclude_payroll]

            'xDateFilter = " AND CONVERT(CHAR(8)," & xHrEmployeeTableAlias & ".appointeddate,112) <= '" & eZeeDate.convertDate(xEndDate).ToString & "' " & _
            '              " AND ISNULL(CONVERT(CHAR(8),TRM.LEAVING,112),'" & eZeeDate.convertDate(xStartDate).ToString & "') >= '" & eZeeDate.convertDate(xStartDate).ToString & "' " & _
            '              " AND ISNULL(CONVERT(CHAR(8),RET.RETIRE,112),'" & eZeeDate.convertDate(xStartDate).ToString & "') >= '" & eZeeDate.convertDate(xStartDate).ToString & "' " & _
            '              " AND ISNULL(CONVERT(CHAR(8),TRM.EOC,112), '" & eZeeDate.convertDate(xStartDate).ToString & "') >= '" & eZeeDate.convertDate(xStartDate).ToString & "' "

            xDateFilter = " AND CONVERT(CHAR(8)," & xHrEmployeeTableAlias & ".appointeddate,112) <= '" & eZeeDate.convertDate(xEndDate).ToString & "' " & _
                          " AND (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN '" & eZeeDate.convertDate(xStartDate).ToString & "' ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) >= '" & eZeeDate.convertDate(xStartDate).ToString & "' " & _
                          " AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN '" & eZeeDate.convertDate(xStartDate).ToString & "' ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) >= '" & eZeeDate.convertDate(xStartDate).ToString & "' " & _
                          " AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN '" & eZeeDate.convertDate(xStartDate).ToString & "' ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) >= '" & eZeeDate.convertDate(xStartDate).ToString & "' "

            If xIncludeReinstementDate Then
                'xDateFilter &= " AND ISNULL(CONVERT(CHAR(8),HIRE.REHIRE,112),'" & eZeeDate.convertDate(xEndDate).ToString & "') <= '" & eZeeDate.convertDate(xEndDate).ToString & "' "
                xDateFilter &= " AND (CASE WHEN CONVERT(CHAR(8), HIRE.REHIRE, 112) IS NULL THEN '" & eZeeDate.convertDate(xEndDate).ToString & "' ELSE CONVERT(CHAR(8), HIRE.REHIRE, 112) END)  <= '" & eZeeDate.convertDate(xEndDate).ToString & "' "
            End If

            If xIncludeExcludeTermEmp_PayProcess = True Then
                'xDateFilter &= " AND ( ( ISNULL(TRM.IsExPayroll, 0) = 0 ) OR ( ISNULL(CONVERT(CHAR(8),TRM.LEAVING,112),'" & eZeeDate.convertDate(xNextDate).ToString & "') > '" & eZeeDate.convertDate(xEndDate).ToString & "' " & _
                '               " AND ISNULL(CONVERT(CHAR(8),RET.RETIRE,112),'" & eZeeDate.convertDate(xNextDate).ToString & "') > '" & eZeeDate.convertDate(xEndDate).ToString & "' " & _
                '               " AND ISNULL(CONVERT(CHAR(8),TRM.EOC,112), '" & eZeeDate.convertDate(xNextDate).ToString & "') > '" & eZeeDate.convertDate(xEndDate).ToString & "' AND TRM.IsExPayroll = 1 ) ) "
                'Sohail (10 Feb 2021) -- Start
                'Blantyre Water Board - Enhancement : OLD-296 : Total Number Employed Field should pick count for processed employees only.  Not ALL active employees.
                'xDateFilter &= " AND ( ( ISNULL(TRM.IsExPayroll, 0) = 0 ) OR ( (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN '" & eZeeDate.convertDate(xNextDate).ToString & "' ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) > '" & eZeeDate.convertDate(xEndDate).ToString & "' " & _
                '               " AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN '" & eZeeDate.convertDate(xNextDate).ToString & "' ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) > '" & eZeeDate.convertDate(xEndDate).ToString & "' " & _
                '               " AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN '" & eZeeDate.convertDate(xNextDate).ToString & "' ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) > '" & eZeeDate.convertDate(xEndDate).ToString & "' AND TRM.IsExPayroll = 1 ) ) "
                xDateFilter &= " AND ( ( ISNULL(TRM.IsExPayroll, 0) = 0 ) OR NOT ( (ISNULL( TRM.EOC, '" & eZeeDate.convertDate(xNextDate).ToString & "') BETWEEN '" & eZeeDate.convertDate(xStartDate).ToString & "' AND '" & eZeeDate.convertDate(xEndDate).ToString & "' OR ISNULL(TRM.LEAVING, '" & eZeeDate.convertDate(xNextDate).ToString & "') BETWEEN '" & eZeeDate.convertDate(xStartDate).ToString & "' AND '" & eZeeDate.convertDate(xEndDate).ToString & "' ) AND TRM.IsExPayroll = 1 )) " & _
                               " AND ( ( ISNULL(RET.IsExPayroll, 0) = 0 ) OR NOT ( (ISNULL( RET.RETIRE, '" & eZeeDate.convertDate(xNextDate).ToString & "') BETWEEN '" & eZeeDate.convertDate(xStartDate).ToString & "' AND '" & eZeeDate.convertDate(xEndDate).ToString & "' ) AND RET.IsExPayroll = 1 )) "
                'Sohail (10 Feb 2021) -- End
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDatesFilterString; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub GetAdvanceFilterQry(ByRef xAdvanceFQry As String, _
                                   Optional ByVal xDateAsOn As Date = Nothing, _
                                   Optional ByVal xDataBaseName As String = "", _
                                   Optional ByVal xEmployeeTableAlias As String = "")
        Try
            If xDateAsOn = Nothing Then xDateAsOn = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            If xDataBaseName.Trim = "" Then xDataBaseName = FinancialYear._Object._DatabaseName
            If xEmployeeTableAlias.Trim = "" Then xEmployeeTableAlias = "hremployee_master"

            xAdvanceFQry = "LEFT JOIN " & _
                           "( " & _
                           "    SELECT " & _
                           "         Trf_AS.employeeunkid " & _
                           "        ,stationunkid " & _
                           "        ,deptgroupunkid " & _
                           "        ,departmentunkid " & _
                           "        ,sectiongroupunkid " & _
                           "        ,sectionunkid " & _
                           "        ,unitgroupunkid " & _
                           "        ,unitunkid " & _
                           "        ,teamunkid " & _
                           "        ,classgroupunkid " & _
                           "        ,classunkid " & _
                           "        ,RUCat.jobgroupunkid " & _
                           "        ,RUCat.jobunkid " & _
                           "        ,CCT.costcenterunkid " & _
                           "        ,GRD.gradegroupunkid " & _
                           "        ,GRD.gradeunkid " & _
                           "        ,GRD.gradelevelunkid " & _
                           "    FROM " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             ETT.employeeunkid AS TrfEmpId " & _
                           "            ,ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                           "            ,ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                           "            ,ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                           "            ,ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                           "            ,ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                           "            ,ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                           "            ,ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                           "            ,ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                           "            ,ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                           "            ,ISNULL(ETT.classunkid, 0) AS classunkid " & _
                           "            ,CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                           "            ,ETT.employeeunkid " & _
                           "            ,ROW_NUMBER() OVER ( PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC ) AS Rno " & _
                           "        FROM " & xDataBaseName & "..hremployee_transfer_tran AS ETT " & _
                           "        WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xDateAsOn).ToString & "' " & _
                           "    ) AS Trf_AS " & _
                           "    JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             ECT.employeeunkid AS CatEmpId " & _
                           "            ,ECT.jobgroupunkid " & _
                           "            ,ECT.jobunkid " & _
                           "            ,CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                           "            ,ECT.employeeunkid " & _
                           "            ,ROW_NUMBER() OVER ( PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC ) AS Rno " & _
                           "        FROM " & xDataBaseName & "..hremployee_categorization_tran AS ECT " & _
                           "        WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xDateAsOn).ToString & "' " & _
                           "    ) AS RUCat ON Trf_AS.employeeunkid = RUCat.employeeunkid " & _
                           "    JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             CCT.employeeunkid AS CCTEmpId " & _
                           "            ,CCT.cctranheadvalueid AS costcenterunkid " & _
                           "            ,CONVERT(CHAR(8), CCT.effectivedate, 112) AS CTEfDt " & _
                           "            ,ROW_NUMBER() OVER ( PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC ) AS Rno " & _
                           "        FROM " & xDataBaseName & "..hremployee_cctranhead_tran AS CCT " & _
                           "        WHERE  CCT.isvoid = 0 AND CCT.istransactionhead = 0 AND CONVERT(CHAR(8), CCT.effectivedate, 112) <= '" & eZeeDate.convertDate(xDateAsOn).ToString & "' " & _
                           "    ) AS CCT ON Trf_AS.employeeunkid = CCT.CCTEmpId " & _
                           "    JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             gradegroupunkid " & _
                           "            ,gradeunkid " & _
                           "            ,gradelevelunkid " & _
                           "            ,employeeunkid " & _
                           "            ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                           "        FROM " & xDataBaseName & "..prsalaryincrement_tran " & _
                           "        WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xDateAsOn).ToString & "' " & _
                           "    ) AS GRD ON Trf_AS.employeeunkid = GRD.employeeunkid " & _
                           " WHERE   Trf_AS.Rno = 1  AND RUCat.Rno = 1 AND CCT.Rno = 1 AND GRD.Rno = 1 " & _
                           ") AS ADF ON ADF.employeeunkid = " & xEmployeeTableAlias & ".employeeunkid "

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AdvanceFilterQry; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [12 MAY 2015] -- END


    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Public Function IsSelfServiceExist() As Boolean
        Dim bln As Boolean = True
        Try

            Dim strIISPath As String = ConfigParameter._Object._ArutiSelfServiceURL

            Dim data As String = ""
            Dim postBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
            Dim postReq As HttpWebRequest = DirectCast(WebRequest.Create(strIISPath & IIf(strIISPath.Substring(strIISPath.Length - 1, 1) <> "/", "/index.aspx", "index.aspx")), HttpWebRequest)
            postReq.Method = "POST"
            postReq.KeepAlive = False
            postReq.ContentType = "application/x-www-form-urlencoded"
            postReq.Referer = strIISPath
            postReq.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0"
            postReq.ContentLength = postBytes.Length
            Dim postresponse As HttpWebResponse
            'S.SANDEEP |31-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Employee Bio Data UAT]
            System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
            'S.SANDEEP |31-MAY-2019| -- END
            'Sohail (02 Jul 2019) -- Start
            'NMB Issue # - 76.1 - Erro : "Configuration Path does not exist" on adding attachment from Desktop. (Unable to acccess self service URL from desktop).
            'System.Net.ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
            'Sohail (07 Aug 2019) -- Start
            'ZRA issue # - 76.1 - The requested security protocol is not supported.
            'System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Try
                System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Catch ex As Exception

            End Try
            'Sohail (07 Aug 2019) -- End
            'Sohail (02 Jul 2019) -- End
            postresponse = DirectCast(postReq.GetResponse(), HttpWebResponse)

            If postresponse.StatusCode = HttpStatusCode.OK Then
                bln = True
            Else
                bln = False
            End If
        Catch ex As Exception
            bln = False
        End Try
        Return bln
    End Function
    'SHANI (16 JUL 2015) -- End 

    'Sohail (13 Jul 2019) -- Start
    'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
    Public Function AcceptAllCertifications(ByVal sender As Object, ByVal certification As System.Security.Cryptography.X509Certificates.X509Certificate, ByVal chain As System.Security.Cryptography.X509Certificates.X509Chain, ByVal sslPolicyErrors As System.Net.Security.SslPolicyErrors) As Boolean
        Return True
    End Function
    'Sohail (13 Jul 2019) -- End

    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.


    Public Function GetXML(ByVal xdsList As DataSet) As String
        Dim strQ As String = ""
        Try
            If xdsList IsNot Nothing Then
                strQ = xdsList.GetXml().Replace("'", "''")
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return strQ
    End Function

    'Pinkal (30-Jun-2016) -- End

    'Sohail (08 Jul 2016) -- Start
    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
    ''' <summary>
    ''' Purpose : To Get Highest Enum Value from Given Enum. (eg. : GetEnumHighestValue(Of enHeadTypeId)())
    ''' Developer: Sohail
    ''' </summary>
    ''' <typeparam name="TEnum">eg. : GetEnumHighestValue(Of enHeadTypeId)()</typeparam>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetEnumHighestValue(Of TEnum)() As TEnum
        Return [Enum].GetValues(GetType(TEnum)).Cast(Of TEnum)().Max()
    End Function
    'Sohail (08 Jul 2016) -- End

    'Sohail (05 Oct 2016) -- Start
    'Enhancement - 63.1 - Common Function to check if Aruti is running on other machines.
    ''' <summary>
    ''' Common Function to check if Aruti is running on other machines.
    ''' </summary>
    ''' <param name="srtDatabaseName">Name of the database to exclude from</param>
    ''' <param name="stroutMachineNames">Rerurn: Comma separated Machine Names. Pass blank string to ignore.</param>
    ''' <param name="stroutSPIDs">Rerurn: Comma separated SPIDs with KILL statements e.g. KILL 52, KILL 53. Pass blank string to ignore.</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function IsArutiRunningOnOtherMachines(ByVal srtDatabaseName As String _
                                                  , ByRef stroutMachineNames As String _
                                                  , ByRef stroutSPIDs As String _
                                                  ) As Boolean
        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception

        Try
            stroutMachineNames = ""
            stroutSPIDs = ""

            strQ = "SELECT   hostname " & _
                          ", spid " & _
                    "FROM    sys.sysprocesses " & _
                    "WHERE   dbid > 0 " & _
                            "AND DB_NAME(dbid) = '" & srtDatabaseName & "' " & _
                            "AND hostname <> HOST_NAME() "

            dsList = objDataOperation.ExecQuery(strQ, "ConnectedHost")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("ConnectedHost").Rows.Count > 0 Then
                stroutMachineNames = String.Join(",", (From p In dsList.Tables("ConnectedHost").AsEnumerable Select (p.Item("hostname").ToString.Trim)).Distinct().ToArray)
                'stroutSPIDs = String.Join(",", (From p In dsList.Tables("ConnectedHost").AsEnumerable Select ("KILL " & p.Item("spid").ToString)).Distinct().ToArray)
                stroutSPIDs = String.Join(" ", (From p In dsList.Tables("ConnectedHost").AsEnumerable Select (" KILL " & p.Item("spid").ToString)).Distinct().ToArray)
            End If

            Return dsList.Tables("ConnectedHost").Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & ":IsArutiRunningOnOtherMachines" & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' KILL spid
    ''' </summary>
    ''' <param name="strSPIDs">pass SPIDs</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Kill_SPIDs(ByVal strSPIDs As String _
                               ) As Boolean

        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception

        Try
            If strSPIDs.Trim = "" Then Return True

            dsList = objDataOperation.ExecQuery(strSPIDs, "ConnectedHost")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & ":Kill_SPIDs" & mstrModuleName)
            Return False
        End Try
    End Function
    'Sohail (05 Oct 2016) -- End



    'Pinkal (03-Apr-2017) -- Start
    'Enhancement - Working On Active directory Changes for PACRA.

    Public Function IsAuthenticated(ByVal mstrIPAddress As String, ByVal mstrDomain As String, ByVal username As String, ByVal pwd As String) As Boolean
        Try

            If mstrDomain.Trim.Length <= 0 Then Return False

            Dim ar() As String = Nothing
            If mstrDomain.Trim.Length > 0 AndAlso mstrDomain.Trim.Contains(".") Then
                ar = mstrDomain.Trim.Split(CChar("."))
                mstrDomain = ""
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        mstrDomain &= ",DC=" & ar(i)
                    Next
                End If
            End If

            If mstrDomain.Trim.Length > 0 Then
                mstrDomain = mstrDomain.Trim.Substring(1)
            End If

            Dim entry As DirectoryEntry = New DirectoryEntry("LDAP://" & mstrIPAddress.Trim & "/" & mstrDomain.Trim, username.Trim, pwd.Trim)

            'Bind to the native AdsObject to force authentication.
            Dim obj As Object = entry.NativeObject
            Dim search As DirectorySearcher = New DirectorySearcher(entry)
            search.Filter = "(SAMAccountName=" & username & ")"
            search.PropertiesToLoad.Add("cn")
            Dim result As SearchResult = search.FindOne()

            If (result Is Nothing) Then
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : IsAuthenticated ; Module Name : " & mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (03-Apr-2017) -- End

    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    Public Function OpenXML_Import(ByVal strFilePath As String) As DataSet
        Dim ds As New DataSet()
        Try
            Using spreadsheetDocument As DocumentFormat.OpenXml.Packaging.SpreadsheetDocument = DocumentFormat.OpenXml.Packaging.SpreadsheetDocument.Open(strFilePath, False)
                Dim workbookPart As DocumentFormat.OpenXml.Packaging.WorkbookPart = spreadsheetDocument.WorkbookPart
                Dim sheets As IEnumerable(Of DocumentFormat.OpenXml.Spreadsheet.Sheet) = spreadsheetDocument.WorkbookPart.Workbook.GetFirstChild(Of DocumentFormat.OpenXml.Spreadsheet.Sheets)().Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)()
                For Each sheet As DocumentFormat.OpenXml.Spreadsheet.Sheet In sheets
                    Dim dt As New DataTable()
                    Dim worksheetPart As DocumentFormat.OpenXml.Packaging.WorksheetPart = CType(spreadsheetDocument.WorkbookPart.GetPartById(sheet.Id.Value), DocumentFormat.OpenXml.Packaging.WorksheetPart)
                    Dim workSheet As DocumentFormat.OpenXml.Spreadsheet.Worksheet = worksheetPart.Worksheet
                    Dim sheetData As DocumentFormat.OpenXml.Spreadsheet.SheetData = workSheet.GetFirstChild(Of DocumentFormat.OpenXml.Spreadsheet.SheetData)()
                    Dim rows As IEnumerable(Of DocumentFormat.OpenXml.Spreadsheet.Row) = sheetData.Descendants(Of DocumentFormat.OpenXml.Spreadsheet.Row)()
                    If rows.Count <= 0 Then Continue For
                    dt.TableName = sheet.Id.Value
                    For Each cell As DocumentFormat.OpenXml.Spreadsheet.Cell In rows.ElementAt(0)
                        'S.SANDEEP [08-Feb-2018] -- START
                        'dt.Columns.Add(GetCellValue(spreadsheetDocument, cell))
                        If cell.CellValue IsNot Nothing Then
                            dt.Columns.Add(GetCellValue(spreadsheetDocument, cell))
                        End If
                        'S.SANDEEP [08-Feb-2018] -- END
                    Next cell
                    Dim blnHasChildren As Boolean = False
                    blnHasChildren = sheetData.HasChildren()
                    If blnHasChildren Then
                        Dim intRowIdx As Integer = 0
                        For Each row As DocumentFormat.OpenXml.Spreadsheet.Row In rows 'this will also include your header row...
                            If intRowIdx > 0 Then
                                Dim tempRow As DataRow = dt.NewRow()
                                Dim cells As IEnumerable(Of DocumentFormat.OpenXml.Spreadsheet.Cell) = SpreedsheetHelper.GetRowCells(row)
                                Dim i As Integer = 0
                                For Each cell As DocumentFormat.OpenXml.Spreadsheet.Cell In cells
                                    Dim currentcellvalue As String = String.Empty
                                    If cell.DataType IsNot Nothing Then
                                        If cell.DataType.Value = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString Then
                                            Dim id As Integer = -1
                                            If Int32.TryParse(cell.InnerText, id) Then
                                                Dim item As DocumentFormat.OpenXml.Spreadsheet.SharedStringItem = GetSharedStringItemById(workbookPart, id)
                                                If item.Text IsNot Nothing Then
                                                    'code to take the string value  
                                                    currentcellvalue = item.Text.Text
                                                ElseIf item.InnerText IsNot Nothing Then
                                                    currentcellvalue = item.InnerText
                                                ElseIf item.InnerXml IsNot Nothing Then
                                                    currentcellvalue = item.InnerXml
                                                End If
                                                tempRow(i) = currentcellvalue
                                            Else
                                                tempRow(i) = cell.InnerText
                                            End If
                                        Else
                                            tempRow(i) = cell.InnerText
                                        End If
                                    Else
                                        'S.SANDEEP [07-Feb-2018] -- START
                                        'ISSUE/ENHANCEMENT : Date Column Returning Double Value
                                        'tempRow(i) = cell.InnerText

                                        'Dim styleIndex As Integer = cell.StyleIndex.Value
                                        'Dim cellFormat As DocumentFormat.OpenXml.Spreadsheet.CellFormat = workbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex)
                                        'Dim formatId As UInt32 = cellFormat.NumberFormatId.Value
                                        'If ((formatId >= 14 AndAlso formatId <= 22) Or (formatId >= 165 AndAlso formatId <= 180) Or formatId = 278 Or formatId = 185 Or formatId = 196 Or formatId = 217 Or formatId = 326) Then
                                        '    Dim oaDate As Double = 0
                                        '    Double.TryParse(cell.InnerText, oaDate)
                                        '    tempRow(i) = DateTime.FromOADate(oaDate).ToShortDateString()
                                        'Else
                                        '    tempRow(i) = cell.InnerText
                                        'End If

                                        'S.SANDEEP [08-Feb-2018] -- START

                                        'S.SANDEEP |25-FEB-2019| -- START
                                        Dim oValue As Double = 0
                                        'S.SANDEEP |25-FEB-2019| -- END
                                        If cell.StyleIndex IsNot Nothing Then
                                            Dim styleIndex As Integer = cell.StyleIndex.Value
                                            Dim cellFormat As DocumentFormat.OpenXml.Spreadsheet.CellFormat = workbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex)
                                            Dim formatId As UInt32 = cellFormat.NumberFormatId.Value
                                            

                                            If ((formatId >= 14 AndAlso formatId <= 22) Or (formatId >= 164 AndAlso formatId <= 182) Or formatId = 278 Or formatId = 185 Or formatId = 196 Or formatId = 217 Or formatId = 326) Then
                                                Dim oaDate As Double = 0
                                                Double.TryParse(cell.InnerText, oaDate)
                                                'S.SANDEEP [10-JUL-2018] -- START
                                                'ISSUE/ENHANCEMENT : {Issue Excel Import}
                                                'tempRow(i) = DateTime.FromOADate(oaDate).ToShortDateString()
                                                If oaDate > 0 Then
                                                    Select Case formatId
                                                        'Pinkal (18-Mar-2021) -- Start
                                                        'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                                                        'Case 181, 168

                                                        'Pinkal (18-May-2021) -- Start
                                                        'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                                                        'Case 181, 168, 176, 14, 22
                                                        Case 181, 168, 176, 14, 22, 178, 167
                                                            'Pinkal (18-May-2021) -- End

                                                            'Pinkal (18-Mar-2021) -- End
                                                            tempRow(i) = DateTime.FromOADate(oaDate)
                                                        Case Else
                                                    tempRow(i) = DateTime.FromOADate(oaDate).ToShortDateString()
                                                    End Select
                                                Else
                                                    'S.SANDEEP |25-FEB-2019| -- START
                                                    'tempRow(i) = cell.InnerText
                                                    Double.TryParse(cell.InnerText, oValue)
                                                    If oValue > 0 Then
                                                        tempRow(i) = oValue
                                                    Else
                                                    tempRow(i) = cell.InnerText
                                                End If
                                                    'S.SANDEEP |25-FEB-2019| -- END
                                                End If
                                                'S.SANDEEP [10-JUL-2018] -- END
                                            Else
                                                'S.SANDEEP |25-FEB-2019| -- START
                                                'tempRow(i) = cell.InnerText
                                                Double.TryParse(cell.InnerText, oValue)
                                                If oValue > 0 Then
                                                    tempRow(i) = oValue
                                                Else
                                                tempRow(i) = cell.InnerText
                                            End If
                                                'S.SANDEEP |25-FEB-2019| -- END
                                            End If
                                        Else
                                            'S.SANDEEP |25-FEB-2019| -- START
                                            'tempRow(i) = cell.InnerText
                                            Double.TryParse(cell.InnerText, oValue)
                                            If oValue > 0 Then
                                                tempRow(i) = oValue
                                        Else
                                            tempRow(i) = cell.InnerText
                                        End If
                                            'S.SANDEEP |25-FEB-2019| -- END
                                        End If
                                        'S.SANDEEP [08-Feb-2018] -- END


                                        'S.SANDEEP [07-Feb-2018] -- END
                                    End If
                                    i += 1
                                Next cell
                                'For i As Integer = 0 To (row.Descendants(Of DocumentFormat.OpenXml.Spreadsheet.Cell)().Count()) - 1
                                '    Dim cell As DocumentFormat.OpenXml.Spreadsheet.Cell = row.Descendants(Of DocumentFormat.OpenXml.Spreadsheet.Cell)().ElementAt(i)
                                '    Dim CellValue As DocumentFormat.OpenXml.Spreadsheet.CellValue = cell.CellValue
                                '    If CellValue IsNot Nothing Then
                                '        tempRow(i) = GetCellValue(spreadsheetDocument, cell)
                                '    Else
                                '        tempRow(i) = ""
                                '    End If
                                'Next i
                                dt.Rows.Add(tempRow)
                            End If
                            intRowIdx += 1
                        Next row
                    End If
                    ds.Tables.Add(dt.Copy())
                Next sheet
            End Using
            Return ds
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function OpenXML_Export(ByVal strFilePath As String, ByVal ds As DataSet) As Integer
        Dim intReturn As Integer = 0
        Try
            Using workbook = DocumentFormat.OpenXml.Packaging.SpreadsheetDocument.Create(strFilePath, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook)
                Dim workbookPart = workbook.AddWorkbookPart()
                workbook.WorkbookPart.Workbook = New DocumentFormat.OpenXml.Spreadsheet.Workbook()
                workbook.WorkbookPart.Workbook.Sheets = New DocumentFormat.OpenXml.Spreadsheet.Sheets()
                For Each table As System.Data.DataTable In ds.Tables
                    Dim sheetPart = workbook.WorkbookPart.AddNewPart(Of DocumentFormat.OpenXml.Packaging.WorksheetPart)()
                    Dim sheetData = New DocumentFormat.OpenXml.Spreadsheet.SheetData()
                    sheetPart.Worksheet = New DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData)

                    Dim sheets As DocumentFormat.OpenXml.Spreadsheet.Sheets = workbook.WorkbookPart.Workbook.GetFirstChild(Of DocumentFormat.OpenXml.Spreadsheet.Sheets)()
                    Dim relationshipId As String = workbook.WorkbookPart.GetIdOfPart(sheetPart)
                    Dim sheetId As UInteger = 1
                    If sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Count() > 0 Then
                        sheetId = sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Select(Function(s) s.SheetId.Value).Max() + 1
                    End If
                    Dim sheet As New DocumentFormat.OpenXml.Spreadsheet.Sheet() With {.Id = relationshipId, .SheetId = sheetId, .Name = table.TableName}
                    sheets.Append(sheet)

                    Dim headerRow As New DocumentFormat.OpenXml.Spreadsheet.Row()

                    Dim columns As New List(Of String)()
                    For Each column As System.Data.DataColumn In table.Columns
                        columns.Add(column.ColumnName)

                        Dim cell As New DocumentFormat.OpenXml.Spreadsheet.Cell()
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                        cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName)
                        headerRow.AppendChild(cell)
                    Next column


                    sheetData.AppendChild(headerRow)

                    For Each dsrow As System.Data.DataRow In table.Rows
                        Dim newRow As New DocumentFormat.OpenXml.Spreadsheet.Row()
                        For Each col As String In columns
                            Dim cell As New DocumentFormat.OpenXml.Spreadsheet.Cell()
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                            cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow(col).ToString())
                            newRow.AppendChild(cell)
                        Next col

                        sheetData.AppendChild(newRow)
                    Next dsrow
                Next table
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return intReturn
    End Function

    'Hemant (24 Dec 2020) -- Start
    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
    Public Function OpenXML_Export(ByVal strFilePath As String, ByVal ds As DataSet, ByVal blnShowHeader As Boolean) As Integer
        Dim intReturn As Integer = 0
        Try
            Using workbook = DocumentFormat.OpenXml.Packaging.SpreadsheetDocument.Create(strFilePath, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook)
                Dim workbookPart = workbook.AddWorkbookPart()
                workbook.WorkbookPart.Workbook = New DocumentFormat.OpenXml.Spreadsheet.Workbook()
                workbook.WorkbookPart.Workbook.Sheets = New DocumentFormat.OpenXml.Spreadsheet.Sheets()
                For Each table As System.Data.DataTable In ds.Tables
                    Dim sheetPart = workbook.WorkbookPart.AddNewPart(Of DocumentFormat.OpenXml.Packaging.WorksheetPart)()
                    Dim sheetData = New DocumentFormat.OpenXml.Spreadsheet.SheetData()
                    sheetPart.Worksheet = New DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData)

                    Dim sheets As DocumentFormat.OpenXml.Spreadsheet.Sheets = workbook.WorkbookPart.Workbook.GetFirstChild(Of DocumentFormat.OpenXml.Spreadsheet.Sheets)()
                    Dim relationshipId As String = workbook.WorkbookPart.GetIdOfPart(sheetPart)
                    Dim sheetId As UInteger = 1
                    If sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Count() > 0 Then
                        sheetId = sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Select(Function(s) s.SheetId.Value).Max() + 1
                    End If
                    Dim sheet As New DocumentFormat.OpenXml.Spreadsheet.Sheet() With {.Id = relationshipId, .SheetId = sheetId, .Name = table.TableName}
                    sheets.Append(sheet)

                    Dim columns As New List(Of String)()
                    If blnShowHeader = True Then
                        Dim headerRow As New DocumentFormat.OpenXml.Spreadsheet.Row()

                        For Each column As System.Data.DataColumn In table.Columns
                            columns.Add(column.ColumnName)

                            Dim cell As New DocumentFormat.OpenXml.Spreadsheet.Cell()
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                            cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName)
                            headerRow.AppendChild(cell)
                        Next column

                        sheetData.AppendChild(headerRow)
                    Else
                        For Each column As System.Data.DataColumn In table.Columns
                            columns.Add(column.ColumnName)
                        Next column
                    End If

                    For Each dsrow As System.Data.DataRow In table.Rows
                        Dim newRow As New DocumentFormat.OpenXml.Spreadsheet.Row()
                        For Each col As String In columns
                            Dim cell As New DocumentFormat.OpenXml.Spreadsheet.Cell()
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                            cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow(col).ToString())
                            newRow.AppendChild(cell)
                        Next col

                        sheetData.AppendChild(newRow)
                    Next dsrow
                Next table
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return intReturn
    End Function
    'Hemant (24 Dec 2020) -- End

    Public Function OpenXML_Export(ByVal strFilePath As String, ByVal dt As DataTable) As Integer
        Dim intReturn As Integer = 0
        Try
            Using workbook = DocumentFormat.OpenXml.Packaging.SpreadsheetDocument.Create(strFilePath, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook)
                Dim workbookPart = workbook.AddWorkbookPart()
                workbook.WorkbookPart.Workbook = New DocumentFormat.OpenXml.Spreadsheet.Workbook()
                workbook.WorkbookPart.Workbook.Sheets = New DocumentFormat.OpenXml.Spreadsheet.Sheets()

                Dim sheetPart = workbook.WorkbookPart.AddNewPart(Of DocumentFormat.OpenXml.Packaging.WorksheetPart)()
                Dim sheetData = New DocumentFormat.OpenXml.Spreadsheet.SheetData()
                sheetPart.Worksheet = New DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData)

                Dim sheets As DocumentFormat.OpenXml.Spreadsheet.Sheets = workbook.WorkbookPart.Workbook.GetFirstChild(Of DocumentFormat.OpenXml.Spreadsheet.Sheets)()
                Dim relationshipId As String = workbook.WorkbookPart.GetIdOfPart(sheetPart)
                Dim sheetId As UInteger = 1
                If sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Count() > 0 Then
                    sheetId = sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Select(Function(s) s.SheetId.Value).Max() + 1
                End If
                Dim sheet As New DocumentFormat.OpenXml.Spreadsheet.Sheet() With {.Id = relationshipId, .SheetId = sheetId, .Name = dt.TableName}
                sheets.Append(sheet)

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : OpenXML_Export ; Module Name : " & mstrModuleName)
        End Try
        Return intReturn
    End Function

    Public Function GetCellValue(ByVal document As DocumentFormat.OpenXml.Packaging.SpreadsheetDocument, ByVal cell As DocumentFormat.OpenXml.Spreadsheet.Cell) As String
        Try
            Dim stringTablePart As DocumentFormat.OpenXml.Packaging.SharedStringTablePart = document.WorkbookPart.SharedStringTablePart
            Dim value As String = cell.CellValue.InnerXml
            If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString Then
                Try
                    Return stringTablePart.SharedStringTable.ChildElements(Int32.Parse(value)).InnerText
                Catch ex As Exception
                    Return ""
                End Try
            Else
                Return value
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetCellValue ; Module Name : " & mstrModuleName)
        End Try
    End Function

    Public Function GetSharedStringItemById(ByVal workbookPart As DocumentFormat.OpenXml.Packaging.WorkbookPart, ByVal id As Integer) As DocumentFormat.OpenXml.Spreadsheet.SharedStringItem
        Return workbookPart.SharedStringTablePart.SharedStringTable.Elements(Of DocumentFormat.OpenXml.Spreadsheet.SharedStringItem)().ElementAt(id)
    End Function
    'S.SANDEEP [12-Jan-2018] -- END

    'Sohail (12 Apr 2019) -- Start
    'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
    Public Sub ExportGenericListToExcel(Of T)(ByVal list As List(Of T), ByVal strSheetName As String, ByVal strFilePath As String)
        Using workbook = DocumentFormat.OpenXml.Packaging.SpreadsheetDocument.Create(strFilePath, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook)
            Dim workbookPart = workbook.AddWorkbookPart()
            workbook.WorkbookPart.Workbook = New DocumentFormat.OpenXml.Spreadsheet.Workbook()
            workbook.WorkbookPart.Workbook.Sheets = New DocumentFormat.OpenXml.Spreadsheet.Sheets()

            Dim sheetPart = workbook.WorkbookPart.AddNewPart(Of DocumentFormat.OpenXml.Packaging.WorksheetPart)()
            Dim sheetData = New DocumentFormat.OpenXml.Spreadsheet.SheetData()
            sheetPart.Worksheet = New DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData)

            Dim sheets As DocumentFormat.OpenXml.Spreadsheet.Sheets = workbook.WorkbookPart.Workbook.GetFirstChild(Of DocumentFormat.OpenXml.Spreadsheet.Sheets)()
            Dim relationshipId As String = workbook.WorkbookPart.GetIdOfPart(sheetPart)
            Dim sheetId As UInteger = 1
            If sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Count() > 0 Then
                sheetId = sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Select(Function(s) s.SheetId.Value).Max() + 1
            End If
            Dim sheet As New DocumentFormat.OpenXml.Spreadsheet.Sheet() With {.Id = relationshipId, .SheetId = sheetId, .Name = strSheetName}
            sheets.Append(sheet)

            Dim headerRow As New DocumentFormat.OpenXml.Spreadsheet.Row()

            Dim properties As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
            Dim strColName As String
            For i As Integer = 0 To properties.Count - 1
                If properties(i).Description.ToLower = "hide" Then Continue For

                strColName = properties(i).Name
                If strColName.StartsWith("_") = True Then strColName = strColName.Substring(1)

                Dim cell As New DocumentFormat.OpenXml.Spreadsheet.Cell()
                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(strColName)
                headerRow.AppendChild(cell)
            Next


            sheetData.AppendChild(headerRow)

            For i As Integer = 0 To list.Count - 1
                Dim newRow As New DocumentFormat.OpenXml.Spreadsheet.Row()
                For j As Integer = 0 To properties.Count - 1
                    If properties(j).Description.ToLower = "hide" Then Continue For

                    Dim cell As New DocumentFormat.OpenXml.Spreadsheet.Cell()
                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                    cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(properties(j).GetValue(list(i)))
                    newRow.AppendChild(cell)
                Next

                sheetData.AppendChild(newRow)
            Next

        End Using

    End Sub
    'Sohail (12 Apr 2019) -- End

    'Sohail (18 Apr 2018) -- Start
    'Malawi Enhancement - Ref # 211 : New statutory report for Malawi - Form P.9 shows annual gross incomes and taxable income for employee in 72.1.
    ''' <summary>
    ''' Return Date to Nth Date format i.e. 31st December 2018
    ''' </summary>
    ''' <param name="dtDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ToNthDateFormat(ByVal dtDate As Date) As String
        Dim dt As Date = dtDate
        Dim strSuffix As String
        Dim strDate As String = dtDate.ToShortDateString()
        Try

            Dim a() As Integer = {11, 12, 13}

            If a.Contains(dt.Day) Then
                strSuffix = "th"
            ElseIf dt.Day Mod 10 = 1 Then
                strSuffix = "st"
            ElseIf dt.Day Mod 10 = 2 Then
                strSuffix = "nd"
            ElseIf dt.Day Mod 10 = 3 Then
                strSuffix = "rd"
            Else
                strSuffix = "th"
            End If

            strDate = String.Format("{1}{2} {0:MMMM}, {0:yyyy}", dtDate, dtDate.Day, strSuffix)

        Catch ex As Exception

        End Try
        Return strDate
    End Function
    'Sohail (18 Apr 2018) -- End


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

    Public Sub GetADConnection(ByRef mstrADIPAddress As String, ByRef mstrADDomain As String, ByRef mstrADUserName As String, ByRef mstrADUserPwd As String, Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim objConfig As New clsConfigOptions
        Try
            objConfig.IsValue_Changed("ADIPAddress", "-999", mstrADIPAddress, objDoOperation)
            objConfig.IsValue_Changed("ADDomain", "-999", mstrADDomain, objDoOperation)
            objConfig.IsValue_Changed("ADDomainUser", "-999", mstrADUserName, objDoOperation)
            objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrADUserPwd, objDoOperation)

            If mstrADUserPwd.Trim.Length > 0 Then
                mstrADUserPwd = clsSecurity.Decrypt(mstrADUserPwd, "ezee")
            End If

            Dim ar() As String = Nothing
            If mstrADDomain.Trim.Length > 0 AndAlso mstrADDomain.Trim.Contains(".") Then
                ar = mstrADDomain.Trim.Split(CChar("."))
                mstrADDomain = ""
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        mstrADDomain &= ",DC=" & ar(i)
                    Next
                End If
            End If

            If mstrADDomain.Trim.Length > 0 Then
                mstrADDomain = mstrADDomain.Trim.Substring(1)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetADConnection ; Module Name : " & mstrModuleName)
        Finally
            objConfig = Nothing
        End Try
    End Sub

    Public Function IsADUserExist(ByVal mstrEmpDisplayName As String, ByVal mstrADIPAddress As String, ByVal mstrADDomain As String, ByVal mstrADUserName As String, ByVal mstrADUserPwd As String, ByRef result As SearchResult) As Boolean
        Dim entry As DirectoryEntry = Nothing
        Try
            '/* START FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT
            entry = New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim & "/" & mstrADDomain.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim)
            Dim obj As Object = entry.NativeObject
            Dim search As DirectorySearcher = New DirectorySearcher(entry)
            search.Filter = "(SAMAccountName=" & mstrEmpDisplayName & ")"
            search.PropertiesToLoad.Add("cn")
            result = search.FindOne()
            If (result IsNot Nothing) Then
                Return True
            End If
            entry.Close()
            '/* END FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT
        Catch ex As Exception

            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If ex.Message.ToString().Contains("The server is not operational.") Then
                Throw New Exception("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.")
            Else
            Throw New Exception(ex.Message & "; Procedure Name: IsADUserExist; Module Name: " & mstrModuleName)
            End If
        Finally
            ' entry.Close()
            'Pinkal (04-Apr-2020) -- End
        End Try
    End Function

    Public Sub SetADProperty(ByVal de As DirectoryEntry, ByVal pName As String, ByVal pValue As String, ByRef mstrProperty As String)
        Try

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If pValue IsNot Nothing Then
                mstrProperty = pName
                If de.Properties.Contains(pName) Then 'The DE contains this property
                    de.Properties(pName).Value = pValue.Trim() 'Update the properties value
                Else    'Property doesnt exist
                    de.Properties(pName).Add(pValue.Trim()) 'Add the property and set it's value
                End If
            End If
            'Pinkal (04-Feb-2019) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & " " & "[" & pName & "]" & "; Procedure Name: SetADProperty; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub EnableDisableActiveDirectoryUser(ByVal blnEnable As Boolean, ByVal mstrUserName As String, ByVal mstrADIPAddress As String, ByVal mstrADDomain As String, ByVal mstrADDomainUser As String, ByVal mstrADDomainUserPwd As String)
        Try
            Dim entry As New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim() & "/" & mstrADDomain.Trim(), mstrADDomainUser.Trim(), mstrADDomainUserPwd.Trim())
            Dim obj As Object = entry.NativeObject
            Dim search As New DirectorySearcher(entry)
            search.Filter = "(SAMAccountName=" & mstrUserName & ")"
            search.PropertiesToLoad.Add("cn")
            search.PropertiesToLoad.Add("userAccountControl")
            Dim result As SearchResult = search.FindOne()
            If result IsNot Nothing Then
                Dim objUserEntry As DirectoryEntry = result.GetDirectoryEntry()
                Dim iValue As Integer = Convert.ToInt32(objUserEntry.Properties("userAccountControl").Value)
                If blnEnable Then ' Enable AD User
                    objUserEntry.Properties("userAccountControl").Value = iValue And Not &H2
                Else ' DisableAD User
                    objUserEntry.Properties("userAccountControl").Value = iValue Or &H2
                End If
                objUserEntry.CommitChanges()
                objUserEntry.Close()
            End If
            entry.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: EnableDisableActiveDirectoryUser; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Aug-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    Public Function JsonStringToDataTable(ByVal jsonString As String) As DataTable
        Dim dt As New DataTable()
        Try
            Dim jsonStringArray() As String = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{")
            Dim ColumnsName As New List(Of String)()

            For Each jSA As String In jsonStringArray

                Dim jsonStringData() As String = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",")

                For Each ColumnsNameData As String In jsonStringData
                    Try
                        Dim idx As Integer = ColumnsNameData.IndexOf(":")
                        Dim ColumnsNameString As String = ColumnsNameData.Substring(0, idx - 1).Replace("""", "").Trim
                        If Not ColumnsName.Contains(ColumnsNameString) Then
                            ColumnsName.Add(ColumnsNameString)
                        End If
                    Catch ex As Exception
                        Throw New Exception(String.Format("Error Parsing Column Name : {0}", ColumnsNameData))
                    End Try
                Next

                Exit For
            Next

            For Each AddColumnName As String In ColumnsName
                dt.Columns.Add(AddColumnName)
            Next

            For Each jSA As String In jsonStringArray
                Dim RowData() As String = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",")
                Dim nr As DataRow = dt.NewRow()
                For Each rowData_Renamed As String In RowData
                    Try
                        Dim idx As Integer = rowData_Renamed.IndexOf(":")
                        Dim RowColumns As String = rowData_Renamed.Substring(0, idx - 1).Replace("""", "").Trim()
                        Dim RowDataString As String = rowData_Renamed.Substring(idx + 1).Replace("""", "").Trim()
                        nr(RowColumns) = RowDataString
                    Catch ex As Exception
                        Continue For
                    End Try
                Next
                dt.Rows.Add(nr)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: JsonStringToDataTable; Module Name: " & mstrModuleName)
        End Try
        Return dt
    End Function

    <DllImport("urlmon.dll", CharSet:=CharSet.Auto)> _
        Private Function FindMimeFromData( _
            ByVal pBC As IntPtr, _
            <MarshalAs(UnmanagedType.LPWStr)> _
            ByVal pwzUrl As String, _
            <MarshalAs(UnmanagedType.LPArray, ArraySubType:=UnmanagedType.I1, SizeParamIndex:=3)> ByVal _
            pBuffer As Byte(), _
            ByVal cbSize As Integer, _
            <MarshalAs(UnmanagedType.LPWStr)> _
            ByVal pwzMimeProposed As String, _
            ByVal dwMimeFlags As Integer, _
            <MarshalAs(UnmanagedType.LPWStr)> _
            ByRef ppwzMimeOut As String, _
            ByVal dwReserved As Integer) As Integer
    End Function

    Public Function GetMimeType(ByVal filepath As String) As String
        Dim mime As String = Nothing
        Try
            Dim MaxContent As Integer = CInt(New FileInfo(filepath).Length)
            If MaxContent > 4096 Then
                MaxContent = 4096
            End If
            Dim fs As New FileStream(filepath, FileMode.Open)
            Dim buf(MaxContent) As Byte
            fs.Read(buf, 0, MaxContent)
            fs.Close()
            Dim result As Integer = FindMimeFromData(IntPtr.Zero, filepath, buf, MaxContent, Nothing, 0, mime, 0)
        Catch ex As Exception
        End Try
        Return mime
    End Function

    'Pinkal (20-Nov-2018) -- End

    'Sohail (26 Nov 2018) -- Start
    'NMB Enhancement - Flex Cube JV Integration in 75.1.
    Public Function GenerateOTP(ByVal intNoOfChars As Integer, ByVal blnIncludeNumbers As Boolean, ByVal blnIncludeSmallLetters As Boolean, ByVal blnIncludeCapitalLetters As Boolean) As String
        Dim strOTP As String = ""
        Try
            If intNoOfChars <= 0 Then Exit Try

            Dim strChars As String = ""
            If blnIncludeNumbers = True Then strChars &= "0123456789"
            If blnIncludeSmallLetters = True Then strChars &= "abcdefghijklmnopqrstuvwxyz"
            If blnIncludeCapitalLetters = True Then strChars &= "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            If strChars.Trim.Length <= 0 Then Exit Try

            Dim charArr As Char() = strChars.ToCharArray()
            Dim strRandom As String = String.Empty
            Dim objran As New Random()
            Dim noofcharacters As Integer = intNoOfChars
            For i As Integer = 0 To noofcharacters - 1

                Dim pos As Integer = objran.[Next](1, charArr.Length)
                If Not strRandom.Contains(charArr.GetValue(pos).ToString()) Then
                    strRandom &= charArr.GetValue(pos).ToString
                Else
                    i -= 1
                End If
            Next

            strOTP = strRandom

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: JsonStringToDataTable; Module Name: " & mstrModuleName)
        End Try
        Return strOTP
    End Function
    'Sohail (26 Nov 2018) -- End

    'Gajanan(22 Nov 2018) -- Start
    ''Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Public StrEmployeeProfileFolder As String = "EmployeeProfile"
    'Gajanan(22 Nov 2018) -- End

    'Sohail (13 Dec 2018) -- Start
    'NMB Enhancement - Exporting Flex Cube NMB JV to CSV in 75.1.
    Public Function DataTableToCSV(ByVal dtTable As DataTable, ByVal chrSeperator As Char, ByVal blnIncludeColumnHeader As Boolean) As StringBuilder
        Dim strSB As New StringBuilder
        Try
            If blnIncludeColumnHeader = True Then

                For i As Integer = 0 To dtTable.Columns.Count - 1
                    strSB.Append(dtTable.Columns(i).Caption)

                    If i < dtTable.Columns.Count - 1 Then
                        strSB.Append(chrSeperator)
                    End If
                Next
                strSB.AppendLine()
            End If

            For Each dtRow As DataRow In dtTable.Rows
                For i As Integer = 0 To dtTable.Columns.Count - 1
                    strSB.Append(dtRow(i).ToString().Replace(chrSeperator, ""))

                    If i < dtTable.Columns.Count - 1 Then
                        strSB.Append(chrSeperator)
                    End If
                Next
                strSB.AppendLine()
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DataTableToCSV; Module Name: " & mstrModuleName)
        End Try
        Return strSB
    End Function
    'Sohail (13 Dec 2018) -- End


    ''Gajanan [26-Feb-2019] -- Start
    '<Runtime.CompilerServices.Extension()> _
    'Public Function TitleCase(ByVal value As String) As String
    '    Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
    '    Dim result As String = info1.ToTitleCase(value.ToLower())
    '    Return result
    'End Function
    ''Gajanan [26-Feb-2019] -- End


    'Gajanan [27-Mar-2019] -- Start
    'Enhancement - Change Email Language
    Public Function getTitleCase(ByVal str As String) As String
        Dim TitleString As String
        Try
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'TitleString = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower)
            TitleString = StrConv(str, VbStrConv.ProperCase)
            'Sohail (18 May 2019) -- End
            Return TitleString
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getTitleCase", "modGlobal")
            Return ""
        End Try
    End Function

    'Gajanan [27-Mar-2019] -- End

    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    Public Function getKeyWordsCompanyAccountJV(Optional ByVal blnIncludeShortName1 As Boolean = True _
                                              , Optional ByVal blnIncludeShortName2 As Boolean = True) As String
        'Sohail (26 Mar 2020) - [blnIncludeShortName1, blnIncludeShortName2]
        Dim strB As New StringBuilder
        Try
            strB.Length = 0

            strB.AppendLine("#companycode#")
            strB.AppendLine("#periodcode#")
            strB.AppendLine("#periodname#")
            strB.AppendLine("#accountcode#")
            strB.AppendLine("#accountname#")
            strB.AppendLine("#tranheadcode#") 'Sohail (08 Apr 2020)
            strB.AppendLine("#tranheadname#")
            'Sohail (26 Mar 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            'strB.AppendLine("#shortname#")
            'strB.AppendLine("#shortname2#")
            If blnIncludeShortName1 = True Then
            strB.AppendLine("#shortname#")
            End If

            If blnIncludeShortName2 = True Then
            strB.AppendLine("#shortname2#")
            End If
            'Sohail (26 Mar 2020) -- End

            Return strB.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getKeyWordsCompanyAccountJV; Module Name: " & mstrModuleName)
            Return ""
        End Try
    End Function

    Public Function getKeyWordsEmployeeAccountJV(Optional ByVal blnIncludeShortName1 As Boolean = True _
                                               , Optional ByVal blnIncludeShortName2 As Boolean = True) As String
        'Sohail (26 Mar 2020) - [blnIncludeShortName1, blnIncludeShortName2]
        Dim strB As New StringBuilder
        Try
            strB.Length = 0

            strB.AppendLine("#companycode#")
            strB.AppendLine("#periodcode#")
            strB.AppendLine("#periodname#")
            strB.AppendLine("#accountcode#")
            strB.AppendLine("#accountname#")
            strB.AppendLine("#tranheadcode#") 'Sohail (08 Apr 2020)
            strB.AppendLine("#tranheadname#")
            strB.AppendLine("#employeecode#")
            strB.AppendLine("#employeename#")
            'Sohail (26 Mar 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            strB.AppendLine("#costcentercode#")
            strB.AppendLine("#costcentername#")
            'Sohail (26 Mar 2020) -- End
            strB.AppendLine("#costcentercustomcode#")
            'Sohail (26 Mar 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            'strB.AppendLine("#shortname#")
            'strB.AppendLine("#shortname2#")
            If blnIncludeShortName1 = True Then
            strB.AppendLine("#shortname#")
            End If

            If blnIncludeShortName2 = True Then
            strB.AppendLine("#shortname2#")
            End If
            'Sohail (26 Mar 2020) -- End

            Return strB.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getKeyWordsEmployeeAccountJV; Module Name: " & mstrModuleName)
            Return ""
        End Try
    End Function

    Public Function getKeyWordsCostCenterAccountJV(Optional ByVal blnIncludeShortName1 As Boolean = True _
                                                 , Optional ByVal blnIncludeShortName2 As Boolean = True) As String
        'Sohail (26 Mar 2020) - [blnIncludeShortName1, blnIncludeShortName2]
        Dim strB As New StringBuilder
        Try
            strB.Length = 0

            strB.AppendLine("#companycode#")
            strB.AppendLine("#periodcode#")
            strB.AppendLine("#periodname#")
            strB.AppendLine("#accountcode#")
            strB.AppendLine("#accountname#")
            strB.AppendLine("#tranheadcode#") 'Sohail (08 Apr 2020)
            strB.AppendLine("#tranheadname#")
            strB.AppendLine("#costcentercode#")
            strB.AppendLine("#costcentercustomcode#")
            strB.AppendLine("#costcentername#")
            'Sohail (26 Mar 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            'strB.AppendLine("#shortname#")
            'strB.AppendLine("#shortname2#")
            If blnIncludeShortName1 = True Then
            strB.AppendLine("#shortname#")
            End If

            If blnIncludeShortName2 = True Then
            strB.AppendLine("#shortname2#")
            End If
            'Sohail (26 Mar 2020) -- End

            Return strB.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getKeyWordsEmployeeAccountJV; Module Name: " & mstrModuleName)
            Return ""
        End Try
    End Function
    'Sohail (03 Mar 2020) -- End


End Module

Public Enum enUserType
    Approver = 1
    Assessor = 2
    'S.SANDEEP [ 04 FEB 2014 ] -- START
    crApprover = 3
    'S.SANDEEP [ 04 FEB 2014 ] -- END
    LoanApprover = 4

    'Pinkal (21-Oct-2016) -- Start
    'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
    Timesheet_Approver = 5
    'Pinkal (21-Oct-2016) -- End

    'S.SANDEEP |19-JUN-2020| -- START
    'ISSUE/ENHANCEMENT : MIGRATION PROCESS
    Calibration_Approver = 6
    'S.SANDEEP |19-JUN-2020| -- END

End Enum

Public Enum enPayrollGroupType
    CostCenter = 1
    'Vendor = 2
    Bank = 3
End Enum



'Pinkal (15-Nov-2013) -- Start
'Enhancement : Oman Changes

Public Enum enAccrueSetting
    Issue_Balance = 1
    Exceeding_balance_Unpaid = 2
    Donot_Issue_On_Exceeding_Balance = 3
    No_Balance = 4
    Donot_Issue_On_Exceeding_Balance_Ason_Date = 5
End Enum

'Pinkal (15-Nov-2013) -- End


'Pinkal (06-Dec-2013) -- Start
'Enhancement : Oman Changes

Public Enum enInOutSource
    LoginPassword = 1
    FingerPrint = 2
    Swipecard = 3
    Import = 4
    Manual = 5
    GroupLogin = 6
    GlobalAdd = 7
    GlobalEdit = 8
    GlobalDelete = 9
End Enum

'Pinkal (06-Dec-2013) -- End


Public Enum enScanAttactRefId


    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'EMPLOYEE_IDENTITY = 1
    'EMPLOYEE_MEMBERSHIP = 2
    'EMPLOYEE_QUALIFICATION = 3
    'EMPLOYEE_JOBHISTORY = 4
    'EMPLOYEE_LEAVEFORM = 5
    'EMPLOYEE_DISCIPLINE = 6
    'APPLICANT_CV = 7
    'APPLICANT_QUALIFICATION = 8
    'APPLICANT_JOBHISTORY = 9
    ''Sandeep [ 21 APRIL 2011 ] -- Start
    ''Issue : SCAN & ATTACHMENT ENHANCEMENT
    'OTHER_DOCUMENTS = 10
    'EMPLOYEE_TRAINING = 11
    ''Sandeep [ 21 APRIL 2011 ] -- End
    IDENTITYS = 1
    MEMBERSHIPS = 2
    QUALIFICATIONS = 3
    JOBHISTORYS = 4
    LEAVEFORMS = 5
    DISCIPLINES = 6
    CURRICULAM_VITAE = 7
    OTHERS = 8
    TRAININGS = 9
    ASSET_DECLARATION = 10 'Sohail (07 Mar 2012)
    DEPENDANTS = 11 'Shani (20 JUN 2015)
    'S.SANDEEP [ 18 FEB 2012 ] -- START

    CLAIM_REQUEST = 12 'Shani (20-Aug-2016) -- 

    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 136
    ASSESSMENT = 13
    'S.SANDEEP [29-NOV-2017] -- END

    'Gajanan [13-AUG-2018] -- Start
    'Enhancement - Implementing Grievance Module.
    GRIEVANCES = 14
    'Gajanan(13-AUG-2018) -- End

    'S.SANDEEP [09-OCT-2018] -- START
    TRAINING_REQUISITION = 15
    'S.SANDEEP [09-OCT-2018] -- END

    'S.SANDEEP |26-APR-2019| -- START
    PERSONAL_ADDRESS_TYPE = 16
    DOMICILE_ADDRESS_TYPE = 17
    RECRUITMENT_ADDRESS_TYPE = 18
    EMERGENCY_CONTACT1 = 19
    EMERGENCY_CONTACT2 = 20
    EMERGENCY_CONTACT3 = 21
    EMPLOYEE_BIRTHINFO = 22
    EMPLYOEE_OTHERLDETAILS = 23
    'S.SANDEEP |26-APR-2019| -- END
    'Sohail (04 Jul 2019) -- Start
    'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (add New document type COVER LETTER).
    COVER_LETTER = 24
    'Sohail (04 Jul 2019) -- End

    'Gajanan [02-SEP-2019] -- Start      
    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    STAFF_REQUISITION = 25
    'Gajanan [02-SEP-2019] -- End

    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    CLAIM_RETIREMENT = 26
    'Pinkal (11-Sep-2019) -- End
    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : New Screen Training Need Form.
    TRAINING_NEED_FORM = 27
    'Sohail (14 Nov 2019) -- End

    PDP = 28
    Talent = 29
    Succession = 30

End Enum

'Public Class ConfigParameter
'    Public Shared ReadOnly Property _Object() As clsConfigOptions
'        Get
'            Return gobjConfigOptions
'        End Get
'    End Property
'End Class

Public Enum enArutiApplicatinType
    Aruti_Configuration = 1
    Aruti_Payroll = 2

    'Pinkal (24-Jan-2011) -- Start

    Aruti_TimeSheet = 3

    'Pinkal (24-Jan-2011) -- End
End Enum

Public Enum enImg_Email_RefId
    Employee_Module = 1
    Applicant_Module = 2
    Leave_Module = 3
    Payroll_Module = 4
    Dependants_Beneficiaries = 5
    Medical_Module = 6  'Pinkal (20-Jan-2012) -- Start
    Discipline_Module = 7 'S.SANDEEP [ 04 FEB 2012 ] -- START -- END
    Appraisal_Module = 8 'S.SANDEEP [ 04 FEB 2012 ] -- START -- END
    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Training_Module = 9
    Scan_Attached_Doc = 10  'For Sending Mail
    'S.SANDEEP [ 18 FEB 2012 ] -- START

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Qualification = 11
    'SHANI (20 JUN 2015) -- End 

    'Shani (20-Aug-2016) -- Start
    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    Claim_Request = 12
    'Shani (20-Aug-2016) -- End

    'S.SANDEEP [30 DEC 2016] -- START
    'ENHANCEMENT : VOLTAMP EMAILING REPORT
    TimeAndAttendance = 13
    'S.SANDEEP [30 DEC 2016] -- END

    'S.SANDEEP [18-AUG-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#290}
    PerformanceAssessment_Employee = 14
    PerformanceAssessment_Assessor = 15
    PerformanceAssessment_Reviewer = 16
    'S.SANDEEP [18-AUG-2018] -- END


    'Pinkal (16-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Leave_Planner_Module = 17  'THIS IS ONLY USED IN REFLEX
    'Pinkal (16-Oct-2018) -- End


    'Gajanan [13-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
    Grievance = 18
    'Gajanan [13-July-2019] -- End

    'Gajanan [02-SEP-2019] -- Start      
    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    Staff_Requisition = 19
    'Gajanan [02-SEP-2019] -- End


    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    Claim_Retirement = 20
    'Pinkal (11-Sep-2019) -- End

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
    Batch_Scheduling = 21
    'Hemant (07 Oct 2019) -- End

    'Hemant (30 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
    Applicant_Job_Vacancy = 22
    'Hemant (30 Oct 2019) -- End
    'Sohail (17 Feb 2021) -- Start
    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
    Loan_Balance = 23
    'Sohail (17 Feb 2021) -- End

End Enum

'Sohail (25 Jan 2011) -- Start
Public Enum enFormula_LeaveType
    PAID_ACCRUE_LEAVE = 1
    PAID_ISSUE_LEAVE = 2
    UNPAID_ISSUE_LEAVE = 3
End Enum

Public Enum enDebitCredit
    DEBIT = 1
    CREDIT = 2
End Enum

Public Enum enJVTransactionType
    TRANSACTION_HEAD = 1
    LOAN = 2
    ADVANCE = 3
    SAVINGS = 4
    'Sohail (15 Feb 2011) -- Start
    CASH = 5
    BANK = 6
    COST_CENTER = 7
    'Sohail (15 Feb 2011) -- End
    PAY_PER_ACTIVITY = 8 'Sohail (21 Jun 2013)
    CR_EXPENSE = 9 'Sohail (12 Nov 2014)
    COMPANY_BANK = 10 'Sohail (03 Jan 2019)
    CR_RETIRE_EXPENSE = 11 'Sohail (07 Jun 2021)
End Enum

Public Enum enAccountGroup
    AccountsPayable = 1
    AccountsReceivable = 2
    Bank = 3
    CostOfGoodsSold = 4
    CreditCard = 5
    Equity = 6
    Expense = 7
    FixedAsset = 8
    Income = 9
    LongTermLiability = 10
    NonPosting = 11
    OtherAsset = 12
    OtherCurrentAsset = 13
    OtherCurrentLiability = 14
    OtherExpense = 15
    OtherIncome = 16
End Enum


Public Enum enIntegration
    QuickBook = 1
    Tally = 2
    PeachTree = 3
    Pastel = 4
    'eZeeNextgen = 5
    'eZeeNisco = 6
    Sun_Account = 5 'Sohail (07 Aug 2012)
    Orbit = 6 'Sohail (26 Dec 2012)
    'Sohail (25 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    'StandardBank = 7 'Sohail (10 Jan 2013)
    'iScala = 8 'Sohail (17 Jan 2013)
    iScala = 7
    'Sohail (25 Jan 2013) -- End
    SAP_JV = 8 'Sohail (25 Feb 2013)
    Sun_Account_5 = 9 'Sohail (19 Mar 2013)
    iScala_2 = 10 'Sohail (03 Mar 2014)
    XERO = 11 'Sohail (06 Dec 2014)
    NETSUITE_ERP = 12 'Sohail (19 Feb 2016)
    FLEX_CUBE_RETAIL = 13 'Sohail (06 Aug 2016)
    SUN_ACCOUNT_PROJECT_JV = 14 'Sohail (02 Sep 2016)
    DYNAMICS_NAV = 15 'Sohail (08 Jul 2017)
    'Sohail (17 Feb 2018) -- Start
    'AMANA Bank Enhancement : Ref. No. 172 - UPLOAD FILE FROM ARUTI TO FLEX CUBE - (RefNo: 171) in 70.1.
    FLEX_CUBE_UPLD = 16
    'Sohail (17 Feb 2018) -- End
    'Sohail (21 Feb 2018) -- Start
    'CCK Enhancement : Ref. No. 174 - SAP Salary Journal - (RefNo: 174) in 70.1.
    SAP_JV_BS_ONE = 17
    'Sohail (21 Feb 2018) -- End

    'Anjan (17 Aug 2018) -- Start
    'Internal Enhancement - included epicor integration for Rutta demo.
    'EPICOR = 18
    'Anjan (17 Aug 2018) -- End

    'Sohail (26 Nov 2018) -- Start
    'NMB Enhancement - Flex Cube JV Integration in 75.1.
    FLEX_CUBE_JV = 18
    'Sohail (26 Nov 2018) -- End
    'Sohail (02 Mar 2019) -- Start
    'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS EFT integration to export data to CBS SQL database in xml format.
    BR_JV = 19
    'Sohail (02 Mar 2019) -- End

    'Hemant (15 Mar 2019) -- Start
    'MELIA ARUSHA ENHANCEMENT - Ref # 3593 - 76.1 : SAP ECC 6.0 JV integration.
    SAP_ECC_6_0_JV = 20
    'Hemant (15 Mar 2019) -- End

    'Hemant (18 Apr 2019) -- Start
    'MWAUWASA ENHANCEMENT - Ref # 3747 - 76.1 : SAGE 300 JV integration.
    SAGE_300_JV = 21
    'Hemant (18 Apr 2019) -- End

    'Hemant (29 May 2019) -- Start
    'MUWASA ENHANCEMENT - Ref # 3329 - 76.1 : PASTEL V2 COMBINED JV INTEGRATION.
    PASTEL_V2_JV = 22
    'Hemant (29 May 2019) -- End
    'Sohail (08 Apr 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
    SAGE_300_ERP_JV = 23
    'Sohail (08 Apr 2020) -- End
    'Sohail (22 Jun 2020) -- Start
    ' CORAL BEACH CLUB enhancement : 0004743 : New JV Integration Coral Sun JV.
    CORAL_SUN_JV = 24
    'Sohail (22 Jun 2020) -- End
    'Sohail (10 Aug 2020) -- Start
    'Freight Forwarders Kenya Enhancement : OLD-58 #  : New Accounting software Integration "CargoWise JV" for Freight Forwarders Kenya Ltd.
    CARGO_WISE_JV = 25
    'Sohail (10 Aug 2020) -- End
    'Sohail (04 Sep 2020) -- Start
    'Benchmark Enhancement : OLD-73 #  : New Accounting software Integration "SAP Standard JV".
    SAP_STANDARD_JV = 26
    'Sohail (04 Sep 2020) -- End
    'Sohail (10 Sep 2020) -- Start
    'Oxygen Communication Ltd Enhancement : OLD-68 #  : New Accounting software Integration "SAGE Evolution JV".
    SAGE_EVOLUTION_JV = 27
    'Sohail (10 Sep 2020) -- End
    'Sohail (16 Nov 2021) -- Start
    'Enhancement : OLD-492 : New Payroll JV Integration - Hakika Bank JV.
    HAKIKA_BANK_JV = 28
    'Sohail (16 Nov 2021) -- End

End Enum

Public Enum enBankPaymentReport
    Bank_payment_List = 1
    Electronic_Fund_Transfer = 2
    EFT_Standard_Bank = 3
    'EFT_PSPF = 4 'Sohail (14 Mar 2013) - Moved to PSPF Statutory reports
    EFT_StandardCharteredBank = 5 'Anjan (04 Feb 2013)
    EFT_SFIBANK = 6 'Anjan (04 Feb 2013)
    'S.SANDEEP [ 07 JAN 2014 ] -- START
    EFT_VOLTAMP = 7
    'S.SANDEEP [ 07 JAN 2014 ] -- END

    'Pinkal (12-Jun-2014) -- Start
    'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
    Bank_Payment_Voucher = 8
    'Pinkal (12-Jun-2014) -- End
    'Sohail (25 Mar 2015) -- Start
    'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance Payment.
    EFT_ADVANCE_SALARY_CSV = 9
    EFT_ADVANCE_SALARY_XLS = 10
    'Sohail (25 Mar 2015) -- End
    'Sohail (12 Feb 2018) -- Start
    'Voltamp Enhancement : New Bank Payment List Report "Salary Format WPS" in 70.1.
    Salary_Format_WPS = 11
    'Sohail (12 Feb 2018) -- End
End Enum
'Sohail (25 Jan 2011) -- End

'Sohail (25 Jan 2013) -- Start
'TRA - ENHANCEMENT
Public Enum enEFTIntegration
    EFT_StandardBank = 1
    'Sohail (14 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    'EFT_PSPF = 2
    'EFT_StandardCharteredBank = 3 'Anjan (04 Feb 2013)
    'EFT_SFIBank = 4 'Anjan (04 Feb 2013)
    EFT_StandardCharteredBank = 2 'Anjan (04 Feb 2013)
    EFT_SFIBank = 3 'Anjan (04 Feb 2013)
    'Sohail (14 Mar 2013) -- End
    EFT_CityDirect = 4 'Sohail (22 Mar 2013)
    EFT_CBA = 5 'Sohail (08 Dec 2014)
    EFT_T24 = 6 'Sohail (12 Dec 2014)
    EFT_EXIM = 7 'Sohail (24 Dec 2014) 
    'Sohail (09 Mar 2015) -- Start
    'Enhancement - New EFT Report Custom CSV Report.
    EFT_CUSTOM_CSV = 8
    EFT_CUSTOM_XLS = 9
    'Sohail (09 Mar 2015) -- End
    'Sohail (01 Jun 2016) -- Start
    'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
    EFT_FLEX_CUBE_RETAIL = 10
    'Sohail (01 Jun 2016) -- End

    'Nilay (10-Nov-2016) -- Start
    'Enhancement : New EFT report : EFT ECO bank for OFFGRID
    EFT_ECO_BANK = 11
    'Nilay (10-Nov-2016) -- End


    'Anjan [21 November 2016] -- Start
    'ENHANCEMENT : Including NEW ACB EFT integration report.
    EFT_ACB_T24WEB = 12
    'Anjan [21 November 2016] -- End

    'Shani(28-FEB-2017) -- Start
    'Enhancement - Add New Bank Payment List Integration for TAMA
    EFT_BANK_PAYMENT_LETTER = 13
    'Shani(28-FEB-2017) -- End

    'Sohail (20 Sep 2017) -- Start
    'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
    EFT_BARCLAYS_BANK = 14
    'Sohail (20 Sep 2017) -- End

    'S.SANDEEP [04-May-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
    EFT_NATIONAL_BANK_OF_MALAWI = 15
    'S.SANDEEP [04-May-2018] -- END

    'S.SANDEEP [19-SEP-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002533|ARUTI-334}
    DFT_CITY_BANK = 16
    'S.SANDEEP [19-SEP-2018] -- END
    'Sohail (04 Nov 2019) -- Start
    'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
    EFT_FNB_BANK = 17
    'Sohail (04 Nov 2019) -- End
    'Hemant (11 Dec 2019) -- Start
    'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
    EFT_STANDARD_CHARTERED_BANK_S2B = 18
    'Hemant (11 Dec 2019) -- End

    'Hemant (29 Jun 2020) -- Start
    'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
    EFT_ABSA_BANK = 19
    'Hemant (29 Jun 2020) -- End

    'Hemant (24 Dec 2020) -- Start
    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
    EFT_NATIONAL_BANK_OF_MALAWI_XLSX = 20
    'Hemant (24 Dec 2020) -- End

    'Sohail (08 Jul 2021) -- Start
    'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
    EFT_EQUITY_BANK_OF_KENYA = 21
    EFT_NATIONAL_BANK_OF_KENYA = 22
    'Sohail (08 Jul 2021) -- End
    'Sohail (27 Jul 2021) -- Start
    'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
    EFT_CITI_BANK_KENYA = 23
    'Sohail (27 Jul 2021) -- End

End Enum
'Sohail (25 Jan 2013) -- End

'Sohail (08 Dec 2014) -- Start
'Enhancement - New Mobile Money EFT integration with MPESA.
Public Enum enMobileMoneyEFTIntegration
    MPESA = 1
End Enum
'Sohail (08 Dec 2014) -- End

'Sohail (14 Mar 2013) -- Start
'TRA - ENHANCEMENT
Public Enum enPSPFIntegration
    EFT_PSPF = 1
End Enum
'Sohail (14 Mar 2013) -- End

'Sohail (03 Mar 2014) -- Start
'Enhancement - iScala2 JV for Aga Khan
Public Enum enCustomCCenter
    CCENTER_CODE = 1
    CCENTER_NAME = 2
    EMPLOYEE_CODE = 3
    BRANCH_CODE = 4
End Enum
'Sohail (03 Mar 2014) -- End

'Sohail (14 Jun 2011) -- Start
'Changes : New Enum for new field 'IsApproved' on ED.
Public Enum enEDHeadApprovalStatus
    Approved = 1
    Pending = 2
    All = 3
End Enum
'Sohail (14 Jun 2011) -- End

'Sohail (12 Oct 2011) -- Start
Public Enum enPeriodCodeNameFormat
    One_Two_Three = 1
    ZeroOne_ZeroTwo_ZeroThree = 2
    MMM_YY = 3
    MMMM_YYYY = 4
End Enum
'Sohail (12 Oct 2011) -- End

'Sohail (26 Nov 2011) -- Start
Public Enum enExempHeadApprovalStatus
    Approved = 1
    Pending = 2
    All = 3
End Enum

Public Enum enViewPayroll_Group
    Earning = 1
    Deduction = 2
    EmployerContribution = 3
    Informational = 4
    Loan = 5
    Advance = 6
    Saving = 7
    PayPerActivity = 8 'Sohail (21 Jun 2013)
    CRExpense = 9 'Sohail (12 Nov 2014)
    Loan_Interest = 10 'Sohail (02 Apr 2018) -
End Enum
'Sohail (26 Nov 2011) -- End

'Sohail (05 Dec 2012) -- Start
'TRA - ENHANCEMENT
Public Enum enEDBatchPostingStatus
    Posted_to_ED = 1
    Not_Posted = 2
    All = 3
End Enum
'Sohail (05 Dec 2012) -- End

'Sohail (22 Oct 2013) -- Start
'TRA - ENHANCEMENT
Public Enum enPaymentRoundingType
    NONE = 0
    AUTOMATIC = 1
    UP = 2
    DOWN = 3
End Enum

Public Enum enPaymentRoundingMultiple
    _1 = 1
    _5 = 2
    _10 = 3
    _50 = 4
    _100 = 5
    _500 = 6
    _1000 = 7
    _200 = 8 'Sohail (08 Apr 2016)
End Enum
'Sohail (22 Oct 2013) -- End

'Sohail (18 Feb 2020) -- Start
'NMB Enhancement # : On the references page on recruitment portal, provide all those fields on configuration where they can be made mandatory or optional.
Public Enum enMandatorySettingRecruitmentGroup
    PersonalInfo = 1
    ContactDetail = 2
    OtherInformation = 3
    Skill = 4
    Qualification = 5
    Experience = 6
    Reference = 7
    Attachment = 8
    SearchJob = 9
End Enum
Public Enum enMandatorySettingRecruitment
    MiddleName = 1
    Gender = 2
    BirthDate = 3
    Nationality = 4

    Address1 = 5
    Address2 = 6
    Country = 7
    State = 8
    City = 9
    Region = 10
    PostCode = 11

    Language1 = 12
    MaritalStatus = 13
    MotherTongue = 14
    CurrentSalary = 15
    ExpectedSalary = 16
    ExpectedBenefits = 17
    NoticePeriod = 18

    QualificationStartDate = 19
    QualificationAwardDate = 20
    HighestQualification = 21

    Employer = 22
    CompanyName = 23
    PositionHeld = 24

    ReferencePosition = 25
    ReferenceType = 26
    ReferenceEmail = 27
    ReferenceAddress = 28
    ReferenceCountry = 29
    ReferenceState = 30
    ReferenceCity = 31
    ReferenceGeneder = 32
    ReferenceMobile = 33
    ReferenceTelNo = 34

    CurriculumVitae = 35
    CoverLetter = 36

    EarliestPossibleStartDate = 37
    VacancyFoundOutFrom = 38
    ApplicantComments = 39

    'Hemant (17 Nov 2020) -- Start
    'Enhancement : Add settings on configration in recruitment mandatory fields for result code and GPA under qualification
    QualificationResultCode = 40
    QualificationGPA = 41
    'Hemant (17 Nov 2020) -- End
End Enum
'Sohail (18 Feb 2020) -- End

''' <summary>
''' For add DISPLAY text and VALUE text manually.
''' </summary>
''' <remarks></remarks>
Public Class ComboBoxValue
    Private sDisplay As String
    Private sValue As Integer

    ''' <summary>
    ''' For add DISPLAY text and VALUE text manually.
    ''' </summary>
    ''' <param name="Display">Display Member Value</param>
    ''' <param name="Value">Display Member Value</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal Display As String, ByVal Value As Integer)
        sDisplay = Display
        sValue = Value
    End Sub

    ''' <summary>
    ''' Display Member Value
    ''' </summary>
    Public Property Display() As String
        Get
            Return sDisplay
        End Get
        Set(ByVal value As String)
            sDisplay = value
        End Set
    End Property

    ''' <summary>
    ''' Display Member Value
    ''' </summary>
    Public Property Value() As Integer
        Get
            Return sValue
        End Get
        Set(ByVal value As Integer)
            sValue = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return sDisplay
    End Function

End Class

'Sohail (16 Oct 2010) -- Start
Public Enum enTranHeadType
    EarningForEmployees = 1
    DeductionForEmployee = 2
    EmployeesStatutoryDeductions = 3
    EmployersStatutoryContributions = 4
    Informational = 5
End Enum

Public Enum enTypeOf
    Salary = 1
    Allowance = 2
    Bonus = 3
    Commission = 4
    'Sohail (16 Oct 2010) -- Start
    'Other_Earnings = 5
    'Taxes = 6
    'Other_Deductions_Emp = 7
    'Employers_Statutory_Contribution = 8
    'Employee_Statutory_Contributions = 9
    'Informational = 10
    'Other_Deductions_Satutary = 11
    'LEAVE_DEDUCTION = 12
    BENEFIT = 5
    Other_Earnings = 6
    LEAVE_DEDUCTION = 7
    Other_Deductions_Emp = 8
    Taxes = 9
    Employee_Statutory_Contributions = 10
    Other_Deductions_Satutary = 11
    Employers_Statutory_Contribution = 12
    Informational = 13
    'Sohail (16 Oct 2010) -- End
    PAY_PER_ACTIVITY = 14 'Sohail (17 Sep 2014)
    'Sohail (18 Apr 2016) -- Start
    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
    NET_PAY_ROUNDING_ADJUSTMENT = 15
    'Sohail (18 Apr 2016) -- End
End Enum

Public Enum enCalcType
    AsComputedValue = 1
    AsComputedOnWithINEXCESSOFTaxSlab = 2
    AsUserDefinedValue = 3
    DEFINED_SALARY = 4
    OnAttendance = 5
    OnHourWorked = 6
    FlatRate_Others = 7
    OverTimeHours = 8
    ShortHours = 9
    'Sohail (23 Dec 2010) -- Start
    NET_PAY = 10
    TOTAL_EARNING = 11
    TOTAL_DEDUCTION = 12
    'Sohail (23 Dec 2010) -- End
    EMPLOYER_CONTRIBUTION_PAYABLE = 13 'Sohail (02 Aug 2011)
    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    TAXABLE_EARNING_TOTAL = 14
    NON_TAXABLE_EARNING_TOTAL = 15
    'Sohail (18 Jan 2012) -- End
    NON_CASH_BENEFIT_TOTAL = 16 'Sohail (18 May 2013)
    ROUNDING_ADJUSTMENT = 17 'Sohail (21 Mar 2014)
    'Sohail (17 Sep 2014) -- Start
    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
    PAY_PER_ACTIVITY = 18
    PAY_PER_ACTIVITY_OT = 19
    PAY_PER_ACTIVITY_PH = 20
    'Sohail (17 Sep 2014) -- End
    'Sohail (18 Apr 2016) -- Start
    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
    NET_PAY_ROUNDING_ADJUSTMENT = 21
    'Sohail (18 Apr 2016) -- End
End Enum

Public Enum enComputeOn
    ComputeOnSpecifiedFormula = 1
    ComputeOnCurrentEarningsTotal = 2
    ComputeOnCurrentDeductionTotal = 3
End Enum

Public Enum enFunction
    Transaction_Head = 1
    Plus = 2
    Minus = 3
    Multiply = 4
    Divide = 5
    Open_Bracket = 6
    Close_Bracket = 7
    Leave_Type = 8
    DAILY_BASIC_SALARY = 9
    HOURLY_BASIC_SALARY = 10
    ON_HOLD_DAYS = 11
    CONSTANT_VALUE = 12
    OVER_TIME_HOURS = 13
    SHORT_HOURS = 14
    BASIC_SALARY = 15 'Sohail (14 Apr 2011)
    'Sohail (12 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    TOTAL_DAYS_IN_PERIOD = 16
    TOTAL_PAYABLE_DAYS = 17
    TOTAL_PAYABLE_HOURS = 18
    TOTAL_DAYS_WORKED = 19
    TOTAL_HOURS_WORKED = 20
    'Sohail (12 Mar 2012) -- End
    'Sohail (12 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    DAILY_BASIC_SALARY_WEEKENDS_INCLUDED = 21
    HOURLY_BASIC_SALARY_WEEKENDS_INCLUDED = 22
    'Sohail (12 Apr 2012) -- End
    CURRENCY_RATE = 23 'Sohail (03 Sep 2012)
    'Sohail (09 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    PAID_ISSUED_LEAVE_WITHIN_PERIOD = 24
    TOTAL_PAID_ISSUED_LEAVED = 25
    UNPAID_ISSUED_LEAVE_WITHIN_PERIOD = 26
    TOTAL_UNPAID_ISSUED_LEAVED = 27
    PAID_CANCELED_LEAVE_WITHIN_PERIOD = 28
    TOTAL_PAID_CANCELED_LEAVED = 29
    UNPAID_CANCELED_LEAVE_WITHIN_PERIOD = 30
    TOTAL_UNPAID_CANCELED_LEAVED = 31
    'Sohail (09 Oct 2012) -- End
    'Sohail (29 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    WEEKDAYS_WORKED_HOURS = 32
    WEEKDAYS_OVERTIME_HOURS = 33
    WEEKDAYS_SHORT_HOURS = 34
    WEEKDAYS_NIGHT_HOURS = 35
    WEEKEND_WORKED_HOURS = 36
    WEEKEND_OVERTIME_HOURS = 37
    WEEKEND_SHORT_HOURS = 38
    WEEKEND_NIGHT_HOURS = 39
    PUBLIC_HOLIDAY_WORKED_HOURS = 40
    PUBLIC_HOLIDAY_OVERTIME_HOURS = 41
    PUBLIC_HOLIDAY_SHORT_HOURS = 42
    PUBLIC_HOLIDAY_NIGHT_HOURS = 43
    'Sohail (29 Oct 2012) -- End
    TOTAL_PUBLIC_HOLIDAY_DAYS = 44 'Sohail (12 Nov 2012)
    'Sohail (21 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    UNIT_TOTAL_MEASUREMENT_WISE = 45
    UNIT_TOTAL_ACTIVITY_WISE = 46
    AMOUNT_TOTAL_MEASUREMENT_WISE = 47
    AMOUNT_TOTAL_ACTIVITY_WISE = 48
    'Sohail (21 Jun 2013) -- End
    'Sohail (10 Jul 2013) -- Start
    'TRA - ENHANCEMENT
    DAILY_BASIC_SALARY_ON_CONSTANT_DAYS = 49
    HOURLY_BASIC_SALARY_ON_CONSTANT_DAYS = 50
    'Sohail (10 Jul 2013) -- End
    'Sohail (24 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    IIF = 51
    YEAR_OF_SERVICE = 52
    OVER_TIME_2_HOURS = 53
    OVER_TIME_3_HOURS = 54
    OVER_TIME_4_HOURS = 55
    WEEKEND_OVERTIME_2_HOURS = 56
    WEEKEND_OVERTIME_3_HOURS = 57
    WEEKEND_OVERTIME_4_HOURS = 58
    WEEKDAYS_OVERTIME_2_HOURS = 59
    WEEKDAYS_OVERTIME_3_HOURS = 60
    WEEKDAYS_OVERTIME_4_HOURS = 61
    DAY_OFF_OVERTIME_1_HOURS = 62
    DAY_OFF_OVERTIME_2_HOURS = 63
    DAY_OFF_OVERTIME_3_HOURS = 64
    DAY_OFF_OVERTIME_4_HOURS = 65
    PUBLIC_HOLIDAY_OVERTIME_2_HOURS = 66
    PUBLIC_HOLIDAY_OVERTIME_3_HOURS = 67
    PUBLIC_HOLIDAY_OVERTIME_4_HOURS = 68
    MONTHS_OF_SERVICE = 69
    'Sohail (24 Sep 2013) -- End
    'Sohail (29 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    TOTAL_PROCESSED_LEAVE = 70
    TOTAL_UNPROCESSED_LEAVE = 71
    'Sohail (29 Oct 2013) -- End
    CUMULATIVE = 72 'Sohail (09 Nov 2013)
    'Sohail (21 Nov 2013) -- Start
    'ENHANCEMENT - OMAN
    DAYS_OF_SERVICE = 73
    TOTAL_YEAR_OF_SERVICE = 74
    TOTAL_MONTHS_OF_SERVICE = 75
    TOTAL_DAYS_OF_SERVICE = 76
    'Sohail (21 Nov 2013) -- End
    LEAVE_BALANCE = 77 'Sohail (10 Dec 2013)
    SALARY_INCREMENT_AMOUNT_WITHIN_PERIOD = 78 'Sohail (18 Nov 2013)
    'Sohail (24 Dec 2013) -- Start
    'Enhancement - Oman
    EMPLOYEE_TOTAL_DAYS_IN_PERIOD = 79
    'Sohail (24 Dec 2013) -- End
    FULL_BASIC_SALARY = 80 'Sohail (14 Mar 2014)
    'Sohail (18 Jun 2014) -- Start
    'Enhancement - TOTAL DAYS IN TnA and Leave PERIOD for Voltamp. 
    TOTAL_DAYS_IN_TNA_PERIOD = 81
    EMPLOYEE_TOTAL_DAYS_IN_TNA_PERIOD = 82
    'Sohail (18 Jun 2014) -- End
    'Sohail (17 Jul 2014) -- Start
    'Enhancement - FULL TOTAL PAYABLE DAYS and FULL TOTAL PAYABLE HOURS function in Transaction head formula.
    FULL_TOTAL_PAYABLE_DAYS = 83
    FULL_TOTAL_PAYABLE_HOURS = 84
    'Sohail (17 Jul 2014) -- End
    'Sohail (02 Sep 2014) -- Start
    'Enhancement - No. of Active and Inactive Dependants Function in Head Formula.
    NO_OF_ACTIVE_DEPENDANTS = 85
    NO_OF_INACTIVE_DEPENDANTS = 86
    'Sohail (02 Sep 2014) -- End
    'Sohail (09 Sep 2014) -- Start
    'Enhancement - OT and PH segregate function for PPA in formula.
    EARNING_ACTIVITY_WISE_TOTAL_BASE_UNIT = 87
    EARNING_ACTIVITY_WISE_TOTAL_OT_UNIT = 88
    EARNING_ACTIVITY_WISE_TOTAL_PH_UNIT = 89
    EARNING_ACTIVITY_WISE_TOTAL_BASE_AMOUNT = 90
    EARNING_ACTIVITY_WISE_TOTAL_OT_AMOUNT = 91
    EARNING_ACTIVITY_WISE_TOTAL_PH_AMOUNT = 92
    'Sohail (09 Sep 2014) -- End
    'Sohail (13 Sep 2014) -- Start
    'Enhancement - Make Activity selection Optional for OT and PH segregate function for PPA in formula.
    DEDUCTION_ACTIVITY_WISE_TOTAL_BASE_UNIT = 93
    DEDUCTION_ACTIVITY_WISE_TOTAL_OT_UNIT = 94
    DEDUCTION_ACTIVITY_WISE_TOTAL_PH_UNIT = 95
    DEDUCTION_ACTIVITY_WISE_TOTAL_BASE_AMOUNT = 96
    DEDUCTION_ACTIVITY_WISE_TOTAL_OT_AMOUNT = 97
    DEDUCTION_ACTIVITY_WISE_TOTAL_PH_AMOUNT = 98
    INFORMATIONAL_ACTIVITY_WISE_TOTAL_BASE_UNIT = 99
    INFORMATIONAL_ACTIVITY_WISE_TOTAL_OT_UNIT = 100
    INFORMATIONAL_ACTIVITY_WISE_TOTAL_PH_UNIT = 101
    INFORMATIONAL_ACTIVITY_WISE_TOTAL_BASE_AMOUNT = 102
    INFORMATIONAL_ACTIVITY_WISE_TOTAL_OT_AMOUNT = 103
    INFORMATIONAL_ACTIVITY_WISE_TOTAL_PH_AMOUNT = 104
    'Sohail (13 Sep 2014) -- End
    'Sohail (24 Sep 2014) -- Start
    'Enhancement - Weekend Days and Public Holiday Days function in transaction head formula.
    WEEKEND_DAYS_IN_PERIOD = 105
    PUBLIC_HOLIDAY_DAYS_IN_PERIOD = 106
    WEEKEND_DAYS_IN_TNA_PERIOD = 107
    PUBLIC_HOLIDAY_DAYS_IN_TNA_PERIOD = 108
    'Sohail (24 Sep 2014) -- End
    'Sohail (26 Sep 2014) -- Start
    'Enhancement - Weekend Hours and Public Holiday Hours function in transaction head formula.
    TOTAL_WORKING_HOURS_IN_PERIOD = 109
    TOTAL_WORKING_HOURS_IN_TNA_PERIOD = 110
    WEEKEND_HOURS_IN_PERIOD = 111
    PUBLIC_HOLIDAY_HOURS_IN_PERIOD = 112
    WEEKEND_HOURS_IN_TNA_PERIOD = 113
    PUBLIC_HOLIDAY_HOURS_IN_TNA_PERIOD = 114
    'Sohail (26 Sep 2014) -- End
    'Sohail (12 Nov 2014) -- Start
    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
    QUANTITY_TOTAL_FOR_LEAVE_CLAIM_REQUEST = 115
    AMOUNT_TOTAL_FOR_LEAVE_CLAIM_REQUEST = 116
    'Sohail (12 Nov 2014) -- End

    'Shani(21-Aug-2015) -- Start
    'Enhancement : Add new functions Quantity Total and Amount Total Functions of Claim & Request for Medical and Miscellaneous Category
    QUANTITY_TOTAL_FOR_MEDICAL_CLAIM_REQUEST = 117
    AMOUNT_TOTAL_FOR_MEDICAL_CLAIM_REQUEST = 118
    QUANTITY_TOTAL_FOR_MISCELLANEOUS_CLAIM_REQUEST = 119
    AMOUNT_TOTAL_FOR_MISCELLANEOUS_CLAIM_REQUEST = 120
    'Shani(21-Aug-2015) -- End
    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    TOTAL_HOURS_WORKED_IN_TNA_PERIOD = 121
    TOTAL_OVER_TIME_1_HOURS = 122
    TOTAL_OVER_TIME_2_HOURS = 123
    TOTAL_OVER_TIME_3_HOURS = 124
    TOTAL_OVER_TIME_4_HOURS = 125
    TOTAL_SHORT_HOURS = 126
    TOTAL_NIGHT_HOURS = 127
    TOTAL_SHORT_HOURS_DAYS = 128
    TOTAL_NIGHT_HOURS_DAYS = 129
    TOTAL_FULL_DAYS_WORKED_IN_TNA_PERIOD = 130
    TOTAL_HALF_DAYS_WORKED_IN_TNA_PERIOD = 131
    'Sohail (18 Jul 2017) -- End
    'Sohail (24 Aug 2017) -- Start
    'B5 Plus Enhancement - 69.1 - New functions to qualify  Total Days not worked in TnA Period.
    TOTAL_DAYS_NOT_WORKED_IN_TNA_PERIOD = 132
    'Sohail (24 Aug 2017) -- End
    'Sohail (24 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
    LOAN_FUNCTIONS = 133
    'Sohail (24 Oct 2017) -- End

    'S.SANDEEP [11-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#194|#ARUTI-82}
    BROUGHTFORWARD_LEAVE_DAYS = 134
    TOTAL_LEAVE_ACCRUED_DAYS = 135
    TOTAL_LEAVE_ACCRUED_DAYS_ASONDATE = 136
    TOTAL_LEAVE_ACCRUED_DAYS_WITHIN_MONTH = 137
    'S.SANDEEP [11-Apr-2018] -- END

    'Hemant (04 Mar 2019) -- Start
    'Internal # 32 : Provide "Modulus" function to finds the remainder after division of one number in transaction head formula.
    MODULUS = 138
    'Hemant (04 Mar 2019) -- End

    'Hemant (16 Apr 2019) -- Start
    'Ref # 3739 - 76.1 : Add payroll functions for late coming and early departure, same as all short hour functions.
    TOTAL_LATE_COMING_DAYS = 139
    TOTAL_EARLY_DEPARTURE_DAYS = 140
    'Hemant (16 Apr 2019) -- End
    'Sohail (30 Aug 2019) -- Start
    'NMB Payroll UAT # - 76.1 -  It should be possible to configure formula and pay year of service as per the    appointed month - New function "Is Appointed Month" to be given which will return 0/1 so user will match this function "Is Appointed Month" with 0 or 1 (constant value) 0 = False 1 = True
    IS_APPOINTED_MONTH = 141
    'Sohail (30 Aug 2019) -- End
    'Sohail (05 Dec 2019) -- Start
    'NMB UAT Enhancement # TC010 : System should be able to link OT module with payroll and compute special overtime (Holidays and weekend) and normal overtime respectively.
    TOTAL_OVER_TIME_REQUISITION_HOURS = 142
    TOTAL_OVER_TIME_REQUISITION_NIGHT_HOURS = 143
    'Sohail (05 Dec 2019) -- End
    'Sohail (02 Dec 2020) -- Start
    'NMB Enhancement : # OLD-215 : New Payroll Functions for Current Service Duration for Employee.
    TOTAL_YEARS_OF_CURRENT_SERVICE = 144
    TOTAL_MONTHS_OF_CURRENT_SERVICE = 145
    TOTAL_DAYS_OF_CURRENT_SERVICE = 146
    'Sohail (02 Dec 2020) -- End
    'Sohail (10 Sep 2021) -- Start
    'Bio Corn Oil Enhancement : OLD-471 : New Payroll function to pass late coming Hours from Attendance Module.
    TOTAL_LATE_COMING_HOURS = 147
    TOTAL_EARLY_DEPARTURE_HOURS = 148
    'Sohail (10 Sep 2021) -- End

End Enum

'Sohail (18 Jul 2017) -- Start
'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
Public Enum enSubFunctionDays
    ALL = 0
    TOTAL_WEEK_DAYS = 1
    TOTAL_WEEKEND_DAYS = 2
    TOTAL_PUBLIC_HOLIDAY_DAYS = 3
    TOTAL_DAY_OFF_DAYS = 4
    TOTAL_SUNDAYS = 5
    TOTAL_MONDAYS = 6
    TOTAL_TUESDAYS = 7
    TOTAL_WEDNESDAYS = 8
    TOTAL_THURSDAYS = 9
    TOTAL_FRIDAYS = 10
    TOTAL_SATURDAYS = 11
End Enum

Public Enum enSubFunctionWorked
    ALL = 0
    ON_WEEK_DAYS = 1
    ON_WEEKEND_DAYS = 2
    ON_PUBLIC_HOLIDAY_DAYS = 3
    ON_DAY_OFF_DAYS = 4
    ON_SUNDAYS = 5
    ON_MONDAYS = 6
    ON_TUESDAYS = 7
    ON_WEDNESDAYS = 8
    ON_THURSDAYS = 9
    ON_FRIDAYS = 10
    ON_SATURDAYS = 11
End Enum

Public Enum enWeekDayID
    SUNDAY = 0
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6
End Enum
'Sohail (18 Jul 2017) -- End

'Sohail (24 Oct 2017) -- Start
'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
Public Enum enSubFunctionLoan
    EMI_AMOUNT = 1
    PRINCIPAL_AMOUNT = 2
    INTEREST_AMOUNT = 3
    TOTAL_LOAN_AMOUNT = 4
    REMAINING_PRINCIPAL_BALANCE = 5
    'Sohail (28 May 2020) -- Start
    'TUJIJENGE Enhancement # 0004707 : Enhancement- Assist to make the loan interest remaining balance to appear on payslip,Either by Adding the loan interest on loan Functions so we can get loan interest remaining balance by computation..
    REMAINING_INTEREST_BALANCE = 6
    'Sohail (28 May 2020) -- End
End Enum
'Sohail (24 Oct 2017) -- End

Public Enum enPaymentBy
    Value = 1
    Percentage = 2
    Fromula = 3  'Sohail (24 Sep 2013)
End Enum

Public Enum enAddDeduct
    Add = 1
    Deduct = 2
    Do_Nothing = 3
End Enum
'Sohail (16 Oct 2010) -- End

'Sohail (16 May 2012) -- Start
'TRA - ENHANCEMENT
Public Enum enTranHeadFormula_DefaultValue
    COMPUTED_VALUE = 0
    ZERO = 1
    ONE = 2
End Enum
'Sohail (16 May 2012) -- End

'Sohail (16 May 2012) -- Start
'TRA - ENHANCEMENT
Public Enum enLogoOnPayslip
    LOGO_AND_COMPANY_DETAILS = 1
    COMPANY_DETAILS = 2
    LOGO = 3
End Enum
'Sohail (16 May 2012) -- End

Public Enum enPrintAction
    None = 0
    Preview = 1
    Print = 2
End Enum

Public Enum enExportAction
    None = 0
    RichText = 2
    Word = 3
    Excel = 4
    PDF = 5
    HTML = 6
    ExcelExtra = 7 'Sohail (08 Dec 2012)
    ExcelHTML = 8 'Sohail (29 Apr 2014)
    ExcelDataOnly = 9 'Pinkal (29-May-2018) -- 'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.

    'Hemant (24 Dec 2020) -- Start
    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
    ExcelXLSX = 10
    'Hemant (24 Dec 2020) -- End

    'S.SANDEEP |18-JUN-2021| -- START
    'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
    ExcelCSV = 11
    'S.SANDEEP |18-JUN-2021| -- END

    'S.SANDEEP |28-JUL-2021| -- START
    ExcelXLSM = 12
    'S.SANDEEP |28-JUL-2021| -- END
End Enum


Public Enum enArutiReport
    PayrollReport = 1
    PayrollSummary = 2
    EmployeeHeadCount = 3
    EmployeeBankRegisterList = 4
    PaySlip = 5
    BankPaymentList = 6
    PensionFundReport = 7
    BankSummary = 8
    EDSpreadSheet = 9
    PayrollVariance = 10
    PayrollTotalVariance = 11
    EmployeeLeaveAbsenceSummary = 12
    EmployeeLeaveBalanceList = 13
    IncomeTaxDeductionP9 = 14
    IncomeTaxFormP10 = 15
    PPFContribution = 16
    LoanAdvanceSaving = 17
    Employee_Skills = 18
    Employee_Beneficairy_Report = 19
    Employee_Qualification_Report = 20
    Employee_Referee_Report = 21
    JournalVoucherLedger = 22
    TrainingCostReport = 23
    DailyTimeSheet = 24 'Vimal (21 Dec 2010) -- Start 
    TrainingListReport = 25 'Sandeep | 04 JAN 2010 | -- Start -- END
    TrainingBudgetReport = 26 'Sandeep | 04 JAN 2010 | -- Start -- END
    CoinageAnalysisReport = 27 'Anjan (22 Jan 2011)
    EmployeeAgeAnalysis = 28 'Sandeep | 04 JAN 2010 | -- Start -- END
    EmployeeDistributionReport = 29 'Sandeep | 04 JAN 2010 | -- Start -- END
    EmployeeMonthlyPhysicalReport = 30 'Sandeep | 04 JAN 2010 | -- Start -- END
    EmployeeAssessmentListReport = 31 'Sandeep | 04 JAN 2010 | -- START -- END
    EmployeeAssessmentFormReport = 32 'Sandeep | 04 JAN 2010 | -- START -- END
    EmployeeBiodataReport = 33 'Anjan (4 Feb 2011)
    EmployeeRelationReport = 34 'Sandeep [ 10 FEB 2011 ] -- Start -- End
    TrainingAnalysisReport = 35 'Sandeep [ 10 FEB 2011 ] -- Start -- End
    Training_Void_CancelReport = 36 'Sandeep [ 10 FEB 2011 ] -- Start -- End
    TrainingEnroll_Void_CancelReport = 37 'Sandeep [ 10 FEB 2011 ] -- Start -- End
    EmployeeTimeSheet = 38 'Pinkal (24-Jan-2011) -- Start -- END
    EmployeeShortTime_OverTime = 39 'Pinkal (24-Jan-2011) -- Start -- END
    LeaveIssue_AT_Report = 40 'Pinkal (16-Feb-2011) -- Start -- END
    LeaveBalance_AT_Report = 41 'Pinkal (16-Feb-2011) -- Start -- END
    Timesheet_AT_Report = 42 'Pinkal (16-Feb-2011) -- Start -- END
    'LeaveApprover_AT_Report = 43 'Pinkal (16-Feb-2011) -- Start -- END
    ATProcessPendingLAReport = 43 'Sandeep [ 01 MARCH 2011 ] -- Start -- End
    ATLoanAdvanceReport = 44 'Sandeep [ 01 MARCH 2011 ] -- Start -- End
    NSSF_FORM_5 = 45 'Sandeep [ 01 MARCH 2011 ] -- Start -- End
    ATPaymentTranReport = 46 'Sandeep [ 01 MARCH 2011 ] -- Start -- End
    JobEmploymentHistory = 47 'Anjan (04 Apr 2011)-Start
    CashPaymentList = 48 'Anjan (04 Apr 2011)-Start
    SDL_Statutory_Report = 49
    ATSavingTranReport = 50 'Pinkal (20-Apr-2011)-Start 
    EmployeeAssetsRegister = 51 ' Anjan (21 May 2011)-Start
    Vacancy_Report = 52   'Sandeep [ 02 JUNE 2011 ]  -- START -- END
    Batch_Schedule_Report = 53 'Sandeep [ 02 JUNE 2011 ]  -- START -- END
    Interview_Analysis_Report = 54 'Sandeep [ 02 JUNE 2011 ]  -- START -- END
    Exempt_Head_Report = 55 'Sandeep [ 02 JUNE 2011 ]  -- START -- END
    Employee_Movement_Report = 56 'Pinkal (15-Jun-2011)-Start
    DisciplineAnalysisReport = 57 'Pinkal (15-Jun-2011)-Start
    Employee_Listing = 58 'Pinkal (15-Jun-2011)-Start 
    TRAMedical_Claim_Report = 59 'Pinkal (07-Jan-2012)
    EDDetail_Report = 60 ' Pinkal (20-Jan-2012)
    EmployeeAssessSoreRatingReport = 61 'S.SANDEEP [ 04 FEB 2012 ] -- START -- END
    AppraisalSummaryReport = 62 'S.SANDEEP [ 04 FEB 2012 ] -- START -- END
    TrainingNeedsPriorityForm = 63 'Sohail (15 Feb 2012)
    EmployeeRetirementReport = 64
    EmployeeWithNoDOB = 65 ' Pinkal (22-Mar-2012)
    EmployeePersonalParticular = 66 'S.SANDEEP [ 24 MARCH 2012 ] -- START -- END
    EmployeeCVReport = 67 'S.SANDEEP [ 24 MARCH 2012 ] -- START -- END
    NewRecruitedEmployee = 68 ' Pinkal (22-Mar-2012)
    TerminatedEmployeeWithReason = 69 ' Pinkal (22-Mar-2012)
    EmployeeStaffList = 70 'S.SANDEEP [ 24 MARCH 2012 ] -- START -- END
    ApplicantQualification = 71 ' Pinkal (22-Mar-2012)
    Training_Planning_Report = 72 'S.SANDEEP [ 02 APRIL 2012 ] -- START -- END
    LAPFContribution = 73 'Sohail (02 Apr 2012)
    'Anjan (20 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    PSPFContribution = 74
    'Anjan (20 Mar 2012)-End 
    EmployeeWorkingStation = 75  'S.SANDEEP [ 04 APRIL 2012 ] -- START -- END
    Current_SalaryGradesReport = 76 'S.SANDEEP [ 04 APRIL 2012 ] -- START -- END
    OverDeduction_NetPay_Report = 77 'S.SANDEEP [ 04 APRIL 2012 ] -- START -- END
    ManningLevel_Report = 78 'S.SANDEEP [ 04 APRIL 2012 ] -- START -- END
    Performance_AppraisalReport = 79 'S.SANDEEP [ 02 APRIL 2012 ] -- START -- END
    'Anjan (20 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    GEPFContribution = 80
    'Anjan (20 Mar 2012)-End 
    Employee_without_Memebership = 81 'S.SANDEEP [ 06 APRIL 2012 ] -- START -- END
    EmployeeWithoutED = 82 'Sohail (06 Apr 2012)
    Employee_ListLocation_Report = 83 'S.SANDEEP [ 06 APRIL 2012 ] -- START -- END
    Applicant_Other_Criteria_Report = 84 'S.SANDEEP [ 06 APRIL 2012 ] -- START -- END
    Discipline_Status_Summary_Report = 85 'S.SANDEEP [ 06 APRIL 2012 ] -- START -- END
    EmployeeDispositionReport = 86 'Sohail (06 Apr 2012)
    EmployeeWithoutBank = 87 'Anjan (09 Mar 2012)
    External_Discipline_Cases = 88 'S.SANDEEP [ 20 APRIL 2012 ] -- START -- END
    BBL_Loan_Report = 89 'S.SANDEEP [ 07 MAY 2012 ] -- START -- END
    ApplicantCVReport = 90 'S.SANDEEP [ 07 MAY 2012 ] -- START -- END
    ShortListed_Applicant = 91  'Pinkal (11-MAY-2012) -- Start
    EmployeeLeaveForm = 92   'Pinkal (15-MAY-2012) -- Start 
    EOCEmployee = 93   'Pinkal (28-May-2012)--- Start
    DisciplinePendingCasesDetail = 94   'Pinkal (03-Jun-2012)--- Start
    EmployeeWithoutEmail = 95   'Pinkal (03-Jun-2012)--- Start
    ApplicantVacancyReport = 96 'S.SANDEEP [ 04 JULY 2012 ] -- START -- END
    Employee_Salary_Change_Report = 97 'S.SANDEEP [ 12 JULY 2012 ] -- START -- END
    Employee_WithSameName = 98 'S.SANDEEP [ 17 JULY 2012 ] -- START -- END
    Employee_SameBankAccount = 99   'Pinkal (21-Jul-2012) -- Start
    ZSSF_FORM = 100 'Anjan (25 Jul 2012) - start
    EmployeeWithoutLeaveApprover = 101  'Pinkal (10-Sep-2012) -- Start
    PayrollHeadCount = 102 'Sohail (08 Nov 2012)
    EmployeeDetail_WithED = 103  'Pinkal (11-Dec-2012) --- Start
    MedicalBillSummaryReport = 104 'Pinkal (14-Dec-2012) -- Start
    LeaveApproverReport = 105 'Pinkal (18-Dec-2012) -- Start
    Orbit = 106 'Sohail (08 Nov 2012)
    EmployeewithMultipleMembership = 107 'Anjan (08 Jan 2013)
    LeaveStatementReport = 108 'Pinkal (18-Jan-2013)
    LoanAdvanceListReport = 109 'S.SANDEEP [ 25 JAN 2013 ] -- START -- END
    EmployeeDurationReport = 110 'Pinkal (24-Jan-2013)
    EndofProbation = 111 'Pinkal (28-Jan-2013)
    'Sohail (07 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    Asset_Declaration_Report = 112
    Asset_Declaration_Category_Wise = 113
    'Sohail (07 Feb 2013) -- End
    Discipline_CaseStatus_Report = 114 'S.SANDEEP [ 09 MAR 2013 ] -- START -- END
    EmployeeWithOutImage = 115 'Pinkal (24-Apr-2013)
    EmployeeWithOutGender = 116 'Pinkal (24-Apr-2013)
    BSC_Planning_Report = 117 'Sohail (30 Apr 2013)
    Tax_Report = 118 'Sohail (18 May 2013)
    'CompanyActiveEmployeeReport = 119 'Pinkal (30-Apr-2013)
    ActivityDoneReport = 119
    ActivityTimesheetReport = 120
    EmployeeIdentityReport = 121
    EmployeeMembershipReport = 122 'Sohail (03 Aug 2013)
    JobReport = 123 'Sohail (07 Aug 2013)
    'Sohail (12 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    P9A_Report = 124
    P10_Report = 125
    PAYE_P11_Report = 126
    'Sohail (12 Aug 2013) -- End
    Za_ITF_P16_Report = 127 'S.SANDEEP [ 09 JULY 2013 ] -- START -- END
    Za_Monthly_AMD_Report = 128 'S.SANDEEP [ 09 JULY 2013 ] -- START -- END
    Za_AnnualPayeReturn = 129 'S.SANDEEP [ 09 JULY 2013 ] -- START -- END
    Za_RemittanceForm = 130 'S.SANDEEP [ 09 JULY 2013 ] -- START -- END
    KN_NHIFReport = 131    'Pinkal (18-Aug-2013) -- Start
    Performance_Evaluation_Report = 132 'S.SANDEEP [ 14 AUG 2013 ] -- START -- END
    Not_Asssessd_Employee_Report = 133 'S.SANDEEP [ 26 SEPT 2013 ] -- START -- END
    Attendance_Summary_Report = 134 'S.SANDEEP [ 26 SEPT 2013 ] -- START -- END
    Holiday_Attendance_Report = 135  'Pinkal (15-Nov-2013) -- Start
    Gratuity_Report = 136 'Sohail (21 Nov 2013)
    LeavePlanReport = 137    'Pinkal (30-Nov-2013) -- Start
    Medical_Monthly_Payment = 138 'S.SANDEEP [ 30 NOV 2013 ] -- START -- END
    EDDetail_PeriodWise_Report = 139
    End_Of_Service_Report = 140 'Sohail (10 Dec 2013)
    Movement_Report = 141 'S.SANDEEP [ 26 DEC 2013 ] -- START -- END
    Employee_Schedule_Report = 142 'S.SANDEEP [ 09 JAN 2014 ] -- START -- END
    Pay_Per_Activity_Report = 143  'Pinkal (06-Mar-2014) -- Start  -- End
    'Sohail (15 Mar 2014) -- Start
    'Enhancement - Cost Center Report Branch Wise & Department Wise.
    CostCenter_BranchWise_Report = 144
    CostCenter_DepartmentWise_Report = 145
    DepartmentGrp_Branchwise_Report = 146
    DepartmentGrp_General_Report = 147
    'Sohail (15 Mar 2014) -- End
    Payment_Journal_Voucher_Report = 148 'Sohail (21 Mar 2014)
    Leave_Form_Approval_Status_Report = 149  'Pinkal (25-Apr-2014) -- Start
    Issued_Leave_Form_Detail_Report = 150  'Pinkal (25-Apr-2014) -- Start
    Long_Service_Award_Report = 151 'Sohail (23 May 2014)
    EPF_Contribution_Report = 152 'Sohail (10 May 2014)
    Employee_With_Out_Report = 153 'S.SANDEEP [ 01 JUL 2014 ] -- START -- END
    ITax_Form_B_Report = 154 'Sohail (26 Jul 2014)
    Leave_Liability_Report = 155 'Pinkal (04-Aug-2014) -- Start  'Enhancement - NEW Leave Report FOR Zanzibar Residence 
    EPF_PeriodWise_Report = 156 'Sohail (12 Aug 2014)
    Monthly_Tax_Income_Report = 157 'Sohail (28 Aug 2014)
    Staff_Movement_Report = 158    'Pinkal (19-Sep-2014) -- Start  'Enhancement -  DEVEPLOING NEW MEDICAL REPORTS FOR FINCA CONGO(FDRC)
    Employee_Medical_Cover_Summary = 159    'Pinkal (19-Sep-2014) -- Start  'Enhancement -  DEVEPLOING NEW MEDICAL REPORTS FOR FINCA CONGO(FDRC)
    Employee_Medical_Cover_Detail = 160 'S.SANDEEP [ 06 OCT 2014 ] -- START -- END
    Salary_Reconciliation_Report = 161 'Sohail (07 Oct 2014) 
    Termination_Package_Report = 162 'Sohail (20 Oct 2014)
    EDDetail_Bank_Account_Report = 163 'Sohail (03 Dec 2014)
    Employee_Salary_On_Hold_Report = 164 'Sohail (05 Dec 2014)
    Air_Passage_Report = 165 'Pinkal (06-Mar-2014) -- Start
    HELB_REPORT = 166 'Sohail (06 Apr 2015)
    DETAILED_SALARY_BREAKDOWN_REPORT = 167 'Sohail (09 Apr 2015) 
    MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT = 168 'Shani (03 Jun 2015) 
    INCOME_TAX_DIVISION_P12_REPORT = 169 'Sohail (05 Jun 2015)
    WCF_REPORT = 170 'Pinkal (21 Jul 2015)
    Employee_Breaktime_Timesheet = 171   'Pinkal (06-Aug-2015) -- Enhancement - WORKING ON CITIONE TNA CHANGES REQUIRED BY RUTTA.
    CR_Summary_Report = 172 'Shani(08-Aug-2015)
    Daily_Attendance_Checking_Report = 173      'Pinkal (14-Oct-2015) 
    ITax_Form_J_Report = 174 'Sohail (30 Oct 2015)
    Employee_Status_Report = 175 'Shani(02-Dec-2015) -- Start
    'Sohail (07 Dec 2015) -- Start
    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
    P10A_P10D_Report = 176
    'Sohail (07 Dec 2015) -- End
    UserDetail_Auditors_Report = 177 'Anjan (27 Jan 2017) Start
    Employee_Seniority_Report = 178  'Shani(01-APR-2016) 'Enhancement : New Seniority Level Report For TNP/KBC
    Employee_Claim_Balance_Report = 179 'Pinkal (07-Jun-2016) --  'Enhancement - IMPLEMENTING EMPLOYEE CLAIM BALANCE LIST REPORT  FOR CCK.
    Late_Arrival_Report = 180    'Pinkal (06-Jun-2016) --  'Enhancement - IMPLEMENTING LATE ARRIVAL AND EARLY GOING REPORT FOR ASP.
    Salary_Budget_Breakdown_Report = 181 'Sohail (07 Jun 2016)
    Salary_Budget_Breakdown_By_Period_Report = 182 'Sohail (02 Sep 2016)
    SUN_Account_Project_JV_Report = 183 'Sohail (01 Oct 2016)
    Employee_List_Assessor_Reviewer_Report = 184 'Shani (24-Oct-2016) -- Start
    'S.SANDEEP [13-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
    Training_BasedOn_Report = 185
    'S.SANDEEP [13-FEB-2017] -- END
    Custom_Item_Value_Report = 186 'Shani(14-FEB-2017) -- Requrement by CCK
    DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP = 187 'Sohail (20 Feb 2017)
    Emplyoee_Project_Code_Timesheet_Report = 188 'Pinkal (16-Feb-2017) --  Working on Employee Budget Timesheet Report for Aga Khan.
    EmployeeMonthlyPayrollReport = 189 'S.SANDEEP [28-FEB-2017] -- START -- CCBRT, MONTHLY PAYROLL REPORT -- END
    PE_Form_Report = 190 'S.SANDEEP [22-MAR-2017] -- START { TNP }-- END
    'Nilay (11 Apr 2017) -- Start
    'Enhancements: New statutory report iTax Form C for CCK
    ITax_Form_C_Report = 191
    'Nilay (11 Apr 2017) -- End


    'Pinkal (22-Apr-2017) -- Start
    'Enhancement - Creating Timesheet Project Summary Report For PSI & AGA Khan .
    Employee_Timesheet_Project_Summary_Report = 192
    'Pinkal (22-Apr-2017) -- End

    'Pinkal (31-Jul-2017) -- Start
    'Enhancement -Create Timesheet Submission Report for THPS .
    Employee_Timesheet_Submission_Report = 193
    'Pinkal (31-Jul-2017) -- End

    'Sohail (02 Aug 2017) -- Start
    'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
    Monthly_Employee_Budget_Variance_Analysis_Report = 194
    'Sohail (02 Aug 2017) -- End

    'S.SANDEEP [05-OCT-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 80
    AssessmentDoneReport = 195
    'S.SANDEEP [05-OCT-2017] -- END

    'S.SANDEEP [11-OCT-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 78
    StaffRequisitionFormReport = 196
    'S.SANDEEP [11-OCT-2017] -- END

    'S.SANDEEP [13-OCT-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 81,82
    Mauritius_PAYE_Report = 197
    Mauritius_NPF_Report = 198
    'S.SANDEEP [13-OCT-2017] -- END

    'Sohail (10 Nov 2017) -- Start
    'Enhancement - 70.1 - Ref. No. 128 - report to describe the budget plan, which will split the budget to amounts depending to project codes %s. this report is like the one we already have "Monthly employee Budget Variance report". the only difference is it isn't having the actual column and the variance column and it is distributing budget not the actual amount..
    Monthly_Finance_Plan_Report = 199
    'Sohail (10 Nov 2017) -- End

    'Sohail (20 Dec 2017) -- Start
    'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
    NHIF_Report_TZ = 200
    'Sohail (20 Dec 2017) -- End

    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 102
    Assessment_Acknowledgment_Report = 201
    'S.SANDEEP [29-NOV-2017] -- END


    'Pinkal (23-Feb-2018) -- Start
    'Enhancement - Working on B5 Customize Medical Reports.
    B5_Advance_Statement_Report = 202
    B5_Payment_Voucher_Report = 203
    'Pinkal (23-Feb-2018) -- End
    'Sohail (29 Mar 2018) -- Start
    'MARE ANGUILLES FARMS LTD Mauritius Enhancement : Ref. No. 186 - Combined Mauritius Statutory -PAYE_NPF Report in 71.1.
    PAYE_NPF_NSF_Report = 204
    'Sohail (29 Mar 2018) -- End
    'Sohail (18 Apr 2018) -- Start
    'Malawi Enhancement - Ref # 211 : New statutory report for Malawi - Form P.9 shows annual gross incomes and taxable income for employee in 72.1.
    PAYE_DEDUCTION_FORM_P9_Report = 205
    'Sohail (18 Apr 2018) -- End

    'Gajanan (04 May 2018) -- Start
    'Malawi Enhancement - Ref # 212 : New statutory report for Malawi - TEVET Levy Report shows period wise gross emoluments for all staff and the corresponding levy applied on them for employee in 72.1.
    TEVET_LEVY_REPORT = 206
    'Gajanan (04 May 2018) -- End

    'Pinkal (12-May-2018) -- Start
    'Enhancement - Ref # 213 Certificate of Employee Leaving to show gross emoluments and tax paid by employee for the period of employment during the year.
    Employee_Leaving_Certificate = 207
    'Pinkal (12-May-2018) -- End

    'Pinkal (12-May-2018) -- Start
    'Enhancement - Ref # 213 Certificate of Employee Leaving to show gross emoluments and tax paid by employee for the period of employment during the year.
    Employee_Claim_Form = 208
    'Pinkal (12-May-2018) -- End


    'Gajanan [13-AUG-2018] -- Start
    'Enhancement - Implementing Grievance Module.
    Grievance_Detail_Report = 209
    'Gajanan(13-AUG-2018) -- End
    'Hemant (21 Nov 2018) -- Start
    'Enhancement : New Statutory Report for (Public Service Social Security Fund) PSSSF for Tanazania in 75.1.
    PSSSF_Report = 210
    'Hemant (21 Nov 2018) -- End

    'Hemant (26 Nov 2018) -- Start
    'Enhancement : Changes As per Rutta Request for UAT of Asset Declaration Template 2
    Asset_Declaration_Status_Report = 211
    'Hemant (26 Nov 2018) -- End

    'S.SANDEEP |12-APR-2019| -- START
    'ENHANCEMENT : ASSESSMENT AUDIT REPORT
    Assessment_Audit_Report = 212
    'S.SANDEEP |12-APR-2019| -- END


    'Pinkal (31-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    Employee_Expense_Report = 213
    'Pinkal (31-May-2019) -- End


    'Pinkal (13-Jun-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    Leave_Application_Report = 214
    'Pinkal (13-Jun-2019) -- End

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Assessment_Calibration_Report = 215
    'S.SANDEEP |25-OCT-2019| -- END


    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    Grievance_Summary_Reports = 216
    'Gajanan [24-OCT-2019] -- End


    'Pinkal (15-Jan-2020) -- Start
    'Enhancements -  Working on OT Requisistion Reports for NMB.
    OTRequisition_Report = 217
    'Pinkal (15-Jan-2020) -- End

    'Sohail (04 Feb 2020) -- Start
    'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
    Confidentiality_Acknowledgement_Report = 218
    'Sohail (04 Feb 2020) -- End

    'Pinkal (29-Jan-2020) -- Start
    'Enhancement - Changes related To OT NMB Testing.
    OTRequisition_Summary_Report = 219
    Employee_OTRequisition_Report = 220
    'Pinkal (29-Jan-2020) -- End


    'Pinkal (07-Mar-2020) -- Start
    'Enhancement - Changes Related to Payroll UAT for NMB.
    Justification_Report = 221
    'Pinkal (07-Mar-2020) -- End

    'S.SANDEEP |25-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : NEW DISCIPLINE REPORT 
    DisciplineCaseDetailReport = 222
    DisciplineOutcomeReport = 223
    DisciplineAppealReport = 224
    'S.SANDEEP |25-MAR-2020| -- END

'Pinkal (16-May-2020) -- Start
    'Enhancement NMB Leave Report Changes -   Working on Leave Reports required by NMB.
    Leave_Absence_DetailedReport = 225
    Leave_Absence_SummaryReport = 226
    'Pinkal (16-May-2020) -- End

    'Gajanan [18-May-2020] -- Start
    'Enhancement:Discipline Module Enhancement NMB
    DisciplineSuspensionReport = 227
    'Gajanan [18-May-2020] -- End

    'Sohail (18 May 2020) -- Start
    'NMB Enhancement # : New report to preview Employee Non-Disclosure Declaration form in ESS and MSS.
    Employee_NonDisclosure_Declaration_Report = 228
    'Sohail (18 May 2020) -- End

    'Hemant (03 Jun 2020) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 3 : A Interview Score report showing interview candidate(s) and the scores of each interviewer to be developed)
    Interview_Score_Report = 229
    'Hemant (03 Jun 2020) -- End

    

    'Pinkal (27-Jun-2020) -- Start
    'Enhancement NMB -   Working on Employee Signature Report.
    Employee_Signature_Report = 230
    'Pinkal (27-Jun-2020) -- End

    'Gajanan [27-June-2020] -- Start
    'Enhancement:Create AT-Non Disclosure Declaration Report
    ATNonDisclosureDeclaration_Report = 231
    'Gajanan [27-June-2020] -- End


    'Pinkal (26-Jun-2020) -- Start
    'Ifakara Enhancement [3011] - Creating Employee Project Description Report in budget timesheet.
    Employee_Project_Description_Report = 232
    'Pinkal (26-Jun-2020) -- End


    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Calibrator Approver Report
    Employee_Calibrator_Approver_Report = 233
    'Pinkal (16-Jul-2020) -- End

    'Hemant (28 Aug 2020) -- Start
    'Enhancement : New Statutory Report for Skills and Development Levy Monthly Return & Statement and Payment of Tax Withheld for Employees for Tanazania
    Skills_And_Development_Levy_Monthly_Return = 234
    Statement_And_Payment_Of_Tax_Withheld_For_Employees = 235
    'Hemant (28 Aug 2020) -- End


    'Pinkal (20-Nov-2020) -- Start
    'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
    FuelConsumption_Report = 236
    'Pinkal (20-Nov-2020) -- End

    'S.SANDEEP |23-JAN-2021| -- START
    'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
    Disciplinary_Penalty_Report = 237
    'S.SANDEEP |23-JAN-2021| -- END
    'Sohail (23 Jan 2021) -- Start
    'Netis Gabon Enhancement : - New statutory report CFP Report.
    CFP_Report = 238
    'Sohail (23 Jan 2021) -- End
    'Sohail (01 Feb 2021) -- Start
    'Netis Gabon Enhancement : OLD-269 : New statutory report IRPP FNH CFP Report.
    IRPP_FNH_CFP_Report = 239
    'Sohail (01 Feb 2021) -- End


    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    Imprest_Balance_Report = 240
    'Pinkal (10-Feb-2021) -- End

    'S.SANDEEP |11-MAR-2021| -- START
    'ISSUE/ENHANCEMENT : Talent & Succession Report 
    Talent_Screening_Status_Report = 241
    Succession_Screening_Status_Report = 242
    'S.SANDEEP |11-MAR-2021| -- END

    'Hemant (22 Mar 2021) -- Start
    'ISSUE/ENHANCEMENT : Talent & Succession Reports
    Talent_Active_Approved_with_Screener_Report = 243
    Talent_Ratio_Of_Active_Staff_Report = 244
    Talent_Approved_Rejected_Report = 245
    Talent_Exits_Report = 246
    Succession_Ratios_Report = 247
    Succession_List_Report = 248
    Succession_Cross_Functional_Nominations = 249
    Succession_Exits_Report = 250
    'Hemant (22 Mar 2021) -- End

    'Pinkal (30-Mar-2021)-- Start
    'NMB Enhancement  -  Working on Employee Recategorization history Report.
    Emp_RecategorizationHistory_Report = 251
    Emp_TransferHistory_Report = 252
    'Pinkal (30-Mar-2021) -- End

    'Gajanan [18-May-2021] -- Start
    PDP_Status_Report = 253
    PDP_Development_Goals_Report = 254
    PDP_Preferred_Move_Report = 255
    PDP_Development_Areas_Report = 256
    PDP_Action_Plan_Progress_Status_Report = 257
    'Gajanan [18-May-2021] -- End

    'Hemant (18 May 2021) -- Start
    'ISSUE/ENHANCEMENT : NMB Training Reports
    Executed_Trainings_Report = 258
    Executed_Trainings_Detailed_Report = 259
    Training_Calendar_Report = 260
    Trainings_Covered_Proportion_Report = 261
    Staff_Trainings_Attended_Report = 262
    'Hemant (18 May 2021) -- End
    'Sohail (20 May 2021) -- Start
    'NMB Enhancement : : New Report "Training Priority Report" in training module.".
    Training_Priority_Report = 263
    'Sohail (20 May 2021) -- End

    'Pinkal (21-May-2021) -- Start
    'Enhancement On Training Reports -   Working on  NMB Training Reports.
    Planned_Training_Report = 264
    'Pinkal (21-May-2021) -- End

'S.SANDEEP |19-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES REPORTS
    Staff_Cmpts_Parameter_Report = 265
    Job_Family_Competencies_Report = 266
    Competence_Assessment_Form_Report = 267
    Competence_Score_Analysis_Report = 268
    Competence_Training_Intervension_Report = 269
    'S.SANDEEP |19-MAY-2021| -- END
    National_Health_Insurance_Report = 270 'Sohail (30 Jun 2021)
    'S.SANDEEP |01-JUL-2021| -- START
    Assessment_Progress_Update = 271
    'S.SANDEEP |01-JUL-2021| -- END
    Approved_CR_Summary_Report = 272 'Pinkal (25-Aug-2021)--KBC Enhancement : Creating Approved Claim Summary Report in Desktop and MSS.
    'Hemant (04 Sep 2021) -- Start
    'ENHANCEMENT : OLD-445 - New Report - Training Feedback Status Report.
    Training_Feedback_Status_Report = 273
    'Hemant (04 Sep 2021) -- End
    'Hemant (13 Sep 2021) -- Start
    'ENHANCEMENT : OLD-446 - New Report - Training Feedback Form.
    Training_Feedback_Form_Report = 274
    'Hemant (13 Sep 2021) -- End
    'Hemant (21 Sep 2021) -- Start
    'ENHANCEMENT : OLD-467 - Training Status Summary Report.
    Training_Status_Summary_Report = 275
    'Hemant (21 Sep 2021) -- End

 'Gajanan [16-Sep-2021] -- Start
    'ENHANCEMENT: OLD-476
    Disciplinary_Charges_Detailed_Report = 276
    'Gajanan [16-Sep-2021] -- End

    'Pinkal (04-Oct-2021)-- Start
    'Leave Planner Repor Enhancement : Creating Planned Leave Report. 
    Planned_Leave_Summary_Report = 277
    'Pinkal (04-Oct-2021) -- End

    'Sohail (27 Oct 2021) -- Start
    'Gabon Enhancement : OLD-474 : New statutory Rapport Trimestriel CNAMGS report for Gabon.
    Rapport_Trimestriel_CNAMGS = 278
    'Sohail (27 Oct 2021) -- End

    'Hemant (22 Nov 2021) -- Start
    'ENHANCEMENT : OLD-490(Finca Uganda) - New Report: Training Budget Report.
    Training_Budget_Spreadsheet = 279
    'Hemant (22 Nov 2021) -- End
    'Sohail (29 Nov 2021) -- Start
    'Enhancement : OLD-478 : Kenya - New statutory report - NITA for Kenya only.
    NITA_Report = 280
    'Sohail (29 Nov 2021) -- End
    'Pinkal (29-Nov-2021)-- Start
    'Netis Gabon Statutory Report.
    Gabon_Declaration_Trimestrielle_Salaires = 281
    'Pinkal (29-Nov-2021) -- End
    'Sohail (03 Dec 2021) -- Start
    'Enhancement : OLD-522 : Netis Gabon - New Statutory Report ID20 ETAT DE LA MASSE SALARIALE for Gabon.
    ID20_ETAT_DE_LA_MASSE_SALARIALE_Report = 282
    'Sohail (03 Dec 2021) -- End
    'Sohail (09 Dec 2021) -- Start
    'Enhancement : OLD-524 : Netis Gabon - New Statutory Report ID21 DECLARATION ET TRAITEMENT DES SALAIRES for Gabon.
    ID21_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report = 283
    'Sohail (09 Dec 2021) -- End
    'Pinkal (10-Dec-2021)-- Start
    'Netis Gabon Statutory Report.
    ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report = 284
    'Pinkal (10-Dec-2021) -- End
End Enum



'Gajanan (30 Oct 2018) -- Start
'Enhancement : Add New Enum For Report Category

Public Enum enArutiReportCategory
    Payroll_Reports = 1
    Statutory_Reports = 2
    Leave_Reports = 3
    Employee_Reports = 4
    Training_Reports = 5
    Discipline_Reports = 6
    Assessment_Reports = 7
    Recruitment_Reports = 8
    Time_And_Attendance_Reports = 9
    Medical_Reports = 10
    Asset_Declaration_Reports = 11
    Claim_And_Request_Reports = 12
    Grievance_Reports = 13

    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    Claim_Retirement_Reports = 14
    'Pinkal (10-Feb-2021) -- End

    'S.SANDEEP |11-MAR-2021| -- START
    'ISSUE/ENHANCEMENT : Talent & Succession Report 
    Talent_Reports = 15
    Succession_Reports = 16
    'S.SANDEEP |11-MAR-2021| -- END

    'Gajanan [18-May-2021] -- Start
    PDP_Reports = 17
    'Gajanan [18-May-2021] -- End

End Enum

'Gajanan(06 Oct 2018) -- End




Public Enum enAnalysisRef
    Employee = 1
    Grade = 2
    Access = 3
    Section = 4
    CostCenter = 5
    PayPoint = 6
    DeptInDeptGroup = 7
    SectionInDepartment = 8
    JobInJobGroup = 9
    ClassInClassGroup = 10
    Station = 11
End Enum
Public Enum enModuleReference
    Payroll = 1
    Leave = 2
    Medical = 3
    Training = 4
    Assessment = 5
    TnA = 6 'Sohail (07 Jan 2014)

    PDP = 7
    Talent = 8
    Succession = 9

    'SHANI (06 JUN 2015) -- Start
    'Enhancement : Changes in C & R module given by Mr.Andrew.
    Miscellaneous = 999
    'SHANI (06 JUN 2015) -- End 

End Enum
'Sohail (15 Dec 2010) -- Start
Public Enum enPaymentMode
    CASH = 1
    CHEQUE = 2
    TRANSFER = 3
    CASH_AND_CHEQUE = 4
End Enum

'Sohail (15 Dec 2010) -- End



'Pinkal (20-Jan-2012) -- Start
'Enhancement : TRA Changes

'Public Enum enCommunctionType
'    None = 0
'    FingerPrint = 1
'    RFID = 2
'End Enum

Public Enum enFingerPrintDevice
    None = 0
    DigitalPersona = 1
    SecuGen = 2
    ZKSoftware = 3
    BioStar = 4
    ZKAcessControl = 5 ' ZK ACESS CONROL for Ocean Link 
    Anviz = 6 'INTEGRATION OF ANVIZ FOR TMJ
    ACTAtek = 7 'INTEGRATION OF ACTATEK FOR SEAMIC
    Handpunch = 8 'Pinkal (06-May-2016)   INTEGRATION OF ACTATEK FOR ASP
    SAGEM = 9  'Pinkal (04-Oct-2017) --  'Enhancement - Working SAGEM Database Device Integration.
    BioStar2 = 10  'Pinkal (12-Jun-2018) -- 'Enhancement - Integrating biostar 2 with Aruti for voltamp.
    FingerTec = 11 'Sohail (29 Jun 2019) - Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
    NewAnviz = 12  'Pinkal (22-Apr-2020) -- Enhancement - New Anviz Integration for Gran Melia.
    HIKVision = 13   'Pinkal (30-Sep-2021)-- DERM Enhancement : HIK Vision Biometric Integration.
End Enum

'Public Enum enFingerPrintConnetionType
'    None = 0
'    TcpIp = 1
'    RS232485 = 2
'    UsbClient = 3
'End Enum

'Pinkal (20-Jan-2012) -- End

' Pinkal (03-Jan-2011) -- End


'Sandeep [ 09 MARCH 2011 ] -- Start
Public Enum enAnalysisReport
    Branch = 1
    Department = 2
    Section = 3
    Unit = 4
    Job = 5
    CostCenter = 6
    SectionGroup = 7
    UnitGroup = 8
    Team = 9
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    DepartmentGroup = 10
    JobGroup = 11
    ClassGroup = 12
    Classs = 13 'Class is a KeyWord
    GradeGroup = 14
    Grade = 15
    GradeLevel = 16
    'Sohail (16 May 2012) -- End
End Enum
'Sandeep [ 09 MARCH 2011 ] -- End 


'S.SANDEEP [ 30 May 2011 ] -- START
'ISSUE : FINCA REQ.
Public Enum enPasswordOption
    PASSWD_EXPIRATION = 1
    PASSWD_LENGTH = 2
    PASSWD_ACC_LOCKOUT = 3
    APPLIC_IDLE_STATE = 4
    USER_LOGIN_MODE = 5  'S.SANDEEP [ 07 NOV 2011 ] -- START
    USER_NAME_LENGTH = 6 'S.SANDEEP [ 01 NOV 2012 ] -- START
    ENF_PASSWD_POLICY = 7 'S.SANDEEP [ 01 NOV 2012 ] -- START
    PROHIBIT_PASSWD_CHANGE = 8 'Sohail (15 Apr 2012)
    'Hemant (25 May 2021) -- Start
    'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
    PASSWD_HISTORY = 9
    'Hemant (25 May 2021) -- End
End Enum
'S.SANDEEP [ 30 May 2011 ] -- END 

'S.SANDEEP [ 08 June 2011 ] -- START
'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
Public Enum enUserDef_RepMode
    EARNINGS = 1
    DEDUCTIONS = 2
    EARNINGANDDEDUCTION = 3
End Enum
'S.SANDEEP [ 08 June 2011 ] -- END

'Pinkal (11-June-2011) -- Start

Public Enum enUserMode
    Loging = 1
    Password = 2
End Enum

'Pinkal (11-June-2011) -- End

'Sohail (02 Aug 2011) -- Start
Public Enum enAccountConfigType
    COMPANY_ACCOUNT_CONFIGURATION = 1
    EMPLOYEE_ACCOUNT_CONFIGURATION = 2
    COST_CENTER_ACCOUNT_CONFIGURATION = 3
End Enum
'Sohail (02 Aug 2011) -- End

'S.SANDEEP [ 28 JULY 2011 ] -- START
'ENHANCEMENT : CHANGE IN LOGIN FLOW. 
Public Enum enLoginMode
    USER = 1
    EMPLOYEE = 2
End Enum

Public Enum enLoanCalcId
    Simple_Interest = 1
    Compound_Interest = 2
    Reducing_Amount = 3
    No_Interest = 4 'S.SANDEEP [06 MAY 2015] -- START -- END
    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Reducing_Balance_With_Fixed_Principal_EMI = 5
    'Sohail (15 Dec 2015) -- End
    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    No_Interest_With_Mapped_Head = 6
    'Sohail (29 Apr 2019) -- End
End Enum

'Sohail (15 Dec 2015) -- Start
'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
Public Enum enLoanInterestCalcType
    DAILY = 1
    MONTHLY = 2
    'Sohail (14 Mar 2017) -- Start
    'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
    BY_TENURE = 3
    'Sohail (14 Mar 2017) -- End
    'Sohail (23 May 2017) -- Start
    'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
    SIMPLE_INTEREST = 4
    'Sohail (23 May 2017) -- End
End Enum
'Sohail (15 Dec 2015) -- End

'S.SANDEEP [ 28 JULY 2011 ] -- END 

'Sohail (31 Aug 2013) -- Start
'TRA - ENHANCEMENT
Public Enum enPayslipTemplate
    DEFAULT_1 = 1
    TRA_2 = 2
    DEFAULT_ONE_SIDED_3 = 3
    FINCA_4 = 4
    OMAN_5 = 5
    TAMA_6 = 6
    VOLTAMP_7 = 7 'Sohail (19 Sep 2013)
    FOUR_PAYSLIPS_8 = 8 'Sohail (07 Mar 2014)
    GHANA_9 = 9 'Sohail (19 Jul 2014)
    FINCA_DRC_10 = 10 'Sohail (11 Sep 2014)
    'Sohail (31 Dec 2014) -- Start
    'RBB & AMI Enhancement - New Payslip Template INDONESIA_11 for RBB and THREE_PAYSLIPS_12 for AMI.
    INDONESIA_11 = 11
    THREE_PAYSLIPS_12 = 12
    'Sohail (31 Dec 2014) -- End

    'S.SANDEEP [22 DEC 2015] -- START
    'KBC - PAYSLIP CHANGES
    ONE_SIDED_KBC_13 = 13
    'S.SANDEEP [22 DEC 2015] -- END
    'Sohail (17 Sep 2016) -- Start
    'Enhancement - 63.1 - New Payslip Template 14 of KBC template 13 with 2 payslip per page for MWAUWSA.
    TWO_SIDED_MWAUWSA_14 = 14
    'Sohail (17 Sep 2016) -- End
    'Sohail (20 Sep 2016) -- Start
    'Enhancement - 63.1 - New Payslip Template 15 for TIMAJA.
    TIMAJA_15 = 15
    'Sohail (20 Sep 2016) -- End
    'Nilay (03-Oct-2016) -- Start
    'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
    IBA_16 = 16
    'Nilay (03-Oct-2016) -- End
    IST_17 = 17 'Sohail (18 Dec 2018)
    SIC_18 = 18 'Sohail (15 Jan 2019)
    NETIS_19 = 19 'Sohail (26 May 2021)
End Enum
'Sohail (31 Aug 2013) -- End

'Sohail (08 Nov 2011) -- Start
Public Enum enSalaryIncrementBy
    Grade = 1
    Percentage = 2
    Amount = 3
    Auto_Increment = 4 'Sohail (27 Apr 2016)
End Enum
'Sohail (08 Nov 2011) -- End

'Sohail (14 Nov 2011) -- Start
Public Enum enJVCompanyConfigRefCode
    AccountCode = 1
    ShortName = 2 'Sohail (05 Jun 2013)
    AccountCode_PeriodCode = 3 'Shani (05 Aug 2015) 
    TranHeadCode_PeriodCode = 4 'Shani (05 Aug 2015) 
    ShortName2 = 5 'Sohail (08 Jul 2017)
    'Sohail (03 Jan 2019) -- Start
    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
    ReferenceCode_Mapped_with_Company_Bank = 6
    'Sohail (03 Jan 2019) -- End
    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    ShortName3 = 7
    'Sohail (03 Mar 2020) -- End
End Enum

Public Enum enJVCompanyConfigRefName
    AccountName = 1
    TranHeadName = 2
    AccountName_TranHeadName = 3
    'Sohail (14 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    ShortName = 4
    ShortName_AccountName = 5
    ShortName_TranHeadName = 6
    'Sohail (14 Jun 2013) -- End
    AccountName_PeriodName = 7 'Shani (05 Aug 2015) 
    TranHeadName_PeriodName = 8 'Shani (05 Aug 2015) 
    ShortName2 = 9 'Sohail (08 Jul 2017)
    'Sohail (03 Jan 2019) -- Start
    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
    ReferenceCode_Mapped_with_Company_Bank = 10
    ReferenceName_Mapped_with_Company_Bank = 11
    'Sohail (03 Jan 2019) -- End
    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    ShortName3 = 12
    'Sohail (03 Mar 2020) -- End
End Enum

Public Enum enJVEmployeeConfigRefCode
    AccountCode = 1
    IDType = 2
    EmployeeCode = 3
    AccountCode_EmployeeCode = 4
    EmployeeCode_AccountCode = 5
    'Sohail (05 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    ShortName = 6
    ShortName_AccountCode = 7
    'Sohail (05 Jun 2013) -- End
    'Shani (05 Aug 2015) -- Start
    'Enhancement - Consolidate heads amount on iScala JV Report if mapped to one account and provide Period Name and Job Group in Reference Name options.
    CostCenterCode = 8
    EmployeeCode_CostCenterCode = 9
    'Shani (05 Aug 2015) -- End
    ShortName2 = 10 'Sohail (08 Jul 2017)
    'Sohail (17 Feb 2018) -- Start
    'AMANA Bank Enhancement : Ref. No. 172 - UPLOAD FILE FROM ARUTI TO FLEX CUBE - (RefNo: 171) in 70.1.
    Employee_Bank_Account_No = 11
    'Sohail (17 Feb 2018) -- End
    'Sohail (03 Jan 2019) -- Start
    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
    ReferenceCode_Mapped_with_Company_Bank = 12
    'Sohail (03 Jan 2019) -- End
    'Hemant (29 May 2019) -- Start
    'MUWASA ENHANCEMENT - Ref # 3329 - 76.1 : PASTEL V2 COMBINED JV INTEGRATION.
    AccountCode_IDType = 13
    'Hemant (29 May 2019) -- End
    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    ShortName3 = 14
    'Sohail (03 Mar 2020) -- End
End Enum

Public Enum enJVEmployeeConfigRefName
    AccountName = 1
    TranHeadName = 2
    EmployeeName = 3
    AccountName_TranHeadName = 4
    AccountName_EmployeeName = 5
    TranHeadName_EmployeeName = 6 'Sohail (12 Dec 2011)
    'Sohail (14 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    ShortName = 7
    ShortName_AccountName = 8
    ShortName_TranHeadName = 9
    'Sohail (14 Jun 2013) -- End
    'Shani (05 Aug 2015) -- Start
    'Enhancement - Consolidate heads amount on iScala JV Report if mapped to one account and provide Period Name and Job Group in Reference Name options.
    CostCenterName = 10
    EmployeeName_CostCenterName = 11
    'Shani (05 Aug 2015) -- End
    ShortName2 = 12 'Sohail (08 Jul 2017)
    'Sohail (17 Feb 2018) -- Start
    'AMANA Bank Enhancement : Ref. No. 172 - UPLOAD FILE FROM ARUTI TO FLEX CUBE - (RefNo: 171) in 70.1.
    Employee_Bank_Account_No = 13
    'Sohail (17 Feb 2018) -- End
    'Sohail (03 Jan 2019) -- Start
    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
    ReferenceCode_Mapped_with_Company_Bank = 14
    ReferenceName_Mapped_with_Company_Bank = 15
    'Sohail (03 Jan 2019) -- End
    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    ShortName3 = 16
    'Sohail (03 Mar 2020) -- End
End Enum

Public Enum enJVCostCenterloyeeConfigRefCode
    AccountCode = 1
    CostCenterCode = 2
    AccountCode_CostCenterCode = 3
    'Sohail (05 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    ShortName = 4
    ShortName_AccountCode = 5
    ShortName_CostCenterCode = 6
    'Sohail (05 Jun 2013) -- End

    'Shani (05 Aug 2015) -- Start
    'Enhancement - Consolidate heads amount on iScala JV Report if mapped to one account and provide Period Name and Job Group in Reference Name options.
    AccountCode_PeriodCode = 7
    'Shani (05 Aug 2015) -- End
    ShortName2 = 8 'Sohail (08 Jul 2017)
    'Sohail (03 Jan 2019) -- Start
    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
    ReferenceCode_Mapped_with_Company_Bank = 9
    'Sohail (03 Jan 2019) -- End
    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    ShortName3 = 10
    'Sohail (03 Mar 2020) -- End
End Enum

Public Enum enJVCostCenterloyeeConfigRefName
    AccountName = 1
    TranHeadName = 2
    CostCenterName = 3
    AccountName_TranHeadName = 4
    AccountName_CostCenterName = 5
    TranHeadName_CostCenterName = 6
    'Sohail (14 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    ShortName = 7
    ShortName_AccountName = 8
    ShortName_TranHeadName = 9
    ShortName_CostCenterName = 10
    'Sohail (14 Jun 2013) -- End

    'Shani (05 Aug 2015) -- Start
    'Enhancement - Consolidate heads amount on iScala JV Report if mapped to one account and provide Period Name and Job Group in Reference Name options.
    AccountName_PeriodName = 11
    JobGroupName = 12
    AccountName_JobGroupName = 13
    AccountName_JobGroupName_PeriodName = 14
    'Shani (05 Aug 2015) -- End
    ShortName2 = 15 'Sohail (08 Jul 2017)
    'Sohail (03 Jan 2019) -- Start
    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
    ReferenceCode_Mapped_with_Company_Bank = 16
    ReferenceName_Mapped_with_Company_Bank = 17
    'Sohail (03 Jan 2019) -- End
    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    ShortName3 = 18
    'Sohail (03 Mar 2020) -- End
End Enum
'Sohail (14 Nov 2011) -- End
'S.SANDEEP [ 09 AUG 2011 ] -- START
'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
Public Enum enWorkingMode
    StandAlone = 0
    Server = 1
    Client = 2
End Enum
'S.SANDEEP [ 09 AUG 2011 ] -- END 

'Pinkal (12-Oct-2011) -- Start
'Enhancement : TRA Changes

Public Enum enMedicalMasterType
    '    'Category = 1
    Medical_Category = 1
    Cover = 2
    Treatment = 3
    Service = 4
End Enum

'Pinkal (12-Oct-2011) -- End


'S.SANDEEP [ 07 NOV 2011 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enAuthenticationMode
    BASIC_AUTHENTICATION = 1
    AD_BASIC_AUTHENTICATION = 2
    AD_SSO_AUTHENTICATION = 3
End Enum
'S.SANDEEP [ 07 NOV 2011 ] -- END

'S.SANDEEP [ 25 DEC 2011 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enVacancyType
    EXTERNAL_VACANCY = 1
    INTERNAL_VACANCY = 2
    'Sohail (18 Apr 2020) -- Start
    'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
    INTERNAL_AND_EXTERNAL = 3
    'Sohail (18 Apr 2020) -- End
End Enum
'S.SANDEEP [ 25 DEC 2011 ] -- END
'Sohail (04 Jan 2012) -- Start
Public Enum enEmpAssetStatus
    Assigned = 1
    Returned = 2
    WrittenOff = 3
    Lost = 4
End Enum
'Sohail (04 Jan 2012) -- End

'S.SANDEEP [ 29 DEC 2011 ] -- START
'ENHANCEMENT : TRA CHANGES 
'TYPE : EMPLOYEMENT CONTRACT PROCESS
Public Enum enBSCPerspective
    FINANCIAL = 1
    CUSTOMER = 2
    BUSINESS_PROCESS = 3
    ORGANIZATION_CAPACITY = 4
End Enum

Public Enum enAllocation
    BRANCH = 1
    DEPARTMENT_GROUP = 2
    DEPARTMENT = 3
    SECTION_GROUP = 4
    SECTION = 5
    UNIT_GROUP = 6
    UNIT = 7
    TEAM = 8
    JOB_GROUP = 9
    JOBS = 10
    EMPLOYEE = 11
    GENERAL = 12
    'S.SANDEEP [ 31 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    CLASS_GROUP = 13
    CLASSES = 14
    'S.SANDEEP [ 31 MAY 2012 ] -- END

    'S.SANDEEP [ 01 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    COST_CENTER = 15
    'S.SANDEEP [ 01 FEB 2013 ] -- END

    'S.SANDEEP [31 AUG 2016] -- START
    'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
    EMPLOYEE_GRADES = 16
    'S.SANDEEP [31 AUG 2016] -- START
End Enum

'Sohail (14 Nov 2019) -- Start
'NMB UAT Enhancement # : Bind Allocation to training course.
Public Enum enAdvanceFilterAllocation
    BRANCH = 1
    DEPARTMENT_GROUP = 2
    DEPARTMENT = 3
    SECTION_GROUP = 4
    SECTION = 5
    UNIT_GROUP = 6
    UNIT = 7
    TEAM = 8
    JOB_GROUP = 9
    JOBS = 10
    CLASS_GROUP = 11
    CLASSES = 12
    GRADE_GROUP = 13
    GRADE = 14
    GRADE_LEVEL = 15
    COST_CENTER_GROUP = 16
    COST_CENTER = 17
    EMPLOYEMENT_TYPE = 18
    RELIGION = 19
    GENDER = 20
    RELATION_FROM_DEPENDENTS = 21
    NATIONALITY = 22
    PAY_TYPE = 23
    PAY_POINT = 24
    MARITAL_STATUS = 25
    SKILL = 26
End Enum
'Sohail (14 Nov 2019) -- End

Public Enum enAssessmentMode
    SELF_ASSESSMENT = 1
    APPRAISER_ASSESSMENT = 2
    REVIEWER_ASSESSMENT = 3
End Enum
'S.SANDEEP [ 29 DEC 2011 ] -- END


'S.SANDEEP [ 04 FEB 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enAppraisal_Modes
    SALARY_INCREMENT = 1
    COMMENDABLE_LETTER = 2
    IMPROVEMENT_LETTER = 3
    WARNING_LETTER = 4
    SETTING_REMINDER = 5
    'S.SANDEEP [12-JUL-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
    BONUS = 6
    'S.SANDEEP [12-JUL-2018] -- END
End Enum
'S.SANDEEP [ 04 FEB 2012 ] -- END

'Sohail (15 Feb 2012) -- Start
'TRA - ENHANCEMENT
Public Enum enTranHeadActiveInActive
    ACTIVE = 1
    INACTIVE = 2
End Enum
'Sohail (15 Feb 2012) -- End

'Sohail (24 Feb 2012) -- Start
'TRA - ENHANCEMENT
Public Enum enTrainingAward_Modes
    SALARY_INCREMENT = 1
    RE_CATEGORIZE = 2
End Enum
'Sohail (24 Feb 2012) -- End
'Pinkal (06-Feb-2012) -- Start
'Enhancement : TRA Changes

Public Enum enCourseType
    Job_Capability = 1
    Career_Development = 2
End Enum

'Pinkal (06-Feb-2012) -- End
'Sohail (17 Apr 2012) -- Start
'TRA - ENHANCEMENT
Public Enum enBasicSalaryComputation
    WITH_WEEKENDS_INCLUDED = 1
    WITH_WEEKENDS_EXCLUDED = 2
    WITH_CONSTANT_DAYS = 3 'Sohail (10 Jul 2013)
End Enum
'Sohail (17 Apr 2012) -- End
'S.SANDEEP [ 21 MAY 2012 ] -- START
'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
Public Enum enLogin_Mode
    DESKTOP = 1
    EMP_SELF_SERVICE = 2
    MGR_SELF_SERVICE = 3
End Enum
'S.SANDEEP [ 21 MAY 2012 ] -- END

'Sohail (06 Jun 2012) -- Start
'TRA - ENHANCEMENT
Public Enum enDatabaseServerSetting
    DEFAULT_SETTING = 1
    CUSTOM_SETTING = 2
End Enum
'Sohail (06 Jun 2012) -- End


'Sohail (24 Sep 2012) -- Start
'TRA - ENHANCEMENT
Public Enum enSalaryChangeApprovalStatus
    Approved = 1
    Pending = 2
    All = 3
End Enum
'Sohail (24 Sep 2012) -- End

'Sohail (28 May 2014) -- Start
'Enhancement - Staff Requisition.
Public Enum enApprovalStatus
    PENDING = 1
    APPROVED = 2
    REJECTED = 3
    CANCELLED = 4
    PUBLISHED = 5
End Enum
'Sohail (28 May 2014) -- End

'Hemant (02 Jul 2019) -- Start
'ENHANCEMENT :  ZRA minimum Requirements
Public Enum enVacancyStatus
    NOT_PUBLISHED = 1
    OPEN = 2
    CLOSED = 3
    FILLED = 4
End Enum
'Hemant (02 Jul 2019) -- End

'Sohail (29 Sep 2021) -- Start
'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
Public Enum enJobAdvert
    INTERNALLY = 1
    EXTERNALLY = 2
    BOTH_INTERNALLY_AND_EXTERNALLY = 3
End Enum
'Sohail (29 Sep 2021) -- End

'S.SANDEEP [ 18 SEP 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enEmployeeDates
    APPOINTED_DATE = 1
    CONFIRM_DATE = 2
    BIRTH_DATE = 3
    SUSPENSION_DATE = 4
    PROBATION_DATE = 5
    EOC_DATE = 6
    LEAVING_DATE = 7
    RETIREMENT_DATE = 8
    REINSTATEMENT_DATE = 9
    MARRIAGE_DATE = 10
    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    EXEMPTION_DATE = 11
    'Sohail (21 Oct 2019) -- End
End Enum

Public Enum enEmployeeData
    DEPENDENTS = 1
    QUALIFICATIONS = 2
    EXPERIENCES = 3
    REFERENCES = 4
    BENEFITS = 5
    COMPANY_ASSETS = 6
End Enum
'S.SANDEEP [ 18 SEP 2012 ] -- END

'S.SANDEEP [ 03 OCT 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enNotificationBank
    BANK_GROUP = 1
    BANK_BRANCH = 2
    COMPANY_BANK = 3
    EMPLOYEE_BANK = 4
End Enum
'S.SANDEEP [ 03 OCT 2012 ] -- END

'Sohail (09 Mar 2013) -- Start
'TRA - ENHANCEMENT
Public Enum enNotificationPayroll
    AUTHORIZED_PAYEMNT = 1
    VOID_AUTHORIZED_PAYEMNT = 2 'Sohail (10 Mar 2014)
End Enum
'Sohail (09 Mar 2013) -- End

'Nilay (17-Aug-2016) -- Start
'ENHANCEMENT : Add Loan Savings Notification Settings
Public Enum enLoanAdvNotification
    LOAN_ASSIGNED = 1
End Enum
'Nilay (17-Aug-2016) -- END


'S.SANDEEP [ 01 NOV 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enPasswordPolicy
    PASSWD_UCASE_CMPL = 1
    PASSWD_LCASE_CMPL = 2
    PASSWD_NCASE_CMPL = 3
    PASSWD_SPLCH_CMPL = 4

    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    PASSWD_COMPLEXITY = 5
    'Pinkal (18-Aug-2018) -- End

End Enum
'S.SANDEEP [ 01 NOV 2012 ] -- END

'Pinkal (01-Dec-2012) -- Start
'Enhancement : TRA Changes

Public Enum enDisplayNameSetting
    NONE = 0
    EMPLOYEECODE = 1
    FLASTNAME = 2
    FIRSTNAME_LASTNAME = 3
    LASTNAME_FIRSTNAME = 4
End Enum

'Pinkal (01-Dec-2012) -- End

'S.SANDEEP [ 28 DEC 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enWeight_Options
    WEIGHT_EACH_ITEM = 1
    WEIGHT_BASED_ON = 2
End Enum

Public Enum enWeight_Types
    'S.SANDEEP [ 05 NOV 2014 ] -- START
    'WEIGTH_OBJECTIVE = 1
    'WEIGTH_KPI = 2
    'WEIGTH_TARGETS = 3
    'WEIGTH_INITIATIVE = 4

    WEIGHT_FIELD1 = 1
    WEIGHT_FIELD2 = 2
    WEIGHT_FIELD3 = 3
    WEIGHT_FIELD4 = 4
    WEIGHT_FIELD5 = 5
    'S.SANDEEP [ 05 NOV 2014 ] -- END
End Enum
'S.SANDEEP [ 28 DEC 2012 ] -- END
'S.SANDEEP [ 07 FEB 2013 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enAD_Report_Parameter
    APP_DATE_FROM = 1
    APP_DATE_BEFORE = 2
    APP_DATE_AFTER = 3
End Enum
'S.SANDEEP [ 07 FEB 2013 ] -- END

'Sohail (07 Feb 2013) -- Start
'TRA - ENHANCEMENT
Public Enum enAD_Categories
    CURRENT_ASSETS = 1
    SHARE_AND_DIVIDENDS = 2
    HOUSES_AND_BUILDINGS = 3
    PARK_FARMS_AND_MINES = 4
    TRANSPORT_EQUIPMENTS = 5
    MACHINERIES_AND_INDUSTRIES = 6
    OTHER_BUSINESSES = 7
    OTHER_COMMERCIAL_INTERESTS = 8
    DEBTS = 9
End Enum

Public Enum enComparison_Operator
    GREATER_THAN = 1
    LESS_THAN = 2
    GREATER_THAN_EQUAL = 3
    LESS_THAN_EQUAL = 4
    EQUAL = 5
    NOT_EQUAL = 6
    BETWEEN = 7
End Enum
'Sohail (07 Feb 2013) -- End
'Pinkal (06-Feb-2013) -- Start
'Enhancement : TRA Changes

Public Enum enLeaveBalanceSetting
    Financial_Year = 1
    ELC = 2
End Enum

'Pinkal (06-Feb-2013) -- End
'S.SANDEEP [ 10 APR 2013 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enObjective_Status
    SUBMIT_APPROVAL = 1
    FINAL_SAVE = 2
    OPEN_CHANGES = 3
    NOT_SUBMIT = 4
    NOT_COMMITTED = 5 'S.SANDEEP [ 05 NOV 2014 ] -- START -- END
    FINAL_COMMITTED = 6 'S.SANDEEP [ 05 NOV 2014 ] -- START -- END
    PERIODIC_REVIEW = 7 'S.SANDEEP [ 05 NOV 2014 ] -- START -- END
End Enum
'S.SANDEEP [ 10 APR 2013 ] -- END

'Pinkal (18-Apr-2013) -- Start
'Enhancement : TRA Changes

Public Enum enConfigOptionGroup
    GENERAL = 1
    PATHS = 2
    INTEGRATION = 3
    SETTINGS = 4
    NOTIFICATION = 5
End Enum

'Pinkal (18-Apr-2013) -- End

'S.SANDEEP [ 14 May 2013 ] -- START
'ENHANCEMENT : TRA ENHANCEMENT
Public Enum enShortListing_Status
    SC_APPROVED = 1
    SC_PENDING = 2
    SC_REJECT = 3
    SC_NONE = 4
End Enum

Public Enum enApplicant_Eligibility
    AE_ELIGIBLE = 0
    AE_NOTELIGIBLE = 1
    AE_ELIGIBLE_APPROVED = 2
    AE_ELIGIBLE_DISAPPROVED = 3
    AE_NONE = 4
End Enum
'S.SANDEEP [ 14 May 2013 ] -- END

'Sohail (28 May 2014) -- Start
'Enhancement - Staff Requisition.
Public Enum enStaffRequisition_Status
    REPLACEMENT = 1
    ADDITIONAL = 2
    REPLACEMENT_AND_ADDITIONAL = 3 'Sohail (18 Mar 2015)
End Enum
'Sohail (28 May 2014) -- End

'Pinkal (03-Sep-2013) -- Start
'Enhancement : TRA Changes

Public Enum enEmployeeReporting
    Assessor_Reviewer = 1
    Leave_Approver = 2
    Employee_Reporting = 3

    'Pinkal (05-May-2020) -- Start
    'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
    Claim_Approver = 4
    OT_Approver = 5
    'Pinkal (05-May-2020) -- End

End Enum

'Pinkal (03-Sep-2013) -- End


'Pinkal (20-Sep-2013) -- Start
'Enhancement : TRA Changes

Public Enum enTnAOperation
    Edit = 1
    RoundOff = 2
End Enum

'Pinkal (20-Sep-2013) -- End

'S.SANDEEP [ 26 SEPT 2013 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Enum enLeaveFreq_Factors
    LVF_LIFE_TIME = 1
    LVF_FINANCIAL_YEAR = 2
    LVF_ELC = 3
End Enum
'S.SANDEEP [ 26 SEPT 2013 ] -- END

'S.SANDEEP [ 17 JAN 2014 ] -- START
Public Enum enManualRoundOffType
    ROUND_IN_TIME = 1
    ROUND_OUT_TIME = 2
    ROUND_BOTH = 3
End Enum
'S.SANDEEP [ 17 JAN 2014 ] -- END


'Pinkal (4-Sep-2014) -- Start
'Enhancement - PAY_A CHANGES IN PPA

Public Enum enPPAMode
    RATE = 1
    OT = 2
    PH = 3
End Enum

'Pinkal (4-Sep-2014) -- End

'S.SANDEEP [ 04 FEB 2014 ] -- START
Public Enum enExpenseType
    EXP_NONE = 0
    EXP_LEAVE = 1
    EXP_MEDICAL = 2
    EXP_TRAINING = 3
    'SHANI (06 JUN 2015) -- Start
    'Enhancement : Changes in C & R module given by Mr.Andrew.
    EXP_MISCELLANEOUS = 4
    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    EXP_IMPREST = 5
    'Pinkal (11-Sep-2019) -- End
End Enum
Public Enum enExpUoM
    UOM_QTY = 1
    UOM_AMOUNT = 2
End Enum
Public Enum enExpFromModuleID
    FROM_EXPENSE = 0
    FROM_LEAVE = 1
    FROM_MEDICAL = 2
    FROM_TRAINING = 3
    'SHANI (06 JUN 2015) -- Start
    'Enhancement : Changes in C & R module given by Mr.Andrew.
    FROM_MISCELLANEOUS = 4
    'SHANI (06 JUN 2015) -- End 

End Enum

'S.SANDEEP [ 04 FEB 2014 ] -- END


'Pinkal (12-Sep-2014) -- Start
'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

Public Enum enSickSheetTemplate
    TRA_1 = 1
    TANAPA_2 = 2

    'Pinkal (25-Nov-2014) -- Start
    'Enhancement -  ADDED NEW SICK SHEET FORMAT FOR VISION FUND TANZANIA.
    VFT_3 = 3
    'Pinkal (25-Nov-2014) -- End
End Enum

'Pinkal (12-Sep-2014) -- End

'S.SANDEEP [ 05 NOV 2014 ] -- START
Public Enum enPACascading
    STRICT_CASCADING = 1
    LOOSE_CASCADING = 2
    STRICT_GOAL_ALIGNMENT = 3
    LOOSE_GOAL_ALIGNMENT = 4
    NEITHER_CASCADING_NOR_ALIGNMENT = 5
End Enum

Public Enum enScoringOption
    SC_WEIGHTED_BASED = 1
    SC_SCALE_BASED = 2
End Enum

Public Enum enCompGoalStatus
    ST_PENDING = 1
    ST_INPROGRESS = 2
    ST_COMPLETE = 3
    ST_CLOSED = 4
    ST_ONTRACK = 5
    ST_ATRISK = 6
    ST_NOTAPPLICABLE = 7
    'S.SANDEEP [05 SEP 2016] -- START
    'ENHANCEMENT : NEW STATUS {BY ANDREW}
    ST_POSTPONED = 8
    ST_BEHIND_SCHEDULE = 9
    ST_AHEAD_SCHEDULE = 10
    'S.SANDEEP [05 SEP 2016] -- START
End Enum

Public Enum enBSC_PlanningType
    ORGANIZATION_BSC = 1
    OWNER_BSC = 2
    EMPLOYEE_BSC = 3
End Enum

Public Enum enEvaluationOrder
    PE_BSC_SECTION = 1
    PE_COMPETENCY_SECTION = 2
    PE_CUSTOM_SECTION = 3
End Enum

Public Enum enAuditType
    ADD = 1
    EDIT = 2
    DELETE = 3
End Enum
'S.SANDEEP [ 05 NOV 2014 ] -- END
'S.SANDEEP [ 01 JAN 2015 ] -- START
Public Enum enAssessmentReportTemplate
    ART_TEMPLATE_1 = 1  'TFDA
    ART_TEMPLATE_2 = 2  'VFT
    ART_TEMPLATE_3 = 3  'TANAPA 'S.SANDEEP [16 MAR 2015] -- START -- END
    ART_TEMPLATE_4 = 4  'MGT
    ART_TEMPLATE_5 = 5  'VOLTAMP [HTML FORMAT] 'S.SANDEEP [09 OCT 2015] -- START -- END
    ART_TEMPLATE_6 = 6  'TRA 'S.SANDEEP [09 OCT 2015] -- START -- END
    ART_TEMPLATE_7 = 7  'AKF  'S.SANDEEP [09 NOV 2015] -- START -- END
    ART_TEMPLATE_8 = 8  'CCBRT 'S.SANDEEP [23 DEC 2015] -- START -- END
    ART_TEMPLATE_9 = 9  'AFRICAN REGENT 'S.SANDEEP [29 DEC 2015] -- START -- END
    ART_TEMPLATE_10 = 10 'KBC
    ART_TEMPLATE_11 = 11 'PSI Malavi 'Shani(16-MAR-2016)
    ART_TEMPLATE_12 = 12 'Crown Pain individual 'Shani(19-APR-2016)
    ART_TEMPLATE_13 = 13 'CCK Shani (05-May-2016)
    ART_TEMPLATE_14 = 14 'TNP(Copy From 6) Shani(20-Sep-2016)
    ART_TEMPLATE_15 = 15 'AKIBA Shani (15-Nov-2016)
    ART_TEMPLATE_16 = 16 'FINCA(TZ) 'S.SANDEEP [20-JUL-2017] -- START -- END
    ART_TEMPLATE_17 = 17 'NMB 'S.SANDEEP |04-DEC-2019| -- START -- END
    ART_TEMPLATE_18 = 18 'FINCA UGANDA 'S.SANDEEP |26-AUG-2020| -- START -- END
End Enum

Public Enum enAssess_Functions_Mode
    COMPUTATION_VARIABLES = 1
    PLUS = 2
    MINUS = 3
    MULTIPLY = 4
    DIVIDE = 5
    OPEN_BRACKET = 6
    CLOSE_BRACKET = 7
    'S.SANDEEP [19 FEB 2015] -- START
    PA_CONSTANT_VALUE = 8
    PRE_DEF_FORMULA = 9
    'S.SANDEEP [19 FEB 2015] -- END
End Enum

Public Enum enAssess_Computation_Types
    BSC_ITEM_WEIGHT = 1
    BSC_ITEM_EMP_SCORE_VALUE = 2
    BSC_ITEM_ASR_SCORE_VALUE = 3
    BSC_ITEM_REV_SCORE_VALUE = 4
    BSC_PARENT_FIELD_WEIGHT = 5
    BSC_ITEM_MAX_SCORE_VALUE = 6
    BSC_OVERALL_WEIGHT = 7
    CMP_ITEM_WEIGHT = 8
    CMP_ITEM_EMP_SCORE_VALUE = 9
    CMP_ITEM_ASR_SCORE_VALUE = 10
    CMP_ITEM_REV_SCORE_VALUE = 11
    CMP_CATEGORY_WEIGHT = 12
    CMP_ITEM_MAX_SCORE_VALUE = 13
    CMP_ASSESSMENT_GROUP_WEIGHT = 14
    CMP_OVERALL_WEIGHT = 15
    BSC_PARENT_ITEMS_ALL_PERSPECTIVE_COUNT = 16
    BSC_LINKED_ITEMS_ALL_PERSPECTIVE_COUNT = 17
    CMP_ASSESSMENT_GROUP_COUNT = 18
    CMP_ITEMS_PER_CATEGORY_COUNT = 19
    CMP_CATEGORY_PER_GROUP_COUNT = 20
    CMP_ITEMS_ALL_CATEGORY_GROUP_COUNT = 21
    CMP_CATEGORY_ALL_GROUP_COUNT = 22
    BSC_RATIO_VALUE = 23
    CMP_RATIO_VALUE = 24
    'S.SANDEEP [21 JAN 2015] -- START
    BSC_TOT_EMP_ROW_SCORE_RATIO = 25
    BSC_TOT_ASR_ROW_SCORE_RATIO = 26
    BSC_TOT_REV_ROW_SCORE_RATIO = 27
    CMP_TOT_EMP_ROW_SCORE_RATIO = 28
    CMP_TOT_ASR_ROW_SCORE_RATIO = 29
    CMP_TOT_REV_ROW_SCORE_RATIO = 30
    'S.SANDEEP [21 JAN 2015] -- END
    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 40
    TOT_FINAL_SCORE_ALL_PERIOD = 31
    TOT_ASSES_PERIOD_COUNT = 32
    'S.SANDEEP [29-NOV-2017] -- END
End Enum

Public Enum enAssess_Computation_Formulas
    BSC_EMP_TOTAL_SCORE = 1
    BSC_ASR_TOTAL_SCORE = 2
    BSC_REV_TOTAL_SCORE = 3
    CMP_EMP_TOTAL_SCORE = 4
    CMP_ASR_TOTAL_SCORE = 5
    CMP_REV_TOTAL_SCORE = 6
    EMP_OVERALL_SCORE = 7
    ASR_OVERALL_SCORE = 8
    REV_OVERALL_SCORE = 9
    POST_TO_PAYROLL_VALUE = 10
    FINAL_RESULT_SCORE = 11 'S.SANDEEP [19 FEB 2015]
    AVG_FINAL_RESULT_SCORE = 12 'S.SANDEEP [29-NOV-2017] -- START {REF-ID # 40} -- END
End Enum
'S.SANDEEP [ 01 JAN 2015 ] -- END

'Hemant (06 Oct 2018) -- Start
'Enhancement : Implementing New Module of Asset Declaration Template 2
Public Enum enAsset_Declaration_Template
    Template1 = 1
    Template2 = 2
End Enum
'Hemant (06 Oct 2018) -- End


'S.SANDEEP [19 FEB 2015] -- START
Public Enum enApprFilterTypeId
    APRL_OVER_ALL = 1
    APRL_EMPLOYEE = 2
    APRL_ASSESSOR = 3
    APRL_REVIEWER = 4
End Enum
'S.SANDEEP [19 FEB 2015] -- END
'Sohail (09 Mar 2015) -- Start
'Enhancement - New EFT Report Custom CSV Report.
Public Enum enEFT_Export_Mode
    CSV = 1
    XLS = 2
End Enum

Public Enum enEFT_EFT_Custom_Columns
    COMP_BANK_CODE = 1
    COMP_BANK_NAME = 2
    COMP_BRANCH_CODE = 3
    COMP_BRANCH_NAME = 4
    COMP_BANK_SORT_CODE = 5
    COMP_BANK_SWIFT_CODE = 6
    COMP_BANK_ACCOUNT_NO = 7
    EMP_BANK_CODE = 8
    EMP_BANK_NAME = 9
    EMP_BRANCH_CODE = 10
    EMP_BRANCH_NAME = 11
    EMP_BANK_SORT_CODE = 12
    EMP_BANK_SWIFT_CODE = 13
    EMP_BANK_ACCOUNT_NO = 14
    EMPLOYEE_CODE = 15
    EMPLOYEE_NAME = 16
    EMP_BANK_AMOUNT = 17
    DESCRIPTION = 18
    EMP_MEMBERSHIP_NO = 19 'Sohail (13 Apr 2016)
    'Hemant (02 Jul 2020) -- Start
    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
    PAYMENT_DATE = 20
    PAYMENT_CURRENCY = 21
    'Hemant (02 Jul 2020) -- End
End Enum
'Sohail (09 Mar 2015) -- End

'Sohail (26 Mar 2020) -- Start
'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
Public Enum enPayrollJournal_Export_Mode
    CSV = 1
    XLS = 2
    Crystal_Report = 3
End Enum

Public Enum enPayrollJournal_Columns
    Account_Code = 1
    Account_Name = 2
    Tran_Head_Name = 3
    Mapped_Ref_Code = 4
    Mapped_Ref_Name = 5
    Short_Name_1 = 6
    Short_Name_2 = 7
    Short_Name_3 = 8
    Debit = 9
    Credit = 10
    Amount = 11
    D_C = 12
    Period_Code = 13
    Period_Name = 14
    Period_End_Date = 15
    Description = 16
    Tran_Head_Code = 17
    'Hemant (13 Jul 2020) -- Start
    'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
    Posting_Date = 18
    'Hemant (13 Jul 2020) -- End
    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Employee code, cost center code, employee name and cost center name column options on custom columns payroll jouranl report.
    Employee_CostCenter_Code = 19
    Employee_CostCenter_Name = 20
    'Sohail (03 Jul 2020) -- End
End Enum
'Sohail (26 Mar 2020) -- End

'Sohail (12 Jan 2015) -- Start
'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
Public Enum enLoanStatus
    IN_PROGRESS = 1
    ON_HOLD = 2
    WRITTEN_OFF = 3
    COMPLETED = 4
    'Sohail (29 Jan 2016) -- Start
    'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
    IN_PROGRESS_WITH_LOAN_DEDUCTED = 5
    'Sohail (29 Jan 2016) -- End
End Enum

Public Enum enSavingStatus
    IN_PROGRESS = 1
    ON_HOLD = 2
    REDEMPTION = 3
    COMPLETED = 4
End Enum
'Sohail (12 Jan 2015) -- End

'S.SANDEEP [ 01 JAN 2015 ] -- END


'Pinkal (19-Mar-2015) -- Start
'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
Public Enum enSwapApproverType
    Leave = 1
    Loan = 2
    Claim_Request = 3
    Performance = 4
End Enum
'Pinkal (19-Mar-2015) -- End

'S.SANDEEP [14 MAR 2015] -- START
Public Enum enEmp_Dates_Transaction
    DT_PROBATION = 1
    DT_CONFIRMATION = 2
    DT_SUSPENSION = 3
    DT_TERMINATION = 4
    DT_REHIRE = 5
    DT_RETIREMENT = 6
    DT_APPOINTED_DATE = 7
    DT_BIRTH_DATE = 8
    DT_FIRST_APP_DATE = 9
    DT_MARRIGE_DATE = 10
    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    DT_EXEMPTION = 11
    'Sohail (21 Oct 2019) -- End
End Enum
'S.SANDEEP [14 MAR 2015] -- END

'S.SANDEEP [04 MAR 2015] -- START
Public Enum enParameterMode
    LN_RATE = 1
    LN_EMI = 2
    LN_TOPUP = 3
End Enum
'S.SANDEEP [04 Mar 2015] -- END
'S.SANDEEP [09 MAY 2016] -- START
'ENHANCEMENT : TRANSACTION SCREEN FOR PROMOTION
Public Enum enSalaryChangeType
    SIMPLE_SALARY_CHANGE = 1
    SALARY_CHANGE_WITH_PROMOTION = 2
End Enum
'S.SANDEEP [09 MAY 2016] -- END

'Nilay (27 Apr 2016) -- Start
'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
'#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
Public Enum enSalaryAnniversaryType
    AUTOMATIC = 1
    MANUAL = 2
End Enum

Public Enum enSalaryAnniversaryMonth
    APPOINTMENT_MONTH = 1
    CONFIRMATION_MONTH = 2
    'S.SANDEEP [25 OCT 2016] -- START
    'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
    DEFINED_MONTH = 3
    'S.SANDEEP [25 OCT 2016] -- END
End Enum
'Nilay (27 Apr 2016) -- End


'Pinkal (18-Jun-2016) -- Start
'Enhancement - IMPLEMENTING Leave Liability Setting to Leave Liablity Report.

Public Enum enLeaveLiabilitySetting
    Days_With_Weekend_Included = 1
    Days_With_Weekend_Excluded = 2
    With_Constant_Days = 3
End Enum

'Pinkal (18-Jun-2016) -- End


'Sohail (07 Jun 2016) -- Start
'Enhancement -  New Budget Redesign for MST in 62.1 SP.
Public Enum enBudgetViewBy
    Allocation = 1
    Employee = 2
End Enum

Public Enum enBudgetPresentation
    TransactionWise = 1
    Summary = 2
End Enum

Public Enum enBudgetWhoToInclude
    ActiveEmployee = 1
    AllAsPerManPowerPlan = 2
End Enum

Public Enum enBudgetSalaryLevel
    Minimum = 1
    Mid = 2
    Maximum = 3
End Enum

Public Enum enBudgetAffectedColumns
    Transaction_Head = 1
    Basic_Salary = 2
    Other_Payroll_Cost = 3
    Both = 4
    Project_Code = 5 'Nilay (22 Nov 2016)
End Enum
'Sohail (07 Jun 2016) -- End

'Sohail (02 Sep 2016) -- Start
'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
Public Enum enUserPriviledge

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    AllowToAssignBenefitGroup = 53
    'Sohail (02 Sep 2019) -- Start
    'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
    '1. Set ED in pending status if user has not priviledge of ED approval
    '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
    '3. Send notifications to ED approvers for all employees ED in one email
    AllowToApproveEarningDeduction = 347
    'Sohail (02 Sep 2019) -- End
    AllowToViewFinalApplicantList = 600
    AllowToEditTimeSheetCard = 830
    AllowToAddEditDeleteGlobalTimeSheet = 831
    AllowToReCalculateTimings = 832
    AllowToRoundoffTimings = 833
    'Varsha Rana (17-Oct-2017) -- End


    AllowToAddBudgetCodes = 1029
    AllowToEditBudgetCodes = 1047
    AllowToDeleteBudgetCodes = 1048
    AllowToViewBudgetCodes = 1049
    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    AllowToCancelApprovedLoanApp = 1050
    'Nilay (20-Sept-2016) -- End
    AllowToApproveGoalsAccomplishment = 1051 'Shani (26-Sep-2016) 

    'Shani (08-Dec-2016) -- Start
    'Enhancement -  Add Employee Allocaion/Date Privilage
    AllowToChangeEmpRecategorize = 1052
    AllowToChangeEmpTransfers = 1053
    AllowToChangeEmpWorkPermit = 1054
    'Shani (08-Dec-2016) -- End

    'Pinkal (21-Oct-2016) -- Start
    'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
    AllowToMapBudgetTimesheetApprover = 1055
    'Pinkal (21-Oct-2016) -- End

    'Pinkal (21-Dec-2016) -- Start
    'Enhancement - Adding Swap Approver Privilage for Leave Module.
    AllowToSwapLeaveApprover = 1056
    'Pinkal (21-Dec-2016) -- End

    'Nilay (13 Apr 2017) -- Start
    'Enhancements: Settings for mandatory options in online recruitment
    AllowToViewSkillExpertise = 1057
    AllowToAddSkillExpertise = 1058
    AllowToEditSkillExpertise = 1059
    AllowToDeleteSkillExpertise = 1060
    'Nilay (13 Apr 2017) -- End


    'Pinkal (03-May-2017) -- Start
    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
    AllowToViewBudgetTimesheetApproverLevelList = 1061
    AllowToAddBudgetTimesheetApproverLevel = 1062
    AllowToEditBudgetTimesheetApproverLevel = 1063
    AllowToDeleteBudgetTimesheetApproverLevel = 1064

    AllowToViewBudgetTimesheetApproverList = 1065
    AllowToAddBudgetTimesheetApprover = 1066
    AllowToEditBudgetTimesheetApprover = 1067
    AllowToDeleteBudgetTimesheetApprover = 1068
    AllowToSetBudgetTimesheetApproverAsActive = 1069
    AllowToSetBudgetTimesheetApproverAsInactive = 1070

    AllowToViewEmployeeBudgetTimesheetList = 1071
    AllowToAddEmployeeBudgetTimesheet = 1072
    AllowToEditEmployeeBudgetTimesheet = 1073
    AllowToDeleteEmployeeBudgetTimesheet = 1074
    AllowToCancelEmployeeBudgetTimesheet = 1075
    AllowToViewPendingEmpBudgetTimesheetSubmitForApproval = 1076
    AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval = 1077
    AllowToSubmitForApprovalForPendingEmpBudgetTimesheet = 1078

    AllowToViewEmployeeBudgetTimesheetApprovalList = 1079
    AllowToChangeEmployeeBudgetTimesheetStatus = 1080

    AllowToMigrateBudgetTimesheetApprover = 1081

    AllowToImportEmployeeShiftAssignment = 1082

    'Pinkal (03-May-2017) -- End


    'Pinkal (23-AUG-2017) -- Start
    'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.
    AllowToEnableDisableADUser = 1083
    'Pinkal (23-AUG-2017) -- End

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges 
    'payroll module.
    AllowToImportEmployeeAccountConfiguration = 1084
    AllowToExportEmployeeAccountConfiguration = 1085
    AllowToGetFileFormatOfEmployeeAccountConfiguration = 1086
    'AllowToAddPayslipGlobalMessage = 1087
    'AllowToEditPayslipGlobalMessage = 1088
    'AllowToDeletePayslipGlobalMessage = 1089
    AllowToDeleteRehireEmployeeDetails = 1087
    AllowToPerformEligibleOperation = 1088
    AllowToPerformNotEligibleOperation = 1089

    AllowToAddLoanApproverLevel = 1090
    AllowToEditLoanApproverLevel = 1091
    AllowToDeleteLoanApproverLevel = 1092
    AllowToAddLoanApprover = 1093
    AllowToEditLoanApprover = 1094
    AllowToDeleteLoanApprover = 1095
    AllowToSetActiveInactiveLoanApprover = 1096
    AllowToViewLoanApproverLevelList = 1097
    AllowToViewLoanApproverList = 1098
    AllowToProcessGlobalLoanApprove = 1099
    AllowToProcessGlobalLoanAssign = 1100
    AllowToViewLoanApprovalList = 1101
    AllowToChangeLoanStatus = 1102
    AllowToTransferLoanApprover = 1103
    AllowToSwapLoanApprover = 1104
    AllowToImportLeaveApprovers = 1105
    AllowToImportLeavePlanner = 1106
    AllowToViewDeviceUserMapping = 1107
    AllowToImportEmployeeBirthInfo = 1108
    AllowToImportEmployeeAddress = 1109
    AllowToViewMissingEmployee = 1110
    AllowToUpdateEmployeeDetails = 1111
    AllowToUpdateEmployeeMovements = 1112
    AllowToViewAssignedShiftList = 1113
    AllowToDeleteAssignedShift = 1114
    AllowToImportCompanyAssets = 1115
    AllowToViewEligibleApplicants = 1116
    AllowToPerformComputeScoreProcess = 1117
    AllowToPerformVoidComputedScore = 1118
    AllowToEditAppraisalSetup = 1119
    AllowToDeleteAppraisalSetup = 1120
    AllowToImportTrainingEnrollment = 1121
    AllowToExportEmployeeList = 1122
    AllowToAddRemoveFields = 1123
    AllowToHistoricalSalaryChange = 1124
    AllowToPostNewProceedings = 1125
    AllowToEditViewProceedings = 1126
    AllowToScanAttachmentDocuments = 1127
    AllowToProceedingSubmitForApproval = 1128
    AllowToApproveDisapproveProceedings = 1129
    AllowToExemptTransactionHead = 1130
    AllowToPostTransactionHead = 1131
    AllowToViewDisciplineHead = 1132
    AllowToPrintPreviewDisciplineCharge = 1133
    AllowToAddDisciplinaryCommittee = 1134
    AllowToEditDeleteDisciplinaryCommittee = 1135
    AllowToViewDisciplineProceedingList = 1136
    AllowToViewDisciplineHearingList = 1137
    AllowToAddDisciplineHearing = 1138
    AllowToEditDisciplineHearing = 1139
    AllowToDeleteDisciplineHearing = 1140
    AllowToMarkAsClosedDisciplineHearing = 1141

    AllowToEditTransferEmployeeDetails = 1142
    AllowToDeleteTransferEmployeeDetails = 1143
    AllowToEditRecategorizeEmployeeDetails = 1144
    AllowToDeleteRecategorizeEmployeeDetails = 1145
    AllowToEditProbationEmployeeDetails = 1146
    AllowToDeleteProbationEmployeeDetails = 1147
    AllowToEditConfirmationEmployeeDetails = 1148
    AllowToDeleteConfirmationEmployeeDetails = 1149
    AllowToEditSuspensionEmployeeDetails = 1150
    AllowToDeleteSuspensionEmployeeDetails = 1151
    AllowToEditTerminationEmployeeDetails = 1152
    AllowToDeleteTerminationEmployeeDetails = 1153
    AllowToRehireEmployee = 1154
    AllowToEditWorkPermitEmployeeDetails = 1155
    AllowToDeleteWorkPermitEmployeeDetails = 1156

    AllowToEditRetiredEmployeeDetails = 1157
    AllowToDeleteRetiredEmployeeDetails = 1158

    AllowToGlobalVoidEmployeeMembership = 1159

    AllowToAddShiftPolicyAssignment = 1160
    AllowToAssignEmployeeDayOff = 1161
    AllowToViewEmployeeDayOff = 1162
    AllowToDeleteEmployeeDayOff = 1163
    AllowToViewAssignedPolicyList = 1164
    AllowToDeleteAssignedPolicy = 1165

    AllowToPerformGlobalGoalOperations = 1166
    AllowToPerformGlobalVoidGoals = 1167
    AllowToSubmitForGoalAccomplished = 1168
    AllowToImportEmployeeGoals = 1169
    AllowToPrintEmployeeScoreCard = 1170
    AllowToExportEmployeeScoreCard = 1171
    AllowToSetTranHeadInactive = 1172

    AllowToAddLeaveFrequency = 1173
    AllowToDeleteLeaveFrequency = 1174
    AllowToViewLeaveFrequency = 1175

    AllowToPerformLeaveAdjustment = 1176
    AllowToEditRehireEmployeeDetails = 1177
    'AllowToDeleteRehireEmployeeDetails = 1178

    'AllowToPerformEligibleOperation = 1179
    'AllowToPerformNotEligibleOperation = 1180


    'Varsha Rana (17-Oct-2017) -- End


    'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
    AllowToSeeEmployeeSignature = 1178
    'S.SANDEEP [11-AUG-2017] -- END

    'S.SANDEEP [16-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 118
    AllowtoAddEmployeeSignature = 1179
    AllowtoDeleteEmployeeSignature = 1180
    'S.SANDEEP [16-Jan-2018] -- END

    'S.SANDEEP [12-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#218|#ARUTI-106}
    AllowtoViewPendingAccrueLeaves = 1181
    'S.SANDEEP [12-Apr-2018] -- END


    'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .
    AllowToViewEmpApproverLevelList = 1182
    AllowToAddEmpApproverLevel = 1183
    AllowToEditEmpApproverLevel = 1184
    AllowToDeleteEmpApproverLevel = 1185

    AllowToViewEmployeeApproverList = 1186
    AllowToAddEmployeeApprover = 1187
    AllowToDeleteEmployeeApprover = 1188
    AllowToSetInActiveEmployeeApprover = 1189
    AllowToSetActiveEmployeeApprover = 1190

    AllowToPerformEmpSubmitForApproval = 1191
    AllowToApproveRejectEmployeeTransfers = 1192
    AllowToApproveRejectEmployeeRecategorize = 1193
    AllowToApproveRejectEmployeeProbation = 1194
    AllowToApproveRejectEmployeeConfirmation = 1195
    AllowToApproveRejectEmployeeSuspension = 1196
    AllowToApproveRejectEmployeeTermination = 1197
    AllowToApproveRejectEmployeeRetirements = 1198
    AllowToApproveRejectEmployeeWorkPermit = 1199
    AllowToApproveRejectEmployeeResidentPermit = 1200
    AllowToApproveRejectEmployeeCostCenter = 1201
    'S.SANDEEP [20-JUN-2018] -- End


    'Gajanan [13-AUG-2018] -- Start
    'Enhancement - Implementing Grievance Module.
    AllowToAddGrievanceApproverLevel = 1202
    AllowToEditGrievanceApproverLevel = 1203
    AllowToDeleteGrievanceApproverLevel = 1204
    AllowToViewGrievanceApproverLevel = 1205
    AllowToAddGrievanceApprover = 1206
    AllowToEditGrievanceApprover = 1207
    AllowToDeleteGrievanceApprover = 1208
    AllowToViewGrievanceApprover = 1209
    AllowToActivateGrievanceApprover = 1210
    AllowToInActivateGrievanceApprover = 1211
    AllowtoApproveGrievance = 1212

    AllowToViewGrievanceResolutionStepList = 1213
    AllowToAddGrievanceResolutionStep = 1214
    AllowToEditGrievanceResolutionStep = 1215
    AllowToDeleteGrievanceResolutionStep = 1216
    'Gajanan(13-AUG-2018) -- End

    'S.SANDEEP [09-OCT-2018] -- START
    AllowToAddTrainingApproverLevel = 1217
    AllowToEditTrainingApproverLevel = 1218
    AllowToDeleteTrainingApproverLevel = 1219
    AllowToViewTrainingApproverLevel = 1220
    AllowToAddTrainingApprover = 1221
    AllowToDeleteTrainingApprover = 1222
    AllowToViewTrainingApprover = 1223
    AllowToApproveTrainingRequisition = 1224
    AllowToActivateTrainingApprover = 1225
    AllowToDeactivateTrainingApprover = 1226
    'S.SANDEEP [09-OCT-2018] -- END

    'Hemant (22 Oct 2018) -- Start
    'Enhancement : Implementing New Module of OT Requisition Approver Level & Approver Master
    AllowToAddOTRequisitionApproverLevel = 1227
    AllowToEditOTRequisitionApproverLevel = 1228
    AllowToDeleteOTRequisitionApproverLevel = 1229
    AllowToViewOTRequisitionApproverLevel = 1230
    AllowToAddOTRequisitionApprover = 1231
    AllowToEditOTRequisitionApprover = 1232
    AllowToDeleteOTRequisitionApprover = 1233
    AllowToViewOTRequisitionApprover = 1234
    AllowToActivateOTRequisitionApprover = 1235
    AllowToInActivateOTRequisitionApprover = 1236
    AllowtoApproveOTRequisition = 1237
    'Hemant (22 Oct 2018) -- End


    'Pinkal (03-Dec-2018) -- Start
    'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
    AllowtoUnlockEmployeeForAssetDeclaration = 1238
    'Pinkal (03-Dec-2018) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    AllowToApproveRejectEmployeeQualifications = 1239
    AllowToApproveRejectEmployeeReferences = 1240
    AllowToApproveRejectEmployeeSkills = 1241
    AllowToApproveRejectEmployeeJobExperiences = 1242
    'Gajanan [17-DEC-2018] -- End

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    AllowToDeletePercentCompletedEmployeeGoals = 1243
    AllowToViewPercentCompletedEmployeeGoals = 1244
    'S.SANDEEP |12-FEB-2019| -- END

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    AllowToApproveRejectEmployeeDependants = 1245
    AllowToApproveRejectEmployeeIdentities = 1246
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [18-Mar-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'AllowToApproveRejectEmployeeMemberships = 1247
    'AllowToApproveRejectEmployeeBenefits = 1248
    AllowToApproveRejectAddress = 1247
    AllowToApproveRejectEmergencyAddress = 1248
    'Gajanan [18-Mar-2019] -- End

    'Sohail (12 Apr 2019) -- Start
    'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
    AllowToArchiveSystemErrorLog = 1249
    AllowToSendSystemErrorLogEmail = 1250
    AllowToExportSystemErrorLog = 1251
    AllowToViewSystemErrorLog = 1252
    'Sohail (12 Apr 2019) -- End

    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    AllowToApproveRejectEmployeeMemberships = 1253
    AllowToApproveRejectPersonalInfo = 1254
    'Gajanan [17-April-2019] -- End

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    AllowToSetDependentActive = 1255
    AllowToSetDependentInactive = 1256
    AllowToDeleteDependentStatus = 1257
    AllowToViewDependentStatus = 1258
    'Sohail (18 May 2019) -- End
    'Sohail (24 Jun 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
    AllowToViewJVPosting = 1259
    'Sohail (24 Jun 2019) -- End

'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    AllowToViewCalibrationApproverLevel = 1260
    AllowToAddCalibrationApproverLevel = 1261
    AllowToEditCalibrationApproverLevel = 1262
    AllowToDeleteCalibrationApproverLevel = 1263
    AllowToViewCalibrationApproverMaster = 1264
    AllowToAddCalibrationApproverMaster = 1265
    AllowToActivateCalibrationApproverMaster = 1266
    AllowToDeleteCalibrationApproverMaster = 1267
    AllowToDeActivateCalibrationApproverMaster = 1268
    AllowToApproveRejectCalibratedScore = 1269
    'S.SANDEEP |27-MAY-2019| -- END

  'Pinkal (27-Jun-2019) -- Start
  'Enhancement - IMPLEMENTING OT MODULE.
    AllowtoViewOTRequisitionApprovalList = 1270
  'Pinkal (27-Jun-2019) -- End

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    AllowToEditCalibratedScore = 1271
    'S.SANDEEP |27-JUL-2019| -- END


    'Pinkal (13-Aug-2019) -- Start
    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
    AllowToViewBudgetTimesheetExemptEmployeeList = 1272
    AllowToAddBudgetTimesheetExemptEmployee = 1273
    AllowToDeleteBudgetTimesheetExemptEmployee = 1274
    'Pinkal (13-Aug-2019) -- End
    'Sohail (29 Aug 2019) -- Start
    'NMB Payroll UAT # TC008 - 76.1 - System should be able to restrict batch posting when it is holiday unless it is activated by user to allow batch posting on holidays.
    AllowToPostJVOnHolidays = 1275
    'Sohail (29 Aug 2019) -- End

'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
    AllowtoCalibrateProvisionalScore = 1276
    'S.SANDEEP |16-AUG-2019| -- END
    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    AllowToSetBenefitAllocationActive = 1277
    AllowToSetBenefitAllocationInactive = 1278
    'Sohail (17 Sep 2019) -- End
    'Sohail (09 Oct 2019) -- Start
    'NMB Enhancement # : privilege  Allow to change date on Closed Period on user master to change eoc / termination date in paid period or closed period.
    AllowToChangeEOCLeavingDateOnClosedPeriod = 1279
    'Sohail (09 Oct 2019) -- End
    'Sohail (16 Oct 2019) -- Start
    'NMB Enhancement # : Add privileges for "Allow To Post Flexcube JV To Oracle" to prevent erroneous postings by other users to Payroll GL accounts.
    AllowToPostFlexcubeJVToOracle = 1280
    'Sohail (16 Oct 2019) -- End
    'Sohail (16 Oct 2019) -- Start
    'NMB Enhancement # : Privileges "Allow to view Paid Amount" to hide Amount on Approval/Authorize Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
    AllowToViewPaidAmount = 1281
    'Sohail (16 Oct 2019) -- End
    'Sohail (18 Oct 2019) -- Start
    'NMB Enhancement # : For Add/New/Edit employee system should allow to set appointment date to last closed period for It should be possible to set appointment date of an employee to past closed period by use of the first appointment date but during payroll process, consider 1st of the following month
    AllowToChangeAppointmentDateOnClosedPeriod = 1282
    'Sohail (18 Oct 2019) -- End
    'Gajanan [18-OCT-2019] -- Start    
    'Enhancement:Provide Privileges "allowto Add/edit Opertional privileges".
    AllowToAddEditOprationalPrivileges = 1283
    'Gajanan [18-OCT-2019] -- End
    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    AllowToApproveRejectEmployeeExemption = 1284
    AllowToSetEmployeeExemptionDate = 1285
    AllowToEditEmployeeExemptionDate = 1286
    AllowToDeleteEmployeeExemptionDate = 1287
    'Sohail (21 Oct 2019) -- End
    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : New Screen Training Need Form.
    AllowToViewTrainingNeedForm = 1288
    AllowToAddTrainingNeedForm = 1289
    AllowToEditTrainingNeedForm = 1290
    AllowToDeleteTrainingNeedForm = 1291
    'Sohail (14 Nov 2019) -- End


    'Pinkal (08-Jan-2020) -- Start
    'Enhancement - NMB - Working on NMB OT Requisition Requirement.
    AllowToViewEmpOTAssignment = 1292
    AllowToAddEmpOTAssignment = 1293
    AllowToDeleteEmpOTAssignment = 1294
    AllowToCancelEmpOTRequisition = 1295
    'Pinkal (08-Jan-2020) -- End
    'Sohail (10 Jan 2020) -- Start
    'NMB Enhancement # : New screen for Country list and allow to edit currency sign on country list.
    AllowToEditCurrencySign = 1296
    'Sohail (10 Jan 2020) -- End

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    AllowToUnlockEmployeeInPlanning = 1297
    AllowToUnlockEmployeeInAssessment = 1298
    'S.SANDEEP |18-JAN-2020| -- END
    'Sohail (04 Feb 2020) -- Start
    'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
    AllowtoUnlockEmployeeForNonDisclosureDeclaration = 1299
    'Sohail (04 Feb 2020) -- End


    'Pinkal (29-Jan-2020) -- Start
    'Enhancement - Changes related To OT NMB Testing.
    AllowToPostOTRequisitionToPayroll = 1300
    'Pinkal (29-Jan-2020) -- End


    'Pinkal (03-Mar-2020) -- Start
    'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
    AllowToMigrateOTRequisitionApprover = 1301
    'Pinkal (03-Mar-2020) -- End

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
    AllowToViewCalibratorList = 1302
    AllowToAddCalibrator = 1303
    AllowToMakeCalibratorActive = 1304
    AllowToDeleteCalibrator = 1305
    AllowToMakeCalibratorInactive = 1306
    AllowToEditCalibrator = 1307
    AllowToEditCalibrationApproverMaster = 1308
    'S.SANDEEP |01-MAY-2020| -- END
    
   'Hemant (03 Jun 2020) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
    AllowtoViewGlobalInterviewAnalysisList = 1309
    'Hemant (03 Jun 2020) -- End


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    AllowToInactiveEmployeeCostCenter = 1310
    AllowToInactiveCompanyAccountConfiguration = 1311
    AllowToInactiveEmployeeAccountConfiguration = 1312
    AllowToInactiveCostCenterAccountConfiguration = 1313
    'Gajanan [24-Aug-2020] -- End

    'Hemant (26 Nov 2020) -- Start
    'Enhancement : Talent Pipeline new changes
    AllowToAddPotentialTalentEmployee = 1314
    AllowToApproveRejectTalentPipelineEmployee = 1315
    AllowToMoveApprovedEmployeeToQulified = 1316
    AllowToViewPotentialTalentEmployee = 1317
    AllowToRemoveTalentProcess = 1318
    'Hemant (26 Nov 2020) -- End

    'Gajanan [17-Dec-2020] -- Start
    AllowToAddPotentialSuccessionEmployee = 1319
    AllowToApproveRejectSuccessionPipelineEmployee = 1320
    AllowToMoveApprovedEmployeeToQulifiedSuccession = 1321
    AllowToViewPotentialSuccessionEmployee = 1322
    AllowToRemoveSuccessionProcess = 1323
    'Gajanan [17-Dec-2020] -- End

    'Gajanan [19-Feb-2021] -- Start
    AllowToSendNotificationToTalentScreener = 1324
    AllowToSendNotificationToTalentEmployee = 1325
    AllowForTalentScreeningProcess = 1326

    AllowToSendNotificationToSuccessionScreener = 1327
    AllowToSendNotificationToSuccessionEmployee = 1328
    AllowForSuccessionScreeningProcess = 1329

    AllowToSendNotificationToTalentApprover = 1330
    AllowToSendNotificationToSuccessionApprover = 1331

    'Gajanan [19-Feb-2021] -- End
'Gajanan [30-JAN-2021] -- Start   
    'Enhancement:Worked On PDP Module
    AllowToViewLearningAndDevelopmentPlan = 1332
    AllowToAddPersonalAnalysisGoal = 1333
    AllowToEditPersonalAnalysisGoal = 1334
    AllowToDeletePersonalAnalysisGoal = 1335
    AllowToAddDevelopmentActionPlan = 1336
    AllowToEditDevelopmentActionPlan = 1337
    AllowToDeleteDevelopmentActionPlan = 1338
    AllowToAddUpdateProgressForDevelopmentActionPlan = 1339
    'Gajanan [30-JAN-2021] -- End

    'Gajanan [26-Feb-2021] -- Start
    AllowToNominateEmployeeForSuccession = 1340
    AllowToModifyTalentSetting = 1341
    AllowToViewTalentSetting = 1342
    AllowToModifySuccessionSetting = 1343
    AllowToViewSuccessionSetting = 1344
    'Gajanan [26-Feb-2021] -- End


    'Pinkal (03-Mar-2021)-- Start
    'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
    AllowToAdjustExpenseBalance = 1345
    'Pinkal (03-Mar-2021) -- End

    'Pinkal (10-Mar-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    AllowToViewRetireClaimApplication = 1346
    AllowToRetireClaimApplication = 1347
    AllowToEditRetiredClaimApplication = 1348
    AllowToViewRetiredApplicationApproval = 1349
    AllowToApproveRetiredApplication = 1350
    AllowToPostRetiredApplicationtoPayroll = 1351
    'Pinkal (10-Mar-2021) -- End

AllowToViewTalentScreeningDetail = 1352
    AllowToViewSuccessionScreeningDetail = 1353

    'Sohail (17 Feb 2021) -- Start
    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
    AllowToViewDepartmentalTrainingNeed = 1354
    AllowToAddDepartmentalTrainingNeed = 1355
    AllowToEditDepartmentalTrainingNeed = 1356
    AllowToDeleteDepartmentalTrainingNeed = 1357
    'Sohail (17 Feb 2021) -- End

    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    AllowToSubmitForApprovalFromDepartmentalTrainingNeed = 1358
    AllowToTentativeApproveForDepartmentalTrainingNeed = 1359
    AllowToSubmitForApprovalFromTrainingBacklog = 1360
    AllowToFinalApproveDepartmentalTrainingNeed = 1361
    AllowToRejectDepartmentalTrainingNeed = 1362
    AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed = 1363
    AllowToUnlockSubmittedForTrainingBudgetApproval = 1364
    AllowToAskForReviewTrainingAsPerAmountSet = 1365
    AllowToUndoApprovedTrainingBudgetApproval = 1366
    'Hemant (26 Mar 2021) -- End
    AllowToAddCommentForDevelopmentActionPlan = 1367
    'Sohail (12 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    AllowToUndoRejectedTrainingBudgetApproval = 1368
    'Sohail (12 Apr 2021) -- End
    AllowToViewActionPlanAssignedTrainingCourses = 1369

    AllowToUnlockPDPForm = 1370
    AllowToLockPDPForm = 1371
    AllowToUndoUpdateProgressForDevelopmentActionPlan = 1372

    'Hemant (15 Apr 2021) -- Start
    'NMB Enhancement : : Approvers concepts need to be revisited. Should be based on the concept of Allocations and no on employee assignment.
    AllowToApproveTrainingRequestRelatedToCost = 1373
    AllowToApproveTrainingRequestForeignTravelling = 1374
    'Hemant (15 Apr 2021) -- End
    'Sohail (26 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    AllowToViewBudgetSummaryForDepartmentalTrainingNeed = 1375
    AllowToSetMaxBudgetForDepartmentalTrainingNeed = 1376
    'Sohail (26 Apr 2021) -- End

    'Hemant (18 May 2021) -- Start
    'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
    AllowToApproveRejectTrainingCompletion = 1377
    AllowToAddAttendedTraining = 1378
    'Hemant (18 May 2021) -- End

    'Hemant (25 May 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
    AllowToMarkTrainingAsComplete = 1379
    'Hemant (25 May 2021) -- End

    'Sohail (06 Oct 2021) -- Start
    'NMB Enhancement : OLD-483 : Separate sender's email configuration on recruitment portal from company email.
    AllowToViewEmailSetup = 1380
    AllowToAddEmailSetup = 1381
    AllowToEditEmailSetup = 1382
    AllowToDeleteEmailSetup = 1383
    'Sohail (06 Oct 2021) -- End

End Enum


'Sohail (02 Sep 2016) -- End

'Nilay (20-Sept-2016) -- Start
'Enhancement : Cancel feature for approved but not assigned loan application
Public Enum enLoanApplicationStatus
    PENDING = 1
    APPROVED = 2
    REJECTED = 3
    ASSIGNED = 4
    CANCELLED = 5
End Enum
'Nilay (20-Sept-2016) -- End

'Nilay (04-Nov-2016) -- Start
'Enhancements: Global Change Status feature requested by {Rutta}
Public Enum enLoanAdvance
    LOAN = 1
    ADVANCE = 2
End Enum
'Nilay (04-Nov-2016) -- End

'Sohail (13 Dec 2016) -- Start
'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional).
Public Enum enLoanAdvanceSavindReportType
    LoanReport = 0
    AdvanceReport = 1
    SavingReport = 2
    LoanRegisterReport = 3
    LoanSchemeWithBankAccountReport = 4
    LoanHistoryReport = 5
    LoanByproduct = 6
    SavingByproduct = 7
    LoanRepaymentScheduleReport = 8
    'Nilay (08 Apr 2017) -- Start
    'Enhancements: New Loan Tracking Report for PACRA
    LoanTrackingReport = 9
    'Nilay (08 Apr 2017) -- End

End Enum
'Sohail (13 Dec 2016) -- End
'Sohail (24 Nov 2016) -- Start
'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
Public Enum enSelfServiceTheme
    Blue = 1
    Black = 2
    Green = 3
    Yellow = 4
    'Sohail (28 Mar 2018) -- Start
    'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
    Brown = 5
    'Sohail (28 Mar 2018) -- End
End Enum

Public Enum enSelfServiceLastView
    MainView = 0
    ReportView = 1
    FavouriteReportView = 2
    FavouriteMainView = 3
End Enum
'Sohail (24 Nov 2016) -- End
'Pinkal (21-Oct-2016) -- Start
'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
Public Enum enLeaveAccrueTenureSetting
    Yearly = 1
    Monthly = 2
End Enum
'Pinkal (21-Oct-2016) -- End
'Nilay (04-Nov-2016) -- Start
'Enhancements: Global Change Status feature requested by {Rutta}
Public Enum enStatusType
    OPEN = 1
    CLOSE = 2
End Enum
'Nilay (04-Nov-2016) -- End

'S.SANDEEP [25-JAN-2017] -- START
'ISSUE/ENHANCEMENT :
Public Enum enReviewerScoreSetting
    None = 0
    ReviewerScoreNeeded = 1
    ReviewerScoreNotNecessary = 2
End Enum
'S.SANDEEP [25-JAN-2017] -- END

'Sohail (27 Apr 2017) -- Start
'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
Public Enum enApplicantQualificationSortBy
    None = 0
    CompletionDate = 1
    HighestQualification = 2
End Enum
'Sohail (27 Apr 2017) -- End


'S.SANDEEP [20-JUN-2018] -- START
'ISSUE/ENHANCEMENT : {Ref#244}
Public Enum enApplyPrivilegeCondition
    APPLY_IN = 1
    APPLY_NOT_IN = 2
End Enum

Public Enum enEmpApproverType
    APPR_EMPLOYEE = 1
    APPR_MOVEMENT = 2
End Enum
'S.SANDEEP [20-JUN-2018] -- END


'S.SANDEEP [20-JUN-2018] -- Start
'Enhancement - Implementing Employee Approver Flow For NMB .

Public Enum enScreenName
    frmDependantsAndBeneficiariesList = 1
    frmQualificationsList = 2
    frmJobHistory_ExperienceList = 3
    frmEmployeeRefereeList = 4
    frmEmployee_Skill_List = 5
    frmAssignGroupBenefit = 6
    frmEmployeeBankList = 7
    frmAssetsRegisterList = 8
    frmLeaveAccrue_AddEdit = 9
    frmLeaveFrequencyList = 10
    frmScanOrAttachmentInfo = 11

    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    frmReportingToEmp = 12
    'Pinkal (18-Aug-2018) -- End


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    frmIdentityInfoList = 13
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [18-Mar-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    frmAddressList = 14
    frmEmergencyAddressList = 15
    'Gajanan [18-Mar-2019] -- End

    'S.SANDEEP |15-APR-2019| -- START
    frmMembershipInfoList = 16
    'S.SANDEEP |15-APR-2019| -- END

    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'frmPersonalinfo = 17
    frmBirthinfo = 17
    frmOtherinfo = 18
    'Gajanan [17-April-2019] -- End
    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    frmDependantStatus = 19
    'Sohail (18 May 2019) -- End

End Enum

'S.SANDEEP [20-JUN-2018] -- End

'S.SANDEEP [09-AUG-2018] -- START
'ISSUE/ENHANCEMENT : {Ref#292}
Public Enum enFlexcubeServiceInfo
    FLX_CUSTOMER_SRV = 1
    FLX_ACCOUNT_SRV = 2
    FLX_USER_SRV = 3
    'S.SANDEEP |11-APR-2019| -- START
    FLX_BLOCK_USER = 4
    'S.SANDEEP |11-APR-2019| -- END

    'S.SANDEEP |13-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : FlexCube {Exit Block User}
    FLX_EXBLK_USER = 5
    'S.SANDEEP |13-DEC-2019| -- END
End Enum
'S.SANDEEP [09-AUG-2018] -- END

'Pinkal (18-Aug-2018) -- Start
'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
Public Enum enADDisplaySettings
    FirstName_OtherName_Surname = 1
    SurName_OtherName_Firstname = 2
    FirstName_Surname = 3
    Surname_Firstname = 4
End Enum
'Pinkal (18-Aug-2018) -- End

'S.SANDEEP [01-OCT-2018] -- START
'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
Public Enum enGoalType
    GT_QUALITATIVE = 1
    GT_QUANTITATIVE = 2
End Enum
'S.SANDEEP [01-OCT-2018] -- END

'Gajanan [13-AUG-2018] -- Start
'Enhancement - Implementing Grievance Module.
Public Enum enGrievanceApproval
    UserAcess = 1
    ApproverEmpMapping = 2
    'Gajanan [24-OCT-2019] -- Start    
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change  
    ReportingTo = 3
    'Gajanan [24-OCT-2019] -- End


End Enum
'Gajanan [13-AUG-2018] -- End

'Gajanan [13-AUG-2018] -- Start
'Enhancement - Implementing Grievance Module.
Public Enum enGrievanceResponse
    AgreedAndClose = 1
    DisagreeAndEscalateToNextLevel = 2
    DisagreeAndClose = 3
    Appeal = 4
    'Gajanan [13-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
    Disagree = 5
    'Gajanan [13-July-2019] -- End

End Enum
'Gajanan [13-AUG-2018] -- End


'Gajanan (06 Oct 2018) -- Start
'Enhancement : Implementing New Module of Asset Declaration Template 2
Public Enum enAssetClientTypeT2
    Supplier = 1
    Borrowing = 2
    Others = 3
End Enum
'Gajanan(06 Oct 2018) -- End


'Pinkal (19-Oct-2018) -- Start
'Enhancement - Leave Enhancement for NMB.
Public Enum enTnAApproverType
    OT_Requisition_Approver = 1
    TnA_Approver = 2
End Enum
'Pinkal (19-Oct-2018) -- End

'S.SANDEEP [07-NOV-2018] -- START
Public Enum enTrainingType
    EXTERNAL = 1
    INTERNAL = 2
End Enum
'S.SANDEEP [07-NOV-2018] -- END

'Gajanan (06 Oct 2018) -- Start
Public Enum enOrganizationChartViewType
    EMPLOYEEWISE = 1
    JOBWISE = 2
    JOBTOJOB = 3
    INTERALLOCATION = 4
End Enum
'Gajanan (06 Oct 2018) -- End


'Pinkal (20-Nov-2018) -- Start
'Enhancement - Working on P2P Integration for NMB.
Public Enum enP2PExpenditureType
    None = 0
    Opex = 1
    Capex = 2
End Enum

'Pinkal (20-Nov-2018) -- End


'Hemant (26 Nov 2018) -- Start
'Enhancement : Changes As per Rutta Request for UAT of Asset Declaration Template 2
Public Enum enAssetDeclarationStatusType
    NOT_SUBMITTED = 1
    SUMBITTED = 2
    NOT_ATTEMPTED = 3
End Enum
'Hemant (26 Nov 2018) -- End

'Gajanan (26 Nov 2018) -- Start
'Enhancement :  Asset declaration Categorywise report Template2 For NMB(TC073)
Public Enum enAD_CategoriesT2
    BUSINESS_DEALINGS = 1
    STAFF_OWNERSHIP = 2
    BANK_ACCOUNTS = 3
    REAL_PROPERTIES = 4
    LIABILITIES = 5
    RELATIVES_IN_EMPLOYMENT = 6
    DEPENDANT_BUSINESS = 7
    DEPENDANT_SHARES = 8
    DEPENDANT_BANK = 9
    DEPENDANT_REAL_PROPERTIES = 10
End Enum
'Gajanan (26 Nov 2018) -- End


'Pinkal (26-Feb-2019) -- Start
'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
Public Enum enExpenseFrequencyCycle
    Financial_ELC = 1
    AppointmentDate = 2
End Enum
'Pinkal (26-Feb-2019) -- End

'S.SANDEEP |08-APR-2019| -- START
Public Enum enFiletype
    CUSTOMER = 1
    ACCOUNT = 2
    USER = 3
    'S.SANDEEP |13-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : FlexCube {Exit Block User}
    STAFF = 4
    'S.SANDEEP |13-DEC-2019| -- END
End Enum

Public Enum enRequestType
    ADD = 1
    MODIFY = 2
    ACTIVATE = 3
    DEACTIVATE = 4
End Enum

Public Enum enRequestForm
    EMPLOYEE = 1
    LEAVE = 2
End Enum
'S.SANDEEP |08-APR-2019| -- END
'Pinkal (01-Apr-2019) -- Start
'Enhancement - Working on Leave Changes for NMB.

Public Enum enLeaveEscalationOptions
    None = 0
    Approve_LeaveForm = 1
    Reject_LeaveForm = 2
    Recurrent_Reminder = 3
End Enum

'Pinkal (01-Apr-2019) -- End

'Gajanan [28-AUG-2019] -- Start      
'ISSUE/ENHANCEMENT : Suspended Employee Report
Public Enum enTerminatedEmployeeReportType
    TerminatedEmployee = 1
    SuspensionEmployee = 2
End Enum

'Gajanan [28-AUG-2019] -- End

'Gajanan [29-AUG-2019] -- Start      
Public Enum enYesno
    None = 0
    Yes = 1
    No = 2
End Enum
'Gajanan [29-AUG-2019] -- End

'Gajanan [09-SEP-2019] -- Start      
'Enhancements: ENHANCEMENTS FOR NMB
Public Enum enVoidCategoryType
    EMPLOYEE = 1
    DISCIPLINE = 2
    ASSESSMENT = 3
    TRAINING = 4
    PAYROLL = 5
    MEDICAL = 6
    LEAVE = 7
    TNA = 8
    LOAN = 9
    SAVINGS = 10
    APPLICANT = 11
    INTERVIEW = 12
    VACANCY = 13
    OTHERS = 14
    PAYMENT_DISAPPROVAL = 15
    STAFF_REQUISITION = 16
End Enum
'Gajanan [09-SEP-2019] -- END      



'Gajanan [23-SEP-2019] -- Start    
'Enhancement:Enforcement of approval migration From Transfer and recategorization.
Public Enum enApprovalMigration
    Leave = 1
    Claim = 2
    Loan = 3
    BudgetTimesheet = 4
    Performance = 5
    ReportingTo = 6
    OTRequisition = 7
End Enum

Public Enum enApprovalMigrationScreenType
    TRANSFER = 1
    RECATEGORIZATION = 2
End Enum


'Gajanan [23-SEP-2019] -- End

'Pinkal (11-Sep-2019) -- Start
'Enhancement NMB - Working On Claim Retirement for NMB.
Public Enum enClaimRetirementType
    General_Retirement = 1
    ExpenseWise_Retirement = 2
End Enum
'Pinkal (11-Sep-2019) -- End

'Hemant (15 Nov 2019) -- Start
'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
Public Enum enJob_Report_Template
    Job_Report = 1
    Job_Listing_Report = 2
End Enum
'Hemant (15 Nov 2019) -- End

'Gajanan [24-OCT-2019] -- Start   
'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
Public Enum enGrievanceResolutionSatus
    Pending = 1
    Resolve = 2
End Enum
'Gajanan [24-OCT-2019] -- End


'Gajanan [9-Dec-2019] -- Start   
'Enhancement:Worked On Orbit Integration Configuration Setup
Public Enum enOrbitServiceInfo
    ORB_CREATECUSTOMER = 1
    ORB_CREATEDEPOSITACCOUNT = 2
    ORB_FINDCUSTOMERSINFORMATION = 3
    ORB_PROCESSBULKPAYMENTS = 4
    'S.SANDEEP |09-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : ARUTI - ORBIT INTEGRATION
    ORB_FETCH_DICTIONARY = 5
    'S.SANDEEP |09-JAN-2020| -- END
End Enum
'Gajanan [9-Dec-2019] -- End

'Gajanan [30-JAN-2021] -- Start   
'Enhancement:Worked On PDP Module
Public Enum enPDPActionPlanStatus
    Pending = 1
    Initiated = 2
    InProgress = 3
    Rotting = 4
    Stopped = 5
    OnEvaluation = 6
    Completed = 7
    Achieved = 8
End Enum
'Gajanan [30-JAN-2021] -- End

Public Enum enScreeningFilterType
    ALL = 1
    ONLYQUALIFIED = 2
    ONLYMANUALLYADDED = 3
End Enum

'Hemant (01 Feb 2021) -- Start
'Enhancement : New Training Module Enhancement
Public Enum enTrainingRequestStatus
    PENDING = 1
    APPROVED = 2
    REJECTED = 3
    ASSIGNED = 4
    CANCELLED = 5
End Enum
'Hemant (01 Feb 2021) -- End


Public Enum enPDPCustomItemViewType
    Table = 1
    List = 2
End Enum


'Gajanan [18-May-2021] -- Start
Public Enum enPDPFormStatusType
    Planned = 1
    NotPlanned = 2
    SubmittedForApproval = 3
    NotSubmittedForApproval = 4
End Enum
'Gajanan [18-May-2021] -- End

Public Enum enPDPViewType
    Selection = 1
    FreeText = 2
End Enum

  

'Sohail (01 Mar 2021) -- Start
'NMB Enhancement : : New screen Departmental Training Need in New UI.
Public Enum enTrainingTargetedGroup
    Pending = 1
    Initiated = 2
    InProgress = 3
    Rotting = 4
    Stopped = 5
    OnEvaluation = 6
    Completed = 7
    Achieved = 8
End Enum
'Sohail (01 Mar 2021) -- End

'Hemant (15 Apr 2021) -- Start
'NMB Enhancement : : Approvers concepts need to be revisited. Should be based on the concept of Allocations and no on employee assignment.
Public Enum enTrainingApproverSettings
    Training_Request_Cost = 1
    Training_Request_Foreign_Travelling = 2
End Enum
'Hemant (15 Apr 2021) -- End

'Sohail (10 Feb 2022) -- Start
'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
Public Enum enTrainingRemainingBalanceBasedOn
    Approved_Amount = 1
    Enrolled_Amount = 2
End Enum
'Sohail (10 Feb 2022) -- End


'Pinkal (01-Jun-2021)-- Start
'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
Public Enum enDashboardItems
    Crd_TodayBirthday = 1
    Crd_PendingTask = 2
    Crd_EmpOnLeave = 3
    Crd_WorkAnniversary = 4
    Crd_UpcomingHolidays = 5
    Crd_TeamMembers = 6
    Crd_NewlyHired = 7
    Crd_StaffTurnOver = 8
    Crd_LeaveAnalysis = 9
End Enum
'Pinkal (01-Jun-2021) -- End

'Hemant (01 Feb 2022) -- Start
'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
Public Enum enAuthenticationProtocol
    NONE = 0
    TLS = 1
    SSL3 = 2
    TLS12 = 3
End Enum
'Hemant (01 Feb 2022) -- End
