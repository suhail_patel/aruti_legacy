﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using Microsoft.Win32;
using System.Data.SqlClient;
using System.Xml;
using System.Net;
using System.Xml.Linq;
using System.DirectoryServices;
using System.Data.OracleClient;

namespace Flexcube_Integration
{
    public partial class ArutiFlexcube : ServiceBase
    {
        public ArutiFlexcube()
        {
            InitializeComponent();
        }

        #region Enums

        private enum enFlexcubeServiceInfo
        {
            FLX_CUSTOMER_SRV = 1,
            FLX_ACCOUNT_SRV = 2,
            FLX_USER_SRV = 3,
            //S.SANDEEP |11-APR-2019| -- START
            FLX_BLOCK_USER = 4
            //S.SANDEEP |11-APR-2019| -- END            
        }

        private enum enFiletype
        {
            CUSTOMER = 1,
            ACCOUNT = 2,
            USER = 3
        }

        private enum enRequestType
        {
            ADD = 1,
            MODIFY = 2,
            ACTIVATE = 3,
            DEACTIVATE = 4
        }

        //S.SANDEEP |08-APR-2019| -- START
        private enum enRequestForm
        {
            EMPLOYEE = 1,
            LEAVE = 2
        }
        //S.SANDEEP |08-APR-2019| -- END

        public enum enEmp_Dates_Transaction
        {
            DT_PROBATION = 1,
            DT_CONFIRMATION = 2,
            DT_SUSPENSION = 3,
            DT_TERMINATION = 4,
            DT_REHIRE = 5,
            DT_RETIREMENT = 6,
            DT_APPOINTED_DATE = 7,
            DT_BIRTH_DATE = 8,
            DT_FIRST_APP_DATE = 9,
            DT_MARRIGE_DATE = 10
        }

        #endregion

        #region Private Variables

        Timer timer = new Timer();
        private const string conREG_NODE = "Software\\NPK\\Aruti";
        SqlConnection sqlCn = null;
        SqlCommand sqlCmd = null;
        bool mblnIsDatabaseAccessible;
        private const long MILLISECOND_IN_MINUTE = 60 * 1000;
        private const long TICKS_IN_MILLISECOND = 10000;
        private const long TICKS_IN_MINUTE = MILLISECOND_IN_MINUTE * TICKS_IN_MILLISECOND;
        private long nextIntervalTick;

        #endregion

        #region Service Methods

        protected override void OnStart(string[] args)
        {
            WriteToFile("Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 60000; //number in milisecinds  
            //timer.Interval = GetInitialInterval();
            timer.Enabled = true;
        }
        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
            timer.Stop();
        }

        //S.SANDEEP |04-NOV-2019| -- START
        //ISSUE/ENHANCEMENT : FLEX OBJECT REF. ERROR
        //private void OnElapsedTime(object source, ElapsedEventArgs e)
        //{
        //    try
        //    {
        //        //System.Diagnostics.Debugger.Launch();
        //        timer.Enabled = false;
        //        string value = convertTime(DateTime.Now).ToString().Substring(0, 5);
        //        if (IsCorrectTime(value))
        //        {
        //            if (IsConnect() == false) { return; }
        //            DataTable dtCList = new DataTable();
        //            dtCList = IsFlexcubeIntegrated();
        //            if (dtCList.Rows.Count > 0)
        //            {
        //                foreach (DataRow dr in dtCList.Rows)
        //                {
        //                    mblnIsDatabaseAccessible = false;
        //                    IsDatabaseAccessible(dr["database_name"].ToString());
        //                    if (mblnIsDatabaseAccessible == false) { continue; }


        //                    DataTable dtParams = new DataTable();
        //                    dtParams = GetFlexcubeParameters(Convert.ToInt32(dr["companyunkid"]));
        //                    Dictionary<int, string> mdicFlexcubeServiceCollection = new Dictionary<int, string>();
        //                    string strAccCategory = ""; string strAccClass = ""; string strEmpAsOnDate = ""; string strExportPath = "";
        //                    if (dtParams.Rows.Count > 0)
        //                    {
        //                        foreach (DataRow dtrow in dtParams.Rows)
        //                        {
        //                            if (dtrow["key_name"].ToString().ToUpper().StartsWith("_FLXSRV_") == true)
        //                            {
        //                                int intKey = Convert.ToInt32(dtrow["key_name"].ToString().ToUpper().Replace("_FLXSRV_", ""));
        //                                if (mdicFlexcubeServiceCollection.ContainsKey(intKey) == false)
        //                                {
        //                                    mdicFlexcubeServiceCollection.Add(intKey, dtrow["key_value"].ToString());
        //                                }
        //                            }
        //                            if (dtrow["key_name"].ToString().ToUpper() == "FLEXCUBEACCOUNTCATEGORY") { strAccCategory = dtrow["key_value"].ToString(); }
        //                            if (dtrow["key_name"].ToString().ToUpper() == "FLEXCUBEACCOUNTCLASS") { strAccClass = dtrow["key_value"].ToString(); }
        //                            if (dtrow["key_name"].ToString().ToUpper() == "EMPLOYEEASONDATE") { strEmpAsOnDate = dtrow["key_value"].ToString(); }
        //                            if (dtrow["key_name"].ToString().ToUpper() == "EXPORTDATAPATH") { strExportPath = dtrow["key_value"].ToString(); }
        //                        }
        //                    }






        //                    //foreach (var item in Enum.GetValues(typeof(enFlexcubeServiceInfo)))
        //                    //{
        //                    if (strEmpAsOnDate.Trim().Length <= 0) { strEmpAsOnDate = convertDate(DateTime.Now).ToString(); }
        //                    DataTable dtEmp = new DataTable();
        //                    dtEmp = GetEmployeeList(dr["database_name"].ToString(), strAccCategory, strAccClass, strEmpAsOnDate, enFlexcubeServiceInfo.FLX_CUSTOMER_SRV);
        //                    if (dtEmp.Rows.Count > 0)
        //                    {
        //                        foreach (DataRow row in dtEmp.Rows)
        //                        {
        //                            string strBuilder, strPrefix;
        //                            strPrefix = "CUST_";
        //                            string oStrMsgId = "";
        //                            string oResponseData = "";
        //                            string strFileName = "";
        //                            bool blnerror = false;
        //                            string strErrorDesc = "";
        //                            string strCustNo = "";

        //                            strBuilder = GenerateFileString(enFiletype.CUSTOMER, strAccCategory, strAccClass, strEmpAsOnDate, row, dr["database_name"].ToString(), ref oStrMsgId);



        //                            if (strBuilder.Trim().Length > 0)
        //                            {
        //                                oResponseData = PostData(strBuilder, mdicFlexcubeServiceCollection[(int)enFlexcubeServiceInfo.FLX_CUSTOMER_SRV], ref strErrorDesc);




        //                                if (oResponseData.Trim().Length > 0)
        //                                {
        //                                    DataSet ds = new DataSet();
        //                                    ds.ReadXml(new System.IO.StringReader(oResponseData));











        //                                    //S.SANDEEP |04-NOV-2019| -- START
        //                                    //ISSUE/ENHANCEMENT : FLEX OBJECT REF. ERROR
        //                                    //if (ds.Tables["FCUBS_HEADER"].Rows.Count > 0)
        //                                    //{
        //                                    //    if (ds.Tables["FCUBS_HEADER"].Rows[0]["MSGSTAT"].ToString() != "SUCCESS")
        //                                    //    {
        //                                    //        if (ds.Tables.Contains("ERROR") == true)
        //                                    //        {
        //                                    //            blnerror = true;
        //                                    //            if (ds.Tables["ERROR"].Rows.Count > 0)
        //                                    //            {
        //                                    //                strErrorDesc = String.Join(",", ds.Tables["ERROR"].AsEnumerable().Select(x => x.Field<string>("ECODE").ToString() + " -> " + x.Field<string>("EDESC")).ToArray());
        //                                    //            }
        //                                    //        }
        //                                    //    }
        //                                    //    else
        //                                    //    {
        //                                    //        if (ds.Tables.Contains("Customer-Full") == true)
        //                                    //        {
        //                                    //            if (ds.Tables["Customer-Full"].Rows.Count > 0)
        //                                    //            {
        //                                    //                strCustNo = ds.Tables["Customer-Full"].Rows[0]["CUSTNO"].ToString();
        //                                    //            }
        //                                    //        }
        //                                    //    }
        //                                    //}
        //                                    if (ds.Tables.Contains("FCUBS_HEADER") == true)
        //                                    {
        //                                        if (ds.Tables["FCUBS_HEADER"].Rows.Count > 0)
        //                                        {
        //                                            if (ds.Tables["FCUBS_HEADER"].Rows[0]["MSGSTAT"].ToString() != "SUCCESS")
        //                                            {
        //                                                if (ds.Tables.Contains("ERROR") == true)
        //                                                {
        //                                                    blnerror = true;
        //                                                    if (ds.Tables["ERROR"].Rows.Count > 0)
        //                                                    {
        //                                                        strErrorDesc = String.Join(",", ds.Tables["ERROR"].AsEnumerable().Select(x => x.Field<string>("ECODE").ToString() + " -> " + x.Field<string>("EDESC")).ToArray());
        //                                                    }
        //                                                }
        //                                            }
        //                                            else
        //                                            {
        //                                                if (ds.Tables.Contains("Customer-Full") == true)
        //                                                {
        //                                                    if (ds.Tables["Customer-Full"].Rows.Count > 0)
        //                                                    {
        //                                                        strCustNo = ds.Tables["Customer-Full"].Rows[0]["CUSTNO"].ToString();
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                    }                                            
        //                                    //S.SANDEEP |04-NOV-2019| -- END

        //                                }
        //                                if (strErrorDesc.Trim().Length > 0) { blnerror = true; }
        //                                strFileName = strPrefix + row["employeecode"].ToString() + "_" + DateTime.Now.ToString("yyyymmdd") + ".xml";
        //                                //S.SANDEEP |08-APR-2019| -- START
        //                                //InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.ADD, (int)enFiletype.CUSTOMER, strFileName, strBuilder, oResponseData, strCustNo, oStrMsgId, blnerror, strErrorDesc, dr["database_name"].ToString());
        //                                InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.ADD, (int)enFiletype.CUSTOMER, strFileName, strBuilder, oResponseData, strCustNo, oStrMsgId, blnerror, strErrorDesc, dr["database_name"].ToString(), (int)enRequestForm.EMPLOYEE, row["employeecode"].ToString());
        //                                //S.SANDEEP |08-APR-2019| -- END     


        //                            }

        //                            /* START FOR ACCOUNT  IT'S RELATION TO ABOVE CUSTOMER CREATION */
        //                            if (blnerror == false)
        //                            {



        //                                strPrefix = "ACCT_";
        //                                oResponseData = "";
        //                                strFileName = "";
        //                                blnerror = false;
        //                                strErrorDesc = "";
        //                                strBuilder = "";//S.SANDEEP |08-APR-2019| -- START -- END


        //                                strBuilder = GenerateFileString(enFiletype.ACCOUNT, strAccCategory, strAccClass, strEmpAsOnDate, row, dr["database_name"].ToString(), ref oStrMsgId);









        //                                if (strBuilder.Trim().Length > 0)
        //                                {
        //                                    oResponseData = PostData(strBuilder, mdicFlexcubeServiceCollection[(int)enFlexcubeServiceInfo.FLX_ACCOUNT_SRV], ref strErrorDesc);



        //                                    if (oResponseData.Trim().Length > 0)
        //                                    {
        //                                        DataSet ds = new DataSet();
        //                                        ds.ReadXml(new System.IO.StringReader(oResponseData));











        //                                        //S.SANDEEP |04-NOV-2019| -- START
        //                                        //ISSUE/ENHANCEMENT : FLEX OBJECT REF. ERROR
        //                                        //if (ds.Tables["FCUBS_HEADER"].Rows.Count > 0)
        //                                        //{
        //                                        //    if (ds.Tables["FCUBS_HEADER"].Rows[0]["MSGSTAT"].ToString() != "SUCCESS")
        //                                        //    {
        //                                        //        if (ds.Tables.Contains("ERROR") == true)
        //                                        //        {
        //                                        //            blnerror = true;
        //                                        //            if (ds.Tables["ERROR"].Rows.Count > 0)
        //                                        //            {
        //                                        //                strErrorDesc = String.Join(",", ds.Tables["ERROR"].AsEnumerable().Select(x => x.Field<string>("ECODE").ToString() + " -> " + x.Field<string>("EDESC")).ToArray());
        //                                        //            }
        //                                        //        }
        //                                        //    }
        //                                        //    else
        //                                        //    {
        //                                        //        if (ds.Tables.Contains("Customer-Full") == true)
        //                                        //        {
        //                                        //            if (ds.Tables["Customer-Full"].Rows.Count > 0)
        //                                        //            {
        //                                        //                strCustNo = ds.Tables["Customer-Full"].Rows[0]["CUSTNO"].ToString();
        //                                        //            }
        //                                        //        }
        //                                        //    }
        //                                        //}
        //                                        if (ds.Tables.Contains("FCUBS_HEADER") == true)
        //                                        {
        //                                            if (ds.Tables["FCUBS_HEADER"].Rows.Count > 0)
        //                                            {
        //                                                if (ds.Tables["FCUBS_HEADER"].Rows[0]["MSGSTAT"].ToString() != "SUCCESS")
        //                                                {
        //                                                    if (ds.Tables.Contains("ERROR") == true)
        //                                                    {
        //                                                        blnerror = true;
        //                                                        if (ds.Tables["ERROR"].Rows.Count > 0)
        //                                                        {
        //                                                            strErrorDesc = String.Join(",", ds.Tables["ERROR"].AsEnumerable().Select(x => x.Field<string>("ECODE").ToString() + " -> " + x.Field<string>("EDESC")).ToArray());
        //                                                        }
        //                                                    }
        //                                                }
        //                                                else
        //                                                {
        //                                                    if (ds.Tables.Contains("Customer-Full") == true)
        //                                                    {
        //                                                        if (ds.Tables["Customer-Full"].Rows.Count > 0)
        //                                                        {
        //                                                            strCustNo = ds.Tables["Customer-Full"].Rows[0]["CUSTNO"].ToString();
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                        }                                                
        //                                        //S.SANDEEP |04-NOV-2019| -- END

        //                                    }
        //                                    if (strErrorDesc.Trim().Length > 0) { blnerror = true; }
        //                                    strFileName = strPrefix + row["employeecode"].ToString() + "_" + DateTime.Now.ToString("yyyymmdd") + ".xml";
        //                                    InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.ADD, (int)enFiletype.ACCOUNT, strFileName, strBuilder, oResponseData, strCustNo, oStrMsgId, blnerror, strErrorDesc, dr["database_name"].ToString(), (int)enRequestForm.EMPLOYEE, row["employeecode"].ToString());

        //                                }
        //                            }
        //                            /* START FOR USER CREATION */

        //                        }
        //                    }
        //                    //}
        //                    //S.SANDEEP |08-APR-2019| -- START
        //                    //S.SANDEEP |11-APR-2019| -- START
        //                    //if (mdicFlexcubeServiceCollection.ContainsKey((int)enFlexcubeServiceInfo.FLX_USER_SRV) == true)


        //                    if (mdicFlexcubeServiceCollection.ContainsKey((int)enFlexcubeServiceInfo.FLX_BLOCK_USER) == true)
        //                    //S.SANDEEP |11-APR-2019| -- END
        //                    {
        //                        dtEmp = GetLeaveData(dr["database_name"].ToString());

        //                        if (dtEmp.Rows.Count > 0)
        //                        {
        //                            Dictionary<int, int> empprocessed = new Dictionary<int, int>();
        //                            foreach (DataRow row in dtEmp.Rows)
        //                            {
        //                                if (empprocessed.ContainsKey(Convert.ToInt32(row["employeeunkid"].ToString())) == true) { continue; }
        //                                empprocessed.Add(Convert.ToInt32(row["employeeunkid"].ToString()), Convert.ToInt32(row["employeeunkid"].ToString()));
        //                                string strBuilder, strPrefix;
        //                                strPrefix = "UBLK_";
        //                                string oStrMsgId = "";
        //                                string strFileName = "";
        //                                bool blnerror = false;
        //                                string oResponseData = "";
        //                                string strErrorDesc = "";
        //                                DataTable flxusr = null;
        //                                string accnum = String.Join("','", dtEmp.AsEnumerable().Where(x => x.Field<int>("employeeunkid") == Convert.ToInt32(row["employeeunkid"])).Select(y => y.Field<string>("accno")).ToArray());
        //                                if (accnum.Trim().Length > 0) { accnum = "'" + accnum + "'"; }
        //                                if (accnum.Trim().Length > 0)
        //                                {

        //                                    flxusr = GetFlexUserDetails(accnum, Convert.ToInt32(dr["companyunkid"]));
        //                                    if (flxusr != null && flxusr.Rows.Count > 0)
        //                                    {

        //                                        //sDate
        //                                        //S.SANDEEP |29-APR-2019| -- START
        //                                        //strBuilder = GenerateCBSFileString(dr["database_name"].ToString(), flxusr.Rows[0]["USER_ID"].ToString(), flxusr.Rows[0]["USER_NAME"].ToString(), row["customcode"].ToString(), "D", row["startdate"].ToString(), ref oStrMsgId, flxusr.Rows[0]["BRANCH"].ToString(), flxusr.Rows[0]["time_level"].ToString());                                                                                                
        //                                        strBuilder = GenerateCBSFileString(dr["database_name"].ToString(), flxusr.Rows[0]["USER_ID"].ToString(), flxusr.Rows[0]["USER_NAME"].ToString(), row["customcode"].ToString(), "D", Convert.ToDateTime(flxusr.Rows[0]["start_date"]).ToString("yyyy-MM-dd"), ref oStrMsgId, flxusr.Rows[0]["BRANCH"].ToString(), flxusr.Rows[0]["time_level"].ToString());
        //                                        //'S.SANDEEP |29-APR-2019| -- END
        //                                        if (strBuilder.Trim().Length > 0)
        //                                        {

        //                                            //S.SANDEEP |11-APR-2019| -- START
        //                                            //oResponseData = PostData(strBuilder, mdicFlexcubeServiceCollection[(int)enFlexcubeServiceInfo.FLX_USER_SRV], ref strErrorDesc);
        //                                            oResponseData = PostData(strBuilder, mdicFlexcubeServiceCollection[(int)enFlexcubeServiceInfo.FLX_BLOCK_USER], ref strErrorDesc);
        //                                            //S.SANDEEP |11-APR-2019| -- END


        //                                            //S.SANDEEP |24-APR-2019| -- START
        //                                            if (oResponseData.Trim().Length > 0)
        //                                            {
        //                                                DataSet ds = new DataSet();
        //                                                ds.ReadXml(new System.IO.StringReader(oResponseData));
        //                                                //S.SANDEEP |04-NOV-2019| -- START
        //                                                //ISSUE/ENHANCEMENT : FLEX OBJECT REF. ERROR
        //                                                //if (ds.Tables["FCUBS_HEADER"].Rows.Count > 0)
        //                                                //{
        //                                                //    if (ds.Tables["FCUBS_HEADER"].Rows[0]["MSGSTAT"].ToString() != "SUCCESS")
        //                                                //    {
        //                                                //        if (ds.Tables.Contains("ERROR") == true)
        //                                                //        {
        //                                                //            blnerror = true;
        //                                                //            if (ds.Tables["ERROR"].Rows.Count > 0)
        //                                                //            {
        //                                                //                strErrorDesc = String.Join(",", ds.Tables["ERROR"].AsEnumerable().Select(x => x.Field<string>("ECODE").ToString() + " -> " + x.Field<string>("EDESC")).ToArray());
        //                                                //            }
        //                                                //        }
        //                                                //    }
        //                                                //}
        //                                                if (ds.Tables.Contains("FCUBS_HEADER") == true)
        //                                                {
        //                                                    if (ds.Tables["FCUBS_HEADER"].Rows.Count > 0)
        //                                                    {
        //                                                        if (ds.Tables["FCUBS_HEADER"].Rows[0]["MSGSTAT"].ToString() != "SUCCESS")
        //                                                        {
        //                                                            if (ds.Tables.Contains("ERROR") == true)
        //                                                            {
        //                                                                blnerror = true;
        //                                                                if (ds.Tables["ERROR"].Rows.Count > 0)
        //                                                                {
        //                                                                    strErrorDesc = String.Join(",", ds.Tables["ERROR"].AsEnumerable().Select(x => x.Field<string>("ECODE").ToString() + " -> " + x.Field<string>("EDESC")).ToArray());
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }                                                        
        //                                                //S.SANDEEP |04-NOV-2019| -- END                                                        
        //                                            }
        //                                            //S.SANDEEP |24-APR-2019| -- END

        //                                            if (strErrorDesc.Trim().Length <= 0)
        //                                            {
        //                                                DataSet ds = new DataSet();
        //                                                ds.ReadXml(new System.IO.StringReader(oResponseData));
        //                                            }
        //                                            else
        //                                            {
        //                                                blnerror = true;
        //                                            }
        //                                            strFileName = strPrefix + row["formno"].ToString() + "_" + DateTime.Now.ToString("yyyymmdd") + ".xml";
        //                                            InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.DEACTIVATE, (int)enFiletype.USER, strFileName, strBuilder, oResponseData, "", oStrMsgId, blnerror, strErrorDesc, dr["database_name"].ToString(), (int)enRequestForm.LEAVE, row["formno"].ToString());

        //                                            //S.SANDEEP |29-APR-2019| -- START
        //                                            //CHANGES MADE AS PER ZEESHAN'S COMMENTS FOR 2 TAGS
        //                                            //TIMELEVEL -->  TO PICK FROM FLX USER TABLE VIEW COL.NAME [time_level]
        //                                            //STRTDATE  -->  TO CHANGE THE FORMAT OLD [DD-MMM-YYYY] TO NEW [YYYY-MM-DD]
        //                                            //S.SANDEEP |29-APR-2019| -- END
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.DEACTIVATE, (int)enFiletype.USER, "", "", "", "", oStrMsgId, true, "Flexcube user not found", dr["database_name"].ToString(), (int)enRequestForm.LEAVE, row["formno"].ToString());
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.DEACTIVATE, (int)enFiletype.USER, "", "", "", "", oStrMsgId, true, "Account number not assigned to employee", dr["database_name"].ToString(), (int)enRequestForm.LEAVE, row["formno"].ToString());
        //                                }
        //                            }
        //                        }
        //                    }                            
        //                    //S.SANDEEP |08-APR-2019| -- END
        //                }
        //            }
        //        }

        //        ///*
        //        ///*  START FOR CALLING ACTIVE DIRECTORY SERVICE */

        //        //if (IsConnect() == false) { return; }

        //        //string mstrADIPAddress = "", mstrADDomain = "", mstrADDomainUser = "", mstrADUserPwd = "";

        //        //DataTable dtDatabase = GetCurrentDBNameWIthADSettings(ref mstrADIPAddress, ref mstrADDomain, ref mstrADDomainUser, ref mstrADUserPwd);

        //        //if (dtDatabase != null && dtDatabase.Rows.Count > 0)
        //        //{
        //        //    foreach (DataRow drRow in dtDatabase.Rows)
        //        //    {
        //        //        DataTable dtEmployee = GetEmployeeDeptJobOnEffectiveDate(drRow["database_name"].ToString(), convertDate(DateTime.Now));

        //        //        if (dtEmployee != null && dtEmployee.Rows.Count > 0)
        //        //        {
        //        //            foreach (DataRow dRow in dtEmployee.Rows)
        //        //            {
        //        //                SearchResult result = IsADUserExist(dRow["displayname"].ToString(), mstrADIPAddress, mstrADDomain, mstrADDomainUser, mstrADUserPwd);
        //        //                if (result != null)
        //        //                {
        //        //                    DirectoryEntry entry = new DirectoryEntry(result.Path);

        //        //                    if (entry.Properties["department"].Value.ToString().Trim() != dRow["Department"].ToString().Trim())
        //        //                    {
        //        //                        SetADProperty(entry, "department", dRow["Department"].ToString());
        //        //                    }

        //        //                    if (entry.Properties["title"].Value.ToString().Trim() != dRow["Job"].ToString().Trim())
        //        //                    {
        //        //                        SetADProperty(entry, "title", dRow["Job"].ToString());
        //        //                    }

        //        //                    entry.CommitChanges();
        //        //                    entry.RefreshCache();
        //        //                    entry.Close();

        //        //                    DateTime dtRehireDate = default(DateTime);
        //        //                    if (IsDisabledADUserFromEmpDates(drRow["database_name"].ToString(), convertDate(DateTime.Now.Date), Convert.ToInt32(dRow["employeeunkid"]), ref dtRehireDate))
        //        //                    {
        //        //                        WriteToFile("IsDisabledADUserFromEmpDates : " + dtRehireDate);
        //        //                        if (dtRehireDate == default(DateTime))
        //        //                        {
        //        //                            WriteToFile("IsDisabledADUserFromEmpDates : Disable");
        //        //                            EnableDisableActiveDirectoryUser(false, dRow["displayname"].ToString(), mstrADIPAddress, mstrADDomain, mstrADDomainUser, mstrADUserPwd);
        //        //                        }
        //        //                        else if (dtRehireDate != default(DateTime) && dtRehireDate <= (DateTime.Now.Date))
        //        //                        {
        //        //                            WriteToFile("IsDisabledADUserFromEmpDates : Enable");
        //        //                            EnableDisableActiveDirectoryUser(true, dRow["displayname"].ToString(), mstrADIPAddress, mstrADDomain, mstrADDomainUser, mstrADUserPwd);
        //        //                        }
        //        //                    }

        //        //                }
        //        //            }
        //        //        }
        //        //    }
        //        //}
        //        ///*  END FOR CALLING ACTIVE DIRECTORY SERVICE */

        //        //*/
        //    }
        //    catch (Exception ex)
        //    {
        //        string strErroMsg = "";
        //        strErroMsg = "Message : " + ex.Message + Environment.NewLine;
        //        if (ex.InnerException != null) { strErroMsg += "InnerException : " + ex.InnerException + Environment.NewLine; }
        //        if (ex.StackTrace != null) { strErroMsg += "StackTrace : " + ex.StackTrace; }
        //        if (ex.Source != null) { strErroMsg += "Source : " + ex.Source; }
        //        if (ex.TargetSite != null) { strErroMsg += "TargetSite : " + ex.TargetSite; }
        //        WriteToFile("OnElapsedTime " + DateTime.Now + " : " + strErroMsg);
        //    }
        //    finally
        //    {
        //        if (timer.Enabled == false)
        //        {
        //            timer.Enabled = true;
        //        }
        //    }
        //}



        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                timer.Enabled = false;
                string value = convertTime(DateTime.Now).ToString().Substring(0, 5);
                if (IsCorrectTime(value))
                {
                    if (IsConnect() == false) { return; }
                    DataTable dtCList = new DataTable();
                    dtCList = IsFlexcubeIntegrated();
                    if (dtCList.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtCList.Rows)
                        {
                            mblnIsDatabaseAccessible = false;
                            IsDatabaseAccessible(dr["database_name"].ToString());
                            if (mblnIsDatabaseAccessible == false) { continue; }
                            DataTable dtParams = new DataTable();
                            dtParams = GetFlexcubeParameters(Convert.ToInt32(dr["companyunkid"]));
                            Dictionary<int, string> mdicFlexcubeServiceCollection = new Dictionary<int, string>();
                            string strAccCategory = ""; string strAccClass = ""; string strEmpAsOnDate = ""; string strExportPath = "";
                            if (dtParams.Rows.Count > 0)
                            {
                                foreach (DataRow dtrow in dtParams.Rows)
                                {
                                    if (dtrow["key_name"].ToString().ToUpper().StartsWith("_FLXSRV_") == true)
                                    {
                                        int intKey = Convert.ToInt32(dtrow["key_name"].ToString().ToUpper().Replace("_FLXSRV_", ""));
                                        if (mdicFlexcubeServiceCollection.ContainsKey(intKey) == false)
                                        {
                                            mdicFlexcubeServiceCollection.Add(intKey, dtrow["key_value"].ToString());
                                        }
                                    }
                                    if (dtrow["key_name"].ToString().ToUpper() == "FLEXCUBEACCOUNTCATEGORY") { strAccCategory = dtrow["key_value"].ToString(); }
                                    if (dtrow["key_name"].ToString().ToUpper() == "FLEXCUBEACCOUNTCLASS") { strAccClass = dtrow["key_value"].ToString(); }
                                    if (dtrow["key_name"].ToString().ToUpper() == "EMPLOYEEASONDATE") { strEmpAsOnDate = dtrow["key_value"].ToString(); }
                                    if (dtrow["key_name"].ToString().ToUpper() == "EXPORTDATAPATH") { strExportPath = dtrow["key_value"].ToString(); }
                                }
                            }
                            if (strEmpAsOnDate.Trim().Length <= 0) { strEmpAsOnDate = convertDate(DateTime.Now).ToString(); }
                            DataTable dtEmp = new DataTable();
                            //BLOCK LEAVE CODE HERE -- START
                            if (mdicFlexcubeServiceCollection.ContainsKey((int)enFlexcubeServiceInfo.FLX_BLOCK_USER) == true)
                            {
                                dtEmp = GetLeaveData(dr["database_name"].ToString());
                                if (dtEmp.Rows.Count > 0)
                                {
                                    Dictionary<int, int> empprocessed = new Dictionary<int, int>();
                                    foreach (DataRow row in dtEmp.Rows)
                                    {
                                        if (empprocessed.ContainsKey(Convert.ToInt32(row["employeeunkid"].ToString())) == true) { continue; }
                                        empprocessed.Add(Convert.ToInt32(row["employeeunkid"].ToString()), Convert.ToInt32(row["employeeunkid"].ToString()));
                                        string strBuilder, strPrefix;
                                        strPrefix = "UBLK_";
                                        string oStrMsgId = "";
                                        string strFileName = "";
                                        bool blnerror = false;
                                        string oResponseData = "";
                                        string strErrorDesc = "";
                                        DataTable flxusr = null;
                                        string accnum = String.Join("','", dtEmp.AsEnumerable().Where(x => x.Field<int>("employeeunkid") == Convert.ToInt32(row["employeeunkid"])).Select(y => y.Field<string>("accno")).ToArray());
                                        if (accnum.Trim().Length > 0) { accnum = "'" + accnum + "'"; }
                                        if (accnum.Trim().Length > 0)
                                        {
                                            flxusr = GetFlexUserDetails(accnum, Convert.ToInt32(dr["companyunkid"]));
                                            if (flxusr != null && flxusr.Rows.Count > 0)
                                            {
                                                if (flxusr.Rows[0]["USER_STATUS"].ToString().ToUpper() == "E")
                                                {
                                                    strBuilder = GenerateCBSFileString(dr["database_name"].ToString(), flxusr.Rows[0]["USER_ID"].ToString(), flxusr.Rows[0]["USER_NAME"].ToString(), row["customcode"].ToString(), "D", Convert.ToDateTime(flxusr.Rows[0]["start_date"]).ToString("yyyy-MM-dd"), ref oStrMsgId, flxusr.Rows[0]["BRANCH"].ToString(), flxusr.Rows[0]["time_level"].ToString());
                                                    if (strBuilder.Trim().Length > 0)
                                                    {
                                                        oResponseData = PostData(strBuilder, mdicFlexcubeServiceCollection[(int)enFlexcubeServiceInfo.FLX_BLOCK_USER], ref strErrorDesc);
                                                        if (oResponseData.Trim().Length > 0)
                                                        {
                                                            DataSet ds = new DataSet();
                                                            ds.ReadXml(new System.IO.StringReader(oResponseData));
                                                            if (ds.Tables.Contains("FCUBS_HEADER") == true)
                                                            {
                                                                if (ds.Tables["FCUBS_HEADER"].Rows.Count > 0)
                                                                {
                                                                    if (ds.Tables["FCUBS_HEADER"].Rows[0]["MSGSTAT"].ToString() != "SUCCESS")
                                                                    {
                                                                        if (ds.Tables.Contains("ERROR") == true)
                                                                        {
                                                                            blnerror = true;
                                                                            if (ds.Tables["ERROR"].Rows.Count > 0)
                                                                            {
                                                                                strErrorDesc = String.Join(",", ds.Tables["ERROR"].AsEnumerable().Select(x => x.Field<string>("ECODE").ToString() + " -> " + x.Field<string>("EDESC")).ToArray());
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (strErrorDesc.Trim().Length <= 0)
                                                        {
                                                            DataSet ds = new DataSet();
                                                            ds.ReadXml(new System.IO.StringReader(oResponseData));
                                                        }
                                                        else
                                                        {
                                                            blnerror = true;
                                                        }
                                                        strFileName = strPrefix + row["formno"].ToString() + "_" + DateTime.Now.ToString("yyyymmdd") + ".xml";
                                                        InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.DEACTIVATE, (int)enFiletype.USER, strFileName, strBuilder, oResponseData, "", oStrMsgId, blnerror, strErrorDesc, dr["database_name"].ToString(), (int)enRequestForm.LEAVE, row["formno"].ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.DEACTIVATE, (int)enFiletype.USER, "", "", "", "", oStrMsgId, true, "User Status : " + flxusr.Rows[0]["USER_STATUS"].ToString(), dr["database_name"].ToString(), (int)enRequestForm.LEAVE, row["formno"].ToString());
                                                }
                                            }
                                            else
                                            {
                                                InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.DEACTIVATE, (int)enFiletype.USER, "", "", "", "", oStrMsgId, true, "Flexcube user not found", dr["database_name"].ToString(), (int)enRequestForm.LEAVE, row["formno"].ToString());
                                            }
                                        }
                                        else
                                        {
                                            InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.DEACTIVATE, (int)enFiletype.USER, "", "", "", "", oStrMsgId, true, "Account number not assigned to employee", dr["database_name"].ToString(), (int)enRequestForm.LEAVE, row["formno"].ToString());
                                        }
                                    }
                                }
                            }
                            //BLOCK LEAVE CODE HERE -- END

                            dtEmp = GetEmployeeList(dr["database_name"].ToString(), strAccCategory, strAccClass, strEmpAsOnDate, enFlexcubeServiceInfo.FLX_CUSTOMER_SRV);
                            if (dtEmp.Rows.Count > 0)
                            {
                                foreach (DataRow row in dtEmp.Rows)
                                {
                                    string strBuilder, strPrefix;
                                    strPrefix = "CUST_";
                                    string oStrMsgId = "";
                                    string oResponseData = "";
                                    string strFileName = "";
                                    bool blnerror = false;
                                    string strErrorDesc = "";
                                    string strCustNo = "";
                                    strBuilder = GenerateFileString(enFiletype.CUSTOMER, strAccCategory, strAccClass, strEmpAsOnDate, row, dr["database_name"].ToString(), ref oStrMsgId);
                                    if (strBuilder.Trim().Length > 0)
                                    {
                                        oResponseData = PostData(strBuilder, mdicFlexcubeServiceCollection[(int)enFlexcubeServiceInfo.FLX_CUSTOMER_SRV], ref strErrorDesc);
                                        if (oResponseData.Trim().Length > 0)
                                        {
                                            DataSet ds = new DataSet();
                                            ds.ReadXml(new System.IO.StringReader(oResponseData));
                                            if (ds.Tables.Contains("FCUBS_HEADER") == true)
                                            {
                                                if (ds.Tables["FCUBS_HEADER"].Rows.Count > 0)
                                                {
                                                    if (ds.Tables["FCUBS_HEADER"].Rows[0]["MSGSTAT"].ToString() != "SUCCESS")
                                                    {
                                                        if (ds.Tables.Contains("ERROR") == true)
                                                        {
                                                            blnerror = true;
                                                            if (ds.Tables["ERROR"].Rows.Count > 0)
                                                            {
                                                                strErrorDesc = String.Join(",", ds.Tables["ERROR"].AsEnumerable().Select(x => x.Field<string>("ECODE").ToString() + " -> " + x.Field<string>("EDESC")).ToArray());
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (ds.Tables.Contains("Customer-Full") == true)
                                                        {
                                                            if (ds.Tables["Customer-Full"].Rows.Count > 0)
                                                            {
                                                                strCustNo = ds.Tables["Customer-Full"].Rows[0]["CUSTNO"].ToString();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (strErrorDesc.Trim().Length > 0) { blnerror = true; }
                                        strFileName = strPrefix + row["employeecode"].ToString() + "_" + DateTime.Now.ToString("yyyymmdd") + ".xml";
                                        InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.ADD, (int)enFiletype.CUSTOMER, strFileName, strBuilder, oResponseData, strCustNo, oStrMsgId, blnerror, strErrorDesc, dr["database_name"].ToString(), (int)enRequestForm.EMPLOYEE, row["employeecode"].ToString());
                                    }
                                    /* START FOR ACCOUNT  IT'S RELATION TO ABOVE CUSTOMER CREATION */
                                    if (blnerror == false)
                                    {
                                        strPrefix = "ACCT_";
                                        oResponseData = "";
                                        strFileName = "";
                                        blnerror = false;
                                        strErrorDesc = "";
                                        strBuilder = "";
                                        strBuilder = GenerateFileString(enFiletype.ACCOUNT, strAccCategory, strAccClass, strEmpAsOnDate, row, dr["database_name"].ToString(), ref oStrMsgId);
                                        if (strBuilder.Trim().Length > 0)
                                        {
                                            oResponseData = PostData(strBuilder, mdicFlexcubeServiceCollection[(int)enFlexcubeServiceInfo.FLX_ACCOUNT_SRV], ref strErrorDesc);
                                            if (oResponseData.Trim().Length > 0)
                                            {
                                                DataSet ds = new DataSet();
                                                ds.ReadXml(new System.IO.StringReader(oResponseData));
                                                if (ds.Tables.Contains("FCUBS_HEADER") == true)
                                                {
                                                    if (ds.Tables["FCUBS_HEADER"].Rows.Count > 0)
                                                    {
                                                        if (ds.Tables["FCUBS_HEADER"].Rows[0]["MSGSTAT"].ToString() != "SUCCESS")
                                                        {
                                                            if (ds.Tables.Contains("ERROR") == true)
                                                            {
                                                                blnerror = true;
                                                                if (ds.Tables["ERROR"].Rows.Count > 0)
                                                                {
                                                                    strErrorDesc = String.Join(",", ds.Tables["ERROR"].AsEnumerable().Select(x => x.Field<string>("ECODE").ToString() + " -> " + x.Field<string>("EDESC")).ToArray());
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (ds.Tables.Contains("Customer-Full") == true)
                                                            {
                                                                if (ds.Tables["Customer-Full"].Rows.Count > 0)
                                                                {
                                                                    strCustNo = ds.Tables["Customer-Full"].Rows[0]["CUSTNO"].ToString();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (strErrorDesc.Trim().Length > 0) { blnerror = true; }
                                            strFileName = strPrefix + row["employeecode"].ToString() + "_" + DateTime.Now.ToString("yyyymmdd") + ".xml";
                                            InsertFlexcubeRequest(Convert.ToInt32(row["employeeunkid"]), (int)enRequestType.ADD, (int)enFiletype.ACCOUNT, strFileName, strBuilder, oResponseData, strCustNo, oStrMsgId, blnerror, strErrorDesc, dr["database_name"].ToString(), (int)enRequestForm.EMPLOYEE, row["employeecode"].ToString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strErroMsg = "";
                strErroMsg = "Message : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null) { strErroMsg += "InnerException : " + ex.InnerException + Environment.NewLine; }
                if (ex.StackTrace != null) { strErroMsg += "StackTrace : " + ex.StackTrace; }
                if (ex.Source != null) { strErroMsg += "Source : " + ex.Source; }
                if (ex.TargetSite != null) { strErroMsg += "TargetSite : " + ex.TargetSite; }
                WriteToFile("OnElapsedTime " + DateTime.Now + " : " + strErroMsg);
            }
            finally
            {
                if (timer.Enabled == false)
                {
                    timer.Enabled = true;
                }
            }
        }
        //S.SANDEEP |04-NOV-2019| -- END


        #endregion

        #region Private Methods

        #region COMMON METHODS

        private bool IsConnect()
        {
            bool blnflag = false;
            try
            {
                sqlCn = new SqlConnection("Data Source=(Local)\\APAYROLL;Initial Catalog=hrmsConfiguration;User ID=sa;Password=pRofessionalaRuti999");
                sqlCn.Open();
                sqlCmd = new SqlCommand();
                sqlCmd.CommandTimeout = 0;
                sqlCmd.Connection = sqlCn;
                blnflag = true;
            }
            catch (Exception ex)
            {
                try
                {
                    sqlCn = new SqlConnection("Data Source=(Local)\\APAYROLL;Initial Catalog=hrmsConfiguration;User ID=aruti_sa;Password=pRofessionalaRuti999");
                    sqlCn.Open();
                    sqlCmd = new SqlCommand();
                    sqlCmd.CommandTimeout = 0;
                    sqlCmd.Connection = sqlCn;
                    blnflag = true;
                }
                catch (Exception ex1)
                {
                    WriteToFile("IsConnect " + DateTime.Now + " : " + ex1.Message);
                }
            }
            return blnflag;
        }

        public void GetDatesFilterString(ref string xJoinDateQry, ref string xDateFilter, DateTime xStartDate, DateTime xEndDate, bool xIncludeReinstementDate, string xStrDatabaseName, string xHrEmployeeTableAlias)
        {
            try
            {
                var xNextDate = xEndDate.AddDays(1);

                xJoinDateQry = " LEFT JOIN " +
                               "( " +
                               "    SELECT " +
                               "         T.EmpId " +
                               "        ,T.EOC " +
                               "        ,T.LEAVING " +
                               "        ,T.isexclude_payroll AS IsExPayroll " +
                               "    FROM " +
                               "    ( " +
                               "        SELECT " +
                               "             employeeunkid AS EmpId " +
                               "            ,date1 AS EOC " +
                               "            ,date2 AS LEAVING " +
                               "            ,effectivedate " +
                               "            ,isexclude_payroll " +
                               "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " +
                               "        FROM " + xStrDatabaseName + "..hremployee_dates_tran WHERE datetypeunkid IN(4) AND isvoid = 0 " +
                               "            AND CONVERT(CHAR(8),effectivedate,112) <= '" + convertDate(xEndDate).ToString() + "' " +
                               "    ) AS T WHERE T.xNo = 1 " +
                               ") AS TRM ON TRM.EmpId = " + xHrEmployeeTableAlias + ".employeeunkid " +
                               "LEFT JOIN " +
                               "( " +
                               "    SELECT " +
                               "         R.EmpId " +
                               "        ,R.RETIRE " +
                               "    FROM " +
                               "    ( " +
                               "        SELECT " +
                               "             employeeunkid AS EmpId " +
                               "            ,date1 AS RETIRE " +
                               "            ,effectivedate " +
                               "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " +
                               "        FROM " + xStrDatabaseName + "..hremployee_dates_tran WHERE datetypeunkid IN(6) AND isvoid = 0 " +
                               "            AND CONVERT(CHAR(8),effectivedate,112) <= '" + convertDate(xEndDate).ToString() + "' " +
                               "    ) AS R WHERE R.xNo = 1 " +
                               ") AS RET ON RET.EmpId = " + xHrEmployeeTableAlias + ".employeeunkid " +
                               "LEFT JOIN " +
                               "( " +
                               "    SELECT " +
                               "         RH.EmpId " +
                               "        ,RH.REHIRE " +
                               "    FROM " +
                               "    ( " +
                               "        SELECT " +
                               "             employeeunkid AS EmpId " +
                               "            ,reinstatment_date AS REHIRE " +
                               "            ,effectivedate " +
                               "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " +
                               "        FROM " + xStrDatabaseName + "..hremployee_rehire_tran WHERE isvoid = 0 " +
                               "            AND CONVERT(CHAR(8),effectivedate,112) <= '" + convertDate(xEndDate).ToString() + "' " +
                               "      ) AS RH WHERE RH.xNo = 1 " +
                               ") AS HIRE ON HIRE.EmpId = " + xHrEmployeeTableAlias + ".employeeunkid ";

                xDateFilter = " AND CONVERT(CHAR(8)," + xHrEmployeeTableAlias + ".appointeddate,112) <= '" + convertDate(xEndDate).ToString() + "' " +
                              " AND (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN '" + convertDate(xStartDate).ToString() + "' ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) >= '" + convertDate(xStartDate).ToString() + "' " +
                              " AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN '" + convertDate(xStartDate).ToString() + "' ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) >= '" + convertDate(xStartDate).ToString() + "' " +
                              " AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN '" + convertDate(xStartDate).ToString() + "' ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) >= '" + convertDate(xStartDate).ToString() + "' ";

                if (xIncludeReinstementDate)
                {
                    xDateFilter += " AND (CASE WHEN CONVERT(CHAR(8), HIRE.REHIRE, 112) IS NULL THEN '" + convertDate(xEndDate).ToString() + "' ELSE CONVERT(CHAR(8), HIRE.REHIRE, 112) END)  <= '" + convertDate(xEndDate).ToString() + "' ";
                }

            }
            catch (Exception ex)
            {
                WriteToFile("GetDatesFilterString " + DateTime.Now + " : " + ex.Message);
            }
            finally
            {
            }
        }

        #endregion

        #region FLEXCUBE

        private DataTable IsFlexcubeIntegrated()
        {
            DataTable dt = new DataTable();
            string StrQ = "";
            try
            {
                StrQ = "SELECT " +
                       "     CF.companyunkid " +
                       "    ,FT.database_name " +
                       "FROM hrmsConfiguration..cfconfiguration AS CF " +
                       "    JOIN hrmsConfiguration..cffinancial_year_tran AS FT ON FT.companyunkid = CF.companyunkid " +
                       "WHERE UPPER(CF.[key_name]) = 'ISHRFLEXCUBEINTEGRATED'  AND UPPER(CF.key_value) = 'TRUE' AND FT.isclosed = 0 ";

                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                sqlCmd.Parameters.Clear();
                sqlCmd.CommandText = StrQ;

                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                WriteToFile("IsFlexcubeIntegrated SQL " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
            catch (Exception ex)
            {
                WriteToFile("IsFlexcubeIntegrated " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
            return dt;
        }

        private DataTable GetFlexcubeParameters(int intcompanyid)
        {
            DataTable dt = new DataTable();
            string StrQ = "";
            string StrCols = "";
            try
            {
                foreach (var item in Enum.GetValues(typeof(enFlexcubeServiceInfo)))
                {
                    StrCols += "'_FLXSRV_" + Convert.ToInt32(item) + "',";
                }
                StrCols += "'FLEXCUBEACCOUNTCATEGORY','FLEXCUBEACCOUNTCLASS','EMPLOYEEASONDATE'";

                StrQ = "SELECT " +
                       "     CF.key_name " +
                       "    ,CF.key_value " +
                       "FROM hrmsConfiguration..cfconfiguration AS CF " +
                       "WHERE CF.[key_name] IN (" + StrCols + ") AND CF.companyunkid = '" + intcompanyid + "' " +
                       "AND CF.key_value <> '' "; //S.SANDEEP |18-JUN-2019| -- START {CF.key_value <> ''} -- END

                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                sqlCmd.Parameters.Clear();
                sqlCmd.CommandText = StrQ;
                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                WriteToFile("GetFlexcubeParameters " + DateTime.Now + " : " + ex.Message);
            }
            return dt;
        }

        private DataTable GetEmployeeList(string dbname, string oAccCategory, string oAccClass, string oEmpAsOnDate, enFlexcubeServiceInfo eSrvInfo)
        {
            DataTable dt = new DataTable();
            string StrQ = "";
            try
            {
                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                string xDateJoinQry, xDataFilterQry;
                xDateJoinQry = ""; xDataFilterQry = "";
                GetDatesFilterString(ref xDateJoinQry, ref xDataFilterQry, convertDate(oEmpAsOnDate).Date, convertDate(oEmpAsOnDate).Date, true, dbname, "hremployee_master");

                StrQ = "SELECT " +
                       "     ISNULL(cc.customcode,'') AS cccode " +
                       "    ,REPLACE(firstname,'''','''''') AS firstname " +
                       "    ,REPLACE(othername,'''','''''') AS othername " +
                       "    ,REPLACE(surname,'''','''''') AS surname " +
                       "    ,REPLACE(employeecode,'''','''''') AS employeecode " +
                       "    ,hremployee_master.employeeunkid " +
                       "    ,ISNULL(REPLACE(cfcommon_master.name,'''',''''''),'') AS identityname " +
                       "    ,ISNULL(REPLACE(hremployee_idinfo_tran.identity_no,'''',''''''),'') AS identityno " +
                       "    ,' " + oAccCategory.Replace("'", "''") + "' AS accategory " +
                       "    ,REPLACE(domicile_address1,'''','''''') + CASE WHEN ISNULL(REPLACE(cfcity_master.name,'''',''''''),'') = '' THEN '' ELSE ',' + REPLACE(cfcity_master.name,'''','''''') END " +
                       "     + CASE WHEN ISNULL(REPLACE(cfstate_master.name,'''',''''''),'') = '' THEN '' ELSE ',' + REPLACE(cfstate_master.name,'''','''''') END " +
                       "     + CASE WHEN ISNULL(REPLACE(cfcountry_master.country_name,'''',''''''),'') = '' THEN '' ELSE ',' + REPLACE(cfcountry_master.country_name,'''','''''') END AS domadd1 " +
                       "    ,REPLACE(domicile_road,'''','''''') AS domadd2 " +
                       "    ,ISNULL(REPLACE(cfstate_master.name,'''',''''''),'') AS birthplace " +
                       "    ,REPLACE(present_mobile,'''','''''') AS mobile " +
                       "    ,REPLACE(present_tel_no,'''','''''') AS tel " +
                       "    ,CASE WHEN birthdate IS NULL THEN '' ELSE REPLACE(CONVERT(NVARCHAR(20),birthdate,102),'.','-') END AS dob " +
                       "    ,CASE WHEN gender = 1 THEN 'M' WHEN gender = 2 THEN 'F' ELSE '' END AS gender " +
                       "    ,ISNULL(REPLACE(jb.job_name,'''',''''''),'') AS job " +
                       "    ,REPLACE(CONVERT(NVARCHAR(20),GETDATE(),102),'.','-') AS tdate " +
                       "    ,ISNULL(REPLACE(tf.name,'''',''''''),'') AS location " +
                       "    ,'" + oAccClass.Replace("'", "''") + "' AS ACCLS " +
                       "FROM " + dbname + "..hremployee_master " +
                       "    LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.domicile_countryunkid " +
                       "    LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfstate_master.stateunkid = hremployee_master.domicile_stateunkid " +
                       "    LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = hremployee_master.domicile_post_townunkid " +
                       "	LEFT JOIN " +
                       "	( " +
                       "		SELECT " +
                       "			 hremployee_transfer_tran.employeeunkid " +
                       "			,hrdepartment_group_master.name " +
                       "			,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " +
                       "		FROM " + dbname + "..hremployee_transfer_tran " +
                       "			LEFT JOIN " + dbname + "..hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hremployee_transfer_tran.deptgroupunkid " +
                       "		WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" + oEmpAsOnDate + "' " +
                       "	) AS tf ON tf.rno = 1 AND tf.employeeunkid = hremployee_master.employeeunkid " +
                       "	LEFT JOIN " +
                       "	( " +
                       "		SELECT " +
                       "			 hremployee_categorization_tran.employeeunkid " +
                       "			,hrjob_master.job_name " +
                       "			,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " +
                       "		FROM " + dbname + "..hremployee_categorization_tran " +
                       "			JOIN " + dbname + "..hrjob_master ON hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " +
                       "		WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" + oEmpAsOnDate + "' " +
                       "	) AS jb ON jb.rno = 1 AND jb.employeeunkid = hremployee_master.employeeunkid " +
                       "	LEFT JOIN " + dbname + "..hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = hremployee_master.employeeunkid AND isdefault = 1 " +
                       "	LEFT JOIN " + dbname + "..cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid AND cfcommon_master.masterunkid = 12 " +
                       "	LEFT JOIN " +
                       "	( " +
                       "		SELECT " +
                       "			 hremployee_cctranhead_tran.employeeunkid " +
                       "			,prcostcenter_master.customcode " +
                       "			,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " +
                       "		FROM " + dbname + "..hremployee_cctranhead_tran " +
                       "			JOIN " + dbname + "..prcostcenter_master ON hremployee_cctranhead_tran.cctranheadvalueid = prcostcenter_master.costcenterunkid " +
                       "		WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" + oEmpAsOnDate + "' " +
                       "	) AS cc ON cc.rno = 1 AND cc.employeeunkid = hremployee_master.employeeunkid ";

                if (xDateJoinQry.Trim().Length > 0)
                {
                    StrQ += " " + xDateJoinQry;
                }

                //StrQ += " WHERE hremployee_master.isapproved = 1 AND hremployee_master.isacc_created = 0 AND hremployee_master.employeeunkid NOT IN (SELECT DISTINCT employeeunkid FROM " + dbname + "..hrflexcube_request_tran WHERE filetypeid = '" + (int)eSrvInfo + "') ";
                StrQ += " WHERE hremployee_master.isapproved = 1 AND hremployee_master.isacc_created = 0 ";

                if (xDataFilterQry.Trim().Length > 0)
                {
                    StrQ += " " + xDataFilterQry;
                }
                sqlCmd.Parameters.Clear();
                sqlCmd.CommandText = StrQ;
                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                WriteToFile("GetEmployeeList " + DateTime.Now + " : " + ex.Message);
            }
            return dt;
        }

        private string GenerateFileString(enFiletype eFiletype, string strAccountCategory, string strAccountClass, string xEmpAsOnDate, DataRow dr, string dbName, ref string strMessageId)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            string StrQ = "";
            try
            {
                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                string StrCurrency = "";
                StrQ = "SELECT TOP 1 currency " +
                       "FROM " + dbName + "..cfexchange_rate " +
                       "    JOIN hrmsConfiguration..cfcountry_master ON cfcountry_master.countryunkid = cfexchange_rate.countryunkid " +
                       "WHERE isbasecurrency = 1 ";

                sqlCmd.CommandText = StrQ;
                StrCurrency = (string)sqlCmd.ExecuteScalar();

                int iCount = 1;
                while (iCount > 0)
                {
                    StrQ = "SELECT CONVERT(NVarChar(5), right(replace(convert(varchar, getdate(),114),':',''),5)) + CONVERT(NVarChar(5), right(newid(),5)) ";
                    sqlCmd.CommandText = StrQ;
                    strMessageId = (string)sqlCmd.ExecuteScalar();

                    StrQ = "SELECT COUNT(1) FROM " + dbName + "..hrflexcube_request_tran WHERE fmsgeid = '" + strMessageId + "' ";
                    sqlCmd.CommandText = StrQ;
                    iCount = Convert.ToInt32(sqlCmd.ExecuteScalar());

                    if (iCount <= 0) { break; }
                }
                StrQ = "SELECT TOP 1 ";
                switch (eFiletype)
                {
                    case enFiletype.CUSTOMER:
                        //S.SANDEEP |08-APR-2019| -- START
                        /*"'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' " +*/
                        //'S.SANDEEP |08-APR-2019| -- END
                        StrQ += "'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' " +
                                ",'<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">'" +
                                ",'<soapenv:Header/>'" +
                                ",'<soapenv:Body>'" +
                                ",'<CREATECUSTOMER_FSFS_REQ xmlns=\"http://fcubs.ofss.com/service/FCUBSCustomerService\">' " +
                                ",'<FCUBS_HEADER>' " +
                                ",'<SOURCE>ARUTI</SOURCE>' " +
                                ",'<UBSCOMP>FCUBS</UBSCOMP>' " +
                                ",'<MSGID>' + '" + strMessageId + "' + '</MSGID>' " +
                                ",'<CORRELID>' + '" + strMessageId + "' + '</CORRELID>' " +
                                ",'<USERID>ARUTI</USERID>' " +
                                ",'<BRANCH>'+'" + dr["cccode"].ToString() + "'+'</BRANCH>' " +
                                ",'<MODULEID>ST</MODULEID>' " +
                                ",'<SERVICE>FCUBSCustomerService</SERVICE>' " +
                                ",'<OPERATION>CreateCustomer</OPERATION>' " +
                                ",'<SOURCE_OPERATION>CreateCustomer</SOURCE_OPERATION>' " +
                                ",'<SOURCE_USERID/>' " +
                                ",'<DESTINATION>FCUBS</DESTINATION>' " +
                                ",'</FCUBS_HEADER>' " +
                                ",'<FCUBS_BODY>' " +
                                ",'<Customer-Full>' " +
                                ",'<NAME>'+ '" + dr["firstname"].ToString() + "' + ' ' + '" + dr["othername"].ToString() + "' + ' ' + '" + dr["surname"].ToString() + "' +'</NAME>' " +
                                ",'<FULLNAME>'+ '" + dr["firstname"].ToString() + "' + ' ' + '" + dr["othername"].ToString() + "' + ' ' + '" + dr["surname"].ToString() + "' +'</FULLNAME>'" +
                                ",'<SNAME>'+ '" + dr["surname"].ToString() + "' +'</SNAME>' " +
                                ",'<ADDRLN1>TEST</ADDRLN1>' " +
                                ",'<COUNTRY>TZ</COUNTRY>' " +
                                ",'<NLTY>TZ</NLTY>' " +
                                ",'<LANG>ENG</LANG>' " +
                                ",'<EXPCNTRY>TZ</EXPCNTRY>' " +
                                ",'<UIDNAME>'+ '" + dr["identityname"].ToString() + "' +'</UIDNAME>' " +
                                ",'<UIDVAL>'+ '" + dr["identityno"].ToString() + "'  +'</UIDVAL>' " +
                                ",'<CCATEG>'+'" + strAccountCategory + "'+'</CCATEG>' /*-- ACCOUNT CATEGORY */ " +
                                ",'<LBRN>'+'" + dr["cccode"].ToString() + "'+'</LBRN>' " +
                                ",'<LIMCCY>" + StrCurrency + "</LIMCCY>' " +
                                ",'<MEDIA>MAIL</MEDIA>' " +
                                ",'<SSN/>' " +
                                ",'<Custpersonal>' " +
                                ",'<FSTNAME>'+ '" + dr["firstname"].ToString() + "' +'</FSTNAME>' " +
                                ",'<MIDNAME>'+ '" + dr["othername"].ToString() + "' +'</MIDNAME>' " +
                                ",'<LSTNAME>'+ '" + dr["surname"].ToString() + "' +'</LSTNAME>' " +
                                ",'<DADD1>'+'" + dr["domadd1"].ToString() + "' +'</DADD1>' " +
                                ",'<DADD2>'+ '" + dr["domadd2"].ToString() + "' +'</DADD2>' " +
                                ",'<BRTHPLCE>'+'" + dr["birthplace"].ToString() + "'+'</BRTHPLCE>' " +
                                ",'<RESSTATUS>R</RESSTATUS>' " +
                                ",'<MOBNUM>'+ '" + dr["mobile"].ToString() + "' +'</MOBNUM>' " +
                                ",'<TEL>'+ '" + dr["tel"].ToString() + "' +'</TEL>' " +
                                ",'<DOB>'+ '" + dr["dob"].ToString() + "' +'</DOB>' " +
                                ",'<SEX>'+'" + dr["gender"].ToString() + "'+'</SEX>' " +
                                ",'</Custpersonal>' " +
                                ",'<Custprof>' " +
                                ",'<PROFESSION>'+ '" + dr["job"].ToString() + "' +'</PROFESSION> ' " +
                                ",'</Custprof>' " +
                                ",'<Custmis>' " +
                                ",'<Dfltcd>' " +
                                ",'<MISCLS>BSC</MISCLS>' " +
                                ",'<MISCD>BS001</MISCD>' " +
                                ",'</Dfltcd>' " +
                                ",'</Custmis>' " +
                                ",'<UDFDETAILS>' " +
                                ",'<FLDNAM>BRELA_SUBMISSION</FLDNAM>' " +
                                ",'<FLDVAL>NA_BRELA</FLDVAL>' " +
                                ",'</UDFDETAILS>' " +
                                ",'<UDFDETAILS>' " +
                                ",'<FLDNAM>BRELA_COMPLIANCE</FLDNAM>' " +
                                ",'<FLDVAL>BRELA_NA</FLDVAL>' " +
                                ",'</UDFDETAILS>' " +
                                ",'</Customer-Full>' " +
                                ",'</FCUBS_BODY>' " +
                                ",'</CREATECUSTOMER_FSFS_REQ>' " +
                                ",'</soapenv:Body>' " +
                                ",'</soapenv:Envelope>' ";
                        break;
                    case enFiletype.ACCOUNT:
                        //S.SANDEEP |08-APR-2019| -- START
                        /*"'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' " +*/
                        //'S.SANDEEP |08-APR-2019| -- END
                        StrQ += "'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' " +
                                ",'<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">'" +
                                ",'<soapenv:Header/>'" +
                                ",'<soapenv:Body>'" +
                                ",'<CREATECUSTACC_FSFS_REQ xmlns=\"http://fcubs.ofss.com/service/FCUBSAccService\">' " +
                                ",'<FCUBS_HEADER>' " +
                                ",'<SOURCE>ESB</SOURCE>' " +
                                ",'<UBSCOMP>FCUBS</UBSCOMP>' " +
                                ",'<MSGID>' + '" + strMessageId + "' + '</MSGID>' " +
                                ",'<CORRELID>' + '" + strMessageId + "' + '</CORRELID>' " +
                                ",'<USERID>' + 'ARUTI' + '</USERID>' " +
                                ",'<BRANCH>'+'" + dr["cccode"].ToString() + "'+'</BRANCH>' " +
                                ",'<MODULEID>ST</MODULEID>' " +
                                ",'<SERVICE>FCUBSAccService</SERVICE>' " +
                                ",'<OPERATION>CreateCustAcc</OPERATION>' " +
                                ",'<SOURCE_OPERATION>CreateCustAcc</SOURCE_OPERATION>' " +
                                ",'<SOURCE_USERID/>' " +
                                ",'<DESTINATION>FCUBS</DESTINATION>' " +
                                ",'</FCUBS_HEADER>' " +
                                ",'<FCUBS_BODY>' " +
                                ",'<Cust-Account-Full>' " +
                                ",'<BRN>'+'" + dr["cccode"].ToString() + "'+'</BRN>' " +
                                ",'<CUSTNO>' + '" + strMessageId + "' + '</CUSTNO>' " +
                                ",'<ACCLS>'+'" + dr["ACCLS"].ToString() + "'+'</ACCLS>' /*-- ACCOUNT CLASS*/ " +
                                ",'<CCY>" + StrCurrency + "</CCY>' /*-- BASECURRENCY*/ " +
                                ",'<CUSTNAME>'+ '" + dr["firstname"].ToString() + "' + ' ' + '" + dr["othername"].ToString() + "' + ' ' + '" + dr["surname"].ToString() + "' +'</CUSTNAME>' " +
                                ",'<ADESC>'+ '" + dr["surname"].ToString() + "' +'</ADESC>' " +
                                ",'<ALTACC/>' " +
                                ",'<MEDIA>MAIL</MEDIA>' " +
                                ",'<ACCOPENDT>'+'" + dr["tdate"].ToString() + "'+'</ACCOPENDT>' /*-- TODAYSDATE*/ " +
                                ",'<LOC>'+ '" + dr["location"].ToString() + "' +'</LOC>' " +
                                ",'<MEDIA>MAIL</MEDIA>' " +
                                ",'<ACSTATNODR>N</ACSTATNODR>' " +
                                ",'<ACSTATNOCR>N</ACSTATNOCR>' " +
                                ",'<ACSTATSTPAY>N</ACSTATSTPAY>' " +
                                ",'<POSTALLOWED>Y</POSTALLOWED>' " +
                                ",'<DORM>N</DORM>' " +
                                ",'<CHQBOOK>N</CHQBOOK>' " +
                                ",'<TXN_ALERTS>N</TXN_ALERTS>' " +
                                ",'<Udf-Details>' " +
                                ",'<FLDNAM>LEAD_GENERATOR</FLDNAM>' " +
                                ",'<FLDVAL>Default</FLDVAL>' " +
                                ",'</Udf-Details>' " +
                                ",'<Udf-Details>' " +
                                ",'<FLDNAM>FINANCIAL_LITERACY</FLDNAM>' " +
                                ",'<FLDVAL>DFLT_FINANCIAL_LITERANCY</FLDVAL>' " +
                                ",'</Udf-Details>' " +
                                ",'<Udf-Details>' " +
                                ",'<FLDNAM>IN_SCHOOL_STATUS</FLDNAM>' " +
                                ",'<FLDVAL>OTHER</FLDVAL>' " +
                                ",'</Udf-Details>' " +
                                ",'<Udf-Details>' " +
                                ",'<FLDNAM>CUST_RELATIONSHIP_MANAGER_CRM</FLDNAM>' " +
                                ",'<FLDVAL>CRM_DFLT</FLDVAL>' " +
                                ",'</Udf-Details>' " +
                                ",'<Udf-Details>' " +
                                ",'<FLDNAM>SECTOR_ACCT</FLDNAM>' " +
                                ",'<FLDVAL>SC999</FLDVAL>' /*-- --> yes I will confirm this, if need to be set on configuration same acc class*/ " +
                                ",'</Udf-Details>' " +
                                ",'<Udf-Details>' " +
                                ",'<FLDNAM>RELATIONSHIP_OFFICER_CSM</FLDNAM>' " +
                                ",'<FLDVAL>CSM_DFLT</FLDVAL>' " +
                                ",'</Udf-Details>' " +
                                ",'<Udf-Details>' " +
                                ",'<FLDNAM>CUST_RELATIONSHIP_MANAGER_CRM</FLDNAM>' " +
                                ",'<FLDVAL>CRM_DFLT</FLDVAL>' " +
                                ",'</Udf-Details>' " +
                                ",'<MAKER>ARUTI</MAKER>' " +
                                ",'<MAKERSTAMP/>' " +
                                ",'<CHECKER>ARUTI</CHECKER>' " +
                                ",'<CHECKERSTAMP/>' " +
                                ",'<MODNO/>' " +
                                ",'<TXNSTAT/>' " +
                                ",'<AUTHSTAT>A</AUTHSTAT>' " +
                                ",'<Misdetails>' " +
                                ",'<MISGRP/>' " +
                                ",'<POOLCD/>' " +
                                ",'<REFRT/>' " +
                                ",'<REFRTTYPE/>' " +
                                ",'<MISGRPTXN/>' " +
                                ",'<REFRTCD/>' " +
                                ",'<REFRTSPRD/>' " +
                                ",'<CALCMETH/>' " +
                                ",'<TXNMIS1/>' " +
                                ",'<TXNMIS2/>' " +
                                ",'<TXNMIS3/>' " +
                                ",'<COMPMIS1/>' " +
                                ",'<COSTCOD1/>' " +
                                ",'<LINKGRP/>' " +
                                ",'<RTFLAG>R</RTFLAG>' " +
                                ",'</Misdetails>' " +
                                ",'</Cust-Account-Full>' " +
                                ",'</FCUBS_BODY>' " +
                                ",'</CREATECUSTACC_FSFS_REQ>' " +
                                ",'</soapenv:Body>' " +
                                ",'</soapenv:Envelope>' "; ;
                        break;
                }
                sqlCmd.CommandText = StrQ;
                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        foreach (DataColumn c in dt.Columns)
                        {
                            string strData = ""; strData = r[c].ToString();
                            SetXMLFormat(ref strData);
                            sb.Append(strData + Environment.NewLine);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile("GenerateFileString QUERY : " + StrQ);
                WriteToFile("GenerateFileString " + DateTime.Now + " : " + ex.Message);
            }
            return sb.ToString();
        }

        private void SetXMLFormat(ref string strData)
        {
            try
            {
                strData = strData.Replace("&", "&amp;");
            }
            catch (Exception ex)
            {
                WriteToFile("SetXMLFormat " + DateTime.Now + " : " + ex.Message);
            }
        }

        public string convertDate(DateTime Date)
        {
            return string.Format("{0:0000}{1:00}{2:00}", Date.Year, Date.Month, Date.Day);
        }

        public System.DateTime convertDate(string DateSting)
        {
            try
            {

                return new DateTime(int.Parse(DateSting.Substring(0, 4))
                    , int.Parse(DateSting.Substring(4, 2))
                    , int.Parse(DateSting.Substring(6, 2))
                    );

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[convertDate]");
            }
        }

        public string convertTime(System.DateTime Time)
        {
            try
            {
                return string.Format("{0:00}:{1:00}:{2:00}", Time.Hour, Time.Minute, Time.Second);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[convertTime]");
            }
        }

        private bool IsCorrectTime(string value)
        {
            bool blnflag = false;
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(conREG_NODE);
                if (key != null)
                {
                    string[] keys = key.GetValueNames();
                    if (Array.IndexOf(keys, "FSrvTime") <= -1)
                    {
                        WriteToFile("Registry Key Not Found, Giving Default Value.");
                        if (value == "00:05")
                        {
                            blnflag = true;
                        }
                    }
                    else
                    {
                        string ovalue = key.GetValue("FSrvTime").ToString();
                        if (ovalue.Trim() == "") { ovalue = "00:05"; }
                        if (value == ovalue)
                        {
                            blnflag = true;
                        }
                    }
                }
                key.Close();
            }
            catch (Exception ex)
            {
                WriteToFile("Exception thrown on " + DateTime.Now + " : " + ex.Message);
            }
            finally
            {

            }
            return blnflag;
        }

        public void WriteToFile(string Message)
        {
            string m_strLogFile = "";
            string m_strFileName = "Arutireflexcube_LOG_" + DateTime.Now.Date.ToString("yyyyMMdd");
            System.IO.StreamWriter file;
            m_strLogFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "\\", m_strFileName + ".txt");
            file = new System.IO.StreamWriter(m_strLogFile, true);
            file.BaseStream.Seek(0, SeekOrigin.End);
            file.WriteLine(Message);
            file.Close();
        }

        //S.SANDEEP |08-APR-2019| -- START
        //private void InsertFlexcubeRequest(int intEmpId, int intRequestTypeId, int intFileTypeId, string strFileName, string strFileData, string strResponseData, string strCustomerNumber, string strMessageId, bool blnError, string strerror_description, string dbName)
        private void InsertFlexcubeRequest(int intEmpId, int intRequestTypeId, int intFileTypeId, string strFileName, string strFileData, string strResponseData, string strCustomerNumber, string strMessageId, bool blnError, string strerror_description, string dbName, int reqmoduleid, string reqmasterid)
        //S.SANDEEP |08-APR-2019| -- END        
        {
            string StrQ = "";
            SqlTransaction sqlt = sqlCn.BeginTransaction();
            try
            {
                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                sqlCmd.Transaction = sqlt;
                StrQ = "INSERT INTO " + dbName + "..hrflexcube_request_tran " +
                       "( " +
                       "     tranguid " +
                       "    ,employeeunkid " +
                       "    ,fmsgeid " +
                       "    ,fcustno " +
                       "    ,requestdate " +
                       "    ,requesttypeid " +
                       "    ,filetypeid " +
                       "    ,filename " +
                       "    ,filedata " +
                       "    ,responsedata " +
                       "    ,iserror " +
                       "    ,error_description " +
                       "    ,reqmoduleid " +
                       "    ,reqmst_number " +
                       ") " +
                       "VALUES " +
                       "( " +
                       "     @tranguid " +
                       "    ,@employeeunkid " +
                       "    ,@fmsgeid " +
                       "    ,@fcustno " +
                       "    ,GETDATE() " +
                       "    ,@requesttypeid " +
                       "    ,@filetypeid " +
                       "    ,@filename " +
                       "    ,@filedata " +
                       "    ,@responsedata " +
                       "    ,@iserror " +
                       "    ,@error_description " +
                       "    ,@reqmoduleid " +
                       "    ,@reqmst_number " +
                       ") ";
                //S.SANDEEP |08-APR-2019| -- START {reqmoduleid,reqmst_number} -- END

                sqlCmd.CommandText = StrQ;
                sqlCmd.Parameters.Clear();

                sqlCmd.Parameters.AddWithValue("@tranguid ", Guid.NewGuid().ToString());
                sqlCmd.Parameters.AddWithValue("@employeeunkid", intEmpId);
                sqlCmd.Parameters.AddWithValue("@fmsgeid", strMessageId);
                sqlCmd.Parameters.AddWithValue("@fcustno", strCustomerNumber);
                sqlCmd.Parameters.AddWithValue("@requesttypeid", intRequestTypeId);
                sqlCmd.Parameters.AddWithValue("@filetypeid", intFileTypeId);
                sqlCmd.Parameters.AddWithValue("@filename", strFileName);
                sqlCmd.Parameters.AddWithValue("@filedata", strFileData);
                sqlCmd.Parameters.AddWithValue("@responsedata", strResponseData);
                sqlCmd.Parameters.AddWithValue("@iserror", blnError);
                sqlCmd.Parameters.AddWithValue("@error_description", strerror_description);

                //S.SANDEEP |08-APR-2019| -- START
                //sqlCmd.ExecuteNonQuery();
                //bool blnIsAccountCreated = false;
                //if (strerror_description.Trim().Length <= 0)
                //{
                //    blnIsAccountCreated = true;
                //}

                //StrQ = "UPDATE " + dbName + "..hremployee_master SET isacc_created = @isacc_created WHERE employeeunkid = @employeeunkid ";
                //sqlCmd.CommandText = StrQ;
                //sqlCmd.Parameters.Clear();
                //sqlCmd.Parameters.AddWithValue("@isacc_created", blnIsAccountCreated);
                //sqlCmd.Parameters.AddWithValue("@employeeunkid", intEmpId);

                //sqlCmd.ExecuteNonQuery();

                sqlCmd.Parameters.AddWithValue("@reqmoduleid", reqmoduleid);
                sqlCmd.Parameters.AddWithValue("@reqmst_number", reqmasterid);

                sqlCmd.ExecuteNonQuery();

                if (strCustomerNumber.Trim().Length > 0)
                {
                    bool blnIsAccountCreated = false;
                    if (strerror_description.Trim().Length <= 0)
                    {
                        blnIsAccountCreated = true;
                    }

                    StrQ = "UPDATE " + dbName + "..hremployee_master SET isacc_created = @isacc_created WHERE employeeunkid = @employeeunkid ";
                    sqlCmd.CommandText = StrQ;
                    sqlCmd.Parameters.Clear();
                    sqlCmd.Parameters.AddWithValue("@isacc_created", blnIsAccountCreated);
                    sqlCmd.Parameters.AddWithValue("@employeeunkid", intEmpId);

                    sqlCmd.ExecuteNonQuery();
                }
                //S.SANDEEP |08-APR-2019| -- END

                sqlt.Commit();
            }
            catch (Exception ex)
            {
                sqlt.Rollback();
                WriteToFile("InsertFlexcubeRequest " + DateTime.Now + " : " + ex.Message);
            }
            finally
            {

            }
        }

        private string PostData(string xmlData, string strServiceURL, ref string err)
        {
            string strResponseData = "";
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlData);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strServiceURL);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(xmlDoc.InnerXml);
                request.ContentType = "text/xml";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                using (StreamReader responseReader = new StreamReader(request.GetResponse().GetResponseStream()))
                {
                    string result = responseReader.ReadToEnd();
                    XDocument ResultXML = XDocument.Parse(result);
                    strResponseData = ResultXML.ToString();
                }
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    using (WebResponse response = ex.Response)
                    {
                        HttpWebResponse httpResponse = (HttpWebResponse)response;
                        using (Stream data = response.GetResponseStream())
                        {
                            using (var reader = new StreamReader(data))
                            {
                                err = reader.ReadToEnd();

                            }
                        }
                    }
                }
                //S.SANDEEP |08-APR-2019| -- START
                else
                {
                    err = ex.Message;
                }
                //S.SANDEEP |08-APR-2019| -- END

                WriteToFile("PostData " + DateTime.Now + " : " + err);
            }
            catch (Exception ex)
            {
                WriteToFile("PostData " + DateTime.Now + " : " + ex.Message);
            }
            return strResponseData;
        }

        private void IsDatabaseAccessible(string strDBName)
        {
            string StrQ = "";
            int intDBId = 0;
            if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
            try
            {
                StrQ = "SELECT database_id " +
                       "FROM sys.databases " +
                       "WHERE name = '" + strDBName + "' AND user_access_desc = 'MULTI_USER' ";
                sqlCmd.Parameters.Clear();
                sqlCmd.CommandText = StrQ;

                intDBId = (int)sqlCmd.ExecuteScalar();
                if (intDBId > 0) { mblnIsDatabaseAccessible = true; }

            }
            catch (Exception ex)
            {
                WriteToFile("IsDatabaseAccessible " + DateTime.Now + " : " + ex.Message);
            }
        }

        private double GetInitialInterval()
        {
            DateTime now = DateTime.Now;
            double timeToNextMin = ((60 - now.Second) * 1000 - now.Millisecond) + 15;
            nextIntervalTick = now.Ticks + ((long)timeToNextMin * TICKS_IN_MILLISECOND);

            return timeToNextMin;
        }

        private double GetInterval()
        {
            nextIntervalTick += TICKS_IN_MINUTE;
            return TicksToMs(nextIntervalTick - DateTime.Now.Ticks);
        }

        private double TicksToMs(long ticks)
        {
            return (double)(ticks / TICKS_IN_MILLISECOND);
        }

        #endregion

        //S.SANDEEP |08-APR-2019| -- START
        #region CBS Block Leave
        private DataTable GetLeaveData(string dbname)
        {
            DataTable dt = new DataTable();
            string StrQ = "";
            try
            {
                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }

                StrQ = "DECLARE @table AS TABLE (empid int, accno nvarchar(max)) " +
                       "IF OBJECT_ID('tempdb..#emp') IS NOT NULL " +
                       "DROP TABLE #emp " +
                       "SELECT " +
                       "     REPLACE(CONVERT(NVARCHAR(20),ISNULL(approve_stdate,startdate),102),'.','-') AS sDate " +
                       "    ,ISNULL(approve_eddate,returndate) AS eDate " +
                       "    ,lvleaveform.formunkid " +
                       "    ,lvleaveform.employeeunkid " +
                       "    ,lvleaveform.formno " +
                       "    ,CM.customcode " +
                       "    ,REPLACE(CONVERT(NVARCHAR(MAX),ISNULL(approve_stdate,startdate),106),' ','-') AS startdate " +
                       "INTO #emp " +
                       "FROM " + dbname + "..lvleaveform " +
                       "    JOIN " + dbname + "..hremployee_master EM ON EM.employeeunkid = lvleaveform.employeeunkid " +
                       "    LEFT JOIN " +
                       "	( " +
                       "		SELECT " +
                       "			 hremployee_cctranhead_tran.cctranheadvalueid " +
                       "			,hremployee_cctranhead_tran.employeeunkid " +
                       "			,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " +
                       "		FROM " + dbname + "..hremployee_cctranhead_tran " +
                       "		WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= @Date " +
                       "	) AS cc ON cc.employeeunkid = EM.employeeunkid AND cc.rno = 1 " +
                       "	JOIN " + dbname + "..prcostcenter_master AS CM ON cc.cctranheadvalueid = CM.costcenterunkid " +
                       "WHERE isvoid = 0 AND statusunkid IN (1,2,7) " +
                       "AND CONVERT(NVARCHAR(8),ISNULL(approve_stdate,startdate),112) <= @Date " +
                       "AND CONVERT(NVARCHAR(8),ISNULL(approve_eddate,returndate),112) >= @Date " +
                       "INSERT INTO @table(empid,accno) " +
                       "SELECT " +
                       "     A.employeeunkid " +
                       "    ,A.accountno " +
                       "FROM " +
                       "(" +
                       "    SELECT " +
                       "         premployee_bank_tran.accountno " +
                       "        ,premployee_bank_tran.employeeunkid " +
                       "        ,DENSE_RANK() OVER (PARTITION  BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " +
                       "    FROM " + dbname + "..premployee_bank_tran " +
                       "        JOIN #emp ON #emp.employeeunkid = premployee_bank_tran.employeeunkid " +
                       "        LEFT JOIN " + dbname + "..cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid AND isactive = 1 " +
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @Date " +
                       ") AS A WHERE A.ROWNO = 1 " +
                       "SELECT " +
                       "     #emp.formunkid " +
                       "    ,#emp.employeeunkid " +
                       "    ,#emp.formno " +
                       "    ,[@table].accno " +
                       "    ,#emp.customcode " +
                       "    ,#emp.startdate " +
                       "    ,#emp.sDate " +
                       "FROM #emp " +
                       "    JOIN @table ON [@table].empid = #emp.employeeunkid " +
                       "WHERE 1 = 1 " +
                       "IF OBJECT_ID('tempdb..#emp') IS NOT NULL " +
                       "DROP TABLE #emp ";

                //S.SANDEEP |04-NOV-2019| -- START
                //ISSUE/ENHANCEMENT : FLEX OBJECT REF. ERROR
                /*
                 * REMOVED : AND CONVERT(NVARCHAR(8),ISNULL(approve_stdate,startdate),112) = @Date
                 * ADDED   : AND CONVERT(NVARCHAR(8),ISNULL(approve_stdate,startdate),112) <= @Date 
                 *           AND CONVERT(NVARCHAR(8),ISNULL(approve_eddate,returndate),112) >= @Date                 
                 */
                //S.SANDEEP |04-NOV-2019| -- END

                sqlCmd.Parameters.Clear();
                sqlCmd.Parameters.AddWithValue("@Date", convertDate(DateTime.Now).ToString());
                sqlCmd.CommandText = StrQ;
                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                WriteToFile("GetLeaveData " + DateTime.Now + " : " + ex.Message);
            }
            return dt;
        }
        private DataTable GetFlexUserDetails(string acctnumber, int intcompanyid)
        {
            DataTable dc = new DataTable();
            string StrQ = "";
            try
            {
                DataTable dt = new DataTable();
                StrQ = "SELECT " +
                       "    CF.key_name " +
                       "   ,CF.key_value " +
                       "FROM hrmsConfiguration..cfconfiguration AS CF " +
                       "WHERE CF.[key_name] IN ('ORACLEHOSTNAME','ORACLEPORTNO','ORACLESERVICENAME','ORACLEUSERNAME','ORACLEUSERPASSWORD') " +
                       "AND CF.companyunkid = '" + intcompanyid + "' AND key_value <> '' ";

                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                sqlCmd.Parameters.Clear();
                sqlCmd.CommandText = StrQ;
                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    Dictionary<string, string> ora_Detail = new Dictionary<string, string>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ora_Detail.ContainsKey(dr["key_name"].ToString()) == false)
                        {
                            ora_Detail.Add(dr["key_name"].ToString(), dr["key_value"].ToString());
                        }
                    }

                    string strConn = "";
                    using (OracleConnection cnnOracle = new OracleConnection())
                    {
                        strConn = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ora_Detail["OracleHostName"].ToString() + ")(PORT=" + ora_Detail["OraclePortNo"].ToString() + ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + ora_Detail["OracleServiceName"].ToString() + "))); User Id=" + ora_Detail["OracleUserName"].ToString() + ";Password=" + clsSecurity.Decrypt(ora_Detail["OracleUserPassword"].ToString(), "ezee").ToString() + "; ";
                        cnnOracle.ConnectionString = strConn;
                        cnnOracle.Open();
                        StrQ = "SELECT " +
                               "     USER_ID " +
                               "    ,USER_NAME " +
                               "    ,BRANCH " +
                               "    ,time_level " +
                               "    ,start_date " +
                               "    ,USER_STATUS " +
                               "FROM fcubs.aruti_user " +
                               "WHERE CUST_AC_NO IN (" + acctnumber + ") ";
                        using (OracleCommand cmdOracle = new OracleCommand())
                        {
                            if (cnnOracle.State == ConnectionState.Closed || cnnOracle.State == ConnectionState.Broken)
                            {
                                cnnOracle.ConnectionString = strConn;
                                cnnOracle.Open();
                            }
                            cmdOracle.Connection = cnnOracle;
                            cmdOracle.CommandType = CommandType.Text;
                            cmdOracle.CommandText = StrQ;
                            OracleDataAdapter oda = new OracleDataAdapter(cmdOracle);
                            oda.Fill(dc);
                        }

                        //S.SANDEEP |29-APR-2019| -- START
                        //CHANGES MADE AS PER ZEESHAN'S COMMENTS FOR 2 TAGS
                        //TIMELEVEL -->  TO PICK FROM FLX USER TABLE VIEW COL.NAME [time_level]
                        //STRTDATE  -->  TO CHANGE THE FORMAT OLD [DD-MMM-YYYY] TO NEW [YYYY-MM-DD]
                        //S.SANDEEP |29-APR-2019| -- END
                        //S.SANDEEP |04-NOV-2019| -- START
                        //ISSUE/ENHANCEMENT : FLEX OBJECT REF. ERROR {USER_STATUS} -- END
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile("GetFlexUserDetails " + DateTime.Now + " : " + ex.Message);
            }

            return dc;
        }
        private string GenerateCBSFileString(string dbName, string flxuserid, string flxusername, string cc_code, string flxuserstate, string startdate, ref string strMsgNo, string flxbranch, string flxtimelevel)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            string strMessageId = "";
            string StrQ = "";
            try
            {
                int iCount = 1;
                while (iCount > 0)
                {
                    StrQ = "SELECT CONVERT(NVarChar(5), right(replace(convert(varchar, getdate(),114),':',''),5)) + CONVERT(NVarChar(5), right(newid(),5)) ";
                    sqlCmd.CommandText = StrQ;
                    strMessageId = (string)sqlCmd.ExecuteScalar();

                    StrQ = "SELECT COUNT(1) FROM " + dbName + "..hrflexcube_request_tran WHERE fmsgeid = '" + strMessageId + "' ";
                    sqlCmd.CommandText = StrQ;
                    iCount = Convert.ToInt32(sqlCmd.ExecuteScalar());

                    if (iCount <= 0) { break; }
                }
                strMsgNo = strMessageId;

                StrQ = "SELECT TOP 1 " +
                       "  '<?xml version=\"1.0\" encoding=\"UTF-8\"?>' " +
                       ", '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"http://fcubs.ofss.com/service/FCUBSACService\">' " +
                       ", '<soapenv:Header/>' " +
                       ", '<soapenv:Body>'" +
                       ", '<MODIFYUSERMAINT_FSFS_REQ xmlns=\"http://fcubs.ofss.com/service/FCUBSSMService\">' " +
                       ", '<FCUBS_HEADER>' " +
                       ", '<SOURCE>ARUTI</SOURCE>' " +
                       ", '<UBSCOMP>FCUBS</UBSCOMP>' " +
                       ", '<MSGID>' + '" + strMessageId + "' + '</MSGID>' " +
                       ", '<CORRELID>' + '" + strMessageId + "' + '</CORRELID>' " +
                       ", '<USERID>ARUTI</USERID>' " +
                       ", '<BRANCH>101</BRANCH>' " +
                       ", '<MODULEID>SM</MODULEID>' " +
                       ", '<SERVICE>FCUBSSMService</SERVICE>' " +
                       ", '<OPERATION>ModifyUserMaint</OPERATION>' " +
                       ", '<SOURCE_OPERATION>ModifyUserMaint</SOURCE_OPERATION>' " +
                       ", '<SOURCE_USERID/>' " +
                       ", '<DESTINATION/>' " +
                       ", '<MULTITRIPID/>' " +
                       ", '<FUNCTIONID/>' " +
                       ", '<ACTION></ACTION>' " +
                       ", '</FCUBS_HEADER>' " +
                       ", '<FCUBS_BODY>' " +
                       ", '<USR-Full>' " +
                       ", '<USRID>'+ '" + flxuserid + "' +'</USRID>' " +
                       ", '<USRNAME>'+ '" + flxusername.Replace("'", "''") + "' +'</USRNAME>' " +
                       ", '<HOMEBRN>'+'" + flxbranch + "'+'</HOMEBRN>' " +
                       ", '<USRSTAT>'+'" + flxuserstate + "'+'</USRSTAT>' " +
                       ", '<USRLANG>ENG</USRLANG>' " +
                       ", '<TIMELEVEL>'+'" + flxtimelevel + "'+'</TIMELEVEL>' " +
                       ", '<STRTDATE>'+ '" + startdate + "' +'</STRTDATE>' " +
                       ", '</USR-Full>' " +
                       ", '</FCUBS_BODY>' " +
                       ", '</MODIFYUSERMAINT_FSFS_REQ>' " +
                       ", '</soapenv:Body>' " +
                       ", '</soapenv:Envelope>' ";

                sqlCmd.CommandText = StrQ;
                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        foreach (DataColumn c in dt.Columns)
                        {
                            string strData = ""; strData = r[c].ToString();
                            SetXMLFormat(ref strData);
                            sb.Append(strData + Environment.NewLine);
                        }
                    }
                }
                //S.SANDEEP |29-APR-2019| -- START
                //CHANGES MADE AS PER ZEESHAN'S COMMENTS FOR 2 TAGS
                //TIMELEVEL -->  TO PICK FROM FLX USER TABLE VIEW COL.NAME [time_level]
                //STRTDATE  -->  TO CHANGE THE FORMAT OLD [DD-MMM-YYYY] TO NEW [YYYY-MM-DD]
                //S.SANDEEP |29-APR-2019| -- END
            }
            catch (Exception ex)
            {
                WriteToFile("GenerateCBSFileString QUERY : " + StrQ);
                WriteToFile("GenerateCBSFileString " + DateTime.Now + " : " + ex.Message);
            }
            return sb.ToString();
        }
        #endregion
        //S.SANDEEP |08-APR-2019| -- END

        #region ACTIVE DIRECTORY

        private DataTable GetCurrentDBNameWIthADSettings(ref string mstrADIPAddress, ref string mstrADDomain, ref string mstrADDomainUser, ref string mstrADUserPwd)
        {
            DataTable dt = new DataTable();
            string StrQ = "";
            try
            {

                StrQ = "SELECT ISNULL(key_value,'') AS key_value FROM hrmsConfiguration..cfconfiguration  WHERE UPPER([key_name]) = 'CREATEADUSERFROMEMPMST' AND companyunkid = -999";
                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                sqlCmd.Parameters.Clear();
                sqlCmd.CommandText = StrQ;
                bool mblnADUserFromEmpMst = Convert.ToBoolean(sqlCmd.ExecuteScalar());

                if (mblnADUserFromEmpMst)
                {

                    StrQ = " SELECT ISNULL([key_name],'') AS [Key_name] " +
                               ", ISNULL([key_value],'') AS [Key_value] " +
                               " FROM hrmsConfiguration..cfconfiguration  " +
                               " WHERE UPPER([key_name]) IN ('ADIPADDRESS','ADDOMAINUSER','ADDOMAINUSERPWD','ADDOMAIN')  AND companyunkid = -999 AND [key_value] <> '' AND [key_value] is not NULL ";

                    if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                    sqlCmd.Parameters.Clear();
                    sqlCmd.CommandText = StrQ;

                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                    da.Fill(dt);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["Key_name"].ToString().ToUpper() == "ADIPADDRESS")
                            {
                                mstrADIPAddress = dr["Key_value"].ToString();
                            }
                            else if (dr["Key_name"].ToString().ToUpper() == "ADDOMAIN")
                            {
                                mstrADDomain = dr["Key_value"].ToString();

                                string[] ar = null;
                                if (mstrADDomain.Trim().Length > 0 && mstrADDomain.Trim().Contains("."))
                                {
                                    ar = mstrADDomain.Trim().Split('.');
                                    mstrADDomain = "";
                                    if (ar.Length > 0)
                                    {
                                        for (int i = 0; i < ar.Length; i++)
                                        {
                                            mstrADDomain += ",DC=" + ar[i];
                                        }
                                    }
                                }
                                if (mstrADDomain.Trim().Length > 0)
                                {
                                    mstrADDomain = mstrADDomain.Trim().Substring(1);
                                }

                            }
                            else if (dr["Key_name"].ToString().ToUpper() == "ADDOMAINUSER")
                            {
                                mstrADDomainUser = dr["Key_value"].ToString();
                            }
                            else if (dr["Key_name"].ToString().ToUpper() == "ADDOMAINUSERPWD")
                            {
                                mstrADUserPwd = clsSecurity.Decrypt(dr["Key_value"].ToString(), "ezee");
                            }
                        }
                    }

                    dt = null;
                    da = null;
                    StrQ = " SELECT yearunkid,database_name,companyunkid,start_date,end_date   " +
                               " FROM hrmsConfiguration..cffinancial_year_tran  " +
                               " WHERE isclosed = 0 ";

                    if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                    sqlCmd.Parameters.Clear();
                    sqlCmd.CommandText = StrQ;

                    dt = new DataTable();
                    da = new SqlDataAdapter(sqlCmd);
                    da.Fill(dt);

                }

            }
            catch (SqlException ex)
            {
                WriteToFile("GetCurrentDBNameWIthADSettings SQL " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
            catch (Exception ex)
            {
                WriteToFile("GetCurrentDBNameWIthADSettings " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
            return dt;
        }

        private DataTable GetEmployeeDeptJobOnEffectiveDate(string mstrDatabaseName, string mstrAsonDate)
        {
            DataTable dtEmployee = new DataTable();
            string StrQ = "";
            try
            {
                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }
                string xDateJoinQry, xDataFilterQry;
                xDateJoinQry = ""; xDataFilterQry = "";
                GetDatesFilterString(ref xDateJoinQry, ref xDataFilterQry, convertDate(mstrAsonDate).Date, convertDate(mstrAsonDate).Date, true, mstrDatabaseName, "hremployee_master");

                StrQ = " SELECT " +
                           "     hremployee_master.employeeunkid " +
                           "    ,ISNULL(hremployee_master.employeecode,'') AS employeecode " +
                           "    ,ISNULL(hremployee_master.firstname,'') AS firstname " +
                           "    ,ISNULL(hremployee_master.othername,'') AS othername " +
                           "    ,ISNULL(hremployee_master.surname,'') AS surname " +
                           "    ,ISNULL(hremployee_master.displayname,'') AS displayname " +
                           "    ,ISNULL(hrdepartment_master.name,'') AS Department " +
                           "    ,ISNULL(hrjob_master.job_name,'') AS Job " +
                           " FROM " + mstrDatabaseName + "..hremployee_master " +
                           " JOIN " +
                           " ( " +
                           "    SELECT " +
                           "        departmentunkid " +
                           "       ,employeeunkid " +
                           "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " +
                           "    FROM " + mstrDatabaseName + "..hremployee_transfer_tran " +
                           "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsonDate " +
                           "  ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " +
                           " JOIN " + mstrDatabaseName + "..hrdepartment_master ON hrdepartment_master.departmentunkid = T.departmentunkid " +
                           " JOIN " +
                           " ( " +
                           "         SELECT " +
                           "         jobunkid " +
                           "        ,employeeunkid " +
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " +
                           "    FROM " + mstrDatabaseName + "..hremployee_categorization_tran " +
                           "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsonDate " +
                           " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " +
                           " JOIN " + mstrDatabaseName + "..hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid ";

                if (xDateJoinQry.Trim().Length > 0)
                {
                    StrQ += " " + xDateJoinQry;
                }

                StrQ += " WHERE hremployee_master.isapproved = 1  ";

                if (xDataFilterQry.Trim().Length > 0)
                {
                    StrQ += " " + xDataFilterQry;
                }
                sqlCmd.Parameters.Clear();
                sqlCmd.Parameters.Add("@EmpAsonDate", SqlDbType.NVarChar).Value = mstrAsonDate;
                sqlCmd.CommandText = StrQ;
                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(dtEmployee);
            }
            catch (SqlException ex)
            {
                WriteToFile("GetEmployeeDeptJobOnEffectiveDate SQL " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
            catch (Exception ex)
            {
                WriteToFile("GetEmployeeDeptJobOnEffectiveDate " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
            return dtEmployee;
        }

        private bool IsDisabledADUserFromEmpDates(string mstrDatabaseName, string mstrAsonDate, int mintEmployeeId, ref DateTime xRehireDate)
        {
            DataTable dtEmployee = new DataTable();
            string StrQ = "";
            try
            {
                if (sqlCmd == null) { sqlCmd = new SqlCommand(); sqlCmd.Connection = sqlCn; }

                StrQ = "SELECT" +
                         "	hremployee_master.employeeunkid" +
                         "  ,ETERM.empl_enddate AS 'EOC Date' " +
                         "  ,ETERM.termination_from_date AS 'Leaving Date ' " +
                         "  ,ERET.termination_to_date AS 'Retirement Date' " +
                         "  ,ERH.reinstatment_date AS 'Rehire Date' " +
                         " FROM " + mstrDatabaseName + "..hremployee_master" +
                         " LEFT JOIN " +
                         "  ( " +
                         "      SELECT " +
                         "		    TERM.TEEmpId" +
                         "	       ,TERM.empl_enddate" +
                         "	       ,TERM.termination_from_date" +
                         "	       ,TERM.TEfDt" +
                         "	    FROM ( " +
                         "              SELECT" +
                         "			        TRM.employeeunkid AS TEEmpId" +
                         "		            ,CONVERT(CHAR(8), TRM.date1, 112) AS empl_enddate" +
                         "		            ,CONVERT(CHAR(8), TRM.date2, 112) AS termination_from_date" +
                         "		            ,CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt" +
                         "		            ,TRM.isexclude_payroll" +
                         "		            ,ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno" +
                         "		        FROM  " + mstrDatabaseName + "..hremployee_dates_tran AS TRM" +
                         "		        WHERE isvoid = 0 AND TRM.datetypeunkid =  " + (int)enEmp_Dates_Transaction.DT_TERMINATION + "  AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @EmpAsonDate) AS TERM WHERE TERM.Rno = 1 " +
                         "           ) AS ETERM 	ON ETERM.TEEmpId = hremployee_master.employeeunkid 	AND ETERM.TEfDt >= CONVERT(CHAR(8), appointeddate, 112) " +
                         " LEFT JOIN " +
                         "  ( " +
                         "      SELECT " +
                         "		RET.REmpId" +
                         "	   ,RET.termination_to_date" +
                         "	   ,RET.REfDt" +
                         "	FROM ( " +
                         "              SELECT" +
                         "			        RTD.employeeunkid AS REmpId " +
                         "		           ,CONVERT(CHAR(8), RTD.date1, 112) AS termination_to_date " +
                         "		           ,CONVERT(CHAR(8), RTD.effectivedate, 112) AS REfDt " +
                         "		           ,ROW_NUMBER() OVER (PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " +
                         "		        FROM " + mstrDatabaseName + "..hremployee_dates_tran AS RTD " +
                         "		        WHERE isvoid = 0 AND RTD.datetypeunkid = " + (int)enEmp_Dates_Transaction.DT_RETIREMENT + " AND CONVERT(CHAR(8), RTD.effectivedate, 112) <= '201809026') AS RET WHERE RET.Rno = 1 " +
                         "            )  AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8), appointeddate, 112) " +
                         " LEFT JOIN  " +
                         "  ( " +
                         "      SELECT " +
                         "		     RH.RHEmpId " +
                         "	        ,RH.reinstatment_date " +
                         "	        ,RH.RHEfDt " +
                         "	    FROM ( " +
                         "                  SELECT" +
                         "			             ERT.employeeunkid AS RHEmpId " +
                         "		                ,CONVERT(CHAR(8), ERT.reinstatment_date, 112) AS reinstatment_date " +
                         "		                ,CONVERT(CHAR(8), ERT.effectivedate, 112) AS RHEfDt " +
                         "		                ,ROW_NUMBER() OVER (PARTITION BY ERT.employeeunkid ORDER BY ERT.effectivedate DESC) AS Rno " +
                         "		            FROM " + mstrDatabaseName + "..hremployee_rehire_tran AS ERT " +
                         "		            WHERE isvoid = 0 AND CONVERT(CHAR(8), ERT.effectivedate, 112) <= @EmpAsonDate) AS RH	WHERE RH.Rno = 1 " +
                         "               )   AS ERH ON ERH.RHEmpId = hremployee_master.employeeunkid AND ERH.RHEfDt >= CONVERT(CHAR(8), appointeddate, 112) " +
                         " WHERE hremployee_master.employeeunkid = @EmployeeId ";

                sqlCmd.Parameters.Clear();
                sqlCmd.Parameters.Add("@EmpAsonDate", SqlDbType.NVarChar).Value = mstrAsonDate;
                sqlCmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = mintEmployeeId;
                sqlCmd.CommandText = StrQ;
                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(dtEmployee);

                DateTime dtRehireDate = default(DateTime);
                xRehireDate = default(DateTime);
                if (dtEmployee != null && dtEmployee.Rows.Count > 0)
                {
                    List<DateTime> lstEmpDates = new List<DateTime>();

                    foreach (DataColumn dc in dtEmployee.Columns)
                    {
                        if (dc.ColumnName.ToUpper() == "EMPLOYEEUNKID") continue;

                        if (dc.ColumnName.ToUpper() != "REHIRE DATE")
                        {
                            if (dtEmployee.Rows[0][dc] != DBNull.Value && dtEmployee.Rows[0][dc].ToString() != "")
                            {
                                lstEmpDates.Add(convertDate(dtEmployee.Rows[0][dc].ToString()).Date);
                            }
                        }
                        else
                        {
                            if (dtEmployee.Rows[0][dc] != DBNull.Value && dtEmployee.Rows[0][dc].ToString() != "")
                            {
                                dtRehireDate = convertDate(dtEmployee.Rows[0][dc].ToString()).Date;
                            }
                        }
                    }

                    if (lstEmpDates != null && lstEmpDates.Count > 0)
                    {
                        DateTime dtTerminationDate = lstEmpDates.Min<DateTime>();
                        if (dtTerminationDate != default(DateTime) && dtTerminationDate <= DateTime.Now.Date)
                        {
                            return true;
                        }
                        if (dtRehireDate != default(DateTime) && dtRehireDate.Date <= DateTime.Now.Date)
                        {
                            xRehireDate = dtRehireDate;
                            return true;
                        }
                    }
                    else
                    {
                        if (dtRehireDate != default(DateTime) && dtRehireDate.Date <= DateTime.Now.Date)
                        {
                            xRehireDate = dtRehireDate;
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile("IsDisabledADUserFromEmpDates " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
            return false;
        }

        private SearchResult IsADUserExist(string mstrEmpDisplayName, string mstrADIPAddress, string mstrADDomain, string mstrADUserName, string mstrADUserPwd)
        {
            DirectoryEntry entry = null;
            SearchResult result = null;
            try
            {


                /* START FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT */
                entry = new DirectoryEntry("LDAP://" + mstrADIPAddress.Trim() + "/" + mstrADDomain.Trim(), mstrADUserName.Trim(), mstrADUserPwd.Trim());
                object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + mstrEmpDisplayName + ")";
                search.PropertiesToLoad.Add("cn");
                result = search.FindOne();

                if (result != null)
                {
                    return result;
                }
                /* END FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT */
            }
            catch (Exception ex)
            {
                WriteToFile("IsADUserExist " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
            finally
            {
                entry.Close();
            }
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return result;
        }

        public void SetADProperty(DirectoryEntry de, string pName, string pValue)
        {
            try
            {
                if (pValue != null)
                {
                    if (de.Properties.Contains(pName)) //The DE contains this property
                    {
                        de.Properties[pName].Value = pValue; //Update the properties value
                    }
                    else //Property doesnt exist
                    {
                        de.Properties[pName].Add(pValue); //Add the property and set it's value
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile("SetADProperty " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
        }

        private void EnableDisableActiveDirectoryUser(bool blnEnable, string mstrUserName, string mstrADIPAddress, string mstrADDomain, string mstrADDomainUser, string mstrADDomainUserPwd)
        {
            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://" + mstrADIPAddress.Trim() + "/" + mstrADDomain.Trim(), mstrADDomainUser.Trim(), mstrADDomainUserPwd.Trim());
                object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + mstrUserName + ")";
                search.PropertiesToLoad.Add("cn");
                search.PropertiesToLoad.Add("userAccountControl");
                SearchResult result = search.FindOne();
                if (result != null)
                {
                    DirectoryEntry objUserEntry = result.GetDirectoryEntry();
                    int iValue = Convert.ToInt32(objUserEntry.Properties["userAccountControl"].Value);
                    if (blnEnable) // Enable AD User
                    {
                        objUserEntry.Properties["userAccountControl"].Value = iValue & ~0x2;
                    }
                    else // DisableAD User
                    {
                        objUserEntry.Properties["userAccountControl"].Value = iValue | 0x2;
                    }
                    objUserEntry.CommitChanges();
                    objUserEntry.Close();
                }
            }
            catch (Exception ex)
            {
                WriteToFile("EnableDisableActiveDirectoryUser " + DateTime.Now + " : " + ex.InnerException + Environment.NewLine + ex.Message);
            }
        }

        #endregion

        #endregion
    }
}
