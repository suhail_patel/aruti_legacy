﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUpdateDatabase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUpdateDatabase))
        Me.bgWorker = New System.ComponentModel.BackgroundWorker
        Me.pbProgress = New System.Windows.Forms.ProgressBar
        Me.lblCaption = New System.Windows.Forms.Label
        Me.lblDataCount = New System.Windows.Forms.Label
        Me.lblDBName = New System.Windows.Forms.Label
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.pnlError = New System.Windows.Forms.Panel
        Me.objlblError = New System.Windows.Forms.Label
        Me.txtError = New System.Windows.Forms.RichTextBox
        Me.pnlError.SuspendLayout()
        Me.SuspendLayout()
        '
        'bgWorker
        '
        Me.bgWorker.WorkerReportsProgress = True
        Me.bgWorker.WorkerSupportsCancellation = True
        '
        'pbProgress
        '
        Me.pbProgress.Location = New System.Drawing.Point(0, 110)
        Me.pbProgress.Name = "pbProgress"
        Me.pbProgress.Size = New System.Drawing.Size(409, 21)
        Me.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbProgress.TabIndex = 0
        '
        'lblCaption
        '
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(408, 53)
        Me.lblCaption.TabIndex = 1
        Me.lblCaption.Text = "Updating Database, Please Wait for Sometime."
        '
        'lblDataCount
        '
        Me.lblDataCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDataCount.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lblDataCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDataCount.ForeColor = System.Drawing.Color.Red
        Me.lblDataCount.Location = New System.Drawing.Point(0, 55)
        Me.lblDataCount.Name = "lblDataCount"
        Me.lblDataCount.Size = New System.Drawing.Size(409, 18)
        Me.lblDataCount.TabIndex = 2
        Me.lblDataCount.Tag = "pRofessionalaRuti999"
        '
        'lblDBName
        '
        Me.lblDBName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDBName.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lblDBName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBName.ForeColor = System.Drawing.Color.ForestGreen
        Me.lblDBName.Location = New System.Drawing.Point(0, 73)
        Me.lblDBName.Name = "lblDBName"
        Me.lblDBName.Size = New System.Drawing.Size(409, 18)
        Me.lblDBName.TabIndex = 3
        Me.lblDBName.Tag = ""
        Me.lblDBName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPercentage
        '
        Me.lblPercentage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPercentage.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lblPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage.ForeColor = System.Drawing.Color.Maroon
        Me.lblPercentage.Location = New System.Drawing.Point(0, 91)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(409, 18)
        Me.lblPercentage.TabIndex = 4
        Me.lblPercentage.Tag = ""
        Me.lblPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlError
        '
        Me.pnlError.AutoScroll = True
        Me.pnlError.Controls.Add(Me.txtError)
        Me.pnlError.Controls.Add(Me.objlblError)
        Me.pnlError.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlError.Location = New System.Drawing.Point(0, 132)
        Me.pnlError.Name = "pnlError"
        Me.pnlError.Size = New System.Drawing.Size(408, 78)
        Me.pnlError.TabIndex = 5
        '
        'objlblError
        '
        Me.objlblError.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblError.ForeColor = System.Drawing.Color.Red
        Me.objlblError.Location = New System.Drawing.Point(393, 63)
        Me.objlblError.Name = "objlblError"
        Me.objlblError.Size = New System.Drawing.Size(10, 10)
        Me.objlblError.TabIndex = 0
        Me.objlblError.Visible = False
        '
        'txtError
        '
        Me.txtError.BackColor = System.Drawing.SystemColors.Control
        Me.txtError.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtError.Location = New System.Drawing.Point(0, 0)
        Me.txtError.Name = "txtError"
        Me.txtError.ReadOnly = True
        Me.txtError.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.txtError.ShortcutsEnabled = False
        Me.txtError.Size = New System.Drawing.Size(408, 78)
        Me.txtError.TabIndex = 1
        Me.txtError.Text = ""
        '
        'frmUpdateDatabase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(408, 210)
        Me.Controls.Add(Me.pnlError)
        Me.Controls.Add(Me.lblPercentage)
        Me.Controls.Add(Me.lblDBName)
        Me.Controls.Add(Me.lblDataCount)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.pbProgress)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUpdateDatabase"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aruti"
        Me.pnlError.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents pbProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents lblDataCount As System.Windows.Forms.Label
    Friend WithEvents lblDBName As System.Windows.Forms.Label
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents pnlError As System.Windows.Forms.Panel
    Friend WithEvents objlblError As System.Windows.Forms.Label
    Friend WithEvents txtError As System.Windows.Forms.RichTextBox

End Class
