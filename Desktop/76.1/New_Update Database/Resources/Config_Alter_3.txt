<?xml version="1.0" standalone="yes"?>
<Data>
<xs:schema id="Data" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
<xs:element name="Data" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
<xs:complexType>
<xs:choice minOccurs="0" maxOccurs="unbounded">
<xs:element name="Data">
<xs:complexType>
<xs:sequence>
<xs:element name="Id" type="xs:int" minOccurs="0" />
<xs:element name="Version" type="xs:string" minOccurs="0" />
<xs:element name="CheckType" type="xs:int" minOccurs="0" />
<xs:element name="ConditionValue1" type="xs:string" minOccurs="0" />
<xs:element name="ConditionValue2" type="xs:string" minOccurs="0" />
<xs:element name="Script" type="xs:string" minOccurs="0" />
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:choice>
</xs:complexType>
</xs:element>
</xs:schema>
<Data>
<Id>1</Id>
<Version>9.0.46.1</Version>
<CheckType>90461</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfbankbranch_master' AND COLUMN_NAME = 'isclearing')
BEGIN
	ALTER TABLE cfbankbranch_master ADD isclearing BIT
	EXECUTE('UPDATE cfbankbranch_master SET isclearing = 1 WHERE isclearing IS NULL')
END
</Script>
</Data>
<Data>
<Id>2</Id>
<Version>9.0.48.1</Version>
<CheckType>90481</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_master' AND COLUMN_NAME = 'stamp')
BEGIN
	ALTER TABLE cfcompany_master ADD stamp IMAGE
END
</Script>
</Data>
<Data>
<Id>3</Id>
<Version>9.0.48.1</Version>
<CheckType>90481</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfpayrollgroup_master' AND COLUMN_NAME = 'referenceno')
BEGIN
	ALTER TABLE cfpayrollgroup_master ADD referenceno cName
	EXECUTE('UPDATE cfpayrollgroup_master SET referenceno = '''' WHERE referenceno IS NULL')
END
</Script>
</Data>
<Data>
<Id>4</Id>
<Version>9.0.58.1</Version>
<CheckType>90581</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfuserprivilegegroup_master' AND COLUMN_NAME = 'isactive')
BEGIN
	ALTER TABLE cfuserprivilegegroup_master ADD isactive BIT DEFAULT CAST(1 AS BIT)
	EXECUTE('UPDATE cfuserprivilegegroup_master SET isactive = 1 WHERE isactive IS NULL')
	EXECUTE('UPDATE cfuserprivilegegroup_master SET isactive = 0 WHERE privilegegroupunkid IN(12,13,14)')
	EXECUTE('UPDATE cfuserprivilege_master SET isactive = 0,isdeleted = ''Y'' WHERE privilegegroupunkid IN(12,13,14)')
END
</Script>
</Data>
<Data>
<Id>5</Id>
<Version>9.0.58.1</Version>
<CheckType>90581</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfpasswd_options' AND COLUMN_NAME = 'isenablealluser')
BEGIN
	ALTER TABLE cfpasswd_options ADD isenablealluser BIT DEFAULT CAST(0 AS BIT)
	EXECUTE('UPDATE cfpasswd_options SET isenablealluser = CAST(0 AS BIT) WHERE isenablealluser IS NULL')
END
</Script>
</Data>
<Data>
<Id>6</Id>
<Version>9.0.62.1</Version>
<CheckType>90621</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfconfiguration' AND COLUMNS.COLUMN_NAME = 'key_value' AND COLUMNS.CHARACTER_MAXIMUM_LENGTH = '8000')
BEGIN
	ALTER TABLE cfconfiguration ALTER COLUMN key_value varchar(max)
END
</Script>
</Data>
<Data>
<Id>7</Id>
<Version>9.0.65.1</Version>
<CheckType>90651</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfuser_master' AND COLUMNS.COLUMN_NAME= 'theme_id')
BEGIN
	ALTER TABLE cfuser_master ADD theme_id INT NULL DEFAULT 1
	EXECUTE('UPDATE cfuser_master SET theme_id = 1 WHERE theme_id IS NULL')
END
</Script>
</Data>
<Data>
<Id>8</Id>
<Version>9.0.65.1</Version>
<CheckType>90651</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfuser_master' AND COLUMNS.COLUMN_NAME= 'lastview_id')
BEGIN
	ALTER TABLE cfuser_master ADD lastview_id INT NULL DEFAULT 0
	EXECUTE('UPDATE cfuser_master SET lastview_id = 0 WHERE lastview_id IS NULL')
END
</Script>
</Data>
<Data>
<Id>9</Id>
<Version>9.0.65.1</Version>
<CheckType>90651</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfuser_master' AND COLUMNS.COLUMN_NAME = 'ismanager')
BEGIN
	UPDATE cfuser_master SET ismanager = 1 WHERE cfuser_master.ismanager IS NULL AND cfuser_master.userunkid = 1
END
</Script>
</Data>
<Data>
<Id>10</Id>
<Version>9.0.69.1</Version>
<CheckType>90691</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_master' AND COLUMN_NAME = 'iscrt_authenticated')
BEGIN
	ALTER TABLE cfcompany_master ADD iscrt_authenticated BIT
	EXECUTE('UPDATE cfcompany_master SET iscrt_authenticated = CAST(0 AS BIT) WHERE iscrt_authenticated IS NULL')
END
</Script>
</Data>
<Data>
<Id>11</Id>
<Version>9.0.69.1</Version>
<CheckType>90691</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'cfenabledisable_aduser')
BEGIN
	IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'cfuseraccess_privilege')
	BEGIN
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_uaccess_privilege' AND OBJECT_NAME(parent_object_id) = 'cfenabledisable_aduser')
		BEGIN
			exec sp_rename @objname = N'[cfenabledisable_aduser].[PK_uaccess_privilege]', @newname = N'PK_useraccess_privilege'
		END
		exec sp_rename 'dbo.cfuseraccess_privilege', 'cfenabledisable_aduser'
		EXEC('Truncate Table cfenabledisable_aduser')
	END
END
</Script>
</Data>
<Data>
<Id>12</Id>
<Version>9.0.69.1</Version>
<CheckType>90691</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfenabledisable_aduser' AND COLUMN_NAME = 'jobunkid')
BEGIN
	exec sp_rename 'cfenabledisable_aduser.jobunkid' , 'timestamp', 'COLUMN'
END
</Script>
</Data>
<Data>
<Id>13</Id>
<Version>9.0.69.1</Version>
<CheckType>90691</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfenabledisable_aduser' AND COLUMN_NAME = 'useraccessprivilegeunkid')
BEGIN
	exec sp_rename 'cfenabledisable_aduser.useraccessprivilegeunkid' , 'tranid', 'COLUMN'
END
</Script>
</Data>
<Data>
<Id>14</Id>
<Version>9.0.70.1</Version>
<CheckType>90691</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
	UPDATE cfuserprivilege_master SET cfuserprivilege_master.privilegegroupunkid = 7 WHERE cfuserprivilege_master.privilegeunkid = 1050 AND cfuserprivilege_master.privilegegroupunkid = 4
</Script>
</Data>
<Data>
<Id>15</Id>
<Version>9.0.70.1</Version>
<CheckType>90701</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_master' AND COLUMN_NAME = 'email_type')
	BEGIN 
		ALTER TABLE cfcompany_master ADD email_type INT NULL DEFAULT 0
		EXEC sp_addextendedproperty
		@name = N'MS_Description' ,
		@value = N'Used to store Email Type to send an email.' ,
		@level0type = N'Schema', 
		@level0name = dbo ,
		@level1type = N'Table',
		@level1name = 'cfcompany_master' ,
		@level2type = N'Column',
		@level2name = 'email_type';
		EXECUTE('UPDATE cfcompany_master SET email_type = 0 WHERE email_type IS NULL')
	END
</Script>
</Data>
<Data>
<Id>16</Id>
<Version>9.0.70.1</Version>
<CheckType>90701</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_master' AND COLUMN_NAME = 'ews_url')
	BEGIN 
		ALTER TABLE cfcompany_master ADD ews_url cName NULL DEFAULT ''
		EXEC sp_addextendedproperty
		@name = N'MS_Description' ,
		@value = N'Used to store Web Service URL for Exchange Server Web Service.' ,
		@level0type = N'Schema', 
		@level0name = dbo ,
		@level1type = N'Table',
		@level1name = 'cfcompany_master' ,
		@level2type = N'Column',
		@level2name = 'ews_url';
		EXECUTE('UPDATE cfcompany_master SET ews_url = '''' WHERE ews_url IS NULL')
	END
</Script>
</Data>
<Data>
<Id>17</Id>
<Version>9.0.70.1</Version>
<CheckType>90701</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_master' AND COLUMN_NAME = 'ews_domain')
	BEGIN 
		ALTER TABLE cfcompany_master ADD ews_domain cName NULL DEFAULT ''
		EXEC sp_addextendedproperty
		@name = N'MS_Description' ,
		@value = N'Used to store Web Service Domain for Exchange Server Web Service.' ,
		@level0type = N'Schema', 
		@level0name = dbo ,
		@level1type = N'Table',
		@level1name = 'cfcompany_master' ,
		@level2type = N'Column',
		@level2name = 'ews_domain';
		EXECUTE('UPDATE cfcompany_master SET ews_domain = '''' WHERE ews_domain IS NULL')
	END
</Script>
</Data>
<Data>
<Id>18</Id>
<Version>9.0.75.1</Version>
<CheckType>90751</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[	
	IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_master' AND COLUMN_NAME = 'isbypassproxy')
	BEGIN
		ALTER TABLE cfcompany_master ADD isbypassproxy BIT
		EXEC('UPDATE cfcompany_master SET isbypassproxy = 0 WHERE isbypassproxy IS NULL')
		EXEC sys.sp_addextendedproperty N'MS_Description','Used to Identify if proxy is enabled while sending emails','SCHEMA',N'dbo','TABLE',N'cfcompany_master','COLUMN',N'isbypassproxy'
	END
]]>
</Script>
</Data>
<Data>
<Id>19</Id>
<Version>9.0.75.1</Version>
<CheckType>90751</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[	
	IF NOT EXISTS (SELECT  COLUMN_NAME From INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcountry_master' and COLUMN_NAME IN ('isocode','isosname')) 
	BEGIN
		ALTER TABLE cfcountry_master add isocode nvarchar(50)
		EXECUTE('UPDATE cfcountry_master SET isocode = '''' WHERE isocode IS NULL')
		EXEC sys.sp_addextendedproperty N'MS_Description','Used to Identify iso numeric code of each country for active directory','SCHEMA',N'dbo','TABLE',N'cfcountry_master','COLUMN',N'isocode'
		
		ALTER TABLE cfcountry_master add isosname nvarchar(50)
		EXECUTE('UPDATE cfcountry_master SET isosname = '''' WHERE isosname IS NULL')
		EXEC sys.sp_addextendedproperty N'MS_Description','Used to Identify iso abbrv. code/name of each country for active directory','SCHEMA',N'dbo','TABLE',N'cfcountry_master','COLUMN',N'isosname'		
		EXECUTE('UPDATE cfcountry_master SET alias =''MNE'' WHERE country_name = ''Montenegro''')
		EXECUTE('UPDATE cfcountry_master SET alias =''PSE'' WHERE country_name = ''Palestine''')
		EXECUTE('UPDATE cfcountry_master SET alias =''YUG'' WHERE country_name = ''Yugoslavia''')		
	END
]]>
</Script>
</Data>
<Data>
<Id>20</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[	
	/*IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cfuser_privilege]') AND name = N'NCIdx_User_Privileges')
	BEGIN
		CREATE NONCLUSTERED INDEX [NCIdx_User_Privileges]
		ON [dbo].[cfuser_privilege] ([privilegeunkid])
		INCLUDE ([userunkid])	
	END*/
]]>
</Script>
</Data>
<Data>
<Id>21</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[	
	/*IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cfuseraccess_privilege_tran]') AND name = N'NCIdx_UserAccessPrivileges')
	BEGIN
		CREATE NONCLUSTERED INDEX [NCIdx_UserAccessPrivileges]
		ON [dbo].[cfuseraccess_privilege_tran] ([useraccessprivilegeunkid])
		INCLUDE ([allocationunkid])
	END*/
]]>
</Script>
</Data>
<Data>
<Id>22</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[	
	/*IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cfuseraccess_privilege_master]') AND name = N'NCIdx_UserAccessPrivilegeMaster_CYR')
	BEGIN
		CREATE NONCLUSTERED INDEX [NCIdx_UserAccessPrivilegeMaster_CYR]
		ON [dbo].[cfuseraccess_privilege_master] ([companyunkid],[yearunkid],[referenceunkid])
		INCLUDE ([userunkid])
	END*/
]]>
</Script>
</Data>
<Data>
<Id>23</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[	
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'cfconfiguration' AND COLUMN_NAME = 'Syncdatetime')
BEGIN
    ALTER TABLE cfconfiguration ADD Syncdatetime DATETIME        
	EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'Used to store Syncdatetime' ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'cfconfiguration',@level2type=N'COLUMN',@level2name=N'Syncdatetime'
END	
]]>
</Script>
</Data>
<Data>
<Id>24</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[	
IF NOT EXISTS ( SELECT  COLUMN_NAME From INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfadattribute_mapping' and COLUMN_NAME = 'companyunkid' ) 
BEGIN
	ALTER TABLE cfadattribute_mapping ADD companyunkid int
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to store company id.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfadattribute_mapping', @level2type=N'COLUMN',@level2name=N'companyunkid'
END
]]>
</Script>
</Data>
<Data>
<Id>25</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[	
IF NOT EXISTS ( SELECT  COLUMN_NAME From INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'atcfadattribute_mapping' and COLUMN_NAME = 'companyunkid' ) 
BEGIN
	ALTER TABLE atcfadattribute_mapping ADD companyunkid int
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to store company id.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'atcfadattribute_mapping', @level2type=N'COLUMN',@level2name=N'companyunkid'
END
]]>
</Script>
</Data>
<Data>
<Id>26</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfpasswd_options' AND COLUMN_NAME = 'passwdlastusednumber')
BEGIN
        ALTER TABLE cfpasswd_options ADD passwdlastusednumber INT
        EXECUTE('UPDATE cfpasswd_options SET passwdlastusednumber = 0 WHERE passwdlastusednumber IS NULL')
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to Store Total Number of last Used Passwords' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfpasswd_options', @level2type=N'COLUMN',@level2name=N'passwdlastusednumber'
END
]]>
</Script>
</Data>
<Data>
<Id>27</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfpasswd_options' AND COLUMN_NAME = 'passwdlastuseddays')
BEGIN
        ALTER TABLE cfpasswd_options ADD passwdlastuseddays INT
        EXECUTE('UPDATE cfpasswd_options SET passwdlastuseddays = 0 WHERE passwdlastuseddays IS NULL')
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to Store Total Number of days for last Used Passwords' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfpasswd_options', @level2type=N'COLUMN',@level2name=N'passwdlastuseddays'
END
]]>
</Script>
</Data>
<Data>
<Id>28</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[
DECLARE @t AS TABLE (id int, coyid int)
DECLARE @cid int, @nid int; SET @cid = 1; SET @nid = 0
INSERT INTO @t (id,coyid) SELECT ROW_NUMBER()OVER(ORDER BY companyunkid), companyunkid FROM hrmsConfiguration..cfcompany_master WHERE isactive = 1
WHILE (@cid <= (SELECT MAX(id) FROM @t))
BEGIN
	SET @nid = (SELECT coyid FROM @t WHERE id = @cid)
	IF @nid > 0
	BEGIN
		INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_1','-16744448',@nid WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @nid AND key_name = '_eStat_1')
        INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_2','-16776961',@nid WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @nid AND key_name = '_eStat_2')
        INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_3','-16777216',@nid WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @nid AND key_name = '_eStat_3')
        INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_4','-65536',@nid WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @nid AND key_name = '_eStat_4')
        INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_5','-8388480',@nid WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @nid AND key_name = '_eStat_5')
        INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_6','-16776961',@nid WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @nid AND key_name = '_eStat_6')
        INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_7','-5952982',@nid WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @nid AND key_name = '_eStat_7')
	END
    SET @cid  = @cid  + 1
END
]]>
</Script>
</Data>
<Data>
<Id>29</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_master' AND COLUMN_NAME = 'protocolunkid')
BEGIN
        ALTER TABLE cfcompany_master ADD protocolunkid INT
        EXECUTE('UPDATE cfcompany_master SET protocolunkid = 0 WHERE protocolunkid IS NULL')
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to Store Protocol Id (None=0, TLS=1, SSL3=2, TLS1.2=3)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfcompany_master', @level2type=N'COLUMN',@level2name=N'protocolunkid'
END
]]>
</Script>
</Data>
<Data>
<Id>30</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfemail_setup' AND COLUMN_NAME = 'protocolunkid')
BEGIN
        ALTER TABLE cfemail_setup ADD protocolunkid INT
        EXECUTE('UPDATE cfemail_setup SET protocolunkid = 0 WHERE protocolunkid IS NULL')
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to Store Protocol Id (None=0, TLS=1, SSL3=2, TLS1.2=3)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'protocolunkid'
END
]]>
</Script>
</Data>
<Data>
<Id>31</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>3</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'atcfemail_setup' AND COLUMN_NAME = 'protocolunkid')
BEGIN
        ALTER TABLE atcfemail_setup ADD protocolunkid INT
        EXECUTE('UPDATE atcfemail_setup SET protocolunkid = 0 WHERE protocolunkid IS NULL')
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to Store Protocol Id (None=0, TLS=1, SSL3=2, TLS1.2=3)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'atcfemail_setup', @level2type=N'COLUMN',@level2name=N'protocolunkid'
END
]]>
</Script>
</Data>
</Data>