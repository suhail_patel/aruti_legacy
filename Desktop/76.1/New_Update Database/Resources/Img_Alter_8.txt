<?xml version="1.0" standalone="yes"?>
<Data>
<xs:schema id="Data" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
<xs:element name="Data" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
<xs:complexType>
<xs:choice minOccurs="0" maxOccurs="unbounded">
<xs:element name="Data">
<xs:complexType>
<xs:sequence>
<xs:element name="Id" type="xs:int" minOccurs="0" />
<xs:element name="Version" type="xs:string" minOccurs="0" />
<xs:element name="CheckType" type="xs:int" minOccurs="0" />
<xs:element name="ConditionValue1" type="xs:string" minOccurs="0" />
<xs:element name="ConditionValue2" type="xs:string" minOccurs="0" />
<xs:element name="Script" type="xs:string" minOccurs="0" />
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:choice>
</xs:complexType>
</xs:element>
</xs:schema>
<Data>
<Id>1</Id>
<Version>9.0.70.1</Version>
<CheckType>90701</CheckType>
<ConditionValue1>8</ConditionValue1>
<ConditionValue2 />
<Script>
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'hremployee_images' AND COLUMN_NAME = 'symsignature')
BEGIN
	ALTER TABLE hremployee_images ADD symsignature IMAGE
	EXEC sp_addextendedproperty
		@name = N'MS_Description' ,
		@value = N'Used to Store the Employee Signature From Symmetry System, If integrated.' ,
		@level0type = N'Schema', 
		@level0name = dbo ,
		@level1type = N'Table',
		@level1name = 'hremployee_images' ,
		@level2type = N'Column',
		@level2name = 'symsignature';	
END
</Script>
</Data>
</Data>